* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bol                                                        *
*              Gestione ODL/OCL                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_984]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-27                                                      *
* Last revis.: 2018-08-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Operazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bol",oParentObject,m.Operazione)
return(i_retval)

define class tgsco_bol as StdBatch
  * --- Local variables
  w_FOUND = .f.
  Operazione = space(15)
  TmpC = space(100)
  TmpN = 0
  TmpL = .f.
  TmpD = ctod("  /  /  ")
  TmpN1 = 0
  w_Continua = .f.
  w_TROV = .f.
  w_PRCOSSTA = 0
  w_VACAOVAL = 0
  w_VADECTOT = 0
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_OP = space(1)
  w_MOLT = 0
  w_OP3 = space(1)
  w_MOLT3 = 0
  w_MAG = space(5)
  w_TP = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_CONUMERO = space(15)
  w_CONUMEROOK = space(15)
  w_CONUMROW = 0
  w_OLDLEADTIME = 0
  w_QTALOT = 0
  w_QTAMIN = 0
  w_GIOAPP = 0
  w_CODART = space(20)
  w_CODGRU = space(5)
  w_DATORD = ctod("  /  /  ")
  w_CODFOR = space(15)
  w_OK0 = .f.
  w_OK1 = .f.
  w_QTALOT0 = 0
  w_QTAMIN0 = 0
  w_GIOAPP0 = 0
  w_QTALOT1 = 0
  w_QTAMIN1 = 0
  w_GIOAPP1 = 0
  w_AGGMAT = .f.
  w_ROWORD = 0
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_DATFIL = ctod("  /  /  ")
  w_ARTIPGES = space(1)
  w_FLESPL = space(1)
  w_QTBASE = 0
  w_UMBASE = space(3)
  w_CAUBASE = space(5)
  w_FLO = space(1)
  w_FLI = space(1)
  w_ARTBASE = space(20)
  w_CODBASE = space(20)
  w_NUMLEV = 0
  w_CODCOM = space(20)
  w_DESCOM = space(40)
  w_ARTCOM = space(20)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_COEIMP = 0
  w_OLCODMAG = space(5)
  w_OLMAGPRE = space(5)
  w_OLMAGWIP = space(5)
  QTC = 0
  w_CODICE = space(15)
  w_STATUS = space(1)
  w_KEYSAL = space(20)
  w_QTASAL = 0
  w_CODMAG = space(5)
  w_FLORDI = space(1)
  w_OLDREC = 0
  w_APPO = space(15)
  w_KEYSAL1 = space(20)
  w_QTASAL1 = 0
  w_CODMAG1 = space(5)
  w_FLIMPE = space(1)
  w_TIPPRE = space(1)
  w_CODDIS = space(20)
  w_TIPCON = space(1)
  w_RIFFAS = 0
  w_ULTFAS = 0
  w_DESFAS = space(40)
  QTAZERO = .f.
  w_MESS = space(200)
  w_TIPIMP = space(1)
  w_MAGIMP = space(5)
  w_QTAUM1 = 0
  w_QTAUM3 = 0
  w_QTAUM2 = 0
  w_UNMIS = space(3)
  w_OLDQTSAL = 0
  w_OLDPERAS = space(3)
  w_OLTCOVAR = space(20)
  w_FLSUG = space(1)
  w_FLCON = space(1)
  w_FLDAP = space(1)
  w_FLPIA = space(1)
  w_FLLAN = space(1)
  w_FAMPRO = space(5)
  w_EVAMOD = space(1)
  w_FLORD = space(1)
  w_FLIMP = space(1)
  w_OLDCOMME = space(15)
  w_ARTIC = space(20)
  w_OLDFCM = space(1)
  w_KEYSAL = space(20)
  w_FLCOMM = space(1)
  w_CLCODDIS = space(20)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_COMMOLD = space(15)
  w_DESCOMM = space(40)
  w_D_CODCOM = space(20)
  w_O_CODCOM = space(20)
  w_D_QTAUM1 = 0
  w_O_QTAUM1 = 0
  w_D_CODART = space(20)
  w_O_CODART = space(20)
  w_D_CODMAG = space(5)
  w_O_CODMAG = space(5)
  w_D_FLORDI = space(1)
  w_O_FLORDI = space(1)
  w_D_FLRISE = space(1)
  w_O_FLRISE = space(1)
  w_D_FLIMPE = space(1)
  w_O_FLIMPE = space(1)
  w_D_FLCOMM = space(1)
  w_O_FLCOMM = space(1)
  w_MSG = space(0)
  objCtrl = .NULL.
  w_oMess = .NULL.
  w_oMess1 = .NULL.
  w_oMess2 = .NULL.
  w_oPart = .NULL.
  w_OLDCOM = .f.
  w_QTACHK = .f.
  objCtrl = .NULL.
  GSCO_AOP = .NULL.
  GSCO_MOL = .NULL.
  GSCO_MCL = .NULL.
  GSCO_MCS = .NULL.
  GSCO_MRL = .NULL.
  GSCO_MMO = .NULL.
  w_cFunction = space(10)
  w_CLKEYRIF = space(10)
  w_MFEXT = space(1)
  TmpProveODL = space(20)
  TmpProveART = space(20)
  w_L_CHECK = space(10)
  w_Cur_Fasi_Mod = space(10)
  w_CURRFFAS = 0
  w_INIVAL = ctod("  /  /  ")
  w_FINVAL = ctod("  /  /  ")
  w_cKEY = space(250)
  w_LTIPGES = space(1)
  w_QTAVER = 0
  w_SERODL = space(15)
  w_STAPAD = space(1)
  w_ST = space(1)
  w_PROG = .NULL.
  w_FLVEAC1 = space(1)
  w_CLADOC = space(2)
  w_PAR = space(1)
  w_PROVE = space(1)
  w_MPS = space(1)
  w_DOCSER = space(10)
  w_ROWNUM = 0
  w_STATOODL = space(1)
  w_CODODL = space(15)
  Albero = .NULL.
  w_NODITV = .NULL.
  w_EVENTO = space(30)
  w_DULTIFAS = space(40)
  w_CPULTFAS = 0
  w_PPMATINP = space(1)
  w_PPMATOUP = space(1)
  w_CORLEA = 0
  w_FASMAT = 0
  w_FASSEL = 0
  w_FASCPR = 0
  w_DATFAS = ctod("  /  /  ")
  w_TIPART = space(1)
  w_COROWNUM = 0
  w_DISPAD = space(20)
  w_CPRFAS = 0
  w_PRENET = space(1)
  w_MGPRENET = space(1)
  w_RIGAVUOTA = space(1)
  ROWORD = 0
  w_MOCODRIC = space(20)
  w_MOCODART = space(20)
  w_MOUNIMIS = space(3)
  w_MOCOEIMP = 0
  w_MOCOEIM1 = 0
  w_OLCICLO = space(1)
  w_RICALCOLA = .f.
  w_CHKCONDL = .f.
  w_QTAMAX = 0
  w_WIPFASEPREC = space(20)
  w_WIPFASEOLD = space(20)
  w_CLWIPOUT = space(20)
  w_CLWIPFPR = space(20)
  w_LRROWNUM = 0
  w_COD_CLA = space(5)
  w_COD_RISO = space(5)
  w_LRUMTATT = space(5)
  w_LRUMTAVV = space(5)
  w_LRUMTLAV = space(5)
  w_LRATTTEM = 0
  w_LRAVVTEM = 0
  w_LRLAVTEM = 0
  w_LRATTSEC = 0
  w_LRAVVSEC = 0
  w_LRLAVSEC = 0
  w_LRNUMIMP = 0
  w_LRPROORA = 0
  w_TIPO = space(2)
  w_UMTDEF = space(5)
  w_CONATT = 0
  w_CONAVV = 0
  w_CONLAV = 0
  w_QTARIS = 0
  w_LR__TIPO = space(2)
  w_LRTEMRIS = 0
  w_LRUMTRIS = space(5)
  w_LRTEMSEC = 0
  w_DES_RISO = space(40)
  w_RLINTEST = space(1)
  w_CODCENTRO = space(20)
  w_STOP = .f.
  w_NFASI = 0
  TmpN3 = 0
  TmpN4 = 0
  w_TEMP5 = space(1)
  m_ODLCUR = space(15)
  m_OLTSTATO = space(1)
  m_OLTCOFOR = space(15)
  m_MAGTER = space(5)
  m_OLTCOMAG = space(5)
  m_OLTEMLAV = 0
  m_OLTDINRIC = ctod("  /  /  ")
  m_OLTDTRIC = ctod("  /  /  ")
  m_OLTCOMME = space(15)
  m_OLTCOATT = space(15)
  m_OLTCODIC = space(41)
  m_CLROWNUM = 0
  m_CLROWORD = 0
  m_OLTUNMIS = space(5)
  m_OLTQTODL = 0
  m_DL__GUID = space(10)
  m_DATORD = ctod("  /  /  ")
  m_CALPROM = .f.
  m_PRDESSUP = space(1)
  w_CALPRZ = 0
  m_CALPRZ = 0
  m_OLTPROVE = space(1)
  w_PESERORD = space(10)
  w_PEQTAABB = 0
  w_RESABB = 0
  w_TOTABB = 0
  w_PROGR = 0
  w_RLTPRLAV = 0
  w_RLTPREVS = 0
  w_RLTPSLAV = 0
  w_RLTPSTDS = 0
  w_CAMBIODATA = .f.
  w_CAMBIOQTA = .f.
  w_NEWDATA = ctod("  /  /  ")
  w_NEWQTA = 0
  w_COCORLEA = 0
  TmpN2 = 0
  w_CODODLFA = space(15)
  w_ROWORD = 0
  w_FLUPD = space(1)
  w_FLCOMD = space(1)
  w_CLFLCOMM = space(1)
  w_CODCOM = space(15)
  w_CLTFAODL = 0
  w_FLRIS = space(1)
  w_MAXFASE = 0
  w_TMPCODODL = space(15)
  w_TMPCPR = 0
  w_TMPCPRIFE = 0
  w_BCHKFASE = .f.
  w_RL__TIPO = space(2)
  w_RLMAGWIP = space(5)
  w_CPROWNUM = 0
  w_ARMAGPRE = space(5)
  w_ARMAGIMP = space(5)
  w_OLMAGWIP = space(5)
  w_OLCODMAG = space(5)
  w_OLDMAGA = space(5)
  w_OLDQTSAL = 0
  w_OLMODPRE = space(1)
  w_OLKEYSAL = space(40)
  w_OLCODART = space(20)
  w_bTrsDontDisplayErr = .f.
  w_OLMAGPRE = space(5)
  w_OLCODMAG = space(5)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_WARN = .f.
  w_PARMSG1 = space(20)
  w_PARMSG2 = space(20)
  w_oPART1 = .NULL.
  w_oPART2 = .NULL.
  w_nPag = 0
  w_ANNULLA = .f.
  w_MEMO = space(0)
  w_NOBLOC = space(0)
  w_DAIM = .f.
  w_OK = .f.
  w_DATOBSO = ctod("  /  /  ")
  w_ROWMESS = space(40)
  w_CONFKME = .f.
  m_OLDTOBSO = ctod("  /  /  ")
  m_OLDATRIC = space(5)
  m_CPROWORD = 0
  m_OLCODICE = space(41)
  m_OLFASRIF = 0
  m_OLQTAMOV = 0
  m_OLQTAUM1 = 0
  w_MATNOFASE = 0
  w_ULTIMAFASE = 0
  w_DULTIFASE = space(40)
  w_CPULTFASE = 0
  Curso = space(10)
  MAXFAS = 0
  MAXFAS1 = 0
  w_NODATE = .f.
  w_NOCLDES = .f.
  w_oPART = .NULL.
  w_NFASI = 0
  w_MESS = space(10)
  w_OPOS = 0
  w_TRSNAME = space(15)
  w_CLROWORD = 0
  o_CLROWORD = 0
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_WIPFASPRE = space(5)
  w_WIPFASOLD = space(5)
  w_PRIMAFASINT = .f.
  w_CPPRIMAFASINT = 0
  w_FAPRIMAFASINT = 0
  w_WPRIMAFASINT = space(5)
  w_CPFASEEXT = 0
  w_FAFASEEXT = 0
  w_WIPFASEEXT = space(5)
  w_OINTEXT = space(1)
  w_NUMFAS = 0
  w_OLDCP = space(1)
  w_OLDOU = space(1)
  w_PRIMOGIRO = .f.
  w_CURRFFAS = 0
  w_CLINDPRE = space(2)
  w_CLFASSEL = space(2)
  o_CLFASSEL = space(2)
  w_CLROWORD = 0
  o_CLROWORD = 0
  w_CLFASSEP = space(1)
  w_CLQTAPRE = 0
  o_CLINDPRE = space(2)
  w_CLCOUPOI = space(1)
  o_CLCOUPOI = space(1)
  w_CLFASOUT = space(1)
  o_CLFASOUT = space(1)
  w_L_CLROWORD = 0
  w_CLCODODL = space(15)
  w_CLSERIAL = space(10)
  w_LockScreen = .f.
  w_TrsRiso = space(10)
  MAXFAS = 0
  MAXFAS1 = 0
  w_CHF1 = .f.
  w_CHF2 = .f.
  Curso = space(10)
  w_EXTCERR = .f.
  w_EXTCONS = .f.
  w_LockScreen = .f.
  w_TrsRiso = space(10)
  w_RLMAGWIP = space(5)
  w_SRV = space(1)
  w_CLFASCLA = space(1)
  w_PROFASMO = .f.
  w_TIPOMODIFICA = space(20)
  w_RECNO = 0
  w_FOUND = .f.
  w_L_OLWIPFPR = space(41)
  w_L_CLWIPFPR = space(41)
  w_L_CLCODFAS = space(41)
  w_CLINDCIC = space(2)
  w_nRecFasi = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CON_TRAD_idx=0
  CON_TRAM_idx=0
  KEY_ARTI_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_RIOR_idx=0
  VALUTE_idx=0
  SALDIART_idx=0
  CONTI_idx=0
  TAB_CICL_idx=0
  DIC_PROD_idx=0
  RIF_GODL_idx=0
  MAGAZZIN_idx=0
  TAB_RISO_idx=0
  MPS_TFOR_idx=0
  SALDICOM_idx=0
  RIS_ORSE_idx=0
  PAR_PROD_idx=0
  CLR_MAST_idx=0
  CIC_DETT_idx=0
  CIC_MAST_idx=0
  ODLMRISO_idx=0
  ODL_RISO_idx=0
  ODL_CICL_idx=0
  UNIMIS_idx=0
  ODL_MAIN_idx=0
  ODL_RISF_idx=0
  TMPODL_CICL_idx=0
  TMPODL_RISO_idx=0
  ART_TEMP_idx=0
  PEG_SELI_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione di GSCO_AOP 
    DIMENSION QTC[99,2]
    * --- -------------------------------------------------------
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_message")
    this.w_oMess1=createobject("ah_message")
    this.w_oMess2=createobject("ah_message")
    this.Operazione = alltrim(this.Operazione)
    * --- Tipo gestione commessa
    if VARTYPE(g_OLDCOM)="L" AND g_OLDCOM
      this.w_OLDCOM = .t.
    else
      this.w_OLDCOM = .f.
    endif
    * --- Puntatore a padre
    * --- Cursore movimentazione
    Private nc, cOC,tc,mc, rc, ts
    this.GSCO_AOP = this.oParentObject
    if Vartype(this.GSCO_AOP.GSCO_MOL)="O"
      this.GSCO_MOL = this.GSCO_AOP.GSCO_MOL
      nc = this.GSCO_MOL.cTrsName
      this.GSCO_MCS = this.GSCO_AOP.GSCO_MCS
      ts = this.GSCO_MCS.cTrsName
      this.GSCO_MMO = this.GSCO_AOP.GSCO_MMO
      mo = this.GSCO_MMO.cTrsName
      this.GSCO_MCL = this.GSCO_AOP.GSCO_MCL
      tc = this.GSCO_MCL.cTrsName
      this.GSCO_MRL = this.GSCO_MCL.GSCO_MRL
      rc = this.GSCO_MRL.cTrsName
    endif
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Recupero stato della form
    this.w_cFunction = Upper(Alltrim(this.GSCO_AOP.cFunction))
    this.w_CLKEYRIF = this.GSCO_AOP.w_CLKEYRIF
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.QTAZERO = .F.
    this.w_EVAMOD = "N"
    * --- Create temporary table TMPODL_CICL
    if !cp_ExistTableDef('TMPODL_CICL')
      i_nIdx=cp_AddTableDef('TMPODL_CICL',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
            )
      this.TMPODL_CICL_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.TMPODL_CICL_idx=0
        this.TMPODL_CICL_idx=cp_ascan(@i_TableProp,lower('TMPODL_CICL'),i_nTables)
      endif
    endif
    * --- Create temporary table TMPODL_RISO
    if !cp_ExistTableDef('TMPODL_RISO')
      i_nIdx=cp_AddTableDef('TMPODL_RISO',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_RISO_proto';
            )
      this.TMPODL_RISO_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.TMPODL_RISO_idx=0
        this.TMPODL_RISO_idx=cp_ascan(@i_TableProp,lower('TMPODL_RISO'),i_nTables)
      endif
    endif
    do case
      case this.Operazione = "TITOLO"
        if g_PROD<>"S" or (g_COLA<>"S" AND this.oParentObject.w_TIPGES="Z") or (g_MMPS<>"S" AND this.oParentObject.w_TIPGES $ "PFT")
          ah_ErrorMsg("Modulo non installato, impossibile proseguire",,"")
          this.oParentObject.ecpQuit()
          Keyboard "{ESC}"
          i_retcode = 'stop'
          return
        endif
        * --- Assegna titolo a gestione
        do case
          case this.oParentObject.w_TIPGES="L"
            this.GSCO_AOP.cComment = ah_Msgformat("Gestione ordini di lavorazione (ODL)")
          case this.oParentObject.w_TIPGES="Z"
            this.GSCO_AOP.cComment = ah_Msgformat("Gestione ordini di conto lavoro (OCL)")
          case this.oParentObject.w_TIPGES="P"
            this.GSCO_AOP.cComment = ah_Msgformat("Gestione ordini di produzione (ODP)")
          case this.oParentObject.w_TIPGES="F"
            this.GSCO_AOP.cComment = ah_Msgformat("Gestione ordini di produzione (Ricambi)")
          case this.oParentObject.w_TIPGES="T"
            this.GSCO_AOP.cComment = ah_Msgformat("Gestione proposte ordini fornitore (ODF)")
          otherwise
            this.GSCO_AOP.cComment = ah_Msgformat("Gestione ordini di acquisto (ODA)")
        endcase
        if g_PRFA="S" AND g_CICLILAV="S" and this.oParentObject.w_TIPGES $ "LZP"
          * --- Uso zoom diverso se ho abilitato i cicli di lavorazione (visualizzo codice ODL padre degli ODL di fase)
          this.GSCO_AOP.cAutozoom = "GSCI"+this.oParentObject.w_TIPGES+ "AOP"
        else
          this.GSCO_AOP.cAutozoom = "GSCO"+this.oParentObject.w_TIPGES+ "AOP"
        endif
        this.GSCO_AOP.SetCaption()     
        * --- Durante la INIT non eseguire la mcalc sul padre (rende editabili i campi con condiz. edit anche se falsa)
        this.bUpdateParentObject=.F.
      case this.Operazione="DATAINIVAR" and !empty(this.oParentObject.w_OLTDINRIC) and this.oParentObject.w_OLTDINRIC<>this.oParentObject.o_OLTDINRIC or this.Operazione="DATAFINVAR" and !empty(this.oParentObject.w_OLTDTRIC) and this.oParentObject.w_OLTDTRIC<>this.oParentObject.o_OLTDTRIC
        if not empty(this.oParentObject.w_OLTQTOD1)
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT(this.w_STOP AND this.w_WARN) OR (!this.w_STOP AND this.w_WARN and this.oParentObject.w_DELOCLFA)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
              this.oParentObject.o_OLTPROVE = this.oParentObject.w_OLTPROVE
              this.w_MFEXT = "N"
              * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno, da trattare come salto codice
              this.Page_16()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_nRecFasi=0 Or (this.w_MFEXT="S" AND this.oParentObject.o_OLTPROVE <> this.oParentObject.w_OLTPROVE)
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
        endif
      case this.Operazione="DATACONVAR" and !empty(this.oParentObject.w_OLTDTCON) and this.oParentObject.w_OLTDTCON<>this.oParentObject.o_OLTDTCON
        if not empty(this.oParentObject.w_OLTQTOD1)
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT(this.w_STOP AND this.w_WARN) OR (!this.w_STOP AND this.w_WARN and this.oParentObject.w_DELOCLFA)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
              this.oParentObject.o_OLTPROVE = this.oParentObject.w_OLTPROVE
              this.w_MFEXT = "N"
              * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno, da trattare come salto codice
              this.Page_16()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_nRecFasi=0 Or (this.w_MFEXT="S" AND this.oParentObject.o_OLTPROVE <> this.oParentObject.w_OLTPROVE)
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
        else
          if this.oParentObject.w_OGGMPS="N"
            * --- Legge LT Sicurezza
            this.oParentObject.w_OLTDTRIC = COCALCLT(this.oParentObject.w_OLTDTCON,this.oParentObject.w_OLTSAFLT,"I", .F. ,"")
          else
            this.oParentObject.w_OLTDTRIC = this.oParentObject.w_OLTDTCON
          endif
        endif
      case this.Operazione = "CONTROLLI"
        do case
          case this.oParentObject.w_OLTSTATO = "F"
            this.TmpC = "Impossibile variare un %1"
            ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
            this.oParentObject.w_TESTFORM = .F.
          otherwise
            this.oParentObject.w_OLTCAMAG = this.oParentObject.w_CAUORD
            this.oParentObject.w_OLTFLORD = IIF(EMPTY(this.oParentObject.w_OLTCAMAG), " ", this.oParentObject.w_AFLORDI)
            this.oParentObject.w_OLTFLIMP = IIF(EMPTY(this.oParentObject.w_OLTCAMAG), " ", this.oParentObject.w_AFLIMPE)
            this.w_FLCOMM = IIF(EMPTY(this.oParentObject.w_OLTCAMAG), " ", this.oParentObject.w_AFLCOMM)
            this.oParentObject.w_OLTFORCO = IIF(EMPTY(this.oParentObject.w_OLTCAMAG), " ", this.oParentObject.w_AFORCO)
            this.oParentObject.w_OLTFCOCO = IIF(EMPTY(this.oParentObject.w_OLTCAMAG), " ", this.oParentObject.w_AFCOCO)
        endcase
        if this.w_cFunction="LOAD" and EMPTY(this.oParentObject.w_OLTDISBA) and this.oParentObject.w_ARTIPART="FS" 
          * --- Non � consentito caricare ordini di fase manualmente se non si inserisce un codice distinta valido
          this.oParentObject.w_TESTFORM = .F.
          this.TmpC = "Impossibile caricare un %1 di fase senza distinta base"
          ah_ErrorMsg(this.TmpC,48,"",COSTRODL("TIPOORD",this.oParentObject.w_TIPGES," "))
        endif
        if this.oParentObject.w_TESTFORM and this.oParentObject.w_OLTSTATO = "L" and this.oParentObject.w_OLTQTOEV+this.oParentObject.w_OLTQTOSC>0
          this.oParentObject.w_TESTFORM = .F.
          this.TmpC = "Impossibile variare un %1 dichiarato"
          ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
        endif
        do case
          case this.oParentObject.w_TIPGES $ "LZ"
            if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and this.oParentObject.w_OLTQTOEV+this.oParentObject.w_OLTQTOSC>0 and this.oParentObject.w_OLFLMODC<>"S" and not empty(this.oParentObject.w_OLTSECIC)
              this.oParentObject.w_HASEVENT = FALSE
              this.TmpC = "Impossibile variare un %1 dichiarato che non ha il flag consenti modifica fasi attivo"
              ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
            endif
        endcase
        if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and not empty(this.oParentObject.w_OLTSECIC)
          * --- Verifica se � stata avanzata qualche fase del ciclo
          * --- Select from ODL_CICL
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SUM(CLAVAUM1+CLSCAUM1) AS QTAVER  from "+i_cTable+" ODL_CICL ";
                +" where CLCODODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+"";
                 ,"_Curs_ODL_CICL")
          else
            select SUM(CLAVAUM1+CLSCAUM1) AS QTAVER from (i_cTable);
             where CLCODODL=this.oParentObject.w_OLCODODL;
              into cursor _Curs_ODL_CICL
          endif
          if used('_Curs_ODL_CICL')
            select _Curs_ODL_CICL
            locate for 1=1
            do while not(eof())
            this.w_QTAVER = _Curs_ODL_CICL.QTAVER
              select _Curs_ODL_CICL
              continue
            enddo
            use
          endif
          do case
            case this.oParentObject.w_TIPGES $ "LZ"
              if this.w_QTAVER>0 and this.oParentObject.w_OLFLMODC<>"S"
                this.oParentObject.w_HASEVENT = FALSE
                this.TmpC = "Impossibile variare un %1 dichiarato che non ha il flag consenti modifica fasi attivo"
                ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
              endif
            otherwise
              if this.w_QTAVER>0
                this.oParentObject.w_HASEVENT = FALSE
                this.TmpC = "Impossibile variare un %1 dichiarato"
                ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
              endif
          endcase
        endif
        * --- Verifica se esistono OCL collegati
        if this.oParentObject.w_TESTFORM and g_PRFA="S" and not empty(this.oParentObject.w_OLTSECIC) and this.oParentObject.w_OLTPROVE="I" and .f.
          this.w_STOP = FALSE
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
                +" where OLTSEODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+" and OLTSTATO='L'";
                 ,"_Curs_ODL_MAST")
          else
            select * from (i_cTable);
             where OLTSEODL=this.oParentObject.w_OLCODODL and OLTSTATO="L";
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            this.w_STOP = TRUE
            exit
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
          if this.w_STOP
            this.oParentObject.w_TESTFORM = FALSE
            this.TmpC = "Attenzione:%0Sono presenti ordini di conto lavoro (OCL) collegati alle fasi del ciclo corrente per i quali sono gi� stati generati ordini a fornitore%0Impossibile variare l'ordine corrente"
            ah_ErrorMsg(this.TmpC,48)
          endif
        endif
        if this.oParentObject.w_TESTFORM and g_COLA="S" and this.oParentObject.w_OLTPROVE="L" and this.oParentObject.w_ARTIPART="FS" and this.oParentObject.w_OLTSTATO $ "L-F"
          this.TmpC = "OCL di fase. Impossibile variare%0Utilizzare l'apposita scelta di men� per variare il terzista"
          ah_ErrorMsg(this.TmpC,48)
          this.oParentObject.w_TESTFORM = FALSE
        endif
        if this.oParentObject.w_TESTFORM and g_COLA="S" and this.oParentObject.w_OLTPROVE="I" and this.oParentObject.w_ARTIPART="FS" and this.oParentObject.w_OLTSTATO $ "L-F"
          this.TmpC = "Oedine di fase. Impossibile variare"
          ah_ErrorMsg(this.TmpC,48)
          this.oParentObject.w_TESTFORM = FALSE
        endif
        if this.oParentObject.w_TESTFORM and (g_PRFA="S" and g_CICLILAV="S") and (not this.oParentObject.w_OLTSTATO $ "SC") and empty(this.oParentObject.w_OLTSECIC) and (not this.oParentObject.w_OLTPROVE = "E")
          * --- Gli ordini in stato 'DA PIANIFICARE' in su, se sono riferiti ad un materiale che ha il ciclo, lo devono avere
          this.TmpC = " "
          * --- Read from CIC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CIC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLSERIAL"+;
              " from "+i_cTable+" CIC_MAST where ";
                  +"CLCODDIS = "+cp_ToStrODBC(this.oParentObject.w_OLTCODIC);
                  +" and CLINDCIC = "+cp_ToStrODBC("00");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLSERIAL;
              from (i_cTable) where;
                  CLCODDIS = this.oParentObject.w_OLTCODIC;
                  and CLINDCIC = "00";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.TmpC = NVL(cp_ToDate(_read_.CLSERIAL),cp_NullValue(_read_.CLSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se ho il ciclo preferenziale l'inserimento del ciclo stesso � obbligatorio, sempre che abbia almeno una fase valida alla data richiesta
          if not empty(this.TmpC)
            * --- Se ho il ciclo preferenziale l'inserimento del ciclo stesso � obbligatorio, sempre che abbia almeno una fase valida alla data richiesta
            * --- Read from CIC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CIC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2],.t.,this.CIC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLINIVAL,CLFINVAL"+;
                " from "+i_cTable+" CIC_DETT where ";
                    +"CLSERIAL = "+cp_ToStrODBC(this.TmpC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLINIVAL,CLFINVAL;
                from (i_cTable) where;
                    CLSERIAL = this.TmpC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_INIVAL = NVL(cp_ToDate(_read_.CLINIVAL),cp_NullValue(_read_.CLINIVAL))
              this.w_FINVAL = NVL(cp_ToDate(_read_.CLFINVAL),cp_NullValue(_read_.CLFINVAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_INIVAL<=this.oParentObject.w_OLTDINRIC and this.oParentObject.w_OLTDINRIC<=this.w_FINVAL
              this.TmpC = "%1 riferito a materiale con ciclo di lavorazione%0Occorre indicare un ciclo"
              ah_ErrorMsg(this.TmpC,48,"",alltrim(COSTRODL("TIPOORD",this.oParentObject.w_TIPGES)))
              this.oParentObject.w_TESTFORM = FALSE
            else
              this.TmpC = "Attenzione, %1 riferito a materiale con ciclo di lavorazione preferenziale non valido alla data selezionata%0Sar� comunque possibile proseguire il salvataggio"
              ah_ErrorMsg(this.TmpC,48,"",alltrim(COSTRODL("TIPOORD",this.oParentObject.w_TIPGES)))
            endif
          else
            * --- Read from CIC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CIC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLSERIAL"+;
                " from "+i_cTable+" CIC_MAST where ";
                    +"CLCODDIS = "+cp_ToStrODBC(this.oParentObject.w_OLTCODIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLSERIAL;
                from (i_cTable) where;
                    CLCODDIS = this.oParentObject.w_OLTCODIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.TmpC = NVL(cp_ToDate(_read_.CLSERIAL),cp_NullValue(_read_.CLSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if not empty(this.TmpC)
              this.TmpC = "Attenzione, %1 riferito a materiale con ciclo di lavorazione privo di ciclo preferenziale%0Sar� comunque possibile proseguire il salvataggio"
              ah_ErrorMsg(this.TmpC,48,"",alltrim(COSTRODL("TIPOORD",this.oParentObject.w_TIPGES)))
            endif
          endif
        endif
        if this.oParentObject.w_TESTFORM and this.oParentObject.w_OLTSTATO $ "MPL" and this.oParentObject.w_OLTPROVE<>"E" and this.oParentObject.w_ARTIPART<>"FS"
          * --- Verifica se nel cursore della movimentazione c'� gia' qualcosa, nel qual caso segnala l'azzeramento
          SELECT (nc)
          COUNT to this.TmpN for (not empty( &nc..t_OLCODICE ))
          if this.tmpN = 0
            ah_ErrorMsg("Non � stata caricata la lista dei materiali stimati%0Impossibile salvare l'ODL",,"")
            this.oParentObject.w_TESTFORM = .F.
          endif
        endif
        if this.oParentObject.w_TESTFORM and empty(this.oParentObject.w_OLTUNMIS)
          this.TmpC = "Occorre indicare l'unit� di misura dell'ordine"
          ah_ErrorMsg(this.TmpC,48)
          this.oParentObject.w_TESTFORM = FALSE
        endif
        if this.oParentObject.w_TESTFORM and ! empty(this.oParentObject.w_OLTUNMIS)
          * --- Verifica Congruenza Unit� di Misura Selezionata
          if this.oParentObject.w_OLTUNMIS="***"
            * --- Fuori Magazzino, Accetta Sempre
          else
            if this.oParentObject.w_OLTUNMIS=this.oParentObject.w_UNMIS1 OR (this.oParentObject.w_OLTUNMIS=this.oParentObject.w_UNMIS2 AND NOT EMPTY(this.oParentObject.w_UNMIS2)) OR (this.oParentObject.w_OLTUNMIS=this.oParentObject.w_UNMIS3 AND NOT EMPTY(this.oParentObject.w_UNMIS3))
              * --- L'U.M. scelta � accettabile
            else
              * --- Il messaggio di errore viene visualizzato direttamente dal check su w_OLTUNMIS sulla maschera padre, qui impedisco solo il salvataggio
              this.oParentObject.w_TESTFORM = FALSE
            endif
          endif
        endif
        if this.oParentObject.w_TESTFORM and g_COLA="S" and this.oParentObject.w_OLTPROVE="L" and empty(this.oParentObject.w_OLTCOFOR)
          * --- Test Fornitore Obbligatorio
          this.TmpC = "Fornitore di conto lavoro non impostato!"
          ah_ErrorMsg(this.TmpC,48)
          this.oParentObject.w_TESTFORM = FALSE
          this.objCtrl = this.GSCO_AOP.getCtrl("w_OLTCOFOR")
          if type("this.objCtrl") = "O" and Not isnull(this.objCtrl)
            this.objCtrl.SetFocus()
            this.objCtrl = .null.
          endif
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_TESTFORM and this.oParentObject.w_OLTSTATO $ "MPL" and !(g_PRFA="S" and g_CICLILAV="S" and this.oParentObject.w_HAILCIC) and this.oParentObject.w_OLTPROVE<>this.oParentObject.w_ARPROPRE
          * --- Notifica all'utente che la provenienza impostata sull'ODL � diversa dalla preferenziale articolo
          this.TmpProveODL = "(" + IIF(this.oParentObject.w_OLTPROVE="I", "Interna", IIF(this.oParentObject.w_OLTPROVE="E", "Esterna", IIF(this.oParentObject.w_OLTPROVE="L", "Conto lavoro", ""))) + ")"
          this.TmpProveART = "(" + IIF(this.oParentObject.w_ARPROPRE="I", "Interna", IIF(this.oParentObject.w_ARPROPRE="E", "Esterna", IIF(this.oParentObject.w_ARPROPRE="L", "Conto lavoro", ""))) + ")"
          this.w_oPart = this.w_oMess.addMsgPart("La provenienza preferenziale dell'articolo �")
          this.w_oPart = this.w_oMess.addMsgPartNL(this.TmpProveART)
          this.w_oPart = this.w_oMess.addMsgPart("La provenienza Impostata sull'ordine �")
          this.w_oPart = this.w_oMess.addMsgPartNL(this.TmpProveODL)
          this.w_oPart = this.w_oMess.addMsgPartNL("Confermi l'impostazione fatta?")
          this.oParentObject.w_TESTFORM = this.w_oMess.ah_Yesno()
        endif
        * --- Verifica obsolescienza codice di ricerca articolo (Testata)        
        * --- Verifica Validit� Articolo
        if this.oParentObject.w_TESTFORM AND not empty(this.oParentObject.w_OLTCODIC) and inlist(this.GSCO_AOP.cFunction,"Load", "Edit")
          this.oParentObject.w_TESTFORM = CHKDTOBS(this.oParentObject.w_ARDTOBSO,this.oParentObject.w_OLTDTRIC)
          if Not this.oParentObject.w_TESTFORM
            if not ah_YesNo("Attenzione%0Articolo obsoleto dal %1%0Si desidera continuare?","", DTOC(this.oParentObject.w_ARDTOBSO) )
              this.oParentObject.w_TESTFORM = .F.
            else
              this.oParentObject.w_TESTFORM = .T.
            endif
          endif
        endif
        * --- Test obsolescienza codici di ricerca (Dettaglio)
        if this.oParentObject.w_TESTFORM=.T. and INLIST(this.GSCO_AOP.cFunction, "Load", "Edit")
          this.w_L_CHECK = SYS(2015)
          this.w_Cur_Fasi_Mod = SYS(2015)
          use in select(this.w_Cur_Fasi_Mod)
          CREATE CURSOR (this.w_Cur_Fasi_Mod) (NumeroFase N(6))
          =wrcursor( this.w_Cur_Fasi_Mod )
          if !Empty(this.GSCO_AOP.w_OLTSECIC) and this.oParentObject.w_TIPGES $ "LZ"
            this.Page_14()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if ! this.oParentObject.w_TESTFORM
              this.w_MEMO = this.w_oMess.ComposeMessage()
            endif
          endif
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          * --- Al termine dei controlli se ci sono stati messaggi di errore li visualizzo
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          this.w_MEMO = alltrim(this.w_MEMO) + IIF( !Empty(alltrim(nvl(this.w_MEMO, ""))), chr(13)+chr(10), "") + this.w_oMess2.ComposeMessage()
          this.w_NOBLOC = this.w_oMess1.ComposeMessage()
          if ! Empty (this.w_MEMO) Or !Empty(this.w_NOBLOC)
            * --- Visualizzo la maschera con all'interno tutti i messagi di errore
            this.w_ANNULLA = False
            this.w_CONFKME = False
            this.w_DAIM = False
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CONFKME = this.w_ANNULLA
          else
            if this.oParentObject.w_TESTFORM
              this.w_CONFKME = True
            else
              this.w_CONFKME = False
            endif
          endif
          this.oParentObject.w_TESTFORM = this.w_CONFKME
          if Not this.oParentObject.w_TESTFORM And Not Empty(this.GSCO_AOP.w_Cur_Del_Ocl) and used(this.GSCO_AOP.w_Cur_Del_Ocl)
            * --- Non proseguo tolgo dal cursore gli OCL inseriti dalla ceckform
            delete from (this.GSCO_AOP.w_Cur_Del_Ocl) where TIPO="C" and CCHECK=this.w_L_CHECK
          endif
          if this.oParentObject.w_TESTFORM and used( this.w_Cur_Fasi_Mod )
            this.w_ULTIMAFASE = 0
            this.w_DULTIFASE = " "
            this.w_CPULTFASE = 0
            * --- Select from TMPODL_CICL
            i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CLROWORD, CLDESFAS  from "+i_cTable+" TMPODL_CICL ";
                  +" where CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL)+" And CLFASSEL='S' And CLULTFAS='S' AND CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF)+"";
                  +" order by CLROWORD";
                   ,"_Curs_TMPODL_CICL")
            else
              select CLROWORD, CLDESFAS from (i_cTable);
               where CLCODODL = this.w_CLCODODL And CLFASSEL="S" And CLULTFAS="S" AND CLKEYRIF = this.w_CLKEYRIF;
               order by CLROWORD;
                into cursor _Curs_TMPODL_CICL
            endif
            if used('_Curs_TMPODL_CICL')
              select _Curs_TMPODL_CICL
              locate for 1=1
              do while not(eof())
              * --- Determino fase in base ai parametri
              this.w_ULTIMAFASE = _Curs_TMPODL_CICL.CLROWORD
              this.w_DULTIFASE = _Curs_TMPODL_CICL.CLDESFAS
              if this.w_PPMATINP="P"
                exit
              endif
                select _Curs_TMPODL_CICL
                continue
              enddo
              use
            endif
            SELECT (this.w_Cur_Fasi_Mod )
            GO TOP
            SCAN
            this.w_CURRFFAS = NVL(NumeroFase, 0)
            * --- A questo punto libero i materiali presenti sull'ordine associati alla fase di conto lavoro
            this.GSCO_MOL.MarkPos()     
            this.GSCO_MOL.FirstRow()     
            do while Not this.GSCO_MOL.Eof_Trs()
              this.GSCO_MOL.SetRow()     
              this.GSCO_MOL.SaveDependsOn()     
              if this.GSCO_MOL.FullRow() And this.GSCO_MOL.w_OLEVAAUT="S" And this.GSCO_MOL.w_OLFASRIF=this.w_CURRFFAS and INLIST(this.GSCO_MOL.RowStatus() , "U" , "A")
                this.GSCO_MOL.w_OLQTAEVA = 0
                this.GSCO_MOL.w_OLQTAEV1 = 0
                this.GSCO_MOL.w_OLFLEVAS = "N"
                this.GSCO_MOL.w_OLEVAAUT = "N"
                this.GSCO_MOL.w_OLQTASAL = this.GSCO_MOL.w_OLQTAUM1
                if this.GSCO_MOL.w_OLFASRIF=0 
                  this.GSCO_MOL.w_OLFASRIF = this.w_ULTIMAFASE
                  this.GSCO_MOL.w_DESFAS = this.w_DULTIFASE
                endif
                this.GSCO_MOL.mCalc(.t.)     
                this.GSCO_MOL.SaveRow()     
              endif
              this.GSCO_MOL.NextRow()     
            enddo
            this.GSCO_MOL.RePos(.F.)     
            SELECT (this.w_Cur_Fasi_Mod )
            ENDSCAN
          endif
          use in select(this.w_Cur_Fasi_Mod)
          * --- --------------------------------------------------------------------------------------------------
        endif
      case this.Operazione = "DEL<=END"
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Operazione = "CARICA_DETT"
        * --- CARICA DETTAGLIO
        if EMPTY(this.oParentObject.w_OLTCOMAG)
          ah_ErrorMsg("Codice magazzino produzione non definito",,"")
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_MAGWIP) AND this.oParentObject.w_GESWIP="S"
          ah_ErrorMsg("Codice magazzino WIP non definito nei parametri",,"")
          i_retcode = 'stop'
          return
        endif
        * --- CARICA DETTAGLIO
        * --- Verifica se il ciclo e' richiesto
        * --- Nel caso in cui sia attivo il flag caricamento automatico ciclo il controllo viene spostato nel GSDB_BTC
        if .F.
          if (g_PRFA="S" and g_CICLILAV="S") and empty(this.oParentObject.w_OLTSECIC) AND this.oParentObject.w_TIPGES<>"S" and w_OLCARAUC<>"S"
            this.TmpC = " "
            * --- Read from CIC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CIC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLSERIAL"+;
                " from "+i_cTable+" CIC_MAST where ";
                    +"CLCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                    +" and CLCODDIS = "+cp_ToStrODBC(this.oParentObject.w_OLTDISBA);
                    +" and CLINDCIC = "+cp_ToStrODBC("00");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLSERIAL;
                from (i_cTable) where;
                    CLCODART = this.oParentObject.w_OLTCOART;
                    and CLCODDIS = this.oParentObject.w_OLTDISBA;
                    and CLINDCIC = "00";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.TmpC = NVL(cp_ToDate(_read_.CLSERIAL),cp_NullValue(_read_.CLSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if not empty(this.TmpC)
              * --- Se ho il ciclo preferenziale l'inserimento del ciclo stesso � obbligatorio, sempre che abbia almeno una fase valida alla data richiesta
              * --- Read from CIC_DETT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CIC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2],.t.,this.CIC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CLINIVAL,CLFINVAL"+;
                  " from "+i_cTable+" CIC_DETT where ";
                      +"CLSERIAL = "+cp_ToStrODBC(this.TmpC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CLINIVAL,CLFINVAL;
                  from (i_cTable) where;
                      CLSERIAL = this.TmpC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_INIVAL = NVL(cp_ToDate(_read_.CLINIVAL),cp_NullValue(_read_.CLINIVAL))
                this.w_FINVAL = NVL(cp_ToDate(_read_.CLFINVAL),cp_NullValue(_read_.CLFINVAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_INIVAL<=this.oParentObject.w_OLTDINRIC and this.oParentObject.w_OLTDINRIC<=this.w_FINVAL
                this.TmpC = "%1 riferito a materiale con ciclo di lavorazione%0Occorre indicare un ciclo"
                ah_ErrorMsg(this.TmpC,48,"",alltrim(PRDSTROL("TIPOORD",this.oParentObject.w_TIPGES)))
                i_retcode = 'stop'
                return
              endif
            endif
          endif
        endif
        if this.oParentObject.w_OLTSTATO $ "MPL" and this.oParentObject.w_OLTPROVE="L" and empty(this.oParentObject.w_OLTCOFOR) and this.oParentObject.w_TIPGES<>"S"
          this.TmpC = " "
          * --- Test Fornitore Obbligatorio
          this.TmpC = "Fornitore di conto lavoro non impostato!"
          ah_ErrorMsg(this.TmpC,48)
          this.objCtrl = this.GSCO_AOP.getCtrl("w_OLTCOFOR")
          if type("this.objCtrl") = "O" and Not isnull(this.objCtrl)
            this.objCtrl.SetFocus()
            this.objCtrl = .null.
          endif
          i_retcode = 'stop'
          return
        endif
        * --- Carica Temporaneo
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_CHKAGG = .T.
      case this.Operazione = "QTAVAR" and this.oParentObject.w_OLTQTODL <>0 and not empty(this.oParentObject.w_OLTDTRIC)
        * --- MODIFICA QUANTITA' PADRE - Deve aggiornare il figlio e ricalcolare i tempi di produzione
        * --- Ricalcola QTA nella prima UM
        this.w_UM1 = PADR(this.oParentObject.w_UNMIS1,3)
        this.w_UM2 = PADR(this.oParentObject.w_UNMIS2,3)
        this.w_UM3 = PADR(this.oParentObject.w_UNMIS3,3)
        this.w_OP = this.oParentObject.w_OPERAT
        this.w_MOLT = this.oParentObject.w_MOLTIP
        this.w_OP3 = this.oParentObject.w_OPERA3
        this.w_MOLT3 = this.oParentObject.w_MOLTI3
        this.oParentObject.w_OLTQTOD1 = CALQTA(this.oParentObject.w_OLTQTODL, this.oParentObject.w_OLTUNMIS,this.w_UM2, this.w_OP, this.w_MOLT, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2,, this.w_UM3, this.w_OP3, this.w_MOLT3)
        * --- Aggiorna Cursore
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_OLTSTATO="L"
          this.GSCO_MOL.MarkPos()     
          this.GSCO_MOL.FirstRow()     
          locate for t_olflprev=1
          if found()
            if ah_YesNo("Attenzione, esistono materiali trasferiti evasi.%0Si desidera azzerare il flag di evasione?","")
              this.w_EVAMOD = "S"
            endif
          endif
          this.GSCO_MOL.RePos(.F.)     
        endif
        if this.oParentObject.w_OLTSTATO $ "MPL"
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Operazione = "LEGGECOSTA"
        if this.oParentObject.w_OLTFLIMC="N"
          this.oParentObject.w_OLTIMCOM = 0
        else
          if not empty(this.oParentObject.w_OLTKEYSA) and not empty(this.oParentObject.w_COCODVAL) and this.oParentObject.w_OLTQTOD1<>0
            * --- Aggiorna Costo Standard
            * --- Read from PAR_RIOR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PRCOSSTA"+;
                " from "+i_cTable+" PAR_RIOR where ";
                    +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTKEYSA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PRCOSSTA;
                from (i_cTable) where;
                    PRCODART = this.oParentObject.w_OLTKEYSA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PRCOSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Calcola
            if this.w_PRCOSSTA<>0
              * --- Calcola
              this.oParentObject.w_OLTIMCOM = cp_round(this.w_PRCOSSTA*this.oParentObject.w_OLTQTOD1, g_PERPVL)
              if this.oParentObject.w_COCODVAL <> g_PERVAL
                * --- Traduce
                this.w_VACAOVAL = 0
                * --- Read from VALUTE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.VALUTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "VACAOVAL,VADECTOT"+;
                    " from "+i_cTable+" VALUTE where ";
                        +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_COCODVAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    VACAOVAL,VADECTOT;
                    from (i_cTable) where;
                        VACODVAL = this.oParentObject.w_COCODVAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VACAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
                  this.w_VADECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.oParentObject.w_OLTIMCOM = cp_ROUND(VAL2CAM(this.oParentObject.w_OLTIMCOM, g_PERVAL, this.oParentObject.w_COCODVAL, this.w_VACAOVAL, i_DATSYS), this.w_VADECTOT)
              endif
            endif
          endif
        endif
      case this.Operazione = "CODICEVAR" and not empty(this.oParentObject.w_OLTCODIC)
        * --- Visualizza POST-IN ASSOCIATO all'articolo
        if not empty(this.oParentObject.w_OLTCOART)
          this.w_cKEY = cp_SetAzi(i_TableProp[this.oParentObject.ART_ICOL_IDX,2])+"\"+cp_ToStr(this.oParentObject.w_OLTCOART,1)
           
 cp_ShowWarn(this.w_cKey,this.oParentObject.ART_ICOL_IDX) 
 
        endif
        * --- Verifica se nel cursore della movimentazione c'� gia' qualcosa, nel qual caso segnala l'azzeramento
        SELECT (nc)
        if not (reccount()=1 and empty( &nc..t_OLCODICE ))
          * --- Non usare ZAP (fallisce storno files collegati)
          ah_ErrorMsg("Azzeramento lista materiali stimati e ciclo di lavorazione associato a ODL",,"")
          SELECT (nc)
          delete ALL
          SELECT (tc)
          delete ALL
          SELECT (ts)
          delete ALL
          if g_PRFA="S" AND g_CICLILAV="S"
            * --- Pulizia Variabili ciclo
            this.oParentObject.w_OLTSECIC = space(10)
            this.GSCO_MOL.w_CODCICLO = space(10)
          else
            * --- Pulizia Variabili ciclo
            this.oParentObject.w_OLTCICLO = SPACE(15)
            this.oParentObject.w_DESCIC = space(40)
          endif
          * --- Nuova Riga del Temporaneo
          this.GSCO_MOL.InitRow()     
          this.GSCO_MCS.InitRow()     
        endif
        * --- Rende nuovamente editabile la testata
        this.oParentObject.w_CHKAGG = .T.
        * --- Azzera le eltre variabili di testata
        this.oParentObject.w_OLTUNMIS = space(3)
        this.oParentObject.w_OLTCOMAG = space(5)
        this.oParentObject.w_TDESMAG = space(30)
        this.oParentObject.w_OLTQTODL = 0
        this.oParentObject.w_OLTQTOD1 = 0
        this.oParentObject.w_OLTDINRIC = cp_CharToDate("  -  - ")
        this.oParentObject.w_OLTDTRIC = cp_CharToDate("  -  - ")
        this.oParentObject.w_OLTEMLAV = 0
        this.oParentObject.w_OLTLEMPS = 0
        this.oParentObject.w_OLTDTMPS = cp_CharToDate("  -  - ")
        this.oParentObject.w_OLTCOFOR = space(15)
        this.oParentObject.w_DESFOR = space(40)
        this.oParentObject.w_OLTCONTR = space(5)
        this.oParentObject.w_CONDES = SPACE(50)
        this.oParentObject.w_OLTCOMME = space(15)
        this.oParentObject.w_DESCAN = space(30)
        this.oParentObject.w_OLTCOATT = space(15)
        this.oParentObject.w_DESATT = space(30)
        * --- Verifica se il ciclo e' presente
        this.oParentObject.w_HAILCIC = FALSE
        if g_PRFA="S" AND g_CICLILAV="S"
          this.GSCO_AOP.NotifyEvent("CycleExist")     
        endif
        this.oParentObject.w_OLTCOCEN = IIF(g_PERCCR="S" and this.oParentObject.w_OLTPROVE="E" , this.oParentObject.w_ARCODCEN, Space(15))
        * --- Verifica Codice di ricerca (Testata)
        if Not (this.oParentObject.w_OLTDTOBS>i_DATSYS or empty(this.oParentObject.w_OLTDTOBS))
          ah_ErrorMsg("Attenzione%0Codice di ricerca obsoleto", 48)
        endif
        if this.oParentObject.w_OLTPROVE $ ("E-L")
          this.oParentObject.w_OLTCOCEN = IIF(g_PERCCR="S" and this.oParentObject.w_OLTPROVE="E" , this.oParentObject.w_ARCODCEN, Space(15))
          * --- La provenienza e' passata da Interna a C/L - Prova a leggere il fornitore abituale
          * --- Read from PAR_RIOR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PRCODFOR"+;
              " from "+i_cTable+" PAR_RIOR where ";
                  +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PRCODFOR;
              from (i_cTable) where;
                  PRCODART = this.oParentObject.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not empty(this.oParentObject.w_OLTCOFOR)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANDESCRI,ANMAGTER"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC("F");
                    +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCOFOR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANDESCRI,ANMAGTER;
                from (i_cTable) where;
                    ANTIPCON = "F";
                    and ANCODICE = this.oParentObject.w_OLTCOFOR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESFOR = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              this.oParentObject.w_MAGTER = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if this.oParentObject.w_OLTPROVE="L" And Empty(this.oParentObject.w_MAGTER) Or empty(this.oParentObject.w_OLTCOFOR)
            this.oParentObject.w_OLTCOFOR = space(15)
            this.oParentObject.w_DESFOR = space(40)
          endif
          this.GSCO_AOP.NotifyEvent("w_OLTCOFOR Changed")     
          if (g_COLA="S" and this.oParentObject.w_OLTPROVE = "L") or (g_MODA="S" and this.oParentObject.w_OLTPROVE = "E")
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.oParentObject.w_OLTSTATO $ "MPL"
              this.Page_8()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      case this.Operazione = "CHIUSURA" and type("this.oParentObject.w_PAROBJ")#"L" and type("this.oParentObject.w_PAROBJ")="O" and !isnull(this.oParentObject.w_PAROBJ)
        * --- Se lanciato da altre gestioni...
        do case
          case "GSCO_KMP" $ upper(this.oParentObject.w_PAROBJ.name)
            * --- Aggiorna Cursore PeriodoMPS
            this.oParentObject.w_PAROBJ.NotifyEvent("ListaODP")     
            * --- Legge Cursore PeriodoODP e detrmina qta totale del periodo selezionato
            select SUM(OLTQTSAL) from (this.oParentObject.w_PAROBJ.w_PeriodoODP.cCursor) into array Totale
            if _Tally > 0
              this.TmpN = Totale(1)
              release Totale
            else
              this.TmpN = 0
            endif
            * --- Determina l'articolo sul quale siamo posizionati sullo zoom principale - Va ad aggiornare la qta
            if not empty(this.oParentObject.w_PAROBJ.w_PSELA)
              select (this.oParentObject.w_PAROBJ.w_ZoomODP.cCursor)
              this.TmpN1 = recno()
              * --- Nome del campo da aggiornale (TOT_xxx)
              NCampo = "TOT_"+this.oParentObject.w_PAROBJ.w_PSELA
              update (this.oParentObject.w_PAROBJ.w_ZoomODP.cCursor) set &NCampo = this.TmpN ;
              where CACODICE = this.oParentObject.w_PAROBJ.w_CODSEL
              * --- Si riposiziona ...
              select (this.oParentObject.w_PAROBJ.w_ZoomODP.cCursor)
              goto this.TmpN1
            endif
            * --- Refresh vari
            this.oParentObject.w_PAROBJ.Refresh()     
          case "GSCL_KST" $ upper(this.oParentObject.w_PAROBJ.name)
            * --- Aggiorna Cursore terza pagina
            this.oParentObject.w_PAROBJ.NotifyEvent("InterrDettODL")     
            this.oParentObject.w_PAROBJ.NotifyEvent("DettTerzisti")     
        endcase
      case this.Operazione = "CICLI"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Operazione = "HAILCIC"
        * --- Verifica se il ciclo e' presente
        this.oParentObject.w_HAILCIC = FALSE
        if g_PRFA="S" and g_CICLILAV="S"
          this.TmpC = " "
          * --- Read from CIC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CIC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLSERIAL"+;
              " from "+i_cTable+" CIC_MAST where ";
                  +"CLCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                  +" and CLCODDIS = "+cp_ToStrODBC(this.oParentObject.w_OLTDISBA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLSERIAL;
              from (i_cTable) where;
                  CLCODART = this.oParentObject.w_OLTCOART;
                  and CLCODDIS = this.oParentObject.w_OLTDISBA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.TmpC = NVL(cp_ToDate(_read_.CLSERIAL),cp_NullValue(_read_.CLSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_HAILCIC = not empty(this.TmpC)
        endif
      case this.Operazione = "CHANGE"
        if .F.
          * --- Al cambio fornitore occorre sistemare i materiali sulla lista di ODL_DETT
          if (g_COLA="S" and this.oParentObject.w_OLTPROVE = "L") or (g_MODA="S" and this.oParentObject.w_OLTPROVE = "E") and this.oParentObject.w_OLTQTOD1>0
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.oParentObject.w_OLTSTATO $ "MP"
              this.Page_7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            this.oParentObject.w_OLTCONTR = space(15)
            this.oParentObject.w_CONDES = SPACE(50)
            if this.oParentObject.w_OLTPROVE$ "E-L" AND this.oParentObject.w_OLTSTATO $ "MP"
              this.Page_7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
        if this.Operazione = "CHANGECOM"
          if this.oParentObject.w_OLTSTATO $ "MPL"
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          * --- Al cambio fornitore occorre sistemare i materiali sulla lista di ODL_DETT
          if (g_COLA="S" and this.oParentObject.w_OLTPROVE = "L") or (g_MODA="S" and this.oParentObject.w_OLTPROVE = "E") and this.oParentObject.w_OLTQTOD1>0
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.oParentObject.w_OLTSTATO $ "MPL"
              this.Page_8()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      case this.Operazione = "PROVENIENZA"
        * --- Se l'articolo non ha la distinta collegata non pu� essere di provenienza interna/conto lavoro
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODDIS"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODDIS;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_OLTCOART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_OLTDISBA = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.oParentObject.w_OLTDISBA) and this.oParentObject.w_ARTIPART<>"FS" AND this.oParentObject.w_OLTPROVE $ "IL"
          Ah_ErrorMsg("Articolo non valido, obsoleto, di provenienza esterna o non gestito",48)
          this.oParentObject.w_OLTCODIC = this.oParentObject.o_OLTCODIC
          this.oParentObject.w_OLTPROVE = this.oParentObject.o_OLTPROVE
          i_retcode = 'stop'
          return
        endif
        * --- Se ci sono i cicli occorre verificare se ci sono degli ordini di fase collegati
        if this.oParentObject.w_HAILCIC
          this.w_CLCODDIS = this.oParentObject.w_OLTDISBA
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT (tc)
          delete ALL
          this.oParentObject.w_OLTCOFOR = space(15)
          this.oParentObject.w_DESFOR = space(40)
          this.oParentObject.w_OLCODVAL = Space(3)
          this.oParentObject.w_OLTCILAV = SPACE(20)
          this.oParentObject.w_OLTSECIC = Space(10)
          this.oParentObject.w_DESCILAV = Space(40)
        else
          SELECT (ts)
          delete ALL
        endif
        do case
          case this.oParentObject.w_OLTPROVE="E"
            this.oParentObject.w_OLTCICLO = SPACE(10)
          otherwise
            this.oParentObject.w_OLTCONTR = SPACE(15)
            this.oParentObject.w_CONDES = SPACE(50)
        endcase
        if this.oParentObject.w_OLTPROVE $ ("E-L")
          this.oParentObject.w_OLTCOCEN = IIF(g_PERCCR="S" and this.oParentObject.w_OLTPROVE="E" , this.oParentObject.w_ARCODCEN, Space(15))
          * --- Read from PAR_RIOR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PRCODFOR"+;
              " from "+i_cTable+" PAR_RIOR where ";
                  +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PRCODFOR;
              from (i_cTable) where;
                  PRCODART = this.oParentObject.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not empty(this.oParentObject.w_OLTCOFOR)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANDESCRI,ANMAGTER"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC("F");
                    +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCOFOR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANDESCRI,ANMAGTER;
                from (i_cTable) where;
                    ANTIPCON = "F";
                    and ANCODICE = this.oParentObject.w_OLTCOFOR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESFOR = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              this.oParentObject.w_MAGTER = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if this.oParentObject.w_OLTPROVE="L" And Empty(this.oParentObject.w_MAGTER) Or empty(this.oParentObject.w_OLTCOFOR)
            this.oParentObject.w_OLTCOFOR = space(15)
            this.oParentObject.w_DESFOR = space(40)
            this.oParentObject.w_OLTCONTR = SPACE(15)
            this.oParentObject.w_CONDES = SPACE(50)
          endif
        else
          this.oParentObject.w_OLTCOFOR = space(15)
          this.oParentObject.w_DESFOR = space(40)
          this.oParentObject.w_OLTCONTR = SPACE(15)
          this.oParentObject.w_CONDES = SPACE(50)
        endif
        if not empty(this.oParentObject.w_OLTPROVE)
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_OLTSTATO $ "MPL"
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.Operazione = "EDITSTARTED"
        this.oParentObject.w_HASEVENT = .T.
        this.oParentObject.w_CHKAGG = (this.oParentObject.w_OLTSTATO<>"L" or not this.oParentObject.w_OLTPROVE $ "E-L") and Not (this.oParentObject.w_OLTPROVE="L" and this.oParentObject.w_ARTIPART="FS")
        if g_COLA="S" and this.oParentObject.w_OLTPROVE = "L" and this.oParentObject.w_ARTIPART="FS" and this.oParentObject.w_OLTSTATO $ "L-F"
          this.oParentObject.w_HASEVENT = FALSE
          this.TmpC = "Ordine di conto lavoro di fase. Impossibile variare%0Utilizzare l'apposita scelta di men� per variare il terzista"
          Ah_ErrorMsg(this.TmpC,48)
        endif
        if this.oParentObject.w_OLTPROVE = "I" and this.oParentObject.w_ARTIPART="FS" and this.oParentObject.w_OLTSTATO $ "L-F"
          this.oParentObject.w_HASEVENT = FALSE
          this.TmpC = "Ordine di fase. Impossibile variare"
          Ah_ErrorMsg(this.TmpC,48)
        endif
        if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "F"
          this.TmpC = "Impossibile variare un %1"
          ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
          this.oParentObject.w_HASEVENT = .F.
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Verifica che mi serve per bloccare l'editabilit� dei campi
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and this.oParentObject.w_OLTQTOEV+this.oParentObject.w_OLTQTOSC > 0
          this.oParentObject.w_HASEVENT = FALSE
          this.TmpC = "Impossibile variare un %1 dichiarato"
          ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
          this.oParentObject.w_CHKAGG = FALSE
        endif
        if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and not empty(this.oParentObject.w_OLTSECIC)
          * --- Select from ODL_CICL
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SUM(CLAVAUM1+CLSCAUM1) AS QTAVER  from "+i_cTable+" ODL_CICL ";
                +" where CLCODODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+"";
                 ,"_Curs_ODL_CICL")
          else
            select SUM(CLAVAUM1+CLSCAUM1) AS QTAVER from (i_cTable);
             where CLCODODL=this.oParentObject.w_OLCODODL;
              into cursor _Curs_ODL_CICL
          endif
          if used('_Curs_ODL_CICL')
            select _Curs_ODL_CICL
            locate for 1=1
            do while not(eof())
            this.w_QTAVER = _Curs_ODL_CICL.QTAVER
              select _Curs_ODL_CICL
              continue
            enddo
            use
          endif
          if this.w_QTAVER>0
            this.oParentObject.w_CHKAGG = FALSE
          endif
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        do case
          case this.oParentObject.w_TIPGES $ "LZ"
            if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and this.oParentObject.w_OLTQTOEV+this.oParentObject.w_OLTQTOSC>0 and this.oParentObject.w_OLFLMODC<>"S" and not empty(this.oParentObject.w_OLTSECIC)
              this.oParentObject.w_HASEVENT = FALSE
              this.TmpC = "Impossibile variare un %1 dichiarato che non ha il flag consenti modifica fasi attivo"
              ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
            endif
          otherwise
            if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and this.oParentObject.w_OLTQTOEV+this.oParentObject.w_OLTQTOSC>0
              this.oParentObject.w_HASEVENT = FALSE
              this.TmpC = "Impossibile variare un %1 dichiarato"
              ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
            endif
        endcase
        if this.oParentObject.w_HASEVENT and this.oParentObject.w_OLTSTATO = "L" and not empty(this.oParentObject.w_OLTSECIC)
          * --- Verifica se � stata avanzata qualche fase del ciclo
          * --- Select from ODL_CICL
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SUM(CLAVAUM1+CLSCAUM1) AS QTAVER  from "+i_cTable+" ODL_CICL ";
                +" where CLCODODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+"";
                 ,"_Curs_ODL_CICL")
          else
            select SUM(CLAVAUM1+CLSCAUM1) AS QTAVER from (i_cTable);
             where CLCODODL=this.oParentObject.w_OLCODODL;
              into cursor _Curs_ODL_CICL
          endif
          if used('_Curs_ODL_CICL')
            select _Curs_ODL_CICL
            locate for 1=1
            do while not(eof())
            this.w_QTAVER = _Curs_ODL_CICL.QTAVER
              select _Curs_ODL_CICL
              continue
            enddo
            use
          endif
          do case
            case this.oParentObject.w_TIPGES $ "LZ"
              if this.w_QTAVER>0 and this.oParentObject.w_OLFLMODC<>"S"
                this.oParentObject.w_HASEVENT = FALSE
                this.TmpC = "Impossibile variare un %1 dichiarato che non ha il flag consenti modifica fasi attivo"
                ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
              endif
            otherwise
              if this.w_QTAVER>0
                this.oParentObject.w_HASEVENT = FALSE
                this.TmpC = "Impossibile variare un %1 dichiarato"
                ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
              endif
          endcase
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        if this.oParentObject.w_HASEVENT
          this.oParentObject.w_STATOGES = upper(this.oParentObject.cFunction)
        endif
      case this.Operazione = "AggiornaOrdiniDiFase"
        if g_PRFA="S" AND g_CICLILAV="S" and (not empty(this.oParentObject.w_OLTSECIC) Or empty(this.oParentObject.w_OLTSECIC) and this.oParentObject.w_OLTPROVE<>"I") and this.oParentObject.w_OLTSTATO $ "MPL" and this.oParentObject.w_STATOGES<>"LOAD"
          if this.oParentObject.w_OLTQTOD1<>this.oParentObject.w_ORIGQTA or this.oParentObject.w_OLTDTRIC<>this.oParentObject.w_ORIGDATA or this.oParentObject.w_OLTSECIC<>this.oParentObject.w_ORIGCICL
            * --- Aggiorna ordini di fase collegati
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        if this.oParentObject.w_AGGOCLFA
          * --- Aggiorna lo stato degli ordini di fase collegati (w_AGGOCLFA valorizzata in AM Replace Init)
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                +i_ccchkf ;
            +" where ";
                +"OLTSEODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                +" and OLTSTATO = "+cp_ToStrODBC("M");
                   )
          else
            update (i_cTable) set;
                OLTSTATO = "P";
                &i_ccchkf. ;
             where;
                OLTSEODL = this.oParentObject.w_OLCODODL;
                and OLTSTATO = "M";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_SERODL = nvl(this.oParentObject.w_OLTSEODL, space(15))
          if !Empty(this.w_SERODL)
            * --- Se ordine di fase, conferma l'ODL padre e tutti gli ordini di fase collegati
            if this.oParentObject.w_ARTIPART="FS" and not empty(this.w_SERODL)
              * --- Legge stato attuale
              * --- Read from ODL_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLTSTATO"+;
                  " from "+i_cTable+" ODL_MAST where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_SERODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLTSTATO;
                  from (i_cTable) where;
                      OLCODODL = this.w_SERODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_STAPAD = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Se suggerito, lo conferma
              if this.w_STAPAD="M"
                * --- Write into ODL_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_SERODL);
                         )
                else
                  update (i_cTable) set;
                      OLTSTATO = "P";
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_SERODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- ... e tutti gli ordini figli (escluso il presente)
                * --- Write into ODL_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                      +i_ccchkf ;
                  +" where ";
                      +"OLTSEODL = "+cp_ToStrODBC(this.w_SERODL);
                      +" and OLTSTATO = "+cp_ToStrODBC("M");
                      +" and OLCODODL <> "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                         )
                else
                  update (i_cTable) set;
                      OLTSTATO = "P";
                      &i_ccchkf. ;
                   where;
                      OLTSEODL = this.w_SERODL;
                      and OLTSTATO = "M";
                      and OLCODODL <> this.oParentObject.w_OLCODODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
        endif
      case this.Operazione $ "PEGGING-AggiornaRisorse"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Operazione $ "PEGPF-PEGFP"
        if this.Operazione="PEGFP"
          CURPEGG=this.GSCO_AOP.w_ZFP.cCursor
          this.w_DOCSER = &CURPEGG..PERIFODL
          this.w_CODODL = this.w_DOCSER
        else
          CURPEGG=this.GSCO_AOP.w_ZPF.cCursor
          this.w_DOCSER = alltrim(left(&CURPEGG..PESERODL,10))
          this.w_CODODL = &CURPEGG..PESERODL
        endif
        this.w_FLVEAC1 = &CURPEGG..FLVEAC
        this.w_CLADOC = &CURPEGG..CLADOC
        this.w_ST = &CURPEGG..PETIPRIF
        this.w_ROWNUM = &CURPEGG..PERIGORD
        this.w_PROVE = &CURPEGG..PROVE
        this.w_CODART = &CURPEGG..CODART
        this.w_STATOODL = &CURPEGG..OLTSTATO
        if this.w_ST="D"
          * --- Dettaglio Documenti
          this.w_PROG = GSAR_BZM( this, this.w_DOCSER, -20)
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if type("this.w_PROG")<>"O" Or isnull(this.w_PROG)
            i_retcode = 'stop'
            return
          endif
          if this.w_CLADOC="OR"
            nf = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_3
            sf = oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_3.GotFocus
          else
            nf = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
            sf = oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.GotFocus
          endif
          * --- Carico il documento
          * --- Mi posiziono sulla riga
          Local L_cTrsName 
 L_cTrsName=this.w_PROG.cTrsname 
 Select (L_cTrsName) 
 Go Top 
 Locate For CPROWNUM= this.w_ROWNUM
          if Found()
            this.w_PROG.oPgFrm.Page1.oPag.oBody.Refresh()     
            this.w_PROG.WorkFromTrs()     
            this.w_PROG.SaveDependsOn()     
            this.w_PROG.SetControlsValue()     
            this.w_PROG.mHideControls()     
            this.w_PROG.ChildrenChangeRow()     
            l_prova=this.w_prog.cfunction
            this.w_PROG.cFunction = "Edit"
            this.w_PROG.oNewFocus = &nf
            this.w_PROG.&sf()     
            this.w_prog.cfunction=l_prova
          endif
        else
          if this.w_ST<>"M"
            gsco_bor(this,this.w_cododl)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Giacenza di magazzino - Non devo aprire nessuna gestione.
          endif
        endif
      case this.Operazione = "SALDIMPS"
        * --- Leggo il vecchio valore dall'ODL
        * --- Read from ODL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLTQTSAL,OLTFLSUG,OLTFLCON,OLTFLDAP,OLTFLPIA,OLTFLLAN,OLTPERAS"+;
            " from "+i_cTable+" ODL_MAST where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLTQTSAL,OLTFLSUG,OLTFLCON,OLTFLDAP,OLTFLPIA,OLTFLLAN,OLTPERAS;
            from (i_cTable) where;
                OLCODODL = this.oParentObject.w_OLCODODL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
          this.w_FLSUG = NVL(cp_ToDate(_read_.OLTFLSUG),cp_NullValue(_read_.OLTFLSUG))
          this.w_FLCON = NVL(cp_ToDate(_read_.OLTFLCON),cp_NullValue(_read_.OLTFLCON))
          this.w_FLDAP = NVL(cp_ToDate(_read_.OLTFLDAP),cp_NullValue(_read_.OLTFLDAP))
          this.w_FLPIA = NVL(cp_ToDate(_read_.OLTFLPIA),cp_NullValue(_read_.OLTFLPIA))
          this.w_FLLAN = NVL(cp_ToDate(_read_.OLTFLLAN),cp_NullValue(_read_.OLTFLLAN))
          this.w_OLDPERAS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_FLSUG = IIF(this.w_FLSUG="+", "-", " ")
        this.w_FLCON = IIF(this.w_FLCON="+", "-", " ")
        this.w_FLDAP = IIF(this.w_FLDAP="+", "-", " ")
        this.w_FLPIA = IIF(this.w_FLPIA= "+", "-", " ")
        this.w_FLLAN = IIF(this.w_FLLAN="+", "-", " ")
        if inlist(this.GSCO_AOP.cFunction, "Query", "Edit")
          * --- Storno i Saldi MPS
          * --- Try
          local bErr_04DEA9A0
          bErr_04DEA9A0=bTrsErr
          this.Try_04DEA9A0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DEA9A0
          * --- End
        endif
        if this.GSCO_AOP.cFunction <> "Query"
          * --- Caso in cui non sono in interrogazione , quindi non st� cancellando
          * --- Aggiorno i saldi MPS con i nuovi valori.
          * --- Try
          local bErr_04DD4740
          bErr_04DD4740=bTrsErr
          this.Try_04DD4740()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DD4740
          * --- End
          this.w_OLTCOVAR = space(20)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODFAM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODFAM;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FAMPRO = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
            i_cOp1=cp_SetTrsOp(this.oParentObject.w_OLTFLSUG,'FMMPSSUG','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.oParentObject.w_OLTFLCON,'FMMPSCON','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.oParentObject.w_OLTFLDAP,'FMMPSDAP','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.oParentObject.w_OLTFLPIA,'FMMPSPIA','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
            i_cOp5=cp_SetTrsOp(this.oParentObject.w_OLTFLLAN,'FMMPSLAN','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSSUG ="+cp_NullLink(i_cOp1,'MPS_TFOR','FMMPSSUG');
            +",FMMPSCON ="+cp_NullLink(i_cOp2,'MPS_TFOR','FMMPSCON');
            +",FMMPSDAP ="+cp_NullLink(i_cOp3,'MPS_TFOR','FMMPSDAP');
            +",FMMPSPIA ="+cp_NullLink(i_cOp4,'MPS_TFOR','FMMPSPIA');
            +",FMMPSLAN ="+cp_NullLink(i_cOp5,'MPS_TFOR','FMMPSLAN');
            +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.oParentObject.w_OLTQTSAL);
            +",FMCODART ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOART),'MPS_TFOR','FMCODART');
            +",FMCODVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OLTCOVAR),'MPS_TFOR','FMCODVAR');
            +",FMKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTKEYSA),'MPS_TFOR','FMKEYSAL');
            +",FMUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UNMIS1),'MPS_TFOR','FMUNIMIS');
            +",FMFAMPRO ="+cp_NullLink(cp_ToStrODBC(this.w_FAMPRO),'MPS_TFOR','FMFAMPRO');
                +i_ccchkf ;
            +" where ";
                +"FMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCODIC);
                +" and FMPERASS = "+cp_ToStrODBC(this.oParentObject.w_OLTPERAS);
                   )
          else
            update (i_cTable) set;
                FMMPSSUG = &i_cOp1.;
                ,FMMPSCON = &i_cOp2.;
                ,FMMPSDAP = &i_cOp3.;
                ,FMMPSPIA = &i_cOp4.;
                ,FMMPSLAN = &i_cOp5.;
                ,FMMPSTOT = FMMPSTOT + this.oParentObject.w_OLTQTSAL;
                ,FMCODART = this.oParentObject.w_OLTCOART;
                ,FMCODVAR = this.w_OLTCOVAR;
                ,FMKEYSAL = this.oParentObject.w_OLTKEYSA;
                ,FMUNIMIS = this.oParentObject.w_UNMIS1;
                ,FMFAMPRO = this.w_FAMPRO;
                &i_ccchkf. ;
             where;
                FMCODICE = this.oParentObject.w_OLTCODIC;
                and FMPERASS = this.oParentObject.w_OLTPERAS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.Operazione = "SALDICOM"
        * --- Aggiornamento Saldi Commessa
        * --- Read from ODL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLTQTSAL,OLTFLORD,OLTFLIMP,OLTCOMME,OLTCOART,OLTKEYSA"+;
            " from "+i_cTable+" ODL_MAST where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLTQTSAL,OLTFLORD,OLTFLIMP,OLTCOMME,OLTCOART,OLTKEYSA;
            from (i_cTable) where;
                OLCODODL = this.oParentObject.w_OLCODODL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
          this.w_FLORD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
          this.w_FLIMP = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
          this.w_OLDCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
          this.w_ARTIC = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
          this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARTIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_ARTIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFCM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        if empty(nvl(this.oParentObject.w_OLTCOMME,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.oParentObject.w_OLTCOMME
        endif
        if empty(nvl(this.w_OLDCOMME,""))
          this.w_COMMOLD = this.w_COMMDEFA
        else
          this.w_COMMOLD = this.w_OLDCOMME
        endif
        if NVL(this.oParentObject.w_ARFLCOMM,"N")="S" or NVL(this.w_OLDFCM,"N")="S" or not empty (nvl(this.w_COMMAPPO," ")) or not empty (nvl(this.w_COMMOLD," ")) 
          * --- Leggo il vecchio valore dall'ODL
          this.w_FLORD = IIF(this.w_FLORD="+", "-", IIF(this.w_FLORD="-", "+", " "))
          this.w_FLIMP = IIF(this.w_FLIMP="+", "-", IIF(this.w_FLIMP="-", "+", " "))
          if ! empty(nvl(this.w_COMMAPPO," ")) or not empty (nvl(this.w_COMMOLD," ")) 
            if (nvl(this.w_COMMAPPO," "))=(nvl(this.w_COMMOLD," ")) and NVL(this.w_OLDFCM,"N")="S" 
              if inlist(this.GSCO_AOP.cFunction, "Query", "Edit")
                * --- Storno i Saldi Commessa
                * --- Try
                local bErr_04E27848
                bErr_04E27848=bTrsErr
                this.Try_04E27848()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04E27848
                * --- End
              endif
            endif
            if NVL(this.oParentObject.w_ARFLCOMM,"N")="S" 
              if this.GSCO_AOP.cFunction <> "Query"
                * --- Caso in cui non sono in interrogazione , quindi non st� cancellando
                * --- Aggiorno i saldi commessa con i nuovi valori.
                * --- Try
                local bErr_04E248D8
                bErr_04E248D8=bTrsErr
                this.Try_04E248D8()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04E248D8
                * --- End
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.oParentObject.w_OLTFLORD,'SCQTOPER','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.oParentObject.w_OLTFLIMP,'SCQTIPER','this.oParentObject.w_OLTQTSAL',this.oParentObject.w_OLTQTSAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_OLTCOMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.oParentObject.w_OLTKEYSA;
                      and SCCODMAG = this.oParentObject.w_OLTCOMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
            endif
          endif
          if ! empty(nvl(this.w_COMMOLD," ")) and (nvl(this.w_COMMAPPO," "))<>(nvl(this.w_COMMOLD," ")) and NVL(this.w_OLDFCM,"N")="S" 
            if inlist(this.GSCO_AOP.cFunction, "Query", "Edit")
              * --- Storno i Saldi Commessa
              * --- Try
              local bErr_04E17600
              bErr_04E17600=bTrsErr
              this.Try_04E17600()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04E17600
              * --- End
            endif
          endif
        endif
        * --- Caso particolare, articolo a commessa con codice commessa vuoto, l'update sui materiali, presente su GSCO_MOL, non aggiorna i saldi sulla commessa di default.
        this.GSCO_MOL.MarkPos()     
        this.GSCO_MOL.FirstRow()     
        do while Not this.GSCO_MOL.Eof_Trs()
          this.GSCO_MOL.SetRow()     
          if this.GSCO_MOL.FullRow() and (INLIST(this.GSCO_MOL.RowStatus() , "U" , "A") or this.GSCO_AOP.cFunction = "Query")
            * --- Vecchi valori
            this.w_O_CODCOM = NVL(this.GSCO_MOL.Get("OLCODCOM"), " " )
            this.w_O_QTAUM1 = NVL(this.GSCO_MOL.Get("OLQTASAL"), 0 )
            this.w_O_CODART = NVL(this.GSCO_MOL.Get("OLKEYSAL"), " " )
            this.w_O_CODMAG = NVL(this.GSCO_MOL.Get("OLCODMAG"), " " )
            this.w_O_FLORDI = NVL(this.GSCO_MOL.Get("OLFLORDI"), " " )
            this.w_O_FLRISE = NVL(this.GSCO_MOL.Get("OLFLRISE"), " " )
            this.w_O_FLIMPE = NVL(this.GSCO_MOL.Get("OLFLIMPE"), " " )
            * --- Inverto i flag per storno vecchi valori
            this.w_O_FLORDI = IIF(this.w_O_FLORDI="+", "-", IIF(this.w_O_FLORDI="-", "+", " "))
            this.w_O_FLRISE = IIF(this.w_O_FLRISE="+", "-", IIF(this.w_O_FLRISE="-", "+", " "))
            this.w_O_FLIMPE = IIF(this.w_O_FLIMPE="+", "-", IIF(this.w_O_FLIMPE="-", "+", " "))
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_O_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_O_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_O_FLCOMM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_D_CODCOM = NVL(this.GSCO_MOL.Get("w_OLCODCOM"), " " )
            this.w_D_QTAUM1 = NVL(this.GSCO_MOL.Get("w_OLQTASAL"), 0 )
            this.w_D_CODART = NVL(this.GSCO_MOL.Get("w_OLKEYSAL"), " " )
            this.w_D_CODMAG = NVL(this.GSCO_MOL.Get("w_OLCODMAG"), " " )
            this.w_D_FLORDI = NVL(this.GSCO_MOL.Get("w_OLFLORDI"), " " )
            this.w_D_FLRISE = NVL(this.GSCO_MOL.Get("w_OLFLRISE"), " " )
            this.w_D_FLIMPE = NVL(this.GSCO_MOL.Get("w_OLFLIMPE"), " " )
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_D_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_D_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_D_FLCOMM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if inlist(this.GSCO_AOP.cFunction, "Query", "Edit")
              if empty(this.w_O_CODCOM) and nvl(this.w_O_FLCOMM,"N")="S"
                * --- Try
                local bErr_04E0F170
                bErr_04E0F170=bTrsErr
                this.Try_04E0F170()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04E0F170
                * --- End
              endif
            endif
            if this.GSCO_AOP.cFunction <> "Query"
              * --- Caso in cui non sono in interrogazione , quindi non st� cancellando
              * --- Aggiorno i saldi commessa con i nuovi valori.
              if empty(this.w_D_CODCOM) and nvl(this.w_D_FLCOMM,"N")="S"
                * --- Try
                local bErr_04E0C800
                bErr_04E0C800=bTrsErr
                this.Try_04E0C800()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04E0C800
                * --- End
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_D_FLORDI,'SCQTOPER','this.w_D_QTAUM1',this.w_D_QTAUM1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_D_FLIMPE,'SCQTIPER','this.w_D_QTAUM1',this.w_D_QTAUM1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_D_CODART);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_D_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_D_CODART;
                      and SCCODMAG = this.w_D_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
            endif
          endif
          * --- Passo alla prossima riga...
          this.GSCO_MOL.NextRow()     
        enddo
        this.GSCO_MOL.RePos(.F.)     
      case this.Operazione = "TV"
        if this.Operazione = "TVPage6"
          this.Albero = this.oParentObject.w_TREEV
          this.w_NODITV = this.Albero.oTree
          * --- Sbianco la tree-view
          this.w_NODITV.Nodes.Clear
          * --- Verifico visibilit� label
          if this.oParentObject.w_TIPGES="L"
            this.GSCO_AOP.w_StatoDOC.visible = .f.
          endif
        endif
        this.w_EVENTO = alltrim(this.Operazione)+alltrim(this.oParentObject.w_TIPGES)
        this.GSCO_AOP.NotifyEvent(this.w_EVENTO)     
        * --- Nuova Interfaccia - evita di spostarsi sulla prima pagina
        this.GSCO_AOP.w_TREEV.oTree.SetFocus()     
      case this.Operazione = "HContr"
        * --- Se sono in modalit� interrgazione devo lanciare la mhidecontrols se cambio riga sulla Tree View
        if this.GSCO_AOP.cFunction="Query"
          this.GSCO_AOP.mhidecontrols()     
        endif
    endcase
    do case
      case this.Operazione="NUOVO"
        * --- Nessuna Operazione...Inizializza solo le Variabili editabili
      case this.Operazione = "CICLOVAR" AND this.oParentObject.w_OLTCICLO<>this.oParentObject.o_OLTCICLO AND (this.oParentObject.w_CHKDETT=.T. OR UPPER(this.oParentObject.w_STATOGES)<>"LOAD")
        if ah_YesNo("Modificando il codice del ciclo semplificato%0verranno azzerate tutte le fasi associate ai componenti%0Proseguo ugualmente?")
          * --- Aggiorna Cursore ...
          SELECT (nc)
          go top
          SCAN FOR (NOT EMPTY(t_OLCODICE)) AND (NOT DELETED())
          * --- Aggiorna Cursore
          replace t_OLFASRIF with 0
          replace t_OLRIFFAS with 0
          replace t_DESFAS with SPACE(40)
          replace i_SRV with iif(upper(this.oParentObject.w_STATOGES)="LOAD" or i_SRV="A","A","U")
          this.GSCO_MOL.WorkFromTrs()     
          this.GSCO_MOL.w_OLRIFFAS = 0
          this.GSCO_MOL.w_OLFASRIF = 0
          this.GSCO_MOL.w_DESFAS = SPACE(40)
          this.GSCO_MOL.bUpdated = True
          this.GSCO_MOL.mCalc(.T.)     
          this.GSCO_MOL.TrsFromWork()     
          SELECT (nc)
          ENDSCAN
          SELECT (nc)
          go top
          * --- Rinfresca padre
          SELECT (nc) 
 GO TOP 
 With this.GSCO_MOL 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.o_OLTCICLO = this.oParentObject.w_OLTCICLO
        else
          * --- Ripristina il Codice Risorsa
          this.oParentObject.w_OLTCICLO = this.oParentObject.o_OLTCICLO
        endif
      case this.Operazione = "CREATMP"
        if cp_ExistTableDef("TMPODL_CICL")
          * --- Try
          local bErr_032966B0
          bErr_032966B0=bTrsErr
          this.Try_032966B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Create temporary table TMPODL_CICL
            i_nIdx=cp_AddTableDef('TMPODL_CICL') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
                  )
            this.TMPODL_CICL_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          bTrsErr=bTrsErr or bErr_032966B0
          * --- End
        endif
        if cp_ExistTableDef("TMPODL_RISO")
          * --- Try
          local bErr_04BE4B68
          bErr_04BE4B68=bTrsErr
          this.Try_04BE4B68()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Create temporary table TMPODL_RISO
            i_nIdx=cp_AddTableDef('TMPODL_RISO') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_RISO_proto';
                  )
            this.TMPODL_RISO_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          bTrsErr=bTrsErr or bErr_04BE4B68
          * --- End
        endif
      case this.Operazione = "DROPTMP"
        * --- Delete from TMPODL_CICL
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_CLKEYRIF;
                and CLCODODL = this.oParentObject.w_OLCODODL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TMPODL_RISO
        i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and RLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_CLKEYRIF;
                and RLCODODL = this.oParentObject.w_OLCODODL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.Operazione = "DIBAVAR"
        * --- Verifica se il ciclo e' presente
        this.oParentObject.w_HAILCIC = FALSE
        if g_PRFA="S" AND g_CICLILAV="S"
          this.GSCO_AOP.NotifyEvent("CycleExist")     
        endif
    endcase
  endproc
  proc Try_04DEA9A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MPS_TFOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLSUG,'FMMPSSUG','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLCON,'FMMPSCON','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLDAP,'FMMPSDAP','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_FLPIA,'FMMPSPIA','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_FLLAN,'FMMPSLAN','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FMMPSSUG ="+cp_NullLink(i_cOp1,'MPS_TFOR','FMMPSSUG');
      +",FMMPSCON ="+cp_NullLink(i_cOp2,'MPS_TFOR','FMMPSCON');
      +",FMMPSDAP ="+cp_NullLink(i_cOp3,'MPS_TFOR','FMMPSDAP');
      +",FMMPSPIA ="+cp_NullLink(i_cOp4,'MPS_TFOR','FMMPSPIA');
      +",FMMPSLAN ="+cp_NullLink(i_cOp5,'MPS_TFOR','FMMPSLAN');
      +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_OLDQTSAL);
          +i_ccchkf ;
      +" where ";
          +"FMCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCODIC);
          +" and FMPERASS = "+cp_ToStrODBC(this.w_OLDPERAS);
             )
    else
      update (i_cTable) set;
          FMMPSSUG = &i_cOp1.;
          ,FMMPSCON = &i_cOp2.;
          ,FMMPSDAP = &i_cOp3.;
          ,FMMPSPIA = &i_cOp4.;
          ,FMMPSLAN = &i_cOp5.;
          ,FMMPSTOT = FMMPSTOT - this.w_OLDQTSAL;
          &i_ccchkf. ;
       where;
          FMCODICE = this.oParentObject.w_OLTCODIC;
          and FMPERASS = this.w_OLDPERAS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi MPS'
      return
    endif
    return
  proc Try_04DD4740()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TFOR
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TFOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FMCODICE"+",FMPERASS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCODIC),'MPS_TFOR','FMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTPERAS),'MPS_TFOR','FMPERASS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FMCODICE',this.oParentObject.w_OLTCODIC,'FMPERASS',this.oParentObject.w_OLTPERAS)
      insert into (i_cTable) (FMCODICE,FMPERASS &i_ccchkf. );
         values (;
           this.oParentObject.w_OLTCODIC;
           ,this.oParentObject.w_OLTPERAS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E27848()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_OLTCOMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
             )
    else
      update (i_cTable) set;
          SCQTOPER = &i_cOp1.;
          ,SCQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_KEYSAL;
          and SCCODMAG = this.oParentObject.w_OLTCOMAG;
          and SCCODCAN = this.w_COMMAPPO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_04E248D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+",SCCODVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTKEYSA),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOART),'SALDICOM','SCCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOVAR),'SALDICOM','SCCODVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.oParentObject.w_OLTKEYSA,'SCCODMAG',this.oParentObject.w_OLTCOMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.oParentObject.w_OLTCOART,'SCCODVAR',this.w_OLTCOVAR)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART,SCCODVAR &i_ccchkf. );
         values (;
           this.oParentObject.w_OLTKEYSA;
           ,this.oParentObject.w_OLTCOMAG;
           ,this.w_COMMAPPO;
           ,this.oParentObject.w_OLTCOART;
           ,this.w_OLTCOVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E17600()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_OLTCOMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMOLD);
             )
    else
      update (i_cTable) set;
          SCQTOPER = &i_cOp1.;
          ,SCQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_KEYSAL;
          and SCCODMAG = this.oParentObject.w_OLTCOMAG;
          and SCCODCAN = this.w_COMMOLD;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_04E0F170()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_O_FLORDI,'SCQTOPER','this.w_O_QTAUM1',this.w_O_QTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_O_FLIMPE,'SCQTIPER','this.w_O_QTAUM1',this.w_O_QTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_O_CODART);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_O_CODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
             )
    else
      update (i_cTable) set;
          SCQTOPER = &i_cOp1.;
          ,SCQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_O_CODART;
          and SCCODMAG = this.w_O_CODMAG;
          and SCCODCAN = this.w_COMMAPPO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_04E0C800()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+",SCCODVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_D_CODART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_D_CODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_D_CODART),'SALDICOM','SCCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOVAR),'SALDICOM','SCCODVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_D_CODART,'SCCODMAG',this.w_D_CODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_D_CODART,'SCCODVAR',this.w_OLTCOVAR)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART,SCCODVAR &i_ccchkf. );
         values (;
           this.w_D_CODART;
           ,this.w_D_CODMAG;
           ,this.w_COMMAPPO;
           ,this.w_D_CODART;
           ,this.w_OLTCOVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_032966B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from TMPODL_CICL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" TMPODL_CICL where ";
            +"1 = "+cp_ToStrODBC(2);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            1 = 2;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_04BE4B68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from TMPODL_RISO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2],.t.,this.TMPODL_RISO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" TMPODL_RISO where ";
            +"1 = "+cp_ToStrODBC(2);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            1 = 2;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo - genera lista materiali ODL
    * --- Con il C/Lavoro, cerca il codice del contratto legato al fornitore intestatario
    if this.oParentObject.w_OLTSTATO $ "MP"
      this.Page_12()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_OLTQTODL<>0 and not empty (this.oParentObject.w_OLTDTRIC)
        if !EMPTY(this.oParentObject.w_OLTDISBA)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARTIPIMP,ARMAGIMP"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARTIPIMP,ARMAGIMP;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPIMP = NVL(cp_ToDate(_read_.ARTIPIMP),cp_NullValue(_read_.ARTIPIMP))
            this.w_MAGIMP = NVL(cp_ToDate(_read_.ARMAGIMP),cp_NullValue(_read_.ARMAGIMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Eplode Distinta Selezionata
          ah_Msg("Fase 1: esplosione distinta base...",.T.)
          this.w_DBCODINI = this.oParentObject.w_OLTCOART
          this.w_DBCODFIN = this.oParentObject.w_OLTCOART
          this.w_MAXLEVEL = 1
          this.w_VERIFICA = "S"
          this.w_FILSTAT = " "
          this.w_DATFIL = this.oParentObject.w_OLTDINRIC
          USE IN SELECT("ELABREC")
          if g_PRFA="S" AND g_CICLILAV="S"
            vq_exec ("..\COLA\EXE\QUERY\GSCOBBED", this, "ELABREC")
          else
            vq_exec ("..\COLA\EXE\QUERY\GSCOABED", this, "ELABREC")
          endif
          USE IN SELECT("TES_PLOS")
          gsar_bde(this,"P")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if USED("TES_PLOS")
            if this.oParentObject.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR)
              * --- Legge Eventuali Materiali del Terzista
              vq_exec ("..\COLA\EXE\QUERY\GSCO_QMT", this, "_MatTer_")
            endif
            this.w_ULTFAS = 0
            if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
              * --- Inserisco nel temporanero le fasi associate alla distinta
              this.w_DATFAS = this.oParentObject.w_OLTDINRIC
              * --- Delete from TMPODL_CICL
              i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                      +" and CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                       )
              else
                delete from (i_cTable) where;
                      CLKEYRIF = this.w_CLKEYRIF;
                      and CLCODODL = this.oParentObject.w_OLCODODL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Insert into TMPODL_CICL
              i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCICBTC",this.TMPODL_CICL_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              * --- Read from PAR_PROD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PPMATINP,PPMATOUP"+;
                  " from "+i_cTable+" PAR_PROD where ";
                      +"PPCODICE = "+cp_ToStrODBC("PP");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PPMATINP,PPMATOUP;
                  from (i_cTable) where;
                      PPCODICE = "PP";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PPMATINP = NVL(cp_ToDate(_read_.PPMATINP),cp_NullValue(_read_.PPMATINP))
                this.w_PPMATOUP = NVL(cp_ToDate(_read_.PPMATOUP),cp_NullValue(_read_.PPMATOUP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Determina prima fase ciclo (per assegnazione rif. fase a lista materiali stimati)
              this.w_ULTFAS = 0
              this.w_DULTIFAS = " "
              this.w_CPULTFAS = 0
              if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
                * --- Select from GSCO_BOL1
                do vq_exec with 'GSCO_BOL1',this,'_Curs_GSCO_BOL1','',.f.,.t.
                if used('_Curs_GSCO_BOL1')
                  select _Curs_GSCO_BOL1
                  locate for 1=1
                  do while not(eof())
                  * --- Determino ultima fase
                  this.w_ULTFAS = _Curs_GSCO_BOL1.CPROWORD
                  this.w_DULTIFAS = _Curs_GSCO_BOL1.CLFASDES
                  this.w_CPULTFAS = _Curs_GSCO_BOL1.CPROWNUM
                  if this.w_PPMATINP="P"
                    exit
                  endif
                    select _Curs_GSCO_BOL1
                    continue
                  enddo
                  use
                endif
              endif
            else
              if this.oParentObject.w_OLTCICLO=this.oParentObject.w_DEFCICLO AND NOT EMPTY(this.oParentObject.w_OLTCICLO)
                * --- Legge Ultima Fase del Ciclo (Per Default)
                * --- Select from TAB_CICL
                i_nConn=i_TableProp[this.TAB_CICL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2],.t.,this.TAB_CICL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select CPROWORD  from "+i_cTable+" TAB_CICL ";
                      +" where CSCODICE="+cp_ToStrODBC(this.oParentObject.w_OLTCICLO)+"";
                      +" order by CPROWORD";
                       ,"_Curs_TAB_CICL")
                else
                  select CPROWORD from (i_cTable);
                   where CSCODICE=this.oParentObject.w_OLTCICLO;
                   order by CPROWORD;
                    into cursor _Curs_TAB_CICL
                endif
                if used('_Curs_TAB_CICL')
                  select _Curs_TAB_CICL
                  locate for 1=1
                  do while not(eof())
                  this.w_ULTFAS = NVL(_Curs_TAB_CICL.CPROWORD, 0)
                    select _Curs_TAB_CICL
                    continue
                  enddo
                  use
                endif
              endif
            endif
            * --- Causale Impegno
            this.w_FLO = " "
            this.w_FLI = " "
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLORDI,CMFLIMPE"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUIMP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLORDI,CMFLIMPE;
                from (i_cTable) where;
                    CMCODICE = this.oParentObject.w_CAUIMP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLO = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
              this.w_FLI = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Cursore di Elaborazione
            CREATE CURSOR __TMP__ ;
            (CODCOM C(20), DESCOM C(40), ARTCOM C(20), UNIMIS C(3), QTAMOV N(12,3), QTAUM1 N(12,3), TIPGES C(1), FLESPL C(1), COEIMP N(15,6), ;
            MAGPRE C(5), FLPREV C(1), CODDIS C(20), RIFFAS N(4,0), TIPART C(2), CPRFAS N(4,0), CORLEA N(7,2))
            this.w_RIFFAS = 0
            this.w_CPRFAS = 0
            this.w_DESFAS = SPACE(40)
            select TES_PLOS
            GO TOP
            * --- Cicla sulla Distinta Base Esplosa
            SCAN FOR NOT EMPTY(DISPAD)
            this.w_NUMLEV = NUMLEV
            this.w_CODCOM = CODCOM
            this.w_DESCOM = DESCOM
            this.w_ARTCOM = ARTCOM
            this.w_UNIMIS = UNMIS0
            this.w_QTAMOV = QTAMOV
            this.w_QTAUM1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
            this.w_COEIMP = cp_ROUND(NVL(QTAMOV, 0), g_PERPQD)
            this.w_ARTIPGES = NVL(TIPGES , " ")
            this.w_FLESPL = NVL(FLESPL, " ")
            this.w_OLMAGPRE = NVL(MAGPRE, " ")
            this.w_TIPPRE = IIF(EMPTY(NVL(FLPREV, " ")), "N", NVL(FLPREV, "N"))
            this.w_TIPART = NVL(TIPART, SPACE(2))
            this.w_CODDIS = NVL(CODDIS, SPACE(20))
            this.w_CORLEA = NVL(CORLEA,0)
            this.w_DISPAD = DISPAD
            this.w_COROWNUM = CPROWNUM
            if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
              if VAL(this.w_NUMLEV) < 2
                * --- Determina fase di utilizzo
                * --- Select from GSCO_BOL3
                do vq_exec with 'GSCO_BOL3',this,'_Curs_GSCO_BOL3','',.f.,.t.
                if used('_Curs_GSCO_BOL3')
                  select _Curs_GSCO_BOL3
                  locate for 1=1
                  if not(eof())
                  do while not(eof())
                  this.w_RIFFAS = _Curs_GSCO_BOL3.CPROWORD
                  this.w_CPRFAS = _Curs_GSCO_BOL3.CPROWNUM
                  this.w_DESFAS = _Curs_GSCO_BOL3.CLFASDES
                    select _Curs_GSCO_BOL3
                    continue
                  enddo
                  else
                    this.w_RIFFAS = 0
                    this.w_CPRFAS = 0
                    this.w_DESFAS = SPACE(40)
                    select _Curs_GSCO_BOL3
                  endif
                  use
                endif
                if this.w_RIFFAS=0
                  this.w_RIFFAS = this.w_ULTFAS
                  this.w_CPRFAS = this.w_CPULTFAS
                endif
                * --- Setta Codice Ciclo
                this.GSCO_MOL.w_CODCICLO = this.oParentObject.w_OLTSECIC
                this.GSCO_MOL.w_FASEPREF = "00"
              endif
            else
              if this.oParentObject.w_OLTCICLO=this.oParentObject.w_DEFCICLO
                this.w_RIFFAS = IIF(NVL(RIFFAS, 0)=0, this.w_ULTFAS, RIFFAS)
              endif
            endif
            * --- Codice Articolo Associato al Componente
            if VAL(this.w_NUMLEV)=0
              * --- Inizia una nuova Distinta Base, inizia da qui a calcolare le qta dei Componenti 
              this.w_QTAMOV = this.oParentObject.w_OLTQTOD1
              this.w_QTAUM1 = this.oParentObject.w_OLTQTOD1
              FOR L_i = 1 TO 99
              QTC[L_i, 1] = 0
              QTC[L_i, 2] = 0
              ENDFOR
            else
              * --- Componente: Prodotto Finito o Semilavorato
              * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
              QTC[VAL(this.w_NUMLEV), 1] = this.w_QTAMOV
              * --- Viene calcolala anche la Qta relativa alla U.M. Principale, per il calcolo del prezzo Unitario 
              QTC[VAL(this.w_NUMLEV), 2] = this.w_QTAUM1
              this.w_QTAMOV = 1
              this.w_QTAUM1 = 1
              FOR L_i = 1 TO VAL(this.w_NUMLEV)
              this.w_QTAMOV = this.w_QTAMOV * IIF(L_i=VAL(this.w_NUMLEV), QTC[L_i, 1] , QTC[L_i, 2] )
              this.w_QTAUM1 = this.w_QTAUM1 * QTC[L_i, 2]
              ENDFOR
              this.w_COEIMP = this.w_QTAMOV
              this.w_QTAMOV = this.w_QTAMOV * this.oParentObject.w_OLTQTOD1
              this.w_QTAUM1 = this.w_QTAUM1 * this.oParentObject.w_OLTQTOD1
            endif
            * --- Scrivo il Temporaneo
            INSERT INTO __TMP__ ;
            (CODCOM, DESCOM, ARTCOM, UNIMIS, QTAMOV, QTAUM1, TIPGES, FLESPL, COEIMP, MAGPRE, FLPREV, CODDIS, RIFFAS,TIPART,CPRFAS,CORLEA) ;
            VALUES (this.w_CODCOM, this.w_DESCOM, this.w_ARTCOM, this.w_UNIMIS, cp_ROUND(this.w_QTAMOV, 3), cp_ROUND(this.w_QTAUM1, 3), this.w_ARTIPGES, this.w_FLESPL, ;
            this.w_COEIMP, this.w_OLMAGPRE, this.w_TIPPRE, this.w_CODDIS, this.w_RIFFAS, this.w_TIPART, this.w_CPRFAS, this.w_CORLEA)
            SELECT TES_PLOS
            ENDSCAN
            * --- Chiude Cursore Esplosione
            USE IN SELECT("TES_PLOS")
            if used("__TMP__")
              * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
              SELECT (nc)
              delete ALL
              * --- Scorre Cursore con materie prime
              this.w_ROWORD = 0
              select __TMP__
              go top
              SCAN FOR NOT EMPTY(NVL(CODCOM,""))
              * --- Creazione DETTAGLIO ODL
              this.w_TIPART = NVL(TIPART, SPACE(2))
              this.w_ARTIPGES = NVL(TIPGES , " ")
              this.w_FLESPL = NVL(FLESPL, " ")
              * --- Salta i materiali gestiti a consumo 
              if this.w_ARTIPGES <> "C" and this.w_TIPART<>"PH"
                * --- Nuova Riga del Temporaneo
                this.GSCO_MOL.InitRow()     
                * --- Valorizza Variabili di Riga ...
                select __TMP__
                this.GSCO_MOL.w_OLCODICE = NVL(CODCOM ," ")
                this.GSCO_MOL.w_DESART = NVL(DESCOM , " ")
                this.GSCO_MOL.w_OLCODART = NVL(ARTCOM , " ")
                this.GSCO_MOL.w_OLUNIMIS = NVL(UNIMIS ," ")
                this.GSCO_MOL.w_OLCOEIMP = NVL(COEIMP , 0)
                this.GSCO_MOL.w_OLQTAMOV = NVL(QTAMOV, 0)
                this.GSCO_MOL.w_OLQTAUM1 = NVL(QTAUM1, 0)
                this.GSCO_MOL.w_CODDIS = NVL(CODDIS, SPACE(20))
                this.GSCO_MOL.w_OLCORRLT = NVL(CORLEA, 0)
                this.w_RIFFAS = NVL(RIFFAS, 0)
                this.w_DESFAS = SPACE(40)
                if g_PRFA<>"S" or g_CICLILAV<>"S"
                  if this.w_RIFFAS<>0
                    * --- Read from TAB_CICL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.TAB_CICL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2],.t.,this.TAB_CICL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "CSDESFAS"+;
                        " from "+i_cTable+" TAB_CICL where ";
                            +"CSCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCICLO);
                            +" and CPROWORD = "+cp_ToStrODBC(this.w_RIFFAS);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        CSDESFAS;
                        from (i_cTable) where;
                            CSCODICE = this.oParentObject.w_OLTCICLO;
                            and CPROWORD = this.w_RIFFAS;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_DESFAS = NVL(cp_ToDate(_read_.CSDESFAS),cp_NullValue(_read_.CSDESFAS))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  this.GSCO_MOL.w_OLRIFFAS = this.w_RIFFAS
                else
                  this.w_CPRFAS = NVL(CPRFAS,0)
                  this.GSCO_MOL.w_OLCPRFAS = this.w_CPRFAS
                  this.GSCO_MOL.w_OLFASRIF = this.w_RIFFAS
                endif
                this.GSCO_MOL.w_DESFAS = this.w_DESFAS
                this.w_ROWORD = this.w_ROWORD+1
                this.GSCO_MOL.w_CPROWORD = this.w_ROWORD * 10
                this.w_ARTBASE = this.GSCO_MOL.w_OLCODART
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARMAGPRE,ARTIPPRE,ARSALCOM"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(this.w_ARTBASE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARMAGPRE,ARTIPPRE,ARSALCOM;
                    from (i_cTable) where;
                        ARCODART = this.w_ARTBASE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                  this.w_OP = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
                  this.w_MOLT = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
                  this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
                  this.w_MAG = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
                  this.w_TP = NVL(cp_ToDate(_read_.ARTIPPRE),cp_NullValue(_read_.ARTIPPRE))
                  this.w_FLCOMM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.GSCO_MOL.w_UNMIS1 = this.w_UM1
                this.GSCO_MOL.w_UNMIS2 = this.w_UM2
                this.GSCO_MOL.w_OPERAT = this.w_OP
                this.GSCO_MOL.w_MOLTIP = this.w_MOLT
                this.GSCO_MOL.w_MAGPRE = this.w_MAG
                this.GSCO_MOL.w_TIPPRE = this.w_TP
                this.GSCO_MOL.w_OLTIPPRE = IIF(EMPTY(this.w_TP), "N", this.w_TP)
                this.w_OLMAGWIP = IIF(this.oParentObject.w_OLTPROVE="L", this.oParentObject.w_MAGTER, this.oParentObject.w_MAGWIP)
                do case
                  case this.w_TIPIMP="C"
                    this.w_OLMAGPRE = IIF(EMPTY(this.w_MAG), this.oParentObject.w_MAGPAR, this.w_MAG)
                  case this.w_TIPIMP="T"
                    this.w_OLMAGPRE = IIF(EMPTY(this.oParentObject.w_OLTCOMAG), this.oParentObject.w_MAGPAR, this.oParentObject.w_OLTCOMAG)
                  case this.w_TIPIMP="F"
                    this.w_OLMAGPRE = IIF(EMPTY(this.w_MAGIMP), this.oParentObject.w_MAGPAR, this.w_MAGIMP)
                endcase
                this.w_OLCODMAG = this.w_OLMAGPRE
                this.w_CODBASE = this.GSCO_MOL.w_OLCODICE
                * --- Read from KEY_ARTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CAUNIMIS,CAOPERAT,CAMOLTIP,CATIPCON"+;
                    " from "+i_cTable+" KEY_ARTI where ";
                        +"CACODICE = "+cp_ToStrODBC(this.w_CODBASE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CAUNIMIS,CAOPERAT,CAMOLTIP,CATIPCON;
                    from (i_cTable) where;
                        CACODICE = this.w_CODBASE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
                  this.w_OP3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
                  this.w_MOLT3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
                  this.w_TIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.GSCO_MOL.w_UNMIS3 = this.w_UM3
                this.GSCO_MOL.w_OPERA3 = this.w_OP3
                this.GSCO_MOL.w_MOLTI3 = this.w_MOLT3
                this.GSCO_MOL.w_TIPCON = this.w_TIPCON
                this.GSCO_MOL.w_OLQTAEVA = 0
                this.GSCO_MOL.w_OLQTAEV1 = 0
                this.GSCO_MOL.w_OLFLEVAS = " "
                this.w_COCORLEA = NVL(CORLEA, 0)
                this.GSCO_MOL.w_OLDATRIC = COCALCLT(this.oParentObject.w_OLTDINRIC,Ceiling(this.w_COCORLEA),"C", .F. ,"")
                this.w_QTBASE = this.GSCO_MOL.w_OLQTAMOV
                this.w_UMBASE = this.GSCO_MOL.w_OLUNIMIS
                this.GSCO_MOL.w_OLQTASAL = this.GSCO_MOL.w_OLQTAUM1
                this.GSCO_MOL.w_OLQTAPRE = 0
                this.GSCO_MOL.w_OLQTAPR1 = 0
                this.GSCO_MOL.w_OLFLPREV = " "
                this.GSCO_MOL.w_OLCAUMAG = this.oParentObject.w_CAUIMP
                this.GSCO_MOL.w_OLFLORDI = this.w_FLO
                this.GSCO_MOL.w_OLFLIMPE = this.w_FLI
                this.GSCO_MOL.w_OLKEYSAL = this.GSCO_MOL.w_OLCODART
                this.GSCO_MOL.w_FLCOMM = this.w_FLCOMM
                this.GSCO_MOL.w_OLCODCOM = iif(NVL(this.w_FLCOMM,"N")="S" and not empty(this.oParentObject.w_OLTCOMME),this.oParentObject.w_OLTCOMME,space(15))
                * --- Assegna deposito di riga
                * --- Read from MAGAZZIN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MGDISMAG"+;
                    " from "+i_cTable+" MAGAZZIN where ";
                        +"MGCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MGDISMAG;
                    from (i_cTable) where;
                        MGCODMAG = this.w_OLCODMAG;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_PRENET = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from MAGAZZIN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MGDISMAG"+;
                    " from "+i_cTable+" MAGAZZIN where ";
                        +"MGCODMAG = "+cp_ToStrODBC(this.w_OLMAGPRE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MGDISMAG;
                    from (i_cTable) where;
                        MGCODMAG = this.w_OLMAGPRE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_MGPRENET = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.GSCO_MOL.w_OLCODMAG = this.w_OLCODMAG
                this.GSCO_MOL.w_PRENET = this.w_PRENET
                this.GSCO_MOL.w_OLMAGPRE = this.w_OLMAGPRE
                this.GSCO_MOL.w_MGPRENET = this.w_MGPRENET
                this.GSCO_MOL.w_OLMAGWIP = this.w_OLMAGWIP
                this.GSCO_MOL.w_OLEVAAUT = "N"
                if this.oParentObject.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR) AND USED("_MatTer_")
                  * --- Legge Eventuali Materiali del Terzista
                  select __TMP__
                  this.w_APPO = NVL(CODCOM ," ")
                  select _MatTer_
                  go top
                  LOCATE FOR NVL(MCCODICE, SPACE(20))=this.w_APPO
                  if FOUND()
                    * --- Se il componente e' presente nei materiali del Terzista non deve eseguire alcun Impegno ne Trasferimento
                    this.GSCO_MOL.w_OLQTAEVA = this.GSCO_MOL.w_OLQTAMOV
                    this.GSCO_MOL.w_OLQTAEV1 = this.GSCO_MOL.w_OLQTAUM1
                    this.GSCO_MOL.w_OLFLEVAS = "S"
                    this.GSCO_MOL.w_OLQTASAL = 0
                    this.GSCO_MOL.w_OLTIPPRE = "N"
                    this.GSCO_MOL.w_OLEVAAUT = "S"
                  endif
                endif
                * --- Carica il Temporaneo dei Dati e skippa al record successivo
                this.GSCO_MOL.TrsFromWork()     
                select __TMP__
              endif
              ENDSCAN
              * --- Rinfrescamenti vari
              SELECT (nc) 
 GO TOP 
 With this.GSCO_MOL 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
              * --- Chiude Cursore Elaborazione
              use in select("__TMP__")
              * --- Aggiorna anche ciclo ODL
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_RIGAVUOTA = "N"
              if this.w_PPMATOUP<>"N" or empty(this.oParentObject.w_OLTSECIC)
                * --- Se ho abilitato il flag 'NON ASSOCIARE' non inserisco alcun materiale di output, il flag se non ho i cicli non � valido.
                this.ROWORD = 0
                SELECT (mo)
                delete ALL
                if this.oParentObject.w_OLTPROVE="I"
                  * --- Carico la lista dei materiali di output solo se sono in caricamento di un ODL
                  if empty(this.oParentObject.w_OLTSECIC)
                    * --- Select from GSCO_BOL7
                    do vq_exec with 'GSCO_BOL7',this,'_Curs_GSCO_BOL7','',.f.,.t.
                    if used('_Curs_GSCO_BOL7')
                      select _Curs_GSCO_BOL7
                      locate for 1=1
                      do while not(eof())
                      * --- --Modifica transitorio
                      this.w_RIGAVUOTA = "S"
                      this.GSCO_MMO.AddRow()     
                      this.ROWORD = this.ROWORD+1
                      = SETVALUELINKED("D" , this.GSCO_MMO , "w_MOCODICE" , nvl(_Curs_GSCO_BOL7.MOCODRIC, SPACE(20)) )
                      = SETVALUELINKED("D" , this.GSCO_MMO , "w_MOCODART" , nvl(_Curs_GSCO_BOL7.MOCODART, space(20)) )
                      this.GSCO_MMO.w_MOUNIMIS = NVL(_Curs_GSCO_BOL7.MOUNIMIS, SPACE(3))
                      this.GSCO_MMO.w_MOCOEIMP = NVL(_Curs_GSCO_BOL7.MOCOEIMP,0)
                      this.GSCO_MMO.w_MOCOEIM1 = NVL(_Curs_GSCO_BOL7.MOCOEIM1, 0)
                      this.GSCO_MMO.TrsFromWork()     
                      this.GSCO_MMO.mCalc(.t.)     
                      this.GSCO_MMO.SaveRow()     
                        select _Curs_GSCO_BOL7
                        continue
                      enddo
                      use
                    endif
                  else
                    if this.w_PPMATOUP="P"
                      * --- Cerco la prima fase di output per associare i materiali
                      * --- Select from CIC_DETT
                      i_nConn=i_TableProp[this.CIC_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2],.t.,this.CIC_DETT_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select min(CPROWORD) as RIGASEL  from "+i_cTable+" CIC_DETT ";
                            +" where CLSERIAL="+cp_ToStrODBC(this.oParentObject.w_OLTSECIC)+" and CIC_DETT.CLFASOUT='S'";
                             ,"_Curs_CIC_DETT")
                      else
                        select min(CPROWORD) as RIGASEL from (i_cTable);
                         where CLSERIAL=this.oParentObject.w_OLTSECIC and CIC_DETT.CLFASOUT="S";
                          into cursor _Curs_CIC_DETT
                      endif
                      if used('_Curs_CIC_DETT')
                        select _Curs_CIC_DETT
                        locate for 1=1
                        do while not(eof())
                        this.w_FASMAT = RIGASEL
                          select _Curs_CIC_DETT
                          continue
                        enddo
                        use
                      endif
                    else
                      * --- Cerco l'ultima fase di output per associare i materiali
                      * --- Select from CIC_DETT
                      i_nConn=i_TableProp[this.CIC_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2],.t.,this.CIC_DETT_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select max(CPROWORD) as RIGASEL  from "+i_cTable+" CIC_DETT ";
                            +" where CLSERIAL="+cp_ToStrODBC(this.oParentObject.w_OLTSECIC)+" and CIC_DETT.CLFASOUT='S'";
                             ,"_Curs_CIC_DETT")
                      else
                        select max(CPROWORD) as RIGASEL from (i_cTable);
                         where CLSERIAL=this.oParentObject.w_OLTSECIC and CIC_DETT.CLFASOUT="S";
                          into cursor _Curs_CIC_DETT
                      endif
                      if used('_Curs_CIC_DETT')
                        select _Curs_CIC_DETT
                        locate for 1=1
                        do while not(eof())
                        this.w_FASMAT = RIGASEL
                          select _Curs_CIC_DETT
                          continue
                        enddo
                        use
                      endif
                    endif
                    * --- Select from GSCO_BOL4
                    do vq_exec with 'GSCO_BOL4',this,'_Curs_GSCO_BOL4','',.f.,.t.
                    if used('_Curs_GSCO_BOL4')
                      select _Curs_GSCO_BOL4
                      locate for 1=1
                      do while not(eof())
                      this.w_RIGAVUOTA = "S"
                      * --- --Modifica transitorio
                      this.GSCO_MMO.AddRow()     
                      this.ROWORD = this.ROWORD+1
                      = SETVALUELINKED("D" , this.GSCO_MMO , "w_MOCODICE" , nvl(_Curs_GSCO_BOL4.MOCODRIC, SPACE(20)) )
                      = SETVALUELINKED("D" , this.GSCO_MMO , "w_MOCODART" , nvl(_Curs_GSCO_BOL4.MOCODART, space(20)) )
                      this.GSCO_MMO.w_MOUNIMIS = NVL(_Curs_GSCO_BOL4.MOUNIMIS, SPACE(3))
                      this.GSCO_MMO.w_MOCOEIMP = NVL(_Curs_GSCO_BOL4.MOCOEIMP,0)
                      this.GSCO_MMO.w_MOCOEIM1 = NVL(_Curs_GSCO_BOL4.MOCOEIM1, 0)
                      this.w_FASSEL = iif(NVL(_Curs_GSCO_BOL4.MOROWORD, 0)<>0,_Curs_GSCO_BOL4.MOROWORD,this.w_FASMAT)
                      this.GSCO_MMO.w_MOFASRIF = this.w_FASSEL
                      * --- Read from CIC_DETT
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.CIC_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2],.t.,this.CIC_DETT_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "CPROWNUM"+;
                          " from "+i_cTable+" CIC_DETT where ";
                              +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OLTSECIC);
                              +" and CPROWORD = "+cp_ToStrODBC(this.w_FASSEL);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          CPROWNUM;
                          from (i_cTable) where;
                              CLSERIAL = this.oParentObject.w_OLTSECIC;
                              and CPROWORD = this.w_FASSEL;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_FASCPR = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      this.GSCO_MMO.w_MOROWNUM = this.w_FASCPR
                      this.GSCO_MMO.TrsFromWork()     
                      this.GSCO_MMO.mCalc(.t.)     
                      this.GSCO_MMO.SaveRow()     
                        select _Curs_GSCO_BOL4
                        continue
                      enddo
                      use
                    endif
                  endif
                  if this.w_RIGAVUOTA="S"
                    this.GSCO_MMO.AddRow()     
                    this.GSCO_MMO.SaveRow()     
                  endif
                endif
              endif
              if g_PRFA="S" and g_CICLILAV="S"
                * --- Sistema le fasi di riferimento
                GSCI_BML(this.GSCO_MOL, "INSFASI")
              endif
              if USED("_MatTer_")
                select _MatTer_
              endif
              this.oParentObject.w_CHKDETT = .T.
            endif
          endif
          * --- Messagio Finale
          if this.oParentObject.w_OLTPROVE <> "E"
            ah_ErrorMsg("Generazione/aggiornamento dati collegati a ODL completata",,"" )
          else
            ah_ErrorMsg("Aggiornamento ODA completato",,"" )
          endif
          USE IN SELECT("ELABREC")
        else
        endif
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Passa di qui quando cambio QUANTITA, oppure FORNITORE, oppure PROVENIENZA
    * --- Aggiorna date
    this.w_DATRIF = this.oParentObject.w_OLTDTRIC
    * --- Determina periodo di appartenenza dell'ODL/OCL
    vq_exec ("..\COLA\EXE\QUERY\GSCO_BOL", this, "__Temp__")
    if used("__Temp__")
      * --- Preleva periodo assoluto
      select __Temp__
      go top
      this.oParentObject.w_OLTPERAS = nvl(__Temp__.TPPERASS, "XXX")
      * --- Chiude cursore
      USE IN __Temp__
    endif
    this.oParentObject.w_OLTPERAS = IIF(Empty(this.oParentObject.w_OLTPERAS), "XXX" , this.oParentObject.w_OLTPERAS)
    * --- Verifica se deve ricalcolare LT e QTA
    this.w_CONUMERO = space(15)
    this.w_OLDLEADTIME = this.oParentObject.w_OLTEMLAV
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorna i tempi ?
    if this.Operazione=="CHANGE" And ((!this.oParentObject.w_CALCDATA And this.w_RICALCOLA) or (this.oParentObject.w_ERDATINI))
      this.TmpL = FALSE
    else
      this.TmpL = TRUE
    endif
    if NOT EMPTY(this.oParentObject.w_OLTDTRIC) AND this.Operazione = "QTAVAR" AND this.oParentObject.w_OLTEMLAV<>this.w_OLDLEADTIME
      * --- Variata la quantita'
      this.TmpL = ah_YesNo("� stata variata la quantit� prevista per l'ordine%0Desideri che venga ricalcolata la data di inizio lavorazione%0sulla base del lead-time presente sull'ordine?")
    else
      if NOT EMPTY(this.oParentObject.w_OLTDINRIC) AND this.oParentObject.w_OLTEMLAV<>this.w_OLDLEADTIME
        * --- Variata la quantita'
        this.TmpL = ah_YesNo("� stato variato il lead-time presente sull'ordine%0Desideri che venga ricalcolata la data di inizio lavorazione?")
      endif
    endif
    if this.TmpL=.T.
      * --- Ricalcola data di inizio (Produzione)
      this.oParentObject.w_OLTDINRIC = COCALCLT(this.oParentObject.w_OLTDTRIC, this.oParentObject.w_OLTEMLAV, "I", .F., "")
      * --- Ricalcola data di inizio (MPS)
      this.oParentObject.w_OLTDTMPS = COCALCLT(this.oParentObject.w_OLTDINRIC, this.oParentObject.w_OLTEMLAV, "A", .F., "")
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina Qta e Tempi per C/L
    this.w_QTALOT = 0
    this.w_QTAMIN = 0
    this.w_GIOAPP = 0
    this.w_QTACHK = .F.
    if empty(nvl(this.w_CONUMERO,"")) and this.Operazione="QTAVAR" 
      this.w_QTACHK = .T.
    endif
    if (g_MODA="S" and this.oParentObject.w_OLTPROVE="E") Or (g_COLA="S" and this.oParentObject.w_OLTPROVE="L") 
      this.w_OLCICLO = IIF(this.oParentObject.w_OLTPROVE="L", "L", "A")
      if this.oParentObject.o_OLTCOFOR<>this.oParentObject.w_OLTCOFOR and !Empty(this.oParentObject.w_OLTCOFOR) And !Empty(this.oParentObject.w_OLTDTRIC) Or !this.oParentObject.w_CALCCONT And (!Empty(this.oParentObject.w_OLTCOFOR) And !Empty(this.oParentObject.w_OLTDTRIC))
        if !this.oParentObject.w_CALCCONT
          this.oParentObject.w_CALCCONT = .T.
          this.w_RICALCOLA = .T.
        endif
      endif
      if Inlist(this.Operazione , "QTAVAR" , "CICLI", "PROVENIENZA" , "CHANGEFOR", "CARICA_DETT") Or this.Operazione=="CHANGE" 
        this.w_CHKCONDL = !Empty(this.oParentObject.w_OLTCOFOR) And !Empty(this.oParentObject.w_OLTQTODL) And !Empty(this.oParentObject.w_OLTDTRIC) And !Empty(this.oParentObject.w_OLTDINRIC) And !Empty(this.oParentObject.w_OLTCOMAG)
        * --- Nel caso in cui all'evento CHANGE non ci sia nessuna modifica sulle date non faccio niente
        if this.Operazione=="CHANGE" And this.oParentObject.w_ERDATINI
          this.w_CHKCONDL = .f.
        endif
        if Inlist(this.Operazione , "CHANGEFOR" , "CICLI", "PROVENIENZA" )
          * --- Se cambio la provenienza oppure cambio il fornitore ricalcolo sempre i contratti
          this.w_RICALCOLA = .T.
        else
          if this.w_CHKCONDL
            this.w_RICALCOLA = ah_YesNo("Desideri che avvenga il ricalcolo del contratto?")
          endif
          * --- Ho cambiato le date le date sull'ODL e ho risposto NO alla domanda del ricalcolo dei contratti, quindi esco
          if this.Operazione=="CHANGE" And !this.w_RICALCOLA
            i_retcode = 'stop'
            return
          endif
        endif
        if this.w_RICALCOLA
          * --- Resetto le variabili dell'ANAGRAFICA
          this.w_CONUMERO = space(15)
          this.w_CONUMEROOK = space(15)
          this.w_CONUMROW = 0
          this.w_GIOAPP = 0
          this.oParentObject.w_OLTCONTR = space(15)
        else
          * --- Se non ricalcolo il listino recupero i valori dall'anagrafica
          this.w_CONUMERO = this.oParentObject.w_OLTCONTR
          this.w_CONUMEROOK = this.oParentObject.w_OLTCONTR
          this.w_CONUMROW = 0
          this.w_GIOAPP = this.oParentObject.w_OLTEMLAV
        endif
        * --- w_CHKCONDL (Se la condizione � piena allora faccio la domanda) altrimenti lo chiede sempre
      else
        this.w_CONUMERO = SPACE(15)
        this.w_CONUMEROOK = SPACE(15)
        if !Empty(this.oParentObject.w_OLTCONTR)
          this.w_RICALCOLA = this.Operazione == "CHANGECONT"
        else
          this.w_RICALCOLA = .F.
        endif
      endif
      * --- Provenienza C/Lavoro (Tranne Gestione Scorta)
      if not empty(this.oParentObject.w_OLTCOFOR) And this.w_RICALCOLA
        this.w_CODART = this.oParentObject.w_OLTCOART
        this.w_CODGRU = this.oParentObject.w_AGRUMER
        this.w_DATORD = this.oParentObject.w_OLTDTRIC && IIF(EMPTY(this.oParentObject.w_OLDATODP), IIF(EMPTY(this.oParentObject.w_OLDATODL), i_DATSYS, this.oParentObject.w_OLDATODL), this.oParentObject.w_OLDATODP)
        * --- Query su contratti
        this.w_OK0 = .F.
        this.w_OK1 = .F.
        if this.oParentObject.w_PREZUM = "S"
          this.w_QTAUM1 = this.oParentObject.w_OLTQTODL
          this.w_UNMIS = this.oParentObject.w_OLTUNMIS
        else
          this.w_QTAUM1 = this.oParentObject.w_OLTQTOD1
          this.w_UNMIS = this.oParentObject.w_UNMIS1
          this.w_QTAUM3 = CALQTA( this.oParentObject.w_OLTQTOD1 ,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
          this.w_QTAUM2 = CALQTA( this.oParentObject.w_OLTQTOD1 ,this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
        endif
        * --- Select from PRDSETLT
        do vq_exec with 'PRDSETLT',this,'_Curs_PRDSETLT','',.f.,.t.
        if used('_Curs_PRDSETLT')
          select _Curs_PRDSETLT
          locate for 1=1
          do while not(eof())
          do case
            case NOT EMPTY(NVL(_Curs_PRDSETLT.COCODART, " "))
              * --- Cerca il Contratto su Articolo (Preferenziale)
              this.w_CONUMERO = _Curs_PRDSETLT.CONUMERO
              this.w_QTALOT0 = NVL(_Curs_PRDSETLT.COLOTMUL, 0)
              this.w_QTAMIN0 = NVL(_Curs_PRDSETLT.COQTAMIN, 0)
              this.w_GIOAPP0 = NVL(_Curs_PRDSETLT.COGIOAPP, 0)
              this.w_CONUMROW = _Curs_PRDSETLT.CPROWNUM
              this.w_OK0 = .T.
            case NOT EMPTY(NVL(_Curs_PRDSETLT.COGRUMER, " "))
              * --- Oppure Quello per Gruppo Merceologico (Alternativo all'Articolo)
              this.w_CONUMERO = _Curs_PRDSETLT.CONUMERO
              this.w_QTALOT1 = NVL(_Curs_PRDSETLT.COLOTMUL, 0)
              this.w_QTAMIN1 = NVL(_Curs_PRDSETLT.COQTAMIN, 0)
              this.w_GIOAPP1 = NVL(_Curs_PRDSETLT.COGIOAPP, 0)
              this.w_CONUMROW = 0
              this.w_OK1 = .T.
          endcase
          do case
            case this.w_OK0=.T.
              this.w_QTALOT = this.w_QTALOT0
              this.w_QTAMIN = this.w_QTAMIN0
              this.w_GIOAPP = this.w_GIOAPP0
              this.w_CONUMEROOK = this.w_CONUMERO
            case this.w_OK1=.T.
              this.w_QTALOT = this.w_QTALOT1
              this.w_QTAMIN = this.w_QTAMIN1
              this.w_GIOAPP = this.w_GIOAPP1
              this.w_CONUMEROOK = this.w_CONUMERO
          endcase
            select _Curs_PRDSETLT
            continue
          enddo
          use
        endif
      endif
      this.w_CONUMERO = this.w_CONUMEROOK
      this.oParentObject.w_OLTCONTR = this.w_CONUMERO
      * --- Read from CON_TRAM
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CON_TRAM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CONUMERO,CODESCON,CODATCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS"+;
          " from "+i_cTable+" CON_TRAM where ";
              +"CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_OLTCONTR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CONUMERO,CODESCON,CODATCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
          from (i_cTable) where;
              CONUMERO = this.oParentObject.w_OLTCONTR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_OLTCONTR = NVL(cp_ToDate(_read_.CONUMERO),cp_NullValue(_read_.CONUMERO))
        this.oParentObject.w_CONDES = NVL(cp_ToDate(_read_.CODESCON),cp_NullValue(_read_.CODESCON))
        this.oParentObject.w_DATCON = NVL(cp_ToDate(_read_.CODATCON),cp_NullValue(_read_.CODATCON))
        this.oParentObject.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
        this.oParentObject.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
        this.oParentObject.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
        this.oParentObject.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
        this.oParentObject.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
        this.oParentObject.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
        this.oParentObject.w_IVACON = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_CONUMERO)
        * --- Legge parametri da dati-magazzino
        this.w_QTALOT = this.oParentObject.w_LOTRIO
        this.w_QTAMIN = this.oParentObject.w_LOTECO
        this.w_GIOAPP = this.oParentObject.w_LEAFIS
        this.w_QTAMAX = this.oParentObject.w_LOTMAX
        * --- Setta valori su
        if EMPTY(this.w_CODFOR) OR this.w_CODFOR = this.oParentObject.w_OLTCOFOR
          * --- Accetta i parametri letti
        else
          * --- Azzera  i parametri letti perch� non corrispondono al fornitore che c'� sull'ordine
          this.w_QTALOT = 0
          this.w_QTAMIN = 0
          this.w_GIOAPP = 0
        endif
      endif
    endif
    * --- Se gestione a scorta cerco solo il codice del contratto lascio i parametri dei dati articolo
    * --- Legge parametri da dati-magazzino
    if this.oParentObject.w_ATIPGES="S"
      this.w_QTALOT = this.oParentObject.w_LOTRIO
      this.w_QTAMIN = this.oParentObject.w_LOTECO
      if this.oParentObject.w_OLTPROVE="I"
        this.w_GIOAPP = this.w_GIOAPP + iif(this.oParentObject.w_LOTMED=0, 0, this.w_GIOAPP * this.oParentObject.w_LEAVAR * ((this.oParentObject.w_OLTQTOD1/this.oParentObject.w_LOTMED) - 1))
      else
        this.w_GIOAPP = this.oParentObject.w_LEAFIS
      endif
      this.w_QTAMAX = this.oParentObject.w_LOTMAX
    endif
    if this.oParentObject.w_OGGMPS <> "N"
      this.oParentObject.w_OLTSAFLT = 0
    endif
    * --- ADATTA QTA ALLA POLITICA DI RIORDINO LETTA
    this.TmpN = this.oParentObject.w_OLTQTODL
    this.TmpN1 = this.oParentObject.w_OLTQTOD1
    if this.oParentObject.w_ARTIPART<>"FS"
      if this.oParentObject.w_ATIPGES <> "C"
        if this.w_QTACHK and not empty(nvl(this.w_CONUMERO,"")) and this.Operazione="QTAVAR" and this.oParentObject.w_OLTQTODL= 0 
          this.TmpN1 = this.w_QTAMIN
          if this.w_QTALOT>0
            this.TmpN1 = iif( Mod(this.TmpN1,this.w_QTALOT)=0, this.TmpN1, (Int(this.TmpN1/this.w_QTALOT)+1)*this.w_QTALOT)
          endif
        else
          if this.w_QTAMIN+this.w_QTALOT<>0 and this.TmpN1>0
            if this.TmpN1 < this.w_QTAMIN and this.w_QTAMIN>0
              this.TmpN1 = this.w_QTAMIN
            endif
            if this.w_QTALOT>0
              this.TmpN1 = iif( Mod(this.TmpN1,this.w_QTALOT)=0, this.TmpN1, (Int(this.TmpN1/this.w_QTALOT)+1)*this.w_QTALOT)
            endif
          endif
        endif
        if this.oParentObject.w_ATIPGES="F"
          if this.w_QTAMAX<>0 and this.TmpN1>0 and this.TmpN1>this.w_QTAMAX
            this.TmpN1 = this.w_QTAMAX
          endif
        endif
      else
        this.TmpN1 = w_PUNLOT
      endif
    endif
    * --- Se variata la qta nella 1^UM la ricalcola nella seconda UM (solo se Pianificata)
    if this.oParentObject.w_OLTQTOD1 <> this.TmpN1
      if this.oParentObject.w_OLTUNMIS <> this.oParentObject.w_UNMIS1
        this.w_UM1 = iif(this.oParentObject.w_OLTUNMIS=this.oParentObject.w_UNMIS3 , this.oParentObject.w_UNMIS3, this.oParentObject.w_UNMIS2)
        this.w_UM2 = this.oParentObject.w_UNMIS1
        this.w_MOLT = iif(this.oParentObject.w_OLTUNMIS=this.oParentObject.w_UNMIS3, this.oParentObject.w_MOLTI3 , this.oParentObject.w_MOLTIP)
        this.w_OP = iif(this.oParentObject.w_OLTUNMIS=this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3 , this.oParentObject.w_OPERAT)
        this.w_OP = iif(this.w_OP="*", "/", "*")
        this.w_UM3 = "   "
        this.w_OP3 = " "
        this.w_MOLT3 = 0
        this.TmpN = CALQTA(this.TmpN1, this.w_UM2,this.w_UM2, this.w_OP, this.w_MOLT, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2,, this.w_UM3, this.w_OP3, this.w_MOLT3)
      else
        this.TmpN = this.TmpN1
      endif
    endif
    if this.Operazione = "QTAVAR"
      if this.oParentObject.w_OLTQTOD1 <> this.TmpN1
        this.TmpC = "Quantit� inserita non compatibile con politica di lottizzazione%0Quantit� corretta: %1 %2 %0Si vuole adeguare la quantit� alla politica di lottizzazione?"
        if ah_YesNo(this.TmpC,"",this.oParentObject.w_OLTUNMIS,tran(this.TmpN,v_PQ(12) ))
          this.oParentObject.w_OLTQTODL = this.TmpN
          this.oParentObject.w_OLTQTOD1 = this.TmpN1
        endif
      endif
    else
      * --- E' cambiato il fornitore o la provenienza
      this.TmpC = " "
      if this.oParentObject.w_OLTQTOD1 <> this.TmpN1
        this.TmpC = "Adeguare quantit� ordine a politica di lottizzazione%0Quantit� originaria: %1 %2%0Quantit� calcolata:%3 %4 "
        if ah_YesNo(this.TmpC,"",this.oParentObject.w_OLTUNMIS,tran(this.oParentObject.w_OLTQTODL,v_PQ(12) ),this.oParentObject.w_OLTUNMIS,tran(this.TmpN,v_PQ(12) ) )
          this.oParentObject.w_OLTQTODL = this.TmpN
          this.oParentObject.w_OLTQTOD1 = this.TmpN1
        endif
      endif
    endif
    * --- ASSEGNA NUOVO LEAD-TIME
    this.oParentObject.w_OLTEMLAV = COSETLT(this.oParentObject.w_OLTCODIC, this.oParentObject.w_OLTPROVE, this.oParentObject.w_OLTQTODL, this.oParentObject.w_OLTQTOD1, this.oParentObject.w_OLTCOFOR+this.oParentObject.w_OLTCONTR, this.oParentObject.w_OLTDTRIC, this.oParentObject.w_OLTUNMIS)
    if this.Operazione=="CHANGE" And this.w_RICALCOLA
      * --- Aggiorno dettaglio materiali
      this.GSCO_MOL.workfromtrs()     
      this.GSCO_MOL.trsfromwork()     
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variazione Data di Inizio o Data di Fine
    this.TmpL = .F.
    if this.Operazione = "DATAINIVAR"
      if this.oParentObject.w_OLTDINRIC<=this.oParentObject.w_OLTDTRIC
        * --- Variata la data di inizio
        this.TmpL = ah_YesNo("� stata variata la data di inizio lavorazione%0Desideri che venga ricalcolata la data di fine lavorazione%0sulla base del lead-time presente sull'ordine?")
        * --- Assegno risposta alla variabile sulla maschera per gesire le risposte e il cambio del listino
        this.oParentObject.w_CALCDATA = this.TmpL
      else
        if NOT EMPTY(this.oParentObject.w_OLTDTRIC)
          ah_ErrorMsg("Data inizio produzione superiore a data fine produzione",,"")
          this.oParentObject.w_OLTDINRIC = this.oParentObject.w_TDINRIC
        else
          this.TmpL = .T.
        endif
      endif
      * --- Ricalcola data fine ?
      if this.TmpL=.T.
        * --- Per PRODUZIONE ricalcola Lead-TIME e Data fine prevista
        this.w_UM1 = this.oParentObject.w_UNMIS1
        this.w_UM2 = this.oParentObject.w_UNMIS2
        this.w_UM3 = this.oParentObject.w_UNMIS3
        this.w_OP = this.oParentObject.w_OPERAT
        this.w_MOLT = this.oParentObject.w_MOLTIP
        this.w_OP3 = this.oParentObject.w_OPERA3
        this.w_MOLT3 = this.oParentObject.w_MOLTI3
        this.oParentObject.w_OLTQTOD1 = CALQTA(this.oParentObject.w_OLTQTODL, this.oParentObject.w_OLTUNMIS,this.w_UM2, this.w_OP, this.w_MOLT, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2,, this.w_UM3, this.w_OP3, this.w_MOLT3)
        * --- Ricalcola LT <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        this.oParentObject.w_OLTEMLAV = COSETLT(this.oParentObject.w_OLTCODIC, this.oParentObject.w_OLTPROVE, this.oParentObject.w_OLTQTODL, this.oParentObject.w_OLTQTOD1, this.oParentObject.w_OLTCOFOR+this.oParentObject.w_OLTCONTR, this.oParentObject.w_OLTDTRIC, this.oParentObject.w_OLTUNMIS)
        * --- Ricalcola data di fine (Produzione)
        this.oParentObject.w_OLTDTRIC = COCALCLT(this.oParentObject.w_OLTDINRIC, this.oParentObject.w_OLTEMLAV, "A", .F., "")
        * --- Ricalcola data di consegna (LT sicurezza)
        this.oParentObject.w_OLTDTCON = COCALCLT(this.oParentObject.w_OLTDTRIC,this.oParentObject.w_OLTSAFLT,"A", .F. ,"")
        * --- Ricalcola data di inizio (MPS)
        this.oParentObject.w_OLTDTMPS = COCALCLT(this.oParentObject.w_OLTDINRIC, this.oParentObject.w_OLTEMLAV, "A", .F., "")
      endif
    else
      * --- Variata la data di fine
      if this.Operazione = "DATAFINVAR"
        * --- Variata la data di fine lavorazione
        this.oParentObject.w_OLTDTCON = COCALCLT(this.oParentObject.w_OLTDTRIC,this.oParentObject.w_OLTSAFLT,"A", .F. ,"")
      else
        * --- Variata la data di consegna
        this.oParentObject.w_OLTDTRIC = COCALCLT(this.oParentObject.w_OLTDTCON,this.oParentObject.w_OLTSAFLT,"I", .F., "")
      endif
      if not empty(this.oParentObject.w_OLTDINRIC) AND this.oParentObject.w_OLTDINRIC<=this.oParentObject.w_OLTDTRIC
        * --- Variata la data di fine
        this.TmpL = ah_YesNo("� stata variata la data di fine lavorazione%0Desideri che venga ricalcolata la data di inizio lavorazione%0sulla base del lead-time presente sull'ordine?")
        * --- Assegno risposta alla variabile sulla maschera per gesire le risposte e il cambio del listino
        this.oParentObject.w_CALCDATA = this.TmpL
      else
        this.TmpL = .T.
        * --- Assegno risposta alla variabile sulla maschera per gesire le risposte e il cambio del listino
        this.oParentObject.w_CALCDATA = this.TmpL
      endif
    endif
    * --- Determina periodo di appartenenza dell' ODL/OCL
    this.w_DATRIF = this.oParentObject.w_OLTDTRIC
    vq_exec ("..\COLA\EXE\QUERY\GSCO_BOL", this, "__Temp__")
    if used("__Temp__")
      * --- Preleva periodo assoluto
      select __Temp__
      go top
      this.oParentObject.w_OLTPERAS = NVL(__Temp__.TPPERASS, "XXX")
      * --- Chiude cursore
      USE IN __Temp__
    endif
    this.oParentObject.w_OLTPERAS = IIF(Empty(this.oParentObject.w_OLTPERAS), "XXX" , this.oParentObject.w_OLTPERAS)
    * --- Se e' stata variata la data di fine e hai chiesto il ricalcolo, determina nuove date
    if this.TmpL and (this.Operazione = "DATAFINVAR" or this.Operazione = "DATACONVAR")
      * --- Ricalcola data di inizio (MPS)
      this.oParentObject.w_OLTDTMPS = COCALCLT(this.oParentObject.w_OLTDINRIC, this.oParentObject.w_OLTEMLAV, "I", .F., "")
      * --- Per PRODUZIONE ricalcola Lead-TIME e Data inizio prevista
      this.w_UM1 = this.oParentObject.w_UNMIS1
      this.w_UM2 = this.oParentObject.w_UNMIS2
      this.w_UM3 = this.oParentObject.w_UNMIS3
      this.w_OP = this.oParentObject.w_OPERAT
      this.w_MOLT = this.oParentObject.w_MOLTIP
      this.w_OP3 = this.oParentObject.w_OPERA3
      this.w_MOLT3 = this.oParentObject.w_MOLTI3
      this.oParentObject.w_OLTQTOD1 = CALQTA(this.oParentObject.w_OLTQTODL, this.oParentObject.w_OLTUNMIS,this.w_UM2, this.w_OP, this.w_MOLT, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2,, this.w_UM3, this.w_OP3, this.w_MOLT3)
      * --- Ricalcola LT <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      this.oParentObject.w_OLTEMLAV = COSETLT(this.oParentObject.w_OLTCODIC, this.oParentObject.w_OLTPROVE, this.oParentObject.w_OLTQTODL, this.oParentObject.w_OLTQTOD1, this.oParentObject.w_OLTCOFOR+this.oParentObject.w_OLTCONTR, this.oParentObject.w_OLTDTRIC, this.oParentObject.w_OLTUNMIS)
      * --- Ricalcola data di inizio (Produzione)
      this.oParentObject.w_OLTDINRIC = COCALCLT(this.oParentObject.w_OLTDTRIC, this.oParentObject.w_OLTEMLAV, "I", .F., "")
    endif
    * --- Aggiorna Lista Materiali ?
    if this.oParentObject.w_OLTSTATO $ "MPL"
      * --- Aggiorna data su Cursore ...
      SELECT (nc)
      this.GSCO_MOL.MarkPos()     
      this.GSCO_MOL.FirstRow()     
      do while Not this.GSCO_MOL.Eof_Trs()
        this.GSCO_MOL.SetRow()     
        if this.GSCO_MOL.FullRow()
          * --- Nuova data richiesta materiali
          this.GSCO_MOL.w_OLDATRIC = this.oParentObject.w_OLTDINRIC
          this.w_COCORLEA = nvl( &nc..t_OLCORRLT ,0)
          * --- Ricalcola data di inizio (Produzione)
          this.GSCO_MOL.w_OLDATRIC = COCALCLT(this.oParentObject.w_OLTDinRIC,Ceiling(this.w_COCORLEA),"C", .F. ,"")
          * --- Aggiorna Cursore
          this.GSCO_MOL.SaveRow()     
          this.GSCO_MOL.bUpdated = True
        endif
        this.GSCO_MOL.NextRow()     
      enddo
      this.GSCO_MOL.RePos(.F.)     
    endif
    * --- Rinfresca padre
    this.GSCO_AOP.refresh()     
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP )
    if g_PRFA="S" and g_CICLILAV="S"
      tc = this.GSCO_MCL.cTrsName
      this.GSCO_MCL.MarkPos()     
      SELECT (tc)
      delete ALL
      if not empty(this.oParentObject.w_OLTSECIC)
        * --- Carica temporaneo
        this.w_WIPFASEPREC = SPACE(20)
        this.w_WIPFASEOLD = SPACE(20)
        * --- Select from GSCO_BOL1
        do vq_exec with 'GSCO_BOL1',this,'_Curs_GSCO_BOL1','',.f.,.t.
        if used('_Curs_GSCO_BOL1')
          select _Curs_GSCO_BOL1
          locate for 1=1
          do while not(eof())
          * --- Nuova Riga del Temporaneo
          this.GSCO_MCL.AddRow()     
          * --- Devo utilizzare la Set altrimenti non viene sovrascritto correttamente il CPROWNUM
          this.GSCO_MCL.Set("CPROWNUM", nvl( _Curs_GSCO_BOL1.CPROWNUM , 0))     
          this.GSCO_MCL.w_CLROWNUM = nvl( _Curs_GSCO_BOL1.CPROWNUM , 0)
          this.GSCO_MCL.w_CLROWORD = nvl( _Curs_GSCO_BOL1.CPROWORD , 0)
          this.GSCO_MCL.w_CLINDPRE = nvl( _Curs_GSCO_BOL1.CLINDFAS , "  ")
          this.GSCO_MCL.w_CLDESFAS = nvl( _Curs_GSCO_BOL1.CLFASDES , space(40))
          this.GSCO_MCL.w_UM1 = this.oParentObject.w_OLTUNMIS
          this.GSCO_MCL.w_CLQTAPRE = this.oParentObject.w_OLTQTODL
          this.GSCO_MCL.w_CLPREUM1 = this.oParentObject.w_OLTQTOD1
          this.GSCO_MCL.w_CLQTADIC = this.oParentObject.w_OLTQTODL
          this.GSCO_MCL.w_CLDICUM1 = this.oParentObject.w_OLTQTOD1
          this.GSCO_MCL.w_CLQTAAVA = 0
          this.GSCO_MCL.w_CLAVAUM1 = 0
          this.GSCO_MCL.w_CLFASEVA = "N"
          this.GSCO_MCL.w_CLFASSEL = iif( this.GSCO_MCL.w_CLINDPRE ="00","S","N")
          this.GSCO_MCL.w_CLPRIFAS = nvl( _Curs_GSCO_BOL1.CLPRIFAS , "N")
          this.GSCO_MCL.w_CLULTFAS = nvl( _Curs_GSCO_BOL1.CLULTFAS , "N")
          this.GSCO_MCL.w_CLCOUPOI = nvl( _Curs_GSCO_BOL1.CLCOUPOI , "N")
          this.GSCO_MCL.w_CLCOUPOI = iif( this.GSCO_MCL.w_CLULTFAS = "S" or nvl(_Curs_GSCO_BOL1.CLFASOUT,"N")="S", "S", this.GSCO_MCL.w_CLCOUPOI)
          this.GSCO_MCL.w_CLFASCOS = iif( this.GSCO_MCL.w_CLCOUPOI = "S", "S", _Curs_GSCO_BOL1.CLFASCOS)
          this.GSCO_MCL.w_CLCPRIFE = nvl( _Curs_GSCO_BOL1.CLCPRIFE , 0)
          this.GSCO_MCL.w_CL__FASE = nvl( _Curs_GSCO_BOL1.CL__FASE , "     ")
          this.GSCO_MCL.w_CLCODFAS = nvl( _Curs_GSCO_BOL1.CLARTFAS , SPACE(20))
          this.GSCO_MCL.w_CLBFRIFE = nvl( _Curs_GSCO_BOL1.CLBFRIFE , 0)
          this.GSCO_MCL.w_CLFASOUT = nvl( _Curs_GSCO_BOL1.CLFASOUT , "N" )
          this.GSCO_MCL.w_FLCOUPOI = this.GSCO_MCL.w_CLCOUPOI
          this.GSCO_MCL.w_FLFASOUT = this.GSCO_MCL.w_CLFASOUT
          this.GSCO_MCL.w_FLPRIFAS = this.GSCO_MCL.w_CLPRIFAS
          this.GSCO_MCL.w_FLULTFAS = this.GSCO_MCL.w_CLULTFAS
          this.GSCO_MCL.w_FLFASSEL = this.GSCO_MCL.w_CLFASSEL
          if this.GSCO_MCL.w_CLFASOUT="S"
            * --- Articolo-WIP Output Fase
            this.w_CLWIPOUT = this.GSCO_MCL.w_CLCODFAS
            * --- Articolo-WIP Input Fase
            this.w_CLWIPFPR = nvl(this.w_WIPFASEPREC, SPACE(20))
            this.w_WIPFASEPREC = this.GSCO_MCL.w_CLCODFAS
          else
            this.w_CLWIPOUT = SPACE(20)
            this.w_CLWIPFPR = SPACE(20)
          endif
          this.TmpC = this.GSCO_MCL.w_CLWIPOUT
          if not empty(this.TmpC)
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADESART"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.TmpC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADESART;
                from (i_cTable) where;
                    CACODICE = this.TmpC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.TmpC = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.GSCO_MCL.w_DESWIPO = this.TmpC
          endif
          * --- Articolo-WIP Input Fase
          this.GSCO_MCL.w_CLWIPFPR = this.w_CLWIPFPR
          * --- Articolo-WIP Output Fase
          this.GSCO_MCL.w_CLWIPOUT = this.w_CLWIPOUT
          this.TmpC = this.GSCO_MCL.w_CLWIPFPR
          if not empty(this.TmpC)
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADESART"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.TmpC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADESART;
                from (i_cTable) where;
                    CACODICE = this.TmpC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.TmpC = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.GSCO_MCL.w_DESWIPI = this.TmpC
          endif
          this.GSCO_MCL.w_CLLTFASE = NVL( _Curs_GSCO_BOL1.CLLTFASE , 0 )
          this.GSCO_MCL.w_CLMAGWIP = NVL( _Curs_GSCO_BOL1.CLMAGWIP , SPACE(5) )
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.GSCO_MCL.SaveRow()     
          this.w_LRROWNUM = _Curs_GSCO_BOL1.CPROWNUM
          if this.oParentObject.w_OLTSTATO $ "M-P"
            * --- Try
            local bErr_050FF0F8
            bErr_050FF0F8=bTrsErr
            this.Try_050FF0F8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_050FF0F8
            * --- End
          endif
            select _Curs_GSCO_BOL1
            continue
          enddo
          use
        endif
      else
        * --- Nuova Riga del Temporaneo
        this.GSCO_MCL.AddRow()     
      endif
      this.GSCO_MCL.RePos()     
    else
      ts = this.GSCO_MCS.cTrsName
      this.GSCO_MCS.MarkPos()     
      SELECT (ts)
      delete ALL
      * --- Nuova Riga del Temporaneo
      if !Empty(this.oParentObject.w_OLTCICLO)
        * --- Select from TAB_CICL
        i_nConn=i_TableProp[this.TAB_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2],.t.,this.TAB_CICL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_CICL ";
              +" where CSCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCICLO)+"";
              +" order by CPROWORD";
               ,"_Curs_TAB_CICL")
        else
          select * from (i_cTable);
           where CSCODICE = this.oParentObject.w_OLTCICLO;
           order by CPROWORD;
            into cursor _Curs_TAB_CICL
        endif
        if used('_Curs_TAB_CICL')
          select _Curs_TAB_CICL
          locate for 1=1
          do while not(eof())
          this.GSCO_MCS.AddRow()     
          this.GSCO_MCS.w_CPROWNUM = _Curs_TAB_CICL.CPROWNUM
          this.GSCO_MCS.w_SMDESFAS = NVL(_Curs_TAB_CICL.CSDESFAS, SPACE(40))
          this.GSCO_MCS.w_SM_SETUP = NVL(_Curs_TAB_CICL.CS_SETUP, SPACE(1))
          * --- Read from TAB_RISO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TAB_RISO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2],.t.,this.TAB_RISO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP"+;
              " from "+i_cTable+" TAB_RISO where ";
                  +"RICODICE = "+cp_ToStrODBC(NVL(_Curs_TAB_CICL.CSCODRIS, SPACE(15)));
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
              from (i_cTable) where;
                  RICODICE = NVL(_Curs_TAB_CICL.CSCODRIS, SPACE(15));
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.GSCO_MCS.w_SMCODRIS = NVL(cp_ToDate(_read_.RICODICE),cp_NullValue(_read_.RICODICE))
            this.GSCO_MCS.w_DESRIS = NVL(cp_ToDate(_read_.RIDESCRI),cp_NullValue(_read_.RIDESCRI))
            this.GSCO_MCS.w_UNMIS1 = NVL(cp_ToDate(_read_.RIUNMIS1),cp_NullValue(_read_.RIUNMIS1))
            this.GSCO_MCS.w_UNMIS2 = NVL(cp_ToDate(_read_.RIUNMIS2),cp_NullValue(_read_.RIUNMIS2))
            this.GSCO_MCS.w_OPERAT = NVL(cp_ToDate(_read_.RIOPERAT),cp_NullValue(_read_.RIOPERAT))
            this.GSCO_MCS.w_MOLTIP = NVL(cp_ToDate(_read_.RIMOLTIP),cp_NullValue(_read_.RIMOLTIP))
            this.GSCO_MCS.w_PREZZO = NVL(cp_ToDate(_read_.RIPREZZO),cp_NullValue(_read_.RIPREZZO))
            this.GSCO_MCS.w_CODVAL = NVL(cp_ToDate(_read_.RICODVAL),cp_NullValue(_read_.RICODVAL))
            this.GSCO_MCS.w_CODREP = NVL(cp_ToDate(_read_.RICODREP),cp_NullValue(_read_.RICODREP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.GSCO_MCS.mcalc(.t.)     
          this.GSCO_MCS.SaveDependsOn()     
          this.GSCO_MCS.SetControlsValue()     
          this.GSCO_MCS.w_SMUNIMIS = NVL(_Curs_TAB_CICL.CSUNIMIS, SPACE(3))
          this.GSCO_MCS.w_SMQTAMOV = NVL(_Curs_TAB_CICL.CSQTAMOV, 0)
          this.GSCO_MCS.mcalc(.t.)     
          this.GSCO_MCS.SaveDependsOn()     
          this.GSCO_MCS.SetControlsValue()     
          this.GSCO_MCS.SaveRow()     
            select _Curs_TAB_CICL
            continue
          enddo
          use
        endif
      else
        * --- Aggiungo una riga altrimenti non posso pi� modificare il ciclo
        this.GSCO_MCS.InitRow()     
      endif
      * --- Rinfrescamenti vari
      this.GSCO_MCS.RePos()     
    endif
  endproc
  proc Try_050FF0F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Leggo le note della fase
    this.GSCO_MRL.w_RLISTLAV = NVL( _Curs_GSCO_BOL1.LRISTLAV , " " )
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.GSCO_MRL.SaveRow()     
    this.GSCO_MRL.MarkPos()     
    this.ROWORD = 0
    SELECT (rc)
    delete ALL
    this.GSCO_MRL.InitRow()     
    * --- Select from GSCO_BOL2
    do vq_exec with 'GSCO_BOL2',this,'_Curs_GSCO_BOL2','',.f.,.t.
    if used('_Curs_GSCO_BOL2')
      select _Curs_GSCO_BOL2
      locate for 1=1
      do while not(eof())
      * --- --Modifica transitorio
      this.GSCO_MRL.AddRow()     
      this.ROWORD = this.ROWORD+1
      this.w_COD_CLA = NVL(_Curs_GSCO_BOL2.LRCODCLA,space(5))
      * --- Tipologia risorsa
      this.w_LR__TIPO = _Curs_GSCO_BOL2.LR__TIPO
      * --- Risorsa
      this.w_COD_RISO = _Curs_GSCO_BOL2.LRCODRIS
      this.w_LRUMTRIS = _Curs_GSCO_BOL2.LRUMTRIS
      * --- --CALCOLO TEMPI 
      * --- Read from RIS_ORSE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "RLCODICE,RLDESCRI,RLINTEST,RLUMTDEF,RLQTARIS"+;
          " from "+i_cTable+" RIS_ORSE where ";
              +"RL__TIPO = "+cp_ToStrODBC(this.w_LR__TIPO);
              +" and RLCODICE = "+cp_ToStrODBC(this.w_COD_RISO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          RLCODICE,RLDESCRI,RLINTEST,RLUMTDEF,RLQTARIS;
          from (i_cTable) where;
              RL__TIPO = this.w_LR__TIPO;
              and RLCODICE = this.w_COD_RISO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COD_RISO = NVL(cp_ToDate(_read_.RLCODICE),cp_NullValue(_read_.RLCODICE))
        this.w_DES_RISO = NVL(cp_ToDate(_read_.RLDESCRI),cp_NullValue(_read_.RLDESCRI))
        this.w_RLINTEST = NVL(cp_ToDate(_read_.RLINTEST),cp_NullValue(_read_.RLINTEST))
        this.w_UMTDEF = NVL(cp_ToDate(_read_.RLUMTDEF),cp_NullValue(_read_.RLUMTDEF))
        this.w_QTARIS = NVL(cp_ToDate(_read_.RLQTARIS),cp_NullValue(_read_.RLQTARIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.GSCO_MRL.w_RL__TIPO = this.w_LR__TIPO
      this.GSCO_MRL.w_RLCODICE = this.w_COD_RISO
      this.GSCO_MRL.w_RLDESCRI = this.w_DES_RISO
      this.GSCO_MRL.w_RLINTEST = this.w_RLINTEST
      this.GSCO_MRL.w_RLTIPTEM = _Curs_GSCO_BOL2.LRTIPTEM
      this.GSCO_MRL.w_RLQTARIS = _Curs_GSCO_BOL2.LRQTARIS
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMDURSEC"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.w_LRUMTRIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMDURSEC;
          from (i_cTable) where;
              UMCODICE = this.w_LRUMTRIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_CONSEC = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_LRTEMRIS = _Curs_GSCO_BOL2.LRTEMRIS
      this.w_LRTEMSEC = this.w_LRTEMRIS*w_CONSEC
      if NOT EMPTY(this.w_COD_CLA)
        * --- Read from CLR_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CLR_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLR_MAST_idx,2],.t.,this.CLR_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CSDESCLA"+;
            " from "+i_cTable+" CLR_MAST where ";
                +"CSCODCLA = "+cp_ToStrODBC(this.w_COD_CLA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CSDESCLA;
            from (i_cTable) where;
                CSCODCLA = this.w_COD_CLA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_DES_CLA = NVL(cp_ToDate(_read_.CSDESCLA),cp_NullValue(_read_.CSDESCLA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_LRNUMIMP = IIF(NVL(_Curs_GSCO_BOL2.LRNUMIMP,0)=0, 1, _Curs_GSCO_BOL2.LRNUMIMP) 
      this.w_LRPROORA = iif(this.w_LRTEMSEC=0, 0, 3600/this.w_LRTEMSEC*IIF(this.w_LR__TIPO="AT", this.w_LRNUMIMP, 1))
      this.GSCO_MRL.w_CPROWORD = this.ROWORD*10
      this.GSCO_MRL.w_RLCODCLA = NVL(_Curs_GSCO_BOL2.LRCODCLA,space(5))
      this.GSCO_MRL.w_UMTDEF = this.w_UMTDEF
      this.GSCO_MRL.w_QTARIS = this.w_QTARIS
      this.GSCO_MRL.w_CONSEC = w_CONSEC
      this.GSCO_MRL.w_RLUMTRIS = this.w_LRUMTRIS
      this.GSCO_MRL.w_RLTEMRIS = this.w_LRTEMRIS
      this.GSCO_MRL.w_RLTEMSEC = this.w_LRTEMSEC
      this.GSCO_MRL.w_RLNUMIMP = IIF(NVL(_Curs_GSCO_BOL2.LRNUMIMP,0)=0, 1, _Curs_GSCO_BOL2.LRNUMIMP) 
      this.GSCO_MRL.w_RLTMPCIC = IIF(nvl(_Curs_GSCO_BOL2.LRTMPCIC, 0)=0, _Curs_GSCO_BOL2.LRLAVTEM, _Curs_GSCO_BOL2.LRTMPCIC)
      this.GSCO_MRL.w_RLTCONSS = 0
      this.GSCO_MRL.mCalc(.t.)     
      this.GSCO_MRL.SaveRow()     
        select _Curs_GSCO_BOL2
        continue
      enddo
      use
    endif
    this.GSCO_MRL.RePos()     
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
      * --- Seleziona ciclo di Lavorazione
      this.Page_12()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
        this.w_MFEXT = "N"
        this.oParentObject.w_OLTPROVE = "I"
        * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno, da trattare come salto codice
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Aggiorna testata ODL
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ricarica lista materiali e lavorazioni
      if this.oParentObject.o_OLTSECIC <> this.oParentObject.w_OLTSECIC and this.oParentObject.w_OLTSTATO $ "MPL"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- La variabile della anagrafica viene valorizzata a falso in modo da non poter pi� editare alcuni campi
      * --- perch� quando viene fatto l'aggiornamento dei dati CP perde il cpcchk e quindi fallisce il salvataggio dei dati
      this.oParentObject.w_CHKAGG = .T.
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- AGGIORNA DETTAGLIO ODL
    if this.oParentObject.w_OLTPROVE="E"
      SELECT (nc)
      delete All
      this.GSCO_MOL.bUpdated = .T.
      this.GSCO_MOL.bHeaderUpdated = .T.
      this.GSCO_MOL.InitRow()     
    endif
    if this.oParentObject.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR) AND this.Operazione = "CHANGEFOR"
      * --- Legge Eventuali Materiali del Terzista
      vq_exec ("..\COLA\EXE\QUERY\GSCO_QMT", this, "_MatTer_")
    endif
    * --- Causale Impegno
    this.w_FLO = " "
    this.w_FLI = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLORDI,CMFLIMPE"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUIMP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLORDI,CMFLIMPE;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_CAUIMP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLO = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_FLI = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_AGGMAT = .F.
    * --- Aggiorna Qta Cursore ...
    this.GSCO_MOL.MarkPos()     
    this.GSCO_MOL.FirstRow()     
    do while Not this.GSCO_MOL.Eof_Trs()
      this.GSCO_MOL.SetRow()     
      if this.GSCO_MOL.FullRow()
        if this.Operazione = "CHANGECOM"
          this.w_ARTBASE = this.GSCO_MOL.w_OLCODART
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARMAGPRE,ARTIPPRE,ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_ARTBASE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARMAGPRE,ARTIPPRE,ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_ARTBASE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_OP = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_MOLT = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            this.w_MAG = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
            this.w_TP = NVL(cp_ToDate(_read_.ARTIPPRE),cp_NullValue(_read_.ARTIPPRE))
            this.w_FLCOMM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.GSCO_MOL.w_FLCOMM = this.w_FLCOMM
          this.w_COMMAPPO = NVL(this.oParentObject.w_OLTCOMME,space(15))
          this.GSCO_MOL.w_OLCODCOM = iif(NVL(this.w_FLCOMM,"N")="S" and not empty(this.w_COMMAPPO),this.w_COMMAPPO,space(15))
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_OLTCOMME);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.oParentObject.w_OLTCOMME;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCOMM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.GSCO_MOL.w_DESCOM = NVL(this.w_DESCOMM,"")
        else
          this.GSCO_MOL.w_OLQTAMOV = this.GSCO_MOL.w_OLCOEIMP * this.oParentObject.w_OLTQTOD1
          this.GSCO_MOL.w_OLUNIMIS = this.GSCO_MOL.w_OLUNIMIS
          this.w_UM1 = padr( nvl( this.GSCO_MOL.w_UNMIS1, space(3)) ,3)
          if this.GSCO_MOL.w_OLUNIMIS <> this.w_UM1
            this.w_UM2 = padr( nvl( this.GSCO_MOL.w_UNMIS2, space(3)) ,3)
            this.w_UM3 = padr( nvl( this.GSCO_MOL.w_UNMIS3, space(3)) ,3)
            this.w_OP = this.GSCO_MOL.w_OPERAT
            this.w_MOLT = this.GSCO_MOL.w_MOLTIP
            this.w_OP3 = this.GSCO_MOL.w_OPERA3
            this.w_MOLT3 = this.GSCO_MOL.w_MOLTI3
            this.GSCO_MOL.w_OLQTAUM1 = CALQTA(this.GSCO_MOL.w_OLQTAMOV, this.GSCO_MOL.w_OLUNIMIS,this.w_UM2, this.w_OP, this.w_MOLT, this.GSCO_MOL.w_FLUSEP, "N", this.GSCO_MOL.w_MODUM2,, this.w_UM3, this.w_OP3, this.w_MOLT3)
          else
            this.GSCO_MOL.w_OLQTAUM1 = this.GSCO_MOL.w_OLQTAMOV
          endif
          if this.oParentObject.w_OLTPROVE="L" AND this.Operazione = "CHANGEFOR"
            * --- Azzero flag evasione
            this.GSCO_MOL.w_OLFLEVAS = "N"
            this.GSCO_MOL.w_OLEVAAUT = "N"
          else
            this.GSCO_MOL.w_OLFLEVAS = iif( this.GSCO_MOL.w_OLFLEVAS ="S", "S", " ")
            this.GSCO_MOL.w_OLEVAAUT = this.GSCO_MOL.w_OLEVAAUT
          endif
          this.GSCO_MOL.w_OLQTAEVA = iif(this.GSCO_MOL.w_OLFLEVAS="S", this.GSCO_MOL.w_OLQTAMOV, this.GSCO_MOL.w_OLQTAEVA)
          this.GSCO_MOL.w_OLQTAEV1 = iif(this.GSCO_MOL.w_OLFLEVAS="S", this.GSCO_MOL.w_OLQTAUM1, this.GSCO_MOL.w_OLQTAEV1)
          this.GSCO_MOL.w_OLQTASAL = IIF(this.GSCO_MOL.w_OLFLEVAS="S" OR this.GSCO_MOL.w_OLQTAEV1>this.GSCO_MOL.w_OLQTAUM1, 0, this.GSCO_MOL.w_OLQTAUM1-this.GSCO_MOL.w_OLQTAEV1)
          this.w_COCORLEA = NVL(this.GSCO_MOL.w_OLCORRLT, 0)
          this.GSCO_MOL.w_OLDATRIC = COCALCLT(this.oParentObject.w_OLTDINRIC,Ceiling(this.w_COCORLEA),"C", .F. ,"")
          this.GSCO_MOL.w_OLTIPPRE = IIF(EMPTY(this.GSCO_MOL.w_TIPPRE), "N", this.GSCO_MOL.w_TIPPRE)
          this.GSCO_MOL.w_OLCAUMAG = this.oParentObject.w_CAUIMP
          this.GSCO_MOL.w_OLFLORDI = this.w_FLO
          this.GSCO_MOL.w_OLFLIMPE = this.w_FLI
          this.GSCO_MOL.w_OLMAGWIP = IIF(this.oParentObject.w_OLTPROVE="L", this.oParentObject.w_MAGTER, this.oParentObject.w_MAGWIP)
          if this.GSCO_MOL.w_OLQTAPRE<this.GSCO_MOL.w_OLQTAMOV and this.GSCO_MOL.w_OLFLPREV="S" and this.w_EVAMOD="S"
            this.GSCO_MOL.w_OLFLPREV = "N"
          endif
          if this.oParentObject.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR) AND this.Operazione = "CHANGEFOR" AND USED("_MatTer_")
            * --- Legge Eventuali Materiali del Terzista
            SELECT (nc)
            this.w_APPO = t_OLCODICE
            select _MatTer_
            go top
            LOCATE FOR NVL(MCCODICE, SPACE(20))=this.w_APPO
            if FOUND()
              * --- Se il componente e' presente nei materiali del Terzista non deve eseguire alcun Impegno ne Trasferimento
              this.GSCO_MOL.w_OLTIPPRE = "N"
              this.GSCO_MOL.w_OLCAUMAG = SPACE(5)
              this.GSCO_MOL.w_OLFLORDI = " "
              this.GSCO_MOL.w_OLFLIMPE = " "
              this.GSCO_MOL.w_OLEVAAUT = "S"
              this.GSCO_MOL.w_OLFLEVAS = "S"
              this.GSCO_MOL.w_OLQTAEVA = iif(this.GSCO_MOL.w_OLFLEVAS="S", this.GSCO_MOL.w_OLQTAMOV, 0)
              this.GSCO_MOL.w_OLQTAEV1 = iif(this.GSCO_MOL.w_OLFLEVAS="S", this.GSCO_MOL.w_OLQTAUM1, 0)
              this.GSCO_MOL.w_OLQTASAL = IIF(this.GSCO_MOL.w_OLFLEVAS="S" OR this.GSCO_MOL.w_OLQTAEV1>this.GSCO_MOL.w_OLQTAUM1, 0, this.GSCO_MOL.w_OLQTAUM1-this.GSCO_MOL.w_OLQTAEV1)
            endif
          endif
        endif
        this.w_AGGMAT = .T.
        * --- Aggiorna Cursore
        this.GSCO_MOL.SaveRow()     
      endif
      this.GSCO_MOL.NextRow()     
    enddo
    * --- Rinfrescamenti vari
    this.GSCO_MOL.RePos(.F.)     
    if g_PRFA="S" and g_CICLILAV="S"
      if !Empty(this.oParentObject.w_OLTSECIC) and this.oParentObject.w_ARTIPART<>"FS"
        * --- Aggiorna lista lavorazioni
        this.GSCO_MCL.MarkPos()     
        this.GSCO_MCL.FirstRow()     
        do while Not this.GSCO_MCL.Eof_Trs()
          this.GSCO_MCL.SetRow()     
          if this.GSCO_MCL.FullRow()
            this.GSCO_MCL.w_CLQTAPRE = this.GSCO_AOP.w_OLTQTODL
            this.GSCO_MCL.w_CLPREUM1 = this.GSCO_AOP.w_OLTQTOD1
            this.GSCO_MCL.w_CLQTADIC = this.GSCO_AOP.w_OLTQTODL
            this.GSCO_MCL.w_CLDICUM1 = this.GSCO_AOP.w_OLTQTOD1
            * --- Aggiorna lista Risorse della fase
            this.GSCO_MRL.MarkPos()     
            this.GSCO_MRL.FirstRow()     
            do while Not this.GSCO_MRL.Eof_Trs()
              this.GSCO_MRL.SetRow()     
              if this.GSCO_MRL.FullRow()
                this.GSCO_MRL.bUpdated = .t.
                this.GSCO_MRL.mCalc(.t.)     
                this.GSCO_MRL.SaveRow()     
              endif
              this.GSCO_MRL.NextRow()     
            enddo
            this.GSCO_MRL.RePos(.F.)     
          endif
          this.GSCO_MCL.SaveRow()     
          this.GSCO_MCL.NextRow()     
        enddo
        * --- Metto una LastRow per riportare il riferimento al cursore sull'ultima riga
        *     (Il ciclo WHILE � andato oltre l'ultima )
        this.GSCO_MCL.RePos(.F.)     
      else
        if !Empty(this.oParentObject.w_OLTSEODL) and this.oParentObject.w_ARTIPART="FS"
          * --- Aggiorna lista Risorse della fase
          this.GSCO_MRL.MarkPos()     
          this.GSCO_MRL.FirstRow()     
          do while Not this.GSCO_MRL.Eof_Trs()
            this.GSCO_MRL.SetRow()     
            if this.GSCO_MRL.FullRow()
              this.GSCO_MRL.bUpdated = .t.
              this.GSCO_MRL.mCalc(.t.)     
              this.GSCO_MRL.SaveRow()     
            endif
            this.GSCO_MRL.NextRow()     
          enddo
          this.GSCO_MRL.RePos(.F.)     
        endif
      endif
    else
      if this.oParentObject.w_OLTPROVE $ "I-L"
        Select(ts)
        COUNT TO this.w_NFASI for !Empty(&ts..t_SMCODRIS)
        if this.w_NFASI=0
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.GSCO_MCS.MarkPos()     
          this.GSCO_MCS.FirstRow()     
          do while Not this.GSCO_MCS.Eof_Trs()
            this.GSCO_MCS.SetRow()     
            if this.GSCO_MCS.FullRow() And this.GSCO_MCS.RowStatus() $ "A-U"
              this.GSCO_MCS.w_SMQTAMO1 = CALQTAADV(this.GSCO_MCS.w_SMQTAMOV,this.GSCO_MCS.w_SMUNIMIS,this.GSCO_MCS.w_UNMIS2,this.GSCO_MCS.w_OPERAT, this.GSCO_MCS.w_MOLTIP, ; 
 this.GSCO_MCS.w_FLUSEP, "N", this.GSCO_MCS.w_MODUM2, "", this.GSCO_MCS.w_UNMIS3, this.GSCO_MCS.w_OPERA3, this.GSCO_MCS.w_MOLTI3, g_PERPQD, ,this.GSCO_MCS , "SMQTAMOV")
              this.GSCO_MCS.SaveRow()     
            endif
            this.GSCO_MCS.NextRow()     
          enddo
          this.GSCO_MCS.RePos()     
          SELECT (ts) 
 GO TOP 
 With this.GSCO_MCS 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
        endif
      endif
    endif
    * --- Messaggio ...
    if this.w_AGGMAT=.T.
      ah_ErrorMsg("Aggiornata lista materiali stimati",,"")
    endif
    * --- Rinfresca padre
    this.GSCO_AOP.refresh()     
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna ordini di fase collegati
    * --- ODL con ciclo di provenienza internza - Se ha fasi esterne, deve aggiornare gli evenutali ordini di fase collegati
    if g_PRFA="S" and g_CICLILAV="S"
      if this.oParentObject.w_OLTSECIC <> this.oParentObject.w_ORIGCICL or this.oParentObject.w_DELOCLFA
        * --- Se cambiato ciclo, cancella tutti gli ordini di fase collegati
        GSCI_BEC(this,"ALL", this.oParentObject.w_OLCODODL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if !empty(this.oParentObject.w_OLTSECIC)
          * --- Ritempifica FASI odl padre
          GSCI_BTF(this,this.oParentObject.w_OLCODODL,this.oParentObject.w_OLTDINRIC,this.oParentObject.w_OLTDTRIC)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Anche se cambia la qta, il LT non cambia perch� legato al centro di lavoro (� fisso)
          ah_msg("Aggiornamento ordini di fase collegati in corso...")
          this.w_CAMBIODATA = ( this.oParentObject.w_OLTDTRIC<>this.oParentObject.w_ORIGDATA )
          this.w_CAMBIOQTA = ( this.oParentObject.w_OLTQTOD1<>this.oParentObject.w_ORIGQTA )
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
                +" where OLTSEODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+"";
                 ,"_Curs_ODL_MAST")
          else
            select * from (i_cTable);
             where OLTSEODL=this.oParentObject.w_OLCODODL;
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            * --- legge la data di inizio e fine dalla fase dell'odl al quale l'ocl fa riferimento
            * --- Read from ODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLDATINI,CLDATFIN"+;
                " from "+i_cTable+" ODL_CICL where ";
                    +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTSEODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTFAODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLDATINI,CLDATFIN;
                from (i_cTable) where;
                    CLCODODL = _Curs_ODL_MAST.OLTSEODL;
                    and CPROWNUM = _Curs_ODL_MAST.OLTFAODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              TmpD1 = NVL(cp_ToDate(_read_.CLDATINI),cp_NullValue(_read_.CLDATINI))
              TmpD2 = NVL(cp_ToDate(_read_.CLDATFIN),cp_NullValue(_read_.CLDATFIN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Aggiorna testata OCL
            this.TmpC = _Curs_ODL_MAST.OLTPERAS
            this.TmpN = iif(this.w_CAMBIOQTA, this.oParentObject.w_OLTQTODL, _Curs_ODL_MAST.OLTQTODL)
            this.TmpN1 = iif(this.w_CAMBIOQTA, this.oParentObject.w_OLTQTOD1, _Curs_ODL_MAST.OLTQTOD1)
            if this.w_CAMBIODATA
              * --- Determina periodo di appartenenza dell'OCL 
              this.w_DATRIF = TmpD2
              * --- Determina periodo di appartenenza dell'ODL/OCL
              vq_exec ("..\COLA\EXE\QUERY\GSCO_BOL", this, "__Temp__")
              if used("__Temp__")
                * --- Preleva periodo assoluto
                select __Temp__
                go top
                this.TmpC = nvl(__Temp__.TPPERASS, "XXX")
                * --- Chiude cursore
                USE IN __Temp__
              endif
            endif
            * --- Aggiorna
            this.w_TEMP5 = iif(_Curs_ODL_MAST.OLTSTATO="M","P",_Curs_ODL_MAST.OLTSTATO)
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTQTODL ="+cp_NullLink(cp_ToStrODBC(this.TmpN),'ODL_MAST','OLTQTODL');
              +",OLTQTOD1 ="+cp_NullLink(cp_ToStrODBC(this.TmpN1),'ODL_MAST','OLTQTOD1');
              +",OLTDTCON ="+cp_NullLink(cp_ToStrODBC(TmpD1),'ODL_MAST','OLTDTCON');
              +",OLTDINRIC ="+cp_NullLink(cp_ToStrODBC(TmpD1),'ODL_MAST','OLTDINRIC');
              +",OLTDTRIC ="+cp_NullLink(cp_ToStrODBC(TmpD2),'ODL_MAST','OLTDTRIC');
              +",OLTPERAS ="+cp_NullLink(cp_ToStrODBC(this.TmpC),'ODL_MAST','OLTPERAS');
              +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.TmpN1),'ODL_MAST','OLTQTSAL');
              +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_Temp5),'ODL_MAST','OLTSTATO');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
                     )
            else
              update (i_cTable) set;
                  OLTQTODL = this.TmpN;
                  ,OLTQTOD1 = this.TmpN1;
                  ,OLTDTCON = TmpD1;
                  ,OLTDINRIC = TmpD1;
                  ,OLTDTRIC = TmpD2;
                  ,OLTPERAS = this.TmpC;
                  ,OLTQTSAL = this.TmpN1;
                  ,OLTSTATO = this.w_Temp5;
                  &i_ccchkf. ;
               where;
                  OLCODODL = _Curs_ODL_MAST.OLCODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if this.w_CAMBIOQTA
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(_Curs_ODL_MAST.OLTFLORD,'SLQTOPER','this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1',this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(_Curs_ODL_MAST.OLTFLIMP,'SLQTIPER','this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1',this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = _Curs_ODL_MAST.OLTKEYSA;
                    and SLCODMAG = _Curs_ODL_MAST.OLTCOMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARFLCOMM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARFLCOMM;
                  from (i_cTable) where;
                      ARCODART = _Curs_ODL_MAST.OLTCOART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(_Curs_ODL_MAST.OLTCOMME," "))
                * --- Aggiorna i saldi commessa
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(_Curs_ODL_MAST.OLTFLORD,'SCQTOPER','this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1',this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(_Curs_ODL_MAST.OLTFLIMP,'SCQTIPER','this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1',this.oParentObject.w_OLTQTOD1-_Curs_ODL_MAST.OLTQTOD1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOMME);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = _Curs_ODL_MAST.OLTKEYSA;
                      and SCCODMAG = _Curs_ODL_MAST.OLTCOMAG;
                      and SCCODCAN = _Curs_ODL_MAST.OLTCOMME;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            * --- Aggiorna dettaglio ...
            this.w_NEWDATA = TmpD1
            * --- Nuova data di inizio lavorazione impostata su OCL
            this.w_NEWQTA = this.TmpN1
            * --- Nuova qta impostata su ordine di fase
            * --- Select from ODL_DETT
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
                  +" where OLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL)+"";
                   ,"_Curs_ODL_DETT")
            else
              select * from (i_cTable);
               where OLCODODL = _Curs_ODL_MAST.OLCODODL;
                into cursor _Curs_ODL_DETT
            endif
            if used('_Curs_ODL_DETT')
              select _Curs_ODL_DETT
              locate for 1=1
              do while not(eof())
              * --- Se cambiata qta padre, aggiorna qta figlio <<<<<<<<<<<<<<<<<
              if this.w_CAMBIOQTA
                this.TmpN = _Curs_ODL_DETT.OLCOEIMP * this.w_NEWQTA
                if _Curs_ODL_DETT.OLUNIMIS <> this.w_UM1
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
                      from (i_cTable) where;
                          ARCODART = _Curs_ODL_DETT.OLCODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                    this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
                    this.w_OP = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
                    this.w_MOLT = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  * --- Read from KEY_ARTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
                      " from "+i_cTable+" KEY_ARTI where ";
                          +"CACODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODICE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CAUNIMIS,CAOPERAT,CAMOLTIP;
                      from (i_cTable) where;
                          CACODICE = _Curs_ODL_DETT.OLCODICE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
                    this.w_OP3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
                    this.w_MOLT3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.TmpC = Padr(_Curs_ODL_DETT.OLUNIMIS ,3)
                  this.w_UM2 = padr( this.w_UM2, 3)
                  this.w_UM3 = padr( this.w_UM3, 3)
                  this.TmpN1 = CALQTA(this.TmpN, this.TmpC, this.w_UM2, this.w_OP, this.w_MOLT,"", "N", "",, this.w_UM3, this.w_OP3, this.w_MOLT3)
                else
                  this.TmpN1 = this.TmpN
                endif
              else
                this.TmpN = _Curs_ODL_DETT.OLQTAMOV
                this.TmpN1 = _Curs_ODL_DETT.OLQTAUM1
              endif
              * --- Data di richiesta del materiali <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
              if this.w_CAMBIODATA
                this.TmpD = this.w_NEWDATA
                * --- Parametro di correzione lead-time su legame
                this.w_COCORLEA = NVL( _Curs_ODL_DETT.OLCORRLT , 0)
                if this.w_COCORLEA <> 0
                  * --- Ricalcola data di inizio (Produzione)
                  this.GSCO_MOL.w_OLDATRIC = COCALCLT(this.oParentObject.w_OLTDinRIC,Ceiling(this.w_COCORLEA),"C",.F.,"")
                endif
              else
                this.TmpD = _Curs_ODL_DETT.OLDATRIC
              endif
              * --- Aggiorna dettaglio <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
              if _Curs_ODL_DETT.OLFLEVAS="S"
                this.TmpN2 = 0
                this.TmpN3 = this.TmpN
                this.TmpN4 = this.TmpN1
              else
                this.TmpN2 = this.TmpN1-_Curs_ODL_DETT.OLQTAEV1
                this.TmpN3 = _Curs_ODL_DETT.OLQTAEVA
                this.TmpN4 = _Curs_ODL_DETT.OLQTAEV1
              endif
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.TmpN),'ODL_DETT','OLQTAMOV');
                +",OLQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.TmpN1),'ODL_DETT','OLQTAUM1');
                +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.TmpN2),'ODL_DETT','OLQTASAL');
                +",OLDATRIC ="+cp_NullLink(cp_ToStrODBC(this.TmpD),'ODL_DETT','OLDATRIC');
                +",OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.TmpN3),'ODL_DETT','OLQTAEVA');
                +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.TmpN4),'ODL_DETT','OLQTAEV1');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    OLQTAMOV = this.TmpN;
                    ,OLQTAUM1 = this.TmpN1;
                    ,OLQTASAL = this.TmpN2;
                    ,OLDATRIC = this.TmpD;
                    ,OLQTAEVA = this.TmpN3;
                    ,OLQTAEV1 = this.TmpN4;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = _Curs_ODL_DETT.OLCODODL;
                    and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Aggiorna i saldi per differenza
              if this.w_CAMBIOQTA
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(_Curs_ODL_DETT.OLFLORDI,'SLQTOPER','this.TmpN2-_Curs_ODL_DETT.OLQTASAL',this.TmpN2-_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(_Curs_ODL_DETT.OLFLIMPE,'SLQTIPER','this.TmpN2-_Curs_ODL_DETT.OLQTASAL',this.TmpN2-_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(_Curs_ODL_DETT.OLFLRISE,'SLQTRPER','this.TmpN2-_Curs_ODL_DETT.OLQTASAL',this.TmpN2-_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = &i_cOp1.;
                      ,SLQTIPER = &i_cOp2.;
                      ,SLQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = _Curs_ODL_DETT.OLKEYSAL;
                      and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARFLCOMM"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARFLCOMM;
                    from (i_cTable) where;
                        ARCODART = _Curs_ODL_DETT.OLCODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(_Curs_ODL_MAST.OLTCOMME," "))
                  * --- Aggiorna i saldi commessa
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                    i_cOp1=cp_SetTrsOp(_Curs_ODL_DETT.OLFLORDI,'SCQTOPER','this.TmpN2-_Curs_ODL_DETT.OLQTASAL',this.TmpN2-_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(_Curs_ODL_DETT.OLFLIMPE,'SCQTIPER','this.TmpN2-_Curs_ODL_DETT.OLQTASAL',this.TmpN2-_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                    i_cOp3=cp_SetTrsOp(_Curs_ODL_DETT.OLFLRISE,'SCQTRPER','this.TmpN2-_Curs_ODL_DETT.OLQTASAL',this.TmpN2-_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                    +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                        +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                        +" and SCCODCAN = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOMME);
                           )
                  else
                    update (i_cTable) set;
                        SCQTOPER = &i_cOp1.;
                        ,SCQTIPER = &i_cOp2.;
                        ,SCQTRPER = &i_cOp3.;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                        and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                        and SCCODCAN = _Curs_ODL_MAST.OLTCOMME;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
                select _Curs_ODL_DETT
                continue
              enddo
              use
            endif
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
        endif
      endif
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce cancellazione ODL/OCL
    this.w_Continua = .T.
    do case
      case this.oParentObject.w_OLTSTATO = "M"
        if this.oParentObject.w_TIPGES="L"
          this.w_oMess.AddMsgPartNL("� richiesta la cancellazione di un ODL suggerito")     
        else
          this.w_oMess.AddMsgPartNL("� richiesta la cancellazione di un OCL suggerito")     
        endif
        do case
          case this.oParentObject.w_TIPGES $ "LZ"
            this.w_oMess.AddMsgPartNL("La cancellazione comporter� l'eliminazione degli impegni direttamente collegati%0con relativo storno dell'ordinato e impegnato%0Confermi richiesta di cancellazione?")     
            this.w_Continua = this.w_oMess.ah_YesNo()
        endcase
      case this.oParentObject.w_OLTSTATO = "P"
        if this.oParentObject.w_TIPGES="L"
          this.w_oMess.AddMsgPartNL("� richiesta la cancellazione di un ODL pianificato")     
        else
          this.w_oMess.AddMsgPartNL("� richiesta la cancellazione di un OCL pianificato")     
        endif
        do case
          case this.oParentObject.w_TIPGES $ "LZ"
            this.w_oMess.AddMsgPartNL("La cancellazione comporter� l'eliminazione degli impegni direttamente collegati%0con relativo storno dell'ordinato e impegnato%0Confermi richiesta di cancellazione?")     
            this.w_Continua = this.w_oMess.ah_YesNo()
        endcase
      case this.oParentObject.w_OLTSTATO = "L"
        do case
          case this.oParentObject.w_TIPGES="L"
            * --- ODL Lanciato, Puo' essere eliminato solo se non Associato ad alcun Buono do Prelievo o Dich. di Produzione
            this.w_APPO = SPACE(15)
            * --- Read from DIC_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIC_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DPSERIAL"+;
                " from "+i_cTable+" DIC_PROD where ";
                    +"DPCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DPSERIAL;
                from (i_cTable) where;
                    DPCODODL = this.oParentObject.w_OLCODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows>0 OR NOT EMPTY(this.w_APPO)
              this.TmpC = "Cancellazione non consentita%0ODL lanciato associato a dichiarazione di produzione (%1)"
              this.w_Continua = .F.
            else
              this.w_APPO = SPACE(15)
              * --- Read from RIF_GODL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.RIF_GODL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2],.t.,this.RIF_GODL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PDSERIAL"+;
                  " from "+i_cTable+" RIF_GODL where ";
                      +"PDCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                      +" and PDTIPGEN = "+cp_ToStrODBC("BP");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PDSERIAL;
                  from (i_cTable) where;
                      PDCODODL = this.oParentObject.w_OLCODODL;
                      and PDTIPGEN = "BP";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_APPO = NVL(cp_ToDate(_read_.PDSERIAL),cp_NullValue(_read_.PDSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_Rows>0 OR NOT EMPTY(this.w_APPO)
                this.TmpC = "Cancellazione non consentita%0ODL lanciato associato a buoni di prelievo (rif. piano n. %1)"
                this.w_Continua = .F.
              endif
            endif
            if this.w_Continua=.F.
              ah_ErrorMsg(this.TmpC,,"", ALLTRIM(this.w_APPO) )
            else
              this.w_oMess.AddMsgPartNL("� richiesta la cancellazione di un ODL lanciato")     
              this.w_oMess.AddMsgPartNL("La cancellazione comporter� l'eliminazione degli impegni direttamente collegati%0con relativo storno dell'ordinato e impegnato%0Confermi richiesta di cancellazione?")     
              this.w_Continua = this.w_oMess.ah_YesNo()
            endif
          otherwise
            this.TmpC = "Impossibile cancellare un %1"
            ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
            this.w_Continua = .F.
        endcase
      case this.oParentObject.w_OLTSTATO = "F"
        this.TmpC = "Impossibile cancellare un %1"
        ah_ErrorMsg(this.TmpC,48,"",COSTRODL("STRODL",this.oParentObject.w_TIPGES,this.oParentObject.w_OLTSTATO))
        this.w_Continua = .F.
    endcase
    if g_PRFA="S" and g_CICLILAV="S" and this.w_Continua
      this.w_STOP = FALSE
      * --- Select from ODL_MAST
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
            +" where OLTSEODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+" and ( OLTSTATO='L' or OLTSTATO='F' )";
             ,"_Curs_ODL_MAST")
      else
        select * from (i_cTable);
         where OLTSEODL=this.oParentObject.w_OLCODODL and ( OLTSTATO="L" or OLTSTATO="F" );
          into cursor _Curs_ODL_MAST
      endif
      if used('_Curs_ODL_MAST')
        select _Curs_ODL_MAST
        locate for 1=1
        do while not(eof())
        this.w_STOP = TRUE
        exit
          select _Curs_ODL_MAST
          continue
        enddo
        use
      endif
      if this.w_STOP
        this.w_oMess.addMsgPartNL("Attenzione:")     
        this.w_oMess.addMsgPartNL("Sono presenti ordini di conto lavoro (OCL) collegati alle fasi del ciclo corrente per i quali sono gi� stati generati ordini a fornitore")     
        this.w_oMess.addMsgPartNL("Impossibile cancellare l'ordine corrente")     
        this.w_oMess.ah_ErrorMsg()
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      else
        if !Empty(this.oParentObject.w_OLTSEODL) and this.oParentObject.w_OLTFAODL > 0
          GSCI_BEC(this,"REOPENMAT",this.oParentObject.w_OLCODODL,this.oParentObject.w_OLTSEODL,this.oParentObject.w_OLTFAODL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          GSCI_BEC(this,"ALL",this.oParentObject.w_OLCODODL,this.oParentObject.w_OLTSEODL,this.oParentObject.w_OLTFAODL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_OLDCOM
          * --- Delete from PEG_SELI
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PESERODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                   )
          else
            delete from (i_cTable) where;
                  PESERODL = this.oParentObject.w_OLCODODL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from PEG_SELI
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PERIFODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                   )
          else
            delete from (i_cTable) where;
                  PERIFODL = this.oParentObject.w_OLCODODL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
    endif
    if !this.w_Continua
      this.TmpC = ah_Msgformat("Cancellazione abortita")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.TmpC
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Pegging - Gestione Risorse delle fasi
    do case
      case this.Operazione = "PEGGING" AND this.w_OLDCOM
        * --- Crea pegging per ordini caricati manualmente
        if not empty(this.oParentObject.w_OLTCOMME)
          this.w_RESABB = this.oParentObject.w_OLTQTSAL
          if this.oParentObject.w_OGGMPS="N"
            * --- Crea pegging 2o livello
            * --- Try
            local bErr_05266160
            bErr_05266160=bTrsErr
            this.Try_05266160()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
            endif
            bTrsErr=bTrsErr or bErr_05266160
            * --- End
          endif
        endif
      case this.Operazione = "AggiornaRisorse" and g_PRFA="S" and g_CICLILAV="S" and !empty(this.oParentObject.w_OLTSECIC)
        * --- Aggiorna i tempi di lavorazione delle risorse
        * --- Try
        local bErr_0525E1E0
        bErr_0525E1E0=bTrsErr
        this.Try_0525E1E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=MSG_TRANSACTION_ERROR
        endif
        bTrsErr=bTrsErr or bErr_0525E1E0
        * --- End
    endcase
  endproc
  proc Try_05266160()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Progressivo
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERORD = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = VAL(nvl(this.w_PESERORD,"0"))
    * --- Select from gsdb7bol
    do vq_exec with 'gsdb7bol',this,'_Curs_gsdb7bol','',.f.,.t.
    if used('_Curs_gsdb7bol')
      select _Curs_gsdb7bol
      locate for 1=1
      do while not(eof())
      if this.w_RESABB>0
        this.w_PEQTAABB = min(this.w_RESABB,_Curs_gsdb7bol.MVQTASAL)
        if this.w_PEQTAABB>0
          this.w_RESABB = this.w_RESABB - this.w_PEQTAABB
          this.w_PROGR = max(this.w_PROGR + 1,1)
          this.w_PESERORD = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
          do case
            case _Curs_gsdb7bol.PETIPRIF="O"
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PETIPRIF"+",PERIFODL"+",PESERODL"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_gsdb7bol.PETIPRIF),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_gsdb7bol.OLCODODL),'PEG_SELI','PERIFODL');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLCODODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAABB),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLCODODL),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOMME),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCODIC),'PEG_SELI','PECODRIC');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERORD,'PETIPRIF',_Curs_gsdb7bol.PETIPRIF,'PERIFODL',_Curs_gsdb7bol.OLCODODL,'PESERODL',this.oParentObject.w_OLCODODL,'PEQTAABB',this.w_PEQTAABB,'PEODLORI',this.oParentObject.w_OLCODODL,'PECODCOM',this.oParentObject.w_OLTCOMME,'PECODRIC',this.oParentObject.w_OLTCODIC)
                insert into (i_cTable) (PESERIAL,PETIPRIF,PERIFODL,PESERODL,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                   values (;
                     this.w_PESERORD;
                     ,_Curs_gsdb7bol.PETIPRIF;
                     ,_Curs_gsdb7bol.OLCODODL;
                     ,this.oParentObject.w_OLCODODL;
                     ,this.w_PEQTAABB;
                     ,this.oParentObject.w_OLCODODL;
                     ,this.oParentObject.w_OLTCOMME;
                     ,this.oParentObject.w_OLTCODIC;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            case _Curs_gsdb7bol.PETIPRIF="D"
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PESERODL"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_gsdb7bol.PETIPRIF),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_gsdb7bol.MVSERIAL),'PEG_SELI','PESERORD');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_gsdb7bol.CPROWNUM),'PEG_SELI','PERIGORD');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLCODODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAABB),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLCODODL),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOMME),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCODIC),'PEG_SELI','PECODRIC');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERORD,'PETIPRIF',_Curs_gsdb7bol.PETIPRIF,'PESERORD',_Curs_gsdb7bol.MVSERIAL,'PERIGORD',_Curs_gsdb7bol.CPROWNUM,'PESERODL',this.oParentObject.w_OLCODODL,'PEQTAABB',this.w_PEQTAABB,'PEODLORI',this.oParentObject.w_OLCODODL,'PECODCOM',this.oParentObject.w_OLTCOMME,'PECODRIC',this.oParentObject.w_OLTCODIC)
                insert into (i_cTable) (PESERIAL,PETIPRIF,PESERORD,PERIGORD,PESERODL,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                   values (;
                     this.w_PESERORD;
                     ,_Curs_gsdb7bol.PETIPRIF;
                     ,_Curs_gsdb7bol.MVSERIAL;
                     ,_Curs_gsdb7bol.CPROWNUM;
                     ,this.oParentObject.w_OLCODODL;
                     ,this.w_PEQTAABB;
                     ,this.oParentObject.w_OLCODODL;
                     ,this.oParentObject.w_OLTCOMME;
                     ,this.oParentObject.w_OLTCODIC;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
          endcase
        endif
      endif
        select _Curs_gsdb7bol
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0525E1E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_DELOCLFA
      * --- Con w_DELOCLFA=.t. elimina tutti gli ordine di fase
      GSCI_BEC(this,"ALL",this.oParentObject.w_OLCODODL)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !this.oParentObject.w_DELOCLFA and this.oParentObject.w_bOclfadadel
      * --- Con w_DELOCLFA=.f. e w_bOclfadadel=.t. elimina solo gli OCL riferiti a fasi di ordine modificate
      L_Cur_Del_Ocl = this.oParentObject.w_Cur_Del_Ocl
      SELECT DISTINCT CLCODODL FROM ( L_Cur_Del_Ocl ) INTO CURSOR "Curs_OCL_da_del"
      if used( "Curs_OCL_da_del" )
        Select( "Curs_OCL_da_del" )
        GO TOP
        SCAN
        this.w_CODODLFA = NVL( Curs_OCL_da_del.CLCODODL, SPACE(15))
        GSCI_BEC(this,"SINGLEBYORD",this.w_CODODLFA)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Select( "Curs_OCL_da_del" )
        ENDSCAN
        use in select("Curs_OCL_da_del")
      endif
    endif
    if Not Empty(this.oParentObject.w_OLTSECIC) and inlist(this.w_cFunction , "LOAD", "EDIT") and this.GSCO_AOP.w_OLTSTATO $ "M-P-L" and this.GSCO_MCL.bupdated
      * --- Query utilizzatata dalla procedura di lancio ODL
      *     Per compatibilit� assegno alla variabile w_CODODL il seriale dell'odl w_CLCODODL
      this.w_CODODL = this.oParentObject.w_OLCODODL
      * --- Select from GSCI5BOL
      do vq_exec with 'GSCI5BOL',this,'_Curs_GSCI5BOL','',.f.,.t.
      if used('_Curs_GSCI5BOL')
        select _Curs_GSCI5BOL
        locate for 1=1
        do while not(eof())
        if this.w_PRIMOGIRO
          this.w_PRIMOGIRO = .F.
        else
          this.w_WIPFASPRE = this.w_WIPFASOLD
        endif
        this.w_WIPFASOLD = nvl(_Curs_GSCI5BOL.RLWIPRIS, " ")
        * --- Aggiorna ,,,
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLWIPDES ="+cp_NullLink(cp_ToStrODBC(this.w_WIPFASPRE),'ODL_CICL','CLWIPDES');
          +",CLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_WIPFASOLD),'ODL_CICL','CLMAGWIP');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(_Curs_GSCI5BOL.CLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_GSCI5BOL.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CLWIPDES = this.w_WIPFASPRE;
              ,CLMAGWIP = this.w_WIPFASOLD;
              &i_ccchkf. ;
           where;
              CLCODODL = _Curs_GSCI5BOL.CLCODODL;
              and CPROWNUM = _Curs_GSCI5BOL.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Verifica se la prima fase � interna
        this.w_PRIMAFASINT = ( nvl(_Curs_GSCI5BOL.RLINTEST,"I")="I" )
        * --- Mi dice se la prima fase del ciclo � interna
        * --- Memorizza numero fase interna e cprownum
        if nvl(_Curs_GSCI5BOL.RLINTEST,"I")="I"
          this.w_CPPRIMAFASINT = nvl(_Curs_GSCI5BOL.CPROWNUM, 0)
          * --- Alla fine del ciclo, mi dice il numero della prima fase interna
          this.w_FAPRIMAFASINT = nvl(_Curs_GSCI5BOL.CLROWORD,0)
          this.w_WPRIMAFASINT = nvl(_Curs_GSCI5BOL.RLWIPRIS, " ")
        endif
        * --- Se necessario memorizza il numero della fase esterna precedente alla prima fase interna
        if nvl(_Curs_GSCI5BOL.RLINTEST,"I")="E" and this.w_OINTEXT="I"
          this.w_CPFASEEXT = nvl(_Curs_GSCI5BOL.CPROWNUM, 0)
          this.w_FAFASEEXT = nvl(_Curs_GSCI5BOL.CLROWORD,0)
          this.w_WIPFASEEXT = nvl(_Curs_GSCI5BOL.RLWIPRIS, " ")
        endif
        this.w_OINTEXT = nvl(_Curs_GSCI5BOL.RLINTEST,"I")
          select _Curs_GSCI5BOL
          continue
        enddo
        use
      endif
    endif
    this.w_MAXFASE = 0
    this.w_TMPCODODL = SPACE(15)
    this.w_TMPCPR = 0
    this.w_TMPCPRIFE = 0
    this.w_BCHKFASE = .F.
    this.w_CLCODODL = this.oParentObject.w_OLCODODL
    if g_PRFA="S" and g_CICLILAV="S" and not Empty(this.oParentObject.w_OLTSECIC) and inlist(this.w_cFunction , "LOAD", "EDIT") and this.GSCO_AOP.w_OLTSTATO $ "M-P-L"
      * --- Legge il WIP del Centro di Lavoro
      this.w_OLCODMAG = space(5)
      * --- Select from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CLROWORD, CPROWNUM, CLFASCLA, CLCPRIFE, CLMAGWIP  from "+i_cTable+" ODL_CICL ";
            +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND CLFASSEL='S' AND CLFASOUT='S'";
            +" order by CLROWORD DESC";
             ,"_Curs_ODL_CICL")
      else
        select CLROWORD, CPROWNUM, CLFASCLA, CLCPRIFE, CLMAGWIP from (i_cTable);
         where CLCODODL=this.w_CLCODODL AND CLFASSEL="S" AND CLFASOUT="S";
         order by CLROWORD DESC;
          into cursor _Curs_ODL_CICL
      endif
      if used('_Curs_ODL_CICL')
        select _Curs_ODL_CICL
        locate for 1=1
        do while not(eof())
        this.w_TMPCPR = _Curs_ODL_CICL.CPROWNUM
        this.w_MAXFASE = _Curs_ODL_CICL.CLROWORD
        this.w_TMPCPRIFE = _Curs_ODL_CICL.CLCPRIFE
        this.w_CLFASCLA = NVL(_Curs_ODL_CICL.CLFASCLA, "N")
        this.w_OLCODMAG = NVL(_Curs_ODL_CICL.CLMAGWIP, SPACE(5))
        * --- Flag che indica se devo associare il materiale di input alla prima oppure all'ultima fase di output
        if this.GSCO_AOP.w_PPMATINP <> "P"
          exit
        endif
          select _Curs_ODL_CICL
          continue
        enddo
        use
      endif
      if .F.
        * --- Select from ODL_RISO
        i_nConn=i_TableProp[this.ODL_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2],.t.,this.ODL_RISO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select RLCODICE  from "+i_cTable+" ODL_RISO ";
              +" where RLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND RL__TIPO='CL' AND RLROWNUM="+cp_ToStrODBC(this.w_TMPCPR)+"";
               ,"_Curs_ODL_RISO")
        else
          select RLCODICE from (i_cTable);
           where RLCODODL=this.w_CLCODODL AND RL__TIPO="CL" AND RLROWNUM=this.w_TMPCPR;
            into cursor _Curs_ODL_RISO
        endif
        if used('_Curs_ODL_RISO')
          select _Curs_ODL_RISO
          locate for 1=1
          do while not(eof())
          * --- Read from RIS_ORSE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RLMAGWIP,RL__TIPO"+;
              " from "+i_cTable+" RIS_ORSE where ";
                  +"RLCODICE = "+cp_ToStrODBC(_Curs_ODL_RISO.RLCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RLMAGWIP,RL__TIPO;
              from (i_cTable) where;
                  RLCODICE = _Curs_ODL_RISO.RLCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RLMAGWIP = NVL(cp_ToDate(_read_.RLMAGWIP),cp_NullValue(_read_.RLMAGWIP))
            this.w_RL__TIPO = NVL(cp_ToDate(_read_.RL__TIPO),cp_NullValue(_read_.RL__TIPO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OLCODMAG = this.w_RLMAGWIP
            select _Curs_ODL_RISO
            continue
          enddo
          use
        endif
      endif
      * --- Verifico se � gi� stato generato l'ordine di fase
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLCODODL"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLTSEODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and OLTFAODL = "+cp_ToStrODBC(this.w_TMPCPR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLCODODL;
          from (i_cTable) where;
              OLTSEODL = this.w_CLCODODL;
              and OLTFAODL = this.w_TMPCPR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TMPCODODL = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_BCHKFASE = .F.
      * --- Select from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CLROWORD, CPROWNUM, CLQTAAVA  from "+i_cTable+" ODL_CICL ";
            +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND CLROWORD="+cp_ToStrODBC(this.w_TMPCPRIFE)+" AND CLFASSEL='S' AND CLCOUPOI = 'S'";
            +" order by CLROWORD DESC";
             ,"_Curs_ODL_CICL")
      else
        select CLROWORD, CPROWNUM, CLQTAAVA from (i_cTable);
         where CLCODODL=this.w_CLCODODL AND CLROWORD=this.w_TMPCPRIFE AND CLFASSEL="S" AND CLCOUPOI = "S";
         order by CLROWORD DESC;
          into cursor _Curs_ODL_CICL
      endif
      if used('_Curs_ODL_CICL')
        select _Curs_ODL_CICL
        locate for 1=1
        do while not(eof())
        this.w_BCHKFASE = NVL(_Curs_ODL_CICL.CLQTAAVA,0) = 0
          select _Curs_ODL_CICL
          continue
        enddo
        use
      endif
      * --- Ho passato il test aggiorno la fase di riferimento
      GSCI_BML(this.GSCO_MOL, "ALLFASI")
      * --- Se ho dei materiali che non sono associati a nessuna fase, verifico nei parametri di produzine
      *     se devo associarli alla prima oppure all'ultima dopo si che procedo se possibile ad associarli automaticamente alla fase
      * --- Select from GSCI10BTC
      do vq_exec with 'GSCI10BTC',this,'_Curs_GSCI10BTC','',.f.,.t.
      if used('_Curs_GSCI10BTC')
        select _Curs_GSCI10BTC
        locate for 1=1
        do while not(eof())
        if !Empty(this.w_TMPCODODL) OR !this.w_BCHKFASE
          if !Empty(this.w_TMPCODODL)
            this.w_MSG = ah_msgFormat("Impossibile associare la fase %1 ai materiali senza fasi di riferimento%0La fase ha generato un ordine di fase" , alltrim(str(this.w_MAXFASE)) )
          else
            if this.w_BCHKFASE > 0
              this.w_MSG = ah_msgFormat("Impossibile associare la fase %1 ai materiali senza fasi di riferimento%0La fase di riferimento � gi� stata dichiarata" , alltrim(str(this.w_MAXFASE)) )
            endif
          endif
          ah_ErrorMsg(this.w_MSG,16,"")
          this.w_bTrsDontDisplayErr = this.GSCO_AOP.bTrsDontDisplayErr
          this.GSCO_AOP.bTrsDontDisplayErr = .T.
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MSG
          i_Error=i_TrsMsg
          return
          exit
        endif
        this.w_CPROWNUM = _Curs_GSCI10BTC.CPROWNUM
        this.w_ARMAGPRE = NVL(_Curs_GSCI10BTC.ARMAGPRE, SPACE(5))
        this.w_ARMAGIMP = NVL(_Curs_GSCI10BTC.ARMAGIMP, SPACE(5))
        this.w_OLMODPRE = NVL(_Curs_GSCI10BTC.OLMODPRE, "M")
        * --- Riferimento vecchio magazzino dei saldi se � cambiato devo aggiornare i saldi per i materiali
        this.w_OLDMAGA = NVL(_Curs_GSCI10BTC.OLCODMAG, SPACE(5))
        this.w_OLMAGPRE = iif(empty(nvl(this.w_ARMAGIMP,"")),iif(empty(this.w_ARMAGPRE),iif(empty(this.oParentObject.w_MAGPAR),g_MAGAZI, this.oParentObject.w_MAGPAR),this.w_ARMAGPRE),this.w_ARMAGIMP)
        if this.GSCO_AOP.w_OLTSTATO="L"
          if this.w_CLFASCLA="S"
            this.w_OLMAGWIP = SPACE(5)
            this.w_OLCODMAG = this.w_OLMAGPRE
          else
            if this.w_OLMODPRE="N"
              this.w_OLMAGWIP = SPACE(5)
              this.w_OLCODMAG = iif(empty(nvl(this.w_ARMAGIMP,"")),iif(empty(this.w_ARMAGPRE),iif(empty(this.oParentObject.w_MAGPAR),g_MAGAZI, this.oParentObject.w_MAGPAR),this.w_ARMAGPRE),this.w_ARMAGIMP)
            else
              this.w_OLCODMAG = iif(empty(this.w_OLCODMAG), this.oParentObject.w_MAGWIP, this.w_OLCODMAG)
              this.w_OLMAGWIP = this.w_OLCODMAG
            endif
          endif
        else
          if this.w_CLFASCLA="S"
            this.w_OLMAGWIP = SPACE(5)
            this.w_OLCODMAG = this.w_OLMAGPRE
          else
            if this.w_OLMODPRE="N"
              this.w_OLMAGWIP = SPACE(5)
              this.w_OLCODMAG = this.w_OLMAGPRE
            else
              this.w_OLMAGWIP = this.w_OLCODMAG
              this.w_OLCODMAG = this.w_OLMAGPRE
            endif
          endif
        endif
        this.w_OLCODART = _Curs_GSCI10BTC.OLCODART
        this.w_OLKEYSAL = _Curs_GSCI10BTC.OLKEYSAL
        this.w_OLDQTSAL = _Curs_GSCI10BTC.OLQTASAL
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLFASRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MAXFASE),'ODL_DETT','OLFASRIF');
          +",OLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_TMPCPRIFE),'ODL_DETT','OLCPRIFE');
          +",OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'ODL_DETT','OLCODMAG');
          +",OLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_OLMAGWIP),'ODL_DETT','OLMAGWIP');
          +",OLMAGPRE ="+cp_NullLink(cp_ToStrODBC(this.w_OLMAGPRE),'ODL_DETT','OLMAGPRE');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              OLFASRIF = this.w_MAXFASE;
              ,OLCPRIFE = this.w_TMPCPRIFE;
              ,OLCODMAG = this.w_OLCODMAG;
              ,OLMAGWIP = this.w_OLMAGWIP;
              ,OLMAGPRE = this.w_OLMAGPRE;
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CLCODODL;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Nel caso sia cambiato il magazzino devo aggiornare i saldi
        if this.w_OLDMAGA<>this.w_OLCODMAG
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
                +" where ODL_MAST-OLCODODL = "+cp_ToStrODBC(this.w_CLCODODL)+"";
                 ,"_Curs_ODL_MAST")
          else
            select * from (i_cTable);
             where ODL_MAST-OLCODODL = this.w_CLCODODL;
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            this.w_CODCOM = _Curs_ODL_MAST.OLTCOMME
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLCOMM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLCOMM;
                from (i_cTable) where;
                    ARCODART = _Curs_ODL_MAST.OLTCOART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLFLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
          this.w_FLORDI = iif(_Curs_GSCI10BTC.OLFLORDI="+","-", iif(_Curs_GSCI10BTC.OLFLORDI="-","+"," "))
          this.w_FLIMPE = iif(_Curs_GSCI10BTC.OLFLIMPE="+","-", iif(_Curs_GSCI10BTC.OLFLIMPE="-","+"," "))
          this.w_FLRISE = iif(_Curs_GSCI10BTC.OLFLRISE="+","-", iif(_Curs_GSCI10BTC.OLFLRISE="-","+"," "))
          this.w_FLCOMD = NVL(_Curs_GSCI10BTC.ARFLCOMM, SPACE(1))
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLDMAGA);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                ,SLQTRPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLDMAGA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NVL(this.w_FLCOMD,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDMAGA);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  ,SCQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLKEYSAL;
                  and SCCODMAG = this.w_OLDMAGA;
                  and SCCODCAN = this.w_CODCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_FLORDI = iif(this.w_FLORDI="+","-", iif(this.w_FLORDI="-","+"," "))
          this.w_FLIMPE = iif(this.w_FLIMPE="+","-", iif(this.w_FLIMPE="-","+"," "))
          this.w_FLRISE = iif(this.w_FLRISE="+","-", iif(this.w_FLRISE="-","+"," "))
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                ,SLQTRPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLCODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- Try
            local bErr_052928D8
            bErr_052928D8=bTrsErr
            this.Try_052928D8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_052928D8
            * --- End
          endif
          this.w_CODCOM = this.oParentObject.w_OLTCOMME
          if NVL(this.w_FLCOMD,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  ,SCQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLKEYSAL;
                  and SCCODMAG = this.w_OLCODMAG;
                  and SCCODCAN = this.w_CODCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_Rows=0
              * --- Try
              local bErr_05290808
              bErr_05290808=bTrsErr
              this.Try_05290808()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_05290808
              * --- End
            endif
          endif
        endif
          select _Curs_GSCI10BTC
          continue
        enddo
        use
      endif
    endif
    GSCI_BCO(this,"CREATEALL", this.oParentObject.w_OLCODODL)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_052928D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLCODART,'SLCODMAG',this.w_OLCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLCODART;
           ,this.w_OLCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          ,SLQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_OLKEYSAL;
          and SLCODMAG = this.w_OLCODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_05290808()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODART"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'SALDICOM','SCCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLCODART,'SCCODMAG',this.w_OLCODMAG,'SCCODART',this.w_OLCODART,'SCCODCAN',this.w_CODCOM)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODART,SCCODCAN &i_ccchkf. );
         values (;
           this.w_OLCODART;
           ,this.w_OLCODMAG;
           ,this.w_OLCODART;
           ,this.w_CODCOM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_OLDQTSAL',this.w_OLDQTSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
             )
    else
      update (i_cTable) set;
          SCQTIPER = &i_cOp1.;
          ,SCQTOPER = &i_cOp2.;
          ,SCQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_OLKEYSAL;
          and SCCODMAG = this.w_OLCODMAG;
          and SCCODCAN = this.w_CODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Effettua il controllo sugli ordini di fase di Fase
    if g_COLA="S" and (this.oParentObject.w_OLTPROVE="I" or this.oParentObject.o_OLTPROVE="I") and not empty(this.oParentObject.w_OLTSECIC) and this.oParentObject.o_OLTSECIC=this.oParentObject.w_ORIGCICL
      * --- Prima di cambiare ciclo, verifica se ci sono degli ordini di fase collegati
      this.w_STOP = FALSE
      this.w_WARN = FALSE
      * --- Select from ODL_MAST
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select OLCODODL,OLTPROVE,OLTSTATO  from "+i_cTable+" ODL_MAST ";
            +" where OLTSEODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+"";
             ,"_Curs_ODL_MAST")
      else
        select OLCODODL,OLTPROVE,OLTSTATO from (i_cTable);
         where OLTSEODL=this.oParentObject.w_OLCODODL;
          into cursor _Curs_ODL_MAST
      endif
      if used('_Curs_ODL_MAST')
        select _Curs_ODL_MAST
        locate for 1=1
        do while not(eof())
        this.w_STOP = this.w_STOP or (nvl(_Curs_ODL_MAST.OLTSTATO," ") $ "F-L")
        this.w_WARN = TRUE
          select _Curs_ODL_MAST
          continue
        enddo
        use
      endif
      if this.w_STOP
        do case
          case this.Operazione="PROVENIENZA"
            this.TmpC = "Impossibile variare la provenienza dell'ordine%0%0Sono presenti ordini di di fase collegati alle fasi del ciclo corrente con stato maggiore di pianificato"
            this.oParentObject.w_OLTCODIC = this.oParentObject.o_OLTCODIC
            this.oParentObject.w_OLTPROVE = this.oParentObject.o_OLTPROVE
          case this.Operazione="CICLI"
            this.TmpC = "Impossibile variare il ciclo di lavorazione%0%0Sono presenti ordini di di fase collegati alle fasi del ciclo corrente con stato maggiore di pianificato"
            this.oParentObject.w_OLTSECIC = this.oParentObject.o_OLTSECIC
          case this.Operazione="CARICA_DETT"
            this.TmpC = "Impossibile variare la lista dei materiali stimati%0%0Sono presenti ordini di di fase collegati alle fasi del ciclo corrente con stato maggiore di pianificato"
          case Inlist(this.Operazione, "DATAINIVAR", "DATAFINVAR", "DATACONVAR")
            this.TmpC = "Impossibile variare le date dell'ordine%0%0Sono presenti ordini di di fase collegati alle fasi del ciclo corrente con stato maggiore di pianificato"
            this.oParentObject.w_OLTPROVE = this.oParentObject.o_OLTPROVE
            this.oParentObject.w_OLTDTCON = this.oParentObject.o_OLTDTCON
            this.oParentObject.w_OLTDTRIC = this.oParentObject.o_OLTDTRIC
            this.oParentObject.w_OLTDINRIC = this.oParentObject.o_OLTDINRIC
          otherwise
            this.TmpC = "Impossibile variare ODL%0%0Sono presenti ordini di di fase collegati alle fasi del ciclo corrente con stato maggiore di pianificato"
        endcase
        ah_ErrorMsg(this.TmpC,16)
        i_retcode = 'stop'
        return
      else
        if this.w_WARN
          do case
            case Inlist(this.Operazione, "PROVENIENZA", "CARICA_DETT", "DATAINIVAR", "DATAFINVAR", "DATACONVAR")
              if !this.oParentObject.w_DELOCLFA
                if this.oParentObject.w_OLTSTATO $ "MP"
                  * --- Verifica se Esistono ordini di fase
                  * --- Read from ODL_MAST
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "OLTFAODL"+;
                      " from "+i_cTable+" ODL_MAST where ";
                          +"OLTSEODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      OLTFAODL;
                      from (i_cTable) where;
                          OLTSEODL = this.oParentObject.w_OLCODODL;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.ROWORD = NVL(cp_ToDate(_read_.OLTFAODL),cp_NullValue(_read_.OLTFAODL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.ROWORD>0
                    this.w_oMess.addMsgPartNL("Attenzione")     
                    do case
                      case this.Operazione="PROVENIENZA"
                        this.w_oMess.addMsgPartNL("Sono presenti ordini di fase gi� confermati [pianificati] collegati alle fasi del ciclo corrente")     
                        this.w_oMess.addMsgPartNL("Modificando la Provenienza tali ordini saranno cancellati")     
                        this.w_oMess.addMsgPartNL("Si vuole continuare?")     
                      otherwise
                        this.w_oMess.addMsgPartNL("Esistono degli ordini di fase collegati che verranno eliminati")     
                        this.w_oMess.addMsgPartNL("Si vuole continuare?")     
                    endcase
                  endif
                endif
              endif
            case this.Operazione="CICLI"
              this.w_oMess.addMsgPartNL("Attenzione")     
              this.w_oMess.addMsgPartNL("Sono presenti ordini di fase gi� confermati [pianificati] collegati alle fasi del ciclo corrente")     
              this.w_oMess.addMsgPartNL("Confermando la presente maschera tali ordini saranno cancellati")     
              this.w_oMess.addMsgPartNL("Confermi richiesta di variazione ciclo di lavorazione?")     
          endcase
          if this.w_oMess.ah_YesNo()
            if Inlist(this.Operazione, "PROVENIENZA", "CARICA_DETT", "DATAINIVAR", "DATAFINVAR", "DATACONVAR")
              this.oParentObject.w_DELOCLFA = True
            endif
          else
            if this.Operazione="PROVENIENZA"
              this.oParentObject.w_DELOCLFA = False
              this.oParentObject.w_OLTCODIC = this.oParentObject.o_OLTCODIC
              this.oParentObject.w_OLTPROVE = this.oParentObject.o_OLTPROVE
            endif
            if Inlist(this.Operazione, "PROVENIENZA", "CARICA_DETT")
              this.oParentObject.w_DELOCLFA = False
            endif
            if Inlist(this.Operazione, "DATAINIVAR", "DATAFINVAR", "DATACONVAR")
              this.oParentObject.w_OLTPROVE = this.oParentObject.o_OLTPROVE
              this.oParentObject.w_OLTDTCON = this.oParentObject.o_OLTDTCON
              this.oParentObject.w_OLTDTRIC = this.oParentObject.o_OLTDTRIC
              this.oParentObject.w_OLTDINRIC = this.oParentObject.o_OLTDINRIC
            endif
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- OGGETTO w_MEMO
    * --- ----------------------------------------------------------------
    this.w_MATNOFASE = 0
    if g_PRFA="S" and g_CICLILAV="S" and !Empty(this.oParentObject.w_OLTSECIC) and INLIST(ALLTRIM(UPPER(this.GSCO_AOP.cFunction)) , "LOAD", "EDIT")
      * --- Sistema le fasi di riferimento
      this.GSCO_MOL.MarkPos()     
      this.GSCO_MOL.FirstRow()     
      do while Not this.GSCO_MOL.Eof_Trs()
        this.GSCO_MOL.SetRow()     
        if this.GSCO_MOL.FullRow() and INLIST(this.GSCO_MOL.RowStatus() , "U" , "A")
          this.m_OLDTOBSO = NVL(this.GSCO_MOL.Get("w_OLDTOBSO"), cp_CharToDate("  -  -  ") )
          this.m_OLDATRIC = NVL(this.GSCO_MOL.Get("w_OLDATRIC"), cp_CharToDate("  -  -  ") )
          if Not(this.m_OLDTOBSO>this.m_OLDATRIC or empty(this.m_OLDTOBSO)) Or Not(this.m_OLDTOBSO>i_DATSYS or empty(this.m_OLDTOBSO))
            this.m_CPROWORD = NVL(this.GSCO_MOL.Get("w_CPROWORD"), " " )
            this.m_OLCODICE = NVL(this.GSCO_MOL.Get("w_OLCODICE"), " " )
            this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Lista materiali, riga %1: codice di ricerca: %2 obsoleto dal %3")
            this.w_oPART1.AddParam(ALLTRIM(STR(this.m_CPROWORD)))     
            this.w_oPART1.AddParam(ALLTRIM(this.m_OLCODICE))     
            this.w_oPART1.AddParam(ALLTRIM(DTOC(this.m_OLDTOBSO)))     
          endif
          this.m_OLQTAMOV = NVL(this.GSCO_MOL.Get("w_OLQTAMOV"), 0 )
          this.m_OLQTAUM1 = NVL(this.GSCO_MOL.Get("w_OLQTAUM1"), 0 )
          this.m_CPROWORD = NVL(this.GSCO_MOL.Get("w_CPROWORD"), " " )
          this.m_OLCODICE = NVL(this.GSCO_MOL.Get("w_OLCODICE"), " " )
          if this.m_OLQTAUM1<=0
            * --- La quantit� convertita nell'U.M. principale � 0 oppure inferiore a 0.001. Il numero massimo di decimali gestiti � 3.
            this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Lista materiali, riga %1: codice di ricerca: %2 quantit� richiesta convertita nell'U.M. principale 0")
            this.w_oPART1.AddParam(ALLTRIM(STR(this.m_CPROWORD)))     
            this.w_oPART1.AddParam(ALLTRIM(this.m_OLCODICE))     
          endif
          if this.m_OLQTAMOV<=0
            * --- La quantit� convertita nell'U.M. � 0 oppure inferiore a 0.001. Il numero massimo di decimali gestiti � 3.
            this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Lista materiali, riga %1: codice di ricerca: %2 quantit� richiesta 0")
            this.w_oPART1.AddParam(ALLTRIM(STR(this.m_CPROWORD)))     
            this.w_oPART1.AddParam(ALLTRIM(this.m_OLCODICE))     
          endif
        endif
        * --- Passo alla prossima riga...
        this.GSCO_MOL.NextRow()     
      enddo
      this.GSCO_MOL.RePos(.F.)     
    endif
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Chiamato da GSCO_MCL
    * --- La parte dei controlli legata ai cicli di lavorazione dell'ordine assomiglia 
    *     un pochino ai controlli che vengogo effettuati al salvataggio del ciclo di lavorazione
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- CONTROLLI IMPLEMENTATI
    * --- 1) Controlla che esista uno e un solo indice di preferenza '00' per ogni Fase
    *     2) Controlla che esista una fase pref. per ogni fase non pref.
    *     3) Controlla che esista una fase pref. per ogni fase non pref.
    *     4) La fase non pu� essere disattivata se la fase � dichiarata
    *     5) Se � presente una sola fase esterna occorre inserire un ordine di conto lavoro
    *     6) Non � possibile attivare pi� fasi alternative per lo stesso codice di fase
    *     7) Non � possibile inserire una fase senza centro di lavoro associato
    *     8) non � possibile inserire una fase se nelle ricorse non sono presenti i tempi
    * --- CONTROLLI RIMOSSI
    * --- 1) Controllo su fasiconsecutive esterne
    *     2) Controllo su fase alternativa esterna
    *     3) Controllo sulle fasi che devono essere tutte attive
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- All'uscita dello zoom vale .t. se si esce con Save
    * --- CheckForm
    this.w_CLCODODL = this.GSCO_AOP.w_OLCODODL
    this.w_CLSERIAL = this.GSCO_AOP.w_OLTSECIC
    this.w_TRSNAME = this.GSCO_MCL.CTRSNAME
    this.Curso = this.GSCO_MCL.cTrsName
    * --- Controlla che siano inserite le date di validit�
    this.w_NODATE = .F.
    this.w_NOCLDES = .F.
    this.w_oMESS=createobject("AH_MESSAGE")
    * --- Flag caricamento ciclo acceso e nessuna fase inserita
    this.w_NFASI = 0
    this.GSCO_MCL.MarkPos()     
    this.w_NFASI = RecCount(this.Curso)
    this.GSCO_MCL.RePos(.F.)     
    if this.w_NFASI > 0
      this.w_LockScreen = this.GSCO_AOP.LockScreen
      this.GSCO_AOP.LockScreen = .t.
      Private L_RLROWNUM, L_CLROWORD, L_CLDESFAS
       L_RLROWNUM=0
       L_CLROWORD=0
      L_CLDESFAS=SPACE(40)
      this.w_TrsRiso = SYS(2015)
      select (this.w_TRSNAME)
      this.GSCO_MCL.MarkPos()     
      this.GSCO_MCL.FirstRow()     
      do while Not this.GSCO_MCL.Eof_Trs()
        this.GSCO_MCL.SetRow()     
        this.GSCO_MCL.NotifyEvent("SetEditableRow")     
        if this.GSCO_MCL.FullRow()
          this.GSCO_MCL.ChildrenChangeRow()     
          this.Curso = this.GSCO_MCL.GSCO_MRL.cTrsName
          L_RLROWNUM = this.GSCO_MCL.w_CPROWNUM
           L_CLROWORD = this.GSCO_MCL.w_CLROWORD
          L_CLDESFAS = this.GSCO_MCL.w_CLDESFAS
          L_EDITROW = this.GSCO_MCL.w_EDITROW
          if Not USED(this.w_TrsRiso)
            Select L_CLROWORD as t_CLROWORD, L_CLDESFAS as T_CLDESFAS, L_RLROWNUM as t_rlrownum, L_EDITROW as t_EDITROW, * ; 
 from (this.Curso) into cursor (this.w_TrsRiso) ReadWrite
          else
            Select(this.w_TrsRiso)
            APPEND FROM DBF(this.Curso)
            Update (this.w_TrsRiso) set T_CLROWORD=L_CLROWORD, T_CLDESFAS=L_CLDESFAS, t_RLROWNUM=L_RLROWNUM, t_EDITROW=L_EDITROW Where nvl(t_rlrownum, 0)=0
          endif
        endif
        this.GSCO_MCL.NextRow()     
      enddo
      this.GSCO_MCL.RePos(.F.)     
      this.Curso = this.GSCO_MCL.cTrsName
      this.GSCO_AOP.LockScreen = this.w_LockScreen
    endif
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Memorizza posizione su cursore
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    select (this.w_TRSNAME)
    this.w_OPOS = recno()
    * --- Prima verifica se c'e' il dettaglio
    count to w_TmpN for not empty(nvl(t_CLDESFAS, " "))
    select (this.w_TRSNAME)
    GO this.w_OPOS
    if w_TMPN=0
      this.oParentObject.w_TESTFORM = .F.
      this.w_MSG = "Non � stato caricato il ciclo di lavorazione associato all'ODL%0Impossibile salvare l'ODL"
      ah_ErrorMsg(this.w_MSG,48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TESTFORM
      Select (this.Curso)
      scan
      this.oParentObject.w_TESTFORM = this.oParentObject.w_TESTFORM and not empty(t_CLDESFAS)
      this.w_NFASI = this.w_NFASI + 1
      Select (this.Curso)
      this.w_CLROWORD = t_clroword
      this.w_MESS = alltrim(t_cldesfas)
      Select (this.Curso)
      endscan
      if not this.oParentObject.w_TESTFORM
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Inserire la descrizione della fase (%1)" )
        this.w_oPART.AddParam(Alltrim(str(this.w_CLROWORD)))     
        this.w_oMESS.ah_ErrorMsg()
        i_retcode = 'stop'
        return
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Controlla che esista uno e un solo indice di preferenza '00' per ogni Fase
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM
      Select t_clroword,t_cldesfas,sum(iif(t_clindpre="00",1,0)) as indfas from (this.w_TRSNAME) ; 
 where t_clfassel=1 group by t_clroword having indfas>1 into cursor minfas
      scan for this.oParentObject.w_TESTFORM
      this.w_CLROWORD = t_clroword
      this.w_MESS = alltrim(t_cldesfas)
      Select this.oParentObject.w_OLTDINRIC as t_clinival, this.oParentObject.w_OLTDTRIC as t_clfinval from (this.w_TRSNAME) ; 
 where t_clroword=this.w_CLROWORD and t_clindpre="00" and t_clfassel=1 into array prefas
      this.MAXFAS = alen(prefas,1)
      do while this.MAXFAS>1 and this.oParentObject.w_TESTFORM
        this.MAXFAS1 = this.MAXFAS - 1
        do while this.MAXFAS1>0 and this.oParentObject.w_TESTFORM
          * --- w_TESTFORM=false se gli intervalli di validit� non sono tutti disgiunti
          this.oParentObject.w_TESTFORM = not ((prefas(this.MAXFAS,1)<=prefas(this.MAXFAS1,1) and prefas(this.MAXFAS1,1)<=prefas(this.MAXFAS,2)) ;
          or (prefas(this.MAXFAS1,1)<=prefas(this.MAXFAS,1) and prefas(this.MAXFAS,1)<=prefas(this.MAXFAS1,2)))
          this.MAXFAS1 = this.MAXFAS1 - 1
        enddo
        this.MAXFAS = this.MAXFAS-1
      enddo
      release prefas
      endscan
      if not this.oParentObject.w_TESTFORM
        this.w_oPART1 = this.w_oMess2.AddMsgPartNL("Nella fase (%1) deve essere impostato un solo indice di preferenza '00'")
        this.w_oPART1.AddParam(Alltrim(this.w_MESS))     
      else
        * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        * --- Controlla che esista una fase pref. per ogni fase non pref.
        * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        Select t_clroword, t_cldesfas, this.oParentObject.w_OLTDINRIC as t_clinival, this.oParentObject.w_OLTDTRIC as t_clfinval from (this.w_TRSNAME) ; 
 where t_clindpre<>"00" and t_clfassel=1 into cursor minfas
        scan for this.oParentObject.w_TESTFORM
        this.w_CLROWORD = t_clroword
        this.w_MESS = alltrim(t_cldesfas)
        Select this.oParentObject.w_OLTDINRIC as t_clinival, this.oParentObject.w_OLTDTRIC as t_clfinval from (this.w_TRSNAME) ; 
 where t_clroword=this.w_CLROWORD and t_clindpre="00" into array prefas
        this.MAXFAS = iif(type("prefas")="U", 0, alen(prefas,1))
        this.w_CHF1 = .F.
        this.w_CHF2 = .F.
        do while this.MAXFAS>0
          this.w_CHF1 = this.w_CHF1 or (prefas(this.MAXFAS,1)<=minfas.t_clinival and minfas.t_clinival<=prefas(this.MAXFAS,2))
          this.w_CHF2 = this.w_CHF2 or (prefas(this.MAXFAS,1)<=minfas.t_clfinval and minfas.t_clfinval<=prefas(this.MAXFAS,2))
          this.MAXFAS = this.MAXFAS-1
        enddo
        release prefas
        this.oParentObject.w_TESTFORM = this.w_CHF1 and this.w_CHF2
        endscan
        if not this.oParentObject.w_TESTFORM
          this.w_oPART1 = this.w_oMess2.AddMsgPartNL("Nella fase (%1) deve essere impostato un solo indice di preferenza '00'")
          this.w_oPART1.AddParam(Alltrim(this.w_MESS))     
        else
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          * --- Controlla che per ogni fase siano impostati i tempi di attrezzaggio, avviamento o lavorazione
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          this.Curso = this.GSCO_MCL.GSCO_MRL.cTrsname
          this.Curso = this.w_TrsRiso
          Select * from (this.Curso) ; 
 where t_rltemris=0 and not empty(nvl(t_rlcodice," ")) and t_editrow ; 
 into cursor minfas
          scan for this.oParentObject.w_TESTFORM
          this.oParentObject.w_TESTFORM = .F.
          endscan
          use in select("minfas")
          if NOT this.oParentObject.w_TESTFORM
            this.w_MESS = "Sono presenti risorse senza tempi di attrezzaggio, avviamento o lavorazione"
            this.w_oPART1 = this.w_oMess2.addmsgpartNL(this.w_MESS)
          else
            * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            * --- Controlla che per ogni fase sia presente un centro di lavoro
            * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            Select DISTINCT T_RLROWNUM, T_RLCODICE , T_RL1TIPO, T_EDITROW from (this.Curso) into cursor __btc1__ where T_RL1TIPO="CL"
            if reccount("__btc1__")>0
              Select T_RLROWNUM, SUM(IIF(Empty(T_RLCODICE), 0 , iif(T_RL1TIPO="CL", 1, 0))) as t_rl_tipo from __BTC1__ ; 
 into cursor minfas where t_editrow group by T_RLROWNUM having SUM(IIF(Empty(T_RLCODICE), 0 , iif(T_RL1TIPO="CL", 1, 0)))=0 ; 
 or SUM(IIF(Empty(T_RLCODICE), 0 , iif(T_RL1TIPO="CL", 1, 0)))>1
              scan for this.oParentObject.w_TESTFORM
              this.oParentObject.w_TESTFORM = .F.
              if NOT this.oParentObject.w_TESTFORM
                if t_rl_tipo > 1
                  this.w_MESS = "Sono presenti fasi con pi� di un centro di lavoro associato%0Inserire un solo centro di lavoro"
                  this.w_oPART1 = this.w_oMess2.addmsgpartNL(this.w_MESS)
                else
                  this.w_MESS = "Sono presenti fasi senza centro di lavoro associato"
                  this.w_oPART1 = this.w_oMess2.addmsgpartNL(this.w_MESS)
                endif
              endif
              endscan
              use in select("minfas")
            else
              this.oParentObject.w_TESTFORM = .F.
              this.w_MESS = "Sono presenti fasi senza centro di lavoro associato"
              this.w_oPART1 = this.w_oMess2.addmsgpartNL(this.w_MESS)
            endif
            use in select("__btc1_")
          endif
        endif
      endif
      use in select("minfas")
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Controlla che per ogni fase non siano attivase pi� fasi alternative
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM
      SELECT T_CLROWORD,sum(T_CLFASSEL) as selez ;
      FROM (this.w_TRSNAME) GROUP BY T_CLROWORD HAVING selez<>1 ;
      INTO CURSOR __BTC__
      if used("__BTC__")
        * --- Scorre cursore risultato della precedente query (cursore __BTC__)
        SELECT "__BTC__"
        GO TOP
        SCAN
        if selez > 1
          this.oParentObject.w_TESTFORM = .F.
          if Not this.oParentObject.w_TESTFORM
            this.w_oPART = this.w_oMESS.AddMsgPartNL("Per la fase ( %1 ) sono attivate %2 fasi alternative")
            this.w_oPART.AddParam(Alltrim(Str(T_clroword,4,0)))     
            this.w_oPART.AddParam(Alltrim(Str(selez,2,0)))     
            this.w_oMESS.ah_ErrorMsg(48, "",.F.)
          endif
        endif
        SELECT "__BTC__"
        ENDSCAN
        * --- Chiude Cursore
        USE IN __BTC__
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Controlli di sicurezza (se per qualche strano motivo fosse editabile il flag anche se la fase � dichiarata)
    *     La fase non pu� essere disattivata se la fase � dichiarata
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM
      SELECT T_CLROWORD,sum(T_CLFASSEL) as selez, SUM(T_FLQTAEVA) AS QTAVER ;
      FROM (this.w_TRSNAME) GROUP BY T_CLROWORD HAVING selez<>1 ;
      INTO CURSOR __BTC__
      * --- Oltre alle quantit� presenti sull'ordine occorre controllare se qualche altro utente nel frattemo ha inserito delle dichiarazioni
      *     In questo caso non posso modificare lo stato della fase
      if used("__BTC__")
        * --- Scorre cursore risultato della precedente query (cursore __BTC__)
        SELECT "__BTC__"
        GO TOP
        SCAN
        if selez=0 and qtaver>0
          this.oParentObject.w_TESTFORM = .F.
          if Not this.oParentObject.w_TESTFORM
            this.w_oPART = this.w_oMESS.AddMsgPartNL("La fase ( %1 ) deve essere attiva perch� dichiarata")
            this.w_oPART.AddParam(Alltrim(Str(T_clroword,4,0)))     
            this.w_oMESS.ah_ErrorMsg(48, "",.F.)
          endif
        endif
        ENDSCAN
        * --- Chiude Cursore
        USE IN __BTC__
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Controlli di sicurezza
    *     Sono presenti fasi precedenti a fasi esterne senza flag di output
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM
      this.Curso = this.GSCO_MCL.GSCO_MRL.cTrsname
      this.Curso = this.w_TrsRiso
      this.w_NFASI = 0
      SELECT * FROM (this.w_TRSNAME) INTO CURSOR __BTC__ WHERE t_clfassel=1 and t_clroword<>0 and !empty(t_cldesfas) order by t_clroword READWRITE
      * --- Come prima cosa aggiorno il flag clfascla sul nuovo cursore per sapere quali sono le fasi esterne
      update __BTC__ set t_clfascla=0
      select t_RLROWNUM, CPROWNUM, min(t_rlintest) as t_rlintest from (this.Curso) into cursor _tmpFascla_ where t_rl1tipo="CL" and t_rlintest=1 group by t_RLROWNUM, CPROWNUM
      this.w_NFASI = Reccount("_tmpFascla_")
      * --- Ciclo sul transitorio delle risorse alla ricerca del centro di lavoro che mi determina la provenienza della fase
      if this.w_NFASI > 0
        SELECT "_tmpFascla_"
        GO TOP
        SCAN
        SELECT __BTC__
        LOCATE FOR CPROWNUM= _tmpFascla_.T_RLROWNUM
        if FOUND()
          replace t_clfascla with 1
        endif
        SELECT "_tmpFascla_"
        ENDSCAN
      endif
      use in select("_tmpFascla_")
      if used("__BTC__")
        this.w_PRIMOGIRO = .T.
        SELECT "__BTC__"
        GO TOP
        SCAN FOR NOT this.w_EXTCERR
        this.w_CLFASOUT = IIF(__BTC__.T_CLFASCLA=1 , 1, nvl (__BTC__.T_CLFASOUT , 0))
        this.w_CLCOUPOI = IIF(this.w_CLFASOUT = 1 , 1, nvl( __BTC__.T_CLCOUPOI , 0))
        if this.w_PRIMOGIRO
          this.w_OLDCP = this.w_CLCOUPOI
          this.w_OLDOU = this.w_CLFASOUT
          this.w_PRIMOGIRO = .F.
        endif
        this.w_EXTCONS = (__BTC__.T_CLFASCLA=1)
        this.w_EXTCERR = this.w_OLDOU<>this.w_CLFASOUT and this.w_EXTCONS
        this.w_OLDCP = this.w_CLCOUPOI
        this.w_OLDOU = this.w_CLFASOUT
        SELECT "__BTC__"
        ENDSCAN
        * --- Chiude Cursore
        if this.w_EXTCERR
          this.oParentObject.w_TESTFORM = .F.
          this.w_MSG = "Attenzione sono presenti fasi esterne con fase precedente senza il flag output attivo"
          this.w_oPART = this.w_oMESS.AddMsgPartNL( this.w_MSG )
        endif
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Sto creando un ODL con una sola fase esterna (devo inserire un OCL)
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM
      SELECT "__BTC__"
      COUNT FOR (__BTC__.T_CLFASSEL=1) TO this.w_NUMFAS
      SELECT "__BTC__"
      GO TOP
      LOCATE FOR T_CLFASCLA=1 AND T_CLFASSEL=1
      if FOUND()
        this.w_EXTCONS = .T.
      else
        this.w_EXTCONS = .F.
      endif
      this.w_EXTCERR = this.w_NUMFAS=1 AND this.oParentObject.w_OLTPROVE="I" and this.w_EXTCONS
      if this.w_EXTCERR
        this.oParentObject.w_TESTFORM = .F.
        this.w_MSG = "Attenzione � presente una sola fase esterna inserire un OCL"
        this.w_oPART = this.w_oMESS.AddMsgPartNL( this.w_MSG )
      endif
      this.w_EXTCERR = this.w_NUMFAS=1 AND this.oParentObject.w_OLTPROVE="L" and !this.w_EXTCONS
      if this.w_EXTCERR
        if this.w_EXTCERR
          this.oParentObject.w_TESTFORM = .F.
          this.w_MSG = "Attenzione � presente una sola fase interna inserire un ODL"
          this.w_oPART = this.w_oMESS.AddMsgPartNL( this.w_MSG )
        endif
      endif
    endif
    if this.w_oPOS > 0
      select (this.w_TRSNAME)
      count to w_TMPN for not deleted()
      if this.w_oPOS <= w_TMPN
        goto this.w_OPOS
      endif
    endif
    USE IN SELECT("__BTC__")
    USE IN SELECT("_ListFasi_")
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Controlli di sicurezza
    *     Non devono esistere righe di materiali associate a fasi senza flag di output
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM and g_PRFA="S" AND g_CICLILAV="S" AND g_COLA = "S" and not empty(this.oParentObject.w_OLTSECIC)
      this.Curso = this.GSCO_MOL.cTrsName
      SELECT t_CLROWORD,t_CLFASOUT FROM (this.w_TRSNAME) INTO CURSOR __BTC__ WHERE t_clfassel=1 and t_clroword<>0 and !empty(t_cldesfas) order by t_clroword READWRITE
      select t_OLFASRIF from (this.Curso) into cursor _tmpFas_ where t_OLFASRIF>0
      if used("__BTC__")
        SELECT "__BTC__"
        GO TOP
        SCAN FOR NOT this.w_EXTCERR
        this.w_CLFASOUT = nvl (__BTC__.T_CLFASOUT , 0)
        this.w_CLROWORD = nvl (__BTC__.t_CLROWORD , 0)
        if this.w_CLFASOUT=0
          SELECT "_tmpFas_"
          GO TOP
          LOCATE FOR this.w_CLROWORD=_tmpFas_.t_OLFASRIF
          if FOUND()
            this.w_EXTCERR = .t.
          endif
        endif
        SELECT "__BTC__"
        ENDSCAN
        * --- Chiude Cursore
        if this.w_EXTCERR
          this.oParentObject.w_TESTFORM = .F.
          this.w_MSG = "Attenzione sono presenti materiali associati a una fase senza il flag output attivo"
          this.w_oPART = this.w_oMESS.AddMsgPartNL( this.w_MSG )
        endif
      endif
      USE IN SELECT("__BTC__")
      USE IN SELECT("_tmpFas_")
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Passati tutti i controlli prima del salvataggio devo controllare, se l'utente ha inserito/spostato la posizione delle fasi
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Ho passato tutti i controlli, effettuo le verifiche
    *     Le verifiche devono essere fatte se siamo in modifica, in fase di caricamento l'utente � libero di effettuare qualsiasi tipo di operazione
    * --- Devo fare tutti i controlli anche se non ho il flag modifica fasi ODL perch� l'utente pu� comunque modificare il flag fase "attiva" e il numero della fase
    if this.oParentObject.w_TESTFORM
      if !Empty(this.w_CLSERIAL) and inlist(this.w_cFunction , "LOAD", "EDIT")
        this.Curso = this.GSCO_MCL.GSCO_MRL.cTrsname
        this.Curso = this.w_TrsRiso
        this.w_NFASI = 0
        SELECT * FROM (this.w_TRSNAME) INTO CURSOR __BTC__ WHERE t_clroword<>0 and !empty(t_cldesfas) order by t_clroword READWRITE
        Index on CPROWNUM tag CPROWNUM
        * --- Come prima cosa aggiorno il flag CLFASCLA sul nuovo cursore per sapere quali snon le fasi esterne
        update __BTC__ set t_clfascla=0
        * --- Resetto i valori dei flag prima dell'inserimento nella tabella temporana
         update __BTC__ set t_clcoupoi=0, t_clfasout=0, t_clultfas=0, t_clcodfas=SPACE(20), t_CLCPRIFE=0,; 
 t_CLBFRIFE=0, t_CLWIPFPR=SPACE(20), t_CLWIPOUT=SPACE(20) where t_CLFASSEL=0
        select t_RLROWNUM, CPROWNUM, min(t_rlintest) as t_rlintest from (this.Curso) into cursor _tmpFascla_ readwrite where t_rl1tipo="CL" and t_rlintest=1 group by t_RLROWNUM, CPROWNUM
        this.w_NFASI = Reccount("_tmpFascla_")
        * --- Ciclo sul transitorio delle risorse alla ricerca del centro di lavoro che mi determina la provenienza della fase
        if this.w_NFASI > 0
          SELECT "_tmpFascla_"
          GO TOP
          SCAN
          SELECT __BTC__
          if SEEK(_tmpFascla_.T_RLROWNUM)
            replace t_clfascla with 1
          endif
          SELECT "_tmpFascla_"
          ENDSCAN
        endif
        if .F.
          * --- Pulisco la tabella per l'elaborazione corrente
          * --- Delete from TMPODL_CICL
          i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                   )
          else
            delete from (i_cTable) where;
                  CLKEYRIF = this.w_CLKEYRIF;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from TMPODL_RISO
          i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                  +" and RLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
                   )
          else
            delete from (i_cTable) where;
                  CLKEYRIF = this.w_CLKEYRIF;
                  and RLCODODL = this.oParentObject.w_OLCODODL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Inserisco i dati in una tabella temporanea di appoggio
          this.w_LockScreen = this.GSCO_AOP.LockScreen
          this.GSCO_AOP.LockScreen = .t.
          Private L_RLROWNUM, L_CLROWORD, L_CLDESFAS
           L_RLROWNUM=0
           L_CLROWORD=0
          L_CLDESFAS=SPACE(40)
          this.w_TrsRiso = SYS(2015)
          * --- Ciclo sul transitorio
          select (this.w_TRSNAME)
          this.GSCO_MCL.MarkPos()     
          this.GSCO_MCL.FirstRow()     
          do while Not this.GSCO_MCL.Eof_Trs()
            this.GSCO_MCL.SetRow()     
            if this.GSCO_MCL.FullRow()
              this.GSCO_MCL.ChildrenChangeRow()     
              if this.GSCO_MCL.w_CLFASSEL="S"
                * --- Ciclo sul corsore per recuperare il magazzino WIP in uso del centro di lavoro in modo da inserirlo nella tebella dei cicli
                this.GSCO_MRL.MarkPos()     
                this.GSCO_MRL.FirstRow()     
                do while Not this.GSCO_MRL.Eof_Trs()
                  this.GSCO_MRL.SetRow()     
                  * --- Insert into TMPODL_RISO
                  i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_RISO_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"CLKEYRIF"+",RLCODODL"+",RLROWNUM"+",CPROWNUM"+",CPROWORD"+",RLCODCLA"+",RLCODICE"+",RLQTAATT"+",RLUMTATT"+",RLATTTEM"+",RLATTSEC"+",RLQTAAVV"+",RLUMTAVV"+",RLAVVTEM"+",RLAVVSEC"+",RLQTALAV"+",RLUMTLAV"+",RLLAVTEM"+",RLLAVSEC"+",RLTPREVS"+",RLTPRATT"+",RLTPRAVV"+",RLTPRLAV"+",RLTPRLAU"+",RLTPSTDS"+",RLQPSATT"+",RLTPSATT"+",RLQPSAVV"+",RLTPSAVV"+",RLQPSLAV"+",RLTPSLAV"+",RLTPSLAU"+",RLTCONSS"+",RLTCOATT"+",RLTCOAVV"+",RLTCOLAV"+",RLTCOEXT"+",RLCODPAD"+",RLNUMIMP"+",RLPROORA"+",RLTMPCIC"+",RLTMPSEC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_CLKEYRIF),'TMPODL_RISO','CLKEYRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODODL),'TMPODL_RISO','RLCODODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLROWNUM),'TMPODL_RISO','RLROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_CPROWNUM),'TMPODL_RISO','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_CPROWORD),'TMPODL_RISO','CPROWORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLCODCLA),'TMPODL_RISO','RLCODCLA');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLCODICE),'TMPODL_RISO','RLCODICE');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLQTAATT),'TMPODL_RISO','RLQTAATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLUMTATT),'TMPODL_RISO','RLUMTATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLATTTEM),'TMPODL_RISO','RLATTTEM');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLATTSEC),'TMPODL_RISO','RLATTSEC');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLQTAAVV),'TMPODL_RISO','RLQTAAVV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLUMTAVV),'TMPODL_RISO','RLUMTAVV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLAVVTEM),'TMPODL_RISO','RLAVVTEM');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLAVVSEC),'TMPODL_RISO','RLAVVSEC');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLQTALAV),'TMPODL_RISO','RLQTALAV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLUMTLAV),'TMPODL_RISO','RLUMTLAV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLLAVTEM),'TMPODL_RISO','RLLAVTEM');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLLAVSEC),'TMPODL_RISO','RLLAVSEC');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPREVS),'TMPODL_RISO','RLTPREVS');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPRATT),'TMPODL_RISO','RLTPRATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPRAVV),'TMPODL_RISO','RLTPRAVV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPRLAV),'TMPODL_RISO','RLTPRLAV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPRLAU),'TMPODL_RISO','RLTPRLAU');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPSTDS),'TMPODL_RISO','RLTPSTDS');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLQPSATT),'TMPODL_RISO','RLQPSATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPSATT),'TMPODL_RISO','RLTPSATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLQPSAVV),'TMPODL_RISO','RLQPSAVV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPSAVV),'TMPODL_RISO','RLTPSAVV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLQPSLAV),'TMPODL_RISO','RLQPSLAV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPSLAV),'TMPODL_RISO','RLTPSLAV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTPSLAU),'TMPODL_RISO','RLTPSLAU');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTCONSS),'TMPODL_RISO','RLTCONSS');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTCOATT),'TMPODL_RISO','RLTCOATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTCOAVV),'TMPODL_RISO','RLTCOAVV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTCOLAV),'TMPODL_RISO','RLTCOLAV');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTCOEXT),'TMPODL_RISO','RLTCOEXT');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLCODPAD),'TMPODL_RISO','RLCODPAD');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLNUMIMP),'TMPODL_RISO','RLNUMIMP');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLPROORA),'TMPODL_RISO','RLPROORA');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTMPCIC),'TMPODL_RISO','RLTMPCIC');
                    +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MRL.w_RLTMPSEC),'TMPODL_RISO','RLTMPSEC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_CLKEYRIF,'RLCODODL',this.w_CLCODODL,'RLROWNUM',this.GSCO_MRL.w_RLROWNUM,'CPROWNUM',this.GSCO_MRL.w_CPROWNUM,'CPROWORD',this.GSCO_MRL.w_CPROWORD,'RLCODCLA',this.GSCO_MRL.w_RLCODCLA,'RLCODICE',this.GSCO_MRL.w_RLCODICE,'RLQTAATT',this.GSCO_MRL.w_RLQTAATT,'RLUMTATT',this.GSCO_MRL.w_RLUMTATT,'RLATTTEM',this.GSCO_MRL.w_RLATTTEM,'RLATTSEC',this.GSCO_MRL.w_RLATTSEC,'RLQTAAVV',this.GSCO_MRL.w_RLQTAAVV)
                    insert into (i_cTable) (CLKEYRIF,RLCODODL,RLROWNUM,CPROWNUM,CPROWORD,RLCODCLA,RLCODICE,RLQTAATT,RLUMTATT,RLATTTEM,RLATTSEC,RLQTAAVV,RLUMTAVV,RLAVVTEM,RLAVVSEC,RLQTALAV,RLUMTLAV,RLLAVTEM,RLLAVSEC,RLTPREVS,RLTPRATT,RLTPRAVV,RLTPRLAV,RLTPRLAU,RLTPSTDS,RLQPSATT,RLTPSATT,RLQPSAVV,RLTPSAVV,RLQPSLAV,RLTPSLAV,RLTPSLAU,RLTCONSS,RLTCOATT,RLTCOAVV,RLTCOLAV,RLTCOEXT,RLCODPAD,RLNUMIMP,RLPROORA,RLTMPCIC,RLTMPSEC &i_ccchkf. );
                       values (;
                         this.w_CLKEYRIF;
                         ,this.w_CLCODODL;
                         ,this.GSCO_MRL.w_RLROWNUM;
                         ,this.GSCO_MRL.w_CPROWNUM;
                         ,this.GSCO_MRL.w_CPROWORD;
                         ,this.GSCO_MRL.w_RLCODCLA;
                         ,this.GSCO_MRL.w_RLCODICE;
                         ,this.GSCO_MRL.w_RLQTAATT;
                         ,this.GSCO_MRL.w_RLUMTATT;
                         ,this.GSCO_MRL.w_RLATTTEM;
                         ,this.GSCO_MRL.w_RLATTSEC;
                         ,this.GSCO_MRL.w_RLQTAAVV;
                         ,this.GSCO_MRL.w_RLUMTAVV;
                         ,this.GSCO_MRL.w_RLAVVTEM;
                         ,this.GSCO_MRL.w_RLAVVSEC;
                         ,this.GSCO_MRL.w_RLQTALAV;
                         ,this.GSCO_MRL.w_RLUMTLAV;
                         ,this.GSCO_MRL.w_RLLAVTEM;
                         ,this.GSCO_MRL.w_RLLAVSEC;
                         ,this.GSCO_MRL.w_RLTPREVS;
                         ,this.GSCO_MRL.w_RLTPRATT;
                         ,this.GSCO_MRL.w_RLTPRAVV;
                         ,this.GSCO_MRL.w_RLTPRLAV;
                         ,this.GSCO_MRL.w_RLTPRLAU;
                         ,this.GSCO_MRL.w_RLTPSTDS;
                         ,this.GSCO_MRL.w_RLQPSATT;
                         ,this.GSCO_MRL.w_RLTPSATT;
                         ,this.GSCO_MRL.w_RLQPSAVV;
                         ,this.GSCO_MRL.w_RLTPSAVV;
                         ,this.GSCO_MRL.w_RLQPSLAV;
                         ,this.GSCO_MRL.w_RLTPSLAV;
                         ,this.GSCO_MRL.w_RLTPSLAU;
                         ,this.GSCO_MRL.w_RLTCONSS;
                         ,this.GSCO_MRL.w_RLTCOATT;
                         ,this.GSCO_MRL.w_RLTCOAVV;
                         ,this.GSCO_MRL.w_RLTCOLAV;
                         ,this.GSCO_MRL.w_RLTCOEXT;
                         ,this.GSCO_MRL.w_RLCODPAD;
                         ,this.GSCO_MRL.w_RLNUMIMP;
                         ,this.GSCO_MRL.w_RLPROORA;
                         ,this.GSCO_MRL.w_RLTMPCIC;
                         ,this.GSCO_MRL.w_RLTMPSEC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                  if this.GSCO_MRL.w_RL__TIPO = "CL"
                    this.w_RLMAGWIP = this.GSCO_MRL.w_RLMAGWIP
                  endif
                  this.GSCO_MRL.NextRow()     
                enddo
                this.GSCO_MRL.RePos(.F.)     
              endif
              * --- Recupero la provenienza della fase  (Interna/conto lavoro)
              SELECT __BTC__
              if SEEK(this.GSCO_MCL.w_CPROWNUM)
                this.w_CLFASCLA = IIF( t_clfascla=0, "N", "S")
              else
                this.w_CLFASCLA = SPACE(1)
              endif
              * --- Try
              local bErr_05456578
              bErr_05456578=bTrsErr
              this.Try_05456578()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_05456578
              * --- End
            endif
            select (this.w_TRSNAME)
            this.GSCO_MCL.NextRow()     
          enddo
          this.GSCO_MCL.RePos(.F.)     
          this.Curso = this.GSCO_MCL.cTrsName
          this.GSCO_AOP.LockScreen = this.w_LockScreen
          * --- --------------------------------------------------------------------------------------------------
          * --- Per verificare la correttezza dei dati inseriti occorre effettuare tutte le operazioni che si effettuano al salvataggio
          *     Utilizziamo la tabella TMPODL_CICL
          * --- Chiamo il batch per la sistemazione delle fasi inserite nella tabella temporanea TMPODL_CICL
          *     GSDBTBFO batch clone del GSCO_BFO che tratta i dati utilizzando TMPODL_CICL anzich� ODL_CICL
          * --- --------------------------------------------------------------------------------------------------
          * --- Elimino i dati riferiti al keyrif (elaborazione corrente)
          *     Il GSDBTBTO per generare i codici che non esistono si appoggia a ART_TEMP
          * --- --------------------------------------------------------------------------------------------------
          * --- Delete from ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CAKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                   )
          else
            delete from (i_cTable) where;
                  CAKEYRIF = this.w_CLKEYRIF;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          GSDBTBFO(this,this.w_CLCODODL, this.w_CLKEYRIF)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        GSCI_BML(this.GSCO_MOL, "ALLFASI")
        * --- --------------------------------------------------------------------------------------------------
        * --- Ora effettuiamo i controlli per validare le modifiche apportate dall'utente
        * --- Controllo se � variata la provenienza per le fasi dichiarate
        * --- --------------------------------------------------------------------------------------------------
        * --- Select from TMPODL_CICL
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPODL_CICL ";
              +" where CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF)+" AND CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL)+" And CLFASSEL='S' And CLFASOUT='S'";
              +" order by CLROWORD";
               ,"_Curs_TMPODL_CICL")
        else
          select * from (i_cTable);
           where CLKEYRIF = this.w_CLKEYRIF AND CLCODODL = this.w_CLCODODL And CLFASSEL="S" And CLFASOUT="S";
           order by CLROWORD;
            into cursor _Curs_TMPODL_CICL
        endif
        if used('_Curs_TMPODL_CICL')
          select _Curs_TMPODL_CICL
          locate for 1=1
          do while not(eof())
          this.w_CLROWORD = _Curs_TMPODL_CICL.CLROWORD
          * --- Select from GSCIC4BTC
          do vq_exec with 'GSCIC4BTC',this,'_Curs_GSCIC4BTC','',.f.,.t.
          if used('_Curs_GSCIC4BTC')
            select _Curs_GSCIC4BTC
            locate for 1=1
            do while not(eof())
            this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile variare provenienza della fase (%1), la fase %2 � gi� dichiarata")
            this.w_oPART1.AddParam(ALLTRIM(STR(this.w_CLROWORD)))     
            this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC4BTC.CLROWORD)))     
            * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            * --- Controlli di sicurezza
            *     Sono presenti fasi di precedenti fasi esterne senza flag di output
            * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
              select _Curs_GSCIC4BTC
              continue
            enddo
            use
          endif
            select _Curs_TMPODL_CICL
            continue
          enddo
          use
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Nel caso in cui l'utente abbia eliminato le tutte le fasi successive ad una certa fase X 
        *     (dichiarata con aggiornamento di magazzino processato) e questa diventa 
        *     ultima fase devo bloccare l'utente altrimenti mi resta a magazzino il codice della fase.
        *     Infatti la procedura aveva caricato il codice di fase anzich� il codice del PF/SL
        * --- Select from GSCIC10BTC
        do vq_exec with 'GSCIC10BTC',this,'_Curs_GSCIC10BTC','',.f.,.t.
        if used('_Curs_GSCIC10BTC')
          select _Curs_GSCIC10BTC
          locate for 1=1
          do while not(eof())
          this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile impostare la fase (%1) come ultima fase, � presente almeno una dichiarazione gi� processata.")
          this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC10BTC.CLROWORD)))     
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          * --- Controlli di sicurezza
          *     Sono presenti fasi di precedenti fasi esterne senza flag di output
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            select _Curs_GSCIC10BTC
            continue
          enddo
          use
        endif
        * --- --------------------------------------------------------------------------------------------------
        * --- Controllo se ho eliminato oppure disattivato una fase di conto lavoro che ha generato OCL
        * --- --------------------------------------------------------------------------------------------------
        vq_exec("GSCIC11BTC",this,"_Curs_GSCIC11BTC")
        SELECT _Curs_GSCIC11BTC
        GO TOP
        SCAN
        * --- Codice ordine di conto lavoro
        this.w_CODODL = NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))
        * --- Resetto sempre la fase cos� posso fare il test a pag5
        this.w_CURRFFAS = 0
        * --- Controllo se � cambiata l'ultima fase
        if _Curs_GSCIC11BTC.CLFASSEL= "S" AND _Curs_GSCIC11BTC.OLULTFAS <> _Curs_GSCIC11BTC.CLULTFAS AND _Curs_GSCIC11BTC.CLULTFAS = "S" AND _Curs_GSCIC11BTC.CLROWORD<>0 and !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15)))
          * --- Se lo stato dell'ocl di fase � suggerito o pianificato allora visualizzo il messaggio come non bloccante
          *     altrimenti  messaggio bloccante
          if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
            this.w_oPART1 = this.w_oMess1.addmsgpartNL("Impostando come ultima la fase (%1) verr� eliminato l'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%4] con stato %5")
            this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
            this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.OLWIPFPR))     
            this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.CLWIPFPR))     
            this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
            this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
            * --- Memorizzo la fase di riferimento per verificare se ci sono materiali di input associati alla fase
            this.w_CURRFFAS = _Curs_GSCIC11BTC.CLROWORD
            INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_CURRFFAS )
            * --- inserisco nel cursore degli ordini di conto lavoro da eliminare il relativo codice
            this.Page_15()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT "_Curs_GSCIC11BTC"
            REPLACE VERIFICATO WITH "S"
          else
            this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile impostare la fase (%1) come ultima, la fase ha generato un ordine di conto lavoro di fase con stato maggiore di Da Ordinare.")
            this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
            this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.OLWIPFPR))     
            this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.CLWIPFPR))     
          endif
        endif
        * --- Controllo se � stata disattivata oppure eliminata la fase
        if _Curs_GSCIC11BTC.OLFASSEL="S" And _Curs_GSCIC11BTC.CLFASSEL<>"S" or _Curs_GSCIC11BTC.CLROWORD=0
          * --- Se la fase � stata disattivata/eliminata verifico se ci sono degli OCL generati
          if Not Empty(this.w_CODODL)
            if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
              this.w_oPART1 = this.w_oMess1.addmsgpartNL("L'eliminazione/disattivazione della fase (%1) comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%2] con stato %3")
              this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.OLROWORD)))     
              this.w_oPART1.AddParam(ALLTRIM(this.w_CODODL))     
              this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
              * --- Memorizzo la fase di riferimento per verificare se ci sono materiali di input associati alla fase
              this.w_CURRFFAS = _Curs_GSCIC11BTC.OLROWORD
              INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_CURRFFAS )
              * --- inserisco nel cursore degli ordini di conto lavoro da eliminare il relativo codice
              this.Page_15()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              SELECT "_Curs_GSCIC11BTC"
              REPLACE VERIFICATO WITH "S"
            else
              this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile eliminare la fase (%1), la fase ha generato l'OCL di fase (%2) con stato maggiore di Da Ordinare.")
              this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.OLROWORD)))     
              this.w_oPART1.AddParam(alltrim(this.w_CODODL))     
            endif
          endif
        else
          if _Curs_GSCIC11BTC.CLROWORD<>0 and !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))) AND _Curs_GSCIC11BTC.OLWIPFPR <> _Curs_GSCIC11BTC.CLWIPFPR
            * --- Se lo stato dell'ocl di fase � suggerito o pianificato allora visualizzo il messaggio come non bloccante
            *     altrimenti  messaggio bloccante
            if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
              this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica dell'input della fase (%1) da %2 a %3 comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%4] con stato %5")
              this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
              this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.OLWIPFPR))     
              this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.CLWIPFPR))     
              this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
              this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
              * --- Memorizzo la fase di riferimento per verificare se ci sono materiali di input associati alla fase
              this.w_CURRFFAS = _Curs_GSCIC11BTC.CLROWORD
              INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_CURRFFAS )
              * --- inserisco nel cursore degli ordini di conto lavoro da eliminare il relativo codice
              this.Page_15()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              SELECT "_Curs_GSCIC11BTC"
              REPLACE VERIFICATO WITH "S"
            else
              this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile modificare linput della fase (%1) da %2 a %3, la fase ha generato un OCL di fase con stato maggiore di Da Ordinare.")
              this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
              this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.OLWIPFPR))     
              this.w_oPART1.AddParam(alltrim(_Curs_GSCIC11BTC.CLWIPFPR))     
            endif
          endif
        endif
        this.w_PROFASMO = .F.
        if _Curs_GSCIC11BTC.CLROWORD<>0 AND _Curs_GSCIC11BTC.CLFASCLA<>_Curs_GSCIC11BTC.OLFASCLA AND !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))) 
          if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
            this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica della provenienza della fase [%1] da (esterna) a (interna) comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%2] con stato %3")
            this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
            this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
            this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
            * --- Riapre lista materiali della fase associatata all'OCL
            this.w_L_CLROWORD = _Curs_GSCIC11BTC.CLROWORD
            INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_L_CLROWORD )
            * --- inserisco nel cursore degli ordini di conto lavoro da eliminare il relativo codice
            this.Page_15()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PROFASMO = .T.
            SELECT "_Curs_GSCIC11BTC"
            REPLACE VERIFICATO WITH "S"
          else
            this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile modificare la provenienza della fase (%1) da %2 a %3, la fase ha generato un OCL di fase con stato maggiore di Da Ordinare.")
            this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
            this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.RLINTESO="E", "Esterna", "Interna")))     
            this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.RLINTEST="E", "Esterna", "Interna")))     
          endif
        endif
        this.w_TIPOMODIFICA = SPACE(20)
        do case
          case _Curs_GSCIC11BTC.CLROWORD<>_Curs_GSCIC11BTC.OLROWORD AND _Curs_GSCIC11BTC.CLROWORD<>0 AND _Curs_GSCIC11BTC.OLROWORD<>0 AND _Curs_GSCIC11BTC.CLCODFAS <> _Curs_GSCIC11BTC.OLCODFAS AND !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15)))
            this.w_TIPOMODIFICA = "SEQUENZA"
          case _Curs_GSCIC11BTC.CLROWORD<>0 AND _Curs_GSCIC11BTC.CLFASCLA<>_Curs_GSCIC11BTC.OLFASCLA AND !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))) 
            this.w_TIPOMODIFICA = "PROVENIENZA"
          case _Curs_GSCIC11BTC.CLROWORD<>0 AND _Curs_GSCIC11BTC.RLCODICO<>_Curs_GSCIC11BTC.RLCODICE AND !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))) 
            if _Curs_GSCIC11BTC.RLMAGWIO <> _Curs_GSCIC11BTC.RLMAGWIP OR _Curs_GSCIC11BTC.RLINTESO <> _Curs_GSCIC11BTC.RLINTEST OR _Curs_GSCIC11BTC.LFCODFOO <> _Curs_GSCIC11BTC.LFCODFOR
              this.w_TIPOMODIFICA = "CENTROLAVORO"
            endif
        endcase
        * --- Nel caso di modifica della quanti� sull'ODL con OCL generati di conto lavoro in stato lanciato o finito questo controlla blocca passando davanti a tutti
        if _Curs_GSCIC11BTC.CLROWORD<>0 AND _Curs_GSCIC11BTC.OLROWORD<>0 AND !EMPTY(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))) AND _Curs_GSCIC11BTC.OLTQTOD1<>_Curs_GSCIC11BTC.CLTQTOD1 AND _Curs_GSCIC11BTC.OLTSTATO $ "L-F"
          this.w_TIPOMODIFICA = "MODQUANTI"
        endif
        if INLIST(this.w_TIPOMODIFICA , "PROVENIENZA" , "CENTROLAVORO", "SEQUENZA", "MODQUANTI")
          if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
            do case
              case this.w_TIPOMODIFICA = "PROVENIENZA"
                this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica della provenienza della fase [%1] da (esterna) a (interna) comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%2] con stato %3")
              case this.w_TIPOMODIFICA = "CENTROLAVORO"
                this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica del centro di lavoro della fase [%1] comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%2] con stato %3")
              case this.w_TIPOMODIFICA = "SEQUENZA"
                this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica della sequenza della fase [%1] da (%2) a (%3) comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%4] con stato %5")
            endcase
            do case
              case this.w_TIPOMODIFICA = "SEQUENZA"
                this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.OLROWORD)))     
                this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.OLROWORD)))     
                this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
              otherwise
                this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
            endcase
            this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
            this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
            * --- Riapre lista materiali della fase associatata all'OCL
            this.w_L_CLROWORD = _Curs_GSCIC11BTC.CLROWORD
            INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_L_CLROWORD )
            * --- inserisco nel cursore degli ordini di conto lavoro da eliminare il relativo codice
            this.Page_15()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PROFASMO = .T.
            SELECT "_Curs_GSCIC11BTC"
            REPLACE VERIFICATO WITH "S"
          else
            this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile variare la fase (%1), la fase ha generato un OCL di fase con stato maggiore di Da Ordinare.")
            this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
          endif
        endif
        if _Curs_GSCIC11BTC.OLWIPFPR <> _Curs_GSCIC11BTC.CLWIPFPR Or (_Curs_GSCIC11BTC.OLWIPFPR = _Curs_GSCIC11BTC.CLWIPFPR and this.w_PROFASMO) Or !Empty(this.w_TIPOMODIFICA)
          * --- se la fase � di conto lacoro, vado a verificare se la fase precedente era di conto lavoro
          *     In questo caso devo eliminare l'OCL di fase legato alla ex fase precedente
          SELECT "_curs_GSCIC11BTC"
          this.w_L_CLCODFAS = _Curs_GSCIC11BTC.CLCODFAS
          this.w_L_OLWIPFPR = _Curs_GSCIC11BTC.OLWIPFPR
          this.w_L_CLWIPFPR = _Curs_GSCIC11BTC.CLWIPFPR
          this.w_RECNO = Recno()
          * --- Controllo la fase precedente corrente
          SELECT "_Curs_GSCIC11BTC"
          GO TOP
          LOCATE FOR _Curs_GSCIC11BTC.OLCODFAS = this.w_L_CLWIPFPR
          this.w_FOUND = FOUND()
          if this.w_FOUND and !Empty(_Curs_GSCIC11BTC.OLCODODL)
            if _Curs_GSCIC11BTC.VERIFICATO<>"S"
              this.w_CODODL = NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))
              if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
                this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica della fase successiva alla fase (%1) comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%2] con stato %3")
                this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
                this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
                this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
                * --- Memorizzo la fase di riferimento per verificare se ci sono materiali di input associati alla fase
                this.w_CURRFFAS = _Curs_GSCIC11BTC.OLROWORD
                INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_CURRFFAS )
                this.Page_15()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                SELECT "_Curs_GSCIC11BTC"
                REPLACE VERIFICATO WITH "S"
              else
                this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile modificare la fase successiva alla fase (%1), la fase ha generato l'OCL di fase [%2] con stato maggiore di Da Ordinare.")
                this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
                this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
              endif
            endif
            * --- Controllo la fase precente OLD
            SELECT "_Curs_GSCIC11BTC"
            GO TOP
            LOCATE FOR _Curs_GSCIC11BTC.OLCODFAS = this.w_L_OLWIPFPR
            this.w_FOUND = FOUND()
            if this.w_FOUND and !Empty(_Curs_GSCIC11BTC.OLCODODL)
              if _Curs_GSCIC11BTC.VERIFICATO<>"S"
                this.w_CODODL = NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))
                if _Curs_GSCIC11BTC.OLTSTATO $ "M-'P"
                  this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica della fase successiva alla fase (%1) comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%2] con stato %3")
                  this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
                  this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
                  this.w_oPART1.AddParam(alltrim(IIF(_Curs_GSCIC11BTC.OLTSTATO="M", "Suggerito", "Pianificato")))     
                  * --- Memorizzo la fase di riferimento per verificare se ci sono materiali di input associati alla fase
                  this.w_CURRFFAS = _Curs_GSCIC11BTC.OLROWORD
                  INSERT INTO ( this.w_Cur_Fasi_Mod ) VALUES ( this.w_CURRFFAS )
                  this.Page_15()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  SELECT "_Curs_GSCIC11BTC"
                  REPLACE VERIFICATO WITH "S"
                else
                  this.w_oPART1 = this.w_oMess2.addmsgpartNL("Impossibile modificare la fase successiva alla fase (%1), la fase ha generato l'OCL di fase [%2] con stato maggiore di Da Ordinare.")
                  this.w_oPART1.AddParam(alltrim(str(_Curs_GSCIC11BTC.CLROWORD)))     
                  this.w_oPART1.AddParam(ALLTRIM(NVL(_Curs_GSCIC11BTC.OLCODODL, SPACE(15))))     
                endif
              endif
            endif
          endif
          SELECT "_Curs_GSCIC11BTC"
          GO this.w_RECNO
        endif
        SELECT _Curs_GSCIC11BTC
        ENDSCAN
        USE IN SELECT("_Curs_GSCIC11BTC")
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.oParentObject.w_TESTFORM and g_PRFA="S" AND g_CICLILAV="S" AND g_COLA = "S" and not empty(this.oParentObject.w_OLTSECIC)
      * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno, da trattare come salto codice
      * --- Il ciclo che viene controllato � il ciclo legato all'ODL infatti potrei anche averlo modificato
      this.w_MFEXT = "N"
      this.oParentObject.o_OLTPROVE = this.oParentObject.w_OLTPROVE
      this.Page_16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.o_OLTPROVE <> this.oParentObject.w_OLTPROVE AND this.oParentObject.w_OLTPROVE = "L"
        this.oParentObject.w_OLTPROVE = "I"
        this.oParentObject.w_OLTCOFOR = space(15)
        this.oParentObject.w_DESFOR = space(40)
        this.oParentObject.w_OLCODVAL = Space(3)
        this.oParentObject.w_LMAGTER = SPACE(5)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GSCO_AOP.mCalc(.t.)     
      endif
      if this.oParentObject.o_OLTPROVE <> this.oParentObject.w_OLTPROVE
        this.oParentObject.w_TESTFORM = FALSE
        this.w_oPART1 = this.w_oMess1.addmsgpartNL("La modifica del ciclo ha comportato la modifica della provenienza dell'ordine")
        this.w_oPART1.AddParam(ALLTRIM(STR(this.m_CPROWORD)))     
        * --- Aggiorna testata ODL
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GSCO_AOP.mCalc(.t.)     
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  endproc
  proc Try_05456578()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPODL_CICL
    i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CLKEYRIF"+",CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CL__FASE"+",CLDESFAS"+",CLSTAPRO"+",CLQTAPRE"+",CLPREUM1"+",CLQTAAVA"+",CLAVAUM1"+",CLQTADIC"+",CLDICUM1"+",CLFASEVA"+",CLCOUPOI"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLBFRIFE"+",CLDATINI"+",CLDATFIN"+",CLWIPOUT"+",CLWIPDES"+",CLWIPFPR"+",CLFANOIN"+",CLFASCON"+",CLCODERR"+",CLSCHEDU"+",CLFASCOS"+",CLCODFAS"+",CLFASSCH"+",CLFASOUT"+",CLFASCLA"+",CLMAGWIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CLKEYRIF),'TMPODL_CICL','CLKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODODL),'TMPODL_CICL','CLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CPROWNUM),'TMPODL_CICL','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLROWNUM),'TMPODL_CICL','CLROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLROWORD),'TMPODL_CICL','CLROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLINDPRE),'TMPODL_CICL','CLINDPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CL__FASE),'TMPODL_CICL','CL__FASE');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLDESFAS),'TMPODL_CICL','CLDESFAS');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLSTAPRO),'TMPODL_CICL','CLSTAPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLQTAPRE),'TMPODL_CICL','CLQTAPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLPREUM1),'TMPODL_CICL','CLPREUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLQTAAVA),'TMPODL_CICL','CLQTAAVA');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLAVAUM1),'TMPODL_CICL','CLAVAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLQTADIC),'TMPODL_CICL','CLQTADIC');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLDICUM1),'TMPODL_CICL','CLDICUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFASEVA),'TMPODL_CICL','CLFASEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLULTFAS),'TMPODL_CICL','CLULTFAS');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFASSEL),'TMPODL_CICL','CLFASSEL');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLDATINI),'TMPODL_CICL','CLDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLDATFIN),'TMPODL_CICL','CLDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'TMPODL_CICL','CLWIPDES');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFANOIN),'TMPODL_CICL','CLFANOIN');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFASCON),'TMPODL_CICL','CLFASCON');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLCODERR),'TMPODL_CICL','CLCODERR');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLSCHEDU),'TMPODL_CICL','CLSCHEDU');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFASCOS),'TMPODL_CICL','CLFASCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLCODFAS),'TMPODL_CICL','CLCODFAS');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFASSCH),'TMPODL_CICL','CLFASSCH');
      +","+cp_NullLink(cp_ToStrODBC(this.GSCO_MCL.w_CLFASOUT),'TMPODL_CICL','CLFASOUT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCLA),'TMPODL_CICL','CLFASCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RLMAGWIP),'TMPODL_CICL','CLMAGWIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_CLKEYRIF,'CLCODODL',this.w_CLCODODL,'CPROWNUM',this.GSCO_MCL.w_CPROWNUM,'CLROWNUM',this.GSCO_MCL.w_CLROWNUM,'CLROWORD',this.GSCO_MCL.w_CLROWORD,'CLINDPRE',this.GSCO_MCL.w_CLINDPRE,'CL__FASE',this.GSCO_MCL.w_CL__FASE,'CLDESFAS',this.GSCO_MCL.w_CLDESFAS,'CLSTAPRO',this.GSCO_MCL.w_CLSTAPRO,'CLQTAPRE',this.GSCO_MCL.w_CLQTAPRE,'CLPREUM1',this.GSCO_MCL.w_CLPREUM1,'CLQTAAVA',this.GSCO_MCL.w_CLQTAAVA)
      insert into (i_cTable) (CLKEYRIF,CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CL__FASE,CLDESFAS,CLSTAPRO,CLQTAPRE,CLPREUM1,CLQTAAVA,CLAVAUM1,CLQTADIC,CLDICUM1,CLFASEVA,CLCOUPOI,CLULTFAS,CLFASSEL,CLCPRIFE,CLBFRIFE,CLDATINI,CLDATFIN,CLWIPOUT,CLWIPDES,CLWIPFPR,CLFANOIN,CLFASCON,CLCODERR,CLSCHEDU,CLFASCOS,CLCODFAS,CLFASSCH,CLFASOUT,CLFASCLA,CLMAGWIP &i_ccchkf. );
         values (;
           this.w_CLKEYRIF;
           ,this.w_CLCODODL;
           ,this.GSCO_MCL.w_CPROWNUM;
           ,this.GSCO_MCL.w_CLROWNUM;
           ,this.GSCO_MCL.w_CLROWORD;
           ,this.GSCO_MCL.w_CLINDPRE;
           ,this.GSCO_MCL.w_CL__FASE;
           ,this.GSCO_MCL.w_CLDESFAS;
           ,this.GSCO_MCL.w_CLSTAPRO;
           ,this.GSCO_MCL.w_CLQTAPRE;
           ,this.GSCO_MCL.w_CLPREUM1;
           ,this.GSCO_MCL.w_CLQTAAVA;
           ,this.GSCO_MCL.w_CLAVAUM1;
           ,this.GSCO_MCL.w_CLQTADIC;
           ,this.GSCO_MCL.w_CLDICUM1;
           ,this.GSCO_MCL.w_CLFASEVA;
           ,this.GSCO_MCL.w_CLCOUPOI;
           ,this.GSCO_MCL.w_CLULTFAS;
           ,this.GSCO_MCL.w_CLFASSEL;
           ,this.GSCO_MCL.w_CLCPRIFE;
           ,this.GSCO_MCL.w_CLBFRIFE;
           ,this.GSCO_MCL.w_CLDATINI;
           ,this.GSCO_MCL.w_CLDATFIN;
           ,this.GSCO_MCL.w_CLWIPOUT;
           ,SPACE(5);
           ,this.GSCO_MCL.w_CLWIPFPR;
           ,this.GSCO_MCL.w_CLFANOIN;
           ,this.GSCO_MCL.w_CLFASCON;
           ,this.GSCO_MCL.w_CLCODERR;
           ,this.GSCO_MCL.w_CLSCHEDU;
           ,this.GSCO_MCL.w_CLFASCOS;
           ,this.GSCO_MCL.w_CLCODFAS;
           ,this.GSCO_MCL.w_CLFASSCH;
           ,this.GSCO_MCL.w_CLFASOUT;
           ,this.w_CLFASCLA;
           ,this.w_RLMAGWIP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FOUND = .F.
    this.w_TIPO = "C"
    L_Cur_Del_Ocl = this.GSCO_AOP.w_Cur_Del_Ocl
    if !Empty(L_Cur_Del_Ocl ) and Used( L_Cur_Del_Ocl )
      SELECT( L_Cur_Del_Ocl )
      GO TOP
      LOCATE FOR CLCODODL = this.w_CODODL and TIPO = this.w_TIPO
      this.w_FOUND = FOUND()
    endif
    if !this.w_FOUND
      this.GSCO_AOP.w_bOclfadadel = True
      if Empty(this.GSCO_AOP.w_Cur_Del_Ocl)
        this.GSCO_AOP.w_Cur_Del_Ocl = SYS(2015)
      endif
      * --- Assegno ad una variabile L_.. poich� devo utilizzare la Macro con il nome del Cursore 
      *     per gli OCL di fase da eliminare
      *     Creo il cursore contenente gli OCL
      L_Cur_Del_Ocl = this.GSCO_AOP.w_Cur_Del_Ocl
      if Not Used( L_Cur_Del_Ocl )
        CREATE CURSOR ( L_Cur_Del_Ocl ) ( CLCODODL C(15) , TIPO C(1), CCHECK C(10) )
        =wrcursor( L_Cur_Del_Ocl )
      endif
      INSERT INTO ( L_Cur_Del_Ocl ) VALUES (this.w_CODODL, this.w_TIPO, this.w_L_CHECK)
    endif
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_PRFA="S" and g_CICLILAV="S" and not empty(this.oParentObject.w_OLTSECIC)
      this.w_CLINDCIC = SPACE(2)
      this.w_nRecFasi = 0
      * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno, da trattare come salto codice
      VQ_EXEC ("..\PRFA\EXE\QUERY\GSCIDBCS", This, "_CicloPref_")
      if used("_CicloPref_")
        this.w_nRecFasi = reccount("_CicloPref_")
        if this.w_nRecFasi = 1
          this.w_MFEXT = "S"
          select _CicloPref_
          go top
          if nvl(_CicloPref_.RLINTEST,"X")="E"
            if this.oParentObject.w_OLTPROVE="I"
              this.oParentObject.w_OLTPROVE = "L"
              this.w_CODCENTRO = nvl(_CicloPref_.LRCODRIS ," ")
              * --- Select from ..\COLA\EXE\QUERY\GSCOFBOL
              do vq_exec with '..\COLA\EXE\QUERY\GSCOFBOL',this,'_Curs__d__d__COLA_EXE_QUERY_GSCOFBOL','',.f.,.t.
              if used('_Curs__d__d__COLA_EXE_QUERY_GSCOFBOL')
                select _Curs__d__d__COLA_EXE_QUERY_GSCOFBOL
                locate for 1=1
                do while not(eof())
                this.oParentObject.w_OLTCOFOR = _Curs__d__d__COLA_EXE_QUERY_GSCOFBOL.CODFOR
                  select _Curs__d__d__COLA_EXE_QUERY_GSCOFBOL
                  continue
                enddo
                use
              endif
              if not empty(this.oParentObject.w_OLTCOFOR)
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANDESCRI,ANMAGTER,ANCODVAL"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC("F");
                        +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCOFOR);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANDESCRI,ANMAGTER,ANCODVAL;
                    from (i_cTable) where;
                        ANTIPCON = "F";
                        and ANCODICE = this.oParentObject.w_OLTCOFOR;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_DESFOR = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
                  this.oParentObject.w_LMAGTER = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
                  this.oParentObject.w_OLCODVAL = NVL(cp_ToDate(_read_.ANCODVAL),cp_NullValue(_read_.ANCODVAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
          else
            if this.oParentObject.w_OLTPROVE = "L"
              this.oParentObject.w_OLTPROVE = "I"
            endif
          endif
        endif
        * --- Chiude cursore
        Use in Select("_CicloPref_")
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Operazione)
    this.Operazione=Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,34)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CON_TRAD'
    this.cWorkTables[4]='CON_TRAM'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='ODL_DETT'
    this.cWorkTables[7]='ODL_MAST'
    this.cWorkTables[8]='PAR_RIOR'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='SALDIART'
    this.cWorkTables[11]='CONTI'
    this.cWorkTables[12]='TAB_CICL'
    this.cWorkTables[13]='DIC_PROD'
    this.cWorkTables[14]='RIF_GODL'
    this.cWorkTables[15]='MAGAZZIN'
    this.cWorkTables[16]='TAB_RISO'
    this.cWorkTables[17]='MPS_TFOR'
    this.cWorkTables[18]='SALDICOM'
    this.cWorkTables[19]='RIS_ORSE'
    this.cWorkTables[20]='PAR_PROD'
    this.cWorkTables[21]='CLR_MAST'
    this.cWorkTables[22]='CIC_DETT'
    this.cWorkTables[23]='CIC_MAST'
    this.cWorkTables[24]='ODLMRISO'
    this.cWorkTables[25]='ODL_RISO'
    this.cWorkTables[26]='ODL_CICL'
    this.cWorkTables[27]='UNIMIS'
    this.cWorkTables[28]='ODL_MAIN'
    this.cWorkTables[29]='ODL_RISF'
    this.cWorkTables[30]='*TMPODL_CICL'
    this.cWorkTables[31]='*TMPODL_RISO'
    this.cWorkTables[32]='ART_TEMP'
    this.cWorkTables[33]='PEG_SELI'
    this.cWorkTables[34]='CAN_TIER'
    return(this.OpenAllTables(34))

  proc CloseCursors()
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_TMPODL_CICL')
      use in _Curs_TMPODL_CICL
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_GSCO_BOL1')
      use in _Curs_GSCO_BOL1
    endif
    if used('_Curs_TAB_CICL')
      use in _Curs_TAB_CICL
    endif
    if used('_Curs_GSCO_BOL3')
      use in _Curs_GSCO_BOL3
    endif
    if used('_Curs_GSCO_BOL7')
      use in _Curs_GSCO_BOL7
    endif
    if used('_Curs_CIC_DETT')
      use in _Curs_CIC_DETT
    endif
    if used('_Curs_CIC_DETT')
      use in _Curs_CIC_DETT
    endif
    if used('_Curs_GSCO_BOL4')
      use in _Curs_GSCO_BOL4
    endif
    if used('_Curs_PRDSETLT')
      use in _Curs_PRDSETLT
    endif
    if used('_Curs_GSCO_BOL1')
      use in _Curs_GSCO_BOL1
    endif
    if used('_Curs_GSCO_BOL2')
      use in _Curs_GSCO_BOL2
    endif
    if used('_Curs_TAB_CICL')
      use in _Curs_TAB_CICL
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_gsdb7bol')
      use in _Curs_gsdb7bol
    endif
    if used('_Curs_GSCI5BOL')
      use in _Curs_GSCI5BOL
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_ODL_RISO')
      use in _Curs_ODL_RISO
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_GSCI10BTC')
      use in _Curs_GSCI10BTC
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_TMPODL_CICL')
      use in _Curs_TMPODL_CICL
    endif
    if used('_Curs_GSCIC4BTC')
      use in _Curs_GSCIC4BTC
    endif
    if used('_Curs_GSCIC10BTC')
      use in _Curs_GSCIC10BTC
    endif
    if used('_Curs__d__d__COLA_EXE_QUERY_GSCOFBOL')
      use in _Curs__d__d__COLA_EXE_QUERY_GSCOFBOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Operazione"
endproc
