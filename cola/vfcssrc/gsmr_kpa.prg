* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_kpa                                                        *
*              Parametri messaggi MRP                                          *
*                                                                              *
*      Author: Zucchetti SpA (DB)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-11                                                      *
* Last revis.: 2009-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_kpa",oParentObject))

* --- Class definition
define class tgsmr_kpa as StdForm
  Top    = 3
  Left   = 17

  * --- Standard Properties
  Width  = 696
  Height = 381+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-17"
  HelpContextID=149515113
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=67

  * --- Constant Properties
  _IDX = 0
  PARA_MRP_IDX = 0
  cPrg = "gsmr_kpa"
  cComment = "Parametri messaggi MRP"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(2)
  o_CODICE = space(2)
  w_PPCODICE = space(10)
  w_PMPERCON = 0
  w_PMODLSUG = space(1)
  w_PMSPOSTI = space(1)
  w_PMSPO_GG = 0
  w_PMSPOCOP = space(1)
  w_PMSANNUL = space(1)
  w_PMSANTIC = space(1)
  w_PMSANCOP = space(1)
  w_PMODLPIA = space(1)
  w_PMPPOSTI = space(1)
  w_PMPPO_GG = 0
  w_PMPPOCOP = space(1)
  w_PMPANNUL = space(1)
  w_PMPANTIC = space(1)
  w_PMPANCOP = space(1)
  w_PMODLLAN = space(1)
  w_PMLPOSTI = space(1)
  w_PMLPO_GG = 0
  w_PMLCHIUD = space(1)
  w_PMOCLSUG = space(1)
  w_PMCPOSTI = space(1)
  w_PMCPO_GG = 0
  w_PMCPOCOP = space(1)
  w_PMCANNUL = space(1)
  w_PMCANTIC = space(1)
  w_PMCANCOP = space(1)
  w_PMOCLPIA = space(1)
  w_PMIPOSTI = space(1)
  w_PMIPO_GG = 0
  w_PMIPOCOP = space(1)
  w_PMIANNUL = space(1)
  w_PMIANTIC = space(1)
  w_PMIANCOP = space(1)
  w_PMOCLORD = space(1)
  w_PMOPOSTI = space(1)
  w_PMOPO_GG = 0
  w_PMOPOCOP = space(1)
  w_PMOANNUL = space(1)
  w_PMOANTIC = space(1)
  w_PMOANCOP = space(1)
  w_PMODASUG = space(1)
  w_PMMPOSTI = space(1)
  w_PMMPO_GG = 0
  w_PMMPOCOP = space(1)
  w_PMMANNUL = space(1)
  w_PMMANTIC = space(1)
  w_PMMANCOP = space(1)
  w_PMODAPIA = space(1)
  w_PMDPOSTI = space(1)
  w_PMDPO_GG = 0
  w_PMDPOCOP = space(1)
  w_PMDANNUL = space(1)
  w_PMDANTIC = space(1)
  w_PMDANCOP = space(1)
  w_PMODAORD = space(1)
  w_PMRPOSTI = space(1)
  w_PMRPO_GG = 0
  w_PMRPOCOP = space(1)
  w_PMRANNUL = space(1)
  w_PMRANTIC = space(1)
  w_PMRANCOP = space(1)
  w_CODICE = space(2)
  w_PMPERCON = 0
  w_CODICE = space(2)
  w_PMPERCON = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_kpaPag1","gsmr_kpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri ODL")
      .Pages(2).addobject("oPag","tgsmr_kpaPag2","gsmr_kpa",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri OCL")
      .Pages(3).addobject("oPag","tgsmr_kpaPag3","gsmr_kpa",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Parametri ODA")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PARA_MRP'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSMR_BPA(this,"WRITE")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(2)
      .w_PPCODICE=space(10)
      .w_PMPERCON=0
      .w_PMODLSUG=space(1)
      .w_PMSPOSTI=space(1)
      .w_PMSPO_GG=0
      .w_PMSPOCOP=space(1)
      .w_PMSANNUL=space(1)
      .w_PMSANTIC=space(1)
      .w_PMSANCOP=space(1)
      .w_PMODLPIA=space(1)
      .w_PMPPOSTI=space(1)
      .w_PMPPO_GG=0
      .w_PMPPOCOP=space(1)
      .w_PMPANNUL=space(1)
      .w_PMPANTIC=space(1)
      .w_PMPANCOP=space(1)
      .w_PMODLLAN=space(1)
      .w_PMLPOSTI=space(1)
      .w_PMLPO_GG=0
      .w_PMLCHIUD=space(1)
      .w_PMOCLSUG=space(1)
      .w_PMCPOSTI=space(1)
      .w_PMCPO_GG=0
      .w_PMCPOCOP=space(1)
      .w_PMCANNUL=space(1)
      .w_PMCANTIC=space(1)
      .w_PMCANCOP=space(1)
      .w_PMOCLPIA=space(1)
      .w_PMIPOSTI=space(1)
      .w_PMIPO_GG=0
      .w_PMIPOCOP=space(1)
      .w_PMIANNUL=space(1)
      .w_PMIANTIC=space(1)
      .w_PMIANCOP=space(1)
      .w_PMOCLORD=space(1)
      .w_PMOPOSTI=space(1)
      .w_PMOPO_GG=0
      .w_PMOPOCOP=space(1)
      .w_PMOANNUL=space(1)
      .w_PMOANTIC=space(1)
      .w_PMOANCOP=space(1)
      .w_PMODASUG=space(1)
      .w_PMMPOSTI=space(1)
      .w_PMMPO_GG=0
      .w_PMMPOCOP=space(1)
      .w_PMMANNUL=space(1)
      .w_PMMANTIC=space(1)
      .w_PMMANCOP=space(1)
      .w_PMODAPIA=space(1)
      .w_PMDPOSTI=space(1)
      .w_PMDPO_GG=0
      .w_PMDPOCOP=space(1)
      .w_PMDANNUL=space(1)
      .w_PMDANTIC=space(1)
      .w_PMDANCOP=space(1)
      .w_PMODAORD=space(1)
      .w_PMRPOSTI=space(1)
      .w_PMRPO_GG=0
      .w_PMRPOCOP=space(1)
      .w_PMRANNUL=space(1)
      .w_PMRANTIC=space(1)
      .w_PMRANCOP=space(1)
      .w_CODICE=space(2)
      .w_PMPERCON=0
      .w_CODICE=space(2)
      .w_PMPERCON=0
        .w_CODICE = iif(type("this.oParentObject")="C", this.oParentObject, "MR")
        .w_PPCODICE = .w_CODICE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,63,.f.)
        .w_CODICE = iif(type("this.oParentObject")="C", this.oParentObject, "MR")
          .DoRTCalc(65,65,.f.)
        .w_CODICE = iif(type("this.oParentObject")="C", this.oParentObject, "MR")
    endwith
    this.DoRTCalc(67,67,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODICE<>.w_CODICE
            .w_PPCODICE = .w_CODICE
          .link_1_2('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,67,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_LEFUJXCXWI()
    with this
          * --- 
          gsmr_bpa(this;
              ,"CHECK";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPMODLSUG_1_7.enabled = this.oPgFrm.Page1.oPag.oPMODLSUG_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPMSPOSTI_1_10.enabled = this.oPgFrm.Page1.oPag.oPMSPOSTI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPMSPO_GG_1_11.enabled = this.oPgFrm.Page1.oPag.oPMSPO_GG_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPMSPOCOP_1_13.enabled_(this.oPgFrm.Page1.oPag.oPMSPOCOP_1_13.mCond())
    this.oPgFrm.Page1.oPag.oPMSANNUL_1_14.enabled = this.oPgFrm.Page1.oPag.oPMSANNUL_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPMSANTIC_1_15.enabled = this.oPgFrm.Page1.oPag.oPMSANTIC_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPMSANCOP_1_16.enabled_(this.oPgFrm.Page1.oPag.oPMSANCOP_1_16.mCond())
    this.oPgFrm.Page1.oPag.oPMPPOSTI_1_20.enabled = this.oPgFrm.Page1.oPag.oPMPPOSTI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPMPPO_GG_1_21.enabled = this.oPgFrm.Page1.oPag.oPMPPO_GG_1_21.mCond()
    this.oPgFrm.Page1.oPag.oPMPPOCOP_1_23.enabled_(this.oPgFrm.Page1.oPag.oPMPPOCOP_1_23.mCond())
    this.oPgFrm.Page1.oPag.oPMPANNUL_1_24.enabled = this.oPgFrm.Page1.oPag.oPMPANNUL_1_24.mCond()
    this.oPgFrm.Page1.oPag.oPMPANTIC_1_25.enabled = this.oPgFrm.Page1.oPag.oPMPANTIC_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPMPANCOP_1_26.enabled_(this.oPgFrm.Page1.oPag.oPMPANCOP_1_26.mCond())
    this.oPgFrm.Page1.oPag.oPMLPOSTI_1_30.enabled = this.oPgFrm.Page1.oPag.oPMLPOSTI_1_30.mCond()
    this.oPgFrm.Page1.oPag.oPMLPO_GG_1_31.enabled = this.oPgFrm.Page1.oPag.oPMLPO_GG_1_31.mCond()
    this.oPgFrm.Page1.oPag.oPMLCHIUD_1_33.enabled = this.oPgFrm.Page1.oPag.oPMLCHIUD_1_33.mCond()
    this.oPgFrm.Page2.oPag.oPMOCLSUG_2_1.enabled = this.oPgFrm.Page2.oPag.oPMOCLSUG_2_1.mCond()
    this.oPgFrm.Page2.oPag.oPMCPOSTI_2_4.enabled = this.oPgFrm.Page2.oPag.oPMCPOSTI_2_4.mCond()
    this.oPgFrm.Page2.oPag.oPMCPO_GG_2_5.enabled = this.oPgFrm.Page2.oPag.oPMCPO_GG_2_5.mCond()
    this.oPgFrm.Page2.oPag.oPMCPOCOP_2_7.enabled_(this.oPgFrm.Page2.oPag.oPMCPOCOP_2_7.mCond())
    this.oPgFrm.Page2.oPag.oPMCANNUL_2_8.enabled = this.oPgFrm.Page2.oPag.oPMCANNUL_2_8.mCond()
    this.oPgFrm.Page2.oPag.oPMCANTIC_2_9.enabled = this.oPgFrm.Page2.oPag.oPMCANTIC_2_9.mCond()
    this.oPgFrm.Page2.oPag.oPMCANCOP_2_10.enabled_(this.oPgFrm.Page2.oPag.oPMCANCOP_2_10.mCond())
    this.oPgFrm.Page2.oPag.oPMIPOSTI_2_14.enabled = this.oPgFrm.Page2.oPag.oPMIPOSTI_2_14.mCond()
    this.oPgFrm.Page2.oPag.oPMIPO_GG_2_15.enabled = this.oPgFrm.Page2.oPag.oPMIPO_GG_2_15.mCond()
    this.oPgFrm.Page2.oPag.oPMIPOCOP_2_17.enabled_(this.oPgFrm.Page2.oPag.oPMIPOCOP_2_17.mCond())
    this.oPgFrm.Page2.oPag.oPMIANNUL_2_18.enabled = this.oPgFrm.Page2.oPag.oPMIANNUL_2_18.mCond()
    this.oPgFrm.Page2.oPag.oPMIANTIC_2_19.enabled = this.oPgFrm.Page2.oPag.oPMIANTIC_2_19.mCond()
    this.oPgFrm.Page2.oPag.oPMIANCOP_2_20.enabled_(this.oPgFrm.Page2.oPag.oPMIANCOP_2_20.mCond())
    this.oPgFrm.Page2.oPag.oPMOPOSTI_2_24.enabled = this.oPgFrm.Page2.oPag.oPMOPOSTI_2_24.mCond()
    this.oPgFrm.Page2.oPag.oPMOPO_GG_2_25.enabled = this.oPgFrm.Page2.oPag.oPMOPO_GG_2_25.mCond()
    this.oPgFrm.Page2.oPag.oPMOPOCOP_2_27.enabled_(this.oPgFrm.Page2.oPag.oPMOPOCOP_2_27.mCond())
    this.oPgFrm.Page2.oPag.oPMOANNUL_2_28.enabled = this.oPgFrm.Page2.oPag.oPMOANNUL_2_28.mCond()
    this.oPgFrm.Page2.oPag.oPMOANTIC_2_29.enabled = this.oPgFrm.Page2.oPag.oPMOANTIC_2_29.mCond()
    this.oPgFrm.Page2.oPag.oPMOANCOP_2_30.enabled_(this.oPgFrm.Page2.oPag.oPMOANCOP_2_30.mCond())
    this.oPgFrm.Page3.oPag.oPMODASUG_3_1.enabled = this.oPgFrm.Page3.oPag.oPMODASUG_3_1.mCond()
    this.oPgFrm.Page3.oPag.oPMMPOSTI_3_4.enabled = this.oPgFrm.Page3.oPag.oPMMPOSTI_3_4.mCond()
    this.oPgFrm.Page3.oPag.oPMMPO_GG_3_5.enabled = this.oPgFrm.Page3.oPag.oPMMPO_GG_3_5.mCond()
    this.oPgFrm.Page3.oPag.oPMMPOCOP_3_7.enabled_(this.oPgFrm.Page3.oPag.oPMMPOCOP_3_7.mCond())
    this.oPgFrm.Page3.oPag.oPMMANNUL_3_8.enabled = this.oPgFrm.Page3.oPag.oPMMANNUL_3_8.mCond()
    this.oPgFrm.Page3.oPag.oPMMANTIC_3_9.enabled = this.oPgFrm.Page3.oPag.oPMMANTIC_3_9.mCond()
    this.oPgFrm.Page3.oPag.oPMMANCOP_3_10.enabled_(this.oPgFrm.Page3.oPag.oPMMANCOP_3_10.mCond())
    this.oPgFrm.Page3.oPag.oPMDPOSTI_3_14.enabled = this.oPgFrm.Page3.oPag.oPMDPOSTI_3_14.mCond()
    this.oPgFrm.Page3.oPag.oPMDPO_GG_3_15.enabled = this.oPgFrm.Page3.oPag.oPMDPO_GG_3_15.mCond()
    this.oPgFrm.Page3.oPag.oPMDPOCOP_3_17.enabled_(this.oPgFrm.Page3.oPag.oPMDPOCOP_3_17.mCond())
    this.oPgFrm.Page3.oPag.oPMDANNUL_3_18.enabled = this.oPgFrm.Page3.oPag.oPMDANNUL_3_18.mCond()
    this.oPgFrm.Page3.oPag.oPMDANTIC_3_19.enabled = this.oPgFrm.Page3.oPag.oPMDANTIC_3_19.mCond()
    this.oPgFrm.Page3.oPag.oPMDANCOP_3_20.enabled_(this.oPgFrm.Page3.oPag.oPMDANCOP_3_20.mCond())
    this.oPgFrm.Page3.oPag.oPMRPOSTI_3_24.enabled = this.oPgFrm.Page3.oPag.oPMRPOSTI_3_24.mCond()
    this.oPgFrm.Page3.oPag.oPMRPO_GG_3_25.enabled = this.oPgFrm.Page3.oPag.oPMRPO_GG_3_25.mCond()
    this.oPgFrm.Page3.oPag.oPMRPOCOP_3_27.enabled_(this.oPgFrm.Page3.oPag.oPMRPOCOP_3_27.mCond())
    this.oPgFrm.Page3.oPag.oPMRANNUL_3_28.enabled = this.oPgFrm.Page3.oPag.oPMRANNUL_3_28.mCond()
    this.oPgFrm.Page3.oPag.oPMRANTIC_3_29.enabled = this.oPgFrm.Page3.oPag.oPMRANTIC_3_29.mCond()
    this.oPgFrm.Page3.oPag.oPMRANCOP_3_30.enabled_(this.oPgFrm.Page3.oPag.oPMRANCOP_3_30.mCond())
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARA_MRP_IDX,3]
    i_lTable = "PARA_MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2], .t., this.PARA_MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where PMCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PMCODICE',this.w_PPCODICE)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PMCODICE,space(10))
      this.w_PMPERCON = NVL(_Link_.PMPERCON,0)
      this.w_PMODLSUG = NVL(_Link_.PMODLSUG,space(1))
      this.w_PMSPOSTI = NVL(_Link_.PMSPOSTI,space(1))
      this.w_PMSPO_GG = NVL(_Link_.PMSPO_GG,0)
      this.w_PMSPOCOP = NVL(_Link_.PMSPOCOP,space(1))
      this.w_PMSANTIC = NVL(_Link_.PMSANTIC,space(1))
      this.w_PMSANCOP = NVL(_Link_.PMSANCOP,space(1))
      this.w_PMSANNUL = NVL(_Link_.PMSANNUL,space(1))
      this.w_PMODLPIA = NVL(_Link_.PMODLPIA,space(1))
      this.w_PMPPOSTI = NVL(_Link_.PMPPOSTI,space(1))
      this.w_PMPPO_GG = NVL(_Link_.PMPPO_GG,0)
      this.w_PMPPOCOP = NVL(_Link_.PMPPOCOP,space(1))
      this.w_PMPANTIC = NVL(_Link_.PMPANTIC,space(1))
      this.w_PMPANCOP = NVL(_Link_.PMPANCOP,space(1))
      this.w_PMPANNUL = NVL(_Link_.PMPANNUL,space(1))
      this.w_PMODLLAN = NVL(_Link_.PMODLLAN,space(1))
      this.w_PMLPOSTI = NVL(_Link_.PMLPOSTI,space(1))
      this.w_PMLPO_GG = NVL(_Link_.PMLPO_GG,0)
      this.w_PMLCHIUD = NVL(_Link_.PMLCHIUD,space(1))
      this.w_PMOCLSUG = NVL(_Link_.PMOCLSUG,space(1))
      this.w_PMCPOSTI = NVL(_Link_.PMCPOSTI,space(1))
      this.w_PMCPO_GG = NVL(_Link_.PMCPO_GG,0)
      this.w_PMCPOCOP = NVL(_Link_.PMCPOCOP,space(1))
      this.w_PMCANTIC = NVL(_Link_.PMCANTIC,space(1))
      this.w_PMCANCOP = NVL(_Link_.PMCANCOP,space(1))
      this.w_PMCANNUL = NVL(_Link_.PMCANNUL,space(1))
      this.w_PMOCLPIA = NVL(_Link_.PMOCLPIA,space(1))
      this.w_PMIPOSTI = NVL(_Link_.PMIPOSTI,space(1))
      this.w_PMIPO_GG = NVL(_Link_.PMIPO_GG,0)
      this.w_PMIPOCOP = NVL(_Link_.PMIPOCOP,space(1))
      this.w_PMIANTIC = NVL(_Link_.PMIANTIC,space(1))
      this.w_PMIANCOP = NVL(_Link_.PMIANCOP,space(1))
      this.w_PMIANNUL = NVL(_Link_.PMIANNUL,space(1))
      this.w_PMOCLORD = NVL(_Link_.PMOCLORD,space(1))
      this.w_PMOPOSTI = NVL(_Link_.PMOPOSTI,space(1))
      this.w_PMOPO_GG = NVL(_Link_.PMOPO_GG,0)
      this.w_PMOPOCOP = NVL(_Link_.PMOPOCOP,space(1))
      this.w_PMOANTIC = NVL(_Link_.PMOANTIC,space(1))
      this.w_PMOANCOP = NVL(_Link_.PMOANCOP,space(1))
      this.w_PMOANNUL = NVL(_Link_.PMOANNUL,space(1))
      this.w_PMODASUG = NVL(_Link_.PMODASUG,space(1))
      this.w_PMMPOSTI = NVL(_Link_.PMMPOSTI,space(1))
      this.w_PMMPO_GG = NVL(_Link_.PMMPO_GG,0)
      this.w_PMMPOCOP = NVL(_Link_.PMMPOCOP,space(1))
      this.w_PMMANTIC = NVL(_Link_.PMMANTIC,space(1))
      this.w_PMMANCOP = NVL(_Link_.PMMANCOP,space(1))
      this.w_PMMANNUL = NVL(_Link_.PMMANNUL,space(1))
      this.w_PMODAPIA = NVL(_Link_.PMODAPIA,space(1))
      this.w_PMDPOSTI = NVL(_Link_.PMDPOSTI,space(1))
      this.w_PMDPO_GG = NVL(_Link_.PMDPO_GG,0)
      this.w_PMDPOCOP = NVL(_Link_.PMDPOCOP,space(1))
      this.w_PMDANTIC = NVL(_Link_.PMDANTIC,space(1))
      this.w_PMDANCOP = NVL(_Link_.PMDANCOP,space(1))
      this.w_PMDANNUL = NVL(_Link_.PMDANNUL,space(1))
      this.w_PMODAORD = NVL(_Link_.PMODALAN,space(1))
      this.w_PMRPOSTI = NVL(_Link_.PMRPOSTI,space(1))
      this.w_PMRPO_GG = NVL(_Link_.PMRPO_GG,0)
      this.w_PMRPOCOP = NVL(_Link_.PMRPOCOP,space(1))
      this.w_PMRANTIC = NVL(_Link_.PMRANTIC,space(1))
      this.w_PMRANCOP = NVL(_Link_.PMRANCOP,space(1))
      this.w_PMRANNUL = NVL(_Link_.PMRANNUL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(10)
      endif
      this.w_PMPERCON = 0
      this.w_PMODLSUG = space(1)
      this.w_PMSPOSTI = space(1)
      this.w_PMSPO_GG = 0
      this.w_PMSPOCOP = space(1)
      this.w_PMSANTIC = space(1)
      this.w_PMSANCOP = space(1)
      this.w_PMSANNUL = space(1)
      this.w_PMODLPIA = space(1)
      this.w_PMPPOSTI = space(1)
      this.w_PMPPO_GG = 0
      this.w_PMPPOCOP = space(1)
      this.w_PMPANTIC = space(1)
      this.w_PMPANCOP = space(1)
      this.w_PMPANNUL = space(1)
      this.w_PMODLLAN = space(1)
      this.w_PMLPOSTI = space(1)
      this.w_PMLPO_GG = 0
      this.w_PMLCHIUD = space(1)
      this.w_PMOCLSUG = space(1)
      this.w_PMCPOSTI = space(1)
      this.w_PMCPO_GG = 0
      this.w_PMCPOCOP = space(1)
      this.w_PMCANTIC = space(1)
      this.w_PMCANCOP = space(1)
      this.w_PMCANNUL = space(1)
      this.w_PMOCLPIA = space(1)
      this.w_PMIPOSTI = space(1)
      this.w_PMIPO_GG = 0
      this.w_PMIPOCOP = space(1)
      this.w_PMIANTIC = space(1)
      this.w_PMIANCOP = space(1)
      this.w_PMIANNUL = space(1)
      this.w_PMOCLORD = space(1)
      this.w_PMOPOSTI = space(1)
      this.w_PMOPO_GG = 0
      this.w_PMOPOCOP = space(1)
      this.w_PMOANTIC = space(1)
      this.w_PMOANCOP = space(1)
      this.w_PMOANNUL = space(1)
      this.w_PMODASUG = space(1)
      this.w_PMMPOSTI = space(1)
      this.w_PMMPO_GG = 0
      this.w_PMMPOCOP = space(1)
      this.w_PMMANTIC = space(1)
      this.w_PMMANCOP = space(1)
      this.w_PMMANNUL = space(1)
      this.w_PMODAPIA = space(1)
      this.w_PMDPOSTI = space(1)
      this.w_PMDPO_GG = 0
      this.w_PMDPOCOP = space(1)
      this.w_PMDANTIC = space(1)
      this.w_PMDANCOP = space(1)
      this.w_PMDANNUL = space(1)
      this.w_PMODAORD = space(1)
      this.w_PMRPOSTI = space(1)
      this.w_PMRPO_GG = 0
      this.w_PMRPOCOP = space(1)
      this.w_PMRANTIC = space(1)
      this.w_PMRANCOP = space(1)
      this.w_PMRANNUL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])+'\'+cp_ToStr(_Link_.PMCODICE,1)
      cp_ShowWarn(i_cKey,this.PARA_MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_1.RadioValue()==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPERCON_1_3.value==this.w_PMPERCON)
      this.oPgFrm.Page1.oPag.oPMPERCON_1_3.value=this.w_PMPERCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPMODLSUG_1_7.RadioValue()==this.w_PMODLSUG)
      this.oPgFrm.Page1.oPag.oPMODLSUG_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMSPOSTI_1_10.RadioValue()==this.w_PMSPOSTI)
      this.oPgFrm.Page1.oPag.oPMSPOSTI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMSPO_GG_1_11.value==this.w_PMSPO_GG)
      this.oPgFrm.Page1.oPag.oPMSPO_GG_1_11.value=this.w_PMSPO_GG
    endif
    if not(this.oPgFrm.Page1.oPag.oPMSPOCOP_1_13.RadioValue()==this.w_PMSPOCOP)
      this.oPgFrm.Page1.oPag.oPMSPOCOP_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMSANNUL_1_14.RadioValue()==this.w_PMSANNUL)
      this.oPgFrm.Page1.oPag.oPMSANNUL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMSANTIC_1_15.RadioValue()==this.w_PMSANTIC)
      this.oPgFrm.Page1.oPag.oPMSANTIC_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMSANCOP_1_16.RadioValue()==this.w_PMSANCOP)
      this.oPgFrm.Page1.oPag.oPMSANCOP_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMODLPIA_1_17.RadioValue()==this.w_PMODLPIA)
      this.oPgFrm.Page1.oPag.oPMODLPIA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPPOSTI_1_20.RadioValue()==this.w_PMPPOSTI)
      this.oPgFrm.Page1.oPag.oPMPPOSTI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPPO_GG_1_21.value==this.w_PMPPO_GG)
      this.oPgFrm.Page1.oPag.oPMPPO_GG_1_21.value=this.w_PMPPO_GG
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPPOCOP_1_23.RadioValue()==this.w_PMPPOCOP)
      this.oPgFrm.Page1.oPag.oPMPPOCOP_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPANNUL_1_24.RadioValue()==this.w_PMPANNUL)
      this.oPgFrm.Page1.oPag.oPMPANNUL_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPANTIC_1_25.RadioValue()==this.w_PMPANTIC)
      this.oPgFrm.Page1.oPag.oPMPANTIC_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMPANCOP_1_26.RadioValue()==this.w_PMPANCOP)
      this.oPgFrm.Page1.oPag.oPMPANCOP_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMODLLAN_1_27.RadioValue()==this.w_PMODLLAN)
      this.oPgFrm.Page1.oPag.oPMODLLAN_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMLPOSTI_1_30.RadioValue()==this.w_PMLPOSTI)
      this.oPgFrm.Page1.oPag.oPMLPOSTI_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPMLPO_GG_1_31.value==this.w_PMLPO_GG)
      this.oPgFrm.Page1.oPag.oPMLPO_GG_1_31.value=this.w_PMLPO_GG
    endif
    if not(this.oPgFrm.Page1.oPag.oPMLCHIUD_1_33.RadioValue()==this.w_PMLCHIUD)
      this.oPgFrm.Page1.oPag.oPMLCHIUD_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOCLSUG_2_1.RadioValue()==this.w_PMOCLSUG)
      this.oPgFrm.Page2.oPag.oPMOCLSUG_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMCPOSTI_2_4.RadioValue()==this.w_PMCPOSTI)
      this.oPgFrm.Page2.oPag.oPMCPOSTI_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMCPO_GG_2_5.value==this.w_PMCPO_GG)
      this.oPgFrm.Page2.oPag.oPMCPO_GG_2_5.value=this.w_PMCPO_GG
    endif
    if not(this.oPgFrm.Page2.oPag.oPMCPOCOP_2_7.RadioValue()==this.w_PMCPOCOP)
      this.oPgFrm.Page2.oPag.oPMCPOCOP_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMCANNUL_2_8.RadioValue()==this.w_PMCANNUL)
      this.oPgFrm.Page2.oPag.oPMCANNUL_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMCANTIC_2_9.RadioValue()==this.w_PMCANTIC)
      this.oPgFrm.Page2.oPag.oPMCANTIC_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMCANCOP_2_10.RadioValue()==this.w_PMCANCOP)
      this.oPgFrm.Page2.oPag.oPMCANCOP_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOCLPIA_2_11.RadioValue()==this.w_PMOCLPIA)
      this.oPgFrm.Page2.oPag.oPMOCLPIA_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMIPOSTI_2_14.RadioValue()==this.w_PMIPOSTI)
      this.oPgFrm.Page2.oPag.oPMIPOSTI_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMIPO_GG_2_15.value==this.w_PMIPO_GG)
      this.oPgFrm.Page2.oPag.oPMIPO_GG_2_15.value=this.w_PMIPO_GG
    endif
    if not(this.oPgFrm.Page2.oPag.oPMIPOCOP_2_17.RadioValue()==this.w_PMIPOCOP)
      this.oPgFrm.Page2.oPag.oPMIPOCOP_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMIANNUL_2_18.RadioValue()==this.w_PMIANNUL)
      this.oPgFrm.Page2.oPag.oPMIANNUL_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMIANTIC_2_19.RadioValue()==this.w_PMIANTIC)
      this.oPgFrm.Page2.oPag.oPMIANTIC_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMIANCOP_2_20.RadioValue()==this.w_PMIANCOP)
      this.oPgFrm.Page2.oPag.oPMIANCOP_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOCLORD_2_21.RadioValue()==this.w_PMOCLORD)
      this.oPgFrm.Page2.oPag.oPMOCLORD_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOPOSTI_2_24.RadioValue()==this.w_PMOPOSTI)
      this.oPgFrm.Page2.oPag.oPMOPOSTI_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOPO_GG_2_25.value==this.w_PMOPO_GG)
      this.oPgFrm.Page2.oPag.oPMOPO_GG_2_25.value=this.w_PMOPO_GG
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOPOCOP_2_27.RadioValue()==this.w_PMOPOCOP)
      this.oPgFrm.Page2.oPag.oPMOPOCOP_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOANNUL_2_28.RadioValue()==this.w_PMOANNUL)
      this.oPgFrm.Page2.oPag.oPMOANNUL_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOANTIC_2_29.RadioValue()==this.w_PMOANTIC)
      this.oPgFrm.Page2.oPag.oPMOANTIC_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMOANCOP_2_30.RadioValue()==this.w_PMOANCOP)
      this.oPgFrm.Page2.oPag.oPMOANCOP_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMODASUG_3_1.RadioValue()==this.w_PMODASUG)
      this.oPgFrm.Page3.oPag.oPMODASUG_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMMPOSTI_3_4.RadioValue()==this.w_PMMPOSTI)
      this.oPgFrm.Page3.oPag.oPMMPOSTI_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMMPO_GG_3_5.value==this.w_PMMPO_GG)
      this.oPgFrm.Page3.oPag.oPMMPO_GG_3_5.value=this.w_PMMPO_GG
    endif
    if not(this.oPgFrm.Page3.oPag.oPMMPOCOP_3_7.RadioValue()==this.w_PMMPOCOP)
      this.oPgFrm.Page3.oPag.oPMMPOCOP_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMMANNUL_3_8.RadioValue()==this.w_PMMANNUL)
      this.oPgFrm.Page3.oPag.oPMMANNUL_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMMANTIC_3_9.RadioValue()==this.w_PMMANTIC)
      this.oPgFrm.Page3.oPag.oPMMANTIC_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMMANCOP_3_10.RadioValue()==this.w_PMMANCOP)
      this.oPgFrm.Page3.oPag.oPMMANCOP_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMODAPIA_3_11.RadioValue()==this.w_PMODAPIA)
      this.oPgFrm.Page3.oPag.oPMODAPIA_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMDPOSTI_3_14.RadioValue()==this.w_PMDPOSTI)
      this.oPgFrm.Page3.oPag.oPMDPOSTI_3_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMDPO_GG_3_15.value==this.w_PMDPO_GG)
      this.oPgFrm.Page3.oPag.oPMDPO_GG_3_15.value=this.w_PMDPO_GG
    endif
    if not(this.oPgFrm.Page3.oPag.oPMDPOCOP_3_17.RadioValue()==this.w_PMDPOCOP)
      this.oPgFrm.Page3.oPag.oPMDPOCOP_3_17.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMDANNUL_3_18.RadioValue()==this.w_PMDANNUL)
      this.oPgFrm.Page3.oPag.oPMDANNUL_3_18.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMDANTIC_3_19.RadioValue()==this.w_PMDANTIC)
      this.oPgFrm.Page3.oPag.oPMDANTIC_3_19.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMDANCOP_3_20.RadioValue()==this.w_PMDANCOP)
      this.oPgFrm.Page3.oPag.oPMDANCOP_3_20.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMODAORD_3_21.RadioValue()==this.w_PMODAORD)
      this.oPgFrm.Page3.oPag.oPMODAORD_3_21.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMRPOSTI_3_24.RadioValue()==this.w_PMRPOSTI)
      this.oPgFrm.Page3.oPag.oPMRPOSTI_3_24.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMRPO_GG_3_25.value==this.w_PMRPO_GG)
      this.oPgFrm.Page3.oPag.oPMRPO_GG_3_25.value=this.w_PMRPO_GG
    endif
    if not(this.oPgFrm.Page3.oPag.oPMRPOCOP_3_27.RadioValue()==this.w_PMRPOCOP)
      this.oPgFrm.Page3.oPag.oPMRPOCOP_3_27.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMRANNUL_3_28.RadioValue()==this.w_PMRANNUL)
      this.oPgFrm.Page3.oPag.oPMRANNUL_3_28.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMRANTIC_3_29.RadioValue()==this.w_PMRANTIC)
      this.oPgFrm.Page3.oPag.oPMRANTIC_3_29.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMRANCOP_3_30.RadioValue()==this.w_PMRANCOP)
      this.oPgFrm.Page3.oPag.oPMRANCOP_3_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODICE_2_33.RadioValue()==this.w_CODICE)
      this.oPgFrm.Page2.oPag.oCODICE_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPMPERCON_2_34.value==this.w_PMPERCON)
      this.oPgFrm.Page2.oPag.oPMPERCON_2_34.value=this.w_PMPERCON
    endif
    if not(this.oPgFrm.Page3.oPag.oCODICE_3_33.RadioValue()==this.w_CODICE)
      this.oPgFrm.Page3.oPag.oCODICE_3_33.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPMPERCON_3_34.value==this.w_PMPERCON)
      this.oPgFrm.Page3.oPag.oPMPERCON_3_34.value=this.w_PMPERCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_PMPERCON>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPMPERCON_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMSPO_GG>=0)  and (.w_PMODLSUG="S" and .w_PMSPOSTI="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPMSPO_GG_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMPPO_GG>=0)  and (.w_PMODLPIA="S" and .w_PMPPOSTI="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPMPPO_GG_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMLPO_GG>=0)  and (.w_PMODLLAN="S" and .w_PMLPOSTI="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPMLPO_GG_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMMPO_GG>=0)  and (.w_PMOCLSUG="S" and .w_PMCPOSTI="S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPMCPO_GG_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMIPO_GG>=0)  and (.w_PMOCLPIA="S" and .w_PMIPOSTI="S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPMIPO_GG_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMOPO_GG>=0)  and (.w_PMOCLORD="S" and .w_PMOPOSTI="S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPMOPO_GG_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMMPO_GG>=0)  and (.w_PMODASUG="S" and .w_PMMPOSTI="S")
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPMMPO_GG_3_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMDPO_GG>=0)  and (.w_PMODAPIA="S" and .w_PMDPOSTI="S")
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPMDPO_GG_3_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PMRPO_GG>=0)  and (.w_PMODAORD="S" and .w_PMRPOSTI="S")
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPMRPO_GG_3_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      this.Calculate_LEFUJXCXWI()
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    return

enddefine

* --- Define pages as container
define class tgsmr_kpaPag1 as StdContainer
  Width  = 692
  height = 381
  stdWidth  = 692
  stdheight = 381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCODICE_1_1 as StdCombo with uid="LFTFBNYFIR",rtseq=1,rtrep=.f.,left=165,top=35,width=146,height=21;
    , ToolTipText = "Seleziona le impostazioni dei parametri";
    , HelpContextID = 258725850;
    , cFormVar="w_CODICE",RowSource=""+"Interni MRP,"+"Esterni MRP", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODICE_1_1.RadioValue()
    return(iif(this.value =1,"MR",;
    iif(this.value =2,"EX",;
    space(2))))
  endfunc
  func oCODICE_1_1.GetRadio()
    this.Parent.oContained.w_CODICE = this.RadioValue()
    return .t.
  endfunc

  func oCODICE_1_1.SetRadio()
    this.Parent.oContained.w_CODICE=trim(this.Parent.oContained.w_CODICE)
    this.value = ;
      iif(this.Parent.oContained.w_CODICE=="MR",1,;
      iif(this.Parent.oContained.w_CODICE=="EX",2,;
      0))
  endfunc

  add object oPMPERCON_1_3 as StdField with uid="XCYGGJMNWG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PMPERCON", cQueryName = "PMPERCON",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Periodo di congelamento",;
    HelpContextID = 8329404,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=164, Top=61, cSayPict='"999"', cGetPict='"999"'

  func oPMPERCON_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMPERCON>=0)
    endwith
    return bRes
  endfunc

  add object oPMODLSUG_1_7 as StdCheck with uid="UOFNKQUTAK",rtseq=4,rtrep=.f.,left=144, top=119, caption="ODL suggeriti",;
    ToolTipText = "Abilita messagi su ODL suggeriti",;
    HelpContextID = 253744957,;
    cFormVar="w_PMODLSUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMODLSUG_1_7.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMODLSUG_1_7.GetRadio()
    this.Parent.oContained.w_PMODLSUG = this.RadioValue()
    return .t.
  endfunc

  func oPMODLSUG_1_7.SetRadio()
    this.Parent.oContained.w_PMODLSUG=trim(this.Parent.oContained.w_PMODLSUG)
    this.value = ;
      iif(this.Parent.oContained.w_PMODLSUG=="S",1,;
      0)
  endfunc

  func oPMODLSUG_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODICE="EX")
    endwith
   endif
  endfunc

  add object oPMSPOSTI_1_10 as StdCheck with uid="CIIUFIFOND",rtseq=5,rtrep=.f.,left=17, top=144, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257693503,;
    cFormVar="w_PMSPOSTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMSPOSTI_1_10.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMSPOSTI_1_10.GetRadio()
    this.Parent.oContained.w_PMSPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMSPOSTI_1_10.SetRadio()
    this.Parent.oContained.w_PMSPOSTI=trim(this.Parent.oContained.w_PMSPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMSPOSTI=="S",1,;
      0)
  endfunc

  func oPMSPOSTI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLSUG="S")
    endwith
   endif
  endfunc

  add object oPMSPO_GG_1_11 as StdField with uid="NLNSBNYYVK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PMSPO_GG", cQueryName = "PMSPO_GG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190584637,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=117, Top=146, cSayPict='"99"', cGetPict='"99"'

  func oPMSPO_GG_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLSUG="S" and .w_PMSPOSTI="S")
    endwith
   endif
  endfunc

  func oPMSPO_GG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMSPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMSPOCOP_1_13 as StdRadio with uid="ZLPORVBHAP",rtseq=7,rtrep=.f.,left=179, top=144, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMSPOCOP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPMSPOCOP_1_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10741946
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10741946
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMSPOCOP_1_13.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMSPOCOP_1_13.GetRadio()
    this.Parent.oContained.w_PMSPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMSPOCOP_1_13.SetRadio()
    this.Parent.oContained.w_PMSPOCOP=trim(this.Parent.oContained.w_PMSPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMSPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMSPOCOP=="N",2,;
      0))
  endfunc

  func oPMSPOCOP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLSUG="S" and .w_PMSPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMSANNUL_1_14 as StdCheck with uid="ZSMREPIZQJ",rtseq=8,rtrep=.f.,left=17, top=169, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171775810,;
    cFormVar="w_PMSANNUL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMSANNUL_1_14.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMSANNUL_1_14.GetRadio()
    this.Parent.oContained.w_PMSANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMSANNUL_1_14.SetRadio()
    this.Parent.oContained.w_PMSANNUL=trim(this.Parent.oContained.w_PMSANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMSANNUL=="S",1,;
      0)
  endfunc

  func oPMSANNUL_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLSUG="S")
    endwith
   endif
  endfunc

  add object oPMSANTIC_1_15 as StdCheck with uid="WSYKDVSJBN",rtseq=9,rtrep=.f.,left=17, top=196, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 4003641,;
    cFormVar="w_PMSANTIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMSANTIC_1_15.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMSANTIC_1_15.GetRadio()
    this.Parent.oContained.w_PMSANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMSANTIC_1_15.SetRadio()
    this.Parent.oContained.w_PMSANTIC=trim(this.Parent.oContained.w_PMSANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMSANTIC=="S",1,;
      0)
  endfunc

  func oPMSANTIC_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLSUG="S")
    endwith
   endif
  endfunc

  add object oPMSANCOP_1_16 as StdRadio with uid="SAWOIGIUMC",rtseq=10,rtrep=.f.,left=179, top=196, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMSANCOP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPMSANCOP_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12773562
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12773562
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMSANCOP_1_16.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMSANCOP_1_16.GetRadio()
    this.Parent.oContained.w_PMSANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMSANCOP_1_16.SetRadio()
    this.Parent.oContained.w_PMSANCOP=trim(this.Parent.oContained.w_PMSANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMSANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMSANCOP=="N",2,;
      0))
  endfunc

  func oPMSANCOP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLSUG="S" and .w_PMSANTIC="S")
    endwith
   endif
  endfunc

  add object oPMODLPIA_1_17 as StdCheck with uid="CILPCUXYBR",rtseq=11,rtrep=.f.,left=144, top=233, caption="ODL pianificati",;
    ToolTipText = "Abilita messagi su ODL pianificati",;
    HelpContextID = 203413303,;
    cFormVar="w_PMODLPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMODLPIA_1_17.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMODLPIA_1_17.GetRadio()
    this.Parent.oContained.w_PMODLPIA = this.RadioValue()
    return .t.
  endfunc

  func oPMODLPIA_1_17.SetRadio()
    this.Parent.oContained.w_PMODLPIA=trim(this.Parent.oContained.w_PMODLPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PMODLPIA=="S",1,;
      0)
  endfunc

  add object oPMPPOSTI_1_20 as StdCheck with uid="DGFYWUWOSD",rtseq=12,rtrep=.f.,left=17, top=258, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257681215,;
    cFormVar="w_PMPPOSTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMPPOSTI_1_20.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMPPOSTI_1_20.GetRadio()
    this.Parent.oContained.w_PMPPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMPPOSTI_1_20.SetRadio()
    this.Parent.oContained.w_PMPPOSTI=trim(this.Parent.oContained.w_PMPPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMPPOSTI=="S",1,;
      0)
  endfunc

  func oPMPPOSTI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLPIA="S")
    endwith
   endif
  endfunc

  add object oPMPPO_GG_1_21 as StdField with uid="UNMNLNDNXK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PMPPO_GG", cQueryName = "PMPPO_GG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190572349,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=120, Top=260, cSayPict='"99"', cGetPict='"99"'

  func oPMPPO_GG_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLPIA="S" and .w_PMPPOSTI="S")
    endwith
   endif
  endfunc

  func oPMPPO_GG_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMPPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMPPOCOP_1_23 as StdRadio with uid="ZUGFPEOGND",rtseq=14,rtrep=.f.,left=179, top=258, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMPPOCOP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPMPPOCOP_1_23.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10754234
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10754234
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMPPOCOP_1_23.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMPPOCOP_1_23.GetRadio()
    this.Parent.oContained.w_PMPPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMPPOCOP_1_23.SetRadio()
    this.Parent.oContained.w_PMPPOCOP=trim(this.Parent.oContained.w_PMPPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMPPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMPPOCOP=="N",2,;
      0))
  endfunc

  func oPMPPOCOP_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLPIA="S" and .w_PMPPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMPANNUL_1_24 as StdCheck with uid="AIUATFTBWF",rtseq=15,rtrep=.f.,left=17, top=285, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171763522,;
    cFormVar="w_PMPANNUL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMPANNUL_1_24.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMPANNUL_1_24.GetRadio()
    this.Parent.oContained.w_PMPANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMPANNUL_1_24.SetRadio()
    this.Parent.oContained.w_PMPANNUL=trim(this.Parent.oContained.w_PMPANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMPANNUL=="S",1,;
      0)
  endfunc

  func oPMPANNUL_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLPIA="S")
    endwith
   endif
  endfunc

  add object oPMPANTIC_1_25 as StdCheck with uid="LIXBCMMSFK",rtseq=16,rtrep=.f.,left=17, top=312, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3991353,;
    cFormVar="w_PMPANTIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMPANTIC_1_25.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMPANTIC_1_25.GetRadio()
    this.Parent.oContained.w_PMPANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMPANTIC_1_25.SetRadio()
    this.Parent.oContained.w_PMPANTIC=trim(this.Parent.oContained.w_PMPANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMPANTIC=="S",1,;
      0)
  endfunc

  func oPMPANTIC_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLPIA="S")
    endwith
   endif
  endfunc

  add object oPMPANCOP_1_26 as StdRadio with uid="JBYKVNSALE",rtseq=17,rtrep=.f.,left=181, top=309, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMPANCOP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPMPANCOP_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12785850
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12785850
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMPANCOP_1_26.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMPANCOP_1_26.GetRadio()
    this.Parent.oContained.w_PMPANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMPANCOP_1_26.SetRadio()
    this.Parent.oContained.w_PMPANCOP=trim(this.Parent.oContained.w_PMPANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMPANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMPANCOP=="N",2,;
      0))
  endfunc

  func oPMPANCOP_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLPIA="S" and .w_PMPANTIC='S')
    endwith
   endif
  endfunc

  add object oPMODLLAN_1_27 as StdCheck with uid="HSOUXZBLIN",rtseq=18,rtrep=.f.,left=494, top=119, caption="ODL lanciati",;
    ToolTipText = "Abilita messagi su ODL lanciati",;
    HelpContextID = 132131004,;
    cFormVar="w_PMODLLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMODLLAN_1_27.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMODLLAN_1_27.GetRadio()
    this.Parent.oContained.w_PMODLLAN = this.RadioValue()
    return .t.
  endfunc

  func oPMODLLAN_1_27.SetRadio()
    this.Parent.oContained.w_PMODLLAN=trim(this.Parent.oContained.w_PMODLLAN)
    this.value = ;
      iif(this.Parent.oContained.w_PMODLLAN=="S",1,;
      0)
  endfunc

  add object oPMLPOSTI_1_30 as StdCheck with uid="DANDKTKWWW",rtseq=19,rtrep=.f.,left=367, top=144, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257664831,;
    cFormVar="w_PMLPOSTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMLPOSTI_1_30.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMLPOSTI_1_30.GetRadio()
    this.Parent.oContained.w_PMLPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMLPOSTI_1_30.SetRadio()
    this.Parent.oContained.w_PMLPOSTI=trim(this.Parent.oContained.w_PMLPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMLPOSTI=="S",1,;
      0)
  endfunc

  func oPMLPOSTI_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLLAN="S")
    endwith
   endif
  endfunc

  add object oPMLPO_GG_1_31 as StdField with uid="TOUDUNXOYL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PMLPO_GG", cQueryName = "PMLPO_GG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190555965,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=465, Top=146, cSayPict='"99"', cGetPict='"99"'

  func oPMLPO_GG_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLLAN="S" and .w_PMLPOSTI="S")
    endwith
   endif
  endfunc

  func oPMLPO_GG_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMLPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMLCHIUD_1_33 as StdCheck with uid="EPRUISEBMY",rtseq=21,rtrep=.f.,left=367, top=169, caption="Chiudere",;
    ToolTipText = "Abilita messaggio chiudere",;
    HelpContextID = 81700666,;
    cFormVar="w_PMLCHIUD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPMLCHIUD_1_33.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMLCHIUD_1_33.GetRadio()
    this.Parent.oContained.w_PMLCHIUD = this.RadioValue()
    return .t.
  endfunc

  func oPMLCHIUD_1_33.SetRadio()
    this.Parent.oContained.w_PMLCHIUD=trim(this.Parent.oContained.w_PMLCHIUD)
    this.value = ;
      iif(this.Parent.oContained.w_PMLCHIUD=="S",1,;
      0)
  endfunc

  func oPMLCHIUD_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODLLAN="S")
    endwith
   endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="WMNAKJHMJE",left=580, top=327, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Aggiorna i parametri";
    , HelpContextID = 58569194;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSMR_BPA(this.Parent.oContained,"WRITE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_36 as StdButton with uid="ERYVBFDDSW",left=632, top=327, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58569194;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="XHEPPDEHWP",Visible=.t., Left=16, Top=64,;
    Alignment=1, Width=144, Height=18,;
    Caption="Periodo di congelamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EYCKKHWWKU",Visible=.t., Left=205, Top=64,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="WLXEHXJKDJ",Visible=.t., Left=17, Top=122,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="MXUDCPVNYW",Visible=.t., Left=154, Top=148,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KMWCJSJTOD",Visible=.t., Left=17, Top=236,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ESBVJCOBBU",Visible=.t., Left=156, Top=262,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FEDEVNLBRH",Visible=.t., Left=367, Top=122,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="QIIGYZFMVS",Visible=.t., Left=505, Top=148,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TIONAIVQPA",Visible=.t., Left=27, Top=39,;
    Alignment=1, Width=133, Height=17,;
    Caption="Parametri messaggi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="OHTTXTIDNB",Visible=.t., Left=17, Top=6,;
    Alignment=0, Width=126, Height=18,;
    Caption="Parametri generali"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BPWTASOJUY",Visible=.t., Left=17, Top=92,;
    Alignment=0, Width=105, Height=18,;
    Caption="Parametri ODL"  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="ZFPBAJHJQA",left=8, top=141, width=327,height=1

  add object oBox_1_19 as StdBox with uid="LCQHIPHKVK",left=8, top=255, width=327,height=1

  add object oBox_1_29 as StdBox with uid="TDAGIULQXK",left=358, top=141, width=232,height=1

  add object oBox_1_39 as StdBox with uid="FEIRZILWNC",left=8, top=25, width=677,height=1

  add object oBox_1_40 as StdBox with uid="BGAMSYPCFM",left=8, top=111, width=677,height=1
enddefine
define class tgsmr_kpaPag2 as StdContainer
  Width  = 692
  height = 381
  stdWidth  = 692
  stdheight = 381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPMOCLSUG_2_1 as StdCheck with uid="ZDYSPPHFVO",rtseq=22,rtrep=.f.,left=144, top=119, caption="OCL suggeriti",;
    ToolTipText = "Abilita messagi su OCL suggeriti",;
    HelpContextID = 253679421,;
    cFormVar="w_PMOCLSUG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMOCLSUG_2_1.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMOCLSUG_2_1.GetRadio()
    this.Parent.oContained.w_PMOCLSUG = this.RadioValue()
    return .t.
  endfunc

  func oPMOCLSUG_2_1.SetRadio()
    this.Parent.oContained.w_PMOCLSUG=trim(this.Parent.oContained.w_PMOCLSUG)
    this.value = ;
      iif(this.Parent.oContained.w_PMOCLSUG=="S",1,;
      0)
  endfunc

  func oPMOCLSUG_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODICE="EX")
    endwith
   endif
  endfunc

  add object oPMCPOSTI_2_4 as StdCheck with uid="HACHMCSBFA",rtseq=23,rtrep=.f.,left=17, top=144, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257627967,;
    cFormVar="w_PMCPOSTI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMCPOSTI_2_4.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMCPOSTI_2_4.GetRadio()
    this.Parent.oContained.w_PMCPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMCPOSTI_2_4.SetRadio()
    this.Parent.oContained.w_PMCPOSTI=trim(this.Parent.oContained.w_PMCPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMCPOSTI=="S",1,;
      0)
  endfunc

  func oPMCPOSTI_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLSUG="S")
    endwith
   endif
  endfunc

  add object oPMCPO_GG_2_5 as StdField with uid="ZNTFPEWXZR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PMCPO_GG", cQueryName = "PMCPO_GG",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190519101,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=117, Top=146, cSayPict='"99"', cGetPict='"99"'

  func oPMCPO_GG_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLSUG="S" and .w_PMCPOSTI="S")
    endwith
   endif
  endfunc

  func oPMCPO_GG_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMMPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMCPOCOP_2_7 as StdRadio with uid="CGGYRYAQIT",rtseq=25,rtrep=.f.,left=179, top=144, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMCPOCOP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPMCPOCOP_2_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10807482
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10807482
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMCPOCOP_2_7.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMCPOCOP_2_7.GetRadio()
    this.Parent.oContained.w_PMCPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMCPOCOP_2_7.SetRadio()
    this.Parent.oContained.w_PMCPOCOP=trim(this.Parent.oContained.w_PMCPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMCPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMCPOCOP=="N",2,;
      0))
  endfunc

  func oPMCPOCOP_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLSUG="S" and .w_PMCPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMCANNUL_2_8 as StdCheck with uid="MFOQXJJSNY",rtseq=26,rtrep=.f.,left=17, top=169, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171710274,;
    cFormVar="w_PMCANNUL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMCANNUL_2_8.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMCANNUL_2_8.GetRadio()
    this.Parent.oContained.w_PMCANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMCANNUL_2_8.SetRadio()
    this.Parent.oContained.w_PMCANNUL=trim(this.Parent.oContained.w_PMCANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMCANNUL=="S",1,;
      0)
  endfunc

  func oPMCANNUL_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLSUG="S")
    endwith
   endif
  endfunc

  add object oPMCANTIC_2_9 as StdCheck with uid="WUFKYCOFNQ",rtseq=27,rtrep=.f.,left=17, top=196, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3938105,;
    cFormVar="w_PMCANTIC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMCANTIC_2_9.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMCANTIC_2_9.GetRadio()
    this.Parent.oContained.w_PMCANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMCANTIC_2_9.SetRadio()
    this.Parent.oContained.w_PMCANTIC=trim(this.Parent.oContained.w_PMCANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMCANTIC=="S",1,;
      0)
  endfunc

  func oPMCANTIC_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLSUG="S")
    endwith
   endif
  endfunc

  add object oPMCANCOP_2_10 as StdRadio with uid="DWPHVNPSQQ",rtseq=28,rtrep=.f.,left=179, top=196, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMCANCOP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPMCANCOP_2_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12839098
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12839098
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMCANCOP_2_10.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMCANCOP_2_10.GetRadio()
    this.Parent.oContained.w_PMCANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMCANCOP_2_10.SetRadio()
    this.Parent.oContained.w_PMCANCOP=trim(this.Parent.oContained.w_PMCANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMCANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMCANCOP=="N",2,;
      0))
  endfunc

  func oPMCANCOP_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLSUG="S" and .w_PMCANTIC="S")
    endwith
   endif
  endfunc

  add object oPMOCLPIA_2_11 as StdCheck with uid="TGFLYHCGSD",rtseq=29,rtrep=.f.,left=144, top=233, caption="OCL da ordinare",;
    ToolTipText = "Abilita messagi su OCL da ordinare",;
    HelpContextID = 203347767,;
    cFormVar="w_PMOCLPIA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMOCLPIA_2_11.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMOCLPIA_2_11.GetRadio()
    this.Parent.oContained.w_PMOCLPIA = this.RadioValue()
    return .t.
  endfunc

  func oPMOCLPIA_2_11.SetRadio()
    this.Parent.oContained.w_PMOCLPIA=trim(this.Parent.oContained.w_PMOCLPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PMOCLPIA=="S",1,;
      0)
  endfunc

  add object oPMIPOSTI_2_14 as StdCheck with uid="ZRUXZBTSTK",rtseq=30,rtrep=.f.,left=17, top=258, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257652543,;
    cFormVar="w_PMIPOSTI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMIPOSTI_2_14.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMIPOSTI_2_14.GetRadio()
    this.Parent.oContained.w_PMIPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMIPOSTI_2_14.SetRadio()
    this.Parent.oContained.w_PMIPOSTI=trim(this.Parent.oContained.w_PMIPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMIPOSTI=="S",1,;
      0)
  endfunc

  func oPMIPOSTI_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLPIA="S")
    endwith
   endif
  endfunc

  add object oPMIPO_GG_2_15 as StdField with uid="PLHSUSPWHG",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PMIPO_GG", cQueryName = "PMIPO_GG",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190543677,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=120, Top=260, cSayPict='"99"', cGetPict='"99"'

  func oPMIPO_GG_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLPIA="S" and .w_PMIPOSTI="S")
    endwith
   endif
  endfunc

  func oPMIPO_GG_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMIPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMIPOCOP_2_17 as StdRadio with uid="HUJMSVMELP",rtseq=32,rtrep=.f.,left=179, top=258, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMIPOCOP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPMIPOCOP_2_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10782906
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10782906
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMIPOCOP_2_17.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMIPOCOP_2_17.GetRadio()
    this.Parent.oContained.w_PMIPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMIPOCOP_2_17.SetRadio()
    this.Parent.oContained.w_PMIPOCOP=trim(this.Parent.oContained.w_PMIPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMIPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMIPOCOP=="N",2,;
      0))
  endfunc

  func oPMIPOCOP_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLPIA="S" and .w_PMIPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMIANNUL_2_18 as StdCheck with uid="WDZYUPUSKD",rtseq=33,rtrep=.f.,left=17, top=285, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171734850,;
    cFormVar="w_PMIANNUL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMIANNUL_2_18.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMIANNUL_2_18.GetRadio()
    this.Parent.oContained.w_PMIANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMIANNUL_2_18.SetRadio()
    this.Parent.oContained.w_PMIANNUL=trim(this.Parent.oContained.w_PMIANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMIANNUL=="S",1,;
      0)
  endfunc

  func oPMIANNUL_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLPIA="S")
    endwith
   endif
  endfunc

  add object oPMIANTIC_2_19 as StdCheck with uid="EIQYZJLIGX",rtseq=34,rtrep=.f.,left=17, top=312, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3962681,;
    cFormVar="w_PMIANTIC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMIANTIC_2_19.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMIANTIC_2_19.GetRadio()
    this.Parent.oContained.w_PMIANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMIANTIC_2_19.SetRadio()
    this.Parent.oContained.w_PMIANTIC=trim(this.Parent.oContained.w_PMIANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMIANTIC=="S",1,;
      0)
  endfunc

  func oPMIANTIC_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLPIA="S")
    endwith
   endif
  endfunc

  add object oPMIANCOP_2_20 as StdRadio with uid="ORBVGNRKMH",rtseq=35,rtrep=.f.,left=181, top=309, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMIANCOP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPMIANCOP_2_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12814522
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12814522
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMIANCOP_2_20.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMIANCOP_2_20.GetRadio()
    this.Parent.oContained.w_PMIANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMIANCOP_2_20.SetRadio()
    this.Parent.oContained.w_PMIANCOP=trim(this.Parent.oContained.w_PMIANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMIANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMIANCOP=="N",2,;
      0))
  endfunc

  func oPMIANCOP_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLPIA="S" and .w_PMIANTIC='S')
    endwith
   endif
  endfunc

  add object oPMOCLORD_2_21 as StdCheck with uid="LMFHWSSPGN",rtseq=36,rtrep=.f.,left=499, top=119, caption="OCL ordinati",;
    ToolTipText = "Abilita messagi su OCL ordinati",;
    HelpContextID = 81864902,;
    cFormVar="w_PMOCLORD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMOCLORD_2_21.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMOCLORD_2_21.GetRadio()
    this.Parent.oContained.w_PMOCLORD = this.RadioValue()
    return .t.
  endfunc

  func oPMOCLORD_2_21.SetRadio()
    this.Parent.oContained.w_PMOCLORD=trim(this.Parent.oContained.w_PMOCLORD)
    this.value = ;
      iif(this.Parent.oContained.w_PMOCLORD=="S",1,;
      0)
  endfunc

  add object oPMOPOSTI_2_24 as StdCheck with uid="ABYNGPNBWU",rtseq=37,rtrep=.f.,left=372, top=144, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257677119,;
    cFormVar="w_PMOPOSTI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMOPOSTI_2_24.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMOPOSTI_2_24.GetRadio()
    this.Parent.oContained.w_PMOPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMOPOSTI_2_24.SetRadio()
    this.Parent.oContained.w_PMOPOSTI=trim(this.Parent.oContained.w_PMOPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMOPOSTI=="S",1,;
      0)
  endfunc

  func oPMOPOSTI_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLORD="S")
    endwith
   endif
  endfunc

  add object oPMOPO_GG_2_25 as StdField with uid="YXMLUFEVNQ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_PMOPO_GG", cQueryName = "PMOPO_GG",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190568253,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=472, Top=146, cSayPict='"99"', cGetPict='"99"'

  func oPMOPO_GG_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLORD="S" and .w_PMOPOSTI="S")
    endwith
   endif
  endfunc

  func oPMOPO_GG_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMOPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMOPOCOP_2_27 as StdRadio with uid="UNYIYASWEN",rtseq=39,rtrep=.f.,left=531, top=144, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMOPOCOP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPMOPOCOP_2_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10758330
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10758330
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMOPOCOP_2_27.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMOPOCOP_2_27.GetRadio()
    this.Parent.oContained.w_PMOPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMOPOCOP_2_27.SetRadio()
    this.Parent.oContained.w_PMOPOCOP=trim(this.Parent.oContained.w_PMOPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMOPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMOPOCOP=="N",2,;
      0))
  endfunc

  func oPMOPOCOP_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLORD="S" and .w_PMOPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMOANNUL_2_28 as StdCheck with uid="JHYXLWUAVN",rtseq=40,rtrep=.f.,left=372, top=169, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171759426,;
    cFormVar="w_PMOANNUL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMOANNUL_2_28.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMOANNUL_2_28.GetRadio()
    this.Parent.oContained.w_PMOANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMOANNUL_2_28.SetRadio()
    this.Parent.oContained.w_PMOANNUL=trim(this.Parent.oContained.w_PMOANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMOANNUL=="S",1,;
      0)
  endfunc

  func oPMOANNUL_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLORD="S")
    endwith
   endif
  endfunc

  add object oPMOANTIC_2_29 as StdCheck with uid="PNZVAYRBMA",rtseq=41,rtrep=.f.,left=372, top=196, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3987257,;
    cFormVar="w_PMOANTIC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPMOANTIC_2_29.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMOANTIC_2_29.GetRadio()
    this.Parent.oContained.w_PMOANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMOANTIC_2_29.SetRadio()
    this.Parent.oContained.w_PMOANTIC=trim(this.Parent.oContained.w_PMOANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMOANTIC=="S",1,;
      0)
  endfunc

  func oPMOANTIC_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLORD="S")
    endwith
   endif
  endfunc

  add object oPMOANCOP_2_30 as StdRadio with uid="HQUBAOVFEQ",rtseq=42,rtrep=.f.,left=531, top=196, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMOANCOP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPMOANCOP_2_30.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12789946
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12789946
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMOANCOP_2_30.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMOANCOP_2_30.GetRadio()
    this.Parent.oContained.w_PMOANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMOANCOP_2_30.SetRadio()
    this.Parent.oContained.w_PMOANCOP=trim(this.Parent.oContained.w_PMOANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMOANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMOANCOP=="N",2,;
      0))
  endfunc

  func oPMOANCOP_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMOCLORD="S" and .w_PMOANTIC="S")
    endwith
   endif
  endfunc


  add object oCODICE_2_33 as StdCombo with uid="GLXEITOITU",rtseq=64,rtrep=.f.,left=165,top=35,width=146,height=21, enabled=.f.;
    , ToolTipText = "Seleziona le impostazioni dei parametri";
    , HelpContextID = 258725850;
    , cFormVar="w_CODICE",RowSource=""+"Interni MRP,"+"Esterni MRP", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCODICE_2_33.RadioValue()
    return(iif(this.value =1,"MR",;
    iif(this.value =2,"EX",;
    space(2))))
  endfunc
  func oCODICE_2_33.GetRadio()
    this.Parent.oContained.w_CODICE = this.RadioValue()
    return .t.
  endfunc

  func oCODICE_2_33.SetRadio()
    this.Parent.oContained.w_CODICE=trim(this.Parent.oContained.w_CODICE)
    this.value = ;
      iif(this.Parent.oContained.w_CODICE=="MR",1,;
      iif(this.Parent.oContained.w_CODICE=="EX",2,;
      0))
  endfunc

  add object oPMPERCON_2_34 as StdField with uid="LDFDAIZYNU",rtseq=65,rtrep=.f.,;
    cFormVar = "w_PMPERCON", cQueryName = "PMPERCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Periodo di congelamento",;
    HelpContextID = 8329404,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=164, Top=61, cSayPict='"999"', cGetPict='"999"'

  add object oStr_2_2 as StdString with uid="WBAMAXEYEB",Visible=.t., Left=17, Top=122,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="GKVUHFPSZY",Visible=.t., Left=154, Top=148,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="NYMHTZQBDB",Visible=.t., Left=17, Top=236,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="IJIWYRVOUV",Visible=.t., Left=156, Top=262,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="DMEJNJKYKR",Visible=.t., Left=372, Top=122,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="IQQCIHGPBP",Visible=.t., Left=508, Top=148,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="LDIPOYFWDF",Visible=.t., Left=17, Top=92,;
    Alignment=0, Width=105, Height=18,;
    Caption="Parametri OCL"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="XBQFHINBIF",Visible=.t., Left=16, Top=64,;
    Alignment=1, Width=144, Height=18,;
    Caption="Periodo di congelamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="MMOMYNKSDX",Visible=.t., Left=205, Top=64,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="HONSJAJHGT",Visible=.t., Left=27, Top=39,;
    Alignment=1, Width=133, Height=17,;
    Caption="Parametri messaggi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="HWEFCXTMHG",Visible=.t., Left=17, Top=6,;
    Alignment=0, Width=126, Height=18,;
    Caption="Parametri generali"  ;
  , bGlobalFont=.t.

  add object oBox_2_3 as StdBox with uid="UYAUJXXGDV",left=8, top=141, width=327,height=1

  add object oBox_2_13 as StdBox with uid="NKEBEIXNGX",left=8, top=255, width=327,height=1

  add object oBox_2_23 as StdBox with uid="ETGYWFJZZK",left=358, top=141, width=327,height=1

  add object oBox_2_32 as StdBox with uid="HLRDEPPNOS",left=8, top=111, width=677,height=1

  add object oBox_2_39 as StdBox with uid="MQMSLFTUJE",left=8, top=25, width=677,height=1
enddefine
define class tgsmr_kpaPag3 as StdContainer
  Width  = 692
  height = 381
  stdWidth  = 692
  stdheight = 381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPMODASUG_3_1 as StdCheck with uid="ZBLUKSTIWN",rtseq=43,rtrep=.f.,left=144, top=119, caption="ODA suggeriti",;
    ToolTipText = "Abilita messagi su ODA suggeriti",;
    HelpContextID = 242210621,;
    cFormVar="w_PMODASUG", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMODASUG_3_1.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMODASUG_3_1.GetRadio()
    this.Parent.oContained.w_PMODASUG = this.RadioValue()
    return .t.
  endfunc

  func oPMODASUG_3_1.SetRadio()
    this.Parent.oContained.w_PMODASUG=trim(this.Parent.oContained.w_PMODASUG)
    this.value = ;
      iif(this.Parent.oContained.w_PMODASUG=="S",1,;
      0)
  endfunc

  func oPMODASUG_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODICE="EX")
    endwith
   endif
  endfunc

  add object oPMMPOSTI_3_4 as StdCheck with uid="PHORVYIGMA",rtseq=44,rtrep=.f.,left=17, top=144, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257668927,;
    cFormVar="w_PMMPOSTI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMMPOSTI_3_4.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMMPOSTI_3_4.GetRadio()
    this.Parent.oContained.w_PMMPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMMPOSTI_3_4.SetRadio()
    this.Parent.oContained.w_PMMPOSTI=trim(this.Parent.oContained.w_PMMPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMMPOSTI=="S",1,;
      0)
  endfunc

  func oPMMPOSTI_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODASUG="S")
    endwith
   endif
  endfunc

  add object oPMMPO_GG_3_5 as StdField with uid="SBKCOKDSSM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_PMMPO_GG", cQueryName = "PMMPO_GG",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190560061,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=117, Top=146, cSayPict='"99"', cGetPict='"99"'

  func oPMMPO_GG_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODASUG="S" and .w_PMMPOSTI="S")
    endwith
   endif
  endfunc

  func oPMMPO_GG_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMMPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMMPOCOP_3_7 as StdRadio with uid="ASXZYVSIYQ",rtseq=46,rtrep=.f.,left=179, top=144, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMMPOCOP", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPMMPOCOP_3_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10766522
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10766522
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMMPOCOP_3_7.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMMPOCOP_3_7.GetRadio()
    this.Parent.oContained.w_PMMPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMMPOCOP_3_7.SetRadio()
    this.Parent.oContained.w_PMMPOCOP=trim(this.Parent.oContained.w_PMMPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMMPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMMPOCOP=="N",2,;
      0))
  endfunc

  func oPMMPOCOP_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODASUG="S" and .w_PMMPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMMANNUL_3_8 as StdCheck with uid="BPULUCFTBH",rtseq=47,rtrep=.f.,left=17, top=169, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171751234,;
    cFormVar="w_PMMANNUL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMMANNUL_3_8.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMMANNUL_3_8.GetRadio()
    this.Parent.oContained.w_PMMANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMMANNUL_3_8.SetRadio()
    this.Parent.oContained.w_PMMANNUL=trim(this.Parent.oContained.w_PMMANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMMANNUL=="S",1,;
      0)
  endfunc

  func oPMMANNUL_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODASUG="S")
    endwith
   endif
  endfunc

  add object oPMMANTIC_3_9 as StdCheck with uid="GTKYGYKWCD",rtseq=48,rtrep=.f.,left=17, top=196, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3979065,;
    cFormVar="w_PMMANTIC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMMANTIC_3_9.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMMANTIC_3_9.GetRadio()
    this.Parent.oContained.w_PMMANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMMANTIC_3_9.SetRadio()
    this.Parent.oContained.w_PMMANTIC=trim(this.Parent.oContained.w_PMMANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMMANTIC=="S",1,;
      0)
  endfunc

  func oPMMANTIC_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODASUG="S")
    endwith
   endif
  endfunc

  add object oPMMANCOP_3_10 as StdRadio with uid="JVFEHDILKM",rtseq=49,rtrep=.f.,left=179, top=196, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMMANCOP", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPMMANCOP_3_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12798138
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12798138
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMMANCOP_3_10.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMMANCOP_3_10.GetRadio()
    this.Parent.oContained.w_PMMANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMMANCOP_3_10.SetRadio()
    this.Parent.oContained.w_PMMANCOP=trim(this.Parent.oContained.w_PMMANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMMANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMMANCOP=="N",2,;
      0))
  endfunc

  func oPMMANCOP_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODASUG="S" and .w_PMMANTIC="S")
    endwith
   endif
  endfunc

  add object oPMODAPIA_3_11 as StdCheck with uid="XAUYWUPAPB",rtseq=50,rtrep=.f.,left=144, top=233, caption="ODA da ordinare",;
    ToolTipText = "Abilita messagi su ODA da ordinare",;
    HelpContextID = 191878967,;
    cFormVar="w_PMODAPIA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMODAPIA_3_11.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMODAPIA_3_11.GetRadio()
    this.Parent.oContained.w_PMODAPIA = this.RadioValue()
    return .t.
  endfunc

  func oPMODAPIA_3_11.SetRadio()
    this.Parent.oContained.w_PMODAPIA=trim(this.Parent.oContained.w_PMODAPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PMODAPIA=="S",1,;
      0)
  endfunc

  add object oPMDPOSTI_3_14 as StdCheck with uid="YWMIQWUGAV",rtseq=51,rtrep=.f.,left=17, top=258, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257632063,;
    cFormVar="w_PMDPOSTI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMDPOSTI_3_14.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMDPOSTI_3_14.GetRadio()
    this.Parent.oContained.w_PMDPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMDPOSTI_3_14.SetRadio()
    this.Parent.oContained.w_PMDPOSTI=trim(this.Parent.oContained.w_PMDPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMDPOSTI=="S",1,;
      0)
  endfunc

  func oPMDPOSTI_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAPIA="S")
    endwith
   endif
  endfunc

  add object oPMDPO_GG_3_15 as StdField with uid="JSHXNXATQH",rtseq=52,rtrep=.f.,;
    cFormVar = "w_PMDPO_GG", cQueryName = "PMDPO_GG",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190523197,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=120, Top=260, cSayPict='"99"', cGetPict='"99"'

  func oPMDPO_GG_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAPIA="S" and .w_PMDPOSTI="S")
    endwith
   endif
  endfunc

  func oPMDPO_GG_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMDPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMDPOCOP_3_17 as StdRadio with uid="MSOXZQDQMN",rtseq=53,rtrep=.f.,left=179, top=258, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMDPOCOP", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPMDPOCOP_3_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10803386
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10803386
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMDPOCOP_3_17.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMDPOCOP_3_17.GetRadio()
    this.Parent.oContained.w_PMDPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMDPOCOP_3_17.SetRadio()
    this.Parent.oContained.w_PMDPOCOP=trim(this.Parent.oContained.w_PMDPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMDPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMDPOCOP=="N",2,;
      0))
  endfunc

  func oPMDPOCOP_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAPIA="S" and .w_PMDPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMDANNUL_3_18 as StdCheck with uid="VPFSOJREFW",rtseq=54,rtrep=.f.,left=17, top=285, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171714370,;
    cFormVar="w_PMDANNUL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMDANNUL_3_18.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMDANNUL_3_18.GetRadio()
    this.Parent.oContained.w_PMDANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMDANNUL_3_18.SetRadio()
    this.Parent.oContained.w_PMDANNUL=trim(this.Parent.oContained.w_PMDANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMDANNUL=="S",1,;
      0)
  endfunc

  func oPMDANNUL_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAPIA="S")
    endwith
   endif
  endfunc

  add object oPMDANTIC_3_19 as StdCheck with uid="WXUVOIWFCQ",rtseq=55,rtrep=.f.,left=17, top=312, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3942201,;
    cFormVar="w_PMDANTIC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMDANTIC_3_19.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMDANTIC_3_19.GetRadio()
    this.Parent.oContained.w_PMDANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMDANTIC_3_19.SetRadio()
    this.Parent.oContained.w_PMDANTIC=trim(this.Parent.oContained.w_PMDANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMDANTIC=="S",1,;
      0)
  endfunc

  func oPMDANTIC_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAPIA="S")
    endwith
   endif
  endfunc

  add object oPMDANCOP_3_20 as StdRadio with uid="QVGUJNTMAO",rtseq=56,rtrep=.f.,left=181, top=309, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMDANCOP", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPMDANCOP_3_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12835002
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12835002
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMDANCOP_3_20.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMDANCOP_3_20.GetRadio()
    this.Parent.oContained.w_PMDANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMDANCOP_3_20.SetRadio()
    this.Parent.oContained.w_PMDANCOP=trim(this.Parent.oContained.w_PMDANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMDANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMDANCOP=="N",2,;
      0))
  endfunc

  func oPMDANCOP_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAPIA="S" and .w_PMDANTIC='S')
    endwith
   endif
  endfunc

  add object oPMODAORD_3_21 as StdCheck with uid="KXQYIYMYWH",rtseq=57,rtrep=.f.,left=499, top=119, caption="ODA ordinati",;
    ToolTipText = "Abilita messagi su ODA ordinati",;
    HelpContextID = 93333702,;
    cFormVar="w_PMODAORD", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMODAORD_3_21.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMODAORD_3_21.GetRadio()
    this.Parent.oContained.w_PMODAORD = this.RadioValue()
    return .t.
  endfunc

  func oPMODAORD_3_21.SetRadio()
    this.Parent.oContained.w_PMODAORD=trim(this.Parent.oContained.w_PMODAORD)
    this.value = ;
      iif(this.Parent.oContained.w_PMODAORD=="S",1,;
      0)
  endfunc

  add object oPMRPOSTI_3_24 as StdCheck with uid="FJRBJASDTU",rtseq=58,rtrep=.f.,left=372, top=144, caption="Posticipare",;
    ToolTipText = "Abilita messaggio posticipare",;
    HelpContextID = 257689407,;
    cFormVar="w_PMRPOSTI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMRPOSTI_3_24.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMRPOSTI_3_24.GetRadio()
    this.Parent.oContained.w_PMRPOSTI = this.RadioValue()
    return .t.
  endfunc

  func oPMRPOSTI_3_24.SetRadio()
    this.Parent.oContained.w_PMRPOSTI=trim(this.Parent.oContained.w_PMRPOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_PMRPOSTI=="S",1,;
      0)
  endfunc

  func oPMRPOSTI_3_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAORD="S")
    endwith
   endif
  endfunc

  add object oPMRPO_GG_3_25 as StdField with uid="DZCLYJEZFM",rtseq=59,rtrep=.f.,;
    cFormVar = "w_PMRPO_GG", cQueryName = "PMRPO_GG",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo giorni per eseguire spostamento",;
    HelpContextID = 190580541,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=472, Top=146, cSayPict='"99"', cGetPict='"99"'

  func oPMRPO_GG_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAORD="S" and .w_PMRPOSTI="S")
    endwith
   endif
  endfunc

  func oPMRPO_GG_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PMRPO_GG>=0)
    endwith
    return bRes
  endfunc

  add object oPMRPOCOP_3_27 as StdRadio with uid="CQUKVKJKAK",rtseq=60,rtrep=.f.,left=531, top=144, width=152,height=35;
    , ToolTipText = "Posticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMRPOCOP", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPMRPOCOP_3_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 10746042
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 10746042
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Posticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMRPOCOP_3_27.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMRPOCOP_3_27.GetRadio()
    this.Parent.oContained.w_PMRPOCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMRPOCOP_3_27.SetRadio()
    this.Parent.oContained.w_PMRPOCOP=trim(this.Parent.oContained.w_PMRPOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMRPOCOP=="S",1,;
      iif(this.Parent.oContained.w_PMRPOCOP=="N",2,;
      0))
  endfunc

  func oPMRPOCOP_3_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAORD="S" and .w_PMRPOSTI="S")
    endwith
   endif
  endfunc

  add object oPMRANNUL_3_28 as StdCheck with uid="KXFBAKCODP",rtseq=61,rtrep=.f.,left=372, top=169, caption="Annullare",;
    ToolTipText = "Abilita messaggio annullare",;
    HelpContextID = 171771714,;
    cFormVar="w_PMRANNUL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMRANNUL_3_28.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMRANNUL_3_28.GetRadio()
    this.Parent.oContained.w_PMRANNUL = this.RadioValue()
    return .t.
  endfunc

  func oPMRANNUL_3_28.SetRadio()
    this.Parent.oContained.w_PMRANNUL=trim(this.Parent.oContained.w_PMRANNUL)
    this.value = ;
      iif(this.Parent.oContained.w_PMRANNUL=="S",1,;
      0)
  endfunc

  func oPMRANNUL_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAORD="S")
    endwith
   endif
  endfunc

  add object oPMRANTIC_3_29 as StdCheck with uid="USEGBRGALC",rtseq=62,rtrep=.f.,left=372, top=196, caption="Anticipare",;
    ToolTipText = "Abilita messaggio anticipare",;
    HelpContextID = 3999545,;
    cFormVar="w_PMRANTIC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPMRANTIC_3_29.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPMRANTIC_3_29.GetRadio()
    this.Parent.oContained.w_PMRANTIC = this.RadioValue()
    return .t.
  endfunc

  func oPMRANTIC_3_29.SetRadio()
    this.Parent.oContained.w_PMRANTIC=trim(this.Parent.oContained.w_PMRANTIC)
    this.value = ;
      iif(this.Parent.oContained.w_PMRANTIC=="S",1,;
      0)
  endfunc

  func oPMRANTIC_3_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAORD="S")
    endwith
   endif
  endfunc

  add object oPMRANCOP_3_30 as StdRadio with uid="KLKZURNRRD",rtseq=63,rtrep=.f.,left=531, top=196, width=152,height=35;
    , ToolTipText = "Anticipa sempre oppure solo se copre il fabbisogno";
    , cFormVar="w_PMRANCOP", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPMRANCOP_3_30.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sempre"
      this.Buttons(1).HelpContextID = 12777658
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Copertura fabbisogno"
      this.Buttons(2).HelpContextID = 12777658
      this.Buttons(2).Top=16
      this.SetAll("Width",150)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Anticipa sempre oppure solo se copre il fabbisogno")
      StdRadio::init()
    endproc

  func oPMRANCOP_3_30.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oPMRANCOP_3_30.GetRadio()
    this.Parent.oContained.w_PMRANCOP = this.RadioValue()
    return .t.
  endfunc

  func oPMRANCOP_3_30.SetRadio()
    this.Parent.oContained.w_PMRANCOP=trim(this.Parent.oContained.w_PMRANCOP)
    this.value = ;
      iif(this.Parent.oContained.w_PMRANCOP=="S",1,;
      iif(this.Parent.oContained.w_PMRANCOP=="N",2,;
      0))
  endfunc

  func oPMRANCOP_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PMODAORD="S" and .w_PMRANTIC="S")
    endwith
   endif
  endfunc


  add object oCODICE_3_33 as StdCombo with uid="JGLYWRMFKM",rtseq=66,rtrep=.f.,left=165,top=35,width=146,height=21, enabled=.f.;
    , ToolTipText = "Seleziona le impostazioni dei parametri";
    , HelpContextID = 258725850;
    , cFormVar="w_CODICE",RowSource=""+"Interni MRP,"+"Esterni MRP", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oCODICE_3_33.RadioValue()
    return(iif(this.value =1,"MR",;
    iif(this.value =2,"EX",;
    space(2))))
  endfunc
  func oCODICE_3_33.GetRadio()
    this.Parent.oContained.w_CODICE = this.RadioValue()
    return .t.
  endfunc

  func oCODICE_3_33.SetRadio()
    this.Parent.oContained.w_CODICE=trim(this.Parent.oContained.w_CODICE)
    this.value = ;
      iif(this.Parent.oContained.w_CODICE=="MR",1,;
      iif(this.Parent.oContained.w_CODICE=="EX",2,;
      0))
  endfunc

  add object oPMPERCON_3_34 as StdField with uid="YTCBTSMCUI",rtseq=67,rtrep=.f.,;
    cFormVar = "w_PMPERCON", cQueryName = "PMPERCON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Periodo di congelamento",;
    HelpContextID = 8329404,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=164, Top=61, cSayPict='"999"', cGetPict='"999"'

  add object oStr_3_2 as StdString with uid="ORZOOTTJUR",Visible=.t., Left=17, Top=122,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="NWMPMRRWLL",Visible=.t., Left=154, Top=148,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="MRXKIFPEAF",Visible=.t., Left=17, Top=236,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="CFJSZTWCIR",Visible=.t., Left=156, Top=262,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="ECWLPBBOCP",Visible=.t., Left=372, Top=122,;
    Alignment=0, Width=126, Height=18,;
    Caption="Abilitazione messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="ZAUNSXRBTY",Visible=.t., Left=508, Top=148,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="CDDMVBDXNW",Visible=.t., Left=17, Top=92,;
    Alignment=0, Width=105, Height=18,;
    Caption="Parametri ODA"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="KCPDYGWUYL",Visible=.t., Left=16, Top=64,;
    Alignment=1, Width=144, Height=18,;
    Caption="Periodo di congelamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="VJLLVYXIKU",Visible=.t., Left=205, Top=64,;
    Alignment=0, Width=18, Height=18,;
    Caption="gg"  ;
  , bGlobalFont=.t.

  add object oStr_3_37 as StdString with uid="BPZDJHSCZV",Visible=.t., Left=27, Top=39,;
    Alignment=1, Width=133, Height=17,;
    Caption="Parametri messaggi:"  ;
  , bGlobalFont=.t.

  add object oStr_3_38 as StdString with uid="QYIALJRYKX",Visible=.t., Left=17, Top=6,;
    Alignment=0, Width=126, Height=18,;
    Caption="Parametri generali"  ;
  , bGlobalFont=.t.

  add object oBox_3_3 as StdBox with uid="TTZCUKIUGY",left=8, top=141, width=327,height=1

  add object oBox_3_13 as StdBox with uid="TVYSDGIEQX",left=8, top=255, width=327,height=1

  add object oBox_3_23 as StdBox with uid="VLGTGROBWA",left=358, top=141, width=327,height=1

  add object oBox_3_32 as StdBox with uid="XBXWSOOQYS",left=8, top=111, width=677,height=1

  add object oBox_3_39 as StdBox with uid="ZKVQJTFYXM",left=7, top=25, width=677,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_kpa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
