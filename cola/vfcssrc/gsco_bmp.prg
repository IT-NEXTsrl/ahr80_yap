* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bmp                                                        *
*              Manutenzione ODL/OCL                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_230]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-04                                                      *
* Last revis.: 2018-09-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bmp",oParentObject,m.Azione)
return(i_retval)

define class tgsco_bmp as StdBatch
  * --- Local variables
  Azione = space(10)
  w_DATRIF = ctod("  /  /  ")
  NumPeriodi = 0
  NumMaxPeriodi = 0
  PrimaColPer = 0
  GRID = .NULL.
  cCursor = space(10)
  ELEN = .NULL.
  DatEla = ctod("  /  /  ")
  gDI = ctod("  /  /  ")
  sDI = ctod("  /  /  ")
  mDI = ctod("  /  /  ")
  tDI = ctod("  /  /  ")
  gDF = ctod("  /  /  ")
  sDF = ctod("  /  /  ")
  mDF = ctod("  /  /  ")
  tDF = ctod("  /  /  ")
  TmpD = ctod("  /  /  ")
  TmpC = space(100)
  TmpC1 = space(10)
  TmpN = 0
  TmpN1 = 0
  TmpN2 = 0
  TmpN3 = 0
  i = 0
  y = 0
  k = 0
  h = 0
  z = 0
  cNRecSel = 0
  w_ONHAND = 0
  w_KEYSAL = space(20)
  PunPAD = .NULL.
  DETT = .NULL.
  cCursDett = space(10)
  tCURPER = space(3)
  tPABCUM = 0
  w_GestioneODP = .f.
  f_DISINI = space(20)
  f_DISFIN = space(20)
  f_GRUMER = space(5)
  f_CATOMO = space(5)
  f_TIPGES = space(1)
  f_CODCOM = space(15)
  f_CODATT = space(15)
  f_CODFOR = space(15)
  f_CODFAM = space(5)
  f_CODMAR = space(50)
  f_PROFIN = space(2)
  f_SEMLAV = space(2)
  f_MATPRI = space(2)
  f_MATFAS = space(2)
  f_CODFAF = space(5)
  f_GRUMEF = space(5)
  f_CATOMF = space(5)
  f_CODMAF = space(5)
  f_MAGINI = space(5)
  f_MAGFIN = space(5)
  f_CODCOF = space(15)
  f_CODATF = space(15)
  f_CODFOF = space(15)
  f_CODFASI = space(66)
  f_CODFASF = space(66)
  f_CRIELA = space(1)
  TmpN5 = 0
  TmpN6 = 0
  w_LockColODP = 0
  w_LockColDETT = 0
  w_c = 0
  TmpC2 = space(100)
  TmpD1 = ctod("  /  /  ")
  w_NewCursMast = space(10)
  cCursMast = space(10)
  w_TMPPER = space(3)
  TmpN4 = 0
  w_NewCursDett = space(10)
  z = 0
  w_TMPPER = space(3)
  GestODP = .NULL.
  tMAGDEF = space(5)
  tCAUORD = space(5)
  tCAUIMP = space(5)
  tOFLORD = space(1)
  tOFLIMP = space(1)
  tDFINE = ctod("  /  /  ")
  tMPCODICE = space(20)
  tDESCOD = space(40)
  tMPCODART = space(20)
  tUNMIS3 = space(3)
  tMOLTI3 = 0
  tOPERA3 = space(1)
  tDESART = space(40)
  tUNMIS1 = space(3)
  tOPERAT = space(1)
  tMOLTIP = 0
  tUNMIS2 = space(3)
  tLOTPRO = 0
  tLOTECO = 0
  tLEAFIS = 0
  tMPLEATIM = 0
  tCOEFLT = 0
  tLOTMED = 0
  tMPDATINI = ctod("  /  /  ")
  tCODFOR = space(15)
  tTEMLAV = 0
  tDINRIC = ctod("  /  /  ")
  tSAFELT = 0
  tDTCON = ctod("  /  /  ")
  w_MAGTER = space(5)
  w_DESFOR = space(40)
  w_RCODICE = space(20)
  w_DATSCA = ctod("  /  /  ")
  pCODICE = space(20)
  pTIPGES = space(1)
  pDETTMPS = space(1)
  cSUMCUMRES = 0
  cPERASS = space(3)
  cPREVIS = 0
  cORDINI = 0
  cRESPRE = 0
  cDOMANDA = 0
  cTIPDTF = space(3)
  cGIODTF = 0
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_MARINI = space(5)
  w_MARFIN = space(5)
  w_ELAMPS = space(1)
  w_ELAODF = space(1)
  w_NewCursDett = space(10)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  MPS_TPER_idx=0
  PAR_PROD_idx=0
  CAM_AGAZ_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Piano ODL/OCL (da GSCO_KMP)
    * --- INTESTAZIONI = Prepara intestazioni dei due zoom della prima pagina (w_ZoomODP, w_DettODP)
    * --- MENU = Prepara menu per manutenzione Piano ODL/OCL
    * --- CARICA = Prepara menu per manutenzione Piano ODL/OCL
    * --- L = Carica Cursore dello Zoom principale con Ordini di Conto Lavoro
    * --- O = Carica Cursore dello Zoom principale con ODL totale
    * --- Codice selezionato per visualizzazione dettaglio formulazione ODL/OCL (per zoom <DettODP>)
    * --- Colonna attiva in w_ZoomODP
    * --- se .T. Ŕ visibile solo lo zoom w_ZoomODP, altrimenti anche w_DettODP
    * --- Altezza dello zoom w_ZoomODP
    * --- Dimensione colonna in visualizzazione Zoom
    * --- Dimensione Font per visualizzazione Zoom
    * --- Numero Periodi dell'orizzonte temporale elaborato
    * --- Massimo Numero Periodi dell'orizzonte temporale elaborato (Default=54)
    * --- Periodo ASSOLUTO selezionato da <ZoomODP> (per zomm <PeriodoODP>)
    * --- Periodo RELATIVO selezionato da <ZoomODP> (per zomm <PeriodoODP>)
    * --- Descrizione Tipo Periodo selezionato
    * --- Data INIZIO periodo selezionato
    * --- Data FINE periodo selezionato
    * --- Codice selezionato per visualizzazione dettaglio del periodo selezionato da zoom ODP (per zoom <PeriodoODP>)
    * --- Valorizzato a w_PARAM per Filtro Zoom <PeriodoODP>
    * --- ODL/OCL Selezionato da zoom del dettaglio periodo (<PeriodoODP>)
    * --- Filtri
    Private Scelta
    Scelta = rTrim(this.Azione)
    this.PunPAD = this.oParentObject
    this.GRID = this.PunPAD.w_ZoomODP.GRD
    this.cCursor = this.PunPad.w_ZoomODP.cCursor
    this.ELEN = this.PunPAD.w_PeriodoODP.GRD
    this.NumPeriodi = this.oParentObject.w_NMAXPE
    this.PrimaColPer = 3
    * --- Prima colonna dei periodi
    this.NumMaxPeriodi = this.PrimaColPer + this.NumPeriodi
    * --- Attiva sempre il pannello di destra per lo scroll
    * --- Controlla se sulla maschera c'Ŕ lo zoom del dettaglio (w_DETTODP)
    if type("this.PUNPAD.w_DettODP") = "U"
      this.w_GestioneODP = .F.
    else
      * --- zoom dettaglio
      this.w_GestioneODP = .T.
      this.DETT = this.PunPAD.w_DettODP.GRD
      this.cCursDETT = this.PunPad.w_DettODP.cCursor
      * --- Attiva sempre il pannello di destra per lo scroll
    endif
    do case
      case this.Azione="INTESTAZIONI"
        if (g_PROD<>"S" AND this.oParentObject.w_PARAM<>"L") OR (g_COLA<>"S" AND this.oParentObject.w_PARAM="L") or (g_MMPS<>"S" AND this.oParentObject.w_PARAM $ "CFT")
          ah_ErrorMsg("Modulo non installato, impossibile proseguire",,"")
          this.oParentObject.ecpQuit()
          Keyboard "{ESC}"
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_LARGCOL=0 OR this.oParentObject.w_DIMFONT=0 OR this.oParentObject.w_NUMPER=0 OR this.oParentObject.w_NMAXPE=0
          do case
            case this.oParentObject.w_PARAM="O"
              this.TmpC = "Parametri di configurazione delle griglie non definiti%0o nessuna generazione ODL eseguita"
            case this.oParentObject.w_PARAM="L"
              this.TmpC = "Parametri di configurazione delle griglie non definiti%0o nessuna generazione OCL eseguita"
            case this.oParentObject.w_PARAM="C"
              this.TmpC = "Parametri di configurazione delle griglie non definiti%0o nessuna generazione ODP eseguita"
            case this.oParentObject.w_PARAM="F"
              this.TmpC = "Parametri di configurazione delle griglie non definiti%0o nessuna generazione ODR eseguita"
            case this.oParentObject.w_PARAM="T"
              this.TmpC = "Parametri di configurazione delle griglie non definiti%0o nessuna generazione ODF eseguita"
            otherwise
              this.TmpC = "Parametri di configurazione delle griglie non definiti%0o nessuna generazione ODA eseguita"
          endcase
          ah_ErrorMsg(this.TmpC,,"")
          this.oParentObject.ecpQuit()
          Keyboard "{ESC}"
          i_retcode = 'stop'
          return
        endif
        * --- All'evento INIT - Prepara colonne Zoom
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Nasconde Zoom Dettaglio
        if this.w_GestioneODP
          this.PunPAD.w_DettODP.visible = .F.
        endif
      case this.Azione="INTERROGA"
        if this.oParentObject.oPgFrm.ActivePage <> 1
          this.oParentObject.oPgFrm.ActivePage = 1
        endif
        * --- Bottone Ricerca - Carica cursore zoom
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Azione="XSCROLL"
        * --- Le colonne bloccate sono 3 ma la terza (la descrizione ha identificativo 58, mentre le prime 2 hanno 1 e 2)
        this.w_LockColODP = this.GRID.LockColumns-1
        this.w_LockColDETT = this.DETT.LockColumns-1
        if this.PunPAD.w_DettODP.Visible
          * --- Attiva sempre il pannello di destra per lo scroll
          this.PunPAD.LockScreen = .T.
          this.w_c = 1
          do while this.w_c <= this.w_LockColODP
            this.GRID.Columns( this.w_c ).Visible = .f.
            this.w_c = this.w_c + 1
          enddo
          this.GRID.Columns(58).Visible = .f.
          this.w_c = 1
          do while this.w_c <= this.w_LockColDETT
            this.DETT.Columns( this.w_c ).Visible = .f.
            this.w_c = this.w_c + 1
          enddo
          this.DETT.Columns(58).Visible = .f.
          if this.GRID.LeftColumn + 2 <> this.DETT.LeftColumn + 2
            * --- Allinea
            this.TmpN = this.DETT.LeftColumn
            do while this.GRID.LeftColumn > this.DETT.LeftColumn
              * --- Scrolla il dettaglio a destra di una posizione
              this.DETT.DoScroll(5)     
              if this.DETT.LeftColumn=this.TmpN
                * --- Lo scroll a destra sul dettaglio non puo' avvenire ...
                this.GRID.DoScroll(4)     
                exit
              else
                this.TmpN = this.DETT.LeftColumn
              endif
            enddo
            do while this.GRID.LeftColumn < this.DETT.LeftColumn
              * --- Scrolla il dettaglio a destra di una posizione
              this.DETT.DoScroll(4)     
            enddo
          endif
          this.w_c = 1
          do while this.w_c <= this.w_LockColODP
            this.GRID.Columns( this.w_c ).Visible = .t.
            this.w_c = this.w_c + 1
          enddo
          this.GRID.Columns(58).Visible = .t.
          this.GRID.Resize()     
          this.w_c = 1
          do while this.w_c <= this.w_LockColDETT
            this.DETT.Columns( this.w_c ).Visible = .t.
            this.w_c = this.w_c + 1
          enddo
          this.DETT.Columns(58).Visible = .t.
          this.DETT.Resize()     
          this.PunPAD.LockScreen = .F.
        endif
      case this.Azione="SELEZIONE"
        * --- Se click su prima colonna, non fa niente
        if this.oParentObject.w_COLSEL > 1
          * --- Valorizza correttamente le variabili sulla maschera
          this.oParentObject.w_CODSEL = Nvl(this.PUNPAD.w_ZoomODP.GetVar("CACODICE"),Space(20))
          if NOT EMPTY(NVL(this.oParentObject.w_CODSEL,""))
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADESART"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODSEL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADESART;
                from (i_cTable) where;
                    CACODICE = this.oParentObject.w_CODSEL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESSEL = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Legge dati da tabella dei periodi
            this.oParentObject.w_PSELA = right("000"+alltrim(str(this.oParentObject.w_ColSel-(this.PrimaColPer+1),3,0)),3)
            * --- Read from MPS_TPER
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MPS_TPER_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TPPERREL,TPDATINI,TPDATFIN"+;
                " from "+i_cTable+" MPS_TPER where ";
                    +"TPPERASS = "+cp_ToStrODBC(this.oParentObject.w_PSELA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TPPERREL,TPDATINI,TPDATFIN;
                from (i_cTable) where;
                    TPPERASS = this.oParentObject.w_PSELA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_PSELR = NVL(cp_ToDate(_read_.TPPERREL),cp_NullValue(_read_.TPPERREL))
              this.oParentObject.w_DIP = NVL(cp_ToDate(_read_.TPDATINI),cp_NullValue(_read_.TPDATINI))
              this.oParentObject.w_DFP = NVL(cp_ToDate(_read_.TPDATFIN),cp_NullValue(_read_.TPDATFIN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Assegna Descrzione del tipo periodo
            this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Giornaliero")
            do case
              case this.oParentObject.w_PSELR="SCAD"
                this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Scaduto")
              case left(this.oParentObject.w_PSELR,1)="G"
                this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Giornaliero")
              case left(this.oParentObject.w_PSELR,1)="S" and alltrim(this.oParentObject.w_PSELR)<>"SCAD"
                this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Settimanale")
              case left(this.oParentObject.w_PSELR,1)="M"
                this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Mensile")
              case left(this.oParentObject.w_PSELR,1)="T"
                this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Trimestrale")
              otherwise
                this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Oltre")
            endcase
            if this.oParentObject.w_TIPOPERIODO <> ah_Msgformat("Oltre")
              * --- Visualizza Zoom Elenco ODL/OCL periodo
              this.PunPAD.NotifyEvent("ListaODP")     
              * --- Attiva la pagina 2 automaticamente
              this.oParentObject.oPgFrm.ActivePage = 2
            endif
          endif
        endif
      case this.Azione="SHOWDETT"
        if this.Azione=="SHOWDETTC" and this.oParentObject.w_ZoomEspanso
          i_retcode = 'stop'
          return
        endif
        this.PunPAD.LockScreen = .T.
        if this.oParentObject.w_ZoomEspanso
          this.oParentObject.w_ZoomEspanso = .f.
          this.PunPAD.w_DettODP.visible = .T.
          this.PunPAD.Resize()     
          this.PunPAD.w_ZoomODP.cHeight = max(this.PunPAD.w_DettODP.TOP - this.PunPAD.w_ZoomODP.Top -12, 0)
          this.PunPAD.w_ZoomODP.Resize()     
          this.PunPAD.w_ZoomODP.GRD.Resize()     
        endif
        * --- Carica Cursore dettaglio
        if this.oParentObject.w_PARAM $ "C-F-T"
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.PunPAD.LockScreen = .F.
      case this.Azione="HIDEDETT"
        * --- zoom dettaglio
        this.DETT = this.PunPAD.w_DettODP.GRD
        this.cCursDETT = this.PunPad.w_DettODP.cCursor
        this.oParentObject.w_ZoomEspanso = .t.
        this.PunPAD.w_ZoomODP.cHeight = max(this.PunPAD.w_DettODP.TOP + this.PunPAD.w_DettODP.Height + this.PunPAD.w_ZoomODP.Top -7 , 0)
        this.PunPAD.w_DettODP.visible = .F.
        this.PunPAD.w_DettODP.zOrder(1)     
        this.PunPAD.Resize()     
        this.PunPAD.w_ZoomODP.Resize()     
      case this.Azione = "MENU_LISTAODP"
        * --- Definisce SHORTCUT
        store space(10) to Scelta
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if not empty( Scelta )
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Azione="VISUALIZZA_ODP" or this.Azione="ELIMINA_ODP" or this.Azione="CARICA_ODP" or this.Azione="MODIFICA_ODP"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Azione="VERIF_TEMP"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SISTEMA  INTESTAZIONI ZOOM (DA LANCIARE SOLO ALLA INIT DELLA MASCHERA)
    if Type("PeriodiODP(1)") $ "L"
      dimension PeriodiODP(100)
      vq_exec("..\COLA\EXE\QUERY\GSCO_BMP", this, "_PeriodiODP_")
      if reccount("_PeriodiODP_")=0
        ah_ErrorMsg("Impossibile creare l'orizzonte temporale%0Verificare i periodi nei parametri produzione","STOP","")
        this.oParentObject.ecpQuit()
        Keyboard "{ESC}"
        i_retcode = 'stop'
        return
      endif
      select * from _PeriodiODP_ into array PeriodiODP
      use in _PeriodiODP_
    endif
    * --- Assegna titolo a gestione
    do case
      case this.oParentObject.w_PARAM="L"
        this.PunPAD.cComment = ah_Msgformat("Piano ordini di conto lavoro (OCL)")
        this.PunPAD.opgfrm.page1.caption = ah_Msgformat("Ordini di conto lavoro (OCL)")
        * --- Verifica visibilitÓ labels colori
        this.PunPAD.opgfrm.page3.opag.SUGGE.visible = .f.
        this.PunPAD.opgfrm.page3.opag.CONF.visible = .f.
        this.PunPAD.opgfrm.page3.opag.DPIA.visible = .f.
      case this.oParentObject.w_PARAM="O"
        this.PunPAD.cComment = ah_Msgformat("Piano ordini di lavorazione (ODL)")
        this.PunPAD.opgfrm.page1.caption = ah_Msgformat("Ordini di lavorazione (ODL)")
        * --- Verifica visibilitÓ labels colori
        this.PunPAD.opgfrm.page3.opag.SUGGE.visible = .f.
        this.PunPAD.opgfrm.page3.opag.CONF.visible = .f.
        this.PunPAD.opgfrm.page3.opag.DPIA.visible = .f.
      case this.oParentObject.w_PARAM="C"
        this.PunPAD.cComment = ah_Msgformat("Piano ordini di produzione MPS (ODP)")
        this.PunPAD.opgfrm.page1.caption = ah_Msgformat("Ordini di produzione (ODP)")
        * --- Verifica visibilitÓ labels colori
        this.PunPAD.opgfrm.page3.opag.SUGGM.visible = .f.
      case this.oParentObject.w_PARAM="F"
        this.PunPAD.cComment = ah_Msgformat("Piano ordini di produzione per ricambi (ODR)")
        this.PunPAD.opgfrm.page1.caption = ah_Msgformat("Ordini di produzione (ODR)")
        * --- Verifica visibilitÓ labels colori
        this.PunPAD.opgfrm.page3.opag.SUGGM.visible = .f.
      case this.oParentObject.w_PARAM="T"
        this.PunPAD.cComment = ah_Msgformat("Piano proposte ordini fornitore (ODF)")
        this.PunPAD.opgfrm.page1.caption = ah_Msgformat("Proposte ordini fornitore (ODF)")
        * --- Verifica visibilitÓ labels colori
        this.PunPAD.opgfrm.page3.opag.SUGGM.visible = .f.
        this.PunPAD.opgfrm.page3.opag.LANC.visible = .f.
      otherwise
        this.PunPAD.cComment = ah_Msgformat("Piano ordini di acquisto (ODA)")
        this.PunPAD.opgfrm.page1.caption = ah_Msgformat("Ordini di acquisto (ODA)")
        * --- Verifica visibilitÓ labels colori
        this.PunPAD.opgfrm.page3.opag.SUGGE.visible = .f.
        this.PunPAD.opgfrm.page3.opag.CONF.visible = .f.
        this.PunPAD.opgfrm.page3.opag.DPIA.visible = .f.
    endcase
    this.oParentObject.w_BASSUG = mod(this.oParentObject.w_PPCOLSUG, (rgb(255,255,255)+1))
    this.oParentObject.w_BASCON = mod(this.oParentObject.w_PPCOLCON, (rgb(255,255,255)+1))
    this.oParentObject.w_BASLAN = mod(this.oParentObject.w_PPCOLMLA, (rgb(255,255,255)+1))
    this.oParentObject.w_BASMIS = mod(this.oParentObject.w_PPCOLMIS, (rgb(255,255,255)+1))
    this.oParentObject.w_BASSUM = mod(this.oParentObject.w_PPCOLSUM, (rgb(255,255,255)+1))
    this.oParentObject.w_BASPIA = mod(this.oParentObject.w_PPCOLPIA, (rgb(255,255,255)+1))
    this.oParentObject.w_BASMDP = mod(this.oParentObject.w_PPCOLMDP, (rgb(255,255,255)+1))
    this.oParentObject.w_TESSUG = int(this.oParentObject.w_PPCOLSUG/(rgb(255,255,255)+1))
    this.oParentObject.w_TESCON = int(this.oParentObject.w_PPCOLCON/(rgb(255,255,255)+1))
    this.oParentObject.w_TESLAN = int(this.oParentObject.w_PPCOLMLA/(rgb(255,255,255)+1))
    this.oParentObject.w_TESMIS = int(this.oParentObject.w_PPCOLMIS/(rgb(255,255,255)+1))
    this.oParentObject.w_TESSUM = int(this.oParentObject.w_PPCOLSUM/(rgb(255,255,255)+1))
    this.oParentObject.w_TESPIA = int(this.oParentObject.w_PPCOLPIA/(rgb(255,255,255)+1))
    this.oParentObject.w_TESMDP = int(this.oParentObject.w_PPCOLMDP/(rgb(255,255,255)+1))
    this.PunPAD.Caption = this.PunPAD.cComment
    * --- Sistema intestazioni, font e larghezza celle
    this.TmpC = "iif(thisform.w_zoomODP.grd.activerow=recno(thisform.w_zoomODP.grd."
    this.TmpC = this.TmpC+"recordsource),rgb(255,255,0),rgb(255,255,255))"
    this.GRID.fontsize = this.oParentObject.w_DimFont
    if .f.
      this.GRID.Column1.DynamicBackColor = this.TmpC
    endif
    this.GRID.Column1.width = 145
    * --- Zoom dettaglio ...
    if this.w_GestioneODP
      this.TmpC = "iif(thisform.w_DettODP.grd.activerow=recno(thisform.w_DettODP.grd."
      this.TmpC = this.TmpC+"recordsource),rgb(255,255,0),rgb(255,255,255))"
      this.DETT.fontsize = this.oParentObject.w_DimFont
      if .f.
        this.DETT.Column1.DynamicBackColor = this.TmpC
      endif
      this.DETT.Column1.movable = .f.
      this.DETT.column1.hdr.forecolor = RGB(128,0,0)
      this.DETT.column1.hdr.fontbold = .T.
      this.DETT.Column1.width = 145
      this.DETT.Column2.movable = .f.
      this.DETT.Column58.movable = .f.
      this.DETT.Column58.width = 1
      this.DETT.rowheight = 13
    endif
    this.i = this.PrimaColPer
    do while this.i <= this.NumMaxPeriodi
      j= alltrim(str(this.i,3,0))
      * --- Determina indice vettore
      this.TmpC = right("000"+alltrim(str(this.i-this.PrimaColPer,2,0)),3)
      this.TmpN = int( Val(this.TmpC) + 1 )
      this.TmpC1 = nvl( PeriodiODP( this.TmpN , 2 ) , "XXXX")
      * --- Periodo relativo
      this.TmpD = cp_ToDate( PeriodiODP( this.TmpN , 3 ) )
      * --- Data inizio periodo
      this.TmpD1 = cp_ToDate( PeriodiODP( this.TmpN , 5 ) )
      * --- Primo giorno lavorativo
      if this.i-this.PrimaColPer<=this.oParentObject.w_NumPer + 1
        this.GRID.Column&j..DynamicBackColor = "mod(COL_"+this.TmpC+",16777216)"
        this.GRID.Column&j..DynamicForeColor = "int(COL_"+this.TmpC+"/16777216)"
        this.GRID.column&j..hdr.forecolor = iif(empty(this.TmpD1), this.oParentObject.w_PPCOLFES, RGB(0,0,0))
        this.GRID.Column&j..width = this.oParentObject.w_LargCol
        this.GRID.Column&j..hdr.caption = iif(this.i-this.PrimaColPer=this.oParentObject.w_NumPer + 1, ah_Msgformat("Oltre"), iif(this.i=this.PrimaColPer,ah_Msgformat("Scaduto"),this.TmpC1+iif(left(this.TmpC1,1)="G"," ("+left(dtoc(this.TmpD),5)+")","-"+str(year(this.TmpD),4,0))))
        if this.w_GestioneODP
          this.DETT.Column&j..width = this.oParentObject.w_LargCol
          this.DETT.Column&j..hdr.caption = iif(this.i-this.PrimaColPer=this.oParentObject.w_NumPer + 1, ah_Msgformat("Oltre"), iif(this.i=this.PrimaColPer,ah_Msgformat("Scaduto"),this.TmpC1+iif(left(this.TmpC1,1)="G"," ("+left(dtoc(this.TmpD),5)+")","-"+str(year(this.TmpD),4,0))))
          this.DETT.column&j..hdr.forecolor = iif(empty(this.TmpD1), this.oParentObject.w_PPCOLFES, RGB(0,0,0))
        endif
      else
        this.GRID.Column&j..visible = .f.
        this.GRID.Column&j..width = 1
        if this.w_GestioneODP
          this.DETT.Column&j..visible = .f.
          this.DETT.Column&j..width = 1
        endif
      endif
      this.GRID.Column&j..movable = .f.
      if this.w_GestioneODP
        this.DETT.Column&j..movable = .f.
      endif
      this.i = this.i + 1
    enddo
    this.GRID.Column1.movable = .f.
    this.GRID.Column2.movable = .f.
    this.GRID.Column58.movable = .f.
    * --- Memorizza Altezza iniziale zoom
    this.oParentObject.w_Zoom1Height = this.GRID.height
    this.DETT.ScrollBars = 0
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna cursore zoom per ODL/OCL/ODA/ODP
    this.w_NewCursMast = this.cCursor
    this.cCursMast = SYS(2015)
    SELECT * FROM (this.w_NewCursMast) INTO CURSOR (this.cCursMast) WHERE 1=0 READWRITE
    if Type("PeriodiODP(1)") $ "L"
      dimension PeriodiODP(100)
      vq_exec("..\COLA\EXE\QUERY\GSCO_BMP", this, "_PeriodiODP_")
      select * from _PeriodiODP_ into array PeriodiODP
      use in _PeriodiODP_
    endif
    * --- Variabile contenete il periodo oltre
    this.w_TMPPER = right("000"+alltrim(str(this.oParentObject.w_NumPer+1,2,0)),3)
    * --- Esegue query per creazione struttura cursore (vuoto) .....
    this.f_DISINI = this.oParentObject.w_fDISINI
    this.f_DISFIN = this.oParentObject.w_fDISFIN
    this.f_GRUMER = this.oParentObject.w_fGRUMER
    this.f_GRUMEF = this.oParentObject.w_fGRUMEF
    this.f_CATOMO = this.oParentObject.w_fCATOMO
    this.f_CATOMF = this.oParentObject.w_fCATOMF
    this.f_TIPGES = iif(this.oParentObject.w_fTIPGES="X",space(1),this.oParentObject.w_fTIPGES)
    this.f_CODCOM = this.oParentObject.w_fCODCOM
    this.f_CODCOF = this.oParentObject.w_fCODCOF
    this.f_CODATT = this.oParentObject.w_fCODATT
    this.f_CODATF = this.oParentObject.w_fCODATF
    this.f_CODFOR = this.oParentObject.w_fCODFOR
    this.f_CODFOF = this.oParentObject.w_fCODFOF
    this.f_CODMAR = this.oParentObject.w_fCODMAR
    this.f_CODMAF = this.oParentObject.w_fCODMAF
    this.f_CODFAM = this.oParentObject.w_fCODFAM
    this.f_CODFAF = this.oParentObject.w_fCODFAF
    this.f_PROFIN = this.oParentObject.w_fPROFIN
    this.f_SEMLAV = this.oParentObject.w_fSEMLAV
    this.f_MATPRI = this.oParentObject.w_fMATPRI
    this.f_MATFAS = this.oParentObject.w_fMATFAS
    this.f_MAGINI = this.oParentObject.w_fMAGINI
    this.f_MAGFIN = this.oParentObject.w_fMAGFIN
    this.f_CODFASI = this.oParentObject.w_fCODFASI
    this.f_CODFASF = this.oParentObject.w_fCODFASF
    this.f_CRIELA = this.oParentObject.w_CRIELA
    ah_Msg("Preparazione dati in corso...",.T.)
    do case
      case this.oParentObject.w_PARAM="L"
        * --- Dati OCL
        vq_exec("..\COLA\EXE\QUERY\GSCO1BMP", this, "_ODP_")
      case this.oParentObject.w_PARAM="O"
        * --- Dati ODL
        vq_exec("..\COLA\EXE\QUERY\GSCO2BMP", this, "_ODP_")
      case this.oParentObject.w_PARAM="C"
        * --- Dati ODP
        vq_exec("..\COLA\EXE\QUERY\GSCO4BMP", this, "_ODP_")
      case this.oParentObject.w_PARAM="F"
        * --- Dati ODR
        vq_exec("..\COLA\EXE\QUERY\GSCO5BMP", this, "_ODP_")
      case this.oParentObject.w_PARAM="T"
        * --- Dati ODF
        vq_exec("..\COLA\EXE\QUERY\GSCO6BMP", this, "_ODP_")
      otherwise
        * --- Dati ODA
        vq_exec("..\COLA\EXE\QUERY\GSCO3BMP", this, "_ODP_")
    endcase
    this.k = rgb(255,255,255)
    this.i = 0
    this.h = 0
    if this.oParentObject.w_PARAM $ "CFT"
      this.TmpN1 = this.oParentObject.w_BASSUG + (this.k+1) * this.oParentObject.w_TESSUG
      this.TmpN2 = this.oParentObject.w_BASCON + (this.k+1) * this.oParentObject.w_TESCON
      this.TmpN5 = this.oParentObject.w_BASMDP + (this.k+1) * this.oParentObject.w_TESMDP
      this.TmpN6 = this.oParentObject.w_BASPIA + (this.k+1) * this.oParentObject.w_TESPIA
    else
      this.TmpN1 = this.oParentObject.w_BASSUM + (this.k+1) * this.oParentObject.w_TESSUM
      this.TmpN2 = this.oParentObject.w_BASPIA + (this.k+1) * this.oParentObject.w_TESPIA
    endif
    this.TmpN3 = this.oParentObject.w_BASMIS + (this.k+1) * this.oParentObject.w_TESMIS
    this.TmpN4 = this.oParentObject.w_BASLAN + (this.k+1) * this.oParentObject.w_TESLAN
    if used("_ODP_")
      * --- Mette sfondo bianco su tutte le variabili, escluso i festivi, per i quali mette grigio
      for this.i=0 to this.oParentObject.w_NumPer + 1
      j = right("000"+alltrim(str(this.i,3,0)),3)
      m.COL_&j = this.k
      next
      * --- Riempie cursore zoom
      Select _ODP_
      SCAN
      * --- Legge e scrive su cursore
      SCATTER MEMVAR
      * --- Mette sfondo bianco su tutte le variabili
      for this.i=0 to this.oParentObject.w_NumPer + 1
      j = right("000"+alltrim(str(this.i,3,0)),3)
      if Type("m.TOT_&j")="U" or m.TOT_&j =0
        m.COL_&j = this.k
      else
        this.h = int( m.COL_&j )
        m.COL_&j = iif(this.h=2, this.TmpN1, iif(this.h=4, this.TmpN2, iif(this.h=3 OR this.h=5 OR this.h=6 or this.h=7 or this.h=9 or this.h=12 or this.h=13 or this.h=14 or this.h=15 or this.h=16,this.TmpN3,iif(this.h=8,this.TmpN4,iif(this.h=10,this.TmpN5, iif(this.h=22,this.TmpN6,this.k))))))
      endif
      next
      Select (this.cCursMast)
      append BLANK
      GATHER MEMVAR
      ENDSCAN
      * --- Chiude cursore query
      Use in _ODP_
      * --- Refresh ...
      Select (this.cCursMast)
      go top
      * --- Rinfresca grid
      wait CLEAR
      this.GRID.RecordSource = this.cCursMast
      this.PunPAD.w_ZoomODP.cCursor = this.cCursMast
      this.GRID.Refresh()     
    endif
    wait CLEAR
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna cursore zoom dettaglio
    * --- Codice Selezionato
    this.w_NewCursDett = this.cCursDETT
    this.cCursDETT = SYS(2015)
    SELECT * FROM (this.w_NewCursDett) INTO CURSOR (this.cCursDETT) WHERE 1=0 READWRITE
    this.z = 7
    this.oParentObject.w_CODICE = Nvl(this.PUNPAD.w_ZoomODP.GetVar("CACODICE"),Space(20))
    if NOT EMPTY(NVL(this.oParentObject.w_CODICE,""))
      * --- Sistema Intestazione:
      this.DETT.column1.hdr.caption = ah_Msgformat("Dett.: %1" , this.oParentObject.w_CODICE)
      lCurs = this.PUNPAD.w_ZoomODP.cCursor
      * --- Lettura w_KEYSAL
      this.w_KEYSAL = SPACE(20)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_KEYSAL = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_KEYSAL)
        * --- Legge Dato Giacenza Fisica ...
        this.w_ONHAND = 0
        vq_exec("..\COLA\EXE\QUERY\GSCO0BMP", this, "_TempGiac_")
        * --- Memorizza Giacenza
        this.w_ONHAND = NVL(_TempGiac_.QTASAL, 0)
        * --- Chiude Cursore
        use in _TempGiac_
        ah_Msg("Lettura dati per visualizzazione dettaglio in corso...",.T.)
        * --- Variabile contenete il periodo oltre
        this.w_TMPPER = right("000"+alltrim(str(this.oParentObject.w_NumPer+1,2,0)),3)
        * --- Crea Cursori
        * --- Cursore ODP_MAST
        * --- Cursore con gli ORDINI CLIENTE e FORNITORE da documenti
        vq_exec("..\COLA\EXE\QUERY\GSCOABMP", this, "_TEMP_")
        SELECT PERASS, MAX(UNIMIS), SUM(FABLOR), SUM(ORDFOR), SUM(ODPSUG), SUM(ODPCON), SUM(ODPTOT) ;
        FROM _TEMP_ GROUP BY PERASS ORDER BY PERASS INTO ARRAY BASE
        this.cNRecSel = _TALLY
        * --- Chiude Cursori
        if used("_TEMP_")
          USE IN _TEMP_
        endif
        if this.cNRecSel > 0
          * --- Array per PAB
          dimension Dettaglio(7, this.oParentObject.w_NumPer+this.PrimaColPer+1)
          for this.i=1 to 7
          for this.k=this.PrimaColPer to this.oParentObject.w_NumPer+this.PrimaColPer+1
          Dettaglio(this.i,this.k) = 0
          next
          next
          * --- Prepara array ...
          dimension Descrizioni(7)
          Descrizioni(1)=ah_Msgformat("Giacenza fisica")
          Descrizioni(2)=ah_Msgformat("Fabbisogno lordo")
          Descrizioni(3)=ah_Msgformat("Ordini fornitore")
          Descrizioni(4)=ah_Msgformat("ODL/OCL/ODA suggeriti")
          Descrizioni(5)=ah_Msgformat("Ordini confermati/lanciati")
          Descrizioni(6)=ah_Msgformat("ODL/OCL/ODA TOTALI")
          Descrizioni(7)=ah_Msgformat("PAB (DisponibilitÓ nel tempo)")
          * --- Cicla sulle righe
          this.TmpN = alen(Base,1)
          this.i = 2
          * --- Cerca unitÓ di misura
          do while empty(Base(1,2)) and this.i<=this.TmpN
            Base(1,2)=Base(this.i,2)
            this.i = this.i + 1
          enddo
          for this.i=1 to 6
          * --- Descrizione e UM
          Dettaglio(this.i,1) = Descrizioni(this.i)
          Dettaglio(this.i, 2) = Base(1,2)
          if this.i=1
            * --- Caso particolare, la giacenza
            Dettaglio(this.i, this.PrimaColPer+1) = this.w_ONHAND
          else
            * --- Cicla su tutti i periodi presenti, per ogni singola voce
            for this.k=1 to this.TmpN
            * --- Legge il periodo assoluto al quale il dato fa riferimento ...
            this.tCURPER = Base(this.k, 1)
            * --- Determina indice della colonna sul quale deve essere posizionato il dato ...
            this.y = int(val(this.tCurPer)) + (this.PrimaColPer+1)
            * --- Aggiorna vettore
            Dettaglio(this.i,this.y) = Base(this.k, this.i+1)
            next
          endif
          next
          * --- Determina PAB
          Dettaglio(7, 1) = Descrizioni(7)
          Dettaglio(7, 2) = Base(1,2)
          this.y = this.PrimaColPer
          Dettaglio(7, this.PrimaColPer) = this.w_ONHAND + Dettaglio(6, this.y) + Dettaglio(3, this.y) - Dettaglio(2, this.y)
          for this.y=this.PrimaColPer+1 to this.PrimaColPer+this.oParentObject.w_NumPer + 1
          Dettaglio(7, this.y) = Dettaglio(7, this.y-1) + Dettaglio(6, this.y) + Dettaglio(3, this.y) - Dettaglio(2, this.y)
          next
          * --- Riempie cursore zoom ...
          select (this.cCursDett)
          append from array Dettaglio
          go top
          * --- Rilascia vettori ...
          release Dettaglio,Base,Descrizioni
        endif
        * --- Rinfresca grid
        wait CLEAR
        this.PUNPAD.w_DettODP.GRD.RecordSource = this.cCursDett
        this.PUNPAD.w_DettODP.cCursor = this.cCursDett
        this.PUNPAD.w_DettODP.Refresh()
        USE IN SELECT(this.w_NewCursDett)
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gestione ODL/OCL
    * --- Legge ODL/OCL selezionato
    this.oParentObject.w_ODPSEL = Nvl(this.PUNPAD.w_PeriodoODP.GetVar("OLCODODL"),Space(20))
    * --- Legge magazzino default
    this.tMAGDEF = " "
    this.tCAUORD = " "
    this.tCAUIMP = " "
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPMAGPRO,PPCAUORD,PPCAUIMP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPMAGPRO,PPCAUORD,PPCAUIMP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.tMAGDEF = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.tCAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.tCAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Flags causale
    if not empty(this.tCAUORD)
      this.tOFLORD = " "
      this.tOFLIMP = " "
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLORDI,CMFLIMPE"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.tCAUORD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLORDI,CMFLIMPE;
          from (i_cTable) where;
              CMCODICE = this.tCAUORD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.tOFLORD = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.tOFLIMP = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Definisce e chiama gestione
    do case
      case this.oParentObject.w_PARAM="O"
        this.GestODP = GSCO_AOP(this.oParentObject, "L")
      case this.oParentObject.w_PARAM="L"
        this.GestODP = GSCO_AOP(this.oParentObject, "Z")
      case this.oParentObject.w_PARAM="C"
        this.GestODP = GSCO_AOP(this.oParentObject, "P")
      case this.oParentObject.w_PARAM="F"
        this.GestODP = GSCO_AOP(this.oParentObject, "F")
      case this.oParentObject.w_PARAM="T"
        this.GestODP = GSCO_AOP(this.oParentObject, "T")
      otherwise
        this.GestODP = GSCO_AOP(this.oParentObject, "E")
    endcase
    * --- Controllo se ha passato il test di accesso
    if !(this.GestODP.bSec1)
      i_retcode = 'stop'
      return
    endif
    do case
      case Scelta = "CARICA_ODP"
        * --- Primo giorno periodo e primo giorno lavorativo
        this.tDFINE = cp_CharToDate("  -  -  ")
        this.tCODFOR = SPACE(15)
        this.w_DESFOR = SPACE(40)
        this.w_MAGTER = SPACE(5)
        * --- Read from MPS_TPER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MPS_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TPDATFIN"+;
            " from "+i_cTable+" MPS_TPER where ";
                +"TPPERASS = "+cp_ToStrODBC(this.oParentObject.w_PSELA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TPDATFIN;
            from (i_cTable) where;
                TPPERASS = this.oParentObject.w_PSELA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tDFINE = NVL(cp_ToDate(_read_.TPDATFIN),cp_NullValue(_read_.TPDATFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Prepara dati ...
        this.tMPCODICE = this.oParentObject.w_CODSEL
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.tMPCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
            from (i_cTable) where;
                CACODICE = this.tMPCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tDESCOD = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.tMPCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.tUNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.tOPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          this.tMOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Dati articolo
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDESART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.tMPCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDESART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2;
            from (i_cTable) where;
                ARCODART = this.tMPCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
          this.tUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.tOPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.tMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.tUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRLOTRIO,PRQTAMIN,PRGIOAPP,PRLEAMPS,PRCOEFLT,PRLOTMED,PRCODFOR,PRSAFELT"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.tMPCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRLOTRIO,PRQTAMIN,PRGIOAPP,PRLEAMPS,PRCOEFLT,PRLOTMED,PRCODFOR,PRSAFELT;
            from (i_cTable) where;
                PRCODART = this.tMPCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tLOTPRO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
          this.tLOTECO = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
          this.tLEAFIS = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
          this.tMPLEATIM = NVL(cp_ToDate(_read_.PRLEAMPS),cp_NullValue(_read_.PRLEAMPS))
          this.tCOEFLT = NVL(cp_ToDate(_read_.PRCOEFLT),cp_NullValue(_read_.PRCOEFLT))
          this.tLOTMED = NVL(cp_ToDate(_read_.PRLOTMED),cp_NullValue(_read_.PRLOTMED))
          this.tCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
          this.tSAFELT = NVL(cp_ToDate(_read_.PRSAFELT),cp_NullValue(_read_.PRSAFELT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDESCRI,ANMAGTER"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC("F");
                +" and ANCODICE = "+cp_ToStrODBC(this.tCODFOR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDESCRI,ANMAGTER;
            from (i_cTable) where;
                ANTIPCON = "F";
                and ANCODICE = this.tCODFOR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESFOR = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
          this.w_MAGTER = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Calcola data inizio MPS
        this.tMPDATINI = COCALCLT(this.tDFine, this.tMPLEATIM, "I", .F., "")
        this.tCODFOR = IIF(this.oParentObject.w_PARAM$"L-E", this.tCODFOR, SPACE(15))
        this.tTEMLAV = COSETLT(this.tMPCODART, IIF(this.oParentObject.w_PARAM="L", "L", "I"), 0, this.tCODFOR, this.tDFine)
        * --- Ricalcola data di inizio (Produzione)
        this.tDINRIC = COCALCLT(this.tDFine, this.tTEMLAV, "I", .F., "")
        this.tDTCON = COCALCLT(this.tDFine,this.tSAFELT,"A", .F. ,"")
        * --- Lancia Caricamento ...
        this.GestODP.ECPLoad()     
        if this.oParentObject.w_PARAM $ "C-F-T"
          this.GestODP.w_OLTSTATO = "C"
        else
          this.GestODP.w_OLTSTATO = "P"
        endif
        this.GestODP.w_OLDATODP = i_DATSYS
        this.GestODP.w_OLOPEODP = i_CODUTE
        this.GestODP.w_OLTCODIC = this.tMPCODICE
        this.GestODP.w_DESCOD = this.tDESCOD
        this.GestODP.w_OLTCOART = this.tMPCODART
        this.GestODP.w_DESART = this.tDESART
        this.GestODP.w_OLTUNMIS = IIF(NOT EMPTY(this.tUNMIS3) AND this.tMOLTI3<>0, this.tUNMIS3, this.tUNMIS1)
        this.GestODP.w_UNMIS1 = this.tUNMIS1
        this.GestODP.w_UNMIS2 = this.tUNMIS2
        this.GestODP.w_UNMIS3 = this.tUNMIS3
        this.GestODP.w_OPERAT = this.tOPERAT
        this.GestODP.w_OPERA3 = this.tOPERA3
        this.GestODP.w_MOLTIP = this.tMOLTIP
        this.GestODP.w_MOLTI3 = this.tMOLTI3
        this.GestODP.w_OLTKEYSA = this.tMPCODART
        this.GestODP.w_STATOGES = "LOAD"
        this.GestODP.w_LEAFIS = this.tLEAFIS
        this.GestODP.w_LOTMED = this.tLOTMED
        this.GestODP.w_LEAVAR = this.tCOEFLT
        this.GestODP.w_OLTLEMPS = this.tMPLEATIM
        this.GestODP.w_OLTEMLAV = this.tTEMLAV
        this.GestODP.w_OLTDTMPS = this.tMPDATINI
        this.GestODP.w_OLTDINRIC = this.tDINRIC
        this.GestODP.w_OLTDTRIC = this.tDFine
        this.GestODP.w_OLTDTCON = this.tDTCON
        this.GestODP.w_OLTSAFLT = this.tSAFELT
        this.GestODP.w_CODART = this.tMPCODART
        this.GestODP.w_LOTECO = this.tLOTECO
        this.GestODP.w_LOTRIO = this.tLOTPRO
        this.GestODP.w_OLTCOMAG = IIF(EMPTY(this.tMAGDEF), g_MAGAZI, this.tMAGDEF)
        this.GestODP.w_CAUORD = this.tCAUORD
        this.GestODP.w_CAUIMP = this.tCAUIMP
        this.GestODP.w_OLTCAMAG = this.tCAUORD
        this.GestODP.w_OLTFLORD = this.tOFLORD
        this.GestODP.w_OLTFLIMP = this.tOFLIMP
        do case
          case this.oParentObject.w_PARAM="L"
            this.GestODP.w_OLTPROVE = "L"
          case this.oParentObject.w_PARAM="O"
            this.GestODP.w_OLTPROVE = "I"
          otherwise
            this.GestODP.w_OLTPROVE = "E"
        endcase
        * --- Assegna il giusto periodo 
        this.GestODP.w_OLTPERAS = "   "
        do case
          case this.GestODP.w_OLTPROVE$"L-E"
            * --- Se provenienza conto lavoro assegno fornitore
            this.GestODP.w_OLTCOFOR = this.tCODFOR
            this.GestODP.w_DESFOR = this.w_DESFOR
            if this.GestODP.w_OLTPROVE="L"
              this.GestODP.w_MAGTER = this.w_MAGTER
            endif
        endcase
        * --- Se oggetto MPS determina periodo MPS
        this.w_DATRIF = this.tDFine
        * --- Determina periodo di appartenenza dell'ODL/OCL
        vq_exec ("..\COLA\EXE\QUERY\GSCO_BOL", this, "__Temp__")
        if used("__Temp__")
          * --- Preleva periodo assoluto
          select __Temp__
          go top
          this.GestODP.w_OLTPERAS = __Temp__.TPPERASS
          * --- Chiude cursore
          USE IN __Temp__
        endif
        this.GestODP.mCalc(.T.)     
      case Scelta="VISUALIZZA_ODP" or Scelta="MODIFICA_ODP" or Scelta="ELIMINA_ODP"
        * --- Carica record e lascia in interrograzione ...
        this.GestODP.w_OLCODODL = this.oParentObject.w_ODPSEL
        this.TMPc = "OLCODODL="+cp_ToStrODBC(this.oParentObject.w_ODPSEL)
        this.GestODP.QueryKeySet(this.TmpC,"")     
        this.GestODP.LoadRecWarn()     
        do case
          case Scelta = "MODIFICA_ODP"
            * --- Modifica record
            this.GestODP.ECPEdit()     
          case Scelta = "ELIMINA_ODP"
            * --- Cancella record ...
            this.GestODP.ECPDelete()     
        endcase
    endcase
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce Menu
    DEFINE POPUP MENUMPS from MRow()+1,MCol()+1 shortcut margin
    DEFINE BAR 1 OF MENUMPS prompt ah_Msgformat("Visualizza") style "B"
    DEFINE BAR 2 OF MENUMPS prompt ah_Msgformat("Modifica")
    DEFINE BAR 3 OF MENUMPS prompt ah_Msgformat("Carica")
    DEFINE BAR 4 OF MENUMPS prompt "\-"
    DEFINE BAR 5 OF MENUMPS prompt ah_Msgformat("Elimina")
    ON SELE BAR 1 OF MENUMPS Scelta="VISUALIZZA_ODP"
    ON SELE BAR 2 OF MENUMPS Scelta="MODIFICA_ODP"
    ON SELE BAR 3 OF MENUMPS Scelta="CARICA_ODP"
    ON SELE BAR 5 OF MENUMPS Scelta="ELIMINA_ODP"
    ACTI POPUP MENUMPS
    DEACTIVATE POPUP MENUMPS
    RELEASE POPUPS MENUMPS EXTENDED
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica Temporale
    * --- ODL/OCL in ritardo
    this.w_DATSCA = i_DATSYS
    do case
      case this.oParentObject.w_PARAM="O"
        vq_exec("..\COLA\EXE\QUERY\GSCOOVER", this, "_Ritardo_")
      case this.oParentObject.w_PARAM="L"
        vq_exec("..\COLA\EXE\QUERY\GSCOLVER", this, "_Ritardo_")
      case this.oParentObject.w_PARAM="C"
        vq_exec("..\COLA\EXE\QUERY\GSCOCVER", this, "_Ritardo_")
      case this.oParentObject.w_PARAM="F"
        vq_exec("..\COLA\EXE\QUERY\GSCOFVER", this, "_Ritardo_")
      case this.oParentObject.w_PARAM="T"
        vq_exec("..\COLA\EXE\QUERY\GSCOTVER", this, "_Ritardo_")
      otherwise
        vq_exec("..\COLA\EXE\QUERY\GSCOEVER", this, "_Ritardo_")
    endcase
    scan for not empty(nvl(perass, " "))
    this.w_RCODICE = CODICE
    PeriodoAss = PERASS
    Select (this.cCursor)
    Locate for CACODICE=this.w_RCODICE
    if found()
      replace COL_&PeriodoAss with this.oParentObject.w_PPCOLMCO
    endif
    Endscan
    if used("_Ritardo_")
      use in _Ritardo_
    endif
    Select (this.cCursor)
    go top
    this.GRID.Refresh()     
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna cursore zoom dettaglio
    * --- Parametri per query GSDBFBRI
    this.w_NewCursDett = this.cCursDETT
    this.cCursDETT = SYS(2015)
    SELECT * FROM (this.w_NewCursDett) INTO CURSOR (this.cCursDETT) WHERE 1=0 READWRITE
    * --- Codice Selezionato
    this.oParentObject.w_CODICE = Nvl(this.PUNPAD.w_ZoomODP.GetVar("CACODICE"),Space(20))
    if NOT EMPTY(NVL(this.oParentObject.w_CODICE,""))
      * --- Sistema Intestazione:
      this.DETT.column1.hdr.caption = iif(empty(this.oParentObject.w_CODICE)," ", AH_MsgFormat("Dett.: %1", Alltrim(this.oParentObject.w_CODICE)))
      lCurs = this.PUNPAD.w_ZoomODP.cCursor
      this.pCODICE = &lCurs..CACODICE
      * --- Lettura w_KEYSAL
      this.w_KEYSAL = SPACE(20)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_KEYSAL = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_KEYSAL)
        * --- Legge Dato Giacenza Fisica ...
        this.w_ONHAND = 0
        vq_exec("..\COLA\EXE\QUERY\GSCO0BMP", this, "_TempGiac_")
        * --- Memorizza Giacenza
        this.w_ONHAND = NVL(_TempGiac_.QTASAL, 0)
        * --- Chiude Cursore
        use in _TempGiac_
        ah_msg("Lettura dati per visualizzazione dettaglio in corso...")
        this.pTIPGES = " "
        this.pDETTMPS = "S"
        if this.oParentObject.w_PARAM="T"
          this.w_ELAODF = "S"
          this.w_ELAMPS = "N"
        else
          this.w_ELAMPS = "S"
          this.w_ELAODF = "N"
        endif
        * --- Cursore con gli ORDINI CLIENTE da documenti (SOLO MAGAZZINI PRODUZIONE)
        this.w_CODINI = this.oParentObject.w_CODICE
        this.w_CODFIN = this.oParentObject.w_CODICE
        vq_exec("..\COLA\exe\QUERY\GSDBFBRI1", this, "__Temp__")
        select PERASS, (this.oParentObject.w_UMARTSE) as UNMIS1, PREVIS, ORDINI, FABBIS, DOMANDA, ; 
 (MPSCON+MPSDAP+MPSPIA+MPSLAN) as MPSCON, ORDFOR, 0 as GIACEN, ; 
 MPSSUG, (MPSSUG+MPSCON+MPSDAP+MPSPIA+MPSLAN) as MPSTOT, PRTIPDTF, PRGIODTF ; 
 from __Temp__ order by PERASS into cursor _Globale_
        * --- Rende scrivibile _Globale_
        xx=WRCursor("_Globale_")
        * --- Chiude Cursori
        if used("__Temp__")
          USE IN __Temp__
        endif
        if False
          * --- Calcola il DTF in modo da tenerne conto nella visualizzazione dei dettagli
          if this.oParentObject.w_cPERDTF = "000" and this.oParentObject.w_PP___DTF>0
            * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
            this.TmpD = COCALCLT(i_DATSYS, this.oParentObject.w_PP___DTF, "A", .T., "")
            if not empty(this.TmpD)
              * --- Select from MPS_TPER
              i_nConn=i_TableProp[this.MPS_TPER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
                    +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" and "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
                     ,"_Curs_MPS_TPER")
              else
                select * from (i_cTable);
                 where TPDATINI<=this.TmpD and this.TmpD<=TPDATFIN;
                  into cursor _Curs_MPS_TPER
              endif
              if used('_Curs_MPS_TPER')
                select _Curs_MPS_TPER
                locate for 1=1
                do while not(eof())
                this.oParentObject.w_cPERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
                  select _Curs_MPS_TPER
                  continue
                enddo
                use
              endif
            endif
          endif
        endif
        * --- Esegue scan inversa cursore
        ah_msg("Determinazione domanda di periodo in corso...")
        this.cSUMCUMRES = 0
        this.oParentObject.w_cPERDTF = "000"
        select _Globale_
        go botto
        this.cTIPDTF = nvl(_Globale_.PRTIPDTF, "E")
        this.cGIODTF = nvl(_Globale_.PRGIODTF, this.oParentObject.w_PP___DTF)
        if this.oParentObject.w_PPTIPDTF="F" or (this.oParentObject.w_PPTIPDTF<>"F" AND this.cTIPDTF="E")
          if this.oParentObject.w_cPERDTF = "000" and this.oParentObject.w_PP___DTF>0
            * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
            this.TmpD = COCALCLT(i_DATSYS, this.oParentObject.w_PP___DTF, "A", .T., "")
            if not empty(this.TmpD)
              * --- Select from MPS_TPER
              i_nConn=i_TableProp[this.MPS_TPER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
                    +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" and "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
                     ,"_Curs_MPS_TPER")
              else
                select * from (i_cTable);
                 where TPDATINI<=this.TmpD and this.TmpD<=TPDATFIN;
                  into cursor _Curs_MPS_TPER
              endif
              if used('_Curs_MPS_TPER')
                select _Curs_MPS_TPER
                locate for 1=1
                do while not(eof())
                this.oParentObject.w_cPERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
                  select _Curs_MPS_TPER
                  continue
                enddo
                use
              endif
            endif
          endif
        else
          * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
          if this.oParentObject.w_cPERDTF = "000" and this.cGIODTF>0
            this.TmpD = COCALCLT(i_DATSYS,this.cGIODTF,"A",TRUE,"")
            if not empty(this.TmpD)
              * --- Select from MPS_TPER
              i_nConn=i_TableProp[this.MPS_TPER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select TPPERASS  from "+i_cTable+" MPS_TPER ";
                    +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" AND "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
                     ,"_Curs_MPS_TPER")
              else
                select TPPERASS from (i_cTable);
                 where TPDATINI<=this.TmpD AND this.TmpD<=TPDATFIN;
                  into cursor _Curs_MPS_TPER
              endif
              if used('_Curs_MPS_TPER')
                select _Curs_MPS_TPER
                locate for 1=1
                do while not(eof())
                this.oParentObject.w_cPERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
                  select _Curs_MPS_TPER
                  continue
                enddo
                use
              endif
            endif
          endif
        endif
        select _Globale_
        do while not bof()
          * --- ESAMINA UN SOLO CODICE
          this.cPERASS = nvl(_Globale_.PERASS, space(3))
          * --- Legge Previsioni e ordini ...
          this.cPREVIS = iif(this.cPERASS>this.oParentObject.w_cPERDTF, nvl(_Globale_.PREVIS,0), 0)
          this.cORDINI = nvl(_Globale_.ORDINI,0)+nvl(_Globale_.FABBIS,0)
          if this.cPREVIS + this.cORDINI <> 0
            * --- Calcola Residuo
            this.cRESPRE = this.cPREVIS - this.cORDINI - this.cSUMCUMRES
            if this.cRESPRE < 0
              this.cSUMCUMRES = - this.cRESPRE
              this.cRESPRE = 0
            else
              this.cSUMCUMRES = 0
            endif
            * --- Domanda ...
            this.cDOMANDA = this.cORDINI + this.cRESPRE
            if this.cDOMANDA > 0
              replace DOMANDA with this.cDOMANDA
            endif
          endif
          * --- Passa al record precedente
          if not bof()
            skip -1
          endif
        enddo
        * --- Copia il risultato della query in un array
        select * from _Globale_ into array Base
        this.cNRecSel = _TALLY
        * --- Chiude cursore ...
        USE IN _Globale_
        if this.cNRecSel > 0
          * --- Array per PAB
          dimension Dettaglio(10,this.oParentObject.w_NumPer+this.PrimaColPer+1)
          for this.i=4 to 9
          for this.k=1 to this.oParentObject.w_NumPer+this.PrimaColPer+1
          Dettaglio(this.i,this.k) = 0
          next
          next
          * --- Azzera previsioni del periodo scaduto
          if Base(1,1) == "000"
            Base(1,3) = 0
          endif
          * --- Prepara array ...
          dimension Descrizioni(10)
          Descrizioni(1)=AH_Msgformat("Previsioni di vendita")
          Descrizioni(2)=AH_Msgformat("Ordini cliente")
          Descrizioni(3)=AH_Msgformat("Fabbisogno interno")
          Descrizioni(4)=AH_Msgformat("DOMANDA")
          Descrizioni(5)=AH_Msgformat("Ordini confermati/pianificati/lanciati")
          Descrizioni(6)=AH_Msgformat("Ordini fornitore")
          Descrizioni(7)=AH_Msgformat("Giacenza fisica")
          Descrizioni(8)=AH_Msgformat("Ordini suggeriti")
          Descrizioni(9)=AH_Msgformat("TOTALE")
          Descrizioni(10)=AH_Msgformat("PAB (disponibilitÓ nel tempo)")
          * --- Cicla sulle righe
          this.TmpN = alen(Base,1)
          for this.i=1 to 9
          * --- Descrizione e UM
          Dettaglio(this.i,1) = Descrizioni(this.i)
          Dettaglio(this.i,2) = Base(1,2)
          if this.i=7
            * --- Caso particolare, la giacenza
            Dettaglio(this.i,4) = this.w_ONHAND
          else
            * --- Cicla su tutti i periodi presenti, per ogni singola voce (previsioni, ordini, ...)
            for this.k=1 to this.TmpN
            * --- Legge il periodo assoluto al quale il dato fa riferimento ...
            this.tCURPER = Base(this.k,1)
            * --- Determina indice della colonna sul quale deve essere posizionato il dato ...
            this.y = int(val(this.tCurPer)) + this.PrimaColPer+1
            * --- Aggiorna vettore
            Dettaglio(this.i,this.y) = Base(this.k,this.i+this.PrimaColPer-1)
            next
          endif
          next
          * --- Determina PAB
          Dettaglio(10,1) = Descrizioni(10)
          Dettaglio(10,2) = Base(1,2)
          for this.y=this.PrimaColPer+1 to this.PrimaColPer+this.oParentObject.w_NumPer+1
          Dettaglio(10,this.y) = iif(this.y=this.PrimaColPer+1, this.w_ONHAND, Dettaglio(10,this.y-1)) + Dettaglio(6,this.y) + Dettaglio(9,this.y) - Dettaglio(4,this.y)
          next
          * --- Riempie cursore zoom ...
          select (this.cCursDett)
          append from array Dettaglio
          go top
          * --- Rilascia vettori ...
          release Dettaglio,Base,Descrizioni
        endif
        * --- Rinfresca grid
        wait CLEAR
        this.DETT.RecordSource = this.cCursDett
        this.PunPAD.w_DettODP.cCursor = this.cCursDett
        this.DETT.refresh()     
        USE IN SELECT(this.w_NewCursDett)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='MPS_TPER'
    this.cWorkTables[3]='PAR_PROD'
    this.cWorkTables[4]='CAM_AGAZ'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='PAR_RIOR'
    this.cWorkTables[7]='CONTI'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
