* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mcl                                                        *
*              Ciclo di lavorazione                                            *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-03                                                      *
* Last revis.: 2015-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mcl")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mcl")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mcl")
  return

* --- Class definition
define class tgsco_mcl as StdPCForm
  Width  = 849
  Height = 463
  Top    = 10
  Left   = 1
  cComment = "Ciclo di lavorazione"
  cPrg = "gsco_mcl"
  HelpContextID=171334295
  add object cnt as tcgsco_mcl
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mcl as PCContext
  w_CLCODODL = space(15)
  w_CHECKFORM = space(1)
  w_FASEVAR = space(1)
  w_CLROWORD = 0
  w_CL__FASE = space(5)
  w_CLDESFAS = space(40)
  w_CLINDPRE = space(2)
  w_CLROWNUM = 0
  w_DESCFA = space(40)
  w_EDITROW = space(1)
  w_EDITCHK = space(1)
  w_CLPREUM1 = 0
  w_CLAVAUM1 = 0
  w_CLSCAUM1 = 0
  w_CLFASEVA = space(1)
  w_CLCOUPOI = space(1)
  w_CLULTFAS = space(1)
  w_CLFASSEL = space(1)
  w_UM1 = space(3)
  w_CLCPRIFE = 0
  w_CLQTAPRE = 0
  w_CLQTAAVA = 0
  w_CLQTASCA = 0
  w_CLWIPOUT = space(20)
  w_DESWIPO = space(40)
  w_CLDATINI = space(8)
  w_CLDATFIN = space(8)
  w_CLWIPFPR = space(20)
  w_DESWIPI = space(40)
  w_CLBFRIFE = 0
  w_CLCODERR = space(10)
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_CLFASCOS = space(1)
  w_CLFASOUT = space(1)
  w_TESTROW = space(10)
  w_NRVARIATA = 0
  w_CLFASCLA = space(1)
  w_EDITALT = space(1)
  w_CLPRIFAS = space(1)
  w_CLLTFASE = 0
  w_CLCODFAS = space(20)
  w_FASDES = space(40)
  w_CLWIPOUT = space(66)
  w_CLCFASPR = space(66)
  w_FLTSTAOL = space(1)
  w_FLQTAEVA = 0
  w_FLTPROVE = space(1)
  w_UPDATED = space(1)
  w_CANCHROW = space(1)
  w_CANDEAROW = space(1)
  w_ROWSTATUS = space(1)
  w_CPROWDEL = 0
  w_ROWORDEL = 0
  w_FLFASEVA = space(1)
  w_FLCOUPOI = space(1)
  w_FLULTFAS = space(1)
  w_FLFASSEL = space(1)
  w_FLFASOUT = space(1)
  w_FLFASCLA = space(1)
  w_FLPRIFAS = space(1)
  w_FLFASCOS = space(1)
  w_CLWIPDES = space(5)
  w_CLMAGWIP = space(5)
  w_CLSEOLFA = space(15)
  w_CLSEOLFA = space(15)
  proc Save(i_oFrom)
    this.w_CLCODODL = i_oFrom.w_CLCODODL
    this.w_CHECKFORM = i_oFrom.w_CHECKFORM
    this.w_FASEVAR = i_oFrom.w_FASEVAR
    this.w_CLROWORD = i_oFrom.w_CLROWORD
    this.w_CL__FASE = i_oFrom.w_CL__FASE
    this.w_CLDESFAS = i_oFrom.w_CLDESFAS
    this.w_CLINDPRE = i_oFrom.w_CLINDPRE
    this.w_CLROWNUM = i_oFrom.w_CLROWNUM
    this.w_DESCFA = i_oFrom.w_DESCFA
    this.w_EDITROW = i_oFrom.w_EDITROW
    this.w_EDITCHK = i_oFrom.w_EDITCHK
    this.w_CLPREUM1 = i_oFrom.w_CLPREUM1
    this.w_CLAVAUM1 = i_oFrom.w_CLAVAUM1
    this.w_CLSCAUM1 = i_oFrom.w_CLSCAUM1
    this.w_CLFASEVA = i_oFrom.w_CLFASEVA
    this.w_CLCOUPOI = i_oFrom.w_CLCOUPOI
    this.w_CLULTFAS = i_oFrom.w_CLULTFAS
    this.w_CLFASSEL = i_oFrom.w_CLFASSEL
    this.w_UM1 = i_oFrom.w_UM1
    this.w_CLCPRIFE = i_oFrom.w_CLCPRIFE
    this.w_CLQTAPRE = i_oFrom.w_CLQTAPRE
    this.w_CLQTAAVA = i_oFrom.w_CLQTAAVA
    this.w_CLQTASCA = i_oFrom.w_CLQTASCA
    this.w_CLWIPOUT = i_oFrom.w_CLWIPOUT
    this.w_DESWIPO = i_oFrom.w_DESWIPO
    this.w_CLDATINI = i_oFrom.w_CLDATINI
    this.w_CLDATFIN = i_oFrom.w_CLDATFIN
    this.w_CLWIPFPR = i_oFrom.w_CLWIPFPR
    this.w_DESWIPI = i_oFrom.w_DESWIPI
    this.w_CLBFRIFE = i_oFrom.w_CLBFRIFE
    this.w_CLCODERR = i_oFrom.w_CLCODERR
    this.w_CLQTADIC = i_oFrom.w_CLQTADIC
    this.w_CLDICUM1 = i_oFrom.w_CLDICUM1
    this.w_CLFASCOS = i_oFrom.w_CLFASCOS
    this.w_CLFASOUT = i_oFrom.w_CLFASOUT
    this.w_TESTROW = i_oFrom.w_TESTROW
    this.w_NRVARIATA = i_oFrom.w_NRVARIATA
    this.w_CLFASCLA = i_oFrom.w_CLFASCLA
    this.w_EDITALT = i_oFrom.w_EDITALT
    this.w_CLPRIFAS = i_oFrom.w_CLPRIFAS
    this.w_CLLTFASE = i_oFrom.w_CLLTFASE
    this.w_CLCODFAS = i_oFrom.w_CLCODFAS
    this.w_FASDES = i_oFrom.w_FASDES
    this.w_CLWIPOUT = i_oFrom.w_CLWIPOUT
    this.w_CLCFASPR = i_oFrom.w_CLCFASPR
    this.w_FLTSTAOL = i_oFrom.w_FLTSTAOL
    this.w_FLQTAEVA = i_oFrom.w_FLQTAEVA
    this.w_FLTPROVE = i_oFrom.w_FLTPROVE
    this.w_UPDATED = i_oFrom.w_UPDATED
    this.w_CANCHROW = i_oFrom.w_CANCHROW
    this.w_CANDEAROW = i_oFrom.w_CANDEAROW
    this.w_ROWSTATUS = i_oFrom.w_ROWSTATUS
    this.w_CPROWDEL = i_oFrom.w_CPROWDEL
    this.w_ROWORDEL = i_oFrom.w_ROWORDEL
    this.w_FLFASEVA = i_oFrom.w_FLFASEVA
    this.w_FLCOUPOI = i_oFrom.w_FLCOUPOI
    this.w_FLULTFAS = i_oFrom.w_FLULTFAS
    this.w_FLFASSEL = i_oFrom.w_FLFASSEL
    this.w_FLFASOUT = i_oFrom.w_FLFASOUT
    this.w_FLFASCLA = i_oFrom.w_FLFASCLA
    this.w_FLPRIFAS = i_oFrom.w_FLPRIFAS
    this.w_FLFASCOS = i_oFrom.w_FLFASCOS
    this.w_CLWIPDES = i_oFrom.w_CLWIPDES
    this.w_CLMAGWIP = i_oFrom.w_CLMAGWIP
    this.w_CLSEOLFA = i_oFrom.w_CLSEOLFA
    this.w_CLSEOLFA = i_oFrom.w_CLSEOLFA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CLCODODL = this.w_CLCODODL
    i_oTo.w_CHECKFORM = this.w_CHECKFORM
    i_oTo.w_FASEVAR = this.w_FASEVAR
    i_oTo.w_CLROWORD = this.w_CLROWORD
    i_oTo.w_CL__FASE = this.w_CL__FASE
    i_oTo.w_CLDESFAS = this.w_CLDESFAS
    i_oTo.w_CLINDPRE = this.w_CLINDPRE
    i_oTo.w_CLROWNUM = this.w_CLROWNUM
    i_oTo.w_DESCFA = this.w_DESCFA
    i_oTo.w_EDITROW = this.w_EDITROW
    i_oTo.w_EDITCHK = this.w_EDITCHK
    i_oTo.w_CLPREUM1 = this.w_CLPREUM1
    i_oTo.w_CLAVAUM1 = this.w_CLAVAUM1
    i_oTo.w_CLSCAUM1 = this.w_CLSCAUM1
    i_oTo.w_CLFASEVA = this.w_CLFASEVA
    i_oTo.w_CLCOUPOI = this.w_CLCOUPOI
    i_oTo.w_CLULTFAS = this.w_CLULTFAS
    i_oTo.w_CLFASSEL = this.w_CLFASSEL
    i_oTo.w_UM1 = this.w_UM1
    i_oTo.w_CLCPRIFE = this.w_CLCPRIFE
    i_oTo.w_CLQTAPRE = this.w_CLQTAPRE
    i_oTo.w_CLQTAAVA = this.w_CLQTAAVA
    i_oTo.w_CLQTASCA = this.w_CLQTASCA
    i_oTo.w_CLWIPOUT = this.w_CLWIPOUT
    i_oTo.w_DESWIPO = this.w_DESWIPO
    i_oTo.w_CLDATINI = this.w_CLDATINI
    i_oTo.w_CLDATFIN = this.w_CLDATFIN
    i_oTo.w_CLWIPFPR = this.w_CLWIPFPR
    i_oTo.w_DESWIPI = this.w_DESWIPI
    i_oTo.w_CLBFRIFE = this.w_CLBFRIFE
    i_oTo.w_CLCODERR = this.w_CLCODERR
    i_oTo.w_CLQTADIC = this.w_CLQTADIC
    i_oTo.w_CLDICUM1 = this.w_CLDICUM1
    i_oTo.w_CLFASCOS = this.w_CLFASCOS
    i_oTo.w_CLFASOUT = this.w_CLFASOUT
    i_oTo.w_TESTROW = this.w_TESTROW
    i_oTo.w_NRVARIATA = this.w_NRVARIATA
    i_oTo.w_CLFASCLA = this.w_CLFASCLA
    i_oTo.w_EDITALT = this.w_EDITALT
    i_oTo.w_CLPRIFAS = this.w_CLPRIFAS
    i_oTo.w_CLLTFASE = this.w_CLLTFASE
    i_oTo.w_CLCODFAS = this.w_CLCODFAS
    i_oTo.w_FASDES = this.w_FASDES
    i_oTo.w_CLWIPOUT = this.w_CLWIPOUT
    i_oTo.w_CLCFASPR = this.w_CLCFASPR
    i_oTo.w_FLTSTAOL = this.w_FLTSTAOL
    i_oTo.w_FLQTAEVA = this.w_FLQTAEVA
    i_oTo.w_FLTPROVE = this.w_FLTPROVE
    i_oTo.w_UPDATED = this.w_UPDATED
    i_oTo.w_CANCHROW = this.w_CANCHROW
    i_oTo.w_CANDEAROW = this.w_CANDEAROW
    i_oTo.w_ROWSTATUS = this.w_ROWSTATUS
    i_oTo.w_CPROWDEL = this.w_CPROWDEL
    i_oTo.w_ROWORDEL = this.w_ROWORDEL
    i_oTo.w_FLFASEVA = this.w_FLFASEVA
    i_oTo.w_FLCOUPOI = this.w_FLCOUPOI
    i_oTo.w_FLULTFAS = this.w_FLULTFAS
    i_oTo.w_FLFASSEL = this.w_FLFASSEL
    i_oTo.w_FLFASOUT = this.w_FLFASOUT
    i_oTo.w_FLFASCLA = this.w_FLFASCLA
    i_oTo.w_FLPRIFAS = this.w_FLPRIFAS
    i_oTo.w_FLFASCOS = this.w_FLFASCOS
    i_oTo.w_CLWIPDES = this.w_CLWIPDES
    i_oTo.w_CLMAGWIP = this.w_CLMAGWIP
    i_oTo.w_CLSEOLFA = this.w_CLSEOLFA
    i_oTo.w_CLSEOLFA = this.w_CLSEOLFA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mcl as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 849
  Height = 463
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-10"
  HelpContextID=171334295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=66

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ODL_CICL_IDX = 0
  KEY_ARTI_IDX = 0
  COD_FASI_IDX = 0
  ART_ICOL_IDX = 0
  ODL_MAST_IDX = 0
  cFile = "ODL_CICL"
  cKeySelect = "CLCODODL"
  cKeyWhere  = "CLCODODL=this.w_CLCODODL"
  cKeyDetail  = "CLCODODL=this.w_CLCODODL"
  cKeyWhereODBC = '"CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)';

  cKeyDetailWhereODBC = '"CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ODL_CICL.CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ODL_CICL.w_CPROWORD'
  cPrg = "gsco_mcl"
  cComment = "Ciclo di lavorazione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CLCODODL = space(15)
  w_CHECKFORM = .F.
  w_FASEVAR = .F.
  w_CLROWORD = 0
  o_CLROWORD = 0
  w_CL__FASE = space(5)
  w_CLDESFAS = space(40)
  o_CLDESFAS = space(40)
  w_CLINDPRE = space(2)
  o_CLINDPRE = space(2)
  w_CLROWNUM = 0
  w_DESCFA = space(40)
  w_EDITROW = .F.
  o_EDITROW = .F.
  w_EDITCHK = .F.
  w_CLPREUM1 = 0
  w_CLAVAUM1 = 0
  w_CLSCAUM1 = 0
  w_CLFASEVA = space(1)
  w_CLCOUPOI = space(1)
  o_CLCOUPOI = space(1)
  w_CLULTFAS = space(1)
  w_CLFASSEL = space(1)
  o_CLFASSEL = space(1)
  w_UM1 = space(3)
  w_CLCPRIFE = 0
  w_CLQTAPRE = 0
  o_CLQTAPRE = 0
  w_CLQTAAVA = 0
  w_CLQTASCA = 0
  w_CLWIPOUT = space(20)
  w_DESWIPO = space(40)
  w_CLDATINI = ctod('  /  /  ')
  w_CLDATFIN = ctod('  /  /  ')
  w_CLWIPFPR = space(20)
  w_DESWIPI = space(40)
  w_CLBFRIFE = 0
  w_CLCODERR = space(10)
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_CLFASCOS = space(1)
  w_CLFASOUT = space(1)
  o_CLFASOUT = space(1)
  w_TESTROW = space(10)
  w_NRVARIATA = 0
  w_CLFASCLA = space(1)
  o_CLFASCLA = space(1)
  w_EDITALT = .F.
  w_CLPRIFAS = space(1)
  w_CLLTFASE = 0
  w_CLCODFAS = space(20)
  w_FASDES = space(40)
  w_CLWIPOUT = space(66)
  w_CLCFASPR = space(66)
  w_FLTSTAOL = space(1)
  w_FLQTAEVA = 0
  w_FLTPROVE = space(1)
  w_UPDATED = .F.
  w_CANCHROW = .F.
  w_CANDEAROW = .F.
  w_ROWSTATUS = space(1)
  w_CPROWDEL = 0
  w_ROWORDEL = 0
  w_FLFASEVA = space(1)
  w_FLCOUPOI = space(1)
  w_FLULTFAS = space(1)
  w_FLFASSEL = space(1)
  w_FLFASOUT = space(1)
  w_FLFASCLA = space(1)
  w_FLPRIFAS = space(1)
  w_FLFASCOS = space(1)
  w_CLWIPDES = space(5)
  w_CLMAGWIP = space(5)
  w_CLSEOLFA = space(15)
  w_CLSEOLFA = space(15)

  * --- Children pointers
  GSCO_MRL = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_mcl
  w_CANADDROW=.F.
  w_CANDELROW=.F.
  w_ISODLDIC=.f.
  
  Func cp_maxclroword()
    LOCAL i_a,i_r
    i_a=SELECT()
    SELECT MAX(t_clroword) as mo FROM alias() INTO CURSOR __maxord__
    i_r=__maxord__.mo
    use
    SELECT (i_a)
    RETURN (i_r)
  EndFunc  
   Func cp_maxclrownum()
    LOCAL i_a,i_r
    i_a=SELECT()
    SELECT MAX(t_clrownum) as mo FROM alias() INTO CURSOR __maxord__
    i_r=__maxord__.mo
    use
    SELECT (i_a)
    RETURN (i_r)
   EndFunc 
   
   * - Gestisce l'aggiunta di riga
   func CanAddRow()
  *       this.w_CANADDROW= this.oParentObject.w_TIPGES<>"Z" AND this.oParentObject.w_OLFLMODC="S"
         This.NotifyEvent("CanAddRow")
         Return ( this.w_CANADDROW )
   EndFunc
  
   * - Gestisce l'eliminazione di riga
   func CanDeleteRow()
         this.w_CANDELROW = inlist(this.cFunction,'Edit','Load') and this.oParentObject.w_TIPGES<>"Z" and this.w_EDITROW
         If this.cFunction='Edit'
           if this.w_CANDELROW
              This.NotifyEvent("CanDeleteRow")
             else
              this.w_CANDELROW=.F.
           endif
         endif
         Return ( this.w_CANDELROW )
   EndFunc
   
   
   
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCO_MRL additive
    with this
      .Pages(1).addobject("oPag","tgsco_mclPag1","gsco_mcl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCO_MRL
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='COD_FASI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='ODL_CICL'
    * --- Area Manuale = Open Work Table
    * --- gsco_mcl
    * colora le righe degli Articoli Alternativi
    mycolor="RGB(128+int(rand(16*abs(   (t_CLROWORD)))*128),;
          128+mod(42+int(rand(16*abs(   (t_CLROWORD)))*128),128),;
          128+mod(84+int(rand(16*abs(   (t_CLROWORD)))*128),128))))"
    
    * This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor=;
    *   "IIF(nvl(T_CLFASSEL,0)=0, RGB(224,223,227) , RGB(255,255,255),"+mycolor
       && IIF(nvl(t_CLFASSEP, 0)=0 , RGB(255,255,255),"+mycolor
    
    
    * colora le righe degli Articoli Alternativi
    mycolor="RGB(128+int(rand(16*abs(   (t_CLROWORD)))*128),;
          128+mod(42+int(rand(16*abs(   (t_CLROWORD)))*128),128),;
          128+mod(84+int(rand(16*abs(   (t_CLROWORD)))*128),128)))"
    
    * This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor=;
       *"IIF(nvl(t_CLROWORD,0)=0, RGB(255,255,255),"+mycolor
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ODL_CICL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ODL_CICL_IDX,3]
  return

  function CreateChildren()
    this.GSCO_MRL = CREATEOBJECT('stdDynamicChild',this,'GSCO_MRL',this.oPgFrm.Page1.oPag.oLinkPC_2_29)
    this.GSCO_MRL.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsco_mcl'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCO_MRL)
      this.GSCO_MRL.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCO_MRL.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCO_MRL.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCO_MRL)
      this.GSCO_MRL.DestroyChildrenChain()
      this.GSCO_MRL=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_29')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCO_MRL.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCO_MRL.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCO_MRL.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCO_MRL.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CLCODODL,"RLCODODL";
             ,.w_CPROWNUM,"RLROWNUM";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_21_joined
    link_2_21_joined=.f.
    local link_2_25_joined
    link_2_25_joined=.f.
    local link_2_40_joined
    link_2_40_joined=.f.
    local link_2_58_joined
    link_2_58_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ODL_CICL where CLCODODL=KeySet.CLCODODL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsco_mcl
      i_cOrder = ' order by CLROWORD '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2],this.bLoadRecFilter,this.ODL_CICL_IDX,"gsco_mcl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ODL_CICL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ODL_CICL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ODL_CICL '
      link_2_21_joined=this.AddJoinedLink_2_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_25_joined=this.AddJoinedLink_2_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_40_joined=this.AddJoinedLink_2_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_58_joined=this.AddJoinedLink_2_58(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CLCODODL',this.w_CLCODODL  )
      select * from (i_cTable) ODL_CICL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CHECKFORM = .T.
        .w_FASEVAR = .F.
        .w_UPDATED = .T.
        .w_CANCHROW = .f.
        .w_CANDEAROW = .f.
        .w_ROWSTATUS = space(1)
        .w_CPROWDEL = 0
        .w_ROWORDEL = 0
        .w_CLCODODL = NVL(CLCODODL,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ODL_CICL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCFA = space(40)
        .w_UM1 = this.oparentobject .w_OLTUNMIS
          .w_DESWIPO = space(40)
          .w_DESWIPI = space(40)
        .w_TESTROW = 'S'
          .w_NRVARIATA = 0
          .w_FASDES = space(40)
          .w_CLCFASPR = space(66)
          .w_FLTSTAOL = space(1)
          .w_FLQTAEVA = 0
          .w_FLTPROVE = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CLROWORD = NVL(CLROWORD,0)
          .w_CL__FASE = NVL(CL__FASE,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_CLDESFAS = NVL(CLDESFAS,space(40))
          .w_CLINDPRE = NVL(CLINDPRE,space(2))
          .w_CLROWNUM = NVL(CLROWNUM,0)
        .w_EDITROW = .w_EDITROW
        .w_EDITCHK = .w_EDITCHK
          .w_CLPREUM1 = NVL(CLPREUM1,0)
          .w_CLAVAUM1 = NVL(CLAVAUM1,0)
          .w_CLSCAUM1 = NVL(CLSCAUM1,0)
          .w_CLFASEVA = NVL(CLFASEVA,space(1))
          .w_CLCOUPOI = NVL(CLCOUPOI,space(1))
          .w_CLULTFAS = NVL(CLULTFAS,space(1))
          .w_CLFASSEL = NVL(CLFASSEL,space(1))
          .w_CLCPRIFE = NVL(CLCPRIFE,0)
          .w_CLQTAPRE = NVL(CLQTAPRE,0)
          .w_CLQTAAVA = NVL(CLQTAAVA,0)
          .w_CLQTASCA = NVL(CLQTASCA,0)
          .w_CLWIPOUT = NVL(CLWIPOUT,space(20))
          if link_2_21_joined
            this.w_CLWIPOUT = NVL(ARCODART221,NVL(this.w_CLWIPOUT,space(20)))
            this.w_DESWIPO = NVL(ARDESART221,space(40))
          else
          .link_2_21('Load')
          endif
          .w_CLDATINI = NVL(cp_ToDate(CLDATINI),ctod("  /  /  "))
          .w_CLDATFIN = NVL(cp_ToDate(CLDATFIN),ctod("  /  /  "))
          .w_CLWIPFPR = NVL(CLWIPFPR,space(20))
          if link_2_25_joined
            this.w_CLWIPFPR = NVL(ARCODART225,NVL(this.w_CLWIPFPR,space(20)))
            this.w_DESWIPI = NVL(ARDESART225,space(40))
          else
          .link_2_25('Load')
          endif
          .w_CLBFRIFE = NVL(CLBFRIFE,0)
          .w_CLCODERR = NVL(CLCODERR,space(10))
          .w_CLQTADIC = NVL(CLQTADIC,0)
          .w_CLDICUM1 = NVL(CLDICUM1,0)
          .w_CLFASCOS = NVL(CLFASCOS,space(1))
          .w_CLFASOUT = NVL(CLFASOUT,space(1))
          .w_CLFASCLA = NVL(CLFASCLA,space(1))
        .w_EDITALT = .w_EDITALT
          .w_CLPRIFAS = NVL(CLPRIFAS,space(1))
          .w_CLLTFASE = NVL(CLLTFASE,0)
          .w_CLCODFAS = NVL(CLCODFAS,space(20))
          if link_2_40_joined
            this.w_CLCODFAS = NVL(ARCODART240,NVL(this.w_CLCODFAS,space(20)))
            this.w_FASDES = NVL(ARDESART240,space(40))
          else
          .link_2_40('Load')
          endif
        .w_CLWIPOUT = .w_CLWIPOUT
          .link_2_42('Load')
          .link_2_43('Load')
        .w_FLFASEVA = .w_CLFASEVA
        .w_FLCOUPOI = .w_CLCOUPOI
        .w_FLULTFAS = .w_CLULTFAS
        .w_FLFASSEL = .w_CLFASSEL
        .w_FLFASOUT = .w_CLFASOUT
        .w_FLFASCLA = .w_CLFASCLA
        .w_FLPRIFAS = .w_CLPRIFAS
        .w_FLFASCOS = .w_CLFASCOS
          .w_CLWIPDES = NVL(CLWIPDES,space(5))
          .w_CLMAGWIP = NVL(CLMAGWIP,space(5))
          .w_CLSEOLFA = NVL(CLSEOLFA,space(15))
          .w_CLSEOLFA = NVL(CLSEOLFA,space(15))
          if link_2_58_joined
            this.w_CLSEOLFA = NVL(OLCODODL258,NVL(this.w_CLSEOLFA,space(15)))
            this.w_FLTSTAOL = NVL(OLTSTATO258,space(1))
            this.w_FLQTAEVA = NVL(OLTQTOEV258,0)
            this.w_FLTPROVE = NVL(OLTPROVE258,space(1))
          else
          .link_2_58('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CLCODODL=space(15)
      .w_CHECKFORM=.f.
      .w_FASEVAR=.f.
      .w_CLROWORD=0
      .w_CL__FASE=space(5)
      .w_CLDESFAS=space(40)
      .w_CLINDPRE=space(2)
      .w_CLROWNUM=0
      .w_DESCFA=space(40)
      .w_EDITROW=.f.
      .w_EDITCHK=.f.
      .w_CLPREUM1=0
      .w_CLAVAUM1=0
      .w_CLSCAUM1=0
      .w_CLFASEVA=space(1)
      .w_CLCOUPOI=space(1)
      .w_CLULTFAS=space(1)
      .w_CLFASSEL=space(1)
      .w_UM1=space(3)
      .w_CLCPRIFE=0
      .w_CLQTAPRE=0
      .w_CLQTAAVA=0
      .w_CLQTASCA=0
      .w_CLWIPOUT=space(20)
      .w_DESWIPO=space(40)
      .w_CLDATINI=ctod("  /  /  ")
      .w_CLDATFIN=ctod("  /  /  ")
      .w_CLWIPFPR=space(20)
      .w_DESWIPI=space(40)
      .w_CLBFRIFE=0
      .w_CLCODERR=space(10)
      .w_CLQTADIC=0
      .w_CLDICUM1=0
      .w_CLFASCOS=space(1)
      .w_CLFASOUT=space(1)
      .w_TESTROW=space(10)
      .w_NRVARIATA=0
      .w_CLFASCLA=space(1)
      .w_EDITALT=.f.
      .w_CLPRIFAS=space(1)
      .w_CLLTFASE=0
      .w_CLCODFAS=space(20)
      .w_FASDES=space(40)
      .w_CLWIPOUT=space(66)
      .w_CLCFASPR=space(66)
      .w_FLTSTAOL=space(1)
      .w_FLQTAEVA=0
      .w_FLTPROVE=space(1)
      .w_UPDATED=.f.
      .w_CANCHROW=.f.
      .w_CANDEAROW=.f.
      .w_ROWSTATUS=space(1)
      .w_CPROWDEL=0
      .w_ROWORDEL=0
      .w_FLFASEVA=space(1)
      .w_FLCOUPOI=space(1)
      .w_FLULTFAS=space(1)
      .w_FLFASSEL=space(1)
      .w_FLFASOUT=space(1)
      .w_FLFASCLA=space(1)
      .w_FLPRIFAS=space(1)
      .w_FLFASCOS=space(1)
      .w_CLWIPDES=space(5)
      .w_CLMAGWIP=space(5)
      .w_CLSEOLFA=space(15)
      .w_CLSEOLFA=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CHECKFORM = .T.
        .w_FASEVAR = .F.
        .w_CLROWORD = MIN( this.cp_maxclroword()+10 , 999)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CL__FASE))
         .link_2_2('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_CLINDPRE = '00'
        .w_CLROWNUM = .w_CPROWNUM
        .DoRTCalc(9,9,.f.)
        .w_EDITROW = .w_EDITROW
        .w_EDITCHK = .w_EDITCHK
        .DoRTCalc(12,15,.f.)
        .w_CLCOUPOI = IIF(.w_CLFASSEL<>'S', 'N', .w_CLCOUPOI)
        .DoRTCalc(17,17,.f.)
        .w_CLFASSEL = 'S'
        .w_UM1 = this.oparentobject .w_OLTUNMIS
        .DoRTCalc(20,24,.f.)
        if not(empty(.w_CLWIPOUT))
         .link_2_21('Full')
        endif
        .DoRTCalc(25,28,.f.)
        if not(empty(.w_CLWIPFPR))
         .link_2_25('Full')
        endif
        .DoRTCalc(29,31,.f.)
        .w_CLQTADIC = .w_CLQTAPRE
        .w_CLDICUM1 = .w_CLPREUM1
        .w_CLFASCOS = 'S'
        .w_CLFASOUT = iif(.w_CLCOUPOI='S', .w_CLFASOUT, 'N')
        .w_TESTROW = 'S'
        .DoRTCalc(37,37,.f.)
        .w_CLFASCLA = 'N'
        .w_EDITALT = .w_EDITALT
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(40,42,.f.)
        if not(empty(.w_CLCODFAS))
         .link_2_40('Full')
        endif
        .DoRTCalc(43,43,.f.)
        .w_CLWIPOUT = .w_CLWIPOUT
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CLWIPOUT))
         .link_2_42('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CLCFASPR))
         .link_2_43('Full')
        endif
        .DoRTCalc(46,48,.f.)
        .w_UPDATED = .T.
        .DoRTCalc(50,54,.f.)
        .w_FLFASEVA = .w_CLFASEVA
        .w_FLCOUPOI = .w_CLCOUPOI
        .w_FLULTFAS = .w_CLULTFAS
        .w_FLFASSEL = .w_CLFASSEL
        .w_FLFASOUT = .w_CLFASOUT
        .w_FLFASCLA = .w_CLFASCLA
        .w_FLPRIFAS = .w_CLPRIFAS
        .w_FLFASCOS = .w_CLFASCOS
        .DoRTCalc(63,66,.f.)
        if not(empty(.w_CLSEOLFA))
         .link_2_58('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'ODL_CICL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCLFASCOS_2_32.enabled = i_bVal
      .Page1.oPag.oCLSEOLFA_2_58.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSCO_MRL.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ODL_CICL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCO_MRL.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODODL,"CLCODODL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CLROWORD N(5);
      ,t_CL__FASE C(5);
      ,t_CLDESFAS C(40);
      ,t_CLINDPRE C(2);
      ,t_CLFASEVA N(3);
      ,t_CLCOUPOI N(3);
      ,t_CLULTFAS N(3);
      ,t_CLFASSEL N(3);
      ,t_UM1 C(3);
      ,t_CLQTAPRE N(12,5);
      ,t_CLQTAAVA N(12,5);
      ,t_CLQTASCA N(12,5);
      ,t_CLWIPOUT C(20);
      ,t_DESWIPO C(40);
      ,t_CLDATINI D(8);
      ,t_CLDATFIN D(8);
      ,t_CLWIPFPR C(20);
      ,t_DESWIPI C(40);
      ,t_CLFASCOS N(3);
      ,t_CLFASOUT N(3);
      ,t_CLFASCLA N(3);
      ,t_CLPRIFAS N(3);
      ,t_CLLTFASE N(12,5);
      ,t_CLCODFAS C(20);
      ,t_FASDES C(40);
      ,t_CLCFASPR C(66);
      ,CPROWNUM N(10);
      ,t_CLROWNUM N(4);
      ,t_DESCFA C(40);
      ,t_EDITROW L(1);
      ,t_EDITCHK L(1);
      ,t_CLPREUM1 N(12,5);
      ,t_CLAVAUM1 N(12,5);
      ,t_CLSCAUM1 N(12,5);
      ,t_CLCPRIFE N(5);
      ,t_CLBFRIFE N(5);
      ,t_CLCODERR C(10);
      ,t_CLQTADIC N(12,5);
      ,t_CLDICUM1 N(12,5);
      ,t_TESTROW C(10);
      ,t_NRVARIATA N(6);
      ,t_EDITALT L(1);
      ,t_FLTSTAOL C(1);
      ,t_FLQTAEVA N(12,3);
      ,t_FLTPROVE C(1);
      ,t_FLFASEVA C(1);
      ,t_FLCOUPOI C(1);
      ,t_FLULTFAS C(1);
      ,t_FLFASSEL C(1);
      ,t_FLFASOUT C(1);
      ,t_FLFASCLA C(1);
      ,t_FLPRIFAS C(1);
      ,t_FLFASCOS C(1);
      ,t_CLWIPDES C(5);
      ,t_CLMAGWIP C(5);
      ,t_CLSEOLFA C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mclbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLROWORD_2_1.controlsource=this.cTrsName+'.t_CLROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.controlsource=this.cTrsName+'.t_CL__FASE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLDESFAS_2_3.controlsource=this.cTrsName+'.t_CLDESFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDPRE_2_4.controlsource=this.cTrsName+'.t_CLINDPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASEVA_2_12.controlsource=this.cTrsName+'.t_CLFASEVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_13.controlsource=this.cTrsName+'.t_CLCOUPOI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_14.controlsource=this.cTrsName+'.t_CLULTFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASSEL_2_15.controlsource=this.cTrsName+'.t_CLFASSEL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUM1_2_16.controlsource=this.cTrsName+'.t_UM1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAPRE_2_18.controlsource=this.cTrsName+'.t_CLQTAPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAAVA_2_19.controlsource=this.cTrsName+'.t_CLQTAAVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTASCA_2_20.controlsource=this.cTrsName+'.t_CLQTASCA'
    this.oPgFRm.Page1.oPag.oCLWIPOUT_2_21.controlsource=this.cTrsName+'.t_CLWIPOUT'
    this.oPgFRm.Page1.oPag.oDESWIPO_2_22.controlsource=this.cTrsName+'.t_DESWIPO'
    this.oPgFRm.Page1.oPag.oCLDATINI_2_23.controlsource=this.cTrsName+'.t_CLDATINI'
    this.oPgFRm.Page1.oPag.oCLDATFIN_2_24.controlsource=this.cTrsName+'.t_CLDATFIN'
    this.oPgFRm.Page1.oPag.oCLWIPFPR_2_25.controlsource=this.cTrsName+'.t_CLWIPFPR'
    this.oPgFRm.Page1.oPag.oDESWIPI_2_26.controlsource=this.cTrsName+'.t_DESWIPI'
    this.oPgFRm.Page1.oPag.oCLFASCOS_2_32.controlsource=this.cTrsName+'.t_CLFASCOS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_33.controlsource=this.cTrsName+'.t_CLFASOUT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_36.controlsource=this.cTrsName+'.t_CLFASCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_38.controlsource=this.cTrsName+'.t_CLPRIFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_39.controlsource=this.cTrsName+'.t_CLLTFASE'
    this.oPgFRm.Page1.oPag.oCLCODFAS_2_40.controlsource=this.cTrsName+'.t_CLCODFAS'
    this.oPgFRm.Page1.oPag.oFASDES_2_41.controlsource=this.cTrsName+'.t_FASDES'
    this.oPgFRm.Page1.oPag.oCLCFASPR_2_43.controlsource=this.cTrsName+'.t_CLCFASPR'
    this.oPgFRm.Page1.oPag.oCLSEOLFA_2_58.controlsource=this.cTrsName+'.t_CLSEOLFA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(63)
    this.AddVLine(114)
    this.AddVLine(275)
    this.AddVLine(303)
    this.AddVLine(336)
    this.AddVLine(416)
    this.AddVLine(495)
    this.AddVLine(576)
    this.AddVLine(659)
    this.AddVLine(683)
    this.AddVLine(706)
    this.AddVLine(729)
    this.AddVLine(752)
    this.AddVLine(775)
    this.AddVLine(798)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
      *
      * insert into ODL_CICL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ODL_CICL')
        i_extval=cp_InsertValODBCExtFlds(this,'ODL_CICL')
        i_cFldBody=" "+;
                  "(CLCODODL,CLROWORD,CL__FASE,CLDESFAS,CLINDPRE"+;
                  ",CLROWNUM,CLPREUM1,CLAVAUM1,CLSCAUM1,CLFASEVA"+;
                  ",CLCOUPOI,CLULTFAS,CLFASSEL,CLCPRIFE,CLQTAPRE"+;
                  ",CLQTAAVA,CLQTASCA,CLWIPOUT,CLDATINI,CLDATFIN"+;
                  ",CLWIPFPR,CLBFRIFE,CLCODERR,CLQTADIC,CLDICUM1"+;
                  ",CLFASCOS,CLFASOUT,CLFASCLA,CLPRIFAS,CLLTFASE"+;
                  ",CLCODFAS,CLWIPDES,CLMAGWIP,CLSEOLFA,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CLCODODL)+","+cp_ToStrODBC(this.w_CLROWORD)+","+cp_ToStrODBCNull(this.w_CL__FASE)+","+cp_ToStrODBC(this.w_CLDESFAS)+","+cp_ToStrODBC(this.w_CLINDPRE)+;
             ","+cp_ToStrODBC(this.w_CLROWNUM)+","+cp_ToStrODBC(this.w_CLPREUM1)+","+cp_ToStrODBC(this.w_CLAVAUM1)+","+cp_ToStrODBC(this.w_CLSCAUM1)+","+cp_ToStrODBC(this.w_CLFASEVA)+;
             ","+cp_ToStrODBC(this.w_CLCOUPOI)+","+cp_ToStrODBC(this.w_CLULTFAS)+","+cp_ToStrODBC(this.w_CLFASSEL)+","+cp_ToStrODBC(this.w_CLCPRIFE)+","+cp_ToStrODBC(this.w_CLQTAPRE)+;
             ","+cp_ToStrODBC(this.w_CLQTAAVA)+","+cp_ToStrODBC(this.w_CLQTASCA)+","+cp_ToStrODBCNull(this.w_CLWIPOUT)+","+cp_ToStrODBC(this.w_CLDATINI)+","+cp_ToStrODBC(this.w_CLDATFIN)+;
             ","+cp_ToStrODBCNull(this.w_CLWIPFPR)+","+cp_ToStrODBC(this.w_CLBFRIFE)+","+cp_ToStrODBC(this.w_CLCODERR)+","+cp_ToStrODBC(this.w_CLQTADIC)+","+cp_ToStrODBC(this.w_CLDICUM1)+;
             ","+cp_ToStrODBC(this.w_CLFASCOS)+","+cp_ToStrODBC(this.w_CLFASOUT)+","+cp_ToStrODBC(this.w_CLFASCLA)+","+cp_ToStrODBC(this.w_CLPRIFAS)+","+cp_ToStrODBC(this.w_CLLTFASE)+;
             ","+cp_ToStrODBCNull(this.w_CLCODFAS)+","+cp_ToStrODBC(this.w_CLWIPDES)+","+cp_ToStrODBC(this.w_CLMAGWIP)+","+cp_ToStrODBC(this.w_CLSEOLFA)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ODL_CICL')
        i_extval=cp_InsertValVFPExtFlds(this,'ODL_CICL')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CLCODODL',this.w_CLCODODL)
        INSERT INTO (i_cTable) (;
                   CLCODODL;
                  ,CLROWORD;
                  ,CL__FASE;
                  ,CLDESFAS;
                  ,CLINDPRE;
                  ,CLROWNUM;
                  ,CLPREUM1;
                  ,CLAVAUM1;
                  ,CLSCAUM1;
                  ,CLFASEVA;
                  ,CLCOUPOI;
                  ,CLULTFAS;
                  ,CLFASSEL;
                  ,CLCPRIFE;
                  ,CLQTAPRE;
                  ,CLQTAAVA;
                  ,CLQTASCA;
                  ,CLWIPOUT;
                  ,CLDATINI;
                  ,CLDATFIN;
                  ,CLWIPFPR;
                  ,CLBFRIFE;
                  ,CLCODERR;
                  ,CLQTADIC;
                  ,CLDICUM1;
                  ,CLFASCOS;
                  ,CLFASOUT;
                  ,CLFASCLA;
                  ,CLPRIFAS;
                  ,CLLTFASE;
                  ,CLCODFAS;
                  ,CLWIPDES;
                  ,CLMAGWIP;
                  ,CLSEOLFA;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CLCODODL;
                  ,this.w_CLROWORD;
                  ,this.w_CL__FASE;
                  ,this.w_CLDESFAS;
                  ,this.w_CLINDPRE;
                  ,this.w_CLROWNUM;
                  ,this.w_CLPREUM1;
                  ,this.w_CLAVAUM1;
                  ,this.w_CLSCAUM1;
                  ,this.w_CLFASEVA;
                  ,this.w_CLCOUPOI;
                  ,this.w_CLULTFAS;
                  ,this.w_CLFASSEL;
                  ,this.w_CLCPRIFE;
                  ,this.w_CLQTAPRE;
                  ,this.w_CLQTAAVA;
                  ,this.w_CLQTASCA;
                  ,this.w_CLWIPOUT;
                  ,this.w_CLDATINI;
                  ,this.w_CLDATFIN;
                  ,this.w_CLWIPFPR;
                  ,this.w_CLBFRIFE;
                  ,this.w_CLCODERR;
                  ,this.w_CLQTADIC;
                  ,this.w_CLDICUM1;
                  ,this.w_CLFASCOS;
                  ,this.w_CLFASOUT;
                  ,this.w_CLFASCLA;
                  ,this.w_CLPRIFAS;
                  ,this.w_CLLTFASE;
                  ,this.w_CLCODFAS;
                  ,this.w_CLWIPDES;
                  ,this.w_CLMAGWIP;
                  ,this.w_CLSEOLFA;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsco_mcl
    * --- In fase di modifica, memorizza i dati prima
    * --- del salvataggio per poi effettuare i controlli
    If .F.
      GSCI_BTC(this, "BEFORE-REPLACE")
    endif
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CLROWORD<>0 and !Empty(t_CLDESFAS)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_CICL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_CICL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CLROWORD<>0 and !Empty(t_CLDESFAS)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSCO_MRL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CLCODODL,"RLCODODL";
                     ,this.w_CPROWNUM,"RLROWNUM";
                     )
              this.GSCO_MRL.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ODL_CICL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_CICL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CLROWORD="+cp_ToStrODBC(this.w_CLROWORD)+;
                     ",CL__FASE="+cp_ToStrODBCNull(this.w_CL__FASE)+;
                     ",CLDESFAS="+cp_ToStrODBC(this.w_CLDESFAS)+;
                     ",CLINDPRE="+cp_ToStrODBC(this.w_CLINDPRE)+;
                     ",CLROWNUM="+cp_ToStrODBC(this.w_CLROWNUM)+;
                     ",CLPREUM1="+cp_ToStrODBC(this.w_CLPREUM1)+;
                     ",CLAVAUM1="+cp_ToStrODBC(this.w_CLAVAUM1)+;
                     ",CLSCAUM1="+cp_ToStrODBC(this.w_CLSCAUM1)+;
                     ",CLFASEVA="+cp_ToStrODBC(this.w_CLFASEVA)+;
                     ",CLCOUPOI="+cp_ToStrODBC(this.w_CLCOUPOI)+;
                     ",CLULTFAS="+cp_ToStrODBC(this.w_CLULTFAS)+;
                     ",CLFASSEL="+cp_ToStrODBC(this.w_CLFASSEL)+;
                     ",CLCPRIFE="+cp_ToStrODBC(this.w_CLCPRIFE)+;
                     ",CLQTAPRE="+cp_ToStrODBC(this.w_CLQTAPRE)+;
                     ",CLQTAAVA="+cp_ToStrODBC(this.w_CLQTAAVA)+;
                     ",CLQTASCA="+cp_ToStrODBC(this.w_CLQTASCA)+;
                     ",CLWIPOUT="+cp_ToStrODBCNull(this.w_CLWIPOUT)+;
                     ",CLDATINI="+cp_ToStrODBC(this.w_CLDATINI)+;
                     ",CLDATFIN="+cp_ToStrODBC(this.w_CLDATFIN)+;
                     ",CLWIPFPR="+cp_ToStrODBCNull(this.w_CLWIPFPR)+;
                     ",CLBFRIFE="+cp_ToStrODBC(this.w_CLBFRIFE)+;
                     ",CLCODERR="+cp_ToStrODBC(this.w_CLCODERR)+;
                     ",CLQTADIC="+cp_ToStrODBC(this.w_CLQTADIC)+;
                     ",CLDICUM1="+cp_ToStrODBC(this.w_CLDICUM1)+;
                     ",CLFASCOS="+cp_ToStrODBC(this.w_CLFASCOS)+;
                     ",CLFASOUT="+cp_ToStrODBC(this.w_CLFASOUT)+;
                     ",CLFASCLA="+cp_ToStrODBC(this.w_CLFASCLA)+;
                     ",CLPRIFAS="+cp_ToStrODBC(this.w_CLPRIFAS)+;
                     ",CLLTFASE="+cp_ToStrODBC(this.w_CLLTFASE)+;
                     ",CLCODFAS="+cp_ToStrODBCNull(this.w_CLCODFAS)+;
                     ",CLWIPDES="+cp_ToStrODBC(this.w_CLWIPDES)+;
                     ",CLMAGWIP="+cp_ToStrODBC(this.w_CLMAGWIP)+;
                     ",CLSEOLFA="+cp_ToStrODBC(this.w_CLSEOLFA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_CICL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CLROWORD=this.w_CLROWORD;
                     ,CL__FASE=this.w_CL__FASE;
                     ,CLDESFAS=this.w_CLDESFAS;
                     ,CLINDPRE=this.w_CLINDPRE;
                     ,CLROWNUM=this.w_CLROWNUM;
                     ,CLPREUM1=this.w_CLPREUM1;
                     ,CLAVAUM1=this.w_CLAVAUM1;
                     ,CLSCAUM1=this.w_CLSCAUM1;
                     ,CLFASEVA=this.w_CLFASEVA;
                     ,CLCOUPOI=this.w_CLCOUPOI;
                     ,CLULTFAS=this.w_CLULTFAS;
                     ,CLFASSEL=this.w_CLFASSEL;
                     ,CLCPRIFE=this.w_CLCPRIFE;
                     ,CLQTAPRE=this.w_CLQTAPRE;
                     ,CLQTAAVA=this.w_CLQTAAVA;
                     ,CLQTASCA=this.w_CLQTASCA;
                     ,CLWIPOUT=this.w_CLWIPOUT;
                     ,CLDATINI=this.w_CLDATINI;
                     ,CLDATFIN=this.w_CLDATFIN;
                     ,CLWIPFPR=this.w_CLWIPFPR;
                     ,CLBFRIFE=this.w_CLBFRIFE;
                     ,CLCODERR=this.w_CLCODERR;
                     ,CLQTADIC=this.w_CLQTADIC;
                     ,CLDICUM1=this.w_CLDICUM1;
                     ,CLFASCOS=this.w_CLFASCOS;
                     ,CLFASOUT=this.w_CLFASOUT;
                     ,CLFASCLA=this.w_CLFASCLA;
                     ,CLPRIFAS=this.w_CLPRIFAS;
                     ,CLLTFASE=this.w_CLLTFASE;
                     ,CLCODFAS=this.w_CLCODFAS;
                     ,CLWIPDES=this.w_CLWIPDES;
                     ,CLMAGWIP=this.w_CLMAGWIP;
                     ,CLSEOLFA=this.w_CLSEOLFA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CLROWORD<>0 and !Empty(t_CLDESFAS))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSCO_MRL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CLCODODL,"RLCODODL";
               ,this.w_CPROWNUM,"RLROWNUM";
               )
          this.GSCO_MRL.mReplace()
          this.GSCO_MRL.bSaveContext=.f.
        endif
      endscan
     this.GSCO_MRL.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsco_mcl
    * --- Controlli sul ciclo di lavorazione
    * --- Setta ultima fase
    If g_PRFA='S' AND g_CICLILAV='S'
      GSCI_BTC(this,"UltFase")
    EndIf
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CLROWORD<>0 and !Empty(t_CLDESFAS)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCO_MRL : Deleting
        this.GSCO_MRL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CLCODODL,"RLCODODL";
               ,this.w_CPROWNUM,"RLROWNUM";
               )
        this.GSCO_MRL.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ODL_CICL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CLROWORD<>0 and !Empty(t_CLDESFAS)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
          .w_EDITROW = .w_EDITROW
          .w_EDITCHK = .w_EDITCHK
        .DoRTCalc(12,15,.t.)
        if .o_CLFASSEL<>.w_CLFASSEL
          .w_CLCOUPOI = IIF(.w_CLFASSEL<>'S', 'N', .w_CLCOUPOI)
        endif
        .DoRTCalc(17,23,.t.)
          .link_2_21('Full')
        .DoRTCalc(25,27,.t.)
          .link_2_25('Full')
        .DoRTCalc(29,31,.t.)
          .w_CLQTADIC = .w_CLQTAPRE
          .w_CLDICUM1 = .w_CLPREUM1
        if .o_CLCOUPOI<>.w_CLCOUPOI
          .w_CLFASCOS = 'S'
        endif
        if .o_CLCOUPOI<>.w_CLCOUPOI
          .w_CLFASOUT = iif(.w_CLCOUPOI='S', .w_CLFASOUT, 'N')
        endif
        .DoRTCalc(36,38,.t.)
          .w_EDITALT = .w_EDITALT
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(40,41,.t.)
          .link_2_40('Full')
        .DoRTCalc(43,43,.t.)
          .w_CLWIPOUT = .w_CLWIPOUT
          .link_2_42('Full')
          .link_2_43('Full')
        Local l_Dep1,l_Dep2
        l_Dep1= .o_CLROWORD<>.w_CLROWORD .or. .o_CLDESFAS<>.w_CLDESFAS .or. .o_CLINDPRE<>.w_CLINDPRE .or. .o_CLQTAPRE<>.w_CLQTAPRE .or. .o_CLFASSEL<>.w_CLFASSEL        l_Dep2= .o_CLCOUPOI<>.w_CLCOUPOI .or. .o_EDITROW<>.w_EDITROW .or. .o_CLFASOUT<>.w_CLFASOUT .or. .o_CLFASCLA<>.w_CLFASCLA
        if m.l_Dep1 .or. m.l_Dep2
          .Calculate_ZFSQAWRSFY()
        endif
        .DoRTCalc(46,54,.t.)
          .w_FLFASEVA = .w_CLFASEVA
        if .o_CLFASSEL<>.w_CLFASSEL
          .w_FLCOUPOI = .w_CLCOUPOI
        endif
          .w_FLULTFAS = .w_CLULTFAS
          .w_FLFASSEL = .w_CLFASSEL
        if .o_CLCOUPOI<>.w_CLCOUPOI
          .w_FLFASOUT = .w_CLFASOUT
        endif
          .w_FLFASCLA = .w_CLFASCLA
          .w_FLPRIFAS = .w_CLPRIFAS
          .w_FLFASCOS = .w_CLFASCOS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(63,66,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CLROWNUM with this.w_CLROWNUM
      replace t_DESCFA with this.w_DESCFA
      replace t_EDITROW with this.w_EDITROW
      replace t_EDITCHK with this.w_EDITCHK
      replace t_CLPREUM1 with this.w_CLPREUM1
      replace t_CLAVAUM1 with this.w_CLAVAUM1
      replace t_CLSCAUM1 with this.w_CLSCAUM1
      replace t_CLCPRIFE with this.w_CLCPRIFE
      replace t_CLBFRIFE with this.w_CLBFRIFE
      replace t_CLCODERR with this.w_CLCODERR
      replace t_CLQTADIC with this.w_CLQTADIC
      replace t_CLDICUM1 with this.w_CLDICUM1
      replace t_TESTROW with this.w_TESTROW
      replace t_NRVARIATA with this.w_NRVARIATA
      replace t_EDITALT with this.w_EDITALT
      replace t_CLWIPOUT with this.w_CLWIPOUT
      replace t_FLTSTAOL with this.w_FLTSTAOL
      replace t_FLQTAEVA with this.w_FLQTAEVA
      replace t_FLTPROVE with this.w_FLTPROVE
      replace t_FLFASEVA with this.w_FLFASEVA
      replace t_FLCOUPOI with this.w_FLCOUPOI
      replace t_FLULTFAS with this.w_FLULTFAS
      replace t_FLFASSEL with this.w_FLFASSEL
      replace t_FLFASOUT with this.w_FLFASOUT
      replace t_FLFASCLA with this.w_FLFASCLA
      replace t_FLPRIFAS with this.w_FLPRIFAS
      replace t_FLFASCOS with this.w_FLFASCOS
      replace t_CLWIPDES with this.w_CLWIPDES
      replace t_CLMAGWIP with this.w_CLMAGWIP
      replace t_CLSEOLFA with this.w_CLSEOLFA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_NMADYLUOXR()
    with this
          * --- GSCI_BTC -> Controlli Finali
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"FinalCheck";
             )
     endif
    endwith
  endproc
  proc Calculate_AJJIFAASEH()
    with this
          * --- GSCI_BTC -> SetEditableRow, w_CLROWORD GotFocus, w_CL__FASE GotFocus, w_CLDESFAS GotFocus, w_CLINDPRE GotFocus, w_CLCOUPOI GotFocus, w_CLFASOUT GotFocus, w_CLFASSEL GotFocus
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"FinalCheck";
             )
     endif
    endwith
  endproc
  proc Calculate_XKUHYXICXR()
    with this
          * --- GSCI_BTC -> w_CLROWORD Changed
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"ChangeOrder";
             )
     endif
    endwith
  endproc
  proc Calculate_XOPMETNIUZ()
    with this
          * --- GSCI_BTC -> w_CL__FASE Changed
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"LoadResourceDetail";
             )
     endif
    endwith
  endproc
  proc Calculate_ZCHQGZMMRE()
    with this
          * --- GSCI_BTC -> CanAddRow
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"CanAddRow";
             )
     endif
    endwith
  endproc
  proc Calculate_NEANOONDBN()
    with this
          * --- GSCI_BTC -> ChangeResource
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"ChangeResource";
             )
     endif
    endwith
  endproc
  proc Calculate_XXYXFLABFO()
    with this
          * --- Memorizzo il numero di riga da eliminare prima della cancellazione
          .w_CPROWDEL = .w_CPROWNUM
          .w_ROWORDEL = .w_CLROWORD
    endwith
  endproc
  proc Calculate_CBLUVKYRWM()
    with this
          * --- GSCI_BTC -> Before row delete
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"BeforeRowDeleted";
             )
     endif
    endwith
  endproc
  proc Calculate_TVRKPXYURE()
    with this
          * --- GSCI_BTC -> w_CLFASSEL Changed
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"CanDeactRow";
             )
     endif
    endwith
  endproc
  proc Calculate_ARRGOTHQCN()
    with this
          * --- GSCI_BTC -> Delete row start
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"DEL-ASSFAS";
             )
     endif
    endwith
  endproc
  proc Calculate_LKSWBBWWYE()
    with this
          * --- GSCI_BTC -> Row deleted
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"RowDeleted";
             )
     endif
    endwith
  endproc
  proc Calculate_ZFSQAWRSFY()
    with this
          * --- Variazioni dati (mi serve per creare le o_xxx)
          .w_UPDATED = .F.
    endwith
  endproc
  proc Calculate_OAKCRQJDAV()
    with this
          * --- GSCI_BTC -> CanDeleteRow
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"CanDeleteRow";
             )
     endif
    endwith
  endproc
  proc Calculate_QDDVIINXYZ()
    with this
          * --- GSCI_BTC -> SetEditableRow
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCI_BTC(this;
              ,"SetEditableRow";
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCL__FASE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCL__FASE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLDESFAS_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLDESFAS_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLINDPRE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLINDPRE_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLCOUPOI_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLCOUPOI_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLFASSEL_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLFASSEL_2_15.mCond()
    this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLFASOUT_2_33.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLFASOUT_2_33.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLLTFASE_2_39.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLLTFASE_2_39.mCond()
    this.oPgFrm.Page1.oPag.oCLSEOLFA_2_58.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCLSEOLFA_2_58.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsco_mcl
            IF Upper(CEVENT)='INIT'
             if Upper(this.GSCO_MRL.class)='STDDYNAMICCHILD'
               This.oPgFrm.Pages[4].opag.uienable(.T.)
               This.oPgFrm.ActivePage=1
             Endif
            Endif       
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Controlli Finali")
          .Calculate_NMADYLUOXR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SetEditableRow") or lower(cEvent)==lower("w_CL__FASE GotFocus") or lower(cEvent)==lower("w_CLCOUPOI GotFocus") or lower(cEvent)==lower("w_CLDESFAS GotFocus") or lower(cEvent)==lower("w_CLFASOUT GotFocus") or lower(cEvent)==lower("w_CLFASSEL GotFocus") or lower(cEvent)==lower("w_CLINDPRE GotFocus") or lower(cEvent)==lower("w_CLROWORD GotFocus")
          .Calculate_AJJIFAASEH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CLROWORD Changed")
          .Calculate_XKUHYXICXR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CL__FASE Changed")
          .Calculate_XOPMETNIUZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CanAddRow")
          .Calculate_ZCHQGZMMRE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeResource")
          .Calculate_NEANOONDBN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Before row delete")
          .Calculate_XXYXFLABFO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Before row delete")
          .Calculate_CBLUVKYRWM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CLFASSEL Changed")
          .Calculate_TVRKPXYURE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete row start")
          .Calculate_ARRGOTHQCN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Row deleted")
          .Calculate_LKSWBBWWYE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init Row") or lower(cEvent)==lower("Row deleted")
          .Calculate_ZFSQAWRSFY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CanDeleteRow")
          .Calculate_OAKCRQJDAV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SetEditableRow")
          .Calculate_QDDVIINXYZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsco_mcl
    this.w_ISODLDIC = this.oParentObject.w_OLTQTOEV > 0
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CL__FASE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_FASI_IDX,3]
    i_lTable = "COD_FASI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2], .t., this.COD_FASI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CL__FASE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COD_FASI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_CL__FASE)+"%");

          i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFCODICE',trim(this.w_CL__FASE))
          select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CL__FASE)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CL__FASE) and !this.bDontReportError
            deferred_cp_zoom('COD_FASI','*','CFCODICE',cp_AbsName(oSource.parent,'oCL__FASE_2_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',oSource.xKey(1))
            select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CL__FASE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_CL__FASE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',this.w_CL__FASE)
            select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CL__FASE = NVL(_Link_.CFCODICE,space(5))
      this.w_CLDESFAS = NVL(_Link_.CFDESCRI,space(40))
      this.w_CLCOUPOI = NVL(_Link_.CFCOUPOI,space(1))
      this.w_CLFASOUT = NVL(_Link_.CFOUTPUT,space(1))
      this.w_CLFASCOS = NVL(_Link_.CFFASCOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CL__FASE = space(5)
      endif
      this.w_CLDESFAS = space(40)
      this.w_CLCOUPOI = space(1)
      this.w_CLFASOUT = space(1)
      this.w_CLFASCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_FASI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CL__FASE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLWIPOUT
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLWIPOUT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLWIPOUT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CLWIPOUT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CLWIPOUT)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLWIPOUT = NVL(_Link_.ARCODART,space(20))
      this.w_DESWIPO = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLWIPOUT = space(20)
      endif
      this.w_DESWIPO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLWIPOUT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_21.ARCODART as ARCODART221"+ ",link_2_21.ARDESART as ARDESART221"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_21 on ODL_CICL.CLWIPOUT=link_2_21.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_21"
          i_cKey=i_cKey+'+" and ODL_CICL.CLWIPOUT=link_2_21.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLWIPFPR
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLWIPFPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLWIPFPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CLWIPFPR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CLWIPFPR)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLWIPFPR = NVL(_Link_.ARCODART,space(20))
      this.w_DESWIPI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLWIPFPR = space(20)
      endif
      this.w_DESWIPI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLWIPFPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_25.ARCODART as ARCODART225"+ ",link_2_25.ARDESART as ARDESART225"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_25 on ODL_CICL.CLWIPFPR=link_2_25.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_25"
          i_cKey=i_cKey+'+" and ODL_CICL.CLWIPFPR=link_2_25.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCODFAS
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CLCODFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CLCODFAS)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODFAS = NVL(_Link_.ARCODART,space(20))
      this.w_FASDES = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODFAS = space(20)
      endif
      this.w_FASDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_40.ARCODART as ARCODART240"+ ",link_2_40.ARDESART as ARDESART240"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_40 on ODL_CICL.CLCODFAS=link_2_40.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_40"
          i_cKey=i_cKey+'+" and ODL_CICL.CLCODFAS=link_2_40.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLWIPOUT
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLWIPOUT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLWIPOUT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CLWIPOUT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CLWIPOUT)
            select CACODICE,CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLWIPOUT = NVL(_Link_.CACODICE,space(66))
      this.w_CLCFASPR = NVL(_Link_.CACODFAS,space(66))
    else
      if i_cCtrl<>'Load'
        this.w_CLWIPOUT = space(66)
      endif
      this.w_CLCFASPR = space(66)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLWIPOUT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLCFASPR
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCFASPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCFASPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(this.w_CLCFASPR);
                   +" and CACODICE="+cp_ToStrODBC(this.w_CLCODFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CLCODFAS;
                       ,'CACODFAS',this.w_CLCFASPR)
            select CACODICE,CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCFASPR = NVL(_Link_.CACODFAS,space(66))
    else
      if i_cCtrl<>'Load'
        this.w_CLCFASPR = space(66)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)+'\'+cp_ToStr(_Link_.CACODFAS,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCFASPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLSEOLFA
  func Link_2_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLSEOLFA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_CLSEOLFA)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTQTOEV,OLTPROVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_CLSEOLFA))
          select OLCODODL,OLTSTATO,OLTQTOEV,OLTPROVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLSEOLFA)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLSEOLFA) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oCLSEOLFA_2_58'),i_cWhere,'GSCO_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTQTOEV,OLTPROVE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTSTATO,OLTQTOEV,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLSEOLFA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTQTOEV,OLTPROVE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_CLSEOLFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_CLSEOLFA)
            select OLCODODL,OLTSTATO,OLTQTOEV,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLSEOLFA = NVL(_Link_.OLCODODL,space(15))
      this.w_FLTSTAOL = NVL(_Link_.OLTSTATO,space(1))
      this.w_FLQTAEVA = NVL(_Link_.OLTQTOEV,0)
      this.w_FLTPROVE = NVL(_Link_.OLTPROVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLSEOLFA = space(15)
      endif
      this.w_FLTSTAOL = space(1)
      this.w_FLQTAEVA = 0
      this.w_FLTPROVE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLSEOLFA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_58(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ODL_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_58.OLCODODL as OLCODODL258"+ ",link_2_58.OLTSTATO as OLTSTATO258"+ ",link_2_58.OLTQTOEV as OLTQTOEV258"+ ",link_2_58.OLTPROVE as OLTPROVE258"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_58 on ODL_CICL.CLSEOLFA=link_2_58.OLCODODL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_58"
          i_cKey=i_cKey+'+" and ODL_CICL.CLSEOLFA=link_2_58.OLCODODL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCLWIPOUT_2_21.value==this.w_CLWIPOUT)
      this.oPgFrm.Page1.oPag.oCLWIPOUT_2_21.value=this.w_CLWIPOUT
      replace t_CLWIPOUT with this.oPgFrm.Page1.oPag.oCLWIPOUT_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESWIPO_2_22.value==this.w_DESWIPO)
      this.oPgFrm.Page1.oPag.oDESWIPO_2_22.value=this.w_DESWIPO
      replace t_DESWIPO with this.oPgFrm.Page1.oPag.oDESWIPO_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDATINI_2_23.value==this.w_CLDATINI)
      this.oPgFrm.Page1.oPag.oCLDATINI_2_23.value=this.w_CLDATINI
      replace t_CLDATINI with this.oPgFrm.Page1.oPag.oCLDATINI_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDATFIN_2_24.value==this.w_CLDATFIN)
      this.oPgFrm.Page1.oPag.oCLDATFIN_2_24.value=this.w_CLDATFIN
      replace t_CLDATFIN with this.oPgFrm.Page1.oPag.oCLDATFIN_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLWIPFPR_2_25.value==this.w_CLWIPFPR)
      this.oPgFrm.Page1.oPag.oCLWIPFPR_2_25.value=this.w_CLWIPFPR
      replace t_CLWIPFPR with this.oPgFrm.Page1.oPag.oCLWIPFPR_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESWIPI_2_26.value==this.w_DESWIPI)
      this.oPgFrm.Page1.oPag.oDESWIPI_2_26.value=this.w_DESWIPI
      replace t_DESWIPI with this.oPgFrm.Page1.oPag.oDESWIPI_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.RadioValue()==this.w_CLFASCOS)
      this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.SetRadio()
      replace t_CLFASCOS with this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODFAS_2_40.value==this.w_CLCODFAS)
      this.oPgFrm.Page1.oPag.oCLCODFAS_2_40.value=this.w_CLCODFAS
      replace t_CLCODFAS with this.oPgFrm.Page1.oPag.oCLCODFAS_2_40.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFASDES_2_41.value==this.w_FASDES)
      this.oPgFrm.Page1.oPag.oFASDES_2_41.value=this.w_FASDES
      replace t_FASDES with this.oPgFrm.Page1.oPag.oFASDES_2_41.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCFASPR_2_43.value==this.w_CLCFASPR)
      this.oPgFrm.Page1.oPag.oCLCFASPR_2_43.value=this.w_CLCFASPR
      replace t_CLCFASPR with this.oPgFrm.Page1.oPag.oCLCFASPR_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLSEOLFA_2_58.value==this.w_CLSEOLFA)
      this.oPgFrm.Page1.oPag.oCLSEOLFA_2_58.value=this.w_CLSEOLFA
      replace t_CLSEOLFA with this.oPgFrm.Page1.oPag.oCLSEOLFA_2_58.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLROWORD_2_1.value==this.w_CLROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLROWORD_2_1.value=this.w_CLROWORD
      replace t_CLROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.value==this.w_CL__FASE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.value=this.w_CL__FASE
      replace t_CL__FASE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLDESFAS_2_3.value==this.w_CLDESFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLDESFAS_2_3.value=this.w_CLDESFAS
      replace t_CLDESFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLDESFAS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDPRE_2_4.value==this.w_CLINDPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDPRE_2_4.value=this.w_CLINDPRE
      replace t_CLINDPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDPRE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASEVA_2_12.RadioValue()==this.w_CLFASEVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASEVA_2_12.SetRadio()
      replace t_CLFASEVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASEVA_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_13.RadioValue()==this.w_CLCOUPOI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_13.SetRadio()
      replace t_CLCOUPOI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_14.RadioValue()==this.w_CLULTFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_14.SetRadio()
      replace t_CLULTFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASSEL_2_15.RadioValue()==this.w_CLFASSEL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASSEL_2_15.SetRadio()
      replace t_CLFASSEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASSEL_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUM1_2_16.value==this.w_UM1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUM1_2_16.value=this.w_UM1
      replace t_UM1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUM1_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAPRE_2_18.value==this.w_CLQTAPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAPRE_2_18.value=this.w_CLQTAPRE
      replace t_CLQTAPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAPRE_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAAVA_2_19.value==this.w_CLQTAAVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAAVA_2_19.value=this.w_CLQTAAVA
      replace t_CLQTAAVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAAVA_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTASCA_2_20.value==this.w_CLQTASCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTASCA_2_20.value=this.w_CLQTASCA
      replace t_CLQTASCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTASCA_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_33.RadioValue()==this.w_CLFASOUT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_33.SetRadio()
      replace t_CLFASOUT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_36.RadioValue()==this.w_CLFASCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_36.SetRadio()
      replace t_CLFASCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_38.RadioValue()==this.w_CLPRIFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_38.SetRadio()
      replace t_CLPRIFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_38.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_39.value==this.w_CLLTFASE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_39.value=this.w_CLLTFASE
      replace t_CLLTFASE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_39.value
    endif
    cp_SetControlsValueExtFlds(this,'ODL_CICL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSCO_MRL.CheckForm()
      if .w_CLROWORD<>0 and !Empty(.w_CLDESFAS)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLROWORD = this.w_CLROWORD
    this.o_CLDESFAS = this.w_CLDESFAS
    this.o_CLINDPRE = this.w_CLINDPRE
    this.o_EDITROW = this.w_EDITROW
    this.o_CLCOUPOI = this.w_CLCOUPOI
    this.o_CLFASSEL = this.w_CLFASSEL
    this.o_CLQTAPRE = this.w_CLQTAPRE
    this.o_CLFASOUT = this.w_CLFASOUT
    this.o_CLFASCLA = this.w_CLFASCLA
    * --- GSCO_MRL : Depends On
    this.GSCO_MRL.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CLROWORD<>0 and !Empty(t_CLDESFAS))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CLROWORD=0
      .w_CL__FASE=space(5)
      .w_CLDESFAS=space(40)
      .w_CLINDPRE=space(2)
      .w_CLROWNUM=0
      .w_DESCFA=space(40)
      .w_EDITROW=.f.
      .w_EDITCHK=.f.
      .w_CLPREUM1=0
      .w_CLAVAUM1=0
      .w_CLSCAUM1=0
      .w_CLFASEVA=space(1)
      .w_CLCOUPOI=space(1)
      .w_CLULTFAS=space(1)
      .w_CLFASSEL=space(1)
      .w_UM1=space(3)
      .w_CLCPRIFE=0
      .w_CLQTAPRE=0
      .w_CLQTAAVA=0
      .w_CLQTASCA=0
      .w_CLWIPOUT=space(20)
      .w_DESWIPO=space(40)
      .w_CLDATINI=ctod("  /  /  ")
      .w_CLDATFIN=ctod("  /  /  ")
      .w_CLWIPFPR=space(20)
      .w_DESWIPI=space(40)
      .w_CLBFRIFE=0
      .w_CLCODERR=space(10)
      .w_CLQTADIC=0
      .w_CLDICUM1=0
      .w_CLFASCOS=space(1)
      .w_CLFASOUT=space(1)
      .w_TESTROW=space(10)
      .w_NRVARIATA=0
      .w_CLFASCLA=space(1)
      .w_EDITALT=.f.
      .w_CLPRIFAS=space(1)
      .w_CLLTFASE=0
      .w_CLCODFAS=space(20)
      .w_FASDES=space(40)
      .w_CLWIPOUT=space(66)
      .w_CLCFASPR=space(66)
      .w_FLTSTAOL=space(1)
      .w_FLQTAEVA=0
      .w_FLTPROVE=space(1)
      .w_FLFASEVA=space(1)
      .w_FLCOUPOI=space(1)
      .w_FLULTFAS=space(1)
      .w_FLFASSEL=space(1)
      .w_FLFASOUT=space(1)
      .w_FLFASCLA=space(1)
      .w_FLPRIFAS=space(1)
      .w_FLFASCOS=space(1)
      .w_CLWIPDES=space(5)
      .w_CLMAGWIP=space(5)
      .w_CLSEOLFA=space(15)
      .w_CLSEOLFA=space(15)
      .DoRTCalc(1,3,.f.)
        .w_CLROWORD = MIN( this.cp_maxclroword()+10 , 999)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_CL__FASE))
        .link_2_2('Full')
      endif
      .DoRTCalc(6,6,.f.)
        .w_CLINDPRE = '00'
        .w_CLROWNUM = .w_CPROWNUM
      .DoRTCalc(9,9,.f.)
        .w_EDITROW = .w_EDITROW
        .w_EDITCHK = .w_EDITCHK
      .DoRTCalc(12,15,.f.)
        .w_CLCOUPOI = IIF(.w_CLFASSEL<>'S', 'N', .w_CLCOUPOI)
      .DoRTCalc(17,17,.f.)
        .w_CLFASSEL = 'S'
        .w_UM1 = this.oparentobject .w_OLTUNMIS
      .DoRTCalc(20,24,.f.)
      if not(empty(.w_CLWIPOUT))
        .link_2_21('Full')
      endif
      .DoRTCalc(25,28,.f.)
      if not(empty(.w_CLWIPFPR))
        .link_2_25('Full')
      endif
      .DoRTCalc(29,31,.f.)
        .w_CLQTADIC = .w_CLQTAPRE
        .w_CLDICUM1 = .w_CLPREUM1
        .w_CLFASCOS = 'S'
        .w_CLFASOUT = iif(.w_CLCOUPOI='S', .w_CLFASOUT, 'N')
        .w_TESTROW = 'S'
      .DoRTCalc(37,37,.f.)
        .w_CLFASCLA = 'N'
        .w_EDITALT = .w_EDITALT
      .DoRTCalc(40,42,.f.)
      if not(empty(.w_CLCODFAS))
        .link_2_40('Full')
      endif
      .DoRTCalc(43,43,.f.)
        .w_CLWIPOUT = .w_CLWIPOUT
      .DoRTCalc(44,44,.f.)
      if not(empty(.w_CLWIPOUT))
        .link_2_42('Full')
      endif
      .DoRTCalc(45,45,.f.)
      if not(empty(.w_CLCFASPR))
        .link_2_43('Full')
      endif
      .DoRTCalc(46,54,.f.)
        .w_FLFASEVA = .w_CLFASEVA
        .w_FLCOUPOI = .w_CLCOUPOI
        .w_FLULTFAS = .w_CLULTFAS
        .w_FLFASSEL = .w_CLFASSEL
        .w_FLFASOUT = .w_CLFASOUT
        .w_FLFASCLA = .w_CLFASCLA
        .w_FLPRIFAS = .w_CLPRIFAS
        .w_FLFASCOS = .w_CLFASCOS
      .DoRTCalc(63,66,.f.)
      if not(empty(.w_CLSEOLFA))
        .link_2_58('Full')
      endif
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CLROWORD = t_CLROWORD
    this.w_CL__FASE = t_CL__FASE
    this.w_CLDESFAS = t_CLDESFAS
    this.w_CLINDPRE = t_CLINDPRE
    this.w_CLROWNUM = t_CLROWNUM
    this.w_DESCFA = t_DESCFA
    this.w_EDITROW = t_EDITROW
    this.w_EDITCHK = t_EDITCHK
    this.w_CLPREUM1 = t_CLPREUM1
    this.w_CLAVAUM1 = t_CLAVAUM1
    this.w_CLSCAUM1 = t_CLSCAUM1
    this.w_CLFASEVA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASEVA_2_12.RadioValue(.t.)
    this.w_CLCOUPOI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_13.RadioValue(.t.)
    this.w_CLULTFAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_14.RadioValue(.t.)
    this.w_CLFASSEL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASSEL_2_15.RadioValue(.t.)
    this.w_UM1 = t_UM1
    this.w_CLCPRIFE = t_CLCPRIFE
    this.w_CLQTAPRE = t_CLQTAPRE
    this.w_CLQTAAVA = t_CLQTAAVA
    this.w_CLQTASCA = t_CLQTASCA
    this.w_CLWIPOUT = t_CLWIPOUT
    this.w_DESWIPO = t_DESWIPO
    this.w_CLDATINI = t_CLDATINI
    this.w_CLDATFIN = t_CLDATFIN
    this.w_CLWIPFPR = t_CLWIPFPR
    this.w_DESWIPI = t_DESWIPI
    this.w_CLBFRIFE = t_CLBFRIFE
    this.w_CLCODERR = t_CLCODERR
    this.w_CLQTADIC = t_CLQTADIC
    this.w_CLDICUM1 = t_CLDICUM1
    this.w_CLFASCOS = this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.RadioValue(.t.)
    this.w_CLFASOUT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_33.RadioValue(.t.)
    this.w_TESTROW = t_TESTROW
    this.w_NRVARIATA = t_NRVARIATA
    this.w_CLFASCLA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_36.RadioValue(.t.)
    this.w_EDITALT = t_EDITALT
    this.w_CLPRIFAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_38.RadioValue(.t.)
    this.w_CLLTFASE = t_CLLTFASE
    this.w_CLCODFAS = t_CLCODFAS
    this.w_FASDES = t_FASDES
    this.w_CLWIPOUT = t_CLWIPOUT
    this.w_CLCFASPR = t_CLCFASPR
    this.w_FLTSTAOL = t_FLTSTAOL
    this.w_FLQTAEVA = t_FLQTAEVA
    this.w_FLTPROVE = t_FLTPROVE
    this.w_FLFASEVA = t_FLFASEVA
    this.w_FLCOUPOI = t_FLCOUPOI
    this.w_FLULTFAS = t_FLULTFAS
    this.w_FLFASSEL = t_FLFASSEL
    this.w_FLFASOUT = t_FLFASOUT
    this.w_FLFASCLA = t_FLFASCLA
    this.w_FLPRIFAS = t_FLPRIFAS
    this.w_FLFASCOS = t_FLFASCOS
    this.w_CLWIPDES = t_CLWIPDES
    this.w_CLMAGWIP = t_CLMAGWIP
    this.w_CLSEOLFA = t_CLSEOLFA
    this.w_CLSEOLFA = t_CLSEOLFA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CLROWORD with this.w_CLROWORD
    replace t_CL__FASE with this.w_CL__FASE
    replace t_CLDESFAS with this.w_CLDESFAS
    replace t_CLINDPRE with this.w_CLINDPRE
    replace t_CLROWNUM with this.w_CLROWNUM
    replace t_DESCFA with this.w_DESCFA
    replace t_EDITROW with this.w_EDITROW
    replace t_EDITCHK with this.w_EDITCHK
    replace t_CLPREUM1 with this.w_CLPREUM1
    replace t_CLAVAUM1 with this.w_CLAVAUM1
    replace t_CLSCAUM1 with this.w_CLSCAUM1
    replace t_CLFASEVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASEVA_2_12.ToRadio()
    replace t_CLCOUPOI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_13.ToRadio()
    replace t_CLULTFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_14.ToRadio()
    replace t_CLFASSEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASSEL_2_15.ToRadio()
    replace t_UM1 with this.w_UM1
    replace t_CLCPRIFE with this.w_CLCPRIFE
    replace t_CLQTAPRE with this.w_CLQTAPRE
    replace t_CLQTAAVA with this.w_CLQTAAVA
    replace t_CLQTASCA with this.w_CLQTASCA
    replace t_CLWIPOUT with this.w_CLWIPOUT
    replace t_DESWIPO with this.w_DESWIPO
    replace t_CLDATINI with this.w_CLDATINI
    replace t_CLDATFIN with this.w_CLDATFIN
    replace t_CLWIPFPR with this.w_CLWIPFPR
    replace t_DESWIPI with this.w_DESWIPI
    replace t_CLBFRIFE with this.w_CLBFRIFE
    replace t_CLCODERR with this.w_CLCODERR
    replace t_CLQTADIC with this.w_CLQTADIC
    replace t_CLDICUM1 with this.w_CLDICUM1
    replace t_CLFASCOS with this.oPgFrm.Page1.oPag.oCLFASCOS_2_32.ToRadio()
    replace t_CLFASOUT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_33.ToRadio()
    replace t_TESTROW with this.w_TESTROW
    replace t_NRVARIATA with this.w_NRVARIATA
    replace t_CLFASCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_36.ToRadio()
    replace t_EDITALT with this.w_EDITALT
    replace t_CLPRIFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_38.ToRadio()
    replace t_CLLTFASE with this.w_CLLTFASE
    replace t_CLCODFAS with this.w_CLCODFAS
    replace t_FASDES with this.w_FASDES
    replace t_CLWIPOUT with this.w_CLWIPOUT
    replace t_CLCFASPR with this.w_CLCFASPR
    replace t_FLTSTAOL with this.w_FLTSTAOL
    replace t_FLQTAEVA with this.w_FLQTAEVA
    replace t_FLTPROVE with this.w_FLTPROVE
    replace t_FLFASEVA with this.w_FLFASEVA
    replace t_FLCOUPOI with this.w_FLCOUPOI
    replace t_FLULTFAS with this.w_FLULTFAS
    replace t_FLFASSEL with this.w_FLFASSEL
    replace t_FLFASOUT with this.w_FLFASOUT
    replace t_FLFASCLA with this.w_FLFASCLA
    replace t_FLPRIFAS with this.w_FLPRIFAS
    replace t_FLFASCOS with this.w_FLFASCOS
    replace t_CLWIPDES with this.w_CLWIPDES
    replace t_CLMAGWIP with this.w_CLMAGWIP
    replace t_CLSEOLFA with this.w_CLSEOLFA
    replace t_CLSEOLFA with this.w_CLSEOLFA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mclPag1 as StdContainer
  Width  = 845
  height = 463
  stdWidth  = 845
  stdheight = 463
  resizeXpos=158
  resizeYpos=85
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=3, width=830,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=16,Field1="CLROWORD",Label1="Fase",Field2="CL__FASE",Label2="Codice",Field3="CLDESFAS",Label3="Descrizione fase",Field4="CLINDPRE",Label4="Pref.",Field5="UM1",Label5="UM",Field6="CLQTAPRE",Label6="Pianificata",Field7="CLQTAAVA",Label7="Prodotta",Field8="CLQTASCA",Label8="Scarta",Field9="CLLTFASE",Label9="lead time",Field10="CLFASEVA",Label10="Ev.",Field11="CLCOUPOI",Label11="CP",Field12="CLFASOUT",Label12="OU",Field13="CLPRIFAS",Label13="PF",Field14="CLULTFAS",Label14="U",Field15="CLFASCLA",Label15="CL",Field16="CLFASSEL",Label16="Attiva",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49445766

  add object oStr_1_3 as StdString with uid="YENDMNOXAR",Visible=.t., Left=5, Top=185,;
    Alignment=1, Width=147, Height=17,;
    Caption="Articolo-WIP output fase:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="GPICRZWPPW",Visible=.t., Left=7, Top=164,;
    Alignment=1, Width=145, Height=17,;
    Caption="Articolo-WIP input fase:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="ILWMAGPGLP",Visible=.t., Left=526, Top=142,;
    Alignment=1, Width=76, Height=13,;
    Caption="Data inizio:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="KEVOMSUOZQ",Visible=.t., Left=679, Top=142,;
    Alignment=1, Width=70, Height=13,;
    Caption="Data fine:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="XFOTUGWUQF",Visible=.t., Left=7, Top=142,;
    Alignment=1, Width=145, Height=17,;
    Caption="Articolo di fase:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="HRAODIUVSC",Visible=.t., Left=5, Top=207,;
    Alignment=1, Width=147, Height=17,;
    Caption="Codifica-WIP output fase:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="QJJZMFBZMB",Visible=.t., Left=569, Top=208,;
    Alignment=1, Width=114, Height=17,;
    Caption="Ordine di fase:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=21,;
    width=827+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=22,width=826+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COD_FASI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCLWIPOUT_2_21.Refresh()
      this.Parent.oDESWIPO_2_22.Refresh()
      this.Parent.oCLDATINI_2_23.Refresh()
      this.Parent.oCLDATFIN_2_24.Refresh()
      this.Parent.oCLWIPFPR_2_25.Refresh()
      this.Parent.oDESWIPI_2_26.Refresh()
      this.Parent.oCLFASCOS_2_32.Refresh()
      this.Parent.oCLCODFAS_2_40.Refresh()
      this.Parent.oFASDES_2_41.Refresh()
      this.Parent.oCLCFASPR_2_43.Refresh()
      this.Parent.oCLSEOLFA_2_58.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- gsco_mcl
      This.Parent.oContained.w_ROWSTATUS = This.Parent.oContained.RowStatus()
      This.Parent.oContained.NotifyEvent('SetEditableRow')
      This.Parent.oContained.GSCO_MRL.SetEditableRow()
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COD_FASI'
        oDropInto=this.oBodyCol.oRow.oCL__FASE_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCLWIPOUT_2_21 as StdTrsField with uid="HXNPINVAGH",rtseq=24,rtrep=.t.,;
    cFormVar="w_CLWIPOUT",value=space(20),enabled=.f.,;
    ToolTipText = "Codice output WIP fase",;
    HelpContextID = 24831110,;
    cTotal="", bFixedPos=.t., cQueryName = "CLWIPOUT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=132, Left=155, Top=183, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CLWIPOUT"

  func oCLWIPOUT_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESWIPO_2_22 as StdTrsField with uid="SRTAWCITLK",rtseq=25,rtrep=.t.,;
    cFormVar="w_DESWIPO",value=space(40),enabled=.f.,;
    HelpContextID = 253940790,;
    cTotal="", bFixedPos=.t., cQueryName = "DESWIPO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=236, Left=288, Top=183, InputMask=replicate('X',40)

  add object oCLDATINI_2_23 as StdTrsField with uid="JSNZWDTMGW",rtseq=26,rtrep=.t.,;
    cFormVar="w_CLDATINI",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data inizio lavorazione fase",;
    HelpContextID = 146533231,;
    cTotal="", bFixedPos=.t., cQueryName = "CLDATINI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=68, Left=603, Top=139

  add object oCLDATFIN_2_24 as StdTrsField with uid="EUTJSSMFON",rtseq=27,rtrep=.t.,;
    cFormVar="w_CLDATFIN",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data fine lavorazione fase",;
    HelpContextID = 96201588,;
    cTotal="", bFixedPos=.t., cQueryName = "CLDATFIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=68, Left=750, Top=139

  add object oCLWIPFPR_2_25 as StdTrsField with uid="NRICFPFIEN",rtseq=28,rtrep=.t.,;
    cFormVar="w_CLWIPFPR",value=space(20),enabled=.f.,;
    ToolTipText = "Codice di input aggiuntivo alla fase (output fase precedente)",;
    HelpContextID = 92609400,;
    cTotal="", bFixedPos=.t., cQueryName = "CLWIPFPR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=132, Left=155, Top=161, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CLWIPFPR"

  func oCLWIPFPR_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESWIPI_2_26 as StdTrsField with uid="WBMHBYSKOM",rtseq=29,rtrep=.t.,;
    cFormVar="w_DESWIPI",value=space(40),enabled=.f.,;
    HelpContextID = 253940790,;
    cTotal="", bFixedPos=.t., cQueryName = "DESWIPI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=236, Left=288, Top=161, InputMask=replicate('X',40)

  add object oLinkPC_2_29 as stdDynamicChildContainer with uid="QNONFWTGFF",bOnScreen=.t.,width=831,height=226,;
   left=6, top=232;


  add object oCLFASCOS_2_32 as StdTrsCheck with uid="ZIGHQMDHMN",rtrep=.t.,;
    cFormVar="w_CLFASCOS",  caption="Costificazione consuntiva",;
    HelpContextID = 44829561,;
    Left=671, Top=161,;
    cTotal="", cQueryName = "CLFASCOS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCLFASCOS_2_32.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASCOS,&i_cF..t_CLFASCOS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASCOS_2_32.GetRadio()
    this.Parent.oContained.w_CLFASCOS = this.RadioValue()
    return .t.
  endfunc

  func oCLFASCOS_2_32.ToRadio()
    this.Parent.oContained.w_CLFASCOS=trim(this.Parent.oContained.w_CLFASCOS)
    return(;
      iif(this.Parent.oContained.w_CLFASCOS=='S',1,;
      0))
  endfunc

  func oCLFASCOS_2_32.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFASCOS_2_32.mCond()
    with this.Parent.oContained
      return (.w_EDITROW and .w_CLCOUPOI<>'S')
    endwith
  endfunc

  add object oCLCODFAS_2_40 as StdTrsField with uid="PLMRCATFSS",rtseq=42,rtrep=.t.,;
    cFormVar="w_CLCODFAS",value=space(20),enabled=.f.,;
    HelpContextID = 188097671,;
    cTotal="", bFixedPos=.t., cQueryName = "CLCODFAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=132, Left=155, Top=139, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CLCODFAS"

  func oCLCODFAS_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CLCFASPR)
        bRes2=.link_2_43('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFASDES_2_41 as StdTrsField with uid="YJXMKTICFW",rtseq=43,rtrep=.t.,;
    cFormVar="w_FASDES",value=space(40),enabled=.f.,;
    HelpContextID = 238038954,;
    cTotal="", bFixedPos=.t., cQueryName = "FASDES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=236, Left=288, Top=139, InputMask=replicate('X',40)

  add object oCLCFASPR_2_43 as StdTrsField with uid="HAXPONIZCM",rtseq=45,rtrep=.t.,;
    cFormVar="w_CLCFASPR",value=space(66),enabled=.f.,;
    HelpContextID = 242164872,;
    cTotal="", bFixedPos=.t., cQueryName = "CLCFASPR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=408, Left=155, Top=205, InputMask=replicate('X',66), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CLCODFAS", oKey_2_1="CACODFAS", oKey_2_2="this.w_CLCFASPR"

  func oCLCFASPR_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCLSEOLFA_2_58 as StdTrsField with uid="BADMFRSEOB",rtseq=66,rtrep=.t.,;
    cFormVar="w_CLSEOLFA",value=space(15),;
    HelpContextID = 76489881,;
    cTotal="", bFixedPos=.t., cQueryName = "CLSEOLFA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=686, Top=205, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_CLSEOLFA"

  func oCLSEOLFA_2_58.mCond()
    with this.Parent.oContained
      return (1=2)
    endwith
  endfunc

  func oCLSEOLFA_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLSEOLFA_2_58.ecpDrop(oSource)
    this.Parent.oContained.link_2_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCLSEOLFA_2_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oCLSEOLFA_2_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"",'',this.parent.oContained
  endproc
  proc oCLSEOLFA_2_58.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLCODODL=this.parent.oContained.w_CLSEOLFA
    i_obj.ecpSave()
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsco_mclBodyRow as CPBodyRowCnt
  Width=817
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCLROWORD_2_1 as StdTrsField with uid="OMGXPNLSUE",rtseq=4,rtrep=.t.,;
    cFormVar="w_CLROWORD",value=0,;
    HelpContextID = 17118358,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=["@Z 999"], cGetPict=["999"]

  add object oCL__FASE_2_2 as StdTrsField with uid="LLFOUCTDYW",rtseq=5,rtrep=.t.,;
    cFormVar="w_CL__FASE",value=space(5),;
    HelpContextID = 287893,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=48, Left=57, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_FASI", oKey_1_1="CFCODICE", oKey_1_2="this.w_CL__FASE"

  func oCL__FASE_2_2.mCond()
    with this.Parent.oContained
      return (.w_EDITROW)
    endwith
  endfunc

  func oCL__FASE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCL__FASE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCL__FASE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_FASI','*','CFCODICE',cp_AbsName(this.parent,'oCL__FASE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCLDESFAS_2_3 as StdTrsField with uid="YEXBQUBXUS",rtseq=6,rtrep=.t.,;
    cFormVar="w_CLDESFAS",value=space(40),;
    HelpContextID = 173020295,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=156, Left=108, Top=0, InputMask=replicate('X',40)

  func oCLDESFAS_2_3.mCond()
    with this.Parent.oContained
      return (.w_EDITROW)
    endwith
  endfunc

  add object oCLINDPRE_2_4 as StdTrsField with uid="HTGBCQZQLE",rtseq=7,rtrep=.t.,;
    cFormVar="w_CLINDPRE",value=space(2),;
    ToolTipText = "Indice di preferenza della fase",;
    HelpContextID = 20366485,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=271, Top=0, InputMask=replicate('X',2)

  func oCLINDPRE_2_4.mCond()
    with this.Parent.oContained
      return (.w_EDITROW)
    endwith
  endfunc

  add object oCLFASEVA_2_12 as StdTrsCheck with uid="IETWSNHZWG",rtrep=.t.,;
    cFormVar="w_CLFASEVA", enabled=.f., caption="",;
    ToolTipText = "Fase evasa",;
    HelpContextID = 190051481,;
    Left=654, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCLFASEVA_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASEVA,&i_cF..t_CLFASEVA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASEVA_2_12.GetRadio()
    this.Parent.oContained.w_CLFASEVA = this.RadioValue()
    return .t.
  endfunc

  func oCLFASEVA_2_12.ToRadio()
    this.Parent.oContained.w_CLFASEVA=trim(this.Parent.oContained.w_CLFASEVA)
    return(;
      iif(this.Parent.oContained.w_CLFASEVA=='S',1,;
      0))
  endfunc

  func oCLFASEVA_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLCOUPOI_2_13 as StdTrsCheck with uid="FZXIHFJYIV",rtrep=.t.,;
    cFormVar="w_CLCOUPOI",  caption="",;
    ToolTipText = "Fase di count point",;
    HelpContextID = 265935727,;
    Left=677, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCLCOUPOI_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLCOUPOI,&i_cF..t_CLCOUPOI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLCOUPOI_2_13.GetRadio()
    this.Parent.oContained.w_CLCOUPOI = this.RadioValue()
    return .t.
  endfunc

  func oCLCOUPOI_2_13.ToRadio()
    this.Parent.oContained.w_CLCOUPOI=trim(this.Parent.oContained.w_CLCOUPOI)
    return(;
      iif(this.Parent.oContained.w_CLCOUPOI=='S',1,;
      0))
  endfunc

  func oCLCOUPOI_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLCOUPOI_2_13.mCond()
    with this.Parent.oContained
      return (.w_EDITCHK)
    endwith
  endfunc

  add object oCLULTFAS_2_14 as StdTrsCheck with uid="AQQHMEHJCT",rtrep=.t.,;
    cFormVar="w_CLULTFAS", enabled=.f., caption="",;
    ToolTipText = "Se attivo indica che � l'ultima fase del ciclo",;
    HelpContextID = 171443335,;
    Left=746, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCLULTFAS_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLULTFAS,&i_cF..t_CLULTFAS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLULTFAS_2_14.GetRadio()
    this.Parent.oContained.w_CLULTFAS = this.RadioValue()
    return .t.
  endfunc

  func oCLULTFAS_2_14.ToRadio()
    this.Parent.oContained.w_CLULTFAS=trim(this.Parent.oContained.w_CLULTFAS)
    return(;
      iif(this.Parent.oContained.w_CLULTFAS=='S',1,;
      0))
  endfunc

  func oCLULTFAS_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLFASSEL_2_15 as StdTrsCheck with uid="OASUBZGDTY",rtrep=.t.,;
    cFormVar="w_CLFASSEL",  caption="",;
    ToolTipText = "Indica se la fase � stata selezionata o no",;
    HelpContextID = 223605902,;
    Left=792, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCLFASSEL_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASSEL,&i_cF..t_CLFASSEL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASSEL_2_15.GetRadio()
    this.Parent.oContained.w_CLFASSEL = this.RadioValue()
    return .t.
  endfunc

  func oCLFASSEL_2_15.ToRadio()
    this.Parent.oContained.w_CLFASSEL=trim(this.Parent.oContained.w_CLFASSEL)
    return(;
      iif(this.Parent.oContained.w_CLFASSEL=='S',1,;
      0))
  endfunc

  func oCLFASSEL_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFASSEL_2_15.mCond()
    with this.Parent.oContained
      return (.w_EDITCHK Or .w_EDITALT and (.w_CLFASEVA<>"S"))
    endwith
  endfunc

  add object oUM1_2_16 as StdTrsField with uid="TZJFUWAGAP",rtseq=19,rtrep=.t.,;
    cFormVar="w_UM1",value=space(3),enabled=.f.,;
    HelpContextID = 171556166,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=297, Top=0, InputMask=replicate('X',3)

  add object oCLQTAPRE_2_18 as StdTrsField with uid="LSKHSURHMY",rtseq=21,rtrep=.t.,;
    cFormVar="w_CLQTAPRE",value=0,enabled=.f.,;
    HelpContextID = 23086229,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=330, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_PQ(12)]

  add object oCLQTAAVA_2_19 as StdTrsField with uid="RCCRXYDSAT",rtseq=22,rtrep=.t.,;
    cFormVar="w_CLQTAAVA",value=0,enabled=.f.,;
    HelpContextID = 6309017,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=410, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_PQ(12)]

  add object oCLQTASCA_2_20 as StdTrsField with uid="HCMVBETSUI",rtseq=23,rtrep=.t.,;
    cFormVar="w_CLQTASCA",value=0,enabled=.f.,;
    HelpContextID = 241190041,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=488, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_PQ(12)]

  add object oCLFASOUT_2_33 as StdTrsCheck with uid="BTPVRIBSIO",rtrep=.t.,;
    cFormVar="w_CLFASOUT",  caption="",;
    ToolTipText = "Fase di output",;
    HelpContextID = 22279302,;
    Left=700, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., backstyle=0;
   , bGlobalFont=.t.


  func oCLFASOUT_2_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASOUT,&i_cF..t_CLFASOUT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASOUT_2_33.GetRadio()
    this.Parent.oContained.w_CLFASOUT = this.RadioValue()
    return .t.
  endfunc

  func oCLFASOUT_2_33.ToRadio()
    this.Parent.oContained.w_CLFASOUT=trim(this.Parent.oContained.w_CLFASOUT)
    return(;
      iif(this.Parent.oContained.w_CLFASOUT=='S',1,;
      0))
  endfunc

  func oCLFASOUT_2_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFASOUT_2_33.mCond()
    with this.Parent.oContained
      return (.w_CLCOUPOI='S' AND .w_EDITCHK)
    endwith
  endfunc

  add object oCLFASCLA_2_36 as StdTrsCheck with uid="LMPUALTUUZ",rtrep=.t.,;
    cFormVar="w_CLFASCLA", enabled=.f., caption="",;
    ToolTipText = "Se attivo la fase � conto lavoro",;
    HelpContextID = 44829543,;
    Left=769, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCLFASCLA_2_36.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASCLA,&i_cF..t_CLFASCLA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASCLA_2_36.GetRadio()
    this.Parent.oContained.w_CLFASCLA = this.RadioValue()
    return .t.
  endfunc

  func oCLFASCLA_2_36.ToRadio()
    this.Parent.oContained.w_CLFASCLA=trim(this.Parent.oContained.w_CLFASCLA)
    return(;
      iif(this.Parent.oContained.w_CLFASCLA=='S',1,;
      0))
  endfunc

  func oCLFASCLA_2_36.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLPRIFAS_2_38 as StdTrsCheck with uid="JETIGHTQLZ",rtrep=.t.,;
    cFormVar="w_CLPRIFAS", enabled=.f., caption="",;
    ToolTipText = "Se attivo indica che � la prima fase del ciclo",;
    HelpContextID = 182604935,;
    Left=723, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCLPRIFAS_2_38.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLPRIFAS,&i_cF..t_CLPRIFAS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLPRIFAS_2_38.GetRadio()
    this.Parent.oContained.w_CLPRIFAS = this.RadioValue()
    return .t.
  endfunc

  func oCLPRIFAS_2_38.ToRadio()
    this.Parent.oContained.w_CLPRIFAS=trim(this.Parent.oContained.w_CLPRIFAS)
    return(;
      iif(this.Parent.oContained.w_CLPRIFAS=='S',1,;
      0))
  endfunc

  func oCLPRIFAS_2_38.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLLTFASE_2_39 as StdTrsField with uid="UUJLNKCAAH",rtseq=41,rtrep=.t.,;
    cFormVar="w_CLLTFASE",value=0,;
    HelpContextID = 1086613,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=572, Top=0, cSayPict=["@Z 99,999,999.999"], cGetPict=["99,999,999.999"]

  func oCLLTFASE_2_39.mCond()
    with this.Parent.oContained
      return (.w_EDITROW)
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCLROWORD_2_1.When()
    return(.t.)
  proc oCLROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCLROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mcl','ODL_CICL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CLCODODL=ODL_CICL.CLCODODL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
