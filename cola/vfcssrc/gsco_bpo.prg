* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bpo                                                        *
*              Gestione piano ODL                                              *
*                                                                              *
*      Author: Zucchetti TAM Srl (SM)                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-27                                                      *
* Last revis.: 2017-06-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bpo",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_bpo as StdBatch
  * --- Local variables
  pAzione = space(10)
  PunPAD = .NULL.
  TMPc = space(10)
  w_CODODL = space(20)
  w_CURS = space(10)
  w_PROVE = space(1)
  GestODL = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azione ...
    * --- Visioni dal padre ...
    * --- Variabili locali
    private Scelta
    Scelta = rTrim(this.pAzione)
    this.PunPAD = this.oParentObject
    this.w_CURS = this.PunPAD.w_ZoomODL.cCursor
    do case
      case this.pAzione="VISUALIZZA_ODL" or this.pAzione="ELIMINA_ODL" or this.pAzione="CARICA_ODL" or this.pAzione="MODIFICA_ODL"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione = "Verifica temporale"
        SELECT (this.w_CURS)
        if Inlist(this.oParentObject.w_MPARAM, "E", "L")
          SCAN FOR STA<>"F" and empty(nvl(oltdtini, {})) and (oltdinric <= i_DATSYS and empty(nvl(oltdtlan, {})) or oltdtric < i_DATSYS )
          REPLACE PIA WITH IIF(oltdinric=i_DATSYS and empty(nvl(oltdtlan, {})) ,1,2)
          ENDSCAN
        else
          SCAN FOR STA<>"F" and ((oltdinric<=i_DATSYS and (oltdtini>i_DATSYS OR empty(nvl(oltdtini, {})))) or OLTDTRIC<i_datsys)
          REPLACE PIA WITH IIF(oltdinric=i_DATSYS,1,2)
          ENDSCAN
        endif
        SELECT (this.w_CURS)
        this.PunPAD.w_ZoomODL.refresh()     
      case this.pAzione = "TI"
        * --- Se la maschera viene lanciata con MPARAM = 'L' (OCL) cambio titolo
        *     alla maschera e cambio le intestazioni dello zoom
        do case
          case this.oParentObject.w_MPARAM="L"
            this.PunPAD.cComment = AH_Msgformat("PIANO ORDINI DI CONTO LAVORO")
            this.PunPAD.Caption = this.PunPAD.cComment
            this.PunPAD.w_ZoomODL.setColumnSource(1,"OLCODODL", AH_Msgformat("OCL"))     
            this.PunPAD.w_ZoomODL.setColumnSource(8,"OLTDTLAN", AH_Msgformat("Lancio OCL"))     
            this.PunPAD.w_ZoomODL.setColumnSource(5,"OLTDTINI","Inizio OCL")     
            this.PunPAD.w_ZoomODL.setColumnSource(7,"OLTDTFIN","Fine OCL")     
            this.PunPAD.w_ZoomODL.setColumnSource(14,"OLTCOFOR","Terzista")     
          case this.oParentObject.w_MPARAM="E"
            this.PunPAD.cComment = "PIANO ORDINI DI ACQUISTO"
            this.PunPAD.Caption = this.PunPAD.cComment
            this.PunPAD.w_ZoomODL.setColumnSource(1,"OLCODODL","ODA")     
            this.PunPAD.w_ZoomODL.setColumnSource(8,"OLTDTLAN","Lancio ODA")     
            this.PunPAD.w_ZoomODL.setColumnSource(5,"OLTDTINI","Inizio ODA")     
            this.PunPAD.w_ZoomODL.setColumnSource(7,"OLTDTFIN","Fine ODA")     
            this.PunPAD.w_ZoomODL.grd.column15.width = 0
            this.PunPAD.w_ZoomODL.grd.column15.Visible = .f.
          otherwise
            this.PunPAD.w_ZoomODL.setColumnSource(1,"OLCODODL","ODL")     
            this.PunPAD.w_ZoomODL.grd.column14.width = 0
            this.PunPAD.w_ZoomODL.grd.column14.Visible = .f.
            this.PunPAD.w_ZoomODL.grd.column15.width = 0
            this.PunPAD.w_ZoomODL.grd.column15.Visible = .f.
        endcase
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge ODP selezionato
    this.oParentObject.w_ODLSEL = this.PUNPAD.w_ZoomODL.GetVar("OLCODODL")
    this.w_PROVE = this.PUNPAD.w_ZoomODL.GetVar("PROV")
    * --- Definisce e chiama gestione
    do case
      case this.w_PROVE="I"
        this.GestODL = GSCO_AOP(.NULL., "L")
      case this.w_PROVE="E"
        this.GestODL = GSCO_AOP(.NULL., "E")
      otherwise
        this.GestODL = GSCO_AOP(.NULL., "Z")
    endcase
    * --- Con il secondo parametro rinfresca zoom MPS e ODL (con Pivot)
    * --- Carica record e lascia in interrograzione ...
    this.GestODL.w_OLCODODL = this.oParentObject.w_ODLSEL
    this.TMPc = "OLCODODL="+cp_ToStrODBC(this.oParentObject.w_ODLSEL)
    this.GestODL.QueryKeySet(this.TmpC,"")     
    this.GestODL.LoadRecWarn()     
    do case
      case Scelta = "MODIFICA_ODL"
        * --- Modifica record
        this.GestODL.ECPEdit()     
      case Scelta = "ELIMINA_ODL"
        * --- Cancella record ...
        this.GestODL.ECPDelete()     
      case Scelta = "CARICA_ODL"
        this.GestODL.ECPLoad()     
        this.GestODL.w_OLTSTATO = "P"
        this.GestODL.mCalc(.T.)     
    endcase
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce Menu
    DEFINE POPUP MENUMPS from MRow()+1,MCol()+1 shortcut margin
    DEFINE BAR 1 OF MENUMPS prompt AH_Msgformat("Visualizza")
    DEFINE BAR 2 OF MENUMPS prompt AH_Msgformat("Modifica")
    DEFINE BAR 3 OF MENUMPS prompt AH_Msgformat("Carica")
    DEFINE BAR 4 OF MENUMPS prompt "\-"
    DEFINE BAR 5 OF MENUMPS prompt AH_Msgformat("Elimina")
    ON SELE BAR 1 OF MENUMPS Scelta="VISUALIZZA_ODL"
    ON SELE BAR 2 OF MENUMPS Scelta="MODIFICA_ODL"
    ON SELE BAR 3 OF MENUMPS Scelta="CARICA_ODL"
    ON SELE BAR 5 OF MENUMPS Scelta="ELIMINA_ODL"
    ACTI POPUP MENUMPS
    DEACTIVATE POPUP MENUMPS
    RELEASE POPUPS MENUMPS EXTENDED
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
