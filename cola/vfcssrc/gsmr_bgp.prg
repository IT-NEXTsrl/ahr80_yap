* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bgp                                                        *
*              Elaborazione MRP                                                *
*                                                                              *
*      Author: Zucchetti TAM SpA (DB)                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_651]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-09                                                      *
* Last revis.: 2018-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsmr_bgp
Declare Integer IsHungAppWindow In user32;
		INTEGER HWnd
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bgp",oParentObject)
return(i_retval)

define class tgsmr_bgp as StdBatch
  * --- Local variables
  w_CANCELROUTINE = .f.
  w_LenKeysal = 0
  w_CHECKINIT = 0
  w_PPCENCOS = space(15)
  w_PPCCSODA = space(15)
  w_PPMAGSCA = space(5)
  w_PPSCAMRP = space(1)
  w_Res = 0
  w_FileName = space(200)
  w_Seriale = space(10)
  f_Error = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_FilePath = space(200)
  w_FL = space(10)
  sp_FILELOG = space(10)
  hf_FILE_LOG = 0
  w_TEMP = space(10)
  w_INIZ_DATE = ctod("  /  /  ")
  w_NRECPARRIMA = 0
  w_SALCOD = space(40)
  w_QTAMAG = 0
  w_RIGAORD = 0
  w_ARFLCOM1 = space(1)
  w_INTWAIT = 0
  w_INTWAIT = 0
  qtaabb = 0
  riga = 0
  w_ABBQTA = 0
  w_SaldProgPH = 0
  w_FabnetPH = 0
  oBtnCancel = .NULL.
  w_INTWAIT1 = 0
  w_DATREG = ctod("  /  /  ")
  w_ORAREG = space(8)
  w_MCRIFOR = space(1)
  w_PROGPH = 0
  w_PROFAN = space(240)
  w_CodSaldo1 = space(40)
  w_FOUND = .f.
  w_GrImpCur = space(5)
  w_CONTRAOLD = space(15)
  w_FORNIOLD = space(15)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CODARTVAR = space(40)
  w_CONUMERO = space(15)
  w_CODCON = space(15)
  w_CENCOS = space(15)
  w_VAL = space(3)
  w_CAO = 0
  w_PZ = 0
  w_S1 = 0
  w_S2 = 0
  w_S3 = 0
  w_S4 = 0
  w_PZ = 0
  w_LOTRIO = 0
  w_QTAMIN = 0
  w_GIOAPP = 0
  w_Fabpar = 0
  w_QTALOT = 0
  w_MINRIO = 0
  w_LOTRIODEF = 0
  w_QTAMINDEF = 0
  w_appo1 = space(10)
  w_MAXDATE = space(8)
  w_dbiniz = 0
  w_NUCODODL = 0
  w_NUMODL = 0
  w_dbiniz1 = 0
  w_OLPROVE = space(1)
  w_WIPFASPRE = space(5)
  w_WIPFASOLD = space(5)
  w_OLTDISBA = space(20)
  w_OLTKEYSA = space(20)
  w_MAGIMP = space(5)
  w_COMMDETT = space(15)
  w_OLTKEYSA = space(20)
  w_OLTIPPRE = space(1)
  w_OLEVAAUT = space(1)
  w_OLTKEYSA = space(20)
  w_OLTKEYSA = space(20)
  w_ODLDAP = space(15)
  w_dbiniz2 = 0
  w_ErrorBID = .f.
  w_DELORDS = space(1)
  w_LenCodSal = 0
  w_CODARTF = space(20)
  w_CODFORF = space(15)
  w_appo1 = space(10)
  w_ErrorLLC = .f.
  w_CodSaldo = space(40)
  w_TipoPadre = space(2)
  w_RifMate = 0
  w_UDPNAME = .f.
  w_SaldProg = 0
  w_ChiaveInd = space(86)
  w_ChiaveInd2 = space(86)
  w_ChiaveSal = space(82)
  w_Dispon = 0
  w_RecNumb = 0
  w_QTA = 0
  w_Padre_er = space(15)
  w_Fabnet = 0
  w_Leadt = 0
  w_ElabFabb = .f.
  w_AltroPer = .f.
  w_Datidx = space(8)
  w_Keyidx = space(86)
  w_Datini = ctod("  /  /  ")
  w_Dateva = ctod("  /  /  ")
  w_Datimp = ctod("  /  /  ")
  w_Tiprec = space(6)
  w_ODLprog = 0
  w_OLCODODL = space(15)
  w_OLTCODIC = space(20)
  w_PPCAUIMP = space(5)
  w_IMPFLORD = space(1)
  w_IMPFLIMP = space(1)
  w_IMPFLRIS = space(1)
  w_PPCAUORD = space(5)
  w_ORDFLORD = space(1)
  w_ORDFLIMP = space(1)
  w_PPMAGPRO = space(5)
  w_PPGIOSCA = 0
  w_PPCARVAL = space(1)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_TMP = space(20)
  w_SERCIC = space(10)
  w_OLRIFFAS = 0
  w_OLCPRIFE = 0
  w_PRRIFFAS = 0
  w_PRCPRIFE = 0
  w_CLFASSEL = space(1)
  w_CLCOUPOI = space(1)
  w_CLFASOUT = space(1)
  w_CLFASCLA = space(1)
  w_CLCODFAS = space(41)
  w_RIGEN = space(1)
  w_ErrMsg = space(10)
  w_RollBack = space(1)
  w_CRIFOR = space(1)
  w_DATELA = ctod("  /  /  ")
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_TipoContra = 0
  w_CLWIPFPR = space(41)
  w_WIPFPR = space(41)
  w_LogErrori = space(10)
  w_CodMag = space(5)
  w_CodVar = space(20)
  w_MatAltern = space(41)
  w_SKIP = .f.
  w_GEN_PDA = .f.
  w_PDAMSG = .f.
  firstcom = .f.
  w_PPBLOMRP = space(1)
  w_PPBPIACL = space(1)
  w_PPMRPDIV = space(1)
  w_PPORDICM = space(1)
  w_LNumErr = 0
  w_LSerial = space(10)
  w_LOperaz = space(2)
  w_LOggErr = space(41)
  w_LErrore = space(80)
  w_LEERR = space(250)
  w_LOra = space(8)
  w_LDettTec = space(1)
  w_LSelOpe = space(1)
  w_LDATINI = ctod("  /  /  ")
  w_LDATFIN = ctod("  /  /  ")
  TmpC = space(100)
  w_CodArt = space(20)
  w_CodFor = space(15)
  w_CONTRA = space(10)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_COMMES = .f.
  w_COMMES1 = .f.
  w_SaldComm = 0
  w_ImpComm = 0
  w_CMFLCOMM = space(1)
  w_FLORCO = space(1)
  w_FLCOCO = space(1)
  w_OLTSTATO = space(1)
  w_OLTCOCOS = space(5)
  w_OLTFLIMC = space(1)
  w_OLTFORCO = space(1)
  w_OLTFCOCO = space(1)
  w_COSTO = 0
  w_COEFFA = 0
  w_COEFFA1 = 0
  w_IDEGRU = space(2)
  w_NUMIDE = 0
  w_CHKDATI = .f.
  w_CURCHECK = 0
  w_QUERYCHK = space(20)
  w_OLFLEVAS = space(1)
  w_OLDPOS = 0
  w_RECCNT = 0
  w_ad_hoc_ENTERPRISE = .f.
  w_CONTART = space(20)
  w_CONTVAR = space(20)
  w_CONTSAL = space(40)
  w_PROVPAD = space(1)
  w_nocomp = .f.
  w_MESSRIP = .f.
  w_MRSERIAL = 0
  w_PEGGING = .f.
  w_NRECELAB = 0
  w_NTMPREC = 0
  w_NRECCUR = 0
  w_PREZUM = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CLFASCOS = space(1)
  w_PRCRIFOR = space(1)
  w_CODCON = space(15)
  w_LOTRIODEF = 0
  w_QTAMINDEF = 0
  w_LOTRIO = 0
  w_QTAMIN = 0
  w_GIOAPP = 0
  w_CDARTICOLO = space(20)
  w_CDVARIANTE = space(20)
  w_appoggio = space(10)
  w_QTAMINTROV = 0
  w_QTAMINSCAN = 0
  w_LOTRIOSCAN = 0
  w_QTAMINPARZ = 0
  TmpN = 0
  w_LOTSMUL = 0
  w_TIPLISBP = space(1)
  w_TIPLISPP = space(1)
  w_GENPODA = space(1)
  w_ELABID = space(1)
  w_PPCENCOS = space(15)
  w_CENCOS = space(15)
  w_QTARES = 0
  w_QTAMAX = 0
  w_QTALOT = 0
  w_MINRIO = 0
  w_Fabpar = 0
  w_codsal = space(40)
  w_FLPROMO = space(1)
  w_NUCODODL = 0
  w_NUMODL = 0
  w_FIRSTODL = space(15)
  w_QTAIMP = 0
  w_Comm = space(15)
  w_FIRSTLAP = .f.
  w_OLDCOM = .f.
  w_RESCOM = 0
  w_GESCOMM = space(2)
  w_OldSaldo = space(40)
  w_NEGAPP = space(80)
  w_QTA1 = 0
  w_CCSODL = space(15)
  w_CCSOCL = space(15)
  w_CCSODA = space(15)
  w_OLCENCOS = space(15)
  w_PROMO = space(1)
  w_MAGRIF = space(5)
  w_PIAODP = .f.
  w_nocomp = .f.
  w_Fabnet1 = 0
  w_recelabor = 0
  w_UMORDI = space(3)
  w_QTAORD = 0
  w_PPMATINP = space(1)
  w_DisponNett = 0
  rigar = 0
  w_QTANC = 0
  contarray = 0
  w_SERODL = space(15)
  w_OLTSTATODP = space(1)
  w_MATOUP = space(1)
  w_UTCV = 0
  w_UTDV = ctod("  /  /  ")
  w_OPV = space(1)
  Padre = .NULL.
  w_PARAM = space(1)
  w_APPO = space(200)
  w_CODINI = space(41)
  w_CODFIN = space(41)
  w_SALINI = space(40)
  w_SALFIN = space(40)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_FAMINI = space(5)
  w_FAMFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_PIAINI = space(5)
  w_PIAFIN = space(5)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_INICLI = space(15)
  w_FINCLI = space(15)
  w_DISMAG = space(1)
  w_GIANEG = space(1)
  w_ORDIPROD = space(1)
  w_IMPEPROD = space(1)
  w_ORDMPS = space(1)
  w_STAORD = space(1)
  w_ELACAT = space(1)
  w_INIELA = ctod("  /  /  ")
  w_FINELA = ctod("  /  /  ")
  w_ORIODL = space(1)
  NC = space(10)
  NM = space(10)
  w_FILTERS = .f.
  w_SELEZ = space(1)
  w_SUGG = space(1)
  w_TIPDOCM = space(0)
  w_LISMAG = space(0)
  w_KEYSAL = space(40)
  w_PATHEL = space(200)
  w_NOTMRP = space(0)
  w_MARINI = space(5)
  w_MARFIN = space(5)
  w_LISINI = space(5)
  w_LISFIN = space(5)
  w_COMINI = space(15)
  w_COMFIN = space(15)
  w_COMODL = space(1)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  w_DOCINI = ctod("  /  /  ")
  w_DOCFIN = ctod("  /  /  ")
  w_FILDOC = space(1)
  w_STIPART = space(1)
  w_PROFIN = space(2)
  w_SEMLAV = space(2)
  w_MATPRI = space(2)
  w_MATAUS = space(2)
  w_MATCON = space(2)
  w_IMBALL = space(2)
  w_GENODA = space(1)
  w_BIDODA = space(1)
  w_MICOM = space(1)
  w_MIODL = space(1)
  w_MIOCL = space(1)
  w_MIODA = space(1)
  w_MAGFOR = space(5)
  w_MAGFOC = space(5)
  w_MAGFOL = space(5)
  w_MAGFOA = space(5)
  w_DISMAGFO = space(1)
  w_DISMGPRO = space(1)
  w_MRPLOG = space(1)
  w_oMRPLOG = .NULL.
  w_LogTitle = space(10)
  w_FL = space(10)
  sp_FILELOG = space(10)
  hf_FILE_LOG = 0
  w_TEMP = space(10)
  w_INIZ = 0
  w_FINE = 0
  w_TOTELAB = 0
  w_finedbiniz = 0
  w_finedbagg = 0
  w_QUANTI = 0
  w_LLCINIZ = 0
  w_LLCFINE = 0
  w_LLCTOT = 0
  w_FILINIZ = 0
  w_FILFIN = 0
  w_FILTOT = 0
  w_IDBINIZ = 0
  w_IDBFIN = 0
  w_IDBTOT = 0
  w_DARINIZ = 0
  w_DARFIN = 0
  w_DARTOT = 0
  w_DOCINIZ = 0
  w_DOCUFIN = 0
  w_DOCTOT = 0
  w_SETFILINIZ = 0
  w_SETFILFIN = 0
  w_SETFILTOT = 0
  w_IMPINIZ = 0
  w_IMPFIN = 0
  w_IMPTOT = 0
  w_ODPINIZ = 0
  w_ODPFIN = 0
  w_ODPTOT = 0
  w_COMINIZ = 0
  w_COMFINE = 0
  w_COMTOT = 0
  w_SLINIZ = 0
  w_SLFIN = 0
  w_SLTOT = 0
  w_CLINIZ = 0
  w_CLFIN = 0
  w_CLTOT = 0
  w_COINIZ = 0
  w_COFIN = 0
  w_COTOT = 0
  w_LEGINIZ = 0
  w_LEGFIN = 0
  w_LEGTOT = 0
  w_INIZINIZ = 0
  w_INIZFIN = 0
  w_INIZTOT = 0
  w_MRPINIZ = 0
  w_MRPFIN = 0
  w_MRPTOT = 0
  w_DBFINIZ = 0
  w_DBFFIN = 0
  w_DBFTOT = 0
  w_COLINIZ = 0
  w_COLFIN = 0
  w_COLTOT = 0
  w_MESINIZ = 0
  w_MESFIN = 0
  w_MESTOT = 0
  w_PDAINIZ = 0
  w_PDAFIN = 0
  w_PDATOT = 0
  w_PEGINIZ = 0
  w_PEGFIN = 0
  w_PEGTOT = 0
  w_STOINIZ = 0
  w_STOFIN = 0
  w_STOTOT = 0
  w_CALINIZ = 0
  w_CALFIN = 0
  w_CALTOT = 0
  w_exit = .f.
  w_PRDESSUP = space(1)
  w_UMLIPREZZO = space(3)
  m_LOTSMUL = 0
  m_QTAMIN = 0
  w_PDCODINI = space(41)
  w_PDCODFIN = space(41)
  w_PDCOMFIN = space(15)
  w_PDCOMINI = space(15)
  w_PDCATFIN = space(5)
  w_PDCATINI = space(5)
  w_PDFAMAFI = space(5)
  w_PDFAMAIN = space(5)
  w_PDFAMFIN = space(5)
  w_PDFAMINI = space(5)
  w_PDGRUFIN = space(5)
  w_PDGRUINI = space(5)
  w_PDMAGFIN = space(5)
  w_PDMAGINI = space(5)
  w_PDMARFIN = space(5)
  w_PDMARINI = space(5)
  w_TIPELA = space(1)
  w_PDPIAFIN = space(5)
  w_PDPIAINI = space(5)
  w_ELAPDF = space(1)
  w_ELAPDI = space(1)
  w_ELAPDS = space(1)
  w_PDNOTEEL = space(0)
  w_PPFLAROB = space(1)
  w_PathRel = space(50)
  w_PathRelChk = space(50)
  w_CRIELA = space(1)
  w_GPCODMAG = space(0)
  ZOOMMAGA = .NULL.
  w_SELZZMAG = .f.
  w_SELZZGMG = .f.
  w_SELART = space(1)
  w_NR_R_ART = 0
  w_NRARTSEL = 0
  w_SELMAG = space(1)
  w_NR_R_MAG = 0
  w_NRMAGSEL = 0
  w_PERPIA = space(1)
  w_PIAPUN = space(1)
  w_TipImpe = space(1)
  w_CodMag = space(5)
  w_DisCodMg = space(1)
  w_GruMag = space(5)
  w_DisGruMg = space(1)
  w_MagImp = space(5)
  w_DisMgImp = space(1)
  w_MagPre = space(5)
  w_DisMgPre = space(1)
  w_MgImpCur = space(5)
  w_DisMgCur = space(1)
  w_COMMDEFA = space(15)
  w_SELEIMPE = space(1)
  w_CambioCodice = .f.
  w_nRecFasi = 0
  w_OLTPROVE = space(1)
  w_CLFPROVE = space(1)
  w_DELORD = space(1)
  w_UseProgBar = .f.
  w_PROGBAR = .NULL.
  w_RootKeyIdx = 0
  w_LenKeyIdx = 0
  w_FILTROART = space(200)
  w_SELEZART = space(1)
  w_SELEZODL = space(1)
  w_FILTERSART = .f.
  w_FILTRODOC = space(200)
  w_SELEZDOC = space(1)
  w_FILTERSDOC = .f.
  w_FILTERSODL = .f.
  w_FILTROODL = space(200)
  w_GRUPPO = space(0)
  w_KeyidxPer = space(86)
  w_DatidxPer = space(8)
  w_bTITLE = space(40)
  w_NTOTCHECK = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  DatRisul = ctod("  /  /  ")
  DatInput = space(8)
  w_DatErr = .f.
  w_DatErrMsg = space(10)
  w_MRDESCRI = space(80)
  w_MRTIPOMS = space(2)
  w_MRINTEXT = space(1)
  w_Disponibilita = 0
  w_TODEL = 0
  w_dataAppo = ctod("  /  /  ")
  w_datiniAppo = ctod("  /  /  ")
  w_point = 0
  w_quantiFab = 0
  w_quantiODL = 0
  w_dispoAppo = 0
  w_disper = 0
  w_codart = space(20)
  w_odlmsg = space(15)
  w_DATALIMITE = ctod("  /  /  ")
  w_DATAIMPE = 0
  w_ENMSG = .f.
  w_STAOD = space(1)
  w_ROWELAB = 0
  w_day = 0
  w_change = .f.
  w_PMPERCON = 0
  w_PMODLSUG = space(1)
  w_PMSPOSTI = space(1)
  w_PMSPO_GG = 0
  w_PMSPOCOP = 0
  w_PMSANTIC = space(1)
  w_PMSANCOP = 0
  w_PMSANNUL = space(1)
  w_PMODLPIA = space(1)
  w_PMPPOSTI = space(1)
  w_PMPPO_GG = 0
  w_PMPPOCOP = 0
  w_PMPANTIC = space(1)
  w_PMPANCOP = 0
  w_PMPANNUL = space(1)
  w_PMODLLAN = space(1)
  w_PMLPOSTI = space(1)
  w_PMLPO_GG = 0
  w_PMLPOCOP = 0
  w_PMLCHIUD = space(1)
  w_PMOCLSUG = space(1)
  w_PMCPOSTI = space(1)
  w_PMCPO_GG = 0
  w_PMCPOCOP = 0
  w_PMCANTIC = space(1)
  w_PMCANCOP = 0
  w_PMCANNUL = space(1)
  w_PMOCLPIA = space(1)
  w_PMIPOSTI = space(1)
  w_PMIPO_GG = 0
  w_PMIPOCOP = 0
  w_PMIANTIC = space(1)
  w_PMIANCOP = 0
  w_PMIANNUL = space(1)
  w_PMOCLORD = space(1)
  w_PMOPOSTI = space(1)
  w_PMOPO_GG = 0
  w_PMOPOCOP = 0
  w_PMOANTIC = space(1)
  w_PMOANCOP = 0
  w_PMOANNUL = space(1)
  w_PMODASUG = space(1)
  w_PMMPOSTI = space(1)
  w_PMMPO_GG = 0
  w_PMMPOCOP = 0
  w_PMMANTIC = space(1)
  w_PMMANCOP = 0
  w_PMMANNUL = space(1)
  w_PMODAPIA = space(1)
  w_PMDPOSTI = space(1)
  w_PMDPO_GG = 0
  w_PMDPOCOP = 0
  w_PMDANTIC = space(1)
  w_PMDANCOP = 0
  w_PMDANNUL = space(1)
  w_PMODAORD = space(1)
  w_PMRPOSTI = space(1)
  w_PMRPO_GG = 0
  w_PMRPOCOP = 0
  w_PMRANTIC = space(1)
  w_PMRANCOP = 0
  w_PMRANNUL = space(1)
  w_POSTI = .f.
  w_ANTIC = .f.
  w_ANNUL = .f.
  w_CANDEL = .f.
  w_ENDEL = .f.
  w_CHKFAB = .f.
  w_CHKDAY = .f.
  w_DAYSPO = 0
  w_DATDIFF = .f.
  w_DATCONG = ctod("  /  /  ")
  w_datcon = ctod("  /  /  ")
  w_ODA = .f.
  w_OCL = .f.
  w_oltemlav = 0
  w_RecSel = 0
  w_RecAtt = 0
  w_DGioCop = space(8)
  w_quantiTOT = 0
  w_qtalorTOT = 0
  w_qtanetTOT = 0
  w_quantvTOT = 0
  w_quanfvTOT = 0
  w_quantiORI = 0
  w_qtalorORI = 0
  w_qtanetORI = 0
  w_quantvORI = 0
  w_quanfvORI = 0
  w_RecOrdSOK = 0
  w_CodNCanc = space(15)
  w_CodCanc = space(15)
  w_ScoMin = .f.
  AggPad = .f.
  oldqtanet = 0
  w_qtaava = 0
  Scoproc = .f.
  RigadaCanc = space(15)
  w_appo1 = space(10)
  w_appo1 = space(10)
  w_LogFileName = space(10)
  w_CursErrorLog = space(10)
  w_cMsg = space(0)
  w_KEYRIF = space(10)
  w_KEYRIFDOC = space(10)
  w_SELEZIDO = space(0)
  w_VERIFICA = space(3)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  MA_COSTI_idx=0
  MPS_TFOR_idx=0
  ODL_CICL_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  ODL_RISO_idx=0
  PAR_PROD_idx=0
  PRD_ERRO_idx=0
  SALDIART_idx=0
  SCCI_ASF_idx=0
  PARA_MRP_idx=0
  MRP_MESS_idx=0
  ART_TEMP_idx=0
  SEL__MRP_idx=0
  SALDICOM_idx=0
  PEG_SELI_idx=0
  MAGAZZIN_idx=0
  ODL_SMPL_idx=0
  GRUDMAG_idx=0
  PAR_RIOR_idx=0
  TMPSALAGG_idx=0
  ART_ICOL_idx=0
  MAGA_TEMP_idx=0
  TMPDOCUM_idx=0
  ODLMRISO_idx=0
  ODL_MOUT_idx=0
  ODL_MAIN_idx=0
  TMPGSDB_MRL_idx=0
  ODL_RISF_idx=0
  TMPGSCO_MMO_idx=0
  odl_mast_idx=0
  TMPART_TEMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ELABORAZIONE MRP
    * --- Modalit� di elaborazione: Rigenerativa, Net-change
    * --- Genera proposte d'aquisto
    * --- Criterio di scelta del fornitore
    * --- Verifica correttezza dati da elaborare
    * --- Abilita i Messaggi di Ripianificazione
    * --- Variabile Utilizzata Per Aggiornamento Pegging
    * --- Abilita il ricalcolo del seriale del file Elabxxx.dbf
    this.Padre = this.oParentobject
    do case
      case type("this.Padre")="O"
        this.w_PARAM = alltrim(upper(this.Padre.Class))
      otherwise
        i_retcode = 'stop'
        return
    endcase
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CANCELROUTINE = .F.
    this.w_LenKeysal = IIF(this.w_CRIELA<>"A", 25, 20)
    * --- Se g_OLDCOM � .T. vecchia gestione commessa
    this.w_OLDCOM = VARTYPE(g_OLDCOM)="L" AND g_OLDCOM
    if this.w_OLDCOM and this.w_CRIELA<>"A"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARFLCOMM = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ARFLCOMM = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows>0
        ah_errormsg("Attenzione!%0Non � possibile eseguire l'elaborazione MRP con un criterio diverso da Aggregata in presenza di articoli personalizzati con la modalit� di pianificazione (Pegging di secondo livello): elaborazione abortita!",16)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Legge parametri di produzione
    this.w_PPCENCOS = SPACE(15)
    GSMR_BPP(this,"READPARPROD")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Empty(Nvl(this.w_COMMDEFA, SPACE(15)))
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = Space(15)
    endif
    if this.w_PPORDICM="S"
      * --- Limite fox totale elementi array=65.000, ho 2 dimensioni quindi il limite dimezza, ne consegue un limite di 32.500 ordini per singolo articolo+commessa
      dimension totord (32500,2)
    endif
    this.w_DATELA = i_DATSYS - this.w_PPGIOSCA
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLORDI,CMFLIMPE,CMFLCOMM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_PPCAUORD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLORDI,CMFLIMPE,CMFLCOMM;
        from (i_cTable) where;
            CMCODICE = this.w_PPCAUORD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ORDFLORD = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_ORDFLIMP = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_CMFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLORDI,CMFLIMPE,CMFLRISE"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_PPCAUIMP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLORDI,CMFLIMPE,CMFLRISE;
        from (i_cTable) where;
            CMCODICE = this.w_PPCAUIMP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IMPFLORD = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_IMPFLIMP = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_IMPFLRIS = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Valuto se devo generare ODA o PDA
    * --- w_GENPODA assume i valori
    *     'O' Genero le ODA
    *     'S' Genero le PDA
    *     'N' Non genero n� le ODA n� le PDA
    this.w_GENPODA = iif(this.oParentObject.w_GENPDA="S",iif(g_MODA="S","O","S"),"N")
    if this.w_OLDCOM
      * --- Verifica giacenza Pegging
      gsmr_brp(this,"MRP")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if empty(this.w_INIELA)
      this.w_INIELA = i_INIDAT 
    endif
    if empty(this.w_FINELA)
      this.w_FINELA = cp_CharToDate("31-12-2099")
    endif
    * --- Variabili usate per gestione path DBF
    * --- Test path MRP
    * --- --Variabili utilizzate per internazionalizzazione messaggi
    this.w_oMess=createobject("Ah_message")
    if this.w_MRPLOG="S" or this.w_PEGGING or this.oParentObject.w_INTERN
      * --- Nel caso di elaborazione Messaggi e Pegging non aggiorno la tabella SEL__MRP, ma in caso di elaborazione Pegging creo 
      *     comunque il DBF, mi serve per l'elaborazione (poi comunque lo elimino)
      * --- Azzero il Contenuto Dell'Array
      Declare ArrPath[3]
      ArrPath[1]="" 
 ArrPath[2]=0 
 ArrPath[3]=""
      if this.oParentObject.w_ELABDBF="N"
        * --- Non ricalcolo il seriale
        this.w_Seriale = this.oparentobject.w_SERDBF
      else
        * --- Ricalcolo il seriale, lo inizializzo a space
        this.w_Seriale = space(10)
      endif
      this.w_Res = GetDirElab("W", @ArrPath,this.w_Seriale)
      this.w_FileName = alltrim(ArrPath[1])
      this.f_Error = ArrPath[2]
      this.w_Seriale = ArrPath[3]
      * --- Mi dice se c'� un errore
      this.w_exit = .f.
      do case
        case this.f_Error = 1
          ah_ErrorMsg("Impossibile trovare il percorso:%0%1%0Elaborazione abortita",16,"",this.w_FileName)
          this.w_exit = .t.
        case this.f_Error = 2
          ah_ErrorMsg("Impossibile aprire (modalit� lettura/scrittura) oppure creare il file:%0%1%0Verificare i diritti di scrittura sulla cartella%0Elaborazione abortita",16,"",this.w_FileName)
          this.w_exit = .t.
        case this.f_Error = 3
          ah_ErrorMsg("Impossibile aggiornare il seriale di elaborazione%0Elaborazione abortita",16)
          this.w_exit = .t.
      endcase
      if this.w_exit
        * --- Nel caso in cui ci siano degli errori devo chiudere la progressbar se attiva 
        *     e alzare i flag di blocco MRP e conto lavoro nei parametri di produzione
        * --- Alzo la bandierina e notifico che c'� un errore (il messaggio per comodit� lo rilascio qui))
        this.w_CHECKINIT = 999
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_ELABDBF="S" and (this.w_PARAM="TGSCO_KGP" or this.w_PARAM="TGSCO_KPD") and this.oParentObject.w_INTERN
      this.w_FilePath = left(fullpath(this.w_FileName),rat("\",fullpath(this.w_FileName)))
      if directory(this.w_FilePath)
        * --- Aggiorno la tabella SEL__MRP
        * --- Write into SEL__MRP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SEL__MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEL__MRP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SEL__MRP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GMFLULEL ="+cp_NullLink(cp_ToStrODBC("N"),'SEL__MRP','GMFLULEL');
              +i_ccchkf ;
          +" where ";
              +"GMFLULEL <> "+cp_ToStrODBC("N");
                 )
        else
          update (i_cTable) set;
              GMFLULEL = "N";
              &i_ccchkf. ;
           where;
              GMFLULEL <> "N";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Try
        local bErr_055145B0
        bErr_055145B0=bTrsErr
        this.Try_055145B0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_055145B0
        * --- End
      else
        ah_ErrorMsg ("Impossibile trovare il percorso:%0%1%0Elaborazione abortita",16,"",this.w_FilePath)
        * --- Alzo la bandierina e notifico che c'� un errore (il messaggio per comodit� lo rilascio qui))
        this.w_CHECKINIT = 999
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Eventuale creazione di un file di log
    if this.w_MRPLOG="S"
      * --- INIZIALIZZA VARIABILI
      this.w_INIZ = seconds()
      this.w_INIZ_DATE = DATE()
      this.w_finedbiniz = 0
      this.w_finedbagg = 0
      this.w_QUANTI = 0
      * --- CURSORE PER I MESSAGGI
      this.w_oMRPLOG=createobject("AH_ERRORLOG")
      this.w_oMRPLOG.ADDMSGLOG(this.w_LogTitle, this.padre.caption, g_APPLICATION, g_VERSION)     
      this.w_oMRPLOG.ADDMSGLOG("================================================================================")     
      this.w_oMRPLOG.ADDMSGLOG("Inizio elaborazione M.R.P: [%1]", ALLTRIM(TTOC(DATETIME())))     
    endif
    if this.w_MRPLOG="S"
      this.w_FILINIZ = seconds()
    endif
    if (this.oParentObject.w_INTERN or this.w_PARAM="TGSMR_KMM") and this.oParentObject.w_MODELA="R"
      do case
        case this.w_SELEIMPE $ "A-E" and (this.w_SELEZDOC="S" or this.w_SELEZART="S" or this.w_SELEZODL="S")
          this.Page_18()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_SELEIMPE="I" and this.w_SELEZART="S"
          this.Page_18()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    endif
    if this.w_SELEZDOC="S"
      * --- Se w_SELEZDOC vale 'S' metto ii documenti nella tabella temporanea TMPDOCUM
      *     per rendere l'elaborazione pi� veloce interrogo i documenti una sola volta
      * --- Insert into TMPDOCUM
      i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR27BGP",this.TMPDOCUM_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_SELEZODL="S"
      * --- Insert into TMPDOCUM
      i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR29BGP",this.TMPDOCUM_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_MRPLOG="S"
      this.w_FILFIN = seconds()
      this.w_FILTOT = this.w_FILFIN-this.w_FILINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di preparazione filtri (esplosione codici): %1", this.TimeMRP(this.w_FILTOT))     
    endif
    if this.oParentObject.w_INTERN
      if this.w_MRPLOG="S"
        this.w_LLCINIZ = seconds()
      endif
      * --- Elabora LLC degli articoli
      do GSMR_BLC with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_UseProgBar
        GesProgBar("M", this)
      endif
      if this.w_ErrorLLC
        i_retcode = 'stop'
        return
      endif
      if this.w_MRPLOG="S"
        this.w_LLCFINE = seconds()
        this.w_LLCTOT = this.w_LLCFINE-this.w_LLCINIZ
        this.w_oMRPLOG.ADDMSGLOG("Tempo di aggiornamento Low Level Code Articoli: %1", this.TimeMRP(this.w_LLCTOT))     
      endif
    endif
    if this.oParentObject.w_INTERN and this.oParentObject.w_MODELA="R"
      if this.w_MRPLOG="S"
        this.w_IDBINIZ = seconds()
      endif
      * --- Inizializza DB
      ah_Msg("Inizializza database...",.T.)
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ErrorBID
        ah_ErrorMsg("Impossibile creare time-buckets del piano ODL, elaborazione annullata","STOP","")
        this.Page_17()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      if this.w_MRPLOG="S"
        this.w_IDBFIN = seconds()
        this.w_IDBTOT = this.w_IDBFIN-this.w_IDBINIZ
        this.w_oMRPLOG.ADDMSGLOG("Tempo di inizializzazione database: %1", this.TimeMRP(this.w_IDBTOT))     
      endif
    endif
    * --- Pulizia Log-Errori
    * --- Delete from PRD_ERRO
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LEOPERAZ = "+cp_ToStrODBC(this.w_LOperaz);
            +" and LECODUTE = "+cp_ToStrODBC(i_CODUTE);
             )
    else
      delete from (i_cTable) where;
            LEOPERAZ = this.w_LOperaz;
            and LECODUTE = i_CODUTE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_PPMAGPRO = iif(empty(nvl(this.w_PPMAGPRO,"")), g_MAGAZI, this.w_PPMAGPRO)
    this.w_FLORCO = iif(this.w_CMFLCOMM="I","+",IIF(this.w_CMFLCOMM="D","-"," "))
    this.w_FLCOCO = iif(this.w_CMFLCOMM="C","+",IIF(this.w_CMFLCOMM="S","-"," "))
    * --- Leggo il flag nettificabile del magazzino produzione
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGDISMAG"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_PPMAGPRO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGDISMAG;
        from (i_cTable) where;
            MGCODMAG = this.w_PPMAGPRO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DISMGPRO = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_UseProgBar
      GesProgBar("M", this, 0,100, ah_msgFormat("Lettura dati in Corso...."))
    else
      ah_msg("Lettura dati in corso...")
    endif
    if this.w_MRPLOG="S"
      this.w_DARINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR1BGP", this, "Articoli")
    Index on Codart1+str(prlowlev) tag codart1
    set order to codart1
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR7BGP", this, "Art_prod")
    Index on codsal1 tag codsal1
    set order to codsal1
    do case
      case this.w_CRIELA="M"
        * --- Elaborazione per magazzino oppure per gruppi di magazzino estraggo anche i dati per magazzino
        VQ_EXEC("..\COLA\EXE\QUERY\GSMR7BGP1", this, "Par_rima")
      case this.w_CRIELA="G"
        VQ_EXEC("..\COLA\EXE\QUERY\GSMR7BGP2", this, "Par_rima")
    endcase
    if this.w_CRIELA<>"A" and Used("Par_rima")
      SELECT("Par_rima")
      Index on codsal2 tag codsal2
      set order to codsal2
      this.w_NRECPARRIMA = RECCOUNT("Par_rima")
    endif
    if this.w_MRPLOG="S"
      this.w_DARFIN = seconds()
      this.w_DARTOT = this.w_DARFIN-this.w_DARINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura dati Articoli: %1", this.TimeMRP(this.w_DARTOT))     
      this.w_DOCINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR4BGP", this, "Documen")
    if this.w_MRPLOG="S"
      this.w_DOCUFIN = seconds()
      this.w_DOCTOT = this.w_DOCUFIN-this.w_DOCINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura dati ordini clienti: %1", this.TimeMRP(this.w_DOCTOT))     
      this.w_SETFILINIZ = seconds()
    endif
    if this.w_MRPLOG="S"
      this.w_CALINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR6BGP", this, "Calendario")
    if this.w_MRPLOG="S"
      this.w_CALFIN = seconds()
      this.w_CALTOT = this.w_CALFIN - this.w_CALINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura calendario: %1", this.TimeMRP(this.w_CALTOT))     
      this.w_IMPINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR3BGP", this, "Impegni")
    if this.w_MRPLOG="S"
      this.w_IMPFIN = seconds()
      this.w_IMPTOT = this.w_IMPFIN-this.w_IMPINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura impegni: %1", this.TimeMRP(this.w_IMPTOT))     
      this.w_ODPINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR8BGP", this, "ODP")
    if this.w_MRPLOG="S"
      this.w_ODPFIN = seconds()
      this.w_ODPTOT = this.w_ODPFIN-this.w_ODPINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura dati ODP/ODL: %1", this.TimeMRP(this.w_ODPTOT))     
      this.w_STOINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMRABGP", this, "OrdiODL") 
 VQ_EXEC("..\COLA\EXE\QUERY\GSMRBBGP", this, "ImpeODL") 
 VQ_EXEC("..\COLA\EXE\QUERY\GSMR_SCO", this, "COrdiODL") 
 VQ_EXEC("..\COLA\EXE\QUERY\GSMR1SCO", this, "CImpeODL")
    if this.w_MRPLOG="S"
      this.w_STOFIN = seconds()
      this.w_STOTOT = this.w_STOFIN-this.w_STOINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura ordinato/impegnato ODL suggeriti (che devono essere cancellati): %1", this.TimeMRP(this.w_STOTOT))     
      this.w_COMINIZ = seconds()
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMRLBGP", this, "Commesse")
    if this.w_OLDCOM
      VQ_EXEC("..\COLA\EXE\QUERY\GSMR12BGP", this, "SalComm")
      * --- Cursori di appoggio per il Pegging di Commessa
      if this.w_PARAM="TGSCO_KGP" and this.oParentObject.w_INTERN and this.oParentObject.w_MODELA="R"
        Select *,iif(empty(nvl(peserord," ")),"B","A") as cordina from SalComm into cursor SalComm READWRITE 
 index on cordina tag cordina
      else
        * --- Aggiornamento pegging e Messaggi di ripianificazione
        Select *,iif(empty(nvl(peserord," ")),"B","A") as cordina from SalComm into cursor SalComm READWRITE 
 index on cordina tag cordina
      endif
      VQ_EXEC("..\COLA\EXE\QUERY\GSMR33BGP", this, "Abbina") 
 index on slcodice tag slcodice
      select Abbina 
 go top 
 scan for tiprig="A" and qtaper>0
      this.w_SALCOD = slcodice
      this.w_QTAMAG = qtaper
      this.w_RIGAORD = recno()
      scan for slcodice=this.w_SALCOD and tiprig="A" and qtaper<0
      if -qtaper=this.w_QTAMAG
        * --- Le due righe si elidono l'una con l'altra
        replace qtaper with 0
        this.w_QTAMAG = 0
      else
        if -qtaper>this.w_QTAMAG
          * --- La giacenza negativa � maggiore della giacenza
          replace qtaper with qtaper+this.w_QTAMAG
          this.w_QTAMAG = 0
        else
          * --- La giacenza � maggiore della giacenza negativa
          replace qtaper with 0
          this.w_QTAMAG = this.w_QTAMAG+qtaper
        endif
      endif
      if this.w_QTAMAG=0
        exit
      endif
      endscan
      go this.w_RIGAORD 
 replace qtaper with this.w_QTAMAG
      endscan
      Select slcodice,slcodmag,sum(qtaper) as perqta from Abbina Group by slcodice,slcodmag having perqta>0 into cursor AbbCom 
 Use in Abbina
      Select * from AbbCom into cursor AbbCom READWRITE 
 index on slcodice tag slcodice
    endif
    if this.w_MRPLOG="S"
      this.w_COMFINE = seconds()
      this.w_COMTOT = this.w_COMFINE-this.w_COMINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura dati commesse: %1", this.TimeMRP(this.w_COMTOT))     
      this.w_SLINIZ = seconds()
    endif
    if this.w_SELEZDOC="S"
      VQ_EXEC("..\COLA\EXE\QUERY\GSMR22BGP", this, "Saldi")
    else
      VQ_EXEC("..\COLA\EXE\QUERY\GSMR2BGP", this, "Saldi")
    endif
    if ! this.w_OLDCOM
      if this.w_SELEZDOC="S"
        VQ_EXEC("..\COLA\EXE\QUERY\GSMR22BGPC", this, "SaldiCom")
      else
        VQ_EXEC("..\COLA\EXE\QUERY\GSMR2BGPC", this, "SaldiCom")
      endif
    endif
    if this.w_MRPLOG="S"
      this.w_SLFIN = seconds()
      this.w_SLTOT = this.w_SLFIN-this.w_SLINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura saldi articolo: %1", this.TimeMRP(this.w_SLTOT))     
    endif
    if this.oParentObject.w_INTERN
      if this.w_MRPLOG="S"
        this.w_LEGINIZ = seconds()
      endif
      VQ_EXEC("..\COLA\EXE\QUERY\GSMR_BGP", this, "Legami")
      if this.w_MRPLOG="S"
        this.w_LEGFIN = seconds()
        this.w_LEGTOT = this.w_LEGFIN-this.w_LEGINIZ
        this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura legami distinte: %1", this.TimeMRP(this.w_LEGTOT))     
      endif
      if g_PRFA="S" and g_CICLILAV="S"
        if this.w_MRPLOG="S"
          this.w_CLINIZ = seconds()
        endif
        VQ_EXEC("..\PRFA\EXE\QUERY\GSCI_BGP", this, "Cicli")
        if this.w_MRPLOG="S"
          this.w_CLFIN = seconds()
          this.w_CLTOT = this.w_CLFIN-this.w_CLINIZ
          this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura dati cicli: %1", this.TimeMRP(this.w_CLTOT))     
        endif
      endif
    endif
    if g_GESCON="S"
      * --- Carica in memoria i Contratti
      if this.w_MRPLOG="S"
        this.w_COINIZ = seconds()
      endif
      if g_COLA="S" and g_GESCON="S"
        VQ_EXEC("..\COLA\EXE\QUERY\GSMREBGP", this, "Contratti")
      endif
      if g_GESCON="S" and this.w_GENPODA="O" and not empty(this.w_CRIFOR)
        * --- Carica in memoria i contratti per gli ODA
        VQ_EXEC("..\COLA\EXE\QUERY\GSMR38BGP", this, "ContrattiA")
        =WrCursor("ContrattiA")
      endif
      if this.w_MRPLOG="S"
        this.w_COFIN = seconds()
        this.w_COTOT = this.w_COFIN-this.w_COINIZ
        this.w_oMRPLOG.ADDMSGLOG("Tempo di lettura dati Contratti: %1", this.TimeMRP(this.w_COTOT))     
      endif
    endif
    if this.w_UseProgBar
      GesProgBar("S", this, 100, 100)
    endif
    if g_COLA="S" and g_PRFA="S" and g_CICLILAV="S"
      if this.w_MRPLOG="S"
        this.w_COINIZ = seconds()
      endif
      * --- Se sono installati i cicli di lavoro e il conto lavoro estraggo tutte le fasi esterne
      *     legate al ciclo preferenziale delle distinte
      *     Mi serve per sapere se devo emettere un ordine oppure no per i materiali di input su fase esterna
      *     Utilizzato a Pag3
      VQ_EXEC("..\PRFA\EXE\QUERY\GSCI3BGP", THIS, "FASEXTODL") 
 Select * from FASEXTODL into cursor FASEXTODL readwrite 
 index on dbkeysal+dtos(clinival)+dtos(clfinval) tag daticic
      if this.w_MRPLOG="S"
        this.w_COFIN = seconds()
        this.w_COTOT = this.w_COFIN-this.w_COINIZ
        this.w_oMRPLOG.ADDMSGLOG("Tempo estrazione fasi esterne: %1", this.TimeMRP(this.w_COTOT))     
      endif
    endif
    * --- Legge Parametri Messaggi Ripianificazione
    * --- Read from PARA_MRP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PARA_MRP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARA_MRP_idx,2],.t.,this.PARA_MRP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PARA_MRP where ";
            +"PMCODICE = "+cp_ToStrODBC(iif(this.oParentObject.w_INTERN, "MR", "EX"));
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            PMCODICE = iif(this.oParentObject.w_INTERN, "MR", "EX");
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PMPERCON = NVL(cp_ToDate(_read_.PMPERCON),cp_NullValue(_read_.PMPERCON))
      this.w_PMODLSUG = NVL(cp_ToDate(_read_.PMODLSUG),cp_NullValue(_read_.PMODLSUG))
      this.w_PMSPOSTI = NVL(cp_ToDate(_read_.PMSPOSTI),cp_NullValue(_read_.PMSPOSTI))
      this.w_PMSPO_GG = NVL(cp_ToDate(_read_.PMSPO_GG),cp_NullValue(_read_.PMSPO_GG))
      this.w_PMSPOCOP = NVL(cp_ToDate(_read_.PMSPOCOP),cp_NullValue(_read_.PMSPOCOP))
      this.w_PMSANTIC = NVL(cp_ToDate(_read_.PMSANTIC),cp_NullValue(_read_.PMSANTIC))
      this.w_PMSANCOP = NVL(cp_ToDate(_read_.PMSANCOP),cp_NullValue(_read_.PMSANCOP))
      this.w_PMSANNUL = NVL(cp_ToDate(_read_.PMSANNUL),cp_NullValue(_read_.PMSANNUL))
      this.w_PMODLPIA = NVL(cp_ToDate(_read_.PMODLPIA),cp_NullValue(_read_.PMODLPIA))
      this.w_PMPPOSTI = NVL(cp_ToDate(_read_.PMPPOSTI),cp_NullValue(_read_.PMPPOSTI))
      this.w_PMPPO_GG = NVL(cp_ToDate(_read_.PMPPO_GG),cp_NullValue(_read_.PMPPO_GG))
      this.w_PMPPOCOP = NVL(cp_ToDate(_read_.PMPPOCOP),cp_NullValue(_read_.PMPPOCOP))
      this.w_PMPANTIC = NVL(cp_ToDate(_read_.PMPANTIC),cp_NullValue(_read_.PMPANTIC))
      this.w_PMPANCOP = NVL(cp_ToDate(_read_.PMPANCOP),cp_NullValue(_read_.PMPANCOP))
      this.w_PMPANNUL = NVL(cp_ToDate(_read_.PMPANNUL),cp_NullValue(_read_.PMPANNUL))
      this.w_PMODLLAN = NVL(cp_ToDate(_read_.PMODLLAN),cp_NullValue(_read_.PMODLLAN))
      this.w_PMLPOSTI = NVL(cp_ToDate(_read_.PMLPOSTI),cp_NullValue(_read_.PMLPOSTI))
      this.w_PMLPO_GG = NVL(cp_ToDate(_read_.PMLPO_GG),cp_NullValue(_read_.PMLPO_GG))
      this.w_PMLCHIUD = NVL(cp_ToDate(_read_.PMLCHIUD),cp_NullValue(_read_.PMLCHIUD))
      this.w_PMOCLORD = NVL(cp_ToDate(_read_.PMOCLORD),cp_NullValue(_read_.PMOCLORD))
      this.w_PMOPOSTI = NVL(cp_ToDate(_read_.PMOPOSTI),cp_NullValue(_read_.PMOPOSTI))
      this.w_PMOPO_GG = NVL(cp_ToDate(_read_.PMOPO_GG),cp_NullValue(_read_.PMOPO_GG))
      this.w_PMOPOCOP = NVL(cp_ToDate(_read_.PMOPOCOP),cp_NullValue(_read_.PMOPOCOP))
      this.w_PMOANTIC = NVL(cp_ToDate(_read_.PMOANTIC),cp_NullValue(_read_.PMOANTIC))
      this.w_PMOANCOP = NVL(cp_ToDate(_read_.PMOANCOP),cp_NullValue(_read_.PMOANCOP))
      this.w_PMOANNUL = NVL(cp_ToDate(_read_.PMOANNUL),cp_NullValue(_read_.PMOANNUL))
      this.w_PMODASUG = NVL(cp_ToDate(_read_.PMODASUG),cp_NullValue(_read_.PMODASUG))
      this.w_PMMPOSTI = NVL(cp_ToDate(_read_.PMMPOSTI),cp_NullValue(_read_.PMMPOSTI))
      this.w_PMMPO_GG = NVL(cp_ToDate(_read_.PMMPO_GG),cp_NullValue(_read_.PMMPO_GG))
      this.w_PMMPOCOP = NVL(cp_ToDate(_read_.PMMPOCOP),cp_NullValue(_read_.PMMPOCOP))
      this.w_PMMANTIC = NVL(cp_ToDate(_read_.PMMANTIC),cp_NullValue(_read_.PMMANTIC))
      this.w_PMMANCOP = NVL(cp_ToDate(_read_.PMMANCOP),cp_NullValue(_read_.PMMANCOP))
      this.w_PMMANNUL = NVL(cp_ToDate(_read_.PMMANNUL),cp_NullValue(_read_.PMMANNUL))
      this.w_PMODAPIA = NVL(cp_ToDate(_read_.PMODAPIA),cp_NullValue(_read_.PMODAPIA))
      this.w_PMDPOSTI = NVL(cp_ToDate(_read_.PMDPOSTI),cp_NullValue(_read_.PMDPOSTI))
      this.w_PMDPO_GG = NVL(cp_ToDate(_read_.PMDPO_GG),cp_NullValue(_read_.PMDPO_GG))
      this.w_PMDPOCOP = NVL(cp_ToDate(_read_.PMDPOCOP),cp_NullValue(_read_.PMDPOCOP))
      this.w_PMDANTIC = NVL(cp_ToDate(_read_.PMDANTIC),cp_NullValue(_read_.PMDANTIC))
      this.w_PMDANCOP = NVL(cp_ToDate(_read_.PMDANCOP),cp_NullValue(_read_.PMDANCOP))
      this.w_PMDANNUL = NVL(cp_ToDate(_read_.PMDANNUL),cp_NullValue(_read_.PMDANNUL))
      this.w_PMODAORD = NVL(cp_ToDate(_read_.PMODALAN),cp_NullValue(_read_.PMODALAN))
      this.w_PMRPOSTI = NVL(cp_ToDate(_read_.PMRPOSTI),cp_NullValue(_read_.PMRPOSTI))
      this.w_PMRPO_GG = NVL(cp_ToDate(_read_.PMRPO_GG),cp_NullValue(_read_.PMRPO_GG))
      this.w_PMRPOCOP = NVL(cp_ToDate(_read_.PMRPOCOP),cp_NullValue(_read_.PMRPOCOP))
      this.w_PMRANTIC = NVL(cp_ToDate(_read_.PMRANTIC),cp_NullValue(_read_.PMRANTIC))
      this.w_PMRANCOP = NVL(cp_ToDate(_read_.PMRANCOP),cp_NullValue(_read_.PMRANCOP))
      this.w_PMRANNUL = NVL(cp_ToDate(_read_.PMRANNUL),cp_NullValue(_read_.PMRANNUL))
      this.w_PMOCLSUG = NVL(cp_ToDate(_read_.PMOCLSUG),cp_NullValue(_read_.PMOCLSUG))
      this.w_PMCPOSTI = NVL(cp_ToDate(_read_.PMCPOSTI),cp_NullValue(_read_.PMCPOSTI))
      this.w_PMCPO_GG = NVL(cp_ToDate(_read_.PMCPO_GG),cp_NullValue(_read_.PMCPO_GG))
      this.w_PMCPOCOP = NVL(cp_ToDate(_read_.PMCPOCOP),cp_NullValue(_read_.PMCPOCOP))
      this.w_PMCANTIC = NVL(cp_ToDate(_read_.PMCANTIC),cp_NullValue(_read_.PMCANTIC))
      this.w_PMCANCOP = NVL(cp_ToDate(_read_.PMCANCOP),cp_NullValue(_read_.PMCANCOP))
      this.w_PMCANNUL = NVL(cp_ToDate(_read_.PMCANNUL),cp_NullValue(_read_.PMCANNUL))
      this.w_PMOCLPIA = NVL(cp_ToDate(_read_.PMOCLPIA),cp_NullValue(_read_.PMOCLPIA))
      this.w_PMIPOSTI = NVL(cp_ToDate(_read_.PMIPOSTI),cp_NullValue(_read_.PMIPOSTI))
      this.w_PMIPO_GG = NVL(cp_ToDate(_read_.PMIPO_GG),cp_NullValue(_read_.PMIPO_GG))
      this.w_PMIPOCOP = NVL(cp_ToDate(_read_.PMIPOCOP),cp_NullValue(_read_.PMIPOCOP))
      this.w_PMIANTIC = NVL(cp_ToDate(_read_.PMIANTIC),cp_NullValue(_read_.PMIANTIC))
      this.w_PMIANCOP = NVL(cp_ToDate(_read_.PMIANCOP),cp_NullValue(_read_.PMIANCOP))
      this.w_PMIANNUL = NVL(cp_ToDate(_read_.PMIANNUL),cp_NullValue(_read_.PMIANNUL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Delete from MRP_MESS
    i_nConn=i_TableProp[this.MRP_MESS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_MRPLOG="S"
      this.w_INIZINIZ = seconds()
    endif
    * --- Inizializza dati
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_MRPLOG="S"
      this.w_INIZFIN = seconds()
      this.w_INIZTOT = this.w_INIZFIN-this.w_INIZINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di inizializzazione dati: %1", this.TimeMRP(this.w_INIZTOT))     
    endif
    if this.w_PMPERCON>0
      * --- Periodo Congelato
      this.w_Leadt = -this.w_PMPERCON
      this.DatInput = dtos(i_DATSYS)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_DATCONG = this.DatRisul
    endif
    * --- Inizio elaborazione MRP
    if this.w_MRPLOG="S"
      this.w_MRPINIZ = seconds()
      this.w_MRPFIN = this.w_MRPINIZ
    endif
    if (this.oParentObject.w_INTERN or this.w_PARAM="TGSMR_KMM") and this.oParentObject.w_MODELA="R"
      * --- Nel caso si stiano effettuando delle selezioni ci si potrebbe trovare nel caso
      *     in cui l'ultimo record non sia una materia prima questo comporta 
      *     la mancata generazione degli ordini.
      *     Per questo motivo viene inserito un record vuoto per ingannare l'algoritmo
      *     e fare cos� generare gli ordini anche dell'ultimo record.
      Select Elabor
      go top
      Scatter Memvar blank
      m.codsal=space(Len(m.CodSal)) 
 m.dateva=ctod("31-12-2099") 
 m.datcon=ctod("31-12-2099") 
 m.datidx=dtos(m.dateva) 
 m.prlowlev=999 
 m.codmag=space(5) 
 m.codcom=space(15) 
 m.codatt=space(15) 
 m.keyidx=right("00"+alltrim(str(m.prlowlev,3,0)),3) + m.codsal + m.codcom + m.codatt + m.datidx + "0" 
 m.tiprec="0SALDI" 
 m.quanti=1 
 m.elabora=.T. 
 append blank 
 gather memvar
      if this.w_PPMRPDIV="S"
        Select Elabor_bis
        go top
        Scatter Memvar blank
        m.codsal=space(Len(m.CodSal)) 
 m.keyidx=right("00"+alltrim(str(m.prlowlev,3,0)),3) + m.codsal + m.codcom + m.codatt + m.datidx + "0" 
 m.tiprec="0SALDI" 
 append blank 
 gather memvar
      endif
    endif
    if this.w_UseProgBar
      Select Elabor
      select distinct codsal from Elabor into cursor nRecProgBar where not proggmps $ "SR" and prlowlev>=0 and tiprec="0"
      this.w_NTMPREC = Reccount("nRecProgBar")
      use in select("nRecProgBar")
      this.w_NRECELAB = this.w_NTMPREC
      this.w_NRECCUR = 0
      GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
      Select Elabor
    endif
    L_GridTime2 = 0
    L_GridTime1 = 0
    L_GridTimeInterval = .3
    if vartype(this.Padre.w_EDITBTN)="C"
      * --- Variabile che serve per abilitare/disabilitare il bottone interrompi
      this.Padre.w_EDITBTN = "N"
      this.Padre.mEnableControls()     
    endif
    this.w_ElabFabb = TRUE
    this.w_FIRSTLAP = .t.
    this.w_RESCOM = 0
    this.w_Comm = "###############"
    this.w_INTWAIT1 = 0
    Select Elabor
    go top
    * --- Si posiziona sul primo record da elaborare
    locate for elabora and not proggmps $ "SR" and prlowlev>=0
    do while not eof("Elabor")
      this.w_recelabor = recno()
      scatter memvar
      if this.w_PPMRPDIV="S"
        Select Elabor_bis
        * --- Mi posiziono sul medesimo record di Elabor, i 2 cursori infatti sono l'uno il proseguimento dell'altro, facendo la scatter valorizzo
        *     i campi non presenti in Elabor, quelli comuni sono uguali.
        go this.w_recelabor
        scatter memvar
        Select Elabor
      endif
      if !this.w_UseProgBar
        * --- Nel caso non utilizzo la ProgressBar utilizzo questa funzion di windows
        *     che restituisce se l'appliocazione � freez
        if MOD(this.w_INTWAIT1, 1000)=0
          lnResult = 0
          lnResult = IsHungAppWindow(_VFP.hWnd)
          if lnResult > 0
            wait wind "" timeout .001
          endif
        endif
      endif
      this.w_CambioCodice = .F.
      if ! this.w_OLDCOM and ARFLCOMM="S"
        if this.w_CodSaldo<>Codsal or (this.w_Comm<>nvl(codcom,space(15)) and ARFLCOMM="S" and (ARFLCOM1<>"C" or tiprec="0"))
          if this.w_ElabFabb
            if this.w_CodSaldo<>Codsal
              this.w_CambioCodice = .T.
              if not empty(this.w_CodSaldo)
                * --- Applico i giorni di copertura
                this.Page_14()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Nuovo articolo da esaminare
              this.w_CodSaldo = codsal
              this.w_SaldProg = quanti
              this.w_ChiaveInd = keyidx
              this.w_Comm = "###############"
              this.w_RESCOM = 0
              this.w_FIRSTLAP = .t.
              this.w_ARFLCOM1 = ARFLCOM1
              if this.w_PPORDICM="S" and ARFLCOM1="C"
                * --- Svuoto l'array dell'ordinato libero per la commessa
                store "" to totord
              endif
              if !this.w_UseProgBar
                * --- --NON OCCORRE TRADUZIONE: uso wait window per migliorare performance
                if INT(recno()/1000)<>this.w_INTWAIT
                  this.w_INTWAIT = INT(recno()/1000)
                  wait window("Record: "+str(recno())+" / LLC: "+ALLTRIM(STR(nvl(prlowlev,0)))+" / codice saldo: "+this.w_CodSaldo) nowait
                endif
              endif
              if this.w_PPMRPDIV="S"
                Select Elabor_bis
              endif
              do case
                case empty(nvl(this.w_PPCARVAL,"")) or this.w_PPCARVAL="CS"
                  this.w_COSTO = nvl(COSSTA,0)
                case this.w_PPCARVAL="CM"
                  this.w_COSTO = nvl(COSMED,0)
                case this.w_PPCARVAL="UC"
                  this.w_COSTO = nvl(COSULT,0)
                case this.w_PPCARVAL="CL"
                  this.w_COSTO = nvl(COSLIS,0)
              endcase
              if this.w_PPMRPDIV="S"
                Select Elabor
              endif
              if artipges="F"
                if tiprec<>"0SALDI"
                  * --- Il saldo non � presente solo per quegli articoli che derivano dall'esplosione di una distinta configurata
                  this.w_ChiaveInd2 = left(this.w_ChiaveInd,this.w_RootKeyIdx)+IIF(this.w_ARFLCOM1="C","19000101"+space(30),space(30)+"19000101")+"0"
                  Select Elabor
                  if Not(indexseek(this.w_ChiaveInd2,.t.))
                     
 datidx="19000101" 
 keyidx=left(m.keyidx,this.w_RootKeyIdx)+ iif(nvl(arflcom1,"N")="C",m.datidx +space(30), ; 
 space(30)+m.datidx) + "0" 
 quanti=0 
 quanfi=0 
 quantv=0 
 quanfv=0 
 qtanet=0 
 qtalor=0 
 cododl=space(15) 
 tiprec="0SALDI" 
 append blank 
 gather memvar
                    if this.w_PPMRPDIV="S"
                      select elabor_bis 
 append blank 
 gather memvar
                      Select Elabor
                    endif
                  endif
                  this.w_SaldProg = quanti
                  this.w_ChiaveInd = keyidx
                endif
                if this.w_SaldProg<0 and artipart $ "PH-DC-PS"
                  * --- ANOMALIA: la diponibilit� di un PH non pu� essere negativa!
                  this.w_ErrMsg = "PH"
                  this.Page_8()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  Select Elabor
                  go bottom
                else
                  * --- Calcola la Disponibilit� (x Gest. a Fabbisogno)
                  this.w_Dispon = 0
                  if not eof("Elabor")
                    skip
                  endif
                  Select Elabor 
 set order to codsal
                  if this.w_PPMRPDIV="S"
                    this.w_recelabor = recno()
                    Select Elabor_bis 
 set order to codsal 
 go this.w_recelabor 
 Select Elabor
                  endif
                  do while not eof("Elabor") and this.w_CodSaldo=codsal
                    if artipart $ "PH-DC-PS"
                      if quanti>0
                        this.w_Dispon = this.w_Dispon+quanti
                      endif
                    else
                      if tiprec<>"7MRPIM" and tiprec<>"7SCO-S" and nvl(mgdismag,"S")<>"N" and tiprec<>"0SCOMM"
                        this.w_Dispon = this.w_Dispon+quanti
                      endif
                    endif
                    if tiprec = "7OL---"
                      if this.w_PPMRPDIV="S"
                        this.w_recelabor = recno()
                        Select Elabor_bis 
 go this.w_recelabor 
 delete 
 Select Elabor
                      endif
                      delete
                    endif
                    skip
                  enddo
                  if this.w_PPMRPDIV="S"
                    * --- Ripristino l'ordinamento ma non mi riposiziono, lo far� al momento in cui sar� necessario
                    Select Elabor_bis 
 set order to keyidx
                  endif
                  Select Elabor 
 set order to keyidx
                  indexseek(this.w_ChiaveInd,.t.)
                  this.w_SaldProg = this.w_SaldProg-this.w_Dispon
                  replace qtalor with this.w_SaldProg
                endif
                if this.w_SaldProg < -0.001
                  if this.w_GIANEG="S"
                    * --- Considero anche le giacenze negative
                    if !(tiprec="0SCOMM" and empty(nvl(codcom," ")))
                      this.w_NEGAPP = ah_msgformat("Giacenza negativa%1", iif(empty(nvl(codcom,"")),""," Commessa: "+alltrim(codcom)))
                      Insert into LogErrori (Codric, Quan, Messagg) values (m.codric, this.w_SaldProg, this.w_NEGAPP)
                    endif
                    Select Elabor 
 indexseek(this.w_ChiaveInd,.t.)
                    if .f.
                      Select Elabor 
 set order to codsal
                      locate for tiprec="7SCO-S" and codsal=this.w_CodSaldo rest
                      Select Elabor 
 set order to keyidx
                      if found()
                        Replace quanti with quanti+this.w_SaldProg, quanfi with quanfi+this.w_SaldProg, ; 
 quantv with quantv+this.w_SaldProg, quanfv with quanfv+this.w_SaldProg
                        this.w_SaldProg = 0
                      endif
                      indexseek(this.w_ChiaveInd,.t.)
                    endif
                  else
                    * --- Ignoro le giacenze negative
                    this.w_SaldProg = 0
                  endif
                endif
              endif
              * --- Se articolo Personalizzato+Nettificazione ho bisogno di un cursore di appoggio per i saldi
              if arflcom1="C" and tiprec="0SALDI"
                * --- Se il cursore di appoggio esiste (per un altro articolo) lo svuoto, se non esiste lo creo
                if used("SalPCom")
                  Select SalPCom 
 zap
                else
                  Create cursor SalPCom (PCODCOM C(15), PCODATT C(15), PSALCOM N(18,6),PSALOLD N(18,6))
                  index on PCODCOM tag PCODCOM
                endif
                Select Elabor
              endif
            else
              * --- Se arflcom1 � 'C' qui entra immediatamente dopo aver elaborato 0SALDI
              * --- Gestione disponibilit� per singola commessa
              this.w_Comm = nvl(codcom,space(15))
              if artipges="F"
                if tiprec<>"0SCOMM"
                  * --- Se il record manca lo inserisco a 0
                   
 datidx="19000102" 
 keyidx=left(m.keyidx,this.w_RootKeyIdx)+ iif(nvl(arflcom1,"N")="C",m.datidx + this.w_Comm + ; 
 space(15), this.w_Comm+space(15)+m.datidx) + "0" 
 quanti=0 
 quanfi=0 
 quantv=0 
 quanfv=0 
 qtanet=0 
 qtalor=0 
 cododl=space(15) 
 tiprec="0SCOMM" 
 codcom=this.w_Comm 
 append blank 
 gather memvar
                  if this.w_PPMRPDIV="S"
                    select elabor_bis 
 append blank 
 gather memvar
                    Select Elabor
                  endif
                endif
                this.w_FIRSTLAP = .f.
                this.w_SaldProg = quanti
                this.w_ChiaveInd = keyidx
                this.rigar = 0
                * --- Calcola la Disponibilit� (x Gest. a Fabbisogno)
                this.w_Dispon = 0
                if not eof("Elabor")
                  skip
                endif
                Select Elabor 
 set order to codsal
                if this.w_PPMRPDIV="S"
                  this.w_recelabor = recno()
                  Select Elabor_bis 
 set order to codsal 
 go this.w_recelabor 
 Select Elabor
                endif
                do while not eof("Elabor") and this.w_CodSaldo=codsal
                  * --- Calcolo disponibilit� per commessa.
                  if this.w_Comm=nvl(codcom,space(15))
                    if artipart $ "PH-DC-PS"
                      if quanti>0
                        this.w_Dispon = this.w_Dispon+quanti
                      endif
                    else
                      if tiprec<>"7MRPIM" and tiprec<>"7SCO-S" and nvl(mgdismag,"S")<>"N" and tiprec<>"0"
                        if arflcom1="C"
                          this.w_Dispon = this.w_Dispon+iif(tiprec="4" and empty(this.w_Comm),0,quanti)
                        else
                          this.w_Dispon = this.w_Dispon+quanti
                        endif
                        if empty(nvl(this.w_COMM,"")) and this.w_PPORDICM="S" and arflcom1="C" and tiprec<>"7ORDCL"
                          this.rigar = this.rigar+1
                          totord (this.rigar,1)=quanti 
 totord (this.rigar,2)=datidx
                        endif
                      endif
                    endif
                  endif
                  skip
                enddo
                if this.w_PPMRPDIV="S"
                  * --- Ripristino l'ordinamento ma non mi riposiziono, lo far� al momento in cui sar� necessario
                  Select Elabor_bis 
 set order to keyidx
                endif
                Select Elabor 
 set order to keyidx
                indexseek(this.w_ChiaveInd,.t.)
                if tiprec="0SALDI"
                  * --- Se sono sul record con commessa vuota la indexseek mi porta su 0SALDI invece che su 0SCOMM
                  skip
                endif
                * --- Attenzione per il record con commessa vuota, se ARFLCOM1='C', w_SaldProg considera anche la quantit� disponibile a magazzino anche se essa 
                *     potrebbe essere usata da impegni con commessa (la quantit� disponibile viene consumata per data quindi il codice commessa non � prioritario).
                this.w_SaldProg = this.w_SaldProg-this.w_Dispon
                replace qtalor with this.w_SaldProg
                if this.w_SaldProg < -0.001
                  if this.w_GIANEG="S"
                    * --- Considero anche le giacenze negative
                    if !(tiprec="0SCOMM" and empty(nvl(codcom," ")))
                      this.w_NEGAPP = ah_msgformat("Giacenza negativa%1", iif(empty(nvl(codcom,"")),""," Commessa: "+alltrim(codcom)))
                      Insert into LogErrori (Codric, Quan, Messagg) values (m.codric, this.w_SaldProg, this.w_NEGAPP)
                    endif
                    Select Elabor 
 indexseek(this.w_ChiaveInd,.t.)
                    Select Elabor 
 set order to codsal
                    locate for tiprec="7SCO-S" and codsal=this.w_CodSaldo and nvl(codcom, space(15))=this.w_comm rest
                    Select Elabor 
 set order to keyidx
                    if found()
                      if this.w_PPMRPDIV="S"
                        this.w_recelabor = recno()
                        Select Elabor_bis
                        go this.w_recelabor
                        Replace quanfi with quanfi+this.w_SaldProg
                        Select Elabor
                      endif
                      Replace quanti with quanti+this.w_SaldProg, quanfi with quanfi+this.w_SaldProg, ; 
 quantv with quantv+this.w_SaldProg, quanfv with quanfv+this.w_SaldProg
                      this.w_SaldProg = 0
                    endif
                    indexseek(this.w_ChiaveInd,.t.)
                  else
                    * --- Ignoro le giacenze negative
                    this.w_SaldProg = 0
                  endif
                endif
                if arflcom1="C" and tiprec="0SCOMM"
                  if used("SalPCom") and not empty(nvl(Elabor.codcom," "))
                    * --- Quantit� strettamente legata ad una commessa specifica
                    Insert into SalPCom (PCODCOM, PCODATT, PSALCOM,PSALOLD) values (Elabor.codcom, Elabor.codatt, this.w_SaldProg, this.w_SaldProg)
                  else
                    * --- Quantit� disponibile non abbinata a commessa, verr� consumata per data
                    this.w_RESCOM = max(Elabor.quanti,0)
                  endif
                  Select Elabor
                endif
              endif
            endif
          else
            this.w_OldSaldo = codsal
            * --- Si posiziona sul primo record (SALDO) dell'articolo esaminato per generare gli ODL
            indexseek(this.w_ChiaveInd,.t.)
            if ! this.w_FIRSTLAP and tiprec<>"0SCOMM" and arflcomm="S"
              * --- Se sono sul record con commessa vuota la indexseek mi porta su 0SALDI invece che su 0SCOMM
              skip
            endif
            scatter memvar
            if this.w_PPMRPDIV="S"
              this.w_recelabor = recno()
              Select Elabor_bis
              * --- Mi posiziono sul medesimo record di Elabor, i 2 cursori infatti sono l'uno il proseguimento dell'altro, facendo la scatter valorizzo
              *     i campi non presenti in Elabor, quelli comuni sono uguali.
              go this.w_recelabor
              scatter memvar
              Select Elabor
            endif
            if this.w_OldSaldo<>codsal
              * --- w_OldSaldo cambia solo al cambiare del codice di ricerca, una sola volta per articolo, il controllo
              *     serve per evitare di lanciare i messaggi ad ogni giro se l'articolo � gestito a commessa
              if this.w_MESSRIP AND artipges="F" and arflcom1<>"C" 
                * --- Messaggi ripianificazione
                numrec=recno()
                this.Page_10()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_PPMRPDIV="S"
                  Select Elabor_bis 
 go numrec
                endif
                Select Elabor 
 go numrec
              endif
            endif
            * --- Elabora ODL per inizio periodo scaduto
            this.w_SaldProg = 0
            if tiprec<>"0SALDI"
              * --- La generazione ODL avviene sulle righe 0SCOMM
              this.w_SKIP = FALSE
            endif
          endif
          this.w_ElabFabb = not this.w_ElabFabb
        else
          if not this.w_ElabFabb
            * --- Elabora i Fabbisogni
            this.w_QTA = iif(tiprec="7",quanti,0)
            this.w_QTA1 = 0
            this.w_COMMES = arflcomm="S" and not empty(nvl(codcom,""))
            this.w_COMMES1 = arflcom1="C"
            if this.w_COMMES
              * --- Se la commessa � vuota (e w_COMMES1 � .T.) la quantit� disponibile non � in SalPCom ma nella variabile w_RESCOM a disposizione del primo fabbisogno che trovo,
              *     in caso contrario leggo la quantit� disponibile per la commessa in esame.
              if this.w_COMMES1
                this.w_QTA1 = iif(tiprec="4",quanti,0)
                if used("SalPCom")
                  Select SalPCom 
 go top 
 locate for PCODCOM=Elabor.codcom
                  if found()
                    this.w_SaldProg = nvl(PSALCOM,0)+this.w_QTA1
                    this.w_Fabnet1 = nvl(PSALOLD,0)+this.w_QTA1
                  endif
                  Select Elabor
                endif
              endif
            else
              if this.w_COMMES1 and empty(nvl(codcom,""))
                * --- Articolo gestito a commessa+nettificazione ma senza codice commessa
                this.w_SaldProg = 0
              endif
            endif
            if artipart $ "PH-DC"
              if tiprec="7OLIMP" or tiprec="7OLIML"
                this.w_QTA = 0
              endif
              if this.w_SaldProg < -this.w_QTA
                * --- Impegna tutta la quantit� disponibile a magazzino
                replace quantv with -this.w_SaldProg*(quantv/quanti), quanti with -this.w_SaldProg, quanfi with -this.w_SaldProg
                if this.w_PPMRPDIV="S"
                  this.w_recelabor = recno()
                  Select Elabor_bis
                  go this.w_recelabor
                  replace quanfi with -this.w_SaldProg
                  Select Elabor
                endif
              else
                * --- La qt� richiesta � gi� disponibile a magazzino
                replace quantv with iif(this.w_QTA=0,this.w_QTA,this.w_QTA*(quantv/quanti)), quanti with this.w_QTA, quanfi with this.w_QTA
                if this.w_PPMRPDIV="S"
                  this.w_recelabor = recno()
                  Select Elabor_bis
                  go this.w_recelabor
                  replace quanfi with this.w_QTA
                  Select Elabor
                endif
              endif
            endif
            this.w_SaldProg = this.w_SaldProg+this.w_QTA
            this.w_Fabnet1 = this.w_Fabnet1+this.w_QTA
            if this.w_SaldProg<0
              * --- Disponibilit� periodo < 0
              this.w_Fabnet = -this.w_SaldProg
              this.w_Fabnet1 = -this.w_Fabnet1
              this.w_SaldProg = 0
              if this.w_COMMES1 and this.w_PPORDICM<>"S"
                * --- Sfrutto giacenze libere da commessa
                if this.w_Fabnet>this.w_RESCOM
                  * --- Fabbisogno maggiore della disponibilit� non abbinata, lo decurto della parte disponibile
                  this.w_Fabnet = this.w_Fabnet-this.w_RESCOM
                  this.w_Fabnet1 = max(this.w_Fabnet1-this.w_RESCOM,0)
                  this.w_RESCOM = 0
                else
                  * --- Fabbisogno coperto completamente della disponibilit� non abbinata
                  this.w_Fabnet1 = max(this.w_Fabnet1-this.w_RESCOM,0)
                  this.w_RESCOM = max(this.w_RESCOM-this.w_Fabnet,0)
                  this.w_Fabnet = 0
                endif
              endif
            else
              this.w_Fabnet = 0
              if this.w_Fabnet1<0
                this.w_Fabnet1 = -this.w_Fabnet1
              else
                this.w_Fabnet1 = 0
              endif
            endif
            if (this.oParentObject.w_INTERN or this.w_PEGGING)
              replace qtalor with this.w_SaldProg, qtanet with this.w_Fabnet
            else
              replace qtalor with this.w_SaldProg, qtanet with iif(this.w_COMMES1,this.w_Fabnet1,this.w_Fabnet)
            endif
            if this.w_COMMES1
              if used("SalPCom")
                * --- Aggiorno cursore
                Select SalPCom 
 go top 
 locate for PCODCOM=Elabor.codcom
                if found()
                  replace PSALCOM with this.w_SaldProg
                endif
                Select Elabor
              endif
            endif
          else
            * --- Elabora gli ODL
            if this.oParentObject.w_INTERN
              this.w_COMMES = arflcomm="S" and not empty(nvl(codcom,""))
              this.w_COMMES1 = arflcom1="C"
              this.w_QTA = iif(tiprec="8MRP-S" or tiprec="8PDA-S" or nvl(mgdismag,"S")="N", 0, qtanet * iif(this.w_COMMES,1,1+prpersca/100))
              this.w_QTANC = 0
              if this.w_PPORDICM="S" and this.w_COMMES1
                * --- Variabile di appoggio usata per lo storno della quantit� disponibile se l'ordinato non a commessa viene usato da un impegnato a commessa
                this.w_QTANC = this.w_QTA
              endif
              if ! this.w_COMMES1 or (! this.w_COMMES and this.w_COMMES1)
                this.w_SaldProg = this.w_SaldProg + iif(tiprec="4" and nvl(mgdismag,"S")<>"N", quanti, 0) - this.w_Qta
              endif
              if this.w_COMMES
                if this.w_CODCOM=nvl(codcom,space(15)) and this.w_CODATT=nvl(codatt,space(15))
                  this.w_SaldComm = this.w_SaldComm + iif(tiprec="4" and ! this.w_COMMES1, quanti, 0) - this.w_Qta
                  this.w_ImpComm = iif(this.w_ImpComm>=0, this.w_ImpComm+impcom, this.w_ImpComm)
                else
                  this.w_SaldComm = iif(tiprec="4" and ! this.w_COMMES1, quanti, 0) - this.w_Qta
                  this.w_ImpComm = iif(tiprec="7ORDCL", impcom, -1)
                  this.w_CODCOM = nvl(codcom,space(15))
                  this.w_CODATT = nvl(codatt,space(15))
                endif
              else
                this.w_SaldComm = 0
                this.w_ImpComm = 0
                this.w_CODCOM = space(15)
                this.w_CODATT = space(15)
              endif
              this.w_COMMES = this.w_COMMES and tiprec<>"4"
              * --- Somma i fabbisogni della stessa data
              this.w_Keyidx = left(Keyidx,this.w_LenKeyIdx)
              this.w_COEFFA = 1
              this.w_COEFFA1 = 1
              if not eof("Elabor") and (this.w_SaldProg<0 or this.w_SaldComm<0)
                if artipart $ "PH-DC-PS"
                  * --- Per i PH-DC-PS e per le commesse non deve sommarizzare
                  this.w_AltroPer = TRUE
                  this.w_COEFFA = cocoeimp
                  this.w_COEFFA1 = cocoeum1
                  this.w_Datidx = Datidx
                else
                  if this.w_PPORDICM="S" and this.w_COMMES1 Or PRPIAPUN <> "S"
                    skip
                    if this.w_PPORDICM="S" and this.w_COMMES1
                      * --- In questo caso non accorpa per evitare calcolo sbagliato del totale
                      this.w_AltroPer = keyidx<>this.w_Keyidx or tiprec="7SCO-S"
                    else
                      this.w_AltroPer = keyidx<>this.w_Keyidx
                    endif
                    skip -1
                    this.w_Datidx = Datidx
                  else
                    this.w_AltroPer = TRUE
                    this.w_Datidx = Datidx
                  endif
                endif
              endif
              * --- La round a 10 serve per evitare possibili problemi con le conversioni
              if (this.w_SaldComm<0 or round(this.w_SaldProg,10)<0) and this.w_AltroPer
                if this.w_COMMES
                  this.w_QTA = -this.w_SaldComm
                  this.w_SaldComm = 0
                  if ! this.w_COMMES1 or (! this.w_COMMES and this.w_COMMES1)
                    this.w_SaldProg = this.w_SaldProg + this.w_Qta
                  endif
                else
                  this.w_QTA = -this.w_SaldProg
                  this.w_SaldProg = 0
                endif
                if this.w_PPORDICM="S" and this.w_COMMES1 and this.w_QTA>0
                  * --- Nell'array totord ho memorizzato quantit� dell'ordinato libero e relativa data dell'ordine, se ne ho e la data corrisponde, consumo questa quantit� prima di fare l'ordine suggerito
                  this.contarray = 0
                  this.w_DisponNett = 0
                  do while this.contarray<=(alen(totord,1)-1) and this.w_QTA>0
                    this.contarray = this.contarray+1
                    if vartype(totord(this.contarray,1))="N"
                      if totord(this.contarray,1)>0
                        if totord(this.contarray,2)<=datidx
                          this.w_DisponNett = this.w_DisponNett+totord(this.contarray,1)
                          if this.w_DisponNett>=this.w_QTANC
                            this.w_DisponNett = this.w_DisponNett-this.w_QTANC
                            * --- Storno ordinato libero
                            this.w_SaldProg = max(this.w_SaldProg - this.w_QTA,0)
                            this.w_QTA = 0
                            totord(this.contarray,1)=this.w_DisponNett
                          else
                            this.w_QTA = this.w_QTA-this.w_DisponNett
                            * --- Storno ordinato libero
                            this.w_SaldProg = max(this.w_SaldProg - this.w_DisponNett,0)
                            this.w_DisponNett = 0
                            totord(this.contarray,1)=this.w_DisponNett
                          endif
                        endif
                      endif
                    else
                      exit
                    endif
                  enddo
                  this.w_QTANC = 0
                endif
                * --- Gestione giacenze libere
                if this.w_PPORDICM="S" and this.w_COMMES1 and this.w_QTA>0
                  * --- Sfrutto giacenze libere da commessa
                  if this.w_QTA>this.w_RESCOM
                    * --- Fabbisogno maggiore della disponibilit� non abbinata, lo decurto della parte disponibile
                    this.w_QTA = this.w_QTA-this.w_RESCOM
                    this.w_RESCOM = 0
                  else
                    * --- Fabbisogno coperto completamente della disponibilit� non abbinata
                    this.w_RESCOM = max(this.w_RESCOM-this.w_QTA,0)
                    this.w_QTA = 0
                  endif
                endif
                if this.w_QTA>0
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if this.w_nocomp
                    * --- Se non ho componenti da esplodere (distinta base non definita alla data di elaborazione) sono gi� sul record giusto
                    this.w_SKIP = FALSE
                  endif
                endif
              else
                if this.w_QTANC>0 and this.w_SaldProg>=0
                  * --- Devo stornare l'array
                  * --- Nell'array totord ho memorizzato quantit� dell'ordinato libero e relativa data dell'ordine, se ne ho e la data corrisponde, consumo questa quantit� prima di fare l'ordine suggerito
                  this.contarray = 0
                  this.w_DisponNett = 0
                  do while this.contarray<=(alen(totord,1)-1) and this.w_QTANC>0
                    this.contarray = this.contarray+1
                    if vartype(totord(this.contarray,1))="N"
                      if totord(this.contarray,1)>0
                        if totord(this.contarray,2)<=datidx
                          this.w_DisponNett = this.w_DisponNett+totord(this.contarray,1)
                          if this.w_DisponNett>=this.w_QTANC
                            this.w_DisponNett = this.w_DisponNett-this.w_QTANC
                            this.w_SaldProg = max(this.w_SaldProg - this.w_QTANC,0)
                            this.w_QTA = this.w_QTA-this.w_QTANC
                            if ! empty(this.w_CODCOM)
                              * --- Storno ordinato libero
                              this.w_SaldComm = this.w_SaldComm + this.w_QTANC
                            endif
                            this.w_QTANC = 0
                            totord(this.contarray,1)=this.w_DisponNett
                          else
                            this.w_QTANC = this.w_QTANC-this.w_DisponNett
                            this.w_SaldProg = max(this.w_SaldProg - this.w_DisponNett,0)
                            this.w_QTA = this.w_QTA - this.w_DisponNett
                            if ! empty(this.w_CODCOM)
                              * --- Storno ordinato libero
                              this.w_SaldComm = this.w_SaldComm + this.w_DisponNett
                            endif
                            this.w_DisponNett = 0
                            totord(this.contarray,1)=this.w_DisponNett
                          endif
                        endif
                      endif
                    else
                      exit
                    endif
                  enddo
                endif
              endif
            endif
          endif
        endif
      else
        if this.w_CodSaldo<>Codsal
          if this.w_ElabFabb
            this.w_CambioCodice = .T.
            if not empty(this.w_CodSaldo)
              * --- Applico i giorni di copertura
              this.Page_14()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Nuovo articolo da esaminare
            this.firstcom = .t.
            this.w_CodSaldo = codsal
            this.w_SaldProg = quanti
            this.w_ChiaveInd = keyidx
            this.w_ARFLCOM1 = ARFLCOM1
            if !this.w_UseProgBar
              * --- --NON OCCORRE TRADUZIONE: uso wait window per migliorare performance
              if INT(recno()/1000)<>this.w_INTWAIT
                this.w_INTWAIT = INT(recno()/1000)
                wait window("Record: "+str(recno())+" / LLC: "+ALLTRIM(STR(nvl(prlowlev,0)))+" / codice saldo: "+this.w_CodSaldo) nowait
              endif
            endif
            if this.w_PPMRPDIV="S"
              Select Elabor_bis
            endif
            do case
              case empty(nvl(this.w_PPCARVAL,"")) or this.w_PPCARVAL="CS"
                this.w_COSTO = nvl(COSSTA,0)
              case this.w_PPCARVAL="CM"
                this.w_COSTO = nvl(COSMED,0)
              case this.w_PPCARVAL="UC"
                this.w_COSTO = nvl(COSULT,0)
              case this.w_PPCARVAL="CL"
                this.w_COSTO = nvl(COSLIS,0)
            endcase
            if this.w_PPMRPDIV="S"
              Select Elabor
            endif
            if artipges="F"
              if tiprec<>"0SALDI"
                * --- Il saldo non � presente solo per quegli articoli che derivano dall'esplosione di una distinta configurata
                this.w_ChiaveInd2 = left(this.w_ChiaveInd,this.w_RootKeyIdx)+IIF(this.w_ARFLCOM1="C","19000101"+space(30),space(30)+"19000101")+"0"
                Select Elabor
                if Not(indexseek(this.w_ChiaveInd2,.t.))
                   datidx="19000101" 
 keyidx=left(m.keyidx,this.w_RootKeyIdx)+space(30)+m.datidx+"0" 
 quanti=0 
 quanfi=0 
 quantv=0 
 quanfv=0 
 qtanet=0 
 qtalor=0 
 cododl=space(15) 
 tiprec="0SALDI" 
 append blank 
 gather memvar
                  if this.w_PPMRPDIV="S"
                    select elabor_bis 
 append blank 
 gather memvar
                    Select Elabor
                  endif
                endif
                this.w_SaldProg = quanti
                this.w_ChiaveInd = keyidx
              endif
              if this.w_SaldProg<0 and artipart $ "PH-DC-PS"
                * --- ANOMALIA: la diponibilit� di un PH non pu� essere negativa!
                this.w_ErrMsg = "PH"
                this.Page_8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                Select Elabor
                go bottom
              else
                * --- Calcola la Disponibilit� (x Gest. a Fabbisogno)
                this.w_Dispon = 0
                if not eof("Elabor")
                  skip
                endif
                Select Elabor 
 set order to codsal
                if this.w_PPMRPDIV="S"
                  this.w_recelabor = recno()
                  Select Elabor_bis 
 set order to codsal 
 go this.w_recelabor 
 Select Elabor
                endif
                do while not eof("Elabor") and this.w_CodSaldo=codsal
                  if artipart $ "PH-DC-PS"
                    if quanti>0
                      this.w_Dispon = this.w_Dispon+quanti
                    endif
                  else
                    if tiprec<>"7MRPIM" and tiprec<>"7SCO-S" and nvl(mgdismag,"S")<>"N" and tiprec<>"0SCOMM"
                      this.w_Dispon = this.w_Dispon+quanti
                    endif
                  endif
                  if tiprec = "7OL---"
                    if this.w_PPMRPDIV="S"
                      this.w_recelabor = recno()
                      Select Elabor_bis 
 go this.w_recelabor 
 delete 
 Select Elabor
                    endif
                    delete
                  endif
                  skip
                enddo
                if this.w_PPMRPDIV="S"
                  * --- Ripristino l'ordinamento ma non mi riposiziono, lo far� al momento in cui sar� necessario
                  Select Elabor_bis 
 set order to keyidx
                endif
                Select Elabor 
 set order to keyidx
                indexseek(this.w_ChiaveInd,.t.)
                this.w_SaldProg = this.w_SaldProg-this.w_Dispon
                replace qtalor with this.w_SaldProg
              endif
              if this.w_SaldProg < -0.001
                if this.w_GIANEG="S"
                  * --- Considero anche le giacenze negative
                  Insert into LogErrori (Codric, Quan, Messagg) values (m.codric, this.w_SaldProg, ah_msgformat("Giacenza negativa"))
                  Select Elabor 
 indexseek(this.w_ChiaveInd,.t.)
                  Select Elabor 
 set order to codsal
                  locate for tiprec="7SCO-S" and codsal=this.w_CodSaldo rest
                  Select Elabor 
 set order to keyidx
                  if found()
                    if this.w_PPMRPDIV="S"
                      this.w_recelabor = recno()
                      Select Elabor_bis
                      go this.w_recelabor
                      Replace quanfi with quanfi+this.w_SaldProg
                      Select Elabor
                    endif
                    Replace quanti with quanti+this.w_SaldProg, quanfi with quanfi+this.w_SaldProg, ; 
 quantv with quantv+this.w_SaldProg, quanfv with quanfv+this.w_SaldProg
                    this.w_SaldProg = 0
                  endif
                  indexseek(this.w_ChiaveInd,.t.)
                else
                  * --- Ignoro le giacenze negative
                  this.w_SaldProg = 0
                endif
              endif
            endif
          else
            * --- Si posiziona sul primo record (SALDO) dell'articolo esaminato per generare gli ODL
            indexseek(this.w_ChiaveInd,.t.)
            scatter memvar
            if this.w_PPMRPDIV="S"
              this.w_recelabor = recno()
              Select Elabor_bis
              * --- Mi posiziono sul medesimo record di Elabor, i 2 cursori infatti sono l'uno il proseguimento dell'altro, facendo la scatter valorizzo
              *     i campi non presenti in Elabor, quelli comuni sono uguali.
              go this.w_recelabor
              scatter memvar
              Select Elabor
            endif
            if this.w_MESSRIP AND artipges="F" and arflcom1<>"C" 
              * --- Messaggi ripianificazione
              numrec=recno()
              this.Page_10()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_PPMRPDIV="S"
                Select Elabor_bis 
 go numrec
              endif
              Select Elabor 
 go numrec
            endif
            * --- Elabora ODL per inizio periodo scaduto
            this.w_SaldProg = 0
            this.w_SKIP = FALSE
          endif
          this.w_ElabFabb = not this.w_ElabFabb
        else
          if not this.w_ElabFabb
            * --- Elabora i Fabbisogni
            this.w_QTA = iif(tiprec="7" and nvl(mgdismag,"S")<>"N",quanti,0)
            this.w_COMMES = arflcomm="S" and not empty(nvl(codcom,""))
            this.w_COMMES1 = arflcom1="C"
            if artipart $ "PH-DC"
              if tiprec="7OLIMP" or tiprec="7OLIML"
                this.w_QTA = 0
              endif
              if prexplph<>"S"
                * --- PREXPLPH -> 'S' = Standard 'D' = Gestisci nei documenti
                *     
                *     Se l'articolo � di tipo fantasma e ho il flag impostato a gestisci nei documenti non considero la disponibilit� 
                *     e dico alla procedura di esplodere sempre il fantasma
                replace quantv with iif(this.w_QTA=0,this.w_QTA,this.w_QTA*(quantv/quanti)), quanti with this.w_QTA, quanfi with 0
                if this.w_PPMRPDIV="S"
                  this.w_recelabor = recno()
                  Select Elabor_bis
                  go this.w_recelabor
                  replace quanfi with this.w_QTA
                  Select Elabor
                endif
              else
                if this.w_SaldProg < -this.w_QTA
                  * --- Impegna tutta la quantit� disponibile a magazzino
                  replace quantv with -this.w_SaldProg*(quantv/quanti), quanti with -this.w_SaldProg, quanfi with -this.w_SaldProg
                  if this.w_PPMRPDIV="S"
                    this.w_recelabor = recno()
                    Select Elabor_bis
                    go this.w_recelabor
                    replace quanfi with -this.w_SaldProg
                    Select Elabor
                  endif
                else
                  * --- La qt� richiesta � gi� disponibile a magazzino
                  replace quantv with iif(this.w_QTA=0,this.w_QTA,this.w_QTA*(quantv/quanti)), quanti with this.w_QTA, quanfi with this.w_QTA
                  if this.w_PPMRPDIV="S"
                    this.w_recelabor = recno()
                    Select Elabor_bis
                    go this.w_recelabor
                    replace quanfi with this.w_QTA
                    Select Elabor
                  endif
                endif
              endif
            endif
            if this.w_COMMES
              this.qtaabb = 0
              * --- Verifico l'esistenza di una quantit� a magazzino riferita a questa commessa
              if this.w_DISMAG="S"
                select SalComm 
 scan for oltcodic=Elabor.Codric and (oltcomme=Elabor.CodCom or empty(oltcomme)) and petiprif="M"
                if this.w_QTA<>0
                  if this.qtaabb=0
                    this.w_Fabnet = -this.w_QTA
                  endif
                  this.qtaabb = PEQTAABB
                  if this.qtaabb<>0 and this.w_Fabnet<>0
                    this.riga = recno()
                    scatter memvar 
 m.petiprif=iif(Elabor.tiprec<>"7ORDCL","O","D") 
 m.perigord=iif(Elabor.tiprec="7MRPIM",Elabor.cprownum,Elabor.perownum) 
 m.oltcomme=Elabor.CodCom
                    if m.petiprif="O"
                      m.perifodl=Elabor.padrered
                      m.peserord=" "
                    else
                      m.peserord=Elabor.padrered
                      m.perifodl=" "
                    endif
                    if this.qtaabb>=this.w_Fabnet
                      replace peqtaabb with this.qtaabb-this.w_Fabnet
                      m.peqtaabb=this.w_Fabnet
                      this.w_Fabnet = 0
                    else
                      this.w_Fabnet = this.w_Fabnet-this.qtaabb
                      replace peqtaabb with 0
                    endif
                    append blank 
 gather memvar
                    go this.riga
                  endif
                endif
                endscan
              endif
              if this.qtaabb=0
                this.w_Fabnet = -this.w_QTA
              endif
              if this.w_COMMES1 and this.w_FABNET>0
                this.w_ABBQTA = 0
                * --- Cerco Giacenze abbinabili presenti a magazzino
                Select AbbCom 
 scan for slcodice=this.w_CodSaldo and PERQTA>0
                if this.w_Fabnet>0
                  * --- Quantit� Abbinabile
                  this.w_ABBQTA = min(this.w_FABNET,AbbCom.PERQTA)
                  * --- Aggiorno Fabbisogno
                  this.w_Fabnet = this.w_Fabnet-this.w_ABBQTA
                  * --- Aggiorno Residuo a Magazzino
                  Select AbbCom 
 replace PERQTA with PERQTA-this.w_ABBQTA
                  * --- Aggiorno SalComm
                  insert into salcomm (PESERIAL,PETIPRIF,PEODLORI,OLTCOMME,OLTCODIC,OLTCOMAG,PEQTAABB,; 
 PESERODL,PERIFODL,PESERORD,PERIGORD,PEQTAORI,CORDINA); 
 values; 
 ("9999999999",iif(Elabor.tiprec<>"7ORDCL","O","D")," Magazz.: "+alltrim(AbbCom.SLCODMAG),Elabor.CodCom,Elabor.CodRic,; 
 AbbCom.SLCODMAG,this.w_ABBQTA," Magazz.: "+alltrim(AbbCom.SLCODMAG),iif(Elabor.tiprec<>"7ORDCL",; 
 Elabor.padrered," "),iif(Elabor.tiprec<>"7ORDCL"," ",Elabor.padrered),iif(Elabor.tiprec="7MRPIM",; 
 Elabor.cprownum,Elabor.perownum),0,"Z")
                  Select AbbCom
                else
                  * --- Fabbisogno esaurito, esco dalla scan
                  exit
                endif
                endscan
              endif
              Select Elabor
            else
              if arflcomm="S" and empty(nvl(codcom,""))
                * --- Potrei avere una quantit� disponibile (contenuta in w_SaldProg) gi� abbinata con un elemento gestito a commessa (codcom non vuoto),
                *     per evitare di abbinare due volte la medesima quantit� riverifico w_SaldProg
                * --- Q.t� totale abbinata
                select SalComm 
 sum peqtaori for oltcodic=Elabor.Codric and petiprif="M" to qtaori
                Select Elabor
                this.w_SaldProg = iif(this.w_SaldProg>0,max(this.w_SaldProg-qtaori,0),this.w_SaldProg)
              endif
              if artipart="PH" and prexplph<>"S"
                * --- PREXPLPH -> 'S' = Standard 'D' = Gestisci nei documenti
                *     
                *     Se l'articolo � di tipo fantasma e ho il flag impostato a gestisci nei documenti non considero la disponibilit� 
                *     e dico alla procedura di esplodere sempre il fantasma
                this.w_SaldProgPH = this.w_QTA
                if this.w_SaldProgPH<0
                  * --- Disponibilit� periodo < 0
                  this.w_FabnetPH = -this.w_SaldProgPH
                  this.w_SaldProgPH = 0
                else
                  this.w_FabnetPH = 0
                endif
              else
                this.w_SaldProg = this.w_SaldProg+this.w_QTA
                if this.w_SaldProg<0
                  * --- Disponibilit� periodo < 0
                  this.w_Fabnet = -this.w_SaldProg
                  this.w_SaldProg = 0
                else
                  this.w_Fabnet = 0
                endif
              endif
            endif
            if artipart="PH" and prexplph<>"S"
              * --- PREXPLPH -> 'S' = Standard 'D' = Gestisci nei documenti
              *     
              *     Se l'articolo � di tipo fantasma e ho il flag impostato a gestisci nei documenti non considero la disponibilit� 
              *     e dico alla procedura di esplodere sempre il fantasma
              replace qtalor with this.w_SaldProgPH, qtanet with this.w_FabnetPH
            else
              replace qtalor with this.w_SaldProg, qtanet with this.w_Fabnet
            endif
          else
            * --- Elabora gli ODL
            if this.oParentObject.w_INTERN
              this.w_COMMES = arflcomm="S" and not empty(nvl(codcom,""))
              this.w_COMMES1 = arflcom1="C"
              this.w_QTA = iif(tiprec="8MRP-S" or tiprec="8PDA-S" or nvl(mgdismag,"S")="N", 0, qtanet * iif(this.w_COMMES,1,1+prpersca/100))
              this.w_SaldProg = this.w_SaldProg + iif(tiprec="4" and nvl(mgdismag,"S")<>"N", quanti, 0) - this.w_Qta
              if this.w_COMMES
                if this.w_CODCOM=nvl(codcom,space(15)) and this.w_CODATT=nvl(codatt,space(15))
                  this.w_SaldComm = this.w_SaldComm + iif(tiprec="4", quanti, 0) - this.w_Qta
                  this.w_ImpComm = iif(this.w_ImpComm>=0, this.w_ImpComm+impcom, this.w_ImpComm)
                else
                  this.w_SaldComm = iif(tiprec="4", quanti, 0) - this.w_Qta
                  this.w_ImpComm = iif(tiprec="7ORDCL", impcom, -1)
                  this.w_CODCOM = nvl(codcom,space(15))
                  this.w_CODATT = nvl(codatt,space(15))
                endif
              else
                this.w_SaldComm = 0
                this.w_ImpComm = 0
                this.w_CODCOM = space(15)
                this.w_CODATT = space(15)
              endif
              this.w_COMMES = this.w_COMMES and tiprec<>"4"
              * --- Somma i fabbisogni della stessa data
              this.w_Keyidx = left(Keyidx,this.w_LenKeyIdx)
              this.w_COEFFA = 1
              this.w_COEFFA1 = 1
              if not eof("Elabor") and (this.w_SaldProg<0 or this.w_SaldComm<0)
                if artipart $ "PH-DC-PS"
                  * --- Per i PH-DC-PS e per le commesse non deve sommarizzare
                  this.w_AltroPer = TRUE
                  this.w_COEFFA = cocoeimp
                  this.w_COEFFA1 = cocoeum1
                  this.w_Datidx = Datidx
                else
                  if PRPIAPUN <> "S"
                    skip
                    this.w_AltroPer = keyidx<>this.w_Keyidx
                    skip -1
                    this.w_Datidx = Datidx
                  else
                    this.w_AltroPer = TRUE
                    this.w_Datidx = Datidx
                  endif
                endif
              endif
              * --- La round a 10 serve per evitare possibili problemi con le conversioni
              if (this.w_SaldComm<0 or round(this.w_SaldProg,10)<0) and this.w_AltroPer
                if this.w_COMMES
                  this.w_QTA = -this.w_SaldComm
                  this.w_SaldComm = 0
                  this.w_SaldProg = this.w_SaldProg + this.w_Qta
                else
                  this.w_QTA = -this.w_SaldProg
                  this.w_SaldProg = 0
                endif
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_nocomp
                  * --- Se non ho componenti da esplodere (distinta base non definita alla data di elaborazione) sono gi� sul record giusto
                  this.w_SKIP = FALSE
                endif
              endif
            endif
          endif
        endif
      endif
      if this.w_SKIP
        * --- Non fa skip solo sul primo record del Saldo nella fase di generazione ODL
        skip
        if this.w_ElabFabb
          locate for elabora and not(proggmps $ "SR") and tiprec<>"8MRP-S" and tiprec<>"8PDA-S" rest
        endif
      else
        this.w_SKIP = TRUE
      endif
      if this.w_MRPLOG="S"
        this.w_MRPTOT = seconds()
        if cp_round(this.w_MRPTOT-this.w_MRPFIN,2)>=0.2
          this.w_oMRPLOG.ADDMSGLOG("Tempo di elaborazione record MRP %2: %1", this.TimeMRP(this.w_MRPTOT-this.w_MRPFIN), alltrim(this.w_CodSaldo)+" "+alltrim(str(recno())))     
        endif
        this.w_MRPFIN = this.w_MRPTOT
        this.w_MESTOT = seconds()-this.w_MESFIN
        if this.w_MESTOT>=1200
          this.w_MESFIN = seconds()
          this.Page_16()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if this.w_CambioCodice
        if this.w_UseProgBar
          this.w_NRECCUR = this.w_NRECCUR + 1
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
        else
          * --- Nel caso non ho la progress riattivo la procedura
          Select Elabor
        endif
        this.w_CambioCodice = .F.
      endif
      if MDOWN()
        this.oBtnCancel = SYS(1270)
        if type("This.oBtnCancel")="O" and !IsNull(this.oBtnCancel) and type("this.oBtnCancel.caption")<>"U"
          this.w_CANCELROUTINE = this.oBtnCancel.Caption =="\<Interrompi"
        endif
      endif
      if this.w_CANCELROUTINE
        Select Elabor
        if ah_Yesno("Si desidera interrompere l'elaborazione?")
          exit
        else
          this.w_CANCELROUTINE = .F.
          Select Elabor
        endif
      endif
      this.w_INTWAIT1 = this.w_INTWAIT1 + 1
      Select Elabor
    enddo
    if vartype(this.Padre.w_EDITBTN)="C"
      * --- Variabile che serve per abilitare/disabilitare il bottone interrompi
      this.Padre.w_EDITBTN = "X"
      this.Padre.mEnableControls()     
      this.Padre.Refresh()     
      wait wind "" timeout 0.001
    endif
    if this.w_CANCELROUTINE
      ctablemrp = "elab"+alltrim(i_codazi)+alltrim(this.w_SERIALE) 
      if used("Elabor")
        if this.w_MRPLOG="S"
          this.w_DBFINIZ = seconds()
        endif
        * --- Memorizza il cursore di elaborazione su disco (dbf)
        USE IN SELECT(ctablemrp)
        if this.oParentObject.w_INTERN or this.w_PEGGING
          if file(this.w_FileName)
            USE (this.w_FileName) IN 0 EXCL
            select (ctablemrp)
            ZAP
          endif
          if this.w_PPMRPDIV="S"
            Select Elabor_bis 
 set order to keyidx 
 w_FileName_bis=LEFT(this.w_FileName, LEN(this.w_filename)-4)+"_bis.dbf" 
 Select * from Elabor_bis order by keyidx into table (w_FileName_bis) 
 ctablemrp_bis=ctablemrp+"_bis" 
 use in select(ctablemrp_bis)
          endif
          Select Elabor 
 set order to keyidx 
 Select * from Elabor order by keyidx into table (this.w_FileName) 
 use in select(ctablemrp)
          if this.w_MRPLOG="S"
            this.w_DBFFIN = seconds()
            this.w_DBFTOT = this.w_DBFFIN-this.w_DBFINIZ
            this.w_oMRPLOG.ADDMSGLOG("Tempo di scrittura DBF su disco: %1", this.TimeMRP(this.w_DBFTOT))     
          endif
          if this.oParentObject.w_INTERN
            USE IN SELECT("Componen")
          endif
        endif
        USE IN SELECT("Elabor") 
 USE IN SELECT("Calend") 
 USE IN SELECT("ODP")
        if this.w_PPMRPDIV="S"
          USE IN SELECT("Elabor_bis")
        endif
        USE IN SELECT("Catena")
        USE IN SELECT("AbbCom")
      endif
      this.Page_17()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_errormsg("Elaborazione interrotta dall'utente!%0elaborazione abortita!",16)
      if this.w_MRPLOG="S"
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if ah_YesNo("Desideri la stampa del Log dell'elaborazione?")
          * --- Lancia Stampa
          this.w_oMRPLOG.PRINTLOG(This , ah_MsgFormat(this.w_LogTitle, this.padre.caption, g_APPLICATION, g_VERSION) , .f.)     
        endif
      endif
      i_retcode = 'stop'
      return
    endif
    if (this.oParentObject.w_INTERN or this.w_PARAM="TGSMR_KMM") and this.oParentObject.w_MODELA="R"
      * --- Elimino il record Vuoto inserito in precedenza;
      *     prima dell'ingresso nella (do while not eof("Elabor"))
      if this.w_PPMRPDIV="S"
        Select Elabor_bis 
 set order to codsal_key
        delete from Elabor_bis where codsal=space(this.w_LenCodSal)
        Select Elabor_bis 
 set order to keyidx
      endif
      Select Elabor 
 set order to codsal_key
      delete from Elabor where codsal=space(this.w_LenCodSal)
      Select Elabor 
 set order to keyidx
    endif
    if this.w_MRPLOG="S"
      this.w_MRPFIN = seconds()
      this.w_MRPTOT = this.w_MRPFIN-this.w_MRPINIZ
      this.w_oMRPLOG.ADDMSGLOG("Tempo di elaborazione dati cursore MRP: %1", this.TimeMRP(this.w_MRPTOT))     
    endif
    this.Padre.mEnableControls()     
    this.Padre.Refresh()     
    wait wind "" timeout 0.001
    if this.w_ErrMsg<>"PH" and this.oParentObject.w_INTERN
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_MRPLOG="S"
        this.w_oMRPLOG.ADDMSGLOG("Tempo di scrittura pre-elaborazione dati su DB: %1", this.TimeMRP(this.w_finedbiniz))     
        this.w_oMRPLOG.ADDMSGLOG("Tempo scrittura dati elaborazione su DB: %1", this.TimeMRP(this.w_finedbagg))     
      endif
    endif
    * --- Chiude i cursori
    ctablemrp = "elab"+alltrim(i_codazi)+alltrim(this.w_SERIALE) 
    if used("Elabor")
      if this.w_MRPLOG="S"
        this.w_DBFINIZ = seconds()
      endif
      * --- Memorizza il cursore di elaborazione su disco (dbf)
      USE IN SELECT(ctablemrp)
      if this.oParentObject.w_INTERN or this.w_PEGGING
        if file(this.w_FileName)
          USE (this.w_FileName) IN 0 EXCL
          select (ctablemrp)
          ZAP
        endif
        if this.w_PPMRPDIV="S"
          Select Elabor_bis 
 set order to keyidx 
 w_FileName_bis=LEFT(this.w_FileName, LEN(this.w_filename)-4)+"_bis.dbf" 
 Select * from Elabor_bis order by keyidx into table (w_FileName_bis) 
 ctablemrp_bis=ctablemrp+"_bis" 
 use in select(ctablemrp_bis)
        endif
        Select Elabor 
 set order to keyidx 
 Select * from Elabor order by keyidx into table (this.w_FileName) 
 use in select(ctablemrp)
        if this.w_MRPLOG="S"
          this.w_DBFFIN = seconds()
          this.w_DBFTOT = this.w_DBFFIN-this.w_DBFINIZ
          this.w_oMRPLOG.ADDMSGLOG("Tempo di scrittura DBF su disco: %1", this.TimeMRP(this.w_DBFTOT))     
        endif
        if this.oParentObject.w_INTERN
          USE IN SELECT("Componen")
        endif
      endif
      USE IN SELECT("Elabor") 
 USE IN SELECT("Calend") 
 USE IN SELECT("ODP")
      if this.w_PPMRPDIV="S"
        USE IN SELECT("Elabor_bis")
      endif
      USE IN SELECT("Catena")
      USE IN SELECT("AbbCom")
    endif
    USE IN SELECT("ODL")
    USE IN SELECT("OrdiODL")
    USE IN SELECT("ImpeODL")
    USE IN SELECT("ImpeODL")
    USE IN SELECT("COrdiODL")
    USE IN SELECT("Saldi")
    USE IN SELECT("SaldiCom")
    USE IN SELECT("odl2del")
    USE IN SELECT("Ordi")
    USE IN SELECT("Commesse")
    USE IN SELECT("Cicli")
    USE IN SELECT("TmpCic")
    USE IN SELECT("TmpCic2")
    USE IN SELECT("Contratti")
    USE IN SELECT("ContrattiA")
    USE IN SELECT("Materzis")
    USE IN SELECT("CodFor")
    USE IN SELECT("Errori")
    USE IN SELECT("SalPCom")
    if this.oParentObject.w_INTERN
      if g_PRFA="S" and g_CICLILAV="S" and empty(this.w_ErrMsg)
        if this.w_MRPLOG="S"
          this.w_COLINIZ = seconds()
        endif
        * --- Esegue elaborazione del conto lavoro
        GSCI_BCO(this,"MRP")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_MRPLOG="S"
          this.w_COLFIN = seconds()
          this.w_COLTOT = this.w_COLFIN-this.w_COLINIZ
          this.w_oMRPLOG.ADDMSGLOG("Tempo di elaborazione conto lavoro: %1", this.TimeMRP(this.w_COLTOT))     
        endif
      endif
      if this.w_MESSRIP
        if this.w_MRPLOG="S"
          this.w_MESINIZ = seconds()
          this.w_MESFIN = seconds()
        endif
        GSMR_BEX(this,"MRP")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_UseProgBar
          GesProgBar("M", this)
        endif
        if this.w_MRPLOG="S"
          this.w_MESFIN = seconds()
          this.w_MESTOT = this.w_MESFIN-this.w_MESINIZ
          this.w_oMRPLOG.ADDMSGLOG("Tempo di elaborazione messaggi di ripianificazione: %1", this.TimeMRP(this.w_MESTOT))     
        endif
      endif
      if this.w_GENPODA="S" and empty(this.w_ErrMsg) and not empty(this.oParentObject.w_CRITFORN)
        * --- Generazione PDA
        if this.w_MRPLOG="S"
          this.w_PDAINIZ = seconds()
        endif
        * --- Generazione PDA (Inizializza variabili del Batch)
        this.w_DATREG = i_DATSYS
        this.w_ORAREG = TIME()
        this.w_MCRIFOR = this.oParentObject.w_CRITFORN
        GSAC_BFB(this,"B")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_MRPLOG="S"
          this.w_PDAFIN = seconds()
          this.w_PDATOT = this.w_PDAFIN-this.w_PDAINIZ
          this.w_oMRPLOG.ADDMSGLOG("Tempo di elaborazione PDA: %1", this.TimeMRP(this.w_PDATOT))     
        endif
      endif
      if empty(this.w_ErrMsg)
        if this.w_MRPLOG="S"
          this.w_PEGINIZ = seconds()
        endif
        * --- Genera Pegging di secondo livello
        do GSMR_BPG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_MRPLOG="S"
          this.w_PEGFIN = seconds()
          this.w_PEGTOT = this.w_PEGFIN-this.w_PEGINIZ
          this.w_oMRPLOG.ADDMSGLOG("Tempo di elaborazione pegging di secondo livello: %1", this.TimeMRP(this.w_PEGTOT))     
        endif
      endif
      if this.oParentObject.w_MODELA="R" and this.w_SELEZ="S"
        * --- Svuoto ART_TEMP
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if this.w_UseProgBar
        GesProgBar("E", this)
      endif
      if used("tmpLLC")
        if RecCount("tmpLLC") > 0
          * --- Stampa nuovi articoli inseriti dal calcolo del LLC
          Select * from tmpLLC into cursor __tmp__
          cp_chprn("..\COLA\EXE\QUERY\gsmr1blc", " ", this)
        endif
        USE IN SELECT("tmpLLC")
      endif
      this.w_LogErrori = "Insert"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Sblocca l'elaborazione
      GSMR_BPP(this,"UNLOCKGEN")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_MRPLOG="S"
        this.w_FINE = seconds()
        * --- se � cambiato il giorno faccio il conto con il numero di secondi di un giorno per il numero di giorni di elaborazione
        this.w_TOTELAB = this.w_fine+86400*(DATE()-this.w_INIZ_DATE)-this.w_iniz
        this.w_oMRPLOG.ADDMSGLOG("Tempo totale di elaborazione: %1", this.TimeMRP(this.w_totelab))     
        this.w_oMRPLOG.ADDMSGLOG("Record elaborati: %1", alltrim(str(this.w_quanti)))     
        this.w_oMRPLOG.ADDMSGLOG("Fine elaborazione: [%1]", ALLTRIM(TTOC(DATETIME())))     
      endif
      if this.w_LNumErr=0 and empty(this.w_ErrMsg)
        ah_ErrorMsg("Aggiornamento completato","!","")
      endif
      if this.w_LNumErr>0
        this.w_LogErrori = "Display"
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      if this.w_PEGGING
        if empty(this.w_ErrMsg)
          this.w_MRPLOG = this.Padre.w_MRPLOG
          * --- Genera Pegging di secondo livello
          do GSMR_BPG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_PARAM<>"TGSMR1BGP"
            * --- Elimina il file DBF usato per l'aggiornamento del Pegging
            l_DBFdacanc=(alltrim(this.w_PATHEL)+"elab"+alltrim(i_CODAZI)+alltrim(this.w_SERIALE)+".DBF") 
 l_ErrorDel=.f. 
 l_OldOnError=ON("Error")
            if file(l_DBFdacanc)
              ON Error l_ErrorDel=.t. 
 delete file (l_DBFdacanc)
              * --- * Ripristina ON ERROR
              if not(empty(l_OldOnError))
                ON Error &l_OldOnError
              else
                ON Error
              endif
              if l_ErrorDel
                ah_ErrorMsg("Impossibile eliminare il file elab%1%2.dbf%0Provvedere manualmente alla sua eliminazione",16,"", alltrim(i_CODAZI), alltrim(this.w_SERIALE))
              endif
            endif
          endif
        endif
      endif
      * --- Sblocca l'elaborazione
      GSMR_BPP(this,"UNLOCKGEN")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_PARAM<>"TGSMR1BGP"
        ah_ErrorMsg("Aggiornamento completato",64)
      endif
      this.w_LogErrori = "Insert"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.Page_17()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    USE IN SELECT("SalComm")
    USE IN SELECT("inputfasext")
    * --- Drop temporary table TMPDOCUM
    i_nIdx=cp_GetTableDefIdx('TMPDOCUM')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDOCUM')
    endif
    if (this.oParentObject.w_INTERN or this.w_PARAM="TGSMR_KMM") and this.oParentObject.w_MODELA="R" and (this.w_SELEZ="S" or this.w_GENPODA="O")
      * --- Nel caso si stiano effettuando delle selezioni pulisco ART_TEMP
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    if this.w_MRPLOG="S"
      L_TAggCur = .f.
      this.Page_16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if ah_YesNo("Desideri la stampa del Log dell'elaborazione?")
        * --- Lancia Stampa
        this.w_oMRPLOG.PRINTLOG(This , ah_MsgFormat(this.w_LogTitle, this.padre.caption, g_APPLICATION, g_VERSION) , .f.)     
      endif
    endif
  endproc
  proc Try_055145B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SEL__MRP
    i_nConn=i_TableProp[this.SEL__MRP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEL__MRP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SEL__MRP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GMSERIAL"+",GMDATELA"+",GMORAELA"+",GMPATHEL"+",GMUTEELA"+",GMCODINI"+",GMCODFIN"+",GMFAMAIN"+",GMFAMAFI"+",GMGRUINI"+",GMGRUFIN"+",GMCATINI"+",GMCATFIN"+",GMLLCINI"+",GMLLCFIN"+",GMMAGINI"+",GMMAGFIN"+",GMPIAINI"+",GMPIAFIN"+",GMFAMINI"+",GMFAMFIN"+",GMINICLI"+",GMFINCLI"+",GMDISMAG"+",GMGIANEG"+",GMORDMPS"+",GMSTAORD"+",GMELACAT"+",GMINIELA"+",GMFINELA"+",GMTIPDOC"+",GMMODELA"+",GMCHKDAT"+",GMMSGRIP"+",GMGENPDA"+",GMCRIFOR"+",GMFLULEL"+",GMNOTEEL"+",GMGENODA"+",GMELABID"+",GMMRPLOG"+",GMORIODL"+",GMMARINI"+",GMMARFIN"+",GMSTIPART"+",GMPROFIN"+",GMSEMLAV"+",GMMATPRI"+",GMCOMINI"+",GMCOMFIN"+",GMCOMODL"+",GMNUMINI"+",GMNUMFIN"+",GMSERIE1"+",GMSERIE2"+",GMDOCINI"+",GMDOCFIN"+",GMMICOMP"+",GMMRFODL"+",GMMRFOCL"+",GMMRFODA"+",GMMAGFOR"+",GMMAGFOC"+",GMMAGFOL"+",GMMAGFOA"+",GMCRIELA"+",GMORDPRD"+",GMIMPPRD"+",GMSELIMP"+",GMPERPIA"+",GMPIAPUN"+",GMDELORD"+",GMLISMAG"+",GUELPDAF"+",GUELPDAS"+",GUFLAROB"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'SEL__MRP','GMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'SEL__MRP','GMDATELA');
      +","+cp_NullLink(cp_ToStrODBC(time()),'SEL__MRP','GMORAELA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FilePath),'SEL__MRP','GMPATHEL');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'SEL__MRP','GMUTEELA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODINI),'SEL__MRP','GMCODINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODFIN),'SEL__MRP','GMCODFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMAINI),'SEL__MRP','GMFAMAIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMAFIN),'SEL__MRP','GMFAMAFI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GRUINI),'SEL__MRP','GMGRUINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GRUFIN),'SEL__MRP','GMGRUFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATINI),'SEL__MRP','GMCATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATFIN),'SEL__MRP','GMCATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LLCINI),'SEL__MRP','GMLLCINI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LLCFIN),'SEL__MRP','GMLLCFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGINI),'SEL__MRP','GMMAGINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGFIN),'SEL__MRP','GMMAGFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PIAINI),'SEL__MRP','GMPIAINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PIAFIN),'SEL__MRP','GMPIAFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMINI),'SEL__MRP','GMFAMINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMFIN),'SEL__MRP','GMFAMFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INICLI),'SEL__MRP','GMINICLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FINCLI),'SEL__MRP','GMFINCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DISMAG),'SEL__MRP','GMDISMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GIANEG),'SEL__MRP','GMGIANEG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDMPS),'SEL__MRP','GMORDMPS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STAORD),'SEL__MRP','GMSTAORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ELACAT),'SEL__MRP','GMELACAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INIELA),'SEL__MRP','GMINIELA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FINELA),'SEL__MRP','GMFINELA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOCM),'SEL__MRP','GMTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MODELA),'SEL__MRP','GMMODELA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CHECKDATI),'SEL__MRP','GMCHKDAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MESSRIPIA),'SEL__MRP','GMMSGRIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GENPODA),'SEL__MRP','GMGENPDA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRITFORN),'SEL__MRP','GMCRIFOR');
      +","+cp_NullLink(cp_ToStrODBC("S"),'SEL__MRP','GMFLULEL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTMRP),'SEL__MRP','GMNOTEEL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GENODA),'SEL__MRP','GMGENODA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ELABID),'SEL__MRP','GMELABID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRPLOG),'SEL__MRP','GMMRPLOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORIODL),'SEL__MRP','GMORIODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MARINI),'SEL__MRP','GMMARINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MARFIN),'SEL__MRP','GMMARFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STIPART),'SEL__MRP','GMSTIPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROFIN),'SEL__MRP','GMPROFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEMLAV),'SEL__MRP','GMSEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATPRI),'SEL__MRP','GMMATPRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMINI),'SEL__MRP','GMCOMINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMFIN),'SEL__MRP','GMCOMFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMODL),'SEL__MRP','GMCOMODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMINI),'SEL__MRP','GMNUMINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMFIN),'SEL__MRP','GMNUMFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERIE1),'SEL__MRP','GMSERIE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERIE2),'SEL__MRP','GMSERIE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DOCINI),'SEL__MRP','GMDOCINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DOCFIN),'SEL__MRP','GMDOCFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MICOM),'SEL__MRP','GMMICOMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MIODL),'SEL__MRP','GMMRFODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MIOCL),'SEL__MRP','GMMRFOCL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MIODA),'SEL__MRP','GMMRFODA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGFOR),'SEL__MRP','GMMAGFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGFOC),'SEL__MRP','GMMAGFOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGFOL),'SEL__MRP','GMMAGFOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGFOA),'SEL__MRP','GMMAGFOA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRIELA),'SEL__MRP','GMCRIELA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDIPROD),'SEL__MRP','GMORDPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPEPROD),'SEL__MRP','GMIMPPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SELEIMPE),'SEL__MRP','GMSELIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERPIA),'SEL__MRP','GMPERPIA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PIAPUN),'SEL__MRP','GMPIAPUN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DELORD),'SEL__MRP','GMDELORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LISMAG),'SEL__MRP','GMLISMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ELAPDF),'SEL__MRP','GUELPDAF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ELAPDS),'SEL__MRP','GUELPDAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PPFLAROB),'SEL__MRP','GUFLAROB');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GMSERIAL',this.w_SERIALE,'GMDATELA',i_datsys,'GMORAELA',time(),'GMPATHEL',this.w_FilePath,'GMUTEELA',i_codute,'GMCODINI',this.w_CODINI,'GMCODFIN',this.w_CODFIN,'GMFAMAIN',this.w_FAMAINI,'GMFAMAFI',this.w_FAMAFIN,'GMGRUINI',this.w_GRUINI,'GMGRUFIN',this.w_GRUFIN,'GMCATINI',this.w_CATINI)
      insert into (i_cTable) (GMSERIAL,GMDATELA,GMORAELA,GMPATHEL,GMUTEELA,GMCODINI,GMCODFIN,GMFAMAIN,GMFAMAFI,GMGRUINI,GMGRUFIN,GMCATINI,GMCATFIN,GMLLCINI,GMLLCFIN,GMMAGINI,GMMAGFIN,GMPIAINI,GMPIAFIN,GMFAMINI,GMFAMFIN,GMINICLI,GMFINCLI,GMDISMAG,GMGIANEG,GMORDMPS,GMSTAORD,GMELACAT,GMINIELA,GMFINELA,GMTIPDOC,GMMODELA,GMCHKDAT,GMMSGRIP,GMGENPDA,GMCRIFOR,GMFLULEL,GMNOTEEL,GMGENODA,GMELABID,GMMRPLOG,GMORIODL,GMMARINI,GMMARFIN,GMSTIPART,GMPROFIN,GMSEMLAV,GMMATPRI,GMCOMINI,GMCOMFIN,GMCOMODL,GMNUMINI,GMNUMFIN,GMSERIE1,GMSERIE2,GMDOCINI,GMDOCFIN,GMMICOMP,GMMRFODL,GMMRFOCL,GMMRFODA,GMMAGFOR,GMMAGFOC,GMMAGFOL,GMMAGFOA,GMCRIELA,GMORDPRD,GMIMPPRD,GMSELIMP,GMPERPIA,GMPIAPUN,GMDELORD,GMLISMAG,GUELPDAF,GUELPDAS,GUFLAROB &i_ccchkf. );
         values (;
           this.w_SERIALE;
           ,i_datsys;
           ,time();
           ,this.w_FilePath;
           ,i_codute;
           ,this.w_CODINI;
           ,this.w_CODFIN;
           ,this.w_FAMAINI;
           ,this.w_FAMAFIN;
           ,this.w_GRUINI;
           ,this.w_GRUFIN;
           ,this.w_CATINI;
           ,this.w_CATFIN;
           ,this.oParentObject.w_LLCINI;
           ,this.oParentObject.w_LLCFIN;
           ,this.w_MAGINI;
           ,this.w_MAGFIN;
           ,this.w_PIAINI;
           ,this.w_PIAFIN;
           ,this.w_FAMINI;
           ,this.w_FAMFIN;
           ,this.w_INICLI;
           ,this.w_FINCLI;
           ,this.w_DISMAG;
           ,this.w_GIANEG;
           ,this.w_ORDMPS;
           ,this.w_STAORD;
           ,this.w_ELACAT;
           ,this.w_INIELA;
           ,this.w_FINELA;
           ,this.w_TIPDOCM;
           ,this.oParentObject.w_MODELA;
           ,this.oParentObject.w_CHECKDATI;
           ,this.oParentObject.w_MESSRIPIA;
           ,this.w_GENPODA;
           ,this.oParentObject.w_CRITFORN;
           ,"S";
           ,this.w_NOTMRP;
           ,this.w_GENODA;
           ,this.w_ELABID;
           ,this.w_MRPLOG;
           ,this.w_ORIODL;
           ,this.w_MARINI;
           ,this.w_MARFIN;
           ,this.w_STIPART;
           ,this.w_PROFIN;
           ,this.w_SEMLAV;
           ,this.w_MATPRI;
           ,this.w_COMINI;
           ,this.w_COMFIN;
           ,this.w_COMODL;
           ,this.w_NUMINI;
           ,this.w_NUMFIN;
           ,this.w_SERIE1;
           ,this.w_SERIE2;
           ,this.w_DOCINI;
           ,this.w_DOCFIN;
           ,this.w_MICOM;
           ,this.w_MIODL;
           ,this.w_MIOCL;
           ,this.w_MIODA;
           ,this.w_MAGFOR;
           ,this.w_MAGFOC;
           ,this.w_MAGFOL;
           ,this.w_MAGFOA;
           ,this.w_CRIELA;
           ,this.w_ORDIPROD;
           ,this.w_IMPEPROD;
           ,this.w_SELEIMPE;
           ,this.w_PERPIA;
           ,this.w_PIAPUN;
           ,this.w_DELORD;
           ,this.w_LISMAG;
           ,this.w_ELAPDF;
           ,this.w_ELAPDS;
           ,this.w_PPFLAROB;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Explode
    this.w_CodSaldo1 = Left(this.w_CodSaldo, this.w_LenCodSal)
    this.w_PROGPH = 0
    this.w_PROFAN = nvl(profan,space(240))
    if ! this.w_PIAODP
      if type("artipart")="C" and artipart $ "PH-DC-PS"
        this.w_Padre_er = padrered
        this.w_TipoPadre = artipart
      else
        this.w_Padre_er = cododl
        this.w_TipoPadre = "**"
      endif
      if arflcomm="S"
        this.w_CODCOM = nvl(codcom,space(15))
      else
        this.w_CODCOM = nvl(codcom,space(15))
      endif
      this.w_CODATT = nvl(codatt,space(15))
    else
      * --- Devo pianificare gli ODP
      this.w_Padre_er = odp.cododl
      this.w_TipoPadre = odp.artipart
      this.w_CODCOM = nvl(odp.codcom,space(15))
      this.w_CODATT = nvl(odp.codatt,space(15))
    endif
    if this.oParentObject.w_MODELA="R" or this.w_Tiprec="4ODL-D"
      * --- Esplode l'ordine e impegna i componenti
      if not empty(this.w_MatAltern) and g_PRFA="S" and g_CICLILAV="S"
        select * from Componen into cursor compon where dbkeysal=this.w_CodSaldo1 and coattges<=datini ; 
 and datini<=codisges and artipges<>"C" and (coidegru<>"AF" or codric=this.w_MatAltern) order by cproword
      else
        select * from Componen into cursor compon where dbkeysal=this.w_CodSaldo1 and coattges<=datini ; 
 and datini<=codisges and artipges<>"C" and (coidegru<>"AF" or coflpref="S") order by cproword
      endif
      Select Elabor
      if RecCount("Compon")=0
        * --- Non esiste una distinta valida: elimina l'ODL
        delete
        Insert into LogErrori (Codric, datini, Messagg) values (m.codric, m.datini, ah_msgformat("Distinta base non definita"))
        this.w_SaldProg = 0
        if (type("artipart")="C" and artipart $ "PH-DC-PS") Or (arflcomm="S" and not empty(nvl(codcom,"")))
          this.w_nocomp = .f.
        else
          this.w_nocomp = .t.
        endif
      else
        if this.w_UDPNAME
          * --- Aggiorna il codice di ricerca
          Replace codric With compon.dbcodice
        endif
        if ! this.w_PIAODP
          do case
            case (m.arpropre="I" and this.w_MIODL = "M") or (m.arpropre="L" and this.w_MIOCL = "M")
              * --- Magazzino preferenziale
              this.w_MAGRIF = m.armagpre
            case (m.arpropre="I" and this.w_MIODL = "C") or (m.arpropre="L" and this.w_MIOCL = "C")
              * --- Criterio di pianificazione
              do case
                case this.w_CRIELA = "G"
                  this.w_MAGRIF = m.grumag
                case this.w_CRIELA = "M"
                  this.w_MAGRIF = m.codmag
                otherwise
                  this.w_MAGRIF = m.armagpre
              endcase
            otherwise
              * --- Forza Magazzino
              this.w_MAGRIF = IIF(m.arpropre="I" , this.w_MAGFOC, this.w_MAGFOL)
          endcase
          if this.w_PPMRPDIV="S"
            * --- Tipologtia impegno [T -> Testata ordine F -> Forza Magazzino C -> Da Componente]
            this.w_TipImpe = nvl(Elabor_bis.artipimp, "T")
            * --- Magazzino di impegno Articolo
            this.w_MagImp = nvl(Elabor_bis.armagimp , space(5))
            this.w_DisMgImp = nvl(Elabor_bis.dismgimp , "N")
          else
            * --- Tipologtia impegno [T -> Testata ordine F -> Forza Magazzino C -> Da Componente]
            this.w_TipImpe = nvl(artipimp, "T")
            * --- Magazzino di impegno Articolo
            this.w_MagImp = nvl(armagimp , space(5))
            this.w_DisMgImp = nvl(dismgimp , "N")
          endif
        else
          * --- Tipologtia impegno [T -> Testata ordine F -> Forza Magazzino C -> Da Componente]
          this.w_TipImpe = nvl(odp.artipimp, "T")
          * --- Magazzino di impegno Articolo
          this.w_MagImp = nvl(odp.armagimp , space(5))
          this.w_DisMgImp = nvl(odp.dismgimp , "N")
        endif
      endif
      select compon
    else
      * --- Segna da elaborare gli articoli appartenenti alla lista dei materiali
      select codsal from elabor where padrered=this.w_Padre_er and tiprec="7OLIM" into cursor compon
    endif
    tiprec="7MRPIM"
    scan
    if not empty(this.w_PROFAN) or artipart="PH" or tipartor="PH"
      this.w_PROGPH = this.w_PROGPH+1
    endif
    scatter memvar
    if this.oParentObject.w_MODELA="R" or this.w_Tiprec="4ODL-D"
      codart=cocodart
      codsal=m.codsal1
      cprownum=iif(this.w_TipoPadre $ "PH-DC-PS", this.w_RifMate, m.cprownum)
      padrered=this.w_Padre_er
      if .F.
        mgmagimp=this.w_MagImp
        dismgmag=this.w_DisMgImp
      endif
      if this.w_PROVPAD="L" and artipart $ "PH-DC-PS"
        * --- Eredita contratto e terzista dal padre
        if this.w_UDPNAME
          codsal1 = this.w_CONTSAL
          prcodfor = this.w_FORNIOLD && elabor.prcodfor
          contra = this.w_CONTRAOLD && elabor.contra
          arpropre = "L"
        else
          codsal1 = dbkeysal
          prcodfor = odp.olcofor
          contra = odp.contra
          arpropre = "L"
        endif
      endif
      * --- Correzione del Lead-Time sul legame
      this.w_Leadt = cocorlea
      if this.w_TipoPadre $ "PH-DC-PS"
        * --- Aggiorna il coefficiente per gli articoli fantasma (aggiunge quello del padre)
        if this.w_PPMRPDIV="S"
          cocorlea=cocorlea+elabor_bis.cocorlea
        else
          cocorlea=cocorlea+elabor.cocorlea
        endif
      endif
      this.w_Datimp = this.w_Datini
      if this.w_Leadt<>0
        this.DatInput = dtos(this.w_Datini)
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- La data dell'impegno non deve essere successiva alla data di richiesta
        this.w_Datimp = iif(this.DatRisul<this.w_Dateva and not empty(this.DatRisul), this.DatRisul, this.w_Dateva)
        select compon
      endif
      datini=this.w_Datimp
      dateva=this.w_Datimp
      datcon=this.w_Datimp
      datidx=dtoc(cp_todate(m.datini),1)
      do case
        case m.prperpia="W"
          * --- Inizio Settimana
          this.w_Datimp = ttod(this.w_Datimp) - dow(ttod(this.w_Datimp), 2) + 1
          m.periodo = alltrim(str(year(this.w_Datimp))) + "-" + m.prperpia + padl(week(this.w_Datimp, 1, 2), 2, "0")
        case m.prperpia="M"
          * --- Inizio Mese
          this.w_Datimp = ttod(this.w_Datimp) - day(ttod(this.w_Datimp)) + 1
          m.periodo = alltrim(str(year(this.w_Datimp))) + "-" + m.prperpia + padl(month(this.w_Datimp), 2, "0")
        case m.prperpia="Q"
          * --- Inizio Trimestre
          this.w_Datimp = date(year(this.w_Datimp), 3 * quarter(this.w_Datimp) - 2 ,1)
          m.periodo = alltrim(str(year(this.w_Datimp))) + "-" + m.prperpia + padl(ceiling(month(this.w_Datimp)*4/12), 2, "0")
        case m.prperpia="F"
          * --- Inizio Quadrimestre
          this.w_Datimp = date(year(this.w_Datimp), 4 * ceiling(month(this.w_Datimp)*3/12) - 3 ,1)
          m.periodo = alltrim(str(year(this.w_Datimp))) + "-" + m.prperpia + padl(ceiling(month(this.w_Datimp)*3/12), 2, "0")
        case m.prperpia="S"
          * --- Inizio Semestre
          this.w_Datimp = date(year(this.w_Datimp), 6 * ceiling(month(this.w_Datimp)*2/12) - 5 ,1)
          m.periodo = alltrim(str(year(this.w_Datimp))) + "-" + m.prperpia + padl(ceiling(month(this.w_Datimp)*2/12), 2, "0")
        otherwise
          * --- Giornaliero (tutto come prima))
          *     L'MRP lavora gi� per data
      endcase
      m.datidx = dtos(this.w_Datimp)
      m.datper = m.datidx
      codcom=iif(arflcomm="S",this.w_CODCOM,space(15)) 
 codatt=iif(arflcomm="S",this.w_CODATT,space(15)) 
 impcom=0
      dismgpre=iif(empty(nvl(m.armagpre,"")), this.w_DISMGPRO, m.dismgpre )
      this.w_DisMgCur = dismgpre
      armagpre=iif(empty(nvl(m.armagpre,"")), this.w_PPMAGPRO, m.armagpre)
      this.w_MgImpCur = armagpre
      dismgmag="S" && this.w_DisMgImp
      do case
        case this.w_MICOM="M"
          codmag = this.w_MgImpCur
          mgdismag = this.w_DisMgCur
        case this.w_MICOM="A"
          do case
            case this.w_TipImpe = "C"
              * --- Magazzino preferenziale articolo (componente)
              codmag = this.w_MgImpCur
              mgdismag = this.w_DisMgCur
            case this.w_TipImpe = "T"
              * --- Magazzino preferenziale articolo (componente)
              codmag=this.w_MAGRIF
              mgdismag="S"
            case this.w_TipImpe = "F"
              * --- Magazzino impegno componenti dell'anagrafica dell'articolo padre
              codmag=this.w_MagImp
              mgdismag=this.w_DisMgImp
          endcase
        case this.w_MICOM="T"
          if ! this.w_PIAODP
            codmag=this.w_MAGRIF
          else
            codmag=nvl(odp.codmag , space(5))
          endif
          mgdismag="S"
        case this.w_MICOM="F"
          codmag = this.w_MagFor
          mgdismag = this.w_DISMAGFO
          dismgmag = this.w_DISMAGFO
      endcase
      this.w_DisMgCur = mgdismag
      * --- Determino il magazzino di impegno in base al criterio di elaborazione
      this.w_MgImpCur = iif(this.w_CRIELA<>"A", m.codmag , space(0))
      if !Empty(this.w_MgImpCur)
        this.w_FOUND = NOT EMPTY(this.w_MgImpCur) and AT("|"+ALLTRIM(this.w_MgImpCur)+"|",this.w_GRUPPO)>0
      else
        this.w_FOUND = .T.
      endif
      if this.w_CRIELA="G"
        this.w_GrImpCur = iif(this.w_CRIELA<>"A", m.codmag , space(0))
      endif
      m.mgmagimp = m.codmag
      codsal = m.codart + this.w_MgImpCur
      keyidx=right("00"+alltrim(str(m.prlowlev,3,0)),3) + m.codsal + iif(nvl(arflcom1,"N")="C",m.datper + m.codcom + m.codatt,m.codcom + m.codatt + m.datper) + "7"
       quanti=cp_round(-this.w_QTA*m.cocoeum1, 6) 
 quantv=cp_round(-this.w_QTA*m.cocoeimp, 6)
       quanfi=m.quanti 
 quanfv=m.quantv
      * --- Valuto se elaborare la riga verifico se sto impegnando un magazzino non nettificabile oppre che non fa parte delle selezioni, segno la riga da non elaborare
      elabora = this.w_IMPEPROD="S" and this.w_FOUND and this.w_DisMgCur="S"
      evaaut="N" 
 coidegru=iif(m.coidegru="AF" or m.coopelog<>"AP", "  ", m.coidegru) 
 coflpref=iif(empty(m.coidegru),"S",m.coflpref)
      if .f.
        coclapre=nvl(m.coclapre,"") 
 coclarif=nvl(m.coclarif,"")
      endif
      if not empty(this.w_PROFAN)
        profan=(alltrim(this.w_PROFAN)+"."+right("00"+alltrim(str(this.w_PROGPH)),3))
      else
        if artipart="PH"
          profan=right("00"+alltrim(str(this.w_PROGPH)),3)
        else
          profan=space(240)
        endif
      endif
      if (this.oParentObject.w_INTERN or this.w_PARAM="TGSMR_KMM") and this.oParentObject.w_MODELA="R" and this.w_SELEZART="S"
        * --- Quando esplodo Controllo che il figlio non sia all'interno delle selezioni
        *     Se non c'� metto il flag elabora a False
        if !seek(m.codsal, "Catena") and not artipart $ "PH-DC-PS" and this.w_ELACAT="N"
          elabora=.f.
        endif
      else
        if this.oParentObject.w_INTERN and this.oParentObject.w_MODELA="R" and arpropre="E" and artipges="F" and this.w_GENPODA<>"O"
          elabora=.f.
        endif
      endif
      if m.coflpref<>"S"
        * --- Azzera l'impegno dei componenti non preferenziali
        quanti=0 
 quantv=0
      endif
      if this.w_TipoPadre $ "PH-DC-PS"
        * --- Aggiorna il coefficiente per gli articoli fantasma
        cocoeum1 = m.cocoeum1*this.w_COEFFA1 
 cocoeimp = m.cocoeimp*this.w_COEFFA1
      endif
      if this.w_PROVPAD="I" and not(artipart $ "PH-DC-PS")
        if this.w_TipoContra>0 and used("Materzis") and not(artipart $ "PH-DC-PS") and clriffas<>0
          select Materzis 
 locate for mccodice=codric
          if Found()
            evaaut="S" 
 quanti=0 
 quantv=0
          endif
        endif
      else
        if this.w_TipoContra>0 and used("Materzis") and not(artipart $ "PH-DC-PS")
          select Materzis 
 locate for mccodice=codric
          if Found()
            evaaut="S" 
 quanti=0 
 quantv=0
          endif
        endif
      endif
      * --- Determino il magazzino e le politiche di lottizzazione relative
      if this.w_CRIELA<>"A" and this.w_NRECPARRIMA >0
        SELECT("Par_rima")
        if Seek(m.codsal)
          scatter memvar
        endif
      endif
      Insert into Elabor from memvar
      if this.w_PPMRPDIV="S"
        Insert into Elabor_bis from memvar
      endif
    endif
    if this.oParentObject.w_MODELA="N"
      * --- Calcola lo storno dell'ordinato degli ODL da cancellare
      Select codsal,codmag, sum(quanti) as Ordi, min(keyidx) as keyidx, artipart from Elabor ; 
 where codsal=m.codsal and tiprec="4ODL-M" group by codsal,codmag into cursor Ordi
      if RecCount()>0
        Select Ordi 
 scan
        this.w_ChiaveSal = left(keyidx,this.w_RootKeyIdx) + space(30) + "19000101" + "0"
        Select Elabor 
 seek(this.w_ChiaveSal) 
 replace quanti with quanti - ordi.ordi 
 endscan
        Select codsal,codmag,ordi,artipart from Ordi into array Ordi 
 Select OrdiODL 
 Append from array Ordi 
 release Ordi 
 use in Ordi
        Declare COrdi[5]
        COrdi[1]="" 
 COrdi[2]="" 
 COrdi[3]="" 
 COrdi[4]=0 
 COrdi[5]=""
        Select codsal,codmag,codcom, sum(quanti) as Ordi, artipart from Elabor ; 
 where codsal=m.codsal and tiprec="4ODL-M" and not empty(nvl(codcom,space(15))) ; 
 group by codsal,codmag,codcom into array COrdi
         Select COrdiODL 
 Append from array COrdi 
 release COrdi
        * --- Memorizza il codice degli ODL da cancellare
        Select cododl from Elabor where codsal=m.codsal and tiprec="4ODL-M" into cursor Odel 
 Select * from Odel into array Odel 
 Select OLSug2del 
 Append from array Odel 
 release Odel
        * --- Elimina gli ODL suggeriti dal cursore
        Delete from Elabor where codsal=m.codsal and tiprec="4ODL-M"
        if this.w_PPMRPDIV="S"
          Delete from Elabor_bis where codsal=m.codsal and tiprec="4ODL-M"
        endif
        if g_COLA="S" AND g_PRFA="S" and g_CICLILAV="S"
          * --- Calcola lo storno dell'ordinato degli OCL di fase da cancellare
          Select codsal,codmag, sum(quanti), artipart from Elabor where tiprec="4ODL-M" ; 
 and oltseodl in (Select cododl from Odel) group by codsal,codmag into array Ordi
          if type("Ordi") <> "U"
            Select OrdiODL
            Append from array Ordi
            release Ordi
            * --- Memorizza il codice degli OCL da cancellare
            Select distinct cododl from Elabor where oltseodl in (Select cododl from Odel) into array Odel
            Select OLSug2del
            Append from array Odel
            Select * from Odel into cursor Odel READWRITE
            Append from array Odel
            release Odel
            * --- Elimina gli OCL suggeriti dal cursore
            Delete from Elabor where oltseodl in (Select cododl from Odel) and tiprec="4ODL-M"
            if this.w_PPMRPDIV="S"
              Delete from Elabor_bis where oltseodl in (Select cododl from Odel) and tiprec="4ODL-M"
            endif
          endif
          Declare COrdi[5]
          COrdi[1]="" 
 COrdi[2]="" 
 COrdi[3]="" 
 COrdi[4]=0 
 COrdi[5]=""
          Select codsal,codmag,codcom, sum(quanti) as Ordi, artipart from Elabor ; 
 where tiprec="4ODL-M" and oltseodl in (Select cododl from Odel) and not empty(nvl(codcom,space(15))) ; 
 group by codsal,codmag,codcom into array COrdi
          if type("Ordi") <> "U"
             Select COrdiODL 
 Append from array COrdi 
 release COrdi
          endif
        endif
        * --- Calcola lo storno dell'impegnato degli ODL da cancellare
        Select codsal,codmag, -sum(quanti) as Impe, min(keyidx) as keyidx, artipart from Elabor ; 
 where cododl in (select cododl from Odel) and tiprec="7OLIMM" group by codsal,codmag ; 
 into cursor Impe
        Select Impe
        scan
        this.w_ChiaveSal = left(keyidx,this.w_RootKeyIdx) + space(30) + "19000101" + "0"
        Select Elabor
        seek(this.w_ChiaveSal)
        replace quanti with quanti + impe.impe
        this.w_ChiaveSal = substr(this.w_ChiaveSal, 4, 40)
        replace elabora with .t. for codsal=this.w_ChiaveSal rest
        endscan
        Declare Impe[5]
        Impe[1]="" 
 Impe[2]="" 
 Impe[3]="" 
 Impe[4]=0 
 Impe[5]=""
        Select codsal,codmag,impe,artipart from Impe into array Impe
        Select ImpeODL
        Append from array Impe
        release Impe
        use in select("Impe")
        Declare CImpe[5]
        CImpe[1]="" 
 CImpe[2]="" 
 CImpe[3]="" 
 CImpe[4]=0 
 CImpe[5]=""
        Select codsal,codmag,codcom, -sum(quanti) as Impe, artipart from Elabor ; 
 where cododl in (select cododl from Odel) and tiprec="7OLIMM" and not empty(nvl(codcom,space(15))) ; 
 group by codsal,codmag,codcom ; 
 into array CImpe
        Select CImpeODL
        Append from array CImpe
        release CImpe
        * --- Elimina gli impegli degli ODL suggeriti dal cursore
        Delete from Elabor where cododl in (select cododl from Odel) and tiprec="7OLIMM"
        if this.w_PPMRPDIV="S"
          Delete from Elabor_bis where cododl in (select cododl from Odel) and tiprec="7OLIMM"
        endif
        use in select("Odel")
      endif
      * --- Segna da elaborare tutti gli articoli
      Update Elabor set elabora=.t. where codsal=m.codsal
    endif
    endscan
    USE IN SELECT("compon")
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GenerazioneODL
    this.w_RecNumb = recno()
    this.w_TipoContra = 0
    this.w_CODCON = SPACE(15)
    this.w_PROVPAD = arpropre
    this.w_nocomp = .f.
    do case
      case artipges="F" and (arpropre="I" or arpropre="L" and g_COLA="S")
        * --- Articolo di produzione Interna o C/Lavoro e Gestione a Fabbisogno
        this.w_Fabnet = this.w_QTA
        this.w_QTAIMP = this.w_QTA
        this.w_CONTSAL = Left(this.w_CodSaldo, this.w_LenCodSal)
        this.w_PREZUM = nvl(ARPREZUM,"N")
        this.w_CENCOS = iif(empty(nvl(arcencos," ")),this.w_PPCENCOS,arcencos)
        if arpropre = "L"
          this.w_UNMIS1 = elabor.arunmis1
          this.w_UNMIS2 = elabor.arunmis2
          this.w_OPERAT = elabor.aroperat
          this.w_MOLTIP = elabor.armoltip
          * --- Articolo di Provenienza Conto Lavoro
          this.w_CONTRAOLD = SPACE(15)
          this.w_FORNIOLD = SPACE(15)
          if artipart $ "PH-DC-PS"
            this.w_CONTSAL = codsal1
            if g_GESCON="S"
              * --- Verifica i materiali forniti dal terzista
              this.w_CONTART = codart
              this.w_CONTVAR = right(codsal1,20)
              this.w_CONTVAR = iif(this.w_CONTVAR = repl("#",20), space(20), this.w_CONTVAR)
              Select Contratti
              do case
                case this.w_PREZUM $ "SN"
                  * --- Considero solo i contratti con UnitÃ  di Misura Contratto=U.M. principale articolo
                  * --- Cerca il contratto per articolo
                  Locate for m.dateva >= codatini and m.dateva <= codatfin and conumero=contra and cocodart=this.w_CONTART and counimis=this.w_unmis1
                  this.w_TipoContra = 2
                otherwise
                  * --- Cerca il contratto per articolo
                  Locate for m.dateva >= codatini and m.dateva <= codatfin and conumero=contra and cocodart=this.w_CONTART
                  this.w_TipoContra = 2
              endcase
              if vartype(g_ATTIVAMRPLOG)="L" and g_ATTIVAMRPLOG
                w_Handle = fopen(w_NOME_FILE_LOG, 2) 
 = FSEEK(w_Handle,0,2) 
                w_record="pagina 3 locate contratti "+str(seconds()-INIZ03,6,3)+" sec." 
 INIZ03=seconds()
                w_t = fputs(w_Handle, w_Record)
                fclose(w_Handle)
              endif
              if Found()
                if this.w_TipoContra=2
                  Select mccodice from Contratti into cursor Materzis ; 
 where conumero=Elabor.contra and cocodart=this.w_CONTART
                endif
                this.w_TipoContra = iif(RecCount("Materzis")=0, 0, this.w_TipoContra)
              else
                this.w_TipoContra = 0
              endif
              if vartype(g_ATTIVAMRPLOG)="L" and g_ATTIVAMRPLOG
                w_Handle = fopen(w_NOME_FILE_LOG, 2) 
 = FSEEK(w_Handle,0,2) 
                w_record="pagina 3 join con elabor "+str(seconds()-INIZ03,6,3)+" sec." 
 INIZ03=seconds()
                w_t = fputs(w_Handle, w_Record)
                fclose(w_Handle)
              endif
            endif
            Select Elabor
          else
            if empty(nvl(prcodfor,"")) or empty(nvl(anmagter,""))
              * --- Segnalazione OCL senza Fornitore
              Replace prcodfor with space(15) 
 prcodfor=space(15) 
 Insert into LogErrori (Codric, Messagg) values (m.codric, ah_Msgformat("Fornitore per C/Lavoro non impostato") )
            else
              if g_GESCON="S"
                * --- Imposta le politiche di lottizzazione e il LT dal contratto
                do case
                  case this.w_PREZUM $ "SN"
                    * --- Sempre U.M Principale
                    Select Contratti
                    * --- Cerca il contratto per articolo
                    Locate for m.dateva >= codatini and m.dateva <= codatfin and cocodclf=prcodfor and cocodart=codart and counimis=this.w_unmis1
                    this.w_TipoContra = 2
                    if not Found()
                      * --- Cerca il contratto per gruppo merceologico
                      Locate for m.dateva >= codatini and m.dateva <= codatfin and cocodclf=prcodfor and cogrumer=argrumer and counimis=this.w_unmis1
                      this.w_TipoContra = 3
                    endif
                  otherwise
                    * --- U.M Converti
                    Select Contratti
                    * --- Cerca il contratto per articolo
                    Locate for m.dateva >= codatini and m.dateva <= codatfin and cocodclf=prcodfor and cocodart=codart
                    this.w_TipoContra = 2
                    if not Found()
                      * --- Cerca il contratto per gruppo merceologico
                      Locate for m.dateva >= codatini and m.dateva <= codatfin and cocodclf=prcodfor and cogrumer=argrumer
                      this.w_TipoContra = 3
                    endif
                endcase
                if Found()
                  Select Elabor 
 Replace contra with Contratti.conumero, prgioapp with Contratti.cogioapp, prlotmed with 0 
 Replace dppolpro with "L", prqtamin with Contratti.coqtamin, prlotrio with Contratti.colotmul 
 contra=Contratti.conumero
                  do case
                    case this.w_TipoContra=2
                      Select mccodice from Contratti into cursor Materzis ; 
 where conumero=Elabor.contra and cocodart=codart
                    case this.w_TipoContra=3
                      Select mccodice from Contratti into cursor Materzis ; 
 where conumero=Elabor.contra and cogrumer=argrumer
                  endcase
                  this.w_TipoContra = iif(RecCount("Materzis")=0, 0, this.w_TipoContra)
                else
                  this.w_TipoContra = 0
                endif
              endif
              Select Elabor
            endif
          endif
        endif
        * --- Politiche di lottizzazione
        Select Elabor
        if arflcomm<>"S" and prgiocop=0
          codcom=space(15)
          codatt=space(15)
          if not artipart $ "PH-DC-PS"
            do case
              case dppolpro = "M" and this.w_Fabnet<prqtamin
                * --- Vincolo quantit� minima
                this.w_Fabnet = prqtamin
              case dppolpro $ "L-X"
                * --- vincolo lotto multiplo
                if this.w_Fabnet<prqtamin
                  this.w_Fabnet = prqtamin
                endif
                if nvl(prlotrio,0)>0
                  this.w_Fabnet = ceiling(this.w_Fabnet/prlotrio)*prlotrio
                endif
            endcase
            this.w_MINRIO = prqtamin
            this.w_QTALOT = prlotrio
          endif
        endif
        this.w_SaldProg = iif(this.w_COMMES, this.w_SaldProg, -this.w_QTAIMP)
        this.w_Fabpar = this.w_Fabnet
        this.w_QTARES = this.w_Fabnet
        if arflcomm="S" or prgiocop <> 0
          this.w_QTAMAX = 0
        else
          this.w_QTAMAX = prqtamax
        endif
        m.datidx = dtos(m.dateva)
        do case
          case m.prperpia="W"
            * --- Inizio Settimana
            m.dateva = ttod(m.dateva) - dow(ttod(m.dateva), 2) + 1
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(week(m.dateva, 1, 2), 2, "0")
          case m.prperpia="M"
            * --- Inizio Mese
            m.dateva = ttod(m.dateva) - day(ttod(m.dateva)) + 1
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(month(m.dateva), 2, "0")
          case m.prperpia="Q"
            * --- Inizio Trimestre
            m.dateva = date(year(m.dateva), 3 * quarter(m.dateva) - 2 ,1)
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(ceiling(month(m.dateva)*4/12), 2, "0")
          case m.prperpia="F"
            * --- Inizio Quadrimestre
            m.dateva = date(year(m.dateva), 4 * ceiling(month(m.dateva)*3/12) - 3 ,1)
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(ceiling(month(m.dateva)*3/12), 2, "0")
          case m.prperpia="S"
            * --- Inizio Semestre
            m.dateva = date(year(m.dateva), 6 * ceiling(month(m.dateva)*2/12) - 5 ,1)
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(ceiling(month(m.dateva)*2/12), 2, "0")
          otherwise
            * --- Giornaliero (tutto come prima))
            *     L'MRP lavora gi� per data
        endcase
        do while this.w_QTARES>0
          * --- Questo ciclo serve a spezzare gli ordini che non rispettano il vincolo di quantitÃ  massima, se il vincolo non c'Ã¨ la procedura fa sempre un solo 'giro'
          * --- Aggiunge l'ordine
          tiprec="8MRP-S"
          codcom=nvl(codcom,space(15))
          codatt=nvl(codatt,space(15))
          cododl=padl(alltrim(str(this.w_ODLprog)), 15)
          padrered=iif(artipart $ "PH.DC.PS", m.padrered, m.cododl)
          prqtamin=prqtamin
          prlotrio=prlotrio
          this.w_ODLprog = this.w_ODLprog + 1
          armagpre=iif(empty(nvl(m.armagpre,"")), this.w_PPMAGPRO, m.armagpre)
          codmag= iif(this.w_CRIELA="A" , m.armagpre, iif(empty(nvl(m.codmag, "")), m.armagpre, m.codmag))
          codsal = m.codart + iif(this.w_CRIELA<>"A", iif(this.w_CRIELA="G", m.grumag, m.codmag) , space(0))
          keyidx=right("00"+alltrim(str(m.prlowlev,3,0)),3) + m.codsal + iif(nvl(arflcom1,"N")="C",m.datidx + m.codcom + m.codatt,m.codcom + m.codatt + m.datidx) + "8"
          m.datidx = dtos(m.dateva)
          m.datper = m.datidx
          Select Elabor
          * --- Lead-Time Sicurezza
          datcon = m.dateva
          this.w_Leadt = prsafelt
          if this.w_Leadt>0 and m.datidx<>"19000101"
            * --- DatInput=w_Datidx
            this.DatInput = dtos(m.dateva)
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            dateva = iif(m.datcon<this.DatRisul or empty(this.DatRisul), m.datcon, this.DatRisul)
          endif
          * --- Lead-Time MPS
          this.w_Leadt = prleamps
          if this.w_Leadt>0 and m.datidx<>"19000101"
            this.DatInput = dtos(m.dateva)
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            datmps = iif(m.dateva<this.DatRisul or empty(this.DatRisul), m.dateva, this.DatRisul)
            mpserr=this.w_DatErr
          else
            datmps = m.dateva
          endif
          * --- Lead-Time Produzione
          Select Elabor
          this.w_Leadt = cp_ROUND(prgioapp + iif(prlotmed=0, 0, prgioapp*dpcoeflt*((this.w_Fabnet/prlotmed) - 1)) + .499, 0)
          if this.w_Leadt>0 and m.datidx<>"19000101"
            this.DatInput = dtos(m.dateva)
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            datini = iif(m.dateva<this.DatRisul or empty(this.DatRisul), m.dateva, this.DatRisul)
            ltperr=this.w_DatErr
          else
            datini = m.dateva
          endif
          Select Elabor 
 leapro=this.w_Leadt 
 qtalor=this.w_Fabnet 
 * --- armagpre=iif(empty(nvl(m.armagpre,"")), this.w_PPMAGPRO, m.armagpre) 
 * --- codmag=m.armagpre 
 
 * codmag=m.armagpre 
 mgdismag= m.dismgpre 
 
 prcodfor=iif(m.arpropre="I", space(10), m.prcodfor)
          if this.w_QTAMAX<>0 and this.w_QTARES>this.w_QTAMAX
            * --- Applico politica di lottizzazione quantit� massima
            this.w_Fabpar = this.w_QTAMAX
            * --- Fabbisogno residuo (da valutare al prossimo giro)
            this.w_QTARES = this.w_QTARES-this.w_Fabpar
          else
            if this.w_QTAMAX=0
              * --- Se la quantit� massima non � gestita faccio un solo giro nello while
              this.w_QTARES = 0
            else
              * --- Se sono all'ultimo giro tengo l'avanzo
              this.w_Fabpar = this.w_QTARES
              this.w_QTARES = 0
            endif
          endif
          if this.w_ImpComm>0
            this.w_ImpComm = cp_round(this.w_QTA * this.w_COSTO, g_PERPVL)
            Select Commesse 
 locate for cncodcan=m.codcom
            if found()
              if cncodval<>g_PERVAL
                this.w_ImpComm = VAL2CAM(this.w_ImpComm,g_PERVAL,cncodval,vacaoval,dateva,vadectot)
              endif
            else
              this.w_ImpComm = 0
            endif
          else
            this.w_ImpComm = 0
          endif
          Select Elabor 
 quanti=this.w_Fabpar 
 quanfi=this.w_Fabpar 
 qtalor=this.w_Fabpar 
 qtanet=this.w_QTAIMP 
 impcom=this.w_ImpComm 
 arcencos=this.w_CENCOS 
 append blank 
 gather memvar
          if this.w_PPMRPDIV="S"
            Select Elabor_bis 
 append blank 
 gather memvar 
 Select Elabor
          endif
          contra=space(15)
          this.w_CONTRAOLD = contra
          this.w_FORNIOLD = prcodfor
          this.w_SaldProg = iif(this.w_COMMES, this.w_SaldProg, this.w_SaldProg+this.w_Fabpar)
          this.w_ImpComm = 0
          * --- impegna i componenti
          this.w_QTA = this.w_Fabpar
          this.w_Datini = datini
          this.w_Dateva = dateva
          this.w_RifMate = cprownum
          this.w_nocomp = .f.
          * --- --------------------------------------------------------------------------------------------------------
          * --- --------------------------------------------------------------------------------------------------------
          * --- --------------------------------------------------------------------------------------------------------
          * --- Aggiunta per verificare se deve elaborare gli impegni derivanti da ODL ma che sono input di fase esterna
          if arpropre<>"L"
            this.w_TipoContra = 0
            USE IN SELECT("MATERZIS")
          endif
          if g_COLA="S" and g_PRFA="S" and g_CICLILAV="S" and g_GESCON="S" and arpropre = "I" and not(artipart $ "PH.PS.DC") AND USED("FASEXTODL") and m.clfasext="S"
            this.w_CONTSAL = this.w_CodSaldo
            this.w_UNMIS1 = elabor.arunmis1
            this.w_UNMIS2 = elabor.arunmis2
            this.w_OPERAT = elabor.aroperat
            this.w_MOLTIP = elabor.armoltip
            * --- Articolo di Provenienza Conto Lavoro
            this.w_CONTRAOLD = SPACE(15)
            this.w_FORNIOLD = SPACE(15)
            this.w_TipoContra = 0
            USE IN SELECT("MATERZIS")
            SELECT * FROM FASEXTODL INTO CURSOR FASEXTDIBA WHERE DBKEYSAL = this.w_CONTSAL and clinival<=this.w_Datini and this.w_Datini<=clfinval
            SELECT "FASEXTDIBA"
            GO TOP
            SCAN
            * --- Verifica i materiali forniti dal terzista
            this.w_CONTART = left(codsal1,20)
            this.w_CONTVAR = right(codsal1,20)
            this.w_CONTVAR = iif(this.w_CONTVAR = repl("#",20), space(20), this.w_CONTVAR)
            Select Contratti
            do case
              case this.w_PREZUM $ "SN"
                * --- Considero solo i contratti con UnitÃ  di Misura Contratto=U.M. principale articolo
                * --- Cerca il contratto per articolo
                Locate for m.dateva >= codatini and m.dateva <= codatfin and conumero=contra and cocodart=this.w_CONTART and counimis=this.w_unmis1
                this.w_TipoContra = 2
              otherwise
                * --- Cerca il contratto per articolo
                Locate for m.dateva >= codatini and m.dateva <= codatfin and conumero=contra and cocodart=this.w_CONTART
                this.w_TipoContra = 2
            endcase
            if vartype(g_ATTIVAMRPLOG)="L" and g_ATTIVAMRPLOG
              w_Handle = fopen(w_NOME_FILE_LOG, 2) 
 = FSEEK(w_Handle,0,2) 
              w_record="pagina 3 locate contratti "+str(seconds()-INIZ03,6,3)+" sec." 
 INIZ03=seconds()
              w_t = fputs(w_Handle, w_Record)
              fclose(w_Handle)
            endif
            if Found()
              if this.w_TipoContra=2
                Select mccodice from Contratti into cursor Materzis ; 
 where conumero=Elabor.contra and cocodart=this.w_CONTART
              endif
              this.w_TipoContra = iif(RecCount("Materzis")=0, 0, this.w_TipoContra)
            else
              this.w_TipoContra = 0
            endif
            SELECT "FASEXTDIBA"
            ENDSCAN
            USE IN SELECT("FASEXTDIBA")
            Select Elabor
          endif
          Select Elabor
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_PPMRPDIV="S"
            Select Elabor_bis 
 go this.w_RecNumb 
 scatter memvar
          endif
          Select Elabor 
 go this.w_RecNumb 
 scatter memvar
        enddo
        Select Elabor
        if not eof()
          skip
        endif
        if PRPIAPUN="S"
          * --- PRPIAPUN -> 'S' = Pianificazione puntuale 'N' = Per data
          *     Se ho il flag pianificazione puntuale attivo non devo fare skip altrimenti salta una riga
          this.w_RecNumb = recno()
          this.w_nocomp = .t.
        else
          this.w_RecNumb = IIF(artipart $ "PH-DC-PS"or (arflcomm="S" and not empty(nvl(codcom, " "))), this.w_RecNumb, recno())
          if (type("artipart")="C" and artipart $ "PH-DC-PS") Or (arflcomm="S" and not empty(nvl(codcom,"")))
            this.w_nocomp = .F.
          endif
        endif
      case arpropre="E" and artipges="F" and this.w_GENPODA="O"
        * --- Articolo di provenienza esterna e Gestione a Fabbisogno
        this.w_Fabnet = this.w_QTA
        this.w_PREZUM = nvl(ARPREZUM,"N")
        this.w_CENCOS = iif(empty(nvl(arcencos," ")),this.w_PPCCSODA,arcencos)
        if type("prcrifor")<>"U"
          this.w_PRCRIFOR = nvl(prcrifor,"N")
        else
          this.w_PRCRIFOR = SPACE(1)
        endif
        if this.w_PRCRIFOR<>"N" and not empty(this.w_PRCRIFOR) and this.w_PRCRIFOR<>"R"
          * --- Se w_PRCRIFOR='N' si applica il criterio specificato nei dati delle PDA altrimenti
          *     quello dei dati articolo magazzino 
        else
          this.w_PRCRIFOR = this.oParentObject.w_CRITFORN
        endif
        this.w_CODCON = nvl(prcodfor,space(15))
        this.w_CONUMERO = space(15)
        this.w_CRIFOR = this.oParentObject.w_CRITFORN
        this.w_CODARTVAR = codart
        m.datidx = dtos(m.dateva)
        do case
          case m.prperpia="W"
            * --- Inizio Settimana
            m.dateva = ttod(m.dateva) - dow(ttod(m.dateva), 2) + 1
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(week(m.dateva, 1, 2), 2, "0")
          case m.prperpia="M"
            * --- Inizio Mese
            m.dateva = ttod(m.dateva) - day(ttod(m.dateva)) + 1
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(month(m.dateva), 2, "0")
          case m.prperpia="Q"
            * --- Inizio Trimestre
            m.dateva = date(year(m.dateva), 3 * quarter(m.dateva) - 2 ,1)
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(ceiling(month(m.dateva)*4/12), 2, "0")
          case m.prperpia="F"
            * --- Inizio Quadrimestre
            m.dateva = date(year(m.dateva), 4 * ceiling(month(m.dateva)*3/12) - 3 ,1)
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(ceiling(month(m.dateva)*3/12), 2, "0")
          case m.prperpia="S"
            * --- Inizio Semestre
            m.dateva = date(year(m.dateva), 6 * ceiling(month(m.dateva)*2/12) - 5 ,1)
            m.periodo = alltrim(str(year(m.dateva))) + "-" + m.prperpia + padl(ceiling(month(m.dateva)*2/12), 2, "0")
          otherwise
            * --- Giornaliero (tutto come prima))
            *     L'MRP lavora gi� per data
        endcase
        this.w_Dateva = m.dateva
        * --- Se � presente il fornitore su PAR_RIOR lo uso altrimenti lo cerco in base 
        *     al criterio di ricerca presente su PAR_RIOR o, se su PAR_RIOR non � 
        *     presente, in base al criterio selezionato sulla maschera di lancio MRP
        * --- Cerca il miglior fornitore dai ContrattiA
        * --- Seleziona Criterio di scelta
        do case
          case this.w_CRIFOR="A"
            * --- Affidabilit�
            OrdinaPer = "Order by coaffida desc"
          case this.w_CRIFOR="T"
            * --- Tempo = giorni di approvvigionamento
            OrdinaPer = "Order by cogioapp"
          case this.w_CRIFOR="R"
            * --- Ripartizione
            OrdinaPer = "Order by coperrip"
          case this.w_CRIFOR="I"
            * --- Priorit�
            OrdinaPer = "Order by copriori desc"
          otherwise
            OrdinaPer = ""
        endcase
        if g_GESCON="S"
          if this.w_CRIFOR<>"Z"
            * --- Seleziona i ContrattiA validi e li ordina per il criterio di scelta
            Select * from ContrattiA ;
            where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
            &OrdinaPer into cursor tempor
          else
            * --- Calcola Prezzo da Contratto
            * --- Prima genera un temporaneo contenente le righe articolo interessate dei ContrattiA validi
            * --- ordinate per Fornitore + Qta Scaglione
            * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
            Select * from ContrattiA ;
            where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin ;
            and nvl(coquanti,0)>=this.w_fabnet and not empty(nvl(cocodclf," ")) and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
            order by cocodclf, coquanti into cursor Contprez
            * --- Calcola Prezzo da Contratto
            * --- Elabora Criterio per Miglior Prezzo
            =WrCursor("Contprez")
            Index On cocodclf+str(coquanti,12,3) Tag Codice
            * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
            this.w_appo1 = "######"
            select Contprez
            go top
            scan
            if cocodclf <> this.w_APPO1
              * --- Nuovo Fornitore, Calcola Prezzo da Contratto
              this.w_PZ = NVL(COPREZZO, 0)
              this.w_S1 = NVL(COSCONT1, 0)
              this.w_S2 = NVL(COSCONT2, 0)
              this.w_S3 = NVL(COSCONT3, 0)
              this.w_S4 = NVL(COSCONT4, 0)
              this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
              this.w_VAL = NVL(COCODVAL, g_PERVAL)
              this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
              if this.w_VAL<>g_PERVAL
                * --- Se altra valuta, riporta alla Valuta di conto
                if this.w_CAO<>0
                  this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
                  replace coprezzo with this.w_PZ
                else
                  * --- Cambio incongruente, record non utilizzabile
                  this.w_PZ = -999
                endif
                select Contprez
              endif
              if this.w_PZ=-999
                delete
              else
                this.w_appo1 = cocodclf
              endif
            else
              * --- Elimina record
              delete
            endif
            endscan
            * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
            * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
            * --- Ordino per il prezzo piu' basso
            Select * from ContPrez where not deleted() ;
            Order By Coprezzo into cursor tempor
            if used("ContPrez")
              Select contPrez
              use
            endif
          endif
          Select tempor
          if reccount()=0 or prgiocop <> 0
            * --- Nessun contratto valido trovato
            if empty(this.w_CODCON)
              this.w_CODCON = space(15)
              this.w_CONUMERO = space(15)
            endif
          else
            go top
            this.w_CODCON = nvl(cocodclf,space(15))
            this.w_QTAMIN = nvl(coqtamin,0)
            this.w_LOTRIO = nvl(colotmul,0)
            this.w_GIOAPP = nvl(cogioapp,0)
            this.w_CONUMERO = nvl(conumero,space(15))
          endif
          use in select("Tempor")
        else
          this.w_CONUMERO = space(15)
        endif
        if Empty(this.w_CONUMERO) or arflcomm="S"
          if Empty(this.w_CONUMERO)
            this.w_GIOAPP = nvl(prgioapp,0)
          endif
          if arflcomm="S"
            this.w_LOTRIODEF = 0
            this.w_QTAMINDEF = 0
            this.w_LOTRIO = 0
            this.w_QTAMIN = 0
          else
            Select Elabor
            if prgiocop <> 0
              this.w_LOTRIODEF = 0
              this.w_QTAMINDEF = 0
            else
              this.w_LOTRIODEF = nvl(prlotrio,0)
              this.w_QTAMINDEF = nvl(prqtamin,0)
            endif
          endif
          this.w_LOTRIO = this.w_LOTRIODEF
          this.w_QTAMIN = this.w_QTAMINDEF
        endif
        * --- Politiche di lottizzazione
        * --- Vincolo quantit�minima
        if this.w_Fabnet<this.w_QTAMIN
          this.w_Fabnet = this.w_QTAMIN
        endif
        * --- Vincolo lotto multiplo
        if nvl(this.w_LOTRIO,0)>0
          this.w_Fabnet = ceiling(this.w_Fabnet/this.w_LOTRIO)*this.w_LOTRIO
        endif
        * --- Aggiunge l'ordine
        Select Elabor
        tiprec="8PDA-S"
        codcom=nvl(codcom,space(15))
        codatt=nvl(codatt,space(15))
        armagpre=iif(empty(nvl(m.armagpre,"")), this.w_PPMAGPRO, m.armagpre)
        codmag= iif(this.w_CRIELA="A" , m.armagpre, iif(empty(nvl(m.codmag, "")), m.armagpre, m.codmag))
        codsal = m.codart + iif(this.w_CRIELA<>"A", iif(this.w_CRIELA="G", m.grumag, m.codmag) , space(0))
        keyidx=right("00"+alltrim(str(m.prlowlev,3,0)),3) + m.codsal + iif(nvl(m.arflcom1,"N")="C",m.datidx + m.codcom + m.codatt, m.codcom + m.codatt + m.datidx) + "8"
        m.datidx = dtos(m.dateva)
        m.datper = m.datidx
        * --- Lead-Time Sicurezza
        datcon = m.dateva
        this.w_Leadt = prsafelt
        if this.w_Leadt>0 and m.datidx<>"19000101"
          * --- DatInput=w_Datidx
          this.DatInput = dtos(m.dateva)
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          dateva = iif(m.datcon<this.DatRisul or empty(this.DatRisul), m.datcon, this.DatRisul)
        endif
        * --- Lead-Time Produzione e MPS
        if prgiocop <> 0
          this.w_Leadt = 0
        else
          this.w_Leadt = this.w_GIOAPP
        endif
        if this.w_Leadt>0 and m.datidx<>"19000101"
          this.DatInput = dtos(m.dateva)
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          datini = iif(m.dateva<this.DatRisul or empty(this.DatRisul), m.dateva, this.DatRisul)
          datmps = iif(m.dateva<this.DatRisul or empty(this.DatRisul), m.dateva, this.DatRisul)
        else
          datmps = m.dateva
          datini = m.dateva
        endif
        qtanet=this.w_QTA
        contra=this.w_CONUMERO
        prcodfor=this.w_CODCON
        prcrifor=this.w_CRIFOR
        arcencos=this.w_CENCOS
        this.w_Fabpar = this.w_Fabnet
        this.w_QTARES = this.w_Fabnet
        if arflcomm="S" or prgiocop <> 0
          this.w_QTAMAX = 0
        else
          this.w_QTAMAX = prqtamax
        endif
        this.w_QTALOT = this.w_LOTRIO
        this.w_MINRIO = this.w_QTAMIN
        do while this.w_QTARES>0
          * --- Questo ciclo serve a spezzare gli ordini che non rispettano il vincolo di quantitÃ  massima, se il vincolo non c'Ã¨ la procedura fa sempre un solo 'giro'
          cododl=padl(alltrim(str(this.w_ODLprog)), 15)
          this.w_ODLprog = this.w_ODLprog + 1
          padrered=iif(artipart $ "PH.DC.PS", m.padrered, m.cododl)
          if this.w_QTAMAX<>0 and this.w_QTARES>this.w_QTAMAX
            * --- Applico politica di lottizzazione quantitÃ  massima
            this.w_Fabpar = this.w_QTAMAX
            * --- Fabbisogno residuo (da valutare al prossimo giro)
            this.w_QTARES = this.w_QTARES-this.w_Fabpar
          else
            if this.w_QTAMAX=0
              * --- Se la quantitÃ  massima non Ã¨ gestita faccio un solo giro nello while
              this.w_QTARES = 0
            else
              * --- Se sono all'ultimo giro tengo l'avanzo
              this.w_Fabpar = this.w_QTARES
              this.w_QTARES = 0
            endif
          endif
          leapro=this.w_Leadt
          if .f.
             armagpre=iif(empty(nvl(m.armagpre,"")), this.w_PPMAGPRO, m.armagpre) 
 codmag=m.armagpre
          endif
          quanti=this.w_Fabpar
          quanfi=this.w_Fabpar
          qtalor=this.w_Fabpar
          Select Elabor
          append blank
          gather memvar
          if this.w_PPMRPDIV="S"
            Select Elabor_bis 
 append blank 
 gather memvar 
 Select Elabor
          endif
          Select Elabor 
 go this.w_RecNumb
        enddo
        Select Elabor
        this.w_SaldProg = this.w_Fabnet-this.w_QTA
        this.w_RecNumb = recno()
        this.w_QTA = this.w_Fabnet
    endcase
    if this.w_PPMRPDIV="S"
      Select Elabor_bis 
 go this.w_RecNumb
    endif
    Select Elabor
    go this.w_RecNumb
    Select Elabor
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento DB
    L_GridTime2 = 0
    L_GridTime1 = 0
    if this.w_MRPLOG="S"
      L_TAggCur = .f.
    endif
    if this.oParentObject.w_MODELA="R" and ((this.w_ELACAT<>"S" and this.w_SELEZ="S") or this.w_GENPODA<>"O")
      * --- Pegging degli ODL suggeriti eliminati (Da aggiornare/cancellare).
      VQ_EXEC("..\COLA\EXE\QUERY\GSMR43BGP", this, "PEGSUG")
    endif
    * --- Attiva i giorni senza ore di lavorazione
    Select * from Calend into cursor Calend READWRITE 
 index on datidx tag datidx
    select max(datidx) as datidx from Calend into Cursor MaxDate
    this.w_MAXDATE = Maxdate.datidx
    use in select("MaxDate")
    * --- ODL suggeriti elaborati dalla procedura
    Select Elabor
    * --- Esegue FLOCK per velocizzare le fasi di update successive (anche se su cursore)
    =FLock()
    * --- Aggiorna padreord per impostare l'ordinamento
    if this.w_PPMRPDIV="S"
      Update Elabor_bis set padreord = padrered + iif(tiprec="8", "7", iif(tiprec="7", "8", "9")), ; 
 aggdb = (artipart<>"PS" and ((tiprec = "7MRPIM" and quanfi<>0) or ((tiprec = "8MRP-S" or tiprec = "8PDA-S")and not artipart $ "PH.DC"))) 
 Select Elabor_bis 
 index on padreord for aggdb tag idxodl
      Select Elabor
    endif
    Update Elabor set padreord = padrered + iif(tiprec="8", "7", iif(tiprec="7", "8", "9")), ; 
 aggdb = (artipart<>"PS" and ((tiprec = "7MRPIM" and quanfi<>0) or ((tiprec = "8MRP-S" or tiprec = "8PDA-S")and not artipart $ "PH.DC"))) 
 index on padreord for aggdb tag idxodl
    go top
    do while not eof("elabor")
      this.w_RecNumb = Recno("Elabor")
      if dtos(Elabor.dateva) > this.w_MAXDATE
        if this.w_PPMRPDIV="S"
          Select Elabor_bis 
 go this.w_RecNumb
          Replace tpperass with "XXX"
          Select Elabor
        else
          Replace tpperass with "XXX"
        endif
      else
        Select Calend
        if seek(dtos(Elabor.dateva))
          if this.w_PPMRPDIV="S"
            Select Elabor_bis 
 go this.w_RecNumb
            Replace tpperass with Calend.tpperass
            Select Elabor
          else
            Select Elabor 
 Replace tpperass with Calend.tpperass
          endif
        else
          Select Elabor
        endif
      endif
      skip
    enddo
    count to this.w_OLDPOS
    this.w_RecNumb = "/" + str(this.w_OLDPOS, 6)
    this.w_Padre_er = space(15)
    if this.w_MRPLOG="S"
      this.w_dbiniz = seconds()
    endif
    if this.w_UseProgBar
      GesProgBar("M", this, 0, VAL(SUBSTR(this.w_RecNumb, 3)), ah_msgformat("Scrittura Dati..."))
      this.w_NRECCUR = 0
      this.w_NRECELAB = VAL(SUBSTR(this.w_RecNumb, 3))
      GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
    endif
    * --- Try
    local bErr_0578A138
    bErr_0578A138=bTrsErr
    this.Try_0578A138()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_ErrMsg = i_errmsg
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_0578A138
    * --- End
  endproc
  proc Try_0578A138()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Elimina vecchi ODL suggeriti
    if this.oParentObject.w_MODELA="N"
      * --- Cancella gli ODL suggeriti per gli articoli rielaborati
      * --- Try
      local bErr_0597C7D0
      bErr_0597C7D0=bTrsErr
      this.Try_0597C7D0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="del odl_mast"
        return
      endif
      bTrsErr=bTrsErr or bErr_0597C7D0
      * --- End
      USE IN SELECT("OLSug2del")
    else
      this.w_OLTSTATO = "/"
      if g_COMM="S"
        * --- Storna i Saldi di Commessa
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS"
          do vq_exec with 'gsmribgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
                  +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
                  +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
                  +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CSCONSUN = MA_COSTI.CSCONSUN-_t2.CSCONSUN";
              +",CSORDIN = MA_COSTI.CSORDIN-_t2.CSORDIN";
              +i_ccchkf;
              +" from "+i_cTable+" MA_COSTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
                  +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
                  +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
                  +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI, "+i_cQueryTable+" _t2 set ";
              +"MA_COSTI.CSCONSUN = MA_COSTI.CSCONSUN-_t2.CSCONSUN";
              +",MA_COSTI.CSORDIN = MA_COSTI.CSORDIN-_t2.CSORDIN";
              +Iif(Empty(i_ccchkf),"",",MA_COSTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="MA_COSTI.CSCODCOM = t2.CSCODCOM";
                  +" and "+"MA_COSTI.CSTIPSTR = t2.CSTIPSTR";
                  +" and "+"MA_COSTI.CSCODMAT = t2.CSCODMAT";
                  +" and "+"MA_COSTI.CSCODCOS = t2.CSCODCOS";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI set (";
              +"CSCONSUN,";
              +"CSORDIN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"CSCONSUN-t2.CSCONSUN,";
              +"CSORDIN-t2.CSORDIN";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
                  +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
                  +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
                  +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI set ";
              +"CSCONSUN = MA_COSTI.CSCONSUN-_t2.CSCONSUN";
              +",CSORDIN = MA_COSTI.CSORDIN-_t2.CSORDIN";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".CSCODCOM = "+i_cQueryTable+".CSCODCOM";
                  +" and "+i_cTable+".CSTIPSTR = "+i_cQueryTable+".CSTIPSTR";
                  +" and "+i_cTable+".CSCODMAT = "+i_cQueryTable+".CSCODMAT";
                  +" and "+i_cTable+".CSCODCOS = "+i_cQueryTable+".CSCODCOS";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CSCONSUN = (select "+i_cTable+".CSCONSUN-CSCONSUN from "+i_cQueryTable+" where "+i_cWhere+")";
              +",CSORDIN = (select "+i_cTable+".CSORDIN-CSORDIN from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Cancella la lista dei materiali degli ODL suggeriti
      * --- Try
      local bErr_05993D20
      bErr_05993D20=bTrsErr
      this.Try_05993D20()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="del odl_dett"
        return
      endif
      bTrsErr=bTrsErr or bErr_05993D20
      * --- End
      * --- Cancella tutti gli ODL suggeriti
      * --- Try
      local bErr_05994C50
      bErr_05994C50=bTrsErr
      this.Try_05994C50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="del odl_mast"
        return
      endif
      bTrsErr=bTrsErr or bErr_05994C50
      * --- End
      * --- Aggiorna il periodo assoluto degli ODL Pianificati e Lanciati
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_cOp2=cp_SetTrsOp(this.w_OPV,'UTCV','this.w_UTCV',this.w_UTCV,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_OPV,'UTDV','this.w_UTDV',this.w_UTDV,'update',i_nConn)
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OLCODODL"
        do vq_exec with 'gsmr9bgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTPERAS = _t2.TPPERASS";
        +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
            +"ODL_MAST.OLTPERAS = _t2.TPPERASS";
        +",ODL_MAST.UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",ODL_MAST.UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
            +"OLTPERAS,";
            +"UTCV,";
            +"UTDV";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.TPPERASS,";
            +cp_NullLink(i_cOp2,'ODL_MAST','UTCV')+",";
            +cp_NullLink(i_cOp3,'ODL_MAST','UTDV')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
            +"OLTPERAS = _t2.TPPERASS";
        +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTPERAS = (select TPPERASS from "+i_cQueryTable+" where "+i_cWhere+")";
        +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Determina numero di ODL da generare (record "MRP-S") ed aggiorna di conseguenza il progressivo PRODL.
    *     In questo modo si evita l'aggiornamento del progressivo per ogni ODL.
    *     Il progressivo viene aggiornato subito con il valore attuale + numeroODL
    this.w_OLCODODL = space(15)
    i_Conn=i_TableProp[this.ODL_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRODL", "i_codazi,w_OLCODODL")
    this.w_FIRSTODL = this.w_OLCODODL
    Select Elabor 
 Go top 
 count for tiprec="8MRP-S" or tiprec="8PDA-S" to this.w_NUMODL
    this.w_NUCODODL = VAL(this.w_OLCODODL) + this.w_NUMODL - 1
    this.w_OLCODODL = RIGHT("000000000000000"+ALLTRIM(STR(this.w_NUCODODL,15)) ,15)
    cp_NextTableProg(this, i_Conn, "PRODL", "i_codazi,w_OLCODODL")
    this.w_NUCODODL = this.w_NUCODODL - this.w_NUMODL
    this.w_NUMODL = 0
    if this.w_MRPLOG="S"
      local L_TAggCur, L_TimeAggCur2, L_TimeAggCur1
      this.w_dbiniz1 = seconds()
      this.w_finedbiniz = this.w_dbiniz1-this.w_dbiniz
      this.w_oMRPLOG.ADDMSGLOG( "Inizio scansione cursore ELABOR"+ " >>>>>>>>>>>>>> [%1]", alltrim(TTOC(DATETIME())))     
      L_TAggCur =.F.
      L_TimeAggCur1 = 0
      L_TimeAggCur2 = 0
    endif
    L_GridTime2 = 0
    L_GridTime1 = 0
    L_GridTimeInterval = .1
    * --- Inserisce ODL elaborati
    Select Elabor 
 go top
    do while not eof("Elabor")
      if this.w_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        L_GridTime1 = seconds() * 1000
        L_GridTime3 = L_GridTime1 - L_GridTime2
        if L_GridTime3 >=1000
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
          L_GridTime2 = L_GridTime1
        endif
      else
        this.w_RECCNT = this.w_RECCNT+1
        if MOD(this.w_RECCNT,1*iif(VAL(SUBSTR(this.w_RecNumb,2,6))>1000,10,1)*iif(VAL(SUBSTR(this.w_RecNumb,2,6))>10000,50,1))=0
          ah_msg(ALLTRIM(STR(this.w_RECCNT,6))+this.w_RecNumb)
          if this.w_MRPLOG="S"
            L_TimeAggCur1 = seconds()
            L_TAggCur =.T.
          endif
        endif
      endif
      if this.w_Padre_er<>left(padreord,15) and tiprec="7MRPIM"
        * --- Impegno derivante da un ODP Da Pianificare: cambia lo stato dell'ODP
        this.w_Padre_er = left(padreord,15)
        this.w_OLCODODL = this.w_Padre_er
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("*"),'ODL_MAST','OLTSTATO');
          +",OLTFLDAP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLDAP');
          +",OLTFLPIA ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLPIA');
          +",OLDATODL ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
          +",OLOPEODL ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
          +",OLTVARIA ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_Padre_er);
                 )
        else
          update (i_cTable) set;
              OLTSTATO = "*";
              ,OLTFLDAP = " ";
              ,OLTFLPIA = "+";
              ,OLDATODL = i_DATSYS;
              ,OLOPEODL = i_CODUTE;
              ,OLTVARIA = "N";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_Padre_er;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_CPROWNUM = 0
        this.w_IDEGRU = "  "
        select ODP
        locate for cododl=this.w_Padre_er
        this.w_OLPROVE = OLPROVE
        this.w_QTA = quanti
        this.w_SERCIC = oltsecic
        this.w_MAGRIF = nvl(codmag,"")
        this.w_Datini = datini
        if this.w_Datini < i_DATSYS
          this.w_LogErrori = "ODP-RIT"
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_nRecFasi = 0
        if g_PRFA="S" and g_CICLILAV="S" and used("Cicli")
          * --- Seleziona il Ciclo indicato sull'ODP
          select distinct clserial, cprownum, cproword, clindfas, cl__fase, clfasdes, clcoupoi, clfasout, clultfas, clcprife, ; 
 clcodfas, clwipfpr, clbfrife, clfascos, clfascla, clfasclo, clmagwip, clprifas, clltfase ; 
 from Cicli where clserial=this.w_SERCIC and clinival<=this.w_Datini and this.w_Datini<=clfinval ; 
 order by cproword desc into cursor TmpCic
          this.w_nRecFasi = RecCount("TmpCic")
          if g_PRFA="S" and g_CICLILAV="S" and used("TmpCic")
            Select * from TmpCic into cursor TmpCic READWRITE 
 Select TmpCic
            if this.w_nRecFasi > 0
              Select TmpCic
              go top
              replace clultfas with "S", clcoupoi with "S", clfasout with "S"
              * --- Verifico che sia presente il flag prima fase
              *     La fase con il flag ppotrebbe non essere valida e quindi lo dovrei ricalcolare
              Select TmpCic
              go top
              locate for clprifas="S"
              if Found()
                replace clprifas with "S", clcoupoi with "S", clfasout with "S"
              else
                Select TmpCic
                go bottom
                do while not bof()
                  if clcoupoi="S" and clfasout="S"
                    replace clprifas with "S"
                    exit
                  endif
                  Select TmpCic
                  skip-1
                enddo
              endif
              if this.w_PPMATINP="P"
                * --- Prima fase: i materiali non associati a nessuna fase sono associati alla prima
                this.w_PRRIFFAS = cproword
                this.w_PRCPRIFE = clcprife
              endif
            endif
            * --- Imposta il Codice output della fase precedente
            this.w_SERCIC = "Z$$$"
            go bottom
            do while not bof()
              if this.w_SERCIC<>clserial
                this.w_SERCIC = clserial
                this.w_CPROWORD = cproword
                this.w_CLFASOUT = NVL(clfasout, "N")
                this.w_CLWIPFPR = SPACE(20)
                if this.w_CLFASOUT="S"
                  this.w_WIPFPR = nvl(clcodfas,SPACE(20))
                else
                  this.w_WIPFPR = SPACE(20)
                endif
              endif
              if this.w_CPROWORD<>cproword
                this.w_CPROWORD = cproword
                this.w_CLFASOUT = NVL(clfasout, "N")
                this.w_CLWIPFPR = SPACE(20)
                if this.w_CLFASOUT="S"
                  this.w_CLWIPFPR = this.w_WIPFPR
                  this.w_WIPFPR = nvl(clcodfas,SPACE(20))
                endif
              endif
              replace clwipfpr with this.w_CLWIPFPR
              skip-1
            enddo
          endif
          Select TmpCic
          go top
          this.w_WIPFASPRE = SPACE(5)
          this.w_WIPFASOLD = SPACE(5)
          if this.w_nRecFasi > 0
            scan
            if this.w_PPMATINP<>"P"
              if RecNo()=1
                * --- Ultima Fase: i materiali non associati a nessuna fase sono associati all'ultima
                this.w_PRRIFFAS = cproword
                this.w_PRCPRIFE = clcprife
                this.w_WIPFASPRE = this.w_WIPFASOLD
              endif
            endif
            this.w_CLFASSEL = iif(clindfas="00","S","N")
            this.w_CLCOUPOI = iif(clultfas="S", "S", clcoupoi)
            this.w_CLFASOUT = NVL(clfasout, "N")
            this.w_CLFASCLA = NVL(clfascla, "N")
            this.w_CLCODFAS = NVL(CLCODFAS, SPACE(20))
            this.w_CLFASCOS = iif(this.w_CLCOUPOI="S","S",clfascos)
            this.w_WIPFASOLD = nvl(clmagwip, space(5))
            if this.w_nRecFasi = 1
              this.w_CLFASCLA = NVL(CLFASCLA, "N")
              if NVL(CLFASCLA, "N")<>NVL(CLFASCLO, "N")
                this.w_CLFASCLA = NVL(CLFASCLO, "N")
              endif
              this.w_CLFPROVE = IIF(this.w_CLFASCLA="S", "L", "I")
              this.w_OLTPROVE = this.w_CLFPROVE
            endif
            * --- Insert into ODL_CICL
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CLDESFAS"+",CLQTAPRE"+",CLPREUM1"+",CLQTAAVA"+",CLAVAUM1"+",CLFASEVA"+",CLCOUPOI"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLWIPOUT"+",CLWIPFPR"+",CLBFRIFE"+",CL__FASE"+",CLFASCOS"+",CLCODFAS"+",CLFASOUT"+",CLFASCLA"+",CLWIPDES"+",CLPRIFAS"+",CLLTFASE"+",CLMAGWIP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_CICL','CLCODODL');
              +","+cp_NullLink(cp_ToStrODBC(cprownum),'ODL_CICL','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(cprownum),'ODL_CICL','CLROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(cproword),'ODL_CICL','CLROWORD');
              +","+cp_NullLink(cp_ToStrODBC(clindfas),'ODL_CICL','CLINDPRE');
              +","+cp_NullLink(cp_ToStrODBC(clfasdes),'ODL_CICL','CLDESFAS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_QTA),'ODL_CICL','CLQTAPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_QTA),'ODL_CICL','CLPREUM1');
              +","+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLQTAAVA');
              +","+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLAVAUM1');
              +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_CICL','CLFASEVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCOUPOI),'ODL_CICL','CLCOUPOI');
              +","+cp_NullLink(cp_ToStrODBC(clultfas),'ODL_CICL','CLULTFAS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASSEL),'ODL_CICL','CLFASSEL');
              +","+cp_NullLink(cp_ToStrODBC(clcprife),'ODL_CICL','CLCPRIFE');
              +","+cp_NullLink(cp_ToStrODBC(clcodfas),'ODL_CICL','CLWIPOUT');
              +","+cp_NullLink(cp_ToStrODBC(clwipfpr),'ODL_CICL','CLWIPFPR');
              +","+cp_NullLink(cp_ToStrODBC(clbfrife),'ODL_CICL','CLBFRIFE');
              +","+cp_NullLink(cp_ToStrODBC(cl__fase),'ODL_CICL','CL__FASE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCOS),'ODL_CICL','CLFASCOS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODFAS),'ODL_CICL','CLCODFAS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASOUT),'ODL_CICL','CLFASOUT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCLA),'ODL_CICL','CLFASCLA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_WIPFASPRE),'ODL_CICL','CLWIPDES');
              +","+cp_NullLink(cp_ToStrODBC(clprifas),'ODL_CICL','CLPRIFAS');
              +","+cp_NullLink(cp_ToStrODBC(clltfase),'ODL_CICL','CLLTFASE');
              +","+cp_NullLink(cp_ToStrODBC(clmagwip),'ODL_CICL','CLMAGWIP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CLCODODL',this.w_OLCODODL,'CPROWNUM',cprownum,'CLROWNUM',cprownum,'CLROWORD',cproword,'CLINDPRE',clindfas,'CLDESFAS',clfasdes,'CLQTAPRE',this.w_QTA,'CLPREUM1',this.w_QTA,'CLQTAAVA',0,'CLAVAUM1',0,'CLFASEVA'," ",'CLCOUPOI',this.w_CLCOUPOI)
              insert into (i_cTable) (CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CLDESFAS,CLQTAPRE,CLPREUM1,CLQTAAVA,CLAVAUM1,CLFASEVA,CLCOUPOI,CLULTFAS,CLFASSEL,CLCPRIFE,CLWIPOUT,CLWIPFPR,CLBFRIFE,CL__FASE,CLFASCOS,CLCODFAS,CLFASOUT,CLFASCLA,CLWIPDES,CLPRIFAS,CLLTFASE,CLMAGWIP &i_ccchkf. );
                 values (;
                   this.w_OLCODODL;
                   ,cprownum;
                   ,cprownum;
                   ,cproword;
                   ,clindfas;
                   ,clfasdes;
                   ,this.w_QTA;
                   ,this.w_QTA;
                   ,0;
                   ,0;
                   ," ";
                   ,this.w_CLCOUPOI;
                   ,clultfas;
                   ,this.w_CLFASSEL;
                   ,clcprife;
                   ,clcodfas;
                   ,clwipfpr;
                   ,clbfrife;
                   ,cl__fase;
                   ,this.w_CLFASCOS;
                   ,this.w_CLCODFAS;
                   ,this.w_CLFASOUT;
                   ,this.w_CLFASCLA;
                   ,this.w_WIPFASPRE;
                   ,clprifas;
                   ,clltfase;
                   ,clmagwip;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            this.w_WIPFASPRE = this.w_WIPFASOLD
            endscan
            if this.w_nRecFasi = 1 AND this.w_OLPROVE<>this.w_OLTPROVE
              * --- Se ho una sola fase e la provenienza � diversa da quella orginale dell'ODP allora la cambio
              * --- Write into ODL_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLTPROVE ="+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_Padre_er);
                       )
              else
                update (i_cTable) set;
                    OLTPROVE = this.w_OLTPROVE;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_Padre_er;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Cursore per il riferimento dei materiali alle fasi
            select midibrow, clinival, clfinval, cproword, clcprife ;
            from Cicli where clserial=this.w_SERCIC into cursor TmpCic
          else
            this.w_SERCIC = space(10)
            this.w_PRRIFFAS = 0
            this.w_PRCPRIFE = 0
          endif
        else
          this.w_SERCIC = space(10)
        endif
        select Elabor
      endif
      do case
        case tiprec="8MRP-S"
          * --- Il seriale � stato spostato avanti prima della scansione del cursore
          *     per il numero di ODL_MAST che andremo a scrivere
          this.w_NUCODODL = this.w_NUCODODL + 1
          this.w_OLCODODL = RIGHT("000000000000000"+ALLTRIM(STR(this.w_NUCODODL,15)),15)
          Select Elabor
          this.w_Padre_er = padrered
          if not empty(this.w_Padre_er)
            this.w_OLDPOS = recno()
            update elabor set padrered=this.w_OLCODODL where padrered=this.w_Padre_er
            if this.w_PPMRPDIV="S"
              update elabor_bis set padrered=this.w_OLCODODL where padrered=this.w_Padre_er
            endif
            if this.w_OLDCOM
              update SalComm set perifodl=this.w_OLCODODL where perifodl=this.w_Padre_er
            endif
            Select Elabor
            go this.w_OLDPOS
            if this.w_MRPLOG="S" and vartype(L_TAggCur )="L" and L_TAggCur 
              L_TimeAggCur2 = seconds()
              this.w_oMRPLOG.ADDMSGLOG(">Tempo update elabor (%1%2) %3 [%4]" , alltrim(STR(this.w_RECCNT,6)) , alltrim(this.w_RecNumb) , this.TimeMRP(L_TimeAggCur2-L_TimeAggCur1), alltrim(ttoc(DATETIME())))     
              L_TAggCur = .f.
            endif
          endif
          Select Elabor
          scatter memvar
          if this.w_PPMRPDIV="S"
            Select Elabor_bis
            go this.w_OLDPOS
            scatter memvar
            Select Elabor
          endif
          this.w_OLTCODIC = codric
          this.w_ImpComm = max(impcom,0)
          this.w_OLTFLIMC = iif(this.w_ImpComm>0 and !empty(codatt),"S","N")
          this.w_OLTFORCO = iif(this.w_OLTFLIMC="S",this.w_FLORCO," ")
          this.w_OLTFCOCO = iif(this.w_OLTFLIMC="S",this.w_FLCOCO," ")
          this.w_IDEGRU = "  "
          this.w_OLTPROVE = m.arpropre
          this.w_nRecFasi = 0
          if g_PRFA="S" and g_CICLILAV="S"
            * --- Seleziona il ciclo preferenziale (del materiale preferenziale)
            Select Cicli 
 set order to clcoddis
            select distinct clserial, clcoddis, cprownum, cproword, clindfas, cl__fase, clfasdes, clcoupoi, clfasout, clultfas, ; 
 clcprife, clcodfas, clwipfpr, clbfrife, clfascos, clfascla, clfasclo, clmagwip, clprifas, clltfase ; 
 from Cicli where clcodart=this.w_OLTCODIC and coflpref<>"N" and clindcic="00" ; 
 and clinival<=m.datini and m.datini<=clfinval order by cproword desc into cursor TmpCic
            this.w_nRecFasi = RecCount("TmpCic")
            * --- TmpCic2 usato per controlli validit� ciclo
            select distinct clserial, clindcic, clinival, clfinval, clcoddis ; 
 from Cicli where clcodart=this.w_OLTCODIC and coflpref<>"N" into cursor TmpCic2
            Select Cicli 
 set order to clserial
            if g_PRFA="S" and g_CICLILAV="S" and used("TmpCic")
              Select * from TmpCic into cursor TmpCic READWRITE 
 Select TmpCic
              if this.w_nRecFasi > 0
                Select TmpCic
                go top
                replace clultfas with "S", clcoupoi with "S", clfasout with "S"
                * --- Verifico che sia presente il flag prima fase
                *     La fase con il flag ppotrebbe non essere valida e quindi lo dovrei ricalcolare
                Select TmpCic
                go top
                locate for clprifas="S"
                if Found()
                  replace clprifas with "S", clcoupoi with "S", clfasout with "S"
                else
                  Select TmpCic
                  go bottom
                  do while not bof()
                    if clcoupoi="S" and clfasout="S"
                      replace clprifas with "S"
                      exit
                    endif
                    Select TmpCic
                    skip-1
                  enddo
                endif
                if this.w_PPMATINP="P"
                  * --- Prima fase: i materiali non associati a nessuna fase sono associati alla prima
                  this.w_PRRIFFAS = cproword
                  this.w_PRCPRIFE = clcprife
                endif
              endif
              * --- Imposta il Codice output della fase precedente
              this.w_OLTDISBA = SPACE(20)
              this.w_SERCIC = "Z$$$"
              this.w_WIPFASPRE = SPACE(5)
              this.w_WIPFASOLD = SPACE(5)
              go bottom
              do while not bof()
                if this.w_SERCIC<>clserial
                  this.w_OLTDISBA = clcoddis
                  this.w_SERCIC = clserial
                  this.w_CPROWORD = cproword
                  this.w_CLFASOUT = NVL(clfasout, "N")
                  this.w_CLWIPFPR = SPACE(20)
                  if this.w_CLFASOUT="S"
                    this.w_WIPFPR = nvl(clcodfas,SPACE(20))
                  else
                    this.w_WIPFPR = SPACE(20)
                  endif
                endif
                if this.w_CPROWORD<>cproword
                  this.w_CPROWORD = cproword
                  this.w_CLFASOUT = NVL(clfasout, "N")
                  this.w_CLWIPFPR = SPACE(20)
                  if this.w_CLFASOUT="S"
                    this.w_CLWIPFPR = this.w_WIPFPR
                    this.w_WIPFPR = nvl(clcodfas,SPACE(20))
                  endif
                endif
                replace clwipfpr with this.w_CLWIPFPR
                skip-1
              enddo
            endif
            Select TmpCic
            go top
            if this.w_nRecFasi = 1
              this.w_CLFASCLA = NVL(CLFASCLA, "N")
              if NVL(CLFASCLA, "N")<>NVL(CLFASCLO, "N")
                this.w_CLFASCLA = NVL(CLFASCLO, "N")
              endif
              this.w_CLFPROVE = IIF(this.w_CLFASCLA="S", "L", "I")
              this.w_OLTPROVE = this.w_CLFPROVE
            endif
            Select TmpCic
            if this.w_nRecFasi > 0
              this.w_SERCIC = clserial
              if this.w_PPMATINP<>"P"
                * --- Ultima Fase: i materiali non associati a nessuna fase sono associati all'ultima
                this.w_PRRIFFAS = cproword
                this.w_PRCPRIFE = clcprife
                this.w_WIPFASPRE = this.w_WIPFASOLD
              endif
            else
              this.w_SERCIC = space(10)
              this.w_PRRIFFAS = 0
              this.w_PRCPRIFE = 0
              this.w_WIPFASPRE = this.w_WIPFASOLD
            endif
            * --- Verifiche validit� cicli
            Select TmpCic2
            if RecCount() > 0
              locate for clindcic="00"
              if found()
                if clinival<=m.datini and m.datini<=clfinval
                  * --- Ciclo valido
                else
                  * --- Ciclo non valido
                  this.w_LogErrori = "CIC-DT"
                  this.Page_8()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              else
                this.w_LogErrori = "CIC-00"
                this.Page_8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
          this.w_OLCENCOS = iif(!empty(NVL(m.arcencos,"")),m.arcencos,iif(m.arpropre="I",this.w_CCSODL,iif(m.arpropre="L",this.w_CCSOCL,this.w_CCSODA)))
          * --- Try
          local bErr_057F5F08
          bErr_057F5F08=bTrsErr
          this.Try_057F5F08()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Raise
            i_Error="ins odl_mast"
            return
          endif
          bTrsErr=bTrsErr or bErr_057F5F08
          * --- End
          this.w_CPROWNUM = 0
        case tiprec="7MRPIM"
          this.w_CPROWNUM = this.w_CPROWNUM+1
          this.w_CPROWORD = this.w_CPROWNUM*10
          select Elabor
          scatter memvar
          this.w_OLTCODIC = codric
          this.w_OLRIFFAS = 0
          this.w_OLCPRIFE = 0
          set order to keyidx
          this.w_Keyidx = Keyidx
          this.w_OLDPOS = recno()
          update elabor set perownum = this.w_CPROWNUM ; 
 where elabor.keyidx=this.w_Keyidx and elabor.padrered=this.w_OLCODODL AND elabor.cprownum=m.cprownum
          if this.w_OLDCOM
            update SalComm set perigord=this.w_CPROWNUM where perifodl=this.w_OLCODODL AND perigord=m.cprownum and oltcodic=this.w_OLTCODIC
          endif
          if this.w_PPMRPDIV="S"
            Select Elabor_bis
            go this.w_OLDPOS
            scatter memvar
            set order to idxodl
          endif
          Select Elabor
          go this.w_OLDPOS
          set order to idxodl
          if not empty(this.w_SERCIC)
            select TmpCic
            locate for midibrow=m.cprownum and clinival<=m.datini and m.datini<=clfinval
            if FOUND()
              this.w_OLRIFFAS = cproword
              this.w_OLCPRIFE = clcprife
            else
              * --- I materiali senza riferimento sono prelevati nella prima fase
              this.w_OLRIFFAS = this.w_PRRIFFAS
              this.w_OLCPRIFE = this.w_PRCPRIFE
            endif
          endif
          Select Elabor
          this.w_OLFLEVAS = evaaut
          if evaaut="S"
            this.w_OLQTAMOV = -quanfv
            this.w_OLQTAUM1 = -quanfi
            if m.coflpref="S"
              this.w_OLQTAEVA = -quanfv
              this.w_OLQTAEV1 = -quanfi
            else
              this.w_OLQTAEVA = 0
              this.w_OLQTAEV1 = 0
              this.w_OLFLEVAS = "N"
            endif
          else
            if m.coflpref="S"
              this.w_OLQTAMOV = -quantv
              this.w_OLQTAUM1 = -quanti
            else
              this.w_OLQTAMOV = -quanfv
              this.w_OLQTAUM1 = -quanfi
            endif
            this.w_OLQTAEVA = 0
            this.w_OLQTAEV1 = 0
          endif
          if not empty(m.coidegru)
            if this.w_IDEGRU=m.coidegru
              * --- Riporta il vecchio numero di riga
              this.w_CPROWORD = this.w_NUMIDE
            else
              * --- Memorizza il numero di riga del gruppo
              this.w_NUMIDE = this.w_CPROWORD
              this.w_IDEGRU = m.coidegru
            endif
          endif
          if m.coflpref="S" or m.coidegru<>"AF"
            * --- Try
            local bErr_05859DC8
            bErr_05859DC8=bTrsErr
            this.Try_05859DC8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- Raise
              i_Error="ins odl_dett"
              return
            endif
            bTrsErr=bTrsErr or bErr_05859DC8
            * --- End
          endif
        case tiprec="8PDA-S" 
          * --- Il seriale � stato spostato avanti prima della scansione del cursore
          *     per il numero di ODL_MAST che andremo a scrivere
          this.w_NUCODODL = this.w_NUCODODL + 1
          this.w_OLCODODL = RIGHT("000000000000000"+ALLTRIM(STR(this.w_NUCODODL,15)),15)
          Select Elabor
          this.w_Padre_er = padrered
          if not empty(this.w_Padre_er)
            this.w_OLDPOS = recno()
            update elabor set padrered=this.w_OLCODODL where padrered=this.w_Padre_er
            if this.w_PPMRPDIV="S"
              update elabor_bis set padrered=this.w_OLCODODL where padrered=this.w_Padre_er
            endif
            if this.w_OLDCOM
              update SalComm set perifodl=this.w_OLCODODL where perifodl=this.w_Padre_er
            endif
            Select Elabor
            go this.w_OLDPOS
            if this.w_MRPLOG="S" and vartype(L_TAggCur )="L" and L_TAggCur 
              L_TimeAggCur2 = seconds()
              this.w_oMRPLOG.ADDMSGLOG(">Tempo update elabor (%1%2) %3 [%4]" , alltrim(STR(this.w_RECCNT,6)) ,alltrim(this.w_RecNumb) , this.TimeMRP(L_TimeAggCur2-L_TimeAggCur1), alltrim(ttoc(DATETIME())))     
              L_TAggCur = .f.
            endif
          endif
          Select Elabor
          scatter memvar
          if this.w_PPMRPDIV="S"
            Select Elabor_bis 
 go this.w_OLDPOS 
 scatter memvar 
 Select Elabor
          endif
          this.w_OLTCODIC = codric
          this.w_UMORDI = m.arunmis1
          this.w_QTAORD = m.quanti
          this.w_ImpComm = max(impcom,0)
          this.w_OLTFLIMC = iif(this.w_ImpComm>0 and !empty(codatt),"S","N")
          this.w_OLTFORCO = iif(this.w_OLTFLIMC="S",this.w_FLORCO," ")
          this.w_OLTFCOCO = iif(this.w_OLTFLIMC="S",this.w_FLCOCO," ")
          this.w_IDEGRU = "  "
          this.w_OLCENCOS = iif(!empty(NVL(m.arcencos,"")),m.arcencos,iif(m.arpropre="I",this.w_CCSODL,iif(m.arpropre="L",this.w_CCSOCL,this.w_CCSODA)))
          * --- Try
          local bErr_054C8E68
          bErr_054C8E68=bTrsErr
          this.Try_054C8E68()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Raise
            i_Error="ins odl_mast"
            return
          endif
          bTrsErr=bTrsErr or bErr_054C8E68
          * --- End
          this.w_CPROWNUM = 0
      endcase
      select Elabor
      skip
    enddo
    if this.oParentObject.w_MODELA="N"
      * --- Aggiorna il periodo assoluto degli ODL Pianificati Oggetto MPS
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_cOp2=cp_SetTrsOp(this.w_OPV,'UTCV','this.w_UTCV',this.w_UTCV,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_OPV,'UTDV','this.w_UTDV',this.w_UTDV,'update',i_nConn)
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OLCODODL"
        do vq_exec with 'gsmr10bgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTPERAS = _t2.TPPERASS";
        +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
            +"ODL_MAST.OLTPERAS = _t2.TPPERASS";
        +",ODL_MAST.UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",ODL_MAST.UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
            +"OLTPERAS,";
            +"UTCV,";
            +"UTDV";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.TPPERASS,";
            +cp_NullLink(i_cOp2,'ODL_MAST','UTCV')+",";
            +cp_NullLink(i_cOp3,'ODL_MAST','UTDV')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
            +"OLTPERAS = _t2.TPPERASS";
        +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTPERAS = (select TPPERASS from "+i_cQueryTable+" where "+i_cWhere+")";
        +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
        +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_OLTSTATO = "+"
    if g_COMM="S"
      * --- Aggiorna i Saldi di Commessa
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS"
        do vq_exec with 'gsmribgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
                +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
                +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
                +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSCONSUN = MA_COSTI.CSCONSUN+_t2.CSCONSUN";
            +",CSORDIN = MA_COSTI.CSORDIN+_t2.CSORDIN";
            +i_ccchkf;
            +" from "+i_cTable+" MA_COSTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
                +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
                +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
                +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI, "+i_cQueryTable+" _t2 set ";
            +"MA_COSTI.CSCONSUN = MA_COSTI.CSCONSUN+_t2.CSCONSUN";
            +",MA_COSTI.CSORDIN = MA_COSTI.CSORDIN+_t2.CSORDIN";
            +Iif(Empty(i_ccchkf),"",",MA_COSTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MA_COSTI.CSCODCOM = t2.CSCODCOM";
                +" and "+"MA_COSTI.CSTIPSTR = t2.CSTIPSTR";
                +" and "+"MA_COSTI.CSCODMAT = t2.CSCODMAT";
                +" and "+"MA_COSTI.CSCODCOS = t2.CSCODCOS";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI set (";
            +"CSCONSUN,";
            +"CSORDIN";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"CSCONSUN+t2.CSCONSUN,";
            +"CSORDIN+t2.CSORDIN";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
                +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
                +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
                +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI set ";
            +"CSCONSUN = MA_COSTI.CSCONSUN+_t2.CSCONSUN";
            +",CSORDIN = MA_COSTI.CSORDIN+_t2.CSORDIN";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CSCODCOM = "+i_cQueryTable+".CSCODCOM";
                +" and "+i_cTable+".CSTIPSTR = "+i_cQueryTable+".CSTIPSTR";
                +" and "+i_cTable+".CSCODMAT = "+i_cQueryTable+".CSCODMAT";
                +" and "+i_cTable+".CSCODCOS = "+i_cQueryTable+".CSCODCOS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSCONSUN = (select "+i_cTable+".CSCONSUN+CSCONSUN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CSORDIN = (select "+i_cTable+".CSORDIN+CSORDIN from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Try
    local bErr_057B1C10
    bErr_057B1C10=bTrsErr
    this.Try_057B1C10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Nessun ciclo associato agli ODL confermati
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_057B1C10
    * --- End
    * --- Try
    local bErr_057BA478
    bErr_057BA478=bTrsErr
    this.Try_057BA478()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Nessun materiali di output associato agli ODL confermati
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_057BA478
    * --- End
    ah_Msg("Aggiornamento saldi in corso...",.T.)
    * --- ODL in stato suggerito
    this.w_OLTSTATO = "+"
    * --- Create temporary table TMPSALAGG
    i_nIdx=cp_AddTableDef('TMPSALAGG') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_JKKWINNIDS[1]
    indexes_JKKWINNIDS[1]='OLKEYSAL,OLCODMAG'
    vq_exec('..\COLA\EXE\QUERY\GSMRSBGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_JKKWINNIDS,.f.)
    this.TMPSALAGG_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- ODP pianificati
    this.w_OLTSTATO = "*"
    * --- Insert into TMPSALAGG
    i_nConn=i_TableProp[this.TMPSALAGG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSALAGG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRSBGP1",this.TMPSALAGG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna i Saldi di Magazzino
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'GSMRSBGP2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTIPER = SALDIART.SLQTIPER+_t2.SLQTIPER";
          +",SLQTOPER = SALDIART.SLQTOPER+_t2.SLQTOPER";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTIPER = SALDIART.SLQTIPER+_t2.SLQTIPER";
          +",SALDIART.SLQTOPER = SALDIART.SLQTOPER+_t2.SLQTOPER";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTIPER,";
          +"SLQTOPER";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTIPER+t2.SLQTIPER,";
          +"SLQTOPER+t2.SLQTOPER";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTIPER = SALDIART.SLQTIPER+_t2.SLQTIPER";
          +",SLQTOPER = SALDIART.SLQTOPER+_t2.SLQTOPER";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTIPER = (select "+i_cTable+".SLQTIPER+SLQTIPER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTOPER = (select "+i_cTable+".SLQTOPER+SLQTOPER from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Try
    local bErr_057C6808
    bErr_057C6808=bTrsErr
    this.Try_057C6808()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Errore Aggiornamento Saldi di Magazzino"
      return
    endif
    bTrsErr=bTrsErr or bErr_057C6808
    * --- End
    if !this.w_OLDCOM
      * --- Aggiorna i Saldi di Magazzino per Commessa
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SCCODICE,SCCODMAG,SCCODCAN"
        do vq_exec with 'GSMRSBGP2C',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER = SALDICOM.SCQTOPER+_t2.SCQTOPER";
            +",SCQTIPER = SALDICOM.SCQTIPER+_t2.SCQTIPER";
            +i_ccchkf;
            +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
            +"SALDICOM.SCQTOPER = SALDICOM.SCQTOPER+_t2.SCQTOPER";
            +",SALDICOM.SCQTIPER = SALDICOM.SCQTIPER+_t2.SCQTIPER";
            +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = t2.SCCODCAN";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
            +"SCQTOPER,";
            +"SCQTIPER";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"SCQTOPER+t2.SCQTOPER,";
            +"SCQTIPER+t2.SCQTIPER";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
            +"SCQTOPER = SALDICOM.SCQTOPER+_t2.SCQTOPER";
            +",SCQTIPER = SALDICOM.SCQTIPER+_t2.SCQTIPER";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
                +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
                +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER = (select "+i_cTable+".SCQTOPER+SCQTOPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SCQTIPER = (select "+i_cTable+".SCQTIPER+SCQTIPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Try
      local bErr_057C8D28
      bErr_057C8D28=bTrsErr
      this.Try_057C8D28()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="Errore Aggiornamento Saldi di Magazzino"
        return
      endif
      bTrsErr=bTrsErr or bErr_057C8D28
      * --- End
    endif
    * --- Drop temporary table TMPSALAGG
    i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALAGG')
    endif
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
          +i_ccchkf ;
      +" where ";
          +"OLTSTATO = "+cp_ToStrODBC("*");
             )
    else
      update (i_cTable) set;
          OLTSTATO = "P";
          &i_ccchkf. ;
       where;
          OLTSTATO = "*";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_STAORD="A"
      * --- Per gli Articoli che hanno il Flag (Stato Ordine Pianificato) su ART_PROD 
      *     metto lo stato a Pianificato, per gli altri articoli lo stato � Suggerito MRP
      vq_exec("GSMR18BGP", this, "Pianif")
      Select Pianif 
 scan
      this.w_ODLDAP = Pianif.OLCODODL
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
            +i_ccchkf ;
        +" where ";
            +"OLTSTATO = "+cp_ToStrODBC("+");
            +" and OLCODODL = "+cp_ToStrODBC(this.w_ODLDAP);
               )
      else
        update (i_cTable) set;
            OLTSTATO = "P";
            &i_ccchkf. ;
         where;
            OLTSTATO = "+";
            and OLCODODL = this.w_ODLDAP;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      update elabor set odlstat="P" where padrered=this.w_ODLDAP and (tiprec="8MRP-S" or tiprec="8PDA-S" or tiprec="7MRPIM")
      Select Pianif
      endscan
      USE IN SELECT("Pianif")
    endif
    if this.w_STAORD="P"
      * --- Ho scelto lo stato Pianificato per tutti
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
            +i_ccchkf ;
        +" where ";
            +"OLTSTATO = "+cp_ToStrODBC("+");
               )
      else
        update (i_cTable) set;
            OLTSTATO = "P";
            &i_ccchkf. ;
         where;
            OLTSTATO = "+";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno Elab
      update elabor set odlstat="P" where tiprec="8MRP-S" or tiprec="8PDA-S" or tiprec="7MRPIM"
    else
      * --- Per gli altri articoli (w_STAORD= 'A' oppure 'S') lo stato � Suggerito MRP
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("M"),'ODL_MAST','OLTSTATO');
            +i_ccchkf ;
        +" where ";
            +"OLTSTATO = "+cp_ToStrODBC("+");
               )
      else
        update (i_cTable) set;
            OLTSTATO = "M";
            &i_ccchkf. ;
         where;
            OLTSTATO = "+";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Aggiorna i Saldi MPS
    ah_Msg("Aggiornamento saldi in corso...",.T.)
    select ODP
    scan
    scatter memvar
    if tiprec="4ODL-D"
      * --- Write into MPS_TFOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FMMPSDAP =FMMPSDAP- "+cp_ToStrODBC(m.quanti);
        +",FMMPSPIA =FMMPSPIA+ "+cp_ToStrODBC(m.quanti);
            +i_ccchkf ;
        +" where ";
            +"FMCODART = "+cp_ToStrODBC(m.codric);
            +" and FMPERASS = "+cp_ToStrODBC(m.oltperas);
               )
      else
        update (i_cTable) set;
            FMMPSDAP = FMMPSDAP - m.quanti;
            ,FMMPSPIA = FMMPSPIA + m.quanti;
            &i_ccchkf. ;
         where;
            FMCODART = m.codric;
            and FMPERASS = m.oltperas;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Azzera il flag ODL variato
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTVARIA ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(m.cododl);
               )
      else
        update (i_cTable) set;
            OLTVARIA = "N";
            &i_ccchkf. ;
         where;
            OLCODODL = m.cododl;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    endscan
    * --- Scrive la data e l'utente che ha eseguito l'elaborazione
    GSMR_BPP(this,"WRITEPARPROD")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_MRPLOG="S"
      this.w_dbiniz2 = seconds()
      this.w_finedbagg = this.w_dbiniz2-this.w_dbiniz1
      this.w_QUANTI = this.w_RECCNT
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0597C7D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Select OLSug2del
    scan
    this.w_OLCODODL = cododl
    * --- Delete from SCCI_ASF
    i_nConn=i_TableProp[this.SCCI_ASF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCCI_ASF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"COD_ODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            COD_ODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MAIN
    i_nConn=i_TableProp[this.ODL_MAIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MICODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            MICODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MOUT
    i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MOCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            MOCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISO
    i_nConn=i_TableProp[this.ODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            RLCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODLMRISO
    i_nConn=i_TableProp[this.ODLMRISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            RLCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISF
    i_nConn=i_TableProp[this.ODL_RISF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RFCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            RFCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_CICL
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            CLCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            OLCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_SMPL
    i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SMCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            SMCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if g_COMM="S"
      * --- Storna i Saldi di Commessa
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTCOMME,OLTCOATT,OLTCOCOS,OLTIMCOM,OLTFORCO,OLTFCOCO"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTCOMME,OLTCOATT,OLTCOCOS,OLTIMCOM,OLTFORCO,OLTFCOCO;
          from (i_cTable) where;
              OLCODODL = this.w_OLCODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
        this.w_CODATT = NVL(cp_ToDate(_read_.OLTCOATT),cp_NullValue(_read_.OLTCOATT))
        this.w_OLTCOCOS = NVL(cp_ToDate(_read_.OLTCOCOS),cp_NullValue(_read_.OLTCOCOS))
        this.w_SaldComm = NVL(cp_ToDate(_read_.OLTIMCOM),cp_NullValue(_read_.OLTIMCOM))
        this.w_OLTFORCO = NVL(cp_ToDate(_read_.OLTFORCO),cp_NullValue(_read_.OLTFORCO))
        this.w_OLTFCOCO = NVL(cp_ToDate(_read_.OLTFCOCO),cp_NullValue(_read_.OLTFCOCO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SaldComm>0 and !empty(this.w_CODATT) and !empty(this.w_OLTCOCOS) and !empty(this.w_OLTFORCO+this.w_OLTFCOCO)
        this.w_SaldProg = -this.w_SaldProg
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_OLTFCOCO,'CSCONSUN','this.w_SaldProg',this.w_SaldProg,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_OLTFORCO,'CSORDIN','this.w_SaldProg',this.w_SaldProg,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
          +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC("A");
              +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(this.w_OLTCOCOS);
                 )
        else
          update (i_cTable) set;
              CSCONSUN = &i_cOp1.;
              ,CSORDIN = &i_cOp2.;
              &i_ccchkf. ;
           where;
              CSCODCOM = this.w_CODCOM;
              and CSTIPSTR = "A";
              and CSCODMAT = this.w_CODATT;
              and CSCODCOS = this.w_OLTCOCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- Delete from ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      delete from (i_cTable) where;
            OLCODODL = this.w_OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    endscan
    return
  proc Try_05993D20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from SCCI_ASF
    i_nConn=i_TableProp[this.SCCI_ASF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCCI_ASF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='COD_ODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS COD_ODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".COD_ODL = "+i_cQueryTable+".COD_ODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".COD_ODL = "+i_cQueryTable+".COD_ODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MOUT
    i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='MOCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS MOCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".MOCODODL = "+i_cQueryTable+".MOCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MOCODODL = "+i_cQueryTable+".MOCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MAIN
    i_nConn=i_TableProp[this.ODL_MAIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='MICODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS MICODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".MICODODL = "+i_cQueryTable+".MICODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MICODODL = "+i_cQueryTable+".MICODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISO
    i_nConn=i_TableProp[this.ODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='RLCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS RLCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".RLCODODL = "+i_cQueryTable+".RLCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".RLCODODL = "+i_cQueryTable+".RLCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODLMRISO
    i_nConn=i_TableProp[this.ODLMRISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='RLCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS RLCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".RLCODODL = "+i_cQueryTable+".RLCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".RLCODODL = "+i_cQueryTable+".RLCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISF
    i_nConn=i_TableProp[this.ODL_RISF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='RFCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS RFCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".RFCODODL = "+i_cQueryTable+".RFCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".RFCODODL = "+i_cQueryTable+".RFCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_CICL
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='CLCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS CLCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".CLCODODL = "+i_cQueryTable+".CLCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".CLCODODL = "+i_cQueryTable+".CLCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='OLCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_SMPL
    i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='SMCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL AS SMCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".SMCODODL = "+i_cQueryTable+".SMCODODL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".SMCODODL = "+i_cQueryTable+".SMCODODL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_05994C50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO);
             )
    else
      delete from (i_cTable) where;
            OLTSTATO = this.w_OLTSTATO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_057F5F08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_057F6F58
    bErr_057F6F58=bTrsErr
    this.Try_057F6F58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if not empty(m.codatt) and not empty(m.catipcos)
        * --- accept error
        bTrsErr=.f.
        * --- Inserisce il Saldo di commessa
        * --- Try
        local bErr_05809400
        bErr_05809400=bTrsErr
        this.Try_05809400()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error="ins odl_mast"
          return
        endif
        bTrsErr=bTrsErr or bErr_05809400
        * --- End
      else
        * --- Raise
        i_Error="ins odl_mast"
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_057F6F58
    * --- End
    if m.datini < i_DATSYS
      this.w_LogErrori = "ODL-RIT"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if mpserr
      this.w_LogErrori = "MPSERR"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if not (m.datobso > m.dateva or empty(m.datobso)) or not (m.datobso > i_datsys or empty(m.datobso))
      this.w_LogErrori = "ARTOBSO"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if g_PRFA="S" and g_CICLILAV="S"
      if not empty(this.w_SERCIC) and used("TmpCic")
        select TmpCic
        scan
        this.w_CLFASSEL = iif(clindfas="00","S","N")
        this.w_CLCOUPOI = iif(clultfas="S", "S", clcoupoi)
        this.w_CLFASOUT = nvl(clfasout, "N")
        this.w_CLFASCLA = NVL(CLFASCLA, "N")
        if this.w_nRecFasi = 1 AND NVL(CLFASCLA, "N")<>NVL(CLFASCLO, "N")
          this.w_CLFASCLA = NVL(CLFASCLO, "N")
        endif
        this.w_CLCODFAS = NVL(CLCODFAS, SPACE(20))
        this.w_CLFASCOS = iif(this.w_CLCOUPOI="S","S",clfascos)
        this.w_WIPFASOLD = nvl(clmagwip, space(5))
        * --- Insert into ODL_CICL
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CLDESFAS"+",CLQTAPRE"+",CLPREUM1"+",CLQTAAVA"+",CLAVAUM1"+",CLFASEVA"+",CLCOUPOI"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLWIPOUT"+",CLWIPFPR"+",CLBFRIFE"+",CL__FASE"+",CLFASCOS"+",CLCODFAS"+",CLFASOUT"+",CLFASCLA"+",CLWIPDES"+",CLPRIFAS"+",CLLTFASE"+",CLMAGWIP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_CICL','CLCODODL');
          +","+cp_NullLink(cp_ToStrODBC(cprownum),'ODL_CICL','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(cprownum),'ODL_CICL','CLROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(cproword),'ODL_CICL','CLROWORD');
          +","+cp_NullLink(cp_ToStrODBC(clindfas),'ODL_CICL','CLINDPRE');
          +","+cp_NullLink(cp_ToStrODBC(clfasdes),'ODL_CICL','CLDESFAS');
          +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_CICL','CLQTAPRE');
          +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_CICL','CLPREUM1');
          +","+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLQTAAVA');
          +","+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLAVAUM1');
          +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_CICL','CLFASEVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLCOUPOI),'ODL_CICL','CLCOUPOI');
          +","+cp_NullLink(cp_ToStrODBC(clultfas),'ODL_CICL','CLULTFAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASSEL),'ODL_CICL','CLFASSEL');
          +","+cp_NullLink(cp_ToStrODBC(clcprife),'ODL_CICL','CLCPRIFE');
          +","+cp_NullLink(cp_ToStrODBC(clcodfas),'ODL_CICL','CLWIPOUT');
          +","+cp_NullLink(cp_ToStrODBC(clwipfpr),'ODL_CICL','CLWIPFPR');
          +","+cp_NullLink(cp_ToStrODBC(clbfrife),'ODL_CICL','CLBFRIFE');
          +","+cp_NullLink(cp_ToStrODBC(cl__fase),'ODL_CICL','CL__FASE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCOS),'ODL_CICL','CLFASCOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODFAS),'ODL_CICL','CLCODFAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASOUT),'ODL_CICL','CLFASOUT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCLA),'ODL_CICL','CLFASCLA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_WIPFASPRE),'ODL_CICL','CLWIPDES');
          +","+cp_NullLink(cp_ToStrODBC(clprifas),'ODL_CICL','CLPRIFAS');
          +","+cp_NullLink(cp_ToStrODBC(clltfase),'ODL_CICL','CLLTFASE');
          +","+cp_NullLink(cp_ToStrODBC(clmagwip),'ODL_CICL','CLMAGWIP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CLCODODL',this.w_OLCODODL,'CPROWNUM',cprownum,'CLROWNUM',cprownum,'CLROWORD',cproword,'CLINDPRE',clindfas,'CLDESFAS',clfasdes,'CLQTAPRE',m.quanti,'CLPREUM1',m.quanti,'CLQTAAVA',0,'CLAVAUM1',0,'CLFASEVA'," ",'CLCOUPOI',this.w_CLCOUPOI)
          insert into (i_cTable) (CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CLDESFAS,CLQTAPRE,CLPREUM1,CLQTAAVA,CLAVAUM1,CLFASEVA,CLCOUPOI,CLULTFAS,CLFASSEL,CLCPRIFE,CLWIPOUT,CLWIPFPR,CLBFRIFE,CL__FASE,CLFASCOS,CLCODFAS,CLFASOUT,CLFASCLA,CLWIPDES,CLPRIFAS,CLLTFASE,CLMAGWIP &i_ccchkf. );
             values (;
               this.w_OLCODODL;
               ,cprownum;
               ,cprownum;
               ,cproword;
               ,clindfas;
               ,clfasdes;
               ,m.quanti;
               ,m.quanti;
               ,0;
               ,0;
               ," ";
               ,this.w_CLCOUPOI;
               ,clultfas;
               ,this.w_CLFASSEL;
               ,clcprife;
               ,clcodfas;
               ,clwipfpr;
               ,clbfrife;
               ,cl__fase;
               ,this.w_CLFASCOS;
               ,this.w_CLCODFAS;
               ,this.w_CLFASOUT;
               ,this.w_CLFASCLA;
               ,this.w_WIPFASPRE;
               ,clprifas;
               ,clltfase;
               ,clmagwip;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_WIPFASPRE = this.w_WIPFASOLD
        endscan
      endif
      * --- Cursore per il riferimento dei materiali alle fasi
      select midibrow, clinival, clfinval, cproword, clcprife ;
      from Cicli where clserial=this.w_SERCIC into cursor TmpCic
    endif
    return
  proc Try_05859DC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_COMMDETT = iif(NVL(m.arflcomm,"N")="S" and ! empty(nvl(m.codcom," ")),m.codcom,space(15))
    this.w_MAGIMP = iif(this.w_MICOM="M",m.armagpre,iif(this.w_MICOM="A",iif(empty(nvl(m.mgmagimp," ")),m.armagpre,m.mgmagimp),this.w_MAGFOR))
    do case
      case this.w_MICOM = "M"
        * --- Magazzino preferenziale
        this.w_MAGIMP = m.armagpre
      case this.w_MICOM = "A"
        this.w_MAGIMP = iif(empty(nvl(m.mgmagimp," ")),m.armagpre,m.mgmagimp)
      case this.w_MICOM = "T"
        * --- Criterio di pianificazione
        this.w_MAGIMP = this.w_MAGRIF
      otherwise
        * --- Forza Magazzino
        this.w_MAGIMP = this.w_MAGFOR
    endcase
    this.w_OLTCODIC = LEFT(NVL(this.w_OLTCODIC, SPACE(20)), 20)
    this.w_OLTKEYSA = LEFT(NVL(m.codsal, SPACE(20)), 20)
    this.w_OLFLEVAS = IIF(this.w_OLFLEVAS="N", " ", this.w_OLFLEVAS)
    this.w_OLTIPPRE = IIF(this.w_OLFLEVAS="S", "N", m.dpmodpre)
    this.w_OLEVAAUT = IIF(NVL(m.evaaut, " ")<>"S", " ", "S")
    if g_PRFA<>"S" OR g_CICLILAV<>"S"
      this.w_OLRIFFAS = NVL(m.dbriffas, 0)
      * --- Insert into ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLCODART"+",OLCODICE"+",OLCODMAG"+",OLCOEIMP"+",OLDATRIC"+",OLEVAAUT"+",OLFLEVAS"+",OLFLIMPE"+",OLFLORDI"+",OLFLPREV"+",OLFLRISE"+",OLKEYSAL"+",OLMAGPRE"+",OLMAGWIP"+",OLPROFAN"+",OLQTAEV1"+",OLQTAEVA"+",OLQTAMOV"+",OLQTAPR1"+",OLQTAPRE"+",OLQTASAL"+",OLQTAUM1"+",OLRIFDOC"+",OLRIFFAS"+",OLCPRIFE"+",OLROWDOC"+",OLTIPPRE"+",OLUNIMIS"+",OLCORRLT"+",OLCODCOM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_DETT','OLCODODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ODL_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ODL_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PPCAUIMP),'ODL_DETT','OLCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(m.codart),'ODL_DETT','OLCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_DETT','OLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMP),'ODL_DETT','OLCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(m.cocoeimp),'ODL_DETT','OLCOEIMP');
        +","+cp_NullLink(cp_ToStrODBC(m.datini),'ODL_DETT','OLDATRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFLIMP),'ODL_DETT','OLFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFLORD),'ODL_DETT','OLFLORDI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFLRIS),'ODL_DETT','OLFLRISE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_DETT','OLKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMP),'ODL_DETT','OLMAGPRE');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'ODL_DETT','OLMAGWIP');
        +","+cp_NullLink(cp_ToStrODBC(m.profan),'ODL_DETT','OLPROFAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
        +","+cp_NullLink(cp_ToStrODBC(-m.quanti),'ODL_DETT','OLQTASAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ODL_DETT','OLRIFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLRIFFAS),'ODL_DETT','OLRIFFAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLCPRIFE),'ODL_DETT','OLCPRIFE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLROWDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
        +","+cp_NullLink(cp_ToStrODBC(m.counimis),'ODL_DETT','OLUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(m.cocorlea),'ODL_DETT','OLCORRLT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDETT),'ODL_DETT','OLCODCOM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'OLCAUMAG',this.w_PPCAUIMP,'OLCODART',m.codart,'OLCODICE',this.w_OLTCODIC,'OLCODMAG',this.w_MAGIMP,'OLCOEIMP',m.cocoeimp,'OLDATRIC',m.datini,'OLEVAAUT',this.w_OLEVAAUT,'OLFLEVAS',this.w_OLFLEVAS,'OLFLIMPE',this.w_IMPFLIMP)
        insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLCODART,OLCODICE,OLCODMAG,OLCOEIMP,OLDATRIC,OLEVAAUT,OLFLEVAS,OLFLIMPE,OLFLORDI,OLFLPREV,OLFLRISE,OLKEYSAL,OLMAGPRE,OLMAGWIP,OLPROFAN,OLQTAEV1,OLQTAEVA,OLQTAMOV,OLQTAPR1,OLQTAPRE,OLQTASAL,OLQTAUM1,OLRIFDOC,OLRIFFAS,OLCPRIFE,OLROWDOC,OLTIPPRE,OLUNIMIS,OLCORRLT,OLCODCOM &i_ccchkf. );
           values (;
             this.w_OLCODODL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_PPCAUIMP;
             ,m.codart;
             ,this.w_OLTCODIC;
             ,this.w_MAGIMP;
             ,m.cocoeimp;
             ,m.datini;
             ,this.w_OLEVAAUT;
             ,this.w_OLFLEVAS;
             ,this.w_IMPFLIMP;
             ,this.w_IMPFLORD;
             ," ";
             ,this.w_IMPFLRIS;
             ,this.w_OLTKEYSA;
             ,this.w_MAGIMP;
             ,SPACE(5);
             ,m.profan;
             ,this.w_OLQTAEV1;
             ,this.w_OLQTAEVA;
             ,this.w_OLQTAMOV;
             ,0;
             ,0;
             ,-m.quanti;
             ,this.w_OLQTAUM1;
             ,SPACE(10);
             ,this.w_OLRIFFAS;
             ,this.w_OLCPRIFE;
             ,0;
             ,this.w_OLTIPPRE;
             ,m.counimis;
             ,m.cocorlea;
             ,this.w_COMMDETT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLCODART"+",OLCODICE"+",OLCODMAG"+",OLCOEIMP"+",OLDATRIC"+",OLEVAAUT"+",OLFLEVAS"+",OLFLIMPE"+",OLFLORDI"+",OLFLPREV"+",OLFLRISE"+",OLKEYSAL"+",OLMAGPRE"+",OLMAGWIP"+",OLPROFAN"+",OLQTAEV1"+",OLQTAEVA"+",OLQTAMOV"+",OLQTAPR1"+",OLQTAPRE"+",OLQTASAL"+",OLQTAUM1"+",OLRIFDOC"+",OLFASRIF"+",OLCPRIFE"+",OLROWDOC"+",OLTIPPRE"+",OLUNIMIS"+",OLCORRLT"+",OLCODCOM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_DETT','OLCODODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ODL_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ODL_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PPCAUIMP),'ODL_DETT','OLCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(m.codart),'ODL_DETT','OLCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_DETT','OLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMP),'ODL_DETT','OLCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(m.cocoeimp),'ODL_DETT','OLCOEIMP');
        +","+cp_NullLink(cp_ToStrODBC(m.datini),'ODL_DETT','OLDATRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFLIMP),'ODL_DETT','OLFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFLORD),'ODL_DETT','OLFLORDI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFLRIS),'ODL_DETT','OLFLRISE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_DETT','OLKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMP),'ODL_DETT','OLMAGPRE');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'ODL_DETT','OLMAGWIP');
        +","+cp_NullLink(cp_ToStrODBC(m.profan),'ODL_DETT','OLPROFAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
        +","+cp_NullLink(cp_ToStrODBC(-m.quanti),'ODL_DETT','OLQTASAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ODL_DETT','OLRIFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLRIFFAS),'ODL_DETT','OLFASRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLCPRIFE),'ODL_DETT','OLCPRIFE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLROWDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
        +","+cp_NullLink(cp_ToStrODBC(m.counimis),'ODL_DETT','OLUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(m.cocorlea),'ODL_DETT','OLCORRLT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDETT),'ODL_DETT','OLCODCOM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'OLCAUMAG',this.w_PPCAUIMP,'OLCODART',m.codart,'OLCODICE',this.w_OLTCODIC,'OLCODMAG',this.w_MAGIMP,'OLCOEIMP',m.cocoeimp,'OLDATRIC',m.datini,'OLEVAAUT',this.w_OLEVAAUT,'OLFLEVAS',this.w_OLFLEVAS,'OLFLIMPE',this.w_IMPFLIMP)
        insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLCODART,OLCODICE,OLCODMAG,OLCOEIMP,OLDATRIC,OLEVAAUT,OLFLEVAS,OLFLIMPE,OLFLORDI,OLFLPREV,OLFLRISE,OLKEYSAL,OLMAGPRE,OLMAGWIP,OLPROFAN,OLQTAEV1,OLQTAEVA,OLQTAMOV,OLQTAPR1,OLQTAPRE,OLQTASAL,OLQTAUM1,OLRIFDOC,OLFASRIF,OLCPRIFE,OLROWDOC,OLTIPPRE,OLUNIMIS,OLCORRLT,OLCODCOM &i_ccchkf. );
           values (;
             this.w_OLCODODL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_PPCAUIMP;
             ,m.codart;
             ,this.w_OLTCODIC;
             ,this.w_MAGIMP;
             ,m.cocoeimp;
             ,m.datini;
             ,this.w_OLEVAAUT;
             ,this.w_OLFLEVAS;
             ,this.w_IMPFLIMP;
             ,this.w_IMPFLORD;
             ," ";
             ,this.w_IMPFLRIS;
             ,this.w_OLTKEYSA;
             ,this.w_MAGIMP;
             ,SPACE(5);
             ,m.profan;
             ,this.w_OLQTAEV1;
             ,this.w_OLQTAEVA;
             ,this.w_OLQTAMOV;
             ,0;
             ,0;
             ,-m.quanti;
             ,this.w_OLQTAUM1;
             ,SPACE(10);
             ,this.w_OLRIFFAS;
             ,this.w_OLCPRIFE;
             ,0;
             ,this.w_OLTIPPRE;
             ,m.counimis;
             ,m.cocorlea;
             ,this.w_COMMDETT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if not (m.datobso > m.datini or empty(m.datobso)) or not (m.datobso > i_datsys or empty(m.datobso))
      this.w_LogErrori = "CMPOBSO"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_054C8E68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_054C9C18
    bErr_054C9C18=bTrsErr
    this.Try_054C9C18()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if not empty(m.codatt) and not empty(m.catipcos)
        * --- accept error
        bTrsErr=.f.
        * --- Inserisce il Saldo di commessa
        * --- Try
        local bErr_058E2B88
        bErr_058E2B88=bTrsErr
        this.Try_058E2B88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error="ins odl_mast"
          return
        endif
        bTrsErr=bTrsErr or bErr_058E2B88
        * --- End
      else
        * --- Raise
        i_Error="ins odl_mast"
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_054C9C18
    * --- End
    if m.datini < i_DATSYS
      this.w_LogErrori = "ODL-RIT"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if mpserr
      this.w_LogErrori = "MPSERR"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if not (m.datobso > m.dateva or empty(m.datobso)) or not (m.datobso > i_datsys or empty(m.datobso))
      this.w_LogErrori = "ODAOBSO"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_057B1C10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_PRFA="S" and g_CICLILAV="S"
      * --- Aggiorna codice distinta base su ordine di lavorazione
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OLCODODL"
        do vq_exec with 'gsmrxbgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTVOCEN = _t2.OLTVOCEN";
            +",OLTDISBA = _t2.OLTDISBA";
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
            +"ODL_MAST.OLTVOCEN = _t2.OLTVOCEN";
            +",ODL_MAST.OLTDISBA = _t2.OLTDISBA";
            +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
            +"OLTVOCEN,";
            +"OLTDISBA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.OLTVOCEN,";
            +"t2.OLTDISBA";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
            +"OLTVOCEN = _t2.OLTVOCEN";
            +",OLTDISBA = _t2.OLTDISBA";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTVOCEN = (select OLTVOCEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",OLTDISBA = (select OLTDISBA from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Risorse della Fase
      ah_msg("Assegnamento risorse di fase in corso...")
      this.w_SERODL = SPACE(15)
      this.w_OLTSTATODP = "*"
      * --- Inserisco Risorse Master
      * --- Create temporary table TMPGSDB_MRL
      i_nIdx=cp_AddTableDef('TMPGSDB_MRL') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_DTCUCTYKAX[2]
      indexes_DTCUCTYKAX[1]='OLTSECIC,CLROWNUM'
      indexes_DTCUCTYKAX[2]='CLCODODL'
      vq_exec('..\PRFA\EXE\QUERY\GSCI1MRL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_DTCUCTYKAX,.f.)
      this.TMPGSDB_MRL_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Insert into ODLMRISO
      i_nConn=i_TableProp[this.ODLMRISO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCI3MRL",this.ODLMRISO_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMPGSDB_MRL
      i_nIdx=cp_GetTableDefIdx('TMPGSDB_MRL')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPGSDB_MRL')
      endif
      * --- Inserisco Risorse Detail
      * --- Create temporary table TMPGSDB_MRL
      i_nIdx=cp_AddTableDef('TMPGSDB_MRL') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_LKCNEVFZDN[2]
      indexes_LKCNEVFZDN[1]='OLTSECIC,CLROWNUM'
      indexes_LKCNEVFZDN[2]='CLCODODL'
      vq_exec('..\PRFA\EXE\QUERY\GSCI2MRL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_LKCNEVFZDN,.f.)
      this.TMPGSDB_MRL_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Insert into ODL_RISO
      i_nConn=i_TableProp[this.ODL_RISO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCI4MRL",this.ODL_RISO_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMPGSDB_MRL
      i_nIdx=cp_GetTableDefIdx('TMPGSDB_MRL')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPGSDB_MRL')
      endif
    else
      ah_Msg("Assegnamento ciclo semplificato in corso...",.T.)
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OLCODODL"
        do vq_exec with 'gsmrxbgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTVOCEN = _t2.OLTVOCEN";
            +",OLTDISBA = _t2.OLTDISBA";
            +",OLTCICLO = _t2.OLTCICLO";
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
            +"ODL_MAST.OLTVOCEN = _t2.OLTVOCEN";
            +",ODL_MAST.OLTDISBA = _t2.OLTDISBA";
            +",ODL_MAST.OLTCICLO = _t2.OLTCICLO";
            +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
            +"OLTVOCEN,";
            +"OLTDISBA,";
            +"OLTCICLO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.OLTVOCEN,";
            +"t2.OLTDISBA,";
            +"t2.OLTCICLO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
            +"OLTVOCEN = _t2.OLTVOCEN";
            +",OLTDISBA = _t2.OLTDISBA";
            +",OLTCICLO = _t2.OLTCICLO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTVOCEN = (select OLTVOCEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",OLTDISBA = (select OLTDISBA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",OLTCICLO = (select OLTCICLO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Riferimento au componenti senza Fase (Inserisce Ultima)
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OLCODODL,CPROWNUM"
        do vq_exec with 'gsmrybgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
                +" and "+"ODL_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLRIFFAS = _t2.OLRIFFAS";
            +i_ccchkf;
            +" from "+i_cTable+" ODL_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
                +" and "+"ODL_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT, "+i_cQueryTable+" _t2 set ";
            +"ODL_DETT.OLRIFFAS = _t2.OLRIFFAS";
            +Iif(Empty(i_ccchkf),"",",ODL_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_DETT.OLCODODL = t2.OLCODODL";
                +" and "+"ODL_DETT.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT set (";
            +"OLRIFFAS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.OLRIFFAS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
                +" and "+"ODL_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT set ";
            +"OLRIFFAS = _t2.OLRIFFAS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLRIFFAS = (select OLRIFFAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Inserisco cicli semplificati sull'odl
      * --- Insert into ODL_SMPL
      i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS_BGP",this.ODL_SMPL_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_057BA478()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Materiali di output
    ah_msg("Assegnamento materiali di output in corso...")
    this.w_OLTSTATODP = "*"
    * --- Inserisco Materiali di output
    if g_PRFA="S" and g_CICLILAV="S"
      * --- Create temporary table TMPGSCO_MMO
      i_nIdx=cp_AddTableDef('TMPGSCO_MMO') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_AIAWNRIECO[2]
      indexes_AIAWNRIECO[1]='MOSERIAL,MOROWNUM'
      indexes_AIAWNRIECO[2]='MOCODODL'
      vq_exec('GSMR_MOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_AIAWNRIECO,.f.)
      this.TMPGSCO_MMO_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPGSCO_MMO
      i_nIdx=cp_AddTableDef('TMPGSCO_MMO') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_SJJORXZPSM[2]
      indexes_SJJORXZPSM[1]='MOSERIAL,MOROWNUM'
      indexes_SJJORXZPSM[2]='MOCODODL'
      vq_exec('..\COLA\EXE\QUERY\GSMRDMOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_SJJORXZPSM,.f.)
      this.TMPGSCO_MMO_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Insert into ODL_MOUT
    i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR1MOU",this.ODL_MOUT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPGSCO_MMO
    i_nIdx=cp_GetTableDefIdx('TMPGSCO_MMO')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPGSCO_MMO')
    endif
    if g_PRFA="S" and g_CICLILAV="S"
      * --- Associo il materiale di output alla fase, solo se l'ODL ha il ciclo e se la fase non � gi� stata assegnata manualmente nei materiali di output del ciclo stesso.
      *     Se w_MATOUP � 'P' saranno tutti associati alla prima fase, altrimenti all'ultima fase. Se w_MATOUP � 'N' i materiali di output non vengono gestiti a livello di fase
      *     e sono gi� filtrati dalla query GSMR_MOU
      * --- Write into ODL_MOUT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MOCODODL,CPROWNUM"
        do vq_exec with 'GSMR2MOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MOUT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MOROWNUM = _t2.CPROWFS";
            +",MOFASRIF = _t2.ROWORDFS ";
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MOUT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT, "+i_cQueryTable+" _t2 set ";
            +"ODL_MOUT.MOROWNUM = _t2.CPROWFS";
            +",ODL_MOUT.MOFASRIF = _t2.ROWORDFS ";
            +Iif(Empty(i_ccchkf),"",",ODL_MOUT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_MOUT.MOCODODL = t2.MOCODODL";
                +" and "+"ODL_MOUT.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT set (";
            +"MOROWNUM,";
            +"MOFASRIF";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWFS,";
            +"t2.ROWORDFS ";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT set ";
            +"MOROWNUM = _t2.CPROWFS";
            +",MOFASRIF = _t2.ROWORDFS ";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MOCODODL = "+i_cQueryTable+".MOCODODL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MOROWNUM = (select CPROWFS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MOFASRIF = (select ROWORDFS  from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_057C6808()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRSBGP3",this.SALDIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_057C8D28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRSBGP3C",this.SALDICOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_057F6F58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OLTCODIC = LEFT(NVL(codric, SPACE(20)), 20)
    this.w_OLTKEYSA = LEFT(NVL(m.codsal, SPACE(20)), 20)
    do case
      case (m.arpropre="I" and this.w_MIODL = "M") or (m.arpropre="L" and this.w_MIOCL = "M")
        * --- Magazzino preferenziale
        this.w_MAGRIF = m.armagpre
      case (m.arpropre="I" and this.w_MIODL = "C") or (m.arpropre="L" and this.w_MIOCL = "C")
        * --- Criterio di pianificazione
        do case
          case this.w_CRIELA = "G"
            this.w_MAGRIF = m.grumag
          case this.w_CRIELA = "M"
            this.w_MAGRIF = m.codmag
          otherwise
            this.w_MAGRIF = m.armagpre
        endcase
      otherwise
        * --- Forza Magazzino
        this.w_MAGRIF = IIF(m.arpropre="I" , this.w_MAGFOC, this.w_MAGFOL)
    endcase
    * --- Insert into ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",OLDATODL"+",OLDATODP"+",OLOPEODL"+",OLOPEODP"+",OLTCAMAG"+",OLTCOART"+",OLTCOATT"+",OLTCOCEN"+",OLTCOCOS"+",OLTCODIC"+",OLTCOFOR"+",OLTCOMAG"+",OLTCOMME"+",OLTCONTR"+",OLTDINRIC"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLEVA"+",OLTFLIMC"+",OLTFLIMP"+",OLTFLORD"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",OLTVARIA"+",UTCC"+",UTDC"+",OLSERMRP"+",OLTSAFLT"+",OLTDTCON"+",OLTSECIC"+",OLTDISBA"+",OLTCRELA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_MAST','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PPCAUORD),'ODL_MAST','OLTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.codart),'ODL_MAST','OLTCOART');
      +","+cp_NullLink(cp_ToStrODBC(m.codatt),'ODL_MAST','OLTCOATT');
      +","+cp_NullLink(cp_ToStrODBC(m.arcencos),'ODL_MAST','OLTCOCEN');
      +","+cp_NullLink(cp_ToStrODBC(m.catipcos),'ODL_MAST','OLTCOCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_MAST','OLTCODIC');
      +","+cp_NullLink(cp_ToStrODBC(m.prcodfor),'ODL_MAST','OLTCOFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGRIF),'ODL_MAST','OLTCOMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.codcom),'ODL_MAST','OLTCOMME');
      +","+cp_NullLink(cp_ToStrODBC(m.contra),'ODL_MAST','OLTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(m.datini),'ODL_MAST','OLTDINRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.datmps),'ODL_MAST','OLTDTMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.dateva),'ODL_MAST','OLTDTRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.leapro),'ODL_MAST','OLTEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFCOCO),'ODL_MAST','OLTFCOCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMC),'ODL_MAST','OLTFLIMC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLIMP),'ODL_MAST','OLTFLIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLORD),'ODL_MAST','OLTFLORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFORCO),'ODL_MAST','OLTFORCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ImpComm),'ODL_MAST','OLTIMCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_MAST','OLTKEYSA');
      +","+cp_NullLink(cp_ToStrODBC(m.prleamps),'ODL_MAST','OLTLEMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.tpperass),'ODL_MAST','OLTPERAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTOD1');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTODL');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTSAL');
      +","+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTSTATO');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
      +","+cp_NullLink(cp_ToStrODBC(m.arunmis1),'ODL_MAST','OLTUNMIS');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Seriale),'ODL_MAST','OLSERMRP');
      +","+cp_NullLink(cp_ToStrODBC(m.prsafelt),'ODL_MAST','OLTSAFLT');
      +","+cp_NullLink(cp_ToStrODBC(m.datcon),'ODL_MAST','OLTDTCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERCIC),'ODL_MAST','OLTSECIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDISBA),'ODL_MAST','OLTDISBA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRIELA),'ODL_MAST','OLTCRELA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'OLDATODL',i_DATSYS,'OLDATODP',i_DATSYS,'OLOPEODL',i_CODUTE,'OLOPEODP',i_CODUTE,'OLTCAMAG',this.w_PPCAUORD,'OLTCOART',m.codart,'OLTCOATT',m.codatt,'OLTCOCEN',m.arcencos,'OLTCOCOS',m.catipcos,'OLTCODIC',this.w_OLTCODIC,'OLTCOFOR',m.prcodfor)
      insert into (i_cTable) (OLCODODL,OLDATODL,OLDATODP,OLOPEODL,OLOPEODP,OLTCAMAG,OLTCOART,OLTCOATT,OLTCOCEN,OLTCOCOS,OLTCODIC,OLTCOFOR,OLTCOMAG,OLTCOMME,OLTCONTR,OLTDINRIC,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLEVA,OLTFLIMC,OLTFLIMP,OLTFLORD,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,OLTVARIA,UTCC,UTDC,OLSERMRP,OLTSAFLT,OLTDTCON,OLTSECIC,OLTDISBA,OLTCRELA &i_ccchkf. );
         values (;
           this.w_OLCODODL;
           ,i_DATSYS;
           ,i_DATSYS;
           ,i_CODUTE;
           ,i_CODUTE;
           ,this.w_PPCAUORD;
           ,m.codart;
           ,m.codatt;
           ,m.arcencos;
           ,m.catipcos;
           ,this.w_OLTCODIC;
           ,m.prcodfor;
           ,this.w_MAGRIF;
           ,m.codcom;
           ,m.contra;
           ,m.datini;
           ,m.datmps;
           ,m.dateva;
           ,m.leapro;
           ,this.w_OLTFCOCO;
           ," ";
           ,this.w_OLTFLIMC;
           ,this.w_ORDFLIMP;
           ,this.w_ORDFLORD;
           ,this.w_OLTFORCO;
           ,this.w_ImpComm;
           ,this.w_OLTKEYSA;
           ,m.prleamps;
           ,m.tpperass;
           ,this.w_OLTPROVE;
           ,m.quanti;
           ,m.quanti;
           ,0;
           ,0;
           ,m.quanti;
           ,"+";
           ,"F";
           ,"A";
           ,m.arunmis1;
           ,"N";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,this.w_Seriale;
           ,m.prsafelt;
           ,m.datcon;
           ,this.w_SERCIC;
           ,this.w_OLTDISBA;
           ,this.w_CRIELA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05809400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(m.codcom),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(m.codatt),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(m.catipcos),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',m.codcom,'CSTIPSTR',"A",'CSCODMAT',m.codatt,'CSCODCOS',m.catipcos)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           m.codcom;
           ,"A";
           ,m.codatt;
           ,m.catipcos;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_OLTCODIC = LEFT(NVL(codric, SPACE(20)), 20)
    this.w_OLTKEYSA = LEFT(NVL(m.codsal, SPACE(20)), 20)
    do case
      case (m.arpropre="I" and this.w_MIODL = "M") or (m.arpropre="L" and this.w_MIOCL = "M")
        * --- Magazzino preferenziale
        this.w_MAGRIF = m.armagpre
      case (m.arpropre="I" and this.w_MIODL = "C") or (m.arpropre="L" and this.w_MIOCL = "C")
        * --- Criterio di pianificazione
        do case
          case this.w_CRIELA = "G"
            this.w_MAGRIF = m.grumag
          case this.w_CRIELA = "M"
            this.w_MAGRIF = m.codmag
          otherwise
            this.w_MAGRIF = m.armagpre
        endcase
      otherwise
        * --- Forza Magazzino
        this.w_MAGRIF = IIF(m.arpropre="I" , this.w_MAGFOC, this.w_MAGFOL)
    endcase
    * --- Insert into ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",OLDATODP"+",OLOPEODP"+",OLDATODL"+",OLOPEODL"+",OLTSTATO"+",OLTCAMAG"+",OLTFLORD"+",OLTFLIMP"+",OLTCOMAG"+",OLTLEMPS"+",OLTDTMPS"+",OLTDINRIC"+",OLTDTRIC"+",OLTEMLAV"+",OLTCODIC"+",OLTCOART"+",OLTUNMIS"+",OLTQTODL"+",OLTQTOD1"+",OLTQTOEV"+",OLTQTOE1"+",OLTKEYSA"+",OLTQTSAL"+",UTCC"+",UTDC"+",OLTCOMME"+",OLTTIPAT"+",OLTCOATT"+",OLTCOCOS"+",OLTFCOCO"+",OLTFORCO"+",OLTPERAS"+",OLTPROVE"+",OLTIMCOM"+",OLTFLIMC"+",OLTTICON"+",OLTCOFOR"+",OLTCONTR"+",OLTFLEVA"+",OLTVARIA"+",OLTCOCEN"+",OLSERMRP"+",OLTSAFLT"+",OLTDTCON"+",OLTSECIC"+",OLTCRELA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_MAST','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
      +","+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTSTATO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PPCAUORD),'ODL_MAST','OLTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLORD),'ODL_MAST','OLTFLORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLIMP),'ODL_MAST','OLTFLIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGRIF),'ODL_MAST','OLTCOMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.prgioapp),'ODL_MAST','OLTLEMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.datmps),'ODL_MAST','OLTDTMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.datini),'ODL_MAST','OLTDINRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.dateva),'ODL_MAST','OLTDTRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.leapro),'ODL_MAST','OLTEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_MAST','OLTCODIC');
      +","+cp_NullLink(cp_ToStrODBC(m.codart),'ODL_MAST','OLTCOART');
      +","+cp_NullLink(cp_ToStrODBC(m.arunmis1),'ODL_MAST','OLTUNMIS');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTODL');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTOD1');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_MAST','OLTKEYSA');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTSAL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(m.codcom),'ODL_MAST','OLTCOMME');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
      +","+cp_NullLink(cp_ToStrODBC(m.codatt),'ODL_MAST','OLTCOATT');
      +","+cp_NullLink(cp_ToStrODBC(m.catipcos),'ODL_MAST','OLTCOCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFCOCO),'ODL_MAST','OLTFCOCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFORCO),'ODL_MAST','OLTFORCO');
      +","+cp_NullLink(cp_ToStrODBC(m.tpperass),'ODL_MAST','OLTPERAS');
      +","+cp_NullLink(cp_ToStrODBC(m.arpropre),'ODL_MAST','OLTPROVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ImpComm),'ODL_MAST','OLTIMCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMC),'ODL_MAST','OLTFLIMC');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
      +","+cp_NullLink(cp_ToStrODBC(m.prcodfor),'ODL_MAST','OLTCOFOR');
      +","+cp_NullLink(cp_ToStrODBC(m.contra),'ODL_MAST','OLTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
      +","+cp_NullLink(cp_ToStrODBC(m.arcencos),'ODL_MAST','OLTCOCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Seriale),'ODL_MAST','OLSERMRP');
      +","+cp_NullLink(cp_ToStrODBC(m.prsafelt),'ODL_MAST','OLTSAFLT');
      +","+cp_NullLink(cp_ToStrODBC(m.datcon),'ODL_MAST','OLTDTCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERCIC),'ODL_MAST','OLTSECIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRIELA),'ODL_MAST','OLTCRELA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'OLDATODP',i_DATSYS,'OLOPEODP',i_CODUTE,'OLDATODL',i_DATSYS,'OLOPEODL',i_CODUTE,'OLTSTATO',"+",'OLTCAMAG',this.w_PPCAUORD,'OLTFLORD',this.w_ORDFLORD,'OLTFLIMP',this.w_ORDFLIMP,'OLTCOMAG',this.w_MAGRIF,'OLTLEMPS',m.prgioapp,'OLTDTMPS',m.datmps)
      insert into (i_cTable) (OLCODODL,OLDATODP,OLOPEODP,OLDATODL,OLOPEODL,OLTSTATO,OLTCAMAG,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTLEMPS,OLTDTMPS,OLTDINRIC,OLTDTRIC,OLTEMLAV,OLTCODIC,OLTCOART,OLTUNMIS,OLTQTODL,OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTKEYSA,OLTQTSAL,UTCC,UTDC,OLTCOMME,OLTTIPAT,OLTCOATT,OLTCOCOS,OLTFCOCO,OLTFORCO,OLTPERAS,OLTPROVE,OLTIMCOM,OLTFLIMC,OLTTICON,OLTCOFOR,OLTCONTR,OLTFLEVA,OLTVARIA,OLTCOCEN,OLSERMRP,OLTSAFLT,OLTDTCON,OLTSECIC,OLTCRELA &i_ccchkf. );
         values (;
           this.w_OLCODODL;
           ,i_DATSYS;
           ,i_CODUTE;
           ,i_DATSYS;
           ,i_CODUTE;
           ,"+";
           ,this.w_PPCAUORD;
           ,this.w_ORDFLORD;
           ,this.w_ORDFLIMP;
           ,this.w_MAGRIF;
           ,m.prgioapp;
           ,m.datmps;
           ,m.datini;
           ,m.dateva;
           ,m.leapro;
           ,this.w_OLTCODIC;
           ,m.codart;
           ,m.arunmis1;
           ,m.quanti;
           ,m.quanti;
           ,0;
           ,0;
           ,this.w_OLTKEYSA;
           ,m.quanti;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,m.codcom;
           ,"A";
           ,m.codatt;
           ,m.catipcos;
           ,this.w_OLTFCOCO;
           ,this.w_OLTFORCO;
           ,m.tpperass;
           ,m.arpropre;
           ,this.w_ImpComm;
           ,this.w_OLTFLIMC;
           ,"F";
           ,m.prcodfor;
           ,m.contra;
           ," ";
           ,"N";
           ,m.arcencos;
           ,this.w_Seriale;
           ,m.prsafelt;
           ,m.datcon;
           ,this.w_SERCIC;
           ,this.w_CRIELA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_054C9C18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OLTCODIC = LEFT(NVL(codric, SPACE(20)), 20)
    this.w_OLTKEYSA = LEFT(NVL(m.codsal, SPACE(20)), 20)
    do case
      case this.w_MIODA = "M"
        * --- Magazzino preferenziale
        this.w_MAGRIF = m.armagpre
      case this.w_MIODA = "C"
        * --- Criterio di pianificazione
        do case
          case this.w_CRIELA = "G"
            this.w_MAGRIF = m.grumag
          case this.w_CRIELA = "M"
            this.w_MAGRIF = m.codmag
          otherwise
            this.w_MAGRIF = m.armagpre
        endcase
      otherwise
        * --- Forza Magazzino
        this.w_MAGRIF = this.w_MAGFOA
    endcase
    * --- Insert into ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",OLDATODL"+",OLDATODP"+",OLOPEODL"+",OLOPEODP"+",OLTCAMAG"+",OLTCOART"+",OLTCOATT"+",OLTCOCEN"+",OLTCOCOS"+",OLTCODIC"+",OLTCOFOR"+",OLTCOMAG"+",OLTCOMME"+",OLTCONTR"+",OLTDINRIC"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLEVA"+",OLTFLIMC"+",OLTFLIMP"+",OLTFLORD"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",OLTVARIA"+",UTCC"+",UTDC"+",OLCRIFOR"+",OLSERMRP"+",OLTSAFLT"+",OLTDTCON"+",OLTSECIC"+",OLTCRELA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_MAST','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PPCAUORD),'ODL_MAST','OLTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.codart),'ODL_MAST','OLTCOART');
      +","+cp_NullLink(cp_ToStrODBC(m.codatt),'ODL_MAST','OLTCOATT');
      +","+cp_NullLink(cp_ToStrODBC(m.arcencos),'ODL_MAST','OLTCOCEN');
      +","+cp_NullLink(cp_ToStrODBC(m.catipcos),'ODL_MAST','OLTCOCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_MAST','OLTCODIC');
      +","+cp_NullLink(cp_ToStrODBC(m.prcodfor),'ODL_MAST','OLTCOFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGRIF),'ODL_MAST','OLTCOMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.codcom),'ODL_MAST','OLTCOMME');
      +","+cp_NullLink(cp_ToStrODBC(m.contra),'ODL_MAST','OLTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(m.datini),'ODL_MAST','OLTDINRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.datmps),'ODL_MAST','OLTDTMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.dateva),'ODL_MAST','OLTDTRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.leapro),'ODL_MAST','OLTEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFCOCO),'ODL_MAST','OLTFCOCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMC),'ODL_MAST','OLTFLIMC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLIMP),'ODL_MAST','OLTFLIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLORD),'ODL_MAST','OLTFLORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFORCO),'ODL_MAST','OLTFORCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ImpComm),'ODL_MAST','OLTIMCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_MAST','OLTKEYSA');
      +","+cp_NullLink(cp_ToStrODBC(m.prleamps),'ODL_MAST','OLTLEMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.tpperass),'ODL_MAST','OLTPERAS');
      +","+cp_NullLink(cp_ToStrODBC(m.arpropre),'ODL_MAST','OLTPROVE');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTOD1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAORD),'ODL_MAST','OLTQTODL');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTSAL');
      +","+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTSTATO');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMORDI),'ODL_MAST','OLTUNMIS');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(m.prcrifor),'ODL_MAST','OLCRIFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Seriale),'ODL_MAST','OLSERMRP');
      +","+cp_NullLink(cp_ToStrODBC(m.prsafelt),'ODL_MAST','OLTSAFLT');
      +","+cp_NullLink(cp_ToStrODBC(m.datcon),'ODL_MAST','OLTDTCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERCIC),'ODL_MAST','OLTSECIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRIELA),'ODL_MAST','OLTCRELA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'OLDATODL',i_DATSYS,'OLDATODP',i_DATSYS,'OLOPEODL',i_CODUTE,'OLOPEODP',i_CODUTE,'OLTCAMAG',this.w_PPCAUORD,'OLTCOART',m.codart,'OLTCOATT',m.codatt,'OLTCOCEN',m.arcencos,'OLTCOCOS',m.catipcos,'OLTCODIC',this.w_OLTCODIC,'OLTCOFOR',m.prcodfor)
      insert into (i_cTable) (OLCODODL,OLDATODL,OLDATODP,OLOPEODL,OLOPEODP,OLTCAMAG,OLTCOART,OLTCOATT,OLTCOCEN,OLTCOCOS,OLTCODIC,OLTCOFOR,OLTCOMAG,OLTCOMME,OLTCONTR,OLTDINRIC,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLEVA,OLTFLIMC,OLTFLIMP,OLTFLORD,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,OLTVARIA,UTCC,UTDC,OLCRIFOR,OLSERMRP,OLTSAFLT,OLTDTCON,OLTSECIC,OLTCRELA &i_ccchkf. );
         values (;
           this.w_OLCODODL;
           ,i_DATSYS;
           ,i_DATSYS;
           ,i_CODUTE;
           ,i_CODUTE;
           ,this.w_PPCAUORD;
           ,m.codart;
           ,m.codatt;
           ,m.arcencos;
           ,m.catipcos;
           ,this.w_OLTCODIC;
           ,m.prcodfor;
           ,this.w_MAGRIF;
           ,m.codcom;
           ,m.contra;
           ,m.datini;
           ,m.datmps;
           ,m.dateva;
           ,m.leapro;
           ,this.w_OLTFCOCO;
           ," ";
           ,this.w_OLTFLIMC;
           ,this.w_ORDFLIMP;
           ,this.w_ORDFLORD;
           ,this.w_OLTFORCO;
           ,this.w_ImpComm;
           ,this.w_OLTKEYSA;
           ,m.prleamps;
           ,m.tpperass;
           ,m.arpropre;
           ,m.quanti;
           ,this.w_QTAORD;
           ,0;
           ,0;
           ,m.quanti;
           ,"+";
           ,"F";
           ,"A";
           ,this.w_UMORDI;
           ,"N";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,m.prcrifor;
           ,this.w_Seriale;
           ,m.prsafelt;
           ,m.datcon;
           ,this.w_SERCIC;
           ,this.w_CRIELA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_058E2B88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(m.codcom),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(m.codatt),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(m.catipcos),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',m.codcom,'CSTIPSTR',"A",'CSCODMAT',m.codatt,'CSCODCOS',m.catipcos)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           m.codcom;
           ,"A";
           ,m.codatt;
           ,m.catipcos;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    do case
      case this.w_MIODA = "M"
        * --- Magazzino preferenziale
        this.w_MAGRIF = m.armagpre
      case this.w_MIODA = "C"
        * --- Criterio di pianificazione
        do case
          case this.w_CRIELA = "G"
            this.w_MAGRIF = m.grumag
          case this.w_CRIELA = "M"
            this.w_MAGRIF = m.codmag
          otherwise
            this.w_MAGRIF = m.armagpre
        endcase
      otherwise
        * --- Forza Magazzino
        this.w_MAGRIF = this.w_MAGFOA
    endcase
    this.w_OLTCODIC = LEFT(NVL(codric, SPACE(20)), 20)
    this.w_OLTKEYSA = LEFT(NVL(m.codsal, SPACE(20)), 20)
    * --- Insert into ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",OLDATODL"+",OLDATODP"+",OLOPEODL"+",OLOPEODP"+",OLTCAMAG"+",OLTCOART"+",OLTCOATT"+",OLTCOCEN"+",OLTCOCOS"+",OLTCODIC"+",OLTCOFOR"+",OLTCOMAG"+",OLTCOMME"+",OLTCONTR"+",OLTDINRIC"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLEVA"+",OLTFLIMC"+",OLTFLIMP"+",OLTFLORD"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",OLTVARIA"+",UTCC"+",UTDC"+",OLCRIFOR"+",OLTSAFLT"+",OLTDTCON"+",OLTSECIC"+",OLTCRELA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_MAST','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PPCAUORD),'ODL_MAST','OLTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.codart),'ODL_MAST','OLTCOART');
      +","+cp_NullLink(cp_ToStrODBC(m.codatt),'ODL_MAST','OLTCOATT');
      +","+cp_NullLink(cp_ToStrODBC(m.arcencos),'ODL_MAST','OLTCOCEN');
      +","+cp_NullLink(cp_ToStrODBC(m.catipcos),'ODL_MAST','OLTCOCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_MAST','OLTCODIC');
      +","+cp_NullLink(cp_ToStrODBC(m.prcodfor),'ODL_MAST','OLTCOFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGRIF),'ODL_MAST','OLTCOMAG');
      +","+cp_NullLink(cp_ToStrODBC(m.codcom),'ODL_MAST','OLTCOMME');
      +","+cp_NullLink(cp_ToStrODBC(m.contra),'ODL_MAST','OLTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(m.datini),'ODL_MAST','OLTDINRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.datmps),'ODL_MAST','OLTDTMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.dateva),'ODL_MAST','OLTDTRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.leapro),'ODL_MAST','OLTEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFCOCO),'ODL_MAST','OLTFCOCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMC),'ODL_MAST','OLTFLIMC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLIMP),'ODL_MAST','OLTFLIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDFLORD),'ODL_MAST','OLTFLORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFORCO),'ODL_MAST','OLTFORCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ImpComm),'ODL_MAST','OLTIMCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_MAST','OLTKEYSA');
      +","+cp_NullLink(cp_ToStrODBC(m.prleamps),'ODL_MAST','OLTLEMPS');
      +","+cp_NullLink(cp_ToStrODBC(m.tpperass),'ODL_MAST','OLTPERAS');
      +","+cp_NullLink(cp_ToStrODBC(m.arpropre),'ODL_MAST','OLTPROVE');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTOD1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAORD),'ODL_MAST','OLTQTODL');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
      +","+cp_NullLink(cp_ToStrODBC(m.quanti),'ODL_MAST','OLTQTSAL');
      +","+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTSTATO');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMORDI),'ODL_MAST','OLTUNMIS');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(m.prcrifor),'ODL_MAST','OLCRIFOR');
      +","+cp_NullLink(cp_ToStrODBC(m.prsafelt),'ODL_MAST','OLTSAFLT');
      +","+cp_NullLink(cp_ToStrODBC(m.datcon),'ODL_MAST','OLTDTCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERCIC),'ODL_MAST','OLTSECIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRIELA),'ODL_MAST','OLTCRELA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'OLDATODL',i_DATSYS,'OLDATODP',i_DATSYS,'OLOPEODL',i_CODUTE,'OLOPEODP',i_CODUTE,'OLTCAMAG',this.w_PPCAUORD,'OLTCOART',m.codart,'OLTCOATT',m.codatt,'OLTCOCEN',m.arcencos,'OLTCOCOS',m.catipcos,'OLTCODIC',this.w_OLTCODIC,'OLTCOFOR',m.prcodfor)
      insert into (i_cTable) (OLCODODL,OLDATODL,OLDATODP,OLOPEODL,OLOPEODP,OLTCAMAG,OLTCOART,OLTCOATT,OLTCOCEN,OLTCOCOS,OLTCODIC,OLTCOFOR,OLTCOMAG,OLTCOMME,OLTCONTR,OLTDINRIC,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLEVA,OLTFLIMC,OLTFLIMP,OLTFLORD,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,OLTVARIA,UTCC,UTDC,OLCRIFOR,OLTSAFLT,OLTDTCON,OLTSECIC,OLTCRELA &i_ccchkf. );
         values (;
           this.w_OLCODODL;
           ,i_DATSYS;
           ,i_DATSYS;
           ,i_CODUTE;
           ,i_CODUTE;
           ,this.w_PPCAUORD;
           ,m.codart;
           ,m.codatt;
           ,m.arcencos;
           ,m.catipcos;
           ,this.w_OLTCODIC;
           ,m.prcodfor;
           ,this.w_MAGRIF;
           ,m.codcom;
           ,m.contra;
           ,m.datini;
           ,m.datmps;
           ,m.dateva;
           ,m.leapro;
           ,this.w_OLTFCOCO;
           ," ";
           ,this.w_OLTFLIMC;
           ,this.w_ORDFLIMP;
           ,this.w_ORDFLORD;
           ,this.w_OLTFORCO;
           ,this.w_ImpComm;
           ,this.w_OLTKEYSA;
           ,m.prleamps;
           ,m.tpperass;
           ,m.arpropre;
           ,m.quanti;
           ,this.w_QTAORD;
           ,0;
           ,0;
           ,m.quanti;
           ,"+";
           ,"F";
           ,"A";
           ,this.w_UMORDI;
           ,"N";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,m.prcrifor;
           ,m.prsafelt;
           ,m.datcon;
           ,this.w_SERCIC;
           ,this.w_CRIELA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza DB
    this.w_DELORDS = SPACE(1)
    if this.w_DELORD<>"T"
      this.w_DELORDS = "S"
    endif
    this.w_ErrorBID = .F.
    * --- Try
    local bErr_059ABE08
    bErr_059ABE08=bTrsErr
    this.Try_059ABE08()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_ErrorBID = .T.
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_059ABE08
    * --- End
  endproc
  proc Try_059ABE08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Ricalcola bidoni temporali ....
    * --- Se w_SELEZ � 'S' solo perch� w_GENODA � 'S' allora devo rigenerare i bidoni temporali degli articoli esterni.
    * --- Se w_ELABID='S' rigenero i bidoni anche se ho i filtri (scelta utente)
    if this.oParentObject.w_INTERN and this.oParentObject.w_MODELA="R" and (this.w_ELABID="S" or this.w_SELEZ<>"S" or (this.w_GENODA="S" and this.w_SELEZ="S" and empty(alltrim(this.w_APPO)) and this.oParentObject.w_LLCINI=0 and this.oParentObject.w_LLCFIN=999 and this.w_STIPART<>"N"))
      do GSCO_BCB with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_INTERN and this.oParentObject.w_MODELA="R" and this.w_ELABID<>"S" and this.w_SELEZ="S"
      * --- Elimina anche ordini di fase compresi nella sleezione
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OLCODODL"
        do vq_exec with '..\COLA\EXE\QUERY\GSMR14BGP2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
        +"ODL_MAST.OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
            +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
            +"OLTSTATO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Segna da cancellare tutti gli ODL suggeriti
      if this.w_SELEZ="S" and (this.w_GENODA="S" or this.w_ELABID="S")
        * --- Elimina anche ordini di fase compresi nella sleezione
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="OLCODODL"
          do vq_exec with '..\COLA\EXE\QUERY\GSMR14BGP2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
          +",OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
              +i_ccchkf;
              +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
          +"ODL_MAST.OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
          +",ODL_MAST.OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
              +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
              +"OLTSTATO,";
              +"OLTPERAS";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO')+",";
              +cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
          +",OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
          +",OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if Empty(this.w_DELORDS)
          if this.w_GENPODA="O" and not empty(this.oParentObject.w_CRITFORN)
            * --- Tutti gli ordini
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                  +i_ccchkf ;
              +" where ";
                  +"OLTSTATO = "+cp_ToStrODBC("M");
                     )
            else
              update (i_cTable) set;
                  OLTSTATO = "-";
                  &i_ccchkf. ;
               where;
                  OLTSTATO = "M";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Tutti gli ordini esclusi gli ODA
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                  +i_ccchkf ;
              +" where ";
                  +"OLTSTATO = "+cp_ToStrODBC("M");
                  +" and OLTPROVE <> "+cp_ToStrODBC("E");
                     )
            else
              update (i_cTable) set;
                  OLTSTATO = "-";
                  &i_ccchkf. ;
               where;
                  OLTSTATO = "M";
                  and OLTPROVE <> "E";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          if g_PRFA="S" and g_CICLILAV="S"
            * --- Articoli di fase collegati agli articoli della selezione
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="OLCODODL"
              do vq_exec with '..\COLA\EXE\QUERY\GSMR16BGP3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                  +i_ccchkf;
                  +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
              +"ODL_MAST.OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                  +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
                  +"OLTSTATO";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="OLCODODL"
            do vq_exec with '..\COLA\EXE\QUERY\GSMR16BGP2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                +i_ccchkf;
                +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
            +"ODL_MAST.OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
                +"OLTSTATO";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
            +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("-"),'ODL_MAST','OLTSTATO');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      if this.w_SELEZ="S" and (this.w_GENODA="S" or this.w_ELABID="S")
        * --- Periodo Assoluto ODA/ODL gi� azzerato in precedenza
      else
        * --- Azzera Periodo Assoluto (del piano ODL) di tutti gli ODL
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_cOp2=cp_SetTrsOp(this.w_OPV,'UTCV','this.w_UTCV',this.w_UTCV,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_OPV,'UTDV','this.w_UTDV',this.w_UTDV,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
          +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
              +i_ccchkf ;
          +" where ";
              +"OLTPROVE <> "+cp_ToStrODBC("E");
              +" and OLTSTATO <> "+cp_ToStrODBC("S");
              +" and OLTSTATO <> "+cp_ToStrODBC("C");
              +" and OLTSTATO <> "+cp_ToStrODBC("D");
                 )
        else
          update (i_cTable) set;
              OLTPERAS = "   ";
              ,UTCV = &i_cOp2.;
              ,UTDV = &i_cOp3.;
              &i_ccchkf. ;
           where;
              OLTPROVE <> "E";
              and OLTSTATO <> "S";
              and OLTSTATO <> "C";
              and OLTSTATO <> "D";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_GENPODA="O" and not empty(this.oParentObject.w_CRITFORN)
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_cOp2=cp_SetTrsOp(this.w_OPV,'UTCV','this.w_UTCV',this.w_UTCV,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_OPV,'UTDV','this.w_UTDV',this.w_UTDV,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
            +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
            +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                +i_ccchkf ;
            +" where ";
                +"OLTPROVE = "+cp_ToStrODBC("E");
                   )
          else
            update (i_cTable) set;
                OLTPERAS = "   ";
                ,UTCV = &i_cOp2.;
                ,UTDV = &i_cOp3.;
                &i_ccchkf. ;
             where;
                OLTPROVE = "E";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    ah_Msg("Aggiornamento saldi in corso...",.T.)
    this.w_OLTSTATO = "-"
    * --- Create temporary table TMPSALAGG
    i_nIdx=cp_AddTableDef('TMPSALAGG') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_IUTEHCCMNP[1]
    indexes_IUTEHCCMNP[1]='OLKEYSAL,OLCODMAG'
    vq_exec('..\COLA\EXE\QUERY\GSMRSBGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_IUTEHCCMNP,.f.)
    this.TMPSALAGG_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiorna i Saldi di Magazzino
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'GSMRSBGP2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTIPER = SALDIART.SLQTIPER-_t2.SLQTIPER";
          +",SLQTOPER = SALDIART.SLQTOPER-_t2.SLQTOPER";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTIPER = SALDIART.SLQTIPER-_t2.SLQTIPER";
          +",SALDIART.SLQTOPER = SALDIART.SLQTOPER-_t2.SLQTOPER";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTIPER,";
          +"SLQTOPER";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTIPER-t2.SLQTIPER,";
          +"SLQTOPER-t2.SLQTOPER";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTIPER = SALDIART.SLQTIPER-_t2.SLQTIPER";
          +",SLQTOPER = SALDIART.SLQTOPER-_t2.SLQTOPER";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTIPER = (select "+i_cTable+".SLQTIPER-SLQTIPER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTOPER = (select "+i_cTable+".SLQTOPER-SLQTOPER from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Try
    local bErr_059C30D0
    bErr_059C30D0=bTrsErr
    this.Try_059C30D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Errore Aggiornamento Saldi di Magazzino"
      return
    endif
    bTrsErr=bTrsErr or bErr_059C30D0
    * --- End
    if !this.w_OLDCOM
      * --- Aggiorna i Saldi di Magazzino per Commessa
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SCCODICE,SCCODMAG,SCCODCAN"
        do vq_exec with 'GSMRSBGP2C',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER = SALDICOM.SCQTOPER-_t2.SCQTOPER";
            +",SCQTIPER = SALDICOM.SCQTIPER-_t2.SCQTIPER";
            +i_ccchkf;
            +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
            +"SALDICOM.SCQTOPER = SALDICOM.SCQTOPER-_t2.SCQTOPER";
            +",SALDICOM.SCQTIPER = SALDICOM.SCQTIPER-_t2.SCQTIPER";
            +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = t2.SCCODCAN";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
            +"SCQTOPER,";
            +"SCQTIPER";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"SCQTOPER-t2.SCQTOPER,";
            +"SCQTIPER-t2.SCQTIPER";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
            +"SCQTOPER = SALDICOM.SCQTOPER-_t2.SCQTOPER";
            +",SCQTIPER = SALDICOM.SCQTIPER-_t2.SCQTIPER";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
                +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
                +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER = (select "+i_cTable+".SCQTOPER-SCQTOPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SCQTIPER = (select "+i_cTable+".SCQTIPER-SCQTIPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Try
      local bErr_059C56B0
      bErr_059C56B0=bTrsErr
      this.Try_059C56B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="Errore Aggiornamento Saldi di Magazzino"
        return
      endif
      bTrsErr=bTrsErr or bErr_059C56B0
      * --- End
    endif
    * --- Drop temporary table TMPSALAGG
    i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALAGG')
    endif
    * --- Write into ODL_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='OLCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL "," from "+i_cQueryTable+" where OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
          +i_ccchkf;
          +" from "+i_cTable+" ODL_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT, "+i_cQueryTable+" _t2 set ";
      +"ODL_DETT.OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
          +Iif(Empty(i_ccchkf),"",",ODL_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT set ";
      +"OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("/"),'ODL_MAST','OLTSTATO');
      +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +i_ccchkf ;
      +" where ";
          +"OLTSTATO = "+cp_ToStrODBC(this.w_OLTSTATO);
             )
    else
      update (i_cTable) set;
          OLTSTATO = "/";
          ,OLTQTSAL = 0;
          &i_ccchkf. ;
       where;
          OLTSTATO = this.w_OLTSTATO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_059C30D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRSBGP3",this.SALDIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_059C56B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRSBGP3C",this.SALDICOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione Dati
    * --- Preparazione dati da elaborare
    if this.w_UseProgBar
      GesProgBar("M", this, 0, 100, ah_msgformat("Elaborazione Dati..."))
      * --- Creazione Cursore ELABOR di elaborazione ....
      GesProgBar("S", this, 10, 100)
    else
      ah_Msg("Elaborazione dati...",.T.)
    endif
    Create cursor LogErrori (ODL C(15), Codric C(41), Quan N(18,6), Datini D(8), Datfin D(8), Messagg C(80))
    if this.oParentObject.w_INTERN
      * --- Cursore dei legami delle distinte
      select * from ((legami inner join Articoli on legami.cocodart=Articoli.codart1) left outer join Art_prod on ; 
 legami.cokeysal=art_prod.codsal1) ; 
 into cursor Componen
      Select * from Componen into cursor Componen READWRITE
      update Componen set prlotrio=nvl(prlotrio,0), prqtamin=nvl(prqtamin,0), prscomin=nvl(prscomin,0), codsal1=cokeysal, artipart=tipartor 
 index on dbkeysal tag keysal
    endif
    * --- Cursore del Calendario aziendale
    select cagiorno, tpperass, dtos(cagiorno) as datidx, canumore>0 as giolav from Calendario into cursor Calend 
 index on datidx tag datidx for giolav 
 use in Calendario
    if this.oParentObject.w_INTERN
      if this.oParentObject.w_MODELA="N"
        * --- I cursori sono vuoti: devono essere creati durante l'elaborazione
        Select * from ImpeODL into cursor ImpeODL READWRITE 
 Select * from OrdiODL into cursor OrdiODL READWRITE 
 Select * from CImpeODL into cursor CImpeODL READWRITE 
 Select * from COrdiODL into cursor COrdiODL READWRITE
        Create Cursor OLSug2del (cododl c(15))
      endif
      if g_PRFA="S" and g_CICLILAV="S" and used("Cicli")
        Select * from Cicli into cursor Cicli READWRITE 
 Select Cicli
        index on clcoddis tag clcoddis 
 index on clserial tag clserial
      endif
    endif
    * --- Cursore di elaborazione dei dati iniziali
    Select Impegni.*, dtos(dateva) as datidx, dtos(dateva) as datper, cododl as odlmsg, space(15) as odamsg from impegni into cursor datiniz
    * --- Storna i saldi dell'Ordinato-Impegnato derivante dagli ODL suggeriti
    Select * from datiniz into cursor datiniz READWRITE
    Select codsal, codsal1, codmag,grumag, Sum(quanti) as quanti from Saldi into cursor Saldi READWRITE group by 1,2,3,4
    this.w_LenCodSal = len(codsal1)
    Select Saldi 
 index on codsal tag codsal
    if ! this.w_OLDCOM
      Select codsal, codsal1, codmag,grumag, codcom, Sum(quanti) as quanti from SaldiCom into cursor SaldiCom READWRITE group by 1,2,3,4,5
      Select SaldiCom 
 index on codsal tag codsal
    endif
    * --- Imposta la data del periodo scaduto
    this.w_Leadt = this.w_PPGIOSCA+1
    this.DatInput = dtos(i_DATSYS)
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    DATEVA=this.DatRisul
    DATCON=date(1900,1,1)
    if ! this.w_OLDCOM
      * --- Aggiunge al cursore di elaborazione dei dati iniziali i Saldi per commessa
      TIPREC="0SCOMM"
      * --- Data 02 invece che 01 come per il record 0SALDI in modo che questa riga vada sempre dopo
      DATIDX="19000102"
      DATPER="19000102"
      Select SaldiCom 
 scan 
 scatter memvar
      m.codart=codsal1
      m.codric=codsal1
      quanti=nvl(quanti,0) 
 Insert into datiniz from memvar 
 endscan
    endif
    * --- Aggiunge al cursore dei elaborazione dei dati iniziali i Saldi
    TIPREC="0SALDI"
    DATIDX="19000101"
    DATPER="19000101"
    Select Saldi 
 scan 
 scatter memvar
    m.codart=codsal1
    m.codric=codsal1
    m.codcom=space(15)
    quanti=nvl(quanti,0) 
 Insert into datiniz from memvar 
 endscan
    * --- Aggiunge al cursore dei elaborazione dei dati iniziali i Documenti
    Select Documen 
 scan 
 scatter memvar 
 DATCON=dateva 
 codart=left(codsal,20)
     DATPER=SPACE(8)
    if Documen.prsafelt>0
      this.w_Leadt = Documen.prsafelt
      this.DatInput = dtos(dateva)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
       DATIDX=dtos(this.DatRisul)
    else
       DATIDX=dtos(dateva)
    endif
    if isnull(dateva)
      this.w_TMP = "Esistono documenti d'ordine senza data di prevista evasione%0Elaborazione abortita"
      ah_errormsg(this.w_TMP, 16)
      i_retcode = 'stop'
      return
    endif
    m.codart=codsal1
    m.codric=codsal1
    codsal = m.codart + iif(this.w_CRIELA="G", nvl(grumag, space(5)), iif(this.w_CRIELA="M", nvl(codmag, space(5)), SPACE(0) ))
    * --- Ordine Cliente
    if ordcli>0
      TIPREC="7ORDCL" 
 QUANTI=-ordcli 
 odlmsg = iif(empty(odlmsg), cododl + "." + padr(alltrim(str(perownum)),4), odlmsg) 
 odamsg = iif(empty(odamsg), space(15), odamsg) 
 Insert into datiniz from memvar
    endif
    * --- Ordine Fornitore
    if ordfor>0
       TIPREC="4ORDFO" 
 QUANTI=ordfor 
 odlmsg = iif(empty(odlmsg), cododl + "." + padr(alltrim(str(perownum)),4), odlmsg) 
 odamsg = iif(empty(odamsg), space(15), odamsg) 
 Insert into datiniz from memvar
    endif
    endscan
    if this.oParentObject.w_LLCINI>this.oParentObject.w_LLCFIN
      this.oParentObject.w_LLCINI = 0
      this.oParentObject.w_LLCFIN = 999
    endif
    * --- Cursore di Elaborazione
    if this.w_CRIELA="A"
      if this.w_PPMRPDIV="S"
        * --- Cursore principale
        Select codsal,codric,codart,codmag,grumag,datmps,datcon,dateva,datini,quanti,tiprec,cododl,olrownum,perownum,cocoeum1,oltseodl ,codcom,codatt,; 
 impcom,contra,prove,mgdismag,datidx,datper,odlmsg,odamsg,artipart,arunmis1,armagpre,artipges,arpropre,space(1) as odlstat,; 
 prleamps,prgioapp,dpcoeflt,prlotmed,dpmodpre,proggmps,dppolpro,prpersca,arflcomm,arunmis2,; 
 prexplph, prpiapun, prperpia, ; 
 aroperat,armoltip,arflcom1,argrumer,arcencos,datobso,mgmagimp,art_prod.codsal1 as codsal1,prscomin,prcoerss,prqtamin,prlotrio,prqtamax,prcodfor,prsafelt , ; 
 prgiocop,prlowlev,anmagter,arprezum,prcrifor,quanti*0 as qtalor, quanti*0 as qtanet, cododl as padrered,"                " as padreord, ; 
 perownum*0 as cprownum, prgioapp*0 as leapro, quanti as quantv, cocoeum1 as cocoeimp,"  " as coidegru, "S" as coflpref,(this.oParentObject.w_MODELA="R") as elabora, ; 
 .f. as aggdb, right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx,; 
 "N" as evaaut, quanti as quanfi, quanti as quanfv, dismgpre, dismgmag ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) left outer join ; 
 Art_prod on datiniz.codsal1=art_prod.codsal1) where prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN into cursor Elabor
        * --- Cursore secondario, i valori del quale non vengono salvati nel DBF
        Select codsal,tiprec,cododl,perownum,oltseodl,odlmsg,artipart,arflcomm,oltemlav,cossta,cosult,cosmed,coslis,artipimp,armagimp,cavoccos,catipcos,quanti as quanfi,; 
 cododl as padrered, "                " as padreord, arunmis1 as counimis, ; 
 perownum*0 as dbriffas, "000" as tpperass, prgioapp*0 as cocorlea, .F. as mpserr, .F. as ltperr,"N" as clfasext, olprofan as profan, ; 
 .f. as aggdb, ; 
 right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx, dismgimp ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) left outer join ; 
 Art_prod on datiniz.codsal1=art_prod.codsal1) where prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN into cursor Elabor_bis
      else
         Select codsal,codric,codart,codmag,grumag,datmps,datcon,dateva,datini,quanti,tiprec,cododl,olrownum,perownum,oltemlav,cocoeum1,oltseodl ,codcom,codatt,; 
 impcom,contra,prove,mgdismag as mgdismag,datidx,datper,odlmsg,odamsg,artipart,tipartor,arunmis1, armagpre,dismgpre,artipges,arpropre,; 
 prleamps,prgioapp,dpcoeflt,prlotmed,dpmodpre,proggmps,dppolpro,prqtamax as dpqtamax,prpersca,arflcomm,catipcos, ; 
 prgiocop as dpgiocop,arunmis2, prexplph, prpiapun, prperpia, ; 
 aroperat,armoltip,arflcom1,argrumer,arcencos,cavoccos,datobso,artipimp,armagimp,dismgimp, mgmagimp, dismgmag, art_prod.codsal1 as codsal1,prscomin,; 
 prcoerss,prqtamin,prlotrio,prqtamax,prcodfor,prsafelt,prgiocop,cossta,cosult,cosmed,coslis,prlowlev,anmagter,arprezum,prcrifor, ; 
 quanti*0 as qtalor, quanti*0 as qtanet, cododl as padrered, "                " as padreord, ; 
 perownum*0 as cprownum, prgioapp*0 as leapro, quanti as quantv, arunmis1 as ; 
 counimis, cocoeum1 as cocoeimp, "  " as coidegru, "S" as coflpref, this.oParentObject.w_MODELA="R" as elabora, "000" as tpperass, .f. as aggdb, ; 
 right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx,; 
 .F. as mpserr, .F. as ltperr, "N" as evaaut, quanti as quanfi, quanti as quanfv, prgioapp*0 as cocorlea, perownum*0 as dbriffas, space(1) as odlstat,; 
 olprofan as profan, "N" as clfasext ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) left outer join ; 
 Art_prod on datiniz.codsal1=art_prod.codsal1) where prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN into cursor Elabor
      endif
    else
      select distinct codsal2 from par_rima into cursor KeyPar_rima
      if this.w_PPMRPDIV="S"
        * --- Cursore principale
        Select codsal,codric,codart,codmag,grumag,datmps,datcon,dateva,datini,quanti,tiprec,cododl,olrownum,perownum,cocoeum1,oltseodl ,codcom,codatt,; 
 impcom,contra,prove,mgdismag,datidx,datper,odlmsg,odamsg,artipart,arunmis1,armagpre,artipges,arpropre,space(1) as odlstat,; 
 prleamps,prgioapp,dpcoeflt,prlotmed,dpmodpre,proggmps,dppolpro,prpersca,arflcomm,arunmis2,; 
 prexplph, prpiapun, prperpia, ; 
 aroperat,armoltip,arflcom1,argrumer,arcencos,datobso,mgmagimp,Par_rima.codsal1 as codsal1,prscomin,prcoerss,prqtamin,prlotrio,prqtamax,prcodfor,prsafelt , ; 
 prgiocop,prlowlev,anmagter,arprezum,prcrifor,quanti*0 as qtalor, quanti*0 as qtanet, cododl as padrered,"                " as padreord, ; 
 perownum*0 as cprownum, prgioapp*0 as leapro, quanti as quantv, cocoeum1 as cocoeimp,"  " as coidegru, "S" as coflpref,(this.oParentObject.w_MODELA="R") as elabora, ; 
 .f. as aggdb, right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx,; 
 "N" as evaaut, quanti as quanfi, quanti as quanfv, dismgpre, dismgmag ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) inner join ; 
 Par_rima on datiniz.codsal=Par_rima.codsal2 ) where prlowlev>= this.oParentObject.w_LLCINI and prlowlev<= this.oParentObject.w_LLCFIN into cursor Elabor;
        union ;
        Select codsal,codric,codart,codmag,grumag,datmps,datcon,dateva,datini,quanti,tiprec,cododl,olrownum,perownum,cocoeum1,oltseodl ,codcom,codatt,; 
 impcom,contra,prove,mgdismag,datidx,datper,odlmsg,odamsg,artipart,arunmis1,armagpre,artipges,arpropre,space(1) as odlstat,; 
 prleamps,prgioapp,dpcoeflt,prlotmed,dpmodpre,proggmps,dppolpro,prpersca,arflcomm,arunmis2,; 
 prexplph, prpiapun, prperpia, ; 
 aroperat,armoltip,arflcom1,argrumer,arcencos,datobso,mgmagimp,art_prod.codsal1 as codsal1,prscomin,prcoerss,prqtamin,prlotrio,prqtamax,prcodfor,prsafelt , ; 
 prgiocop,prlowlev,anmagter,arprezum,prcrifor,quanti*0 as qtalor, quanti*0 as qtanet, cododl as padrered,"                " as padreord, ; 
 perownum*0 as cprownum, prgioapp*0 as leapro, quanti as quantv, cocoeum1 as cocoeimp,"  " as coidegru, "S" as coflpref,(this.oParentObject.w_MODELA="R") as elabora, ; 
 .f. as aggdb, right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx,; 
 "N" as evaaut, quanti as quanfi, quanti as quanfv, dismgpre, dismgmag ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) left outer join ; 
 Art_prod on datiniz.codsal1=art_prod.codsal1) ; 
 where prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN and datiniz.codsal not in (select codsal2 from KeyPar_rima)
        * --- Cursore secondario, i valori del quale non vengono salvati nel DBF
        Select codsal,tiprec,cododl,perownum,oltseodl,odlmsg,artipart,arflcomm,oltemlav,cossta,cosult,cosmed,coslis,artipimp,armagimp,cavoccos,catipcos,quanti as quanfi,; 
 cododl as padrered, "                " as padreord, arunmis1 as counimis, ; 
 perownum*0 as dbriffas, "000" as tpperass, prgioapp*0 as cocorlea, .F. as mpserr, .F. as ltperr,"N" as clfasext, olprofan as profan, ; 
 .f. as aggdb, ; 
 right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx, dismgimp ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) inner join ; 
 Par_rima on datiniz.codsal=Par_rima.codsal2 ) where prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN into cursor Elabor_bis ;
        union ;
        Select codsal,tiprec,cododl,perownum,oltseodl,odlmsg,artipart,arflcomm,oltemlav,cossta,cosult,cosmed,coslis,artipimp,armagimp,cavoccos,catipcos,quanti as quanfi,; 
 cododl as padrered, "                " as padreord, arunmis1 as counimis, ; 
 perownum*0 as dbriffas, "000" as tpperass, prgioapp*0 as cocorlea, .F. as mpserr, .F. as ltperr,"N" as clfasext, olprofan as profan, ; 
 .f. as aggdb, ; 
 right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx, dismgimp ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) left outer join ; 
 Art_prod on datiniz.codsal1=art_prod.codsal1) ; 
 where prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN and datiniz.codsal not in (select codsal2 from KeyPar_rima) 
      else
         Select codsal,codric,codart,codmag,grumag,datmps,datcon,dateva,datini,quanti,tiprec,cododl,olrownum,perownum,oltemlav,cocoeum1,oltseodl ,codcom,codatt,; 
 impcom,contra,prove,mgdismag as mgdismag,datidx,datper,odlmsg,odamsg,artipart,tipartor,arunmis1, armagpre,dismgpre,artipges,arpropre,; 
 prleamps,prgioapp,dpcoeflt,prlotmed,dpmodpre,proggmps,dppolpro,prqtamax as dpqtamax,prpersca,arflcomm,catipcos, ; 
 prgiocop as dpgiocop,arunmis2, prexplph, prpiapun, prperpia, ; 
 aroperat,armoltip,arflcom1,argrumer,arcencos,cavoccos,datobso,artipimp,armagimp,dismgimp, mgmagimp, dismgmag, par_rima.codsal1 as codsal1,prscomin,; 
 prcoerss,prqtamin,prlotrio,prqtamax,prcodfor,prsafelt,prgiocop,cossta,cosult,cosmed,coslis,prlowlev,anmagter,arprezum,prcrifor, ; 
 quanti*0 as qtalor, quanti*0 as qtanet, cododl as padrered, "                " as padreord, ; 
 perownum*0 as cprownum, prgioapp*0 as leapro, quanti as quantv, arunmis1 as ; 
 counimis, cocoeum1 as cocoeimp, "  " as coidegru, "S" as coflpref, this.oParentObject.w_MODELA="R" as elabora, "000" as tpperass, .f. as aggdb, ; 
 right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx,; 
 .F. as mpserr, .F. as ltperr, "N" as evaaut, quanti as quanfi, quanti as quanfv, prgioapp*0 as cocorlea, perownum*0 as dbriffas, space(1) as odlstat,; 
 olprofan as profan, "N" as clfasext ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) inner join ; 
 Par_rima on datiniz.codsal=Par_rima.codsal2 ) where !Isnull(Par_rima.codsal2) and prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN into cursor Elabor ;
        union ;
         Select codsal,codric,codart,codmag,grumag,datmps,datcon,dateva,datini,quanti,tiprec,cododl,olrownum,perownum,oltemlav,cocoeum1,oltseodl ,codcom,codatt,; 
 impcom,contra,prove,mgdismag as mgdismag,datidx,datper,odlmsg,odamsg,artipart,tipartor,arunmis1, armagpre,dismgpre,artipges,arpropre,; 
 prleamps,prgioapp,dpcoeflt,prlotmed,dpmodpre,proggmps,dppolpro,prqtamax as dpqtamax,prpersca,arflcomm,catipcos, ; 
 prgiocop as dpgiocop,arunmis2, prexplph, prpiapun, prperpia, ; 
 aroperat,armoltip,arflcom1,argrumer,arcencos,cavoccos,datobso,artipimp,armagimp,dismgimp, mgmagimp, dismgmag, art_prod.codsal1 as codsal1,prscomin,; 
 prcoerss,prqtamin,prlotrio,prqtamax,prcodfor,prsafelt,prgiocop,cossta,cosult,cosmed,coslis,prlowlev,anmagter,arprezum,prcrifor, ; 
 quanti*0 as qtalor, quanti*0 as qtanet, cododl as padrered, "                " as padreord, ; 
 perownum*0 as cprownum, prgioapp*0 as leapro, quanti as quantv, arunmis1 as ; 
 counimis, cocoeum1 as cocoeimp, "  " as coidegru, "S" as coflpref, this.oParentObject.w_MODELA="R" as elabora, "000" as tpperass, .f. as aggdb, ; 
 right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1) as keyidx,; 
 .F. as mpserr, .F. as ltperr, "N" as evaaut, quanti as quanfi, quanti as quanfv, prgioapp*0 as cocorlea, perownum*0 as dbriffas, space(1) as odlstat,; 
 olprofan as profan, "N" as clfasext ; 
 from ((datiniz inner join Articoli on datiniz.codart=Articoli.codart1) left outer join ; 
 KeyPar_rima on datiniz.codsal=KeyPar_rima.codsal2 left outer join Art_prod on datiniz.codsal1=art_prod.codsal1) ; 
 where Isnull(KeyPar_rima.codsal2) and prlowlev>=this.oParentObject.w_LLCINI and prlowlev<=this.oParentObject.w_LLCFIN
      endif
      USE IN SELECT("KeyPar_rima")
    endif
    * --- Chiude i cursori delle query
    USE IN SELECT("Articoli") 
 USE IN SELECT("Art_prod") 
 USE IN SELECT("Saldi") 
 USE IN SELECT("Impegni") 
 USE IN SELECT("Documen") 
 USE IN SELECT("Datiniz") 
 USE IN SELECT("Legami")
    if this.w_PPMRPDIV="S"
      Select * from elabor into cursor elabor READWRITE 
 Select * from elabor_bis into cursor elabor_bis READWRITE
    else
      Select * from elabor into cursor elabor READWRITE
    endif
    if this.oParentObject.w_INTERN
       update elabor set prlotrio=nvl(prlotrio,0), prqtamin=nvl(prqtamin,0), prscomin=nvl(prscomin,0), prlowlev=nvl(prlowlev,0),; 
 prscomin=nvl(prscomin,0), quanti=cp_round(quanti,6), codart=nvl(codart,space(20)),; 
 artipges=iif(artipart $ "PH-DC-PS", "F", artipges), arpropre=iif(arpropre="L" and artipart $ "PH-DC-PS", "I", arpropre), ; 
 datcon=iif(empty(nvl(datcon,0)),dateva,datcon)
    else
       update elabor set quanti=cp_round(quanti,6), codart=nvl(codart,space(20)),; 
 datcon=iif(empty(nvl(datcon,0)),dateva,datcon), artipges=iif(artipart $ "PH-DC-PS", "F", artipges), ; 
 arpropre=iif(arpropre="L" and artipart $ "PH-DC-PS", "I", arpropre)
    endif
    if this.w_CRIELA="A"
      update elabor set codmag=iif(empty(nvl(armagpre,"")), this.w_PPMAGPRO, armagpre)
      update elabor set codcom=space(15), codatt=space(15), keyidx=left(Keyidx,this.w_RootKeyIdx)+space(30)+right(keyidx,9) where arflcomm<>"S"
    else
      update elabor set codmag=iif(empty(nvl(codmag," ")), iif(empty(nvl(armagpre,"")), this.w_PPMAGPRO, armagpre) , codmag)
      update elabor set codcom=space(15), codatt=space(15), keyidx=left(keyidx,this.w_RootKeyIdx)+space(30)+right(keyidx,9) where arflcomm<>"S"
    endif
    select elabor 
 index on keyidx tag keyidx 
 index on padrered tag idxpdr 
 set order to keyidx
    if this.w_PPMRPDIV="S"
      update elabor_bis set keyidx=left(Keyidx,this.w_RootKeyIdx)+space(30)+right(keyidx,9) where arflcomm<>"S" 
 select elabor_bis 
 index on keyidx tag keyidx 
 index on padrered tag idxpdr 
 set order to keyidx
    endif
    * --- Aggiunge il record per il ripristino della scorta di sicurezza
    Select Elabor 
 Scan for tiprec="0" and not artipart $ "PH-PS-DC" and prlowlev>=0
    this.w_RecSel = recno()
    if this.w_PPMRPDIV="S"
      Select Elabor_bis 
 go this.w_RecSel 
 scatter memvar 
 Select Elabor
    endif
    if (arflcomm<>"S" or (tiprec="0SCOMM" and empty(codcom))) and (this.w_SELEIMPE="A" or this.w_SELEZ<>"S")
      * --- Se SELEIMPE (filtro impegni/articoli) � 'I' (impegni) ed ho dei filtri, non ripristino la scorta di sicurezza perch�, nel caso in cui il record dell'ordinato che 
      *     la ripristina non rientri nelle selezioni esso non verrebbe cancellato ma verrebbe rispristinata la SS creando una anomalia (2 odl che ripristinano la SS)
      * --- Ripristino scorta di sicurezza per articoli a commessa personalizzata una sola volta (per la riga relativa al saldo libero)
       scatter memvar 
 tiprec = "7SCO-S" 
 quanti = -prscomin 
 quantv = -prscomin 
 quanfi = -prscomin 
 quanfv = -prscomin 
 dateva = i_DATSYS 
 datcon = i_DATSYS 
 datidx = dtos(i_DATSYS) 
 datper = dtoc(i_DATSYS)
    else
      * --- Solo ripristino giacenza negativa
      if arflcomm<>"S" or tiprec<>"0SALDI"
         scatter memvar 
 tiprec = "7SCO-S" 
 quanti = -prscomin*0 
 quantv = -prscomin*0 
 quanfi = -prscomin*0 
 quanfv = -prscomin*0 
 dateva = i_DATSYS 
 datcon = i_DATSYS 
 datidx = dtos(i_DATSYS) 
 datper = dtoc(i_DATSYS)
      endif
    endif
    if arflcomm<>"S" or tiprec<>"0SALDI"
      if prcoerss > 0 and prscomin > 0 and prgioapp > 0 and arpropre <> "E" and arpropre<>"L"
        * --- Sposta la data di impegno per il ripristino della Scorta di Sicurezza
        this.DatInput = dtos(i_DATSYS)
        this.w_Leadt = -prgioapp*prcoerss
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        dateva = this.DatRisul
        datidx = dtos(this.DatRisul)
        datper = dtoc(this.DatRisul)
      endif
      if arflcomm<>"S"
         keyidx = left(keyidx,this.w_RootKeyIdx)+space(30)+m.datidx+"7"
      else
         keyidx = left(keyidx,this.w_RootKeyIdx)+ iif(nvl(arflcom1,"N")="C",m.datidx + m.codcom + m.codatt, m.codcom + m.codatt + m.datidx) + "7"
      endif
       Insert into Elabor from memvar
      if this.w_PPMRPDIV="S"
         Insert into Elabor_bis from memvar
      endif
    endif
    Select Elabor 
 go this.w_RecSel
    endscan
    if g_COLA="S" and g_PRFA="S" and g_CICLILAV="S" and USED("FASEXTODL")
      if this.w_PPMRPDIV="S"
        update Elabor_bis set clfasext="S" where codsal in (select distinct dbkeysal from fasextodl)
      else
        update Elabor set clfasext="S" where codsal in (select distinct dbkeysal from fasextodl)
      endif
    endif
    if this.oParentObject.w_INTERN
      if g_COLA="S" and g_PRFA="S" and g_CICLILAV="S"
        if Reccount("ODP") > 0
          * --- Se sono installati i cicli di lavoro e il conto lavoro estraggo tutte le fasi esterne
          *     legate al ciclo associato all'ODP
          *     Mi serve per sapere se devo emettere un ordine oppure no per i materiali di input su fase esterna
          *     Utilizzato a Pag3
          VQ_EXEC("..\PRFA\EXE\QUERY\GSCI2BGP", THIS, "FASEXTODP") 
 Select * from FASEXTODP into cursor FASEXTODP readwrite 
 index on olcododl+dtos(clinival) tag cicodp
        endif
      endif
      * --- Aggiunge impegni da ODP da pianificare
      this.w_UDPNAME = FALSE
      Select ODP 
 scan 
 scatter memvar 
 artipart = "  "
      this.w_CodSaldo = codsal
      this.w_CodArt = left(this.w_CodSaldo,this.w_LenCodSal)
      this.w_PROVPAD = olprove
      this.w_RifMate = 0
      this.w_QTA = quanti
      this.w_Datini = datini
      this.w_Dateva = dateva
      this.w_Tiprec = tiprec
      this.w_MatAltern = nvl(clrifmat,"")
      this.w_CodFor = nvl(olcofor,"")
      this.w_TipoContra = 0
      if g_COLA="S" and g_PRFA="S" and g_CICLILAV="S" and olprove = "I" and not(artipart $ "PH.PS.DC")
        this.w_TipoContra = 0
        use in select("Materzis")
        if USED("FASEXTODP")
          SELECT "FASEXTODP"
          GO TOP
          SCAN FOR OLCODODL=m.CODODL and clinival<=m.Datini and m.Datini<=clfinval
          SELECT "FASEXTODP"
          ENDSCAN
        endif
      endif
      if g_GESCON="S" and olprove="L" and not empty(this.w_CodFor)
        * --- Controlla se i materiali sono forniti dal terzista
        Select Contratti
        * --- Cerca il contratto per articolo
        Locate for conumero=contra and cocodart=this.w_CodArt
        this.w_TipoContra = 2
        if Found()
          this.w_CONTRA = Contratti.conumero
          Select mccodice from Contratti into cursor Materzis ; 
 where conumero=this.w_CONTRA and cocodart=this.w_CodArt
          this.w_TipoContra = iif(RecCount("Materzis")>0, this.w_TipoContra, 0)
        else
          this.w_TipoContra = 0
        endif
      endif
      this.w_PIAODP = .T.
      Select Elabor
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      USE IN SELECT("Materzis")
      select odp
      endscan
      USE IN SELECT("FASEXTODP")
      Select Elabor
      * --- Questo assegnamento serve per rendere sempre vera la condizione a Pag2
      this.w_Tiprec = "4ODL-D"
      this.w_UDPNAME = TRUE
      this.w_PIAODP = .F.
      * --- Gli ODL Suggeriti dall'MRP selezionano sempre il materiale alternativo preferenziale
      this.w_MatAltern = ""
      * --- Crea indice su Elabor per velocizzare MRP
      if this.w_PPMRPDIV="S"
        Select Elabor_bis 
 index on codsal+keyidx tag codsal collate "MACHINE" 
 index on codsal tag codsal_key 
 index on cododl+keyidx tag cododl collate "MACHINE" 
 set order to keyidx
      endif
      Select Elabor 
 index on codsal+keyidx tag codsal collate "MACHINE" 
 index on codsal tag codsal_key 
 index on cododl+keyidx tag cododl collate "MACHINE" 
 set order to keyidx
      if g_COLA="S" and g_PRFA="S" and g_CICLILAV="S"
        * --- Gli articoli in C/Lavoro che hanno il ciclo con pi� di una fase generano ODL Interni
        set order to codsal_key
        vq_exec("..\PRFA\EXE\QUERY\GSCIDBGP", this, "Intern") 
 Update Elabor set arpropre="I" where codsal in (select distinct dbkeysal from Intern) 
 Update Componen set arpropre="I" where codsal1 in (select distinct dbkeysal from Intern)
        * --- Gli articoli interni con ciclo monofase in C/Lavoro generano ODL esterni (OCL)
        vq_exec("..\PRFA\EXE\QUERY\GSCIFBGP", this, "Intern")
        * --- Imposta i Lead Time dai Dati/Articoli per gli articoli in Conto Lavoro che non hanno il ciclo di lavorazione
        if .f.
          Update Elabor set prgioapp=prgioapp, prlotmed=0, dpqtamin=prqtamin, dplotrio=prlotrio, dppolpro="X" ; 
 where arpropre="L" and codsal not in (select distinct dbkeysal from intern) 
 Update Componen set prgioapp=prgioapp, prlotmed=0, dpqtamin=prqtamin, dplotrio=prlotrio, dppolpro="X" ; 
 where arpropre="L" and codsal1 not in (select distinct dbkeysal from intern)
        endif
        * --- Imposta i Lead Time dai Dati/Articoli per gli articoli che hanno il ciclo di lavorazione
        Select Intern 
 Go top
        select Elabor 
 Go Top
        if .F.
          Update Elabor set arpropre="L", prgioapp=iif(nvl(prcodfor,Intern.lfcodfor)=Intern.lfcodfor, prgioapp, 0), ; 
 prlotmed=0, dppolpro="X", dpqtamin=iif(nvl(prcodfor,Intern.lfcodfor)=Intern.lfcodfor, prqtamin, 0), ; 
 dplotrio=iif(nvl(prcodfor,Intern.lfcodfor)=Intern.lfcodfor, prlotrio, 0), prcodfor=Intern.lfcodfor, anmagter="xxx" ; 
 from intern where elabor.codsal=Intern.dbkeysal
        endif
        Select Intern 
 Go top
        select Componen 
 Go Top
        if .F.
          Update Componen set arpropre="L", prgioapp=iif(nvl(prcodfor,Intern.lfcodfor)=Intern.lfcodfor, prgioapp, 0), ; 
 prlotmed=0, dppolpro="X", dpqtamin=iif(nvl(prcodfor,Intern.lfcodfor)=Intern.lfcodfor, prqtamin, 0), ; 
 dplotrio=iif(nvl(prcodfor,Intern.lfcodfor)=Intern.lfcodfor, prlotrio, 0), prcodfor=Intern.lfcodfor, anmagter="xxx" ; 
 from intern where Componen.codsal1=Intern.dbkeysal 
        endif
        use in select("Intern")
        select Elabor 
 Go Top
        SCAN FOR INLIST(TIPREC , "7ORDCL" , "4ORDFO") and nvl(prsafelt,0) > 0
        scatter memvar
        this.w_Leadt = nvl(prsafelt, 0)
        if this.w_Leadt>0 and m.datidx<>"19000101"
          this.DatInput = dtos(dateva)
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           DATIDX=dtos(this.DatRisul)
        else
          this.DatRisul = dateva
        endif
        Select Elabor
        replace datidx with dtos(this.DatRisul), keyidx with right("00"+alltrim(str(nvl(prlowlev,0),3,0)),3) + codsal + iif(nvl(arflcom1,"N")="C",datidx + nvl(codcom,space(15))+ ; 
 nvl(codatt,space(15)),nvl(codcom,space(15))+ nvl(codatt,space(15)) +datidx)+left(tiprec,1)
        select Elabor 
 endscan
        Select Elabor 
 set order to keyidx
      else
        if g_COLA="S"
          * --- Imposta i Lead Time dai Dati/Articoli per gli articoli in Conto Lavoro che non hanno il ciclo di lavorazione
          if .f.
            Update Elabor set prgioapp=prgioapp, prlotmed=0, dpqtamin=prqtamin, dplotrio=prlotrio, dppolpro="X" where arpropre="L" 
 Update Componen set prgioapp=prgioapp, prlotmed=0, dpqtamin=prqtamin, dplotrio=prlotrio, dppolpro="X" where arpropre="L"
          endif
        endif
      endif
    else
      if this.w_PPMRPDIV="S"
        Select Elabor_bis 
 index on codsal+keyidx tag codsal collate "MACHINE" 
 index on codsal tag codsal_key 
 index on cododl+keyidx tag cododl collate "MACHINE" 
 set order to keyidx
      endif
      Select Elabor 
 index on codsal+keyidx tag codsal COLLATE "MACHINE" 
 index on codsal tag codsal_key 
 index on cododl+keyidx tag cododl collate "MACHINE" 
 set order to keyidx
    endif
    if g_COLA="S" Or (this.w_GENPODA="O" and not empty(this.oParentObject.w_CRITFORN))
      * --- Gestione della Scorta di Sicurezza
      Select Elabor 
 scan for arpropre$"L-E" and tiprec="7SCO-S"
      if prcoerss > 0 and prscomin > 0
        * --- Sposta la data di impegno per il ripristino della Scorta di Sicurezza
        *     -Se la provenienza � interna la condizione � prcoerss > 0 and prscomin > 0 and prgioapp > 0, il campo di riferimento � Elabor.prgioapp (gi� valutata in precedenza)
        *     -Se la provenienza � esterna o di conto lavoro se c'� un contratto vale quanto l� indicato (Contratti.cogioapp) altrimenti vale la condizione prcoerss > 0 and prscomin > 0 and prgioapp > 0 , 
        *       il campo di riferimento � Elabor.prgioapp
        if arpropre="L"
          if empty(nvl(prcodfor,"")) or empty(nvl(anmagter,""))
            * --- OCL senza Fornitore
            this.w_GIOAPP = 0
            this.w_CONUMERO = space(15)
          else
            if g_GESCON="S"
              * --- Imposta le politiche di lottizzazione e il LT dal contratto
              do case
                case this.w_PREZUM $ "SN"
                  * --- Sempre U.M Principale
                  Select Contratti
                  * --- Cerca il contratto per articolo
                  Locate for i_DATSYS>=codatini and cocodclf=prcodfor and cocodart=codart and counimis=this.w_unmis1
                  this.w_TipoContra = 2
                  if not Found()
                    * --- Cerca il contratto per gruppo merceologico
                    Locate for i_DATSYS>=codatini and cocodclf=prcodfor and cogrumer=argrumer and counimis=this.w_unmis1
                    this.w_TipoContra = 3
                  endif
                otherwise
                  * --- U.M Converti
                  Select Contratti
                  * --- Cerca il contratto per articolo
                  Locate for i_DATSYS>=codatini and cocodclf=prcodfor and cocodart=codart
                  this.w_TipoContra = 2
                  if not Found()
                    * --- Cerca il contratto per gruppo merceologico
                    Locate for i_DATSYS>=codatini and cocodclf=prcodfor and cogrumer=argrumer
                    this.w_TipoContra = 3
                  endif
              endcase
              if Found()
                this.w_GIOAPP = nvl(Contratti.cogioapp,0)
                this.w_CONUMERO = nvl(Contratti.conumero,space(15))
              else
                this.w_GIOAPP = 0
                this.w_CONUMERO = space(15)
              endif
            endif
            Select Elabor
          endif
        else
          * --- Cerca il miglior fornitore dai ContrattiA
          * --- Seleziona Criterio di scelta
          do case
            case this.w_CRIFOR="A"
              * --- Affidabilit�
              OrdinaPer = "Order by coaffida desc"
            case this.w_CRIFOR="T"
              * --- Tempo = giorni di approvvigionamento
              OrdinaPer = "Order by cogioapp"
            case this.w_CRIFOR="R"
              * --- Ripartizione
              OrdinaPer = "Order by coperrip"
            case this.w_CRIFOR="I"
              * --- Priorit�
            otherwise
              OrdinaPer = ""
          endcase
          if g_GESCON="S"
            if this.w_CRIFOR<>"Z"
              * --- Seleziona i ContrattiA validi e li ordina per il criterio di scelta
              Select * from ContrattiA ;
              where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
              &OrdinaPer into cursor tempor
            else
              * --- Calcola Prezzo da Contratto
              * --- Prima genera un temporaneo contenente le righe articolo interessate dei ContrattiA validi
              * --- ordinate per Fornitore + Qta Scaglione
              * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
              Select * from ContrattiA ;
              where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin ;
              and nvl(coquanti,0)>=this.w_fabnet and not empty(nvl(cocodclf," ")) and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
              order by cocodclf, coquanti into cursor Contprez
              * --- Calcola Prezzo da Contratto
              * --- Elabora Criterio per Miglior Prezzo
              =WrCursor("Contprez")
              Index On cocodclf+str(coquanti,12,3) Tag Codice
              * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
              this.w_appo1 = "######"
              select Contprez
              go top
              scan
              if cocodclf <> this.w_APPO1
                * --- Nuovo Fornitore, Calcola Prezzo da Contratto
                this.w_PZ = NVL(COPREZZO, 0)
                this.w_S1 = NVL(COSCONT1, 0)
                this.w_S2 = NVL(COSCONT2, 0)
                this.w_S3 = NVL(COSCONT3, 0)
                this.w_S4 = NVL(COSCONT4, 0)
                this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
                this.w_VAL = NVL(COCODVAL, g_PERVAL)
                this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
                if this.w_VAL<>g_PERVAL
                  * --- Se altra valuta, riporta alla Valuta di conto
                  if this.w_CAO<>0
                    this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
                    replace coprezzo with this.w_PZ
                  else
                    * --- Cambio incongruente, record non utilizzabile
                    this.w_PZ = -999
                  endif
                  select Contprez
                endif
                if this.w_PZ=-999
                  delete
                else
                  this.w_appo1 = cocodclf
                endif
              else
                * --- Elimina record
                delete
              endif
              endscan
              * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
              * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
              * --- Ordino per il prezzo piu' basso
              Select * from ContPrez where not deleted() ;
              Order By Coprezzo into cursor tempor
              if used("ContPrez")
                Select contPrez
                use
              endif
            endif
            if reccount()=0
              * --- Nessun contratto valido trovato
              if empty(this.w_CODCON)
                this.w_CODCON = space(15)
                this.w_CONUMERO = space(15)
              endif
            else
              go top
              this.w_GIOAPP = nvl(cogioapp,0)
              this.w_CONUMERO = nvl(conumero,space(15))
            endif
          else
            this.w_CONUMERO = space(15)
          endif
        endif
        if not empty(this.w_CONUMERO)
          * --- Ho un contratto valido
          if this.w_GIOAPP > 0
            * --- Sposta la data di impegno per il ripristino della Scorta di Sicurezza ricalcolandola in base al Lead time del contratto
            this.DatInput = dtos(i_DATSYS)
            this.w_Leadt = -this.w_GIOAPP*Elabor.prcoerss
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select Elabor
            replace dateva with this.DatRisul, datidx with dtos(this.DatRisul), datper with dtoc(this.DatRisul), keyidx with left(keyidx,this.w_RootKeyIdx)+space(30)+datidx+"7", prgioapp with Elabor.prgioapp
          endif
        else
          * --- Non ho un contratto valido
          Select Elabor
          if prgioapp > 0
            * --- Sposta la data di impegno per il ripristino della Scorta di Sicurezza
            this.DatInput = dtos(i_DATSYS)
            this.w_Leadt = -prgioapp*prcoerss
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select Elabor
            replace dateva with this.DatRisul, datidx with dtos(this.DatRisul), datper with dtoc(this.DatRisul), keyidx with left(keyidx,this.w_RootKeyIdx)+space(30)+datidx+"7", prgioapp with Elabor.prgioapp
          endif
        endif
      endif
      endscan
    endif
    local l_Periodo, l_PeriodoD, l_PeriodoW, l_PeriodoM, l_PeriodoQ, l_PeriodoF, l_PeriodoS
    l_PeriodoW = "dtos(ttod(dateva) - dow(ttod(dateva), 2) + 1)" 
 l_PeriodoM = "dtos(ttod(dateva) - day(ttod(dateva)) + 1)" 
 l_PeriodoQ = "dtos(date(year(dateva), 3 * quarter(dateva) - 2 ,1))" 
 l_PeriodoF = "dtos(date(year(dateva), 4 * ceiling(month(dateva)*3/12) - 3 ,1))" 
 l_PeriodoS = "dtos(date(year(dateva), 6 * ceiling(month(dateva)*2/12) - 5 ,1))"
    l_Periodo = "iif(prperpia='W', " + l_PeriodoW + ",  iif(prperpia='M', " + l_PeriodoM + ", iif(prperpia='Q', " + l_PeriodoQ + ", iif(prperpia='F', " + l_PeriodoF + " , iif(prperpia='S', " + l_PeriodoS + ", dtos(dateva) )))))"
    update elabor set datper=&l_Periodo where tiprec<>"0"
    update elabor set keyidx=left(keyidx,this.w_RootKeyIdx) + iif(nvl(arflcom1,"N")="C", datper + codcom + codatt, codcom + codatt + datper) + left(tiprec,1)
    this.w_CodSaldo = Space(this.w_LenCodSal)
    this.w_SaldProg = 0
    if this.oParentObject.w_UPDELA AND this.oParentObject.w_INTERN
      * --- Crea indice su ODL ver velocizzare l'aggiornamento da messaggi
      if this.w_PPMRPDIV="S"
        Select Elabor_bis 
 index on odlmsg for not(empty(odlmsg)) tag odlmsg 
 set order to keyidx
      endif
      Select Elabor 
 index on odlmsg for not(empty(odlmsg)) tag odlmsg 
 set order to keyidx
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione variabili
    * --- Elabora i Fabbisogni oppure genera ODL
    * --- Variabili utilizzate nella gestione del Log degli errori
    * --- Indica se sono stati effettuati ulteriori filtri sui magazzini (da zoom in GSAC_KGF)
    * --- Indica se sono stati effettuati ulteriori filtri sui gruppi di magazzino (da zoom in GSAC_KGF)
    * --- Variabili per la gestione dei filtri su zoom
    * --- Tipo di impegno da anagrafica articolo
    * --- Variabili di appoggio per Magazzini impegno
    * --- Variabile che contiene il codice del magazzino di impegno utilizzato
    * --- Variabile che contiene il flag nettificabile del magazzino di impegno utilizzato
    this.w_MESSRIP = this.oParentObject.w_MESSRIPIA="S"
    this.w_PEGGING = this.oParentObject.w_PEGGING2="S"
    this.w_MRPLOG = "N"
    this.w_ODLprog = 1
    this.w_RIGEN = iif(not this.oParentObject.w_INTERN or this.oParentObject.w_MODELA="N", "M", "\")
    this.w_RollBack = "D"
    this.w_SKIP = TRUE
    this.w_CRIFOR = iif(empty(nvl(this.oParentObject.w_CRITFORN,"")),"N",this.oParentObject.w_CRITFORN)
    this.w_LOperaz = "MR"
    this.w_LOra = time()
    this.w_LDettTec = "N"
    this.w_LSelOpe = "T"
    this.w_COEFFA = 1
    this.w_COEFFA1 = 1
    this.w_GENODA = "N"
    this.w_BIDODA = "N"
    this.w_ELABID = "N"
    this.w_SELEIMPE = "A"
    this.w_SELEZDOC = "N"
    this.w_SELEZART = "N"
    this.w_IMPEPROD = "S"
    this.w_ORDIPROD = "S"
    this.w_codsal = space(this.w_LenCodSal)
    this.w_UTCV = 0
    this.w_UTDV = ctod("  /  /  ")
    this.w_OPV = " "
    this.w_UseProgBar = g_UseProgBar and vartype(this.Padre.w_PROGBAR)="O"
    if this.w_UseProgBar
      this.w_PROGBAR = this.Padre.w_PROGBAR
    endif
    this.w_KEYRIF = SYS(2015)
    this.w_DELORD = "T"
    this.w_ELAPDF = "F"
    this.w_ELAPDS = "S"
    this.w_PPFLAROB = "S"
    if TYPE("this.padre.w_KEYRIF")="C"
      this.w_KEYRIF = this.Padre.w_KEYRIF
    endif
    if TYPE("this.padre.w_CRIELA")="C"
      this.w_CRIELA = this.Padre.w_CRIELA
    else
      this.w_CRIELA = "A"
    endif
    * --- Se criterio di elaborazione non settato restituisco errore
    * --- w_PERPIA (Pianificazione per periodo)
    *     D = Day
    *     W = Week
    *     M = Month
    *     Q = Qarter
    *     F = Four Month
    *     S = Semestral
    *     A = Da anagrafica articoli
    if TYPE("this.padre.w_PERPIA")="C"
      this.w_PERPIA = this.Padre.w_PERPIA
    else
      this.w_PERPIA = "D"
    endif
    * --- w_PIAPUN (Pianificazione puntuale)
    *     S = Si
    *     N = No
    *     A = Da anagrafica articoli
    if TYPE("this.padre.w_PIAPUN")="C"
      this.w_PIAPUN = this.Padre.w_PIAPUN
    else
      this.w_PIAPUN = "N"
    endif
    if Empty(NVL(this.w_CRIELA, SPACE(1)))
      ah_errormsg("Attenzione!%0Non � possibile eseguire l'elaborazione MRP senza aver specificato il criterio di elaborazione. Elaborazione abortita!",16)
      this.w_CHECKINIT = 999
      i_retcode = 'stop'
      return
    endif
    if Empty(NVL(this.w_PERPIA, SPACE(1)))
      ah_errormsg("Attenzione!%0Non � possibile eseguire l'elaborazione MRP senza aver specificato un valore relativo al periodo di pianificazione. Elaborazione abortita!",16)
      this.w_CHECKINIT = 999
      i_retcode = 'stop'
      return
    endif
    if Empty(NVL(this.w_PIAPUN, SPACE(1)))
      ah_errormsg("Attenzione!%0Non � possibile eseguire l'elaborazione MRP senza aver specificato un valore relativo alla pianificazione puntuale. Elaborazione abortita!",16)
      this.w_CHECKINIT = 999
      i_retcode = 'stop'
      return
    endif
    if this.w_CRIELA = "A"
      * --- Se il criterio di elaborazione � aggregata forzo sempre per sicurezza l'eliminazione di tutt gli ordini suggeriti
      this.w_DELORD = "T"
    else
      if TYPE("this.padre.w_DELORD")="C"
        if Empty(NVL(this.Padre.w_DELORD, SPACE(1)))
          this.Padre.w_DELORD = "T"
          this.Padre.SetControlsValue()     
        endif
        this.w_DELORD = this.Padre.w_DELORD
      else
        this.w_DELORD = "T"
      endif
    endif
    * --- Create temporary table TMPDOCUM
    i_nIdx=cp_AddTableDef('TMPDOCUM') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\COLA\EXE\QUERY\GSMR50BGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPDOCUM_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Pianificazione per Magazzino
    *     3 + 20 + 5 + 15 + 15 + 8 + 1 = 67
    *     Pianificazione Aggregata
    *     3 + 20 + 15 + 15 + 8 + 1 = 62
    this.w_LenKeyIdx = IIF(this.w_CRIELA = "A", 61, 66)
    * --- la radice
    *     3 + 20 + 5 = 28
    *     3 + 20 = 22
    this.w_RootKeyIdx = IIF(this.w_CRIELA = "A", 23, 28)
    if .f.
      * --- Pianificazione per Magazzino
      *     3 + 40 + 5 + 15 + 15 + 8 + 1 = 87
      *     Pianificazione Aggregata
      *     3 + 40 + 15 + 15 + 8 + 1 = 82
      this.w_LenKeyIdx = IIF(this.w_CRIELA = "A", 81, 86)
      * --- La radice
      *     3 + 40 + 5 = 48
      *     3 + 40 = 43
      this.w_RootKeyIdx = IIF(this.w_CRIELA = "A", 43, 48)
    endif
    this.w_FILTERS = false
    this.w_SELEZ = "N"
    this.w_MATOUP = this.Padre.w_PPMATOUP
    if TYPE("this.padre.w_MRPLOG")="C"
      this.w_MRPLOG = this.Padre.w_MRPLOG
      if this.w_MRPLOG="S"
        this.w_LogTitle = space(10)+"Log elaborazione %1 %2 %3"
      endif
      this.w_NOTMRP = iif(type("this.padre.w_NOTMRP")<>"U",this.Padre.w_NOTMRP,"")
      if type("this.padre.w_CODINI")="C"
        this.w_MICOM = this.Padre.w_MICOM
        this.w_MIODL = this.Padre.w_MIODL
        this.w_MIOCL = this.Padre.w_MIOCL
        this.w_MIODA = this.Padre.w_MIODA
      endif
      if type("this.padre.w_ELAPDF")="C"
        this.w_ELAPDF = this.Padre.w_ELAPDF
      endif
      if type("this.padre.w_ELAPDS")="C"
        this.w_ELAPDS = this.Padre.w_ELAPDS
      endif
      if type("this.padre.w_PPFLAROB")="C"
        this.w_PPFLAROB = this.Padre.w_PPFLAROB
      endif
      if this.oParentObject.w_MODELA="R" and type("this.padre.w_CODINI")="C"
        this.w_CODINI = this.Padre.w_CODINI
        this.w_CODFIN = this.Padre.w_CODFIN
        this.w_SALINI = this.Padre.w_SALINI
        this.w_SALFIN = this.Padre.w_SALFIN
        this.w_FAMAINI = this.Padre.w_FAMAINI
        this.w_FAMAFIN = this.Padre.w_FAMAFIN
        if type("this.padre.w_FAMINI")="C"
          this.w_FAMINI = this.Padre.w_FAMINI
        endif
        if type("this.padre.w_FAMFIN")="C"
          this.w_FAMFIN = this.Padre.w_FAMFIN
        endif
        this.w_GRUINI = this.Padre.w_GRUINI
        this.w_GRUFIN = this.Padre.w_GRUFIN
        if type("this.padre.w_PIAINI")="C"
          this.w_PIAINI = this.Padre.w_PIAINI
        endif
        if type("this.padre.w_PIAFIN")="C"
          this.w_PIAFIN = this.Padre.w_PIAFIN
        endif
        this.w_MAGINI = this.Padre.w_MAGINI
        this.w_MAGFIN = this.Padre.w_MAGFIN
        this.w_MARINI = this.Padre.w_MARINI
        this.w_MARFIN = this.Padre.w_MARFIN
        if type("this.padre.w_LISINI")="C"
          this.w_LISINI = this.Padre.w_LISINI
        endif
        if type("this.padre.w_LISFIN")="C"
          this.w_LISFIN = this.Padre.w_LISFIN
        endif
        this.w_COMINI = this.Padre.w_COMINI
        this.w_COMFIN = this.Padre.w_COMFIN
        this.w_COMODL = this.Padre.w_COMODL
        this.w_NUMINI = this.Padre.w_NUMINI
        this.w_NUMFIN = this.Padre.w_NUMFIN
        this.w_SERIE1 = this.Padre.w_SERIE1
        this.w_SERIE2 = this.Padre.w_SERIE2
        this.w_DOCINI = NVL(this.Padre.w_DOCINI,ctod("  -  -  "))
        this.w_DOCFIN = NVL(this.Padre.w_DOCFIN,ctod("  -  -  "))
        this.w_INIELA = this.Padre.w_INIELA
        this.w_FINELA = this.Padre.w_FINELA
        this.w_ORIODL = this.Padre.w_ORIODL
        this.w_INICLI = this.Padre.w_INICLI
        this.w_FINCLI = this.Padre.w_FINCLI
        this.w_CATINI = this.Padre.w_CATINI
        this.w_CATFIN = this.Padre.w_CATFIN
        this.w_STIPART = this.Padre.w_STIPART
        this.w_PROFIN = this.Padre.w_PROFIN
        this.w_SEMLAV = this.Padre.w_SEMLAV
        this.w_MATPRI = this.Padre.w_MATPRI
        this.w_SELEIMPE = this.Padre.w_SELEIMPE
        if type("this.padre.w_MATAUS")="C"
          this.w_MATAUS = this.Padre.w_MATAUS
          this.w_MATCON = this.Padre.w_MATCON
          this.w_IMBALL = this.Padre.w_IMBALL
        endif
        this.w_FILTERSART = .F.
        this.w_FILTERSDOC = .F.
        this.w_FILTERSODL = .F.
        this.w_GENODA = iif(empty(nvl(this.Padre.w_GENODA," ")),"N",this.Padre.w_GENODA)
        this.w_ORDIPROD = this.Padre.w_ORDIPROD
        this.w_IMPEPROD = this.Padre.w_IMPEPROD
        * --- Filtro articoli
        this.w_FILTROART = this.w_CODINI+this.w_CODFIN+this.w_FAMAINI+this.w_FAMAFIN+this.w_FAMINI+this.w_FAMFIN+this.w_GRUINI+this.w_GRUFIN
        this.w_FILTROART = this.w_FILTROART+this.w_MARINI+this.w_MARFIN+this.w_INICLI+this.w_FINCLI+this.w_COMINI+this.w_COMFIN+this.w_SERIE1+this.w_SERIE2
        this.w_FILTROART = this.w_FILTROART+this.w_MAGINI+this.w_MAGFIN+this.w_CATINI+this.w_CATFIN
        this.w_FILTERSART = IIF(this.oParentObject.w_MODELA="N", False, !empty(alltrim(this.w_FILTROART)) or this.oParentObject.w_LLCINI<>0 or this.oParentObject.w_LLCFIN<>999 or this.w_GENODA="S" or this.w_STIPART="N")
        this.w_SELEZART = IIF(this.w_FILTERSART, "S", "N")
        * --- Filtro documenti
        * --- or w_NUMINI<>1 or w_NUMFIN<>999999 or w_NUMREI<>1 or w_NUMREF<>999999 or ! empty(w_DOCINI) or ! empty(w_DOCFIN) or ! empty(w_REGINI) or ! empty(w_REGFIN) or w_CAUSALI<>0 or w_INIELA<>ctod('01-01-1900') or w_FINELA<>ctod('31-12-2099')
        this.w_FILTRODOC = this.w_INICLI+this.w_FINCLI+this.w_COMINI+this.w_COMFIN+this.w_SERIE1+this.w_SERIE2
        if .f.
          this.w_FILTERSDOC = IIF(this.oParentObject.w_MODELA="N", False, !Empty(this.w_FILTRODOC) or this.w_NUMINI<>1 or this.w_NUMFIN<>999999999999999 or ! empty(this.w_DOCINI) or ! empty(this.w_DOCFIN) or this.oParentObject.w_CAUSALI<>0 or this.w_ORDIPROD<>"S")
        endif
        this.w_FILTERSDOC = IIF(this.oParentObject.w_MODELA="N", False, !Empty(this.w_FILTRODOC) or this.w_NUMINI<>1 or this.w_NUMFIN<>999999999999999 or ! empty(this.w_DOCINI) or ! empty(this.w_DOCFIN) or this.oParentObject.w_CAUSALI<>0)
        this.w_FILTERSDOC = IIF(this.oParentObject.w_MODELA="N", False, this.w_FILTERSDOC or (this.w_ORIODL $ "D-T" and (this.w_INIELA<>ctod("01-01-1900") or this.w_FINELA<>ctod("31-12-2099"))))
        this.w_SELEZDOC = IIF(this.w_FILTERSDOC, "S", "N")
        * --- Filtro Ordini di produzione
        this.w_FILTERSODL = .F.
        this.w_FILTROODL = this.w_COMINI+this.w_COMFIN
        this.w_FILTERSODL = IIF(this.oParentObject.w_MODELA="N", False, !Empty(this.w_FILTROODL) )
        this.w_FILTERSODL = IIF(this.oParentObject.w_MODELA="N", False, this.w_FILTERSODL or (this.w_ORIODL $ "O-T" and (this.w_INIELA<>ctod("01-01-1900") or this.w_FINELA<>ctod("31-12-2099"))))
        this.w_SELEZODL = IIF(this.w_FILTERSODL, "S", "N")
        * --- Altri filtri
        this.w_BIDODA = IIF(IIF(this.oParentObject.w_MODELA="N", False, this.w_FILTERSDOC or this.oParentObject.w_LLCINI<>0 or this.oParentObject.w_LLCFIN<>999), "S", "N")
        this.w_FILTERS = IIF(this.oParentObject.w_MODELA="N", false, !empty(alltrim(this.w_APPO)) or this.oParentObject.w_LLCINI<>0 or this.oParentObject.w_LLCFIN<>999 or this.w_GENODA="S" or this.w_NUMINI<>1 or this.w_NUMFIN<>999999999999999 or !empty(this.w_DOCINI) or !empty(this.w_DOCFIN) or this.oParentObject.w_CAUSALI<>0 or this.w_INIELA<>ctod("01-01-1900") or this.w_FINELA<>ctod("31-12-2099")or this.w_STIPART="N")
        this.w_SELEZ = IIF(this.w_FILTERSART OR this.w_FILTERSDOC, "S", "N")
        this.w_SELEZ = IIF(this.w_FILTERSART OR this.w_FILTERSDOC, "S", "N")
        this.w_SUGG = IIF( this.w_SELEZDOC="S" or this.w_SELEZART="S", "S", "N")
        this.w_ELACAT = this.Padre.w_ELACAT
        if Empty(NVL(this.Padre.w_STAORD, SPACE(1)))
          this.Padre.w_STAORD = "S"
          this.Padre.SetControlsValue()     
        endif
        this.w_STAORD = this.Padre.w_STAORD
        this.w_ORDMPS = this.Padre.w_ORDMPS
        this.w_DISMAG = this.Padre.w_DISMAG
        this.w_GIANEG = this.Padre.w_GIANEG
        this.w_ELABID = this.Padre.w_ELABID
        this.w_MICOM = this.Padre.w_MICOM
        this.w_MIODL = this.Padre.w_MIODL
        this.w_MIOCL = this.Padre.w_MIOCL
        this.w_MIODA = this.Padre.w_MIODA
        this.w_MAGFOR = this.Padre.w_MAGFOR
        this.w_DISMAGFO = this.Padre.w_DISMAGFO
        this.w_MAGFOC = this.Padre.w_MAGFOC
        this.w_MAGFOL = this.Padre.w_MAGFOL
        this.w_MAGFOA = this.Padre.w_MAGFOA
        this.NC = this.Padre.w_SZOOM.cCursor
        * --- Inserisco le scelte effettuate per le causali di documento da elaborare in un campo MEMO
        Select(this.NC) 
 scan 
 scatter memvar
        this.w_TIPDOCM = this.w_TIPDOCM+left(m.tdtipdoc+space(5),5)+left(m.tddesdoc+space(35),35)+left(m.aggsaldi+space(9),9)+alltrim(str(m.xchk))+chr(13)+chr(10)
        endscan
        this.NM = this.Padre.w_ZOOMMAGA.cCursor
        * --- Inserisco le scelte effettuate per magazzini/gruppi da elaborare in un campo MEMO
        * --- Nel caso di elaborazione aggregata si considerano tutti i magazzini
        this.w_LISMAG = ""
        if this.w_CRIELA<>"A"
          Select(this.NM) 
 scan 
 scatter memvar
          if m.xchk=1
            this.w_LISMAG = this.w_LISMAG+ALLTRIM(m.mgcodmag)+CHR(255)
          endif
          endscan
        endif
      endif
    endif
    * --- Ritorna il valore della variabile w_SELMAG utilizzata come parametro nella query GSMR2BGPG
    if this.w_CRIELA $ "G-M"
      * --- Select from MAGA_TEMP
      i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT(*) AS NUMREC  from "+i_cTable+" MAGA_TEMP ";
            +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.w_KEYRIF)+"";
             ,"_Curs_MAGA_TEMP")
      else
        select COUNT(*) AS NUMREC from (i_cTable);
         where MGSELEZI="S" AND MGKEYRIF=this.w_KEYRIF;
          into cursor _Curs_MAGA_TEMP
      endif
      if used('_Curs_MAGA_TEMP')
        select _Curs_MAGA_TEMP
        locate for 1=1
        do while not(eof())
        this.w_NRMAGSEL = _Curs_MAGA_TEMP.NUMREC
          select _Curs_MAGA_TEMP
          continue
        enddo
        use
      endif
      this.w_SELZZMAG = this.w_CRIELA $ "M-G" Or this.w_NRMAGSEL>0
    endif
    this.w_SELMAG = IIF(this.w_SELZZMAG, "S", "N")
    this.w_SUGG = IIF( this.w_SUGG="S" or this.w_SELMAG="S", "S", "N")
    this.w_GRUPPO = "|"
    if this.w_CRIELA = "G"
      * --- Select from MAGA_TEMP
      i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MGCODMAG  from "+i_cTable+" MAGA_TEMP ";
            +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.w_KEYRIF)+"";
             ,"_Curs_MAGA_TEMP")
      else
        select MGCODMAG from (i_cTable);
         where MGSELEZI="S" AND MGKEYRIF=this.w_KEYRIF;
          into cursor _Curs_MAGA_TEMP
      endif
      if used('_Curs_MAGA_TEMP')
        select _Curs_MAGA_TEMP
        locate for 1=1
        do while not(eof())
        this.w_GRUPPO = this.w_GRUPPO+alltrim(_Curs_MAGA_TEMP.MGCODMAG)+"|"
          select _Curs_MAGA_TEMP
          continue
        enddo
        use
      endif
    else
      * --- Select from MAGA_TEMP
      i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select DISTINCT MGCODMAG AS MGCODMAG  from "+i_cTable+" MAGA_TEMP ";
            +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.w_KEYRIF)+"";
             ,"_Curs_MAGA_TEMP")
      else
        select DISTINCT MGCODMAG AS MGCODMAG from (i_cTable);
         where MGSELEZI="S" AND MGKEYRIF=this.w_KEYRIF;
          into cursor _Curs_MAGA_TEMP
      endif
      if used('_Curs_MAGA_TEMP')
        select _Curs_MAGA_TEMP
        locate for 1=1
        do while not(eof())
        this.w_GRUPPO = this.w_GRUPPO+alltrim(_Curs_MAGA_TEMP.MGCODMAG)+"|"
          select _Curs_MAGA_TEMP
          continue
        enddo
        use
      endif
    endif
    if this.w_UseProgBar
      if this.w_PARAM<>"TGSMR1BGP"
        this.w_bTITLE = alltrim(this.oparentobject.cComment)
      else
        this.w_bTITLE = ah_msgformat("ELABORAZIONE MRP-ORDERS")
      endif
    endif
    if this.oParentObject.w_INTERN
      this.w_MRINTEXT = "I"
      this.w_CHKDATI = this.oParentObject.w_CHECKDATI="S"
      this.w_CURCHECK = 1
      this.w_NTOTCHECK = 6
      if this.w_CHKDATI
        if this.w_UseProgBar
          GesProgBar("M", this, 0, this.w_NTOTCHECK, ah_msgformat("Test Correttezza Dati in Corso..."))
        else
          ah_Msg("Verifica correttezza dati",.T.)
        endif
        do while this.w_CHKDATI and this.w_CURCHECK<=this.w_NTOTCHECK
          * --- Controlli preliminari su correttezza dei dati
          *     1- Dati su ODL_MAST corrotti
          *     2- Dati su ODL_DETT corrotti
          *     3- Dati su Layout incompleti
          *     4- Dati su Risorse incompleti
          *     5- Dati su KEY_ARTI corrotti
          *     6- Dati su DIS_COMP corrotti
          *     7- Disponibilit� Articoli Fantasma Negativa
          *     8- Disponibilit� Contabile Articoli Fantasma Negativa
          this.w_QUERYCHK = "..\COLA\EXE\QUERY\GSMR_BK"+alltrim(str(this.w_CURCHECK))
          if this.w_CURCHECK=1
            vq_exec(this.w_QUERYCHK, this, "__tmp__")
            Select * from __tmp__ into cursor __tmp__ READWRITE
          else
            vq_exec(this.w_QUERYCHK, this, "aux")
            if recCount("aux")>0
              * --- Duplica i dati su cursore base
              Select aux 
 Scan 
 scatter memvar 
 insert into __tmp__ from memvar 
 endscan
            endif
          endif
          this.w_CURCHECK = this.w_CURCHECK + 1
          if this.w_UseProgBar
            GesProgBar("S", this, this.w_CURCHECK, this.w_NTOTCHECK)
          endif
          if g_PRFA="S" and g_CICLILAV="S"
            if this.w_CURCHECK=5
              this.w_CURCHECK = this.w_CURCHECK + 1
            endif
          else
            if this.w_CURCHECK=3
              this.w_CURCHECK = 6
            endif
          endif
        enddo
        if used("__tmp__") and RecCount("__tmp__")>0
          if this.w_UseProgBar
            GesProgBar("E", this)
          endif
          * --- Duplica i record su __tmp__
          ah_ErrorMsg("Dati non validi: elaborazione abortita","STOP","")
          cp_chprn("..\COLA\EXE\QUERY\GSMR_BK1", " ", this)
          this.w_CHKDATI = False
        endif
        USE IN SELECT("__TMP__")
        USE IN SELECT("AUX")
        if not this.w_CHKDATI
          i_retcode = 'stop'
          return
        endif
      endif
    else
      this.w_MRINTEXT = "E"
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione degli errori
    do case
      case empty(this.w_LogErrori)
        if this.oParentObject.w_MODELA="R"
          this.w_RollBack = "-"
          * --- Aggiorna il periodo assoluto (Piano ODL) degli ODL
          if g_APPLICATION="ADHOC REVOLUTION"
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_cOp2=cp_SetTrsOp(this.w_OPV,'UTCV','this.w_UTCV',this.w_UTCV,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_OPV,'UTDV','this.w_UTDV',this.w_UTDV,'update',i_nConn)
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="OLCODODL"
              do vq_exec with 'GSMR9BGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTPERAS = _t2.TPPERASS";
              +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +i_ccchkf;
                  +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
                  +"ODL_MAST.OLTPERAS = _t2.TPPERASS";
              +",ODL_MAST.UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",ODL_MAST.UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
                  +"OLTPERAS,";
                  +"UTCV,";
                  +"UTDV";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.TPPERASS,";
                  +cp_NullLink(i_cOp2,'ODL_MAST','UTCV')+",";
                  +cp_NullLink(i_cOp3,'ODL_MAST','UTDV')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
                  +"OLTPERAS = _t2.TPPERASS";
              +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTPERAS = (select TPPERASS from "+i_cQueryTable+" where "+i_cWhere+")";
              +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='impossibile ripristinare piano ODL'
              return
            endif
          else
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_cOp2=cp_SetTrsOp(this.w_OPV,'UTCV','this.w_UTCV',this.w_UTCV,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_OPV,'UTDV','this.w_UTDV',this.w_UTDV,'update',i_nConn)
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="OLCODODL"
              do vq_exec with 'GSMR9BGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTPEROL = _t2.TPPERASS";
              +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +i_ccchkf;
                  +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
                  +"ODL_MAST.OLTPEROL = _t2.TPPERASS";
              +",ODL_MAST.UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",ODL_MAST.UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
                  +"OLTPEROL,";
                  +"UTCV,";
                  +"UTDV";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.TPPERASS,";
                  +cp_NullLink(i_cOp2,'ODL_MAST','UTCV')+",";
                  +cp_NullLink(i_cOp3,'ODL_MAST','UTDV')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
                  +"OLTPEROL = _t2.TPPERASS";
              +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTPEROL = (select TPPERASS from "+i_cQueryTable+" where "+i_cWhere+")";
              +",UTCV ="+cp_NullLink(i_cOp2,'ODL_MAST','UTCV');
              +",UTDV ="+cp_NullLink(i_cOp3,'ODL_MAST','UTDV');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='impossibile ripristinare piano ODL'
              return
            endif
          endif
          * --- Ripristina lo stato degli ODL
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("M"),'ODL_MAST','OLTSTATO');
                +i_ccchkf ;
            +" where ";
                +"OLTSTATO = "+cp_ToStrODBC("-");
                   )
          else
            update (i_cTable) set;
                OLTSTATO = "M";
                &i_ccchkf. ;
             where;
                OLTSTATO = "-";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Oggetto per messaggi incrementali
        this.w_oMess=createobject("Ah_Message")
        do case
          case this.w_ErrMsg="del odl_mast" or this.w_ErrMsg="del odl_dett"
            this.w_oMess.AddMsgPartNL("Impossibile eliminare vecchi ordini di lavorazione")     
          case this.w_ErrMsg="ins odl_mast"
            this.w_oMess.AddMsgPartNL("Impossibile inserire nuovi ordini di lavorazione")     
          case this.w_ErrMsg="ins odl_dett"
            this.w_oPart = this.w_oMess.AddMsgPartNL("Impossibile aggiornare ordine di lavorazione '%1'")
            this.w_oPart.AddParam(this.w_OLCODODL)     
          case this.w_ErrMsg="ins saldiart"
            this.w_oMess.AddMsgPartNL("Impossibile aggiornare i saldi di magazzino%0")     
          case this.w_ErrMsg="ins saldicom"
            this.w_oMess.AddMsgPartNL("Impossibile aggiornare i saldi di magazzino per commessa%0")     
          case this.w_ErrMsg="PH"
            if empty(right(this.w_CodSaldo,20))
              this.w_oPart = this.w_oMess.AddMsgPartNL("La disponibilit� dell'articolo <%1> � negativa: impossibile proseguire elaborazione correttamente")
              this.w_oPart.AddParam(alltrim(left(this.w_CodSaldo,20)))     
            else
              this.w_oPart = this.w_oMess.AddMsgPartNL("La disponibilit� dell'articolo <%1> � negativa: impossibile proseguire elaborazione correttamente")
              this.w_oPart.AddParam(alltrim(left(this.w_CodSaldo,20)))     
            endif
          otherwise
            this.w_oPart = this.w_oMess.AddMsgPartNL("%1")
            this.w_oPart.AddParam(this.w_ErrMsg)     
        endcase
        this.w_oMess.AddMsgPartNL("L'elaborazione non � stata scritta sul database")     
        if this.w_ErrMsg<>"PH"
          this.w_oPart = this.w_oMess.AddMsgPartNL("%0Error= %1")
          this.w_oPart.AddParam(message())     
        endif
        this.w_oMess.Ah_ErrorMsg("STOP")     
      case this.w_LogErrori="ODP-RIT"
        * --- ODP in ritardo
        this.w_Dateva = dateva
        this.w_OLTCODIC = codric
        Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
        (this.w_OLCODODL, this.w_OLTCODIC, this.w_QTA, this.w_Datini, this.w_Dateva, ;
        iif(this.w_Dateva<i_DATSYS, ah_Msgformat("Pianificato nel passato"), ah_Msgformat("Ordine in ritardo") ))
      case this.w_LogErrori="ODL-RIT"
        * --- ODL in ritardo
        Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
        (this.w_OLCODODL, this.w_OLTCODIC, m.quanti, m.datini, m.dateva, ;
        iif(ltperr, ah_Msgformat("Data inizio inferiore al limite dell'orizzonte di pianificazione ODL"), ;
        iif(m.dateva < i_DATSYS, ah_Msgformat("Pianificato nel passato"), ah_Msgformat("Ordine in ritardo") )))
      case this.w_LogErrori="MPSERR"
        * --- Data inizio MPS dell'ODL precedente il periodo scaduto
        Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
        (this.w_OLCODODL, this.w_OLTCODIC, m.quanti, m.datini, m.dateva, ;
        ah_Msgformat("Data inizio MPS inferiore al limite dell'orizzonte di pianificazione ODL") )
      case this.w_LogErrori="CMPOBSO"
        * --- ODL/OCL Per Articoli Obsoleti
        this.w_OLTCODIC = codric
        Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
        (this.w_OLCODODL, SPACE(41) , m.quanti, m.datini, cp_CharToDate("  -  -  "), ah_Msgformat("Riga %1: codice di ricerca: %2 obsoleto dal %3", alltrim(str(this.w_CPROWORD)),alltrim(this.w_OLTCODIC), DTOC(m.datobso) ) )
      case this.w_LogErrori $"ARTOBSO ODAOBSO"
        * --- ODL/OCL Per Articoli Obsoleti
        this.w_OLTCODIC = codric
        if this.w_LogErrori="ARTOBSO"
          Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
          (this.w_OLCODODL, this.w_OLTCODIC, m.quanti, m.datini, m.dateva, ah_Msgformat("Ordine per articolo obsoleto (dal %1)", DTOC(m.datobso) ) )
        else
          Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
          (this.w_OLCODODL, this.w_OLTCODIC, m.quanti, m.datini, m.dateva, ah_Msgformat("Ordine di Acquisto per articolo obsoleto (dal %1)", DTOC(m.datobso) ) )
        endif
      case this.w_LogErrori="Display"
        * --- Errore durante la generazione degli OCL
        this.TmpC = "Si sono verificati %1 errori di pianificazione durante l'elaborazione MRP%0Desideri la stampa dell'elenco degli errori?"
        if ah_YesNo(this.TmpC,"",alltrim(str(this.w_LNumErr,5,0)))
          * --- Lancia Stampa
          if g_APPLICATION="ADHOC REVOLUTION"
            do GSCO_BLE with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            do GSDB_BLE with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.w_LogErrori="Insert" and RecCount("LogErrori")>0
        * --- Scrive log errori
        * --- Try
        local bErr_05D0F610
        bErr_05D0F610=bTrsErr
        this.Try_05D0F610()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Impossibile aggiornare log-errori","STOP","")
        endif
        bTrsErr=bTrsErr or bErr_05D0F610
        * --- End
      case this.w_LogErrori="Insert" and RecCount("LogErrori")=0
        USE IN SELECT("LogErrori")
      case this.w_LogErrori="CIC-DT"
        * --- Ciclo preferenziale non valido alla data
        Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
        (this.w_OLCODODL, this.w_OLTCODIC, m.quanti, m.datini, m.dateva, ;
        ah_msgformat("Distinta con ciclo preferenziale non valido alla data selezionata"))
      case this.w_LogErrori="CIC-00"
        * --- Ciclo preferenziale non esistente
        Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
        (this.w_OLCODODL, this.w_OLTCODIC, m.quanti, m.datini, m.dateva, ;
        ah_msgformat("Distinta priva di ciclo preferenziale"))
    endcase
    this.w_LogErrori = ""
  endproc
  proc Try_05D0F610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Determina progressivo log
    this.w_LSerial = space(10)
    this.w_LSerial = cp_GetProg("PRD_ERRO","PRERR",this.w_LSerial,i_CODAZI)
    * --- Scrive LOG
    if not empty(this.w_LSerial)
      Select LogErrori
      scan
      this.w_LNumErr = this.w_LNumErr + 1
      if empty(ODL)
        this.w_LOggErr = codric
        this.w_LErrore = rtrim(messagg) + iif(Quan<>0, " ( "+trans(Quan,v_pq(12))+" )", "")
        this.w_LErrore = this.w_LErrore + iif(empty(datini), "", " "+ah_Msgformat("(al %1)",dtoc(datini) ) )
      else
        this.w_LOggErr = ODL
        this.w_LErrore = messagg
      endif
      * --- Insert into PRD_ERRO
      i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRD_ERRO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LESERIAL"+",LEROWNUM"+",LEOPERAZ"+",LEDATOPE"+",LEORAOPE"+",LECODUTE"+",LECODICE"+",LEERRORE"+",LESTAMPA"+",LESTORIC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_LSerial),'PRD_ERRO','LESERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LNumErr),'PRD_ERRO','LEROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LOperaz),'PRD_ERRO','LEOPERAZ');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PRD_ERRO','LEDATOPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LOra),'PRD_ERRO','LEORAOPE');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PRD_ERRO','LECODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LOggErr),'PRD_ERRO','LECODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LErrore),'PRD_ERRO','LEERRORE');
        +","+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTAMPA');
        +","+cp_NullLink(cp_ToStrODBC("S"),'PRD_ERRO','LESTORIC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LESERIAL',this.w_LSerial,'LEROWNUM',this.w_LNumErr,'LEOPERAZ',this.w_LOperaz,'LEDATOPE',i_DATSYS,'LEORAOPE',this.w_LOra,'LECODUTE',i_CODUTE,'LECODICE',this.w_LOggErr,'LEERRORE',this.w_LErrore,'LESTAMPA',"N",'LESTORIC',"S")
        insert into (i_cTable) (LESERIAL,LEROWNUM,LEOPERAZ,LEDATOPE,LEORAOPE,LECODUTE,LECODICE,LEERRORE,LESTAMPA,LESTORIC &i_ccchkf. );
           values (;
             this.w_LSerial;
             ,this.w_LNumErr;
             ,this.w_LOperaz;
             ,i_DATSYS;
             ,this.w_LOra;
             ,i_CODUTE;
             ,this.w_LOggErr;
             ,this.w_LErrore;
             ,"N";
             ,"S";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if this.w_MESSRIP
        this.w_MRSERIAL = this.w_MRSERIAL+1
        * --- Insert into MRP_MESS
        i_nConn=i_TableProp[this.MRP_MESS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
          +","+cp_NullLink(cp_ToStrODBC(LogErrori.odl),'MRP_MESS','MRCODODL');
          +","+cp_NullLink(cp_ToStrODBC(LogErrori.codric),'MRP_MESS','MRCODRIC');
          +","+cp_NullLink(cp_ToStrODBC(LogErrori.quan),'MRP_MESS','MRQTAODL');
          +","+cp_NullLink(cp_ToStrODBC("WR"),'MRP_MESS','MRTIPOMS');
          +","+cp_NullLink(cp_ToStrODBC(padr(this.w_LErrore,80)),'MRP_MESS','MRDESCRI');
          +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAIN');
          +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAFI');
          +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
          +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
          +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',LogErrori.odl,'MRCODRIC',LogErrori.codric,'MRQTAODL',LogErrori.quan,'MRTIPOMS',"WR",'MRDESCRI',padr(this.w_LErrore,80),'MRDATAIN',{},'MRDATAFI',{},'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
          insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
             values (;
               padl(alltrim(str(this.w_MRSERIAL)),10,"0");
               ,i_DATSYS;
               ,LogErrori.odl;
               ,LogErrori.codric;
               ,LogErrori.quan;
               ,"WR";
               ,padr(this.w_LErrore,80);
               ,{};
               ,{};
               ,"N";
               ,{};
               ,0;
               ,this.w_MRINTEXT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      endscan
      USE IN SELECT("LogErrori")
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSDB_BLT
    * --- Calcola la data di inizio in base al Lead Time
    SET_NEAR = Set("NEAR")
    SET NEAR ON
    this.DatRisul = {}
    this.w_DatErr = FALSE
    this.w_DatErrMsg = ""
    this.w_Leadt = nvl(this.w_Leadt,0)
    select Calend
    if seek(this.DatInput)
      * --- Data evasione trovata
      skip -ceiling(this.w_Leadt)
      if bof()
        * --- Data anteriore all'orizzonte di pianificazione
        this.w_DatErrMsg = "Raggiunto limite inferiore dell'orizzonte di pianificazione"
        this.w_DatErr = TRUE
      else
        if eof()
          * --- Oltre l'ultimo record
          go bottom
        endif
      endif
    else
      if EOF()
        * --- Data posteriore all'orizzonte di pianificazione
        this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
        this.w_DatErr = TRUE
      else
        skip -(ceiling(this.w_Leadt)+1)
        if bof()
          * --- Data anteriore all'orizzonte di pianificazione
          this.w_DatErrMsg = "Raggiunto limite inferiore dell'orizzonte di pianificazione"
          this.w_DatErr = TRUE
        endif
      endif
    endif
    this.DatRisul = cagiorno
    SET NEAR &SET_NEAR
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Lettura elaborazione
    * --- Parametri Messaggi MRP
    * --- ODL Suggeriti
    * --- ODL Pianificati
    * --- ODL Lanciati
    * --- OCL Suggeriti
    * --- OCL Da ordinare
    * --- OCL Ordinati
    * --- ODA Suggeriti
    * --- ODA Da ordinare
    * --- ODA Ordinati
    * --- --
    select Elabor
    if nvl(arflcomm, "N")="S"
      set order to codsal_key
      Select distinct nvl(codcom,space(15)) as codcom, nvl(codatt,space(15)) as codatt ; 
 from Elabor where codsal=this.w_CodSaldo into cursor comatt
      Select Elabor 
 set order to keyidx 
 Select comatt
      * --- --Si utilizza il cursore commatt solo nel caso in cui 
      *     all'articolo sia associata una commessa
      scan
      this.Page_15()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      endscan
      use in select("comatt")
    else
      this.Page_15()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ANTICIPO DATA ODL
    Select Ripiani
    go top
    if TIPREC="0"
      SKIP
    endif
    this.w_DATALIMITE = recno()
    do while not(EOF())
      * --- Messaggi anticipare l'ODL
      if Left(tiprec,1)="4" 
        this.w_Disponibilita = Ripiani.disponibilita-Ripiani.quanti
        this.w_odlmsg = Ripiani.odlmsg
        this.w_ROWELAB = Ripiani.rowelab
        this.w_datiniAppo = Ripiani.dateva
        if this.w_Disponibilita<0
          this.w_ENMSG = False
          this.w_CHKFAB = True
          this.w_STAOD = RIGHT(Ripiani.tiprec,1)
          this.w_ODA = substr(Ripiani.tiprec,2,3)="PDA" or nvl(Ripiani.prove,"I")="E"
          this.w_OCL = nvl(Ripiani.prove,"I")="L"
          do case
            case this.w_STAOD="P"
              this.w_ENMSG = iif(this.w_ODA,this.w_PMDANTIC="S",iif(this.w_OCL,this.w_PMIANTIC="S",this.w_PMPANTIC="S"))
              this.w_CHKFAB = iif(this.w_ODA,this.w_PMDANCOP="S",iif(this.w_OCL,this.w_PMIANCOP="S",this.w_PMPANCOP="S"))
            case this.w_STAOD="M"
              this.w_ENMSG = iif(this.w_ODA,this.w_PMMANTIC="S",iif(this.w_OCL,this.w_PMCANTIC="S",this.w_PMSANTIC="S"))
              this.w_CHKFAB = iif(this.w_ODA,this.w_PMMANCOP="S",iif(this.w_OCL,this.w_PMCANCOP="S",this.w_PMSANCOP="S"))
            case this.w_STAOD="O"
              if this.w_ODA and not empty(nvl(Ripiani.odamsg,""))
                this.w_ENMSG = iif(this.w_ODA,this.w_PMRANTIC="S",this.w_PMOANTIC="S")
                this.w_CHKFAB = iif(this.w_ODA,this.w_PMRANCOP="S",this.w_PMOANCOP="S")
              else
                this.w_odlmsg = Ripiani.odamsg
                this.w_ENMSG = this.w_PMRANTIC="S"
                this.w_CHKFAB = this.w_PMRANCOP="S"
              endif
          endcase
          * --- --� stato anticipato un impegno devo adeguare la data del ODL, devo memorizzare la data
          *     di evasione dell'ordine andare avanti nel cursore trovarare ODL e cambiare la sua data
          * --- Salva puntatore al record
          this.w_point = recno()
          this.w_quantiODL = Ripiani.quanti
          * --- -- Scan Ripiani ricerca odl--- cambio data ripercussione figlio
          * --- --Occorre andare indietro nel tempo per determinare la data del primo fabbisogno
          this.w_change = False
          Select Ripiani
          go this.w_DATALIMITE
          do while not(this.w_change or this.w_point<recno())
            if qtanet>0 and disponibilita<0 and (this.w_quantiODL>=QTANET OR this.w_CHKFAB) and this.w_datiniAppo<>dateva
              * --- --Controllo se ODL Copre il fabbisogno precedente all'odl
              * --- --Allineare l'ordine odl all'impegno
              * --- --La data iniziale deve essere ricalcolato
              * --- --Devo variare la disponibilit� in quel periodo aggiungendo la quantit� dell'ordine
              *     ODL
              this.w_dataAppo = dateva
              this.w_DATALIMITE = recno()
              if this.w_ENMSG
                this.w_MRDESCRI = "Anticipare"
                this.w_MRTIPOMS = "AN"
                * --- Aggiorna la disponibilit�
                Select Ripiani
                do while recno() < this.w_point
                  replace disponibilita with disponibilita + this.w_quantiODL
                  replace qtanet with qtanet - this.w_quantiODL
                  skip
                enddo
                go this.w_point
                Insert into Errori (CodiceArticolo, cododl, TipoOrdini, Quantita, Datini , Datfin, Messaggi, rowelab) ; 
 values (Ripiani.codric, this.w_odlmsg, Ripiani.tiprec, Ripiani.quanti, this.w_datiniAppo, this.w_dataAppo, this.w_MRDESCRI, this.w_ROWELAB)
                this.w_MRSERIAL = this.w_MRSERIAL+1
                * --- Insert into MRP_MESS
                i_nConn=i_TableProp[this.MRP_MESS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_odlmsg),'MRP_MESS','MRCODODL');
                  +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
                  +","+cp_NullLink(cp_ToStrODBC(Ripiani.quanti),'MRP_MESS','MRQTAODL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRTIPOMS),'MRP_MESS','MRTIPOMS');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRDESCRI),'MRP_MESS','MRDESCRI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_datiniAppo),'MRP_MESS','MRDATAIN');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_dataAppo),'MRP_MESS','MRDATAFI');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
                  +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
                  +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',this.w_odlmsg,'MRCODRIC',Ripiani.codric,'MRQTAODL',Ripiani.quanti,'MRTIPOMS',this.w_MRTIPOMS,'MRDESCRI',this.w_MRDESCRI,'MRDATAIN',this.w_datiniAppo,'MRDATAFI',this.w_dataAppo,'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
                  insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                     values (;
                       padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                       ,i_DATSYS;
                       ,this.w_odlmsg;
                       ,Ripiani.codric;
                       ,Ripiani.quanti;
                       ,this.w_MRTIPOMS;
                       ,this.w_MRDESCRI;
                       ,this.w_datiniAppo;
                       ,this.w_dataAppo;
                       ,"N";
                       ,{};
                       ,0;
                       ,this.w_MRINTEXT;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
              Select Ripiani
              go this.w_point
              this.w_change = TRUE
            endif
            if not eof()
              skip
            endif
          enddo
          if not(this.w_change) and left(tiprec,2)<>"7S" and left(tiprec,2)<>"0S"
            Select Ripiani
            go(this.w_point)
            if this.w_ENMSG
              Insert into Errori (CodiceArticolo,cododl, TipoOrdini, Quantita, Datini , Datfin, Messaggi) ; 
 values (Ripiani.codric, this.w_odlmsg, Ripiani.tiprec, Ripiani.quanti, {}, {}, "Non � possibile anticipare la data") 
              this.w_MRSERIAL = this.w_MRSERIAL+1
              * --- Insert into MRP_MESS
              i_nConn=i_TableProp[this.MRP_MESS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_odlmsg),'MRP_MESS','MRCODODL');
                +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
                +","+cp_NullLink(cp_ToStrODBC(Ripiani.quanti),'MRP_MESS','MRQTAODL');
                +","+cp_NullLink(cp_ToStrODBC("AX"),'MRP_MESS','MRTIPOMS');
                +","+cp_NullLink(cp_ToStrODBC("Non � possibile anticipare la data"),'MRP_MESS','MRDESCRI');
                +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAIN');
                +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAFI');
                +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
                +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
                +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',this.w_odlmsg,'MRCODRIC',Ripiani.codric,'MRQTAODL',Ripiani.quanti,'MRTIPOMS',"AX",'MRDESCRI',"Non � possibile anticipare la data",'MRDATAIN',{},'MRDATAFI',{},'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
                insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                   values (;
                     padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                     ,i_DATSYS;
                     ,this.w_odlmsg;
                     ,Ripiani.codric;
                     ,Ripiani.quanti;
                     ,"AX";
                     ,"Non � possibile anticipare la data";
                     ,{};
                     ,{};
                     ,"N";
                     ,{};
                     ,0;
                     ,this.w_MRINTEXT;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              Select Ripiani
              if not eof()
                skip
                do while not eof() and this.w_quantiODL>0 and qtanet>0 and this.w_datiniAppo=dateva 
                  this.w_quantiODL = this.w_quantiODL - qtanet
                  skip
                  this.w_DATALIMITE = min(recno(), reccount())
                enddo
              endif
            endif
          endif
          go this.w_point
        else
          this.w_DATALIMITE = recno()
        endif
      endif
      if not(eOF())
        skip
      endif
    enddo
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --POSTICIPO DATA ODL O CANCELLAZIONE ORDINE
    Select Ripiani
    go bottom
    this.w_TODEL = disponibilita
    this.w_DATALIMITE = reccount()
    do while not(BOF())
      * --- Messaggi ritardare l'ODL
      if Left(tiprec,1)="4" 
        this.w_Disponibilita = disponibilita-quanti
        * --- Salva date ODL
        this.w_dataAppo = dateva
        * --- Salvata puntatore al record sulla posizione dell'odl da posticipare
        this.w_point = recno()
        skip
        this.w_DATAIMPE = 0
        do while not eof() and this.w_dataAppo=dateva
          if qtanet>0
            this.w_Disponibilita = this.w_Disponibilita - qtanet
            this.w_DATAIMPE = recno()
          endif
          skip
        enddo
        go this.w_point
        if this.w_Disponibilita>=0 
          * --- --In questo caso l'ordine potrebbe essere stato posticipato, occorre adeguare l'odl all'ordine occorre andare avanti nel
          *      tempo e controllare se esiste una disponibilit� negativa che copre il fabbisogno, altrimenti viene eliminato,se questo � 
          *     verificato si adegua la data, se la disponibilit� dei periodi successivi risulta essere sempre positiva
          *     in questo caso l'odl viene cancellato
          this.w_ENMSG = False
          this.w_ENDEL = False
          this.w_CHKFAB = True
          this.w_STAOD = RIGHT(tiprec,1)
          this.w_ODA = substr(Ripiani.tiprec,2,3)="PDA" or nvl(Ripiani.prove,"I")="E"
          this.w_OCL = nvl(Ripiani.prove,"I")="L"
          do case
            case this.w_STAOD="P"
              this.w_ENMSG = iif(this.w_ODA,this.w_PMDPOSTI="S",iif(this.w_OCL,this.w_PMIPOSTI="S",this.w_PMPPOSTI="S"))
              this.w_ENDEL = iif(this.w_ODA,this.w_PMDANNUL="S",iif(this.w_OCL,this.w_PMIANNUL="S",this.w_PMPANNUL="S"))
              this.w_CHKFAB = iif(this.w_ODA,this.w_PMDPOCOP="S",iif(this.w_OCL,this.w_PMIPOCOP="S",this.w_PMPPOCOP="S"))
              this.w_DAYSPO = iif(this.w_ODA,this.w_PMDPO_GG,iif(this.w_OCL,this.w_PMIPO_GG,this.w_PMPPO_GG))
            case this.w_STAOD="M"
              this.w_ENMSG = iif(this.w_ODA,this.w_PMMPOSTI="S",iif(this.w_OCL,this.w_PMCPOSTI="S",this.w_PMSPOSTI="S"))
              this.w_ENDEL = iif(this.w_ODA,this.w_PMMANNUL="S",iif(this.w_OCL,this.w_PMCANNUL="S",this.w_PMSANNUL="S"))
              this.w_CHKFAB = iif(this.w_ODA,this.w_PMMPOCOP="S",iif(this.w_OCL,this.w_PMCPOCOP="S",this.w_PMSPOCOP="S"))
              this.w_DAYSPO = iif(this.w_ODA,this.w_PMMPO_GG,iif(this.w_OCL,this.w_PMCPO_GG,this.w_PMSPO_GG))
            case this.w_STAOD="L"
              this.w_ENMSG = iif(this.w_ODA,this.w_PMRPOSTI="S",this.w_PMLPOSTI="S")
              this.w_ENDEL = iif(this.w_ODA,this.w_PMRANNUL="S",this.w_PMLCHIUD="S")
              this.w_DAYSPO = iif(this.w_ODA,this.w_PMRPO_GG,this.w_PMLPO_GG)
            case this.w_STAOD="O"
              this.w_ENMSG = iif(this.w_ODA,this.w_PMRPOSTI="S",this.w_PMOPOSTI="S")
              this.w_ENDEL = iif(this.w_ODA,this.w_PMRANNUL="S",this.w_PMOANNUL="S")
              this.w_CHKFAB = iif(this.w_ODA,this.w_PMRPOCOP="S",this.w_PMOPOCOP="S")
              this.w_DAYSPO = iif(this.w_ODA,this.w_PMRPO_GG,this.w_PMOPO_GG)
              if this.w_ODA and not empty(nvl(Ripiani.odamsg,""))
                this.w_odlmsg = Ripiani.odamsg
              endif
          endcase
          * --- ---Ricerca data
          * --- --Salvataggio fabbisogno
          this.w_quantiFab = quanti
          this.w_dataAppo = dateva
          this.w_datiniAppo = datini
          this.w_day = this.w_dataAppo-this.w_datiniAppo
          this.w_odlmsg = odlmsg
          this.w_ROWELAB = rowelab
          this.w_datiniAppo = dateva
          * --- --POSTICIPO DATA ODL O CANCELLAZIONE ORDINE
          this.w_change = FALSE
          this.w_DATDIFF = True
          this.w_CANDEL = True
          do while not this.w_change and RECNO()<=this.w_DATALIMITE and not eof()
            if qtanet>0 and disponibilita<this.w_quantiFab and (qtanet<=this.w_quantiFab OR this.w_CHKFAB) and this.w_datiniAppo<>dateva
              * --- --ho trovato il periodo in cui L'odl va spostato
              this.w_dataAppo = dateva
              if ! this.w_CHKFAB
                this.w_DATALIMITE = RECNO()
              endif
              skip -1
              * --- Aggiorna la disponibilit�
              do while recno()>this.w_point
                replace disponibilita with disponibilita - this.w_quantiFab
                skip -1
              enddo
              go this.w_point
              if this.w_DAYSPO <= 0
                this.w_CHKDAY = True
              else
                if vartype(this.w_dataAppo)="T"
                  this.w_CHKDAY = (this.w_DAYSPO <= (this.w_dataAppo-this.w_datiniAppo)/86400)
                else
                  this.w_CHKDAY = (this.w_DAYSPO <= this.w_dataAppo-this.w_datiniAppo)
                endif
              endif
              if this.w_ENMSG AND this.w_CHKDAY
                this.w_MRDESCRI = "Posticipare"
                this.w_MRTIPOMS = "PO"
                Insert into Errori (CodiceArticolo, cododl, TipoOrdini, Quantita, Datini , Datfin, Messaggi, rowelab) ; 
 values (Ripiani.codric, this.w_odlmsg, Ripiani.tiprec, Ripiani.quanti, this.w_datiniAppo, this.w_dataAppo, this.w_MRDESCRI, this.w_ROWELAB)
                this.w_MRSERIAL = this.w_MRSERIAL+1
                * --- Insert into MRP_MESS
                i_nConn=i_TableProp[this.MRP_MESS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_odlmsg),'MRP_MESS','MRCODODL');
                  +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
                  +","+cp_NullLink(cp_ToStrODBC(Ripiani.quanti),'MRP_MESS','MRQTAODL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRTIPOMS),'MRP_MESS','MRTIPOMS');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRDESCRI),'MRP_MESS','MRDESCRI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_datiniAppo),'MRP_MESS','MRDATAIN');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_dataAppo),'MRP_MESS','MRDATAFI');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
                  +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
                  +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',this.w_odlmsg,'MRCODRIC',Ripiani.codric,'MRQTAODL',Ripiani.quanti,'MRTIPOMS',this.w_MRTIPOMS,'MRDESCRI',this.w_MRDESCRI,'MRDATAIN',this.w_datiniAppo,'MRDATAFI',this.w_dataAppo,'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
                  insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                     values (;
                       padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                       ,i_DATSYS;
                       ,this.w_odlmsg;
                       ,Ripiani.codric;
                       ,Ripiani.quanti;
                       ,this.w_MRTIPOMS;
                       ,this.w_MRDESCRI;
                       ,this.w_datiniAppo;
                       ,this.w_dataAppo;
                       ,"N";
                       ,{};
                       ,0;
                       ,this.w_MRINTEXT;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
              this.w_change = True
            endif
            if qtanet>0
              this.w_CANDEL = disponibilita>=this.w_quantiFab
              this.w_DATDIFF = this.w_datiniAppo<>dateva
            else
              this.w_DATDIFF = True
            endif
            skip
          enddo
          if this.w_DATDIFF
            if NOT(this.w_change)and left(tiprec,2)<>"7S" and left(tiprec,2)<>"0S"
              * --- --L'odl potrebbe essere cancellato
              this.w_DATALIMITE = min(RecNo(), this.w_DATALIMITE)
              go this.w_point
              if this.w_CANDEL and this.w_TODEL>=this.w_quantiFab
                if this.w_ENDEL
                  this.w_MRDESCRI = "Annullare"
                  this.w_MRTIPOMS = "CA"
                  Insert into Errori (CodiceArticolo, cododl, TipoOrdini, Quantita, Datini , Datfin, Messaggi, rowelab) ; 
 values (Ripiani.codric, this.w_odlmsg, Ripiani.tiprec, Ripiani.quanti, {}, {}, this.w_MRDESCRI, this.w_ROWELAB)
                  this.w_MRSERIAL = this.w_MRSERIAL+1
                  * --- Insert into MRP_MESS
                  i_nConn=i_TableProp[this.MRP_MESS_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_odlmsg),'MRP_MESS','MRCODODL');
                    +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
                    +","+cp_NullLink(cp_ToStrODBC(Ripiani.quanti),'MRP_MESS','MRQTAODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MRTIPOMS),'MRP_MESS','MRTIPOMS');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MRDESCRI),'MRP_MESS','MRDESCRI');
                    +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAIN');
                    +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAFI');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
                    +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
                    +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',this.w_odlmsg,'MRCODRIC',Ripiani.codric,'MRQTAODL',Ripiani.quanti,'MRTIPOMS',this.w_MRTIPOMS,'MRDESCRI',this.w_MRDESCRI,'MRDATAIN',{},'MRDATAFI',{},'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
                    insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                       values (;
                         padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                         ,i_DATSYS;
                         ,this.w_odlmsg;
                         ,Ripiani.codric;
                         ,Ripiani.quanti;
                         ,this.w_MRTIPOMS;
                         ,this.w_MRDESCRI;
                         ,{};
                         ,{};
                         ,"N";
                         ,{};
                         ,0;
                         ,this.w_MRINTEXT;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                  Select Ripiani
                  this.w_TODEL = this.w_TODEL - this.w_quantiFab
                  * --- Aggiorna la disponibilit�
                  do while not eof()
                    replace disponibilita with disponibilita - this.w_quantiFab
                    skip
                  enddo
                endif
              else
                Insert into Errori (CodiceArticolo, cododl, TipoOrdini, Quantita, Datini , Datfin, Messaggi, rowelab) ; 
 values (Ripiani.codric, this.w_odlmsg, Ripiani.tiprec, Ripiani.quanti, {}, {}, ; 
 "Non � possibile posticipare: quantit� insufficiente", this.w_ROWELAB)
                this.w_MRSERIAL = this.w_MRSERIAL+1
                * --- Insert into MRP_MESS
                i_nConn=i_TableProp[this.MRP_MESS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_odlmsg),'MRP_MESS','MRCODODL');
                  +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
                  +","+cp_NullLink(cp_ToStrODBC(Ripiani.quanti),'MRP_MESS','MRQTAODL');
                  +","+cp_NullLink(cp_ToStrODBC("PX"),'MRP_MESS','MRTIPOMS');
                  +","+cp_NullLink(cp_ToStrODBC("Non � possibile posticipare: quantit� insufficiente"),'MRP_MESS','MRDESCRI');
                  +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAIN');
                  +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAFI');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
                  +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
                  +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',this.w_odlmsg,'MRCODRIC',Ripiani.codric,'MRQTAODL',Ripiani.quanti,'MRTIPOMS',"PX",'MRDESCRI',"Non � possibile posticipare: quantit� insufficiente",'MRDATAIN',{},'MRDATAFI',{},'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
                  insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                     values (;
                       padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                       ,i_DATSYS;
                       ,this.w_odlmsg;
                       ,Ripiani.codric;
                       ,Ripiani.quanti;
                       ,"PX";
                       ,"Non � possibile posticipare: quantit� insufficiente";
                       ,{};
                       ,{};
                       ,"N";
                       ,{};
                       ,0;
                       ,this.w_MRINTEXT;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
            endif
          else
            this.w_DATALIMITE = RECNO()
          endif
          go this.w_point
        else
          * --- --� stato anticipato un impegno devo adeguare la data del ODL, devo memorizzare la data
          *     di evasione dell'ordine andare avanti nel cursore trovarare ODL e cambiare la sua data
          * --- --Salvataggio puntatore al record
          this.w_DATALIMITE = max(this.w_DATAIMPE,recno())
        endif
      endif
      if not(BOF())
        skip(-1)
      endif
    enddo
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Allinea il cursore di elaborazione ai messaggi proposti
    Select Errori
    scan
    this.w_odlmsg = cododl
    this.w_dataAppo = datfin
    do case
      case Messaggi="Anticipare" or Messaggi="Posticipare"
        Select Elabor
        if this.oParentObject.w_MODELA="N"
          set order to odlmsg
          * --- Segna da elaborare gli articoli appartenenti alla lista dei materiali
          select distinct codsal from elabor where padrered=this.w_odlmsg and tiprec="7OLIM" into cursor compon
          Select Elabor 
 set order to codsal_key
          Update Elabor set elabora=.t. where codsal in (select codsal from compon)
        endif
        Select Elabor 
 set order to codsal
        if this.w_PPMRPDIV="S"
          Select Elabor_bis 
 set order to codsal 
 Select Elabor
        endif
        * --- Aggiorna testata
        go Errori.rowelab
        * --- Lead-Time Sicurezza
        this.w_Leadt = nvl(prsafelt, 0)
        if this.w_Leadt>0
          this.DatInput = dtos(this.w_dataAppo)
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_dataAppo = iif(empty(this.DatRisul), this.w_dataAppo, this.DatRisul)
        endif
        if this.w_PPMRPDIV="S"
          Select Elabor_bis 
 go Errori.rowelab
          this.w_oltemlav = Elabor_bis.oltemlav
          Select Elabor
        else
          this.w_oltemlav = Elabor.oltemlav
        endif
        if this.w_oltemlav=0
          this.w_datiniAppo = this.w_dataAppo
        else
          * --- Lead-Time Produzione
          this.w_Leadt = this.w_oltemlav
          this.DatInput = dtos(this.w_dataAppo)
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_datiniAppo = iif(this.w_dataAppo<this.DatRisul or empty(this.DatRisul), this.w_dataAppo, this.DatRisul)
        endif
        Select Elabor
        Replace datini with this.w_datiniAppo, dateva with this.w_dataAppo, datcon with this.w_dataAppo, datidx with dtos(this.w_dataAppo)
        Replace keyidx with left(keyidx, this.w_RootKeyIdx+30)+datidx+right(keyidx,1)
        if this.w_PPMRPDIV="S"
          Select Elabor_bis 
 Replace keyidx with left(keyidx, 73)+dtos(this.w_dataAppo)+right(keyidx,1) 
 Select Elabor
        endif
        * --- Esamina i materiali
        set order to odlmsg
        locate for odlmsg=this.w_odlmsg rest
        do while not eof()
          if tiprec="7OL"
            if this.w_PPMRPDIV="S"
              this.w_recelabor = recno()
              Select Elabor_bis 
 go this.w_recelabor
              this.w_oltemlav = Elabor_bis.oltemlav
              Select Elabor
            else
              this.w_oltemlav = Elabor.oltemlav
            endif
            if this.w_oltemlav=0
              this.w_datcon = this.w_datiniAppo
            else
              * --- Lead-Time Correzione
              this.w_Leadt = this.w_oltemlav
              this.DatInput = dtos(this.w_datiniAppo)
              this.Page_9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_datcon = iif(this.w_datiniAppo<this.DatRisul or empty(this.DatRisul), this.w_datiniAppo, this.DatRisul)
              Select Elabor
            endif
            Replace dateva with this.w_datcon, datcon with this.w_datcon, datidx with dtos(this.w_datcon)
            Replace keyidx with left(keyidx, this.w_RootKeyIdx+30)+datidx+right(keyidx,1)
            if this.w_PPMRPDIV="S"
              this.w_recelabor = recno()
              Select Elabor_bis 
 set order to odlmsg 
 go this.w_recelabor 
 Replace keyidx with left(keyidx, 73)+dtos(this.w_datcon)+right(keyidx,1) 
 Select Elabor
            endif
          endif
          continue
        enddo
      case Messaggi="Annullare"
        Select Elabor
        if this.oParentObject.w_MODELA="N"
          set order to odlmsg
          * --- Segna da elaborare gli articoli appartenenti alla lista dei materiali
          select distinct codsal from elabor where padrered=this.w_odlmsg and tiprec="7OLIM" into cursor compon
          Select Elabor 
 set order to codsal_key
          Update Elabor set elabora=.t. where codsal in (select codsal from compon)
        endif
        Select Elabor 
 set order to codsal
        * --- Elimina testata
        go Errori.rowelab
        delete
        if this.w_PPMRPDIV="S"
          Select Elabor_bis 
 set order to codsal
          go Errori.rowelab
          delete
          set order to odlmsg 
 Select Elabor
        endif
        * --- Elimina i materiali
        set order to odlmsg
        locate for odlmsg=this.w_odlmsg rest
        do while not eof()
          if tiprec="7OL"
            Replace tiprec with "7OL---"
            if this.w_PPMRPDIV="S"
              this.w_recelabor = recno()
              Select Elabor_bis
              set order to odlmsg
              go this.w_recelabor
              Replace tiprec with "7OL---"
              Select Elabor
            endif
          endif
          continue
        enddo
    endcase
    endscan
    Select Errori
    zap
    if this.w_PPMRPDIV="S"
      Select Elabor_bis 
 set order to keyidx
    endif
    Select Elabor
    set order to keyidx
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Memorizzo la posizione attuale sul cursore Elab.
    *     Alla fine dell'elaborazione dei giorni di copertura ci devo tornare.
    this.w_RecSel = recno()
    * --- Mi posiziono sul Codice da verificare
    Select Elabor 
 indexseek(this.w_ChiaveInd,.t.)
    Select Elabor 
 set order to codsal
    locate for codsal=this.w_CodSaldo and tiprec<>"0SALDI" rest 
    if found()
      if prgiocop = 0
        * --- Giorni di copertura non gestiti, proseguo (torno a Pag.1).
      else
        * --- Gestione con Giorni di Copertura
        *     ---------------------------------------------------
        * --- Inizializzazione variabili
        this.w_quantiTOT = 0
        this.w_qtalorTOT = 0
        this.w_qtanetTOT = 0
        this.w_quantvTOT = 0
        this.w_quanfvTOT = 0
        this.w_qtaava = 0
        this.AggPad = .f.
        this.Scoproc = .f.
        this.w_DGioCop = space(8)
        this.w_CodCanc = space(15)
        * --- Ciclo per raggruppare i periodi
        do while not eof() and codsal=this.w_CodSaldo
          if tiprec="8MRP-S" or tiprec="8PDA-S"
            * --- Il record relativo al ripristino della scorta di sicurezza non deve essere accorpato con un ordine nello scaduto, anche se � incluso nel periodo.
            *     In questa situazione quando devo fare un record che soddisfa un impegno con tiprec='7SCO-S' chiudo il periodo attivo e ne apro uno nuovo.
            skip -1
            if tiprec="7SCO-S" and quanti<>0 and ! this.ScoProc
              * --- Esiste solo un record di scorta di sicurezza per codice articolo, Scoproc serve per evitare anomalie in caso di cancellazione del record 
              *     relativo alla scorta di sicurezza (cancellazione che avviene se la quantit� avanzata dal periodo precedente copre la Scorta di Sicurezza).
              this.w_ScoMin = .t.
              this.Scoproc = .t.
            else
              this.w_ScoMin = .f.
            endif
            skip
            if this.w_qtaava>=qtanet
              * --- C'� un avanzo del periodo precedente che mi consente di non fare questo ordine.
              * --- Aggiorno l'avanzo del periodo
              this.w_qtaava = this.w_qtaava-qtanet
              * --- Elimino il record corrente
              this.w_CodCanc = cododl
              this.w_RecAtt = recno()
              if not empty(this.w_CodCanc)
                Select Elabor 
 set order to cododl
                delete for cododl=this.w_CodCanc
                if this.w_PPMRPDIV="S"
                  Select Elabor_bis 
 set order to cododl
                  delete for cododl=this.w_CodCanc
                  Select Elabor_bis 
 set order to codsal
                endif
                Select Elabor 
 set order to codsal
              endif
              * --- Mi riposiziono sul record precedente perch� questo non esiste pi� (l'ho appena cancellato) 
              *     in modo che la skip alla fine del ciclo vada al record da aggiornare successivo a questo.
              go this.w_RecAtt 
 skip -1
              this.w_CodCanc = space(15)
            else
              * --- Non c'� avanzo dal periodo precedente o comunque l'avanzo non � sufficiente a coprire il fabbisogno.
              if empty(this.w_DGioCop)
                * --- Il primo ordine suggerito che trovo � quello sul quale raggruppare, inizializzo il periodo sulla base di esso
                * --- Calcolo il periodo di copertura
                this.w_Datidx = dtos(dateva)
                this.DatInput = dtos(dateva)
                this.w_Leadt = -prgiocop
                this.Page_9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_DGioCop = dtos(this.DatRisul)
                Select Elabor
                * --- Inizializzo il periodo
                this.AggPad = .t.
                this.w_RecOrdSOK = recno()
                this.w_CodNCanc = cododl
                Select Elabor
                this.w_quantiORI = quanti
                this.w_qtalorORI = qtalor
                this.w_qtanetORI = qtanet
                this.w_quantvORI = quantv
                this.w_quanfvORI = quanfv
                this.w_quantiTOT = quanti-this.w_qtaava
                this.w_qtalorTOT = qtalor-this.w_qtaava
                this.w_qtanetTOT = qtanet-this.w_qtaava
                this.w_quantvTOT = quantv+this.w_qtaava
                this.w_quanfvTOT = quanfv+this.w_qtaava
                * --- Aggiorno le quantit� del record (se  necessario)
                if this.w_qtaava<>0
                  replace quanti with this.w_quantiTOT, qtalor with this.w_qtalorTOT,qtanet with this.w_qtanetTOT, quantv with this.w_quantvTOT, quanfv with this.w_quanfvTOT
                endif
                this.w_qtaava = 0
              else
                * --- Non sono sicuramente all'inizio del periodo, verifico se sono alla fine del periodo o su un record che ripristina la Scorta di Sicurezza
                *     (in questo caso devo aggiornare l'ordine relativo al periodo precedente) oppure se mi trovo su un ordine da accorpare.
                if this.w_datidx<=dtos(dateva) and dtos(dateva)<=this.w_DGioCop and ! this.w_ScoMin
                  * --- Accorpo le quantit� ed elimino l'ordine corrente e tutti i suoi figli
                  this.w_quantiTOT = this.w_quantiTOT+quanti-this.w_qtaava
                  this.w_qtalorTOT = this.w_qtalorTOT+qtalor-this.w_qtaava
                  this.w_qtanetTOT = this.w_qtanetTOT+qtanet-this.w_qtaava
                  this.w_quantvTOT = this.w_quantvTOT+quantv+this.w_qtaava
                  this.w_quanfvTOT = this.w_quanfvTOT+quanfv+this.w_qtaava
                  this.w_qtaava = 0
                  this.w_CodCanc = cododl
                  this.w_RecAtt = recno()
                  if not empty(this.w_CodCanc)
                    Select Elabor 
 set order to cododl
                    delete for cododl=this.w_CodCanc
                    if this.w_PPMRPDIV="S"
                      Select Elabor_bis 
 set order to cododl
                      delete for cododl=this.w_CodCanc
                      Select Elabor_bis 
 set order to codsal
                    endif
                    Select Elabor 
 set order to codsal
                  endif
                  this.w_CodCanc = space(15)
                  go this.w_RecOrdSOK 
 replace quanti with this.w_quantiTOT, qtalor with this.w_qtalorTOT,qtanet with this.w_qtanetTOT, quantv with this.w_quantvTOT, quanfv with this.w_quanfvTOT
                  * --- Mi riposiziono sul record precedente perch� questo non esiste pi� (l'ho appena cancellato) 
                  *     in modo che la skip alla fine del ciclo vada al record da aggiornare successivo a questo.
                  go this.w_RecAtt 
 skip-1
                else
                  * --- Ordine da non accorpare al periodo in esame.
                  *     Chiudo l'ordine del periodo e mi riposiziono sul presente record che rappresenter� 
                  *     l'inizio del periodo successivo.
                  if this.AggPad
                    this.w_RecAtt = recno()
                    * --- Applico le politiche di lottizzazione sull'ordine
                    if not empty(this.w_CodNCanc)
                      Select Elabor 
 go this.w_RecOrdSOK
                      if cododl=this.w_CodNCanc
                        this.w_Fabnet = qtanet-this.w_qtaava
                        this.oldqtanet = this.w_Fabnet
                        if arflcomm<>"S"
                          codcom=space(15)
                          codatt=space(15)
                          if arpropre="L"
                            this.w_CODCON = nvl(prcodfor,space(15))
                          endif
                          do case
                            case arpropre="E"
                              * --- Cerca il miglior fornitore dai ContrattiA
                              * --- Seleziona Criterio di scelta
                              do case
                                case this.w_CRIFOR="A"
                                  * --- Affidabilit�
                                  OrdinaPer = "Order by coaffida desc"
                                case this.w_CRIFOR="T"
                                  * --- Tempo = giorni di approvvigionamento
                                  OrdinaPer = "Order by cogioapp"
                                case this.w_CRIFOR="R"
                                  * --- Ripartizione
                                  OrdinaPer = "Order by coperrip"
                                case this.w_CRIFOR="I"
                                  * --- Priorit�
                                  OrdinaPer = "Order by copriori desc"
                                otherwise
                                  OrdinaPer = ""
                              endcase
                              if g_GESCON="S"
                                if this.w_CRIFOR<>"Z"
                                  * --- Seleziona i ContrattiA validi e li ordina per il criterio di scelta
                                  Select * from ContrattiA ;
                                  where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
                                  &OrdinaPer into cursor tempor
                                else
                                  * --- Calcola Prezzo da Contratto
                                  * --- Prima genera un temporaneo contenente le righe articolo interessate dei ContrattiA validi
                                  * --- ordinate per Fornitore + Qta Scaglione
                                  * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
                                  Select * from ContrattiA ;
                                  where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin ;
                                  and nvl(coquanti,0)>=this.w_fabnet and not empty(nvl(cocodclf," ")) and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
                                  order by cocodclf, coquanti into cursor Contprez
                                  * --- Calcola Prezzo da Contratto
                                  * --- Elabora Criterio per Miglior Prezzo
                                  =WrCursor("Contprez")
                                  Index On cocodclf+str(coquanti,12,3) Tag Codice
                                  * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
                                  this.w_appo1 = "######"
                                  select Contprez
                                  go top
                                  scan
                                  if cocodclf <> this.w_APPO1
                                    * --- Nuovo Fornitore, Calcola Prezzo da Contratto
                                    this.w_PZ = NVL(COPREZZO, 0)
                                    this.w_S1 = NVL(COSCONT1, 0)
                                    this.w_S2 = NVL(COSCONT2, 0)
                                    this.w_S3 = NVL(COSCONT3, 0)
                                    this.w_S4 = NVL(COSCONT4, 0)
                                    this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
                                    this.w_VAL = NVL(COCODVAL, g_PERVAL)
                                    this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
                                    if this.w_VAL<>g_PERVAL
                                      * --- Se altra valuta, riporta alla Valuta di conto
                                      if this.w_CAO<>0
                                        this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
                                        replace coprezzo with this.w_PZ
                                      else
                                        * --- Cambio incongruente, record non utilizzabile
                                        this.w_PZ = -999
                                      endif
                                      select Contprez
                                    endif
                                    if this.w_PZ=-999
                                      delete
                                    else
                                      this.w_appo1 = cocodclf
                                    endif
                                  else
                                    * --- Elimina record
                                    delete
                                  endif
                                  endscan
                                  * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
                                  * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
                                  * --- Ordino per il prezzo piu' basso
                                  Select * from ContPrez where not deleted() ;
                                  Order By Coprezzo into cursor tempor
                                  if used("ContPrez")
                                    Select contPrez
                                    use
                                  endif
                                endif
                                if reccount()=0
                                  * --- Nessun contratto valido trovato
                                  if empty(this.w_CODCON)
                                    this.w_CODCON = space(15)
                                    this.w_CONUMERO = space(15)
                                  endif
                                else
                                  go top
                                  this.w_CODCON = nvl(cocodclf,space(15))
                                  this.w_QTAMIN = nvl(coqtamin,0)
                                  this.w_LOTRIO = nvl(colotmul,0)
                                  this.w_GIOAPP = nvl(cogioapp,0)
                                  this.w_CONUMERO = nvl(conumero,space(15))
                                endif
                              else
                                this.w_CONUMERO = space(15)
                              endif
                              * --- Politiche di lottizzazione
                              if Empty(this.w_CONUMERO)
                                Select Elabor
                                if arflcomm="S"
                                  this.w_LOTRIODEF = 0
                                  this.w_QTAMINDEF = 0
                                else
                                  this.w_LOTRIODEF = nvl(prlotrio,0)
                                  this.w_QTAMINDEF = nvl(prqtamin,0)
                                endif
                                this.w_LOTRIO = this.w_LOTRIODEF
                                this.w_QTAMIN = this.w_QTAMINDEF
                                this.w_GIOAPP = nvl(prgioapp,0)
                              endif
                              * --- Vincolo quantit� minima
                              if this.w_Fabnet<this.w_QTAMIN
                                this.w_Fabnet = this.w_QTAMIN
                              endif
                              * --- Vincolo lotto multiplo
                              if nvl(this.w_LOTRIO,0)>0
                                this.w_Fabnet = ceiling(this.w_Fabnet/this.w_LOTRIO)*this.w_LOTRIO
                              endif
                              * --- Gestione q.t� massima
                              Select Elabor
                              this.w_QTARES = this.w_Fabnet
                              this.w_QTAMAX = prqtamax
                              this.w_Dateva = dateva
                            case dppolpro = "M" and this.w_Fabnet<prqtamin
                              * --- Vincolo quantit� minima
                              this.w_Fabnet = prqtamin
                            case dppolpro $ "L-X"
                              * --- Vincolo lotto multiplo
                              if this.w_Fabnet<prqtamin
                                this.w_Fabnet = prqtamin
                              endif
                              if nvl(prlotrio,0)>0
                                this.w_Fabnet = ceiling(this.w_Fabnet/prlotrio)*prlotrio
                              endif
                              * --- Gestione q.t� massima
                              this.w_QTARES = this.w_Fabnet
                              this.w_QTAMAX = prqtamax
                          endcase
                        endif
                        if this.w_QTAMAX<>0
                          do while this.w_QTARES>0
                            * --- Questo ciclo serve a spezzare gli ordini che non rispettano il vincolo di quantit� massima, se il vincolo non c'� la procedura fa sempre un solo 'giro'
                            if this.w_QTARES>this.w_QTAMAX
                              * --- Applico politica di lottizzazione quantit� massima
                              this.w_qtaava = this.w_QTAMAX
                              * --- Fabbisogno residuo (da valutare al prossimo giro)
                              this.w_QTARES = this.w_QTARES-this.w_qtaava
                              * --- Aggiunge l'ordine
                              Select Elabor 
 scatter memvar
                              this.riga = recno()
                              if this.w_PPMRPDIV="S"
                                Select Elabor_bis 
 go this.riga 
 scatter memvar 
 Select Elabor
                              endif
                              cododl=padl(alltrim(str(this.w_ODLprog)), 15)
                              padrered=iif(artipart $ "PH.DC.PS", m.padrered, m.cododl)
                              this.w_ODLprog = this.w_ODLprog + 1
                              if arpropre="L"
                                prcodfor=this.w_CODCON
                              endif
                              if arpropre="E"
                                contra=this.w_CONUMERO
                                prcodfor=this.w_CODCON
                                prcrifor=this.w_CRIFOR
                                arcencos=this.w_CENCOS
                                * --- Lead-Time Produzione e MPS
                                this.w_Leadt = this.w_GIOAPP
                                if this.w_Leadt>0 and m.datidx<>"19000101"
                                  this.DatRisul = Cp_chartodate(LEFT(alltrim(dtos(this.w_dateva)),4)+"-"+SUBSTR(alltrim(dtos(this.w_dateva)),5,2)+"-"+right(alltrim(dtos(this.w_dateva)),2),"yyyy-mm-dd")
                                  this.w_Datidx = dtos(dateva)
                                  this.DatInput = dtos(dateva)
                                  this.Page_9()
                                  if i_retcode='stop' or !empty(i_Error)
                                    return
                                  endif
                                  this.w_Datini = iif(this.w_dateva<this.DatRisul or empty(this.DatRisul), this.w_dateva, this.DatRisul)
                                else
                                  this.w_Datini = this.w_dateva
                                endif
                                select Elabor
                                if this.w_Leadt>0
                                  replace datini with this.w_Datini, datmps with this.w_Datini, leapro with this.w_Leadt
                                endif
                              endif
                               quanti=this.w_qtaava 
 quanfi=this.w_qtaava 
 qtalor=this.w_qtaava 
 quantv=-this.w_qtaava 
 quanfv=-this.w_qtaava 
 qtanet=this.oldqtanet 
 append blank 
 gather memvar
                              if this.w_PPMRPDIV="S"
                                Select Elabor_bis
                                 append blank 
 gather memvar
                                Select Elabor
                              endif
                              * --- Impegna i componenti
                              this.w_QTA = this.w_qtaava
                              this.w_Datini = datini
                              this.w_Dateva = dateva
                              this.w_RifMate = cprownum
                              if arpropre<>"E"
                                this.Page_2()
                                if i_retcode='stop' or !empty(i_Error)
                                  return
                                endif
                              endif
                              Select Elabor
                              go this.riga
                            else
                              if this.w_PPMRPDIV="S"
                                this.w_recelabor = recno()
                                Select Elabor_bis
                                go this.w_recelabor
                                replace quanfi with this.w_QTARES
                                Select Elabor
                              endif
                              replace quanti with this.w_QTARES, quanfi with this.w_QTARES, qtalor with this.w_QTARES, quantv with -this.w_QTARES, quanfv with -this.w_QTARES, qtanet with this.oldqtanet, contra with this.w_CONUMERO,prcodfor with iif(arpropre$"E-L",this.w_CODCON,space(15)),prcrifor with iif(arpropre="E",this.w_CRIFOR,space(1)),arcencos with iif(arpropre="E",this.w_CENCOS,space(15))
                              this.w_Leadt = this.w_GIOAPP
                              if arpropre="E" and this.w_Leadt>0
                                replace datini with this.w_Datini, datmps with this.w_Datini, leapro with this.w_Leadt
                              endif
                              if this.w_PPMRPDIV="S"
                                Select Elabor_bis 
 set order to cododl
                                update elabor_bis set quanfi=abs(this.w_QTARES/this.w_quantiORI)*quanfi where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                                Select Elabor_bis 
 set order to codsal
                              endif
                              Select Elabor 
 set order to cododl
                              update elabor set quanti=abs(this.w_QTARES/this.w_quantiORI)*quanti, qtalor=abs(this.w_QTARES/this.w_quantiORI)*qtalor, qtanet=abs(this.w_QTARES/this.w_quantiORI)*qtanet, quanfi=abs(this.w_QTARES/this.w_quantiORI)*quanfi, quantv=abs(this.w_QTARES/this.w_quantiORI)*quantv, quanfv=abs(this.w_QTARES/this.w_quantiORI)*quanfv where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                              Select Elabor 
 set order to codsal
                              * --- Mi riposiziono sul record precedente in modo che la skip alla fine del ciclo ritorni qui e faccia iniziare il nuovo periodo.
                              select Elabor
                              go this.w_RecAtt 
 skip-1
                              * --- Se sono all'ultimo giro tengo l'avanzo
                              this.w_qtaava = this.w_Fabnet-this.oldqtanet
                              this.w_QTARES = 0
                            endif
                          enddo
                        else
                          this.w_qtaava = this.w_Fabnet-this.oldqtanet
                          if this.w_PPMRPDIV="S"
                            this.w_recelabor = recno()
                            Select Elabor_bis
                            go this.w_recelabor
                            replace quanfi with this.w_Fabnet
                            Select Elabor
                          endif
                          replace quanti with this.w_Fabnet, quanfi with this.w_Fabnet, qtalor with this.w_Fabnet, quantv with -this.w_Fabnet, quanfv with -this.w_Fabnet, qtanet with this.oldqtanet, contra with this.w_CONUMERO,prcodfor with iif(arpropre$"E-L",this.w_CODCON,space(15)),prcrifor with iif(arpropre="E",this.w_CRIFOR,space(1)),arcencos with iif(arpropre="E",this.w_CENCOS,space(15))
                          if arpropre="E"
                            * --- Lead-Time Produzione e MPS
                            this.w_Leadt = this.w_GIOAPP
                            if this.w_Leadt>0 and datidx<>"19000101"
                              this.DatRisul = Cp_chartodate(LEFT(alltrim(dtos(this.w_dateva)),4)+"-"+SUBSTR(alltrim(dtos(this.w_dateva)),5,2)+"-"+right(alltrim(dtos(this.w_dateva)),2),"yyyy-mm-dd")
                              this.w_Datidx = dtos(dateva)
                              this.DatInput = dtos(dateva)
                              this.Page_9()
                              if i_retcode='stop' or !empty(i_Error)
                                return
                              endif
                              this.w_Datini = iif(this.w_dateva<this.DatRisul or empty(this.DatRisul), this.w_dateva, this.DatRisul)
                            else
                              this.w_Datini = this.w_dateva
                            endif
                            if this.w_Leadt>0
                              Select Elabor
                              replace datini with this.w_Datini, datmps with this.w_Datini, leapro with this.w_Leadt
                            endif
                          endif
                          if this.w_PPMRPDIV="S"
                            Select Elabor_bis 
 set order to cododl
                            update elabor_bis set quanfi=abs(this.w_Fabnet/this.w_quantiORI)*quanfi where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                            Select Elabor_bis 
 set order to codsal
                          endif
                          Select Elabor 
 set order to cododl
                          update elabor set quanti=abs(this.w_Fabnet/this.w_quantiORI)*quanti, qtalor=abs(this.w_Fabnet/this.w_quantiORI)*qtalor, qtanet=abs(this.w_Fabnet/this.w_quantiORI)*qtanet, quanfi=abs(this.w_Fabnet/this.w_quantiORI)*quanfi, quantv=abs(this.w_Fabnet/this.w_quantiORI)*quantv, quanfv=abs(this.w_Fabnet/this.w_quantiORI)*quanfv where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                          Select Elabor 
 set order to codsal
                          * --- Mi riposiziono sul record precedente in modo che la skip alla fine del ciclo ritorni qui e faccia iniziare il nuovo periodo.
                          select Elabor
                          go this.w_RecAtt 
 skip-1
                        endif
                      endif
                      this.w_CodNCanc = space(15)
                    endif
                    this.AggPad = .f.
                    this.w_DGioCop = space(8)
                  endif
                endif
              endif
            endif
          endif
          skip
        enddo
        * --- Se AggPad � true sono uscito dal ciclo senza aggiornare l'ultimo periodo, lo faccio adesso
        if this.AggPad
          * --- Applico le politiche di lottizzazione sull'ordine (w_RecOrdSOK)
          if not empty(this.w_CodNCanc)
            Select Elabor 
 set order to cododl
            Select Elabor 
 go this.w_RecOrdSOK
            if cododl=this.w_CodNCanc
              if this.w_qtaava>=qtanet
                * --- C'� un avanzo del periodo precedente che mi consente di non fare questo ordine.
                this.w_qtaava = this.w_qtaava-qtanet
                this.RigadaCanc = cododl
                if not empty(this.RigadaCanc)
                  delete for cododl=this.RigadaCanc
                  if this.w_PPMRPDIV="S"
                    Select Elabor_bis 
 set order to cododl
                    delete for cododl=this.RigadaCanc
                    Select Elabor
                  endif
                endif
              else
                this.w_Fabnet = qtanet-this.w_qtaava
                this.oldqtanet = this.w_Fabnet
                if arflcomm<>"S"
                  codcom=space(15)
                  codatt=space(15)
                  if arpropre="L"
                    this.w_CODCON = nvl(prcodfor,space(15))
                  endif
                  do case
                    case arpropre="E"
                      * --- Cerca il miglior fornitore dai ContrattiA
                      * --- Seleziona Criterio di scelta
                      do case
                        case this.w_CRIFOR="A"
                          * --- Affidabilit�
                          OrdinaPer = "Order by coaffida desc"
                        case this.w_CRIFOR="T"
                          * --- Tempo = giorni di approvvigionamento
                          OrdinaPer = "Order by cogioapp"
                        case this.w_CRIFOR="R"
                          * --- Ripartizione
                          OrdinaPer = "Order by coperrip"
                        case this.w_CRIFOR="I"
                          * --- Priorit�
                          OrdinaPer = "Order by copriori desc"
                        otherwise
                          OrdinaPer = ""
                      endcase
                      if g_GESCON="S"
                        if this.w_CRIFOR<>"Z"
                          * --- Seleziona i ContrattiA validi e li ordina per il criterio di scelta
                          Select * from ContrattiA ;
                          where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
                          &OrdinaPer into cursor tempor
                        else
                          * --- Calcola Prezzo da Contratto
                          * --- Prima genera un temporaneo contenente le righe articolo interessate dei ContrattiA validi
                          * --- ordinate per Fornitore + Qta Scaglione
                          * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
                          Select * from ContrattiA ;
                          where fbcodart=this.w_CODARTVAR and codatini<=i_DatSys and i_DatSys<=codatfin ;
                          and nvl(coquanti,0)>=this.w_fabnet and not empty(nvl(cocodclf," ")) and (cocodclf=this.w_CODCON or empty(nvl(this.w_CODCON,"")));
                          order by cocodclf, coquanti into cursor Contprez
                          * --- Calcola Prezzo da Contratto
                          * --- Elabora Criterio per Miglior Prezzo
                          =WrCursor("Contprez")
                          Index On cocodclf+str(coquanti,12,3) Tag Codice
                          * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
                          this.w_appo1 = "######"
                          select Contprez
                          go top
                          scan
                          if cocodclf <> this.w_APPO1
                            * --- Nuovo Fornitore, Calcola Prezzo da Contratto
                            this.w_PZ = NVL(COPREZZO, 0)
                            this.w_S1 = NVL(COSCONT1, 0)
                            this.w_S2 = NVL(COSCONT2, 0)
                            this.w_S3 = NVL(COSCONT3, 0)
                            this.w_S4 = NVL(COSCONT4, 0)
                            this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
                            this.w_VAL = NVL(COCODVAL, g_PERVAL)
                            this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
                            if this.w_VAL<>g_PERVAL
                              * --- Se altra valuta, riporta alla Valuta di conto
                              if this.w_CAO<>0
                                this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
                                replace coprezzo with this.w_PZ
                              else
                                * --- Cambio incongruente, record non utilizzabile
                                this.w_PZ = -999
                              endif
                              select Contprez
                            endif
                            if this.w_PZ=-999
                              delete
                            else
                              this.w_appo1 = cocodclf
                            endif
                          else
                            * --- Elimina record
                            delete
                          endif
                          endscan
                          * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
                          * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
                          * --- Ordino per il prezzo piu' basso
                          Select * from ContPrez where not deleted() ;
                          Order By Coprezzo into cursor tempor
                          if used("ContPrez")
                            Select contPrez
                            use
                          endif
                        endif
                        if reccount()=0
                          * --- Nessun contratto valido trovato
                          if empty(this.w_CODCON)
                            this.w_CODCON = space(15)
                            this.w_CONUMERO = space(15)
                          endif
                        else
                          go top
                          this.w_CODCON = nvl(cocodclf,space(15))
                          this.w_QTAMIN = nvl(coqtamin,0)
                          this.w_LOTRIO = nvl(colotmul,0)
                          this.w_GIOAPP = nvl(cogioapp,0)
                          this.w_CONUMERO = nvl(conumero,space(15))
                        endif
                      else
                        this.w_CONUMERO = space(15)
                      endif
                      * --- Politiche di lottizzazione
                      if Empty(this.w_CONUMERO)
                        Select Elabor
                        if arflcomm="S"
                          this.w_LOTRIODEF = 0
                          this.w_QTAMINDEF = 0
                        else
                          this.w_LOTRIODEF = nvl(prlotrio,0)
                          this.w_QTAMINDEF = nvl(prqtamin,0)
                        endif
                        this.w_LOTRIO = this.w_LOTRIODEF
                        this.w_QTAMIN = this.w_QTAMINDEF
                        this.w_GIOAPP = nvl(prgioapp,0)
                      endif
                      * --- Vincolo quantit� minima
                      if this.w_Fabnet<this.w_QTAMIN
                        this.w_Fabnet = this.w_QTAMIN
                      endif
                      * --- Vincolo lotto multiplo
                      if nvl(this.w_LOTRIO,0)>0
                        this.w_Fabnet = ceiling(this.w_Fabnet/this.w_LOTRIO)*this.w_LOTRIO
                      endif
                      * --- Gestione q.t� massima
                      Select Elabor
                      this.w_QTARES = this.w_Fabnet
                      this.w_QTAMAX = prqtamax
                      this.w_Dateva = dateva
                    case dppolpro = "M" and this.w_Fabnet<prqtamin
                      * --- Vincolo quantit� minima
                      this.w_Fabnet = prqtamin
                    case dppolpro $ "L-X"
                      * --- Vincolo lotto multiplo
                      if this.w_Fabnet<prqtamin
                        this.w_Fabnet = prqtamin
                      endif
                      if nvl(prlotrio,0)>0
                        this.w_Fabnet = ceiling(this.w_Fabnet/prlotrio)*prlotrio
                      endif
                      * --- Gestione q.t� massima
                      this.w_QTARES = this.w_Fabnet
                      this.w_QTAMAX = prqtamax
                  endcase
                endif
                if this.w_QTAMAX<>0
                  do while this.w_QTARES>0
                    * --- Questo ciclo serve a spezzare gli ordini che non rispettano il vincolo di quantit� massima, se il vincolo non c'� la procedura fa sempre un solo 'giro'
                    if this.w_QTARES>this.w_QTAMAX
                      * --- Applico politica di lottizzazione quantit� massima
                      this.w_qtaava = this.w_QTAMAX
                      * --- Fabbisogno residuo (da valutare al prossimo giro)
                      this.w_QTARES = this.w_QTARES-this.w_qtaava
                      * --- Aggiunge l'ordine
                      Select Elabor 
 scatter memvar
                      this.riga = recno()
                      if this.w_PPMRPDIV="S"
                        Select Elabor_bis 
 go this.riga 
 scatter memvar 
 Select Elabor
                      endif
                      cododl=padl(alltrim(str(this.w_ODLprog)), 15)
                      padrered=iif(artipart $ "PH.DC.PS", m.padrered, m.cododl)
                      this.w_ODLprog = this.w_ODLprog + 1
                      if arpropre="L"
                        prcodfor=this.w_CODCON
                      endif
                      if arpropre="E"
                        contra=this.w_CONUMERO
                        prcodfor=this.w_CODCON
                        prcrifor=this.w_CRIFOR
                        arcencos=this.w_CENCOS
                        * --- Lead-Time Produzione e MPS
                        this.w_Leadt = this.w_GIOAPP
                        if this.w_Leadt>0 and m.datidx<>"19000101"
                          this.DatRisul = Cp_chartodate(LEFT(alltrim(dtos(this.w_dateva)),4)+"-"+SUBSTR(alltrim(dtos(this.w_dateva)),5,2)+"-"+right(alltrim(dtos(this.w_dateva)),2),"yyyy-mm-dd")
                          this.w_Datidx = dtos(dateva)
                          this.DatInput = dtos(dateva)
                          this.Page_9()
                          if i_retcode='stop' or !empty(i_Error)
                            return
                          endif
                          this.w_Datini = iif(this.w_dateva<this.DatRisul or empty(this.DatRisul), this.w_dateva, this.DatRisul)
                        else
                          this.w_Datini = this.w_dateva
                        endif
                        select Elabor
                        if this.w_Leadt>0
                          datini=this.w_Datini 
 datmps=this.w_Datini 
 leapro=this.w_Leadt
                        endif
                      endif
                       quanti=this.w_qtaava 
 quanfi=this.w_qtaava 
 qtalor=this.w_qtaava 
 quantv=-this.w_qtaava 
 quanfv=-this.w_qtaava 
 qtanet=this.oldqtanet 
 append blank 
 gather memvar
                      if this.w_PPMRPDIV="S"
                        Select Elabor_bis 
 append blank 
 gather memvar
                        Select Elabor
                      endif
                      * --- Impegna i componenti
                      this.w_QTA = this.w_qtaava
                      this.w_Datini = datini
                      this.w_Dateva = dateva
                      this.w_RifMate = cprownum
                      if arpropre<>"E"
                        this.Page_2()
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                      endif
                      if this.w_PPMRPDIV="S"
                        Select Elabor_bis
                        go this.riga
                      endif
                      Select Elabor
                      go this.riga
                    else
                      if this.w_PPMRPDIV="S"
                        this.w_recelabor = recno()
                        Select Elabor_bis
                        go this.w_recelabor
                        replace quanfi with this.w_QTARES
                        Select Elabor
                      endif
                      replace quanti with this.w_QTARES, quanfi with this.w_QTARES, qtalor with this.w_QTARES, quantv with -this.w_QTARES, quanfv with -this.w_QTARES, qtanet with this.oldqtanet, contra with this.w_CONUMERO,prcodfor with iif(arpropre$"E-L",this.w_CODCON,space(15)),prcrifor with iif(arpropre="E",this.w_CRIFOR,space(1)),arcencos with iif(arpropre="E",this.w_CENCOS,space(15))
                      this.w_Leadt = this.w_GIOAPP
                      if arpropre="E" and this.w_Leadt>0
                        replace datini with this.w_Datini, datmps with this.w_Datini, leapro with this.w_Leadt
                      endif
                      if this.w_PPMRPDIV="S"
                        Select Elabor_bis 
 set order to cododl
                        update elabor_bis set quanfi=abs(this.w_QTARES/this.w_quantiORI)*quanfi where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                        Select Elabor_bis 
 set order to codsal
                      endif
                      Select Elabor 
 set order to cododl
                      update elabor set quanti=abs(this.w_QTARES/this.w_quantiORI)*quanti, qtalor=abs(this.w_QTARES/this.w_quantiORI)*qtalor, qtanet=abs(this.w_QTARES/this.w_quantiORI)*qtanet, quanfi=abs(this.w_QTARES/this.w_quantiORI)*quanfi, quantv=abs(this.w_QTARES/this.w_quantiORI)*quantv, quanfv=abs(this.w_QTARES/this.w_quantiORI)*quanfv where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                      this.w_CodNCanc = space(15)
                      Select Elabor 
 set order to codsal
                      * --- Se sono all'ultimo giro tengo l'avanzo
                      this.w_qtaava = this.w_Fabnet-this.oldqtanet
                      this.w_QTARES = 0
                    endif
                  enddo
                else
                  if this.w_PPMRPDIV="S"
                    this.w_recelabor = recno()
                    Select Elabor_bis
                    go this.w_recelabor
                    replace quanfi with this.w_Fabnet
                    Select Elabor
                  endif
                  replace quanti with this.w_Fabnet, quanfi with this.w_Fabnet, qtalor with this.w_Fabnet, quantv with -this.w_Fabnet, quanfv with -this.w_Fabnet, qtanet with this.oldqtanet, contra with this.w_CONUMERO,prcodfor with iif(arpropre$"E-L",this.w_CODCON,space(15)),prcrifor with iif(arpropre="E",this.w_CRIFOR,space(1)),arcencos with iif(arpropre="E",this.w_CENCOS,space(15))
                  if arpropre="E"
                    * --- Lead-Time Produzione e MPS
                    this.w_Leadt = this.w_GIOAPP
                    if this.w_Leadt>0 and datidx<>"19000101"
                      this.DatRisul = Cp_chartodate(LEFT(alltrim(dtos(this.w_dateva)),4)+"-"+SUBSTR(alltrim(dtos(this.w_dateva)),5,2)+"-"+right(alltrim(dtos(this.w_dateva)),2),"yyyy-mm-dd")
                      this.w_Datidx = dtos(dateva)
                      this.DatInput = dtos(dateva)
                      this.Page_9()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      this.w_Datini = iif(this.w_dateva<this.DatRisul or empty(this.DatRisul), this.w_dateva, this.DatRisul)
                    else
                      this.w_Datini = this.w_dateva
                    endif
                    select Elabor
                    if this.w_Leadt>0
                      replace datini with this.w_Datini, datmps with this.w_Datini, leapro with this.w_Leadt
                    endif
                  endif
                  if this.w_PPMRPDIV="S"
                    update elabor_bis set quanfi=abs(this.w_Fabnet/this.w_quantiORI)*quanfi where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                  endif
                  update elabor set quanti=abs(this.w_Fabnet/this.w_quantiORI)*quanti, qtalor=abs(this.w_Fabnet/this.w_quantiORI)*qtalor, qtanet=abs(this.w_Fabnet/this.w_quantiORI)*qtanet, quanfi=abs(this.w_Fabnet/this.w_quantiORI)*quanfi, quantv=abs(this.w_Fabnet/this.w_quantiORI)*quantv, quanfv=abs(this.w_Fabnet/this.w_quantiORI)*quanfv where cododl=this.w_CodNCanc and recno()<>this.w_RecOrdSOK
                  this.w_CodNCanc = space(15)
                  select Elabor
                  this.w_qtaava = this.w_Fabnet-this.oldqtanet
                endif
              endif
            endif
          endif
          this.AggPad = .f.
          this.w_qtaava = 0
        endif
      endif
    endif
    * --- Ritorno nella posizione originaria
    if this.w_PPMRPDIV="S"
      Select Elabor_bis 
 set order to keyidx
      go this.w_RecSel
    endif
    Select Elabor 
 set order to keyidx
    go this.w_RecSel
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_dispoAppo = 0
    if nvl(arflcomm, "N")="S"
      Select Elabor 
 set order to codsal_key
      Select codric, iif(tiprec="7O", cp_CharToDatetime(RIGHT(datper, 2) + "-" + SUBSTR(datper, 5, 2) + "-" + LEFT(datper, 4) + " 00:00:00") , datcon) as dateva, datini, tiprec, iif(empty(odlmsg), cododl, odlmsg) as odlmsg, iif(empty(odamsg), space(15), odamsg) as odamsg, quanti, qtanet as disponibilita, ; 
 qtanet, qtalor, recno() as rowelab, iif(empty(nvl(prove," ")),arpropre,prove) as prove from Elabor ; 
 where codsal=this.w_CodSaldo and nvl(codcom,space(15))=comatt.codcom and nvl(codatt,space(15))=comatt.codatt ; 
 into cursor Ripiani
      Select Elabor 
 set order to keyidx 
 Select Ripiani
      this.w_DATALIMITE = reccount()
      if this.w_DATALIMITE > 2 or not empty(comatt.codcom)
        this.w_POSTI = "S" $ this.w_PMSPOSTI + this.w_PMPPOSTI + this.w_PMLPOSTI + this.w_PMOPOSTI + this.w_PMMPOSTI + this.w_PMDPOSTI + this.w_PMRPOSTI + this.w_PMCPOSTI + this.w_PMIPOSTI
        this.w_POSTI = this.w_POSTI OR "S" $ this.w_PMSANNUL + this.w_PMPANNUL + this.w_PMLCHIUD + this.w_PMOANNUL + this.w_PMMANNUL + this.w_PMDANNUL + this.w_PMRANNUL + this.w_PMCANNUL + this.w_PMIANNUL
        this.w_ANTIC = "S" $ this.w_PMSANTIC + this.w_PMPANTIC + this.w_PMOANTIC + this.w_PMMANTIC + this.w_PMDANTIC + this.w_PMRANTIC + this.w_PMCANTIC + this.w_PMIANTIC
        if not used("Errori")
          Create cursor Errori (CodiceArticolo C(41), cododl C(15), TipoOrdini C(6), Quantita N(18,6), Datini D(8), Datfin D(8), ; 
 Messaggi C(80), rowelab N(6,0))
        endif
        Select * from Ripiani order by 2, 4 into cursor Ripiani READWRITE
        Select Ripiani
        * --- Calcolo disponibilit� progressiva per articolo
        this.w_Disponibilita = 0
        scan
        if left(tiprec,2)<>"0S"
          this.w_Disponibilita = this.w_Disponibilita+iif(tiprec="4", quanti, -qtanet)
        endif
        if not Empty(this.w_DATCONG) and dateva<this.w_DATCONG
          * --- Elimina i record del periodo congelato
          delete
        else
          * --- Aggiorna la disponibilit�
          replace disponibilita with this.w_Disponibilita
        endif
        endscan
        if this.w_ANTIC
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_POSTI
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_UPDELA AND RecCount("Errori")>0
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if not this.oParentObject.w_UPDELA
          Select Ripiani 
 locate for disponibilita<0
          if found()
            this.w_MRSERIAL = this.w_MRSERIAL+1
            * --- Insert into MRP_MESS
            i_nConn=i_TableProp[this.MRP_MESS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
              +","+cp_NullLink(cp_ToStrODBC(space(15)),'MRP_MESS','MRCODODL');
              +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
              +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRQTAODL');
              +","+cp_NullLink(cp_ToStrODBC("MR"),'MRP_MESS','MRTIPOMS');
              +","+cp_NullLink(cp_ToStrODBC("Esistono fabbisogni non coperti"),'MRP_MESS','MRDESCRI');
              +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAIN');
              +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAFI');
              +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
              +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
              +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',space(15),'MRCODRIC',Ripiani.codric,'MRQTAODL',0,'MRTIPOMS',"MR",'MRDESCRI',"Esistono fabbisogni non coperti",'MRDATAIN',{},'MRDATAFI',{},'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
              insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                 values (;
                   padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                   ,i_DATSYS;
                   ,space(15);
                   ,Ripiani.codric;
                   ,0;
                   ,"MR";
                   ,"Esistono fabbisogni non coperti";
                   ,{};
                   ,{};
                   ,"N";
                   ,{};
                   ,0;
                   ,this.w_MRINTEXT;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
    else
      Select Elabor 
 set order to codsal_key
      Select codric, iif(tiprec="7O", cp_CharToDatetime(RIGHT(datper, 2) + "-" + SUBSTR(datper, 5, 2) + "-" + LEFT(datper, 4) + " 00:00:00") , datcon) as dateva, datini, tiprec, ; 
 iif(empty(odlmsg), cododl, odlmsg) as odlmsg, iif(empty(odamsg), space(15), odamsg) as odamsg, quanti, qtanet as disponibilita, ; 
 qtanet, qtalor, recno() as rowelab, iif(empty(nvl(prove," ")),arpropre,prove) as prove from Elabor ; 
 where codsal=this.w_CodSaldo into cursor Ripiani
      Select Elabor 
 set order to keyidx 
 Select Ripiani
      this.w_DATALIMITE = reccount()
      if this.w_DATALIMITE > 2
        this.w_POSTI = "S" $ this.w_PMSPOSTI + this.w_PMPPOSTI + this.w_PMLPOSTI + this.w_PMOPOSTI + this.w_PMMPOSTI + this.w_PMDPOSTI + this.w_PMRPOSTI + this.w_PMCPOSTI + this.w_PMIPOSTI
        this.w_POSTI = this.w_POSTI OR "S" $ this.w_PMSANNUL + this.w_PMPANNUL + this.w_PMLCHIUD + this.w_PMOANNUL + this.w_PMMANNUL + this.w_PMDANNUL + this.w_PMRANNUL + this.w_PMCANNUL + this.w_PMIANNUL
        this.w_ANTIC = "S" $ this.w_PMSANTIC + this.w_PMPANTIC + this.w_PMOANTIC + this.w_PMMANTIC + this.w_PMDANTIC + this.w_PMRANTIC + this.w_PMCANTIC + this.w_PMIANTIC
        if not used("Errori")
          Create cursor Errori (CodiceArticolo C(41), cododl C(15), TipoOrdini C(6), Quantita N(18,6), Datini D(8), Datfin D(8), Messaggi C(80), rowelab N(6,0))
        endif
        * --- --Inseriti indici su ripiani
        Select * from Ripiani order by 2,4 into cursor Ripiani READWRITE
        Select Ripiani
        * --- Calcolo disponibilit� progressiva per articolo
        this.w_Disponibilita = 0
        scan
        if left(tiprec,2)<>"0S"
          this.w_Disponibilita = this.w_Disponibilita+iif(tiprec="4", quanti, -qtanet)
        endif
        if not Empty(this.w_DATCONG) and dateva<this.w_DATCONG
          * --- Elimina i record del periodo congelato
          delete
        else
          * --- Aggiorna la disponibilit�
          replace disponibilita with this.w_Disponibilita
        endif
        endscan
        if this.w_ANTIC
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_POSTI
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_UPDELA AND RecCount("Errori")>0
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if not this.oParentObject.w_UPDELA
          Select Ripiani 
 locate for disponibilita<0
          if found()
            this.w_MRSERIAL = this.w_MRSERIAL+1
            * --- Insert into MRP_MESS
            i_nConn=i_TableProp[this.MRP_MESS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MRP_MESS_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MRSERIAL"+",MR__DATA"+",MRCODODL"+",MRCODRIC"+",MRQTAODL"+",MRTIPOMS"+",MRDESCRI"+",MRDATAIN"+",MRDATAFI"+",MRESEGUI"+",MRDATAEX"+",MRUTEXEC"+",MRINTEXT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(padl(alltrim(str(this.w_MRSERIAL)),10,"0")),'MRP_MESS','MRSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MR__DATA');
              +","+cp_NullLink(cp_ToStrODBC(space(15)),'MRP_MESS','MRCODODL');
              +","+cp_NullLink(cp_ToStrODBC(Ripiani.codric),'MRP_MESS','MRCODRIC');
              +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRQTAODL');
              +","+cp_NullLink(cp_ToStrODBC("MR"),'MRP_MESS','MRTIPOMS');
              +","+cp_NullLink(cp_ToStrODBC("Esistono fabbisogni non coperti"),'MRP_MESS','MRDESCRI');
              +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAIN');
              +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAFI');
              +","+cp_NullLink(cp_ToStrODBC("N"),'MRP_MESS','MRESEGUI');
              +","+cp_NullLink(cp_ToStrODBC({}),'MRP_MESS','MRDATAEX');
              +","+cp_NullLink(cp_ToStrODBC(0),'MRP_MESS','MRUTEXEC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRINTEXT),'MRP_MESS','MRINTEXT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',padl(alltrim(str(this.w_MRSERIAL)),10,"0"),'MR__DATA',i_DATSYS,'MRCODODL',space(15),'MRCODRIC',Ripiani.codric,'MRQTAODL',0,'MRTIPOMS',"MR",'MRDESCRI',"Esistono fabbisogni non coperti",'MRDATAIN',{},'MRDATAFI',{},'MRESEGUI',"N",'MRDATAEX',{},'MRUTEXEC',0)
              insert into (i_cTable) (MRSERIAL,MR__DATA,MRCODODL,MRCODRIC,MRQTAODL,MRTIPOMS,MRDESCRI,MRDATAIN,MRDATAFI,MRESEGUI,MRDATAEX,MRUTEXEC,MRINTEXT &i_ccchkf. );
                 values (;
                   padl(alltrim(str(this.w_MRSERIAL)),10,"0");
                   ,i_DATSYS;
                   ,space(15);
                   ,Ripiani.codric;
                   ,0;
                   ,"MR";
                   ,"Esistono fabbisogni non coperti";
                   ,{};
                   ,{};
                   ,"N";
                   ,{};
                   ,0;
                   ,this.w_MRINTEXT;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    USE IN SELECT("RIPIANI")
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea file di log
    if this.w_oMRPLOG.ISFULLLOG()
      this.w_LogFileName = LEFT(this.w_FileName, LEN(this.w_filename)-4)+".LOG"
      this.w_CursErrorLog = this.w_oMRPLOG.cCursMessErr
      l_ErrCurs = this.w_oMRPLOG.cCursMessErr
      SELECT(l_ErrCurs) 
 GO TOP
      this.hf_FILE_LOG = fcreate(this.w_LogFileName)
      if this.hf_FILE_LOG>=0
        do while not eof()
          this.w_cMsg = &l_ErrCurs..Msg
          * --- Scrive Log
          this.w_TEMP = fPuts(this.hf_FILE_LOG, this.w_cMsg)
          SELECT (l_ErrCurs)
          skip +1
        enddo
        =fCLOSE(this.hf_FILE_LOG)
      endif
    endif
  endproc


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    USE IN SELECT("Articoli")
    USE IN SELECT("Art_prod")
    USE IN SELECT("Par_rima")
    USE IN SELECT("KeyPar_rima")
    USE IN SELECT("Saldi")
    USE IN SELECT("Impegni")
    USE IN SELECT("Documen")
    USE IN SELECT("Datiniz")
    USE IN SELECT("Legami")
    USE IN SELECT("Elabor")
    USE IN SELECT("Calend")
    USE IN SELECT("ODP")
    USE IN SELECT("SalComm")
    USE IN SELECT("inputfasext")
    USE IN SELECT("ODL")
    USE IN SELECT("OrdiODL")
    USE IN SELECT("ImpeODL")
    USE IN SELECT("ImpeODL")
    USE IN SELECT("COrdiODL")
    USE IN SELECT("Saldi")
    USE IN SELECT("SaldiCom")
    USE IN SELECT("odl2del")
    USE IN SELECT("Ordi")
    USE IN SELECT("Commesse")
    USE IN SELECT("Cicli")
    USE IN SELECT("TmpCic")
    USE IN SELECT("TmpCic2")
    USE IN SELECT("Contratti")
    USE IN SELECT("ContrattiA")
    USE IN SELECT("Materzis")
    USE IN SELECT("CodFor")
    USE IN SELECT("Errori")
    USE IN SELECT("SalPCom")
    USE IN SELECT("Componen")
    USE IN SELECT("Cimpeodl")
    USE IN SELECT("FASEXTODL")
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Filtri elaborazione
    * --- Pulisco la temporanea
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_KEYRIFDOC = SYS(2015)
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIFDOC);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIFDOC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_OLDCOM
      this.w_SELEIMPE = "A"
    endif
    * --- Creo tabella temporanea di appoggio che contiene le chiavi dei saldi che arrivano dai documenti oppure dagli ODL
    this.w_SELEZIDO = "N"
    if this.w_SELEZDOC="S" OR this.w_SELEZODL="S"
      this.w_SELEZIDO = "S"
    endif
    * --- Create temporary table TMPART_TEMP
    i_nIdx=cp_AddTableDef('TMPART_TEMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ART_TEMP_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"CAKEYSAL AS CODSAL "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPART_TEMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if this.w_SELEZDOC="S"
      * --- Insert into TMPART_TEMP
      i_nConn=i_TableProp[this.TMPART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR17BGP",this.TMPART_TEMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_SELEZODL="S"
      * --- Insert into TMPART_TEMP
      i_nConn=i_TableProp[this.TMPART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR17BGPB",this.TMPART_TEMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_SELEZART="S" AND this.w_SELEZDOC<>"S" && AND this.w_SELEZODL<>"S"
      * --- Faccio la query direttamente, in questo caso posso farlo e cos� � pi� veloce
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR14BGP",this.ART_TEMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      this.w_SUGG = "S"
      if this.w_SELEIMPE $ "A-E" 
        * --- Devo costruire il cursore da inserire in ART_TEMP passo per passo
        * --- Cursore articoli
        VQ_EXEC("..\COLA\EXE\QUERY\GSMR14BGP", this, "ArtFil")
        if this.oParentObject.w_CAUSALI<>0
          * --- Cursore documenti
          VQ_EXEC("..\COLA\EXE\QUERY\GSMR17BGP1", this, "DocFil")
          * --- Applico filtro causali
          select CODSAL from (this.NC) ; 
 inner join DocFil on MVTIPDOC=TDTIPDOC order by CODSAL into cursor DocFil where xchk<>0
          * --- Cursore finale - insert in ART_TEMP
          Select distinct * from ArtFil where CAKEYSAL in (select codsal from DocFil) into cursor ArtTemp
          USE IN SELECT("DocFil")
        else
          Select distinct * from ArtFil into cursor ArtTemp
        endif
        USE IN SELECT("ArtFil")
        * --- Inserisce nella tabella ART_TEMP gli articoli selezionati
        select ArtTemp 
 scan
        * --- Try
        local bErr_06086190
        bErr_06086190=bTrsErr
        this.Try_06086190()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_06086190
        * --- End
        endscan
        USE IN SELECT("ArtTemp")
      else
        * --- Faccio la query direttamente, in questo caso posso farlo e cos� � pi� veloce
        * --- Insert into ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR14BGP1",this.ART_TEMP_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Drop temporary table TMPART_TEMP
    i_nIdx=cp_GetTableDefIdx('TMPART_TEMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPART_TEMP')
    endif
    if this.w_ELACAT="S" and (this.w_SELEZART="S" Or this.w_SELEZDOC="S") and this.w_GENODA<>"S"
      do GSMR_BED with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_VERIFICA="ZZZ"
        * --- Errore in fase di esplosione
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Alzo la bandierina e notifico che c'� un errore (il messaggio per comodit� lo rilascio qui))
        this.w_CHECKINIT = 999
        i_retcode = 'stop'
        return
      endif
    endif
    VQ_EXEC("..\COLA\EXE\QUERY\GSMR15BGP", this, "Catena")
    Select * from Catena into cursor Catena READWRITE 
 index on cakeysal tag cakeysal
    do case
      case this.w_SELEIMPE="A"
        * --- Nel caso di selezione impegni (Articolo)
        *     La variabile w_SELEZART deve valere 'S' per filtrare gli articoli
        *     La Variabile w_SELEZDOC deve valere 'N' non deve filtrare gli impegni
        this.w_SELEZDOC = "N"
        this.w_SELEZART = "S"
      case this.w_SELEIMPE="I"
        * --- Nel caso di Slezione impegni (Articolo)
        *     La variabile w_SELEZART deve avere il valore impostato in maschera
        *     La Variabile w_SELEZDOC deve valere 'S' deve filtrare gli impegni
        this.w_SELEZDOC = "S"
      otherwise
        * --- va bene tutto cos�
    endcase
  endproc
  proc Try_06086190()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(ArtTemp.Cakeyrif),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(ArtTemp.Cacodric),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(ArtTemp.Cakeysal),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(1),'ART_TEMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',ArtTemp.Cakeyrif,'CACODRIC',ArtTemp.Cacodric,'CAKEYSAL',ArtTemp.Cakeysal,'CPROWNUM',1,'CACODDIS',SPACE(20))
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CPROWNUM,CACODDIS &i_ccchkf. );
         values (;
           ArtTemp.Cakeyrif;
           ,ArtTemp.Cacodric;
           ,ArtTemp.Cakeysal;
           ,1;
           ,SPACE(20);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,33)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='MA_COSTI'
    this.cWorkTables[3]='MPS_TFOR'
    this.cWorkTables[4]='ODL_CICL'
    this.cWorkTables[5]='ODL_DETT'
    this.cWorkTables[6]='ODL_MAST'
    this.cWorkTables[7]='ODL_RISO'
    this.cWorkTables[8]='PAR_PROD'
    this.cWorkTables[9]='PRD_ERRO'
    this.cWorkTables[10]='SALDIART'
    this.cWorkTables[11]='SCCI_ASF'
    this.cWorkTables[12]='PARA_MRP'
    this.cWorkTables[13]='MRP_MESS'
    this.cWorkTables[14]='ART_TEMP'
    this.cWorkTables[15]='SEL__MRP'
    this.cWorkTables[16]='SALDICOM'
    this.cWorkTables[17]='PEG_SELI'
    this.cWorkTables[18]='MAGAZZIN'
    this.cWorkTables[19]='ODL_SMPL'
    this.cWorkTables[20]='GRUDMAG'
    this.cWorkTables[21]='PAR_RIOR'
    this.cWorkTables[22]='*TMPSALAGG'
    this.cWorkTables[23]='ART_ICOL'
    this.cWorkTables[24]='MAGA_TEMP'
    this.cWorkTables[25]='*TMPDOCUM'
    this.cWorkTables[26]='ODLMRISO'
    this.cWorkTables[27]='ODL_MOUT'
    this.cWorkTables[28]='ODL_MAIN'
    this.cWorkTables[29]='*TMPGSDB_MRL'
    this.cWorkTables[30]='ODL_RISF'
    this.cWorkTables[31]='*TMPGSCO_MMO'
    this.cWorkTables[32]='odl_mast'
    this.cWorkTables[33]='*TMPART_TEMP'
    return(this.OpenAllTables(33))

  proc CloseCursors()
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsmr_bgp
  Function TimeMRP(w_second)
  local timemessage
  if w_second<60
    if int(w_second)=w_second
      timemessage=alltrim(str(w_second,10))+space(1)+ah_msgformat('secondi')
    else
      timemessage=alltrim(str(w_second,10,3))+space(1)+ah_msgformat('secondi')
    endif
  else
    if w_second/60<60
      if int(w_second/60)=1
        timemessage=alltrim(str(int(w_second/60),10))+space(1)+ah_msgformat('minuto')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')    
      else
        timemessage=alltrim(str(int(w_second/60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      endif
    else
      if int(w_second/60/60)=1
        timemessage=alltrim(str(int(w_second/60/60),10))+space(1)+ah_msgformat('ora')+space(1)+alltrim(str(mod(int(w_second/60),60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      else
        timemessage=alltrim(str(int(w_second/60/60),10))+space(1)+ah_msgformat('ore')+space(1)+alltrim(str(mod(int(w_second/60),60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      endif
    endif
  endif
    return timemessage
  EndFunc
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
