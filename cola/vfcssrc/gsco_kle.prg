* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kle                                                        *
*              Visualizza errori generazione fabbisogni                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_54]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-24                                                      *
* Last revis.: 2008-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kle",oParentObject))

* --- Class definition
define class tgsco_kle as StdForm
  Top    = 9
  Left   = 7

  * --- Standard Properties
  Width  = 770
  Height = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-18"
  HelpContextID=216638825
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  cPrg = "gsco_kle"
  cComment = "Visualizza errori generazione fabbisogni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_LOperaz = space(2)
  w_LSELOPE = space(1)
  w_ERRTEC = space(0)
  w_lDATINI = ctod('  /  /  ')
  w_lDATFIN = ctod('  /  /  ')
  w_LSerial = space(10)
  w_DettTec = space(1)
  w_LDettTec = space(1)
  w_ARCODART = space(10)
  w_ZoomLOG = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_klePag1","gsco_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Lista errori")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.olDATINI_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomLOG = this.oPgFrm.Pages(1).oPag.ZoomLOG
    DoDefault()
    proc Destroy()
      this.w_ZoomLOG = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAR_PROD'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCO_BLE with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LOperaz=space(2)
      .w_LSELOPE=space(1)
      .w_ERRTEC=space(0)
      .w_lDATINI=ctod("  /  /  ")
      .w_lDATFIN=ctod("  /  /  ")
      .w_LSerial=space(10)
      .w_DettTec=space(1)
      .w_LDettTec=space(1)
      .w_ARCODART=space(10)
        .w_LOperaz = 'MR'
        .w_LSELOPE = "T"
      .oPgFrm.Page1.oPag.ZoomLOG.Calculate()
        .w_ERRTEC = iif(.w_LDettTec="S",Nvl(.w_ZoomLOG.GetVar("LEMESSAG"),Space(10)),"Dettaglio Tecnico non disponibile")
          .DoRTCalc(4,6,.f.)
        .w_DettTec = ' '
        .w_LDettTec = "N"
        .w_ARCODART = .w_ZoomLOG.GETVAR('LECODICE')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomLOG.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_ERRTEC = iif(.w_LDettTec="S",Nvl(.w_ZoomLOG.GetVar("LEMESSAG"),Space(10)),"Dettaglio Tecnico non disponibile")
        .DoRTCalc(4,7,.t.)
            .w_LDettTec = "N"
            .w_ARCODART = .w_ZoomLOG.GETVAR('LECODICE')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomLOG.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomLOG.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oERRTEC_1_5.value==this.w_ERRTEC)
      this.oPgFrm.Page1.oPag.oERRTEC_1_5.value=this.w_ERRTEC
    endif
    if not(this.oPgFrm.Page1.oPag.olDATINI_1_7.value==this.w_lDATINI)
      this.oPgFrm.Page1.oPag.olDATINI_1_7.value=this.w_lDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.olDATFIN_1_8.value==this.w_lDATFIN)
      this.oPgFrm.Page1.oPag.olDATFIN_1_8.value=this.w_lDATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_lDATINI<=.w_lDATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.olDATFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco_klePag1 as StdContainer
  Width  = 766
  height = 424
  stdWidth  = 766
  stdheight = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomLOG as cp_zoombox with uid="NOMGJKDEGU",left=0, top=56, width=764,height=305,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PRD_ERRO",cZoomFile="GSCO_KLE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZC",bRetriveAllRows=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 229794278


  add object oBtn_1_4 as StdButton with uid="GWAUTGCEFV",left=713, top=7, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 212014077;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oERRTEC_1_5 as StdMemo with uid="SINLTFLYSM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ERRTEC", cQueryName = "ERRTEC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 180343110,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=48, Width=532, Left=127, Top=368

  add object olDATINI_1_7 as StdField with uid="RGQJUDRGMY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_lDATINI", cQueryName = "lDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 167856714,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=102, Top=7

  add object olDATFIN_1_8 as StdField with uid="PCNZOPXKPL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_lDATFIN", cQueryName = "lDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 254888522,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=295, Top=7

  func olDATFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_lDATINI<=.w_lDATFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="XAOFRYAIID",left=663, top=367, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare il log degli errori";
    , HelpContextID = 193586214;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        do GSCO_BLE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="KYQESPJULE",left=713, top=367, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 209321402;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="PAHLFTAQUW",Visible=.t., Left=3, Top=370,;
    Alignment=1, Width=122, Height=18,;
    Caption="Descrizione errore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="BIRKHXHVYP",Visible=.t., Left=6, Top=7,;
    Alignment=1, Width=93, Height=15,;
    Caption="Data iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="KXELLGYXKX",Visible=.t., Left=189, Top=7,;
    Alignment=1, Width=102, Height=15,;
    Caption="Data finale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
