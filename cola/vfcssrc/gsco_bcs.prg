* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bcs                                                        *
*              Cambio stato ODL/OCL                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_701]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-07                                                      *
* Last revis.: 2018-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bcs",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_bcs as StdBatch
  * --- Local variables
  pAzione = space(2)
  w_LNumErr = 0
  w_nRecSel = 0
  w_nRecEla = 0
  NC = space(10)
  TmpC = space(100)
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_CODODL = space(15)
  w_CODICE = space(20)
  w_CODART = space(20)
  w_COFOR = space(15)
  w_PROVEN = space(1)
  w_RECFAS = 0
  w_FASCOL = space(15)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  ODL_MAST_idx=0
  ODL_DETT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Pianificazione ODL/OCL Suggeriti (da GSCO_KCS)
    * --- Log Errori
    * --- Variabili Cursore
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_LNumErr = 0
    * --- Nome cursore collegato allo zoom
    if nvl(this.oParentObject.w_SELEZI,"")="Z"
      * --- Solo se si arriva dal Pegging di secondo Livello (GSMR_BPE)
      this.NC = "GSCOBCS"
    else
      this.NC = this.oParentObject.w_ZoomSel.cCursor
    endif
    do case
      case this.pAzione="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione="INTERROGA"
        * --- Visualizza Zoom Elenco ODL/OCL periodo
        this.oParentObject.NotifyEvent("Interroga")
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
      case this.pAzione="AG"
        * --- Aggiornamento Status
        * --- Controlla selezioni
        SELECT (this.NC)
        GO TOP
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel>0
          * --- Crea il File delle Messaggistiche di Errore
          CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
          * --- Legge cursore di selezione ...
          this.w_nRecSel = 0
          this.w_nRecEla = 0
          * --- Cicla sui codici selezionati ...
          SELECT (this.NC)
          GO TOP
          SCAN FOR xChk=1 AND NOT EMPTY(NVL(OLCODODL,"")) 
          * --- Legge dati di interesse ....
          this.w_CODODL = OLCODODL
          this.w_CODICE = OLTCODIC
          this.w_CODART = NVL(OLTCOART, SPACE(15))
          this.w_COFOR = NVL(OLTCOFOR, SPACE(15))
          this.w_PROVEN = NVL(OLTPROVE, " ")
          * --- Azione ...
          this.w_nRecSel = this.w_nRecSel + 1
          do case
            case this.w_PROVEN="L" AND EMPTY(this.w_COFOR)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice fornitore C/Lavoro non definito")
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_CODART)
              this.w_LOggErr = this.w_CODODL
              if this.w_PROVEN="L"
                this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              else
                this.w_LErrore = ah_Msgformat("Elaborazione codice ODL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              endif
              this.w_LTesMes = ah_Msgformat("Codice articolo non definito")
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            otherwise
              vq_exec("..\COLA\EXE\QUERY\GSCO2BCS.VQR",this,"FASCOL")
              this.w_RECFAS = reccount("FASCOL")
              if this.w_RECFAS>0
                * --- L'ODL che sto analizzando ha delle fasi
                Select FASCOL 
 GO TOP 
 SCAN
                * --- Legge dati di interesse ....
                this.w_FASCOL = FASCOL.OLCODODL
                this.w_CODICE = FASCOL.OLTCODIC
                this.w_CODART = NVL(FASCOL.OLTCOART, SPACE(15))
                this.w_COFOR = NVL(FASCOL.OLTCOFOR, SPACE(15))
                this.w_PROVEN = NVL(FASCOL.OLTPROVE, " ")
                ENDSCAN
                do case
                  case this.w_PROVEN="L" AND EMPTY(this.w_COFOR)
                    this.w_LOggErr = this.w_FASCOL
                    this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
                    this.w_LTesMes = ah_Msgformat("Codice fornitore C/Lavoro non definito")
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  case EMPTY(this.w_CODART)
                    this.w_LOggErr = this.w_FASCOL
                    if this.w_PROVEN="L"
                      this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
                    else
                      this.w_LErrore = ah_Msgformat("Elaborazione codice ODL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
                    endif
                    this.w_LTesMes = ah_Msgformat("Codice articolo non definito")
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  otherwise
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             )
                    else
                      update (i_cTable) set;
                          OLTSTATO = "P";
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_CODODL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore scrittura ODL_MAST (1)'
                      return
                    endif
                    this.w_nRecEla = this.w_nRecEla + 1
                    Select FASCOL 
 GO TOP 
 SCAN
                    this.w_FASCOL = FASCOL.OLCODODL
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_FASCOL);
                             )
                    else
                      update (i_cTable) set;
                          OLTSTATO = "P";
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_FASCOL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore scrittura ODL_MAST (1)'
                      return
                    endif
                    this.w_nRecEla = this.w_nRecEla + 1
                    ENDSCAN
                    USE IN Select ("FASCOL")
                endcase
              else
                * --- Write into ODL_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                         )
                else
                  update (i_cTable) set;
                      OLTSTATO = "P";
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore scrittura ODL_MAST (1)'
                  return
                endif
                this.w_nRecEla = this.w_nRecEla + 1
              endif
          endcase
          SELECT (this.NC)
          ENDSCAN
          this.w_oPart = this.w_oMess.AddMsgPartNL("Elaborazione terminata%0N.%1 records elaborati%0su %2 records selezionati")
          this.w_oPart.AddParam(alltrim(str(this.w_nRecEla,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecSel,5,0)))     
          if USED("MessErr") AND this.w_LNumErr>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?")
            this.w_oPart.AddParam(alltrim(str(this.w_LNumErr,5,0)))     
            if this.w_oMess.ah_YesNo()
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
            endif
          else
            this.w_oMess.ah_ErrorMsg()
          endif
          * --- Chiusura Cursori
          if USED("MessErr")
            SELECT MessErr
            USE
          endif
          if USED("__TMP__")
            SELECT __TMP__
            USE
          endif
          this.oParentObject.NotifyEvent("Interroga")
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare","!","")
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    * --- Scrive LOG
    if EMPTY(this.w_LTesMes)
      this.w_LTesMes = "Message()= "+message()
    endif
    INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
    (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LTesMes)
    this.w_LTesMes = ""
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='ODL_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
