* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bet                                                        *
*              Eventi scelta fornitore                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_384]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-18                                                      *
* Last revis.: 2015-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bet",oParentObject,m.pOper)
return(i_retval)

define class tgsco_bet as StdBatch
  * --- Local variables
  pOper = space(1)
  w_OPER = space(1)
  Padre = .NULL.
  cCursor = space(10)
  Griglia = .NULL.
  QTARTOT = 0
  QTAREVA = 0
  w_ZOOM = space(10)
  w_ZOOM1 = space(10)
  GestODP = .NULL.
  TMPc = space(10)
  w_OREC = 0
  w_CONTINUA = .f.
  w_ERRORE = .f.
  w_OLTCODIC = space(20)
  w_LEADPAR = 0
  w_oQTAMIN = 0
  w_oQTALOT = 0
  w_OLUNIMIS = space(3)
  w_QUAIN = 0
  w_QUAIN1 = 0
  w_mARTICOLO = space(20)
  w_UM3 = space(3)
  w_OP3 = space(1)
  w_MO3 = 0
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_OP1 = space(1)
  w_MO1 = 0
  w_FOR_IN = space(15)
  w_FOROUT = space(15)
  w_QUAOUT = 0
  w_QUAOUT1 = 0
  w_COGIOAPP = 0
  w_TmpC = space(100)
  iCODFORN = space(15)
  iLOTMUL = 0
  iQTAMIN = 0
  iGGAPPR = 0
  iTESTO = space(100)
  iFORABBIN = space(15)
  w_OLCODODL = space(15)
  w_OLTCOFOR = space(15)
  w_OLTCONTR = space(15)
  iCODFORPA = space(15)
  iQTAMINAB = 0
  iLOTMULAB = 0
  w_TIPGES = space(1)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_CONTINUA = .f.
  w_oPart2 = .NULL.
  w_oMess1 = .NULL.
  w_oPart1 = .NULL.
  * --- WorkFile variables
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione eventi da Scelta terzista (da GSCO_KST)
    * --- INDICE OPERAZIONI ESEGUIBILI
    * --- A = Riempie zoom FORNITORI ABBINABILI (pag.1)
    * --- B = operazioni legate alla procedura di abbinamento
    * --- P = operazioni legate alla procedura di separazione
    * --- D = Attiva zoom pagina 3 (dettaglio ordini)
    * --- 2 = Passa a pagina 2 e rimpie gli zoom con i dati per l'abbinamento
    * --- S = Seleziona tutto il contenuto dello zoom 1 a pagina 2
    * --- E =Deseleziona tutto il contenuto dello zoom 1 a pagina 2
    * --- T = Selezione fornitori per dettaglio ordini
    * --- U = Deselezione fornitori per dettaglio ordini
    * --- M = modifica ordine selezionato
    * --- V = visualizza ordine selezionato
    * --- C = cancella ordine selezionato
    * --- Visibilit� dal padre
    this.Padre = this.oParentObject
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_OPER = this.pOper
    this.w_TIPGES = this.Padre.w_TIPGES
    do case
      case this.w_OPER="D"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<< Attiva zoom pagina 3 (dettagli ordini)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER="A"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<< Riempie zoom FORNITORI ABBINABILI (pag.1)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER="2"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<< ATTIVA PAGINA 2 E CALCOLA I DATI DEGLI ZOOM
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER="S"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Seleziona tutti gli ordini
        this.cCursor = this.Padre.w_POCLz5.cCursor
        Select (this.cCursor)
        replace all xchk with 1
      case this.w_OPER="E"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Deseleziona tutti gli ordini
        this.cCursor = this.Padre.w_POCLz5.cCursor
        Select (this.cCursor)
        replace all xchk with 0
      case this.w_OPER="B"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER="P"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER $ "T1-T2-U1-U2"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<
        if this.w_OPER $ "T1-U1"
          * --- Terzisti Pag.1
          this.w_ZOOM = this.oParentObject.w_TERZIS
        else
          * --- Terzisti Pag.2 (Abbina/Separa)
          this.w_ZOOM = this.oParentObject.w_POCLz6
        endif
        NC = this.w_Zoom.cCursor
        SELECT (NC)
        this.w_OREC = RECNO()
        this.oParentObject.w_SELEFORN = IIF(this.w_OPER $ "T2-U2", this.oParentObject.w_SELEFORN, "")
        GO TOP
        SCAN
        if RECNO()=this.w_OREC AND this.w_OPER $ "T1-T2"
          this.oParentObject.w_SELEFORN = IIF(this.w_OPER="T2", this.oParentObject.w_SELEFORN, NVL(COCODCLF, SPACE(15)))
        else
          REPLACE XCHK WITH 0
        endif
        ENDSCAN
        GOTO this.w_OREC
      case this.w_OPER $ "MCV"
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- M = modifica ordine selezionato
        * --- C = cancella ordine selezionato
        * --- V = visualizza ordine selezionato
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Abbinamenti (pag.2 maschera)
    * --- Zoom elenco ordini abbinati/da abbinare
    this.w_ZOOM = this.oParentObject.w_POCLz5
    NC = this.w_Zoom.cCursor
    SELECT * FROM (NC) WHERE XCHK = 1 INTO CURSOR __xch__
    * --- __xck__ == elenco ordini
    * --- Zoom elenco fornitori abbinabili
    this.w_ZOOM1 = this.oParentObject.w_POCLz6
    NC1 = this.w_Zoom1.cCursor
    SELECT * FROM (NC1) WHERE XCHK = 1 INTO CURSOR __xc1__
    * --- __xc1__ == elenco terzista
    go top
    * --- Controlli ...
    do case
      case reccount("__xch__")>=1 AND reccount("__xc1__")=0
        ah_ErrorMsg("Selezionare un terzista","!","")
      case reccount("__xch__")=0
        ah_ErrorMsg("Selezionare uno o pi� ordini","!","")
      case reccount("__xch__")>=1 AND reccount("__xc1__")>1
        ah_ErrorMsg("Selezionare solo un terzista","!","")
      case reccount("__xch__")>=1 AND reccount("__xc1__")=1
        this.w_CONTINUA = .T.
        if this.oParentObject.w_nABBINA=0
          this.w_oPart2 = this.w_oMess.AddMsgPartNL("Abbinamento %1 a %2: la procedura pu� essere lanciata in modalit� simulata; in questo caso vengono aggiornati solamente gli zoom della pagina 2")
          this.w_oPart2.AddParam(IIF(this.w_TIPGES="E", "ODA", "OCL" ))     
          this.w_oPart2.AddParam(IIF(this.w_TIPGES= "E", "fornitore", "terzista" ))     
          this.w_oMess.AddMsgPartNL("In modalit� definitiva, vengono aggiornati i dati del/gli ordini selezionati compresi i dettagli relativi ai materiali%0")     
          if this.oParentObject.w_FLGSIMUL="S"
            this.w_oMess.AddMsgPartNL("Modalit� selezionata: SIMULATA. Confermi richiesta abbinamento?")     
          else
            this.w_oMess.AddMsgPartNL("Modalit� selezionata: DEFINITIVA. Confermi richiesta abbinamento?")     
          endif
          this.w_CONTINUA = this.w_oMess.ah_YesNo()
          this.oParentObject.w_nABBINA = iif(this.w_CONTINUA,1,0)
        endif
        if this.w_CONTINUA
          do case
            case this.oParentObject.w_FLGSIMUL="S"
              * --- Modalit� simulata - aggiorna solo cursore
              this.iFORABBIN = NVL(__xc1__.COCODCLF, SPACE(15))
              UPDATE (NC) SET OLTCOFOR=this.iFORABBIN WHERE XCHK = 1
              * --- Aggiorna griglia
              this.Griglia = this.Padre.w_POCLz5.GRD
              this.Griglia.Refresh()     
              * --- Calcola qta non abbinata
              Select SUM(OLTQTODL) AS TotNull from (NC) Where EMPTY(NVL(OLTCOFOR,"")) GROUP BY ST into cursor __tot__
              if reccount("__tot__")>0
                this.oParentObject.w_QTADAABB = NVL(__tot__.TotNull, 0)
              else
                this.oParentObject.w_QTADAABB = 0
              endif
            case this.oParentObject.w_FLGSIMUL="D"
              * --- Modalit� definitiva
              select __xch__
              scan
              * --- Ordini da aggiornare
              this.w_OLCODODL = __xch__.OLCODODL
              * --- Legge dati fornitore selezionato
              this.w_OLTCOFOR = NVL(__xc1__.COCODCLF, SPACE(15))
              this.w_OLTCONTR = NVL( __xc1__.CONUMERO, SPACE(15))
              if alltrim(this.w_OLTCONTR)=ah_Msgformat("Dati anagrafici")
                this.w_OLTCONTR = ""
              endif
              * --- Esegue abbinamento
              ah_Msg("Aggiornamento %1: %2", .T.,.F.,.F., IIF(this.w_TIPGES="E", "ODA", "OCL") , this.w_OLCODODL)
              do GSCO_BAO with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              select __xch__
              endscan
              * --- Riesegue query
              this.oParentObject.NotifyEvent("InterDettAbbina")
          endcase
        else
          ah_ErrorMsg("Nessuna modifica apportata","!","")
        endif
    endcase
    * --- Chiude cursori
    if used("__xch__")
      use in __xch__
    endif
    if used("__xc1__")
      use in __xc1__
    endif
    if used("__tot__")
      use in __tot__
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SeparaPOCLTerzista
    * --- Variabili private di supporto
    private NC,NC1
    * --- Zoom elenco OCL abbinati/da abbinare
    this.w_ZOOM = this.oParentObject.w_POCLz5
    NC = this.w_Zoom.cCursor
    SELECT * FROM (NC) WHERE XCHK = 1 INTO CURSOR __xch__
    * --- __xck__ == elenco OCL
    * --- Zoom elenco fornitori abbinabili
    this.w_ZOOM1 = this.oParentObject.w_POCLz6
    NC1 = this.w_Zoom1.cCursor
    SELECT * FROM (NC1) WHERE XCHK = 1 INTO CURSOR __xc1__
    * --- __xc1__ == elenco terzista
    * --- Controlli ...
    do case
      case reccount("__xch__")=1 AND reccount("__xc1__")=0
        ah_ErrorMsg("Selezionare un terzista","!","")
      case reccount("__xch__")=0
        ah_ErrorMsg("Selezionare un OCL","!","")
      case reccount("__xch__")>1
        ah_ErrorMsg("Selezionare un solo OCL","!","")
      case reccount("__xch__")=1 AND reccount("__xc1__")>1
        ah_ErrorMsg("Selezionare un solo terzista","!","")
      case reccount("__xch__")=1 AND reccount("__xc1__")=1
        this.w_CONTINUA = .T.
        if this.oParentObject.w_FLGSIMUL="S"
          this.w_CONTINUA = ah_YesNo("Procedura non disponibile in modalit� simulata%0Proseguo in modalit� definitiva?")
        endif
        if this.w_CONTINUA=.T.
          * --- Scan del cursore (per comodit� visto che contiene un solo record !)
          select __xch__
          scan
          * --- Verifica se OCL di origine � abbinata
          this.iCODFORPA = NVL(__xch__.OLTCOFOR, SPACE(15))
          if EMPTY(this.iCODFORPA)
            ah_ErrorMsg("OCL non abbinato. Impossibile dividere",,"")
          else
            if this.oParentObject.w_nSEPARA=0
              * --- Avviso
              * --- Oggetto per messaggi incrementali
              this.w_oMess1=createobject("Ah_Message")
              this.w_oPART1 = this.w_oMess1.AddMsgPartNL("Separazione %1: la procedura divide l'%1 selezionato tra due %2")
              this.w_oPART1.addParam(IIF(this.w_TIPGES="E", "ODA", "OCL"))     
              this.w_oPART1.addParam(IIF(this.w_TIPGES="E", "fornitori", "terzisti"))     
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Il primo fornitore � quello attualmente abbinato, (%1)%0il secondo � quello selezionato nello zoom fornitori abbinabili%0%0Confermi richiesta?")
              this.w_oPART1.addParam( ALLTRIM(this.iCODFORPA))     
              this.w_CONTINUA = this.w_oMess1.ah_YesNo()
              this.oParentObject.w_nSEPARA = iif(this.w_CONTINUA,1,0)
            endif
            if this.w_CONTINUA=.T.
              * --- Dati OCL di origine il LT
              this.w_OLCODODL = NVL(__xch__.OLCODODL, SPACE(15))
              this.w_LEADPAR = nvl(__xch__.OLTEMLAV, 0)
              this.w_oQTAMIN = nvl(__xch__.QTAMIN, 0)
              this.w_oQTALOT = nvl(__xch__.LOTMUL, 0)
              this.w_OLTCODIC = nvl(__xch__.OLTCODIC, SPACE(20))
              * --- Quantita' abbinata
              this.w_OLUNIMIS = NVL(__xch__.OLTUNMIS, SPACE(3))
              this.w_QUAIN = NVL(__xch__.OLTQTODL, 0)
              this.w_QUAIN1 = NVL(__xch__.OLTQTOD1, 0)
              * --- Se il fornitore selezionato � quello preferenziale,
              this.w_OLTCOFOR = NVL(__xc1__.COCODCLF, SPACE(15))
              this.w_OLTCONTR = NVL( __xc1__.CONUMERO, SPACE(15))
              this.w_COGIOAPP = nvl (__xc1__.COGIOAPP, 0)
              this.iQTAMINAB = NVL(__xc1__.COQTAMIN, 0)
              this.iLOTMULAB = NVL(__xc1__.COLOTMUL, 0)
              if alltrim(this.w_OLTCONTR)=Ah_Msgformat("Dati anagrafici")
                this.w_OLTCONTR = ""
              endif
              * --- Maschera per la divisione delle quantit�
              this.w_mARTICOLO = SPACE(20)
              this.w_UM3 = SPACE(3)
              this.w_OP3 = " "
              this.w_MO3 = 0
              * --- Read from KEY_ARTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CAUNIMIS,CAOPERAT,CAMOLTIP,CACODART"+;
                  " from "+i_cTable+" KEY_ARTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.w_OLTCODIC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CAUNIMIS,CAOPERAT,CAMOLTIP,CACODART;
                  from (i_cTable) where;
                      CACODICE = this.w_OLTCODIC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
                this.w_OP3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
                this.w_MO3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
                this.w_mARTICOLO = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if NOT EMPTY(this.w_mARTICOLO)
                this.w_UM1 = SPACE(3)
                this.w_UM2 = SPACE(3)
                this.w_OP1 = " "
                this.w_MO1 = 0
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(this.w_mARTICOLO);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
                    from (i_cTable) where;
                        ARCODART = this.w_mARTICOLO;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                  this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
                  this.w_OP1 = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
                  this.w_MO1 = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_FOR_IN = this.iCODFORPA
                this.w_FOROUT = this.w_OLTCOFOR
                this.w_QUAOUT = 0
                this.w_QUAOUT1 = 0
                this.w_ERRORE = .T.
                do GSCO_KSA with this
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_ERRORE=.T. OR this.w_QUAOUT1=0 OR this.w_QUAIN1=0
                  ah_ErrorMsg("Separazione %1 non permessa o operazione sospesa",,"", IIF(this.w_TIPGES="E", "ODA", "OCL"))
                else
                  ah_Msg("Aggiornamento %1: %2",.T.,.F.,.F., IIF(this.w_TIPGES="E", "ODA", "OCL") , this.w_OLCODODL)
                  do GSCO_BSO with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  ah_ErrorMsg("Separazione effettuata con successo",,"")
                endif
              endif
            else
              ah_ErrorMsg("Nessuna modifica apportata",,"")
            endif
          endif
          select __xch__
          endscan
          if this.w_ERRORE=.F.
            this.oParentObject.NotifyEvent("InterDettAbbina")
            this.oParentObject.NotifyEvent("DettTerzistiAbbinabili")
          endif
        endif
    endcase
    * --- Chiude cursori
    if used("__xch__")
      use in __xch__
    endif
    if used("__xc1__")
      use in __xc1__
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempie zoom FORNITORI ABBINABILI (pag.1)
    * --- Dettaglio terzisti abbinati all'articolo selezionato
    this.cCursor = this.Padre.w_TERZIS.cCursor
    this.Griglia = this.Padre.w_TERZIS.GRD
    * --- Pulizia cursore
    select (this.cCursor)
    zap
    * --- seleziona terzisti abbinati con contratto
    do VQ_EXEC WITH "..\COLA\EXE\QUERY\GSCO2QST",this,"_ODCL_"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- seleziona terzisti abbinati presenti in anagrafica dati articolo magazzino
    do VQ_EXEC WITH "..\COLA\EXE\QUERY\GSCO8QST",this,"AbitODCL"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Unisce
    Select AbitODCL
    SCAN
    SCATTER MEMVAR
    Select _ODCL_
    append BLANK
    GATHER MEMVAR
    Select AbitODCL
    ENDSCAN
    * --- Riempie cursore zoom
    Select _ODCL_
    SCAN
    * --- Legge e scrive su cursore
    SCATTER MEMVAR
    this.iCODFORN = NVL(COCODCLF, SPACE(15))
    do VQ_EXEC WITH "..\COLA\EXE\QUERY\GSCO5QST",this,"AbbODCL"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if reccount("AbbODCL")>0
      select AbbODCL
      this.QTARTOT = NVL(OLTQTOD1, 0)
      this.QTAREVA = NVL(OLTQTOE1, 0)
    else
      this.QTARTOT = 0
      this.QTAREVA = 0
    endif
    Select (this.cCursor)
    append BLANK
    GATHER MEMVAR
    REPLACE ABBINATA WITH this.QTARTOT
    REPLACE EVASA WITH this.QTAREVA
    Select _ODCL_
    ENDSCAN
    * --- Refresh ...
    select (this.cCursor)
    go top
    this.Griglia.Refresh()     
    * --- Chiude cursori
    if used("_ODCL_")
      use in _ODCL_
    endif
    if used("AbitODCL")
      use in AbitODCL
    endif
    if used("AbbODCL")
      use in AbbODCL
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attiva zoom pagina 3 (dettagli OCL)
    * --- Dettaglio POCL per articolo
    this.w_ZOOM = this.oParentObject.w_POCLz1
    NC = this.w_Zoom.cCursor
    SELECT * FROM (NC) WHERE XCHK = 1 INTO CURSOR __xch__
    if used("__xch__")
      do case
        case reccount("__xch__")=1
          * --- Notifica eventi per riempire zoom
          this.Padre.NotifyEvent("InterrDettODL")     
          this.Padre.NotifyEvent("InterDettAbbina")     
          * --- Attiva pagina 3
          this.Padre.oPgFrm.ActivePage = 3
        case reccount("__xch__")>1
          ah_ErrorMsg("Selezionare un solo articolo",,"")
          this.Padre.oPgFrm.ActivePage = 1
          SELECT(NC)
        case reccount("__xch__")=0
          ah_ErrorMsg("Selezionare almeno un articolo",,"")
          this.Padre.oPgFrm.ActivePage = 1
          SELECT(NC)
      endcase
      * --- Chiude cursori
      if used("__xch__")
        use in __xch__
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ATTIVA PAGINA 2 E CALCOLA I DATI DEGLI ZOOM
    * --- Dettaglio POCL per articolo
    this.w_ZOOM = this.oParentObject.w_POCLz1
    NC = this.w_Zoom.cCursor
    SELECT * FROM (NC) WHERE XCHK = 1 INTO CURSOR __xch__
    if used("__xch__")
      do case
        case reccount("__xch__")>1
          ah_ErrorMsg("Selezionare un solo articolo",,"")
          this.Padre.oPgFrm.ActivePage = 1
          i_retcode = 'stop'
          return
        case reccount("__xch__")=0
          ah_ErrorMsg("Occorre selezionare un articolo",,"")
          this.Padre.oPgFrm.ActivePage = 1
          i_retcode = 'stop'
          return
      endcase
      * --- Chiude cursori
      if used("__xch__")
        use in __xch__
      endif
    endif
    * --- Attiva pagina 2
    this.Padre.oPgFrm.ActivePage = 2
    * --- Riferimenti a primo zoom pagina 2
    this.w_ZOOM = this.Padre.w_POCLz5
    this.Griglia = this.w_ZOOM.GRD
    this.cCursor = this.w_ZOOM.cCursor
    * --- Riempie cursore zoom
    Select (this.cCursor)
    zap
    do VQ_EXEC WITH "..\COLA\EXE\QUERY\GSCO9QST",this,"_deODCL_"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scan cursore ottenuto e aggiunge dati ...
    if used("_deODCL_")
      * --- Rende cursore scrivibile
      Select _deODCL_
      SCAN
      SCATTER MEMVAR
      this.iCODFORN = nvl(_deODCL_.OLTCOFOR, space(15))
      this.iLOTMUL = 0
      this.iQTAMIN = 0
      this.iGGAPPR = 0
      do VQ_EXEC WITH "..\COLA\EXE\QUERY\GSCO13QS",this,"DatForCON"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if reccount("DatForCON")>0
        this.iQTAMIN = nvl(DatForCON.COQTAMIN, 0)
        this.iLOTMUL = nvl(DatForCON.COLOTMUL, 0)
        this.iGGAPPR = nvl(DatForCON.COGIOAPP, 0)
      endif
      * --- Scrive cursore
      Select (this.cCursor)
      append BLANK
      GATHER MEMVAR
      Replace QTAMIN with this.iQTAMIN, GGAPPR with this.iGGAPPR, LOTMUL with this.iLOTMUL
      Select _deODCL_
      ENDSCAN
    endif
    * --- Calcola qta residua da abbinare
    Select SUM(OLTQTODL) AS TotNull from _deODCL_ Where EMPTY(NVL(OLTCOFOR,"")) GROUP BY ST into cursor __tot__
    if reccount("__tot__")>0
      this.oParentObject.w_QTADAABB = NVL(__tot__.TotNull, 0)
    else
      this.oParentObject.w_QTADAABB = 0
    endif
    * --- Refresh ...
    select (this.cCursor)
    go top
    this.Griglia.Refresh()     
    * --- Riferimenti a secondo zoom pagina 2
    this.oParentObject.NotifyEvent("DettTerzistiAbbinabili")
    * --- Chiude cursori
    if used("_deODCL_")
      use in _deODCL_
    endif
    if used("DatForCON")
      use in DatForCON
    endif
    if used("__tot__")
      use in __tot__
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gestione OCL
    * --- Legge ODP selezionato
    this.oParentObject.w_OCLSEL = Nvl ( this.Padre.w_POCLz3.GetVar("OLCODODL") , Space(15))
    * --- Definisce e chiama gestione
    if this.w_TIPGES="L"
      this.GestODP = GSCO_AOP(this.oParentObject, "Z")
    else
      this.GestODP = GSCO_AOP(this.oParentObject, "E")
    endif
    * --- Controllo se ha passato il test di accesso
    if !(this.GestODP.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Carica record e lascia in interrograzione ...
    this.GestODP.w_OLCODODL = this.oParentObject.w_OCLSEL
    this.TMPc = "OLCODODL="+cp_ToStrODBC(this.oParentObject.w_OCLSEL)
    this.GestODP.QueryKeySet(this.TmpC,"")     
    this.GestODP.LoadRecWarn()     
    * --- M = modifica ordine selezionato
    * --- C = cancella ordine selezionato
    do case
      case this.pOper = "M"
        * --- Modifica record
        this.GestODP.ECPEdit()     
      case this.pOper = "C"
        * --- Cancella record ...
        this.GestODP.ECPDelete()     
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
