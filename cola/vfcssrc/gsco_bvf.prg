* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bvf                                                        *
*              Verifica fattibilita piano ODL                                  *
*                                                                              *
*      Author: TAM Software Srl (SM)                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_159]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-30                                                      *
* Last revis.: 2012-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bvf",oParentObject)
return(i_retval)

define class tgsco_bvf as StdBatch
  * --- Local variables
  w_KEYSAL = space(20)
  w_CODODL = space(15)
  w_RIGMAT = 0
  Disponibile = 0
  tmpN = 0
  tmpC = space(100)
  w_ALMENOUno = .f.
  w_UNONo = .f.
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_FLSUG = space(1)
  w_FLPIA = space(1)
  w_FLLAN = space(1)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_ODLINI = space(15)
  w_ODLFIN = space(15)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MARINI = space(5)
  w_MARFIN = space(5)
  w_CODFOR = space(15)
  w_TIPGES = space(1)
  w_PROPRE = space(1)
  w_PARAM = space(1)
  w_ARFABB = space(1)
  w_ARSCO = space(1)
  w_PROINT = space(1)
  w_PROEST = space(1)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_ORAVER = space(5)
  w_ELAVER = ctod("  /  /  ")
  * --- WorkFile variables
  PAR_PROD_idx=0
  ODL_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica fattibilitā (da GSCO_SVF)
    * --- Variabili per compatibilitā con la query GSDB_SMM
    this.w_FLSUG = "M"
    this.w_FLPIA = "P"
    this.w_FLLAN = "L"
    this.w_ARFABB = "F"
    this.w_ARSCO = "S"
    this.w_PROINT = "I"
    this.w_PROEST = "L"
    this.w_DATAINI = this.oParentObject.w_DATINI
    this.w_DATAFIN = this.oParentObject.w_DATFIN
    * --- Pulizia dati
    ah_msg("Pulizia dati precedente elaborazione...")
    * --- Inizializzo OLSTARIG a 'X' e  a OLPABPRE a 0
    * --- Write into ODL_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLSTARIG ="+cp_NullLink(cp_ToStrODBC("X"),'ODL_DETT','OLSTARIG');
      +",OLPABPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLPABPRE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          OLSTARIG = "X";
          ,OLPABPRE = 0;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Crea cursori con i dati
    * --- Data Iniziale Vuota Inizializzata per le Queries
    this.oParentObject.w_DATINI = IIF(EMPTY(this.oParentObject.w_DATINI), i_INIDAT, this.oParentObject.w_DATINI)
    if this.oParentObject.w_VERIMATE = "C"
      * --- Cursore con i dati OR/IM da documenti (SOLO MAGAZZINI PRODUZIONE) e ODL
      ah_Msg("Lettura dati in corso...",.T.)
      vq_exec("..\COLA\EXE\QUERY\GSCO1BVF", this, "_Globale_")
    else
      * --- Cursore con i dati OR/IM da Materiali ODL con data evasione da w_DATINI a w_DATFIN
      ah_Msg("Lettura impegni da ODL in corso...",.T.)
      vq_exec("..\COLA\EXE\QUERY\GSCO6BVF", this, "_Globale_")
    endif
    * --- Ctea tabella TMP
    * --- Rende il cursore scrivibile e crea indice
    this.tmpN = WRCursor("_Globale_")
    select _Globale_
    index on KEYSAL+dtos(DATEVA)+iif(qtasal>0,"+","-") tag TAG1
    set order to 1
    * --- REM - LA GIACENZA INIZIALE VIENE LETTA NELLA QUERY GSDB_BVF utilizzata nella SELECT FROM QUERY ... sotto
    * --- CICLA SUI MATERIALI DA VERIFICARE
    this.w_ALMENOUno = .F.
    this.w_UNONo = .F.
    ah_Msg("Lettura saldi disponibilitā...",.T.)
    vq_exec("..\COLA\EXE\QUERY\GSCO_BVF", this, "_Saldi_")
    if USED("_Saldi_")
      SELECT _Saldi_
      GO TOP
      SCAN FOR NOT EMPTY(NVL(KEYSAL,""))
      * --- Chiave Saldo e giacenza iniziale....
      this.w_KEYSAL = KEYSAL
      this.Disponibile = NVL(QTASAL, 0)
      this.w_ALMENOUno = .T.
      * --- Messaggio
      ah_Msg("Esame codice %1 in corso...",.T.,.F.,.F., rtrim(this.w_KEYSAL) )
      * --- Legge cursore globale e crea PAB cumulato
      select _Globale_
      seek this.w_KEYSAL
      do while not eof() and _Globale_.KEYSAL = this.w_KEYSAL
        * --- Aggiorna PAB cumulativo ...
        this.Disponibile = this.Disponibile + _Globale_.QTASAL
        if _Globale_.DaVeri = "S"
          * --- Coordinate materiale
          this.w_CODODL = _Globale_.CODODL
          this.w_RIGMAT = _Globale_.TROWNUM
          * --- Verifica disponibilitā materiale (se č in negativo)
          if this.Disponibile < 0
            * --- Aggiorna riga materiale ODL
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLSTARIG ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_DETT','OLSTARIG');
              +",OLPABPRE ="+cp_NullLink(cp_ToStrODBC(this.Disponibile),'ODL_DETT','OLPABPRE');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGMAT);
                     )
            else
              update (i_cTable) set;
                  OLSTARIG = "N";
                  ,OLPABPRE = this.Disponibile;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_CODODL;
                  and CPROWNUM = this.w_RIGMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_UNONo = .T.
          else
            * --- Aggiorna riga materiale ODL
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLSTARIG ="+cp_NullLink(cp_ToStrODBC("D"),'ODL_DETT','OLSTARIG');
              +",OLPABPRE ="+cp_NullLink(cp_ToStrODBC(this.Disponibile),'ODL_DETT','OLPABPRE');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGMAT);
                     )
            else
              update (i_cTable) set;
                  OLSTARIG = "D";
                  ,OLPABPRE = this.Disponibile;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_CODODL;
                  and CPROWNUM = this.w_RIGMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        * --- Avanza di un record ...
        select _Globale_
        if not eof()
          skip
        endif
      enddo
      SELECT _Saldi_
      ENDSCAN
    endif
    * --- Chiude ultimo cursore ...
    if used("_Saldi_")
      USE IN _Saldi_
    endif
    if used("_Globale_")
      USE IN _Globale_
    endif
    * --- Aggiorna dati ultima elaborazione ...
    this.w_ORAVER = left(Time(),5)
    this.w_ELAVER = date()
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPOPEVER ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_PROD','PPOPEVER');
      +",PPELAVER ="+cp_NullLink(cp_ToStrODBC(this.w_ELAVER),'PAR_PROD','PPELAVER');
      +",PPORAVER ="+cp_NullLink(cp_ToStrODBC(this.w_ORAVER),'PAR_PROD','PPORAVER');
      +",PPDINVER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINI),'PAR_PROD','PPDINVER');
      +",PPDFIVER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'PAR_PROD','PPDFIVER');
      +",PPTIPVER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_VERIMATE),'PAR_PROD','PPTIPVER');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("PP");
             )
    else
      update (i_cTable) set;
          PPOPEVER = i_CODUTE;
          ,PPELAVER = this.w_ELAVER;
          ,PPORAVER = this.w_ORAVER;
          ,PPDINVER = this.oParentObject.w_DATINI;
          ,PPDFIVER = this.oParentObject.w_DATFIN;
          ,PPTIPVER = this.oParentObject.w_VERIMATE;
          &i_ccchkf. ;
       where;
          PPCODICE = "PP";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Messaggio di Chiusura ...
    L_TIPVER = this.oParentObject.w_VERIMATE
    L_TIPSTA = "N"
    L_TIPMAT = "T"
    L_DATINI = IIF(this.oParentObject.w_DATINI=i_INIDAT, cp_CharToDate("  -  -  "), this.oParentObject.w_DATINI)
    L_DATFIN = this.oParentObject.w_DATFIN
    if this.w_ALMENOUno
      if this.w_UNONo
        this.tmpC = "Sono stati rilevati ordini con materiali mancanti (non fattibili)%0Vuoi la stampa dei materiali non disponibili?"
        if ah_YesNo(this.TmpC)
          vx_exec("..\COLA\EXE\QUERY\GSCO_SMM.VQR",this)
        endif
      else
        this.tmpC = "Elaborazione terminata%0Tutti gli ODL risultano fattibili"
        ah_ErrorMsg(this.tmpC,48)
      endif
    else
      this.tmpC = "Per il periodo considerato, non sono presenti ODL da verificare"
      ah_ErrorMsg(this.tmpC,48)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='ODL_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
