* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bsf                                                        *
*              Dettaglio import piano di produzione                            *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-01                                                      *
* Last revis.: 2010-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bsf",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsco_bsf as StdBatch
  * --- Local variables
  w_PARAM = space(10)
  w_GSDB_KSP = .NULL.
  w_ZOOM = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Strutture per generazione Mov. di Magazzino (da GSDB_KGM)
    this.w_GSDB_KSP = this.oParentObject
    do case
      case this.w_PARAM="BLANK"
        this.w_ZOOM = this.w_GSDB_KSP.w_ZoomMast
        * --- Lancia la Query
        ah_msg("Ricerca Scaletta")
        this.w_GSDB_KSP.NotifyEvent("Calcola")     
        WAIT CLEAR
        NC = this.w_Zoom.cCursor
        UPDATE (NC) SET XCHK = 0
        GO TOP
        * --- Setta Proprieta' Campi del Cursore
        FOR I=1 TO this.w_Zoom.grd.ColumnCount
        NC = ALLTRIM(STR(I))
        if UPPER(this.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
          this.w_ZOOM.grd.Column&NC..DynamicForeColor = "IIF(XCHK=0, RGB(0,0,0), RGB(0,0,255))"
          this.w_ZOOM.grd.Column&NC..DynamicBackColor = "IIF(XCHK=1, RGB(255,255,192), RGB(255,255,255))"
        endif
        ENDFOR
        if used("GeneApp")
          use in GeneApp
        endif
        * --- Crea il Cursore di Appoggio Dettagli (Conterra' i Riferimenti alle Righe/Documenti DA GENERARE)
        CREATE CURSOR GeneApp (XCHK N(1), SCSERIAL C(15), CPROWORD N(5), CPROWNUM N(5), SCQTARES N(12,3), SCRESODL N(12,3))
      case this.w_PARAM="Interroga"
        * --- Pulisce cursore di appoggio e aggiorna lo zoom in testata
        CREATE CURSOR GeneApp (XCHK N(1), SCSERIAL C(15), CPROWORD N(5), CPROWNUM N(5), SCQTARES N(12,3))
        this.w_GSDB_KSP.NotifyEvent("Calcola")     
        this.w_GSDB_KSP.NotifyEvent("CalcolaDett")     
        this.w_GSDB_KSP.oPgFrm.ActivePage = 1
    endcase
  endproc


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
