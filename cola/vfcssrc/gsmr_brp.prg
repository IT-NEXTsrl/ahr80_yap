* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_brp                                                        *
*              Verifica congruit� giacenze Pegging di commessa                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-15                                                      *
* Last revis.: 2012-08-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_brp",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_brp as StdBatch
  * --- Local variables
  pAzione = space(2)
  w_QTASAL = 0
  w_NUMREC = 0
  w_CODSAL = space(40)
  w_CODMAG = space(5)
  * --- WorkFile variables
  PEG_SELI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica congruit� giacenze Pegging di Commessa (DA GSMR_BGP)
    do case
      case this.pAzione="MRP"
        * --- Verifico che le giacenze presenti su PEG_SELI siano 'coperte' dalle quantit�' presenti nei saldi di magazzino.
        vq_exec("..\cola\exe\query\GSMR_BRP", this, "_PEGGING_") 
 vq_exec("..\cola\exe\query\GSMR1BRP", this, "_SALDI_")
    endcase
    Select _SALDI_
    scan
    this.w_QTASAL = _SALDI_.QTASAL
    this.w_CODSAL = _SALDI_.SLCODICE
    this.w_CODMAG = _SALDI_.SLCODMAG
    this.w_NUMREC = recno()
    Select _PEGGING_
    scan for KEYSAL=this.w_CODSAL and CODMAG=this.w_CODMAG
    if this.w_QTASAL>=_PEGGING_.QTAABB
      * --- Ho copertura a magazzino, proseguo la verifica stornando la parte di saldo impegnata dalla riga di Pegging in esame
      this.w_QTASAL = this.w_QTASAL-_PEGGING_.QTAABB
      if _PEGGING_.QTAABB=0
        * --- Riga a 0, tale riga � evasa quindi va eliminata
        * --- Delete from PEG_SELI
        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PESERIAL = "+cp_ToStrODBC(_PEGGING_.SERIAL);
                 )
        else
          delete from (i_cTable) where;
                PESERIAL = _PEGGING_.SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    else
      * --- Se non ho copertura a magazzino, allora storno o elimino la relativa riga di Pegging
      do case
        case this.pAzione="MRP"
          if this.w_QTASAL>0
            * --- Riga parzialmente coperta
            * --- Write into PEG_SELI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(this.w_QTASAL),'PEG_SELI','PEQTAABB');
                  +i_ccchkf ;
              +" where ";
                  +"PESERIAL = "+cp_ToStrODBC(_PEGGING_.SERIAL);
                     )
            else
              update (i_cTable) set;
                  PEQTAABB = this.w_QTASAL;
                  &i_ccchkf. ;
               where;
                  PESERIAL = _PEGGING_.SERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_QTASAL = 0
          else
            * --- Riga completamente scoperta
            * --- Delete from PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PESERIAL = "+cp_ToStrODBC(_PEGGING_.SERIAL);
                     )
            else
              delete from (i_cTable) where;
                    PESERIAL = _PEGGING_.SERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
      endcase
    endif
    endscan
    Select _SALDI_
    go this.w_NUMREC
    endscan
    if used("_SALDI_")
      Use in _SALDI_
    endif
    if used("_PEGGING_")
      Use in _PEGGING_
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PEG_SELI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
