* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_kgf                                                        *
*              Generazione piano produzione                                    *
*                                                                              *
*      Author: Zucchetti TAM Srl (SM)                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-08                                                      *
* Last revis.: 2017-01-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdb_kgf",oParentObject))

* --- Class definition
define class tgsdb_kgf as StdForm
  Top    = 4
  Left   = 5

  * --- Standard Properties
  Width  = 827
  Height = 410
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-01-16"
  HelpContextID=32142441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  MARCHI_IDX = 0
  ART_ICOL_IDX = 0
  PAR_PROD_IDX = 0
  cPrg = "gsdb_kgf"
  cComment = "Generazione piano produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  o_PARAM = space(1)
  w_ELASCORTA = space(1)
  o_ELASCORTA = space(1)
  w_ED = .F.
  o_ED = .F.
  w_FLTEMPOR = space(1)
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  o_CODFIN = space(20)
  w_FAMAINI = space(5)
  o_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  o_FAMAFIN = space(5)
  w_GRUINI = space(5)
  o_GRUINI = space(5)
  w_GRUFIN = space(5)
  o_GRUFIN = space(5)
  w_CATINI = space(5)
  o_CATINI = space(5)
  w_CATFIN = space(5)
  o_CATFIN = space(5)
  w_MAGINI = space(5)
  o_MAGINI = space(5)
  w_MAGFIN = space(5)
  o_MAGFIN = space(5)
  w_MARINI = space(5)
  o_MARINI = space(5)
  w_MARFIN = space(5)
  o_MARFIN = space(5)
  w_ODF = space(1)
  o_ODF = space(1)
  w_STATO = space(1)
  o_STATO = space(1)
  w_STATO = space(1)
  w_STATO1 = space(1)
  w_CRIFOR = space(1)
  o_CRIFOR = space(1)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESMARI = space(35)
  w_DESMARF = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_FLCOMM = space(1)
  w_ELAMPS = space(1)
  w_ELAODF = space(1)
  w_PPTIPDTF = space(1)
  w_PP___DTF = 0
  w_PARPROD = space(10)
  o_PARPROD = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdb_kgfPag1","gsdb_kgf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELASCORTA_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='FAM_ARTI'
    this.cWorkTables[3]='GRUMERC'
    this.cWorkTables[4]='CATEGOMO'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='PAR_PROD'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSDB_BGF(this,"G")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_ELASCORTA=space(1)
      .w_ED=.f.
      .w_FLTEMPOR=space(1)
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_MARINI=space(5)
      .w_MARFIN=space(5)
      .w_ODF=space(1)
      .w_STATO=space(1)
      .w_STATO=space(1)
      .w_STATO1=space(1)
      .w_CRIFOR=space(1)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESMARI=space(35)
      .w_DESMARF=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_FLCOMM=space(1)
      .w_ELAMPS=space(1)
      .w_ELAODF=space(1)
      .w_PPTIPDTF=space(1)
      .w_PP___DTF=0
      .w_PARPROD=space(10)
        .w_PARAM = this.oParentObject
          .DoRTCalc(2,2,.f.)
        .w_ED = empty(.w_CODINI) and empty(.w_CODFIN) and empty(.w_FAMAINI) and empty(.w_FAMAFIN) and empty(.w_GRUINI) and empty(.w_GRUFIN) and empty(.w_CATINI) and empty(.w_CATFIN) and empty(.w_MAGINI) and empty(.w_MAGFIN) and empty(.w_MARINI) and empty(.w_MARFIN)
        .w_FLTEMPOR = iif(.w_ED, 'S',.w_FLTEMPOR)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODINI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODFIN))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_GRUINI))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CATINI))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CATFIN))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_MAGINI))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MARINI))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MARFIN))
          .link_1_16('Full')
        endif
        .w_ODF = 'N'
        .w_STATO = 'S'
        .w_STATO = 'S'
        .w_STATO1 = 'S'
        .w_CRIFOR = 'I'
      .oPgFrm.Page1.oPag.oObj_1_48.Calculate(AH_MsgFormat("Questa funzione esegue la rigenerazione del piano MPS per l'orizzonte temporale definito nella tabella parametri.") ,'',)
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate(AH_MsgFormat(iif(.w_PARAM="F","L'elaborazione considera gli articoli di provenienza preferenziale esterna a fabbisogno con sistema d'ordine oggetto MPS.","L'elaborazione considera gli articoli gestiti a fabbisogno con sistema d'ordine oggetto MPS o ricambio.")) ,'',)
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate(AH_MsgFormat("E' possibile estendere l'elaborazione agli articoli gestiti a scorta abilitando le relative opzioni.") ,'',)
          .DoRTCalc(22,33,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
          .DoRTCalc(35,35,.f.)
        .w_ELAMPS = iif(.w_PARAM='F','N','S')
        .w_ELAODF = iif(.w_PARAM='F','S',.w_ODF)
        .w_PPTIPDTF = 'F'
          .DoRTCalc(39,39,.f.)
        .w_PARPROD = 'PP'
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_PARPROD))
          .link_1_63('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        Local l_Dep1,l_Dep2,l_Dep3
        l_Dep1= .o_CODINI<>.w_CODINI .or. .o_CODFIN<>.w_CODFIN .or. .o_CATFIN<>.w_CATFIN .or. .o_CATINI<>.w_CATINI .or. .o_FAMAINI<>.w_FAMAINI        l_Dep2= .o_FAMAFIN<>.w_FAMAFIN .or. .o_GRUFIN<>.w_GRUFIN .or. .o_GRUINI<>.w_GRUINI .or. .o_MAGINI<>.w_MAGINI .or. .o_MAGFIN<>.w_MAGFIN        l_Dep3= .o_MARFIN<>.w_MARFIN .or. .o_MARINI<>.w_MARINI .or. .o_CRIFOR<>.w_CRIFOR .or. .o_STATO<>.w_STATO
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3
            .w_ED = empty(.w_CODINI) and empty(.w_CODFIN) and empty(.w_FAMAINI) and empty(.w_FAMAFIN) and empty(.w_GRUINI) and empty(.w_GRUFIN) and empty(.w_CATINI) and empty(.w_CATFIN) and empty(.w_MAGINI) and empty(.w_MAGFIN) and empty(.w_MARINI) and empty(.w_MARFIN)
        endif
        Local l_Dep1,l_Dep2,l_Dep3
        l_Dep1= .o_ED<>.w_ED .or. .o_CATFIN<>.w_CATFIN .or. .o_CATINI<>.w_CATINI .or. .o_CODFIN<>.w_CODFIN .or. .o_CODINI<>.w_CODINI        l_Dep2= .o_CRIFOR<>.w_CRIFOR .or. .o_ELASCORTA<>.w_ELASCORTA .or. .o_FAMAFIN<>.w_FAMAFIN .or. .o_FAMAINI<>.w_FAMAINI .or. .o_GRUFIN<>.w_GRUFIN        l_Dep3= .o_GRUINI<>.w_GRUINI .or. .o_MAGFIN<>.w_MAGFIN .or. .o_MAGINI<>.w_MAGINI .or. .o_MARFIN<>.w_MARFIN .or. .o_MARINI<>.w_MARINI
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3
            .w_FLTEMPOR = iif(.w_ED, 'S',.w_FLTEMPOR)
        endif
        .DoRTCalc(5,5,.t.)
        if .o_CODINI<>.w_CODINI
          .link_1_6('Full')
        endif
        if .o_FAMAFIN<>.w_FAMAFIN
          .link_1_7('Full')
        endif
        if .o_FAMAINI<>.w_FAMAINI
          .link_1_8('Full')
        endif
        if .o_GRUFIN<>.w_GRUFIN
          .link_1_9('Full')
        endif
        if .o_GRUINI<>.w_GRUINI
          .link_1_10('Full')
        endif
        if .o_CATFIN<>.w_CATFIN
          .link_1_11('Full')
        endif
        if .o_CATINI<>.w_CATINI
          .link_1_12('Full')
        endif
        if .o_MAGFIN<>.w_MAGFIN
          .link_1_13('Full')
        endif
        if .o_MAGINI<>.w_MAGINI
          .link_1_14('Full')
        endif
        if .o_MARFIN<>.w_MARFIN
          .link_1_15('Full')
        endif
        if .o_MARINI<>.w_MARINI
          .link_1_16('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(AH_MsgFormat("Questa funzione esegue la rigenerazione del piano MPS per l'orizzonte temporale definito nella tabella parametri.") ,'',)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(AH_MsgFormat(iif(.w_PARAM="F","L'elaborazione considera gli articoli di provenienza preferenziale esterna a fabbisogno con sistema d'ordine oggetto MPS.","L'elaborazione considera gli articoli gestiti a fabbisogno con sistema d'ordine oggetto MPS o ricambio.")) ,'',)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(AH_MsgFormat("E' possibile estendere l'elaborazione agli articoli gestiti a scorta abilitando le relative opzioni.") ,'',)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .DoRTCalc(17,35,.t.)
            .w_ELAMPS = iif(.w_PARAM='F','N','S')
        if .o_PARAM<>.w_PARAM.or. .o_ODF<>.w_ODF
            .w_ELAODF = iif(.w_PARAM='F','S',.w_ODF)
        endif
        .DoRTCalc(38,39,.t.)
        if .o_PARPROD<>.w_PARPROD
          .link_1_63('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(AH_MsgFormat("Questa funzione esegue la rigenerazione del piano MPS per l'orizzonte temporale definito nella tabella parametri.") ,'',)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(AH_MsgFormat(iif(.w_PARAM="F","L'elaborazione considera gli articoli di provenienza preferenziale esterna a fabbisogno con sistema d'ordine oggetto MPS.","L'elaborazione considera gli articoli gestiti a fabbisogno con sistema d'ordine oggetto MPS o ricambio.")) ,'',)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(AH_MsgFormat("E' possibile estendere l'elaborazione agli articoli gestiti a scorta abilitando le relative opzioni.") ,'',)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLTEMPOR_1_4.visible=!this.oPgFrm.Page1.oPag.oFLTEMPOR_1_4.mHide()
    this.oPgFrm.Page1.oPag.oODF_1_17.visible=!this.oPgFrm.Page1.oPag.oODF_1_17.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_18.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_18.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_19.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oSTATO1_1_20.visible=!this.oPgFrm.Page1.oPag.oSTATO1_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCRIFOR_1_21.visible=!this.oPgFrm.Page1.oPag.oCRIFOR_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_5'),i_cWhere,'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_6'),i_cWhere,'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_7'),i_cWhere,'GSAR_AFA',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_8'),i_cWhere,'GSAR_AFA',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_9'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_10'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_11'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_12'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_13'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_14'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARINI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARINI_1_15'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARINI = space(5)
      endif
      this.w_DESMARI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MARINI = space(5)
        this.w_DESMARI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARFIN
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARFIN_1_16'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARFIN = space(5)
      endif
      this.w_DESMARF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARINI <= .w_MARFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MARFIN = space(5)
        this.w_DESMARF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PARPROD
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PARPROD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PARPROD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PP___DTF";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PARPROD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PARPROD)
            select PPCODICE,PP___DTF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PARPROD = NVL(_Link_.PPCODICE,space(10))
      this.w_PP___DTF = NVL(_Link_.PP___DTF,0)
    else
      if i_cCtrl<>'Load'
        this.w_PARPROD = space(10)
      endif
      this.w_PP___DTF = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PARPROD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELASCORTA_1_2.RadioValue()==this.w_ELASCORTA)
      this.oPgFrm.Page1.oPag.oELASCORTA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTEMPOR_1_4.RadioValue()==this.w_FLTEMPOR)
      this.oPgFrm.Page1.oPag.oFLTEMPOR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_5.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_5.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_6.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_6.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_7.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_7.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_8.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_8.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_9.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_9.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_10.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_10.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_11.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_11.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_12.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_12.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_13.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_13.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_14.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_14.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMARINI_1_15.value==this.w_MARINI)
      this.oPgFrm.Page1.oPag.oMARINI_1_15.value=this.w_MARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMARFIN_1_16.value==this.w_MARFIN)
      this.oPgFrm.Page1.oPag.oMARFIN_1_16.value=this.w_MARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oODF_1_17.RadioValue()==this.w_ODF)
      this.oPgFrm.Page1.oPag.oODF_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_18.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_19.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO1_1_20.RadioValue()==this.w_STATO1)
      this.oPgFrm.Page1.oPag.oSTATO1_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCRIFOR_1_21.RadioValue()==this.w_CRIFOR)
      this.oPgFrm.Page1.oPag.oCRIFOR_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_25.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_25.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_27.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_27.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_28.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_28.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_30.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_30.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_32.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_32.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_33.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_33.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_34.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_34.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_38.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_38.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_39.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_39.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_40.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_40.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMARI_1_45.value==this.w_DESMARI)
      this.oPgFrm.Page1.oPag.oDESMARI_1_45.value=this.w_DESMARI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMARF_1_47.value==this.w_DESMARF)
      this.oPgFrm.Page1.oPag.oDESMARF_1_47.value=this.w_DESMARF
    endif
    if not(this.oPgFrm.Page1.oPag.oPPTIPDTF_1_59.RadioValue()==this.w_PPTIPDTF)
      this.oPgFrm.Page1.oPag.oPPTIPDTF_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPP___DTF_1_60.value==this.w_PP___DTF)
      this.oPgFrm.Page1.oPag.oPP___DTF_1_60.value=this.w_PP___DTF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CODINI<=.w_CODFIN or empty(.w_CODINI)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN)  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN))  and not(empty(.w_MARINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARINI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARINI <= .w_MARFIN)  and not(empty(.w_MARFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PARAM = this.w_PARAM
    this.o_ELASCORTA = this.w_ELASCORTA
    this.o_ED = this.w_ED
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_FAMAINI = this.w_FAMAINI
    this.o_FAMAFIN = this.w_FAMAFIN
    this.o_GRUINI = this.w_GRUINI
    this.o_GRUFIN = this.w_GRUFIN
    this.o_CATINI = this.w_CATINI
    this.o_CATFIN = this.w_CATFIN
    this.o_MAGINI = this.w_MAGINI
    this.o_MAGFIN = this.w_MAGFIN
    this.o_MARINI = this.w_MARINI
    this.o_MARFIN = this.w_MARFIN
    this.o_ODF = this.w_ODF
    this.o_STATO = this.w_STATO
    this.o_CRIFOR = this.w_CRIFOR
    this.o_PARPROD = this.w_PARPROD
    return

enddefine

* --- Define pages as container
define class tgsdb_kgfPag1 as StdContainer
  Width  = 823
  height = 410
  stdWidth  = 823
  stdheight = 410
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELASCORTA_1_2 as StdCheck with uid="XZJRKHSRIO",rtseq=2,rtrep=.f.,left=111, top=99, caption="Articoli a scorta a quantitą costante",;
    ToolTipText = "Se attivo: esegue elaborazione articoli gestiti a scorta",;
    HelpContextID = 27062442,;
    cFormVar="w_ELASCORTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELASCORTA_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELASCORTA_1_2.GetRadio()
    this.Parent.oContained.w_ELASCORTA = this.RadioValue()
    return .t.
  endfunc

  func oELASCORTA_1_2.SetRadio()
    this.Parent.oContained.w_ELASCORTA=trim(this.Parent.oContained.w_ELASCORTA)
    this.value = ;
      iif(this.Parent.oContained.w_ELASCORTA=='S',1,;
      0)
  endfunc

  add object oFLTEMPOR_1_4 as StdCheck with uid="EWXZMFFKFO",rtseq=4,rtrep=.f.,left=588, top=99, caption="Rigenera bidoni temporali",;
    ToolTipText = "Se attivo: esegue rigenerazione dei bidoni temporali",;
    HelpContextID = 53484712,;
    cFormVar="w_FLTEMPOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTEMPOR_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLTEMPOR_1_4.GetRadio()
    this.Parent.oContained.w_FLTEMPOR = this.RadioValue()
    return .t.
  endfunc

  func oFLTEMPOR_1_4.SetRadio()
    this.Parent.oContained.w_FLTEMPOR=trim(this.Parent.oContained.w_FLTEMPOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLTEMPOR=='S',1,;
      0)
  endfunc

  func oFLTEMPOR_1_4.mHide()
    with this.Parent.oContained
      return (.w_ED)
    endwith
  endfunc

  add object oCODINI_1_5 as StdField with uid="LNNZXSNIRS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo di inizio selezione",;
    HelpContextID = 205725478,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=111, Top=134, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODFIN_1_6 as StdField with uid="LYRVUKLWRB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articoli di fine selezione",;
    HelpContextID = 15736614,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=111, Top=159, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  proc oCODFIN_1_6.mDefault
    with this.Parent.oContained
      if empty(.w_CODFIN)
        .w_CODFIN = .w_CODINI
      endif
    endwith
  endproc

  func oCODFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_1_7 as StdField with uid="BAUZFWPTWC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 252993194,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=184, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie articoli",'',this.parent.oContained
  endproc
  proc oFAMAINI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMAINI
     i_obj.ecpSave()
  endproc

  add object oFAMAFIN_1_8 as StdField with uid="GHDDKOKVIG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 71589546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=515, Top=184, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie articoli",'',this.parent.oContained
  endproc
  proc oFAMAFIN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMAFIN
     i_obj.ecpSave()
  endproc

  add object oGRUINI_1_9 as StdField with uid="VYQYSTJFCH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 205795942,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=209, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_10 as StdField with uid="BKTBROMCMD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 15807078,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=515, Top=209, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_10.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_11 as StdField with uid="WQNLFQEXDD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 205787430,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=234, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_12 as StdField with uid="WHQQEZYTLE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 15798566,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=515, Top=234, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_12.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oMAGINI_1_13 as StdField with uid="WWAXPPIMPZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di inizio selezione",;
    HelpContextID = 205734342,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMAGFIN_1_14 as StdField with uid="IPBLQAUROZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di fine selezione",;
    HelpContextID = 15745478,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=515, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_14.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMARINI_1_15 as StdField with uid="YZOGKGSXPU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MARINI", cQueryName = "MARINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di inizio selezione",;
    HelpContextID = 205779398,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=284, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARINI"

  func oMARINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARINI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARINI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARINI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARINI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARINI
     i_obj.ecpSave()
  endproc

  add object oMARFIN_1_16 as StdField with uid="NRZQAZMNEF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MARFIN", cQueryName = "MARFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di fine selezione",;
    HelpContextID = 15790534,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=515, Top=284, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARFIN"

  proc oMARFIN_1_16.mDefault
    with this.Parent.oContained
      if empty(.w_MARFIN)
        .w_MARFIN = .w_MARINI
      endif
    endwith
  endproc

  func oMARFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARFIN_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARFIN_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARFIN_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARFIN_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARFIN
     i_obj.ecpSave()
  endproc

  add object oODF_1_17 as StdCheck with uid="YHJQHNQJGK",rtseq=17,rtrep=.f.,left=177, top=309, caption="Genera ODF",;
    ToolTipText = "Se attivo: genera ordini a fornitore per articoli oggetto MPS di proveninza esterna",;
    HelpContextID = 31836954,;
    cFormVar="w_ODF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oODF_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oODF_1_17.GetRadio()
    this.Parent.oContained.w_ODF = this.RadioValue()
    return .t.
  endfunc

  func oODF_1_17.SetRadio()
    this.Parent.oContained.w_ODF=trim(this.Parent.oContained.w_ODF)
    this.value = ;
      iif(this.Parent.oContained.w_ODF=='S',1,;
      0)
  endfunc

  func oODF_1_17.mHide()
    with this.Parent.oContained
      return (.w_PARAM='F' or g_MODA<>'S')
    endwith
  endfunc


  add object oSTATO_1_18 as StdCombo with uid="MCZCYDEPTE",rtseq=18,rtrep=.f.,left=513,top=312,width=153,height=21;
    , ToolTipText = "Stato degli ODF generati";
    , HelpContextID = 211946202;
    , cFormVar="w_STATO",RowSource=""+"Suggerito,"+"Confermato,"+"Da ordinare,"+"Da anagrafica articolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'P',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oSTATO_1_18.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_18.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='C',2,;
      iif(this.Parent.oContained.w_STATO=='P',3,;
      iif(this.Parent.oContained.w_STATO=='A',4,;
      0))))
  endfunc

  func oSTATO_1_18.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'F')
    endwith
  endfunc


  add object oSTATO_1_19 as StdCombo with uid="VMVXKYCGHE",rtseq=19,rtrep=.f.,left=513,top=312,width=153,height=21;
    , ToolTipText = "Stato degli ODP generati relativamente agli articoli con tipologia di gestione a fabbisogno";
    , HelpContextID = 211946202;
    , cFormVar="w_STATO",RowSource=""+"Suggerito,"+"Confermato,"+"Da pianificare,"+"Da anagrafica articolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oSTATO_1_19.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_19.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='C',2,;
      iif(this.Parent.oContained.w_STATO=='D',3,;
      iif(this.Parent.oContained.w_STATO=='A',4,;
      0))))
  endfunc

  func oSTATO_1_19.mHide()
    with this.Parent.oContained
      return (.w_PARAM='F')
    endwith
  endfunc


  add object oSTATO1_1_20 as StdCombo with uid="ZNGZXVRQYM",rtseq=20,rtrep=.f.,left=177,top=339,width=153,height=21;
    , ToolTipText = "Stato degli ODF generati relativamente agli articoli con tipologia di gestione a fabbisogno";
    , HelpContextID = 73266470;
    , cFormVar="w_STATO1",RowSource=""+"Suggerito,"+"Confermato,"+"Da ordinare,"+"Da anagrafica articolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO1_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'P',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oSTATO1_1_20.GetRadio()
    this.Parent.oContained.w_STATO1 = this.RadioValue()
    return .t.
  endfunc

  func oSTATO1_1_20.SetRadio()
    this.Parent.oContained.w_STATO1=trim(this.Parent.oContained.w_STATO1)
    this.value = ;
      iif(this.Parent.oContained.w_STATO1=='S',1,;
      iif(this.Parent.oContained.w_STATO1=='C',2,;
      iif(this.Parent.oContained.w_STATO1=='P',3,;
      iif(this.Parent.oContained.w_STATO1=='A',4,;
      0))))
  endfunc

  func oSTATO1_1_20.mHide()
    with this.Parent.oContained
      return (.w_PARAM='F' or .w_ELAODF<>'S')
    endwith
  endfunc


  add object oCRIFOR_1_21 as StdCombo with uid="BKBXNLQDDK",rtseq=21,rtrep=.f.,left=513,top=339,width=153,height=21;
    , ToolTipText = "Criterio di scelta di default del miglior fornitore in base ai contratti in essere";
    , HelpContextID = 89158182;
    , cFormVar="w_CRIFOR",RowSource=""+"Prioritą,"+"Tempo,"+"Prezzo,"+"Affidabilitą", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIFOR_1_21.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oCRIFOR_1_21.GetRadio()
    this.Parent.oContained.w_CRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCRIFOR_1_21.SetRadio()
    this.Parent.oContained.w_CRIFOR=trim(this.Parent.oContained.w_CRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CRIFOR=='I',1,;
      iif(this.Parent.oContained.w_CRIFOR=='T',2,;
      iif(this.Parent.oContained.w_CRIFOR=='Z',3,;
      iif(this.Parent.oContained.w_CRIFOR=='A',4,;
      0))))
  endfunc

  func oCRIFOR_1_21.mHide()
    with this.Parent.oContained
      return (.w_ELAODF<>'S')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="EMLNSKOOAT",left=715, top=357, width=48,height=45,;
    CpPicture="BMP\GENERA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il piano principale di produzione";
    , HelpContextID = 58803478;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSDB_BGF(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="FIHDDDZPON",left=765, top=357, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58803478;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAGI_1_25 as StdField with uid="QONUNYBHAK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 109574858,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=176, Top=259, InputMask=replicate('X',30)

  add object oDESMAGF_1_27 as StdField with uid="EBXVFVWORM",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158860598,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=581, Top=259, InputMask=replicate('X',30)

  add object oDESINI_1_28 as StdField with uid="SSNEPWHMKY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 205784374,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=287, Top=134, InputMask=replicate('X',40)

  add object oDESFIN_1_30 as StdField with uid="LMJMLCZYUA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 15795510,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=287, Top=159, InputMask=replicate('X',40)

  add object oDESFAMAI_1_32 as StdField with uid="AOYBYAMKXI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259065215,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=176, Top=184, InputMask=replicate('X',35)

  add object oDESGRUI_1_33 as StdField with uid="ZABORREMDH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125696714,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=176, Top=209, InputMask=replicate('X',35)

  add object oDESCATI_1_34 as StdField with uid="ZFWYARIWSP",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160561866,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=176, Top=234, InputMask=replicate('X',35)

  add object oDESFAMAF_1_38 as StdField with uid="WOCDSQXKSA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259065212,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=581, Top=184, InputMask=replicate('X',35)

  add object oDESGRUF_1_39 as StdField with uid="PYGQUHDJMJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125696714,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=581, Top=209, InputMask=replicate('X',35)

  add object oDESCATF_1_40 as StdField with uid="SXSGGSHCII",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160561866,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=581, Top=234, InputMask=replicate('X',35)

  add object oDESMARI_1_45 as StdField with uid="MXNLJGFEDS",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESMARI", cQueryName = "DESMARI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193460938,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=176, Top=284, InputMask=replicate('X',35)

  add object oDESMARF_1_47 as StdField with uid="UFBSQRHGVN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESMARF", cQueryName = "DESMARF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193460938,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=581, Top=284, InputMask=replicate('X',35)


  add object oObj_1_48 as cp_calclbl with uid="FZCVVXCORO",left=8, top=4, width=806,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 145855206


  add object oObj_1_49 as cp_calclbl with uid="CXKRTDRUHV",left=8, top=30, width=807,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 145855206


  add object oObj_1_50 as cp_calclbl with uid="ZTNNSBAIQP",left=8, top=56, width=807,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 145855206


  add object oObj_1_55 as cp_runprogram with uid="XMYZSZSXGZ",left=178, top=472, width=146,height=21,;
    caption='GSDB_BGF("T")',;
   bGlobalFont=.t.,;
    prg="GSDB_BGF('T')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 117209044


  add object oPPTIPDTF_1_59 as StdCombo with uid="CAWZZMJXMN",rtseq=38,rtrep=.f.,left=174,top=367,width=153,height=22;
    , ToolTipText = "Indica la modalitą di gestione del DTF (demand time fence) dando la possibilitą di gestire il parametro per articolo oppure generico per elaborazione";
    , HelpContextID = 124002620;
    , cFormVar="w_PPTIPDTF",RowSource=""+"Da anagrafica articolo,"+"Forza valore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPTIPDTF_1_59.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPPTIPDTF_1_59.GetRadio()
    this.Parent.oContained.w_PPTIPDTF = this.RadioValue()
    return .t.
  endfunc

  func oPPTIPDTF_1_59.SetRadio()
    this.Parent.oContained.w_PPTIPDTF=trim(this.Parent.oContained.w_PPTIPDTF)
    this.value = ;
      iif(this.Parent.oContained.w_PPTIPDTF=='A',1,;
      iif(this.Parent.oContained.w_PPTIPDTF=='F',2,;
      0))
  endfunc

  add object oPP___DTF_1_60 as StdField with uid="UMQULEECOL",rtseq=39,rtrep=.f.,;
    cFormVar = "w_PP___DTF", cQueryName = "PP___DTF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Demand time fence (non considera le previsioni nei prossimi N giorni)",;
    HelpContextID = 141218108,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=513, Top=367

  add object oStr_1_24 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=12, Top=259,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=420, Top=259,;
    Alignment=1, Width=92, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="NMLJSVRZGE",Visible=.t., Left=12, Top=134,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="XRBSMYKOYR",Visible=.t., Left=12, Top=159,;
    Alignment=1, Width=97, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=12, Top=184,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=12, Top=209,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=12, Top=234,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BETNLUNOYI",Visible=.t., Left=420, Top=184,;
    Alignment=1, Width=92, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=420, Top=209,;
    Alignment=1, Width=92, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=420, Top=234,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="SGSTIMCHVV",Visible=.t., Left=12, Top=284,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="ZYLPGFCGXX",Visible=.t., Left=420, Top=284,;
    Alignment=1, Width=92, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="YYWJXGHCUL",Visible=.t., Left=367, Top=314,;
    Alignment=1, Width=143, Height=18,;
    Caption="Stato degli ordini generati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="GOBGUXNNPC",Visible=.t., Left=367, Top=341,;
    Alignment=1, Width=143, Height=18,;
    Caption="Criterio scelta fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_ELAODF<>'S')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="RZGCZNXUEO",Visible=.t., Left=72, Top=341,;
    Alignment=1, Width=100, Height=18,;
    Caption="Stato ODF:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_PARAM='F' or .w_ELAODF<>'S')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="LSCCHYBDBU",Visible=.t., Left=28, Top=370,;
    Alignment=1, Width=143, Height=18,;
    Caption="Tipo di gestione DTF:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="VXOPVPCUQD",Visible=.t., Left=442, Top=371,;
    Alignment=1, Width=68, Height=18,;
    Caption="Giorni DTF:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdb_kgf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
