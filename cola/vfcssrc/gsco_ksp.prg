* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_ksp                                                        *
*              Import piano di produzione                                      *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-23                                                      *
* Last revis.: 2016-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_ksp",oParentObject))

* --- Class definition
define class tgsco_ksp as StdForm
  Top    = -2
  Left   = 5

  * --- Standard Properties
  Width  = 826
  Height = 460
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-13"
  HelpContextID=169237143
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  MAGAZZIN_IDX = 0
  ODL_MAST_IDX = 0
  cPrg = "gsco_ksp"
  cComment = "Import piano di produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SCCODRIS = space(20)
  w_SCTIPPIA = space(1)
  w_SCTIPSCA = space(3)
  w_SCDATINI = space(3)
  w_SCPIAINI = ctod('  /  /  ')
  w_QUERY = space(254)
  w_QUERYDETT = space(254)
  w_SELEZI = space(1)
  w_SERSCA = space(15)
  w_ROWDETT = space(10)
  w_SLQTARES = space(1)
  w_ZoomMast = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kspPag1","gsco_ksp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Lista scaletta")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='ODL_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SCCODRIS=space(20)
      .w_SCTIPPIA=space(1)
      .w_SCTIPSCA=space(3)
      .w_SCDATINI=space(3)
      .w_SCPIAINI=ctod("  /  /  ")
      .w_QUERY=space(254)
      .w_QUERYDETT=space(254)
      .w_SELEZI=space(1)
      .w_SERSCA=space(15)
      .w_ROWDETT=space(10)
      .w_SLQTARES=space(1)
      .w_SLQTARES=oParentObject.w_SLQTARES
        .w_SCCODRIS = this.oparentobject.w_SCCODRIS
        .w_SCTIPPIA = this.oparentobject.w_SCTIPPIA
        .w_SCTIPSCA = this.oparentobject.w_SCTIPSCA
        .w_SCDATINI = this.oparentobject.w_DATINI
        .w_SCPIAINI = this.oparentobject.w_DATINI
        .w_QUERY = IIF( .w_SCTIPSCA='CSQ' , '..\COLA\EXE\QUERY\GSCO_KSP', '..\PRFA\EXE\QUERY\GSCI_KSP')
        .w_QUERYDETT = IIF( .w_SCTIPSCA='CSQ' , '..\COLA\EXE\QUERY\GSCODKSP', '..\PRFA\EXE\QUERY\GSCIDKSP')
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .w_SELEZI = "D"
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_SERSCA = .w_ZoomMast.getVar('SCSERIAL')
        .w_ROWDETT = .w_ZoomDett.getVar('SCROWNUM')
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERSCA)
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_SLQTARES = "S"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SLQTARES=.w_SLQTARES
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_QUERY = IIF( .w_SCTIPSCA='CSQ' , '..\COLA\EXE\QUERY\GSCO_KSP', '..\PRFA\EXE\QUERY\GSCI_KSP')
            .w_QUERYDETT = IIF( .w_SCTIPSCA='CSQ' , '..\COLA\EXE\QUERY\GSCODKSP', '..\PRFA\EXE\QUERY\GSCIDKSP')
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .DoRTCalc(8,8,.t.)
            .w_SERSCA = .w_ZoomMast.getVar('SCSERIAL')
            .w_ROWDETT = .w_ZoomDett.getVar('SCROWNUM')
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERSCA)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERSCA)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return

  proc Calculate_VGSIDVLNBH()
    with this
          * --- 
          .w_ZoomMast.cCpQueryName = .w_QUERY
          .w_ZoomDett.cCpQueryName = .w_QUERYDETT
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_VGSIDVLNBH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_11.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTARES_1_33.RadioValue()==this.w_SLQTARES)
      this.oPgFrm.Page1.oPag.oSLQTARES_1_33.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco_kspPag1 as StdContainer
  Width  = 822
  height = 460
  stdWidth  = 822
  stdheight = 460
  resizeXpos=394
  resizeYpos=273
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_8 as StdButton with uid="DERMGDMCBN",left=709, top=410, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per importare le righe dei materiali";
    , HelpContextID = 8252394;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="CNWSZLWHAD",left=763, top=410, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare la selezione";
    , HelpContextID = 8252394;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomMast as cp_szoombox with uid="FNBDDSEHEU",left=-1, top=29, width=825,height=157,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="SCALETTM",cZoomFile="GSCO_KSP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 189636122

  add object oSELEZI_1_11 as StdRadio with uid="XBGVNIVMJH",rtseq=8,rtrep=.f.,left=15, top=398, width=139,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_11.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 117414618
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 117414618
      this.Buttons(2).Top=15
      this.SetAll("Width",137)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_11.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_11.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_11.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oObj_1_19 as cp_runprogram with uid="UCOVTBEFCP",left=259, top=530, width=246,height=19,;
    caption='GSCO_BIS("MRC")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("MRC")',;
    cEvent = "ZOOMMAST row checked",;
    nPag=1;
    , HelpContextID = 148952889


  add object oObj_1_20 as cp_runprogram with uid="MELIDWAVDO",left=259, top=476, width=246,height=19,;
    caption='GSCO_BIS("DBQ")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("DBQ")',;
    cEvent = "ZOOMDETT before query",;
    nPag=1;
    , HelpContextID = 162547513


  add object oObj_1_21 as cp_runprogram with uid="TQBLDXFFMS",left=259, top=494, width=246,height=19,;
    caption='GSCO_BIS("DAQ")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("DAQ")',;
    cEvent = "ZOOMDETT after query",;
    nPag=1;
    , HelpContextID = 162481977


  add object oObj_1_22 as cp_runprogram with uid="VCEUQAKZRM",left=1, top=495, width=246,height=19,;
    caption='GSCO_BIS("DRC")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("DRC")',;
    cEvent = "ZOOMDETT row checked",;
    nPag=1;
    , HelpContextID = 148916025


  add object oObj_1_24 as cp_runprogram with uid="KYQFHFRZIY",left=1, top=477, width=168,height=19,;
    caption='GSCO_BSF',;
   bGlobalFont=.t.,;
    prg="GSCO_BSF('BLANK')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 228990292


  add object ZoomDett as cp_szoombox with uid="DOCUKGPXAE",left=-1, top=200, width=825,height=194,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="SCALETTA",cZoomFile="GSCODKSP",bOptions=.f.,bAdvancedOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",bAdvOptions=.t.,cZoomOnZoom="",;
    cEvent = "CalcolaDett",;
    nPag=1;
    , HelpContextID = 189636122


  add object oObj_1_28 as cp_runprogram with uid="YDSPRCAHPJ",left=518, top=531, width=169,height=18,;
    caption='GSCO_BIS("UPS")',;
   bGlobalFont=.t.,;
    prg="GSCO_BIS('UPS')",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 165631801


  add object oObj_1_29 as cp_runprogram with uid="LVGMKMZWWB",left=259, top=512, width=246,height=19,;
    caption='GSCO_BIS("MRU")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("MRU")',;
    cEvent = "ZOOMMAST row unchecked",;
    nPag=1;
    , HelpContextID = 167827257


  add object oObj_1_30 as cp_runprogram with uid="JNJZCQLSBW",left=1, top=513, width=246,height=19,;
    caption='GSCO_BIS("SCH")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("SCH")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 153237305


  add object oObj_1_31 as cp_runprogram with uid="SMPDOYZTCB",left=1, top=530, width=246,height=19,;
    caption='GSCO_BIS("DRU")',;
   bGlobalFont=.t.,;
    prg='GSCO_BIS("DRU")',;
    cEvent = "ZOOMDETT row unchecked",;
    nPag=1;
    , HelpContextID = 167790393

  add object oSLQTARES_1_33 as StdRadio with uid="GBFPFFKKWK",rtseq=11,rtrep=.f.,left=454, top=417, width=205,height=34;
    , cFormVar="w_SLQTARES", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSLQTARES_1_33.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Residuo piano"
      this.Buttons(1).HelpContextID = 8371321
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Residuo ordine"
      this.Buttons(2).HelpContextID = 8371321
      this.Buttons(2).Top=16
      this.SetAll("Width",203)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSLQTARES_1_33.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"O",;
    space(1))))
  endfunc
  func oSLQTARES_1_33.GetRadio()
    this.Parent.oContained.w_SLQTARES = this.RadioValue()
    return .t.
  endfunc

  func oSLQTARES_1_33.SetRadio()
    this.Parent.oContained.w_SLQTARES=trim(this.Parent.oContained.w_SLQTARES)
    this.value = ;
      iif(this.Parent.oContained.w_SLQTARES=="S",1,;
      iif(this.Parent.oContained.w_SLQTARES=="O",2,;
      0))
  endfunc

  add object oStr_1_12 as StdString with uid="GHRXHKRNEC",Visible=.t., Left=654, Top=10,;
    Alignment=0, Width=38, Height=15,;
    Caption="(Blue)"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="CWZJEMIGKA",Visible=.t., Left=480, Top=10,;
    Alignment=0, Width=57, Height=15,;
    Caption="(Nero)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="MFEYEBHDXK",Visible=.t., Left=546, Top=10,;
    Alignment=0, Width=102, Height=15,;
    Caption="Non selezionata;"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="VQQDBKHDKE",Visible=.t., Left=698, Top=10,;
    Alignment=0, Width=77, Height=15,;
    Caption="Selezionata"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LXGYJPIPXB",Visible=.t., Left=382, Top=10,;
    Alignment=1, Width=91, Height=15,;
    Caption="Legenda righe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="BJDVDXKKSP",Visible=.t., Left=3, Top=17,;
    Alignment=0, Width=151, Height=18,;
    Caption="Elenco piani di produzione"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XZQODTUNOM",Visible=.t., Left=5, Top=188,;
    Alignment=0, Width=89, Height=15,;
    Caption="Dettaglio righe"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HADVXXCTXF",Visible=.t., Left=449, Top=397,;
    Alignment=0, Width=140, Height=16,;
    Caption="Opzioni di importazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_32 as StdBox with uid="WFNAOZJXBO",left=446, top=412, width=237,height=43
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_ksp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
