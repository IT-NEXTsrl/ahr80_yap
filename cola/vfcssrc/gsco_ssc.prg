* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_ssc                                                        *
*              Stampa piano di produzione                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-24                                                      *
* Last revis.: 2016-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_ssc",oParentObject))

* --- Class definition
define class tgsco_ssc as StdForm
  Top    = 13
  Left   = 110

  * --- Standard Properties
  Width  = 589
  Height = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-06-17"
  HelpContextID=90809705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  ODL_MAST_IDX = 0
  SCALETTM_IDX = 0
  RIS_ORSE_IDX = 0
  ART_ICOL_IDX = 0
  MAT_CRI_IDX = 0
  cPrg = "gsco_ssc"
  cComment = "Stampa piano di produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_punpadre = space(10)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_TIPO = space(3)
  w_SERIAL = space(15)
  o_SERIAL = space(15)
  w_SERIAL1 = space(15)
  w_SCCODRIS = space(20)
  o_SCCODRIS = space(20)
  w_SCCODRI1 = space(20)
  w_PARAM = space(1)
  w_SCTIPSCA = space(3)
  w_CLACRI = space(5)
  w_CLADESCRI = space(40)
  w_CLACRI1 = space(5)
  w_CLADESCRI1 = space(40)
  w_CLACRI2 = space(5)
  w_TIPOSTA = space(3)
  o_TIPOSTA = space(3)
  w_CLADESCRI2 = space(40)
  w_FLRIEPI = space(1)
  w_RL1_TIPO = space(2)
  w_RL__TIPO = space(2)
  w_RLDESCRI = space(40)
  w_RLDESCR1 = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_sscPag1","gsco_ssc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSERIAL_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='SCALETTM'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='MAT_CRI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_punpadre=space(10)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_TIPO=space(3)
      .w_SERIAL=space(15)
      .w_SERIAL1=space(15)
      .w_SCCODRIS=space(20)
      .w_SCCODRI1=space(20)
      .w_PARAM=space(1)
      .w_SCTIPSCA=space(3)
      .w_CLACRI=space(5)
      .w_CLADESCRI=space(40)
      .w_CLACRI1=space(5)
      .w_CLADESCRI1=space(40)
      .w_CLACRI2=space(5)
      .w_TIPOSTA=space(3)
      .w_CLADESCRI2=space(40)
      .w_FLRIEPI=space(1)
      .w_RL1_TIPO=space(2)
      .w_RL__TIPO=space(2)
      .w_RLDESCRI=space(40)
      .w_RLDESCR1=space(40)
        .w_punpadre = this.oParentObject
          .DoRTCalc(2,3,.f.)
        .w_TIPO = 'CLQ'
        .w_SERIAL = iif(type("this.w_punpadre")="O", .w_punpadre.w_SERIAL , ' ')
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_SERIAL))
          .link_1_6('Full')
        endif
        .w_SERIAL1 = iif(type("this.w_punpadre")="O", .w_punpadre.w_SERIAL1 , ' ')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SERIAL1))
          .link_1_7('Full')
        endif
        .w_SCCODRIS = ' '
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_SCCODRIS))
          .link_1_10('Full')
        endif
        .w_SCCODRI1 = .w_SCCODRIS
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SCCODRI1))
          .link_1_11('Full')
        endif
        .w_PARAM = ''
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CLACRI))
          .link_1_14('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_CLACRI1))
          .link_1_17('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_CLACRI2))
          .link_1_20('Full')
        endif
        .w_TIPOSTA = "SA3"
          .DoRTCalc(17,17,.f.)
        .w_FLRIEPI = "N"
    endwith
    this.DoRTCalc(19,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_SERIAL<>.w_SERIAL
            .w_SERIAL1 = iif(type("this.w_punpadre")="O", .w_punpadre.w_SERIAL1 , ' ')
          .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_SCCODRIS<>.w_SCCODRIS
            .w_SCCODRI1 = .w_SCCODRIS
          .link_1_11('Full')
        endif
        .DoRTCalc(9,17,.t.)
        if .o_TIPOSTA<>.w_TIPOSTA
            .w_FLRIEPI = "N"
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLRIEPI_1_24.enabled = this.oPgFrm.Page1.oPag.oFLRIEPI_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SERIAL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_lTable = "SCALETTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2], .t., this.SCALETTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SCALETTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCSERIAL like "+cp_ToStrODBC(trim(this.w_SERIAL)+"%");

          i_ret=cp_SQL(i_nConn,"select SCSERIAL,SCTIPSCA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCSERIAL',trim(this.w_SERIAL))
          select SCSERIAL,SCTIPSCA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERIAL)==trim(_Link_.SCSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERIAL) and !this.bDontReportError
            deferred_cp_zoom('SCALETTM','*','SCSERIAL',cp_AbsName(oSource.parent,'oSERIAL_1_6'),i_cWhere,'',"Piani di produzione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCSERIAL,SCTIPSCA";
                     +" from "+i_cTable+" "+i_lTable+" where SCSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCSERIAL',oSource.xKey(1))
            select SCSERIAL,SCTIPSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCSERIAL,SCTIPSCA";
                   +" from "+i_cTable+" "+i_lTable+" where SCSERIAL="+cp_ToStrODBC(this.w_SERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCSERIAL',this.w_SERIAL)
            select SCSERIAL,SCTIPSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIAL = NVL(_Link_.SCSERIAL,space(15))
      this.w_SCTIPSCA = NVL(_Link_.SCTIPSCA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SERIAL = space(15)
      endif
      this.w_SCTIPSCA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])+'\'+cp_ToStr(_Link_.SCSERIAL,1)
      cp_ShowWarn(i_cKey,this.SCALETTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERIAL1
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_lTable = "SCALETTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2], .t., this.SCALETTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIAL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SCALETTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCSERIAL like "+cp_ToStrODBC(trim(this.w_SERIAL1)+"%");

          i_ret=cp_SQL(i_nConn,"select SCSERIAL,SCTIPSCA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCSERIAL',trim(this.w_SERIAL1))
          select SCSERIAL,SCTIPSCA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERIAL1)==trim(_Link_.SCSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERIAL1) and !this.bDontReportError
            deferred_cp_zoom('SCALETTM','*','SCSERIAL',cp_AbsName(oSource.parent,'oSERIAL1_1_7'),i_cWhere,'',"Piani di produzione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCSERIAL,SCTIPSCA";
                     +" from "+i_cTable+" "+i_lTable+" where SCSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCSERIAL',oSource.xKey(1))
            select SCSERIAL,SCTIPSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIAL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCSERIAL,SCTIPSCA";
                   +" from "+i_cTable+" "+i_lTable+" where SCSERIAL="+cp_ToStrODBC(this.w_SERIAL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCSERIAL',this.w_SERIAL1)
            select SCSERIAL,SCTIPSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIAL1 = NVL(_Link_.SCSERIAL,space(15))
      this.w_SCTIPSCA = NVL(_Link_.SCTIPSCA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SERIAL1 = space(15)
      endif
      this.w_SCTIPSCA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])+'\'+cp_ToStr(_Link_.SCSERIAL,1)
      cp_ShowWarn(i_cKey,this.SCALETTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIAL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODRIS
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_SCCODRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RLCODICE',trim(this.w_SCCODRIS))
          select RLCODICE,RLDESCRI,RL__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODRIS)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODRIS) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RLCODICE',cp_AbsName(oSource.parent,'oSCCODRIS_1_10'),i_cWhere,'',"Risorse",'GSCI_ZLR.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',oSource.xKey(1))
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCCODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',this.w_SCCODRIS)
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODRIS = NVL(_Link_.RLCODICE,space(20))
      this.w_RLDESCRI = NVL(_Link_.RLDESCRI,space(40))
      this.w_RL__TIPO = NVL(_Link_.RL__TIPO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODRIS = space(20)
      endif
      this.w_RLDESCRI = space(40)
      this.w_RL__TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_SCCODRIS<=.w_SCCODRI1 or empty(.w_SCCODRI1)) AND .w_RL__TIPO $ 'CL-SQ-AT-MA'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        endif
        this.w_SCCODRIS = space(20)
        this.w_RLDESCRI = space(40)
        this.w_RL__TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODRI1
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODRI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_SCCODRI1)+"%");

          i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RLCODICE',trim(this.w_SCCODRI1))
          select RLCODICE,RLDESCRI,RL__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODRI1)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODRI1) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RLCODICE',cp_AbsName(oSource.parent,'oSCCODRI1_1_11'),i_cWhere,'',"Risorse",'GSCI_ZLR.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',oSource.xKey(1))
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODRI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCCODRI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',this.w_SCCODRI1)
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODRI1 = NVL(_Link_.RLCODICE,space(20))
      this.w_RLDESCR1 = NVL(_Link_.RLDESCRI,space(40))
      this.w_RL1_TIPO = NVL(_Link_.RL__TIPO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODRI1 = space(20)
      endif
      this.w_RLDESCR1 = space(40)
      this.w_RL1_TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_SCCODRIS<=.w_SCCODRI1 or empty(.w_SCCODRIS)) AND .w_RL1_TIPO $ 'CL-SQ-AT-MA'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        endif
        this.w_SCCODRI1 = space(20)
        this.w_RLDESCR1 = space(40)
        this.w_RL1_TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODRI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLACRI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAT_CRI_IDX,3]
    i_lTable = "MAT_CRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2], .t., this.MAT_CRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLACRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAT_CRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MTCODICE like "+cp_ToStrODBC(trim(this.w_CLACRI)+"%");

          i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MTCODICE',trim(this.w_CLACRI))
          select MTCODICE,MTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLACRI)==trim(_Link_.MTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLACRI) and !this.bDontReportError
            deferred_cp_zoom('MAT_CRI','*','MTCODICE',cp_AbsName(oSource.parent,'oCLACRI_1_14'),i_cWhere,'',"Classe materiali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',oSource.xKey(1))
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLACRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(this.w_CLACRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',this.w_CLACRI)
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLACRI = NVL(_Link_.MTCODICE,space(5))
      this.w_CLADESCRI = NVL(_Link_.MTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLACRI = space(5)
      endif
      this.w_CLADESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])+'\'+cp_ToStr(_Link_.MTCODICE,1)
      cp_ShowWarn(i_cKey,this.MAT_CRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLACRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLACRI1
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAT_CRI_IDX,3]
    i_lTable = "MAT_CRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2], .t., this.MAT_CRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLACRI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAT_CRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MTCODICE like "+cp_ToStrODBC(trim(this.w_CLACRI1)+"%");

          i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MTCODICE',trim(this.w_CLACRI1))
          select MTCODICE,MTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLACRI1)==trim(_Link_.MTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLACRI1) and !this.bDontReportError
            deferred_cp_zoom('MAT_CRI','*','MTCODICE',cp_AbsName(oSource.parent,'oCLACRI1_1_17'),i_cWhere,'',"Classe materiali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',oSource.xKey(1))
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLACRI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(this.w_CLACRI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',this.w_CLACRI1)
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLACRI1 = NVL(_Link_.MTCODICE,space(5))
      this.w_CLADESCRI1 = NVL(_Link_.MTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLACRI1 = space(5)
      endif
      this.w_CLADESCRI1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])+'\'+cp_ToStr(_Link_.MTCODICE,1)
      cp_ShowWarn(i_cKey,this.MAT_CRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLACRI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLACRI2
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAT_CRI_IDX,3]
    i_lTable = "MAT_CRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2], .t., this.MAT_CRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLACRI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAT_CRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MTCODICE like "+cp_ToStrODBC(trim(this.w_CLACRI2)+"%");

          i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MTCODICE',trim(this.w_CLACRI2))
          select MTCODICE,MTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLACRI2)==trim(_Link_.MTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLACRI2) and !this.bDontReportError
            deferred_cp_zoom('MAT_CRI','*','MTCODICE',cp_AbsName(oSource.parent,'oCLACRI2_1_20'),i_cWhere,'',"Classe materiali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',oSource.xKey(1))
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLACRI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(this.w_CLACRI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',this.w_CLACRI2)
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLACRI2 = NVL(_Link_.MTCODICE,space(5))
      this.w_CLADESCRI2 = NVL(_Link_.MTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLACRI2 = space(5)
      endif
      this.w_CLADESCRI2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])+'\'+cp_ToStr(_Link_.MTCODICE,1)
      cp_ShowWarn(i_cKey,this.MAT_CRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLACRI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSERIAL_1_6.value==this.w_SERIAL)
      this.oPgFrm.Page1.oPag.oSERIAL_1_6.value=this.w_SERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIAL1_1_7.value==this.w_SERIAL1)
      this.oPgFrm.Page1.oPag.oSERIAL1_1_7.value=this.w_SERIAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODRIS_1_10.value==this.w_SCCODRIS)
      this.oPgFrm.Page1.oPag.oSCCODRIS_1_10.value=this.w_SCCODRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODRI1_1_11.value==this.w_SCCODRI1)
      this.oPgFrm.Page1.oPag.oSCCODRI1_1_11.value=this.w_SCCODRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCLACRI_1_14.value==this.w_CLACRI)
      this.oPgFrm.Page1.oPag.oCLACRI_1_14.value=this.w_CLACRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADESCRI_1_15.value==this.w_CLADESCRI)
      this.oPgFrm.Page1.oPag.oCLADESCRI_1_15.value=this.w_CLADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLACRI1_1_17.value==this.w_CLACRI1)
      this.oPgFrm.Page1.oPag.oCLACRI1_1_17.value=this.w_CLACRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADESCRI1_1_18.value==this.w_CLADESCRI1)
      this.oPgFrm.Page1.oPag.oCLADESCRI1_1_18.value=this.w_CLADESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCLACRI2_1_20.value==this.w_CLACRI2)
      this.oPgFrm.Page1.oPag.oCLACRI2_1_20.value=this.w_CLACRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTA_1_21.RadioValue()==this.w_TIPOSTA)
      this.oPgFrm.Page1.oPag.oTIPOSTA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADESCRI2_1_22.value==this.w_CLADESCRI2)
      this.oPgFrm.Page1.oPag.oCLADESCRI2_1_22.value=this.w_CLADESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRIEPI_1_24.RadioValue()==this.w_FLRIEPI)
      this.oPgFrm.Page1.oPag.oFLRIEPI_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_31.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_31.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCR1_1_32.value==this.w_RLDESCR1)
      this.oPgFrm.Page1.oPag.oRLDESCR1_1_32.value=this.w_RLDESCR1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_SCCODRIS<=.w_SCCODRI1 or empty(.w_SCCODRI1)) AND .w_RL__TIPO $ 'CL-SQ-AT-MA')  and not(empty(.w_SCCODRIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODRIS_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
          case   not((.w_SCCODRIS<=.w_SCCODRI1 or empty(.w_SCCODRIS)) AND .w_RL1_TIPO $ 'CL-SQ-AT-MA')  and not(empty(.w_SCCODRI1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODRI1_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SERIAL = this.w_SERIAL
    this.o_SCCODRIS = this.w_SCCODRIS
    this.o_TIPOSTA = this.w_TIPOSTA
    return

enddefine

* --- Define pages as container
define class tgsco_sscPag1 as StdContainer
  Width  = 585
  height = 256
  stdWidth  = 585
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSERIAL_1_6 as StdField with uid="BPIQAORFNW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SERIAL", cQueryName = "SERIAL",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Scaletta",;
    HelpContextID = 84622042,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=110, Top=15, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SCALETTM", oKey_1_1="SCSERIAL", oKey_1_2="this.w_SERIAL"

  func oSERIAL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERIAL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERIAL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SCALETTM','*','SCSERIAL',cp_AbsName(this.parent,'oSERIAL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Piani di produzione",'',this.parent.oContained
  endproc

  add object oSERIAL1_1_7 as StdField with uid="ILEROOORMB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SERIAL1", cQueryName = "SERIAL1",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Scaletta",;
    HelpContextID = 183813414,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=110, Top=37, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SCALETTM", oKey_1_1="SCSERIAL", oKey_1_2="this.w_SERIAL1"

  func oSERIAL1_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERIAL1_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERIAL1_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SCALETTM','*','SCSERIAL',cp_AbsName(this.parent,'oSERIAL1_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Piani di produzione",'',this.parent.oContained
  endproc

  add object oSCCODRIS_1_10 as StdField with uid="ZXAKOQMVQY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SCCODRIS", cQueryName = "SCCODRIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inestente.",;
    ToolTipText = "Codice risorsa",;
    HelpContextID = 248917127,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=110, Top=62, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RLCODICE", oKey_1_2="this.w_SCCODRIS"

  func oSCCODRIS_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODRIS_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODRIS_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RIS_ORSE','*','RLCODICE',cp_AbsName(this.parent,'oSCCODRIS_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCI_ZLR.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oSCCODRI1_1_11 as StdField with uid="QBJVVUDABI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SCCODRI1", cQueryName = "SCCODRI1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inestente.",;
    ToolTipText = "Codice risorsa",;
    HelpContextID = 248917161,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=110, Top=85, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RLCODICE", oKey_1_2="this.w_SCCODRI1"

  func oSCCODRI1_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODRI1_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODRI1_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RIS_ORSE','*','RLCODICE',cp_AbsName(this.parent,'oSCCODRI1_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCI_ZLR.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oCLACRI_1_14 as StdField with uid="UKCFHAKFUV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLACRI", cQueryName = "CLACRI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe materiale critico",;
    HelpContextID = 117589210,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=110, Top=111, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAT_CRI", oKey_1_1="MTCODICE", oKey_1_2="this.w_CLACRI"

  func oCLACRI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLACRI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLACRI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAT_CRI','*','MTCODICE',cp_AbsName(this.parent,'oCLACRI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classe materiali",'',this.parent.oContained
  endproc

  add object oCLADESCRI_1_15 as StdField with uid="DVZTFQAMSM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CLADESCRI", cQueryName = "CLADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36618248,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=265, Top=111, InputMask=replicate('X',40)

  add object oCLACRI1_1_17 as StdField with uid="VMFFVHRCXZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CLACRI1", cQueryName = "CLACRI1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe materiale critico",;
    HelpContextID = 150846246,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=110, Top=133, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAT_CRI", oKey_1_1="MTCODICE", oKey_1_2="this.w_CLACRI1"

  func oCLACRI1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLACRI1_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLACRI1_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAT_CRI','*','MTCODICE',cp_AbsName(this.parent,'oCLACRI1_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classe materiali",'',this.parent.oContained
  endproc

  add object oCLADESCRI1_1_18 as StdField with uid="YRXFENBKFA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CLADESCRI1", cQueryName = "CLADESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36630792,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=265, Top=133, InputMask=replicate('X',40)

  add object oCLACRI2_1_20 as StdField with uid="ZDFUQBAXUX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CLACRI2", cQueryName = "CLACRI2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe materiale critico",;
    HelpContextID = 150846246,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=110, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAT_CRI", oKey_1_1="MTCODICE", oKey_1_2="this.w_CLACRI2"

  func oCLACRI2_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLACRI2_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLACRI2_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAT_CRI','*','MTCODICE',cp_AbsName(this.parent,'oCLACRI2_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classe materiali",'',this.parent.oContained
  endproc


  add object oTIPOSTA_1_21 as StdCombo with uid="PJFTKXJAUT",rtseq=16,rtrep=.f.,left=110,top=185,width=225,height=21;
    , ToolTipText = "Tipo di stampa";
    , HelpContextID = 68856118;
    , cFormVar="w_TIPOSTA",RowSource=""+"Stampa piano in formato A3,"+"Stampa piano in formato A4,"+"Stampa gantt in formato A4", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTA_1_21.RadioValue()
    return(iif(this.value =1,"SA3",;
    iif(this.value =2,"SA4",;
    iif(this.value =3,"GA4",;
    space(3)))))
  endfunc
  func oTIPOSTA_1_21.GetRadio()
    this.Parent.oContained.w_TIPOSTA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTA_1_21.SetRadio()
    this.Parent.oContained.w_TIPOSTA=trim(this.Parent.oContained.w_TIPOSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTA=="SA3",1,;
      iif(this.Parent.oContained.w_TIPOSTA=="SA4",2,;
      iif(this.Parent.oContained.w_TIPOSTA=="GA4",3,;
      0)))
  endfunc

  add object oCLADESCRI2_1_22 as StdField with uid="CAMOSSCSER",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CLADESCRI2", cQueryName = "CLADESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36631048,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=265, Top=155, InputMask=replicate('X',40)

  add object oFLRIEPI_1_24 as StdCheck with uid="LOWCZIXLII",rtseq=18,rtrep=.f.,left=110, top=211, caption="Stampa riepilogo consumo periodo",;
    ToolTipText = "Stampa il riepilogo del consumo di periodo",;
    HelpContextID = 13317290,;
    cFormVar="w_FLRIEPI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRIEPI_1_24.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLRIEPI_1_24.GetRadio()
    this.Parent.oContained.w_FLRIEPI = this.RadioValue()
    return .t.
  endfunc

  func oFLRIEPI_1_24.SetRadio()
    this.Parent.oContained.w_FLRIEPI=trim(this.Parent.oContained.w_FLRIEPI)
    this.value = ;
      iif(this.Parent.oContained.w_FLRIEPI=="S",1,;
      0)
  endfunc

  func oFLRIEPI_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_TIPOSTA) and .w_TIPOSTA<>'GA4')
    endwith
   endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="QXASNQRVDV",left=465, top=201, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 136214;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        do GSCO_BSS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_TIPOSTA))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="LHZBFLWXLA",left=515, top=201, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 136214;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRLDESCRI_1_31 as StdField with uid="WVKCUZVJKH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51375199,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=280, Top=62, InputMask=replicate('X',40)

  add object oRLDESCR1_1_32 as StdField with uid="LBTNQHSSJT",rtseq=22,rtrep=.f.,;
    cFormVar = "w_RLDESCR1", cQueryName = "RLDESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51375175,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=280, Top=85, InputMask=replicate('X',40)

  add object oStr_1_2 as StdString with uid="RZWEJVDCHV",Visible=.t., Left=12, Top=187,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YDOVYKLNDS",Visible=.t., Left=48, Top=40,;
    Alignment=1, Width=61, Height=18,;
    Caption="A piano:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WTLDUNDJFJ",Visible=.t., Left=43, Top=18,;
    Alignment=1, Width=66, Height=18,;
    Caption="Da piano:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="GNRWOFULMO",Visible=.t., Left=14, Top=114,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classe criticit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="DWGYHFNRZH",Visible=.t., Left=14, Top=136,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classe criticit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="FJHPFEYNYD",Visible=.t., Left=14, Top=158,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classe criticit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BMMABXHRUQ",Visible=.t., Left=32, Top=88,;
    Alignment=1, Width=77, Height=18,;
    Caption="A risorsa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="NXBOBQUKAN",Visible=.t., Left=19, Top=66,;
    Alignment=1, Width=90, Height=18,;
    Caption="Da risorsa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_ssc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
