* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bpg                                                        *
*              Pegging 2 livello                                               *
*                                                                              *
*      Author: ZUCCHETTI TAM SPA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-11                                                      *
* Last revis.: 2017-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bpg",oParentObject)
return(i_retval)

define class tgsmr_bpg as StdBatch
  * --- Local variables
  w_oPEGLOG = .NULL.
  w_Recno = 0
  w_RecnoImp = 0
  w_RecnoOrd = 0
  w_QTA = 0
  w_QTAIMP = 0
  w_QTAORD = 0
  w_QTAABB = 0
  w_SaldoIMP = 0
  w_SaldoORD = 0
  w_PROGR = 0
  w_ODLIMP = space(15)
  w_ODLORD = space(15)
  w_RIGIMP = 0
  w_RIGORD = 0
  w_PESERIAL = space(10)
  w_DatIMP = space(8)
  w_DatORD = space(8)
  w_USELAB = .f.
  w_QTATRS = 0
  w_NRECCUR = 0
  w_NRECELAB = 0
  w_CODRIC = space(41)
  w_MAXRND = 0
  w_RND = 0
  w_RIF = space(1)
  w_CODCOM = space(15)
  w_DATIDX = space(8)
  w_OLDCOM1 = space(1)
  w_SaldoAPP = 0
  w_DATAIMP = ctod("  /  /  ")
  w_PPORDICM = space(1)
  w_KEYSAL = space(40)
  w_LogTitle = space(10)
  w_LenCodSal = 0
  w_Res = 0
  w_FileName = space(200)
  f_Error = 0
  w_PATHMRP = space(200)
  w_CurDir = space(200)
  w_cTableMRP = space(200)
  w_PathFound = .f.
  w_FL = space(10)
  sp_FILELOG = space(10)
  hf_FILE_LOG = 0
  w_TEMP = space(10)
  TmpC = space(100)
  w_LogFileName = space(10)
  w_cMsg = space(0)
  qtaabb = 0
  riga = 0
  w_Fabnet = 0
  * --- WorkFile variables
  PEG_SELI_idx=0
  PAR_PROD_idx=0
  TMPPEG_SELI_idx=0
  TMPPEG2SELI_idx=0
  TMPPEG3SELI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora il Pegging di secondo livello a seguito dell'elaborazione MRP (da GSMR_BGP)
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPORDICM"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPORDICM;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPORDICM = NVL(cp_ToDate(_read_.PPORDICM),cp_NullValue(_read_.PPORDICM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Inizializzazione variabile usata per il controllo del loop
    this.w_MAXRND = 100
    this.w_USELAB = used("Elabor")
    if not this.w_USELAB
      * --- Test path MRP
      if ! this.oParentObject.w_PEGGING
        * --- Azzero il Contenuto Dell'Array
        Declare ArrPath[3]
        ArrPath[1]="" 
 ArrPath[2]=0 
 ArrPath[3]=""
        this.w_Res = GetDirElab("R", @ArrPath,this.oParentObject.w_SERIALE)
        this.w_FileName = alltrim(ArrPath[1])
        this.f_Error = ArrPath[2]
        do case
          case this.f_Error = 0
            use (this.w_FileName) in 0 alias Elabor
          case this.f_Error = 1
            ah_ErrorMsg("Impossibile trovare il percorso:%0%1%0Impossibile aggiornare il pegging",16,"",this.w_FileName)
            i_retcode = 'stop'
            return
          case this.f_Error = 2
            ah_ErrorMsg("File: %1 non trovato%0Verificare che il file esista e riprovare%0Impossibile aggiornare il pegging",16,"",this.w_FileName)
            i_retcode = 'stop'
            return
        endcase
      else
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPATHMRP"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPATHMRP;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PATHMRP = NVL(cp_ToDate(_read_.PPATHMRP),cp_NullValue(_read_.PPATHMRP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Percorso Corrente (punta alla exe di AHE)
        this.w_CurDir = sys(5)+sys(2003)+"\"
        * --- Nome del File di Elaborazione MRP
        this.w_FileName = "elab"+alltrim(i_codazi) + alltrim(this.oParentObject.w_SERIALE) + ".dbf"
        this.w_PATHMRP = nvl(this.w_PATHMRP,"")
        if !empty(this.w_PATHMRP)
          this.w_PathFound = True
          * --- Per evitare errori Recupero FULLPATH()
          this.w_PATHMRP = FULLPATH(alltrim(nvl(this.w_PATHMRP,"")))
          * --- Se non c'� aggiungo Slash finale
          this.w_PATHMRP = this.w_PATHMRP + iif(right(this.w_PATHMRP,1)="\","","\")
          * --- Verifico che la Directory Esista
          this.w_PathFound = DIRECTORY(this.w_PATHMRP)
          if !this.w_PathFound
            * --- In questo caso ho un errore
            * --- In questo caso ho un errore
            ah_ErrorMsg("Impossibile trovare il percorso:%0%1%0Impossibile aggiornare il pegging",16,"",this.w_FileName)
            i_retcode = 'stop'
            return
          endif
          this.w_cTableMRP = this.w_PATHMRP + this.w_FileName
        else
          this.w_cTableMRP = this.w_FileName
        endif
        if !file(this.w_cTableMRP)
          * --- In questo caso ho un errore
          ah_ErrorMsg("File: %1 non trovato%0Verificare che il file esista e riprovare%0Impossibile aggiornare il pegging",16,"",this.w_FileName)
          i_retcode = 'stop'
          return
        endif
        this.w_FileName = this.w_cTableMRP
        use (this.w_FileName) in 0 alias Elabor
      endif
    endif
    * --- Eventuale creazione di un file di log
    if this.oParentObject.w_MRPLOG="S"
      * --- CURSORE PER I MESSAGGI
      this.w_LogTitle = space(10)+"Log elaborazione Pegging %2 %3"
      this.w_oPEGLOG=createobject("AH_ERRORLOG")
      this.w_oPEGLOG.ADDMSGLOG(this.w_LogTitle, g_APPLICATION, g_VERSION)     
      this.w_oPEGLOG.ADDMSGLOG("================================================================================")     
      this.w_oPEGLOG.ADDMSGLOG("Inizio elaborazione: [%1]", ALLTRIM(TTOC(DATETIME())))     
    endif
    if Used("Elabor")
      Select Elabor
      this.w_LenCodSal = len(codsal)
      Select Elabor
      go top
      if g_UseProgBar
        this.w_NRECCUR = 0
        this.w_NRECELAB = reccount()
        if !this.oParentObject.w_OLDCOM
          this.w_NRECELAB = this.w_NRECELAB * 3
        else
          this.w_NRECELAB = this.w_NRECELAB * 2
        endif
        GesProgBar("M", this, 0, 0, "Aggiornamento Pegging in corso...")
      else
        this.w_NRECCUR = 0
        this.w_NRECELAB = reccount()
        ah_msg("Aggiornamento pegging")
      endif
      * --- Try
      local bErr_058A32B8
      bErr_058A32B8=bTrsErr
      this.Try_058A32B8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'aggiornamento del pegging",48)
      endif
      bTrsErr=bTrsErr or bErr_058A32B8
      * --- End
    else
      ah_ErrorMsg("Impossibile aggiornare il pegging",16)
    endif
    USE IN SELECT("Elabor")
    USE IN SELECT("SalComm")
    USE IN SELECT("PEGSUG")
    USE IN SELECT("ODLSUG")
    USE IN SELECT("SalPCom")
    if this.oParentObject.w_MRPLOG="S"
      this.w_oPEGLOG.ADDMSGLOG("Fine elaborazione: [%1]", ALLTRIM(TTOC(DATETIME())))     
      if this.w_oPEGLOG.ISFULLLOG()
        this.w_LogFileName = LEFT(this.w_FileName, LEN(this.w_filename)-4)+"_PEG"+".LOG"
        l_ErrCurs = this.w_oPEGLOG.cCursMessErr
        SELECT(l_ErrCurs) 
 GO TOP
        do while not eof()
          this.w_cMsg = this.w_cMsg + chr(13)+chr(10) + &l_ErrCurs..Msg
          SELECT (l_ErrCurs)
          skip +1
        enddo
        this.hf_FILE_LOG = fcreate(this.w_LogFileName)
        if this.hf_FILE_LOG>=0
          * --- Scrive Log
          this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG, this.w_cMsg)
          =fCLOSE(this.hf_FILE_LOG)
        endif
      endif
    endif
  endproc
  proc Try_058A32B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Pulisce precedente elaborazione
    this.w_OLDCOM1 = iif(this.oParentObject.w_OLDCOM,"S","N")
    if (this.oParentObject.w_GENPODA="O" or g_MODA<>"S") and this.oParentObject.w_SELEZ<>"S"
      * --- Se elaboro le ODA oppure se non le gestisco e non ho filtri di selezione elimino tutto
      * --- Delete from PEG_SELI
      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PESERIAL = "+i_cQueryTable+".PESERIAL";
      
        do vq_exec with 'gsmr_bpg',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Se non elaboro le ODA e le gestisco oppure ho dei filtri non devo cancellare il pegging relativo agli articoli non elaborati
      * --- Create temporary table TMPPEG_SELI
      i_nIdx=cp_AddTableDef('TMPPEG_SELI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_HWJJADUGVB[1]
      indexes_HWJJADUGVB[1]='PGSERODL'
      vq_exec('gsmr2bpg',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_HWJJADUGVB,.f.)
      this.TMPPEG_SELI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Se il DB � Oracle non posso usare come filtro l'espressione costruita che invece SQL Server accetta, faccio 2 cancellazioni separate perch� risparmio 3 query e quindi anche molto tempo
      if CP_DBTYPE="Oracle"
        * --- Delete from PEG_SELI
        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".PESERIAL = "+i_cQueryTable+".PESERIAL";
        
          do vq_exec with 'gsmr1bpg1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Preparo la tabella temporanea con i dati corretti
        * --- Create temporary table TMPPEG2SELI
        i_nIdx=cp_AddTableDef('TMPPEG2SELI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_ZQQJWNPIDM[1]
        indexes_ZQQJWNPIDM[1]='PESERORD1,PERIFODL,PESERODL'
        vq_exec('..\COLA\EXE\QUERY\GSMR1BPG2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ZQQJWNPIDM,.f.)
        this.TMPPEG2SELI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Create temporary table TMPPEG3SELI
        i_nIdx=cp_AddTableDef('TMPPEG3SELI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_HYHNZKABDW[1]
        indexes_HYHNZKABDW[1]='OLCODODL'
        vq_exec('GSMR4BPG',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_HYHNZKABDW,.f.)
        this.TMPPEG3SELI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Delete from PEG_SELI
        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".PESERIAL = "+i_cQueryTable+".PESERIAL";
        
          do vq_exec with 'gsmr1bpg3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Drop temporary table TMPPEG_SELI
      i_nIdx=cp_GetTableDefIdx('TMPPEG_SELI')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPEG_SELI')
      endif
      * --- Drop temporary table TMPPEG2SELI
      i_nIdx=cp_GetTableDefIdx('TMPPEG2SELI')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPEG2SELI')
      endif
      * --- Drop temporary table TMPPEG3SELI
      i_nIdx=cp_GetTableDefIdx('TMPPEG3SELI')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPEG3SELI')
      endif
    endif
    if this.oParentObject.w_OLDCOM
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if (this.oParentObject.w_GENPODA="O" or g_MODA<>"S") and this.oParentObject.w_SELEZ<>"S"
      this.w_PROGR = 0
    else
      if this.oParentObject.w_OLDCOM
        * --- Select from GSDBNBPG
        do vq_exec with 'GSDBNBPG',this,'_Curs_GSDBNBPG','',.f.,.t.
        if used('_Curs_GSDBNBPG')
          select _Curs_GSDBNBPG
          locate for 1=1
          do while not(eof())
          this.w_PESERIAL = _Curs_GSDBNBPG.PESERIAL
            select _Curs_GSDBNBPG
            continue
          enddo
          use
        endif
      else
        * --- Select from GSDBMBPG
        do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
        if used('_Curs_GSDBMBPG')
          select _Curs_GSDBMBPG
          locate for 1=1
          do while not(eof())
          this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
            select _Curs_GSDBMBPG
            continue
          enddo
          use
        endif
      endif
      if used("PEGSUG") and this.oParentObject.w_OLDCOM
        if Reccount("PEGSUG")>0
          * --- Potrebbe accadere che c'� il cursore in uso ma senza dati in questo caso azzererebbe la variabile dando errore di chiave
          CALCULATE MAX(peserial) TO maxserial IN PEGSUG
          if val(nvl(maxserial,"0000000001"))>val(this.w_PESERIAL)
            this.w_PESERIAL = nvl(maxserial,"0000000001")
          endif
        endif
      endif
      this.w_PROGR = MAX(0,ABS(VAL(nvl(this.w_PESERIAL,"0"))))
    endif
    if ! this.oParentObject.w_OLDCOM
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !g_UseProgBar
      ah_msg("Aggiornamento pegging")
    endif
    Select Elabor
    go top
    this.w_CODRIC = space(41)
    this.w_KEYSAL = Space(this.w_LenCodSal) && Space(Len(CodSal))
    do while not eof()
      if !g_UseProgBar
        if MOD(this.w_Recno, 1000)=0
          lnResult = 0
          lnResult = IsHungAppWindow(_VFP.hWnd)
          if lnResult > 0
            wait wind "" timeout .001
          endif
        endif
      endif
      if artipart<>"PS" and (arflcomm<>"S" or (empty(nvl(codcom,"")) and this.oParentObject.w_OLDCOM))
        this.w_Recno = RecNo()
        if codsal<>this.w_KEYSAL
          * --- Record riferito ad un nuovo articolo
          *     Azzero le variabili
          this.w_RecnoImp = this.w_Recno
          this.w_QTAIMP = 0
          this.w_SaldoIMP = this.w_QTAIMP
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = 0
          this.w_SaldoORD = this.w_QTAORD
          this.w_CODRIC = codric
          this.w_KEYSAL = CODSAL
        endif
        if tiprec="0"
          * --- Record riferito ad un nuovo articolo
          if qtalor>0
            * --- In precedenza era
            *     qtalor>0 or prscomin=0
            *     perch� avendo una giacenza negativa (qtalor<0 )e gestendo la scorta minima (prscomin>0) 
            *     si sarebbe dovuto calcolare l'impegno aggiungendo la giacenza negativa alla scorta di 
            *     sicurezza, considerando altrimenti la giacenza negativa come un normalissimo impegno.
            this.w_QTA = qtalor
            this.w_QTATRS = 0
            if this.oParentObject.w_OLDCOM and used("SalComm")
              if arflcomm="S" and empty(nvl(codcom,""))
                * --- Potrei avere una quantit� disponibile (contenuta in qtalor) gi� abbinata con un elemento gestito a commessa (codcom non vuoto),
                *     per evitare di abbinare due volte la medesima quantit� riverifico w_QTA
                * --- Q.t� totale abbinata
                select SalComm 
 sum peqtaori for oltcodic=Elabor.Codric and petiprif="M" to qtaori
                Select Elabor
                this.w_QTA = iif(this.w_QTA>0,max(this.w_QTA-qtaori,0),this.w_QTA)
              endif
            endif
          else
            this.w_QTA = 0
            this.w_QTATRS = qtalor
          endif
          this.w_RecnoImp = this.w_Recno
          this.w_QTAIMP = max(-this.w_QTA,0)
          this.w_SaldoIMP = this.w_QTAIMP
          this.w_ODLIMP = iif(this.w_QTA<0, "Magazzino", "")
          this.w_RIGIMP = 0
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = max(this.w_QTA,0)
          this.w_SaldoORD = this.w_QTAORD
          this.w_ODLORD = iif(this.w_QTA>0, "Magazzino", "")
          this.w_RIGORD = 0
        else
          this.w_QTA = iif(tiprec="7SCO-S",-prscomin+this.w_QTATRS,quanti)
          if tiprec="7SCO-S" AND this.w_QTA<0 AND this.w_SaldoIMP=0
            this.w_ODLIMP = "Magazzino"
          endif
          this.w_SaldoIMP = this.w_SaldoIMP + max(-this.w_QTA,0)
          if this.w_QTAIMP=0
            this.w_RecnoImp = this.w_Recno
            this.w_QTAIMP = max(-this.w_QTA,0)
            if this.w_ODLIMP<>"Magazzino" or ! empty (nvl(padrered," "))
              this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
            endif
            this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
          endif
          this.w_SaldoORD = this.w_SaldoORD + max(this.w_QTA,0)
          if this.w_QTAORD=0
            this.w_RecnoOrd = this.w_Recno
            this.w_QTAORD = max(this.w_QTA,0)
            this.w_ODLORD = iif(tiprec="4ORDFO",iif(arpropre="E",iif(len(alltrim(odamsg))<>15,padrered,odamsg),iif(len(alltrim(odlmsg))<>15,padrered,odlmsg)),padrered)
            this.w_RIGORD = perownum
          endif
        endif
        * --- Inizializzo contatore per verifica loop
        this.w_RND = 0
        do while this.w_SaldoORD>0 and this.w_SaldoIMP>0
          * --- Incremento contatore per verifica loop
          this.w_RND = this.w_RND+1
          * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
          this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
          if not empty(this.w_ODLORD)
            this.w_PROGR = max(this.w_PROGR + 1,1)
            if this.oParentObject.w_OLDCOM
              this.w_PESERIAL = "-"+right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),9)
            else
              this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
            endif
            * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
            this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
            do case
              case this.w_ODLIMP="Magazzino"
                * --- Abbinamento tra ODL e sottoscorta a magazzino
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"M";
                       ,this.w_ODLIMP;
                       ,this.w_QTAABB;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case len(alltrim(this.w_ODLIMP))=15
                * --- Abbinamento tra ODL e impegno da ODL
                if this.w_ODLORD<>this.w_ODLIMP
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODRIC',this.w_CODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_ODLORD;
                         ,"O";
                         ,this.w_ODLIMP;
                         ,this.w_RIGIMP;
                         ,this.w_QTAABB;
                         ,this.w_ODLORD;
                         ,this.w_CODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              case len(alltrim(this.w_ODLIMP))=10
                * --- Abbinamento tra ODL e impegno da documento
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"D";
                       ,left(this.w_ODLIMP,10);
                       ,this.w_RIGIMP;
                       ,this.w_QTAABB;
                       ,this.w_ODLORD;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
            endcase
          endif
          this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
          this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
          this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
          this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
          go this.w_RecnoImp
          if this.w_QTAIMP=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti>=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti<0
              this.w_QTAIMP = iif(tiprec="7SCO-S",prscomin-this.w_QTATRS,-quanti)
              this.w_RecnoImp = Recno()
              this.w_ODLIMP = iif(tiprec="7SCO-S","Magazzino",iif(not empty(nvl(oltseodl," ")),oltseodl,padrered))
              this.w_RIGIMP = iif(tiprec="7SCO-S","Magazzino",iif(not empty(nvl(oltseodl," ")),olrownum,perownum))
            endif
          endif
          go this.w_RecnoOrd
          if this.w_QTAORD=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti<=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti>0
              this.w_QTAORD = quanti
              this.w_RecnoOrd = Recno()
              this.w_ODLORD = iif(tiprec="4ORDFO",iif(arpropre="E",iif(len(alltrim(odamsg))<>15,padrered,odamsg),iif(len(alltrim(odlmsg))<>15,padrered,odlmsg)),padrered)
              this.w_RIGORD = perownum
            endif
          endif
          if this.w_RND=this.w_MAXRND
            * --- Ci potrebbe essere un loop, azzero la q.t� e proseguo
            this.w_SaldoORD = 0
            if this.oParentObject.w_MRPLOG="S"
              this.w_oPEGLOG.ADDMSGLOG(">Si � verificato un loop sul pegging, verificare codice padre %1 codice figlio %2 ", alltrim(this.w_ODLIMP) , alltrim(this.w_ODLORD))     
            endif
          endif
        enddo
        go this.w_Recno
        if not eof()
          skip
        endif
      else
        skip
      endif
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        if MOD(this.w_Recno,1*iif(this.w_NRECELAB>1000,10,1)*iif(this.w_NRECELAB>10000,50,1))=0
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
        endif
      else
        if MOD(this.w_Recno,1*iif(this.w_NRECELAB>1000,10,1)*iif(this.w_NRECELAB>10000,50,1))=0
          cp_msg("Aggiornamento pegging...." + alltrim(str(this.w_Recno))+" / "+alltrim(str(this.w_NRECELAB)))
        endif
      endif
    enddo
    if this.oParentObject.w_OLDCOM
      if used("SalComm")
        * --- Rimetto a posto il magazzino della commessa
        select SalComm 
 scan for petiprif="M"
        this.w_PESERIAL = SalComm.PESERIAL
        this.w_QTAABB = SalComm.PEQTAABB
        activerec=recno()
        * --- La vecchia riga con petiprif='M' deve essere eliminata se qtaabb � a 0 oppure � una quantit� abbinata ad un documento evaso, 
        *     in caso contrario deve semplicemente essere aggiornata.
        if this.w_QTAABB<=0 or not empty(nvl(peserord," "))
          * --- Delete from PEG_SELI
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PESERIAL = this.w_PESERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        else
          * --- Write into PEG_SELI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
            +",PERIFODL ="+cp_NullLink(cp_ToStrODBC(.null.),'PEG_SELI','PERIFODL');
            +",PESERORD ="+cp_NullLink(cp_ToStrODBC(.null.),'PEG_SELI','PESERORD');
            +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PERIGORD');
                +i_ccchkf ;
            +" where ";
                +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                   )
          else
            update (i_cTable) set;
                PEQTAABB = this.w_QTAABB;
                ,PERIFODL = .null.;
                ,PESERORD = .null.;
                ,PERIGORD = 0;
                &i_ccchkf. ;
             where;
                PESERIAL = this.w_PESERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        select SalComm 
 go activerec
        endscan
      endif
      if used("PEGSUG")
        * --- Rimetto a posto il magazzino della commessa
        select PEGSUG 
 scan
        this.w_PESERIAL = PEGSUG.PESERIAL
        this.w_RIF = PEGSUG.RIF
        activerec=recno()
        if this.w_RIF="X"
          * --- Write into PEG_SELI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PERIFODL ="+cp_NullLink(cp_ToStrODBC("AP"),'PEG_SELI','PERIFODL');
                +i_ccchkf ;
            +" where ";
                +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                   )
          else
            update (i_cTable) set;
                PERIFODL = "AP";
                &i_ccchkf. ;
             where;
                PESERIAL = this.w_PESERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into PEG_SELI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PESERODL ="+cp_NullLink(cp_ToStrODBC("AP"),'PEG_SELI','PESERODL');
                +i_ccchkf ;
            +" where ";
                +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                   )
          else
            update (i_cTable) set;
                PESERODL = "AP";
                &i_ccchkf. ;
             where;
                PESERIAL = this.w_PESERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        select PEGSUG 
 go activerec
        endscan
        if (this.oParentObject.w_ELACAT<>"S" and this.oParentObject.w_SELEZ="S") or (this.oParentObject.w_GENPODA<>"O" and g_MODA="S")
          vq_exec("..\COLA\EXE\QUERY\GSMR45BGP", this, "ODLSUG")
          select ODLSUG 
 scan
          this.w_PROGR = max(this.w_PROGR + 1,1)
          if this.oParentObject.w_OLDCOM
            this.w_PESERIAL = "-"+right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),9)
          else
            * --- Qui non dovrebbe mai entrare...
            this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
          endif
          this.w_ODLIMP = ODLSUG.OLCODODL
          this.w_RIGIMP = ODLSUG.CPROWNUM
          this.w_CODRIC = ODLSUG.OLCODICE
          * --- Insert into PEG_SELI
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODRIC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
            +","+cp_NullLink(cp_ToStrODBC("AL"),'PEG_SELI','PESERODL');
            +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
            +","+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PEQTAABB');
            +","+cp_NullLink(cp_ToStrODBC("AL"),'PEG_SELI','PEODLORI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',"AL",'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',0,'PEODLORI',"AL",'PECODRIC',this.w_CODRIC)
            insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODRIC &i_ccchkf. );
               values (;
                 this.w_PESERIAL;
                 ,"AL";
                 ,"O";
                 ,this.w_ODLIMP;
                 ,this.w_RIGIMP;
                 ,0;
                 ,"AL";
                 ,this.w_CODRIC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          endscan
        endif
      endif
    endif
    wait clear
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pegging di Commessa con gestione saldi da pegging
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = MAX(0,VAL(nvl(this.w_PESERIAL,"0")))
    Select Elabor
    go top
    do while not eof()
      if !g_UseProgBar
        if MOD(this.w_Recno, 1000)=0
          lnResult = 0
          lnResult = IsHungAppWindow(_VFP.hWnd)
          if lnResult > 0
            wait wind "" timeout .001
          endif
        endif
      endif
      if tiprec="0" or arflcomm<>"S" or empty(nvl(codcom,""))
        skip
        this.w_QTAIMP = 0
        this.w_SaldoIMP = 0
        this.w_QTAORD = 0
        this.w_SaldoORD = 0
      else
        this.w_Recno = RecNo()
        if codcom<>this.w_CODCOM or codsal<>this.w_KEYSAL
          * --- Record riferito ad un nuovo articolo/Commessa
          this.w_CODCOM = codcom
          this.w_DATIDX = datidx
          this.w_RecnoImp = this.w_Recno
          this.w_QTAIMP = 0
          this.w_SaldoIMP = this.w_QTAIMP
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = 0
          this.w_SaldoORD = this.w_QTAORD
          this.w_CODRIC = codric
          this.w_KEYSAL = CODSAL
        endif
        this.w_QTA = quanti
        this.w_SaldoIMP = this.w_SaldoIMP + max(-this.w_QTA,0)
        if this.w_QTAIMP=0
          this.w_RecnoImp = this.w_Recno
          this.w_QTAIMP = max(-this.w_QTA,0)
          this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
          this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
        endif
        this.w_SaldoORD = this.w_SaldoORD + max(this.w_QTA,0)
        if this.w_QTAORD=0
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = max(this.w_QTA,0)
          this.w_ODLORD = iif(tiprec="4ORDFO",iif(arpropre="E",iif(len(alltrim(odamsg))<>15,padrered,odamsg),iif(len(alltrim(odlmsg))<>15,padrered,odlmsg)),padrered)
          this.w_RIGORD = perownum
        endif
        if used("SalComm") and this.w_SaldoIMP>0
          if Elabor.proggmps="S" or this.oParentObject.w_PEGGING
            * --- Gli articoli oggetto MPS non vengono processati in fase di elaborazione MRP, � necessario ricalcolare qui il pegging di 2� livello.
            *     La stessa cosa vale nel caso l'elaborazione MRP non sia stata eseguita (Funzione di aggiornamento pegging)
            this.qtaabb = 0
            this.w_Fabnet = this.w_SaldoIMP
            * --- Verifico l'esistenza di una quantit� a magazzino riferita a questa commessa
            select SalComm 
 scan for oltcodic=Elabor.Codric and oltcomme=Elabor.CodCom and petiprif="M"
            this.qtaabb = PEQTAABB
            if this.qtaabb<>0 and this.w_Fabnet<>0
              this.riga = recno()
              scatter memvar 
 m.petiprif=iif(Elabor.tiprec<>"7ORDCL","O","D") 
 m.perigord=iif(Elabor.tiprec="7MRPIM",Elabor.cprownum,Elabor.perownum)
              if m.petiprif="O"
                m.perifodl=Elabor.padrered
                m.peserord=" "
              else
                m.peserord=Elabor.padrered
                m.perifodl=" "
              endif
              if this.qtaabb>=this.w_Fabnet
                replace peqtaabb with this.qtaabb-this.w_Fabnet
                m.peqtaabb=this.w_Fabnet
                this.w_Fabnet = 0
              else
                this.w_Fabnet = this.w_Fabnet-this.qtaabb
                replace peqtaabb with 0
              endif
              append blank 
 gather memvar
              go this.riga
            endif
            endscan
          endif
          * --- Rimetto a posto il magazzino della commessa
          select SalComm 
 scan for oltcodic=Elabor.Codric and oltcomme=Elabor.CodCom and (((petiprif="O" and Elabor.padrered=perifodl) or (petiprif="D" and left(Elabor.padrered,10)=peserord)) and Elabor.perownum=perigord)
          * --- Aggiungo le nuove righe
          this.w_QTAABB = SalComm.peqtaabb
          this.w_PROGR = max(this.w_PROGR + 1,1)
          this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
          activerec=recno()
          if SalComm.petiprif="D"
            * --- Da Documento
            * --- Insert into PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.PESERODL),'PEG_SELI','PESERODL');
              +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.peserord),'PEG_SELI','PESERORD');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.peqtaabb),'PEG_SELI','PEQTAABB');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.peodlori),'PEG_SELI','PEODLORI');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.perigord),'PEG_SELI','PERIGORD');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.oltcomme),'PEG_SELI','PECODCOM');
              +","+cp_NullLink(cp_ToStrODBC(SalComm.oltcodic),'PEG_SELI','PECODRIC');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',SalComm.PESERODL,'PETIPRIF',"M",'PESERORD',SalComm.peserord,'PEQTAABB',SalComm.peqtaabb,'PEODLORI',SalComm.peodlori,'PERIGORD',SalComm.perigord,'PECODCOM',SalComm.oltcomme,'PECODRIC',SalComm.oltcodic)
              insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                 values (;
                   this.w_PESERIAL;
                   ,SalComm.PESERODL;
                   ,"M";
                   ,SalComm.peserord;
                   ,SalComm.peqtaabb;
                   ,SalComm.peodlori;
                   ,SalComm.perigord;
                   ,SalComm.oltcomme;
                   ,SalComm.oltcodic;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Da ODL
            if SalComm.PESERODL<>SalComm.perifodl
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.PESERODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.perifodl),'PEG_SELI','PERIFODL');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.peqtaabb),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.peodlori),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.perigord),'PEG_SELI','PERIGORD');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.oltcomme),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(SalComm.oltcodic),'PEG_SELI','PECODRIC');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',SalComm.PESERODL,'PETIPRIF',"M",'PERIFODL',SalComm.perifodl,'PEQTAABB',SalComm.peqtaabb,'PEODLORI',SalComm.peodlori,'PERIGORD',SalComm.perigord,'PECODCOM',SalComm.oltcomme,'PECODRIC',SalComm.oltcodic)
                insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                   values (;
                     this.w_PESERIAL;
                     ,SalComm.PESERODL;
                     ,"M";
                     ,SalComm.perifodl;
                     ,SalComm.peqtaabb;
                     ,SalComm.peodlori;
                     ,SalComm.perigord;
                     ,SalComm.oltcomme;
                     ,SalComm.oltcodic;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
          this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
          this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
          Select Elabor 
 go this.w_RecnoImp
          if this.w_QTAIMP=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti>=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti<0
              this.w_QTAIMP = -quanti
              this.w_RecnoImp = Recno()
              this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
              this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
            endif
          endif
          select SalComm 
 go activerec
          endscan
        endif
        Select Elabor
        * --- Inizializzo contatore per verifica loop
        this.w_RND = 0
        do while this.w_SaldoORD>0 and this.w_SaldoIMP>0
          * --- Incremento contatore per verifica loop
          this.w_RND = this.w_RND+1
          * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
          this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
          if not empty(this.w_ODLORD)
            this.w_PROGR = max(this.w_PROGR + 1,1)
            this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
            * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
            this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
            do case
              case this.w_ODLIMP="Magazzino"
                * --- Abbinamento tra ODL e sottoscorta a magazzino
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"M";
                       ,this.w_ODLIMP;
                       ,this.w_QTAABB;
                       ,this.w_CODCOM;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case len(alltrim(this.w_ODLIMP))=15
                * --- Abbinamento tra ODL e impegno da ODL
                if this.w_ODLORD<>this.w_ODLIMP
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_ODLORD;
                         ,"O";
                         ,this.w_ODLIMP;
                         ,this.w_RIGIMP;
                         ,this.w_QTAABB;
                         ,this.w_ODLORD;
                         ,this.w_CODCOM;
                         ,this.w_CODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              case len(alltrim(this.w_ODLIMP))=10
                * --- Abbinamento tra ODL e impegno da documento
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"D";
                       ,left(this.w_ODLIMP,10);
                       ,this.w_RIGIMP;
                       ,this.w_QTAABB;
                       ,this.w_ODLORD;
                       ,this.w_CODCOM;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
            endcase
          endif
          this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
          this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
          this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
          this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
          go this.w_RecnoImp
          if this.w_QTAIMP=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti>=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti<0
              this.w_QTAIMP = -quanti
              this.w_RecnoImp = Recno()
              this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
              this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
            endif
          endif
          go this.w_RecnoOrd
          if this.w_QTAORD=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti<=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti>0
              this.w_QTAORD = quanti
              this.w_RecnoOrd = Recno()
              this.w_ODLORD = iif(tiprec="4ORDFO",iif(arpropre="E",iif(len(alltrim(odamsg))<>15,padrered,odamsg),iif(len(alltrim(odlmsg))<>15,padrered,odlmsg)),padrered)
              this.w_RIGORD = perownum
            endif
          endif
          if this.w_RND=this.w_MAXRND
            * --- Ci potrebbe essere un loop, azzero la q.t� e proseguo
            this.w_SaldoORD = 0
            if this.oParentObject.w_MRPLOG="S"
              this.w_oPEGLOG.ADDMSGLOG(">Si � verificato un loop sul pegging, verificare codice padre %1 codice figlio %2  commessa %3", alltrim(this.w_ODLIMP) , alltrim(this.w_ODLORD) , alltrim(this.w_CODRIC) , alltrim(this.w_CODCOM))     
            endif
          endif
        enddo
        go this.w_Recno
        if not eof()
          skip
        endif
      endif
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        if MOD(this.w_NRECCUR,1*iif(this.w_NRECELAB>1000,10,1)*iif(this.w_NRECELAB>10000,50,1))=0
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
        endif
      endif
    enddo
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pegging di Commessa standard senza gestione saldi da pegging
    Select Elabor
    go top
    this.w_CODRIC = space(41)
    this.w_KEYSAL = Space(this.w_LenCodSal)
    this.w_CODCOM = space(15)
    do while not eof()
      if !g_UseProgBar
        if MOD(this.w_Recno, 1000)=0
          lnResult = 0
          lnResult = IsHungAppWindow(_VFP.hWnd)
          if lnResult > 0
            wait wind "" timeout .001
          endif
        endif
      endif
      if tiprec="0" or arflcomm<>"S" or arflcom1="C"
        this.w_QTAIMP = 0
        this.w_SaldoIMP = 0
        this.w_QTAORD = 0
        this.w_SaldoORD = 0
        if tiprec="0SCOMM" and arflcom1<>"C" and arflcomm="S" and qtalor>0
          this.w_Recno = RecNo()
          this.w_QTA = qtalor
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = max(this.w_QTA,0)
          this.w_SaldoORD = this.w_QTAORD
          this.w_ODLORD = iif(this.w_QTA>0, "Magazzino", "")
          this.w_RIGORD = 0
          this.w_CODCOM = nvl(codcom,space(15))
          this.w_CODRIC = codric
          this.w_KEYSAL = CODSAL
        endif
        skip
      else
        this.w_Recno = RecNo()
        * --- La commessa vuota viene trattata come una normale commessa
        if nvl(codcom,space(15))<>this.w_CODCOM or codsal<>this.w_KEYSAL
          * --- Record riferito ad un nuovo articolo/Commessa
          this.w_CODCOM = nvl(codcom,space(15))
          this.w_RecnoImp = this.w_Recno
          this.w_QTAIMP = 0
          this.w_SaldoIMP = this.w_QTAIMP
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = 0
          this.w_SaldoORD = this.w_QTAORD
          this.w_CODRIC = codric
          this.w_KEYSAL = CODSAL
        endif
        this.w_QTA = quanti
        this.w_SaldoIMP = this.w_SaldoIMP + max(-this.w_QTA,0)
        if this.w_QTAIMP=0
          this.w_RecnoImp = this.w_Recno
          this.w_QTAIMP = max(-this.w_QTA,0)
          this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
          this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
        endif
        this.w_SaldoORD = this.w_SaldoORD + max(this.w_QTA,0)
        if this.w_QTAORD=0
          this.w_RecnoOrd = this.w_Recno
          this.w_QTAORD = max(this.w_QTA,0)
          this.w_ODLORD = padrered
          this.w_RIGORD = perownum
        endif
        * --- Inizializzo contatore per verifica loop
        this.w_RND = 0
        do while this.w_SaldoORD>0 and this.w_SaldoIMP>0
          * --- Incremento contatore per verifica loop
          this.w_RND = this.w_RND+1
          * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
          this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
          if not empty(this.w_ODLORD)
            this.w_PROGR = max(this.w_PROGR + 1,1)
            this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
            * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
            this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
            do case
              case this.w_ODLIMP="Magazzino"
                * --- Abbinamento tra ODL e sottoscorta a magazzino
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"M";
                       ,this.w_ODLIMP;
                       ,this.w_QTAABB;
                       ,this.w_CODCOM;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case len(alltrim(this.w_ODLIMP))=15
                * --- Abbinamento tra ODL e impegno da ODL
                if this.w_ODLORD<>this.w_ODLIMP
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_ODLORD;
                         ,"O";
                         ,this.w_ODLIMP;
                         ,this.w_RIGIMP;
                         ,this.w_QTAABB;
                         ,this.w_ODLORD;
                         ,this.w_CODCOM;
                         ,this.w_CODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              case len(alltrim(this.w_ODLIMP))=10
                * --- Abbinamento tra ODL e impegno da documento
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"D";
                       ,left(this.w_ODLIMP,10);
                       ,this.w_RIGIMP;
                       ,this.w_QTAABB;
                       ,this.w_ODLORD;
                       ,this.w_CODCOM;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
            endcase
          endif
          this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
          this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
          this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
          this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
          go this.w_RecnoImp
          if this.w_QTAIMP=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti>=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti<0
              this.w_QTAIMP = -quanti
              this.w_RecnoImp = Recno()
              this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
              this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
            endif
          endif
          go this.w_RecnoOrd
          if this.w_QTAORD=0 and RecNo()<>this.w_Recno
            skip
            do while not eof() and RecNo()<>this.w_Recno and quanti<=0
              skip
            enddo
            if not eof() and RecNo()<>this.w_Recno and quanti>0
              this.w_QTAORD = quanti
              this.w_RecnoOrd = Recno()
              this.w_ODLORD = padrered
              this.w_RIGORD = perownum
            endif
          endif
          if this.w_RND=this.w_MAXRND
            * --- Ci potrebbe essere un loop, azzero la q.t� e proseguo
            this.w_SaldoORD = 0
            if this.oParentObject.w_MRPLOG="S"
              this.w_oPEGLOG.ADDMSGLOG(">Si � verificato un loop sul pegging, verificare codice padre %1 codice figlio %2  commessa %3", alltrim(this.w_ODLIMP) , alltrim(this.w_ODLORD) , alltrim(this.w_CODRIC) , alltrim(this.w_CODCOM))     
            endif
          endif
        enddo
        go this.w_Recno
        if not eof()
          skip
        endif
      endif
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        if MOD(this.w_NRECCUR,1*iif(this.w_NRECELAB>1000,10,1)*iif(this.w_NRECELAB>10000,50,1))=0
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
        endif
      endif
    enddo
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pegging di Commessa con nettificazione senza gestione saldi da pegging
    Select Elabor
    go top
    this.w_CODRIC = space(41)
    this.w_KEYSAL = Space(this.w_LenCodSal)
    this.w_CODCOM = space(15)
    do while not eof()
      if !g_UseProgBar
        if MOD(this.w_Recno, 1000)=0
          lnResult = 0
          lnResult = IsHungAppWindow(_VFP.hWnd)
          if lnResult > 0
            wait wind "" timeout .001
          endif
        endif
      endif
      if tiprec="0" or arflcomm<>"S" or arflcom1<>"C" or codsal<>this.w_KEYSAL
        this.w_QTAIMP = 0
        this.w_SaldoIMP = 0
        this.w_QTAORD = 0
        this.w_SaldoORD = 0
        * --- Se articolo Personalizzato+Nettificazione ho bisogno di un cursore di appoggio per i saldi
        if (arflcom1="C" and tiprec="0SALDI") or codsal<>this.w_KEYSAL
          * --- Prima di svuotare il cursore lo uso per aggiornare il pegging-l'ultimo record viene valutato fuori dal ciclo
          if used("SalPCom")
            Select SalPCom 
 go top 
 scan for PTIPGES="I" and PQTAORD>0
            this.w_RecnoImp = recno()
            this.w_QTAIMP = PQTAORD
            this.w_SaldoIMP = PSALORD
            this.w_ODLIMP = iif(empty(nvl(PODLORD," ")),"Magazzino",PODLORD)
            this.w_RIGIMP = PRIGORD
            this.w_DATAIMP = PDATORD
            this.w_CODCOM = PCODCOM
            * --- Aggiorno cursore
            replace PQTAORD with 0, PSALORD with 0
            * --- Prima vedo se ho disponibilit� a magazzino
            if this.w_SaldoIMP>0
              * --- Verifico se � presente una quantit� a magazzino legata alla commessa
              Select SalPCom 
 go top 
 locate for PCODCOM=this.w_codcom and PTIPGES="G" and PQTAORD>0
              if found()
                this.w_RecnoOrd = PRECORD
                this.w_QTAORD = PQTAORD
                this.w_SaldoORD = PSALORD
                this.w_ODLORD = "Magazzino"
                this.w_RIGORD = 0
                this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
                if not empty(this.w_ODLORD) and this.w_QTAABB>0
                  this.w_PROGR = max(this.w_PROGR + 1,1)
                  this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                  do case
                    case this.w_ODLIMP="Magazzino"
                      * --- Abbinamento tra ODL e sottoscorta a magazzino
                      * --- Insert into PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                        +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                           values (;
                             this.w_PESERIAL;
                             ,this.w_ODLORD;
                             ,"M";
                             ,this.w_ODLIMP;
                             ,this.w_QTAABB;
                             ,this.w_CODCOM;
                             ,this.w_CODRIC;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                    case len(alltrim(this.w_ODLIMP))=15
                      * --- Abbinamento tra ODL e impegno da ODL
                      if this.w_ODLORD<>this.w_ODLIMP
                        * --- Insert into PEG_SELI
                        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_ccchkf=''
                        i_ccchkv=''
                        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                      " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                          cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                          +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                               +i_ccchkv+")")
                        else
                          cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                          insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                             values (;
                               this.w_PESERIAL;
                               ,this.w_ODLORD;
                               ,"O";
                               ,this.w_ODLIMP;
                               ,this.w_RIGIMP;
                               ,this.w_QTAABB;
                               ,this.w_ODLORD;
                               ,this.w_CODCOM;
                               ,this.w_CODRIC;
                               &i_ccchkv. )
                          i_Rows=iif(bTrsErr,0,1)
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if i_Rows<0 or bTrsErr
                          * --- Error: insert not accepted
                          i_Error=MSG_INSERT_ERROR
                          return
                        endif
                      endif
                    case len(alltrim(this.w_ODLIMP))=10
                      * --- Abbinamento tra ODL e impegno da documento
                      * --- Insert into PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                        +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                        +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                           values (;
                             this.w_PESERIAL;
                             ,this.w_ODLORD;
                             ,"D";
                             ,left(this.w_ODLIMP,10);
                             ,this.w_RIGIMP;
                             ,this.w_QTAABB;
                             ,this.w_ODLORD;
                             ,this.w_CODCOM;
                             ,this.w_CODRIC;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                  endcase
                endif
                if this.w_QTAIMP>this.w_QTAORD
                  this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
                  this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
                  this.w_QTAORD = 0
                  this.w_SaldoORD = 0
                else
                  this.w_QTAIMP = 0
                  this.w_SaldoIMP = 0
                  this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
                  this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
                endif
                * --- Aggiorno cursore
                Select SalPCom
                replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
              endif
              * --- Variabile di appoggio, usata per evitare loop.
              *     Nel caso di articoli Ogg. MPS, che non vengono elaborati da MRP � infatti possibile che l'impegnato non sia completamente assegnabile,
              *     in questo caso se w_SaldoAPP e w_SaldoIMP sono uguali esco dallo while sottostante. Questo accade anche se uso della giacenza libera.
              this.w_SaldoAPP = 0
              do while this.w_SaldoIMP>0 and this.w_SaldoIMP<>this.w_SaldoAPP
                this.w_SaldoAPP = this.w_SaldoIMP
                Select SalPCom 
 go top 
 locate for PCODCOM=this.w_codcom and PTIPGES="S" and PQTAORD>0 and this.w_DATAIMP>=PDATORD
                if found()
                  this.w_RecnoOrd = recno()
                  this.w_QTAORD = PQTAORD
                  this.w_SaldoORD = PSALORD
                  this.w_ODLORD = PODLORD
                  this.w_RIGORD = PRIGORD
                  if this.w_SaldoORD>0
                    * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
                    this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
                    if not empty(this.w_ODLORD)
                      this.w_PROGR = max(this.w_PROGR + 1,1)
                      this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                      * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
                      this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
                      do case
                        case this.w_ODLIMP="Magazzino"
                          * --- Abbinamento tra ODL e sottoscorta a magazzino
                          * --- Insert into PEG_SELI
                          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_ccchkf=''
                          i_ccchkv=''
                          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                        " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                            cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                            +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                 +i_ccchkv+")")
                          else
                            cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                            insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                               values (;
                                 this.w_PESERIAL;
                                 ,this.w_ODLORD;
                                 ,"M";
                                 ,this.w_ODLIMP;
                                 ,this.w_QTAABB;
                                 ,this.w_CODCOM;
                                 ,this.w_CODRIC;
                                 &i_ccchkv. )
                            i_Rows=iif(bTrsErr,0,1)
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if i_Rows<0 or bTrsErr
                            * --- Error: insert not accepted
                            i_Error=MSG_INSERT_ERROR
                            return
                          endif
                        case len(alltrim(this.w_ODLIMP))=15
                          * --- Abbinamento tra ODL e impegno da ODL
                          if this.w_ODLORD<>this.w_ODLIMP
                            * --- Insert into PEG_SELI
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_ccchkf=''
                            i_ccchkv=''
                            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                          " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                              cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                              +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                   +i_ccchkv+")")
                            else
                              cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                              insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                                 values (;
                                   this.w_PESERIAL;
                                   ,this.w_ODLORD;
                                   ,"O";
                                   ,this.w_ODLIMP;
                                   ,this.w_RIGIMP;
                                   ,this.w_QTAABB;
                                   ,this.w_ODLORD;
                                   ,this.w_CODCOM;
                                   ,this.w_CODRIC;
                                   &i_ccchkv. )
                              i_Rows=iif(bTrsErr,0,1)
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if i_Rows<0 or bTrsErr
                              * --- Error: insert not accepted
                              i_Error=MSG_INSERT_ERROR
                              return
                            endif
                          endif
                        case len(alltrim(this.w_ODLIMP))=10
                          * --- Abbinamento tra ODL e impegno da documento
                          * --- Insert into PEG_SELI
                          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_ccchkf=''
                          i_ccchkv=''
                          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                        " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                            cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                            +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                            +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                 +i_ccchkv+")")
                          else
                            cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                            insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                               values (;
                                 this.w_PESERIAL;
                                 ,this.w_ODLORD;
                                 ,"D";
                                 ,left(this.w_ODLIMP,10);
                                 ,this.w_RIGIMP;
                                 ,this.w_QTAABB;
                                 ,this.w_ODLORD;
                                 ,this.w_CODCOM;
                                 ,this.w_CODRIC;
                                 &i_ccchkv. )
                            i_Rows=iif(bTrsErr,0,1)
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if i_Rows<0 or bTrsErr
                            * --- Error: insert not accepted
                            i_Error=MSG_INSERT_ERROR
                            return
                          endif
                      endcase
                    endif
                    if this.w_QTAIMP>this.w_QTAORD
                      this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
                      this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
                      this.w_QTAORD = 0
                      this.w_SaldoORD = 0
                    else
                      this.w_QTAIMP = 0
                      this.w_SaldoIMP = 0
                      this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
                      this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
                    endif
                    * --- Aggiorno cursore
                    Select SalPCom
                    replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
                    Select SalPCom 
 go this.w_RecnoImp
                    replace PQTAORD with this.w_QTAIMP, PSALORD with this.w_SaldoIMP
                    go this.w_RecnoOrd
                  endif
                endif
                if not eof()
                  skip
                endif
              enddo
              * --- Verifico se � presente una quantit� a magazzino libera solo se necessario
              if this.w_SaldoIMP>0
                Select SalPCom 
 go top 
 locate for empty(PCODCOM) and PTIPGES="G" and PQTAORD>0
                if found()
                  this.w_RecnoOrd = recno()
                  this.w_QTAORD = PQTAORD
                  this.w_SaldoORD = PSALORD
                  this.w_ODLORD = "Magazzino"
                  this.w_RIGORD = 0
                  this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
                  if not empty(this.w_ODLORD) and this.w_QTAABB>0
                    this.w_PROGR = max(this.w_PROGR + 1,1)
                    this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                    do case
                      case this.w_ODLIMP="Magazzino"
                        * --- Abbinamento tra ODL e sottoscorta a magazzino
                        * --- Insert into PEG_SELI
                        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_ccchkf=''
                        i_ccchkv=''
                        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                      " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                          cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                          +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                               +i_ccchkv+")")
                        else
                          cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                          insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                             values (;
                               this.w_PESERIAL;
                               ,this.w_ODLORD;
                               ,"M";
                               ,this.w_ODLIMP;
                               ,this.w_QTAABB;
                               ,this.w_CODCOM;
                               ,this.w_CODRIC;
                               &i_ccchkv. )
                          i_Rows=iif(bTrsErr,0,1)
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if i_Rows<0 or bTrsErr
                          * --- Error: insert not accepted
                          i_Error=MSG_INSERT_ERROR
                          return
                        endif
                      case len(alltrim(this.w_ODLIMP))=15
                        * --- Abbinamento tra ODL e impegno da ODL
                        if this.w_ODLORD<>this.w_ODLIMP
                          * --- Insert into PEG_SELI
                          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_ccchkf=''
                          i_ccchkv=''
                          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                        " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                            cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                            +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                 +i_ccchkv+")")
                          else
                            cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                            insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                               values (;
                                 this.w_PESERIAL;
                                 ,this.w_ODLORD;
                                 ,"O";
                                 ,this.w_ODLIMP;
                                 ,this.w_RIGIMP;
                                 ,this.w_QTAABB;
                                 ,this.w_ODLORD;
                                 ,this.w_CODCOM;
                                 ,this.w_CODRIC;
                                 &i_ccchkv. )
                            i_Rows=iif(bTrsErr,0,1)
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if i_Rows<0 or bTrsErr
                            * --- Error: insert not accepted
                            i_Error=MSG_INSERT_ERROR
                            return
                          endif
                        endif
                      case len(alltrim(this.w_ODLIMP))=10
                        * --- Abbinamento tra ODL e impegno da documento
                        * --- Insert into PEG_SELI
                        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_ccchkf=''
                        i_ccchkv=''
                        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                      " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                          cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                          +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                          +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                               +i_ccchkv+")")
                        else
                          cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                          insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                             values (;
                               this.w_PESERIAL;
                               ,this.w_ODLORD;
                               ,"D";
                               ,left(this.w_ODLIMP,10);
                               ,this.w_RIGIMP;
                               ,this.w_QTAABB;
                               ,this.w_ODLORD;
                               ,this.w_CODCOM;
                               ,this.w_CODRIC;
                               &i_ccchkv. )
                          i_Rows=iif(bTrsErr,0,1)
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if i_Rows<0 or bTrsErr
                          * --- Error: insert not accepted
                          i_Error=MSG_INSERT_ERROR
                          return
                        endif
                    endcase
                  endif
                  if this.w_QTAIMP>this.w_QTAORD
                    this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
                    this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
                    this.w_QTAORD = 0
                    this.w_SaldoORD = 0
                  else
                    this.w_QTAIMP = 0
                    this.w_SaldoIMP = 0
                    this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
                    this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
                  endif
                  * --- Aggiorno cursore
                  Select SalPCom
                  replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
                endif
              endif
              if this.w_PPORDICM="S"
                * --- Variabile di appoggio, usata per evitare loop.
                *     Nel caso di articoli Ogg. MPS, che non vengono elaborati da MRP � infatti possibile che l'impegnato non sia completamente assegnabile,
                *     in questo caso se w_SaldoAPP e w_SaldoIMP sono uguali esco dallo while sottostante. Questo accade anche se uso della giacenza libera.
                this.w_SaldoAPP = 0
                do while this.w_SaldoIMP>0 and this.w_SaldoIMP<>this.w_SaldoAPP
                  this.w_SaldoAPP = this.w_SaldoIMP
                  Select SalPCom 
 go top 
 locate for empty(PCODCOM) and PTIPGES="S" and PQTAORD>0 and this.w_DATAIMP>=PDATORD
                  if found()
                    this.w_RecnoOrd = recno()
                    this.w_QTAORD = PQTAORD
                    this.w_SaldoORD = PSALORD
                    this.w_ODLORD = PODLORD
                    this.w_RIGORD = PRIGORD
                    if this.w_SaldoORD>0
                      * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
                      this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
                      if not empty(this.w_ODLORD)
                        this.w_PROGR = max(this.w_PROGR + 1,1)
                        this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                        * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
                        this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
                        do case
                          case this.w_ODLIMP="Magazzino"
                            * --- Abbinamento tra ODL e sottoscorta a magazzino
                            * --- Insert into PEG_SELI
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_ccchkf=''
                            i_ccchkv=''
                            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                          " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                              cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                              +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                   +i_ccchkv+")")
                            else
                              cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                              insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                                 values (;
                                   this.w_PESERIAL;
                                   ,this.w_ODLORD;
                                   ,"M";
                                   ,this.w_ODLIMP;
                                   ,this.w_QTAABB;
                                   ,this.w_CODCOM;
                                   ,this.w_CODRIC;
                                   &i_ccchkv. )
                              i_Rows=iif(bTrsErr,0,1)
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if i_Rows<0 or bTrsErr
                              * --- Error: insert not accepted
                              i_Error=MSG_INSERT_ERROR
                              return
                            endif
                          case len(alltrim(this.w_ODLIMP))=15
                            * --- Abbinamento tra ODL e impegno da ODL
                            if this.w_ODLORD<>this.w_ODLIMP
                              * --- Insert into PEG_SELI
                              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                              i_commit = .f.
                              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                cp_BeginTrs()
                                i_commit = .t.
                              endif
                              i_ccchkf=''
                              i_ccchkv=''
                              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                              if i_nConn<>0
                                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                            " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                                +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                                +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                     +i_ccchkv+")")
                              else
                                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                                insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                                   values (;
                                     this.w_PESERIAL;
                                     ,this.w_ODLORD;
                                     ,"O";
                                     ,this.w_ODLIMP;
                                     ,this.w_RIGIMP;
                                     ,this.w_QTAABB;
                                     ,this.w_ODLORD;
                                     ,this.w_CODCOM;
                                     ,this.w_CODRIC;
                                     &i_ccchkv. )
                                i_Rows=iif(bTrsErr,0,1)
                              endif
                              if i_commit
                                cp_EndTrs(.t.)
                              endif
                              if i_Rows<0 or bTrsErr
                                * --- Error: insert not accepted
                                i_Error=MSG_INSERT_ERROR
                                return
                              endif
                            endif
                          case len(alltrim(this.w_ODLIMP))=10
                            * --- Abbinamento tra ODL e impegno da documento
                            * --- Insert into PEG_SELI
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_ccchkf=''
                            i_ccchkv=''
                            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                          " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                              cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                              +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                              +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                              +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                                   +i_ccchkv+")")
                            else
                              cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                              insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                                 values (;
                                   this.w_PESERIAL;
                                   ,this.w_ODLORD;
                                   ,"D";
                                   ,left(this.w_ODLIMP,10);
                                   ,this.w_RIGIMP;
                                   ,this.w_QTAABB;
                                   ,this.w_ODLORD;
                                   ,this.w_CODCOM;
                                   ,this.w_CODRIC;
                                   &i_ccchkv. )
                              i_Rows=iif(bTrsErr,0,1)
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if i_Rows<0 or bTrsErr
                              * --- Error: insert not accepted
                              i_Error=MSG_INSERT_ERROR
                              return
                            endif
                        endcase
                      endif
                      if this.w_QTAIMP>this.w_QTAORD
                        this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
                        this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
                        this.w_QTAORD = 0
                        this.w_SaldoORD = 0
                      else
                        this.w_QTAIMP = 0
                        this.w_SaldoIMP = 0
                        this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
                        this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
                      endif
                      * --- Aggiorno cursore
                      Select SalPCom
                      replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
                      Select SalPCom 
 go this.w_RecnoImp
                      replace PQTAORD with this.w_QTAIMP, PSALORD with this.w_SaldoIMP
                      go this.w_RecnoOrd
                    endif
                  endif
                  if not eof()
                    skip
                  endif
                enddo
              endif
            endif
            go this.w_RecnoImp 
 endscan
          endif
          * --- Se il cursore di appoggio esiste (per un altro articolo) lo svuoto, se non esiste lo creo
          if used("SalPCom")
            Select SalPCom 
 zap
          else
            Create cursor SalPCom (PCODCOM C(15), PCODATT C(15), PRECORD N(5,0), PQTAORD N(18,6), PSALORD N(18,6), PTIPGES C(1),PODLORD C(15),PRIGORD N(4,0),PDATORD D)
          endif
          Select Elabor
        endif
        this.w_KEYSAL = CODSAL
        if tiprec="0SCOMM" and arflcom1="C"
          * --- Inserisco riga saldo magazzino
          if (Elabor.qtalor>0 and not empty(nvl(Elabor.codcom,"               "))) or (Elabor.quanti>0 and empty(nvl(Elabor.codcom,"               ")))
            this.w_Recno = RecNo()
            Insert into SalPCom (PCODCOM, PCODATT, PRECORD, PQTAORD, PSALORD,PTIPGES) values (nvl(Elabor.codcom,"               "), Elabor.codatt, this.w_Recno,iif(empty(nvl(Elabor.codcom," ")),Elabor.quanti,Elabor.qtalor),iif(empty(nvl(Elabor.codcom," ")),Elabor.quanti,Elabor.qtalor),"G")
            Select Elabor
          endif
        endif
        skip
      else
        this.w_Recno = RecNo()
        this.w_CODCOM = codcom
        this.w_QTA = quanti
        * --- Riga di impegno
        this.w_SaldoIMP = this.w_SaldoIMP + max(-this.w_QTA,0)
        this.w_RecnoImp = this.w_Recno
        this.w_QTAIMP = max(-this.w_QTA,0)
        this.w_ODLIMP = iif(not empty(nvl(oltseodl," ")),oltseodl,padrered)
        this.w_RIGIMP = iif(not empty(nvl(oltseodl," ")),olrownum,perownum)
        if this.w_QTAIMP>0
          Insert into SalPCom (PCODCOM, PCODATT, PRECORD, PQTAORD, PSALORD,PTIPGES,PODLORD,PRIGORD,PDATORD) values (nvl(this.w_codcom,"               "), Elabor.codatt, this.w_Recno,max(this.w_QTAIMP,0),max(this.w_SaldoIMP,0),"I",this.w_ODLIMP,this.w_RIGIMP,Elabor.dateva)
          Select Elabor
          go this.w_Recno
        endif
        * --- Riga di ordine
        this.w_SaldoORD = this.w_SaldoORD + max(this.w_QTA,0)
        this.w_RecnoOrd = this.w_Recno
        this.w_QTAORD = max(this.w_QTA,0)
        this.w_ODLORD = padrered
        this.w_RIGORD = perownum
        if this.w_QTAORD>0
          Insert into SalPCom (PCODCOM, PCODATT, PRECORD, PQTAORD, PSALORD,PTIPGES,PODLORD,PRIGORD,PDATORD) values (nvl(this.w_codcom,"               "), Elabor.codatt, this.w_Recno,max(this.w_QTAORD,0),max(this.w_SaldoORD,0),"S",this.w_ODLORD,this.w_RIGORD,Elabor.dateva)
          Select Elabor
          go this.w_Recno
        endif
        if not eof()
          skip
        endif
      endif
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        if MOD(this.w_NRECCUR,1*iif(this.w_NRECELAB>1000,10,1)*iif(this.w_NRECELAB>10000,50,1))=0
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
        endif
      endif
    enddo
    * --- Qui aggiorno il record che � rimasto fuori dall'elaborazione (l'ultimo)
    if used("SalPCom")
      this.w_QTAIMP = 0
      this.w_SaldoIMP = 0
      this.w_QTAORD = 0
      this.w_SaldoORD = 0
      Select SalPCom 
 go top 
 scan for PTIPGES="I" and PQTAORD>0
      this.w_RecnoImp = recno()
      this.w_QTAIMP = PQTAORD
      this.w_SaldoIMP = PSALORD
      this.w_ODLIMP = iif(empty(nvl(PODLORD," ")),"Magazzino",PODLORD)
      this.w_RIGIMP = PRIGORD
      this.w_DATAIMP = PDATORD
      this.w_CODCOM = PCODCOM
      * --- Aggiorno cursore
      replace PQTAORD with 0, PSALORD with 0
      * --- Prima vedo se ho disponibilit� a magazzino
      if this.w_SaldoIMP>0
        * --- Verifico se � presente una quantit� a magazzino legata alla commessa
        Select SalPCom 
 go top 
 locate for PCODCOM=this.w_codcom and PTIPGES="G" and PQTAORD>0
        if found()
          this.w_RecnoOrd = recno()
          this.w_QTAORD = PQTAORD
          this.w_SaldoORD = PSALORD
          this.w_ODLORD = "Magazzino"
          this.w_RIGORD = 0
          this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
          if not empty(this.w_ODLORD) and this.w_QTAABB>0
            this.w_PROGR = max(this.w_PROGR + 1,1)
            this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
            do case
              case this.w_ODLIMP="Magazzino"
                * --- Abbinamento tra ODL e sottoscorta a magazzino
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"M";
                       ,this.w_ODLIMP;
                       ,this.w_QTAABB;
                       ,this.w_CODCOM;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case len(alltrim(this.w_ODLIMP))=15
                * --- Abbinamento tra ODL e impegno da ODL
                if this.w_ODLORD<>this.w_ODLIMP
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_ODLORD;
                         ,"O";
                         ,this.w_ODLIMP;
                         ,this.w_RIGIMP;
                         ,this.w_QTAABB;
                         ,this.w_ODLORD;
                         ,this.w_CODCOM;
                         ,this.w_CODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              case len(alltrim(this.w_ODLIMP))=10
                * --- Abbinamento tra ODL e impegno da documento
                * --- Insert into PEG_SELI
                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                  +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                     values (;
                       this.w_PESERIAL;
                       ,this.w_ODLORD;
                       ,"D";
                       ,left(this.w_ODLIMP,10);
                       ,this.w_RIGIMP;
                       ,this.w_QTAABB;
                       ,this.w_ODLORD;
                       ,this.w_CODCOM;
                       ,this.w_CODRIC;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
            endcase
          endif
          if this.w_QTAIMP>this.w_QTAORD
            this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
            this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
            this.w_QTAORD = 0
            this.w_SaldoORD = 0
          else
            this.w_QTAIMP = 0
            this.w_SaldoIMP = 0
            this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
            this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
          endif
          * --- Aggiorno cursore
          Select SalPCom
          replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
        endif
        * --- Variabile di appoggio, usata per evitare loop.
        *     Nel caso di articoli Ogg. MPS, che non vengono elaborati da MRP � infatti possibile che l'impegnato non sia completamente assegnabile,
        *     in questo caso se w_SaldoAPP e w_SaldoIMP sono uguali esco dallo while sottostante. Questo accade anche se uso della giacenza libera.
        this.w_SaldoAPP = 0
        do while this.w_SaldoIMP>0 and this.w_SaldoIMP<>this.w_SaldoAPP
          this.w_SaldoAPP = this.w_SaldoIMP
          Select SalPCom 
 go top 
 locate for PCODCOM=this.w_codcom and PTIPGES="S" and PQTAORD>0 and this.w_DATAIMP>=PDATORD
          if found()
            this.w_RecnoOrd = recno()
            this.w_QTAORD = PQTAORD
            this.w_SaldoORD = PSALORD
            this.w_ODLORD = PODLORD
            this.w_RIGORD = PRIGORD
            if this.w_SaldoORD>0
              * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
              this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
              if not empty(this.w_ODLORD)
                this.w_PROGR = max(this.w_PROGR + 1,1)
                this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
                this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
                do case
                  case this.w_ODLIMP="Magazzino"
                    * --- Abbinamento tra ODL e sottoscorta a magazzino
                    * --- Insert into PEG_SELI
                    i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                      +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                      insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                         values (;
                           this.w_PESERIAL;
                           ,this.w_ODLORD;
                           ,"M";
                           ,this.w_ODLIMP;
                           ,this.w_QTAABB;
                           ,this.w_CODCOM;
                           ,this.w_CODRIC;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                  case len(alltrim(this.w_ODLIMP))=15
                    * --- Abbinamento tra ODL e impegno da ODL
                    if this.w_ODLORD<>this.w_ODLIMP
                      * --- Insert into PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                        +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                           values (;
                             this.w_PESERIAL;
                             ,this.w_ODLORD;
                             ,"O";
                             ,this.w_ODLIMP;
                             ,this.w_RIGIMP;
                             ,this.w_QTAABB;
                             ,this.w_ODLORD;
                             ,this.w_CODCOM;
                             ,this.w_CODRIC;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                    endif
                  case len(alltrim(this.w_ODLIMP))=10
                    * --- Abbinamento tra ODL e impegno da documento
                    * --- Insert into PEG_SELI
                    i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                      +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                      +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                      insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                         values (;
                           this.w_PESERIAL;
                           ,this.w_ODLORD;
                           ,"D";
                           ,left(this.w_ODLIMP,10);
                           ,this.w_RIGIMP;
                           ,this.w_QTAABB;
                           ,this.w_ODLORD;
                           ,this.w_CODCOM;
                           ,this.w_CODRIC;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                endcase
              endif
              if this.w_QTAIMP>this.w_QTAORD
                this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
                this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
                this.w_QTAORD = 0
                this.w_SaldoORD = 0
              else
                this.w_QTAIMP = 0
                this.w_SaldoIMP = 0
                this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
                this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
              endif
              * --- Aggiorno cursore
              Select SalPCom
              replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
              Select SalPCom 
 go this.w_RecnoImp
              replace PQTAORD with this.w_QTAIMP, PSALORD with this.w_SaldoIMP
              go this.w_RecnoOrd
            endif
          endif
          if not eof()
            skip
          endif
        enddo
        * --- Verifico se � presente una quantit� a magazzino libera solo se necessario
        if this.w_SaldoIMP>0
          Select SalPCom 
 go top 
 locate for empty(PCODCOM) and PTIPGES="G" and PQTAORD>0
          if found()
            this.w_RecnoOrd = recno()
            this.w_QTAORD = PQTAORD
            this.w_SaldoORD = PSALORD
            this.w_ODLORD = "Magazzino"
            this.w_RIGORD = 0
            this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
            if not empty(this.w_ODLORD) and this.w_QTAABB>0
              this.w_PROGR = max(this.w_PROGR + 1,1)
              this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
              do case
                case this.w_ODLIMP="Magazzino"
                  * --- Abbinamento tra ODL e sottoscorta a magazzino
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_ODLORD;
                         ,"M";
                         ,this.w_ODLIMP;
                         ,this.w_QTAABB;
                         ,this.w_CODCOM;
                         ,this.w_CODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                case len(alltrim(this.w_ODLIMP))=15
                  * --- Abbinamento tra ODL e impegno da ODL
                  if this.w_ODLORD<>this.w_ODLIMP
                    * --- Insert into PEG_SELI
                    i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                      +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                      insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                         values (;
                           this.w_PESERIAL;
                           ,this.w_ODLORD;
                           ,"O";
                           ,this.w_ODLIMP;
                           ,this.w_RIGIMP;
                           ,this.w_QTAABB;
                           ,this.w_ODLORD;
                           ,this.w_CODCOM;
                           ,this.w_CODRIC;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                  endif
                case len(alltrim(this.w_ODLIMP))=10
                  * --- Abbinamento tra ODL e impegno da documento
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_ODLORD;
                         ,"D";
                         ,left(this.w_ODLIMP,10);
                         ,this.w_RIGIMP;
                         ,this.w_QTAABB;
                         ,this.w_ODLORD;
                         ,this.w_CODCOM;
                         ,this.w_CODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
              endcase
            endif
            if this.w_QTAIMP>this.w_QTAORD
              this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
              this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
              this.w_QTAORD = 0
              this.w_SaldoORD = 0
            else
              this.w_QTAIMP = 0
              this.w_SaldoIMP = 0
              this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
              this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
            endif
            * --- Aggiorno cursore
            Select SalPCom
            replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
          endif
        endif
        if this.w_PPORDICM="S"
          * --- Variabile di appoggio, usata per evitare loop.
          *     Nel caso di articoli Ogg. MPS, che non vengono elaborati da MRP � infatti possibile che l'impegnato non sia completamente assegnabile,
          *     in questo caso se w_SaldoAPP e w_SaldoIMP sono uguali esco dallo while sottostante. Questo accade anche se uso della giacenza libera.
          this.w_SaldoAPP = 0
          do while this.w_SaldoIMP>0 and this.w_SaldoIMP<>this.w_SaldoAPP
            this.w_SaldoAPP = this.w_SaldoIMP
            Select SalPCom 
 go top 
 locate for empty(PCODCOM) and PTIPGES="S" and PQTAORD>0 and this.w_DATAIMP>=PDATORD
            if found()
              this.w_RecnoOrd = recno()
              this.w_QTAORD = PQTAORD
              this.w_SaldoORD = PSALORD
              this.w_ODLORD = PODLORD
              this.w_RIGORD = PRIGORD
              if this.w_SaldoORD>0
                * --- Si pu� creare un abbinamento: massima  quantit� abbinabile
                this.w_QTAABB = min(this.w_QTAIMP,this.w_QTAORD)
                if not empty(this.w_ODLORD)
                  this.w_PROGR = max(this.w_PROGR + 1,1)
                  this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                  * --- Se l'ordinato � un Doc (es. C/L) aggiunge il numero di riga
                  this.w_ODLORD = iif(len(alltrim(this.w_ODLORD))=10, left(this.w_ODLORD,10)+str(this.w_RIGORD,4,0), this.w_ODLORD)
                  do case
                    case this.w_ODLIMP="Magazzino"
                      * --- Abbinamento tra ODL e sottoscorta a magazzino
                      * --- Insert into PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                        +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"M",'PERIFODL',this.w_ODLIMP,'PEQTAABB',this.w_QTAABB,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PECODCOM,PECODRIC &i_ccchkf. );
                           values (;
                             this.w_PESERIAL;
                             ,this.w_ODLORD;
                             ,"M";
                             ,this.w_ODLIMP;
                             ,this.w_QTAABB;
                             ,this.w_CODCOM;
                             ,this.w_CODRIC;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                    case len(alltrim(this.w_ODLIMP))=15
                      * --- Abbinamento tra ODL e impegno da ODL
                      if this.w_ODLORD<>this.w_ODLIMP
                        * --- Insert into PEG_SELI
                        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_ccchkf=''
                        i_ccchkv=''
                        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                      " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                          cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                          +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLIMP),'PEG_SELI','PERIFODL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                               +i_ccchkv+")")
                        else
                          cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"O",'PERIFODL',this.w_ODLIMP,'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                          insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                             values (;
                               this.w_PESERIAL;
                               ,this.w_ODLORD;
                               ,"O";
                               ,this.w_ODLIMP;
                               ,this.w_RIGIMP;
                               ,this.w_QTAABB;
                               ,this.w_ODLORD;
                               ,this.w_CODCOM;
                               ,this.w_CODRIC;
                               &i_ccchkv. )
                          i_Rows=iif(bTrsErr,0,1)
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if i_Rows<0 or bTrsErr
                          * --- Error: insert not accepted
                          i_Error=MSG_INSERT_ERROR
                          return
                        endif
                      endif
                    case len(alltrim(this.w_ODLIMP))=10
                      * --- Abbinamento tra ODL e impegno da documento
                      * --- Insert into PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PESERODL');
                        +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                        +","+cp_NullLink(cp_ToStrODBC(left(this.w_ODLIMP,10)),'PEG_SELI','PESERORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGIMP),'PEG_SELI','PERIGORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAABB),'PEG_SELI','PEQTAABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ODLORD),'PEG_SELI','PEODLORI');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'PEG_SELI','PECODRIC');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_ODLORD,'PETIPRIF',"D",'PESERORD',left(this.w_ODLIMP,10),'PERIGORD',this.w_RIGIMP,'PEQTAABB',this.w_QTAABB,'PEODLORI',this.w_ODLORD,'PECODCOM',this.w_CODCOM,'PECODRIC',this.w_CODRIC)
                        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                           values (;
                             this.w_PESERIAL;
                             ,this.w_ODLORD;
                             ,"D";
                             ,left(this.w_ODLIMP,10);
                             ,this.w_RIGIMP;
                             ,this.w_QTAABB;
                             ,this.w_ODLORD;
                             ,this.w_CODCOM;
                             ,this.w_CODRIC;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                  endcase
                endif
                if this.w_QTAIMP>this.w_QTAORD
                  this.w_QTAIMP = this.w_QTAIMP-this.w_QTAABB
                  this.w_SaldoIMP = this.w_SaldoIMP-this.w_QTAABB
                  this.w_QTAORD = 0
                  this.w_SaldoORD = 0
                else
                  this.w_QTAIMP = 0
                  this.w_SaldoIMP = 0
                  this.w_QTAORD = this.w_QTAORD-this.w_QTAABB
                  this.w_SaldoORD = this.w_SaldoORD-this.w_QTAABB
                endif
                * --- Aggiorno cursore
                Select SalPCom
                replace PQTAORD with this.w_QTAORD, PSALORD with this.w_SaldoORD
                Select SalPCom 
 go this.w_RecnoImp
                replace PQTAORD with this.w_QTAIMP, PSALORD with this.w_SaldoIMP
                go this.w_RecnoOrd
              endif
            endif
            if not eof()
              skip
            endif
          enddo
        endif
      endif
      go this.w_RecnoImp 
 endscan
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PEG_SELI'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='*TMPPEG_SELI'
    this.cWorkTables[4]='*TMPPEG2SELI'
    this.cWorkTables[5]='*TMPPEG3SELI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSDBNBPG')
      use in _Curs_GSDBNBPG
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
