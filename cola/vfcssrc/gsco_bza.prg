* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bza                                                        *
*              Lancia ODL da dich.produzione                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-27                                                      *
* Last revis.: 2015-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bza",oParentObject)
return(i_retval)

define class tgsco_bza as StdBatch
  * --- Local variables
  w_CODODL = space(15)
  TmpC = space(15)
  w_PROG = .NULL.
  w_TPPROVE = space(1)
  * --- WorkFile variables
  ODL_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiama ODL da Dichiarazione di Produzione  (GSCO_ADP)
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_CODODL = Nvl(g_oMenu.oKey(1,3),SPACE(15))
      this.w_PROG = GSCO_BOR(This, this.w_CODODL)
      if Type("g_oMenu.oKey")<>"U" and !Empty(this.w_CODODL)
        * --- Lanciato tramite tasto destro 
        if g_oMenu.cbatchtype # "A"
          do case
            case g_oMenu.cbatchtype = "M"
              this.w_PROG.ecpEdit()     
            case g_oMenu.cbatchtype = "L"
              this.w_PROG.ecpLoad()     
          endcase
        endif
      endif
    else
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      this.w_CODODL = &cCurs..OLCODODL
      this.w_CODODL = NVL(this.w_CODODL, SPACE(15))
      GSCO_BOR(this,this.w_CODODL)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ODL_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
