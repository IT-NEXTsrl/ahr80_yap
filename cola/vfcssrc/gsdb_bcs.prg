* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bcs                                                        *
*              Cambio stato ODL/OCL                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_701]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-07                                                      *
* Last revis.: 2016-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bcs",oParentObject,m.pAzione)
return(i_retval)

define class tgsdb_bcs as StdBatch
  * --- Local variables
  pAzione = space(2)
  w_LNumErr = 0
  w_nRecSel = 0
  w_nRecEla = 0
  NC = space(10)
  TmpC = space(100)
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_CODODL = space(15)
  w_CODICE = space(20)
  w_CODART = space(20)
  w_COFOR = space(15)
  w_PROVEN = space(1)
  w_PERASS = space(3)
  w_QTAODL = 0
  w_CODMAG = space(5)
  w_STAODL = space(1)
  w_CAUORD = space(5)
  w_CAUO_OR = space(1)
  w_CAUO_IM = space(1)
  w_FLSU = space(1)
  w_FLCO = space(1)
  w_FLGORD = space(1)
  w_FLGIMP = space(1)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_OLTCOMME = space(15)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  CAM_AGAZ_idx=0
  MPS_TFOR_idx=0
  SALDIART_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Pianificazione ODP/ODR/ODF Suggeriti (da GSDB_KCS)
    * --- Log Errori
    * --- Variabili Cursore
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_LNumErr = 0
    * --- Nome cursore collegato allo zoom
    this.NC = this.oParentObject.w_ZoomSel.cCursor
    do case
      case this.pAzione="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione="INTERROGA"
        * --- Visualizza Zoom Elenco ODL/OCL periodo
        this.oParentObject.NotifyEvent("Interroga")
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
      case this.pAzione="AG"
        * --- Aggiornamento Status
        * --- Controlla selezioni
        SELECT (this.NC)
        GO TOP
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel>0
          * --- Crea il File delle Messaggistiche di Errore
          CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
          * --- Legge cursore di selezione ...
          this.w_nRecSel = 0
          this.w_nRecEla = 0
          * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
          this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
          * --- Cicla sui codici selezionati ...
          SELECT (this.NC)
          GO TOP
          SCAN FOR xChk=1 AND NOT EMPTY(NVL(OLCODODL,"")) 
          * --- Legge dati di interesse ....
          this.w_CODODL = OLCODODL
          this.w_CODICE = OLTCODIC
          this.w_CODART = NVL(OLTCOART, SPACE(15))
          this.w_COFOR = NVL(OLTCOFOR, SPACE(15))
          this.w_PROVEN = NVL(OLTPROVE, " ")
          this.w_PERASS = NVL(OLTPERAS, " ")
          this.w_QTAODL = nvl(OLTQTSAL,0)
          this.w_CODMAG = OLTCOMAG
          this.w_STAODL = nvl(OLTSTATO, " " )
          this.w_FLGORD = nvl(OLTFLORD," ")
          this.w_FLGIMP = nvl(OLTFLIMP, " ")
          * --- Azione ...
          this.w_nRecSel = this.w_nRecSel + 1
          do case
            case this.w_PROVEN="L" AND EMPTY(this.w_COFOR)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice fornitore C/Lavoro non definito")
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_CODART)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice articolo non definito")
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            otherwise
              this.w_CAUORD = " "
              * --- Read from PAR_PROD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PPCAUORD"+;
                  " from "+i_cTable+" PAR_PROD where ";
                      +"PPCODICE = "+cp_ToStrODBC("PP");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PPCAUORD;
                  from (i_cTable) where;
                      PPCODICE = "PP";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CAUO_OR = " "
              this.w_CAUO_IM = " "
              * --- Read from CAM_AGAZ
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CMFLORDI,CMFLIMPE"+;
                  " from "+i_cTable+" CAM_AGAZ where ";
                      +"CMCODICE = "+cp_ToStrODBC(this.w_CAUORD);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CMFLORDI,CMFLIMPE;
                  from (i_cTable) where;
                      CMCODICE = this.w_CAUORD;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CAUO_OR = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
                this.w_CAUO_IM = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              do case
                case this.oParentObject.w_TIPGST $ "S-F-H"
                  * --- Conferma ODP/ODR/ODF suggerito
                  if this.oParentObject.w_TIPGST="H"
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("C"),'ODL_MAST','OLTSTATO');
                      +",OLTFLSUG ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLSUG');
                      +",OLTFLDAP ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLDAP');
                      +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
                      +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_OR),'ODL_MAST','OLTFLORD');
                      +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_IM),'ODL_MAST','OLTFLIMP');
                      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCV');
                      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDV');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             )
                    else
                      update (i_cTable) set;
                          OLTSTATO = "C";
                          ,OLTFLSUG = " ";
                          ,OLTFLDAP = "+";
                          ,OLTCAMAG = this.w_CAUORD;
                          ,OLTFLORD = this.w_CAUO_OR;
                          ,OLTFLIMP = this.w_CAUO_IM;
                          ,UTCV = i_CODUTE;
                          ,UTDV = SetInfoDate( g_CALUTD );
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_CODODL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore scrittura ODL_MAST (1)'
                      return
                    endif
                    * --- Write into MPS_TFOR
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"FMMPSSUG =FMMPSSUG- "+cp_ToStrODBC(this.w_QTAODL);
                      +",FMMPSDAP =FMMPSDAP+ "+cp_ToStrODBC(this.w_QTAODL);
                          +i_ccchkf ;
                      +" where ";
                          +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                          +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                             )
                    else
                      update (i_cTable) set;
                          FMMPSSUG = FMMPSSUG - this.w_QTAODL;
                          ,FMMPSDAP = FMMPSDAP + this.w_QTAODL;
                          &i_ccchkf. ;
                       where;
                          FMCODART = this.w_CODICE;
                          and FMPERASS = this.w_PERASS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  else
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("C"),'ODL_MAST','OLTSTATO');
                      +",OLTFLSUG ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLSUG');
                      +",OLTFLCON ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLCON');
                      +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
                      +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_OR),'ODL_MAST','OLTFLORD');
                      +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_IM),'ODL_MAST','OLTFLIMP');
                      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCV');
                      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDV');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             )
                    else
                      update (i_cTable) set;
                          OLTSTATO = "C";
                          ,OLTFLSUG = " ";
                          ,OLTFLCON = "+";
                          ,OLTCAMAG = this.w_CAUORD;
                          ,OLTFLORD = this.w_CAUO_OR;
                          ,OLTFLIMP = this.w_CAUO_IM;
                          ,UTCV = i_CODUTE;
                          ,UTDV = SetInfoDate( g_CALUTD );
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_CODODL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore scrittura ODL_MAST (1)'
                      return
                    endif
                    * --- Write into MPS_TFOR
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"FMMPSSUG =FMMPSSUG- "+cp_ToStrODBC(this.w_QTAODL);
                      +",FMMPSCON =FMMPSCON+ "+cp_ToStrODBC(this.w_QTAODL);
                          +i_ccchkf ;
                      +" where ";
                          +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                          +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                             )
                    else
                      update (i_cTable) set;
                          FMMPSSUG = FMMPSSUG - this.w_QTAODL;
                          ,FMMPSCON = FMMPSCON + this.w_QTAODL;
                          &i_ccchkf. ;
                       where;
                          FMCODART = this.w_CODICE;
                          and FMPERASS = this.w_PERASS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  * --- Aggiorna Saldo (magazzino w_CODMAG definito su ODP) ...
                  * --- Try
                  local bErr_02B58FA0
                  bErr_02B58FA0=bTrsErr
                  this.Try_02B58FA0()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_02B58FA0
                  * --- End
                  * --- Write into SALDIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_CAUO_OR,'SLQTOPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_CAUO_IM,'SLQTIPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                    +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
                        +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                           )
                  else
                    update (i_cTable) set;
                        SLQTOPER = &i_cOp1.;
                        ,SLQTIPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SLCODICE = this.w_CODART;
                        and SLCODMAG = this.w_CODMAG;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Saldi commessa
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_CODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_SALCOM="S"
                    * --- Read from ODL_MAST
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "OLTCOMME"+;
                        " from "+i_cTable+" ODL_MAST where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        OLTCOMME;
                        from (i_cTable) where;
                            OLCODODL = this.w_CODODL;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if empty(nvl(this.w_OLTCOMME,""))
                      this.w_COMMAPPO = this.w_COMMDEFA
                    else
                      this.w_COMMAPPO = this.w_OLTCOMME
                    endif
                    * --- Try
                    local bErr_040BE2E0
                    bErr_040BE2E0=bTrsErr
                    this.Try_040BE2E0()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_040BE2E0
                    * --- End
                    * --- Write into SALDICOM
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDICOM_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                      i_cOp1=cp_SetTrsOp(this.w_CAUO_OR,'SCQTOPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
                      i_cOp2=cp_SetTrsOp(this.w_CAUO_IM,'SCQTIPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                          +i_ccchkf ;
                      +" where ";
                          +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
                          +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                             )
                    else
                      update (i_cTable) set;
                          SCQTOPER = &i_cOp1.;
                          ,SCQTIPER = &i_cOp2.;
                          &i_ccchkf. ;
                       where;
                          SCCODICE = this.w_CODART;
                          and SCCODMAG = this.w_CODMAG;
                          and SCCODCAN = this.w_COMMAPPO;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore Aggiornamento Saldi Commessa'
                      return
                    endif
                  endif
                case this.oParentObject.w_TIPGST $ "C-R-I"
                  * --- Pianificazione ODP/ODR/ODF confermato
                  if this.oParentObject.w_TIPGST="I"
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
                      +",OLTFLSUG ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLSUG');
                      +",OLTFLCON ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLCON');
                      +",OLTFLDAP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLDAP');
                      +",OLTFLPIA ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLPIA');
                      +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
                      +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_OR),'ODL_MAST','OLTFLORD');
                      +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_IM),'ODL_MAST','OLTFLIMP');
                      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCV');
                      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDV');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             )
                    else
                      update (i_cTable) set;
                          OLTSTATO = "P";
                          ,OLTFLSUG = " ";
                          ,OLTFLCON = " ";
                          ,OLTFLDAP = " ";
                          ,OLTFLPIA = "+";
                          ,OLTCAMAG = this.w_CAUORD;
                          ,OLTFLORD = this.w_CAUO_OR;
                          ,OLTFLIMP = this.w_CAUO_IM;
                          ,UTCV = i_CODUTE;
                          ,UTDV = SetInfoDate( g_CALUTD );
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_CODODL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore scrittura ODL_MAST (1)'
                      return
                    endif
                    * --- Write into MPS_TFOR
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"FMMPSDAP =FMMPSDAP- "+cp_ToStrODBC(this.w_QTAODL);
                      +",FMMPSPIA =FMMPSPIA+ "+cp_ToStrODBC(this.w_QTAODL);
                          +i_ccchkf ;
                      +" where ";
                          +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                          +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                             )
                    else
                      update (i_cTable) set;
                          FMMPSDAP = FMMPSDAP - this.w_QTAODL;
                          ,FMMPSPIA = FMMPSPIA + this.w_QTAODL;
                          &i_ccchkf. ;
                       where;
                          FMCODART = this.w_CODICE;
                          and FMPERASS = this.w_PERASS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  else
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("D"),'ODL_MAST','OLTSTATO');
                      +",OLTFLSUG ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLSUG');
                      +",OLTFLCON ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLCON');
                      +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
                      +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_OR),'ODL_MAST','OLTFLORD');
                      +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(this.w_CAUO_IM),'ODL_MAST','OLTFLIMP');
                      +",OLTFLDAP ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLDAP');
                      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCV');
                      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDV');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             )
                    else
                      update (i_cTable) set;
                          OLTSTATO = "D";
                          ,OLTFLSUG = " ";
                          ,OLTFLCON = " ";
                          ,OLTCAMAG = this.w_CAUORD;
                          ,OLTFLORD = this.w_CAUO_OR;
                          ,OLTFLIMP = this.w_CAUO_IM;
                          ,OLTFLDAP = "+";
                          ,UTCV = i_CODUTE;
                          ,UTDV = SetInfoDate( g_CALUTD );
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_CODODL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore scrittura ODL_MAST (1)'
                      return
                    endif
                    * --- Write into MPS_TFOR
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"FMMPSDAP =FMMPSDAP+ "+cp_ToStrODBC(this.w_QTAODL);
                      +",FMMPSCON =FMMPSCON- "+cp_ToStrODBC(this.w_QTAODL);
                          +i_ccchkf ;
                      +" where ";
                          +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                          +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                             )
                    else
                      update (i_cTable) set;
                          FMMPSDAP = FMMPSDAP + this.w_QTAODL;
                          ,FMMPSCON = FMMPSCON - this.w_QTAODL;
                          &i_ccchkf. ;
                       where;
                          FMCODART = this.w_CODICE;
                          and FMPERASS = this.w_PERASS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                otherwise
                  * --- Eliminazione ODP/ODR/ODF
                  if this.oParentObject.w_TIPGST="N"
                    do case
                      case this.w_STAODL="C"
                        * --- Write into MPS_TFOR
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"FMMPSDAP =FMMPSDAP- "+cp_ToStrODBC(this.w_QTAODL);
                          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
                              +i_ccchkf ;
                          +" where ";
                              +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                                 )
                        else
                          update (i_cTable) set;
                              FMMPSDAP = FMMPSDAP - this.w_QTAODL;
                              ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
                              &i_ccchkf. ;
                           where;
                              FMCODART = this.w_CODICE;
                              and FMPERASS = this.w_PERASS;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error=MSG_WRITE_ERROR
                          return
                        endif
                      case this.w_STAODL="S"
                        * --- Write into MPS_TFOR
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"FMMPSSUG =FMMPSSUG- "+cp_ToStrODBC(this.w_QTAODL);
                          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
                              +i_ccchkf ;
                          +" where ";
                              +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                                 )
                        else
                          update (i_cTable) set;
                              FMMPSSUG = FMMPSSUG - this.w_QTAODL;
                              ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
                              &i_ccchkf. ;
                           where;
                              FMCODART = this.w_CODICE;
                              and FMPERASS = this.w_PERASS;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error=MSG_WRITE_ERROR
                          return
                        endif
                    endcase
                  else
                    if this.w_STAODL="L"
                      * --- Write into MPS_TFOR
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_QTAODL);
                        +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
                            +i_ccchkf ;
                        +" where ";
                            +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                            +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                               )
                      else
                        update (i_cTable) set;
                            FMMPSLAN = FMMPSLAN - this.w_QTAODL;
                            ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
                            &i_ccchkf. ;
                         where;
                            FMCODART = this.w_CODICE;
                            and FMPERASS = this.w_PERASS;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    else
                      * --- Stato = 'P'
                      * --- Write into MPS_TFOR
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"FMMPSPIA =FMMPSPIA- "+cp_ToStrODBC(this.w_QTAODL);
                        +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
                            +i_ccchkf ;
                        +" where ";
                            +"FMCODART = "+cp_ToStrODBC(this.w_CODICE);
                            +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                               )
                      else
                        update (i_cTable) set;
                            FMMPSPIA = FMMPSPIA - this.w_QTAODL;
                            ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
                            &i_ccchkf. ;
                         where;
                            FMCODART = this.w_CODICE;
                            and FMPERASS = this.w_PERASS;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    endif
                  endif
                  * --- Write into SALDIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SLQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SLQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                    +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
                        +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                           )
                  else
                    update (i_cTable) set;
                        SLQTOPER = &i_cOp1.;
                        ,SLQTIPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SLCODICE = this.w_CODART;
                        and SLCODMAG = this.w_CODMAG;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Saldi commessa
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_CODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_SALCOM="S"
                    * --- Read from ODL_MAST
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "OLTCOMME"+;
                        " from "+i_cTable+" ODL_MAST where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        OLTCOMME;
                        from (i_cTable) where;
                            OLCODODL = this.w_CODODL;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if empty(nvl(this.w_OLTCOMME,""))
                      this.w_COMMAPPO = this.w_COMMDEFA
                    else
                      this.w_COMMAPPO = this.w_OLTCOMME
                    endif
                    * --- Write into SALDICOM
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDICOM_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                      i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SCQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
                      i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SCQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                          +i_ccchkf ;
                      +" where ";
                          +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
                          +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                             )
                    else
                      update (i_cTable) set;
                          SCQTOPER = &i_cOp1.;
                          ,SCQTIPER = &i_cOp2.;
                          &i_ccchkf. ;
                       where;
                          SCCODICE = this.w_CODART;
                          and SCCODMAG = this.w_CODMAG;
                          and SCCODCAN = this.w_COMMAPPO;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore Aggiornamento Saldi Commessa'
                      return
                    endif
                  endif
                  * --- Delete from ODL_MAST
                  i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                           )
                  else
                    delete from (i_cTable) where;
                          OLCODODL = this.w_CODODL;

                    i_Rows=_tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    * --- Error: delete not accepted
                    i_Error=MSG_DELETE_ERROR
                    return
                  endif
              endcase
              this.w_nRecEla = this.w_nRecEla + 1
          endcase
          SELECT (this.NC)
          ENDSCAN
          this.w_oPart = this.w_oMess.AddMsgPartNL("Elaborazione terminata%0N.%1 records elaborati%0su %2 records selezionati")
          this.w_oPart.AddParam(alltrim(str(this.w_nRecEla,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecSel,5,0)))     
          if USED("MessErr") AND this.w_LNumErr>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?")
            this.w_oPart.AddParam(alltrim(str(this.w_LNumErr,5,0)))     
            if this.w_oMess.ah_YesNo()
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
            endif
          else
            this.w_oMess.ah_ErrorMsg()
          endif
          * --- Chiusura Cursori
          if USED("MessErr")
            SELECT MessErr
            USE
          endif
          if USED("__TMP__")
            SELECT __TMP__
            USE
          endif
          this.oParentObject.NotifyEvent("Interroga")
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare","!","")
        endif
    endcase
  endproc
  proc Try_02B58FA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODART),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_CODART,'SLCODMAG',this.w_CODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_CODART;
           ,this.w_CODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_040BE2E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_CODART,'SCCODMAG',this.w_CODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_CODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_CODART;
           ,this.w_CODMAG;
           ,this.w_COMMAPPO;
           ,this.w_CODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    * --- Scrive LOG
    if EMPTY(this.w_LTesMes)
      this.w_LTesMes = "Message()= "+message()
    endif
    INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
    (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LTesMes)
    this.w_LTesMes = ""
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='CAM_AGAZ'
    this.cWorkTables[4]='MPS_TFOR'
    this.cWorkTables[5]='SALDIART'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='SALDICOM'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
