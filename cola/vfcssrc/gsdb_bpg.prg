* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bpg                                                        *
*              Previsioni di vendita giornaliere                               *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-05                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Tipope
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bpg",oParentObject,m.Tipope)
return(i_retval)

define class tgsdb_bpg as StdBatch
  * --- Local variables
  Tipope = space(1)
  w_OBJS = .NULL.
  w_PVPERIOD = space(1)
  w_PVPERMES = 0
  w_PVPERSET = 0
  w_PVPERGIO = 0
  w_PV__DATA = ctod("  /  /  ")
  w_PVQUANTI = 0
  w_PV__ANNO = 0
  w_KEYSAL = space(40)
  w_MESS = space(1)
  w_OLDQUANT = 0
  w_DATACURS = ctod("  /  /  ")
  w_CICLA = .f.
  w_PUNPADRE = .NULL.
  * --- WorkFile variables
  VEN_PREV_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola le previsioni di vendita giornaliere (da GSDB_KPG)
    * --- Tipo operazione
    * --- queste variabili sono utilizzate tramite le macro di VF (non � necessario definirle)
    this.w_PVPERIOD = "G"
    this.w_PV__ANNO = this.oParentObject.w_ANNOS
    this.w_PV__DATA = this.oParentObject.w_GIOR1D
    this.w_PUNPADRE = this.oParentObject
    do case
      case this.Tipope = "I"
        * --- Interroga
        if this.oParentObject.w_PRESET>0
          if this.oParentObject.w_edits
            this.w_PVPERGIO = 1
            this.w_CICLA = .T.
            do while this.w_CICLA AND this.w_PVPERGIO<=7
              l_GIORind=ALLTRIM(STR(this.w_PVPERGIO,2,0))
              this.w_CICLA = this.w_PUNPADRE.w_GIOR&l_GIORind.T=0
              this.w_PVPERGIO = this.w_PVPERGIO + 1
            enddo
            if this.w_PVPERGIO<8
              l_GIORind=ALLTRIM(STR(this.w_PVPERGIO-1,2,0))
              this.w_PUNPADRE.w_GIOR&l_GIORind = this.oParentObject.w_PRESET
            endif
          else
            * --- Calcola le previsioni giornaliere
            * --- Select from GSDB_BPG
            do vq_exec with 'GSDB_BPG',this,'_Curs_GSDB_BPG','',.f.,.t.
            if used('_Curs_GSDB_BPG')
              select _Curs_GSDB_BPG
              locate for 1=1
              do while not(eof())
              this.w_DATACURS = iif(type("_Curs_GSDB_BPG.PV__DATA")="T",ttod(_Curs_GSDB_BPG.PV__DATA),_Curs_GSDB_BPG.PV__DATA )
              this.w_PVPERGIO = this.w_DATACURS - this.oParentObject.w_GIOR1D + 1
              this.w_PVQUANTI = _Curs_GSDB_BPG.PVQUANTI
              l_GIORind=ALLTRIM(STR(this.w_PVPERGIO,2,0))
              this.w_PUNPADRE.w_GIOR&l_GIORind = this.w_PVQUANTI
              this.w_PUNPADRE.w_GIOR&l_GIORind.O = this.w_PVQUANTI
                select _Curs_GSDB_BPG
                continue
              enddo
              use
            endif
          endif
        endif
      case this.Tipope = "A"
        * --- Puntatore alla maschera GSDB_KPS
        this.w_OBJS = this.oParentObject.oParentObject.oParentObject
        * --- Aggiorna le previsioni giornaliere
        if this.oParentObject.w_edits AND this.oParentObject.w_PRESET<>0 AND this.oParentObject.w_RIPARTITA <> this.oParentObject.w_PRESET
          this.w_MESS = "La somma delle previsioni giornaliere � diversa da quella settimanale%0Continuo?"
          if not ah_YesNo(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Try
        local bErr_03930E60
        bErr_03930E60=bTrsErr
        this.Try_03930E60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Impossibile aggiornare il database%0%1",16,"",message()) 
        endif
        bTrsErr=bTrsErr or bErr_03930E60
        * --- End
        this.oParentObject.w_PRESET = this.oParentObject.w_RIPARTITA
        this.oParentObject.w_edits = (this.oParentObject.w_PRESET = 0)
        this.w_PUNPADRE.lockscreen = .t.
        this.w_PUNPADRE.mReplace(.t.)     
        this.w_PUNPADRE.lockscreen = .f.
        this.w_PUNPADRE.ecpquit()     
    endcase
  endproc
  proc Try_03930E60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Se la settimana � editabile si deve cancellare la previsione
    if this.oParentObject.w_edits
      * --- Calcola la settimana corrente
      do GSDB_BCG with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcola il primo giorno della settimana
      this.w_PVPERIOD = "S"
      GSDB_BCG(this,.T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if EMPTY(this.w_PV__DATA)
        this.w_PV__DATA = this.oParentObject.w_GIOR1D
      else
        * --- Delete from VEN_PREV
        i_nConn=i_TableProp[this.VEN_PREV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTS);
                 )
        else
          delete from (i_cTable) where;
                PV__DATA = this.w_PV__DATA;
                and PVCODART = this.oParentObject.w_CODARTS;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Se esiste una previsione mensile la cancella
      this.w_PVPERIOD = "M"
      GSDB_BCG(this,.T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Read from VEN_PREV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VEN_PREV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PVPERIOD,PVQUANTI"+;
          " from "+i_cTable+" VEN_PREV where ";
              +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
              +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PVPERIOD,PVQUANTI;
          from (i_cTable) where;
              PV__DATA = this.w_PV__DATA;
              and PVCODART = this.oParentObject.w_CODARTS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PVPERIOD = NVL(cp_ToDate(_read_.PVPERIOD),cp_NullValue(_read_.PVPERIOD))
        this.w_PVQUANTI = NVL(cp_ToDate(_read_.PVQUANTI),cp_NullValue(_read_.PVQUANTI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_PVPERIOD="M" and this.w_PVQUANTI>0
        * --- Delete from VEN_PREV
        i_nConn=i_TableProp[this.VEN_PREV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTS);
                 )
        else
          delete from (i_cTable) where;
                PV__DATA = this.w_PV__DATA;
                and PVCODART = this.oParentObject.w_CODARTS;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    this.w_PVPERIOD = "G"
    this.w_PV__DATA = this.oParentObject.w_GIOR1D
    do GSDB_BCG with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PVPERGIO = 7
    do while this.w_PVPERGIO>0
      l_GIORind = ALLTRIM(STR(this.w_PVPERGIO,2,0))
      this.w_PVQUANTI = this.w_PUNPADRE.w_GIOR&l_GIORind
      this.w_OLDQUANT = this.w_PUNPADRE.w_GIOR&l_GIORind.O
      this.w_PV__DATA = this.oParentObject.w_GIOR1D + this.w_PVPERGIO - 1
      if this.w_PVQUANTI <> this.w_OLDQUANT
        if this.w_PVQUANTI = 0
          * --- Delete from VEN_PREV
          i_nConn=i_TableProp[this.VEN_PREV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                  +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTS);
                   )
          else
            delete from (i_cTable) where;
                  PV__DATA = this.w_PV__DATA;
                  and PVCODART = this.oParentObject.w_CODARTS;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        else
          * --- Try
          local bErr_039315E0
          bErr_039315E0=bTrsErr
          this.Try_039315E0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_039315E0
          * --- End
          * --- Write into VEN_PREV
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.VEN_PREV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_PREV_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PVQUANTI ="+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
                +i_ccchkf ;
            +" where ";
                +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTS);
                   )
          else
            update (i_cTable) set;
                PVQUANTI = this.w_PVQUANTI;
                &i_ccchkf. ;
             where;
                PV__DATA = this.w_PV__DATA;
                and PVCODART = this.oParentObject.w_CODARTS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_OBJS.w_EDITM = .f.
        endif
        this.w_PUNPADRE.w_GIOR&l_GIORind.O = this.w_PVQUANTI 
      endif
      this.w_PVPERGIO = this.w_PVPERGIO - 1 
    enddo
    * --- commit
    cp_EndTrs(.t.)
    this.w_OBJS.NotifyEvent("Init")     
    return
  proc Try_039315E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VEN_PREV
    i_nConn=i_TableProp[this.VEN_PREV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VEN_PREV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PV__DATA"+",PVCODART"+",PVPERIOD"+",PVPERMES"+",PVQUANTI"+",PV__ANNO"+",PVPERSET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PV__DATA),'VEN_PREV','PV__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODARTS),'VEN_PREV','PVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERIOD),'VEN_PREV','PVPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERMES),'VEN_PREV','PVPERMES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PV__ANNO),'VEN_PREV','PV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERSET),'VEN_PREV','PVPERSET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PV__DATA',this.w_PV__DATA,'PVCODART',this.oParentObject.w_CODARTS,'PVPERIOD',this.w_PVPERIOD,'PVPERMES',this.w_PVPERMES,'PVQUANTI',this.w_PVQUANTI,'PV__ANNO',this.w_PV__ANNO,'PVPERSET',this.w_PVPERSET)
      insert into (i_cTable) (PV__DATA,PVCODART,PVPERIOD,PVPERMES,PVQUANTI,PV__ANNO,PVPERSET &i_ccchkf. );
         values (;
           this.w_PV__DATA;
           ,this.oParentObject.w_CODARTS;
           ,this.w_PVPERIOD;
           ,this.w_PVPERMES;
           ,this.w_PVQUANTI;
           ,this.w_PV__ANNO;
           ,this.w_PVPERSET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,Tipope)
    this.Tipope=Tipope
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VEN_PREV'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSDB_BPG')
      use in _Curs_GSDB_BPG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Tipope"
endproc
