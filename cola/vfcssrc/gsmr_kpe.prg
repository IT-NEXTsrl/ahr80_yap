* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_kpe                                                        *
*              Pegging di secondo livello                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_192]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-15                                                      *
* Last revis.: 2015-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_kpe",oParentObject))

* --- Class definition
define class tgsmr_kpe as StdForm
  Top    = -2
  Left   = 3

  * --- Standard Properties
  Width  = 826
  Height = 531+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-13"
  HelpContextID=149515113
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=79

  * --- Constant Properties
  _IDX = 0
  ODL_MAST_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  DISMBASE_IDX = 0
  GRUMERC_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  KEY_ARTI_IDX = 0
  AZIENDA_IDX = 0
  DOC_MAST_IDX = 0
  cPrg = "gsmr_kpe"
  cComment = "Pegging di secondo livello"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLSELEZ = space(1)
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_TIPDOC = space(5)
  w_CODMAG = space(5)
  w_NUMINI = 0
  w_SERIE1 = space(10)
  w_NUMFIN = 0
  w_SERIE2 = space(10)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  o_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_FORSEL = space(15)
  w_FORSEL = space(15)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_TIPGES = space(1)
  w_TIPOARTI = space(2)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_OLCODINI = space(15)
  w_OLCODFIN = space(15)
  w_OLCODINI = space(15)
  w_OLCODFIN = space(15)
  w_COMODL = space(15)
  w_PEGG = space(2)
  o_PEGG = space(2)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESDISI = space(40)
  w_DESDISF = space(40)
  w_TIPATT = space(1)
  w_DESFOR = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_DESDOC = space(35)
  w_CODAZI = space(5)
  w_PROVE = space(1)
  w_ST = space(1)
  w_STATO = space(10)
  w_COLORE = space(50)
  w_CODODL = space(15)
  w_CODRIC = space(41)
  w_CADESAR = space(40)
  w_QTAORD = 0
  w_UNIMIS = space(3)
  w_DATEVA = ctod('  /  /  ')
  w_QTAEVA = 0
  w_QTARES = 0
  w_MAGAZ = space(5)
  w_AZIENDA = space(5)
  w_gestbu = space(1)
  w_TIPFOR = space(1)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_FLVEAC1 = space(1)
  w_CLADOC = space(2)
  w_ROWNUM = 0
  w_CODART = space(20)
  w_PEGPLI = space(1)
  w_CODODL1 = space(10)
  w_ST1 = space(1)
  w_STATOR = space(15)
  w_TUTTI = space(1)
  o_TUTTI = space(1)
  w_FATTIBILI = space(1)
  w_NFATTIBILI = space(1)
  w_PFATTIBILI = space(1)
  w_DESCOM = space(30)
  w_TREEV = .NULL.
  w_statoODL = .NULL.
  w_TRFLGHT = .NULL.
  w_StatoDoc = .NULL.
  w_TRFLGHT1 = .NULL.
  w_TRFLGHT3 = .NULL.
  w_TRFLGHT2 = .NULL.
  w_NFAT = .NULL.
  w_FATT = .NULL.
  w_PFAT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsmr_kpe
  *Abilita le scrollbar
  ScrollBars=3
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_kpePag1","gsmr_kpe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsmr_kpePag2","gsmr_kpe",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tree-view padre-figlio")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLSELEZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEV = this.oPgFrm.Pages(2).oPag.TREEV
    this.w_statoODL = this.oPgFrm.Pages(2).oPag.statoODL
    this.w_TRFLGHT = this.oPgFrm.Pages(2).oPag.TRFLGHT
    this.w_StatoDoc = this.oPgFrm.Pages(2).oPag.StatoDoc
    this.w_TRFLGHT1 = this.oPgFrm.Pages(1).oPag.TRFLGHT1
    this.w_TRFLGHT3 = this.oPgFrm.Pages(1).oPag.TRFLGHT3
    this.w_TRFLGHT2 = this.oPgFrm.Pages(1).oPag.TRFLGHT2
    this.w_NFAT = this.oPgFrm.Pages(2).oPag.NFAT
    this.w_FATT = this.oPgFrm.Pages(2).oPag.FATT
    this.w_PFAT = this.oPgFrm.Pages(2).oPag.PFAT
    DoDefault()
    proc Destroy()
      this.w_TREEV = .NULL.
      this.w_statoODL = .NULL.
      this.w_TRFLGHT = .NULL.
      this.w_StatoDoc = .NULL.
      this.w_TRFLGHT1 = .NULL.
      this.w_TRFLGHT3 = .NULL.
      this.w_TRFLGHT2 = .NULL.
      this.w_NFAT = .NULL.
      this.w_FATT = .NULL.
      this.w_PFAT = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='ATTIVITA'
    this.cWorkTables[5]='FAM_ARTI'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='DISMBASE'
    this.cWorkTables[8]='GRUMERC'
    this.cWorkTables[9]='CONTI'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='AZIENDA'
    this.cWorkTables[13]='DOC_MAST'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLSELEZ=space(1)
      .w_FLVEAC=space(1)
      .w_TIPDOC=space(5)
      .w_CODMAG=space(5)
      .w_NUMINI=0
      .w_SERIE1=space(10)
      .w_NUMFIN=0
      .w_SERIE2=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_FORSEL=space(15)
      .w_FORSEL=space(15)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_DBCODINI=space(20)
      .w_DBCODFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPGES=space(1)
      .w_TIPOARTI=space(2)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_OLCODINI=space(15)
      .w_OLCODFIN=space(15)
      .w_OLCODINI=space(15)
      .w_OLCODFIN=space(15)
      .w_COMODL=space(15)
      .w_PEGG=space(2)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESDISI=space(40)
      .w_DESDISF=space(40)
      .w_TIPATT=space(1)
      .w_DESFOR=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESDOC=space(35)
      .w_CODAZI=space(5)
      .w_PROVE=space(1)
      .w_ST=space(1)
      .w_STATO=space(10)
      .w_COLORE=space(50)
      .w_CODODL=space(15)
      .w_CODRIC=space(41)
      .w_CADESAR=space(40)
      .w_QTAORD=0
      .w_UNIMIS=space(3)
      .w_DATEVA=ctod("  /  /  ")
      .w_QTAEVA=0
      .w_QTARES=0
      .w_MAGAZ=space(5)
      .w_AZIENDA=space(5)
      .w_gestbu=space(1)
      .w_TIPFOR=space(1)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_FLVEAC1=space(1)
      .w_CLADOC=space(2)
      .w_ROWNUM=0
      .w_CODART=space(20)
      .w_PEGPLI=space(1)
      .w_CODODL1=space(10)
      .w_ST1=space(1)
      .w_STATOR=space(15)
      .w_TUTTI=space(1)
      .w_FATTIBILI=space(1)
      .w_NFATTIBILI=space(1)
      .w_PFATTIBILI=space(1)
      .w_DESCOM=space(30)
        .w_FLSELEZ = "T"
        .w_FLVEAC = "T"
        .w_TIPDOC = ' '
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODMAG))
          .link_1_4('Full')
        endif
        .w_NUMINI = 1
        .w_SERIE1 = ''
        .w_NUMFIN = 999999999999999
        .w_SERIE2 = ''
          .DoRTCalc(9,9,.f.)
        .w_DATFIN = .w_DATINI
          .DoRTCalc(11,11,.f.)
        .w_DOCFIN = .w_DOCINI
        .w_FORSEL = ' '
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_FORSEL))
          .link_1_14('Full')
        endif
        .w_FORSEL = ' '
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_FORSEL))
          .link_1_15('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODCOM))
          .link_1_16('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODATT))
          .link_1_17('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DBCODINI))
          .link_1_18('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_DBCODFIN))
          .link_1_19('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_20('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_21('Full')
        endif
        .w_OBTEST = i_DATSYS
        .w_TIPGES = "G"
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_GRUINI))
          .link_1_25('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_26('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CATINI))
          .link_1_27('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CATFIN))
          .link_1_28('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_OLCODINI))
          .link_1_29('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_OLCODFIN))
          .link_1_30('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_OLCODINI))
          .link_1_31('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_OLCODFIN))
          .link_1_32('Full')
        endif
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_COMODL))
          .link_1_33('Full')
        endif
        .w_PEGG = 'PF'
          .DoRTCalc(34,43,.f.)
        .w_TIPATT = "A"
      .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
          .DoRTCalc(45,47,.f.)
        .w_CODAZI = i_codazi
      .oPgFrm.Page2.oPag.TREEV.Calculate()
        .w_PROVE = .w_TREEV.getvar("PROVE")
        .w_ST = .w_TREEV.getvar("OLTSTATO1")
        .w_STATO = iif(.w_ST='M','Suggerito',iif(.w_ST='P','Pianificato',iif(.w_ST='L',iif(.w_prove<>'L','Lanciato','Ordinato'),iif(.w_ST='C','Confermato',iif(.w_ST='D','Da Pianificare',iif(.w_ST='S','Suggerito',iif(.w_ST='F','Finito','')))))))
      .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .w_COLORE = iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'X') $ 'M-S','bmp/Lanciato.bmp',iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'X') $ 'P-C-D','bmp/Pianificato.bmp',iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'X')='L','bmp/Suggerito.bmp','')))
        .w_CODODL = iif(.w_PEGG='FP',.w_TREEV.getvar("PERIFODL"),iif(.w_TREEV.getvar("OLTSTATO1")='R' and .w_TREEV.getvar("LIVELLO")=1,.w_TREEv.getvar("PERIFODL"),.w_TREEV.getvar("PESERODL")))
        .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC1"),'')
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CODRIC))
          .link_2_9('Full')
        endif
      .oPgFrm.Page2.oPag.statoODL.Calculate(.w_STATO)
      .oPgFrm.Page2.oPag.TRFLGHT.Calculate(.w_colore)
          .DoRTCalc(55,55,.f.)
        .w_QTAORD = .w_TREEV.getvar("OLTQTOD1")
        .w_UNIMIS = .w_TREEV.getvar("ARUNMIS1")
        .w_DATEVA = .w_TREEV.getvar("OLTDTRIC1")
        .w_QTAEVA = .w_TREEV.getvar("OLTQTOE1")
        .w_QTARES = nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0)
        .w_MAGAZ = .w_TREEV.getvar("OLTCOMAG")
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_75('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
          .DoRTCalc(63,63,.f.)
        .w_TIPFOR = iif(.w_FLVEAC='V','C',iif(.w_FLVEAC='A','F','T'))
        .w_NUMDOC = .w_TREEV.getvar("NUMDOC")
        .w_ALFDOC = .w_TREEV.getvar("ALFDOC")
      .oPgFrm.Page2.oPag.oObj_2_34.Calculate()
        .w_FLVEAC1 = .w_TREEV.getvar("FLVEAC")
        .w_CLADOC = .w_TREEV.getvar("CLADOC")
        .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
      .oPgFrm.Page2.oPag.oObj_2_39.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_40.Calculate()
          .DoRTCalc(70,70,.f.)
        .w_PEGPLI = 'N'
        .w_CODODL1 = iif(.w_PEGG='FP',nvl(.w_TREEV.getvar("PERIFODL"),''),iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'')='R' and nvl(.w_TREEV.getvar("LIVELLO"),0)=1,nvl(.w_TREEv.getvar("PERIFODL"),''),nvl(.w_TREEV.getvar("PESERODL"),'')))
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_CODODL1))
          .link_2_41('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_42.Calculate()
          .DoRTCalc(73,73,.f.)
        .w_STATOR = iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N','Confermato',''))
      .oPgFrm.Page2.oPag.StatoDoc.Calculate(.w_STATOR)
        .w_TUTTI = 'T'
        .w_FATTIBILI = iif(.w_TUTTI='T','F',.w_FATTIBILI)
        .w_NFATTIBILI = iif(.w_TUTTI='T','N',.w_NFATTIBILI)
        .w_PFATTIBILI = iif(.w_TUTTI='T','P',.w_PFATTIBILI)
      .oPgFrm.Page1.oPag.TRFLGHT1.Calculate()
      .oPgFrm.Page1.oPag.TRFLGHT3.Calculate()
      .oPgFrm.Page1.oPag.TRFLGHT2.Calculate()
      .oPgFrm.Page2.oPag.NFAT.Calculate(iif(nvl(.w_NFATTIBILI,'')='N'and .w_PEGG='PF',AH_MsgFormat("Ordini non fattibili"),' '),rgb(255,0,0))
      .oPgFrm.Page2.oPag.FATT.Calculate(iif(nvl(.w_FATTIBILI,'')='F' and .w_PEGG='PF',AH_MsgFormat("Ordini fattibili"),' '),rgb(0,255,100))
      .oPgFrm.Page2.oPag.PFAT.Calculate(iif(nvl(.w_PFATTIBILI,'')='P' and .w_PEGG='PF',AH_MsgFormat("Ordini parzialmente fattibili"),' '),rgb(250,250,100))
    endwith
    this.DoRTCalc(79,79,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_27.enabled = this.oPgFrm.Page2.oPag.oBtn_2_27.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_49.enabled = this.oPgFrm.Page2.oPag.oBtn_2_49.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_TIPDOC = ' '
          .link_1_3('Full')
        endif
        .DoRTCalc(4,9,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = .w_DATINI
        endif
        .DoRTCalc(11,11,.t.)
        if .o_DOCINI<>.w_DOCINI
            .w_DOCFIN = .w_DOCINI
        endif
        if .o_FLVEAC<>.w_FLVEAC
            .w_FORSEL = ' '
          .link_1_14('Full')
        endif
        if .o_FLVEAC<>.w_FLVEAC
            .w_FORSEL = ' '
          .link_1_15('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.TREEV.Calculate()
        .DoRTCalc(15,48,.t.)
            .w_PROVE = .w_TREEV.getvar("PROVE")
            .w_ST = .w_TREEV.getvar("OLTSTATO1")
            .w_STATO = iif(.w_ST='M','Suggerito',iif(.w_ST='P','Pianificato',iif(.w_ST='L',iif(.w_prove<>'L','Lanciato','Ordinato'),iif(.w_ST='C','Confermato',iif(.w_ST='D','Da Pianificare',iif(.w_ST='S','Suggerito',iif(.w_ST='F','Finito','')))))))
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
            .w_COLORE = iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'X') $ 'M-S','bmp/Lanciato.bmp',iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'X') $ 'P-C-D','bmp/Pianificato.bmp',iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'X')='L','bmp/Suggerito.bmp','')))
            .w_CODODL = iif(.w_PEGG='FP',.w_TREEV.getvar("PERIFODL"),iif(.w_TREEV.getvar("OLTSTATO1")='R' and .w_TREEV.getvar("LIVELLO")=1,.w_TREEv.getvar("PERIFODL"),.w_TREEV.getvar("PESERODL")))
            .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC1"),'')
          .link_2_9('Full')
        .oPgFrm.Page2.oPag.statoODL.Calculate(.w_STATO)
        .oPgFrm.Page2.oPag.TRFLGHT.Calculate(.w_colore)
        .DoRTCalc(55,55,.t.)
            .w_QTAORD = .w_TREEV.getvar("OLTQTOD1")
            .w_UNIMIS = .w_TREEV.getvar("ARUNMIS1")
            .w_DATEVA = .w_TREEV.getvar("OLTDTRIC1")
            .w_QTAEVA = .w_TREEV.getvar("OLTQTOE1")
            .w_QTARES = nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0)
            .w_MAGAZ = .w_TREEV.getvar("OLTCOMAG")
          .link_1_75('Full')
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .DoRTCalc(63,63,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_TIPFOR = iif(.w_FLVEAC='V','C',iif(.w_FLVEAC='A','F','T'))
        endif
            .w_NUMDOC = .w_TREEV.getvar("NUMDOC")
            .w_ALFDOC = .w_TREEV.getvar("ALFDOC")
        .oPgFrm.Page2.oPag.oObj_2_34.Calculate()
            .w_FLVEAC1 = .w_TREEV.getvar("FLVEAC")
            .w_CLADOC = .w_TREEV.getvar("CLADOC")
            .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
        .oPgFrm.Page2.oPag.oObj_2_39.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_40.Calculate()
        .DoRTCalc(70,71,.t.)
            .w_CODODL1 = iif(.w_PEGG='FP',nvl(.w_TREEV.getvar("PERIFODL"),''),iif(nvl(.w_TREEV.getvar("OLTSTATO1"),'')='R' and nvl(.w_TREEV.getvar("LIVELLO"),0)=1,nvl(.w_TREEv.getvar("PERIFODL"),''),nvl(.w_TREEV.getvar("PESERODL"),'')))
          .link_2_41('Full')
        .oPgFrm.Page2.oPag.oObj_2_42.Calculate()
        .DoRTCalc(73,73,.t.)
            .w_STATOR = iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N','Confermato',''))
        .oPgFrm.Page2.oPag.StatoDoc.Calculate(.w_STATOR)
        if .o_PEGG<>.w_PEGG
            .w_TUTTI = 'T'
        endif
        if .o_PEGG<>.w_PEGG.or. .o_TUTTI<>.w_TUTTI
            .w_FATTIBILI = iif(.w_TUTTI='T','F',.w_FATTIBILI)
        endif
        if .o_PEGG<>.w_PEGG.or. .o_TUTTI<>.w_TUTTI
            .w_NFATTIBILI = iif(.w_TUTTI='T','N',.w_NFATTIBILI)
        endif
        if .o_PEGG<>.w_PEGG.or. .o_TUTTI<>.w_TUTTI
            .w_PFATTIBILI = iif(.w_TUTTI='T','P',.w_PFATTIBILI)
        endif
        .oPgFrm.Page1.oPag.TRFLGHT1.Calculate()
        .oPgFrm.Page1.oPag.TRFLGHT3.Calculate()
        .oPgFrm.Page1.oPag.TRFLGHT2.Calculate()
        .oPgFrm.Page2.oPag.NFAT.Calculate(iif(nvl(.w_NFATTIBILI,'')='N'and .w_PEGG='PF',AH_MsgFormat("Ordini non fattibili"),' '),rgb(255,0,0))
        .oPgFrm.Page2.oPag.FATT.Calculate(iif(nvl(.w_FATTIBILI,'')='F' and .w_PEGG='PF',AH_MsgFormat("Ordini fattibili"),' '),rgb(0,255,100))
        .oPgFrm.Page2.oPag.PFAT.Calculate(iif(nvl(.w_PFATTIBILI,'')='P' and .w_PEGG='PF',AH_MsgFormat("Ordini parzialmente fattibili"),' '),rgb(250,250,100))
        * --- Area Manuale = Calculate
        * --- gsmr_kpe
        .oPgFrm.Page2.oPag.NFAT.fontbold=.T.
        .oPgFrm.Page2.oPag.FATT.fontbold=.T.
        .oPgFrm.Page2.oPag.PFAT.fontbold=.T.
        
        local l_obj
        l_obj= this.getctrl('StatoODL')
        if type('l_obj.tooltiptext')='C'
        l_obj.tooltiptext=iif(empty(this.w_STATO),'','StatoODL')
        endif
        
        local l_obj1
        l_obj1= this.getctrl('StatoDoc')
        if type('l_obj1.tooltiptext')='C'
        l_obj1.tooltiptext=iif(empty(this.w_STATOR),'','StatoDocumento')
        endif
        
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(79,79,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.TREEV.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.statoODL.Calculate(.w_STATO)
        .oPgFrm.Page2.oPag.TRFLGHT.Calculate(.w_colore)
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_39.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page2.oPag.StatoDoc.Calculate(.w_STATOR)
        .oPgFrm.Page1.oPag.TRFLGHT1.Calculate()
        .oPgFrm.Page1.oPag.TRFLGHT3.Calculate()
        .oPgFrm.Page1.oPag.TRFLGHT2.Calculate()
        .oPgFrm.Page2.oPag.NFAT.Calculate(iif(nvl(.w_NFATTIBILI,'')='N'and .w_PEGG='PF',AH_MsgFormat("Ordini non fattibili"),' '),rgb(255,0,0))
        .oPgFrm.Page2.oPag.FATT.Calculate(iif(nvl(.w_FATTIBILI,'')='F' and .w_PEGG='PF',AH_MsgFormat("Ordini fattibili"),' '),rgb(0,255,100))
        .oPgFrm.Page2.oPag.PFAT.Calculate(iif(nvl(.w_PFATTIBILI,'')='P' and .w_PEGG='PF',AH_MsgFormat("Ordini parzialmente fattibili"),' '),rgb(250,250,100))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_16.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_17.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCOMODL_1_33.enabled = this.oPgFrm.Page1.oPag.oCOMODL_1_33.mCond()
    this.oPgFrm.Page1.oPag.oFATTIBILI_1_88.enabled = this.oPgFrm.Page1.oPag.oFATTIBILI_1_88.mCond()
    this.oPgFrm.Page1.oPag.oNFATTIBILI_1_89.enabled = this.oPgFrm.Page1.oPag.oNFATTIBILI_1_89.mCond()
    this.oPgFrm.Page1.oPag.oPFATTIBILI_1_90.enabled = this.oPgFrm.Page1.oPag.oPFATTIBILI_1_90.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFORSEL_1_14.visible=!this.oPgFrm.Page1.oPag.oFORSEL_1_14.mHide()
    this.oPgFrm.Page1.oPag.oFORSEL_1_15.visible=!this.oPgFrm.Page1.oPag.oFORSEL_1_15.mHide()
    this.oPgFrm.Page1.oPag.oOLCODINI_1_29.visible=!this.oPgFrm.Page1.oPag.oOLCODINI_1_29.mHide()
    this.oPgFrm.Page1.oPag.oOLCODFIN_1_30.visible=!this.oPgFrm.Page1.oPag.oOLCODFIN_1_30.mHide()
    this.oPgFrm.Page1.oPag.oOLCODINI_1_31.visible=!this.oPgFrm.Page1.oPag.oOLCODINI_1_31.mHide()
    this.oPgFrm.Page1.oPag.oOLCODFIN_1_32.visible=!this.oPgFrm.Page1.oPag.oOLCODFIN_1_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page2.oPag.oCODODL_2_8.visible=!this.oPgFrm.Page2.oPag.oCODODL_2_8.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oQTAORD_2_16.visible=!this.oPgFrm.Page2.oPag.oQTAORD_2_16.mHide()
    this.oPgFrm.Page2.oPag.oUNIMIS_2_17.visible=!this.oPgFrm.Page2.oPag.oUNIMIS_2_17.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oDATEVA_2_20.visible=!this.oPgFrm.Page2.oPag.oDATEVA_2_20.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oQTAEVA_2_22.visible=!this.oPgFrm.Page2.oPag.oQTAEVA_2_22.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.oQTARES_2_24.visible=!this.oPgFrm.Page2.oPag.oQTARES_2_24.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page2.oPag.oMAGAZ_2_26.visible=!this.oPgFrm.Page2.oPag.oMAGAZ_2_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_29.visible=!this.oPgFrm.Page2.oPag.oStr_2_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page2.oPag.oNUMDOC_2_30.visible=!this.oPgFrm.Page2.oPag.oNUMDOC_2_30.mHide()
    this.oPgFrm.Page2.oPag.oALFDOC_2_31.visible=!this.oPgFrm.Page2.oPag.oALFDOC_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_33.visible=!this.oPgFrm.Page2.oPag.oStr_2_33.mHide()
    this.oPgFrm.Page2.oPag.oCODODL1_2_41.visible=!this.oPgFrm.Page2.oPag.oCODODL1_2_41.mHide()
    this.oPgFrm.Page1.oPag.oTUTTI_1_87.visible=!this.oPgFrm.Page1.oPag.oTUTTI_1_87.mHide()
    this.oPgFrm.Page1.oPag.oFATTIBILI_1_88.visible=!this.oPgFrm.Page1.oPag.oFATTIBILI_1_88.mHide()
    this.oPgFrm.Page1.oPag.oNFATTIBILI_1_89.visible=!this.oPgFrm.Page1.oPag.oNFATTIBILI_1_89.mHide()
    this.oPgFrm.Page1.oPag.oPFATTIBILI_1_90.visible=!this.oPgFrm.Page1.oPag.oPFATTIBILI_1_90.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_1.Event(cEvent)
      .oPgFrm.Page2.oPag.TREEV.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page2.oPag.statoODL.Event(cEvent)
      .oPgFrm.Page2.oPag.TRFLGHT.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_34.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_39.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_40.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_42.Event(cEvent)
      .oPgFrm.Page2.oPag.StatoDoc.Event(cEvent)
      .oPgFrm.Page1.oPag.TRFLGHT1.Event(cEvent)
      .oPgFrm.Page1.oPag.TRFLGHT3.Event(cEvent)
      .oPgFrm.Page1.oPag.TRFLGHT2.Event(cEvent)
      .oPgFrm.Page2.oPag.NFAT.Event(cEvent)
      .oPgFrm.Page2.oPag.FATT.Event(cEvent)
      .oPgFrm.Page2.oPag.PFAT.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_3'),i_cWhere,'',"Causali documenti",'GSMR_ZCD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_4'),i_cWhere,'',"Magazzini",'GSDB_SCG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_1_14'),i_cWhere,'',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_1_15'),i_cWhere,'',"Conti",'GSCO_ZCF.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',oSource.xKey(1))
            select ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',this.w_FORSEL)
            select ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and (looktab('CONTI','ANTIPCON','ANCODICE',.w_FORSEL) $ 'C-F')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_16'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_17'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODINI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODINI_1_18'),i_cWhere,'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODFIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODFIN_1_19'),i_cWhere,'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_20'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_21'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_25'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_26'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_27'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_28'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODINI
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODINI_1_29'),i_cWhere,'',"",'gsmr_zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODFIN
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODFIN_1_30'),i_cWhere,'',"",'gsmr_zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODINI
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODINI_1_31'),i_cWhere,'',"",'gsmr1zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODFIN
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODFIN_1_32'),i_cWhere,'',"",'gsmr1zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMODL
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMODL)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMODL))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMODL)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMODL)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMODL)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMODL) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMODL_1_33'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMODL)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMODL = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMODL = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIC
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODRIC)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIC = NVL(_Link_.CACODICE,space(41))
      this.w_CADESAR = NVL(_Link_.CADESART,space(40))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIC = space(41)
      endif
      this.w_CADESAR = space(40)
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZIENDA
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODODL1
  func Link_2_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODODL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODODL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_CODODL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_CODODL1)
            select MVSERIAL,MVFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODODL1 = NVL(_Link_.MVSERIAL,space(10))
      this.w_ST1 = NVL(_Link_.MVFLPROV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODODL1 = space(10)
      endif
      this.w_ST1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODODL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLSELEZ_1_1.RadioValue()==this.w_FLSELEZ)
      this.oPgFrm.Page1.oPag.oFLSELEZ_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_2.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_3.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_3.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_4.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_4.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_5.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_5.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_6.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_6.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_7.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_7.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_8.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_8.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_12.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_12.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_13.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_13.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFORSEL_1_14.value==this.w_FORSEL)
      this.oPgFrm.Page1.oPag.oFORSEL_1_14.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oFORSEL_1_15.value==this.w_FORSEL)
      this.oPgFrm.Page1.oPag.oFORSEL_1_15.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_16.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_16.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_17.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_17.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODINI_1_18.value==this.w_DBCODINI)
      this.oPgFrm.Page1.oPag.oDBCODINI_1_18.value=this.w_DBCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODFIN_1_19.value==this.w_DBCODFIN)
      this.oPgFrm.Page1.oPag.oDBCODFIN_1_19.value=this.w_DBCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_20.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_20.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_21.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_21.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_25.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_25.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_26.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_26.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_27.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_27.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_28.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_28.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODINI_1_29.value==this.w_OLCODINI)
      this.oPgFrm.Page1.oPag.oOLCODINI_1_29.value=this.w_OLCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODFIN_1_30.value==this.w_OLCODFIN)
      this.oPgFrm.Page1.oPag.oOLCODFIN_1_30.value=this.w_OLCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODINI_1_31.value==this.w_OLCODINI)
      this.oPgFrm.Page1.oPag.oOLCODINI_1_31.value=this.w_OLCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODFIN_1_32.value==this.w_OLCODFIN)
      this.oPgFrm.Page1.oPag.oOLCODFIN_1_32.value=this.w_OLCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMODL_1_33.value==this.w_COMODL)
      this.oPgFrm.Page1.oPag.oCOMODL_1_33.value=this.w_COMODL
    endif
    if not(this.oPgFrm.Page1.oPag.oPEGG_1_34.RadioValue()==this.w_PEGG)
      this.oPgFrm.Page1.oPag.oPEGG_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_42.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_42.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_44.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_44.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_45.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_45.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_46.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_46.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_47.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_47.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_51.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_51.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_52.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_52.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_53.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_53.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISI_1_57.value==this.w_DESDISI)
      this.oPgFrm.Page1.oPag.oDESDISI_1_57.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISF_1_58.value==this.w_DESDISF)
      this.oPgFrm.Page1.oPag.oDESDISF_1_58.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_65.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_65.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_68.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_68.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODODL_2_8.value==this.w_CODODL)
      this.oPgFrm.Page2.oPag.oCODODL_2_8.value=this.w_CODODL
    endif
    if not(this.oPgFrm.Page2.oPag.oCODRIC_2_9.value==this.w_CODRIC)
      this.oPgFrm.Page2.oPag.oCODRIC_2_9.value=this.w_CODRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oCADESAR_2_10.value==this.w_CADESAR)
      this.oPgFrm.Page2.oPag.oCADESAR_2_10.value=this.w_CADESAR
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAORD_2_16.value==this.w_QTAORD)
      this.oPgFrm.Page2.oPag.oQTAORD_2_16.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page2.oPag.oUNIMIS_2_17.value==this.w_UNIMIS)
      this.oPgFrm.Page2.oPag.oUNIMIS_2_17.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDATEVA_2_20.value==this.w_DATEVA)
      this.oPgFrm.Page2.oPag.oDATEVA_2_20.value=this.w_DATEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAEVA_2_22.value==this.w_QTAEVA)
      this.oPgFrm.Page2.oPag.oQTAEVA_2_22.value=this.w_QTAEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oQTARES_2_24.value==this.w_QTARES)
      this.oPgFrm.Page2.oPag.oQTARES_2_24.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGAZ_2_26.value==this.w_MAGAZ)
      this.oPgFrm.Page2.oPag.oMAGAZ_2_26.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOC_2_30.value==this.w_NUMDOC)
      this.oPgFrm.Page2.oPag.oNUMDOC_2_30.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOC_2_31.value==this.w_ALFDOC)
      this.oPgFrm.Page2.oPag.oALFDOC_2_31.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODODL1_2_41.value==this.w_CODODL1)
      this.oPgFrm.Page2.oPag.oCODODL1_2_41.value=this.w_CODODL1
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_1_87.RadioValue()==this.w_TUTTI)
      this.oPgFrm.Page1.oPag.oTUTTI_1_87.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFATTIBILI_1_88.RadioValue()==this.w_FATTIBILI)
      this.oPgFrm.Page1.oPag.oFATTIBILI_1_88.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNFATTIBILI_1_89.RadioValue()==this.w_NFATTIBILI)
      this.oPgFrm.Page1.oPag.oNFATTIBILI_1_89.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPFATTIBILI_1_90.RadioValue()==this.w_PFATTIBILI)
      this.oPgFrm.Page1.oPag.oPFATTIBILI_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_99.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_99.value=this.w_DESCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore della serie finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore della serie finale")
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(.w_TIPFOR='T')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORSEL_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and (looktab('CONTI','ANTIPCON','ANCODICE',.w_FORSEL) $ 'C-F'))  and not(.w_TIPFOR<>'T')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORSEL_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN))  and not(empty(.w_DBCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI))  and not(empty(.w_DBCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN))  and not(.w_pegg='FP')  and not(empty(.w_OLCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODINI_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI))  and not(.w_pegg='FP')  and not(empty(.w_OLCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODFIN_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN))  and not(.w_pegg='PF')  and not(empty(.w_OLCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODINI_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI))  and not(.w_pegg='PF')  and not(empty(.w_OLCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODFIN_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_DATINI = this.w_DATINI
    this.o_DOCINI = this.w_DOCINI
    this.o_PEGG = this.w_PEGG
    this.o_TUTTI = this.w_TUTTI
    return

enddefine

* --- Define pages as container
define class tgsmr_kpePag1 as StdContainer
  Width  = 822
  height = 531
  stdWidth  = 822
  stdheight = 531
  resizeXpos=480
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLSELEZ_1_1 as StdCombo with uid="YMCDAEDSKE",rtseq=1,rtrep=.f.,left=109,top=38,width=167,height=21;
    , ToolTipText = "Selezione treeview (documenti/ordini)";
    , HelpContextID = 249490090;
    , cFormVar="w_FLSELEZ",RowSource=""+"Tutti (ordini e documenti),"+"Visualizza solo ordini,"+"Visualizza solo documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSELEZ_1_1.RadioValue()
    return(iif(this.value =1,"T",;
    iif(this.value =2,"O",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oFLSELEZ_1_1.GetRadio()
    this.Parent.oContained.w_FLSELEZ = this.RadioValue()
    return .t.
  endfunc

  func oFLSELEZ_1_1.SetRadio()
    this.Parent.oContained.w_FLSELEZ=trim(this.Parent.oContained.w_FLSELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_FLSELEZ=="T",1,;
      iif(this.Parent.oContained.w_FLSELEZ=="O",2,;
      iif(this.Parent.oContained.w_FLSELEZ=="D",3,;
      0)))
  endfunc


  add object oFLVEAC_1_2 as StdCombo with uid="VNKCJROECU",rtseq=2,rtrep=.f.,left=107,top=89,width=74,height=21;
    , ToolTipText = "Selezione ciclo (vendite/acquisti)";
    , HelpContextID = 242304342;
    , cFormVar="w_FLVEAC",RowSource=""+"Acquisti,"+"Vendite,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_2.RadioValue()
    return(iif(this.value =1,"A",;
    iif(this.value =2,"V",;
    iif(this.value =3,"T",;
    space(1)))))
  endfunc
  func oFLVEAC_1_2.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_2.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=="A",1,;
      iif(this.Parent.oContained.w_FLVEAC=="V",2,;
      iif(this.Parent.oContained.w_FLVEAC=="T",3,;
      0)))
  endfunc

  add object oTIPDOC_1_3 as StdField with uid="FYFYYJENFJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento",;
    HelpContextID = 256893750,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=307, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSMR_ZCD.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oCODMAG_1_4 as StdField with uid="WWAXPPIMPZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 41429030,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=751, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'GSDB_SCG.MAGAZZIN_VZM',this.parent.oContained
  endproc

  add object oNUMINI_1_5 as StdField with uid="IKHMXRTVNM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 88391382,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=107, Top=112, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_6 as StdField with uid="CPLEKOYGGO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 55247066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=234, Top=112, InputMask=replicate('X',10)

  func oSERIE1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_7 as StdField with uid="ZQAMWANEZG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 166837974,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=107, Top=135, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_8 as StdField with uid="DHTTXQQJBV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 38469850,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=234, Top=135, InputMask=replicate('X',10)

  func oSERIE2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oDATINI_1_10 as StdField with uid="QMGDPWFMND",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 88414774,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=431, Top=112

  func oDATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="VRSTSJQFDH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 166861366,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=431, Top=135

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDOCINI_1_12 as StdField with uid="NLSHJRFBFX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 88348726,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=648, Top=112

  func oDOCINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_13 as StdField with uid="WKYDEMVJUZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 166795318,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=648, Top=135

  func oDOCFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oFORSEL_1_14 as StdField with uid="GRXGWWUYUW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 129960022,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=158, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPFOR='T')
    endwith
  endfunc

  func oFORSEL_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Conti",'',this.parent.oContained
  endproc

  add object oFORSEL_1_15 as StdField with uid="GVTZDQGQGN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 129960022,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=158, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANCODICE", oKey_1_2="this.w_FORSEL"

  func oFORSEL_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPFOR<>'T')
    endwith
  endfunc

  proc oFORSEL_1_15.mAfter
    with this.Parent.oContained
      .w_FORSEL=CALCZER(.w_FORSEL, 'CONTI')
    endwith
  endproc

  func oFORSEL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CONTI','*','ANCODICE',cp_AbsName(this.parent,'oFORSEL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Conti",'GSCO_ZCF.CONTI_VZM',this.parent.oContained
  endproc

  add object oCODCOM_1_16 as StdField with uid="YLQVWTCUFO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 156117030,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=181, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_17 as StdField with uid="CMRHBCMAKU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 10233894,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=204, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" and not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oDBCODINI_1_18 as StdField with uid="EGVKVVQLEU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DBCODINI", cQueryName = "DBCODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 190182529,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=107, Top=250, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODINI"

  func oDBCODINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODINI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODINI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODINI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDBCODFIN_1_19 as StdField with uid="VCEEWLYQYH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DBCODFIN", cQueryName = "DBCODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 240514172,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=107, Top=273, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODFIN"

  proc oDBCODFIN_1_19.mDefault
    with this.Parent.oContained
      if empty(.w_DBCODFIN)
        .w_DBCODFIN = .w_DBCODINI
      endif
    endwith
  endproc

  func oDBCODFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODFIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODFIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODFIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_1_20 as StdField with uid="BAUZFWPTWC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 101930410,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=296, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_21 as StdField with uid="GHDDKOKVIG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 188962218,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=319, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_21.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_25 as StdField with uid="VYQYSTJFCH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 88423270,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=342, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_26 as StdField with uid="BKTBROMCMD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 166869862,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=365, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_26.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_27 as StdField with uid="WQNLFQEXDD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 88414758,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=389, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_28 as StdField with uid="WHQQEZYTLE",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 166861350,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=412, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_28.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oOLCODINI_1_29 as StdField with uid="QBFYXJIXEG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_OLCODINI", cQueryName = "OLCODINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL/OCL di inizio selezione",;
    HelpContextID = 190179793,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=437, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODINI"

  func oOLCODINI_1_29.mHide()
    with this.Parent.oContained
      return (.w_pegg='FP')
    endwith
  endfunc

  func oOLCODINI_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODINI_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODINI_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODINI_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'gsmr_zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oOLCODFIN_1_30 as StdField with uid="RYBOKFOTRV",rtseq=29,rtrep=.f.,;
    cFormVar = "w_OLCODFIN", cQueryName = "OLCODFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL/OCL di inizio selezione",;
    HelpContextID = 240511436,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=345, Top=437, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODFIN"

  proc oOLCODFIN_1_30.mDefault
    with this.Parent.oContained
      if empty(.w_OLCODFIN)
        .w_OLCODFIN = .w_OLCODINI
      endif
    endwith
  endproc

  func oOLCODFIN_1_30.mHide()
    with this.Parent.oContained
      return (.w_pegg='FP')
    endwith
  endfunc

  func oOLCODFIN_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODFIN_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODFIN_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODFIN_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'gsmr_zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oOLCODINI_1_31 as StdField with uid="VSXCGDIPHN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_OLCODINI", cQueryName = "OLCODINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL/OCL/ODA di inizio selezione",;
    HelpContextID = 190179793,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=437, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODINI"

  func oOLCODINI_1_31.mHide()
    with this.Parent.oContained
      return (.w_pegg='PF')
    endwith
  endfunc

  func oOLCODINI_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODINI_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODINI_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODINI_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'gsmr1zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oOLCODFIN_1_32 as StdField with uid="XAVPSNBDBO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_OLCODFIN", cQueryName = "OLCODFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL/OCL/ODA di fine selezione",;
    HelpContextID = 240511436,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=345, Top=437, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODFIN"

  proc oOLCODFIN_1_32.mDefault
    with this.Parent.oContained
      if empty(.w_OLCODFIN)
        .w_OLCODFIN = .w_OLCODINI
      endif
    endwith
  endproc

  func oOLCODFIN_1_32.mHide()
    with this.Parent.oContained
      return (.w_pegg='PF')
    endwith
  endfunc

  func oOLCODFIN_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODFIN_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODFIN_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODFIN_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'gsmr1zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oCOMODL_1_33 as StdField with uid="JDHYNCICPU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_COMODL", cQueryName = "COMODL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa ordine",;
    HelpContextID = 128628774,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=461, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMODL"

  func oCOMODL_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCOMODL_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMODL_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMODL_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMODL_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oPEGG_1_34 as StdRadio with uid="ZNMNWJQWXZ",rtseq=33,rtrep=.f.,left=107, top=487, width=429,height=24;
    , ToolTipText = "Tipo di visualizzazione";
    , cFormVar="w_PEGG", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPEGG_1_34.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Pegging padre-figlio"
      this.Buttons(1).HelpContextID = 144552202
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Pegging padre-figlio","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Pegging figlio-padre"
      this.Buttons(2).HelpContextID = 144552202
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Pegging figlio-padre","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",24)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di visualizzazione")
      StdRadio::init()
    endproc

  func oPEGG_1_34.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'FP',;
    space(2))))
  endfunc
  func oPEGG_1_34.GetRadio()
    this.Parent.oContained.w_PEGG = this.RadioValue()
    return .t.
  endfunc

  func oPEGG_1_34.SetRadio()
    this.Parent.oContained.w_PEGG=trim(this.Parent.oContained.w_PEGG)
    this.value = ;
      iif(this.Parent.oContained.w_PEGG=='PF',1,;
      iif(this.Parent.oContained.w_PEGG=='FP',2,;
      0))
  endfunc


  add object oBtn_1_35 as StdButton with uid="PPJFABIUST",left=719, top=486, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 27407126;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSMR_BPE(this.Parent.oContained,"Tree-FigPadre")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PEGG='PF')
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="WBDQHNKONY",left=719, top=486, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 27407126;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSMR_BPE(this.Parent.oContained,"Tree-PadreFig")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PEGG='FP')
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="MWXKLVEWEK",left=770, top=486, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 142197690;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAN_1_42 as StdField with uid="PJHERXXTYJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158273078,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=241, Top=181, InputMask=replicate('X',30)

  add object oDESATT_1_44 as StdField with uid="JRUPUPZCWV",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 10292790,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=241, Top=204, InputMask=replicate('X',30)

  add object oDESFAMAI_1_45 as StdField with uid="AOYBYAMKXI",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141692543,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=296, InputMask=replicate('X',35)

  add object oDESGRUI_1_46 as StdField with uid="ZABORREMDH",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 243069386,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=342, InputMask=replicate('X',35)

  add object oDESCATI_1_47 as StdField with uid="ZFWYARIWSP",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 9499082,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=389, InputMask=replicate('X',35)

  add object oDESFAMAF_1_51 as StdField with uid="WOCDSQXKSA",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141692540,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=173, Top=319, InputMask=replicate('X',35)

  add object oDESGRUF_1_52 as StdField with uid="PYGQUHDJMJ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 243069386,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=173, Top=365, InputMask=replicate('X',35)

  add object oDESCATF_1_53 as StdField with uid="SXSGGSHCII",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258936374,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=173, Top=412, InputMask=replicate('X',35)

  add object oDESDISI_1_57 as StdField with uid="QSEMNVVIFP",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17822154,;
   bGlobalFont=.t.,;
    Height=21, Width=382, Left=262, Top=250, InputMask=replicate('X',40)

  add object oDESDISF_1_58 as StdField with uid="BLVZKOKGVG",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 250613302,;
   bGlobalFont=.t.,;
    Height=21, Width=382, Left=262, Top=273, InputMask=replicate('X',40)

  add object oDESFOR_1_65 as StdField with uid="RJLIHNJXRO",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 240258614,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=241, Top=158, InputMask=replicate('X',40)

  add object oDESDOC_1_68 as StdField with uid="RMROTHRADO",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 256904758,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=372, Top=89, InputMask=replicate('X',35)


  add object oObj_1_77 as cp_runprogram with uid="ZGDVVCLSGO",left=0, top=539, width=248,height=19,;
    caption='GSMR_BPE(NOME)',;
   bGlobalFont=.t.,;
    prg="GSMR_BPE('NOME')",;
    cEvent = "w_PEGG Changed",;
    nPag=1;
    , HelpContextID = 218103083

  add object oTUTTI_1_87 as StdCheck with uid="PHVHVQZCUR",rtseq=75,rtrep=.f.,left=458, top=343, caption="Tutti",;
    HelpContextID = 67096778,;
    cFormVar="w_TUTTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_1_87.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oTUTTI_1_87.GetRadio()
    this.Parent.oContained.w_TUTTI = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_1_87.SetRadio()
    this.Parent.oContained.w_TUTTI=trim(this.Parent.oContained.w_TUTTI)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI=='T',1,;
      0)
  endfunc

  func oTUTTI_1_87.mHide()
    with this.Parent.oContained
      return (.w_PEGG='FP')
    endwith
  endfunc

  add object oFATTIBILI_1_88 as StdCheck with uid="EGNVOFZTJY",rtseq=76,rtrep=.f.,left=458, top=365, caption="Ordini fattibili",;
    HelpContextID = 33546446,;
    cFormVar="w_FATTIBILI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFATTIBILI_1_88.RadioValue()
    return(iif(this.value =1,'F',;
    ' '))
  endfunc
  func oFATTIBILI_1_88.GetRadio()
    this.Parent.oContained.w_FATTIBILI = this.RadioValue()
    return .t.
  endfunc

  func oFATTIBILI_1_88.SetRadio()
    this.Parent.oContained.w_FATTIBILI=trim(this.Parent.oContained.w_FATTIBILI)
    this.value = ;
      iif(this.Parent.oContained.w_FATTIBILI=='F',1,;
      0)
  endfunc

  func oFATTIBILI_1_88.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TUTTI<>'T')
    endwith
   endif
  endfunc

  func oFATTIBILI_1_88.mHide()
    with this.Parent.oContained
      return (.w_PEGG='FP')
    endwith
  endfunc

  add object oNFATTIBILI_1_89 as StdCheck with uid="TUJXLPROTX",rtseq=77,rtrep=.f.,left=620, top=343, caption="Ordini non fattibili",;
    HelpContextID = 95370719,;
    cFormVar="w_NFATTIBILI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNFATTIBILI_1_89.RadioValue()
    return(iif(this.value =1,'N',;
    ' '))
  endfunc
  func oNFATTIBILI_1_89.GetRadio()
    this.Parent.oContained.w_NFATTIBILI = this.RadioValue()
    return .t.
  endfunc

  func oNFATTIBILI_1_89.SetRadio()
    this.Parent.oContained.w_NFATTIBILI=trim(this.Parent.oContained.w_NFATTIBILI)
    this.value = ;
      iif(this.Parent.oContained.w_NFATTIBILI=='N',1,;
      0)
  endfunc

  func oNFATTIBILI_1_89.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TUTTI<>'T')
    endwith
   endif
  endfunc

  func oNFATTIBILI_1_89.mHide()
    with this.Parent.oContained
      return (.w_PEGG='FP')
    endwith
  endfunc

  add object oPFATTIBILI_1_90 as StdCheck with uid="BVOLHHTHRE",rtseq=78,rtrep=.f.,left=620, top=365, caption="Ordini parz. fattibili",;
    HelpContextID = 95370751,;
    cFormVar="w_PFATTIBILI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPFATTIBILI_1_90.RadioValue()
    return(iif(this.value =1,'P',;
    ' '))
  endfunc
  func oPFATTIBILI_1_90.GetRadio()
    this.Parent.oContained.w_PFATTIBILI = this.RadioValue()
    return .t.
  endfunc

  func oPFATTIBILI_1_90.SetRadio()
    this.Parent.oContained.w_PFATTIBILI=trim(this.Parent.oContained.w_PFATTIBILI)
    this.value = ;
      iif(this.Parent.oContained.w_PFATTIBILI=='P',1,;
      0)
  endfunc

  func oPFATTIBILI_1_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TUTTI<>'T')
    endwith
   endif
  endfunc

  func oPFATTIBILI_1_90.mHide()
    with this.Parent.oContained
      return (.w_PEGG='FP')
    endwith
  endfunc


  add object TRFLGHT1 as cp_showimage with uid="HVXLBULWVM",left=794, top=341, width=20,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="bmp/Lanciato.bmp",BackStyle=0,;
    nPag=1;
    , HelpContextID = 28482534


  add object TRFLGHT3 as cp_showimage with uid="KDJQBGCMAR",left=594, top=365, width=20,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="bmp/Suggerito.bmp",BackStyle=0,;
    nPag=1;
    , HelpContextID = 28482534


  add object TRFLGHT2 as cp_showimage with uid="XCWDZOCQUR",left=794, top=365, width=20,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="bmp/Pianificato.bmp",BackStyle=0,;
    nPag=1;
    , HelpContextID = 28482534

  add object oDESCOM_1_99 as StdField with uid="PSJFTRZIYY",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 156175926,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=240, Top=461, InputMask=replicate('X',30)

  add object oStr_1_9 as StdString with uid="CGEEJWMTRL",Visible=.t., Left=227, Top=135,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XRXCRISBDD",Visible=.t., Left=205, Top=91,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="OLHXGJTWFV",Visible=.t., Left=1, Top=250,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="IEHZXMOABQ",Visible=.t., Left=1, Top=273,;
    Alignment=1, Width=105, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=-3, Top=181,;
    Alignment=1, Width=109, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=-3, Top=204,;
    Alignment=1, Width=109, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=1, Top=296,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=1, Top=342,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=1, Top=389,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="BETNLUNOYI",Visible=.t., Left=1, Top=321,;
    Alignment=1, Width=105, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=1, Top=365,;
    Alignment=1, Width=105, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=1, Top=412,;
    Alignment=1, Width=105, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="ILMDRJPSBA",Visible=.t., Left=4, Top=63,;
    Alignment=0, Width=382, Height=18,;
    Caption="Selezioni documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="PEISJRCHUW",Visible=.t., Left=4, Top=225,;
    Alignment=0, Width=382, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="UZFXVUFHLH",Visible=.t., Left=-3, Top=158,;
    Alignment=1, Width=109, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC<>'A')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="DLAOKFKNHU",Visible=.t., Left=-3, Top=89,;
    Alignment=1, Width=109, Height=15,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="HRQFQNITXV",Visible=.t., Left=329, Top=115,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="DNNAWKFMZY",Visible=.t., Left=329, Top=137,;
    Alignment=1, Width=99, Height=15,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="PYKVJJBJUN",Visible=.t., Left=513, Top=115,;
    Alignment=1, Width=132, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="BGHQBDDCZG",Visible=.t., Left=554, Top=137,;
    Alignment=1, Width=91, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=663, Top=91,;
    Alignment=1, Width=85, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="UTVCTDGVMU",Visible=.t., Left=-3, Top=158,;
    Alignment=1, Width=109, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC<>'V')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="UZSDVBSZPX",Visible=.t., Left=-3, Top=158,;
    Alignment=1, Width=109, Height=15,;
    Caption="Cliente\fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC<>'T')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="BKVUFSLWMR",Visible=.t., Left=1, Top=437,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="HGANHIHTKB",Visible=.t., Left=254, Top=437,;
    Alignment=1, Width=90, Height=15,;
    Caption="A ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="VQZWRBIBXU",Visible=.t., Left=-3, Top=115,;
    Alignment=1, Width=109, Height=15,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="PMVZWORABY",Visible=.t., Left=-3, Top=137,;
    Alignment=1, Width=109, Height=15,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="UKWJKYAMUK",Visible=.t., Left=227, Top=112,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="YBBKTVDFPN",Visible=.t., Left=14, Top=38,;
    Alignment=1, Width=95, Height=18,;
    Caption="Visualizza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="MUAUZXLTVE",Visible=.t., Left=4, Top=13,;
    Alignment=0, Width=382, Height=18,;
    Caption="Selezioni tree-view (documenti / ordini di produzione)"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="HWGEPIQTHQ",Visible=.t., Left=1, Top=461,;
    Alignment=1, Width=105, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="QRIECBBMTU",Visible=.t., Left=247, Top=615,;
    Alignment=0, Width=501, Height=18,;
    Caption="modifica sar� necessario variare anche la sua condizione di visibilit� nel batch gsmr_bpe."  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="NUBBWSRSUO",Visible=.t., Left=247, Top=601,;
    Alignment=0, Width=551, Height=18,;
    Caption="La sequenza del box che racchiude le selezioni del tipo ordine da visualizzare � 93, se la si"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oBox_1_61 as StdBox with uid="OTTRKDEJWA",left=0, top=82, width=819,height=0

  add object oBox_1_63 as StdBox with uid="BQDUBJBORW",left=1, top=244, width=819,height=0

  add object oBox_1_93 as StdBox with uid="NICMASLRTN",left=451, top=333, width=369,height=63

  add object oBox_1_97 as StdBox with uid="QZUYSEROBR",left=-1, top=31, width=819,height=1
enddefine
define class tgsmr_kpePag2 as StdContainer
  Width  = 822
  height = 531
  stdWidth  = 822
  stdheight = 531
  resizeXpos=428
  resizeYpos=207
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_1 as cp_runprogram with uid="EBVGCFPGTX",left=18, top=595, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMR_BPE('Page2')",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 28482534


  add object TREEV as cp_Treeview with uid="QTFEDDFLDB",left=0, top=3, width=823,height=392,;
    caption='TREEV',;
   bGlobalFont=.t.,;
    cCursor="pegging",cShowFields="DESCRI",cNodeShowField="",cLeafShowField="",cNodeBmp="odll.bmp",cLeafBmp="",nIndent=0,;
    cEvent = "updtreev",;
    nPag=2;
    , HelpContextID = 54510538


  add object oObj_2_6 as cp_runprogram with uid="BHGWZXUNHT",left=836, top=54, width=72,height=22,;
    caption='GSMR_BPE',;
   bGlobalFont=.t.,;
    prg='GSMR_BPE("Done")',;
    cEvent = "Done",;
    nPag=2;
    , HelpContextID = 257801387

  add object oCODODL_2_8 as StdField with uid="EFOBYYZJFF",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CODODL", cQueryName = "CODODL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 128591910,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=65, Top=411, InputMask=replicate('X',15)

  func oCODODL_2_8.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='R')
    endwith
  endfunc

  add object oCODRIC_2_9 as StdField with uid="VXOAYLFGWV",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODRIC", cQueryName = "CODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(41), bMultilanguage =  .f.,;
    HelpContextID = 251471910,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=65, Top=457, InputMask=replicate('X',41), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODRIC"

  func oCODRIC_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCADESAR_2_10 as StdField with uid="RXUHOYJINC",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CADESAR", cQueryName = "CADESAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 227547686,;
   bGlobalFont=.t.,;
    Height=21, Width=403, Left=385, Top=457, InputMask=replicate('X',40)


  add object statoODL as cp_calclbl with uid="AYCVNYJAJD",left=204, top=413, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , ToolTipText = "Stato ordine";
    , HelpContextID = 28482534


  add object TRFLGHT as cp_showimage with uid="WXFLELIPYJ",left=307, top=409, width=20,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="bmp\logobianco.bmp",BackStyle=0,;
    nPag=2;
    , HelpContextID = 28482534

  add object oQTAORD_2_16 as StdField with uid="ZJAXUJZHTF",rtseq=56,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 9043462,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=65, Top=434, cSayPict="V_PQ(14)"

  func oQTAORD_2_16.mHide()
    with this.Parent.oContained
      return (nvl(.w_CODODL,'')='Magazzino')
    endwith
  endfunc

  add object oUNIMIS_2_17 as StdField with uid="COHMDEMJGX",rtseq=57,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 251164742,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=733, Top=411, InputMask=replicate('X',3)

  func oUNIMIS_2_17.mHide()
    with this.Parent.oContained
      return (nvl(.w_CODODL,'')='Magazzino')
    endwith
  endfunc

  add object oDATEVA_2_20 as StdField with uid="GYYMZDDJAC",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DATEVA", cQueryName = "DATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 230758966,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=618, Top=411

  func oDATEVA_2_20.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oQTAEVA_2_22 as StdField with uid="BQYZGBVNCU",rtseq=59,rtrep=.f.,;
    cFormVar = "w_QTAEVA", cQueryName = "QTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 230686214,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=253, Top=434, cSayPict="V_PQ(14)"

  func oQTAEVA_2_22.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oQTARES_2_24 as StdField with uid="TRUDNJFLPI",rtseq=60,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 247266822,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=473, Top=434, cSayPict="V_PQ(14)"

  func oQTARES_2_24.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oMAGAZ_2_26 as StdField with uid="ULHQATTNMM",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 50574650,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=733, Top=434, InputMask=replicate('X',5)

  func oMAGAZ_2_26.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc


  add object oBtn_2_27 as StdButton with uid="WZCLYAIBJF",left=770, top=486, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142197690;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMDOC_2_30 as StdField with uid="LGLFEGUFHL",rtseq=65,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 256884438,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=65, Top=411, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMDOC_2_30.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'R')
    endwith
  endfunc

  add object oALFDOC_2_31 as StdField with uid="DJPOAMSRPN",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 256853254,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=198, Top=411, InputMask=replicate('X',10)

  func oALFDOC_2_31.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'R' or empty(.w_ALFDOC))
    endwith
  endfunc


  add object oObj_2_34 as cp_runprogram with uid="CUZXWQIQWL",left=153, top=595, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMR_BPE('PopUp')",;
    cEvent = "w_treev NodeRightClick",;
    nPag=2;
    , HelpContextID = 28482534


  add object oObj_2_39 as cp_runprogram with uid="PFRWIUKVRB",left=288, top=595, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMR_BPE('EXPNODE')",;
    cEvent = "w_treev Expanded",;
    nPag=2;
    , HelpContextID = 28482534


  add object oObj_2_40 as cp_runprogram with uid="SRJLFCCKTX",left=423, top=595, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMR_BPE('COLLNODE')",;
    cEvent = "w_treev Collapsed",;
    nPag=2;
    , HelpContextID = 28482534

  add object oCODODL1_2_41 as StdField with uid="QYTPUPFOOG",rtseq=72,rtrep=.f.,;
    cFormVar = "w_CODODL1", cQueryName = "CODODL1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 128591910,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=347, Top=411, InputMask=replicate('X',10), cLinkFile="DOC_MAST", oKey_1_1="MVSERIAL", oKey_1_2="this.w_CODODL1"

  func oCODODL1_2_41.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'R')
    endwith
  endfunc

  func oCODODL1_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oObj_2_42 as cp_runprogram with uid="RXSZKICEZO",left=558, top=595, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMR_BPE('PopRid')",;
    cEvent = "w_treev MouseRightClick",;
    nPag=2;
    , HelpContextID = 28482534


  add object StatoDoc as cp_calclbl with uid="EHWMUAAYRY",left=444, top=413, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , ToolTipText = "Statodocumento";
    , HelpContextID = 28482534


  add object NFAT as cp_calclbl with uid="WSWIVJWQAT",left=66, top=495, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=2;
    , HelpContextID = 28482534


  add object FATT as cp_calclbl with uid="CFSCJVMVGQ",left=267, top=495, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=2;
    , HelpContextID = 28482534


  add object PFAT as cp_calclbl with uid="DNJKEHRMTW",left=439, top=495, width=170,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=2;
    , HelpContextID = 28482534


  add object oBtn_2_49 as StdButton with uid="TCINDJTGWB",left=721, top=486, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Stampa la treeview";
    , HelpContextID = 260709926;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_49.Click()
      with this.Parent.oContained
        GSMR_BPE(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_11 as StdString with uid="OHCFHJNCKJ",Visible=.t., Left=1, Top=457,;
    Alignment=1, Width=61, Height=15,;
    Caption="Distinta:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G' or nvl(.w_PROVE,'')='E')
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="TYMQXOFOZF",Visible=.t., Left=1, Top=414,;
    Alignment=1, Width=61, Height=15,;
    Caption="Ordine:"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'') $ 'R-G')
    endwith
  endfunc

  add object oStr_2_15 as StdString with uid="FAADZNHFCC",Visible=.t., Left=697, Top=414,;
    Alignment=1, Width=34, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (nvl(.w_CODODL,'')='Magazzino')
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="MAWHPOBZVA",Visible=.t., Left=1, Top=434,;
    Alignment=1, Width=61, Height=15,;
    Caption="Ordinato:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="VYBTMGIRDF",Visible=.t., Left=550, Top=414,;
    Alignment=1, Width=65, Height=15,;
    Caption="Evasione:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="SDSTTXINGH",Visible=.t., Left=195, Top=436,;
    Alignment=1, Width=55, Height=15,;
    Caption="Evaso:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oStr_2_23 as StdString with uid="QOOJGWSKSX",Visible=.t., Left=404, Top=436,;
    Alignment=1, Width=66, Height=15,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oStr_2_25 as StdString with uid="LSEEDUNPIB",Visible=.t., Left=662, Top=437,;
    Alignment=1, Width=69, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')='G')
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="JMRBMFTLET",Visible=.t., Left=302, Top=414,;
    Alignment=1, Width=45, Height=15,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'R')
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="GMKPVWKITH",Visible=.t., Left=1, Top=434,;
    Alignment=1, Width=61, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  func oStr_2_29.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'G' or nvl(.w_CODODL,'')='Magazzino')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="MADMCVWCKJ",Visible=.t., Left=192, Top=414,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'R' or empty(.w_ALFDOC))
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="FEOVYMXXOC",Visible=.t., Left=1, Top=414,;
    Alignment=1, Width=61, Height=15,;
    Caption="Num.doc.:"  ;
  , bGlobalFont=.t.

  func oStr_2_33.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'R')
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="NUWVNLMBAK",Visible=.t., Left=1, Top=457,;
    Alignment=1, Width=61, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (nvl(.w_ST,'')<>'G' and nvl(.w_PROVE,' ')<>'E')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_kpe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
