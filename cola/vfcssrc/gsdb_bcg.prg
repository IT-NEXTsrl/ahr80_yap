* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bcg                                                        *
*              Calcola il giorno del periodo                                   *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-04                                                      *
* Last revis.: 2012-10-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bcg",oParentObject,m.w_CAL)
return(i_retval)

define class tgsdb_bcg as StdBatch
  * --- Local variables
  w_CAL = .f.
  w_tmpd = ctod("  /  /  ")
  w_tmpd1 = ctod("  /  /  ")
  w_tmpd2 = ctod("  /  /  ")
  w_CALEND = space(5)
  w_SETTINI = ctod("  /  /  ")
  w_SETTFIN = ctod("  /  /  ")
  * --- WorkFile variables
  CAL_AZIE_idx=0
  PAR_PROD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il primo giorno del periodo selezionato (da GSDB_BPM)
    * --- Se TRUE restituisce il primo giorno del periodo nel calendario aziendale
    if this.w_CAL
      * --- Read from PAR_PROD
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPCALSTA"+;
          " from "+i_cTable+" PAR_PROD where ";
              +"PPCODICE = "+cp_ToStrODBC("PP");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPCALSTA;
          from (i_cTable) where;
              PPCODICE = "PP";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CALEND = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_CALEND)
        ah_ErrorMsg("Il calendario aziendale non � definito",16)
        this.oParentObject.w_PV__DATA = cp_CharToDate("  -  -  ")
        this.oParentObject.w_PVPERSET = 0
        this.oParentObject.w_PVPERMES = 0
        i_retcode = 'stop'
        return
      endif
    endif
    if (this.oParentObject.w_PV__ANNO<0) or (this.oParentObject.w_PV__ANNO>99 and this.oParentObject.w_PV__ANNO<1990) or (this.oParentObject.w_PV__ANNO>2099)
      ah_ErrorMsg("Inserire un anno compreso tra 1990 e 2099",48)
      i_retcode = 'stop'
      return
    endif
    do case
      case this.oParentObject.w_PVPERIOD="M"
        * --- Periodo mensile: primo luned� del mese
        if this.oParentObject.w_PVPERMES>12 or this.oParentObject.w_PVPERMES<1
          ah_ErrorMsg("Inserire un valore mensile compreso tra 1 e 12",48)
          i_retcode = 'stop'
          return
        endif
        this.w_tmpd = Date(this.oParentObject.w_PV__ANNO, this.oParentObject.w_PVPERMES, 1)
        if this.oParentObject.w_PVPERMES<>1
          this.w_tmpd = this.w_tmpd + 7-dow(this.w_tmpd,3)
        else
          * --- Per quanto previsto dalla normativa UNI 7180-73 e successive revisioni il calcolo dell'inizio del mese 1 fa eccezione.
          if dow(this.w_tmpd,2)<5 and dow(this.w_tmpd,2)<>1
            this.w_tmpd = this.w_tmpd - dow(this.w_tmpd,3)
          else
            this.w_tmpd = this.w_tmpd + 7-dow(this.w_tmpd,3)
          endif
        endif
        if this.w_CAL
          this.w_tmpd1 = this.w_tmpd
          this.w_tmpd2 = this.w_tmpd1+27
          if DAY(this.w_tmpd2+7)<7
            * --- Aggiunge la 5^ settimana del mese
            this.w_tmpd2 = this.w_tmpd1+7
          endif
        endif
      case this.oParentObject.w_PVPERIOD="S"
        * --- Periodo settimanale
        if this.oParentObject.w_PVPERSET>53 or this.oParentObject.w_PVPERSET<1
          ah_ErrorMsg("Inserire un valore settimanale compreso tra 1 e 53",48)
          i_retcode = 'stop'
          return
        endif
        this.w_tmpd = Date(this.oParentObject.w_PV__ANNO,1,1)
        this.w_tmpd = this.w_tmpd - dow(this.w_tmpd,3)+IIF(dow(this.w_tmpd,3)>3,7,0) + 7*(this.oParentObject.w_PVPERSET-1)
        if this.w_CAL
          this.w_tmpd1 = this.w_tmpd
          this.w_tmpd2 = this.w_tmpd1+7
        endif
      case this.oParentObject.w_PVPERIOD="G"
        this.w_tmpd = this.oParentObject.w_PV__DATA
    endcase
    if this.w_CAL
      * --- Select from CAL_AZIE
      i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select min(CAGIORNO) as CAGIORNO  from "+i_cTable+" CAL_AZIE ";
            +" where CACODCAL="+cp_ToStrODBC(this.w_CALEND)+" and CAGIORNO>="+cp_ToStrODBC(this.w_tmpd1)+" and CAGIORNO<="+cp_ToStrODBC(this.w_tmpd2)+"";
             ,"_Curs_CAL_AZIE")
      else
        select min(CAGIORNO) as CAGIORNO from (i_cTable);
         where CACODCAL=this.w_CALEND and CAGIORNO>=this.w_tmpd1 and CAGIORNO<=this.w_tmpd2;
          into cursor _Curs_CAL_AZIE
      endif
      if used('_Curs_CAL_AZIE')
        select _Curs_CAL_AZIE
        locate for 1=1
        do while not(eof())
        this.w_tmpd = cp_todate(_Curs_CAL_AZIE.CAGIORNO)
          select _Curs_CAL_AZIE
          continue
        enddo
        use
      endif
    endif
    if empty(this.w_tmpd)
      this.oParentObject.w_PV__DATA = this.w_tmpd
      this.oParentObject.w_PVPERSET = 0
      this.oParentObject.w_PVPERMES = 0
      ah_ErrorMsg("Nel periodo selezionato non � definito il calendario",48)
    else
      this.oParentObject.w_PV__DATA = this.w_tmpd
      this.w_tmpd = this.w_tmpd - dow(this.w_tmpd,2) +1
      this.oParentObject.w_PVPERSET = week(this.w_tmpd,2,2)
      * --- Calcolo il periodo mensile
      * --- Luned�
      this.w_SETTINI = this.w_tmpd-iif(dow(this.w_tmpd,3)=7,0,dow(this.w_tmpd,3))
      * --- Domenica
      this.w_SETTFIN = this.w_tmpd-iif(dow(this.w_tmpd,3)=7,0,dow(this.w_tmpd,3))+6
      if month(this.w_SETTINI)=month(this.w_SETTFIN)
        * --- La settimana in esame � interamente all'interno dello stesso mese
        this.oParentObject.w_PVPERMES = month(this.w_tmpd)
      else
        * --- Per vedere in quale periodo mensile deve essere collocata la settimana fa fede il Gioved� - Solo ultima settimana dell'anno
        if month(this.w_SETTINI)=12 and month(this.w_SETTFIN)=1
          this.oParentObject.w_PVPERMES = month(this.w_SETTINI+3)
        else
          this.oParentObject.w_PVPERMES = month(this.w_tmpd)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_CAL)
    this.w_CAL=w_CAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CAL_AZIE'
    this.cWorkTables[2]='PAR_PROD'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_CAL_AZIE')
      use in _Curs_CAL_AZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CAL"
endproc
