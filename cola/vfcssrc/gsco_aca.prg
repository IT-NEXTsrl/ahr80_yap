* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_aca                                                        *
*              Calendario aziendale                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_28]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-21                                                      *
* Last revis.: 2002-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_aca"))

* --- Class definition
define class tgsco_aca as StdForm
  Top    = 34
  Left   = 47

  * --- Standard Properties
  Width  = 461
  Height = 79+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2002-05-27"
  HelpContextID=158751383
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  CAL_AZIE_IDX = 0
  cFile = "CAL_AZIE"
  cKeySelect = "CAGIORNO"
  cKeyWhere  = "CAGIORNO=this.w_CAGIORNO"
  cKeyWhereODBC = '"CAGIORNO="+cp_ToStrODBC(this.w_CAGIORNO,"D")';

  cKeyWhereODBCqualified = '"CAL_AZIE.CAGIORNO="+cp_ToStrODBC(this.w_CAGIORNO,"D")';

  cPrg = "gsco_aca"
  cComment = "Calendario aziendale"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CAGIORNO = ctod('  /  /  ')
  w_CADESGIO = space(40)
  w_CAGIOLAV = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAL_AZIE','gsco_aca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_acaPag1","gsco_aca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Calendario")
      .Pages(1).HelpContextID = 188549592
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCAGIORNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAL_AZIE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAL_AZIE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAL_AZIE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CAGIORNO = NVL(CAGIORNO,ctod("  /  /  "))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAL_AZIE where CAGIORNO=KeySet.CAGIORNO
    *
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAL_AZIE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAL_AZIE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAL_AZIE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CAGIORNO',this.w_CAGIORNO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CAGIORNO = NVL(cp_ToDate(CAGIORNO),ctod("  /  /  "))
        .w_CADESGIO = NVL(CADESGIO,space(40))
        .w_CAGIOLAV = NVL(CAGIOLAV,space(1))
        cp_LoadRecExtFlds(this,'CAL_AZIE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CAGIORNO = ctod("  /  /  ")
      .w_CADESGIO = space(40)
      .w_CAGIOLAV = space(1)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAL_AZIE')
    this.DoRTCalc(1,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCAGIORNO_1_1.enabled = i_bVal
      .Page1.oPag.oCADESGIO_1_3.enabled = i_bVal
      .Page1.oPag.oCAGIOLAV_1_4.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCAGIORNO_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCAGIORNO_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAL_AZIE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAGIORNO,"CAGIORNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESGIO,"CADESGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAGIOLAV,"CAGIOLAV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    i_lTable = "CAL_AZIE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAL_AZIE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAL_AZIE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAL_AZIE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAL_AZIE')
        i_extval=cp_InsertValODBCExtFlds(this,'CAL_AZIE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CAGIORNO,CADESGIO,CAGIOLAV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CAGIORNO)+;
                  ","+cp_ToStrODBC(this.w_CADESGIO)+;
                  ","+cp_ToStrODBC(this.w_CAGIOLAV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAL_AZIE')
        i_extval=cp_InsertValVFPExtFlds(this,'CAL_AZIE')
        cp_CheckDeletedKey(i_cTable,0,'CAGIORNO',this.w_CAGIORNO)
        INSERT INTO (i_cTable);
              (CAGIORNO,CADESGIO,CAGIOLAV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CAGIORNO;
                  ,this.w_CADESGIO;
                  ,this.w_CAGIOLAV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAL_AZIE_IDX,i_nConn)
      *
      * update CAL_AZIE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAL_AZIE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CADESGIO="+cp_ToStrODBC(this.w_CADESGIO)+;
             ",CAGIOLAV="+cp_ToStrODBC(this.w_CAGIOLAV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAL_AZIE')
        i_cWhere = cp_PKFox(i_cTable  ,'CAGIORNO',this.w_CAGIORNO  )
        UPDATE (i_cTable) SET;
              CADESGIO=this.w_CADESGIO;
             ,CAGIOLAV=this.w_CAGIOLAV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAL_AZIE_IDX,i_nConn)
      *
      * delete CAL_AZIE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CAGIORNO',this.w_CAGIORNO  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCAGIORNO_1_1.value==this.w_CAGIORNO)
      this.oPgFrm.Page1.oPag.oCAGIORNO_1_1.value=this.w_CAGIORNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESGIO_1_3.value==this.w_CADESGIO)
      this.oPgFrm.Page1.oPag.oCADESGIO_1_3.value=this.w_CADESGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAGIOLAV_1_4.RadioValue()==this.w_CAGIOLAV)
      this.oPgFrm.Page1.oPag.oCAGIOLAV_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAL_AZIE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco_acaPag1 as StdContainer
  Width  = 457
  height = 79
  stdWidth  = 457
  stdheight = 79
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAGIORNO_1_1 as StdField with uid="OKWUJTIOUN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CAGIORNO", cQueryName = "CAGIORNO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data",;
    HelpContextID = 256634763,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=72, Left=95, Top=17

  add object oCADESGIO_1_3 as StdField with uid="YDGOEGUIJQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CADESGIO", cQueryName = "CADESGIO",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione giorno",;
    HelpContextID = 99606645,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=171, Top=17, InputMask=replicate('X',40)

  add object oCAGIOLAV_1_4 as StdCheck with uid="OALLOFWUIW",rtseq=3,rtrep=.f.,left=95, top=46, caption="Giorno lavorativo",;
    ToolTipText = "Se attivo: giorno lavorativo, altrimenti festivo",;
    HelpContextID = 88862596,;
    cFormVar="w_CAGIOLAV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAGIOLAV_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAGIOLAV_1_4.GetRadio()
    this.Parent.oContained.w_CAGIOLAV = this.RadioValue()
    return .t.
  endfunc

  func oCAGIOLAV_1_4.SetRadio()
    this.Parent.oContained.w_CAGIOLAV=trim(this.Parent.oContained.w_CAGIOLAV)
    this.value = ;
      iif(this.Parent.oContained.w_CAGIOLAV=='S',1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="AGCGCVQNXZ",Visible=.t., Left=39, Top=17,;
    Alignment=1, Width=53, Height=15,;
    Caption="Giorno:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_aca','CAL_AZIE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CAGIORNO=CAL_AZIE.CAGIORNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
