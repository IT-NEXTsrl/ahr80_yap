* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bpd                                                        *
*              Duplica previsioni di vendita                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-05                                                      *
* Last revis.: 2012-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bpd",oParentObject)
return(i_retval)

define class tgsdb_bpd as StdBatch
  * --- Local variables
  w_PVPERIOD = space(1)
  w_PVPERMES = 0
  w_PVPERSET = 0
  w_PV__ANNO = 0
  w_PV__DATA = ctod("  /  /  ")
  w_CODART = space(20)
  w_KEYSAL = space(40)
  w_PVQUANTI = 0
  w_PERETTIF = 0
  w_MESS = space(10)
  w_CODINIZ = space(20)
  w_CODFINE = space(20)
  * --- WorkFile variables
  VEN_PREV_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplicazione previsioni di vendita (da GSDB_KPD)
    this.w_PVPERIOD = "M"
    this.w_PV__ANNO = this.oParentObject.w_ANNODES
    this.w_PERETTIF = 1 + this.oParentObject.w_PERETT/100
    this.w_CODINIZ = this.oParentObject.w_CODINI
    this.w_CODFINE = this.oParentObject.w_CODFIN
    * --- Previsioni di vendita di w_ANNO
    do vq_exec with "..\cola\exe\query\gsdb_bpd",this,"Previs"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Previsioni di vendita di w_ANNODES
    do vq_exec with "..\cola\exe\query\gsdb1bpd",this,"Desti"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case this.oParentObject.w_SOVRA="M"
        * --- Mantiene le previsioni esistenti
        Select Desti
        SCAN
        DELETE FOR Previs.pvcodart=Desti.pvcodart AND Previs.pvpermes=Desti.pvpermes ;
        IN Previs
        ENDSCAN
      case this.oParentObject.w_SOVRA="S"
        * --- Sovrascrive le previsioni esistenti
        * --- Cancella le previsioni esistenti
        * --- Try
        local bErr_03782D48
        bErr_03782D48=bTrsErr
        this.Try_03782D48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(this.w_MESS,16)
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_03782D48
        * --- End
    endcase
    * --- Try
    local bErr_036BC540
    bErr_036BC540=bTrsErr
    this.Try_036BC540()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(this.w_MESS,16)
    endif
    bTrsErr=bTrsErr or bErr_036BC540
    * --- End
  endproc
  proc Try_03782D48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select Desti
    SCAN
    this.w_PV__DATA = Desti.pv__data
    this.w_CODART = LEFT(Desti.pvcodart,20)
    * --- Try
    local bErr_03A80370
    bErr_03A80370=bTrsErr
    this.Try_03A80370()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS = AH_msgFormat("Errore di scrittura sulla base dati: duplicazione annullata")
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    bTrsErr=bTrsErr or bErr_03A80370
    * --- End
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_036BC540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select Previs
    SCAN
    this.w_CODART = LEFT(Previs.pvcodart,20)
    this.w_PVQUANTI = Previs.pvquanti * this.w_PERETTIF
    this.w_PVPERMES = Previs.pvpermes
    * --- Calcola la data (chiave primaria)
    GSDB_BCG(this,.t.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_PVQUANTI>0
      * --- Try
      local bErr_036C0980
      bErr_036C0980=bTrsErr
      this.Try_036C0980()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_msgFormat("Errore di scrittura sulla base dati: duplicazione annullata")
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
      bTrsErr=bTrsErr or bErr_036C0980
      * --- End
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    this.w_MESS = "Duplicazione delle previsioni terminata con successo"
    ah_ErrorMsg(this.w_MESS,48)
    return
  proc Try_03A80370()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from VEN_PREV
    i_nConn=i_TableProp[this.VEN_PREV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
            +" and PVCODART = "+cp_ToStrODBC(this.w_CODART);
             )
    else
      delete from (i_cTable) where;
            PV__DATA = this.w_PV__DATA;
            and PVCODART = this.w_CODART;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_036C0980()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VEN_PREV
    i_nConn=i_TableProp[this.VEN_PREV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VEN_PREV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PV__DATA"+",PVCODART"+",PVPERIOD"+",PVPERMES"+",PVQUANTI"+",PV__ANNO"+",PVPERSET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PV__DATA),'VEN_PREV','PV__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'VEN_PREV','PVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERIOD),'VEN_PREV','PVPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERMES),'VEN_PREV','PVPERMES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PV__ANNO),'VEN_PREV','PV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERSET),'VEN_PREV','PVPERSET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PV__DATA',this.w_PV__DATA,'PVCODART',this.w_CODART,'PVPERIOD',this.w_PVPERIOD,'PVPERMES',this.w_PVPERMES,'PVQUANTI',this.w_PVQUANTI,'PV__ANNO',this.w_PV__ANNO,'PVPERSET',this.w_PVPERSET)
      insert into (i_cTable) (PV__DATA,PVCODART,PVPERIOD,PVPERMES,PVQUANTI,PV__ANNO,PVPERSET &i_ccchkf. );
         values (;
           this.w_PV__DATA;
           ,this.w_CODART;
           ,this.w_PVPERIOD;
           ,this.w_PVPERMES;
           ,this.w_PVQUANTI;
           ,this.w_PV__ANNO;
           ,this.w_PVPERSET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VEN_PREV'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
