* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kpo                                                        *
*              Piano ordini di lavorazione                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-16                                                      *
* Last revis.: 2017-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kpo",oParentObject))

* --- Class definition
define class tgsco_kpo as StdForm
  Top    = 0
  Left   = 4

  * --- Standard Properties
  Width  = 807
  Height = 543+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-21"
  HelpContextID=118905495
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=57

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  DISMBASE_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  CONTI_IDX = 0
  SEL__MRP_IDX = 0
  cPrg = "gsco_kpo"
  cComment = "Piano ordini di lavorazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MPARAM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_TIPATT = space(1)
  w_TIPGES = space(1)
  w_TIPOARTI = space(1)
  w_pOLTPROVE = space(1)
  w_TIPCON = space(1)
  w_MPS_ODL = space(10)
  w_MAGTER = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_PARAM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ODLSEL = space(15)
  w_DISINI = space(20)
  w_DISFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_CODCON = space(15)
  w_SERMRP = space(10)
  w_CODFASI = space(66)
  w_CODFASF = space(66)
  w_TIPGES = space(1)
  w_DININI = ctod('  /  /  ')
  w_DINFIN = ctod('  /  /  ')
  w_DFIINI = ctod('  /  /  ')
  w_DFIFIN = ctod('  /  /  ')
  w_SUGGER = space(1)
  w_PIANIF = space(1)
  w_LANCIA = space(1)
  w_FINITO = space(1)
  w_CRIELA = space(1)
  o_CRIELA = space(1)
  w_SELEZM = space(1)
  o_SELEZM = space(1)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_DESDISI = space(40)
  w_DESDISF = space(40)
  w_DESFOR = space(40)
  w_VISFASI = space(1)
  w_RIGALIS = 0
  w_RET = .F.
  w_KEYRIF = space(10)
  w_ZoomODL = .NULL.
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kpoPag1","gsco_kpo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Visualizza piano ordini")
      .Pages(2).addobject("oPag","tgsco_kpoPag2","gsco_kpo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomODL = this.oPgFrm.Pages(1).oPag.ZoomODL
    this.w_ZOOMMAGA = this.oPgFrm.Pages(2).oPag.ZOOMMAGA
    this.w_LBLMAGA = this.oPgFrm.Pages(2).oPag.LBLMAGA
    DoDefault()
    proc Destroy()
      this.w_ZoomODL = .NULL.
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='CONTI'
    this.cWorkTables[10]='SEL__MRP'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MPARAM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPATT=space(1)
      .w_TIPGES=space(1)
      .w_TIPOARTI=space(1)
      .w_pOLTPROVE=space(1)
      .w_TIPCON=space(1)
      .w_MPS_ODL=space(10)
      .w_MAGTER=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_PARAM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_ODLSEL=space(15)
      .w_DISINI=space(20)
      .w_DISFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_CODCON=space(15)
      .w_SERMRP=space(10)
      .w_CODFASI=space(66)
      .w_CODFASF=space(66)
      .w_TIPGES=space(1)
      .w_DININI=ctod("  /  /  ")
      .w_DINFIN=ctod("  /  /  ")
      .w_DFIINI=ctod("  /  /  ")
      .w_DFIFIN=ctod("  /  /  ")
      .w_SUGGER=space(1)
      .w_PIANIF=space(1)
      .w_LANCIA=space(1)
      .w_FINITO=space(1)
      .w_CRIELA=space(1)
      .w_SELEZM=space(1)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
      .w_DESDISI=space(40)
      .w_DESDISF=space(40)
      .w_DESFOR=space(40)
      .w_VISFASI=space(1)
      .w_RIGALIS=0
      .w_RET=.f.
      .w_KEYRIF=space(10)
        .w_MPARAM = this.oParentObject
        .w_OBTEST = i_DATSYS
        .w_TIPATT = "A"
        .w_TIPGES = "G"
        .w_TIPOARTI = ''
        .w_pOLTPROVE = IIF(.w_MPARAM='E', 'E', 'L')
        .w_TIPCON = 'F'
        .w_MPS_ODL = .w_MPARAM
          .DoRTCalc(9,11,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.ZoomODL.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .w_ODLSEL = .w_ZoomODL.GetVar("OLCODODL")
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate(AH_MsgFormat(IIF(.w_MPARAM="I", "Stato ordini di lavorazione (ODL)", IIF(.w_MPARAM='L', "Stato ordini di conto lavoro (OCL)", IIF(.w_MPARAM="E", "Stato ordini di acquisto (ODA)", IIF(.w_MPARAM="S", "Stato ordini di smontaggio (ODS)", "Stato ordini di lavorazione (ODL)"))))))
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_DISINI))
          .link_2_1('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_DISFIN))
          .link_2_2('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_FAMAINI))
          .link_2_3('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_FAMAFIN))
          .link_2_4('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_GRUINI))
          .link_2_5('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_GRUFIN))
          .link_2_6('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CATINI))
          .link_2_7('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CATFIN))
          .link_2_8('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MAGINI))
          .link_2_9('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_MAGFIN))
          .link_2_10('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CODCOM))
          .link_2_11('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CODATT))
          .link_2_12('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODCON))
          .link_2_13('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_SERMRP))
          .link_2_14('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CODFASI))
          .link_2_15('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_CODFASF))
          .link_2_16('Full')
        endif
        .w_TIPGES = 'X'
          .DoRTCalc(31,34,.f.)
        .w_SUGGER = 'M'
        .w_PIANIF = 'P'
        .w_LANCIA = "L"
        .w_FINITO = "\"
        .w_CRIELA = "A"
      .oPgFrm.Page2.oPag.ZOOMMAGA.Calculate()
        .w_SELEZM = "S"
          .DoRTCalc(41,53,.f.)
        .w_VISFASI = IIF( g_PRFA='S' AND g_CICLILAV='S' and .w_MPARAM<>'E', 'S', 'N')
      .oPgFrm.Page2.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
          .DoRTCalc(55,56,.f.)
        .w_KEYRIF = SYS(2015)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_38.enabled = this.oPgFrm.Page2.oPag.oBtn_2_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_39.enabled = this.oPgFrm.Page2.oPag.oBtn_2_39.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomODL.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .DoRTCalc(1,12,.t.)
            .w_ODLSEL = .w_ZoomODL.GetVar("OLCODODL")
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(AH_MsgFormat(IIF(.w_MPARAM="I", "Stato ordini di lavorazione (ODL)", IIF(.w_MPARAM='L', "Stato ordini di conto lavoro (OCL)", IIF(.w_MPARAM="E", "Stato ordini di acquisto (ODA)", IIF(.w_MPARAM="S", "Stato ordini di smontaggio (ODS)", "Stato ordini di lavorazione (ODL)"))))))
        if .o_CRIELA<>.w_CRIELA
          .Calculate_VASGIKTUPU()
        endif
        if .o_SELEZM<>.w_SELEZM
          .Calculate_LOGQKNSSGF()
        endif
        .oPgFrm.Page2.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page2.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,57,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomODL.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(AH_MsgFormat(IIF(.w_MPARAM="I", "Stato ordini di lavorazione (ODL)", IIF(.w_MPARAM='L', "Stato ordini di conto lavoro (OCL)", IIF(.w_MPARAM="E", "Stato ordini di acquisto (ODA)", IIF(.w_MPARAM="S", "Stato ordini di smontaggio (ODS)", "Stato ordini di lavorazione (ODL)"))))))
        .oPgFrm.Page2.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page2.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
    endwith
  return

  proc Calculate_SIIWOFCZUM()
    with this
          * --- Lancia evento interroga
          .oPgFrm.ActivePage = 1
          .w_RET = this.notifyevent("Interroga")
    endwith
  endproc
  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          .w_ZOOMMAGA.cZoomFile = IIF(.w_CRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_CRIELA = 'G', "QUERY\GSVEGKGF" , "QUERY\GSVEFKGF")
    endwith
  endproc
  proc Calculate_XOHCHPGRKK()
    with this
          * --- Condizione di editing Label e zoom
          .w_LBLMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.GRD.Enabled = .w_ZOOMMAGA.Enabled
    endwith
  endproc
  proc Calculate_LRBMCECHHD()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_VASGIKTUPU()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_CRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_CRIELA $ 'G-M'
          .w_RET = .NotifyEvent("InterrogaMaga")
          .w_SELEZM = IIF(.w_CRIELA $ 'G-M' , 'S', SPACE(1) )
      if .w_CRIELA $ 'G-M'
          .w_RET = .w_ZOOMMAGA.CheckAll()
      endif
      if .w_CRIELA = 'A'
          .w_RET = .w_ZOOMMAGA.UnCheckAll()
      endif
    endwith
  endproc
  proc Calculate_LOGQKNSSGF()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
      if .w_SELEZM='S'
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
      if .w_SELEZM='D'
          .w_RET = .w_ZOOMMAGA.uncheckall()
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCODCOM_2_11.enabled = this.oPgFrm.Page2.oPag.oCODCOM_2_11.mCond()
    this.oPgFrm.Page2.oPag.oCODATT_2_12.enabled = this.oPgFrm.Page2.oPag.oCODATT_2_12.mCond()
    this.oPgFrm.Page2.oPag.oCODCON_2_13.enabled = this.oPgFrm.Page2.oPag.oCODCON_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCODFASI_2_15.enabled = this.oPgFrm.Page2.oPag.oCODFASI_2_15.mCond()
    this.oPgFrm.Page2.oPag.oCODFASF_2_16.enabled = this.oPgFrm.Page2.oPag.oCODFASF_2_16.mCond()
    this.oPgFrm.Page2.oPag.oSELEZM_2_37.enabled_(this.oPgFrm.Page2.oPag.oSELEZM_2_37.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_31.visible=!this.oPgFrm.Page1.oPag.oBtn_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page2.oPag.oCODFASI_2_15.visible=!this.oPgFrm.Page2.oPag.oCODFASI_2_15.mHide()
    this.oPgFrm.Page2.oPag.oCODFASF_2_16.visible=!this.oPgFrm.Page2.oPag.oCODFASF_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_74.visible=!this.oPgFrm.Page2.oPag.oStr_2_74.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_75.visible=!this.oPgFrm.Page2.oPag.oStr_2_75.mHide()
    this.oPgFrm.Page2.oPag.oVISFASI_2_78.visible=!this.oPgFrm.Page2.oPag.oVISFASI_2_78.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_85.visible=!this.oPgFrm.Page2.oPag.oStr_2_85.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_86.visible=!this.oPgFrm.Page2.oPag.oStr_2_86.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomODL.Event(cEvent)
        if lower(cEvent)==lower("Sinterroga")
          .Calculate_SIIWOFCZUM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed") or lower(cEvent)==lower("w_ZOOMMAGA after query")
          .Calculate_XOHCHPGRKK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMMAGA menucheck") or lower(cEvent)==lower("w_ZOOMMAGA row checked") or lower(cEvent)==lower("w_ZOOMMAGA row unchecked") or lower(cEvent)==lower("w_zoommaga rowcheckall") or lower(cEvent)==lower("w_zoommaga rowuncheckall")
          .Calculate_LRBMCECHHD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_VASGIKTUPU()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page2.oPag.LBLMAGA.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsco_kpo
    if upper(cEvent)='FORMLOAD'
      this.w_ZOOMMAGA.GRD.SCROLLBARS=2
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DISINI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DISINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DISINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDISINI_2_1'),i_cWhere,'',"Distinta base",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DISINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DISINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DISINI <= .w_DISFIN OR EMPTY(.w_DISFIN)) and COCHKAR(.w_DISINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DISINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISFIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DISFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DISFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDISFIN_2_2'),i_cWhere,'',"Distinta base",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DISFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DISFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DISINI <= .w_DISFIN and COCHKAR(.w_DISFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DISFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_2_3'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_2_4'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_2_5'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_2_6'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_2_7'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_2_8'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_2_9'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_2_10'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_2_11'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_2_12'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_2_13'),i_cWhere,'',"FORNITORI",'GSDB_KCS1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un fornitore di conto lavoro")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTER = NVL(_Link_.ANMAGTER,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_MAGTER = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and (.w_MPS_ODL='L' And not empty(.w_MAGTER) Or .w_MPS_ODL<>'L')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un fornitore di conto lavoro")
        endif
        this.w_CODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_MAGTER = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERMRP
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_lTable = "SEL__MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2], .t., this.SEL__MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERMRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEL__MRP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMSERIAL like "+cp_ToStrODBC(trim(this.w_SERMRP)+"%");

          i_ret=cp_SQL(i_nConn,"select GMSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMSERIAL',trim(this.w_SERMRP))
          select GMSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERMRP)==trim(_Link_.GMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERMRP) and !this.bDontReportError
            deferred_cp_zoom('SEL__MRP','*','GMSERIAL',cp_AbsName(oSource.parent,'oSERMRP_2_14'),i_cWhere,'',"Archivio generazione MRP",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where GMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMSERIAL',oSource.xKey(1))
            select GMSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERMRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where GMSERIAL="+cp_ToStrODBC(this.w_SERMRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMSERIAL',this.w_SERMRP)
            select GMSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERMRP = NVL(_Link_.GMSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SERMRP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])+'\'+cp_ToStr(_Link_.GMSERIAL,1)
      cp_ShowWarn(i_cKey,this.SEL__MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERMRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFASI
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFASI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODFAS like "+cp_ToStrODBC(trim(this.w_CODFASI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODFAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODFAS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODFAS',trim(this.w_CODFASI))
          select CACODFAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODFAS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFASI)==trim(_Link_.CACODFAS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFASI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODFAS',cp_AbsName(oSource.parent,'oCODFASI_2_15'),i_cWhere,'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                     +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',oSource.xKey(1))
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFASI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(this.w_CODFASI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',this.w_CODFASI)
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFASI = NVL(_Link_.CACODFAS,space(66))
    else
      if i_cCtrl<>'Load'
        this.w_CODFASI = space(66)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODFASI<=.w_CODFASF or empty(.w_CODFASF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFASI = space(66)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODFAS,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFASI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFASF
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFASF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODFAS like "+cp_ToStrODBC(trim(this.w_CODFASF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODFAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODFAS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODFAS',trim(this.w_CODFASF))
          select CACODFAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODFAS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFASF)==trim(_Link_.CACODFAS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFASF) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODFAS',cp_AbsName(oSource.parent,'oCODFASF_2_16'),i_cWhere,'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                     +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',oSource.xKey(1))
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFASF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(this.w_CODFASF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',this.w_CODFASF)
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFASF = NVL(_Link_.CACODFAS,space(66))
    else
      if i_cCtrl<>'Load'
        this.w_CODFASF = space(66)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODFASI<=.w_CODFASF or empty(.w_CODFASI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFASF = space(66)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODFAS,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFASF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oDISINI_2_1.value==this.w_DISINI)
      this.oPgFrm.Page2.oPag.oDISINI_2_1.value=this.w_DISINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDISFIN_2_2.value==this.w_DISFIN)
      this.oPgFrm.Page2.oPag.oDISFIN_2_2.value=this.w_DISFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMAINI_2_3.value==this.w_FAMAINI)
      this.oPgFrm.Page2.oPag.oFAMAINI_2_3.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMAFIN_2_4.value==this.w_FAMAFIN)
      this.oPgFrm.Page2.oPag.oFAMAFIN_2_4.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUINI_2_5.value==this.w_GRUINI)
      this.oPgFrm.Page2.oPag.oGRUINI_2_5.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUFIN_2_6.value==this.w_GRUFIN)
      this.oPgFrm.Page2.oPag.oGRUFIN_2_6.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCATINI_2_7.value==this.w_CATINI)
      this.oPgFrm.Page2.oPag.oCATINI_2_7.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCATFIN_2_8.value==this.w_CATFIN)
      this.oPgFrm.Page2.oPag.oCATFIN_2_8.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGINI_2_9.value==this.w_MAGINI)
      this.oPgFrm.Page2.oPag.oMAGINI_2_9.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGFIN_2_10.value==this.w_MAGFIN)
      this.oPgFrm.Page2.oPag.oMAGFIN_2_10.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_11.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_11.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATT_2_12.value==this.w_CODATT)
      this.oPgFrm.Page2.oPag.oCODATT_2_12.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_2_13.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_2_13.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oSERMRP_2_14.value==this.w_SERMRP)
      this.oPgFrm.Page2.oPag.oSERMRP_2_14.value=this.w_SERMRP
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFASI_2_15.value==this.w_CODFASI)
      this.oPgFrm.Page2.oPag.oCODFASI_2_15.value=this.w_CODFASI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFASF_2_16.value==this.w_CODFASF)
      this.oPgFrm.Page2.oPag.oCODFASF_2_16.value=this.w_CODFASF
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPGES_2_17.RadioValue()==this.w_TIPGES)
      this.oPgFrm.Page2.oPag.oTIPGES_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDININI_2_18.value==this.w_DININI)
      this.oPgFrm.Page2.oPag.oDININI_2_18.value=this.w_DININI
    endif
    if not(this.oPgFrm.Page2.oPag.oDINFIN_2_19.value==this.w_DINFIN)
      this.oPgFrm.Page2.oPag.oDINFIN_2_19.value=this.w_DINFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDFIINI_2_20.value==this.w_DFIINI)
      this.oPgFrm.Page2.oPag.oDFIINI_2_20.value=this.w_DFIINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDFIFIN_2_21.value==this.w_DFIFIN)
      this.oPgFrm.Page2.oPag.oDFIFIN_2_21.value=this.w_DFIFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSUGGER_2_25.RadioValue()==this.w_SUGGER)
      this.oPgFrm.Page2.oPag.oSUGGER_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPIANIF_2_26.RadioValue()==this.w_PIANIF)
      this.oPgFrm.Page2.oPag.oPIANIF_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLANCIA_2_27.RadioValue()==this.w_LANCIA)
      this.oPgFrm.Page2.oPag.oLANCIA_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFINITO_2_28.RadioValue()==this.w_FINITO)
      this.oPgFrm.Page2.oPag.oFINITO_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCRIELA_2_35.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page2.oPag.oCRIELA_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZM_2_37.RadioValue()==this.w_SELEZM)
      this.oPgFrm.Page2.oPag.oSELEZM_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAN_2_47.value==this.w_DESCAN)
      this.oPgFrm.Page2.oPag.oDESCAN_2_47.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT_2_49.value==this.w_DESATT)
      this.oPgFrm.Page2.oPag.oDESATT_2_49.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAI_2_50.value==this.w_DESFAMAI)
      this.oPgFrm.Page2.oPag.oDESFAMAI_2_50.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUI_2_51.value==this.w_DESGRUI)
      this.oPgFrm.Page2.oPag.oDESGRUI_2_51.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATI_2_52.value==this.w_DESCATI)
      this.oPgFrm.Page2.oPag.oDESCATI_2_52.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAF_2_56.value==this.w_DESFAMAF)
      this.oPgFrm.Page2.oPag.oDESFAMAF_2_56.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUF_2_57.value==this.w_DESGRUF)
      this.oPgFrm.Page2.oPag.oDESGRUF_2_57.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATF_2_58.value==this.w_DESCATF)
      this.oPgFrm.Page2.oPag.oDESCATF_2_58.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGI_2_63.value==this.w_DESMAGI)
      this.oPgFrm.Page2.oPag.oDESMAGI_2_63.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGF_2_65.value==this.w_DESMAGF)
      this.oPgFrm.Page2.oPag.oDESMAGF_2_65.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDISI_2_67.value==this.w_DESDISI)
      this.oPgFrm.Page2.oPag.oDESDISI_2_67.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDISF_2_69.value==this.w_DESDISF)
      this.oPgFrm.Page2.oPag.oDESDISF_2_69.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_76.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_76.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oVISFASI_2_78.RadioValue()==this.w_VISFASI)
      this.oPgFrm.Page2.oPag.oVISFASI_2_78.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DISINI <= .w_DISFIN OR EMPTY(.w_DISFIN)) and COCHKAR(.w_DISINI))  and not(empty(.w_DISINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDISINI_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DISINI <= .w_DISFIN and COCHKAR(.w_DISFIN))  and not(empty(.w_DISFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDISFIN_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMAINI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMAFIN_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUINI_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUFIN_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCATINI_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCATFIN_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMAGINI_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN)  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMAGFIN_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and (.w_MPS_ODL='L' And not empty(.w_MAGTER) Or .w_MPS_ODL<>'L'))  and (g_COLA="S" and .w_MPS_ODL='L' Or g_MODA='S' And .w_MPS_ODL='E')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un fornitore di conto lavoro")
          case   not(.w_CODFASI<=.w_CODFASF or empty(.w_CODFASF))  and not(.w_MPARAM='E')  and (g_PRFA='S' AND g_CICLILAV='S')  and not(empty(.w_CODFASI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFASI_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODFASI<=.w_CODFASF or empty(.w_CODFASI))  and not(.w_MPARAM='E')  and (g_PRFA='S' AND g_CICLILAV='S')  and not(empty(.w_CODFASF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFASF_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DININI<=.w_DINFIN or empty(.w_DINFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDININI_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DININI<=.w_DINFIN or empty(.w_DININI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDINFIN_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DFIINI<=.w_DFIFIN or empty(.w_DFIFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDFIINI_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DFIINI<=.w_DFIFIN or empty(.w_DFIINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDFIFIN_2_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CRIELA = this.w_CRIELA
    this.o_SELEZM = this.w_SELEZM
    return

enddefine

* --- Define pages as container
define class tgsco_kpoPag1 as StdContainer
  Width  = 803
  height = 543
  stdWidth  = 803
  stdheight = 543
  resizeXpos=443
  resizeYpos=220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomODL as cp_zoombox with uid="SARZPLMRGC",left=17, top=49, width=786,height=432,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ODL_MAST",cZoomFile="GSCO_KPO",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bRetriveAllRows=.f.,bQueryOnLoad=.t.,cMenuFile="GSCO_KPO",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_14 as StdButton with uid="LXSIITXWMU",left=745, top=5, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue interrogazione";
    , HelpContextID = 118905590;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .notifyevent("Sinterroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="IXTLGCYOYA",left=590, top=492, width=48,height=45,;
    CpPicture="BMP\STAMPA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la lista OCL";
    , HelpContextID = 58584042;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      do GSCL_SPO with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MPARAM='L')
      endwith
    endif
  endfunc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MPARAM<>'L')
     endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="AHYJXDMAZH",left=590, top=492, width=48,height=45,;
    CpPicture="BMP\STAMPA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la lista ODA";
    , HelpContextID = 58584042;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      do GSCO_SPO with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MPARAM='E')
      endwith
    endif
  endfunc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MPARAM<>'E')
     endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="ZGENOSFTEE",left=590, top=492, width=48,height=45,;
    CpPicture="BMP\STAMPA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la lista ODL";
    , HelpContextID = 58584042;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      do GSCO_SOL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MPARAM='I')
      endwith
    endif
  endfunc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MPARAM<>'I')
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="PCPHGAXHMT",left=644, top=492, width=48,height=45,;
    CpPicture="BMP\DIBA21.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la verifica temporale";
    , HelpContextID = 58584042;
    , Caption='\<Ver.Temp';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSCO_BPO(this.Parent.oContained,"Verifica temporale")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="JIMABNCPBI",left=746, top=492, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 118905590;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_28 as cp_runprogram with uid="XWZFIUFFER",left=828, top=20, width=227,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BPO("VISUALIZZA_ODL")',;
    cEvent = "w_zoomodl selected",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_30 as cp_runprogram with uid="RFLKRTOYFP",left=828, top=46, width=227,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BPO("MENU_LISTAODL")',;
    cEvent = "w_zoomodl MouseRightClick",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_31 as StdButton with uid="TETENPUJZR",left=695, top=492, width=48,height=45,;
    CpPicture="BMP\DIBA24.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la lista dei materiali";
    , HelpContextID = 58584042;
    , Caption='\<Lista Mat';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      do GSCO_SVF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_MPARAM $ 'E-S')
      endwith
    endif
  endfunc

  func oBtn_1_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MPARAM $ 'E-S')
     endwith
    endif
  endfunc


  add object oObj_1_34 as cp_runprogram with uid="XMYZSZSXGZ",left=828, top=72, width=227,height=23,;
    caption='GSCO_BPO("TI")',;
   bGlobalFont=.t.,;
    prg="GSCO_BPO('TI')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 87537867


  add object oObj_1_37 as cp_calclbl with uid="HXBESCYRWM",left=147, top=12, width=208,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Stato ordini di lavorazione (ODL)",FontBold=1,;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_38 as cp_runprogram with uid="UZEKAXHMMN",left=828, top=-4, width=227,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BPO('ESCI')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 239967770

  add object oStr_1_22 as StdString with uid="HQTUAZJBUQ",Visible=.t., Left=172, Top=28,;
    Alignment=0, Width=82, Height=15,;
    Caption="Pianificato"    , forecolor=rgb(0,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_MPARAM $ 'EL')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="RKWFCFTVCY",Visible=.t., Left=293, Top=28,;
    Alignment=0, Width=57, Height=15,;
    Caption="Lanciato"    , forecolor=rgb(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_MPARAM $ 'EL')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="OAHAFDKAQV",Visible=.t., Left=399, Top=28,;
    Alignment=0, Width=57, Height=15,;
    Caption="Finito"    , forecolor=rgb(0,128,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="QPHENPPGNJ",Visible=.t., Left=523, Top=28,;
    Alignment=2, Width=85, Height=15,;
    Caption="Critico"    , backcolor=rgb(255,255,0), backstyle=1;
  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RJNSIOYHND",Visible=.t., Left=622, Top=28,;
    Alignment=2, Width=85, Height=15,;
    Caption="In ritardo"    , backcolor=rgb(255,128,128), backstyle=1;
  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HOMLNHXKFU",Visible=.t., Left=519, Top=10,;
    Alignment=2, Width=187, Height=15,;
    Caption="Situazione temporale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="RZCKYDGVNO",Visible=.t., Left=61, Top=28,;
    Alignment=0, Width=82, Height=15,;
    Caption="Suggerito"    , forecolor=rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_MPARAM='S')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="QWWYQRUBWY",Visible=.t., Left=172, Top=28,;
    Alignment=0, Width=82, Height=15,;
    Caption="Da ordinare"    , forecolor=rgb(0,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (! .w_MPARAM $ 'EL')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="NKUGBGIJGD",Visible=.t., Left=293, Top=28,;
    Alignment=0, Width=67, Height=15,;
    Caption="Ordinato"    , forecolor=rgb(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (! .w_MPARAM $ 'EL')
    endwith
  endfunc

  add object oBox_1_21 as StdBox with uid="QCLIRBAUES",left=18, top=7, width=478,height=40

  add object oBox_1_27 as StdBox with uid="EZGCIJMHPJ",left=496, top=7, width=242,height=40
enddefine
define class tgsco_kpoPag2 as StdContainer
  Width  = 803
  height = 543
  stdWidth  = 803
  stdheight = 543
  resizeXpos=483
  resizeYpos=255
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDISINI_2_1 as StdField with uid="YWHDPUYAJR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DISINI", cQueryName = "DISINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta per inizio selezione",;
    HelpContextID = 180037578,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=111, Top=34, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DISINI"

  func oDISINI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISINI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISINI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDISINI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Distinta base",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDISFIN_2_2 as StdField with uid="KREOZVMBQJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DISFIN", cQueryName = "DISFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta per fine selezione",;
    HelpContextID = 101590986,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=111, Top=57, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DISFIN"

  proc oDISFIN_2_2.mDefault
    with this.Parent.oContained
      if empty(.w_DISFIN)
        .w_DISFIN = .w_DISINI
      endif
    endwith
  endproc

  func oDISFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISFIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISFIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDISFIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Distinta base",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_2_3 as StdField with uid="YHAICNGUXI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 166490198,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=80, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_2_4 as StdField with uid="NKHVVGAWBH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 188977066,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=103, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_2_4.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_2_5 as StdField with uid="MOONSGRLZI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 180027034,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=126, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_2_6 as StdField with uid="ZRACCDCBBB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 101580442,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_2_6.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_2_7 as StdField with uid="DZJDTBXLDQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 180035546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=172, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_2_8 as StdField with uid="IRNXNIGSBO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 101588954,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=195, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_2_8.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oMAGINI_2_9 as StdField with uid="CQKMNZLRTQ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 180088634,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=246, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMAGFIN_2_10 as StdField with uid="LJGJRYSZIP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 101642042,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=269, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_2_10.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oCODCOM_2_11 as StdField with uid="EXQODIOMOH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 112333274,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=292, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_2_12('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_2_12 as StdField with uid="NHVJWOTPPC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita",;
    HelpContextID = 258216410,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=316, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" and not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oCODCON_2_13 as StdField with uid="DJFRGGMNRU",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un fornitore di conto lavoro",;
    ToolTipText = "Codice fornitore conto lavoro...",;
    HelpContextID = 95556058,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=340, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COLA="S" and .w_MPS_ODL='L' Or g_MODA='S' And .w_MPS_ODL='E')
    endwith
   endif
  endfunc

  proc oCODCON_2_13.mAfter
    with this.Parent.oContained
       .w_CODCON=CALCZER(.w_CODCON, 'CONTI')
    endwith
  endproc

  func oCODCON_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FORNITORI",'GSDB_KCS1.CONTI_VZM',this.parent.oContained
  endproc

  add object oSERMRP_2_14 as StdField with uid="EEAADUFPEU",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SERMRP", cQueryName = "SERMRP",nZero=10,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale di elaborazione MRP",;
    HelpContextID = 58145498,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=111, Top=363, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEL__MRP", oKey_1_1="GMSERIAL", oKey_1_2="this.w_SERMRP"

  func oSERMRP_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERMRP_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERMRP_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEL__MRP','*','GMSERIAL',cp_AbsName(this.parent,'oSERMRP_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivio generazione MRP",'',this.parent.oContained
  endproc

  add object oCODFASI_2_15 as StdField with uid="IDUNPUJVAF",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODFASI", cQueryName = "CODFASI",;
    bObbl = .f. , nPag = 2, value=space(66), bMultilanguage =  .f.,;
    ToolTipText = "Codice di fase di inizio selezione",;
    HelpContextID = 242282022,;
   bGlobalFont=.t.,;
    Height=21, Width=475, Left=111, Top=386, InputMask=replicate('X',66), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODFAS", oKey_1_2="this.w_CODFASI"

  func oCODFASI_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' AND g_CICLILAV='S')
    endwith
   endif
  endfunc

  func oCODFASI_2_15.mHide()
    with this.Parent.oContained
      return (.w_MPARAM='E')
    endwith
  endfunc

  func oCODFASI_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFASI_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFASI_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODFAS',cp_AbsName(this.parent,'oCODFASI_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oCODFASF_2_16 as StdField with uid="PXXEHJTQRD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODFASF", cQueryName = "CODFASF",;
    bObbl = .f. , nPag = 2, value=space(66), bMultilanguage =  .f.,;
    ToolTipText = "Codice di fase di fine selezione",;
    HelpContextID = 242282022,;
   bGlobalFont=.t.,;
    Height=21, Width=475, Left=111, Top=410, InputMask=replicate('X',66), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODFAS", oKey_1_2="this.w_CODFASF"

  proc oCODFASF_2_16.mDefault
    with this.Parent.oContained
      if empty(.w_CODFASF)
        .w_CODFASF = .w_CODFASI
      endif
    endwith
  endproc

  func oCODFASF_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' AND g_CICLILAV='S')
    endwith
   endif
  endfunc

  func oCODFASF_2_16.mHide()
    with this.Parent.oContained
      return (.w_MPARAM='E')
    endwith
  endfunc

  func oCODFASF_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFASF_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFASF_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODFAS',cp_AbsName(this.parent,'oCODFASF_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this.parent.oContained
  endproc


  add object oTIPGES_2_17 as StdCombo with uid="QFVODWEYFZ",rtseq=30,rtrep=.f.,left=689,top=388,width=109,height=21;
    , ToolTipText = "Tipo gestione articolo";
    , HelpContextID = 21845706;
    , cFormVar="w_TIPGES",RowSource=""+"Fabbisogno,"+"Scorta,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPGES_2_17.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oTIPGES_2_17.GetRadio()
    this.Parent.oContained.w_TIPGES = this.RadioValue()
    return .t.
  endfunc

  func oTIPGES_2_17.SetRadio()
    this.Parent.oContained.w_TIPGES=trim(this.Parent.oContained.w_TIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGES=='F',1,;
      iif(this.Parent.oContained.w_TIPGES=='S',2,;
      iif(this.Parent.oContained.w_TIPGES=='X',3,;
      0)))
  endfunc

  add object oDININI_2_18 as StdField with uid="DWDYLAEQAF",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DININI", cQueryName = "DININI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 180058058,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=111, Top=460

  func oDININI_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DININI<=.w_DINFIN or empty(.w_DINFIN))
    endwith
    return bRes
  endfunc

  add object oDINFIN_2_19 as StdField with uid="SKRSVLASNU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DINFIN", cQueryName = "DINFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine",;
    HelpContextID = 101611466,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=294, Top=460

  proc oDINFIN_2_19.mDefault
    with this.Parent.oContained
      if empty(.w_DINFIN)
        .w_DINFIN = .w_DININI
      endif
    endwith
  endproc

  func oDINFIN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DININI<=.w_DINFIN or empty(.w_DININI))
    endwith
    return bRes
  endfunc

  add object oDFIINI_2_20 as StdField with uid="WTVHRIJCBX",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DFIINI", cQueryName = "DFIINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 180079306,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=523, Top=460

  func oDFIINI_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DFIINI<=.w_DFIFIN or empty(.w_DFIFIN))
    endwith
    return bRes
  endfunc

  add object oDFIFIN_2_21 as StdField with uid="CIQSGHNOZL",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DFIFIN", cQueryName = "DFIFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine",;
    HelpContextID = 101632714,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=702, Top=460

  proc oDFIFIN_2_21.mDefault
    with this.Parent.oContained
      if empty(.w_DFIFIN)
        .w_DFIFIN = .w_DFIINI
      endif
    endwith
  endproc

  func oDFIFIN_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DFIINI<=.w_DFIFIN or empty(.w_DFIINI))
    endwith
    return bRes
  endfunc

  add object oSUGGER_2_25 as StdCheck with uid="XVWHFVQMAQ",rtseq=35,rtrep=.f.,left=23, top=507, caption="Suggerito",;
    HelpContextID = 38656730,;
    cFormVar="w_SUGGER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSUGGER_2_25.RadioValue()
    return(iif(this.value =1,'M',;
    '\'))
  endfunc
  func oSUGGER_2_25.GetRadio()
    this.Parent.oContained.w_SUGGER = this.RadioValue()
    return .t.
  endfunc

  func oSUGGER_2_25.SetRadio()
    this.Parent.oContained.w_SUGGER=trim(this.Parent.oContained.w_SUGGER)
    this.value = ;
      iif(this.Parent.oContained.w_SUGGER=='M',1,;
      0)
  endfunc

  add object oPIANIF_2_26 as StdCheck with uid="WUOPMVAEMD",rtseq=36,rtrep=.f.,left=111, top=507, caption="Pianificato",;
    HelpContextID = 235357962,;
    cFormVar="w_PIANIF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPIANIF_2_26.RadioValue()
    return(iif(this.value =1,'P',;
    '\'))
  endfunc
  func oPIANIF_2_26.GetRadio()
    this.Parent.oContained.w_PIANIF = this.RadioValue()
    return .t.
  endfunc

  func oPIANIF_2_26.SetRadio()
    this.Parent.oContained.w_PIANIF=trim(this.Parent.oContained.w_PIANIF)
    this.value = ;
      iif(this.Parent.oContained.w_PIANIF=='P',1,;
      0)
  endfunc

  add object oLANCIA_2_27 as StdCheck with uid="IFRDPDYAKQ",rtseq=37,rtrep=.f.,left=219, top=507, caption="Lanciato",;
    HelpContextID = 51478346,;
    cFormVar="w_LANCIA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oLANCIA_2_27.RadioValue()
    return(iif(this.value =1,'L',;
    '\'))
  endfunc
  func oLANCIA_2_27.GetRadio()
    this.Parent.oContained.w_LANCIA = this.RadioValue()
    return .t.
  endfunc

  func oLANCIA_2_27.SetRadio()
    this.Parent.oContained.w_LANCIA=trim(this.Parent.oContained.w_LANCIA)
    this.value = ;
      iif(this.Parent.oContained.w_LANCIA=='L',1,;
      0)
  endfunc

  add object oFINITO_2_28 as StdCheck with uid="RQTLYQXEOF",rtseq=38,rtrep=.f.,left=325, top=507, caption="Finito",;
    HelpContextID = 73103274,;
    cFormVar="w_FINITO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFINITO_2_28.RadioValue()
    return(iif(this.value =1,'F',;
    '\'))
  endfunc
  func oFINITO_2_28.GetRadio()
    this.Parent.oContained.w_FINITO = this.RadioValue()
    return .t.
  endfunc

  func oFINITO_2_28.SetRadio()
    this.Parent.oContained.w_FINITO=trim(this.Parent.oContained.w_FINITO)
    this.value = ;
      iif(this.Parent.oContained.w_FINITO=='F',1,;
      0)
  endfunc


  add object oCRIELA_2_35 as StdCombo with uid="VSDAZQQBWZ",rtseq=39,rtrep=.f.,left=641,top=83,width=157,height=21;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 48217818;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCRIELA_2_35.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_2_35.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_2_35.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=451, top=126, width=352,height=237,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=2;
    , HelpContextID = 239967770

  add object oSELEZM_2_37 as StdRadio with uid="LTQWYYFJNE",rtseq=40,rtrep=.f.,left=563, top=366, width=239,height=20;
    , cFormVar="w_SELEZM", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZM_2_37.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 100637402
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 100637402
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZM_2_37.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZM_2_37.GetRadio()
    this.Parent.oContained.w_SELEZM = this.RadioValue()
    return .t.
  endfunc

  func oSELEZM_2_37.SetRadio()
    this.Parent.oContained.w_SELEZM=trim(this.Parent.oContained.w_SELEZM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZM=="S",1,;
      iif(this.Parent.oContained.w_SELEZM=="D",2,;
      0))
  endfunc

  func oSELEZM_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIELA $ 'G-M')
    endwith
   endif
  endfunc


  add object oBtn_2_38 as StdButton with uid="ZHZYLRMAHB",left=696, top=496, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Riesegue interrogazione";
    , HelpContextID = 118905590;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_38.Click()
      with this.Parent.oContained
        .notifyevent("SInterroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_39 as StdButton with uid="ABWEJRUGJP",left=746, top=496, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 118905590;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAN_2_47 as StdField with uid="PJHERXXTYJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 110177226,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=243, Top=292, InputMask=replicate('X',30)

  add object oDESATT_2_49 as StdField with uid="JRUPUPZCWV",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 258157514,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=243, Top=316, InputMask=replicate('X',30)

  add object oDESFAMAI_2_50 as StdField with uid="AOYBYAMKXI",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 126757761,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=80, InputMask=replicate('X',35)

  add object oDESGRUI_2_51 as StdField with uid="ZABORREMDH",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25351222,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=126, InputMask=replicate('X',35)

  add object oDESCATI_2_52 as StdField with uid="ZFWYARIWSP",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258921526,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=172, InputMask=replicate('X',35)

  add object oDESFAMAF_2_56 as StdField with uid="WOCDSQXKSA",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 126757764,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=103, InputMask=replicate('X',35)

  add object oDESGRUF_2_57 as StdField with uid="PYGQUHDJMJ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25351222,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=149, InputMask=replicate('X',35)

  add object oDESCATF_2_58 as StdField with uid="SXSGGSHCII",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258921526,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=195, InputMask=replicate('X',35)

  add object oDESMAGI_2_63 as StdField with uid="QONUNYBHAK",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 41473078,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=174, Top=246, InputMask=replicate('X',30)

  add object oDESMAGF_2_65 as StdField with uid="EBXVFVWORM",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 41473078,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=173, Top=269, InputMask=replicate('X',30)

  add object oDESDISI_2_67 as StdField with uid="QSEMNVVIFP",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 250598454,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=277, Top=34, InputMask=replicate('X',40)

  add object oDESDISF_2_69 as StdField with uid="BLVZKOKGVG",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 250598454,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=277, Top=57, InputMask=replicate('X',40)

  add object oDESFOR_2_76 as StdField with uid="RJLIHNJXRO",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 28191690,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=243, Top=340, InputMask=replicate('X',40)

  add object oVISFASI_2_78 as StdCheck with uid="RDWKTDSHYZ",rtseq=54,rtrep=.f.,left=450, top=507, caption="Visualizza ordini di fase",;
    HelpContextID = 242342230,;
    cFormVar="w_VISFASI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVISFASI_2_78.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISFASI_2_78.GetRadio()
    this.Parent.oContained.w_VISFASI = this.RadioValue()
    return .t.
  endfunc

  func oVISFASI_2_78.SetRadio()
    this.Parent.oContained.w_VISFASI=trim(this.Parent.oContained.w_VISFASI)
    this.value = ;
      iif(this.Parent.oContained.w_VISFASI=='S',1,;
      0)
  endfunc

  func oVISFASI_2_78.mHide()
    with this.Parent.oContained
      return (!(g_PRFA='S' AND g_CICLILAV='S') or .w_MPARAM='E')
    endwith
  endfunc


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=458, top=116, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=2;
    , HelpContextID = 239967770

  add object oStr_2_22 as StdString with uid="DADRQEEMPZ",Visible=.t., Left=22, Top=460,;
    Alignment=1, Width=88, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="TUFGOJTXDO",Visible=.t., Left=207, Top=460,;
    Alignment=1, Width=86, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="UVLFZGJAIQ",Visible=.t., Left=23, Top=484,;
    Alignment=0, Width=119, Height=15,;
    Caption="Stato avanzamento"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="CNBJQOZXUP",Visible=.t., Left=23, Top=434,;
    Alignment=0, Width=217, Height=15,;
    Caption="Intervallo di inizio lavorazione"  ;
  , bGlobalFont=.t.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (.w_pOLTPROVE="E")
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="GMULINMLGZ",Visible=.t., Left=435, Top=460,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="RZOCRZYSJY",Visible=.t., Left=620, Top=460,;
    Alignment=1, Width=81, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="DMEDRLEOXT",Visible=.t., Left=449, Top=434,;
    Alignment=0, Width=208, Height=15,;
    Caption="Intervallo di fine lavorazione"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (.w_pOLTPROVE="E")
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=8, Top=293,;
    Alignment=1, Width=102, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=8, Top=317,;
    Alignment=1, Width=102, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=15, Top=83,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=15, Top=128,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=15, Top=174,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="BETNLUNOYI",Visible=.t., Left=15, Top=106,;
    Alignment=1, Width=95, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=15, Top=151,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=15, Top=197,;
    Alignment=1, Width=95, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_62 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=8, Top=246,;
    Alignment=1, Width=102, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=8, Top=269,;
    Alignment=1, Width=102, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_66 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=15, Top=37,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_68 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=15, Top=59,;
    Alignment=1, Width=95, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_70 as StdString with uid="WHGWHEWFXM",Visible=.t., Left=10, Top=8,;
    Alignment=0, Width=187, Height=18,;
    Caption="Codice articolo"  ;
  , bGlobalFont=.t.

  add object oStr_2_72 as StdString with uid="UXMZEKGIAI",Visible=.t., Left=9, Top=222,;
    Alignment=0, Width=187, Height=15,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="BIWCPCYJUI",Visible=.t., Left=23, Top=434,;
    Alignment=0, Width=187, Height=15,;
    Caption="Intervallo su data ordine"  ;
  , bGlobalFont=.t.

  func oStr_2_74.mHide()
    with this.Parent.oContained
      return (.w_pOLTPROVE<>"E")
    endwith
  endfunc

  add object oStr_2_75 as StdString with uid="HUASICBFFG",Visible=.t., Left=449, Top=434,;
    Alignment=0, Width=187, Height=15,;
    Caption="Intervallo su data evasione"  ;
  , bGlobalFont=.t.

  func oStr_2_75.mHide()
    with this.Parent.oContained
      return (.w_pOLTPROVE<>"E")
    endwith
  endfunc

  add object oStr_2_77 as StdString with uid="IKCZCLZTPB",Visible=.t., Left=17, Top=343,;
    Alignment=1, Width=93, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=454, Top=83,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_81 as StdString with uid="AGNAIFECYP",Visible=.t., Left=-31, Top=364,;
    Alignment=1, Width=141, Height=15,;
    Caption="Seriale MRP:"  ;
  , bGlobalFont=.t.

  add object oStr_2_84 as StdString with uid="CJIPWEAGWS",Visible=.t., Left=589, Top=389,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_85 as StdString with uid="VBDGHHTYIN",Visible=.t., Left=3, Top=410,;
    Alignment=1, Width=107, Height=15,;
    Caption="A codice di fase:"  ;
  , bGlobalFont=.t.

  func oStr_2_85.mHide()
    with this.Parent.oContained
      return (.w_MPARAM='E')
    endwith
  endfunc

  add object oStr_2_86 as StdString with uid="XMQKHQVLBA",Visible=.t., Left=-1, Top=386,;
    Alignment=1, Width=111, Height=15,;
    Caption="Da codice di fase:"  ;
  , bGlobalFont=.t.

  func oStr_2_86.mHide()
    with this.Parent.oContained
      return (.w_MPARAM='E')
    endwith
  endfunc

  add object oBox_2_41 as StdBox with uid="JIDEAZMPUB",left=10, top=452, width=772,height=1

  add object oBox_2_45 as StdBox with uid="MQZDGDNPAK",left=10, top=503, width=385,height=1

  add object oBox_2_71 as StdBox with uid="AVMMROXYXX",left=10, top=28, width=789,height=1

  add object oBox_2_73 as StdBox with uid="XNHQMLLQVI",left=5, top=239, width=441,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kpo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
