* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bgd                                                        *
*              Generazione documenti da ODL/OCL                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_762]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-04                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bgd",oParentObject)
return(i_retval)

define class tgsco_bgd as StdBatch
  * --- Local variables
  w_MVSERIAL = space(10)
  w_PDDATORD = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_APPO = space(10)
  w_ROWNUM = 0
  w_CODART = space(20)
  w_CODICE1 = space(20)
  w_DESART = space(40)
  w_DESSUP = space(0)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_INIDIS = .f.
  w_INSROW = 0
  w_CRIVAL = space(2)
  w_FLPACK = space(1)
  w_TIPCO = space(5)
  w_TIPCO1 = space(5)
  w_TIPCO2 = space(5)
  w_TIPCO3 = space(5)
  w_MVFLANAL = space(1)
  w_PRZVAC = space(1)
  w_MV_SEGNO = space(1)
  w_MPQTAEVA = 0
  w_MPQTAEV1 = 0
  w_OLFLEVAS = space(1)
  w_FLORD = space(1)
  w_FLIMP = space(1)
  w_EVADI = space(1)
  w_OLQTASAL = 0
  w_OK_COMM = .f.
  w_MVFLSCOM = space(1)
  w_CLADOC = space(2)
  w_ESEDOC = space(4)
  w_ANNDOC = space(4)
  w_MPFLEVAS = space(1)
  w_PERASS = space(3)
  w_OLQTMOV = 0
  w_CONVE = 0
  w_FLSPIN = space(1)
  w_FLSPTR = space(1)
  w_FLSPIM = space(1)
  w_ROWMAT = 0
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_OCODART = space(20)
  w_OCOMME = space(15)
  w_OLTSEODL = space(15)
  w_ULTFAS = space(1)
  w_PDFASPER = 0
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_MVIMPCOM = 0
  w_COCODVAL = space(3)
  w_MVCODCOS = space(5)
  w_CODCOS = space(5)
  w_DELTESTATA = space(1)
  w_FLCOM = space(1)
  w_MVTFLCOM = space(1)
  w_TIPGES = space(1)
  w_OLTIPSCL = space(1)
  w_MVTFLCOM = space(1)
  w_LOOP = 0
  w_DBRK = space(10)
  w_OBRK = space(10)
  w_MVTIPOPE = space(10)
  w_NUMCOR = space(25)
  w_ANTIPOPE = space(10)
  w_ANCONCON = space(1)
  w_CPROWORD = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVFLVEAC = space(1)
  w_MVEMERIC = space(1)
  w_MVCLADOC = space(2)
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MVIVAINC = space(5)
  w_MVSPEINC = 0
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVFLSALD = space(1)
  w_CPROWNUM = 0
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVSPEIMB = 0
  w_MVFLORDI = space(1)
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVTINCOM = ctod("  /  /  ")
  w_MVIVAIMB = space(5)
  w_MVFLIMPE = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVCODMAG = space(5)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVVOCCEN = space(15)
  w_MVDESART = space(40)
  w_MVCODLOT = space(20)
  w_MVCODUBI = space(20)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVCODCEN = space(15)
  w_MV__NOTE = space(10)
  w_MVDESSUP = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVCODCOM = space(15)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOR = space(1)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVSCONTI = 0
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVVALNAZ = space(3)
  w_MFLAVAL = space(1)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(10)
  w_MVCODPOR = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVCODVET = space(5)
  w_MVVALRIG = 0
  w_MVFLELGM = space(1)
  w_MVPESNET = 0
  w_MVCODSPE = space(3)
  w_MVIMPSCO = 0
  w_MVRIFDIC = space(10)
  w_MVTIPATT = space(1)
  w_MVVALMAG = 0
  w_MVPESNET = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVFLTRAS = space(1)
  w_MVCODATT = space(15)
  w_MVIMPNAZ = 0
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVAFLOM1 = space(1)
  w_MVAIMPS1 = 0
  w_MVUMSUPP = space(3)
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_MVAFLOM2 = space(1)
  w_MVAIMPS2 = 0
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVAIMPS3 = 0
  w_MVCODDES = space(5)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_CAOCON = 0
  w_VALCON = space(3)
  w_MVAFLOM4 = space(1)
  w_MVAIMPS4 = 0
  w_MVIMPARR = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVAIMPS5 = 0
  w_MVDATEVA = ctod("  /  /  ")
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVAIMPS6 = 0
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVIMPACC = 0
  w_MVDATTRA = ctod("  /  /  ")
  w_MVORATRA = space(2)
  w_MVMINTRA = space(2)
  w_MVNOTAGG = space(40)
  w_PDROWNUM = 0
  w_DCROWNUM = 0
  w_DCROWNUM1 = 0
  w_MVVOCCOS = space(15)
  w_MVRITPRE = 0
  w_CAOVAL = 0
  w_BOLARR = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_MFLCASC = space(1)
  w_IMPARR = 0
  w_BOLMIN = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_MFLORDI = space(1)
  w_RSIMPRAT = 0
  w_TOTMERCE = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_MFLIMPE = space(1)
  w_DECTOT = 0
  w_TOTALE = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_MFLRISE = space(1)
  w_BOLESE = 0
  w_TOTIMPON = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_MFLELGM = space(1)
  w_BOLSUP = 0
  w_TPNDOC = space(3)
  w_TOTIMPOS = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_MFLCOMM = space(1)
  w_BOLCAM = 0
  w_TOTFATTU = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_PERIVE = 0
  w_CAONAZ = 0
  w_CLBOLFAT = space(1)
  w_BOLIVE = space(1)
  w_RSMODPAG = space(10)
  w_RSNUMRAT = 0
  w_MVDATEVA = ctod("  /  /  ")
  w_RSDATRAT = ctod("  /  /  ")
  w_CODESE = space(4)
  w_ACCPRE = 0
  w_ORDNUM = 0
  w_CODNAZ = space(3)
  w_APPO1 = 0
  w_DETNUM = 0
  w_ACQINT = space(1)
  w_CATCOM = space(3)
  w_IVACON = space(1)
  w_FLFOBO = space(1)
  w_UNMIS1 = space(3)
  w_LIPREZZO = 0
  w_QUACON = space(1)
  w_IVALIS = space(1)
  w_UNMIS2 = space(3)
  w_CODART = space(20)
  w_CATSCC = space(5)
  w_QUALIS = space(1)
  w_UNMIS3 = space(3)
  w_CATSCM = space(5)
  w_DECUNI = 0
  w_MOLTIP = 0
  w_DATREG = ctod("  /  /  ")
  w_GRUMER = space(5)
  w_QUAN = 0
  w_MOLTI3 = 0
  w_CODVAL = space(3)
  w_ROWNUM = 0
  w_QTAUM1 = 0
  w_OPERAT = space(1)
  w_CODLIS = space(5)
  w_OPERA3 = space(1)
  w_CODCON = space(15)
  w_OK = .f.
  w_CATCLI = space(5)
  w_PZ0 = 0
  w_PZ1 = 0
  w_PZ2 = 0
  w_OK0 = .f.
  w_CATART = space(5)
  w_S10 = 0
  w_S11 = 0
  w_S12 = 0
  w_OK1 = .f.
  w_ARRSUP = 0
  w_S20 = 0
  w_S21 = 0
  w_S22 = 0
  w_OK2 = .f.
  w_SCOCON = .f.
  w_S30 = 0
  w_S31 = 0
  w_S32 = 0
  w_DT0 = ctod("  /  /  ")
  w_CODGRU = space(5)
  w_S40 = 0
  w_S41 = 0
  w_S42 = 0
  w_DT1 = ctod("  /  /  ")
  w_RN0 = 0
  w_RN1 = 0
  w_RN2 = 0
  w_PAGA = space(1)
  w_SCOLIS = space(1)
  w_DISMAG = space(1)
  w_STADOC = space(1)
  w_MVCAUCOL = space(5)
  w_MVCODMAT = space(5)
  w_MF2CASC = space(1)
  w_MF2ORDI = space(1)
  w_MF2IMPE = space(1)
  w_MF2RISE = space(1)
  w_PDSERIAL = space(10)
  w_PDTIPGEN = space(2)
  w_PDFLEVAS = space(1)
  w_DPSERIAL = space(10)
  w_SDIC = space(10)
  w_DPCODLOT = space(20)
  w_DPCODUBI = space(20)
  w_PDSERDOC = space(10)
  w_PDROWDOC = 0
  w_OLCODODL = space(15)
  w_OLCODMAG = space(5)
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OQTOR1 = 0
  w_OQTEV1 = 0
  w_OQTSAL = 0
  w_NQTSAL = 0
  w_OKEYSA = space(20)
  w_FLSERA = space(1)
  w_OSTATO = space(1)
  w_OSERORD = space(10)
  w_SERORD = space(10)
  w_ODTINI = ctod("  /  /  ")
  w_ODTFIN = ctod("  /  /  ")
  w_MVSERDDT = space(10)
  w_MVROWDDT = 0
  w_PDSERORD = space(10)
  w_NUMDDT = 0
  w_ALFDDT = space(10)
  w_DATDDT = ctod("  /  /  ")
  w_NUMORD = 0
  w_ALFORD = space(2)
  w_DATORD = ctod("  /  /  ")
  w_FLNSRI = space(1)
  w_MVCODODL = space(15)
  w_MVRIGMAT = 0
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_DTOBSO = ctod("  /  /  ")
  w_OLDMAG = space(5)
  w_FLLOTT = space(1)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_CALPRZ = 0
  w_INDIVA = 0
  w_INDIVE = 0
  w_MVIMPCOM = 0
  w_COCODVAL = space(3)
  w_DECCOM = 0
  w_OLDTEVCL = space(10)
  w_PRELAUT = space(1)
  w_OLFLORDI = space(1)
  w_OLFLIMPE = space(1)
  w_MODRIF = space(5)
  w_CODLIN = space(3)
  w_MVLOTMAG = space(5)
  w_DELETING = space(1)
  w_KEYSAL = space(20)
  w_MVFLSCOM = space(1)
  w_DATCOM = ctod("  /  /  ")
  w_DESCOD = space(41)
  w_TIPART = space(2)
  w_FLMATR = space(1)
  w_STR = space(1)
  w_SLQTAPER = 0
  w_SLQTRPER = 0
  w_DPDATREG = ctod("  /  /  ")
  w_OQTUM1 = 0
  w_OQTEV1 = 0
  w_OQTPR1 = 0
  w_OLMAGPRE = space(5)
  w_oOLQTASAL = 0
  w_QTASAL = 0
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_CICLI = .f.
  w_OLTFAODL = 0
  w_DPQTAPRO = 0
  w_DPQTAPR1 = 0
  w_DPQTASCA = 0
  w_DPQTASC1 = 0
  w_CLFASEVA = space(1)
  w_CLAVAUM1 = 0
  w_CLQTAAVA = 0
  w_CLQTASCA = 0
  w_CLSCAUM1 = 0
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_FASE = 0
  w_DPDICUM1 = 0
  w_SERDT = space(10)
  w_ROWORD = 0
  w_FLEVAS = space(1)
  w_MAGIMP = space(5)
  w_MATQTAUM1 = 0
  w_MATQTAMOV = 0
  w_RCMV = space(1)
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTALIS = 0
  w_TIPOIN = space(5)
  w_FLEVAS = space(1)
  w_FLVEAC = space(1)
  w_SPUNTA = space(1)
  w_CATDOC = space(2)
  w_CLIFOR = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_DATA1 = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_NUMINI = 0
  w_NUMFIN = 0
  w_serie1 = space(2)
  w_serie2 = space(2)
  w_STAMPA = 0
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_CODAGE = space(5)
  w_LINGUA = space(3)
  w_CATEGO = space(2)
  w_CODVET = space(5)
  w_CODZON = space(3)
  w_CODDES = space(3)
  w_CODPAG = space(5)
  w_NOSBAN = space(15)
  w_OK = .f.
  w_TIPOPE = space(1)
  w_NDIC = 0
  w_ALFADIC = space(2)
  w_IMPDIC = 0
  w_ADIC = space(4)
  w_IMPUTI = 0
  w_DDIC = ctod("  /  /  ")
  w_CODIVE = space(5)
  w_TDIC = space(1)
  w_RIFDIC = space(10)
  w_TIVA = space(1)
  w_DICODICE = space(15)
  w_DITIPCON = space(15)
  w_DIDATDOC = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_CODODL = space(15)
  w_OK_LET = .f.
  w_OLCODART = space(20)
  w_OLKEYSAL = space(40)
  w_OLMAGORI = space(5)
  w_OLMAGWIP = space(5)
  w_NETMGORI = space(1)
  w_NETMGWIP = space(1)
  w_FLIMPE = space(1)
  w_FLIMP2 = space(1)
  w_FLORD = space(1)
  w_FLORD2 = space(1)
  w_FLRISE = space(1)
  w_FLRISE2 = space(1)
  w_OLTIPSCL = space(1)
  w_OLTCOMME = space(15)
  w_FLCOM = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CON_COSC_idx=0
  CON_TRAM_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  DIC_MATR_idx=0
  DIC_MCOM_idx=0
  DIC_PROD_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  LIS_SCAG_idx=0
  LISTINI_idx=0
  MAGAZZIN_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  OUT_PUTS_idx=0
  PAG_2AME_idx=0
  PAG_AMEN_idx=0
  RIF_GODL_idx=0
  RIFMGODL_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  CAN_TIER_idx=0
  MA_COSTI_idx=0
  VOC_COST_idx=0
  MAT_PROD_idx=0
  PEG_SELI_idx=0
  AVA_LOT_idx=0
  MPS_TFOR_idx=0
  SALDICOM_idx=0
  UNIMIS_idx=0
  ODL_CICL_idx=0
  ODL_RISF_idx=0
  ODL_RISO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documenti da Produzione / Conto Lavoro
    * --- Generazione Ordini a Fornitori (Terzisti) (da GSCL_BGL) ; w_OPERAZ = 'OR'
    * --- Generazione DDT di Trasferimento (da GSCL_BGT) ; w_OPERAZ = 'DT'
    * --- Generazione Buoni di Prelievo (da GSCO_BGI) ; w_OPERAZ = 'BP'
    * --- Generazione Carichi da Dichiarazione di Produzione (da GSCO_BDP) ; w_OPERAZ = 'CA'
    * --- Generazione Scarichi da Dichiarazione di Produzione (da GSCO_BDP) ; w_OPERAZ = 'SC'
    * --- Generazione Risorse da Dichiarazione di Produzione (da GSCO_BDP) ; w_OPERAZ = 'RI'
    * --- Generazione Materiali di output da Dichiarazione di Produzione (da GSCO_BDP) ; w_OPERAZ = 'MO'
    * --- Generazione Scarichi Materiali da DDT di Acquisto C/Lavoro (da GSCL_BRL) ; w_OPERAZ = 'SM'
    * --- Switch codice lungo / corto da DDT di Acquisto C/Lavoro (da GSCL_BRL) ; w_OPERAZ = 'CS'
    this.w_MVSERIAL = this.oParentObject.w_MVSERIAL
    * --- spese incasso/trasporto/Imballo
    this.w_MVDATTRA = cp_CharToDate("  -  -  ")
    this.w_MVORATRA = "  "
    this.w_MVMINTRA = "  "
    this.w_MVNOTAGG = " "
    this.w_CRIVAL = "  "
    this.w_PDSERIAL = SPACE(10)
    this.w_MVSERDDT = SPACE(10)
    this.w_MVROWDDT = 0
    this.w_NUMDDT = 0
    this.w_ALFDDT = Space(10)
    this.w_DATDDT = cp_CharToDate("  -  -  ")
    this.w_NUMORD = 0
    this.w_ALFORD = "  "
    this.w_DATORD = cp_CharToDate("  -  -  ")
    this.w_FLPACK = " "
    this.w_TIPCO = SPACE(5)
    this.w_MVNUMRIF = -20 
    this.w_NUMINI = 0
    this.w_NUMFIN = 0
    * --- w_MV ... Lasciare Locali
    WITH this.oParentObject
    this.w_MVDATDOC = .w_MVDATDOC
    this.w_MVDATDIV = .w_MVDATDIV
    this.w_MVFLVEAC = .w_MVFLVEAC
    this.w_MVEMERIC = .w_MVEMERIC
    if this.w_MVEMERIC="V"
      this.w_MVDATDOC = this.oParentObject.w_MVDATREG
    else
      this.w_MVDATDOC = .w_MVDATDOC
    endif
    this.w_MVCLADOC = .w_MVCLADOC
    this.w_CLADOC = NVL(this.w_MVCLADOC,"")
    this.w_FLNSRI = " "
    this.w_TPNDOC = "   "
    this.w_DPCODLOT = SPACE(20)
    this.w_DPCODUBI = SPACE(20)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TD_SEGNO,TDFLCOMM,TDFLSPIN,TDFLSPTR,TDFLSPIM"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TD_SEGNO,TDFLCOMM,TDFLSPIN,TDFLSPTR,TDFLSPIM;
        from (i_cTable) where;
            TDTIPDOC = this.oParentObject.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
      this.w_FLCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
      this.w_FLSPIN = NVL(cp_ToDate(_read_.TDFLSPIN),cp_NullValue(_read_.TDFLSPIN))
      this.w_FLSPTR = NVL(cp_ToDate(_read_.TDFLSPTR),cp_NullValue(_read_.TDFLSPTR))
      this.w_FLSPIM = NVL(cp_ToDate(_read_.TDFLSPIM),cp_NullValue(_read_.TDFLSPIM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVTFLCOM = IIF(g_COMM="S" AND this.oParentObject.w_OPERAZ $ "CA-SC-OR-SM" , this.w_FLCOM , SPACE(1))
    this.w_DELTESTATA = "S"
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    if this.oParentObject.w_OPERAZ $ "OR-DT-BP"
      * --- Devo recuperare la provenienza per sapere che movimento inserisco in archivio
      *     Ho sia gli ordini a fornitore da ODA che OCL
      *     Per uniformit� di gestione lo faccio anche per i buoni di prelievo e piano di spedizione a terzista
      if type("this.oparentobject.oParentobject.w_TIPGES") <> "U"
        this.w_TIPGES = .oParentObject.w_TIPGES
      endif
      if type("this.oparentobject.oParentobject.w_OLTIPSCL") <> "U"
        this.w_OLTIPSCL = .oParentObject.w_OLTIPSCL
      endif
    endif
    if Empty(this.w_TIPGES)
      this.w_TIPGES = "I"
    endif
    do case
      case this.oParentObject.w_OPERAZ $ "DT-BP"
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLPACK,TDFLELAN"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLPACK,TDFLELAN;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLPACK = NVL(cp_ToDate(_read_.TDFLPACK),cp_NullValue(_read_.TDFLPACK))
          this.w_MVFLANAL = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVDATTRA = .oParentObject.w_PPDATTRA
        this.w_MVORATRA = .oParentObject.w_PPORATRA
        this.w_MVMINTRA = .oParentObject.w_PPMINTRA
        this.w_MVNOTAGG = .oParentObject.w_PPNOTAGG
        this.w_STADOC = .oParentObject.w_STADOC
        if this.oParentObject.w_OPERAZ="DT" AND NOT EMPTY(g_ARTDES)
          * --- Eventuale Stampa riga Descrittiva di Riferimento Ordine
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLNSRI,TDTPNDOC,TDMODRIF"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLNSRI,TDTPNDOC,TDMODRIF;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLNSRI = NVL(cp_ToDate(_read_.TDFLNSRI),cp_NullValue(_read_.TDFLNSRI))
            this.w_TPNDOC = NVL(cp_ToDate(_read_.TDTPNDOC),cp_NullValue(_read_.TDTPNDOC))
            this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case this.oParentObject.w_OPERAZ $ "CA-SC-RI-MO"
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLELAN"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLELAN;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVFLANAL = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if type("this.oparentobject.oParentobject.w_CRIVAL") <> "U"
          this.w_CRIVAL = .oParentObject.w_CRIVAL
        endif
        if type("this.oparentobject.oParentobject.w_STADOC") <> "U"
          this.w_STADOC = .oParentObject.w_STADOC
        endif
        if this.oParentObject.w_OPERAZ="CA"
          this.w_DPCODLOT = .oParentObject.w_DPCODLOT
          this.w_DPCODUBI = .oParentObject.w_DPCODUBI
          if g_COMM="S"
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDFLCOMM"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDFLCOMM;
                from (i_cTable) where;
                    TDTIPDOC = this.oParentObject.w_MVTIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVTFLCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_MVTFLCOM = SPACE(1)
          endif
        endif
      case this.oParentObject.w_OPERAZ $ "SM-CS"
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLELAN,TDFLCOMM"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLELAN,TDFLCOMM;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVFLANAL = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
          this.w_MVTFLCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVSERDDT = .oParentObject.w_MVSERIAL
        this.w_MVROWDDT = .w_ROWORD
        this.w_CRIVAL = .w_CRIVAL
        this.w_STADOC = " "
        if NOT EMPTY(this.w_MVSERDDT)
          * --- Legge riferimenti Doc. da Stampare 
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVNUMDOC,MVALFDOC,MVDATDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERDDT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVNUMDOC,MVALFDOC,MVDATDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERDDT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMDDT = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDDT = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_DATDDT = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if type("this.oparentobject.w_DELTESTATA") <> "U"
          this.w_DELTESTATA = this.oParentObject.w_DELTESTATA
        endif
        Create cursor DOCGENE (SERDOC C(10), ROWMAT N(6,0), ROWDOC N(6,0))
      otherwise
        this.w_STADOC = .oParentObject.w_STADOC
        this.w_PRZVAC = .oParentObject.w_PRZVAC
    endcase
    if type("this.oparentobject.oParentobject.w_STADOC") <> "U"
      this.w_STADOC = .oParentObject.w_STADOC
    endif
    ENDWITH
    this.w_MVVALNAZ = g_PERVAL
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_CODESE = this.oParentObject.w_MVCODESE
    this.w_FLFOBO = " "
    this.w_ACQINT = "N"
    * --- Flag generazione Distinta documenti
    this.w_INIDIS = .F.
    * --- Rate Scadenze
    DIMENSION DR[1000, 9]
    this.w_LOOP = 1
    do while this.w_LOOP <= alen(DR,1)
      DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ")
      DR[ this.w_LOOP , 2] = 0
      DR[ this.w_LOOP , 3] = "  "
      DR[ this.w_LOOP , 4] = "  "
      DR[ this.w_LOOP , 5] = "  "
      DR[ this.w_LOOP , 6] = "  "
      DR[ this.w_LOOP , 7] = " "
      DR[ this.w_LOOP , 8] = " "
      DR[ this.w_LOOP , 9] = " "
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Legge Informazioni di Riga di Default
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MFLCASC = " "
    this.w_MFLORDI = " "
    this.w_MFLIMPE = " "
    this.w_MFLRISE = " "
    this.w_MFLELGM = " "
    this.w_MFLAVAL = " "
    this.w_MVCAUCOL = SPACE(5)
    this.w_MVCODMAT = SPACE(5)
    this.w_MF2CASC = " "
    this.w_MF2ORDI = " "
    this.w_MF2IMPE = " "
    this.w_MF2RISE = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMCAUCOL"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMCAUCOL;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MVTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.w_MFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      this.w_MVCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MFLCASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLCASC)
    this.w_MFLORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLORDI)
    this.w_MFLIMPE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLIMPE)
    this.w_MFLRISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLRISE)
    if NOT EMPTY(this.w_MVCAUCOL)
      * --- Caso DDT di Trasferimento
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_MVCAUCOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
          from (i_cTable) where;
              CMCODICE = this.w_MVCAUCOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MF2CASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2CASC)
      this.w_MF2ORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2ORDI)
      this.w_MF2IMPE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2IMPE)
      this.w_MF2RISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2RISE)
    endif
    if USED("GENEORDI")
      * --- Inizio Aggiornamento
      if RECCOUNT("GENEORDI") > 0
        ah_Msg("Inizio fase di generazione...",.T.)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Chiude i Cursori lasciati Aperti
      use in GENEORDI
    endif
    if this.oParentObject.w_nRecDoc>0
      * --- Lancia il Report (Tranne se Dichiarazioni di Produzione gestite nel batch precedente)
      do case
        case this.oParentObject.w_OPERAZ="CA"
          if NOT EMPTY(this.w_MVSERIAL)
            this.oParentObject.w_DOCCAR = this.w_MVSERIAL
          endif
        case this.oParentObject.w_OPERAZ$"SC-SM-CS"
          if NOT EMPTY(this.w_MVSERIAL)
            this.oParentObject.w_DOCSCA = this.w_MVSERIAL
          endif
        case this.oParentObject.w_OPERAZ="RI"
          if NOT EMPTY(this.w_MVSERIAL)
            this.oParentObject.w_DOCSER = this.w_MVSERIAL
          endif
        case this.oParentObject.w_OPERAZ="MO"
          if NOT EMPTY(this.w_MVSERIAL)
            this.oParentObject.w_DOCMOU = this.w_MVSERIAL
          endif
        otherwise
          if this.w_INIDIS=.T. AND NOT EMPTY(this.w_PDSERIAL) AND this.w_STADOC="S"
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    endif
    * --- Elimina Cursore di Appoggio se esiste ancora
    use in select("GeneApp")
    * --- Elimina Cursore di Appoggio se esiste ancora
    use in select("PAGAM")
    * --- Elimina Cursore di Appoggio se esiste ancora
    use in select("__TMP__")
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti da ODL/OCL
    * --- Testa il Cambio di Fornitore
    this.w_OLDMAG = "#####"
    this.w_FLUBIC = " "
    this.w_DBRK = "##zz##"
    * --- Creo Cursore Errori Per Lotti/Ubicazioni/Matricole
    CREATE CURSOR MATRERRO ( SERDOC C(10), NUMDOC N(15,0), DATDOC D(8), ROWNUM N(4,0), CODART C(20), DESART C(40),MAGAZZ C(5), TIPO C(1))
    SELECT GENEORDI
    GO TOP
    SCAN FOR NOT EMPTY(NVL(PDSERIAL," ")) OR (NOT EMPTY(NVL(PDCODICE," ")) AND this.oParentObject.w_OPERAZ$"SC-SM-CS")
    * --- Testa Cambio Documento
    if this.oParentObject.w_OPERAZ="SM"
      this.w_ROWMAT = NVL(CPROWNUM,0)
    endif
    this.w_OBRK = NVL(PDORDINE, SPACE(15))
    if this.w_DBRK<>this.w_OBRK
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT GENEORDI
      * --- Inizializza i dati di Testata del Nuovo Documento
      this.w_PDDATORD = CP_TODATE(PDDATORD)
      this.w_PRELAUT = SPACE(1)
      this.w_MVCODCON = PDCODCON
      this.w_MVCODIVE = NVL(CODIVE, SPACE(5))
      this.w_MVCODPAG = NVL(CODPAG, SPACE(5))
      this.w_MVCODBAN = NVL(CODBAN, SPACE(10))
      this.w_MVCODBA2 = ANCODBA2
      this.w_MVCODVAL = NVL(CODVAL, SPACE(5))
      this.w_MVSCOCL1 = NVL(SCOCL1,0)
      this.w_MVSCOCL2 = NVL(SCOCL2,0)
      this.w_MVSCOPAG = NVL(SCOPAG,0)
      this.w_MVFLSCOR = NVL(FLSCOR, "N")
      this.w_MVTCOLIS = NVL(MVTCOLIS, SPACE(5))
      this.w_MVTCONTR = SPACE(15)
      this.w_MVCODVAL = IIF(EMPTY(this.w_MVCODVAL), this.w_MVVALNAZ, this.w_MVCODVAL)
      this.w_MVCAOVAL = this.w_CAONAZ
      this.oParentObject.w_MVDATCIV = this.w_MVDATDOC
      this.oParentObject.w_MVDATREG = this.w_MVDATDOC
      if this.w_MVCODVAL<>this.w_MVVALNAZ
        this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 0)
      endif
      this.w_MVCAOVAL = IIF(this.w_MVCAOVAL=0, g_CAOVAL, this.w_MVCAOVAL)
      * --- Legge Dati Associati alla Valuta
      this.w_DECTOT = 0
      this.w_DECUNI = 0
      this.w_BOLESE = 0
      this.w_BOLSUP = 0
      this.w_BOLCAM = 0
      this.w_BOLARR = 0
      this.w_BOLMIN = 0
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
          from (i_cTable) where;
              VACODVAL = this.w_MVCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
        this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
        this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
        this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
        this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
        this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT GENEORDI
      * --- calcola le Spese di Incasso
      this.w_MVSPEINC = 0
      this.w_MVSPEINC = CALSPEINC( this.w_MVFLVEAC, this.w_MVCODPAG, this.w_MVCODVAL, this.oParentObject.w_MVTIPCON, this.w_MVCODCON, this.w_FLSPIN )
      this.w_MVSPEIMB = 0
      this.w_MVSPETRA = 0
      this.w_MVSPEBOL = 0
      this.w_MVIMPARR = 0
      this.w_MVACCPRE = 0
      this.w_MVIVAINC = SPACE(5)
      this.w_MVIVAIMB = SPACE(5)
      this.w_MVIVATRA = SPACE(5)
      this.w_MVIVABOL = SPACE(5)
      this.w_MVIVABOL = iif(empty(this.w_MVIVABOL),IIF(this.w_MVFLVEAC="V", g_COIBOL, g_COABOL),this.w_MVIVABOL)
      this.w_MVCODDES = SPACE(5)
      this.w_MVCODVET = SPACE(5)
      this.w_MVCODSPE = SPACE(3)
      this.w_MVCODPOR = " "
      this.w_GIORN1 = NVL(ANGIOSC1, 0)
      this.w_GIORN2 = NVL(ANGIOSC2, 0)
      this.w_MESE1 = NVL(AN1MESCL, 0)
      this.w_MESE2 = NVL(AN2MESCL, 0)
      this.w_GIOFIS = NVL(ANGIOFIS, 0)
      this.w_CLBOLFAT = NVL(ANBOLFAT, " ")
      this.w_CODNAZ = NVL(ANNAZION, g_CODNAZ)
      this.w_CATSCC = NVL(ANCATSCM, SPACE(5))
      this.w_CAOCON = this.w_MVCAOVAL
      this.w_VALCON = this.w_MVCODVAL
      this.w_BOLINC = " "
      this.w_BOLIMB = " "
      this.w_BOLTRA = " "
      this.w_BOLBOL = " "
      this.w_BOLIVE = " "
      this.w_PEIINC = 0
      this.w_PEIIMB = 0
      this.w_PEITRA = 0
      this.w_PERIVE = 0
      this.w_IVACON = " "
      this.w_IVALIS = " "
      this.w_QUACON = " "
      this.w_QUALIS = " "
      if NOT EMPTY(this.w_MVTCOLIS)
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSIVALIS,LSQUANTI"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.w_MVTCOLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSIVALIS,LSQUANTI;
            from (i_cTable) where;
                LSCODLIS = this.w_MVTCOLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
          this.w_QUALIS = NVL(cp_ToDate(_read_.LSQUANTI),cp_NullValue(_read_.LSQUANTI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT GENEORDI
      endif
      * --- Propone dati Accompagnatori (Riferimento: 'CO' = Consegna)
      * --- Controlla l'obsolescenza dei dati accompagnatori
      this.w_MVTIPOPE = Space(10)
      * --- Select from DES_DIVE
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
            +" where DDTIPCON="+cp_ToStrODBC(this.oParentObject.w_MVTIPCON)+" AND DDCODICE="+cp_ToStrODBC(this.w_MVCODCON)+" AND DDTIPRIF='CO' AND DDPREDEF='S'";
             ,"_Curs_DES_DIVE")
      else
        select * from (i_cTable);
         where DDTIPCON=this.oParentObject.w_MVTIPCON AND DDCODICE=this.w_MVCODCON AND DDTIPRIF="CO" AND DDPREDEF="S";
          into cursor _Curs_DES_DIVE
      endif
      if used('_Curs_DES_DIVE')
        select _Curs_DES_DIVE
        locate for 1=1
        do while not(eof())
        this.w_DTOBSO = NVL(CP_TODATE(_Curs_DES_DIVE.DDDTOBSO),cp_CharToDate("  -  -  "))
        if (this.w_DTOBSO>this.w_MVDATDOC OR EMPTY(this.w_DTOBSO)) 
          this.w_MVCODDES = NVL(_Curs_DES_DIVE.DDCODDES, SPACE(5))
          this.w_MVCODVET = NVL(_Curs_DES_DIVE.DDCODVET, SPACE(5))
          this.w_MVCODPOR = NVL(_Curs_DES_DIVE.DDCODPOR," ")
          this.w_MVCODSPE = NVL(_Curs_DES_DIVE.DDCODSPE,SPACE(3))
          this.w_MVTIPOPE = Nvl( _Curs_DES_DIVE.DDTIPOPE, Space(10) )
        endif
          select _Curs_DES_DIVE
          continue
        enddo
        use
      endif
      if this.oParentObject.w_OPERAZ="OR"
        * --- Dichiarazione di intento
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if NOT EMPTY(this.w_MVCODIVE)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVBOLIVA,IVPERIVA,IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVBOLIVA,IVPERIVA,IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_MVCODIVE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT GENEORDI
      endif
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANNUMCOR,AFFLINTR,ANCATCOM,ANCODLIN,ANTIPOPE,ANCONCON"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANNUMCOR,AFFLINTR,ANCATCOM,ANCODLIN,ANTIPOPE,ANCONCON;
          from (i_cTable) where;
              ANTIPCON = this.oParentObject.w_MVTIPCON;
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
        this.w_ACQINT = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
        this.w_CATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
        this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
        this.w_ANTIPOPE = NVL(cp_ToDate(_read_.ANTIPOPE),cp_NullValue(_read_.ANTIPOPE))
        this.w_ANCONCON = NVL(cp_ToDate(_read_.ANCONCON),cp_NullValue(_read_.ANCONCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Empty(this.w_MVCODBAN)
        * --- Il numero di conto corrente devo riportarlo solo se c'� la banca 
        this.w_NUMCOR = Space(25)
      endif
      SELECT GENEORDI
      this.w_DBRK = NVL(PDORDINE, SPACE(15))
    endif
    SELECT GENEORDI
    * --- Scrive nuova Riga sul Temporaneo di Appoggio
    this.w_MVTIPRIG = "R"
    this.w_MVCODICE = NVL(PDCODICE, SPACE(20))
    this.w_MVCODART = NVL(PDCODART, SPACE(20))
    this.w_MVDESART = NVL(CADESART, SPACE(40))
    this.w_MVDESSUP = NVL(CADESSUP, SPACE(10))
    this.w_MVUNIMIS = NVL(PDUMORDI, SPACE(3))
    this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
    this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
    this.w_CATSCM = NVL(ARCATSCM, SPACE(5))
    this.w_GRUMER = NVL(ARGRUMER, SPACE(5))
    this.w_MVCODLIS = this.w_MVTCOLIS
    this.w_MVQTAMOV = NVL(PDQTAORD, 0)
    this.w_MVQTAUM1 = NVL(PDQTAUM1, 0)
    this.w_MVDATEVA = CP_TODATE(PDDATEVA)
    this.w_MVPREZZO = 0
    this.w_MVSCONT1 = 0
    this.w_MVSCONT2 = 0
    this.w_MVSCONT3 = 0
    this.w_MVSCONT4 = 0
    this.w_MVFLOMAG = "X"
    this.w_MVCODIVA = CALCODIV(this.w_MVCODART, this.oParentObject.w_MVTIPCON, this.w_MVCODCON , this.w_MVTIPOPE, IIF( this.w_MVFLVEAC="V", this.oParentObject.w_MVDATREG, this.w_MVDATDOC), this.w_ANTIPOPE)
    * --- Rileggo i dati realtivi all'IVA..
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIND,IVPERIVA,IVBOLIVA"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIND,IVPERIVA,IVBOLIVA;
        from (i_cTable) where;
            IVCODIVA = this.w_MVCODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
      this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura della Percentuale di indetraibilit�
    SELECT GENEORDI
    this.w_MVVALRIG = 0
    this.w_MVIMPACC = 0
    this.w_MVIMPSCO = 0
    this.w_MVVALMAG = 0
    this.w_MVIMPNAZ = 0
    this.w_LIPREZZO = 0
    this.w_MVPREZZO = 0
    this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
    this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
    this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
    this.w_MOLTIP = NVL(ARMOLTIP, 0)
    this.w_MOLTI3 = NVL(CAMOLTIP, 0)
    this.w_OPERAT = NVL(AROPERAT, " ")
    this.w_OPERA3 = NVL(CAOPERAT, " ")
    this.w_FLSERA = NVL(ARTIPSER,SPACE(1))
    this.w_MVPESNET = NVL(ARPESNET, 0)
    this.w_MVNOMENC = NVL(ARNOMENC, SPACE(8))
    this.w_MVFLTRAS = IIF(NVL(ARDATINT,"F")="S","Z",IIF( NVL(ARDATINT,"F")="F"," ",IIF( NVL(ARDATINT,"F")="I","I","S")))
    this.w_MVUMSUPP = NVL(ARUMSUPP, SPACE(3))
    this.w_MVMOLSUP = NVL(ARMOLSUP, 0)
    this.w_MVCODMAG = NVL(PDCODMAG, SPACE(5))
    this.w_OLCODMAG = NVL(PDCODMAG, SPACE(5))
    this.w_MVCODMAT = SPACE(5)
    this.w_DISMAG = " "
    this.w_FLLOTT = NVL(ARFLLOTT, " ")
    this.w_F2UBIC = " "
    this.w_OLQTAMOV = 0
    this.w_OLQTAUM1 = 0
    this.w_PDFLEVAS = " "
    this.w_OLCODODL = PDSERIAL
    this.w_PDROWNUM = NVL(PDROWNUM, 0)
    if this.oParentObject.w_OPERAZ = "SC"
      this.w_DCROWNUM = NVL(DCROWNUM, 0)
    endif
    this.w_PDSERORD = SPACE(10)
    do case
      case this.oParentObject.w_OPERAZ $ "DT-BP"
        this.w_MVCODMAG = NVL(PDMAGPRE, SPACE(5))
        this.w_MVCODMAT = NVL(PDMAGWIP, SPACE(5))
        * --- Magazino Nettificabile
        this.w_DISMAG = NVL(DISMAG, " ")
        this.w_F2UBIC = NVL(FLUBI2, " ")
        * --- Qta da stornare nei saldi Impegnato (Tutta)
        this.w_OLQTAUM1 = NVL(PDQTAMO1, 0)
        this.w_PDFLEVAS = NVL(PDFLEVAS, " ")
        if this.oParentObject.w_OPERAZ="DT" AND this.w_FLNSRI="S" AND NOT EMPTY(g_ARTDES)
          this.w_PDSERORD = NVL(PDSERDOC, SPACE(10))
        endif
        if this.oParentObject.w_OPERAZ = "BP"
          * --- Modalit� di prelievo
          this.w_PRELAUT = NVL(PDTIPPRE, "N")
        else
          this.w_PRELAUT = "M"
        endif
      case this.oParentObject.w_OPERAZ = "CA"
        this.w_OLCODMAG = NVL(PDMAGPRE, SPACE(5))
        * --- Magazino Nettificabile
        this.w_DPSERIAL = NVL(PDORDINE, SPACE(10))
        this.w_DISMAG = NVL(DISMAG, " ")
        * --- Qta da stornare nei saldi Impegnato (Tutta)
        this.w_OLQTAMOV = NVL(PDQTAMOV, 0)
        this.w_OLQTAUM1 = NVL(PDQTAMO1, 0)
        this.w_PDFLEVAS = NVL(PDFLEVAS, " ")
      case this.oParentObject.w_OPERAZ = "SC"
        * --- Magazino Nettificabile
        this.w_DPSERIAL = NVL(PDORDINE, SPACE(10))
        this.w_DISMAG = NVL(DISMAG, " ")
        * --- Qta da stornare nei saldi Impegnato (Tutta)
        this.w_OLQTAMOV = NVL(PDQTAMOV, 0)
        this.w_OLQTAUM1 = NVL(PDQTAMO1, 0)
        this.w_PDFLEVAS = NVL(PDFLEVAS, " ")
      case this.oParentObject.w_OPERAZ $ "SM-CS"
        if this.oParentObject.w_OPERAZ<>"CS" and not empty(this.w_MVCODMAG)
          this.w_MVCODMAG = NVL(PDMAGWIP, SPACE(5))
          this.w_MVCODMAT = SPACE(5)
        endif
        this.w_DISMAG = NVL(DISMAG, " ")
        * --- Qta da Evadere sul Dettaglio OCL
        this.w_OLQTAMOV = NVL(PDQTAMOV, 0)
        this.w_OLQTAUM1 = NVL(PDQTAMO1, 0)
        this.w_PDFLEVAS = NVL(PDFLEVAS, " ")
        this.w_ROWMAT = NVL(CPROWNUM,0)
      case this.oParentObject.w_OPERAZ $ "RI-MO"
        * --- Magazino Nettificabile
        this.w_DPSERIAL = NVL(PDORDINE, SPACE(10))
        this.w_DISMAG = NVL(DISMAG, " ")
        this.w_PDFLEVAS = NVL(PDFLEVAS, " ")
        if this.oParentObject.w_OPERAZ = "RI"
          * --- Riferimento fase
          this.w_PDFASPER = NVL(RFRIFFAS , 0)
          this.w_OLQTAUM1 = NVL(PDQTAMO1, 0)
        endif
    endcase
    * --- Qui calcolare il Prezzo
    if this.oParentObject.w_OPERAZ ="OR"
      * --- Cerco il contratto sull'ODL altrimenti non lo applico
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTCONTR"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTCONTR;
          from (i_cTable) where;
              OLCODODL = this.w_OLCODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTCONTR = NVL(cp_ToDate(_read_.OLTCONTR),cp_NullValue(_read_.OLTCONTR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVCONTRA = this.w_MVTCONTR
    endif
    if this.w_MVTIPRIG="R" AND this.w_MVQTAMOV>0 AND (this.oParentObject.w_OPERAZ $ "CA-SC-SM-RI-CS" OR NOT EMPTY(this.w_MVCODLIS) OR NOT EMPTY(this.w_MVCONTRA))
      * --- Calcola Prezzo Da Listini/Contratti oppure dai Criteri di Valorizzazione in caso di Doc.Carico/Scarico
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT GENEORDI
    endif
    if this.w_MVQTAMOV<>0
      this.w_APPO = this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.w_MVUNIMIS+this.w_OPERAT+this.w_OPERA3+this.w_IVALIS+"P"+ALLTRIM(STR(this.w_DECUNI))
      if EMPTY(this.w_CLUNIMIS)
        this.w_MVPREZZO = CALMMLIS(this.w_LIPREZZO,this.w_APPO,this.w_MOLTIP,this.w_MOLTI3, IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA))
      else
        this.w_MVPREZZO = CALMMLIS(this.w_LIPREZZO,this.w_APPO,1,1, IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA))
      endif
      this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
      this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR,this.w_MVVALRIG, this.w_MVIMPSCO, this.w_MVIMPACC, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
      this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, g_CAOVAL, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
    endif
    this.w_MVCODCEN = NVL(PDCODCEN, SPACE(15))
    this.w_MVVOCCOS = NVL(PDVOCCOS, SPACE(15))
    this.w_MVCODCOM = NVL(PDCODCOM, SPACE(15))
    if this.oParentObject.w_OPERAZ = "SM"
      * --- Sulla query sono vuoti, li prendo dal padre
      this.w_MVCODCEN = NVL(this.oparentobject.oParentobject.w_MVCODCEN, SPACE(15))
      if empty(this.w_MVCODCEN)
        * --- Se � vuota la voce di costo della dichiarazione allora leggo quella
        *     dell'articolo
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODCEN"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODCEN;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVCODCEN = NVL(cp_ToDate(_read_.ARCODCEN),cp_NullValue(_read_.ARCODCEN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if .f.
        * --- Per coerenza con le dichiarazioni di produzione la voce di costo la leggo sempre dall'articolo
        this.w_MVVOCCOS = NVL(this.oparentobject.oParentobject.w_MVVOCCEN, SPACE(15))
      endif
      this.w_MVCODCOM = NVL(this.oparentobject.oParentobject.w_MVCODCOM, SPACE(15))
    endif
    if Not Empty(this.w_MVCODCOM)
      * --- Leggo valuta della Commessa e suoi decimali
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNCODVAL"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNCODVAL;
          from (i_cTable) where;
              CNCODCAN = this.w_MVCODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_COCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.w_MVCODCOM),"S", " "), this.w_MVCODVAL, this.w_COCODVAL, this.w_MVIMPNAZ, this.w_MVCAOVAL, this.w_MVDATDOC, this.w_DECCOM )
    endif
    this.w_MVTIPATT = NVL(PDTIPATT,"A")
    this.w_MVCODATT = NVL(PDCODATT, SPACE(15))
    if this.oParentObject.w_OPERAZ = "SM"
      * --- Sulla query sono vuoti, li prendo dal padre
      this.w_MVTIPATT = NVL(this.oparentobject.oParentobject.w_MVTIPATT,"A")
      this.w_MVCODATT = NVL(this.oparentobject.oParentobject.w_MVCODATT, SPACE(15))
    endif
    if g_PERUBI<>"S" OR EMPTY(this.w_MVCODMAG)
      this.w_FLUBIC = " "
    else
      if this.w_OLDMAG<>this.w_MVCODMAG
        * --- Legge Flag Ubicazione (Solo al cambio magazzino)
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGFLUBIC"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGFLUBIC;
            from (i_cTable) where;
                MGCODMAG = this.w_MVCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    this.w_OLDMAG = this.w_MVCODMAG
    if this.oParentObject.w_OPERAZ $ "CA-OR-SC-SM"
      if g_COMM="S"
        this.w_OK_COMM = g_COMM="S" And this.w_MFLCOMM<>"N" And not empty(this.w_MVCODCOM) and not empty(this.w_MVCODATT)
      endif
      if nvl(this.w_MVTFLCOM, " ") = "S" or nvl(this.w_MVFLANAL, " ") = "S"
        if empty(this.w_MVVOCCOS) and (this.oParentObject.w_OPERAZ="SM" or this.w_OK_COMM)
          * --- Se � vuota la voce di costo della dichiarazione allora leggo quella
          *     dell'articolo
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARVOCCEN"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARVOCCEN;
              from (i_cTable) where;
                  ARCODART = this.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVVOCCOS = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      * --- Costi di Commessa Commessa
      if nvl(this.w_MVTFLCOM, " ") = "S"
        if this.w_OK_COMM
          this.w_MVFLORCO = IIF(g_COMM="S" AND this.w_MVFLVEAC$ "AV" ,iif(this.w_MFLCOMM="I","+",IIF(this.w_MFLCOMM="D","-"," "))," ")
          this.w_MVFLCOCO = IIF(g_COMM="S" AND this.w_MVFLVEAC $ "AV" ,iif(this.w_MFLCOMM="C","+",IIF(this.w_MFLCOMM="S","-"," "))," ")
          this.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.w_MVCODCOM),"S", " "), this.w_MVCODVAL, this.w_COCODVAL, this.w_MVIMPNAZ, this.w_MVCAOVAL, this.w_MVDATDOC, this.w_DECCOM )
          this.w_CODCOS = SPACE(5)
          this.w_MVCODCOS = this.w_CODCOS
        endif
        if this.w_OK_COMM
          if NOT EMPTY(this.w_MVVOCCOS)
            * --- Read from VOC_COST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOC_COST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VCTIPCOS"+;
                " from "+i_cTable+" VOC_COST where ";
                    +"VCCODICE = "+cp_ToStrODBC(this.w_MVVOCCOS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VCTIPCOS;
                from (i_cTable) where;
                    VCCODICE = this.w_MVVOCCOS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCOS = NVL(cp_ToDate(_read_.VCTIPCOS),cp_NullValue(_read_.VCTIPCOS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MVCODCOS = this.w_CODCOS
          endif
        endif
      endif
    endif
    * --- Totalizzatori
    this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X", this.w_MVVALRIG, 0)
    do case
      case this.oParentObject.w_OPERAZ="SC"
        INSERT INTO GeneApp ;
        (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
        t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
        t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
        t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
        t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
        t_MVFLORCO, t_MVFLCOCO, t_MVCODCOS, ;
        t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ;
        t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
        t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
        t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ;
        t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, ;
        t_OLCODODL, t_PDROWNUM, t_DCROWNUM, t_OLCODMAG, t_MVCODMAT, t_DISMAG, t_OLQTAMOV, t_OLQTAUM1, ;
        t_PDFLEVAS, t_FLSERA, t_FLLOTT, t_FLUBIC, t_FLUBI2, t_PDSERORD,t_MVIMPCOM, t_MVIMPAC2, t_MVFLTRAS) ;
        VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
        this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
        this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
        this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
        this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
        this.w_MVFLORCO, this.w_MVFLCOCO, this.w_MVCODCOS, ;
        this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ;
        this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
        this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
        this.w_MVCODMAG, this.w_MVCODCEN, this.w_MVVOCCOS, ;
        this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_MVDATEVA, ;
        this.w_OLCODODL, this.w_PDROWNUM, this.w_DCROWNUM, this.w_OLCODMAG, this.w_MVCODMAT, this.w_DISMAG, this.w_OLQTAMOV, this.w_OLQTAUM1, ;
        this.w_PDFLEVAS, this.w_FLSERA, this.w_FLLOTT, this.w_FLUBIC, this.w_F2UBIC, this.w_PDSERORD,this.w_MVIMPCOM, 0,this.w_MVFLTRAS)
      case this.oParentObject.w_OPERAZ$"SM-CS"
        INSERT INTO GeneApp ;
        (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
        t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
        t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
        t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
        t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
        t_MVFLORCO, t_MVFLCOCO, t_MVCODCOS, ;
        t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ;
        t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
        t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
        t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ;
        t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, ;
        t_OLCODODL, t_PDROWNUM, t_OLCODMAG, t_MVCODMAT, t_DISMAG, t_OLQTAMOV, t_OLQTAUM1, ;
        t_PDFLEVAS, t_OLMODPRE, t_FLSERA, t_FLLOTT, t_FLUBIC, t_FLUBI2, t_PDSERORD, t_MVIMPCOM, t_MVIMPAC2, t_ROWMAT, t_MVFLTRAS) ;
        VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
        this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
        this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
        this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
        this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
        this.w_MVFLORCO, this.w_MVFLCOCO, this.w_MVCODCOS, ;
        this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ;
        this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
        this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
        this.w_MVCODMAG, this.w_MVCODCEN, this.w_MVVOCCOS, ;
        this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_MVDATEVA, ;
        this.w_OLCODODL, this.w_PDROWNUM, this.w_OLCODMAG, this.w_MVCODMAT, this.w_DISMAG, this.w_OLQTAMOV, this.w_OLQTAUM1, ;
        this.w_PDFLEVAS, this.w_PRELAUT, this.w_FLSERA, this.w_FLLOTT, this.w_FLUBIC, this.w_F2UBIC, this.w_PDSERORD, this.w_MVIMPCOM, 0, this.w_ROWMAT, this.w_MVFLTRAS)
      otherwise
        INSERT INTO GeneApp ;
        (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
        t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
        t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
        t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
        t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
        t_MVFLORCO, t_MVFLCOCO, t_MVCODCOS, ;
        t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ;
        t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
        t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
        t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ;
        t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, ;
        t_OLCODODL, t_PDROWNUM, t_OLCODMAG, t_MVCODMAT, t_DISMAG, t_OLQTAMOV, t_OLQTAUM1, ;
        t_PDFLEVAS, t_OLMODPRE, t_FLSERA, t_FLLOTT, t_FLUBIC, t_FLUBI2, t_PDSERORD, t_MVIMPCOM, t_MVIMPAC2, t_MVFLTRAS) ;
        VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
        this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
        this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
        this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
        this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
        this.w_MVFLORCO, this.w_MVFLCOCO, this.w_MVCODCOS, ;
        this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ;
        this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
        this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
        this.w_MVCODMAG, this.w_MVCODCEN, this.w_MVVOCCOS, ;
        this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_MVDATEVA, ;
        this.w_OLCODODL, this.w_PDROWNUM, this.w_OLCODMAG, this.w_MVCODMAT, this.w_DISMAG, this.w_OLQTAMOV, this.w_OLQTAUM1, ;
        this.w_PDFLEVAS, this.w_PRELAUT, this.w_FLSERA, this.w_FLLOTT, this.w_FLUBIC, this.w_F2UBIC, this.w_PDSERORD, this.w_MVIMPCOM, 0, this.w_MVFLTRAS)
    endcase
    SELECT GENEORDI
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive il Nuovo Documento di Destinazione
    this.w_SERORD = "AAAAAAAAAA"
    this.w_OSERORD = "ZZZZZZZZZZ"
    if this.w_DBRK <> "##zz##" AND RECCOUNT("GeneApp") > 0
      * --- Calcola Sconti su Omaggio
      this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
      * --- Aggiorna le Spese Accessorie e gli Sconti Finali
      this.w_MVSCONTI = Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
      this.w_MVFLSALD = " "
      * --- Lancia Batch per ripartizione spese accessorie
      *     Il 5� e l'ultimo parametro sono le spese accessorie che in questo caso non esistono
      this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.oParentObject.w_MVDATREG, this.w_MVDATDOC)
      GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, 0, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, 0)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcoli Finali
      GSAR_BFA(this,"D")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Nel caso di variazione di riga di rientro da conto lavoro non viene eliminata la testata del documento pertanto non serve ricrearla
      if this.w_DELTESTATA="S" or this.oParentObject.w_OPERAZ<>"SM" or this.oParentObject.w_OPERAZ<>"CS"
        * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
        this.w_MVSERIAL = SPACE(10)
        this.oParentObject.w_MVSERIAL = SPACE(10)
        this.oParentObject.w_MVNUMEST = 0
        this.oParentObject.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
        this.w_MVNUMRIF = -20
        * --- Controllo se il pagamento associato al fornitore prevede data diversa
        this.w_PAGA = "N"
        * --- Select from PAG_2AME
        i_nConn=i_TableProp[this.PAG_2AME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAG_2AME ";
              +" where P2CODICE="+cp_ToStrODBC(this.w_MVCODPAG)+" AND P2TIPSCA='DD'";
               ,"_Curs_PAG_2AME")
        else
          select * from (i_cTable);
           where P2CODICE=this.w_MVCODPAG AND P2TIPSCA="DD";
            into cursor _Curs_PAG_2AME
        endif
        if used('_Curs_PAG_2AME')
          select _Curs_PAG_2AME
          locate for 1=1
          do while not(eof())
          * --- Se prevede data diversa 
          this.w_PAGA = "S"
            select _Curs_PAG_2AME
            continue
          enddo
          use
        endif
        * --- Controlla se il Documento ha dlelle righe (in caso di Doc. di Trasferimento potrebbero non esserci ma, la transazione deve andare a buon fine)
      endif
      this.w_INSROW = 0
      SELECT GeneApp
      GO TOP
      COUNT FOR (t_MVQTAMOV<>0 OR t_MVTIPRIG="D") AND t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE) TO this.w_INSROW
      * --- Try
      local bErr_0513F7E8
      bErr_0513F7E8=bTrsErr
      this.Try_0513F7E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.oParentObject.w_OPERAZ<>"SM" AND this.oParentObject.w_OPERAZ<>"CS"
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        this.oParentObject.w_LOggErr = this.w_OLCODODL
        if this.oParentObject.w_OPERAZ $ "CA-SC-SM-RI-MO"
          if this.oParentObject.w_OPERAZ="CA"
            this.oParentObject.w_LErrore = ah_Msgformat("Generazione documento di carico non eseguita (%1)", ALLTRIM(this.w_MVCODICE) )
          else
            this.oParentObject.w_LErrore = ah_Msgformat("Generazione documento di scarico non eseguita (%1)", ALLTRIM(this.w_MVCODICE) )
          endif
        else
          do case
            case this.oParentObject.w_OPERAZ="BP"
              this.oParentObject.w_LErrore = ah_Msgformat("Generazione prelievo non eseguita (%1)", ALLTRIM(this.w_MVCODICE) )
            case this.oParentObject.w_OPERAZ="DT"
              this.oParentObject.w_LErrore = ah_Msgformat("Generazione trasferimento non eseguita (%1)", ALLTRIM(this.w_MVCODICE) )
            otherwise
              this.oParentObject.w_LErrore = ah_Msgformat("Generazione ordine non eseguita (%1)", ALLTRIM(this.w_MVCODICE) )
          endcase
        endif
        * --- Incrementa numero errori
        this.oParentObject.w_LNumErr = this.oParentObject.w_LNumErr + 1
        * --- Scrive LOG
        if EMPTY(this.oParentObject.w_LTesMes)
          this.oParentObject.w_LTesMes = "Message()= "+message()
        endif
        INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
        (this.oParentObject.w_LNumErr, this.oParentObject.w_LOggErr, this.oParentObject.w_LErrore, this.oParentObject.w_LTesMes)
        this.oParentObject.w_LTesMes = ""
      endif
      bTrsErr=bTrsErr or bErr_0513F7E8
      * --- End
    endif
    * --- Azzera il Temporaneo di Appoggio
    do case
      case this.oParentObject.w_OPERAZ="SC"
        CREATE CURSOR GeneApp ;
        (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
        t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
        t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
        t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
        t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ;
        t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
        t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
        t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
        t_MVFLORCO C(1), t_MVFLCOCO C(1), t_MVCODCOS C(5), ;
        t_MVCODMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), ;
        t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), ;
        t_OLCODODL C(15), t_PDROWNUM N(4,0), t_DCROWNUM N(4,0), t_OLCODMAG C(5), t_MVCODMAT C(5), ;
        t_DISMAG C(1), t_OLQTAMOV N(12,3), t_OLQTAUM1 N(12,3), t_PDFLEVAS C(1), ;
        t_FLSERA C(1), t_FLLOTT C(1), t_FLUBIC C(1), t_FLUBI2 C(1), t_PDSERORD C(10) ,t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), t_MVFLTRAS C(1))
      case this.oParentObject.w_OPERAZ$"SM-CS"
        CREATE CURSOR GeneApp ;
        (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
        t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
        t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
        t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
        t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ;
        t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
        t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
        t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
        t_MVFLORCO C(1), t_MVFLCOCO C(1), t_MVCODCOS C(5), ;
        t_MVCODMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), ;
        t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), ;
        t_OLCODODL C(15), t_PDROWNUM N(4,0), t_OLCODMAG C(5), t_MVCODMAT C(5), ;
        t_DISMAG C(1), t_OLQTAMOV N(12,3), t_OLQTAUM1 N(12,3), t_PDFLEVAS C(1), t_OLMODPRE C(1),;
        t_FLSERA C(1), t_FLLOTT C(1), t_FLUBIC C(1), t_FLUBI2 C(1), t_PDSERORD C(10), t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), t_ROWMAT N(6,0), t_MVFLTRAS C(1))
      otherwise
        CREATE CURSOR GeneApp ;
        (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
        t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
        t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
        t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
        t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ;
        t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
        t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
        t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
        t_MVFLORCO C(1), t_MVFLCOCO C(1), t_MVCODCOS C(5), ;
        t_MVCODMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), ;
        t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), ;
        t_OLCODODL C(15), t_PDROWNUM N(4,0), t_OLCODMAG C(5), t_MVCODMAT C(5), ;
        t_DISMAG C(1), t_OLQTAMOV N(12,3), t_OLQTAUM1 N(12,3), t_PDFLEVAS C(1), t_OLMODPRE C(1),;
        t_FLSERA C(1), t_FLLOTT C(1), t_FLUBIC C(1), t_FLUBI2 C(1), t_PDSERORD C(10), t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), t_MVFLTRAS C(1))
    endcase
    * --- Reinizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
    this.w_MVTOTRIT = 0
    this.w_OLDTEVCL = SPACE(10)
  endproc
  proc Try_0513F7E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_OPERAZ<>"SM" AND this.oParentObject.w_OPERAZ<>"CS"
      * --- begin transaction
      cp_BeginTrs()
    endif
    if this.oParentObject.w_OPERAZ $ "OR-DT-BP" AND this.w_INIDIS=.F.
      * --- Crea il Piano di Generazione Ordini a Terzisti / DDT di Trasferimento / Buoni di Prelievo
      this.w_PDSERIAL = SPACE(10)
      this.w_PDTIPGEN = this.oParentObject.w_OPERAZ
      i_Conn=i_TableProp[this.RIFMGODL_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEPIA"+this.w_PDTIPGEN, "i_codazi,w_PDSERIAL")
      * --- Insert into RIFMGODL
      i_nConn=i_TableProp[this.RIFMGODL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIFMGODL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIFMGODL_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PDSERIAL"+",PDTIPGEN"+",PDDATGEN"+",PDTIPDOC"+",PDFLPROV"+",PDCODUTE"+",PDPROVEN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PDSERIAL),'RIFMGODL','PDSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PDTIPGEN),'RIFMGODL','PDTIPGEN');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'RIFMGODL','PDDATGEN');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPDOC),'RIFMGODL','PDTIPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLPROV),'RIFMGODL','PDFLPROV');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RIFMGODL','PDCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPGES),'RIFMGODL','PDPROVEN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_PDSERIAL,'PDTIPGEN',this.w_PDTIPGEN,'PDDATGEN',i_DATSYS,'PDTIPDOC',this.oParentObject.w_MVTIPDOC,'PDFLPROV',this.oParentObject.w_MVFLPROV,'PDCODUTE',i_CODUTE,'PDPROVEN',this.w_TIPGES)
        insert into (i_cTable) (PDSERIAL,PDTIPGEN,PDDATGEN,PDTIPDOC,PDFLPROV,PDCODUTE,PDPROVEN &i_ccchkf. );
           values (;
             this.w_PDSERIAL;
             ,this.w_PDTIPGEN;
             ,i_DATSYS;
             ,this.oParentObject.w_MVTIPDOC;
             ,this.oParentObject.w_MVFLPROV;
             ,i_CODUTE;
             ,this.w_TIPGES;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento RIFMGODL'
        return
      endif
      this.w_INIDIS = .T.
    endif
    if this.w_INSROW<>0
      if this.w_DELTESTATA="S" or !this.oParentObject.w_OPERAZ $ "SM-CS"
        * --- Azzero numero documento altrimenti c'� il rischio che vengano generati documenti con lo stesso numero in caso di multiutenza
        this.oParentObject.w_MVNUMDOC = 0
        this.w_ESEDOC = this.oParentObject.w_MVCODESE
        this.w_ANNDOC = this.oParentObject.w_MVANNDOC
        i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        cp_NextTableProg(this.oParentObject, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
        cp_NextTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
        this.oParentObject.w_MVNUMREG = this.oParentObject.w_MVNUMDOC
        if this.w_NUMINI=0
          this.w_NUMINI = this.oParentObject.w_MVNUMREG
          this.w_NUMFIN = this.oParentObject.w_MVNUMREG
          this.w_DATAIN = this.w_MVDATDOC
          this.w_DATAFI = this.w_MVDATDOC
        else
          if this.oParentObject.w_MVNUMREG > this.w_NUMFIN
            this.w_NUMFIN = this.oParentObject.w_MVNUMREG
          endif
          if this.w_MVDATDOC > this.w_DATAFI
            this.w_DATAFI = this.w_MVDATDOC
          endif
        endif
        this.w_MVSERIAL = this.oParentObject.w_MVSERIAL
        * --- Scrive la Testata
        * --- Insert into DOC_MAST
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MVALFDOC"+",MVANNDOC"+",MVCAUCON"+",MVCLADOC"+",MVCODAGE"+",MVCODCON"+",MVCODDES"+",MVCODESE"+",MVCODPOR"+",MVCODSPE"+",MVCODUTE"+",MVCODVET"+",MVDATCIV"+",MVDATDOC"+",MVDATREG"+",MVFLACCO"+",MVFLCONT"+",MVFLGIOM"+",MVFLINTE"+",MVFLPROV"+",MVFLSCOR"+",MVFLVEAC"+",MVGENEFF"+",MVGENPRO"+",MVNUMDOC"+",MVNUMREG"+",MVPRD"+",MVRIFDIC"+",MVSERIAL"+",MVTCAMAG"+",MVTCOLIS"+",MVTCONTR"+",MVTFRAGG"+",MVTIPCON"+",MVTIPDOC"+",MVTIPORN"+",MVFLSALD"+",MVCATOPE"+",MVTIPOPE"+",MVEMERIC"+",MVCONCON"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFDOC),'DOC_MAST','MVALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANNDOC),'DOC_MAST','MVANNDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUCON),'DOC_MAST','MVCAUCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_MAST','MVCODAGE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESEDOC),'DOC_MAST','MVCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUTE),'DOC_MAST','MVCODUTE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATCIV),'DOC_MAST','MVDATCIV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_MAST','MVDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLACCO),'DOC_MAST','MVFLACCO');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLINTE),'DOC_MAST','MVFLINTE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLPROV),'DOC_MAST','MVFLPROV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMREG),'DOC_MAST','MVNUMREG');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRD),'DOC_MAST','MVPRD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPORN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSALD),'DOC_MAST','MVFLSALD');
          +","+cp_NullLink(cp_ToStrODBC("OP"),'DOC_MAST','MVCATOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPOPE),'DOC_MAST','MVTIPOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVEMERIC),'DOC_MAST','MVEMERIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCONCON),'DOC_MAST','MVCONCON');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MVALFDOC',this.oParentObject.w_MVALFDOC,'MVANNDOC',this.w_ANNDOC,'MVCAUCON',this.oParentObject.w_MVCAUCON,'MVCLADOC',this.w_MVCLADOC,'MVCODAGE',SPACE(5),'MVCODCON',this.w_MVCODCON,'MVCODDES',this.w_MVCODDES,'MVCODESE',this.w_ESEDOC,'MVCODPOR',this.w_MVCODPOR,'MVCODSPE',this.w_MVCODSPE,'MVCODUTE',this.oParentObject.w_MVCODUTE,'MVCODVET',this.w_MVCODVET)
          insert into (i_cTable) (MVALFDOC,MVANNDOC,MVCAUCON,MVCLADOC,MVCODAGE,MVCODCON,MVCODDES,MVCODESE,MVCODPOR,MVCODSPE,MVCODUTE,MVCODVET,MVDATCIV,MVDATDOC,MVDATREG,MVFLACCO,MVFLCONT,MVFLGIOM,MVFLINTE,MVFLPROV,MVFLSCOR,MVFLVEAC,MVGENEFF,MVGENPRO,MVNUMDOC,MVNUMREG,MVPRD,MVRIFDIC,MVSERIAL,MVTCAMAG,MVTCOLIS,MVTCONTR,MVTFRAGG,MVTIPCON,MVTIPDOC,MVTIPORN,MVFLSALD,MVCATOPE,MVTIPOPE,MVEMERIC,MVCONCON &i_ccchkf. );
             values (;
               this.oParentObject.w_MVALFDOC;
               ,this.w_ANNDOC;
               ,this.oParentObject.w_MVCAUCON;
               ,this.w_MVCLADOC;
               ,SPACE(5);
               ,this.w_MVCODCON;
               ,this.w_MVCODDES;
               ,this.w_ESEDOC;
               ,this.w_MVCODPOR;
               ,this.w_MVCODSPE;
               ,this.oParentObject.w_MVCODUTE;
               ,this.w_MVCODVET;
               ,this.oParentObject.w_MVDATCIV;
               ,this.w_MVDATDOC;
               ,this.oParentObject.w_MVDATREG;
               ,this.oParentObject.w_MVFLACCO;
               ," ";
               ," ";
               ,this.oParentObject.w_MVFLINTE;
               ,this.oParentObject.w_MVFLPROV;
               ,this.w_MVFLSCOR;
               ,this.w_MVFLVEAC;
               ," ";
               ," ";
               ,this.oParentObject.w_MVNUMDOC;
               ,this.oParentObject.w_MVNUMREG;
               ,this.oParentObject.w_MVPRD;
               ,this.w_MVRIFDIC;
               ,this.w_MVSERIAL;
               ,this.oParentObject.w_MVTCAMAG;
               ,this.w_MVTCOLIS;
               ,this.w_MVTCONTR;
               ,this.oParentObject.w_MVTFRAGG;
               ,this.oParentObject.w_MVTIPCON;
               ,this.oParentObject.w_MVTIPDOC;
               ,this.oParentObject.w_MVTIPCON;
               ,this.w_MVFLSALD;
               ,"OP";
               ,this.w_MVTIPOPE;
               ,this.w_MVEMERIC;
               ,this.w_ANCONCON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in inserimento DOC_MAST (1)'
          return
        endif
        * --- Occorrono 3 frasi per problema lunghezza stringa INSERT
        this.w_MV__NOTE = ""
        do case
          case this.oParentObject.w_OPERAZ="OR"
            this.w_MV__NOTE = ah_Msgformat("Rif. piano di manutenzione ordini a fornitore n. %1" , this.w_PDSERIAL)
          case this.oParentObject.w_OPERAZ="DT"
            this.w_MV__NOTE = ah_Msgformat("Rif. piano di manutenzione DDT di trasferimento n. %1" , this.w_PDSERIAL)
          case this.oParentObject.w_OPERAZ="BP"
            this.w_MV__NOTE = ah_Msgformat("Rif. piano di manutenzione buoni di prelievo n. %1" , this.w_PDSERIAL)
          case this.oParentObject.w_OPERAZ = "CA"
            this.w_MV__NOTE = ah_Msgformat("Rif. dichiarazione di produzione n. %1" , this.w_DPSERIAL)
          case this.oParentObject.w_OPERAZ $"SC-RI-MO"
            this.w_MV__NOTE = ah_Msgformat("Rif. dichiarazione di produzione n. %1", this.w_DPSERIAL)
          case this.oParentObject.w_OPERAZ $ "SM-CS"
            this.w_MV__NOTE = ah_Msgformat("Rif. OCL n. %1" , this.w_OLCODODL)
        endcase
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
          +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
          +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
          +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
          +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
          +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
          +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
          +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
          +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
          +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
          +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
          +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
          +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
          +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
          +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
          +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
          +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
          +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
          +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
          +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
          +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFEST),'DOC_MAST','MVALFEST');
          +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRP),'DOC_MAST','MVPRP');
          +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNPRO),'DOC_MAST','MVANNPRO');
          +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMEST),'DOC_MAST','MVNUMEST');
          +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLFOSC');
          +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVNOTAGG');
          +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
          +",MV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MV__NOTE),'DOC_MAST','MV__NOTE');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVCODPAG = this.w_MVCODPAG;
              ,MVCODIVE = this.w_MVCODIVE;
              ,MVSCONTI = this.w_MVSCONTI;
              ,MVSPEINC = this.w_MVSPEINC;
              ,MVIVAINC = this.w_MVIVAINC;
              ,MVFLRINC = this.w_MVFLRINC;
              ,MVSPETRA = this.w_MVSPETRA;
              ,MVIVATRA = this.w_MVIVATRA;
              ,MVFLRTRA = this.w_MVFLRTRA;
              ,MVSPEIMB = this.w_MVSPEIMB;
              ,MVIVAIMB = this.w_MVIVAIMB;
              ,MVFLRIMB = this.w_MVFLRIMB;
              ,MVSPEBOL = this.w_MVSPEBOL;
              ,MVCODBAN = this.w_MVCODBAN;
              ,MVVALNAZ = this.w_MVVALNAZ;
              ,MVCODVAL = this.w_MVCODVAL;
              ,MVCAOVAL = this.w_MVCAOVAL;
              ,MVSCOCL1 = this.w_MVSCOCL1;
              ,MVSCOCL2 = this.w_MVSCOCL2;
              ,MVSCOPAG = this.w_MVSCOPAG;
              ,MVALFEST = this.oParentObject.w_MVALFEST;
              ,MVPRP = this.oParentObject.w_MVPRP;
              ,MVANNPRO = this.oParentObject.w_MVANNPRO;
              ,MVNUMEST = this.oParentObject.w_MVNUMEST;
              ,MVFLFOSC = " ";
              ,MVNOTAGG = " ";
              ,MVCODBA2 = this.w_MVCODBA2;
              ,MV__NOTE = this.w_MV__NOTE;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura DOC_MAST (1)'
          return
        endif
        * --- Se il pagamento legato al fornitore prevede data diversa nell'ordine verr� 
        *     aggiornato il campo Data diversa con la data inserita nella maschera di generazione ordini
        this.w_APPO = IIF(this.w_PAGA="S", this.w_MVDATDIV, cp_CharToDate("  -  -  "))
        if this.oParentObject.w_OPERAZ $ "CA-SC-RI-MO"
          * --- Generazione Carichi/Scarichi da Dichiarazione di Produzione inserisce riferimento Fittizio per blocco Modifiche nei Documento
          this.w_PDSERIAL = IIF(this.oParentObject.w_OPERAZ="CA", "CARICO", "SCARICO")
        endif
        if this.oParentObject.w_OPERAZ $ "SM-CS"
          * --- Generazione Carichi/Scarichi da Dichiarazione di Produzione inserisce riferimento Fittizio per blocco Modifiche nei Documento
          if this.oParentObject.w_OPERAZ = "SM"
            * --- Scarico materiali di conto lavoro
            this.w_PDSERIAL = "SCARICOMCL"
          else
            * --- Switch
            *     Scarico fase di conto lavoro
            *     Carico prodotto finito ultima fase di conto lavoro
            this.w_PDSERIAL = IIF(this.oParentObject.w_OPERAZCL="SF", "SCARICOFCL", "CARICOFCL")
          endif
        endif
        * --- Significato Campi DOC_MAST: MVRIFODL = w_PDSERIAL ; MVSERDDT = w_MVSERDDT
        * --- w_PDSERIAL = <Rif. Piano di manutenzione> (OR, TD, BP) ; 'CARICO' (CA) ; 'SCARICO' (SC) ; <Vuoto> (SM)
        * --- w_MVSERDDT = <Rif. Serial Doc. di Origine (SM) ; <Vuoto> (OR, DT, BP, CA, SC)
        * --- w_MVROWDDT = <Rif. Num Riga Doc. di Origine (SM) 
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','UTCV');
          +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_MAST','UTDV');
          +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_APPO),'DOC_MAST','MVDATDIV');
          +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
          +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
          +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
          +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
          +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
          +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
          +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
          +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
          +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
          +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
          +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
          +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
          +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
          +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
          +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
          +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
          +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
          +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
          +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
          +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
          +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
          +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
          +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
          +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
          +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
          +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
          +",MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
          +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
          +",MVDATTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATTRA),'DOC_MAST','MVDATTRA');
          +",MVORATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVORATRA),'DOC_MAST','MVORATRA');
          +",MVMINTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVMINTRA),'DOC_MAST','MVMINTRA');
          +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(this.w_MVNOTAGG),'DOC_MAST','MVNOTAGG');
          +",MVRIFFAD ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFFAD');
          +",MVRIFODL ="+cp_NullLink(cp_ToStrODBC(this.w_PDSERIAL),'DOC_MAST','MVRIFODL');
          +",MVSERDDT ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERDDT),'DOC_MAST','MVSERDDT');
          +",MVROWDDT ="+cp_NullLink(cp_ToStrODBC(this.w_MVROWDDT),'DOC_MAST','MVROWDDT');
          +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
          +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'DOC_MAST','MVNUMCOR');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              UTCC = i_CODUTE;
              ,UTCV = 0;
              ,UTDC = SetInfoDate( g_CALUTD );
              ,UTDV = cp_CharToDate("  -  -    ");
              ,MVDATDIV = this.w_APPO;
              ,MVACCONT = this.w_MVACCONT;
              ,MVACCPRE = this.w_MVACCPRE;
              ,MVACIVA1 = this.w_MVACIVA1;
              ,MVACIVA2 = this.w_MVACIVA2;
              ,MVACIVA3 = this.w_MVACIVA3;
              ,MVACIVA4 = this.w_MVACIVA4;
              ,MVACIVA5 = this.w_MVACIVA5;
              ,MVACIVA6 = this.w_MVACIVA6;
              ,MVAFLOM1 = this.w_MVAFLOM1;
              ,MVAFLOM2 = this.w_MVAFLOM2;
              ,MVAFLOM3 = this.w_MVAFLOM3;
              ,MVAFLOM4 = this.w_MVAFLOM4;
              ,MVAFLOM5 = this.w_MVAFLOM5;
              ,MVAFLOM6 = this.w_MVAFLOM6;
              ,MVAIMPN1 = this.w_MVAIMPN1;
              ,MVAIMPN2 = this.w_MVAIMPN2;
              ,MVAIMPN3 = this.w_MVAIMPN3;
              ,MVAIMPN4 = this.w_MVAIMPN4;
              ,MVAIMPN5 = this.w_MVAIMPN5;
              ,MVAIMPN6 = this.w_MVAIMPN6;
              ,MVAIMPS1 = this.w_MVAIMPS1;
              ,MVAIMPS2 = this.w_MVAIMPS2;
              ,MVAIMPS3 = this.w_MVAIMPS3;
              ,MVAIMPS4 = this.w_MVAIMPS4;
              ,MVAIMPS5 = this.w_MVAIMPS5;
              ,MVAIMPS6 = this.w_MVAIMPS6;
              ,MVIMPARR = this.w_MVIMPARR;
              ,MVIVABOL = this.w_MVIVABOL;
              ,MVDATTRA = this.w_MVDATTRA;
              ,MVORATRA = this.w_MVORATRA;
              ,MVMINTRA = this.w_MVMINTRA;
              ,MVNOTAGG = this.w_MVNOTAGG;
              ,MVRIFFAD = SPACE(10);
              ,MVRIFODL = this.w_PDSERIAL;
              ,MVSERDDT = this.w_MVSERDDT;
              ,MVROWDDT = this.w_MVROWDDT;
              ,MVFLSCOM = this.w_MVFLSCOM;
              ,MVNUMCOR = this.w_NUMCOR;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura DOC_MAST (2)'
          return
        endif
        * --- Cicla sulle Righe Documento
        ah_Msg("Generazione documento: %1",.T.,.F.,.F., ALLTRIM(STR(this.oParentObject.w_MVNUMDOC,15)) )
      else
        this.w_MVSERIAL = this.oParentObject.w_SEDOC
      endif
    endif
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
    * --- Scrive il Dettaglio
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    * --- Traduzione in Lingua della Descrizione Articoli (da GSVE_MDV)
    if NOT (EMPTY(this.w_MVCODICE) OR EMPTY(this.w_CODLIN) OR this.w_CODLIN=g_CODLIN)
      * --- Legge la Traduzione
      DECLARE ARRRIS (2) 
 a=Tradlin(this.w_MVCODICE,this.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
      if Not Empty(this.w_DESCOD)
        this.w_MVDESART = this.w_DESCOD
        this.w_MVDESSUP = this.w_DESSUP
      endif
      SELECT GeneApp
    endif
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVIMPACC = t_MVIMPACC
    this.w_MVIMPSCO = t_MVIMPSCO
    this.w_MVVALMAG = t_MVVALMAG
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVSCONT1 = t_MVSCONT1
    this.w_MVSCONT2 = t_MVSCONT2
    this.w_MVSCONT3 = t_MVSCONT3
    this.w_MVSCONT4 = t_MVSCONT4
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPESNET = t_MVPESNET
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVFLTRAS = t_MVFLTRAS
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_MVCODMAG = t_MVCODMAG
    this.w_OLCODMAG = t_OLCODMAG
    this.w_OLQTAMOV = t_OLQTAMOV
    this.w_OLQTAUM1 = t_OLQTAUM1
    this.w_MVCODMAT = t_MVCODMAT
    this.w_DISMAG = t_DISMAG
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVVOCCOS = t_MVVOCCOS
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVTIPATT = t_MVTIPATT
    this.w_MVCODATT = t_MVCODATT
    this.w_MVDATEVA = t_MVDATEVA
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MVFLORDI = this.w_MFLORDI
    this.w_MVFLIMPE = this.w_MFLIMPE
    this.w_MVFLELGM = this.w_MFLELGM
    this.w_OLCODODL = t_OLCODODL
    this.w_PDROWNUM = t_PDROWNUM
    if this.oParentObject.w_OPERAZ = "SC"
      this.w_DCROWNUM = t_DCROWNUM
    endif
    this.w_PDFLEVAS = t_PDFLEVAS
    if this.oParentObject.w_OPERAZ $ "BP-DT"
      * --- Nel caso di Riga a Zero e Flag Evaso Scrivo scrivo sulla riga dell'OCL
      *     Il seriale della generazione che lo ha evaso per riaprire il flag in caso
      *     di cancellazione
      this.w_OLDTEVCL = IIF(this.w_OLQTAMOV=0 and this.w_PDFLEVAS="S", this.w_PDSERIAL ,SPACE(10))
    endif
    this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND this.w_MFLAVAL<>"N", this.w_MVCODART, SPACE(20))
    this.w_MVCODMAG = IIF(EMPTY(this.w_MVKEYSAL), SPACE(5), this.w_MVCODMAG)
    this.w_FLLOTT = t_FLLOTT
    this.w_FLUBIC = t_FLUBIC
    this.w_F2UBIC = t_FLUBI2
    this.w_MVFLLOTT = " "
    this.w_MVF2LOTT = " "
    this.w_MVCODLOT = SPACE(20)
    this.w_MVCODUBI = SPACE(20)
    * --- Gestione Commessa
    this.w_MVFLORCO = SPACE(1)
    this.w_MVFLCOCO = SPACE(1)
    this.w_MVCODCOS = SPACE(5)
    this.w_OK_COMM = .F.
    if this.oParentObject.w_OPERAZ $ "CA-OR-SC-SM"
      if g_COMM="S"
        this.w_MVCODCOM = NVL(t_MVCODCOM, SPACE(15))
        this.w_MVCODATT = NVL(t_MVCODATT, SPACE(15))
        this.w_OK_COMM = g_COMM="S" And this.w_MFLCOMM<>"N" And not empty(this.w_MVCODCOM) and not empty(this.w_MVCODATT)
      endif
      if this.w_OK_COMM and nvl(this.w_MVTFLCOM, " ") = "S"
        this.w_MVIMPCOM = t_MVIMPCOM
        this.w_MVFLORCO = t_MVFLORCO
        this.w_MVFLCOCO = t_MVFLCOCO
        this.w_MVCODCOS = t_MVCODCOS
      endif
    endif
    * --- Gestione Analitica
    this.w_MVVOCCEN = SPACE(15)
    if g_PERCCR="S" 
      * --- Voce di Costo 
      this.w_MVVOCCEN = this.w_MVVOCCOS
      if NOT EMPTY(this.w_MVVOCCEN) OR this.w_MVTIPRIG $ "MF"
        this.w_MVCODCEN = t_MVCODCEN
      endif
      this.w_MVCODCOM = NVL(t_MVCODCOM, SPACE(15))
      this.w_MVCODATT = IIF(g_COMM="S", t_MVCODATT, SPACE(15))
      this.w_MVIMPCOM = IIF(g_COMM="S", t_MVIMPCOM, 0)
    endif
    this.w_PDSERDOC = SPACE(10)
    this.w_PDROWDOC = 0
    if this.w_INSROW<>0
      this.w_PDSERDOC = this.w_MVSERIAL
      this.w_MVCODODL = this.w_OLCODODL
      this.w_MVRIGMAT = this.w_PDROWNUM
      if this.w_MVQTAMOV<>0 OR this.w_MVTIPRIG="D"
        if ((g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_FLUBIC="S")) AND NOT EMPTY(this.w_MVCODMAG)
          this.w_MVFLLOTT = LEFT(ALLTRIM(this.w_MFLCASC)+IIF(this.w_MFLRISE="+", "-", IIF(this.w_MFLRISE="-", "+", " ")), 1)
        endif
        if ((g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_F2UBIC="S")) AND NOT EMPTY(this.w_MVCODMAT)
          this.w_MVF2LOTT = LEFT(ALLTRIM(this.w_MF2CASC)+IIF(this.w_MF2RISE="+", "-", IIF(this.w_MF2RISE="-", "+", " ")), 1)
        endif
        if this.oParentObject.w_OPERAZ="DT" AND this.w_FLNSRI="S" AND NOT EMPTY(g_ARTDES)
          * --- Se DDT Legge Rif. Ordine di Origine e, se cambiato, Scrive nuova Riga Descrittiva sul Temporaneo di Appoggio
          SELECT GeneApp
          this.w_SERORD = t_PDSERORD
          if this.w_SERORD<>this.w_OSERORD AND NOT EMPTY(this.w_SERORD)
            this.w_NUMORD = 0
            this.w_ALFORD = "  "
            this.w_DATORD = cp_CharToDate("  -  -  ")
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVNUMDOC,MVALFDOC,MVDATDOC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERORD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVNUMDOC,MVALFDOC,MVDATDOC;
                from (i_cTable) where;
                    MVSERIAL = this.w_SERORD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMORD = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
              this.w_ALFORD = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
              this.w_DATORD = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
             
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_NUMORD)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_ALFORD 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]= DTOC(this.w_DATORD) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]="" 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=this.w_MVCODCON 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= "" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.oParentObject.w_MVTIPCON
            this.w_APPO = LEFT(ah_Msgformat("Rif. ordine n. %1 - del %2" ,ALLTRIM(STR(this.w_NUMORD))+IIF(EMPTY(this.w_ALFORD),"", "/"+ALLTRIM(this.w_ALFORD)), DTOC(this.w_DATORD) ),40)
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_CPROWORD = this.w_CPROWORD + 10
            * --- Insert into DOC_DETT
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CPROWNUM"+",CPROWORD"+",MVCAUMAG"+",MVCODART"+",MVCODCLA"+",MVCODICE"+",MVDESART"+",MVF2CASC"+",MVF2IMPE"+",MVF2LOTT"+",MVF2ORDI"+",MVF2RISE"+",MVFLARIF"+",MVFLCASC"+",MVFLIMPE"+",MVFLLOTT"+",MVFLOMAG"+",MVFLORDI"+",MVFLRAGG"+",MVFLRISE"+",MVFLTRAS"+",MVIMPACC"+",MVIMPNAZ"+",MVIMPPRO"+",MVIMPSCO"+",MVMOLSUP"+",MVNUMCOL"+",MVNUMRIF"+",MVPERPRO"+",MVPESNET"+",MVPREZZO"+",MVQTAMOV"+",MVQTASAL"+",MVQTAUM1"+",MVROWRIF"+",MVSERIAL"+",MVSERRIF"+",MVTIPRIG"+",MVVALMAG"+",MVVALRIG"+",MV_SEGNO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCAMAG),'DOC_DETT','MVCAUMAG');
              +","+cp_NullLink(cp_ToStrODBC(g_ARTDES),'DOC_DETT','MVCODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TPNDOC),'DOC_DETT','MVCODCLA');
              +","+cp_NullLink(cp_ToStrODBC(g_ARTDES),'DOC_DETT','MVCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPO),'DOC_DETT','MVDESART');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2LOTT');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLCASC');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLIMPE');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLLOTT');
              +","+cp_NullLink(cp_ToStrODBC("X"),'DOC_DETT','MVFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLORDI');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRISE');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLTRAS');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPACC');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPSCO');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVMOLSUP');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPESNET');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
              +","+cp_NullLink(cp_ToStrODBC("D"),'DOC_DETT','MVTIPRIG');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MVCAUMAG',this.oParentObject.w_MVTCAMAG,'MVCODART',g_ARTDES,'MVCODCLA',this.w_TPNDOC,'MVCODICE',g_ARTDES,'MVDESART',this.w_APPO,'MVF2CASC'," ",'MVF2IMPE'," ",'MVF2LOTT'," ",'MVF2ORDI'," ",'MVF2RISE'," ")
              insert into (i_cTable) (CPROWNUM,CPROWORD,MVCAUMAG,MVCODART,MVCODCLA,MVCODICE,MVDESART,MVF2CASC,MVF2IMPE,MVF2LOTT,MVF2ORDI,MVF2RISE,MVFLARIF,MVFLCASC,MVFLIMPE,MVFLLOTT,MVFLOMAG,MVFLORDI,MVFLRAGG,MVFLRISE,MVFLTRAS,MVIMPACC,MVIMPNAZ,MVIMPPRO,MVIMPSCO,MVMOLSUP,MVNUMCOL,MVNUMRIF,MVPERPRO,MVPESNET,MVPREZZO,MVQTAMOV,MVQTASAL,MVQTAUM1,MVROWRIF,MVSERIAL,MVSERRIF,MVTIPRIG,MVVALMAG,MVVALRIG,MV_SEGNO &i_ccchkf. );
                 values (;
                   this.w_CPROWNUM;
                   ,this.w_CPROWORD;
                   ,this.oParentObject.w_MVTCAMAG;
                   ,g_ARTDES;
                   ,this.w_TPNDOC;
                   ,g_ARTDES;
                   ,this.w_APPO;
                   ," ";
                   ," ";
                   ," ";
                   ," ";
                   ," ";
                   ," ";
                   ," ";
                   ," ";
                   ," ";
                   ,"X";
                   ," ";
                   ,this.oParentObject.w_MVTFRAGG;
                   ," ";
                   ," ";
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,this.w_MVNUMRIF;
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,0;
                   ,this.w_MVSERIAL;
                   ,SPACE(10);
                   ,"D";
                   ,0;
                   ,0;
                   ,this.w_MV_SEGNO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento DOC_DETT'
              return
            endif
            SELECT GeneApp
          endif
          this.w_OSERORD = this.w_SERORD
        endif
        * --- In casi di Prelievi potrebbe essere stato tutto evaso
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWORD + 10
        * --- Aggiunge riferimenti al Documento di Origine
        this.w_APPO = ""
        do case
          case this.oParentObject.w_OPERAZ="OR"
            if this.w_TIPGES="E"
              this.w_APPO = ah_Msgformat("Rif. ODA n. %1", this.w_OLCODODL)
            else
              this.w_APPO = ah_Msgformat("Rif. OCL n. %1", this.w_OLCODODL)
            endif
          case this.oParentObject.w_OPERAZ="DT"
            this.w_APPO = ah_Msgformat("Rif. OCL n. %1 - n. riga materiali: %2", this.w_OLCODODL, ALLTRIM(STR(this.w_PDROWNUM)) )
          case this.oParentObject.w_OPERAZ="BP"
            this.w_APPO = ah_Msgformat("Rif. ODL n. %1 - n. riga materiali: %2", this.w_OLCODODL, ALLTRIM(STR(this.w_PDROWNUM)) )
          case this.oParentObject.w_OPERAZ = "CA"
            this.w_MVCODLOT = IIF(g_PERLOT="S" AND this.w_FLLOTT$ "SC" AND this.w_MVFLLOTT $ "+-" AND this.w_MVQTAMOV<>0, this.w_DPCODLOT, SPACE(20))
            this.w_MVCODUBI = IIF(g_PERUBI="S" AND this.w_FLUBIC="S" AND this.w_MVFLLOTT $ "+-" AND this.w_MVQTAMOV<>0, this.w_DPCODUBI, SPACE(20))
            this.w_APPO = ah_Msgformat("Rif. ODL n. %1", this.w_OLCODODL)
          case this.oParentObject.w_OPERAZ $"SC-RI-MO"
            this.w_APPO = ah_Msgformat("Rif. ODL n. %1 - n. riga materiali: %2", this.w_OLCODODL, ALLTRIM(STR(this.w_PDROWNUM)) )
          case this.oParentObject.w_OPERAZ $ "SM-CS"
            this.w_APPO = ah_Msgformat("Rif. DDT n. %1 - del %2%0Rif. OCL n.%3 - n. riga materiali: %4", ALLTRIM(STR(this.w_NUMDDT,15)) + IIF(EMPTY(this.w_ALFDDT), "", "/"+ALLTRIM(this.w_ALFDDT)) , DTOC(this.w_DATDDT), this.w_OLCODODL, ALLTRIM(STR(this.w_PDROWNUM)) )
        endcase
        this.w_MVDESSUP = IIF(EMPTY(this.w_APPO), this.w_MVDESSUP, this.w_APPO + IIF(EMPTY(this.w_MVDESSUP), "", CHR(13) + this.w_MVDESSUP))
        * --- Significato Campi DOC_DETT: MVCODODL = w_MVCODODL ; MVRIGMAT = w_MVRIGMAT
        * --- w_MVCODODL (OLCODODL) = <Rif. Serial Codice ODL/OCL di Origine> (OR, TD, BP, CA, SC, SM) 
        * --- w_MVRIGMAT (PDROWNUM) = <N.Riga Materiale ODL_DETT di Origine> (DT, BP, SC, SM) ; <0> (OR, CA)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARTIPCO1,ARTIPCO2,ARGESMAT,ARPREZUM,ARTIPART"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARTIPCO1,ARTIPCO2,ARGESMAT,ARPREZUM,ARTIPART;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCO1 = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
          this.w_TIPCO2 = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
          this.w_FLMATR = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
          this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLPACK="S"
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CATIPCO3"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CATIPCO3;
              from (i_cTable) where;
                  CACODICE = this.w_MVCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPCO3 = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TIPCO = IIF(this.w_MVUNIMIS=this.w_UNMIS3,this.w_TIPCO3,IIF(this.w_MVUNIMIS=this.w_UNMIS2,this.w_TIPCO2,this.w_TIPCO1))
        endif
        * --- Controllo gestione matricole e lotti/ubicazioni
        if this.w_TIPART<>"FS"
          if g_MATR="S" AND this.w_FLMATR="S"
            INSERT INTO MATRERRO (SERDOC, NUMDOC, DATDOC, ROWNUM, CODART, DESART, MAGAZZ, TIPO); 
 VALUES (this.w_MVSERIAL,this.oParentObject.w_MVNUMDOC, this.w_MVDATDOC, this.w_CPROWNUM, this.w_MVCODART,this.w_MVDESART, this.w_MVCODMAG, "M")
          endif
          if g_PERLOT="S" AND this.w_FLLOTT<>"N"
            INSERT INTO MATRERRO (SERDOC, NUMDOC, DATDOC, ROWNUM, CODART, DESART, MAGAZZ, TIPO); 
 VALUES (this.w_MVSERIAL,this.oParentObject.w_MVNUMDOC, this.w_MVDATDOC, this.w_CPROWNUM, this.w_MVCODART, this.w_MVDESART, this.w_MVCODMAG, "L")
          endif
          if g_PERUBI="S" AND this.w_FLUBIC="S"
            INSERT INTO MATRERRO (SERDOC, NUMDOC, DATDOC, ROWNUM, CODART, DESART, MAGAZZ, TIPO); 
 VALUES (this.w_MVSERIAL,this.oParentObject.w_MVNUMDOC, this.w_MVDATDOC, this.w_CPROWNUM, this.w_MVCODART, this.w_MVDESART, this.w_MVCODMAG, "U")
          endif
        endif
        this.w_MVLOTMAG = iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUBI ) , SPACE(5) , this.w_MVCODMAG )
        * --- Insert into DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCONTRO"+",MVCAUMAG"+",MVCODCLA"+",MVCONTRA"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVFLOMAG"+",MVCODIVA"+",MVCONIND"+",MVVALRIG"+",MVIMPACC"+",MVIMPSCO"+",MVVALMAG"+",MVIMPNAZ"+",MVPERPRO"+",MVIMPPRO"+",MVPESNET"+",MVNOMENC"+",MVMOLSUP"+",MVNUMCOL"+",MVFLTRAS"+",MVFLRAGG"+",MVSERRIF"+",MVROWRIF"+",MVFLARIF"+",MVQTASAL"+",MVFLCOCO"+",MVFLORCO"+",MVCODCOS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
          +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
          +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
          +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
          +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVROWRIF');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLCOCO),'DOC_DETT','MVFLCOCO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLORCO),'DOC_DETT','MVFLORCO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOS),'DOC_DETT','MVCODCOS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_CPROWORD,'MVTIPRIG',this.w_MVTIPRIG,'MVCODICE',this.w_MVCODICE,'MVCODART',this.w_MVCODART,'MVDESART',this.w_MVDESART,'MVDESSUP',this.w_MVDESSUP,'MVUNIMIS',this.w_MVUNIMIS,'MVCATCON',this.w_MVCATCON,'MVCONTRO',SPACE(15))
          insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCONTRO,MVCAUMAG,MVCODCLA,MVCONTRA,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVFLOMAG,MVCODIVA,MVCONIND,MVVALRIG,MVIMPACC,MVIMPSCO,MVVALMAG,MVIMPNAZ,MVPERPRO,MVIMPPRO,MVPESNET,MVNOMENC,MVMOLSUP,MVNUMCOL,MVFLTRAS,MVFLRAGG,MVSERRIF,MVROWRIF,MVFLARIF,MVQTASAL,MVFLCOCO,MVFLORCO,MVCODCOS &i_ccchkf. );
             values (;
               this.w_MVSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_MVNUMRIF;
               ,this.w_CPROWORD;
               ,this.w_MVTIPRIG;
               ,this.w_MVCODICE;
               ,this.w_MVCODART;
               ,this.w_MVDESART;
               ,this.w_MVDESSUP;
               ,this.w_MVUNIMIS;
               ,this.w_MVCATCON;
               ,SPACE(15);
               ,this.w_MVCAUMAG;
               ,this.w_MVCODCLA;
               ,this.w_MVCONTRA;
               ,this.w_MVCODLIS;
               ,this.w_MVQTAMOV;
               ,this.w_MVQTAUM1;
               ,this.w_MVPREZZO;
               ,this.w_MVSCONT1;
               ,this.w_MVSCONT2;
               ,this.w_MVSCONT3;
               ,this.w_MVSCONT4;
               ,this.w_MVFLOMAG;
               ,this.w_MVCODIVA;
               ,this.w_MVCONIND;
               ,this.w_MVVALRIG;
               ,this.w_MVIMPACC;
               ,this.w_MVIMPSCO;
               ,this.w_MVVALMAG;
               ,this.w_MVIMPNAZ;
               ,0;
               ,0;
               ,this.w_MVPESNET;
               ,this.w_MVNOMENC;
               ,this.w_MVMOLSUP;
               ,0;
               ,this.w_MVFLTRAS;
               ,this.oParentObject.w_MVTFRAGG;
               ,SPACE(10);
               ,0;
               ," ";
               ,this.w_MVQTAUM1;
               ,this.w_MVFLCOCO;
               ,this.w_MVFLORCO;
               ,this.w_MVCODCOS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in inserimento DOC_DETT'
          return
        endif
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
          +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
          +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
          +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
          +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
          +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODUB2');
          +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MF2CASC),'DOC_DETT','MVF2CASC');
          +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MF2IMPE),'DOC_DETT','MVF2IMPE');
          +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MF2ORDI),'DOC_DETT','MVF2ORDI');
          +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MF2RISE),'DOC_DETT','MVF2RISE');
          +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
          +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELGM),'DOC_DETT','MVFLELGM');
          +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
          +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
          +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
          +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCOL),'DOC_DETT','MVCAUCOL');
          +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
          +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
          +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
          +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
          +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
          +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
          +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'DOC_DETT','MVCODMAT');
          +",MVCODODL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODODL),'DOC_DETT','MVCODODL');
          +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
          +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
          +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
          +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
          +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
          +",MVRIGMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIGMAT),'DOC_DETT','MVRIGMAT');
          +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
          +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
          +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
          +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
          +",MVCODCOL ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCO),'DOC_DETT','MVCODCOL');
          +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLANAL),'DOC_DETT','MVFLELAN');
          +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
          +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
          +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                 )
        else
          update (i_cTable) set;
              MVFLERIF = " ";
              ,MVFLARIF = " ";
              ,MVFLRIPA = " ";
              ,MVFLEVAS = " ";
              ,MVIMPEVA = 0;
              ,MVQTAEVA = 0;
              ,MVQTAEV1 = 0;
              ,MVCODUB2 = SPACE(20);
              ,MVF2CASC = this.w_MF2CASC;
              ,MVF2IMPE = this.w_MF2IMPE;
              ,MVF2ORDI = this.w_MF2ORDI;
              ,MVF2RISE = this.w_MF2RISE;
              ,MVFLCASC = this.w_MFLCASC;
              ,MVFLELGM = this.w_MFLELGM;
              ,MVFLIMPE = this.w_MFLIMPE;
              ,MVFLORDI = this.w_MFLORDI;
              ,MVFLRISE = this.w_MFLRISE;
              ,MVCAUCOL = this.w_MVCAUCOL;
              ,MVCAUMAG = this.w_MVCAUMAG;
              ,MVCODATT = this.w_MVCODATT;
              ,MVCODCEN = this.w_MVCODCEN;
              ,MVCODCOM = this.w_MVCODCOM;
              ,MVCODLOT = this.w_MVCODLOT;
              ,MVCODMAG = this.w_MVCODMAG;
              ,MVCODMAT = this.w_MVCODMAT;
              ,MVCODODL = this.w_MVCODODL;
              ,MVCODUBI = this.w_MVCODUBI;
              ,MVDATEVA = this.w_MVDATEVA;
              ,MVF2LOTT = this.w_MVF2LOTT;
              ,MVFLLOTT = this.w_MVFLLOTT;
              ,MVKEYSAL = this.w_MVKEYSAL;
              ,MVRIGMAT = this.w_MVRIGMAT;
              ,MVTIPATT = this.w_MVTIPATT;
              ,MVVOCCEN = this.w_MVVOCCEN;
              ,MVQTAIMP = 0;
              ,MVQTAIM1 = 0;
              ,MVCODCOL = this.w_TIPCO;
              ,MVFLELAN = this.w_MVFLANAL;
              ,MV_SEGNO = this.w_MV_SEGNO;
              ,MVIMPCOM = this.w_MVIMPCOM;
              ,MVLOTMAG = this.w_MVLOTMAG;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.w_CPROWNUM;
              and MVNUMRIF = this.w_MVNUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura DOC_DETT'
          return
        endif
        if this.oParentObject.w_OPERAZ$"SM-CS"
          this.w_ROWMAT = nvl(t_ROWMAT,0)
          INSERT INTO DOCGENE (SERDOC, ROWMAT,ROWDOC) values (this.w_MVSERIAL, this.w_ROWMAT,this.w_CPROWNUM)
        endif
        this.w_PDROWDOC = this.w_CPROWNUM
        if g_MATR="S" AND this.w_FLMATR="S" and this.oParentObject.w_OPERAZ $ "SC-CA"
          * --- Inserisco sulle righe delle Dichiarazioni matricole il riferimento al Documento associato
          if this.oParentObject.w_OPERAZ = "SC"
            * --- Se w_PDROWNUM=0 (Componente aggiunto alla lista dei materiali a livello di dichiarazione) il riferimento corretto 
            *     alla riga del documento � w_DCROWNUM invece del sopra menzionato w_PDROWNUM
            this.w_DCROWNUM1 = iif(this.w_PDROWNUM=0,this.w_DCROWNUM,this.w_PDROWNUM)
            * --- Write into DIC_MCOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MTRIFDIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DIC_MCOM','MTRIFDIS');
              +",MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DIC_MCOM','MTROWDIS');
                  +i_ccchkf ;
              +" where ";
                  +"MTSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                  +" and MTROWODL = "+cp_ToStrODBC(this.w_DCROWNUM1);
                     )
            else
              update (i_cTable) set;
                  MTRIFDIS = this.w_MVSERIAL;
                  ,MTROWDIS = this.w_CPROWNUM;
                  &i_ccchkf. ;
               where;
                  MTSERIAL = this.w_DPSERIAL;
                  and MTROWODL = this.w_DCROWNUM1;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into DIC_MATR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIC_MATR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MTRIFDIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DIC_MATR','MTRIFDIS');
              +",MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DIC_MATR','MTROWDIS');
                  +i_ccchkf ;
              +" where ";
                  +"MTSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                  +" and MTMAGCAR = "+cp_ToStrODBC(this.w_MVCODMAG);
                     )
            else
              update (i_cTable) set;
                  MTRIFDIS = this.w_MVSERIAL;
                  ,MTROWDIS = this.w_CPROWNUM;
                  &i_ccchkf. ;
               where;
                  MTSERIAL = this.w_DPSERIAL;
                  and MTMAGCAR = this.w_MVCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          if (g_PERLOT="S" AND this.w_FLLOTT<>"N") or (g_PERUBI="S" AND this.w_FLUBIC="S") and this.oParentObject.w_OPERAZ = "SC"
            * --- Inserisco sulle righe delle Dichiarazioni matricole il riferimento al Documento associato
            this.w_DCROWNUM1 = iif(this.w_PDROWNUM=0,this.w_DCROWNUM,this.w_PDROWNUM)
            if this.w_PDROWNUM<>0
              * --- Write into AVA_LOT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.AVA_LOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AVA_LOT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.AVA_LOT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CLRIFDIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'AVA_LOT','CLRIFDIS');
                +",CLROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'AVA_LOT','CLROWDIS');
                    +i_ccchkf ;
                +" where ";
                    +"CLDICHIA = "+cp_ToStrODBC(this.w_DPSERIAL);
                    +" and CLROWODL = "+cp_ToStrODBC(this.w_DCROWNUM1);
                       )
              else
                update (i_cTable) set;
                    CLRIFDIS = this.w_MVSERIAL;
                    ,CLROWDIS = this.w_CPROWNUM;
                    &i_ccchkf. ;
                 where;
                    CLDICHIA = this.w_DPSERIAL;
                    and CLROWODL = this.w_DCROWNUM1;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Per le righe non legate ad ODL (inserite manualmente in maschera dichiarazioni) CLROWODL � sempre 0
              * --- Write into AVA_LOT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.AVA_LOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AVA_LOT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.AVA_LOT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CLRIFDIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'AVA_LOT','CLRIFDIS');
                +",CLROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'AVA_LOT','CLROWDIS');
                    +i_ccchkf ;
                +" where ";
                    +"CLDICHIA = "+cp_ToStrODBC(this.w_DPSERIAL);
                    +" and CLROWCNM = "+cp_ToStrODBC(this.w_DCROWNUM1);
                       )
              else
                update (i_cTable) set;
                    CLRIFDIS = this.w_MVSERIAL;
                    ,CLROWDIS = this.w_CPROWNUM;
                    &i_ccchkf. ;
                 where;
                    CLDICHIA = this.w_DPSERIAL;
                    and CLROWCNM = this.w_DCROWNUM1;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
      endif
      if this.oParentObject.w_OPERAZ $ "CA-SC-SM"
        * --- Aggiornamento saldi Commessa
        if this.w_OK_COMM
          * --- Try
          local bErr_052FD0C0
          bErr_052FD0C0=bTrsErr
          this.Try_052FD0C0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_052FD0C0
          * --- End
          * --- Write into MA_COSTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MA_COSTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MVFLCOCO,'CSCONSUN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
            +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
                +i_ccchkf ;
            +" where ";
                +"CSCODCOM = "+cp_ToStrODBC(this.w_MVCODCOM);
                +" and CSCODMAT = "+cp_ToStrODBC(this.w_MVCODATT);
                +" and CSCODCOS = "+cp_ToStrODBC(this.w_MVCODCOS);
                   )
          else
            update (i_cTable) set;
                CSCONSUN = &i_cOp1.;
                ,CSORDIN = &i_cOp2.;
                &i_ccchkf. ;
             where;
                CSCODCOM = this.w_MVCODCOM;
                and CSCODMAT = this.w_MVCODATT;
                and CSCODCOS = this.w_MVCODCOS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      * --- Aggiorna Saldi
      if this.w_MVQTAUM1<>0 AND NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG)
        if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
          * --- Try
          local bErr_052C74D0
          bErr_052C74D0=bTrsErr
          this.Try_052C74D0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_052C74D0
          * --- End
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_MVKEYSAL;
                and SLCODMAG = this.w_MVCODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (1)'
            return
          endif
          * --- Saldi commessa
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_MVCODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_MVCODCOM
            endif
            * --- Try
            local bErr_052CA440
            bErr_052CA440=bTrsErr
            this.Try_052CA440()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_052CA440
            * --- End
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_MVKEYSAL;
                  and SCCODMAG = this.w_MVCODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDICOM (1)'
              return
            endif
          endif
        endif
        * --- Causale Collegata (DDT di Trasferimento)
        if NOT EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE) AND NOT EMPTY(this.w_MVCODMAT)
          * --- Try
          local bErr_052D4DC0
          bErr_052D4DC0=bTrsErr
          this.Try_052D4DC0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_052D4DC0
          * --- End
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_MVKEYSAL;
                and SLCODMAG = this.w_MVCODMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (2)'
            return
          endif
        endif
        * --- Saldi commessa
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_MVCODCOM
          endif
          * --- Try
          local bErr_052D19A0
          bErr_052D19A0=bTrsErr
          this.Try_052D19A0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_052D19A0
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAT;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDICOM (1)'
            return
          endif
        endif
      endif
      if this.oParentObject.w_OPERAZ = "SC"
        * --- Controllo che la giacenza non sia negativa
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLQTAPER,SLQTRPER"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLQTAPER,SLQTRPER;
            from (i_cTable) where;
                SLCODICE = this.w_MVKEYSAL;
                and SLCODMAG = this.w_MVCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SLQTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
          this.w_SLQTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_SLQTAPER = this.w_SLQTAPER - this.w_SLQTRPER
        if this.w_SLQTAPER < 0
          * --- Read from DIC_PROD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPFLEVAS,DPDATREG"+;
              " from "+i_cTable+" DIC_PROD where ";
                  +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPFLEVAS,DPDATREG;
              from (i_cTable) where;
                  DPSERIAL = this.w_DPSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EVADI = NVL(cp_ToDate(_read_.DPFLEVAS),cp_NullValue(_read_.DPFLEVAS))
            this.w_DPDATREG = NVL(cp_ToDate(_read_.DPDATREG),cp_NullValue(_read_.DPDATREG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_LOggErr = this.w_OLCODODL
          this.oParentObject.w_LErrore = Ah_msgformat("Dichiarazione n. %1 del %2", alltrim(this.w_DPSERIAL),dtoc(cp_todate(this.w_DPDATREG)))
          * --- Incrementa numero errori
          this.oParentObject.w_LNumErr = this.oParentObject.w_LNumErr + 1
          * --- Scrive LOG
          this.oParentObject.w_LTesMes = Ah_msgformat("Articolo %1 (%2) con giacenza negativa (%3) nel magazzino %4",alltrim(this.w_MVCODART), ALLTRIM(this.w_MVDESART), ALLTRIM(STR(this.w_SLQTAPER)), ALLTRIM(this.w_MVCODMAG))
          INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
          (this.oParentObject.w_LNumErr, this.oParentObject.w_LOggErr, this.oParentObject.w_LErrore, this.oParentObject.w_LTesMes)
          this.oParentObject.w_LTesMes = ""
        endif
      endif
    endif
    * --- Setta lo stato dell' ODL a 'L' (Lanciato)
    if this.w_MVTIPRIG="R"
      do case
        case this.oParentObject.w_OPERAZ="OR"
          * --- Generazione OCL
          * --- Aggiorna ODL_MAST (Stato: L)
          * --- Inoltre azzera i flag di Aggiornamento Saldi (l'Ordinato viene trasferito sull'Ordine a Terzista)
          if this.oParentObject.w_MVFLPROV="S"
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("L"),'ODL_MAST','OLTSTATO');
              +",OLTDTLAN ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLTDTLAN');
              +",OLTFLPIA ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLPIA');
              +",OLTFLLAN ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLLAN');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                     )
            else
              update (i_cTable) set;
                  OLTSTATO = "L";
                  ,OLTDTLAN = i_DATSYS;
                  ,OLTFLPIA = " ";
                  ,OLTFLLAN = "+";
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura ODL_MAST'
              return
            endif
          else
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("L"),'ODL_MAST','OLTSTATO');
              +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(null),'ODL_MAST','OLTCAMAG');
              +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLORD');
              +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLIMP');
              +",OLTDTLAN ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLTDTLAN');
              +",OLTFLPIA ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLPIA');
              +",OLTFLLAN ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLLAN');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                     )
            else
              update (i_cTable) set;
                  OLTSTATO = "L";
                  ,OLTCAMAG = null;
                  ,OLTFLORD = " ";
                  ,OLTFLIMP = " ";
                  ,OLTDTLAN = i_DATSYS;
                  ,OLTFLPIA = " ";
                  ,OLTFLLAN = "+";
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura ODL_MAST'
              return
            endif
          endif
          * --- Aggiorna saldi MPS
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTPERAS"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTPERAS;
              from (i_cTable) where;
                  OLCODODL = this.w_OLCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSPIA =FMMPSPIA- "+cp_ToStrODBC(this.w_MVQTAUM1);
            +",FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_MVQTAUM1);
                +i_ccchkf ;
            +" where ";
                +"FMCODART = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                   )
          else
            update (i_cTable) set;
                FMMPSPIA = FMMPSPIA - this.w_MVQTAUM1;
                ,FMMPSLAN = FMMPSLAN + this.w_MVQTAUM1;
                &i_ccchkf. ;
             where;
                FMCODART = this.w_MVKEYSAL;
                and FMPERASS = this.w_PERASS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_PDROWDOC>0
            * --- Insert into RIF_GODL
            i_nConn=i_TableProp[this.RIF_GODL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIF_GODL_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PDSERIAL"+",PDTIPGEN"+",PDCODODL"+",PDROWODL"+",PDSERDOC"+",PDROWDOC"+",PDQTAPRE"+",PDQTAPR1"+",PDFLPREV"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PDSERIAL),'RIF_GODL','PDSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PDTIPGEN),'RIF_GODL','PDTIPGEN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'RIF_GODL','PDCODODL');
              +","+cp_NullLink(cp_ToStrODBC(-1),'RIF_GODL','PDROWODL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PDSERDOC),'RIF_GODL','PDSERDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PDROWDOC),'RIF_GODL','PDROWDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'RIF_GODL','PDQTAPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'RIF_GODL','PDQTAPR1');
              +","+cp_NullLink(cp_ToStrODBC(" "),'RIF_GODL','PDFLPREV');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_PDSERIAL,'PDTIPGEN',this.w_PDTIPGEN,'PDCODODL',this.w_OLCODODL,'PDROWODL',-1,'PDSERDOC',this.w_PDSERDOC,'PDROWDOC',this.w_PDROWDOC,'PDQTAPRE',this.w_MVQTAMOV,'PDQTAPR1',this.w_MVQTAUM1,'PDFLPREV'," ")
              insert into (i_cTable) (PDSERIAL,PDTIPGEN,PDCODODL,PDROWODL,PDSERDOC,PDROWDOC,PDQTAPRE,PDQTAPR1,PDFLPREV &i_ccchkf. );
                 values (;
                   this.w_PDSERIAL;
                   ,this.w_PDTIPGEN;
                   ,this.w_OLCODODL;
                   ,-1;
                   ,this.w_PDSERDOC;
                   ,this.w_PDROWDOC;
                   ,this.w_MVQTAMOV;
                   ,this.w_MVQTAUM1;
                   ," ";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento RIF_GODL'
              return
            endif
          endif
          if this.w_MVQTAUM1<>0 AND NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) and this.oParentObject.w_MVFLPROV<>"S"
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER =SLQTOPER- "+cp_ToStrODBC(this.w_MVQTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = SLQTOPER - this.w_MVQTAUM1;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_MVKEYSAL;
                  and SLCODMAG = this.w_OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDIART (3)'
              return
            endif
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_MVCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_MVCODCOM,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_MVCODCOM
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER =SCQTOPER- "+cp_ToStrODBC(this.w_MVQTAUM1);
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = SCQTOPER - this.w_MVQTAUM1;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_MVKEYSAL;
                    and SCCODMAG = this.w_OLCODMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM (1)'
                return
              endif
            endif
          endif
          if this.w_OLTIPSCL="O"
            this.Pag8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Scrivo sulla testata dell'OCL che il trasferimento � stato genberato dall'ordine a fornitore
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTIPSCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTIPSCL),'ODL_MAST','OLTIPSCL');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                     )
            else
              update (i_cTable) set;
                  OLTIPSCL = this.w_OLTIPSCL;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Contatore ODL Lanciati
          this.oParentObject.w_nRecEla = this.oParentObject.w_nRecEla + 1
        case this.oParentObject.w_OPERAZ $ "DT-BP"
          * --- --------------------
          * --- Generazione DDT di Trasferimento o Buono di Prelievo
          this.w_OQTSAL = 0
          this.w_OLQTMOV = 0
          this.w_OQTUM1 = 0
          this.w_OQTPR1 = 0
          this.w_OKEYSA = SPACE(20)
          this.w_OCODART = SPACE(20)
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTCOMME"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTCOMME;
              from (i_cTable) where;
                  OLCODODL = this.w_OLCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ODL_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLKEYSAL,OLQTASAL,OLQTAUM1,OLQTAPR1,OLQTAEV1,OLFLIMPE,OLFLORDI,OLMAGPRE,OLQTAMOV,OLCODART"+;
              " from "+i_cTable+" ODL_DETT where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLKEYSAL,OLQTASAL,OLQTAUM1,OLQTAPR1,OLQTAEV1,OLFLIMPE,OLFLORDI,OLMAGPRE,OLQTAMOV,OLCODART;
              from (i_cTable) where;
                  OLCODODL = this.w_OLCODODL;
                  and CPROWNUM = this.w_PDROWNUM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OKEYSA = NVL(cp_ToDate(_read_.OLKEYSAL),cp_NullValue(_read_.OLKEYSAL))
            this.w_OQTSAL = NVL(cp_ToDate(_read_.OLQTASAL),cp_NullValue(_read_.OLQTASAL))
            this.w_OQTUM1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
            this.w_OQTPR1 = NVL(cp_ToDate(_read_.OLQTAPR1),cp_NullValue(_read_.OLQTAPR1))
            this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLQTAEV1),cp_NullValue(_read_.OLQTAEV1))
            this.w_OLFLIMPE = NVL(cp_ToDate(_read_.OLFLIMPE),cp_NullValue(_read_.OLFLIMPE))
            this.w_OLFLORDI = NVL(cp_ToDate(_read_.OLFLORDI),cp_NullValue(_read_.OLFLORDI))
            this.w_OLMAGPRE = NVL(cp_ToDate(_read_.OLMAGPRE),cp_NullValue(_read_.OLMAGPRE))
            this.w_OLQTMOV = NVL(cp_ToDate(_read_.OLQTAMOV),cp_NullValue(_read_.OLQTAMOV))
            this.w_OCODART = NVL(cp_ToDate(_read_.OLCODART),cp_NullValue(_read_.OLCODART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_FLORD = iif(this.w_OLFLORDI="+","-",iif(this.w_OLFLORDI="-","+"," "))
          this.w_FLIMP = iif(this.w_OLFLIMPE="+","-",iif(this.w_OLFLIMPE="-","+"," "))
          if this.w_DISMAG="S"
            if this.w_OLCODMAG <> this.w_MVCODMAT
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_OKEYSA;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_OKEYSA;
                      and SCCODMAG = this.w_OLCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
            * --- Scrive sul magazzino WIP
            this.w_FLORD = this.w_OLFLORDI
            this.w_FLIMP = this.w_OLFLIMPE
            if this.w_OLCODMAG <> this.w_MVCODMAT
              * --- Trasferisce l'Impegno se il Magazzino e' Nettificabile
              * --- Try
              local bErr_051F1C70
              bErr_051F1C70=bTrsErr
              this.Try_051F1C70()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_051F1C70
              * --- End
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_MVKEYSAL;
                    and SLCODMAG = this.w_MVCODMAT;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART (5)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_MVCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_MVCODCOM,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_MVCODCOM
                endif
                * --- Try
                local bErr_051F4550
                bErr_051F4550=bTrsErr
                this.Try_051F4550()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_051F4550
                * --- End
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OQTUM1',this.w_OQTUM1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_MVKEYSAL;
                      and SCCODMAG = this.w_MVCODMAT;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
            * --- Aggiorna Dettaglio ODL
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'ODL_DETT','OLCODMAG');
              +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLDTEVCL),'ODL_DETT','OLDTEVCL');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     )
            else
              update (i_cTable) set;
                  OLCODMAG = this.w_MVCODMAT;
                  ,OLDTEVCL = this.w_OLDTEVCL;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;
                  and CPROWNUM = this.w_PDROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Totale dei movimenti di scarico
            do vq_exec with "..\cola\exe\query\gsco_bgd", this, "_Curs_DOC_DETTS"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if used("_Curs_DOC_DETTS")
               select _Curs_DOC_DETTS 
 locate for 1=1
              this.w_QTAMOV = nvl(_Curs_DOC_DETTS.MVQTAMOV,0)
              this.w_QTAUM1 = nvl(_Curs_DOC_DETTS.MVQTAUM1,0)
            endif
            * --- Se il WIP non � nettificabile, al momento del prelievo deve essere stornato l'impegno dalla lista materiali ODL
            * --- Devo verificare le quantit� trasferite
            this.w_oOLQTASAL = this.w_OQTSAL
            this.w_QTASAL = this.w_OQTSAL
            * --- Riga ODL ancora aperta
            this.w_OQTSAL = max(this.w_OQTUM1-max(this.w_MVQTAUM1+this.w_OQTPR1,this.w_QTAUM1),0)
            this.w_QTASAL = max(this.w_oOLQTASAL-this.w_OQTSAL,0)
            * --- Evasione riga ODL
            *     - Nel caso in cui la quantit� del saldo � zero
            *     - Nel caso in cui � l'utente ad accendere manualmente il flag
            if this.w_OQTSAL=0 or this.w_PDFLEVAS="S"
              this.w_OLFLEVAS = "S"
              * --- Evasione manuale azzero la quantit� del saldo
              if this.w_PDFLEVAS="S"
                this.w_OQTSAL = 0
              endif
              this.w_QTAMOV = this.w_OLQTMOV
              this.w_QTAUM1 = this.w_OQTUM1
            else
              this.w_OLFLEVAS = "N"
              this.w_QTAMOV = min(this.w_QTAMOV+nvl(this.w_MVQTAMOV,0),this.w_OLQTMOV)
              this.w_QTAUM1 = min(this.w_QTAUM1+nvl(this.w_MVQTAUM1,0),this.w_OQTUM1)
            endif
            * --- Riassegno w_QTASAL per gestire l'evasione della riga manualmente
            *     w_QTASAL    -> Aggiorna il saldo
            *     w_OQTASAL -> Aggiorna Qta del saldo sull'ODL
            this.w_QTASAL = IIF(this.w_OQTSAL= 0, this.w_oOLQTASAL, this.w_QTASAL)
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'ODL_DETT','OLQTAEVA');
              +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAUM1),'ODL_DETT','OLQTAEV1');
              +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OQTSAL),'ODL_DETT','OLQTASAL');
              +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
              +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLDTEVCL),'ODL_DETT','OLDTEVCL');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     )
            else
              update (i_cTable) set;
                  OLQTAEVA = this.w_QTAMOV;
                  ,OLQTAEV1 = this.w_QTAUM1;
                  ,OLQTASAL = this.w_OQTSAL;
                  ,OLFLEVAS = this.w_OLFLEVAS;
                  ,OLDTEVCL = this.w_OLDTEVCL;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;
                  and CPROWNUM = this.w_PDROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if this.w_QTASAL<>0
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_OKEYSA;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_OKEYSA;
                      and SCCODMAG = this.w_OLCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
          endif
          * --- Aggiorna Qta prelevate
          if this.w_PDFLEVAS="S"
            this.w_FLEVAS = "S"
          else
            this.w_FLEVAS = iif(this.w_OQTPR1+this.w_MVQTAUM1>=this.w_OQTUM1, "S", " ")
          endif
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLQTAPRE =OLQTAPRE+ "+cp_ToStrODBC(this.w_MVQTAMOV);
            +",OLQTAPR1 =OLQTAPR1+ "+cp_ToStrODBC(this.w_MVQTAUM1);
            +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(this.w_FLEVAS),'ODL_DETT','OLFLPREV');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                   )
          else
            update (i_cTable) set;
                OLQTAPRE = OLQTAPRE + this.w_MVQTAMOV;
                ,OLQTAPR1 = OLQTAPR1 + this.w_MVQTAUM1;
                ,OLFLPREV = this.w_FLEVAS;
                &i_ccchkf. ;
             where;
                OLCODODL = this.w_OLCODODL;
                and CPROWNUM = this.w_PDROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if used("_Curs_DOC_DETTS")
            use in _Curs_DOC_DETTS
          endif
          * --- Insert into RIF_GODL
          i_nConn=i_TableProp[this.RIF_GODL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIF_GODL_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PDSERIAL"+",PDTIPGEN"+",PDCODODL"+",PDROWODL"+",PDSERDOC"+",PDROWDOC"+",PDQTAPRE"+",PDQTAPR1"+",PDFLPREV"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PDSERIAL),'RIF_GODL','PDSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PDTIPGEN),'RIF_GODL','PDTIPGEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'RIF_GODL','PDCODODL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PDROWNUM),'RIF_GODL','PDROWODL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PDSERDOC),'RIF_GODL','PDSERDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PDROWDOC),'RIF_GODL','PDROWDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'RIF_GODL','PDQTAPRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'RIF_GODL','PDQTAPR1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PDFLEVAS),'RIF_GODL','PDFLPREV');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_PDSERIAL,'PDTIPGEN',this.w_PDTIPGEN,'PDCODODL',this.w_OLCODODL,'PDROWODL',this.w_PDROWNUM,'PDSERDOC',this.w_PDSERDOC,'PDROWDOC',this.w_PDROWDOC,'PDQTAPRE',this.w_MVQTAMOV,'PDQTAPR1',this.w_MVQTAUM1,'PDFLPREV',this.w_PDFLEVAS)
            insert into (i_cTable) (PDSERIAL,PDTIPGEN,PDCODODL,PDROWODL,PDSERDOC,PDROWDOC,PDQTAPRE,PDQTAPR1,PDFLPREV &i_ccchkf. );
               values (;
                 this.w_PDSERIAL;
                 ,this.w_PDTIPGEN;
                 ,this.w_OLCODODL;
                 ,this.w_PDROWNUM;
                 ,this.w_PDSERDOC;
                 ,this.w_PDROWDOC;
                 ,this.w_MVQTAMOV;
                 ,this.w_MVQTAUM1;
                 ,this.w_PDFLEVAS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Errore in inserimento RIF_GODL'
            return
          endif
          if this.oParentObject.w_OPERAZ = "DT" AND this.w_OLTIPSCL="S"
            * --- Scrivo sulla testata dell'OCL che il trasferimento � stato genberato dall'ordine a fornitore
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTIPSCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTIPSCL),'ODL_MAST','OLTIPSCL');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                     )
            else
              update (i_cTable) set;
                  OLTIPSCL = this.w_OLTIPSCL;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Contatore ODL Lanciati
          this.oParentObject.w_nRecEla = this.oParentObject.w_nRecEla + 1
          if this.w_MVQTAUM1=0 AND this.w_MVTIPRIG<>"D"
            * --- Segnala ODL/OCL non Prelevato
            * --- Incrementa numero errori
            this.oParentObject.w_LNumErr = this.oParentObject.w_LNumErr + 1
            * --- Scrive LOG
            this.oParentObject.w_LOggErr = this.w_OLCODODL
            this.oParentObject.w_LErrore = ah_Msgformat("Nessun prelievo effettuato per il codice: %1", ALLTRIM(this.w_MVCODICE) )
            this.oParentObject.w_LTesMes = ah_Msgformat("Il magazzino di prelievo non ha sufficiente disponibilit�%0o il magazzino di destinazione ha gi� sufficiente disponibilit�%0***** Solo warning")
            INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
            (this.oParentObject.w_LNumErr, this.oParentObject.w_LOggErr, this.oParentObject.w_LErrore, this.oParentObject.w_LTesMes)
          endif
        case this.oParentObject.w_OPERAZ="CA"
          * --- Carico da Produzione 
          this.w_OQTOR1 = 0
          this.w_OQTEV1 = 0
          this.w_OQTSAL = 0
          this.w_NQTSAL = 0
          this.w_OKEYSA = SPACE(20)
          this.w_ODTINI = cp_CharToDate("  -  -  ")
          this.w_ODTFIN = cp_CharToDate("  -  -  ")
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOART,OLTCOMME,OLTULFAS,OLTSEODL,OLTFAODL"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOART,OLTCOMME,OLTULFAS,OLTSEODL,OLTFAODL;
              from (i_cTable) where;
                  OLCODODL = this.w_OLCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
            this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
            this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
            this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
            this.w_ODTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
            this.w_ODTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
            this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
            this.w_OCODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
            this.w_OCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
            this.w_ULTFAS = NVL(cp_ToDate(_read_.OLTULFAS),cp_NullValue(_read_.OLTULFAS))
            this.w_OLTSEODL = NVL(cp_ToDate(_read_.OLTSEODL),cp_NullValue(_read_.OLTSEODL))
            this.w_OLTFAODL = NVL(cp_ToDate(_read_.OLTFAODL),cp_NullValue(_read_.OLTFAODL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_DPQTAPRO = 0
          this.w_DPQTAPR1 = 0
          this.w_DPQTASCA = 0
          this.w_DPQTASC1 = 0
          do case
            case this.w_PDROWNUM=1
              this.w_DPQTAPRO = this.w_MVQTAMOV
              this.w_DPQTAPR1 = this.w_MVQTAUM1
            case this.w_PDROWNUM=2
              this.w_DPQTASCA = this.w_MVQTAMOV
              this.w_DPQTASC1 = this.w_MVQTAUM1
          endcase
          * --- Verifico se � una dichiarazione che arriva dal modulo (Produzione funzioni avanzate)
          this.w_CICLI = !Empty(this.w_OLTSEODL) and this.w_OLTFAODL>0
          if this.w_CICLI
            * --- Read from ODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLAVAUM1,CLQTAAVA,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1,CLROWORD"+;
                " from "+i_cTable+" ODL_CICL where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_OLTFAODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLAVAUM1,CLQTAAVA,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1,CLROWORD;
                from (i_cTable) where;
                    CLCODODL = this.w_OLTSEODL;
                    and CPROWNUM = this.w_OLTFAODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLAVAUM1 = NVL(cp_ToDate(_read_.CLAVAUM1),cp_NullValue(_read_.CLAVAUM1))
              this.w_CLQTAAVA = NVL(cp_ToDate(_read_.CLQTAAVA),cp_NullValue(_read_.CLQTAAVA))
              this.w_CLQTASCA = NVL(cp_ToDate(_read_.CLQTASCA),cp_NullValue(_read_.CLQTASCA))
              this.w_CLSCAUM1 = NVL(cp_ToDate(_read_.CLSCAUM1),cp_NullValue(_read_.CLSCAUM1))
              this.w_CLQTADIC = NVL(cp_ToDate(_read_.CLQTADIC),cp_NullValue(_read_.CLQTADIC))
              this.w_CLDICUM1 = NVL(cp_ToDate(_read_.CLDICUM1),cp_NullValue(_read_.CLDICUM1))
              this.w_FASE = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Imposto il flag evaso
            * --- Select from DIC_PROD
            i_nConn=i_TableProp[this.DIC_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select DPCODODL, SUM(DPQTASC1+DPQTAPR1) AS RDQTAAV1  from "+i_cTable+" DIC_PROD ";
                  +" where DPCODODL="+cp_ToStrODBC(this.w_OLCODODL)+"";
                  +" group by DPCODODL";
                   ,"_Curs_DIC_PROD")
            else
              select DPCODODL, SUM(DPQTASC1+DPQTAPR1) AS RDQTAAV1 from (i_cTable);
               where DPCODODL=this.w_OLCODODL;
               group by DPCODODL;
                into cursor _Curs_DIC_PROD
            endif
            if used('_Curs_DIC_PROD')
              select _Curs_DIC_PROD
              locate for 1=1
              do while not(eof())
              * --- Quantit� totale dichiarata (compresa ultima)
              this.w_DPDICUM1 = NVL(_Curs_DIC_PROD.RDQTAAV1,0)
                select _Curs_DIC_PROD
                continue
              enddo
              use
            endif
            this.w_CLFASEVA = IIF(this.w_PDFLEVAS="S" or this.w_OQTOR1 <= this.w_DPDICUM1 , "S", "N")
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLQTAAVA =CLQTAAVA+ "+cp_ToStrODBC(this.w_DPQTAPRO);
              +",CLAVAUM1 =CLAVAUM1+ "+cp_ToStrODBC(this.w_DPQTAPR1);
              +",CLQTASCA =CLQTASCA+ "+cp_ToStrODBC(this.w_DPQTASCA);
              +",CLSCAUM1 =CLSCAUM1+ "+cp_ToStrODBC(this.w_DPQTASC1);
              +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLFASEVA),'ODL_CICL','CLFASEVA');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                  +" and CLBFRIFE = "+cp_ToStrODBC(this.w_FASE);
                     )
            else
              update (i_cTable) set;
                  CLQTAAVA = CLQTAAVA + this.w_DPQTAPRO;
                  ,CLAVAUM1 = CLAVAUM1 + this.w_DPQTAPR1;
                  ,CLQTASCA = CLQTASCA + this.w_DPQTASCA;
                  ,CLSCAUM1 = CLSCAUM1 + this.w_DPQTASC1;
                  ,CLFASEVA = this.w_CLFASEVA;
                  &i_ccchkf. ;
               where;
                  CLCODODL = this.w_OLTSEODL;
                  and CLBFRIFE = this.w_FASE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if this.w_CLFASEVA="S"
              * --- Ripristina la quantita dichiarabile
              * --- Write into ODL_CICL
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CLQTADIC ="+cp_NullLink(cp_ToStrODBC(this.w_CLQTAAVA+this.w_CLQTASCA),'ODL_CICL','CLQTADIC');
                +",CLDICUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_CLAVAUM1+this.w_CLSCAUM1),'ODL_CICL','CLDICUM1');
                    +i_ccchkf ;
                +" where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                    +" and CLROWORD > "+cp_ToStrODBC(this.w_FASE);
                       )
              else
                update (i_cTable) set;
                    CLQTADIC = this.w_CLQTAAVA+this.w_CLQTASCA;
                    ,CLDICUM1 = this.w_CLAVAUM1+this.w_CLSCAUM1;
                    &i_ccchkf. ;
                 where;
                    CLCODODL = this.w_OLTSEODL;
                    and CLROWORD > this.w_FASE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            if this.w_PDFLEVAS<>"S" AND this.w_CLFASEVA="S"
              this.w_PDFLEVAS = this.w_CLFASEVA
            endif
          endif
          this.w_OQTEV1 = this.w_OQTEV1 + this.w_OLQTAUM1
          * --- Aggiorna Qta Evasa sull ODL
          this.w_NQTSAL = IIF(this.w_PDFLEVAS="S", 0, MAX(this.w_OQTOR1-this.w_OQTEV1, 0))
          * --- Aggiorna lo Stato "Finito"
          this.w_OSTATO = IIF(this.w_NQTSAL=0, "F", "L")
          this.w_ODTINI = IIF(EMPTY(this.w_ODTINI), this.w_MVDATDOC, this.w_ODTINI)
          this.w_ODTFIN = IIF(this.w_OSTATO="F", this.w_MVDATDOC, cp_CharToDate("  -  -  "))
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTQTOEV =OLTQTOEV+ "+cp_ToStrODBC(this.w_OLQTAMOV);
            +",OLTQTOE1 =OLTQTOE1+ "+cp_ToStrODBC(this.w_OLQTAUM1);
            +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(this.w_PDFLEVAS),'ODL_MAST','OLTFLEVA');
            +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTSAL),'ODL_MAST','OLTQTSAL');
            +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OSTATO),'ODL_MAST','OLTSTATO');
            +",OLTDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_ODTINI),'ODL_MAST','OLTDTINI');
            +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
            +",OLTQTPRO =OLTQTPRO+ "+cp_ToStrODBC(this.w_DPQTAPRO);
            +",OLTQTPR1 =OLTQTPR1+ "+cp_ToStrODBC(this.w_DPQTAPR1);
            +",OLTQTOSC =OLTQTOSC+ "+cp_ToStrODBC(this.w_DPQTASCA);
            +",OLTQTOS1 =OLTQTOS1+ "+cp_ToStrODBC(this.w_DPQTASC1);
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                   )
          else
            update (i_cTable) set;
                OLTQTOEV = OLTQTOEV + this.w_OLQTAMOV;
                ,OLTQTOE1 = OLTQTOE1 + this.w_OLQTAUM1;
                ,OLTFLEVA = this.w_PDFLEVAS;
                ,OLTQTSAL = this.w_NQTSAL;
                ,OLTSTATO = this.w_OSTATO;
                ,OLTDTINI = this.w_ODTINI;
                ,OLTDTFIN = this.w_ODTFIN;
                ,OLTQTPRO = OLTQTPRO + this.w_DPQTAPRO;
                ,OLTQTPR1 = OLTQTPR1 + this.w_DPQTAPR1;
                ,OLTQTOSC = OLTQTOSC + this.w_DPQTASCA;
                ,OLTQTOS1 = OLTQTOS1 + this.w_DPQTASC1;
                &i_ccchkf. ;
             where;
                OLCODODL = this.w_OLCODODL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura ODL_MAST'
            return
          endif
          * --- Aggiorna saldi MPS
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_OLQTAUM1);
            +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_OLQTAUM1);
                +i_ccchkf ;
            +" where ";
                +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                   )
          else
            update (i_cTable) set;
                FMMPSLAN = FMMPSLAN - this.w_OLQTAUM1;
                ,FMMPSTOT = FMMPSTOT - this.w_OLQTAUM1;
                &i_ccchkf. ;
             where;
                FMCODART = this.w_OKEYSA;
                and FMPERASS = this.w_PERASS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Storna Ordinato dai Saldi
          this.w_NQTSAL = this.w_NQTSAL - this.w_OQTSAL 
          if this.w_NQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OLCODMAG)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = SLQTOPER + this.w_NQTSAL;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OKEYSA;
                  and SLCODMAG = this.w_OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDIART (3)'
              return
            endif
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_OCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_OCOMME,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_OCOMME
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = SCQTOPER + this.w_NQTSAL;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OKEYSA;
                    and SCCODMAG = this.w_OLCODMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM (1)'
                return
              endif
            endif
          endif
          if this.w_CICLI and this.w_ULTFAS="S"
            * --- Carico da Produzione 
            this.w_OQTOR1 = 0
            this.w_OQTEV1 = 0
            this.w_OQTSAL = 0
            this.w_NQTSAL = 0
            this.w_OKEYSA = SPACE(20)
            this.w_ODTINI = cp_CharToDate("  -  -  ")
            this.w_ODTFIN = cp_CharToDate("  -  -  ")
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOART,OLTCOMME"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOART,OLTCOMME;
                from (i_cTable) where;
                    OLCODODL = this.w_OLTSEODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
              this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
              this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
              this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
              this.w_ODTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
              this.w_ODTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
              this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
              this.w_OCODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
              this.w_OCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OQTEV1 = this.w_OQTEV1 + this.w_OLQTAUM1
            * --- Aggiorna Qta Evasa sull ODL
            this.w_NQTSAL = IIF(this.w_PDFLEVAS="S", 0, MAX(this.w_OQTOR1-this.w_OQTEV1, 0))
            * --- Aggiorna lo Stato "Finito"
            this.w_OSTATO = IIF(this.w_NQTSAL=0, "F", "L")
            this.w_ODTINI = IIF(EMPTY(this.w_ODTINI), this.w_MVDATDOC, this.w_ODTINI)
            this.w_ODTFIN = IIF(this.w_OSTATO="F", this.w_MVDATDOC, cp_CharToDate("  -  -  "))
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTQTOEV =OLTQTOEV+ "+cp_ToStrODBC(this.w_OLQTAMOV);
              +",OLTQTOE1 =OLTQTOE1+ "+cp_ToStrODBC(this.w_OLQTAUM1);
              +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(this.w_PDFLEVAS),'ODL_MAST','OLTFLEVA');
              +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTSAL),'ODL_MAST','OLTQTSAL');
              +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OSTATO),'ODL_MAST','OLTSTATO');
              +",OLTDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_ODTINI),'ODL_MAST','OLTDTINI');
              +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
              +",OLTQTPRO =OLTQTPRO+ "+cp_ToStrODBC(this.w_DPQTAPRO);
              +",OLTQTPR1 =OLTQTPR1+ "+cp_ToStrODBC(this.w_DPQTAPR1);
              +",OLTQTOSC =OLTQTOSC+ "+cp_ToStrODBC(this.w_DPQTASCA);
              +",OLTQTOS1 =OLTQTOS1+ "+cp_ToStrODBC(this.w_DPQTASC1);
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                     )
            else
              update (i_cTable) set;
                  OLTQTOEV = OLTQTOEV + this.w_OLQTAMOV;
                  ,OLTQTOE1 = OLTQTOE1 + this.w_OLQTAUM1;
                  ,OLTFLEVA = this.w_PDFLEVAS;
                  ,OLTQTSAL = this.w_NQTSAL;
                  ,OLTSTATO = this.w_OSTATO;
                  ,OLTDTINI = this.w_ODTINI;
                  ,OLTDTFIN = this.w_ODTFIN;
                  ,OLTQTPRO = OLTQTPRO + this.w_DPQTAPRO;
                  ,OLTQTPR1 = OLTQTPR1 + this.w_DPQTAPR1;
                  ,OLTQTOSC = OLTQTOSC + this.w_DPQTASCA;
                  ,OLTQTOS1 = OLTQTOS1 + this.w_DPQTASC1;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLTSEODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura ODL_MAST'
              return
            endif
            * --- Aggiorna saldi MPS
            * --- Write into MPS_TFOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_OLQTAUM1);
              +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_OLQTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                  +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                     )
            else
              update (i_cTable) set;
                  FMMPSLAN = FMMPSLAN - this.w_OLQTAUM1;
                  ,FMMPSTOT = FMMPSTOT - this.w_OLQTAUM1;
                  &i_ccchkf. ;
               where;
                  FMCODART = this.w_OKEYSA;
                  and FMPERASS = this.w_PERASS;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Storna Ordinato dai Saldi
            this.w_NQTSAL = this.w_NQTSAL - this.w_OQTSAL 
            if this.w_NQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OLCODMAG)
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = SLQTOPER + this.w_NQTSAL;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_OKEYSA;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART (3)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = SCQTOPER + this.w_NQTSAL;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_OKEYSA;
                      and SCCODMAG = this.w_OLCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
          endif
          this.oParentObject.w_nRecEla = this.oParentObject.w_nRecEla + 1
        case this.oParentObject.w_OPERAZ $ "SC-SM-CS"
          * --- Scarico Materiali per Produzione 
          * --- Scarico da Rientro Materiali per C/Lavoro
          this.w_OQTOR1 = 0
          this.w_OQTEV1 = 0
          this.w_OQTSAL = 0
          this.w_NQTSAL = 0
          this.w_OKEYSA = SPACE(20)
          this.w_MPQTAEVA = 0
          this.w_MPQTAEV1 = 0
          this.w_OLFLEVAS = SPACE(1)
          this.w_FLORD = SPACE(1)
          this.w_FLIMP = SPACE(1)
          this.w_EVADI = SPACE(1)
          this.w_OLQTASAL = 0
          if this.oParentObject.w_OPERAZ = "SC"
            * --- Read from DIC_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIC_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DPFLEVAS"+;
                " from "+i_cTable+" DIC_PROD where ";
                    +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DPFLEVAS;
                from (i_cTable) where;
                    DPSERIAL = this.w_DPSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_EVADI = NVL(cp_ToDate(_read_.DPFLEVAS),cp_NullValue(_read_.DPFLEVAS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from MAT_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAT_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MPQTAEVA,MPQTAEV1,MPFLEVAS"+;
                " from "+i_cTable+" MAT_PROD where ";
                    +"MPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_DCROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MPQTAEVA,MPQTAEV1,MPFLEVAS;
                from (i_cTable) where;
                    MPSERIAL = this.w_DPSERIAL;
                    and CPROWNUM = this.w_DCROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MPQTAEVA = NVL(cp_ToDate(_read_.MPQTAEVA),cp_NullValue(_read_.MPQTAEVA))
              this.w_MPQTAEV1 = NVL(cp_ToDate(_read_.MPQTAEV1),cp_NullValue(_read_.MPQTAEV1))
              this.w_MPFLEVAS = NVL(cp_ToDate(_read_.MPFLEVAS),cp_NullValue(_read_.MPFLEVAS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from ODL_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLQTAUM1,OLQTAEV1,OLKEYSAL,OLQTASAL,OLFLEVAS,OLFLIMPE,OLFLORDI,OLCODMAG,OLCODART"+;
                " from "+i_cTable+" ODL_DETT where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLQTAUM1,OLQTAEV1,OLKEYSAL,OLQTASAL,OLFLEVAS,OLFLIMPE,OLFLORDI,OLCODMAG,OLCODART;
                from (i_cTable) where;
                    OLCODODL = this.w_OLCODODL;
                    and CPROWNUM = this.w_PDROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
              this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLQTAEV1),cp_NullValue(_read_.OLQTAEV1))
              this.w_OKEYSA = NVL(cp_ToDate(_read_.OLKEYSAL),cp_NullValue(_read_.OLKEYSAL))
              this.w_OQTSAL = NVL(cp_ToDate(_read_.OLQTASAL),cp_NullValue(_read_.OLQTASAL))
              this.w_OLFLEVAS = NVL(cp_ToDate(_read_.OLFLEVAS),cp_NullValue(_read_.OLFLEVAS))
              this.w_FLIMP = NVL(cp_ToDate(_read_.OLFLIMPE),cp_NullValue(_read_.OLFLIMPE))
              this.w_FLORD = NVL(cp_ToDate(_read_.OLFLORDI),cp_NullValue(_read_.OLFLORDI))
              this.w_OLCODMAG = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
              this.w_OCODART = NVL(cp_ToDate(_read_.OLCODART),cp_NullValue(_read_.OLCODART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTCOMME"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTCOMME;
                from (i_cTable) where;
                    OLCODODL = this.w_OLCODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FLORD = iif(this.w_FLORD="+","-",iif(this.w_FLORD="-","+"," "))
            this.w_FLIMP = iif(this.w_FLIMP="+","-",iif(this.w_FLIMP="-","+"," "))
            if this.w_EVADI="S" or this.w_MPQTAEV1 >= this.w_OQTSAL or this.w_MPFLEVAS="S"
              this.w_OLFLEVAS = "S"
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLQTAEVA =OLQTAEVA+ "+cp_ToStrODBC(this.w_MPQTAEVA);
                +",OLQTAEV1 =OLQTAEV1+ "+cp_ToStrODBC(this.w_MPQTAEV1);
                +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
                +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                       )
              else
                update (i_cTable) set;
                    OLQTAEVA = OLQTAEVA + this.w_MPQTAEVA;
                    ,OLQTAEV1 = OLQTAEV1 + this.w_MPQTAEV1;
                    ,OLFLEVAS = this.w_OLFLEVAS;
                    ,OLQTASAL = 0;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_OLCODODL;
                    and CPROWNUM = this.w_PDROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_OKEYSA;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_OKEYSA;
                      and SCCODMAG = this.w_OLCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            else
              this.w_OQTSAL = this.w_MPQTAEV1
              this.w_OLFLEVAS = IIF(this.w_OLFLEVAS="S", "S", IIF(this.w_PDFLEVAS<> "S", "N", "S"))
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLQTAEVA =OLQTAEVA+ "+cp_ToStrODBC(this.w_MPQTAEVA);
                +",OLQTAEV1 =OLQTAEV1+ "+cp_ToStrODBC(this.w_MPQTAEV1);
                +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
                +",OLQTASAL =OLQTASAL- "+cp_ToStrODBC(this.w_OQTSAL);
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                       )
              else
                update (i_cTable) set;
                    OLQTAEVA = OLQTAEVA + this.w_MPQTAEVA;
                    ,OLQTAEV1 = OLQTAEV1 + this.w_MPQTAEV1;
                    ,OLFLEVAS = this.w_OLFLEVAS;
                    ,OLQTASAL = OLQTASAL - this.w_OQTSAL;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_OLCODODL;
                    and CPROWNUM = this.w_PDROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_OKEYSA;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_OKEYSA;
                      and SCCODMAG = this.w_OLCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
          else
            this.w_SERDT = this.oParentObject.w_SERIAL
            this.w_ROWORD = this.oParentObject.w_ROWORD
            * --- Read from ODL_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLQTAUM1,OLQTAEV1,OLKEYSAL,OLQTASAL,OLFLEVAS,OLCODMAG,OLCODART"+;
                " from "+i_cTable+" ODL_DETT where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLQTAUM1,OLQTAEV1,OLKEYSAL,OLQTASAL,OLFLEVAS,OLCODMAG,OLCODART;
                from (i_cTable) where;
                    OLCODODL = this.w_OLCODODL;
                    and CPROWNUM = this.w_PDROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
              this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLQTAEV1),cp_NullValue(_read_.OLQTAEV1))
              this.w_OKEYSA = NVL(cp_ToDate(_read_.OLKEYSAL),cp_NullValue(_read_.OLKEYSAL))
              this.w_OQTSAL = NVL(cp_ToDate(_read_.OLQTASAL),cp_NullValue(_read_.OLQTASAL))
              this.w_OLFLEVAS = NVL(cp_ToDate(_read_.OLFLEVAS),cp_NullValue(_read_.OLFLEVAS))
              this.w_MAGIMP = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
              this.w_OCODART = NVL(cp_ToDate(_read_.OLCODART),cp_NullValue(_read_.OLCODART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTCOMME"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTCOMME;
                from (i_cTable) where;
                    OLCODODL = this.w_OLCODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Recupero la quantit� da evadere dall'eventuale consuntivazione materiali
            * --- Read from MAT_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAT_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MPQTAEV1,MPQTAEVA,MPFLEVAS"+;
                " from "+i_cTable+" MAT_PROD where ";
                    +"MPSERIAL = "+cp_ToStrODBC(this.w_SERDT);
                    +" and MPROWDOC = "+cp_ToStrODBC(this.w_ROWORD);
                    +" and MPROWODL = "+cp_ToStrODBC(this.w_PDROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MPQTAEV1,MPQTAEVA,MPFLEVAS;
                from (i_cTable) where;
                    MPSERIAL = this.w_SERDT;
                    and MPROWDOC = this.w_ROWORD;
                    and MPROWODL = this.w_PDROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MATQTAUM1 = NVL(cp_ToDate(_read_.MPQTAEV1),cp_NullValue(_read_.MPQTAEV1))
              this.w_MATQTAMOV = NVL(cp_ToDate(_read_.MPQTAEVA),cp_NullValue(_read_.MPQTAEVA))
              this.w_FLEVAS = NVL(cp_ToDate(_read_.MPFLEVAS),cp_NullValue(_read_.MPFLEVAS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OLQTAUM1 = MAX(this.w_MATQTAUM1,this.w_OLQTAUM1)
            this.w_OLQTAMOV = MAX(this.w_MATQTAMOV,this.w_OLQTAMOV)
            this.w_OQTEV1 = this.w_OQTEV1 + this.w_OLQTAUM1
            this.w_NQTSAL = IIF(this.w_PDFLEVAS="S", 0, MAX(this.w_OQTOR1-this.w_OQTEV1, 0))
            if empty(this.w_FLEVAS)
              this.w_FLEVAS = iif(this.w_OLQTAUM1>=this.w_OQTSAL,"S","")
            endif
            * --- Aggiorna Qta Evasa sull ODL
            if this.w_OLFLEVAS<>"S"
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLQTAEVA =OLQTAEVA+ "+cp_ToStrODBC(this.w_OLQTAMOV);
                +",OLQTAEV1 =OLQTAEV1+ "+cp_ToStrODBC(this.w_OLQTAUM1);
                +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_FLEVAS),'ODL_DETT','OLFLEVAS');
                +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTSAL),'ODL_DETT','OLQTASAL');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                       )
              else
                update (i_cTable) set;
                    OLQTAEVA = OLQTAEVA + this.w_OLQTAMOV;
                    ,OLQTAEV1 = OLQTAEV1 + this.w_OLQTAUM1;
                    ,OLFLEVAS = this.w_FLEVAS;
                    ,OLQTASAL = this.w_NQTSAL;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_OLCODODL;
                    and CPROWNUM = this.w_PDROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in Scrittura ODL_DETT'
                return
              endif
            endif
            * --- Se Nettificabile, Storna Impegnato dai Saldi
            this.w_NQTSAL = this.w_NQTSAL - this.w_OQTSAL 
            if this.w_DISMAG="S" AND this.w_NQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OLCODMAG)
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                       )
              else
                update (i_cTable) set;
                    SLQTIPER = SLQTIPER + this.w_NQTSAL;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_OKEYSA;
                    and SLCODMAG = this.w_MAGIMP;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in Scrittura SALDIART (3)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTIPER = SCQTIPER + this.w_NQTSAL;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_OKEYSA;
                      and SCCODMAG = this.w_MAGIMP;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
          endif
          this.oParentObject.w_nRecEla = this.oParentObject.w_nRecEla + 1
        case this.oParentObject.w_OPERAZ = "RI"
          * --- Verifico se � una dichiarazione che arriva dal modulo (Produzione funzioni avanzate)
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTSEODL,OLTFAODL"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTSEODL,OLTFAODL;
              from (i_cTable) where;
                  OLCODODL = this.w_OLCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OLTSEODL = NVL(cp_ToDate(_read_.OLTSEODL),cp_NullValue(_read_.OLTSEODL))
            this.w_OLTFAODL = NVL(cp_ToDate(_read_.OLTFAODL),cp_NullValue(_read_.OLTFAODL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CICLI = !Empty(this.w_OLTSEODL) and this.w_OLTFAODL>0
          if this.w_CICLI
            * --- Write into ODL_RISF
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_RISF_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_RISF_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RFTCONSS =RFTCONSS+ "+cp_ToStrODBC(this.w_OLQTAUM1);
              +",RFTCORIS =RFTCORIS+ "+cp_ToStrODBC(this.w_OLQTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"RFCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                  +" and RFRIFFAS = "+cp_ToStrODBC(this.w_PDFASPER);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     )
            else
              update (i_cTable) set;
                  RFTCONSS = RFTCONSS + this.w_OLQTAUM1;
                  ,RFTCORIS = RFTCORIS + this.w_OLQTAUM1;
                  &i_ccchkf. ;
               where;
                  RFCODODL = this.w_OLCODODL;
                  and RFRIFFAS = this.w_PDFASPER;
                  and CPROWNUM = this.w_PDROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Write into ODL_RISO
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_RISO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_RISO_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RLTCONSS =RLTCONSS+ "+cp_ToStrODBC(this.w_OLQTAUM1);
              +",RLTCORIS =RLTCORIS+ "+cp_ToStrODBC(this.w_OLQTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"RLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                  +" and RLROWNUM = "+cp_ToStrODBC(this.w_PDFASPER);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     )
            else
              update (i_cTable) set;
                  RLTCONSS = RLTCONSS + this.w_OLQTAUM1;
                  ,RLTCORIS = RLTCORIS + this.w_OLQTAUM1;
                  &i_ccchkf. ;
               where;
                  RLCODODL = this.w_OLTSEODL;
                  and RLROWNUM = this.w_PDFASPER;
                  and CPROWNUM = this.w_PDROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
      endcase
    endif
    SELECT GeneApp
    ENDSCAN
    if this.oParentObject.w_OPERAZ="SC" and this.w_EVADI="S"
      * --- Devo aggiornare la qta saldo e impostare il flag evaso sui materiali dell'ODL 
      *     che non sono stati scaricati dalla dichiarazione
      * --- Read from DIC_PROD
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIC_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCODODL"+;
          " from "+i_cTable+" DIC_PROD where ";
              +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCODODL;
          from (i_cTable) where;
              DPSERIAL = this.w_DPSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OLCODODL = NVL(cp_ToDate(_read_.DPCODODL),cp_NullValue(_read_.DPCODODL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DELETING = "N"
      this.w_SDIC = this.w_DPSERIAL
      * --- Select from gsco2bgd
      do vq_exec with 'gsco2bgd',this,'_Curs_gsco2bgd','',.f.,.t.
      if used('_Curs_gsco2bgd')
        select _Curs_gsco2bgd
        locate for 1=1
        do while not(eof())
        this.w_PDROWNUM = _Curs_gsco2bgd.CPROWNUM
        this.w_OQTSAL = _Curs_gsco2bgd.OLQTASAL
        this.w_FLIMP = _Curs_gsco2bgd.OLFLIMPE
        this.w_FLORD = _Curs_gsco2bgd.OLFLORDI
        this.w_OLCODMAG = _Curs_gsco2bgd.OLCODMAG
        this.w_KEYSAL = _Curs_gsco2bgd.OLKEYSAL
        this.w_OCODART = _Curs_gsco2bgd.OLCODART
        this.w_OCOMME = _Curs_gsco2bgd.OLTCOMME
        this.w_OLFLEVAS = "S"
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
          +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                 )
        else
          update (i_cTable) set;
              OLFLEVAS = this.w_OLFLEVAS;
              ,OLQTASAL = 0;
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_OLCODODL;
              and CPROWNUM = this.w_PDROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Try
        local bErr_0518ADC0
        bErr_0518ADC0=bTrsErr
        this.Try_0518ADC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0518ADC0
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_KEYSAL;
              and SLCODMAG = this.w_OLCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_OCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_OCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_OCOMME,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_OCOMME
          endif
          * --- Try
          local bErr_0517D290
          bErr_0517D290=bTrsErr
          this.Try_0517D290()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0517D290
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                ,SCQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_KEYSAL;
                and SCCODMAG = this.w_OLCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDICOM (1)'
            return
          endif
        endif
          select _Curs_gsco2bgd
          continue
        enddo
        use
      endif
    endif
    if this.w_INSROW<>0
      * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
      FOR l_i=1 TO (alen(DR,1)-1)
      if NOT EMPTY(DR[l_i, 1]) AND NOT EMPTY(DR[l_i, 2])
        this.w_RSNUMRAT = l_i
        this.w_RSDATRAT = DR[l_i, 1]
        this.w_RSIMPRAT = DR[l_i, 2]
        this.w_RSMODPAG = DR[l_i, 3]
        * --- Scrive Doc_Rate
        * --- Insert into DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_RATE','RSSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_MVSERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ")
          insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP &i_ccchkf. );
             values (;
               this.w_MVSERIAL;
               ,this.w_RSNUMRAT;
               ,this.w_RSDATRAT;
               ,this.w_RSIMPRAT;
               ,this.w_RSMODPAG;
               ," ";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in inserimento DOC_RATE'
          return
        endif
      endif
      NEXT
      do case
        case this.oParentObject.w_OPERAZ="CA"
          * --- Scrive Riferimento su Dichiarazione di Produzione
          * --- Write into DIC_PROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_PROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DPRIFCAR ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DIC_PROD','DPRIFCAR');
                +i_ccchkf ;
            +" where ";
                +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                   )
          else
            update (i_cTable) set;
                DPRIFCAR = this.w_MVSERIAL;
                &i_ccchkf. ;
             where;
                DPSERIAL = this.w_DPSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura DIC_PROD (1)'
            return
          endif
        case this.oParentObject.w_OPERAZ="SC"
          * --- Scrive Riferimento su Dichiarazione di Produzione
          * --- Write into DIC_PROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_PROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DPRIFSCA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DIC_PROD','DPRIFSCA');
                +i_ccchkf ;
            +" where ";
                +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                   )
          else
            update (i_cTable) set;
                DPRIFSCA = this.w_MVSERIAL;
                &i_ccchkf. ;
             where;
                DPSERIAL = this.w_DPSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura DIC_PROD (2)'
            return
          endif
        case this.oParentObject.w_OPERAZ="RI"
          * --- Scrive Riferimento su Dichiarazione di Produzione
          * --- Write into DIC_PROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_PROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DPRIFSER ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DIC_PROD','DPRIFSER');
                +i_ccchkf ;
            +" where ";
                +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                   )
          else
            update (i_cTable) set;
                DPRIFSER = this.w_MVSERIAL;
                &i_ccchkf. ;
             where;
                DPSERIAL = this.w_DPSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura DIC_PROD (2)'
            return
          endif
        case this.oParentObject.w_OPERAZ="MO"
          * --- Scrive Riferimento su Dichiarazione di Produzione
          * --- Write into DIC_PROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_PROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DPRIFMOU ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DIC_PROD','DPRIFMOU');
                +i_ccchkf ;
            +" where ";
                +"DPSERIAL = "+cp_ToStrODBC(this.w_DPSERIAL);
                   )
          else
            update (i_cTable) set;
                DPRIFMOU = this.w_MVSERIAL;
                &i_ccchkf. ;
             where;
                DPSERIAL = this.w_DPSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura DIC_PROD (2)'
            return
          endif
      endcase
      * --- Riferimento al Piano di Generazione
      this.oParentObject.w_nRecDoc = this.oParentObject.w_nRecDoc + 1
    endif
    if this.oParentObject.w_OPERAZ<>"SM" AND this.oParentObject.w_OPERAZ<>"CS"
      * --- commit
      cp_EndTrs(.t.)
    endif
    * --- NUMDOC azzerato per evitare che i documenti succerssivi vengano 'Forzati'
    this.oParentObject.w_MVNUMDOC = 0
    return
  proc Try_052FD0C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOS),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_MVCODCOM,'CSTIPSTR',"A",'CSCODMAT',this.w_MVCODATT,'CSCODCOS',this.w_MVCODCOS)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           this.w_MVCODCOM;
           ,"A";
           ,this.w_MVCODATT;
           ,this.w_MVCODCOS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_052C74D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDIART (1)'
      return
    endif
    return
  proc Try_052CA440()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDICOM (1)'
      return
    endif
    return
  proc Try_052D4DC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAT)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_052D19A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAT,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAT;
           ,this.w_COMMAPPO;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDICOM (1)'
      return
    endif
    return
  proc Try_051F1C70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAT)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_051F4550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAT,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAT;
           ,this.w_COMMAPPO;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDICOM (1)'
      return
    endif
    return
  proc Try_0518ADC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_KEYSAL,'SLCODMAG',this.w_OLCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,this.w_OLCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDIART (1)'
      return
    endif
    return
  proc Try_0517D290()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_KEYSAL,'SCCODMAG',this.w_OLCODMAG,'SCCODCAN',this.w_COMMAPPO)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,this.w_OLCODMAG;
           ,this.w_COMMAPPO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDICOM (1)'
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Prezzo e Sconti
    if this.oParentObject.w_OPERAZ $ "CA-SC-SM-CS"
      * --- Se carico/scarico legge i Criteri di valorizzazione Impostati nei Parametri
      if USED("APPOVALO")
        SELECT APPOVALO
        if RECCOUNT()>0
          GO TOP
          do case
            case this.w_CRIVAL="CL"
              * --- Leggo il Listino Qta Scaglione e Data di Attivaziione piu' Prossimi
              LOCATE FOR NVL(LICODART,"  ")=this.w_MVCODART AND NVL(LIQUANTI,0)>=this.w_MVQTAUM1
              if FOUND()
                this.w_LIPREZZO = NVL(LIPREZZO, 0)
                if this.w_LIPREZZO<>0 AND NVL(LSIVALIS,"N")="L" AND NVL(IVPERIVA,0)<>0
                  * --- Se Listino al Lordo , Nettifica
                  this.w_APPO = cp_ROUND(this.w_LIPREZZO - (this.w_LIPREZZO / (1 + (IVPERIVA / 100))), this.w_DECUNI)
                  this.w_LIPREZZO = this.w_LIPREZZO - this.w_APPO
                endif
              endif
            case this.w_CRIVAL="US"
              * --- Cerco il Costo dell'Articolo nei Dati Articoli (Prima sul Magazzino)
              LOCATE FOR NVL(PRCODART,"  ")=this.w_MVCODART AND NVL(PRCODMAG, SPACE(5))=this.w_MVCODMAG
              if FOUND()
                this.w_LIPREZZO = NVL(PRCOSSTA, 0)
              else
                * --- ...Se non esiste sul Magazzino nei Dati generici
                GO TOP
                LOCATE FOR NVL(PRCODART,"  ")=this.w_MVCODART AND NVL(PRCODMAG, SPACE(5))="#####"
                if FOUND()
                  this.w_LIPREZZO = NVL(PRCOSSTA, 0)
                endif
              endif
            case this.w_CRIVAL $ "CS-CM-UC-UP"
              * --- Cerco il Costo dell'Articolo nel Cursore calcolato sull'Inventario
              LOCATE FOR NVL(DICODICE,"  ")=this.w_MVCODART
              if FOUND()
                this.w_LIPREZZO = NVL(IIF(this.w_CRIVAL="CS", DICOSSTA, IIF(this.w_CRIVAL="CM", DICOSMPP, IIF(this.w_CRIVAL="UP", DIPRZMPP, DICOSULT))), 0)
              endif
            case this.w_CRIVAL="UA"
              * --- Cerco il Costo dell'Articolo nei Saldi Articoli
              LOCATE FOR NVL(SLCODICE,"  ")=this.w_MVCODART AND NVL(SLCODMAG, SPACE(5))=this.w_MVCODMAG
              if FOUND()
                this.w_LIPREZZO = NVL(SLVALUCA, 0)
              endif
          endcase
        endif
      endif
    else
      if this.oParentObject.w_OPERAZ="RI"
        if USED("APPORISO")
          SELECT APPORISO
          if RECCOUNT()>0
            GO TOP
            LOCATE FOR NVL(DRSERVIZ,"  ")=this.w_MVCODART and NVL(DRCODRIS,"  ")=NVL(Geneordi.DRCODRIS,"  ") and NVL(DRTIPRIS,"  ")=NVL(Geneordi.DRTIPRIS,"  ") and Geneordi.PDDATORD>=DRDATINI and Geneordi.PDDATORD<DRDATFIN
            if FOUND()
              * --- Il costo sulla risorsa � sempre espresso in ore
              this.w_RCMV = "N"
              if this.w_MVUNIMIS=NVL(Geneordi.ARUNMIS1, SPACE(3)) or this.w_MVUNIMIS=NVL(Geneordi.ARUNMIS2, SPACE(3))
                * --- L'UM della risorsa corrisponde a quello del servizio, converto, se necessario, ma non modifico w_MVUNIMIS
              else
                * --- Converto, se necessario, e modifico w_MVUNIMIS prendendo l'UM principale del servizio che deve obbligatoriamente essere una UM tempi
                * --- Read from UNIMIS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.UNIMIS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "UMDURSEC"+;
                    " from "+i_cTable+" UNIMIS where ";
                        +"UMCODICE = "+cp_ToStrODBC(this.w_MVUNIMIS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    UMDURSEC;
                    from (i_cTable) where;
                        UMCODICE = this.w_MVUNIMIS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CONVE = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_MVQTAMOV = cp_Round(NVL(this.w_MVQTAMOV, 0)*this.w_CONVE,3)
                this.w_MVUNIMIS = NVL(Geneordi.ARUNMIS1, SPACE(3))
                this.w_RCMV = "S"
              endif
              * --- Read from UNIMIS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.UNIMIS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "UMDURSEC"+;
                  " from "+i_cTable+" UNIMIS where ";
                      +"UMCODICE = "+cp_ToStrODBC(this.w_MVUNIMIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  UMDURSEC;
                  from (i_cTable) where;
                      UMCODICE = this.w_MVUNIMIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CONVE = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CLUNIMIS = this.w_MVUNIMIS
              if this.w_RCMV="S"
                this.w_MVQTAMOV = cp_Round(NVL(this.w_MVQTAMOV, 0)/this.w_CONVE,3)
              endif
              this.w_CONVE = this.w_CONVE/3600
              this.w_LIPREZZO = cp_Round(NVL(PRCOSSTA, 0)*this.w_CONVE,this.w_DECUNI)
              * --- w_MVQTAUM1 � originariamente sempre espressa in secondi
              * --- Read from UNIMIS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.UNIMIS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "UMDURSEC"+;
                  " from "+i_cTable+" UNIMIS where ";
                      +"UMCODICE = "+cp_ToStrODBC(Geneordi.ARUNMIS1);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  UMDURSEC;
                  from (i_cTable) where;
                      UMCODICE = Geneordi.ARUNMIS1;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CONVE = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_CONVE>1
                this.w_CONVE = this.w_CONVE/3600
                this.w_MVQTAUM1 = cp_Round(NVL(this.w_MVQTAUM1, 0)*this.w_CONVE,3)
              endif
            endif
          endif
        endif
      else
        this.w_CODART = this.w_MVCODART
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARPREZUM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARPREZUM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DATREG = this.oParentObject.w_MVDATREG
        this.w_CODVAL = this.w_MVCODVAL
        this.w_CODLIS = this.w_MVCODLIS
        this.w_CODCON = this.w_MVCONTRA
        this.w_CATCLI = this.w_CATSCC
        this.w_CATART = this.w_CATSCM
        this.w_ARRSUP = IIF(this.w_DECTOT=0, .499, .00499)
        this.w_CODGRU = this.w_GRUMER
        this.w_SCOCON = .F.
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSFLSCON"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.w_CODLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSFLSCON;
            from (i_cTable) where;
                LSCODLIS = this.w_CODLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        DECLARE ARRCALC (16,1)
        * --- Azzero l'Array che verr� riempito dalla Funzione
        ARRCALC(1)=0
        this.w_QTAUM3 = CALQTA( this.w_MVQTAUM1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        this.w_QTAUM2 = CALQTA( this.w_MVQTAUM1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        DIMENSION pArrUm[9]
        pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MVUNIMIS 
 pArrUm [3] = this.w_MVQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MVQTAUM1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
        this.w_CALPRZ = CalPrzli( iif(Empty(this.w_MVTCONTR),Repl("X",16),this.w_MVTCONTR) , this.oParentObject.w_MVTIPCON , this.w_CODLIS , this.w_CODART , this.w_CODGRU , this.w_MVQTAUM1 , this.w_CODVAL , this.w_MVCAOVAL , this.w_DatReg , this.w_CATCLI , this.w_CATART, this.w_CODVAL, this.w_MVCODCON, this.w_CATCOM, this.w_MVFLSCOR, this.w_SCOLIS, 0,"C", @ARRCALC, this.w_PRZVAC , this.w_MVFLVEAC,"N", @pArrUm )
        this.w_LIPREZZO = ARRCALC(5)
        this.w_CLUNIMIS = ARRCALC(16)
        this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
        this.w_MVSCONT1 = ARRCALC(1)
        this.w_MVSCONT2 = ARRCALC(2)
        this.w_MVSCONT3 = ARRCALC(3)
        this.w_MVSCONT4 = ARRCALC(4)
        this.w_IVACON = ARRCALC(12)
        this.w_MVCONTRA = IIF(ARRCALC(9)="XXXXXXXXXXXXXXXX",SPACE(15),ARRCALC(9))
        if this.w_MVUNIMIS<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
          this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_MVQTAUM1, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
          this.w_LIPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_MVQTAMOV, this.w_QTALIS, "N", 0, this.w_DECUNI), this.w_DECUNI)
        endif
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Documenti Associati al Piano
    * --- Variabili da passare alle Stampe
    this.w_CLIFOR = ""
    this.w_DATA1 = i_INIDAT
    this.w_DATA2 = i_FINDAT
    this.w_TIPOIN = this.oParentObject.w_MVTIPDOC
    this.w_MVSERIAL = ""
    this.oParentObject.w_MVSERIAL=""
    GSVE_BRD(This, "F")
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prende dati dichiarazione di intento
    if this.oParentObject.w_MVTIPCON $ "CF" AND EMPTY(this.w_MVCODIVE) AND NOT EMPTY(this.w_MVCODCON)
      * --- Se Cliente/Fornitore e no Codice Iva Non Imponibile
      * --- Verifica se esiste una Dichiarazione di Intento Valida
      this.w_OK = .F.
      this.w_NDIC = 0
      this.w_TIPOPE = "X"
      this.w_ADIC = SPACE(4)
      this.w_ALFADIC = Space(2)
      this.w_IMPDIC = 0
      this.w_DDIC = cp_CharToDate("  -  -  ")
      this.w_IMPUTI = 0
      this.w_TDIC = "X"
      this.w_CODIVE = SPACE(5)
      this.w_TIVA = " "
      this.oParentObject.w_LTesMes = " "
      this.w_DATINI = cp_CharToDate("  -  -  ")
      this.w_RIFDIC = SPACE(10)
      this.w_DATFIN = cp_CharToDate("  -  -  ")
      this.w_MVRIFDIC = Space(15)
      this.w_DICODICE = this.w_MVCODCON
      this.w_DITIPCON = this.oParentObject.w_MVTIPCON
      this.w_DIDATDOC = this.w_MVDATDOC
      * --- Lettura lettera di intento valida
      DECLARE ARRDIC (14,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRDIC(1)=0
      this.w_OK_LET = CAL_LETT(this.w_MVDATDOC,this.oParentObject.w_MVTIPCON,this.w_MVCODCON, @ArrDic, SPACE(10), this.w_MVCODDES)
      if this.w_OK_LET
        * --- Parametri
        *     pDatRif : Data di Riferimento per filtro su Lettere di intento
        *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
        *     pCodCon : Codice Cliente/Fornitore
        *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
        *     
        *     pArrDic[ 1 ]   = Numero Dichiarazione
        *     pArrDic[ 2 ]   = Tipo Operazione
        *     pArrDic[ 3 ]   = Anno Dichiarazione
        *     pArrDic[ 4 ]   = Importo Dichiarazione
        *     pArrDic[ 5 ]   = Data Dichiarazione
        *     pArrDic[ 6 ]   = Importo Utilizzato
        *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
        *     pArrDic[ 8 ]   = Codice Iva Agevolata
        *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
        *     pArrDic[ 10 ] = Data Inizio Validit�
        *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
        *     pArrDic[ 12 ] = Data Obsolescenza
        this.w_NDIC = ArrDic(1)
        this.w_TIPOPE = ArrDic(2)
        this.w_ADIC = ArrDic(3)
        this.w_IMPDIC = ArrDic(4)
        this.w_DDIC = ArrDic(5)
        this.w_IMPUTI = ArrDic(6)
        this.w_TDIC = ArrDic(7)
        this.w_CODIVE = ArrDic(8)
        this.w_TIVA = ArrDic(9)
        this.w_DATINI = ArrDic(10)
        this.w_RIFDIC = ArrDic(11)
        this.w_DATFIN = ArrDic(12)
        this.w_ALFADIC = ArrDic(13)
      endif
      do case
        case this.w_TIPOPE = "I"
          * --- Importo Definito OK Se Importo Dichiarato>Importo Utilizzato
          if this.w_IMPUTI>this.w_IMPDIC
            this.w_OK = .T.
            this.oParentObject.w_LTesMes = ah_Msgformat("Importo disponibile della dichiarazione di esenzione esaurito")
          else
            this.w_OK = .T.
            this.oParentObject.w_LTesMes = ah_Msgformat("Verificare dichiarazione di intento (importo definito)")
          endif
        case this.w_TIPOPE = "O"
          * --- Operazione Specifica
          this.w_OK = .T.
          this.oParentObject.w_LTesMes = ah_Msgformat("Verificare dichiarazione di intento (operazione specifica)")
          * --- Notificare questo messaggio in una stampa
        case this.w_TIPOPE = "D"
          * --- Periodo Definito
          this.w_OK = .T.
          this.oParentObject.w_LTesMes = ah_Msgformat("Verificare dichiarazione di intento (definizione periodo)")
          * --- Notificare questo messaggio in una stampa
      endcase
      if NOT EMPTY(this.oParentObject.w_LTesMes)
        * --- Scrive LOG
        SELECT Geneordi
        this.w_CODODL = PDSERIAL
        this.oParentObject.w_LNumErr = this.oParentObject.w_LNumErr + 1
        this.oParentObject.w_LOggErr = this.w_CODODL
        this.oParentObject.w_LErrore = ah_Msgformat("Verificare documenti generati")
        this.oParentObject.w_LTesMes = ah_Msgformat("%1%0Rif. dichiarazione di intento n.: %2/%3/%4 del %5%0***** Solo warning", this.oParentObject.w_LTesMes, ALLTRIM(STR(this.w_NDIC)), this.w_ADIC, this.w_ALFADIC,DTOC(this.w_DDIC) )
        INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
        (this.oParentObject.w_LNumErr, this.oParentObject.w_LOggErr, this.oParentObject.w_LErrore, this.oParentObject.w_LTesMes)
        this.oParentObject.w_LTesMes = ""
      endif
      if this.w_OK
        * --- Se Ok riporta i dati dell'Esenzione
        this.w_MVCODIVE = this.w_CODIVE
        this.w_MVRIFDIC = this.w_RIFDIC
      endif
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso in cui voglio spostare l'impegno all'ordine devo verificare se il magazzino del terzista � nettificabile
    *     altrimenti non faccio niente
    * --- Viene verificato il magazzino del terzista se nettificabile
    this.w_NETMGORI = " "
    this.w_NETMGWIP = " "
    this.w_FLIMPE = " "
    this.w_FLIMP2 = " "
    this.w_FLORD = " "
    this.w_FLORD2 = " "
    this.w_FLRISE = " "
    this.w_FLRISE2 = " "
    * --- Select from GSCO3BGD
    do vq_exec with 'GSCO3BGD',this,'_Curs_GSCO3BGD','',.f.,.t.
    if used('_Curs_GSCO3BGD')
      select _Curs_GSCO3BGD
      locate for 1=1
      do while not(eof())
      * --- Leggo i valori dei flag impegnato/ordinato/riservato
      this.w_FLIMPE = NVL(_Curs_GSCO3BGD.OLFLIMPE , SPACE(1))
      this.w_FLORD = NVL(_Curs_GSCO3BGD.OLFLORDI , SPACE(1))
      this.w_FLRISE = NVL(_Curs_GSCO3BGD.OLFLRISE , SPACE(1))
      this.w_OLQTASAL = NVL(_Curs_GSCO3BGD.OLQTASAL, 0)
      this.w_OLCODART = NVL(_Curs_GSCO3BGD.OLCODART, SPACE(20) )
      this.w_OLKEYSAL = NVL(_Curs_GSCO3BGD.OLKEYSAL, SPACE(40) )
      this.w_OLMAGORI = NVL(_Curs_GSCO3BGD.OLMAGORI, SPACE(5) )
      this.w_OLTCOMME = NVL(_Curs_GSCO3BGD.OLTCOMME, SPACE(15) )
      this.w_FLCOM = NVL(_Curs_GSCO3BGD.ARSALCOM, "N" )
      this.w_OLMAGWIP = NVL(_Curs_GSCO3BGD.OLMAGWIP, SPACE(5) )
      this.w_NETMGWIP = NVL(_Curs_GSCO3BGD.NETMGWIP, "N")
      if this.w_NETMGWIP="S" AND this.w_OLQTASAL<>0 AND NOT EMPTY(this.w_OLKEYSAL) AND NOT EMPTY(this.w_OLMAGORI) AND NOT EMPTY(this.w_OLMAGWIP)
        this.w_NETMGORI = " "
        this.w_NETMGWIP = " "
        * --- Storno i flag sul magazzino di origine
        this.w_FLIMPE = IIF(this.w_FLIMPE="+", "-", IIF(this.w_FLIMPE="-", "+", " "))
        this.w_FLORD = IIF(this.w_FLORD="+", "-", IIF(this.w_FLORD="-", "+", " "))
        this.w_FLRISE = IIF(this.w_FLRISE="+", "-", IIF(this.w_FLRISE="-", "+", " "))
        * --- Magazzino principale
        if NOT EMPTY(this.w_FLIMPE+this.w_FLORD+this.w_FLRISE)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGORI);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLMAGORI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- Try
            local bErr_0544E2C8
            bErr_0544E2C8=bTrsErr
            this.Try_0544E2C8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_0544E2C8
            * --- End
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGORI);
                     )
            else
              update (i_cTable) set;
                  SLQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OLKEYSAL;
                  and SLCODMAG = this.w_OLMAGORI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if NVL(this.w_FLCOM,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGORI);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLKEYSAL;
                  and SCCODMAG = this.w_OLMAGORI;
                  and SCCODCAN = this.w_OLTCOMME;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_Rows=0
              * --- Aggiorna i saldi commessa
              * --- Try
              local bErr_0544BAA8
              bErr_0544BAA8=bTrsErr
              this.Try_0544BAA8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_0544BAA8
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGORI);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                       )
              else
                update (i_cTable) set;
                    SCQTIPER = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OLKEYSAL;
                    and SCCODMAG = this.w_OLMAGORI;
                    and SCCODCAN = this.w_OLTCOMME;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        * --- Imposto i flag per il magazzino del terzista
        this.w_FLIMP2 = IIF(this.w_FLIMPE="+", "-", IIF(this.w_FLIMPE="-", "+", " "))
        this.w_FLORD2 = IIF(this.w_FLORD="+", "-", IIF(this.w_FLORD="-", "+", " "))
        this.w_FLRISE2 = IIF(this.w_FLRISE="+", "-", IIF(this.w_FLRISE="-", "+", " "))
        * --- Magazzino terzista
        if NOT EMPTY(this.w_FLIMP2+this.w_FLORD2+this.w_FLRISE2)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLMAGWIP;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- Try
            local bErr_05445FE8
            bErr_05445FE8=bTrsErr
            this.Try_05445FE8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_05445FE8
            * --- End
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                     )
            else
              update (i_cTable) set;
                  SLQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OLKEYSAL;
                  and SLCODMAG = this.w_OLMAGWIP;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if NVL(this.w_FLCOM,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLKEYSAL;
                  and SCCODMAG = this.w_OLMAGWIP;
                  and SCCODCAN = this.w_OLTCOMME;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_Rows=0
              * --- Try
              local bErr_0542A248
              bErr_0542A248=bTrsErr
              this.Try_0542A248()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_0542A248
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                       )
              else
                update (i_cTable) set;
                    SCQTIPER = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OLKEYSAL;
                    and SCCODMAG = this.w_OLMAGWIP;
                    and SCCODCAN = this.w_OLTCOMME;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_OLMAGWIP),'ODL_DETT','OLCODMAG');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_GSCO3BGD.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              OLCODMAG = this.w_OLMAGWIP;
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_OLCODODL;
              and CPROWNUM = _Curs_GSCO3BGD.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_GSCO3BGD
        continue
      enddo
      use
    endif
  endproc
  proc Try_0544E2C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGORI),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLKEYSAL,'SLCODMAG',this.w_OLMAGORI)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGORI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0544BAA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGORI),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLKEYSAL,'SCCODMAG',this.w_OLMAGORI,'SCCODCAN',this.w_OLTCOMME)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGORI;
           ,this.w_OLTCOMME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05445FE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGWIP),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLKEYSAL,'SLCODMAG',this.w_OLMAGWIP)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGWIP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0542A248()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGWIP),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLKEYSAL,'SCCODMAG',this.w_OLMAGWIP,'SCCODCAN',this.w_OLTCOMME)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGWIP;
           ,this.w_OLTCOMME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,39)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CON_COSC'
    this.cWorkTables[4]='CON_TRAM'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='DIC_MATR'
    this.cWorkTables[8]='DIC_MCOM'
    this.cWorkTables[9]='DIC_PROD'
    this.cWorkTables[10]='DOC_DETT'
    this.cWorkTables[11]='DOC_MAST'
    this.cWorkTables[12]='DOC_RATE'
    this.cWorkTables[13]='KEY_ARTI'
    this.cWorkTables[14]='LIS_SCAG'
    this.cWorkTables[15]='LISTINI'
    this.cWorkTables[16]='MAGAZZIN'
    this.cWorkTables[17]='ODL_DETT'
    this.cWorkTables[18]='ODL_MAST'
    this.cWorkTables[19]='OUT_PUTS'
    this.cWorkTables[20]='PAG_2AME'
    this.cWorkTables[21]='PAG_AMEN'
    this.cWorkTables[22]='RIF_GODL'
    this.cWorkTables[23]='RIFMGODL'
    this.cWorkTables[24]='SALDIART'
    this.cWorkTables[25]='TIP_DOCU'
    this.cWorkTables[26]='VALUTE'
    this.cWorkTables[27]='VOCIIVA'
    this.cWorkTables[28]='CAN_TIER'
    this.cWorkTables[29]='MA_COSTI'
    this.cWorkTables[30]='VOC_COST'
    this.cWorkTables[31]='MAT_PROD'
    this.cWorkTables[32]='PEG_SELI'
    this.cWorkTables[33]='AVA_LOT'
    this.cWorkTables[34]='MPS_TFOR'
    this.cWorkTables[35]='SALDICOM'
    this.cWorkTables[36]='UNIMIS'
    this.cWorkTables[37]='ODL_CICL'
    this.cWorkTables[38]='ODL_RISF'
    this.cWorkTables[39]='ODL_RISO'
    return(this.OpenAllTables(39))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_PAG_2AME')
      use in _Curs_PAG_2AME
    endif
    if used('_Curs_DIC_PROD')
      use in _Curs_DIC_PROD
    endif
    if used('_Curs_gsco2bgd')
      use in _Curs_gsco2bgd
    endif
    if used('_Curs_GSCO3BGD')
      use in _Curs_GSCO3BGD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
