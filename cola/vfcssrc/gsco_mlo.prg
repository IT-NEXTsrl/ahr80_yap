* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mlo                                                        *
*              Consuntivazione lotti                                           *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-11                                                      *
* Last revis.: 2015-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mlo")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mlo")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mlo")
  return

* --- Class definition
define class tgsco_mlo as StdPCForm
  Width  = 671
  Height = 194
  Top    = 10
  Left   = 10
  cComment = "Consuntivazione lotti"
  cPrg = "gsco_mlo"
  HelpContextID=53893783
  add object cnt as tcgsco_mlo
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mlo as PCContext
  w_CLDICHIA = space(15)
  w_CLROWCNM = 0
  w_CLNUMRIF = 0
  w_CLFLCASC = space(1)
  w_CLCODMAG = space(5)
  w_CLLOTART = space(20)
  w_MAGUBI = space(5)
  w_UBIDEF = space(20)
  w_CLCODUBI = space(20)
  w_UNIMIS = space(3)
  w_CLQTAMOV = 0
  w_OLDQTAMOV = 0
  w_TOTQTA = 0
  w_ARTLOT = space(20)
  w_FLSTAT = space(1)
  w_LOTZOOM = space(1)
  w_CODFOR = space(15)
  w_FLLOTT = space(1)
  w_FLUBIC = space(1)
  w_CODART = space(20)
  w_CPROWNUM = 0
  w_UBIZOOM = space(1)
  w_CLCODLOT = space(20)
  w_FLPRG = space(1)
  w_CODCOM = space(15)
  w_DATREG = space(8)
  w_DISLOT = space(5)
  w_SERODL = space(15)
  w_ROWODL = 0
  w_QTAORI = 0
  w_CLROWODL = 0
  w_VALRIG = 0
  w_LQTA = 0
  w_LQTR = 0
  w_QTAESI = 0
  w_CLROWDOC = 0
  w_TOTC = 0
  w_TOTS = 0
  w_QTAC = 0
  w_QTAS = 0
  proc Save(i_oFrom)
    this.w_CLDICHIA = i_oFrom.w_CLDICHIA
    this.w_CLROWCNM = i_oFrom.w_CLROWCNM
    this.w_CLNUMRIF = i_oFrom.w_CLNUMRIF
    this.w_CLFLCASC = i_oFrom.w_CLFLCASC
    this.w_CLCODMAG = i_oFrom.w_CLCODMAG
    this.w_CLLOTART = i_oFrom.w_CLLOTART
    this.w_MAGUBI = i_oFrom.w_MAGUBI
    this.w_UBIDEF = i_oFrom.w_UBIDEF
    this.w_CLCODUBI = i_oFrom.w_CLCODUBI
    this.w_UNIMIS = i_oFrom.w_UNIMIS
    this.w_CLQTAMOV = i_oFrom.w_CLQTAMOV
    this.w_OLDQTAMOV = i_oFrom.w_OLDQTAMOV
    this.w_TOTQTA = i_oFrom.w_TOTQTA
    this.w_ARTLOT = i_oFrom.w_ARTLOT
    this.w_FLSTAT = i_oFrom.w_FLSTAT
    this.w_LOTZOOM = i_oFrom.w_LOTZOOM
    this.w_CODFOR = i_oFrom.w_CODFOR
    this.w_FLLOTT = i_oFrom.w_FLLOTT
    this.w_FLUBIC = i_oFrom.w_FLUBIC
    this.w_CODART = i_oFrom.w_CODART
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_UBIZOOM = i_oFrom.w_UBIZOOM
    this.w_CLCODLOT = i_oFrom.w_CLCODLOT
    this.w_FLPRG = i_oFrom.w_FLPRG
    this.w_CODCOM = i_oFrom.w_CODCOM
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_DISLOT = i_oFrom.w_DISLOT
    this.w_SERODL = i_oFrom.w_SERODL
    this.w_ROWODL = i_oFrom.w_ROWODL
    this.w_QTAORI = i_oFrom.w_QTAORI
    this.w_CLROWODL = i_oFrom.w_CLROWODL
    this.w_VALRIG = i_oFrom.w_VALRIG
    this.w_LQTA = i_oFrom.w_LQTA
    this.w_LQTR = i_oFrom.w_LQTR
    this.w_QTAESI = i_oFrom.w_QTAESI
    this.w_CLROWDOC = i_oFrom.w_CLROWDOC
    this.w_TOTC = i_oFrom.w_TOTC
    this.w_TOTS = i_oFrom.w_TOTS
    this.w_QTAC = i_oFrom.w_QTAC
    this.w_QTAS = i_oFrom.w_QTAS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CLDICHIA = this.w_CLDICHIA
    i_oTo.w_CLROWCNM = this.w_CLROWCNM
    i_oTo.w_CLNUMRIF = this.w_CLNUMRIF
    i_oTo.w_CLFLCASC = this.w_CLFLCASC
    i_oTo.w_CLCODMAG = this.w_CLCODMAG
    i_oTo.w_CLLOTART = this.w_CLLOTART
    i_oTo.w_MAGUBI = this.w_MAGUBI
    i_oTo.w_UBIDEF = this.w_UBIDEF
    i_oTo.w_CLCODUBI = this.w_CLCODUBI
    i_oTo.w_UNIMIS = this.w_UNIMIS
    i_oTo.w_CLQTAMOV = this.w_CLQTAMOV
    i_oTo.w_OLDQTAMOV = this.w_OLDQTAMOV
    i_oTo.w_TOTQTA = this.w_TOTQTA
    i_oTo.w_ARTLOT = this.w_ARTLOT
    i_oTo.w_FLSTAT = this.w_FLSTAT
    i_oTo.w_LOTZOOM = this.w_LOTZOOM
    i_oTo.w_CODFOR = this.w_CODFOR
    i_oTo.w_FLLOTT = this.w_FLLOTT
    i_oTo.w_FLUBIC = this.w_FLUBIC
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_UBIZOOM = this.w_UBIZOOM
    i_oTo.w_CLCODLOT = this.w_CLCODLOT
    i_oTo.w_FLPRG = this.w_FLPRG
    i_oTo.w_CODCOM = this.w_CODCOM
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_DISLOT = this.w_DISLOT
    i_oTo.w_SERODL = this.w_SERODL
    i_oTo.w_ROWODL = this.w_ROWODL
    i_oTo.w_QTAORI = this.w_QTAORI
    i_oTo.w_CLROWODL = this.w_CLROWODL
    i_oTo.w_VALRIG = this.w_VALRIG
    i_oTo.w_LQTA = this.w_LQTA
    i_oTo.w_LQTR = this.w_LQTR
    i_oTo.w_QTAESI = this.w_QTAESI
    i_oTo.w_CLROWDOC = this.w_CLROWDOC
    i_oTo.w_TOTC = this.w_TOTC
    i_oTo.w_TOTS = this.w_TOTS
    i_oTo.w_QTAC = this.w_QTAC
    i_oTo.w_QTAS = this.w_QTAS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mlo as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 671
  Height = 194
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-10"
  HelpContextID=53893783
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AVA_LOT_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  PAR_RIMA_IDX = 0
  SALDILOT_IDX = 0
  cFile = "AVA_LOT"
  cKeySelect = "CLDICHIA,CLROWCNM,CLNUMRIF,CLROWDOC"
  cKeyWhere  = "CLDICHIA=this.w_CLDICHIA and CLROWCNM=this.w_CLROWCNM and CLNUMRIF=this.w_CLNUMRIF and CLROWDOC=this.w_CLROWDOC"
  cKeyDetail  = "CLDICHIA=this.w_CLDICHIA and CLROWCNM=this.w_CLROWCNM and CLNUMRIF=this.w_CLNUMRIF and CPROWNUM=this.w_CPROWNUM and CLCODLOT=this.w_CLCODLOT and CLROWDOC=this.w_CLROWDOC"
  cKeyWhereODBC = '"CLDICHIA="+cp_ToStrODBC(this.w_CLDICHIA)';
      +'+" and CLROWCNM="+cp_ToStrODBC(this.w_CLROWCNM)';
      +'+" and CLNUMRIF="+cp_ToStrODBC(this.w_CLNUMRIF)';
      +'+" and CLROWDOC="+cp_ToStrODBC(this.w_CLROWDOC)';

  cKeyDetailWhereODBC = '"CLDICHIA="+cp_ToStrODBC(this.w_CLDICHIA)';
      +'+" and CLROWCNM="+cp_ToStrODBC(this.w_CLROWCNM)';
      +'+" and CLNUMRIF="+cp_ToStrODBC(this.w_CLNUMRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CLCODLOT="+cp_ToStrODBC(this.w_CLCODLOT)';
      +'+" and CLROWDOC="+cp_ToStrODBC(this.w_CLROWDOC)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"AVA_LOT.CLDICHIA="+cp_ToStrODBC(this.w_CLDICHIA)';
      +'+" and AVA_LOT.CLROWCNM="+cp_ToStrODBC(this.w_CLROWCNM)';
      +'+" and AVA_LOT.CLNUMRIF="+cp_ToStrODBC(this.w_CLNUMRIF)';
      +'+" and AVA_LOT.CLROWDOC="+cp_ToStrODBC(this.w_CLROWDOC)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'AVA_LOT.CPROWNUM '
  cPrg = "gsco_mlo"
  cComment = "Consuntivazione lotti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CLDICHIA = space(15)
  w_CLROWCNM = 0
  w_CLNUMRIF = 0
  w_CLFLCASC = space(1)
  w_CLCODMAG = space(5)
  o_CLCODMAG = space(5)
  w_CLLOTART = space(20)
  o_CLLOTART = space(20)
  w_MAGUBI = space(5)
  w_UBIDEF = space(20)
  w_CLCODUBI = space(20)
  w_UNIMIS = space(3)
  w_CLQTAMOV = 0
  o_CLQTAMOV = 0
  w_OLDQTAMOV = 0
  w_TOTQTA = 0
  w_ARTLOT = space(20)
  w_FLSTAT = space(1)
  w_LOTZOOM = .F.
  w_CODFOR = space(15)
  w_FLLOTT = space(1)
  w_FLUBIC = space(1)
  w_CODART = space(20)
  w_CPROWNUM = 0
  w_UBIZOOM = .F.
  w_CLCODLOT = space(20)
  o_CLCODLOT = space(20)
  w_FLPRG = space(1)
  w_CODCOM = space(15)
  w_DATREG = ctod('  /  /  ')
  w_DISLOT = space(5)
  w_SERODL = space(15)
  w_ROWODL = 0
  w_QTAORI = 0
  w_CLROWODL = 0
  w_VALRIG = 0
  w_LQTA = 0
  w_LQTR = 0
  w_QTAESI = 0
  w_CLROWDOC = 0
  w_TOTC = 0
  w_TOTS = 0
  w_QTAC = 0
  w_QTAS = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mloPag1","gsco_mlo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='UBICAZIO'
    this.cWorkTables[3]='PAR_RIMA'
    this.cWorkTables[4]='SALDILOT'
    this.cWorkTables[5]='AVA_LOT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AVA_LOT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AVA_LOT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mlo'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AVA_LOT where CLDICHIA=KeySet.CLDICHIA
    *                            and CLROWCNM=KeySet.CLROWCNM
    *                            and CLNUMRIF=KeySet.CLNUMRIF
    *                            and CPROWNUM=KeySet.CPROWNUM
    *                            and CLCODLOT=KeySet.CLCODLOT
    *                            and CLROWDOC=KeySet.CLROWDOC
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AVA_LOT_IDX,2],this.bLoadRecFilter,this.AVA_LOT_IDX,"gsco_mlo")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AVA_LOT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AVA_LOT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AVA_LOT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CLDICHIA',this.w_CLDICHIA  ,'CLROWCNM',this.w_CLROWCNM  ,'CLNUMRIF',this.w_CLNUMRIF  ,'CLROWDOC',this.w_CLROWDOC  )
      select * from (i_cTable) AVA_LOT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTQTA = 0
        .w_TOTC = 0
        .w_TOTS = 0
        .w_CLDICHIA = NVL(CLDICHIA,space(15))
        .w_CLROWCNM = NVL(CLROWCNM,0)
        .w_CLNUMRIF = NVL(CLNUMRIF,0)
        .w_CODFOR = SPACE(15)
        .w_FLLOTT = this.oParentObject .w_FLLOTT
        .w_FLUBIC = this.oParentObject .w_FLUBI
        .w_CODART = this.oParentObject .w_MPCODART
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_FLPRG = 'L'
        .w_CODCOM = this.oParentObject .w_MPCODICE
        .w_DATREG = this.oParentObject.oParentObject .w_DPDATREG
        .w_SERODL = this.oParentObject .w_MPSERODL
        .w_ROWODL = this.oParentObject .w_MPROWODL
        .w_QTAORI = this.oParentObject .w_MPQTAUM1
        .w_CLROWDOC = NVL(CLROWDOC,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'AVA_LOT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTQTA = 0
      this.w_TOTC = 0
      this.w_TOTS = 0
      scan
        with this
          .w_UBIDEF = space(20)
        .w_UNIMIS = this.oParentObject .w_UNMIS1
          .w_ARTLOT = space(20)
          .w_FLSTAT = space(1)
        .w_UBIZOOM = .F.
          .w_LQTA = 0
          .w_LQTR = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CLFLCASC = NVL(CLFLCASC,space(1))
          .w_CLCODMAG = NVL(CLCODMAG,space(5))
          .w_CLLOTART = NVL(CLLOTART,space(20))
          .link_2_3('Load')
        .w_MAGUBI = .w_CLCODMAG
          .link_2_4('Load')
          .w_CLCODUBI = NVL(CLCODUBI,space(20))
          * evitabile
          *.link_2_6('Load')
          .w_CLQTAMOV = NVL(CLQTAMOV,0)
        .w_OLDQTAMOV = iif(this.cFunction<>'Edit' or this.rowstatus()='A',0,iif(empty(.w_OLDQTAMOV),.w_CLQTAMOV,.w_OLDQTAMOV))
        .w_LOTZOOM = .F.
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_CLCODLOT = NVL(CLCODLOT,space(20))
          .link_2_16('Load')
        .w_DISLOT = .w_CLFLCASC
          .link_2_17('Load')
          .w_CLROWODL = NVL(CLROWODL,0)
        .w_VALRIG = .w_CLQTAMOV
        .w_QTAESI = .w_LQTA - .w_LQTR
        .w_QTAC = IIF(.w_CLFLCASC='C', .w_CLQTAMOV, 0)
        .w_QTAS = IIF(.w_CLFLCASC='S', .w_CLQTAMOV, 0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTQTA = .w_TOTQTA+.w_VALRIG
          .w_TOTC = .w_TOTC+.w_QTAC
          .w_TOTS = .w_TOTS+.w_QTAS
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_FLLOTT = this.oParentObject .w_FLLOTT
        .w_FLUBIC = this.oParentObject .w_FLUBI
        .w_CODART = this.oParentObject .w_MPCODART
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_FLPRG = 'L'
        .w_CODCOM = this.oParentObject .w_MPCODICE
        .w_DATREG = this.oParentObject.oParentObject .w_DPDATREG
        .w_SERODL = this.oParentObject .w_MPSERODL
        .w_ROWODL = this.oParentObject .w_MPROWODL
        .w_QTAORI = this.oParentObject .w_MPQTAUM1
        .w_QTAC = IIF(.w_CLFLCASC='C', .w_CLQTAMOV, 0)
        .w_QTAS = IIF(.w_CLFLCASC='S', .w_CLQTAMOV, 0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CLDICHIA=space(15)
      .w_CLROWCNM=0
      .w_CLNUMRIF=0
      .w_CLFLCASC=space(1)
      .w_CLCODMAG=space(5)
      .w_CLLOTART=space(20)
      .w_MAGUBI=space(5)
      .w_UBIDEF=space(20)
      .w_CLCODUBI=space(20)
      .w_UNIMIS=space(3)
      .w_CLQTAMOV=0
      .w_OLDQTAMOV=0
      .w_TOTQTA=0
      .w_ARTLOT=space(20)
      .w_FLSTAT=space(1)
      .w_LOTZOOM=.f.
      .w_CODFOR=space(15)
      .w_FLLOTT=space(1)
      .w_FLUBIC=space(1)
      .w_CODART=space(20)
      .w_CPROWNUM=0
      .w_UBIZOOM=.f.
      .w_CLCODLOT=space(20)
      .w_FLPRG=space(1)
      .w_CODCOM=space(15)
      .w_DATREG=ctod("  /  /  ")
      .w_DISLOT=space(5)
      .w_SERODL=space(15)
      .w_ROWODL=0
      .w_QTAORI=0
      .w_CLROWODL=0
      .w_VALRIG=0
      .w_LQTA=0
      .w_LQTR=0
      .w_QTAESI=0
      .w_CLROWDOC=0
      .w_TOTC=0
      .w_TOTS=0
      .w_QTAC=0
      .w_QTAS=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_CLFLCASC = 'S'
        .w_CLCODMAG = this.oParentObject .w_MPCODMAG
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CLLOTART))
         .link_2_3('Full')
        endif
        .w_MAGUBI = .w_CLCODMAG
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MAGUBI))
         .link_2_4('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_CLCODUBI = IIF(g_PERUBI='S' AND .w_FLUBIC='S', .w_UBIDEF, SPACE(20))
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CLCODUBI))
         .link_2_6('Full')
        endif
        .w_UNIMIS = this.oParentObject .w_UNMIS1
        .DoRTCalc(11,11,.f.)
        .w_OLDQTAMOV = iif(this.cFunction<>'Edit' or this.rowstatus()='A',0,iif(empty(.w_OLDQTAMOV),.w_CLQTAMOV,.w_OLDQTAMOV))
        .DoRTCalc(13,15,.f.)
        .w_LOTZOOM = .F.
        .w_CODFOR = SPACE(15)
        .w_FLLOTT = this.oParentObject .w_FLLOTT
        .w_FLUBIC = this.oParentObject .w_FLUBI
        .w_CODART = this.oParentObject .w_MPCODART
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(21,21,.f.)
        .w_UBIZOOM = .F.
        .w_CLCODLOT = .w_CLLOTART
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CLCODLOT))
         .link_2_16('Full')
        endif
        .w_FLPRG = 'L'
        .w_CODCOM = this.oParentObject .w_MPCODICE
        .w_DATREG = this.oParentObject.oParentObject .w_DPDATREG
        .w_DISLOT = .w_CLFLCASC
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_DISLOT))
         .link_2_17('Full')
        endif
        .w_SERODL = this.oParentObject .w_MPSERODL
        .w_ROWODL = this.oParentObject .w_MPROWODL
        .w_QTAORI = this.oParentObject .w_MPQTAUM1
        .w_CLROWODL = this.oParentObject .w_MPROWODL
        .w_VALRIG = .w_CLQTAMOV
        .DoRTCalc(33,34,.f.)
        .w_QTAESI = .w_LQTA - .w_LQTR
        .DoRTCalc(36,38,.f.)
        .w_QTAC = IIF(.w_CLFLCASC='C', .w_CLQTAMOV, 0)
        .w_QTAS = IIF(.w_CLFLCASC='S', .w_CLQTAMOV, 0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'AVA_LOT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_9.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AVA_LOT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDICHIA,"CLDICHIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLROWCNM,"CLROWCNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLNUMRIF,"CLNUMRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLROWDOC,"CLROWDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CLFLCASC N(3);
      ,t_CLCODMAG C(5);
      ,t_CLLOTART C(20);
      ,t_CLCODUBI C(20);
      ,t_UNIMIS C(3);
      ,t_CLQTAMOV N(12,3);
      ,t_OLDQTAMOV N(12,3);
      ,t_LQTA N(12,3);
      ,t_LQTR N(12,3);
      ,t_QTAESI N(12,3);
      ,CPROWNUM N(10);
      ,t_MAGUBI C(5);
      ,t_UBIDEF C(20);
      ,t_ARTLOT C(20);
      ,t_FLSTAT C(1);
      ,t_LOTZOOM L(1);
      ,t_CPROWNUM N(4);
      ,t_UBIZOOM L(1);
      ,t_CLCODLOT C(20);
      ,t_DISLOT C(5);
      ,t_CLROWODL N(4);
      ,t_VALRIG N(12,3);
      ,t_QTAC N(18,4);
      ,t_QTAS N(18,4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mlobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1.controlsource=this.cTrsName+'.t_CLFLCASC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODMAG_2_2.controlsource=this.cTrsName+'.t_CLCODMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLLOTART_2_3.controlsource=this.cTrsName+'.t_CLLOTART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODUBI_2_6.controlsource=this.cTrsName+'.t_CLCODUBI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUNIMIS_2_7.controlsource=this.cTrsName+'.t_UNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAMOV_2_8.controlsource=this.cTrsName+'.t_CLQTAMOV'
    this.oPgFRm.Page1.oPag.oOLDQTAMOV_2_9.controlsource=this.cTrsName+'.t_OLDQTAMOV'
    this.oPgFRm.Page1.oPag.oLQTA_2_19.controlsource=this.cTrsName+'.t_LQTA'
    this.oPgFRm.Page1.oPag.oLQTR_2_20.controlsource=this.cTrsName+'.t_LQTR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oQTAESI_2_21.controlsource=this.cTrsName+'.t_QTAESI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(83)
    this.AddVLine(145)
    this.AddVLine(316)
    this.AddVLine(488)
    this.AddVLine(537)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AVA_LOT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AVA_LOT_IDX,2])
      *
      * insert into AVA_LOT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AVA_LOT')
        i_extval=cp_InsertValODBCExtFlds(this,'AVA_LOT')
        i_cFldBody=" "+;
                  "(CLDICHIA,CLROWCNM,CLNUMRIF,CLFLCASC,CLCODMAG"+;
                  ",CLLOTART,CLCODUBI,CLQTAMOV,CLCODLOT,CLROWODL"+;
                  ",CLROWDOC,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CLDICHIA)+","+cp_ToStrODBC(this.w_CLROWCNM)+","+cp_ToStrODBC(this.w_CLNUMRIF)+","+cp_ToStrODBC(this.w_CLFLCASC)+","+cp_ToStrODBC(this.w_CLCODMAG)+;
             ","+cp_ToStrODBCNull(this.w_CLLOTART)+","+cp_ToStrODBCNull(this.w_CLCODUBI)+","+cp_ToStrODBC(this.w_CLQTAMOV)+","+cp_ToStrODBCNull(this.w_CLCODLOT)+","+cp_ToStrODBC(this.w_CLROWODL)+;
             ","+cp_ToStrODBC(this.w_CLROWDOC)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AVA_LOT')
        i_extval=cp_InsertValVFPExtFlds(this,'AVA_LOT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CLDICHIA',this.w_CLDICHIA,'CLROWCNM',this.w_CLROWCNM,'CLNUMRIF',this.w_CLNUMRIF,'CPROWNUM',this.w_CPROWNUM,'CLCODLOT',this.w_CLCODLOT,'CLROWDOC',this.w_CLROWDOC)
        INSERT INTO (i_cTable) (;
                   CLDICHIA;
                  ,CLROWCNM;
                  ,CLNUMRIF;
                  ,CLFLCASC;
                  ,CLCODMAG;
                  ,CLLOTART;
                  ,CLCODUBI;
                  ,CLQTAMOV;
                  ,CLCODLOT;
                  ,CLROWODL;
                  ,CLROWDOC;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CLDICHIA;
                  ,this.w_CLROWCNM;
                  ,this.w_CLNUMRIF;
                  ,this.w_CLFLCASC;
                  ,this.w_CLCODMAG;
                  ,this.w_CLLOTART;
                  ,this.w_CLCODUBI;
                  ,this.w_CLQTAMOV;
                  ,this.w_CLCODLOT;
                  ,this.w_CLROWODL;
                  ,this.w_CLROWDOC;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AVA_LOT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CLROWODL<>t_CLROWODL
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (t_CLQTAMOV>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AVA_LOT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AVA_LOT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CLROWODL<>t_CLROWODL
            i_bUpdAll = .t.
          endif
        endif
        scan for (t_CLQTAMOV>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AVA_LOT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AVA_LOT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CLFLCASC="+cp_ToStrODBC(this.w_CLFLCASC)+;
                     ",CLCODMAG="+cp_ToStrODBC(this.w_CLCODMAG)+;
                     ",CLLOTART="+cp_ToStrODBCNull(this.w_CLLOTART)+;
                     ",CLCODUBI="+cp_ToStrODBCNull(this.w_CLCODUBI)+;
                     ",CLQTAMOV="+cp_ToStrODBC(this.w_CLQTAMOV)+;
                     ",CLROWODL="+cp_ToStrODBC(this.w_CLROWODL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AVA_LOT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CLFLCASC=this.w_CLFLCASC;
                     ,CLCODMAG=this.w_CLCODMAG;
                     ,CLLOTART=this.w_CLLOTART;
                     ,CLCODUBI=this.w_CLCODUBI;
                     ,CLQTAMOV=this.w_CLQTAMOV;
                     ,CLROWODL=this.w_CLROWODL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AVA_LOT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CLQTAMOV>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AVA_LOT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CLQTAMOV>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AVA_LOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AVA_LOT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CLCODMAG<>.w_CLCODMAG
          .w_MAGUBI = .w_CLCODMAG
          .link_2_4('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_CLCODMAG<>.w_CLCODMAG
          .w_CLCODUBI = IIF(g_PERUBI='S' AND .w_FLUBIC='S', .w_UBIDEF, SPACE(20))
          .link_2_6('Full')
        endif
        .DoRTCalc(10,11,.t.)
        if .o_CLQTAMOV<>.w_CLQTAMOV
          .w_OLDQTAMOV = iif(this.cFunction<>'Edit' or this.rowstatus()='A',0,iif(empty(.w_OLDQTAMOV),.w_CLQTAMOV,.w_OLDQTAMOV))
        endif
        .DoRTCalc(13,15,.t.)
        if .o_CLCODLOT<>.w_CLCODLOT
          .w_LOTZOOM = .F.
        endif
          .w_CODFOR = SPACE(15)
          .w_FLLOTT = this.oParentObject .w_FLLOTT
          .w_FLUBIC = this.oParentObject .w_FLUBI
          .w_CODART = this.oParentObject .w_MPCODART
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(21,22,.t.)
        if .o_CLLOTART<>.w_CLLOTART
          .w_CLCODLOT = .w_CLLOTART
          .link_2_16('Full')
        endif
          .w_FLPRG = 'L'
          .w_CODCOM = this.oParentObject .w_MPCODICE
          .w_DATREG = this.oParentObject.oParentObject .w_DPDATREG
        if .o_CLCODMAG<>.w_CLCODMAG
          .w_DISLOT = .w_CLFLCASC
          .link_2_17('Full')
        endif
          .w_SERODL = this.oParentObject .w_MPSERODL
          .w_ROWODL = this.oParentObject .w_MPROWODL
          .w_QTAORI = this.oParentObject .w_MPQTAUM1
          .w_CLROWODL = this.oParentObject .w_MPROWODL
          .w_TOTQTA = .w_TOTQTA-.w_valrig
          .w_VALRIG = .w_CLQTAMOV
          .w_TOTQTA = .w_TOTQTA+.w_valrig
        .DoRTCalc(33,34,.t.)
          .w_QTAESI = .w_LQTA - .w_LQTR
        .DoRTCalc(36,38,.t.)
          .w_QTAC = IIF(.w_CLFLCASC='C', .w_CLQTAMOV, 0)
          .w_QTAS = IIF(.w_CLFLCASC='S', .w_CLQTAMOV, 0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MAGUBI with this.w_MAGUBI
      replace t_UBIDEF with this.w_UBIDEF
      replace t_ARTLOT with this.w_ARTLOT
      replace t_FLSTAT with this.w_FLSTAT
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_UBIZOOM with this.w_UBIZOOM
      replace t_CLCODLOT with this.w_CLCODLOT
      replace t_DISLOT with this.w_DISLOT
      replace t_CLROWODL with this.w_CLROWODL
      replace t_VALRIG with this.w_VALRIG
      replace t_QTAC with this.w_QTAC
      replace t_QTAS with this.w_QTAS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLLOTART_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLLOTART_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLCODUBI_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLCODUBI_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTOTS_1_19.visible=!this.oPgFrm.Page1.oPag.oTOTS_1_19.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLQTA_2_19.visible=!this.oPgFrm.Page1.oPag.oLQTA_2_19.mHide()
    this.oPgFrm.Page1.oPag.oLQTR_2_20.visible=!this.oPgFrm.Page1.oPag.oLQTR_2_20.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAESI_2_21.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAESI_2_21.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLLOTART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLLOTART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_CLLOTART)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_CLLOTART))
          select LOCODART,LOCODICE,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLLOTART)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLLOTART) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oCLLOTART_2_3'),i_cWhere,'GSAR_ALO',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o non disponibile o di un altro articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLLOTART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CLLOTART);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_CLLOTART)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLLOTART = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLLOTART = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLOTUB(.w_CLCODLOT,'',0,.w_CLCODMAG,.w_CODART,.w_CODFOR,.w_CLFLCASC,.w_FLSTAT,.w_LOTZOOM,.w_DATREG,' ','L','','',.F.,'')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o non disponibile o di un altro articolo")
        endif
        this.w_CLLOTART = space(20)
        this.w_ARTLOT = space(20)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLLOTART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGUBI
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
    i_lTable = "PAR_RIMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2], .t., this.PAR_RIMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRCODMAG,PRUBIDEF";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODMAG="+cp_ToStrODBC(this.w_MAGUBI);
                   +" and PRCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_CODART;
                       ,'PRCODMAG',this.w_MAGUBI)
            select PRCODART,PRCODMAG,PRUBIDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGUBI = NVL(_Link_.PRCODMAG,space(5))
      this.w_UBIDEF = NVL(_Link_.PRUBIDEF,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MAGUBI = space(5)
      endif
      this.w_UBIDEF = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)+'\'+cp_ToStr(_Link_.PRCODMAG,1)
      cp_ShowWarn(i_cKey,this.PAR_RIMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLCODUBI
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CLCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CLCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CLCODMAG;
                     ,'UBCODICE',trim(this.w_CLCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCLCODUBI_2_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CLCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CLCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CLCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CLCODMAG;
                       ,'UBCODICE',this.w_CLCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLOTUB(.w_CLCODUBI,' ',0,.w_CLCODMAG,.w_CODART,.w_CODFOR,.w_CLFLCASC,.w_FLSTAT,.w_UBIZOOM,.w_DATREG,' ','U','     ','     ',.t.,'N')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CLCODUBI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLCODLOT
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_lTable = "SALDILOT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2], .t., this.SALDILOT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUCODMAG,SUCODLOT,SUCODART,SUQTAPER,SUQTRPER";
                   +" from "+i_cTable+" "+i_lTable+" where SUCODLOT="+cp_ToStrODBC(this.w_CLCODLOT);
                   +" and SUCODMAG="+cp_ToStrODBC(this.w_CLCODMAG);
                   +" and SUCODLOT="+cp_ToStrODBC(this.w_CLCODLOT);
                   +" and SUCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUCODMAG',this.w_CLCODMAG;
                       ,'SUCODLOT',this.w_CLCODLOT;
                       ,'SUCODART',this.w_CODART;
                       ,'SUCODLOT',this.w_CLCODLOT)
            select SUCODMAG,SUCODLOT,SUCODART,SUQTAPER,SUQTRPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODLOT = NVL(_Link_.SUCODLOT,space(20))
      this.w_LQTA = NVL(_Link_.SUQTAPER,0)
      this.w_LQTR = NVL(_Link_.SUQTRPER,0)
    else
      if i_cCtrl<>'Load'
        this.w_CLCODLOT = space(20)
      endif
      this.w_LQTA = 0
      this.w_LQTR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])+'\'+cp_ToStr(_Link_.SUCODMAG,1)+'\'+cp_ToStr(_Link_.SUCODLOT,1)+'\'+cp_ToStr(_Link_.SUCODART,1)+'\'+cp_ToStr(_Link_.SUCODLOT,1)
      cp_ShowWarn(i_cKey,this.SALDILOT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISLOT
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
    i_lTable = "PAR_RIMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2], .t., this.PAR_RIMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRCODMAG,PRUBIDEF";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODMAG="+cp_ToStrODBC(this.w_DISLOT);
                   +" and PRCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_CODART;
                       ,'PRCODMAG',this.w_DISLOT)
            select PRCODART,PRCODMAG,PRUBIDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISLOT = NVL(_Link_.PRCODMAG,space(5))
      this.w_UBIDEF = NVL(_Link_.PRUBIDEF,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DISLOT = space(5)
      endif
      this.w_UBIDEF = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)+'\'+cp_ToStr(_Link_.PRCODMAG,1)
      cp_ShowWarn(i_cKey,this.PAR_RIMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oOLDQTAMOV_2_9.value==this.w_OLDQTAMOV)
      this.oPgFrm.Page1.oPag.oOLDQTAMOV_2_9.value=this.w_OLDQTAMOV
      replace t_OLDQTAMOV with this.oPgFrm.Page1.oPag.oOLDQTAMOV_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTQTA_3_1.value==this.w_TOTQTA)
      this.oPgFrm.Page1.oPag.oTOTQTA_3_1.value=this.w_TOTQTA
    endif
    if not(this.oPgFrm.Page1.oPag.oLQTA_2_19.value==this.w_LQTA)
      this.oPgFrm.Page1.oPag.oLQTA_2_19.value=this.w_LQTA
      replace t_LQTA with this.oPgFrm.Page1.oPag.oLQTA_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLQTR_2_20.value==this.w_LQTR)
      this.oPgFrm.Page1.oPag.oLQTR_2_20.value=this.w_LQTR
      replace t_LQTR with this.oPgFrm.Page1.oPag.oLQTR_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTS_1_19.value==this.w_TOTS)
      this.oPgFrm.Page1.oPag.oTOTS_1_19.value=this.w_TOTS
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1.RadioValue()==this.w_CLFLCASC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1.SetRadio()
      replace t_CLFLCASC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODMAG_2_2.value==this.w_CLCODMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODMAG_2_2.value=this.w_CLCODMAG
      replace t_CLCODMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODMAG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLOTART_2_3.value==this.w_CLLOTART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLOTART_2_3.value=this.w_CLLOTART
      replace t_CLLOTART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLOTART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODUBI_2_6.value==this.w_CLCODUBI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODUBI_2_6.value=this.w_CLCODUBI
      replace t_CLCODUBI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODUBI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUNIMIS_2_7.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUNIMIS_2_7.value=this.w_UNIMIS
      replace t_UNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUNIMIS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAMOV_2_8.value==this.w_CLQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAMOV_2_8.value=this.w_CLQTAMOV
      replace t_CLQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLQTAMOV_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAESI_2_21.value==this.w_QTAESI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAESI_2_21.value=this.w_QTAESI
      replace t_QTAESI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAESI_2_21.value
    endif
    cp_SetControlsValueExtFlds(this,'AVA_LOT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsco_mlo
      i_oldarea=select()
      if reccount(.cTrsName)>0
      IF i_bRes=.T. AND ((g_PERLOT='S' AND .w_FLLOTT $ 'SC')  AND (.cFunction='Load' OR .cFunction='Edit'))
        Select t_CLLOTART as LOTTO, nvl(t_CLCODUBI,'     ') as CODUBI,sum(t_CLQTAMOV) as QTA,sum(t_OLDQTAMOV) as QOLD, max(t_QTAESI) as ESI from (.cTrsName);
        where !EMPTY(nvl(t_CLLOTART,''))  and not deleted() group by LOTTO, CODUBI into cursor test
        Select ('test')
        locate for (QTA-QOLD)>ESI
        if found()
          i_bRes = .f.     
          i_bnoChk = .f.
          i_cErrorMsg = ah_MsgFormat("Quantit� lotto superiore a quella disponibile")
        endif
        use in select('Test')
      endif
      endif
      if (not empty(.w_CLLOTART) or not empty(.w_CLCODUBI)) and .w_CLQTAMOV>0
        IF i_bRes=.T. and .w_TOTQTA <> .w_QTAORI
           local messerr
           i_bRes = .f.
           i_bnoChk = .f.	
           if .w_TOTQTA > .w_QTAORI
              messerr = Ah_MsgFormat("Quantit� superiore (%1) alla quantit� da scaricare (%2)", alltrim(str(.w_TOTQTA)),alltrim(str(.w_QTAORI)))
           else
              messerr = Ah_MsgFormat("Quantit� inferiore (%1) alla quantit� da scaricare (%2)", alltrim(str(.w_TOTQTA)),alltrim(str(.w_QTAORI)))
           endif
           i_cErrorMsg = messerr
        endif
      endif
      select (i_oldarea)
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(.w_TOTQTA<=.oParentObject .w_MPQTAUM1)
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Quantit� maggiore del totale da scaricare")
        case   (empty(.w_CLFLCASC) or not(.w_CLFLCASC='S')) and (.w_CLQTAMOV>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La riga di riferimento � di scarico, la selezione non � corretta")
        case   (empty(.w_CLLOTART) or not(CHKLOTUB(.w_CLCODLOT,'',0,.w_CLCODMAG,.w_CODART,.w_CODFOR,.w_CLFLCASC,.w_FLSTAT,.w_LOTZOOM,.w_DATREG,' ','L','','',.F.,''))) and (g_PERLOT='S' AND .w_FLLOTT<>'N') and (.w_CLQTAMOV>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLOTART_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Lotto inesistente o non disponibile o di un altro articolo")
        case   (empty(.w_CLCODUBI) or not(CHKLOTUB(.w_CLCODUBI,' ',0,.w_CLCODMAG,.w_CODART,.w_CODFOR,.w_CLFLCASC,.w_FLSTAT,.w_UBIZOOM,.w_DATREG,' ','U','     ','     ',.t.,'N'))) and (g_PERUBI='S' AND .w_FLUBIC='S') and (.w_CLQTAMOV>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODUBI_2_6
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if .w_CLQTAMOV>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLCODMAG = this.w_CLCODMAG
    this.o_CLLOTART = this.w_CLLOTART
    this.o_CLQTAMOV = this.w_CLQTAMOV
    this.o_CLCODLOT = this.w_CLCODLOT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CLQTAMOV>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CLFLCASC=space(1)
      .w_CLCODMAG=space(5)
      .w_CLLOTART=space(20)
      .w_MAGUBI=space(5)
      .w_UBIDEF=space(20)
      .w_CLCODUBI=space(20)
      .w_UNIMIS=space(3)
      .w_CLQTAMOV=0
      .w_OLDQTAMOV=0
      .w_ARTLOT=space(20)
      .w_FLSTAT=space(1)
      .w_LOTZOOM=.f.
      .w_UBIZOOM=.f.
      .w_CLCODLOT=space(20)
      .w_DISLOT=space(5)
      .w_CLROWODL=0
      .w_VALRIG=0
      .w_LQTA=0
      .w_LQTR=0
      .w_QTAESI=0
      .w_QTAC=0
      .w_QTAS=0
      .DoRTCalc(1,3,.f.)
        .w_CLFLCASC = 'S'
        .w_CLCODMAG = this.oParentObject .w_MPCODMAG
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_CLLOTART))
        .link_2_3('Full')
      endif
        .w_MAGUBI = .w_CLCODMAG
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_MAGUBI))
        .link_2_4('Full')
      endif
      .DoRTCalc(8,8,.f.)
        .w_CLCODUBI = IIF(g_PERUBI='S' AND .w_FLUBIC='S', .w_UBIDEF, SPACE(20))
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_CLCODUBI))
        .link_2_6('Full')
      endif
        .w_UNIMIS = this.oParentObject .w_UNMIS1
      .DoRTCalc(11,11,.f.)
        .w_OLDQTAMOV = iif(this.cFunction<>'Edit' or this.rowstatus()='A',0,iif(empty(.w_OLDQTAMOV),.w_CLQTAMOV,.w_OLDQTAMOV))
      .DoRTCalc(13,15,.f.)
        .w_LOTZOOM = .F.
      .DoRTCalc(17,21,.f.)
        .w_UBIZOOM = .F.
        .w_CLCODLOT = .w_CLLOTART
      .DoRTCalc(23,23,.f.)
      if not(empty(.w_CLCODLOT))
        .link_2_16('Full')
      endif
      .DoRTCalc(24,26,.f.)
        .w_DISLOT = .w_CLFLCASC
      .DoRTCalc(27,27,.f.)
      if not(empty(.w_DISLOT))
        .link_2_17('Full')
      endif
      .DoRTCalc(28,30,.f.)
        .w_CLROWODL = this.oParentObject .w_MPROWODL
        .w_VALRIG = .w_CLQTAMOV
      .DoRTCalc(33,34,.f.)
        .w_QTAESI = .w_LQTA - .w_LQTR
      .DoRTCalc(36,38,.f.)
        .w_QTAC = IIF(.w_CLFLCASC='C', .w_CLQTAMOV, 0)
        .w_QTAS = IIF(.w_CLFLCASC='S', .w_CLQTAMOV, 0)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CLFLCASC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1.RadioValue(.t.)
    this.w_CLCODMAG = t_CLCODMAG
    this.w_CLLOTART = t_CLLOTART
    this.w_MAGUBI = t_MAGUBI
    this.w_UBIDEF = t_UBIDEF
    this.w_CLCODUBI = t_CLCODUBI
    this.w_UNIMIS = t_UNIMIS
    this.w_CLQTAMOV = t_CLQTAMOV
    this.w_OLDQTAMOV = t_OLDQTAMOV
    this.w_ARTLOT = t_ARTLOT
    this.w_FLSTAT = t_FLSTAT
    this.w_LOTZOOM = t_LOTZOOM
    this.w_CPROWNUM = t_CPROWNUM
    this.w_UBIZOOM = t_UBIZOOM
    this.w_CLCODLOT = t_CLCODLOT
    this.w_DISLOT = t_DISLOT
    this.w_CLROWODL = t_CLROWODL
    this.w_VALRIG = t_VALRIG
    this.w_LQTA = t_LQTA
    this.w_LQTR = t_LQTR
    this.w_QTAESI = t_QTAESI
    this.w_QTAC = t_QTAC
    this.w_QTAS = t_QTAS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CLFLCASC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFLCASC_2_1.ToRadio()
    replace t_CLCODMAG with this.w_CLCODMAG
    replace t_CLLOTART with this.w_CLLOTART
    replace t_MAGUBI with this.w_MAGUBI
    replace t_UBIDEF with this.w_UBIDEF
    replace t_CLCODUBI with this.w_CLCODUBI
    replace t_UNIMIS with this.w_UNIMIS
    replace t_CLQTAMOV with this.w_CLQTAMOV
    replace t_OLDQTAMOV with this.w_OLDQTAMOV
    replace t_ARTLOT with this.w_ARTLOT
    replace t_FLSTAT with this.w_FLSTAT
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_UBIZOOM with this.w_UBIZOOM
    replace t_CLCODLOT with this.w_CLCODLOT
    replace t_DISLOT with this.w_DISLOT
    replace t_CLROWODL with this.w_CLROWODL
    replace t_VALRIG with this.w_VALRIG
    replace t_LQTA with this.w_LQTA
    replace t_LQTR with this.w_LQTR
    replace t_QTAESI with this.w_QTAESI
    replace t_QTAC with this.w_QTAC
    replace t_QTAS with this.w_QTAS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTQTA = .w_TOTQTA-.w_valrig
        .w_TOTC = .w_TOTC-.w_qtac
        .w_TOTS = .w_TOTS-.w_qtas
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mloPag1 as StdContainer
  Width  = 667
  height = 221
  stdWidth  = 667
  stdheight = 221
  resizeXpos=284
  resizeYpos=94
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_8 as cp_runprogram with uid="URSTUWEHUQ",left=462, top=204, width=202,height=20,;
    caption='GSCO_BLZ',;
   bGlobalFont=.t.,;
    prg="GSCO_BLZ('CREATEROW')",;
    cEvent = "Blank,Ricalcola",;
    nPag=1;
    , HelpContextID = 192537280


  add object oBtn_1_9 as StdButton with uid="MOSCKGIKYL",left=9, top=145, width=48,height=45,;
    CpPicture="BMP\CARLOTTI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per creare un nuovo lotto con i parametri impostati";
    , HelpContextID = 59438086;
    , TabStop=.f., Caption='\<Car. Lott.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLFLCASC<>'C' Or Not (g_PERLOT='S' AND .w_FLLOTT='S'))
    endwith
   endif
  endfunc

  add object oTOTS_1_19 as StdField with uid="HMVOAPNOCE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_TOTS", cQueryName = "TOTS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 59698998,;
   bGlobalFont=.t.,;
    Height=18, Width=72, Left=309, Top=202, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oTOTS_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=1)
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=8, width=650,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CLFLCASC",Label1="Flag",Field2="CLCODMAG",Label2="Magazzino",Field3="CLLOTART",Label3="Lotto",Field4="QTAESI",Label4="<Etichetta>",Field5="CLCODUBI",Label5="Ubicazione",Field6="UNIMIS",Label6="U.M.",Field7="CLQTAMOV",Label7="Quantit�",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 166886278

  add object oStr_1_4 as StdString with uid="UMDKQDJKSU",Visible=.t., Left=501, Top=150,;
    Alignment=1, Width=37, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=27,;
    width=646+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=28,width=645+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LOTTIART|UBICAZIO|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oOLDQTAMOV_2_9.Refresh()
      this.Parent.oLQTA_2_19.Refresh()
      this.Parent.oLQTR_2_20.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LOTTIART'
        oDropInto=this.oBodyCol.oRow.oCLLOTART_2_3
      case cFile='UBICAZIO'
        oDropInto=this.oBodyCol.oRow.oCLCODUBI_2_6
    endcase
    return(oDropInto)
  EndFunc


  add object oOLDQTAMOV_2_9 as StdTrsField with uid="SNEVOLSIJW",rtseq=12,rtrep=.t.,;
    cFormVar="w_OLDQTAMOV",value=0,enabled=.f.,;
    ToolTipText = "Valore iniziale di CLQTAMOV (usata per controlli lotti quando si effettuano variazioni in edit)",;
    HelpContextID = 164360597,;
    cTotal="", bFixedPos=.t., cQueryName = "OLDQTAMOV",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=686, Top=320

  add object oLQTA_2_19 as StdTrsField with uid="GVFGFIDCKE",rtseq=33,rtrep=.t.,;
    cFormVar="w_LQTA",value=0,enabled=.f.,;
    HelpContextID = 58519734,;
    cTotal="", bFixedPos=.t., cQueryName = "LQTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=18, Width=72, Left=232, Top=222

  func oLQTA_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=1)
    endwith
    endif
  endfunc

  add object oLQTR_2_20 as StdTrsField with uid="PJGRVLMYSF",rtseq=34,rtrep=.t.,;
    cFormVar="w_LQTR",value=0,enabled=.f.,;
    HelpContextID = 59633846,;
    cTotal="", bFixedPos=.t., cQueryName = "LQTR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=18, Width=72, Left=232, Top=242

  func oLQTR_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=1)
    endwith
    endif
  endfunc

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTQTA_3_1 as StdField with uid="PIPQWVGYSV",rtseq=13,rtrep=.f.,;
    cFormVar="w_TOTQTA",value=0,enabled=.f.,;
    HelpContextID = 104009930,;
    cQueryName = "TOTQTA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=540, Top=150, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]
enddefine

* --- Defining Body row
define class tgsco_mloBodyRow as CPBodyRowCnt
  Width=636
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCLFLCASC_2_1 as StdTrsCombo with uid="XIMHTQXZUI",rtrep=.t.,;
    cFormVar="w_CLFLCASC", RowSource=""+"Carica,"+"Scarica" , ;
    ToolTipText = "Flag: C= carica; S= scarica il lotto",;
    HelpContextID = 122221719,;
    Height=21, Width=72, Left=-2, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2  , sErrorMsg = "La riga di riferimento � di scarico, la selezione non � corretta";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCLFLCASC_2_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFLCASC,&i_cF..t_CLFLCASC),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oCLFLCASC_2_1.GetRadio()
    this.Parent.oContained.w_CLFLCASC = this.RadioValue()
    return .t.
  endfunc

  func oCLFLCASC_2_1.ToRadio()
    this.Parent.oContained.w_CLFLCASC=trim(this.Parent.oContained.w_CLFLCASC)
    return(;
      iif(this.Parent.oContained.w_CLFLCASC=='C',1,;
      iif(this.Parent.oContained.w_CLFLCASC=='S',2,;
      0)))
  endfunc

  func oCLFLCASC_2_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFLCASC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CLFLCASC='S')
    endwith
    return bRes
  endfunc

  add object oCLCODMAG_2_2 as StdTrsField with uid="DKGSJYULZQ",rtseq=5,rtrep=.t.,;
    cFormVar="w_CLCODMAG",value=space(5),enabled=.f.,;
    HelpContextID = 188097683,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=71, Top=0, InputMask=replicate('X',5)

  func oCLCODMAG_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CLCODUBI)
        bRes2=.link_2_6('Full')
      endif
      if .not. empty(.w_CLCODLOT)
        bRes2=.link_2_16('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCLLOTART_2_3 as StdTrsField with uid="RIUEZOOJBZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_CLLOTART",value=space(20),;
    ToolTipText = "Codice del lotto da caricare o scaricare",;
    HelpContextID = 104174726,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Lotto inesistente o non disponibile o di un altro articolo",;
   bGlobalFont=.t.,;
    Height=17, Width=165, Left=135, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_CLLOTART"

  func oCLLOTART_2_3.mCond()
    with this.Parent.oContained
      return (g_PERLOT='S' AND .w_FLLOTT<>'N')
    endwith
  endfunc

  func oCLLOTART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLLOTART_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCLLOTART_2_3.mZoom
      with this.Parent.oContained
        do GSMD_BZL with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oCLLOTART_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_CLLOTART
    i_obj.ecpSave()
  endproc

  add object oCLCODUBI_2_6 as StdTrsField with uid="DLDQFGIMAK",rtseq=9,rtrep=.t.,;
    cFormVar="w_CLCODUBI",value=space(20),;
    ToolTipText = "Codice dell'ubicazione da caricare o scaricare",;
    HelpContextID = 53879953,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=306, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CLCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CLCODUBI"

  func oCLCODUBI_2_6.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND .w_FLUBIC='S')
    endwith
  endfunc

  func oCLCODUBI_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODUBI_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCLCODUBI_2_6.mZoom
      with this.Parent.oContained
        GSMD_BZU(this.Parent.oContained,"A")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oUNIMIS_2_7 as StdTrsField with uid="TITNKFYMEF",rtseq=10,rtrep=.t.,;
    cFormVar="w_UNIMIS",value=space(3),enabled=.f.,;
    HelpContextID = 82297274,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=45, Left=477, Top=0, InputMask=replicate('X',3)

  add object oCLQTAMOV_2_8 as StdTrsField with uid="QYBBCKSLFZ",rtseq=11,rtrep=.t.,;
    cFormVar="w_CLQTAMOV",value=0,;
    HelpContextID = 190858372,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=104, Left=527, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  proc oCLQTAMOV_2_8.mDefault
    with this.Parent.oContained
      if empty(.w_CLQTAMOV)
        .w_CLQTAMOV = .oParentObject .w_MPQTAUM1 - IIF(.w_CLFLCASC='C', .w_TOTC, .w_TOTS)
      endif
    endwith
  endproc

  add object oQTAESI_2_21 as StdTrsField with uid="FMGCMCIHYX",rtseq=35,rtrep=.t.,;
    cFormVar="w_QTAESI",value=0,enabled=.f.,;
    HelpContextID = 240139258,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=140, Top=173

  func oQTAESI_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=1)
    endwith
    endif
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCLFLCASC_2_1.When()
    return(.t.)
  proc oCLFLCASC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCLFLCASC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mlo','AVA_LOT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CLDICHIA=AVA_LOT.CLDICHIA";
  +" and "+i_cAliasName2+".CLROWCNM=AVA_LOT.CLROWCNM";
  +" and "+i_cAliasName2+".CLNUMRIF=AVA_LOT.CLNUMRIF";
  +" and "+i_cAliasName2+".CLROWDOC=AVA_LOT.CLROWDOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
