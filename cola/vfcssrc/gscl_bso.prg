* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscl_bso                                                        *
*              Separazione OCL                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_203]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-22                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscl_bso",oParentObject)
return(i_retval)

define class tgscl_bso as StdBatch
  * --- Local variables
  w_CODICE = space(15)
  w_OLDREC = 0
  w_APPO = space(15)
  w_CODART = space(20)
  w_OLSERIAL = space(15)
  w_DTIPGES = space(1)
  w_DATREG = ctod("  /  /  ")
  w_ORAREG = space(8)
  w_TmpD = ctod("  /  /  ")
  w_TmpC = space(10)
  w_ERRORE = .f.
  w_SALCOM = space(1)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_OLDATODP = ctod("  /  /  ")
  w_OLOPEODP = 0
  w_OLDATODL = ctod("  /  /  ")
  w_OLOPEODL = 0
  w_OLTSTATO = space(1)
  w_OLTCAMAG = space(5)
  w_OLTFLORD = space(1)
  w_OLTFLIMP = space(1)
  w_OLTCOMAG = space(5)
  w_OLTLEMPS = 0
  w_OLTDTMPS = ctod("  /  /  ")
  w_OLTDINRIC = ctod("  /  /  ")
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLTPERAS = space(3)
  w_OLTEMLAV = 0
  w_OLTDTLAN = ctod("  /  /  ")
  w_OLTDTINI = ctod("  /  /  ")
  w_OLTDTFIN = ctod("  /  /  ")
  w_OLTCODIC = space(20)
  w_OLTCOART = space(20)
  w_OLTUNMIS = space(3)
  w_OLTQTODL = 0
  w_OLTQTOD1 = 0
  w_OLTQTOEV = 0
  w_OLTQTOE1 = 0
  w_OLTFLEVA = space(1)
  w_OLTKEYSA = space(20)
  w_OLTQTSAL = 0
  w_OLTCOCEN = space(15)
  w_OLTVOCEN = space(15)
  w_OLTCOMME = space(15)
  w_OLTTIPAT = space(1)
  w_OLTCOATT = space(15)
  w_OLTCOCOS = space(5)
  w_OLTFCOCO = space(1)
  w_OLTFORCO = space(1)
  w_OLTFLIMC = space(1)
  w_OLTIMCOM = 0
  w_OLTPROVE = space(1)
  w_OLTTICON = space(1)
  w_OLTDISBA = space(20)
  w_OLTCICLO = space(15)
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_OLFLEVAS = space(1)
  w_OLTIPPRE = space(1)
  w_OLQTASAL = 0
  w_OLKEYSAL = space(20)
  w_OLCODICE = space(20)
  w_OLCODART = space(20)
  w_OLCODMAG = space(5)
  w_OLEVAAUT = space(1)
  w_OLRIFFAS = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_OQTSAL = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CON_MACL_idx=0
  CON_TRAD_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  SALDIART_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Separazione OCL (da GSCL_BET)
    * --- ODL_MAST
    this.w_ERRORE = .F.
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    this.w_OLTCODIC = SPACE(20)
    * --- Legge codice ODL di Origine
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            OLCODODL = this.oParentObject.w_OLCODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLDATODP = NVL(cp_ToDate(_read_.OLDATODP),cp_NullValue(_read_.OLDATODP))
      this.w_OLOPEODP = NVL(cp_ToDate(_read_.OLOPEODP),cp_NullValue(_read_.OLOPEODP))
      this.w_OLDATODL = NVL(cp_ToDate(_read_.OLDATODL),cp_NullValue(_read_.OLDATODL))
      this.w_OLOPEODL = NVL(cp_ToDate(_read_.OLOPEODL),cp_NullValue(_read_.OLOPEODL))
      this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
      this.w_OLTCAMAG = NVL(cp_ToDate(_read_.OLTCAMAG),cp_NullValue(_read_.OLTCAMAG))
      this.w_OLTFLORD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
      this.w_OLTFLIMP = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
      this.w_OLTCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
      this.w_OLTLEMPS = NVL(cp_ToDate(_read_.OLTLEMPS),cp_NullValue(_read_.OLTLEMPS))
      this.w_OLTDTMPS = NVL(cp_ToDate(_read_.OLTDTMPS),cp_NullValue(_read_.OLTDTMPS))
      this.w_OLTDINRIC = NVL(cp_ToDate(_read_.OLTDINRIC),cp_NullValue(_read_.OLTDINRIC))
      this.w_OLTDTRIC = NVL(cp_ToDate(_read_.OLTDTRIC),cp_NullValue(_read_.OLTDTRIC))
      this.w_OLTPERAS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
      this.w_OLTEMLAV = NVL(cp_ToDate(_read_.OLTEMLAV),cp_NullValue(_read_.OLTEMLAV))
      this.w_OLTDTLAN = NVL(cp_ToDate(_read_.OLTDTLAN),cp_NullValue(_read_.OLTDTLAN))
      this.w_OLTDTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
      this.w_OLTDTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
      this.w_OLTCODIC = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
      this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
      this.w_OLTUNMIS = NVL(cp_ToDate(_read_.OLTUNMIS),cp_NullValue(_read_.OLTUNMIS))
      this.w_OLTQTODL = NVL(cp_ToDate(_read_.OLTQTODL),cp_NullValue(_read_.OLTQTODL))
      this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
      this.w_OLTQTOEV = NVL(cp_ToDate(_read_.OLTQTOEV),cp_NullValue(_read_.OLTQTOEV))
      this.w_OLTQTOE1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
      this.w_OLTFLEVA = NVL(cp_ToDate(_read_.OLTFLEVA),cp_NullValue(_read_.OLTFLEVA))
      this.w_OLTKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
      this.w_OLTQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
      this.w_OLTCOCEN = NVL(cp_ToDate(_read_.OLTCOCEN),cp_NullValue(_read_.OLTCOCEN))
      this.w_OLTVOCEN = NVL(cp_ToDate(_read_.OLTVOCEN),cp_NullValue(_read_.OLTVOCEN))
      this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
      this.w_OLTTIPAT = NVL(cp_ToDate(_read_.OLTTIPAT),cp_NullValue(_read_.OLTTIPAT))
      this.w_OLTCOATT = NVL(cp_ToDate(_read_.OLTCOATT),cp_NullValue(_read_.OLTCOATT))
      this.w_OLTCOCOS = NVL(cp_ToDate(_read_.OLTCOCOS),cp_NullValue(_read_.OLTCOCOS))
      this.w_OLTFCOCO = NVL(cp_ToDate(_read_.OLTFCOCO),cp_NullValue(_read_.OLTFCOCO))
      this.w_OLTFORCO = NVL(cp_ToDate(_read_.OLTFORCO),cp_NullValue(_read_.OLTFORCO))
      this.w_OLTFLIMC = NVL(cp_ToDate(_read_.OLTFLIMC),cp_NullValue(_read_.OLTFLIMC))
      this.w_OLTIMCOM = NVL(cp_ToDate(_read_.OLTIMCOM),cp_NullValue(_read_.OLTIMCOM))
      this.w_OLTPROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
      this.w_OLTTICON = NVL(cp_ToDate(_read_.OLTTICON),cp_NullValue(_read_.OLTTICON))
      this.w_OLTDISBA = NVL(cp_ToDate(_read_.OLTDISBA),cp_NullValue(_read_.OLTDISBA))
      this.w_OLTCICLO = NVL(cp_ToDate(_read_.OLTCICLO),cp_NullValue(_read_.OLTCICLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DATREG = IIF(EMPTY(this.w_OLDATODP), i_DATSYS, this.w_OLDATODP)
    this.w_ORAREG = TIME()
    if NOT EMPTY(this.w_OLTCODIC)
      * --- Try
      local bErr_02B5B820
      bErr_02B5B820=bTrsErr
      this.Try_02B5B820()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_ERRORE = .T.
        ah_ErrorMsg("Impossibile separare OCL di origine","STOP","")
      endif
      bTrsErr=bTrsErr or bErr_02B5B820
      * --- End
    endif
  endproc
  proc Try_02B5B820()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Ordini OCL (ODL_MAST)
    * --- Assegna Progressivo OCL
    this.w_OLSERIAL = SPACE(15)
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    cp_NextTableProg(this, i_nConn, "PRODL", "i_CODAZI,w_OLSERIAL")
    * --- Se tra i due fornitori cambia il LT, ricalcola data inizio lavorazione e data impegno materiali
    if this.oParentObject.w_LEADPAR<>this.oParentObject.w_COGIOAPP
      * --- Nuovo LT
      this.w_DTIPGES = " "
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPGES"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPGES;
          from (i_cTable) where;
              ARCODART = this.w_OLTCOART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_OLTEMLAV = this.oParentObject.w_COGIOAPP
      this.w_OLTDTRIC = IIF(this.w_DTIPGES="S", this.w_DATREG + this.w_OLTEMLAV, this.w_OLTDTRIC)
      this.w_OLTDINRIC = IIF(this.w_DTIPGES="S", this.w_DATREG, this.w_OLTDTRIC - this.w_OLTEMLAV)
      this.w_OLTDTMPS = this.w_OLTDINRIC
    endif
    if this.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR)
      * --- Legge Eventuali Materiali del Terzista
      vq_exec ("..\COLA\EXE\QUERY\GSCO_QMT", this, "_MatTer_")
    endif
    * --- Insert into ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",OLDATODP"+",OLOPEODP"+",OLTSTATO"+",OLTCAMAG"+",OLTFLORD"+",OLTFLIMP"+",OLTCOMAG"+",OLTLEMPS"+",OLTDTMPS"+",OLTDINRIC"+",OLTDTRIC"+",OLTPERAS"+",OLTEMLAV"+",OLTCODIC"+",OLTCOART"+",OLTUNMIS"+",OLTQTODL"+",OLTQTOD1"+",OLTQTOEV"+",OLTQTOE1"+",OLTFLEVA"+",OLTKEYSA"+",OLTQTSAL"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",OLTCOCEN"+",OLTVOCEN"+",OLTCOMME"+",OLTTIPAT"+",OLTCOATT"+",OLTCOCOS"+",OLTFCOCO"+",OLTFORCO"+",OLTFLIMC"+",OLTIMCOM"+",OLTPROVE"+",OLTTICON"+",OLTCOFOR"+",OLTSEDOC"+",OLSTAODL"+",OLTCONTR"+",OLTDISBA"+",OLTCICLO"+",OLDATODL"+",OLOPEODL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_MAST','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLDATODP),'ODL_MAST','OLDATODP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLOPEODP),'ODL_MAST','OLOPEODP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCAMAG),'ODL_MAST','OLTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLORD),'ODL_MAST','OLTFLORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMP),'ODL_MAST','OLTFLIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'ODL_MAST','OLTCOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTLEMPS),'ODL_MAST','OLTLEMPS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDTMPS),'ODL_MAST','OLTDTMPS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDINRIC),'ODL_MAST','OLTDINRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDTRIC),'ODL_MAST','OLTDTRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPERAS),'ODL_MAST','OLTPERAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTEMLAV),'ODL_MAST','OLTEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_MAST','OLTCODIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOART),'ODL_MAST','OLTCOART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTUNMIS),'ODL_MAST','OLTUNMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QUAOUT),'ODL_MAST','OLTQTODL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QUAOUT1),'ODL_MAST','OLTQTOD1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOEV),'ODL_MAST','OLTQTOEV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOE1),'ODL_MAST','OLTQTOE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_MAST','OLTFLEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_MAST','OLTKEYSA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QUAOUT1),'ODL_MAST','OLTQTSAL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'ODL_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOCEN),'ODL_MAST','OLTCOCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTVOCEN),'ODL_MAST','OLTVOCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'ODL_MAST','OLTCOMME');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOATT),'ODL_MAST','OLTCOATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOCOS),'ODL_MAST','OLTCOCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFCOCO),'ODL_MAST','OLTFCOCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFORCO),'ODL_MAST','OLTFORCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMC),'ODL_MAST','OLTFLIMC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIMCOM),'ODL_MAST','OLTIMCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOFOR),'ODL_MAST','OLTCOFOR');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ODL_MAST','OLTSEDOC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLSTAODL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCONTR),'ODL_MAST','OLTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDISBA),'ODL_MAST','OLTDISBA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCICLO),'ODL_MAST','OLTCICLO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLDATODL),'ODL_MAST','OLDATODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLOPEODL),'ODL_MAST','OLOPEODL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'OLDATODP',this.w_OLDATODP,'OLOPEODP',this.w_OLOPEODP,'OLTSTATO',this.w_OLTSTATO,'OLTCAMAG',this.w_OLTCAMAG,'OLTFLORD',this.w_OLTFLORD,'OLTFLIMP',this.w_OLTFLIMP,'OLTCOMAG',this.w_OLTCOMAG,'OLTLEMPS',this.w_OLTLEMPS,'OLTDTMPS',this.w_OLTDTMPS,'OLTDINRIC',this.w_OLTDINRIC,'OLTDTRIC',this.w_OLTDTRIC)
      insert into (i_cTable) (OLCODODL,OLDATODP,OLOPEODP,OLTSTATO,OLTCAMAG,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTLEMPS,OLTDTMPS,OLTDINRIC,OLTDTRIC,OLTPERAS,OLTEMLAV,OLTCODIC,OLTCOART,OLTUNMIS,OLTQTODL,OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTFLEVA,OLTKEYSA,OLTQTSAL,UTCC,UTDC,UTCV,UTDV,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTTIPAT,OLTCOATT,OLTCOCOS,OLTFCOCO,OLTFORCO,OLTFLIMC,OLTIMCOM,OLTPROVE,OLTTICON,OLTCOFOR,OLTSEDOC,OLSTAODL,OLTCONTR,OLTDISBA,OLTCICLO,OLDATODL,OLOPEODL &i_ccchkf. );
         values (;
           this.w_OLSERIAL;
           ,this.w_OLDATODP;
           ,this.w_OLOPEODP;
           ,this.w_OLTSTATO;
           ,this.w_OLTCAMAG;
           ,this.w_OLTFLORD;
           ,this.w_OLTFLIMP;
           ,this.w_OLTCOMAG;
           ,this.w_OLTLEMPS;
           ,this.w_OLTDTMPS;
           ,this.w_OLTDINRIC;
           ,this.w_OLTDTRIC;
           ,this.w_OLTPERAS;
           ,this.w_OLTEMLAV;
           ,this.w_OLTCODIC;
           ,this.w_OLTCOART;
           ,this.w_OLTUNMIS;
           ,this.oParentObject.w_QUAOUT;
           ,this.oParentObject.w_QUAOUT1;
           ,this.w_OLTQTOEV;
           ,this.w_OLTQTOE1;
           ,this.w_OLTFLEVA;
           ,this.w_OLTKEYSA;
           ,this.oParentObject.w_QUAOUT1;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_OLTCOCEN;
           ,this.w_OLTVOCEN;
           ,this.w_OLTCOMME;
           ,"A";
           ,this.w_OLTCOATT;
           ,this.w_OLTCOCOS;
           ,this.w_OLTFCOCO;
           ,this.w_OLTFORCO;
           ,this.w_OLTFLIMC;
           ,this.w_OLTIMCOM;
           ,this.w_OLTPROVE;
           ,"F";
           ,this.oParentObject.w_OLTCOFOR;
           ,SPACE(10);
           ," ";
           ,this.oParentObject.w_OLTCONTR;
           ,this.w_OLTDISBA;
           ,this.w_OLTCICLO;
           ,this.w_OLDATODL;
           ,this.w_OLOPEODL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento ODL_MAST (2)'
      return
    endif
    * --- Aggiorna Saldi OCL Destinazione
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.oParentObject.w_QUAOUT1',this.oParentObject.w_QUAOUT1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.oParentObject.w_QUAOUT1',this.oParentObject.w_QUAOUT1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_OLTKEYSA;
          and SLCODMAG = this.w_OLTCOMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_OLTCOART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_SALCOM="S"
      if empty(nvl(this.w_OLTCOMME,""))
        this.w_COMMAPPO = this.w_COMMDEFA
      else
        this.w_COMMAPPO = this.w_OLTCOMME
      endif
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SCQTOPER','this.oParentObject.w_QUAOUT1',this.oParentObject.w_QUAOUT1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SCQTIPER','this.oParentObject.w_QUAOUT1',this.oParentObject.w_QUAOUT1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
            +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
               )
      else
        update (i_cTable) set;
            SCQTOPER = &i_cOp1.;
            ,SCQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_OLTKEYSA;
            and SCCODMAG = this.w_OLTCOMAG;
            and SCCODCAN = this.w_COMMAPPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore Aggiornamento Saldi Commessa'
        return
      endif
    endif
    * --- Cicla su ODL_DETT di Origine
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    * --- Select from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
          +" where OLCODODL="+cp_ToStrODBC(this.oParentObject.w_OLCODODL)+"";
           ,"_Curs_ODL_DETT")
    else
      select * from (i_cTable);
       where OLCODODL=this.oParentObject.w_OLCODODL;
        into cursor _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      select _Curs_ODL_DETT
      locate for 1=1
      do while not(eof())
      this.w_OQTSAL = NVL(_Curs_ODL_DETT.OLQTASAL, 0)
      this.w_OLCODICE = NVL(_Curs_ODL_DETT.OLCODICE, SPACE(20))
      this.w_OLCODART = NVL(_Curs_ODL_DETT.OLCODART, SPACE(20))
      this.w_OLCODMAG = NVL(_Curs_ODL_DETT.OLCODMAG, " ")
      this.w_OLKEYSAL = NVL(_Curs_ODL_DETT.OLKEYSAL, " ")
      this.w_OLQTAMOV = this.oParentObject.w_QUAIN * NVL(_Curs_ODL_DETT.OLCOEIMP, 0)
      this.w_OLQTAUM1 = this.oParentObject.w_QUAIN1 * NVL(_Curs_ODL_DETT.OLCOEIMP, 0)
      this.w_OLRIFFAS = NVL(_Curs_ODL_DETT.OLRIFFAS, 0)
      * --- Si prevede Evaso Totalmente o Niente
      this.w_OLFLEVAS = NVL(_Curs_ODL_DETT.OLFLEVAS," ")
      this.w_OLQTAEVA = IIF(this.w_OLFLEVAS="S", this.w_OLQTAMOV, 0)
      this.w_OLQTAEV1 = IIF(this.w_OLFLEVAS="S", this.w_OLQTAUM1, 0)
      this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S", 0, this.w_OLQTAUM1)
      * --- Aggiorna nuove Quantita' su dettaglio Origine
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
        +",OLQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
        +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
        +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
        +",OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
               )
      else
        update (i_cTable) set;
            OLQTAMOV = this.w_OLQTAMOV;
            ,OLQTAUM1 = this.w_OLQTAUM1;
            ,OLQTASAL = this.w_OLQTASAL;
            ,OLQTAEV1 = this.w_OLQTAEV1;
            ,OLQTAEVA = this.w_OLQTAEVA;
            &i_ccchkf. ;
         where;
            OLCODODL = _Curs_ODL_DETT.OLCODODL;
            and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Saldi OCL Origine
      this.w_OLQTASAL = this.w_OLQTASAL-this.w_OQTSAL
      if NOT EMPTY(this.w_OLKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_OLQTASAL);
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTIPER = SLQTIPER + this.w_OLQTASAL;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_OLKEYSAL;
              and SLCODMAG = this.w_OLCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_OLCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_OLCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_OLTCOMME,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_OLTCOMME
          endif
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_OLQTASAL);
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTIPER = SCQTIPER + this.w_OLQTASAL;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_OLKEYSAL;
                and SCCODMAG = this.w_OLCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
      * --- Inserisce nuovo ODL_DETT
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_OLQTAMOV = this.oParentObject.w_QUAOUT * NVL(_Curs_ODL_DETT.OLCOEIMP, 0)
      this.w_OLQTAUM1 = this.oParentObject.w_QUAOUT1 * NVL(_Curs_ODL_DETT.OLCOEIMP, 0)
      this.w_OLTIPPRE = " "
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPPRE"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_OLCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPPRE;
          from (i_cTable) where;
              ARCODART = this.w_OLCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OLTIPPRE = NVL(cp_ToDate(_read_.ARTIPPRE),cp_NullValue(_read_.ARTIPPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_OLTIPPRE = IIF(this.w_OLTIPPRE $ "AM", this.w_OLTIPPRE, "N")
      this.w_OLFLEVAS = " "
      this.w_OLEVAAUT = " "
      if this.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR) AND USED("_MatTer_")
        * --- Legge Eventuali Materiali del Terzista
        i_nOldArea=select()
        select _MatTer_
        go top
        LOCATE FOR NVL(MCCODICE, SPACE(20))=this.w_OLCODICE
        if FOUND()
          * --- Se il componente e' presente nei materiali del Terzista non deve eseguire alcun Impegno ne Trasferimento
          this.w_OLFLEVAS = "S"
          this.w_OLTIPPRE = "N"
          this.w_OLEVAAUT = "S"
        endif
        select (i_nOldArea)
      endif
      * --- Si prevede Evaso Totalmente o Niente
      this.w_OLQTAEVA = IIF(this.w_OLFLEVAS="S", this.w_OLQTAMOV, 0)
      this.w_OLQTAEV1 = IIF(this.w_OLFLEVAS="S", this.w_OLQTAUM1, 0)
      this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S", 0, this.w_OLQTAUM1)
      if this.w_OLQTAMOV<>0
        * --- Try
        local bErr_04127C10
        bErr_04127C10=bTrsErr
        this.Try_04127C10()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04127C10
        * --- End
        if NOT EMPTY(this.w_OLKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_OLQTASAL);
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = SLQTIPER + this.w_OLQTASAL;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLCODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_ODL_DETT
        continue
      enddo
      use
    endif
    if this.oParentObject.w_QUAIN=0
      * --- Se trasferisce tutto, Elimina OCL di Origine
      * --- Delete from ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
               )
      else
        delete from (i_cTable) where;
              OLCODODL = this.oParentObject.w_OLCODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione ODL_DETT (2)'
        return
      endif
      * --- Delete from ODL_MAST
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
               )
      else
        delete from (i_cTable) where;
              OLCODODL = this.oParentObject.w_OLCODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione ODL_MAST (2)'
        return
      endif
    else
      * --- Aggiorna testata OCL di Origine
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTQTODL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QUAIN),'ODL_MAST','OLTQTODL');
        +",OLTQTOD1 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QUAIN1),'ODL_MAST','OLTQTOD1');
        +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QUAIN1),'ODL_MAST','OLTQTSAL');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
               )
      else
        update (i_cTable) set;
            OLTQTODL = this.oParentObject.w_QUAIN;
            ,OLTQTOD1 = this.oParentObject.w_QUAIN1;
            ,OLTQTSAL = this.oParentObject.w_QUAIN1;
            &i_ccchkf. ;
         where;
            OLCODODL = this.oParentObject.w_OLCODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore in scrittura ODL_MAST (1)'
        return
      endif
    endif
    * --- Aggiorna Saldi OCL Origine
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.oParentObject.w_QUAIN1-this.w_OLTQTOD1',this.oParentObject.w_QUAIN1-this.w_OLTQTOD1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.oParentObject.w_QUAIN1-this.w_OLTQTOD1',this.oParentObject.w_QUAIN1-this.w_OLTQTOD1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_OLTKEYSA;
          and SLCODMAG = this.w_OLTCOMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if USED("_MatTer_")
      select _MatTer_
      USE
    endif
  endproc
  proc Try_04127C10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLFLORDI"+",OLFLIMPE"+",OLFLRISE"+",OLCODMAG"+",OLCODICE"+",OLCODART"+",OLUNIMIS"+",OLCOEIMP"+",OLQTAMOV"+",OLQTAUM1"+",OLQTAEVA"+",OLQTAEV1"+",OLFLEVAS"+",OLKEYSAL"+",OLQTASAL"+",OLQTAPRE"+",OLQTAPR1"+",OLFLPREV"+",OLDATRIC"+",OLMAGPRE"+",OLMAGWIP"+",OLTIPPRE"+",OLRIFDOC"+",OLROWDOC"+",OLRIFFAS"+",OLEVAAUT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_DETT','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ODL_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ODL_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCAUMAG),'ODL_DETT','OLCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLORDI),'ODL_DETT','OLFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLIMPE),'ODL_DETT','OLFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLRISE),'ODL_DETT','OLFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'ODL_DETT','OLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODICE),'ODL_DETT','OLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'ODL_DETT','OLCODART');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLUNIMIS),'ODL_DETT','OLUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCOEIMP),'ODL_DETT','OLCOEIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'ODL_DETT','OLKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLPREV),'ODL_DETT','OLFLPREV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLDATRIC),'ODL_DETT','OLDATRIC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLMAGPRE),'ODL_DETT','OLMAGPRE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLMAGWIP),'ODL_DETT','OLMAGWIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ODL_DETT','OLRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLROWDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLRIFFAS),'ODL_DETT','OLRIFFAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'OLCAUMAG',_Curs_ODL_DETT.OLCAUMAG,'OLFLORDI',_Curs_ODL_DETT.OLFLORDI,'OLFLIMPE',_Curs_ODL_DETT.OLFLIMPE,'OLFLRISE',_Curs_ODL_DETT.OLFLRISE,'OLCODMAG',this.w_OLCODMAG,'OLCODICE',this.w_OLCODICE,'OLCODART',this.w_OLCODART,'OLUNIMIS',_Curs_ODL_DETT.OLUNIMIS,'OLCOEIMP',_Curs_ODL_DETT.OLCOEIMP)
      insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLFLORDI,OLFLIMPE,OLFLRISE,OLCODMAG,OLCODICE,OLCODART,OLUNIMIS,OLCOEIMP,OLQTAMOV,OLQTAUM1,OLQTAEVA,OLQTAEV1,OLFLEVAS,OLKEYSAL,OLQTASAL,OLQTAPRE,OLQTAPR1,OLFLPREV,OLDATRIC,OLMAGPRE,OLMAGWIP,OLTIPPRE,OLRIFDOC,OLROWDOC,OLRIFFAS,OLEVAAUT &i_ccchkf. );
         values (;
           this.w_OLSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,_Curs_ODL_DETT.OLCAUMAG;
           ,_Curs_ODL_DETT.OLFLORDI;
           ,_Curs_ODL_DETT.OLFLIMPE;
           ,_Curs_ODL_DETT.OLFLRISE;
           ,this.w_OLCODMAG;
           ,this.w_OLCODICE;
           ,this.w_OLCODART;
           ,_Curs_ODL_DETT.OLUNIMIS;
           ,_Curs_ODL_DETT.OLCOEIMP;
           ,this.w_OLQTAMOV;
           ,this.w_OLQTAUM1;
           ,this.w_OLQTAEVA;
           ,this.w_OLQTAEV1;
           ,this.w_OLFLEVAS;
           ,this.w_OLKEYSAL;
           ,this.w_OLQTASAL;
           ,0;
           ,0;
           ,_Curs_ODL_DETT.OLFLPREV;
           ,_Curs_ODL_DETT.OLDATRIC;
           ,_Curs_ODL_DETT.OLMAGPRE;
           ,_Curs_ODL_DETT.OLMAGWIP;
           ,this.w_OLTIPPRE;
           ,SPACE(10);
           ,0;
           ,this.w_OLRIFFAS;
           ,this.w_OLEVAAUT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento ODL_DETT (2)'
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_MACL'
    this.cWorkTables[3]='CON_TRAD'
    this.cWorkTables[4]='ODL_DETT'
    this.cWorkTables[5]='ODL_MAST'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='SALDICOM'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
