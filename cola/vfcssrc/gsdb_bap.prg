* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bap                                                        *
*              Generazione ATP - PAB                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-19                                                      *
* Last revis.: 2017-01-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pCodice
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bap",oParentObject,m.pAzione,m.pCodice)
return(i_retval)

define class tgsdb_bap as StdBatch
  * --- Local variables
  pAzione = space(1)
  pCodice = space(20)
  PunPAD = .NULL.
  GRID = .NULL.
  cCursor = space(10)
  NumPeriodi = 0
  NumMaxPeriodi = 0
  PrimaColPer = 0
  cPERDTF = space(3)
  cONHAND = 0
  cPREVIS = 0
  cORDINI = 0
  cRESPRE = 0
  cORDFOR = 0
  cORDMPS = 0
  cMPSTOT = 0
  cPAB = 0
  cATP = 0
  cATPCUM = 0
  cPERASS = space(3)
  cDOMANDA = 0
  nPERDTF = 0
  cCURKEY = space(40)
  w_NUMROW = 0
  i = 0
  TmpC = space(10)
  TmpN = 0
  TmpC1 = space(10)
  TmpD = ctod("  /  /  ")
  r = 0
  indDTF = 0
  TmpN1 = 0
  cSUMCUMRES = 0
  cPERMPS = space(3)
  * --- WorkFile variables
  MPS_TPER_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce PAB e ATP del codice passato come parametro
    * --- Flag Azione (G=Genera ODP, C=Sistema Colore, R=Ricostruisce Saldo)
    * --- Codice (saldi) = Vuoto => tutti
    * --- Parametri produzione
    this.PunPAD = this.oParentObject
    this.GRID = this.PunPAD.w_ZoomMPS.GRD
    this.cCursor = this.PunPad.w_ZoomMPS.cCursor
    this.NumPeriodi = 100
    * --- MAX 100
    this.NumMaxPeriodi = 1+ this.NumPeriodi +1
    this.PrimaColPer = 2
    this.cPERDTF = this.oParentObject.w_PERDTF
    do case
      case this.pAzione="Intestazione"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione="G" or this.pAzione="Azzera"
        if empty(this.pCodice) or this.pAzione="Azzera"
          * --- Azzera il cursore
          select (this.cCursor)
          zap
          this.GRID.refresh()     
          this.oParentObject.w_ONHAND = 0
        else
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione RIGENERATIVA (rilegge tutto dal database)
    * --- Legge dato giacenza fisica
    ah_msg("Lettura dati giacenze in corso...")
    vq_exec ("..\COLA\exe\query\GSDB0BAP", this, "_TempGiac_")
    select _TempGiac_
    Locate for KEYSAL=this.pCodice
    if found()
      this.cONHAND = nvl(_TempGiac_.QTASAL,0)
    else
      this.cONHAND = 0
    endif
    * --- Composizione DOMANDA (Ordini + Previsioni)
    * --- Cursore con i dati IM da documenti (SOLO MAGAZZINI PRODUZIONE)
    ah_msg("Lettura ordini in corso...")
    vq_exec ("..\COLA\exe\query\GSDB1BAP", this, "_TempDocu_")
    * --- Cursore con i dati da previsoni di vendita
    ah_msg("Lettura previsioni di vendita in corso...")
    vq_exec ("..\COLA\exe\query\GSDB5BAP", this, "_TempPrev_")
    * --- Cursore con i dati IM di produzione
    ah_msg("Lettura ordini di produzione in corso...")
    vq_exec ("..\COLA\exe\query\GSDB3BAP", this, "_TempODP_")
    * --- Unisce Cursori
    select PERASS, ORDINI*0 as PREVIS, ORDINI, ORDFOR, ORDINI*0 as MPSTOT ; 
 from _TempDocu_ ; 
 union ALL select PERASS, MPSTOT*0 as PREVIS, MPSTOT*0 as ORDINI, MPSTOT*0 as ORDFOR, MPSTOT ; 
 from _TempODP_ ; 
 union ALL select PERASS, PREVIS, PREVIS*0 AS ORDINI, PREVIS*0 AS ORDFOR, PREVIS*0 AS MPSTOT ; 
 from _TempPrev_ ; 
 into cursor __Temp__
    * --- Tiene conto del DTF (Demand Time Fence)
    this.cPERDTF = "000"
    if this.oParentObject.w_PPTIPDTF="F" or (this.oParentObject.w_PPTIPDTF<>"F" AND this.oParentObject.w_PRTIPDTF="E")
      if this.oParentObject.w_PP___DTF>0
        * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
        this.TmpD = COCALCLT(i_DATSYS,this.oParentObject.w_PP___DTF,"A",TRUE,"")
        if not empty(this.TmpD)
          * --- Select from MPS_TPER
          i_nConn=i_TableProp[this.MPS_TPER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
                +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" and "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
                 ,"_Curs_MPS_TPER")
          else
            select * from (i_cTable);
             where TPDATINI<=this.TmpD and this.TmpD<=TPDATFIN;
              into cursor _Curs_MPS_TPER
          endif
          if used('_Curs_MPS_TPER')
            select _Curs_MPS_TPER
            locate for 1=1
            do while not(eof())
            this.cPERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
              select _Curs_MPS_TPER
              continue
            enddo
            use
          endif
        endif
      endif
    else
      * --- Articolo
      if this.oParentObject.w_PRGIODTF>0
        * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
        this.TmpD = COCALCLT(i_DATSYS,this.oParentObject.w_PRGIODTF,"A",TRUE,"")
        * --- Select from MPS_TPER
        i_nConn=i_TableProp[this.MPS_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select TPPERASS  from "+i_cTable+" MPS_TPER ";
              +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" AND "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
               ,"_Curs_MPS_TPER")
        else
          select TPPERASS from (i_cTable);
           where TPDATINI<=this.TmpD AND this.TmpD<=TPDATFIN;
            into cursor _Curs_MPS_TPER
        endif
        if used('_Curs_MPS_TPER')
          select _Curs_MPS_TPER
          locate for 1=1
          do while not(eof())
          this.cPERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
            select _Curs_MPS_TPER
            continue
          enddo
          use
        endif
      endif
    endif
    * --- Somme ....
    select PERASS, sum(PREVIS), sum(ORDINI), sum(ORDINI), sum(ORDFOR), sum(ORDFOR), ; 
 sum(ORDINI), sum(MPSTOT), sum(MPSTOT), sum(MPSTOT), sum(MPSTOT) ; 
 from __Temp__ group by PERASS order by PERASS ; 
 into array Globale
    * --- 1 = PERASS
    * --- 2 = PREVIS
    * --- 3 = ORDINI
    * --- 4 = RESPRE
    * --- 5 = DOMANDA
    * --- 6 = ORDFOR
    * --- 7 = ORDMPS
    * --- 8 = MPSTOT
    * --- 9 = PAB
    * --- 10 = ATP
    * --- 11 = ATPCUM
    * --- Chiude Cursori
    if used("__Temp__")
      USE IN __Temp__
    endif
    if used("_TempDocu_")
      USE IN _TempDocu_
    endif
    if used("_TempODP_")
      USE IN _TempODP_
    endif
    if used("_TempPrev_")
      USE IN _TempPrev_
    endif
    if used("_TempGiac_")
      USE IN _TempGiac_
    endif
    if type("Globale")="U"
      select (this.cCursor)
      zap
      i_retcode = 'stop'
      return
    endif
    this.w_NUMROW = alen(Globale,1)
    this.i = this.w_NUMROW
    do while this.i>=2
      this.cPERASS = globale(this.i,1)
      this.cPREVIS = globale(this.i,2)
      this.cORDINI = globale(this.i,3)
      * --- Previsioni, Residuo Previsionale e Domanda
      if this.cPERASS>=this.cPERDTF
        this.cRESPRE = this.cPREVIS - this.cORDINI - this.cSUMCUMRES
        if this.cRESPRE<0
          this.cSUMCUMRES = -this.cRESPRE
          this.cRESPRE = 0
        else
          this.cSUMCUMRES = 0
        endif
        this.cDOMANDA = this.cRESPRE + this.cORDINI
      else
        this.cPREVIS = -1
        this.cRESPRE = -1
        this.cDOMANDA = this.cORDINI
      endif
      globale(this.i,2) = this.cPREVIS
      globale(this.i,4) = this.cRESPRE
      globale(this.i,5) = this.cDOMANDA
      * --- Calcola la somma degli ordini per ogni periodo dove MPSTOT>0 o ORDFOR>0
      if Globale(this.i,8)=0 and Globale(this.i,6)=0
        globale(this.i,7)=0
        this.cORDMPS = this.cORDMPS + this.cORDINI
      else
        globale(this.i,7) = this.cORDMPS + this.cORDINI
        this.cORDMPS = 0
      endif
      this.i = this.i - 1
    enddo
    if globale(1,1)="000"
      globale(1,2) = -1
      globale(this.i,4) = -1
    endif
    globale(this.i,5) = globale(this.i,3)
    globale(1,7) = this.cORDMPS + globale(1,3)
    * --- PAB Iniziale
    this.cPAB = this.cONHAND
    this.oParentObject.w_ONHAND = this.cONHAND
    * --- Calcola PAB, ATP e ATPCUM
    this.i = 1
    do while this.i<=this.w_NUMROW
      * --- Dati periodo
      this.cDOMANDA = Globale(this.i,5)
      this.cORDFOR = nvl(Globale(this.i,6), 0)
      this.cORDMPS = nvl(Globale(this.i,7), 0)
      this.cMPSTOT = nvl(Globale(this.i,8), 0)
      this.cPAB = this.cPAB + this.cMPSTOT + this.cORDFOR - this.cDOMANDA
      this.cATP = this.cMPSTOT + this.cORDFOR - this.cORDMPS + iif(this.i=1,this.cONHAND,0)
      this.cATPCUM = this.cATPCUM +this.cATP
      globale(this.i,9) = this.cPAB
      globale(this.i,10) = this.cATP
      globale(this.i,11) = this.cATPCUM
      this.i = this.i + 1
    enddo
    this.nPERDTF = int(val(this.cPERDTF))+this.PrimaColPer-1
    * --- Azzera il cursore
    select (this.cCursor)
    zap
    scatter to cur
    * --- Inserisce la trasposta di Globale nel cursore
    this.r = 2
    do while this.r<=11
      do case
        case this.r = 2
          cur(1) = "Previsioni"
          this.indDTF = this.PrimaColPer
          do while this.indDTF<=this.nPERDTF
            cur(this.indDTF) = -1
            this.indDTF = this.indDTF+1
          enddo
        case this.r = 3
          cur(1) = "Ordini cliente"
          this.indDTF = this.PrimaColPer
          do while this.indDTF<=this.nPERDTF
            cur(this.indDTF) = 0
            this.indDTF = this.indDTF+1
          enddo
        case this.r=4
          cur(1) = "Domanda"
          this.r = 5
        case this.r=6
          cur(1) = "Ordini fornitore"
        case this.r=7
          cur(1) = "MPS totale"
          this.r = 8
        case this.r=9
          cur(1) = "PAB"
        case this.r=10
          cur(1) = "ATP"
        case this.r=11
          cur(1) = "ATP cumulato"
      endcase
      this.i = 1
      do while this.i<=this.w_NUMROW
        this.TmpN = int(val(Globale(this.i,1)))+2
        cur(this.TmpN) = Globale(this.i,this.r)
        * --- Riporta ATP cumulato (ON-HAND)
        if this.r>=9 and this.i=1
          this.TmpN1 = this.TmpN - 1
          do while this.TmpN1 > 1
            do case
              case this.r=9
                cur(this.TmpN1) = this.cONHAND
              case this.r=10
                if this.TmpN1=2
                  cur(this.TmpN1) = min((this.cONHAND-Globale(1,3)+Globale(1,8)),this.cONHAND)
                else
                  cur(this.TmpN1) = this.cONHAND*0
                endif
              case this.r=11
                cur(this.TmpN1) = min(Globale(1,11),this.cONHAND)
            endcase
            this.TmpN1 = this.TmpN1 - 1
          enddo
        endif
        * --- Riporta ATP e PAB
        if this.r>=9 and this.i<=this.w_NUMROW
          this.TmpN1 = iif(this.i=this.w_NUMROW, this.oParentObject.w_NUMPER+3, int(val(Globale(this.i+1,1)))+2)
          this.TmpN = this.TmpN + 1
          do while this.TmpN < this.TmpN1
            cur(this.TmpN) = iif(this.r=10,0,Globale(this.i,this.r))
            this.TmpN = this.TmpN + 1
          enddo
        endif
        this.i = this.i + 1
      enddo
      append blank
      gather from cur
      this.r = this.r + 1
    enddo
    go top
    this.cPERMPS = "000"
    if this.oParentObject.w_LEAMPS>0
      * --- Determina l'intervallo fino al LT-MPS
      this.TmpD = COCALCLT(i_DATSYS,this.oParentObject.w_LEAMPS,"A",FALSE,"")
      if not empty(this.TmpD)
        * --- Select from MPS_TPER
        i_nConn=i_TableProp[this.MPS_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
              +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" and "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
               ,"_Curs_MPS_TPER")
        else
          select * from (i_cTable);
           where TPDATINI<=this.TmpD and this.TmpD<=TPDATFIN;
            into cursor _Curs_MPS_TPER
        endif
        if used('_Curs_MPS_TPER')
          select _Curs_MPS_TPER
          locate for 1=1
          do while not(eof())
          this.cPERMPS = nvl(_Curs_MPS_TPER.TPPERASS,"000")
            select _Curs_MPS_TPER
            continue
          enddo
          use
        endif
      endif
    endif
    this.i = this.PrimaColPer
    do while this.i < int(val(this.cPERMPS))+2
      j= alltrim(str(this.i,3,0))
      this.GRID.Column&j..hdr.forecolor = 255
      this.i = this.i + 1
    enddo
    do while this.i <= this.oParentObject.w_NUMPER
      j= alltrim(str(this.i,3,0))
      this.GRID.Column&j..hdr.forecolor = 0
      this.i = this.i + 1
    enddo
    this.GRID.refresh()     
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea array periodi
    dimension PeriodiMPS(100)
    vq_exec ("..\COLA\exe\query\GSDB_BAP", this, "_PeriodiMPS_")
    Cur=WrCursor("_PeriodiMPS_")
    select * from _PeriodiMPS_ into array PeriodiMPS
    Use in _PeriodiMPS_
    this.GRID.fontsize = this.oParentObject.w_DimFont
    this.i = this.PrimaColPer
    do while this.i<=this.NumMaxPeriodi
      j= alltrim(str(this.i,3,0))
      * --- Determina indice vettore
      this.TmpC = right("000"+alltrim(str(this.i-this.PrimaColPer,2,0)),3)
      this.TmpN = int( Val(this.TmpC) + 1 )
      this.TmpC1 = nvl( PeriodiMPS( this.TmpN , 2 ) , "XXXX")
      * --- Periodo relativo
      this.TmpD = cp_ToDate( PeriodiMPS( this.TmpN , 3 ) )
      * --- Data inizio periodo
      if this.i-this.PrimaColPer<=this.oParentObject.w_NumPer
        this.GRID.Column&j..width = this.oParentObject.w_LargCol
        this.GRID.Column&j..hdr.caption = iif(this.i=this.PrimaColPer,"Scaduto",this.TmpC1+iif(left(this.TmpC1,1)="G"," ("+left(dtoc(this.TmpD),5)+")", "-"+str(year(this.TmpD),4,0)))
        this.GRID.Column&j..DynamicForeColor = "iif(recno()=1 and TOT_"+this.TmpC+"=-1,12632256,0)"
        this.GRID.Column&j..DynamicBackColor = "iif(recno()=8,8454143,iif(recno()=1 and TOT_"+this.TmpC+"=-1,12632256,16777215))"
      else
        this.GRID.Column&j..visible = .f.
        this.GRID.Column&j..width = 0
      endif
      this.i = this.i + 1
    enddo
    this.GRID.Column1.width = 100
    this.GRID.ScrollBars = 1
    * --- Calcolo del DTF (Demand Time Fence)
    this.oParentObject.w_PERDTF = "000"
    if this.oParentObject.w_PP___DTF>0
      * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
      this.TmpD = COCALCLT(i_DATSYS,this.oParentObject.w_PP___DTF,"A",TRUE,"")
      if not empty(this.TmpD)
        * --- Select from MPS_TPER
        i_nConn=i_TableProp[this.MPS_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
              +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" and "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
               ,"_Curs_MPS_TPER")
        else
          select * from (i_cTable);
           where TPDATINI<=this.TmpD and this.TmpD<=TPDATFIN;
            into cursor _Curs_MPS_TPER
        endif
        if used('_Curs_MPS_TPER')
          select _Curs_MPS_TPER
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_PERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
            select _Curs_MPS_TPER
            continue
          enddo
          use
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione,pCodice)
    this.pAzione=pAzione
    this.pCodice=pCodice
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MPS_TPER'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pCodice"
endproc
