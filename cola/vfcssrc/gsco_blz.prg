* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_blz                                                        *
*              Gestione lotti da dich. prod.                                   *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-12                                                      *
* Last revis.: 2014-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_blz",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_blz as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_PADRE = .NULL.
  w_TIPDICHIA = space(1)
  w_CODFOR = space(15)
  w_DATREG = ctod("  /  /  ")
  w_LOCODICE = space(20)
  w_LOTTOART = space(20)
  w_LOCODUBI = space(20)
  w_CODART = space(20)
  w_CODMAG = space(5)
  w_LCODMAG = space(5)
  w_LODATSCA = ctod("  /  /  ")
  w_MLCODICE = space(20)
  w_CODODL = space(15)
  w_RIGMAT = 0
  w_BASE = 0
  w_DICHIA = space(15)
  w_ROWCNM = 0
  w_NUMRIF = 0
  w_QTATOT = 0
  w_QTALOT = 0
  w_LOTESI = 0
  w_QTARES = 0
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_MOVLOT = 0
  w_ORECO = 0
  w_LOFLSTAT = space(1)
  w_nAbsRow = 0
  w_nRelRow = 0
  w_OKRIG = .f.
  * --- WorkFile variables
  LOTTIART_idx=0
  AVA_LOT_idx=0
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da GSDB_MLO(Movimenti Lotti/Ubicazioni da Consuntivazione Dichiarazioni di Produzione)
    this.w_MLCODICE = Space ( 20 )
    WITH this.oParentObject.oParentObject
    * --- Variabili da Detail Consuntivazione ODL (GSDB_MMP)
    this.w_CODART = .w_MPCODART
    this.w_LCODMAG = .w_MPCODMAG
    this.w_CODODL = .w_MPSERODL
    this.w_RIGMAT = .w_MPROWODL
    this.w_BASE = .w_MPQTAUM1
    this.w_DICHIA = .w_MPSERIAL
    this.w_ROWCNM = .w_CPROWNUM
    this.w_NUMRIF = .w_MPNUMRIF
    * --- Da GSDB_ARD/GSRA_ADO (Dichiarazione di Produzione)
    this.w_DATREG = .oParentobject.w_DPDATREG
    ENDWITH
    this.w_CODFOR = SPACE(15)
    this.w_CODMAG = this.oParentObject.w_CLCODMAG
    this.w_LOCODICE = SPACE(20)
    this.w_LOCODUBI = SPACE(20)
    this.w_CODUBI = SPACE(20)
    this.w_LODATSCA = cp_CharToDate("  -  -  ")
    do case
      case this.pAzione="ZOOM"
        * --- Seleziona Viste del Form
        vx_exec("QUERY\GSMA_QLO.VZM",this)
        if NOT EMPTY(NVL(this.w_LOCODICE," "))
          this.oParentObject.w_CLCODLOT = this.w_LOCODICE
          this.oParentObject.w_CLLOTART = this.w_LOTTOART
          this.oParentObject.w_ARTLOT = SPACE(20)
          this.oParentObject.w_VARLOT = SPACE(20)
          this.oParentObject.w_FLSTAT = SPACE(20)
          * --- Read from LOTTIART
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LOTTIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LOCODART,LOCODVAR,LOFLSTAT"+;
              " from "+i_cTable+" LOTTIART where ";
                  +"LOCODICE = "+cp_ToStrODBC(this.oParentObject.w_CLCODLOT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LOCODART,LOCODVAR,LOFLSTAT;
              from (i_cTable) where;
                  LOCODICE = this.oParentObject.w_CLCODLOT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_ARTLOT = NVL(cp_ToDate(_read_.LOCODART),cp_NullValue(_read_.LOCODART))
            this.oParentObject.w_VARLOT = NVL(cp_ToDate(_read_.LOCODVAR),cp_NullValue(_read_.LOCODVAR))
            this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_VARLOT = LEFT(this.oParentObject.w_VARLOT+SPACE(20), 20)
          if g_PERUBI="S" AND this.oParentObject.w_FLUBIC="S"
            this.oParentObject.w_CLCODUBI = NVL(this.w_LOCODUBI, SPACE(20))
          endif
          * --- Mi dice che il Lotto � stato caricato da Zoom (quindi non devo fare alcuni Check)
          this.oParentObject.w_LOTZOOM = .T.
          if NOT EMPTY(NVL(this.w_LODATSCA, cp_CharToDate("  -  -  "))) AND this.w_LODATSCA<this.w_DATREG
            AH_ERRORMSG("Il lotto selezionato � scaduto",48)
          endif
          * --- Notifica Riga Variata
          SELECT (this.oParentObject.cTrsName)
          if I_SRV=" " AND NOT DELETED()
            REPLACE i_SRV WITH "U"
          endif
        else
          * --- Mi dice che il Lotto � stato caricato da Zoom (quindi non devo fare alcuni Check)
          this.oParentObject.w_LOTZOOM = .F.
        endif
      case this.pAzione="CREATEROW"
        this.w_OKRIG = .F.
        this.w_PADRE = this.oParentObject
        if g_PERLOT="S" AND this.oParentObject.w_FLLOTT<>"N"
          if !empty(this.oParentObject.w_SERODL) and this.oParentObject.w_FLLOTT<>"C"
            * --- Select from GSCO_BLZ
            do vq_exec with 'GSCO_BLZ',this,'_Curs_GSCO_BLZ','',.f.,.t.
            if used('_Curs_GSCO_BLZ')
              select _Curs_GSCO_BLZ
              locate for 1=1
              do while not(eof())
              this.w_CODLOT = NVL(_Curs_GSCO_BLZ.MVCODLOT,SPACE(20))
              if !EMPTY(this.w_CODLOT)
                this.w_OKRIG = .T.
              endif
              if g_PERUBI="S" AND this.oParentObject.w_FLUBIC="S"
                this.w_CODUBI = NVL(_Curs_GSCO_BLZ.MVCODUBI,SPACE(20))
              endif
              this.w_QTALOT = NVL(_Curs_GSCO_BLZ.QTALOT,0)
              this.w_MOVLOT = iif(this.w_BASE>this.w_QTALOT, this.w_QTALOT, this.w_BASE)
              this.w_BASE = this.w_BASE - this.w_MOVLOT
              if this.w_MOVLOT > 0
                * --- Inserisco la riga del lotto
                this.w_PADRE.AddRow()     
                this.w_PADRE.ChildrenChangeRow()     
                this.oParentObject.w_CLCODLOT = this.w_CODLOT
                this.oParentObject.w_CLLOTART = this.w_CODLOT
                this.oParentObject.w_CLCODUBI = this.w_CODUBI
                this.oParentObject.w_CLCODMAG = this.w_CODMAG
                this.oParentObject.w_CLQTAMOV = this.w_MOVLOT
                this.oParentObject.w_QTAESI = _Curs_GSCO_BLZ.QTALOT
                this.oParentObject.w_VALRIG = this.w_MOVLOT
                this.oParentObject.w_TOTQTA = this.oParentObject.w_TOTQTA + this.w_MOVLOT
                this.oParentObject.w_FLSTAT = _Curs_GSCO_BLZ.LOFLSTAT
                this.w_PADRE.SaveRow()     
              endif
                select _Curs_GSCO_BLZ
                continue
              enddo
              use
            endif
          endif
          if this.w_OKRIG=.F.
            if this.oParentObject.w_FLLOTT="C"
              * --- Consumo automatico
              vq_exec("GSMD1LOT.VQR",this,"LOTTI")
              Select * from Lotti into cursor Lotti NoFilter
              * --- Quantit� effettiva su cui eseguire proporzione quantit� importata
              this.w_QTATOT = this.w_PADRE.oParentObject.w_MPQTAUM1
              =WRCURSOR("LOTTI")
              do while this.w_QTATOT>0
                this.w_CODLOT = CERCLOT(this.w_DATREG,this.w_CODART,"LOTTI")
                if NOT EMPTY(this.w_CODLOT)
                  SELECT LOTTI
                  GO TOP
                  LOCATE FOR this.w_CODART=NVL(CODART,SPACE(20)) AND this.w_CODLOT=NVL(LOTTI.LOCODICE,SPACE(20))
                  if FOUND()
                    * --- Assegno informazioni della riga documento  che variano 
                    this.w_QTALOT = NVL(LOTTI.LOQTAESI,0)
                    this.w_LOTESI = NVL(LOTTI.LOQTAESI,0)
                    this.w_CODLOT = NVL(LOTTI.LOCODICE,SPACE(20))
                    this.w_CODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
                    this.w_CODMAG = NVL(LOTTI.CODMAG,SPACE(5))
                    this.w_LOFLSTAT = NVL(LOTTI.LOFLSTAT,"")
                    if this.w_QTATOT>this.w_QTALOT
                      this.w_QTATOT = this.w_QTATOT - this.w_QTALOT
                      * --- Elimino il lotto per intero
                      SELECT LOTTI
                      DELETE
                    else
                      * --- Consumo in parte la quantit� del lotto quindi creo una riga pari
                      *     alla quantit� del documento di origine
                      this.w_QTALOT = this.w_QTATOT
                      this.w_QTARES = this.w_QTALOT - this.w_QTATOT
                      this.w_QTATOT = 0
                    endif
                  endif
                else
                  * --- Creo Riga con lotto vuoto
                  this.w_QTALOT = this.w_QTATOT
                  this.w_QTATOT = 0
                endif
                this.w_OKRIG = .T.
                this.w_PADRE.AddRow()     
                this.w_PADRE.ChildrenChangeRow()     
                this.oParentObject.w_CLCODLOT = this.w_CODLOT
                this.oParentObject.w_CLLOTART = this.w_CODLOT
                this.oParentObject.w_CLCODUBI = this.w_CODUBI
                this.oParentObject.w_CLCODMAG = this.w_CODMAG
                this.oParentObject.w_CLQTAMOV = this.w_QTALOT
                this.oParentObject.w_QTAESI = this.w_LOTESI
                this.oParentObject.w_VALRIG = this.w_QTALOT
                this.oParentObject.w_TOTQTA = this.oParentObject.w_TOTQTA + this.w_QTALOT
                this.oParentObject.w_FLSTAT = this.w_LOFLSTAT
                this.w_PADRE.SaveRow()     
              enddo
              Use in Select("Lotti")
            endif
          endif
        else
          if g_PERUBI="S" AND this.oParentObject.w_FLUBIC="S"
            * --- Per le righe aggiunte manualmente nella dichiarazione non ci pu� essere un trasferimento
            if not empty(nvl(this.oParentObject.w_SERODL,""))
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MVCODLOT,MVCODUBI,MVQTAUM1  from "+i_cTable+" DOC_DETT ";
                    +" where MVCODODL="+cp_ToStrODBC(this.oParentObject.w_SERODL)+" and MVRIGMAT="+cp_ToStrODBC(this.oParentObject.w_ROWODL)+"";
                     ,"_Curs_DOC_DETT")
              else
                select MVCODLOT,MVCODUBI,MVQTAUM1 from (i_cTable);
                 where MVCODODL=this.oParentObject.w_SERODL and MVRIGMAT=this.oParentObject.w_ROWODL;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.w_OKRIG = .T.
                this.w_CODUBI = NVL(_Curs_DOC_DETT.MVCODUBI,SPACE(20))
                this.w_QTALOT = NVL(_Curs_DOC_DETT.MVQTAUM1,0)
                this.w_MOVLOT = iif(this.w_BASE>this.w_QTALOT, this.w_QTALOT, this.w_BASE)
                this.w_BASE = this.w_BASE - this.w_MOVLOT
                if this.w_MOVLOT > 0
                  * --- Inserisco la riga del lotto
                  * --- Read from LOTTIART
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LOTTIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LOCODART,LOFLSTAT"+;
                      " from "+i_cTable+" LOTTIART where ";
                          +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LOCODART,LOFLSTAT;
                      from (i_cTable) where;
                          LOCODICE = this.w_CODLOT;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.oParentObject.w_ARTLOT = NVL(cp_ToDate(_read_.LOCODART),cp_NullValue(_read_.LOCODART))
                    this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_PADRE.AddRow()     
                  this.w_PADRE.ChildrenChangeRow()     
                  this.oParentObject.w_CLCODUBI = this.w_CODUBI
                  this.oParentObject.w_CLQTAMOV = this.w_MOVLOT
                  this.oParentObject.w_CLDICHIA = this.w_DICHIA
                  this.oParentObject.w_CLROWCNM = this.w_ROWCNM
                  this.oParentObject.w_CLNUMRIF = this.w_NUMRIF
                  this.oParentObject.w_CLFLCASC = "S"
                  this.oParentObject.w_CLCODMAG = this.w_CODMAG
                  this.w_PADRE.SaveRow()     
                endif
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
            endif
          endif
        endif
      case this.pAzione="UBIC"
        * --- Seleziona Viste del Form
        if this.oParentObject.w_CLFLCASC="C"
          * --- Carica il Lotto
          vx_exec("QUERY\GSMA_QUC.VZM",this)
        else
          * --- Scarica il Lotto
          vx_exec("QUERY\GSMA_QUS.VZM",this)
        endif
        if NOT EMPTY(NVL(this.w_CODUBI," "))
          this.oParentObject.w_CLCODUBI = this.w_CODUBI
          this.oParentObject.w_CLCODLOT = IIF(this.oParentObject.w_CLFLCASC="C", this.oParentObject.w_CLCODLOT,this.w_CODLOT)
          * --- Mi dice se il dato proviene da uno Zoom o da inserimento manuale
          this.oParentObject.w_UBIZOOM = .T.
          * --- Notifica Riga Variata
          SELECT (this.oParentObject.cTrsName)
          if I_SRV=" " AND NOT DELETED()
            REPLACE i_SRV WITH "U"
          endif
        else
          this.oParentObject.w_UBIZOOM = .F.
        endif
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='AVA_LOT'
    this.cWorkTables[3]='DOC_DETT'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSCO_BLZ')
      use in _Curs_GSCO_BLZ
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
