* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bpm                                                        *
*              Previsioni di vendita mensili                                   *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-04                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Tipope,w_mese
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bpm",oParentObject,m.Tipope,m.w_mese)
return(i_retval)

define class tgsdb_bpm as StdBatch
  * --- Local variables
  Tipope = space(1)
  w_mese = 0
  w_ARTIPGES = space(1)
  w_PVPERIOD = space(1)
  w_PVPERMES = 0
  w_PVPERSET = 0
  w_KEYSAL = space(40)
  w_PV__DATA = ctod("  /  /  ")
  w_PVQUANTI = 0
  w_PV__ANNO = 0
  w_OGGMPS = space(1)
  w_PunPadre = .NULL.
  w_CODARTM = space(20)
  w_DESARTM = space(40)
  w_PVUNIMISM = space(3)
  w_ANNOM = 0
  w_editm = .f.
  w_DATINI = ctod("  /  /  ")
  w_SETTINI = 0
  w_PREMES = 0
  * --- WorkFile variables
  VEN_PREV_idx=0
  ART_PROD_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola le previsioni di vendita mensili (da GSDB_KPM)
    * --- Tipo operazione
    * --- queste variabili sono utilizzate tramite le macro di VF (non � necessario definirle)
    this.w_PVPERIOD = "M"
    this.w_PV__ANNO = this.oParentObject.w_ANNO
    this.w_PunPadre = this.oParentObject
    do case
      case this.Tipope="I"
        * --- Interroga
        if empty(this.oParentObject.w_CODART) or empty(this.oParentObject.w_ANNO)
          i_retcode = 'stop'
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARTIPGES"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARTIPGES;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_ARTIPGES<>"F"
          ah_ErrorMsg("Errore: articolo non gestito a fabbisogno",16)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PROGGMPS"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PROGGMPS;
            from (i_cTable) where;
                PRCODART = this.oParentObject.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OGGMPS = NVL(cp_ToDate(_read_.PROGGMPS),cp_NullValue(_read_.PROGGMPS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_OGGMPS) or this.w_OGGMPS="N"
          ah_ErrorMsg("Articolo non valido: non associato a nessun sistema d'ordine",16)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        * --- Abilita e azzera tutti i textbox relativi a periodi mensili
        this.w_PVPERMES = 12
        do while this.w_PVPERMES>0
          l_MESEind = ALLTRIM(STR(this.w_PVPERMES,2,0))
          this.w_PunPadre.w_MESE&l_MESEind.H = (cp_CharToDate("01-"+l_MESEind+"-"+STR(this.w_PV__ANNO,4,0)) >= cp_CharToDate("01"+right(dtoc(i_DATSYS),8)))
          this.w_PunPadre.w_MESE&l_MESEind = 0
          this.w_PunPadre.w_MESE&l_MESEind.O = 0
          this.w_PVPERMES = this.w_PVPERMES - 1
        enddo
        * --- Calcola le previsioni mensili
        * --- Select from GSDB_BPM
        do vq_exec with 'GSDB_BPM',this,'_Curs_GSDB_BPM','',.f.,.t.
        if used('_Curs_GSDB_BPM')
          select _Curs_GSDB_BPM
          locate for 1=1
          do while not(eof())
          this.w_PVPERMES = _Curs_GSDB_BPM.PVPERMES
          this.w_PVQUANTI = _Curs_GSDB_BPM.PVQUANTI
          l_MESEind=ALLTRIM(STR(this.w_PVPERMES,2,0))
          this.w_PunPadre.w_MESE&l_MESEind = this.w_PVQUANTI
          this.w_PunPadre.w_MESE&l_MESEind.O = this.w_PVQUANTI
            select _Curs_GSDB_BPM
            continue
          enddo
          use
        endif
        * --- Disabilita i textbox che contengono previsioni settimanali o giornaliere
        * --- Select from GSDB1BPM
        do vq_exec with 'GSDB1BPM',this,'_Curs_GSDB1BPM','',.f.,.t.
        if used('_Curs_GSDB1BPM')
          select _Curs_GSDB1BPM
          locate for 1=1
          do while not(eof())
          this.w_PVPERMES = _Curs_GSDB1BPM.PVPERMES
          l_MESEind = ALLTRIM(STR(this.w_PVPERMES,2,0))
          this.w_PunPadre.w_MESE&l_MESEind.H = .F.
            select _Curs_GSDB1BPM
            continue
          enddo
          use
        endif
      case this.Tipope="A"
        * --- Aggiorna le previsioni mensili
        this.w_PVPERMES = 12
        do while this.w_PVPERMES>0
          l_MESEind = ALLTRIM(STR(this.w_PVPERMES,2,0))
          if this.w_PunPadre.w_MESE&l_MESEind.H AND this.w_PunPadre.w_MESE&l_MESEind <> this.w_PunPadre.w_MESE&l_MESEind.O
            this.w_PVQUANTI = this.w_PunPadre.w_MESE&l_MESEind
            GSDB_BCG(this,.t.)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_PVQUANTI = 0
              * --- Delete from VEN_PREV
              i_nConn=i_TableProp[this.VEN_PREV_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                      +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                       )
              else
                delete from (i_cTable) where;
                      PV__DATA = this.w_PV__DATA;
                      and PVCODART = this.oParentObject.w_CODART;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            else
              * --- Try
              local bErr_03A557A8
              bErr_03A557A8=bTrsErr
              this.Try_03A557A8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- Write into VEN_PREV
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.VEN_PREV_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_PREV_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PVQUANTI ="+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
                      +i_ccchkf ;
                  +" where ";
                      +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                      +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                         )
                else
                  update (i_cTable) set;
                      PVQUANTI = this.w_PVQUANTI;
                      &i_ccchkf. ;
                   where;
                      PV__DATA = this.w_PV__DATA;
                      and PVCODART = this.oParentObject.w_CODART;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              bTrsErr=bTrsErr or bErr_03A557A8
              * --- End
            endif
            this.w_PunPadre.w_MESE&l_MESEind.O = this.w_PVQUANTI
          endif
          this.w_PVPERMES = this.w_PVPERMES - 1
        enddo
        ah_ErrorMsg("Previsioni aggiornate", 48)
      case this.Tipope="S"
        if empty(this.oParentObject.w_CODART) or empty(this.oParentObject.w_ANNO)
          * --- Lancia la maschera delle previsioni settimanali
          i_retcode = 'stop'
          return
        endif
        this.w_CODARTM = this.oParentObject.w_CODART
        this.w_DESARTM = this.oParentObject.w_DESART
        this.w_PVUNIMISM = this.oParentObject.w_PVUNIMIS
        this.w_ANNOM = this.oParentObject.w_ANNO
        this.w_PVPERMES = this.w_mese
        do GSDB_BCG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_DATINI = this.w_PV__DATA
        this.w_SETTINI = this.w_PVPERSET
        l_MESEind = alltrim(str(this.w_PVPERMES,2,0))
        this.w_PREMES = this.w_PunPadre.w_MESE&l_MESEind
        this.w_editm = this.w_PunPadre.w_MESE&l_MESEind.H
        do GSDB_KPS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PunPadre.w_MESE&l_MESEind = this.w_PREMES
        this.w_PunPadre.w_MESE&l_MESEind.H = this.w_editm and date(this.oParentObject.w_ANNO,this.w_PVPERMES,28)>=i_DATSYS
        if not this.w_editm
          * --- Disabilita i textbox che contengono previsioni settimanali o giornaliere
          * --- Select from GSDB1BPM
          do vq_exec with 'GSDB1BPM',this,'_Curs_GSDB1BPM','',.f.,.t.
          if used('_Curs_GSDB1BPM')
            select _Curs_GSDB1BPM
            locate for 1=1
            do while not(eof())
            this.w_PVPERMES = _Curs_GSDB1BPM.PVPERMES
            l_MESEind = ALLTRIM(STR(this.w_PVPERMES,2,0))
            this.w_PunPadre.w_MESE&l_MESEind.H = .F.
              select _Curs_GSDB1BPM
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_03A557A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VEN_PREV
    i_nConn=i_TableProp[this.VEN_PREV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VEN_PREV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PV__DATA"+",PVCODART"+",PVPERIOD"+",PVPERMES"+",PVQUANTI"+",PV__ANNO"+",PVPERSET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PV__DATA),'VEN_PREV','PV__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'VEN_PREV','PVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERIOD),'VEN_PREV','PVPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERMES),'VEN_PREV','PVPERMES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PV__ANNO),'VEN_PREV','PV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERSET),'VEN_PREV','PVPERSET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PV__DATA',this.w_PV__DATA,'PVCODART',this.oParentObject.w_CODART,'PVPERIOD',this.w_PVPERIOD,'PVPERMES',this.w_PVPERMES,'PVQUANTI',this.w_PVQUANTI,'PV__ANNO',this.w_PV__ANNO,'PVPERSET',this.w_PVPERSET)
      insert into (i_cTable) (PV__DATA,PVCODART,PVPERIOD,PVPERMES,PVQUANTI,PV__ANNO,PVPERSET &i_ccchkf. );
         values (;
           this.w_PV__DATA;
           ,this.oParentObject.w_CODART;
           ,this.w_PVPERIOD;
           ,this.w_PVPERMES;
           ,this.w_PVQUANTI;
           ,this.w_PV__ANNO;
           ,this.w_PVPERSET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sbianca le variabili
    this.oParentObject.w_CODART = space(20)
    this.oParentObject.w_DESART = space(40)
    this.oParentObject.w_PVUNIMIS = space(3)
    this.oParentObject.w_FLCOMM = space(1)
    this.w_PVPERMES = 12
    do while this.w_PVPERMES>0
      l_MESEind = ALLTRIM(STR(this.w_PVPERMES,2,0))
      this.w_PunPadre.w_MESE&l_MESEind.H = (cp_CharToDate("01-"+l_MESEind+"-"+STR(this.w_PV__ANNO,4,0)) >= cp_CharToDate("01"+right(dtoc(i_DATSYS),8)))
      this.w_PunPadre.w_MESE&l_MESEind = 0
      this.w_PunPadre.w_MESE&l_MESEind.O = 0
      this.w_PVPERMES = this.w_PVPERMES - 1
    enddo
  endproc


  proc Init(oParentObject,Tipope,w_mese)
    this.Tipope=Tipope
    this.w_mese=w_mese
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='VEN_PREV'
    this.cWorkTables[2]='ART_PROD'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='PAR_RIOR'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSDB_BPM')
      use in _Curs_GSDB_BPM
    endif
    if used('_Curs_GSDB1BPM')
      use in _Curs_GSDB1BPM
    endif
    if used('_Curs_GSDB1BPM')
      use in _Curs_GSDB1BPM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Tipope,w_mese"
endproc
