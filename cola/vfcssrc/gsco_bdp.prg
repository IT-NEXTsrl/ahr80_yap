* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bdp                                                        *
*              Eventi da dichiarazione di produzione                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1149]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-16                                                      *
* Last revis.: 2018-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,TipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bdp",oParentObject,m.TipOpe)
return(i_retval)

define class tgsco_bdp as StdBatch
  * --- Local variables
  TipOpe = space(15)
  GSCO_MCO = .NULL.
  PUNMAT = .NULL.
  PUNRIS = .NULL.
  PUNMOU = .NULL.
  w_ROWORD = 0
  w_QTAUM1 = 0
  w_COEIMP = 0
  TmpC = space(100)
  w_AGGMAT = .f.
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_FLINTE = space(1)
  w_CAUMAG = space(5)
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_CAUCOL = space(5)
  w_FLPPRO = space(1)
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_OP = space(1)
  w_MOLT = 0
  w_OP3 = space(1)
  w_MOLT3 = 0
  w_MPUNIMIS = space(3)
  w_MPQTAMOV = 0
  w_MPQTAUM1 = 0
  w_MPFLEVAS = space(1)
  w_MPCODCOM = space(15)
  w_OK = .f.
  w_QTAPR1 = 0
  w_KEYSAL = space(20)
  w_CODMAG = space(5)
  w_CODMAT = space(5)
  w_MFLCASC = space(1)
  w_MFLORDI = space(1)
  w_MFLIMPE = space(1)
  w_MFLRISE = space(1)
  w_MF2CASC = space(1)
  w_MF2ORDI = space(1)
  w_MF2IMPE = space(1)
  w_MF2RISE = space(1)
  w_OQTMOV = 0
  w_OQTUM1 = 0
  w_NQTSAL = 0
  w_NQTEV1 = 0
  w_NQTEVA = 0
  w_OQTOR1 = 0
  w_OQTEVA = 0
  w_OQTEV1 = 0
  w_OQTSAL = 0
  w_OSTATO = space(1)
  w_ODTFIN = ctod("  /  /  ")
  w_OKEYSA = space(20)
  w_OCOMAG = space(5)
  w_DISMAG = space(1)
  w_PDROWNUM = 0
  w_SERDOC = space(10)
  w_RECO = 0
  w_CODVAL = space(3)
  w_PREZZO = 0
  w_CAOVAL = 0
  w_RPUNIMIS = space(3)
  w_RPQTAMOV = 0
  w_RPQTAUNI = 0
  w_RPSETUP = space(1)
  w_PRENAZ = 0
  w_PREUM = 0
  w_TOTDAR = 0
  w_TOTALE = 0
  w_QDPPRO = 0
  w_QDPSCA = 0
  w_QDPPR1 = 0
  w_QDPSC1 = 0
  w_PROG = .NULL.
  w_SERIAL = space(10)
  w_CLADOC = space(2)
  w_FLVEAC = space(1)
  w_DATMAT = ctod("  /  /  ")
  w_OLCODODL = space(15)
  w_DELETING = space(1)
  w_OLKEYSAL = space(20)
  w_SDIC = space(10)
  w_FLUBI = space(1)
  w_DCROWNUM = 0
  w_SALCNV = 0
  w_DIFFMOV = 0
  w_QTASAL = 0
  w_QTARIC = 0
  w_TOTMOV = 0
  w_ODLRIG = 0
  w_PERASS = space(3)
  W_UNIMIS = space(3)
  w_FAMPRO = space(5)
  w_GESWIP = space(1)
  w_PGSERIAL = space(15)
  w_PGROWNUM = 0
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_COMMDEFA = space(15)
  w_OLDCOM = .f.
  w_DSCFAS = space(40)
  w_UMTFAS = space(3)
  w_QTAFAS = 0
  w_INTESFAS = space(1)
  w_MWIPFAS = space(5)
  w_TMPSEC = 0
  w_DSCLFAS = space(40)
  w_TESTPRO = .f.
  w_CICLI = .f.
  w_CREADOC = .f.
  w_TATTIV = space(1)
  w_SALQTA = 0
  w_FASCOMME = space(15)
  w_CAUMGM = space(5)
  w_FLINTM = space(1)
  w_MOUODL = 0
  w_MDCODICE = space(20)
  w_MDCODART = space(20)
  w_MDCODMAG = space(5)
  w_DESART = space(40)
  w_MAGPRE = space(5)
  w_UNIMIS3 = space(3)
  w_OPERAT3 = space(1)
  w_MOLTIP3 = 0
  w_UNIMIS1 = space(3)
  w_OPERAT1 = space(1)
  w_MOLTIP1 = 0
  w_UNIMIS2 = space(3)
  w_FLUMSEP = space(1)
  w_MATCOMME = space(1)
  w_FLOTTI = space(1)
  w_FLUBIC = space(1)
  w_OQTASAL = 0
  w_OEVA = space(1)
  w_NOMSG = space(1)
  w_CHECKMATR = .f.
  w_DIFFE = 0
  w_DASTOR = 0
  w_OLFLEVAS = space(1)
  w_OLFLORDI = space(1)
  w_OLFLIMPE = space(1)
  w_QTADAB = 0
  w_MAGTRA = space(5)
  w_QTAPR1 = 0
  w_QTAPRE = 0
  w_RIFMAG = space(15)
  w_PECODODL = space(15)
  w_PECODART = space(20)
  w_PEQTAUM1 = 0
  w_PEQTAOLD = 0
  w_PGCODODL = space(15)
  w_MAGSCA = space(5)
  w_MAGRIF = space(5)
  w_QTASC1 = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  GSCO_ADP = .NULL.
  w_CLASS = space(10)
  w_OLTFLEVA = space(1)
  w_oQTAAVA = 0
  w_oQTADIC = 0
  w_oFLGEVA = space(1)
  w_DPDICUM1 = 0
  w_CLAVAUM1 = 0
  w_CLQTAAVA = 0
  w_CLQTASCA = 0
  w_CLSCAUM1 = 0
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_OQTPRO = 0
  w_OQTPR1 = 0
  w_OQTOSC = 0
  w_OQTOS1 = 0
  w_NQTPRO = 0
  w_NQTPR1 = 0
  w_NQTOSC = 0
  w_NQTOS1 = 0
  w_SERORD = space(10)
  w_RIFODL = space(15)
  w_RIGORD = 0
  w_TIPRIF = space(1)
  w_RIGUPD = space(1)
  w_CLAVAUM1 = 0
  w_CLQTAAVA = 0
  w_CLQTASCA = 0
  w_CLSCAUM1 = 0
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_CLFASEVA = space(1)
  w_oQTAMOV = 0
  w_oQTAUM1 = 0
  w_oQTASCA = 0
  w_oQTASC1 = 0
  w_MATRKO = .f.
  w_MATFOUND = .f.
  w_Msg = space(100)
  w_CntMat = 0
  w_TIPART = space(2)
  radice = space(240)
  w_QTATOT = 0
  riga = 0
  w_PDSERIAL = space(15)
  w_PDROWNUM = 0
  w_QTARES = 0
  w_RISODL = 0
  w_PRIMOGIRO = .f.
  w_CONVE = 0
  w_VALCONV = 0
  w_TEMSEC = 0
  w_OPERAZ = space(2)
  w_MVTIPDOC = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLVEAC = space(1)
  w_MVEMERIC = space(1)
  w_MVCLADOC = space(2)
  w_MVFLPROV = space(1)
  w_NUMSCO = 0
  w_DOCCAR = space(10)
  w_DOCSCA = space(10)
  w_DOCSER = space(10)
  w_DOCMOU = space(10)
  w_nRecSel = 0
  w_nRecEla = 0
  w_nRecDoc = 0
  w_LNumErr = 0
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_WIPNETT = space(1)
  w_MPCODMAG = space(5)
  w_QTAPPO = 0
  w_QTAMOV = 0
  w_ODLRIG = 0
  w_MPQTAEVA = 0
  w_MPQTAEV1 = 0
  w_NUMINV = space(6)
  w_ESEINV = space(4)
  w_ODTINI = ctod("  /  /  ")
  w_MAGCOD = space(5)
  w_SALKEY = space(20)
  w_FLORDD = space(1)
  w_FLIMPD = space(1)
  w_OCODART = space(20)
  w_OQTODL1 = 0
  w_OQTEVS1 = 0
  w_QTAMOVT = 0
  w_QTAMOVT1 = 0
  w_QTOD1 = 0
  w_TOTQTA = 0
  w_PRZUNI = 0
  w_PRZTOT = 0
  w_RIGA = 0
  w_DECTOT = 0
  w_DECUNI = 0
  w_VALCOD = space(3)
  w_VALRIG = 0
  w_VALMAG = 0
  w_IMPNAZ = 0
  w_IMPSCO = 0
  w_MOVQTA = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_FLSCOR = space(1)
  w_IMPACC = 0
  w_PERIVA = 0
  w_PERIVE = 0
  w_CODIVA = space(5)
  w_FLSTRN = space(1)
  w_CAMBIO = 0
  w_DATDCM = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_INDIVA = 0
  w_INDIVE = 0
  w_FLORCO = space(1)
  w_FLCOCO = space(1)
  w_IFLCOCO = space(1)
  w_IFLORCO = space(1)
  w_CODCOM = space(15)
  w_CODCEN = space(15)
  w_VOCCOS = space(5)
  w_COCODVAL = space(3)
  w_DECCOM = 0
  w_OLDIMPCOM = 0
  w_TIPATT = space(1)
  w_CODATT = space(15)
  w_IMPCOM = 0
  w_MFLCOMM = space(1)
  w_CAUMAGA = space(5)
  w_MVTFLCOM = space(1)
  w_OK_COMM = .f.
  w_LFLVEAC = space(1)
  w_FLANAL = space(1)
  w_TIPOIN = space(5)
  w_FLEVAS = space(1)
  w_FLVEAC = space(1)
  w_SPUNTA = space(1)
  w_CATDOC = space(2)
  w_CLIFOR = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_DATA1 = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_NUMINI = 0
  w_NUMFIN = 0
  w_serie1 = space(2)
  w_serie2 = space(2)
  w_STAMPA = 0
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_CODESE = space(2)
  w_WIPSCA = space(1)
  w_NETSCA = space(1)
  w_TIPCON = space(1)
  w_CODAGE = space(5)
  w_LINGUA = space(3)
  w_CATEGO = space(2)
  w_CODVET = space(5)
  w_CODZON = space(3)
  w_CATCOM = space(3)
  w_CODDES = space(5)
  w_CODPAG = space(5)
  w_NOSBAN = space(15)
  w_MF2ORCO = space(1)
  w_MF2COCO = space(1)
  w_MFCODCOS = space(3)
  w_MFCODCOM = space(15)
  w_MFCODATT = space(15)
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_MVIMPCOM = 0
  w_STORNAMAT = .f.
  w_MTCODMAT = space(40)
  w_MSGMATRI = space(10)
  w_MT__FLAG = space(1)
  w_AM_PRENO = 0
  w_ROWRIF = 0
  w_NUMRIF = 0
  w_CODODL = space(15)
  w_TROV = .f.
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  w_MVCODART = space(20)
  w_MVDESART = space(40)
  w_MVUNIMIS = space(3)
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVLOTMAG = space(5)
  w_MVLOTMAT = space(5)
  w_MVCODLOT = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVF2CASC = space(1)
  w_MVF2RISE = space(1)
  w_MVFLUBIC = space(1)
  w_MVFLUBI2 = space(1)
  w_MVARTLOT = space(1)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVRIFRIG = 0
  w_MVTIPRIG = space(1)
  w_MVCAUMAG = space(5)
  w_MVCATCON = space(5)
  w_MVCONTRA = space(15)
  w_MVDESSUP = space(0)
  w_MVINICOM = ctod("  /  /  ")
  w_MVFINCOM = ctod("  /  /  ")
  w_MVCODLIS = space(5)
  w_MVCODCLA = space(3)
  w_MVPREZZO = 0
  w_VALUNI = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVFLOMAG = space(1)
  w_MVIMPACC = 0
  w_MVIMPSCO = 0
  w_MVIMPNAZ = 0
  w_MVVALMAG = 0
  w_MVVALRIG = 0
  w_MVPESNET = 0
  w_MVFLRAGG = space(1)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVCODIVA = space(5)
  w_MVCONIND = space(15)
  w_MVNOMENC = space(8)
  w_MVFLTRAS = space(1)
  w_MVMOLSUP = 0
  w_MVCAUCOL = space(5)
  w_MVCODATT = space(15)
  w_MVCODCEN = space(15)
  w_MVCODCOM = space(15)
  w_MVCODODL = space(15)
  w_MVF2ORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLELGM = space(1)
  w_MVFLIMPE = space(1)
  w_MVKEYSAL = space(20)
  w_MVTIPATT = space(1)
  w_MVRIGMAT = 0
  w_MVVOCCEN = space(15)
  w_CONFERMA = .f.
  w_DATDOC = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CAUDOC = space(5)
  w_DESDOC = space(40)
  w_OLDROW = 0
  w_OLDART = space(20)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_DATREG = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_ROWMAT = 0
  CONTA = 0
  SERDOC = space(10)
  RIGDOC = 0
  NUMROW = 0
  LOTTO = space(20)
  UBICA = space(20)
  UBICA2 = space(20)
  QTA1 = 0
  QTA2 = 0
  RIFRIGA = 0
  w_MVFLELAN = space(1)
  w_MV_SEGNO = space(1)
  w_GENROWDOC = 0
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_QTALOT = 0
  w_MOVLOT = 0
  w_BASE = 0
  w_OKRIG = .f.
  w_SERODL = space(15)
  w_ROWODL = 0
  w_LCODMAG = space(5)
  w_CODMAG = space(5)
  w_CODART = space(20)
  w_LOTESI = 0
  w_LOFLSTAT = space(1)
  w_CONTAREC = 0
  w_MODUM2D = space(1)
  w_FLUSEPD = space(1)
  w_PEQTAUM1 = 0
  w_PECODODL = space(15)
  w_PESERIAL = space(15)
  w_PEQTAABB = 0
  w_PENUMRIF = 0
  w_PESERORD = space(10)
  w_PERIGORD = 0
  w_PROGR = 0
  w_PETIPRIF = space(1)
  w_PECODART = space(20)
  w_DPOGGMPS = space(1)
  w_PECODCOM = space(15)
  w_PECODRIC = space(20)
  w_PEFLCOMM = space(1)
  w_PERIFTRS = space(20)
  w_TRSRIFODL = space(15)
  w_TRSSERORD = space(10)
  w_TRSRIGORD = 0
  w_OLTCOMME = space(15)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  SALDIART_idx=0
  DELTA_idx=0
  ODL_MAST_idx=0
  ODL_DETT_idx=0
  MAGAZZIN_idx=0
  DIC_PROD_idx=0
  AGG_LOTT_idx=0
  SPL_LOTT_idx=0
  DIC_MATR_idx=0
  DIC_MCOM_idx=0
  DELTA_idx=0
  MOVIMATR_idx=0
  MATRICOL_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  MA_COSTI_idx=0
  MAT_PROD_idx=0
  UNIMIS_idx=0
  ART_PROD_idx=0
  PEG_SELI_idx=0
  PAR_PROD_idx=0
  ODL_SMPL_idx=0
  LOTTIART_idx=0
  MPS_TFOR_idx=0
  SALDICOM_idx=0
  RIS_ORSE_idx=0
  CLR_MAST_idx=0
  VALUTE_idx=0
  CAN_TIER_idx=0
  VOC_COST_idx=0
  VOCIIVA_idx=0
  ODL_RISF_idx=0
  ODL_CICL_idx=0
  ODL_RISO_idx=0
  RIS_DICH_idx=0
  DIC_COMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione di GSCO_ADP 
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.GSCO_ADP = this.oParentObject
    this.w_CLASS = UPPER(ALLTRIM(this.GSCO_ADP.Class))
    this.w_CICLI = this.w_CLASS="TGSCI_ADP"
    * --- Puntatore a padre
    this.PUNMAT = this.GSCO_ADP.GSCO_MMP
    this.PUNMOU = this.GSCO_ADP.GSCO_MOU
    this.w_CREADOC = .t.
    this.w_NOMSG = "N"
    if this.w_CICLI
      * --- Cicli di lavorazione abilitati
      this.w_TATTIV = this.GSCO_ADP.w_TATTIV
      this.PUNRIS = this.GSCO_ADP.GSCI_MRP
      if this.oParentObject.w_DPFASOUT<>"S"
        * --- Se ho i cicli di lavorazione e la fase non � di output non creo i documenti di carico/scarico, creo solo il documento delle risorse
        this.w_CREADOC = .f.
      endif
    else
      * --- Cicli di lavorazione non abilitati (uso i cicli semplificati)
      this.w_CICLI = .f.
      this.w_TATTIV = "Q"
      this.PUNRIS = this.GSCO_ADP.GSCO_MRP
    endif
    this.oParentObject.w_STATOGES = upper(this.GSCO_ADP.cFunction)
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCODCOM"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCODCOM;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMMDEFA = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(nvl(this.w_COMMDEFA,""))
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = space(15)
    endif
    * --- Tipologia gestione commessa
    if VARTYPE(g_OLDCOM)="L" AND g_OLDCOM
      this.w_OLDCOM = .t.
    else
      this.w_OLDCOM = .f.
    endif
    do case
      case this.TipOpe="MATERIALI"
        * --- Blocco l'ODL perch� lo sto modificando (valorizzo il flag OLTSOSPE) - Caricamento
        *     Operazione eseguita per evitare che l'ODL venga processato in contemporanea da pi� utenti che creerebbero una situazione inconsistente
        if this.oParentObject.w_DPCODODL<>this.oParentObject.o_DPCODODL
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTSOSPE ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTSOSPE');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.o_DPCODODL);
                   )
          else
            update (i_cTable) set;
                OLTSOSPE = " ";
                &i_ccchkf. ;
             where;
                OLCODODL = this.oParentObject.o_DPCODODL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSOSPE ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTSOSPE');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                 )
        else
          update (i_cTable) set;
              OLTSOSPE = "S";
              &i_ccchkf. ;
           where;
              OLCODODL = this.oParentObject.w_DPCODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NOT EMPTY(this.oParentObject.w_DPCODODL)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.TipOpe="QTAMAT"
        if NOT EMPTY(this.oParentObject.w_DPCODODL)
          * --- Read from PAR_PROD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PPMAGSCA"+;
              " from "+i_cTable+" PAR_PROD where ";
                  +"PPCODICE = "+cp_ToStrODBC("PP");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PPMAGSCA;
              from (i_cTable) where;
                  PPCODICE = "PP";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MAGSCA = NVL(cp_ToDate(_read_.PPMAGSCA),cp_NullValue(_read_.PPMAGSCA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.TipOpe="CONTROLLI"
        this.oParentObject.w_TESTFORM = .T.
        if this.oParentObject.w_TESTFORM
          if !Empty(this.oParentObject.w_DPODLFAS) and this.oParentObject.w_FASE>0
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTFLEVA"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTFLEVA;
                from (i_cTable) where;
                    OLCODODL = this.oParentObject.w_DPCODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLTFLEVA = NVL(cp_ToDate(_read_.OLTFLEVA),cp_NullValue(_read_.OLTFLEVA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_OLTFLEVA="S"
              this.w_Msg = "Fase %1 gi� completata%0Impossibile confermare"
              ah_ErrorMsg(this.w_Msg,16,"", this.oParentObject.w_DPCODODL)
              this.oParentObject.w_TESTFORM = .F.
            endif
          endif
        endif
        if empty(nvl(this.oParentObject.w_DPCODODL , space(15)))
          this.w_Msg = "� necessario inserire un codice ODL valido%0Impossibile confermare"
          ah_ErrorMsg(this.w_Msg,16)
          this.oParentObject.w_TESTFORM = .F.
        endif
        if this.oParentObject.w_TESTFORM and !EMPTY(this.oParentObject.w_DPCODODL) and NOT EMPTY(this.oParentObject.w_DPSERIAL)
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Controllo quantita' fasi
        if this.oParentObject.w_TESTFORM and !Empty(this.oParentObject.w_DPODLFAS) and this.oParentObject.w_FASE>0 and !Empty(this.oParentObject.w_OLTSECPR)
          * --- Read from DIC_PROD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPQTAPRO,DPQTAPR1,DPQTASCA,DPQTASC1"+;
              " from "+i_cTable+" DIC_PROD where ";
                  +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPQTAPRO,DPQTAPR1,DPQTASCA,DPQTASC1;
              from (i_cTable) where;
                  DPSERIAL = this.oParentObject.w_DPSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_oQTAMOV = NVL(cp_ToDate(_read_.DPQTAPRO),cp_NullValue(_read_.DPQTAPRO))
            this.w_oQTAUM1 = NVL(cp_ToDate(_read_.DPQTAPR1),cp_NullValue(_read_.DPQTAPR1))
            this.w_oQTASCA = NVL(cp_ToDate(_read_.DPQTASCA),cp_NullValue(_read_.DPQTASCA))
            this.w_oQTASC1 = NVL(cp_ToDate(_read_.DPQTASC1),cp_NullValue(_read_.DPQTASC1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Legge qta movimentata
          this.w_oQTAAVA = 0
          this.w_oQTADIC = 0
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select OLTQTPRO AS CLQTAAVA, OLTFLEVA AS CLFASEVA  from "+i_cTable+" ODL_MAST ";
                +" where OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLTSECPR)+"";
                 ,"_Curs_ODL_MAST")
          else
            select OLTQTPRO AS CLQTAAVA, OLTFLEVA AS CLFASEVA from (i_cTable);
             where OLCODODL = this.oParentObject.w_OLTSECPR;
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            this.w_oQTAAVA = NVL(_Curs_ODL_MAST.CLQTAAVA, 0)
            this.w_oFLGEVA = NVL(_Curs_ODL_MAST.CLFASEVA, "N")
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
          * --- Cicla sulle fasi selezionate e CP precedenti
          * --- All'uscita dalla select in _oQTAAVA c'e' la qta evasa nella fase precedente, in w_oFLGEVA se la fase e' evasa
          if this.oParentObject.w_OLTQTOEV + this.oParentObject.w_DPQTAPRO + this.oParentObject.w_DPQTASCA - (this.w_oQTAMOV+this.w_oQTASCA) > this.w_oQTAAVA
            this.w_Msg = "Impossibile avanzare una fase per una quantit� maggiore della fase count point precedente (%1 %2)%0"
            ah_ErrorMsg(this.w_Msg,16,"",this.oParentObject.w_DPUNIMIS, tran(this.w_oQTAAVA,v_PQ(12)))
            this.oParentObject.w_TESTFORM = .F.
          endif
          * --- Altro controllo, non si puo' chiudere una fase se non e' chiusa la precedente
          if this.oParentObject.w_TESTFORM
            if this.oParentObject.w_DPFLEVAS="S" and this.w_OFLGEVA<>"S"
              this.w_Msg = "Impossibile completare una fase senza aver completato la precedente count point"
              ah_ErrorMsg(this.w_Msg,16)
              this.oParentObject.w_TESTFORM = .F.
            endif
          endif
        endif
        * --- Gestione Matricole: forza l'aggiornamento del figlio
        if this.oParentObject.w_TESTFORM and this.oParentObject.w_MATENABL and this.oParentObject.w_DPCOMMAT="S"
          this.GSCO_MCO = this.GSCO_ADP.GSCO_MCO
          if this.GSCO_MCO.class = "Stdlazychild"
            this.GSCO_MCO.LinkPCClick()     
            this.oParentObject.w_TESTFORM = .F.
          else
            this.GSCO_MCO.cnt.bUpdated = True
          endif
        endif
      case this.TipOpe="CARICA"
        this.oParentObject.w_TESTFORM = .T.
        if NOT EMPTY(this.oParentObject.w_DPSERIAL)
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_TESTFORM=.T.
            * --- Read from PAR_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PPGESWIP"+;
                " from "+i_cTable+" PAR_PROD where ";
                    +"PPCODICE = "+cp_ToStrODBC("PP");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PPGESWIP;
                from (i_cTable) where;
                    PPCODICE = "PP";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_GESWIP = NVL(cp_ToDate(_read_.PPGESWIP),cp_NullValue(_read_.PPGESWIP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.oParentObject.w_STATOGES="EDIT"
              * --- Se variazione Rilegge nuovo Riferimento Documenti
              * --- Read from DIC_PROD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIC_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DPRIFCAR,DPRIFSCA"+;
                  " from "+i_cTable+" DIC_PROD where ";
                      +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DPRIFCAR,DPRIFSCA;
                  from (i_cTable) where;
                      DPSERIAL = this.oParentObject.w_DPSERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DPRIFCAR = NVL(cp_ToDate(_read_.DPRIFCAR),cp_NullValue(_read_.DPRIFCAR))
                this.oParentObject.w_DPRIFSCA = NVL(cp_ToDate(_read_.DPRIFSCA),cp_NullValue(_read_.DPRIFSCA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
        endif
        * --- Sblocco l'ODL (sbianco il flag OLTSOSPE)
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSOSPE ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTSOSPE');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                 )
        else
          update (i_cTable) set;
              OLTSOSPE = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.oParentObject.w_DPCODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.TipOpe="CREA"
        if this.w_CICLI
          * --- Imposto il flag evaso
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTQTOE1"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTQTOE1;
              from (i_cTable) where;
                  OLCODODL = this.oParentObject.w_DPCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CLDICUM1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Select from DIC_PROD
          i_nConn=i_TableProp[this.DIC_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select DPCODODL, SUM(DPQTASC1+DPQTAPR1) AS RDQTAAV1  from "+i_cTable+" DIC_PROD ";
                +" where DPCODODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+"";
                +" group by DPCODODL";
                 ,"_Curs_DIC_PROD")
          else
            select DPCODODL, SUM(DPQTASC1+DPQTAPR1) AS RDQTAAV1 from (i_cTable);
             where DPCODODL=this.oParentObject.w_DPCODODL;
             group by DPCODODL;
              into cursor _Curs_DIC_PROD
          endif
          if used('_Curs_DIC_PROD')
            select _Curs_DIC_PROD
            locate for 1=1
            do while not(eof())
            * --- Quantit� totale dichiarata (compresa ultima)
            this.w_DPDICUM1 = NVL(_Curs_DIC_PROD.RDQTAAV1,0)
              select _Curs_DIC_PROD
              continue
            enddo
            use
          endif
          this.w_CLFASEVA = IIF(this.oParentObject.w_DPFLEVAS="S" or this.w_CLDICUM1 <= this.w_DPDICUM1 , "S", "N")
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLQTAAVA =CLQTAAVA+ "+cp_ToStrODBC(this.oParentObject.w_DPQTAPRO);
            +",CLAVAUM1 =CLAVAUM1+ "+cp_ToStrODBC(this.oParentObject.w_DPQTAPR1);
            +",CLQTASCA =CLQTASCA+ "+cp_ToStrODBC(this.oParentObject.w_DPQTASCA);
            +",CLSCAUM1 =CLSCAUM1+ "+cp_ToStrODBC(this.oParentObject.w_DPQTASC1);
            +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLFASEVA),'ODL_CICL','CLFASEVA');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                +" and CLBFRIFE = "+cp_ToStrODBC(this.oParentObject.w_FASE);
                   )
          else
            update (i_cTable) set;
                CLQTAAVA = CLQTAAVA + this.oParentObject.w_DPQTAPRO;
                ,CLAVAUM1 = CLAVAUM1 + this.oParentObject.w_DPQTAPR1;
                ,CLQTASCA = CLQTASCA + this.oParentObject.w_DPQTASCA;
                ,CLSCAUM1 = CLSCAUM1 + this.oParentObject.w_DPQTASC1;
                ,CLFASEVA = this.w_CLFASEVA;
                &i_ccchkf. ;
             where;
                CLCODODL = this.oParentObject.w_DPODLFAS;
                and CLBFRIFE = this.oParentObject.w_FASE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_CLFASEVA="S"
            * --- Ripristina la quantita dichiarabile
            * --- Read from ODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLAVAUM1,CLQTAAVA,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1"+;
                " from "+i_cTable+" ODL_CICL where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_DPCPRFAS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLAVAUM1,CLQTAAVA,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1;
                from (i_cTable) where;
                    CLCODODL = this.oParentObject.w_DPODLFAS;
                    and CPROWNUM = this.oParentObject.w_DPCPRFAS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLAVAUM1 = NVL(cp_ToDate(_read_.CLAVAUM1),cp_NullValue(_read_.CLAVAUM1))
              this.w_CLQTAAVA = NVL(cp_ToDate(_read_.CLQTAAVA),cp_NullValue(_read_.CLQTAAVA))
              this.w_CLQTASCA = NVL(cp_ToDate(_read_.CLQTASCA),cp_NullValue(_read_.CLQTASCA))
              this.w_CLSCAUM1 = NVL(cp_ToDate(_read_.CLSCAUM1),cp_NullValue(_read_.CLSCAUM1))
              this.w_CLQTADIC = NVL(cp_ToDate(_read_.CLQTADIC),cp_NullValue(_read_.CLQTADIC))
              this.w_CLDICUM1 = NVL(cp_ToDate(_read_.CLDICUM1),cp_NullValue(_read_.CLDICUM1))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLQTADIC ="+cp_NullLink(cp_ToStrODBC(this.w_CLQTAAVA+this.w_CLQTASCA),'ODL_CICL','CLQTADIC');
              +",CLDICUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_CLAVAUM1+this.w_CLSCAUM1),'ODL_CICL','CLDICUM1');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                  +" and CLROWORD > "+cp_ToStrODBC(this.oParentObject.w_FASE);
                     )
            else
              update (i_cTable) set;
                  CLQTADIC = this.w_CLQTAAVA+this.w_CLQTASCA;
                  ,CLDICUM1 = this.w_CLAVAUM1+this.w_CLSCAUM1;
                  &i_ccchkf. ;
               where;
                  CLCODODL = this.oParentObject.w_DPODLFAS;
                  and CLROWORD > this.oParentObject.w_FASE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      case this.TipOpe="ELIMINA"
        * --- Elimina Documenti associati al Piano (sotto Transazione)
        this.w_TESTPRO = .f.
        this.oParentObject.w_TESTFORM = .T.
        this.w_NOMSG = "S"
        if NOT EMPTY(this.oParentObject.w_DPSERIAL)
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_TESTFORM=.T.
            if this.w_CICLI
              * --- Cicli di lavorazione attivi
              this.w_TESTPRO = NOT EMPTY(this.oParentObject.w_DPRIFCAR) OR NOT EMPTY(this.oParentObject.w_DPRIFSCA) OR NOT EMPTY(this.oParentObject.w_DPRIFSER) OR NOT EMPTY(this.oParentObject.w_DPRIFMOU)
            else
              * --- Dichiarazione standard
              this.w_TESTPRO = NOT EMPTY(this.oParentObject.w_DPRIFCAR) OR NOT EMPTY(this.oParentObject.w_DPRIFSCA) OR NOT EMPTY(this.oParentObject.w_DPRIFMOU)
            endif
            if this.w_TESTPRO
              if this.oParentObject.w_STATOGES="QUERY"
                this.w_oMess.AddMsgPartNL("ATTENZIONE:%0La cancellazione della dichiarazione di produzione comporter�%0l'eliminazione di tutti i documenti di carico / scarico ad essa associati")     
                this.w_oMess.AddMsgPartNL("Verr� inoltre ripristinato sull'ODL, l'ordinato/impegnato precedente%0Confermi eliminazione?")     
                this.w_OK = this.w_oMess.ah_YesNo()
                this.TmpC = ah_Msgformat("Eliminazione abbandonata")
              else
                this.w_OK = ah_YesNo("ATTENZIONE:%0La modifica della dichiarazione di produzione comporter�%0l'aggiornamento di tutti i documenti di carico / scarico ad essa associati%0Confermi variazione?")
                this.TmpC = ah_Msgformat("Variazione abbandonata")
              endif
              if this.w_OK=.T.
                if NOT EMPTY(this.oParentObject.w_DPRIFCAR)
                  this.w_SERDOC = this.oParentObject.w_DPRIFCAR
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                if NOT EMPTY(this.oParentObject.w_DPRIFSCA)
                  this.w_SERDOC = this.oParentObject.w_DPRIFSCA
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                if NOT EMPTY(this.oParentObject.w_DPRIFMOU)
                  this.w_SERDOC = this.oParentObject.w_DPRIFMOU
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                if this.w_CICLI
                  if NOT EMPTY(this.oParentObject.w_DPRIFSER)
                    this.w_SERDOC = this.oParentObject.w_DPRIFSER
                    this.Page_7()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
                if this.w_OK=.T.
                  this.w_OQTOR1 = 0
                  this.w_OQTEVA = 0
                  this.w_OQTEV1 = 0
                  this.w_OQTSAL = 0
                  this.w_NQTSAL = 0
                  this.w_OKEYSA = SPACE(20)
                  this.w_OCOMAG = SPACE(5)
                  this.w_OSTATO = "L"
                  this.w_ODTFIN = cp_CharToDate("  -  -  ")
                  this.w_QDPPRO = this.oParentObject.w_DPQTAPRO
                  this.w_QDPSCA = this.oParentObject.w_DPQTASCA
                  this.w_QDPPR1 = this.oParentObject.w_DPQTAPR1
                  this.w_QDPSC1 = this.oParentObject.w_DPQTASC1
                  if this.oParentObject.w_STATOGES<>"QUERY"
                    * --- Se variazione Storna il valore Originario
                    * --- Read from DIC_PROD
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.DIC_PROD_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "DPQTAPR1,DPQTAPRO,DPQTASC1,DPQTASCA"+;
                        " from "+i_cTable+" DIC_PROD where ";
                            +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERIAL);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        DPQTAPR1,DPQTAPRO,DPQTASC1,DPQTASCA;
                        from (i_cTable) where;
                            DPSERIAL = this.oParentObject.w_DPSERIAL;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_QDPPR1 = NVL(cp_ToDate(_read_.DPQTAPR1),cp_NullValue(_read_.DPQTAPR1))
                      this.w_QDPPRO = NVL(cp_ToDate(_read_.DPQTAPRO),cp_NullValue(_read_.DPQTAPRO))
                      this.w_QDPSC1 = NVL(cp_ToDate(_read_.DPQTASC1),cp_NullValue(_read_.DPQTASC1))
                      this.w_QDPSCA = NVL(cp_ToDate(_read_.DPQTASCA),cp_NullValue(_read_.DPQTASCA))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Legge Materiali
                    vq_exec("..\COLA\EXE\QUERY\GSCO_QMA.VQR",this,"OLDMAT")
                  endif
                  * --- --
                  * --- --
                  * --- Read from ODL_MAST
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTFIN,OLTPERAS,OLTCOMME,OLTQTPRO,OLTQTPR1,OLTQTOSC,OLTQTOS1"+;
                      " from "+i_cTable+" ODL_MAST where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTFIN,OLTPERAS,OLTCOMME,OLTQTPRO,OLTQTPR1,OLTQTOSC,OLTQTOS1;
                      from (i_cTable) where;
                          OLCODODL = this.oParentObject.w_DPCODODL;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
                    this.w_OQTEVA = NVL(cp_ToDate(_read_.OLTQTOEV),cp_NullValue(_read_.OLTQTOEV))
                    this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
                    this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
                    this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
                    this.w_OCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
                    this.w_OSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                    this.w_ODTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
                    this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
                    this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
                    this.w_OQTPRO = NVL(cp_ToDate(_read_.OLTQTPRO),cp_NullValue(_read_.OLTQTPRO))
                    this.w_OQTPR1 = NVL(cp_ToDate(_read_.OLTQTPR1),cp_NullValue(_read_.OLTQTPR1))
                    this.w_OQTOSC = NVL(cp_ToDate(_read_.OLTQTOSC),cp_NullValue(_read_.OLTQTOSC))
                    this.w_OQTOS1 = NVL(cp_ToDate(_read_.OLTQTOS1),cp_NullValue(_read_.OLTQTOS1))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_NQTEVA = this.w_OQTEVA - (this.w_QDPPRO+this.w_QDPSCA)
                  this.w_NQTEV1 = this.w_OQTEV1 - (this.w_QDPPR1+this.w_QDPSC1)
                  this.w_NQTPRO = this.w_OQTPRO - this.w_QDPPRO
                  this.w_NQTPR1 = this.w_OQTPR1 - this.w_QDPPR1
                  this.w_NQTOSC = this.w_OQTOSC - this.w_QDPSCA
                  this.w_NQTOS1 = this.w_OQTOS1 - this.w_QDPSC1
                  * --- Aggiorna Qta Evasa sull ODL
                  this.w_NQTSAL = MAX(this.w_OQTOR1-this.w_NQTEV1, 0)
                  * --- Ripristina lo Stato "Lanciato"
                  this.w_OSTATO = IIF(this.w_NQTSAL=0, "F", "L")
                  this.w_ODTFIN = IIF(this.w_OSTATO="L", cp_CharToDate("  -  -  "), this.w_ODTFIN)
                  * --- Write into ODL_MAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OLTQTOEV ="+cp_NullLink(cp_ToStrODBC(this.w_NQTEVA),'ODL_MAST','OLTQTOEV');
                    +",OLTQTOE1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTEV1),'ODL_MAST','OLTQTOE1');
                    +",OLTQTPRO ="+cp_NullLink(cp_ToStrODBC(this.w_NQTPRO),'ODL_MAST','OLTQTPRO');
                    +",OLTQTPR1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTPR1),'ODL_MAST','OLTQTPR1');
                    +",OLTQTOSC ="+cp_NullLink(cp_ToStrODBC(this.w_NQTOSC),'ODL_MAST','OLTQTOSC');
                    +",OLTQTOS1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTOS1),'ODL_MAST','OLTQTOS1');
                    +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
                    +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTSAL),'ODL_MAST','OLTQTSAL');
                    +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OSTATO),'ODL_MAST','OLTSTATO');
                    +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
                        +i_ccchkf ;
                    +" where ";
                        +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                           )
                  else
                    update (i_cTable) set;
                        OLTQTOEV = this.w_NQTEVA;
                        ,OLTQTOE1 = this.w_NQTEV1;
                        ,OLTQTPRO = this.w_NQTPRO;
                        ,OLTQTPR1 = this.w_NQTPR1;
                        ,OLTQTOSC = this.w_NQTOSC;
                        ,OLTQTOS1 = this.w_NQTOS1;
                        ,OLTFLEVA = " ";
                        ,OLTQTSAL = this.w_NQTSAL;
                        ,OLTSTATO = this.w_OSTATO;
                        ,OLTDTFIN = this.w_ODTFIN;
                        &i_ccchkf. ;
                     where;
                        OLCODODL = this.oParentObject.w_DPCODODL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore in scrittura ODL_MAST'
                    return
                  endif
                  * --- Aggiorna saldi MPS
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARCODFAM,ARUNMIS1,ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARCODFAM,ARUNMIS1,ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_OKEYSA;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_FAMPRO = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
                    this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  * --- Try
                  local bErr_04E99248
                  bErr_04E99248=bTrsErr
                  this.Try_04E99248()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_04E99248
                  * --- End
                  * --- Write into MPS_TFOR
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_NQTSAL);
                    +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.w_NQTSAL);
                        +i_ccchkf ;
                    +" where ";
                        +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                        +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                           )
                  else
                    update (i_cTable) set;
                        FMMPSLAN = FMMPSLAN + this.w_NQTSAL;
                        ,FMMPSTOT = FMMPSTOT + this.w_NQTSAL;
                        &i_ccchkf. ;
                     where;
                        FMCODART = this.w_OKEYSA;
                        and FMPERASS = this.w_PERASS;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Storna Ordinato dai Saldi
                  this.w_NQTSAL = this.w_NQTSAL - this.w_OQTSAL 
                  if this.w_NQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OCOMAG)
                    * --- Write into SALDIART
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDIART_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                          +i_ccchkf ;
                      +" where ";
                          +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                          +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                             )
                    else
                      update (i_cTable) set;
                          SLQTOPER = SLQTOPER + this.w_NQTSAL;
                          &i_ccchkf. ;
                       where;
                          SLCODICE = this.w_OKEYSA;
                          and SLCODMAG = this.w_OCOMAG;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore in scrittura SALDIART (3)'
                      return
                    endif
                    * --- Saldi commessa
                    if this.w_SALCOM="S"
                      if empty(nvl(this.w_OLTCOMME,""))
                        this.w_COMMAPPO = this.w_COMMDEFA
                      else
                        this.w_COMMAPPO = this.w_OLTCOMME
                      endif
                      * --- Write into SALDICOM
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDICOM_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                            +i_ccchkf ;
                        +" where ";
                            +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                            +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                               )
                      else
                        update (i_cTable) set;
                            SCQTOPER = SCQTOPER + this.w_NQTSAL;
                            &i_ccchkf. ;
                         where;
                            SCCODICE = this.w_OKEYSA;
                            and SCCODMAG = this.w_OCOMAG;
                            and SCCODCAN = this.w_COMMAPPO;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura SALDICOM'
                        return
                      endif
                    endif
                  endif
                  if this.oParentObject.w_DPULTFAS="S" and !Empty(this.oParentObject.w_DPODLFAS)
                    * --- Riapro anche il padre (per gli ODL di fase)
                    * --- Read from ODL_MAST
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTFIN,OLTPERAS,OLTCOMME,OLTQTPRO,OLTQTPR1,OLTQTOSC,OLTQTOS1"+;
                        " from "+i_cTable+" ODL_MAST where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTFIN,OLTPERAS,OLTCOMME,OLTQTPRO,OLTQTPR1,OLTQTOSC,OLTQTOS1;
                        from (i_cTable) where;
                            OLCODODL = this.oParentObject.w_DPODLFAS;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
                      this.w_OQTEVA = NVL(cp_ToDate(_read_.OLTQTOEV),cp_NullValue(_read_.OLTQTOEV))
                      this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
                      this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
                      this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
                      this.w_OCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
                      this.w_OSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                      this.w_ODTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
                      this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
                      this.w_FASCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
                      this.w_OQTPRO = NVL(cp_ToDate(_read_.OLTQTPRO),cp_NullValue(_read_.OLTQTPRO))
                      this.w_OQTPR1 = NVL(cp_ToDate(_read_.OLTQTPR1),cp_NullValue(_read_.OLTQTPR1))
                      this.w_OQTOSC = NVL(cp_ToDate(_read_.OLTQTOSC),cp_NullValue(_read_.OLTQTOSC))
                      this.w_OQTOS1 = NVL(cp_ToDate(_read_.OLTQTOS1),cp_NullValue(_read_.OLTQTOS1))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.w_NQTEVA = this.w_OQTEVA - (this.w_QDPPRO+this.w_QDPSCA)
                    this.w_NQTEV1 = this.w_OQTEV1 - (this.w_QDPPR1+this.w_QDPSC1)
                    this.w_NQTPRO = this.w_OQTPRO - this.w_QDPPRO
                    this.w_NQTPR1 = this.w_OQTPR1 - this.w_QDPPR1
                    this.w_NQTOSC = this.w_OQTOSC - this.w_QDPSCA
                    this.w_NQTOS1 = this.w_OQTOS1 - this.w_QDPSC1
                    * --- Aggiorna Qta Evasa sull ODL
                    this.w_NQTSAL = MAX(this.w_OQTOR1-this.w_NQTEV1, 0)
                    * --- Ripristina lo Stato "Lanciato"
                    this.w_OSTATO = IIF(this.w_NQTSAL=0, "F", "L")
                    this.w_ODTFIN = IIF(this.w_OSTATO="L", cp_CharToDate("  -  -  "), this.w_ODTFIN)
                    * --- Write into ODL_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLTQTOEV ="+cp_NullLink(cp_ToStrODBC(this.w_NQTEVA),'ODL_MAST','OLTQTOEV');
                      +",OLTQTOE1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTEV1),'ODL_MAST','OLTQTOE1');
                      +",OLTQTPRO ="+cp_NullLink(cp_ToStrODBC(this.w_NQTPRO),'ODL_MAST','OLTQTPRO');
                      +",OLTQTPR1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTPR1),'ODL_MAST','OLTQTPR1');
                      +",OLTQTOSC ="+cp_NullLink(cp_ToStrODBC(this.w_NQTOSC),'ODL_MAST','OLTQTOSC');
                      +",OLTQTOS1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTOS1),'ODL_MAST','OLTQTOS1');
                      +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
                      +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTSAL),'ODL_MAST','OLTQTSAL');
                      +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OSTATO),'ODL_MAST','OLTSTATO');
                      +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                             )
                    else
                      update (i_cTable) set;
                          OLTQTOEV = this.w_NQTEVA;
                          ,OLTQTOE1 = this.w_NQTEV1;
                          ,OLTQTPRO = this.w_NQTPRO;
                          ,OLTQTPR1 = this.w_NQTPR1;
                          ,OLTQTOSC = this.w_NQTOSC;
                          ,OLTQTOS1 = this.w_NQTOS1;
                          ,OLTFLEVA = " ";
                          ,OLTQTSAL = this.w_NQTSAL;
                          ,OLTSTATO = this.w_OSTATO;
                          ,OLTDTFIN = this.w_ODTFIN;
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.oParentObject.w_DPODLFAS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore in scrittura ODL_MAST'
                      return
                    endif
                    * --- Aggiorna saldi MPS
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARCODFAM,ARUNMIS1,ARSALCOM"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARCODFAM,ARUNMIS1,ARSALCOM;
                        from (i_cTable) where;
                            ARCODART = this.w_OKEYSA;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_FAMPRO = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
                      this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Try
                    local bErr_04EB79A0
                    bErr_04EB79A0=bTrsErr
                    this.Try_04EB79A0()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_04EB79A0
                    * --- End
                    * --- Write into MPS_TFOR
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_NQTSAL);
                      +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.w_NQTSAL);
                          +i_ccchkf ;
                      +" where ";
                          +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                          +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                             )
                    else
                      update (i_cTable) set;
                          FMMPSLAN = FMMPSLAN + this.w_NQTSAL;
                          ,FMMPSTOT = FMMPSTOT + this.w_NQTSAL;
                          &i_ccchkf. ;
                       where;
                          FMCODART = this.w_OKEYSA;
                          and FMPERASS = this.w_PERASS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                    * --- Storna Ordinato dai Saldi
                    this.w_NQTSAL = this.w_NQTSAL - this.w_OQTSAL 
                    if this.w_NQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OCOMAG)
                      * --- Write into SALDIART
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDIART_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                            +i_ccchkf ;
                        +" where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                               )
                      else
                        update (i_cTable) set;
                            SLQTOPER = SLQTOPER + this.w_NQTSAL;
                            &i_ccchkf. ;
                         where;
                            SLCODICE = this.w_OKEYSA;
                            and SLCODMAG = this.w_OCOMAG;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura SALDIART (3)'
                        return
                      endif
                      * --- Saldi commessa
                      if this.w_SALCOM="S"
                        if empty(nvl(this.w_FASCOMME,""))
                          this.w_COMMAPPO = this.w_COMMDEFA
                        else
                          this.w_COMMAPPO = this.w_FASCOMME
                        endif
                        * --- Write into SALDICOM
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDICOM_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_NQTSAL);
                              +i_ccchkf ;
                          +" where ";
                              +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                              +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                 )
                        else
                          update (i_cTable) set;
                              SCQTOPER = SCQTOPER + this.w_NQTSAL;
                              &i_ccchkf. ;
                           where;
                              SCCODICE = this.w_OKEYSA;
                              and SCCODMAG = this.w_OCOMAG;
                              and SCCODCAN = this.w_COMMAPPO;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore in scrittura SALDICOM'
                          return
                        endif
                      endif
                    endif
                  endif
                  if this.w_OLDCOM
                    * --- Storna il Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    * --- Primo giro, quantit� abbinata al magazzino
                    this.w_QTADAB = 0
                    this.w_PGCODODL = this.oParentObject.w_DPCODODL
                    this.w_PEQTAUM1 = this.oParentObject.w_DPQTAPR1
                    if this.w_PEQTAUM1>0
                      this.w_PECODODL = " Magazz.: "+this.oParentObject.w_DPCODMAG
                      * --- Select from PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB,PESERORD,PERIGORD,PERIFODL  from "+i_cTable+" PEG_SELI ";
                            +" where PEODLORI="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_PECODODL)+"";
                             ,"_Curs_PEG_SELI")
                      else
                        select PESERIAL,PEQTAABB,PESERORD,PERIGORD,PERIFODL from (i_cTable);
                         where PEODLORI=this.oParentObject.w_DPCODODL AND PETIPRIF="M" AND PESERODL=this.w_PECODODL;
                          into cursor _Curs_PEG_SELI
                      endif
                      if used('_Curs_PEG_SELI')
                        select _Curs_PEG_SELI
                        locate for 1=1
                        do while not(eof())
                        if this.w_PEQTAUM1>0
                          this.w_SERORD = _Curs_PEG_SELI.PESERORD
                          this.w_RIFODL = _Curs_PEG_SELI.PERIFODL
                          this.w_RIGORD = _Curs_PEG_SELI.PERIGORD
                          this.w_TIPRIF = iif(empty(nvl(this.w_RIFODL,"")),"D","O")
                          if _Curs_PEG_SELI.PEQTAABB>this.w_PEQTAUM1
                            * --- Write into PEG_SELI
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_ccchkf=''
                            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                              +"PEQTAABB =PEQTAABB- "+cp_ToStrODBC(this.w_PEQTAUM1);
                                  +i_ccchkf ;
                              +" where ";
                                  +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                                     )
                            else
                              update (i_cTable) set;
                                  PEQTAABB = PEQTAABB - this.w_PEQTAUM1;
                                  &i_ccchkf. ;
                               where;
                                  PESERIAL = _Curs_PEG_SELI.PESERIAL;

                              i_Rows = _tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              i_Error=MSG_WRITE_ERROR
                              return
                            endif
                            this.w_QTADAB = this.w_PEQTAUM1
                            this.w_PEQTAUM1 = 0
                            this.w_RIGUPD = "N"
                            * --- Select from GSCO10BDP
                            do vq_exec with 'GSCO10BDP',this,'_Curs_GSCO10BDP','',.f.,.t.
                            if used('_Curs_GSCO10BDP')
                              select _Curs_GSCO10BDP
                              locate for 1=1
                              do while not(eof())
                              * --- Write into PEG_SELI
                              i_commit = .f.
                              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                cp_BeginTrs()
                                i_commit = .t.
                              endif
                              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                              i_ccchkf=''
                              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                              if i_nConn<>0
                                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                                +"PEQTAABB =PEQTAABB+ "+cp_ToStrODBC(this.w_QTADAB);
                                    +i_ccchkf ;
                                +" where ";
                                    +"PESERIAL = "+cp_ToStrODBC(_Curs_GSCO10BDP.PESERIAL);
                                       )
                              else
                                update (i_cTable) set;
                                    PEQTAABB = PEQTAABB + this.w_QTADAB;
                                    &i_ccchkf. ;
                                 where;
                                    PESERIAL = _Curs_GSCO10BDP.PESERIAL;

                                i_Rows = _tally
                              endif
                              if i_commit
                                cp_EndTrs(.t.)
                              endif
                              if bTrsErr
                                i_Error=MSG_WRITE_ERROR
                                return
                              endif
                              this.w_QTADAB = 0
                              this.w_RIGUPD = "S"
                                select _Curs_GSCO10BDP
                                continue
                              enddo
                              use
                            endif
                            if this.w_RIGUPD="N"
                              * --- Select from GSDBMBPG
                              do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
                              if used('_Curs_GSDBMBPG')
                                select _Curs_GSDBMBPG
                                locate for 1=1
                                do while not(eof())
                                this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
                                  select _Curs_GSDBMBPG
                                  continue
                                enddo
                                use
                              endif
                              this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
                              this.w_PROGR = max(this.w_PROGR + 1,1)
                              this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                              if this.w_TIPRIF="D"
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_SERORD),'PEG_SELI','PESERORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"D",'PESERORD',this.w_SERORD,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"D";
                                       ,this.w_SERORD;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              else
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIFODL),'PEG_SELI','PERIFODL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"O",'PERIFODL',this.w_RIFODL,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"O";
                                       ,this.w_RIFODL;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              endif
                            endif
                          else
                            * --- Delete from PEG_SELI
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                    +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                                     )
                            else
                              delete from (i_cTable) where;
                                    PESERIAL = _Curs_PEG_SELI.PESERIAL;

                              i_Rows=_tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              * --- Error: delete not accepted
                              i_Error=MSG_DELETE_ERROR
                              return
                            endif
                            this.w_PEQTAUM1 = this.w_PEQTAUM1-_Curs_PEG_SELI.PEQTAABB
                            this.w_QTADAB = _Curs_PEG_SELI.PEQTAABB
                            this.w_RIGUPD = "N"
                            * --- Select from GSCO10BDP
                            do vq_exec with 'GSCO10BDP',this,'_Curs_GSCO10BDP','',.f.,.t.
                            if used('_Curs_GSCO10BDP')
                              select _Curs_GSCO10BDP
                              locate for 1=1
                              do while not(eof())
                              * --- Write into PEG_SELI
                              i_commit = .f.
                              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                cp_BeginTrs()
                                i_commit = .t.
                              endif
                              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                              i_ccchkf=''
                              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                              if i_nConn<>0
                                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                                +"PEQTAABB =PEQTAABB+ "+cp_ToStrODBC(this.w_QTADAB);
                                    +i_ccchkf ;
                                +" where ";
                                    +"PESERIAL = "+cp_ToStrODBC(_Curs_GSCO10BDP.PESERIAL);
                                       )
                              else
                                update (i_cTable) set;
                                    PEQTAABB = PEQTAABB + this.w_QTADAB;
                                    &i_ccchkf. ;
                                 where;
                                    PESERIAL = _Curs_GSCO10BDP.PESERIAL;

                                i_Rows = _tally
                              endif
                              if i_commit
                                cp_EndTrs(.t.)
                              endif
                              if bTrsErr
                                i_Error=MSG_WRITE_ERROR
                                return
                              endif
                              this.w_QTADAB = 0
                              this.w_RIGUPD = "S"
                                select _Curs_GSCO10BDP
                                continue
                              enddo
                              use
                            endif
                            if this.w_RIGUPD="N"
                              * --- Select from GSDBMBPG
                              do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
                              if used('_Curs_GSDBMBPG')
                                select _Curs_GSDBMBPG
                                locate for 1=1
                                do while not(eof())
                                this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
                                  select _Curs_GSDBMBPG
                                  continue
                                enddo
                                use
                              endif
                              this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
                              this.w_PROGR = max(this.w_PROGR + 1,1)
                              this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                              if this.w_TIPRIF="D"
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_SERORD),'PEG_SELI','PESERORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"D",'PESERORD',this.w_SERORD,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"D";
                                       ,this.w_SERORD;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              else
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIFODL),'PEG_SELI','PERIFODL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"O",'PERIFODL',this.w_RIFODL,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"O";
                                       ,this.w_RIFODL;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              endif
                            endif
                          endif
                        endif
                          select _Curs_PEG_SELI
                          continue
                        enddo
                        use
                      endif
                    endif
                    this.w_PEQTAUM1 = this.oParentObject.w_DPQTASC1
                    * --- Secondo giro, quantit� abbinata al magazzino scarti
                    if this.w_PEQTAUM1>0
                      this.w_PECODODL = " Magazz.: "+this.oParentObject.w_MGSCAR
                      * --- Select from PEG_SELI
                      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB,PESERORD,PERIGORD,PERIFODL  from "+i_cTable+" PEG_SELI ";
                            +" where PEODLORI="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_PECODODL)+"";
                             ,"_Curs_PEG_SELI")
                      else
                        select PESERIAL,PEQTAABB,PESERORD,PERIGORD,PERIFODL from (i_cTable);
                         where PEODLORI=this.oParentObject.w_DPCODODL AND PETIPRIF="M" AND PESERODL=this.w_PECODODL;
                          into cursor _Curs_PEG_SELI
                      endif
                      if used('_Curs_PEG_SELI')
                        select _Curs_PEG_SELI
                        locate for 1=1
                        do while not(eof())
                        if this.w_PEQTAUM1>0
                          this.w_SERORD = _Curs_PEG_SELI.PESERORD
                          this.w_RIFODL = _Curs_PEG_SELI.PERIFODL
                          this.w_RIGORD = _Curs_PEG_SELI.PERIGORD
                          this.w_TIPRIF = iif(empty(nvl(this.w_RIFODL,"")),"D","O")
                          if _Curs_PEG_SELI.PEQTAABB>this.w_PEQTAUM1
                            * --- Write into PEG_SELI
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_ccchkf=''
                            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                              +"PEQTAABB =PEQTAABB- "+cp_ToStrODBC(this.w_PEQTAUM1);
                                  +i_ccchkf ;
                              +" where ";
                                  +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                                     )
                            else
                              update (i_cTable) set;
                                  PEQTAABB = PEQTAABB - this.w_PEQTAUM1;
                                  &i_ccchkf. ;
                               where;
                                  PESERIAL = _Curs_PEG_SELI.PESERIAL;

                              i_Rows = _tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              i_Error=MSG_WRITE_ERROR
                              return
                            endif
                            this.w_QTADAB = this.w_PEQTAUM1
                            this.w_PEQTAUM1 = 0
                            this.w_RIGUPD = "N"
                            * --- Select from GSCO10BDP
                            do vq_exec with 'GSCO10BDP',this,'_Curs_GSCO10BDP','',.f.,.t.
                            if used('_Curs_GSCO10BDP')
                              select _Curs_GSCO10BDP
                              locate for 1=1
                              do while not(eof())
                              * --- Write into PEG_SELI
                              i_commit = .f.
                              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                cp_BeginTrs()
                                i_commit = .t.
                              endif
                              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                              i_ccchkf=''
                              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                              if i_nConn<>0
                                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                                +"PEQTAABB =PEQTAABB+ "+cp_ToStrODBC(this.w_QTADAB);
                                    +i_ccchkf ;
                                +" where ";
                                    +"PESERIAL = "+cp_ToStrODBC(_Curs_GSCO10BDP.PESERIAL);
                                       )
                              else
                                update (i_cTable) set;
                                    PEQTAABB = PEQTAABB + this.w_QTADAB;
                                    &i_ccchkf. ;
                                 where;
                                    PESERIAL = _Curs_GSCO10BDP.PESERIAL;

                                i_Rows = _tally
                              endif
                              if i_commit
                                cp_EndTrs(.t.)
                              endif
                              if bTrsErr
                                i_Error=MSG_WRITE_ERROR
                                return
                              endif
                              this.w_QTADAB = 0
                              this.w_RIGUPD = "S"
                                select _Curs_GSCO10BDP
                                continue
                              enddo
                              use
                            endif
                            if this.w_RIGUPD="N"
                              * --- Select from GSDBMBPG
                              do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
                              if used('_Curs_GSDBMBPG')
                                select _Curs_GSDBMBPG
                                locate for 1=1
                                do while not(eof())
                                this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
                                  select _Curs_GSDBMBPG
                                  continue
                                enddo
                                use
                              endif
                              this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
                              this.w_PROGR = max(this.w_PROGR + 1,1)
                              this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                              if this.w_TIPRIF="D"
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_SERORD),'PEG_SELI','PESERORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"D",'PESERORD',this.w_SERORD,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"D";
                                       ,this.w_SERORD;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              else
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIFODL),'PEG_SELI','PERIFODL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"O",'PERIFODL',this.w_RIFODL,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"O";
                                       ,this.w_RIFODL;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              endif
                            endif
                          else
                            * --- Delete from PEG_SELI
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                    +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                                     )
                            else
                              delete from (i_cTable) where;
                                    PESERIAL = _Curs_PEG_SELI.PESERIAL;

                              i_Rows=_tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              * --- Error: delete not accepted
                              i_Error=MSG_DELETE_ERROR
                              return
                            endif
                            this.w_PEQTAUM1 = this.w_PEQTAUM1-_Curs_PEG_SELI.PEQTAABB
                            this.w_QTADAB = _Curs_PEG_SELI.PEQTAABB
                            this.w_RIGUPD = "N"
                            * --- Select from GSCO10BDP
                            do vq_exec with 'GSCO10BDP',this,'_Curs_GSCO10BDP','',.f.,.t.
                            if used('_Curs_GSCO10BDP')
                              select _Curs_GSCO10BDP
                              locate for 1=1
                              do while not(eof())
                              * --- Write into PEG_SELI
                              i_commit = .f.
                              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                cp_BeginTrs()
                                i_commit = .t.
                              endif
                              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                              i_ccchkf=''
                              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                              if i_nConn<>0
                                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                                +"PEQTAABB =PEQTAABB+ "+cp_ToStrODBC(this.w_QTADAB);
                                    +i_ccchkf ;
                                +" where ";
                                    +"PESERIAL = "+cp_ToStrODBC(_Curs_GSCO10BDP.PESERIAL);
                                       )
                              else
                                update (i_cTable) set;
                                    PEQTAABB = PEQTAABB + this.w_QTADAB;
                                    &i_ccchkf. ;
                                 where;
                                    PESERIAL = _Curs_GSCO10BDP.PESERIAL;

                                i_Rows = _tally
                              endif
                              if i_commit
                                cp_EndTrs(.t.)
                              endif
                              if bTrsErr
                                i_Error=MSG_WRITE_ERROR
                                return
                              endif
                              this.w_QTADAB = 0
                              this.w_RIGUPD = "S"
                                select _Curs_GSCO10BDP
                                continue
                              enddo
                              use
                            endif
                            if this.w_RIGUPD="N"
                              * --- Select from GSDBMBPG
                              do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
                              if used('_Curs_GSDBMBPG')
                                select _Curs_GSDBMBPG
                                locate for 1=1
                                do while not(eof())
                                this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
                                  select _Curs_GSDBMBPG
                                  continue
                                enddo
                                use
                              endif
                              this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
                              this.w_PROGR = max(this.w_PROGR + 1,1)
                              this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
                              if this.w_TIPRIF="D"
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_SERORD),'PEG_SELI','PESERORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"D",'PESERORD',this.w_SERORD,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"D";
                                       ,this.w_SERORD;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              else
                                * --- Insert into PEG_SELI
                                i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                                i_commit = .f.
                                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                                  cp_BeginTrs()
                                  i_commit = .t.
                                endif
                                i_ccchkf=''
                                i_ccchkv=''
                                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                                if i_nConn<>0
                                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                              " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                                  cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PESERODL');
                                  +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIFODL),'PEG_SELI','PERIFODL');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_QTADAB),'PEG_SELI','PEQTAABB');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGORD),'PEG_SELI','PERIGORD');
                                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                                  +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'PEG_SELI','PECODRIC');
                                       +i_ccchkv+")")
                                else
                                  cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_DPCODODL,'PETIPRIF',"O",'PERIFODL',this.w_RIFODL,'PEQTAABB',this.w_QTADAB,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_RIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_OKEYSA)
                                  insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                                     values (;
                                       this.w_PESERIAL;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,"O";
                                       ,this.w_RIFODL;
                                       ,this.w_QTADAB;
                                       ,this.oParentObject.w_DPCODODL;
                                       ,this.w_RIGORD;
                                       ,this.oParentObject.w_DPCODCOM;
                                       ,this.w_OKEYSA;
                                       &i_ccchkv. )
                                  i_Rows=iif(bTrsErr,0,1)
                                endif
                                if i_commit
                                  cp_EndTrs(.t.)
                                endif
                                if i_Rows<0 or bTrsErr
                                  * --- Error: insert not accepted
                                  i_Error=MSG_INSERT_ERROR
                                  return
                                endif
                              endif
                            endif
                          endif
                        endif
                          select _Curs_PEG_SELI
                          continue
                        enddo
                        use
                      endif
                    endif
                    this.w_PEQTAUM1 = 0
                  endif
                  if this.oParentObject.w_STATOGES="QUERY" OR USED("OLDMAT")
                    * --- Storna Impegnato
                    NC = IIF(this.oParentObject.w_STATOGES="QUERY", this.GSCO_ADP.GSCO_MMP.cTrsName, "OLDMAT")
                    OLDSET = SET("DELETED")
                    SET DELETED OFF
                    SELECT (NC)
                    this.w_RECO = RECNO()
                    GO TOP
                    SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(t_MPCODICE) AND t_MPSERODL=this.oParentObject.w_DPCODODL AND i_SRV<>"A"
                    this.w_PDROWNUM = t_MPROWODL
                    this.w_MPQTAMOV = t_MPQTAMOV
                    this.w_MPQTAUM1 = t_MPQTAUM1
                    this.w_MPQTAEVA = t_MPQTAEVA
                    this.w_MPQTAEV1 = t_MPQTAEV1
                    if this.w_PDROWNUM<>0
                      this.w_OQTOR1 = 0
                      this.w_OQTEVA = 0
                      this.w_OQTEV1 = 0
                      this.w_OQTSAL = 0
                      this.w_NQTSAL = 0
                      this.w_OKEYSA = SPACE(20)
                      this.w_OCOMAG = SPACE(5)
                      * --- Read from ODL_DETT
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "OLQTAUM1,OLQTAEVA,OLQTAEV1,OLKEYSAL,OLQTASAL,OLCODMAG,OLFLEVAS,OLFLORDI,OLFLIMPE,OLCODCOM"+;
                          " from "+i_cTable+" ODL_DETT where ";
                              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                              +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          OLQTAUM1,OLQTAEVA,OLQTAEV1,OLKEYSAL,OLQTASAL,OLCODMAG,OLFLEVAS,OLFLORDI,OLFLIMPE,OLCODCOM;
                          from (i_cTable) where;
                              OLCODODL = this.oParentObject.w_DPCODODL;
                              and CPROWNUM = this.w_PDROWNUM;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_OQTOR1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
                        this.w_OQTEVA = NVL(cp_ToDate(_read_.OLQTAEVA),cp_NullValue(_read_.OLQTAEVA))
                        this.w_OQTEV1 = NVL(cp_ToDate(_read_.OLQTAEV1),cp_NullValue(_read_.OLQTAEV1))
                        this.w_OKEYSA = NVL(cp_ToDate(_read_.OLKEYSAL),cp_NullValue(_read_.OLKEYSAL))
                        this.w_OQTSAL = NVL(cp_ToDate(_read_.OLQTASAL),cp_NullValue(_read_.OLQTASAL))
                        this.w_OCOMAG = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
                        this.w_OLFLEVAS = NVL(cp_ToDate(_read_.OLFLEVAS),cp_NullValue(_read_.OLFLEVAS))
                        this.w_OLFLORDI = NVL(cp_ToDate(_read_.OLFLORDI),cp_NullValue(_read_.OLFLORDI))
                        this.w_OLFLIMPE = NVL(cp_ToDate(_read_.OLFLIMPE),cp_NullValue(_read_.OLFLIMPE))
                        this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLCODCOM),cp_NullValue(_read_.OLCODCOM))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      * --- Verifica se � stata evasa una qta superiore a quanto era da produrre
                      this.w_DIFFE = max(this.w_OQTEV1 - this.w_OQTOR1 , 0)
                      * --- Calcola qta da stornare sui saldi articoli e sui saldi MPS
                      this.w_DASTOR = this.w_MPQTAEV1 - this.w_DIFFE
                      this.w_QTASC1 = this.w_DASTOR
                      if this.w_OLFLEVAS="S"
                        * --- Se la riga era evasa la riapre per tutta la quantit�
                        this.w_DASTOR = max(this.w_DASTOR, this.w_OQTOR1 + this.w_MPQTAEV1 - this.w_OQTEV1)
                      endif
                      if this.w_OLDCOM
                        * --- Storna il Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        this.w_QTADAB = 0
                        * --- Read from ODL_DETT
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "OLQTAPR1,OLCODMAG,OLMAGPRE"+;
                            " from "+i_cTable+" ODL_DETT where ";
                                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                                +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            OLQTAPR1,OLCODMAG,OLMAGPRE;
                            from (i_cTable) where;
                                OLCODODL = this.oParentObject.w_DPCODODL;
                                and CPROWNUM = this.w_PDROWNUM;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_QTAPR1 = NVL(cp_ToDate(_read_.OLQTAPR1),cp_NullValue(_read_.OLQTAPR1))
                          this.w_MAGTRA = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
                          this.w_MAGRIF = NVL(cp_ToDate(_read_.OLMAGPRE),cp_NullValue(_read_.OLMAGPRE))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        if this.w_QTAPR1>0
                          * --- Sottraggo alla quantit� trasferita per l'ODL quella gi� utilizzata
                          this.w_QTAPR1 = max(this.w_QTAPR1,0)
                          this.w_QTADAB = this.w_QTADAB+this.w_QTAPR1
                        endif
                        this.w_PEQTAUM1 = min(this.w_MPQTAUM1,this.w_QTADAB)
                        this.w_PEQTAOLD = this.w_PEQTAUM1
                        this.w_RIFMAG = " Magazz.: " + this.w_MAGTRA
                        * --- Primo giro quantit� abbinata al WIP terzista
                        * --- Select from PEG_SELI
                        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB  from "+i_cTable+" PEG_SELI ";
                              +" where PERIFODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" AND PERIGORD="+cp_ToStrODBC(this.w_PDROWNUM)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_RIFMAG)+"";
                               ,"_Curs_PEG_SELI")
                        else
                          select PESERIAL,PEQTAABB from (i_cTable);
                           where PERIFODL=this.oParentObject.w_DPCODODL AND PERIGORD=this.w_PDROWNUM AND PETIPRIF="M" AND PESERODL=this.w_RIFMAG;
                            into cursor _Curs_PEG_SELI
                        endif
                        if used('_Curs_PEG_SELI')
                          select _Curs_PEG_SELI
                          locate for 1=1
                          do while not(eof())
                          * --- Se la select non restituisce nulla fare insert?
                          * --- Write into PEG_SELI
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(_Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                                +i_ccchkf ;
                            +" where ";
                                +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                                   )
                          else
                            update (i_cTable) set;
                                PEQTAABB = _Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1;
                                &i_ccchkf. ;
                             where;
                                PESERIAL = _Curs_PEG_SELI.PESERIAL;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                          this.w_PEQTAUM1 = 0
                            select _Curs_PEG_SELI
                            continue
                          enddo
                          use
                        endif
                        this.w_PEQTAUM1 = this.w_MPQTAUM1-this.w_PEQTAOLD
                        this.w_RIFMAG = " Magazz.: " + this.w_MAGRIF
                        * --- Secondo giro, quantit� abbinata a magazzino originale di riferimento (solo materiale non trasferito)
                        if this.w_PEQTAUM1>0
                          * --- Select from PEG_SELI
                          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB  from "+i_cTable+" PEG_SELI ";
                                +" where PERIFODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" AND PERIGORD="+cp_ToStrODBC(this.w_PDROWNUM)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_RIFMAG)+"";
                                 ,"_Curs_PEG_SELI")
                          else
                            select PESERIAL,PEQTAABB from (i_cTable);
                             where PERIFODL=this.oParentObject.w_DPCODODL AND PERIGORD=this.w_PDROWNUM AND PETIPRIF="M" AND PESERODL=this.w_RIFMAG;
                              into cursor _Curs_PEG_SELI
                          endif
                          if used('_Curs_PEG_SELI')
                            select _Curs_PEG_SELI
                            locate for 1=1
                            do while not(eof())
                            * --- Se la select non restituisce nulla fare insert?
                            * --- Write into PEG_SELI
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                            i_ccchkf=''
                            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                              +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(_Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                                  +i_ccchkf ;
                              +" where ";
                                  +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                                     )
                            else
                              update (i_cTable) set;
                                  PEQTAABB = _Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1;
                                  &i_ccchkf. ;
                               where;
                                  PESERIAL = _Curs_PEG_SELI.PESERIAL;

                              i_Rows = _tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              i_Error=MSG_WRITE_ERROR
                              return
                            endif
                            this.w_PEQTAUM1 = 0
                              select _Curs_PEG_SELI
                              continue
                            enddo
                            use
                          endif
                        endif
                      endif
                      * --- Aggiorna Qta Evasa sull ODL
                      * --- Write into ODL_DETT
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"OLQTAEVA =OLQTAEVA- "+cp_ToStrODBC(this.w_MPQTAEVA);
                        +",OLQTAEV1 =OLQTAEV1- "+cp_ToStrODBC(this.w_MPQTAEV1);
                        +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLEVAS');
                        +",OLQTASAL =OLQTASAL+ "+cp_ToStrODBC(this.w_DASTOR);
                            +i_ccchkf ;
                        +" where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                            +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                               )
                      else
                        update (i_cTable) set;
                            OLQTAEVA = OLQTAEVA - this.w_MPQTAEVA;
                            ,OLQTAEV1 = OLQTAEV1 - this.w_MPQTAEV1;
                            ,OLFLEVAS = " ";
                            ,OLQTASAL = OLQTASAL + this.w_DASTOR;
                            &i_ccchkf. ;
                         where;
                            OLCODODL = this.oParentObject.w_DPCODODL;
                            and CPROWNUM = this.w_PDROWNUM;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura ODL_DETT'
                        return
                      endif
                      * --- Se Nettificabile, Storna Impegnato dai Saldi
                      this.w_DISMAG = " "
                      * --- Read from MAGAZZIN
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "MGDISMAG"+;
                          " from "+i_cTable+" MAGAZZIN where ";
                              +"MGCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          MGDISMAG;
                          from (i_cTable) where;
                              MGCODMAG = this.w_OCOMAG;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_DASTOR<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OCOMAG)
                        if this.w_DISMAG="S"
                          * --- Write into SALDIART
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.SALDIART_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                            i_cOp1=cp_SetTrsOp(this.w_OLFLORDI,'SLQTOPER','this.w_DASTOR',this.w_DASTOR,'update',i_nConn)
                            i_cOp2=cp_SetTrsOp(this.w_OLFLIMPE,'SLQTIPER','this.w_DASTOR',this.w_DASTOR,'update',i_nConn)
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                                +i_ccchkf ;
                            +" where ";
                                +"SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                                +" and SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                                   )
                          else
                            update (i_cTable) set;
                                SLQTOPER = &i_cOp1.;
                                ,SLQTIPER = &i_cOp2.;
                                &i_ccchkf. ;
                             where;
                                SLCODMAG = this.w_OCOMAG;
                                and SLCODICE = this.w_OKEYSA;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                          * --- Saldi commessa
                          * --- Read from ART_ICOL
                          i_nOldArea=select()
                          if used('_read_')
                            select _read_
                            use
                          endif
                          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select "+;
                              "ARSALCOM"+;
                              " from "+i_cTable+" ART_ICOL where ";
                                  +"ARCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                                   ,"_read_")
                            i_Rows=iif(used('_read_'),reccount(),0)
                          else
                            select;
                              ARSALCOM;
                              from (i_cTable) where;
                                  ARCODART = this.w_OKEYSA;
                               into cursor _read_
                            i_Rows=_tally
                          endif
                          if used('_read_')
                            locate for 1=1
                            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                            use
                          else
                            * --- Error: sql sentence error.
                            i_Error = MSG_READ_ERROR
                            return
                          endif
                          select (i_nOldArea)
                          if this.w_SALCOM="S"
                            if empty(nvl(this.w_OLTCOMME,""))
                              this.w_COMMAPPO = this.w_COMMDEFA
                            else
                              this.w_COMMAPPO = this.w_OLTCOMME
                            endif
                            * --- Write into SALDICOM
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_nConn=i_TableProp[this.SALDICOM_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                              i_cOp1=cp_SetTrsOp(this.w_OLFLORDI,'SCQTOPER','this.w_DASTOR',this.w_DASTOR,'update',i_nConn)
                              i_cOp2=cp_SetTrsOp(this.w_OLFLIMPE,'SCQTIPER','this.w_DASTOR',this.w_DASTOR,'update',i_nConn)
                            i_ccchkf=''
                            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                                  +i_ccchkf ;
                              +" where ";
                                  +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                     )
                            else
                              update (i_cTable) set;
                                  SCQTOPER = &i_cOp1.;
                                  ,SCQTIPER = &i_cOp2.;
                                  &i_ccchkf. ;
                               where;
                                  SCCODICE = this.w_OKEYSA;
                                  and SCCODMAG = this.w_OCOMAG;
                                  and SCCODCAN = this.w_COMMAPPO;

                              i_Rows = _tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              i_Error='Errore in scrittura SALDICOM'
                              return
                            endif
                          endif
                        endif
                      endif
                    endif
                    SELECT (NC)
                    ENDSCAN
                    SELECT (NC)
                    if this.w_RECO>0 AND this.w_RECO<=RECCOUNT()
                      GOTO this.w_RECO
                    else
                      GO TOP
                    endif
                    * --- set deleted ripristinata al valore originario al tern�mine del Batch
                    SET DELETED &OLDSET
                    WAIT CLEAR
                  endif
                  USE IN SELECT("OLDMAT")
                endif
              endif
              if this.w_OK=.F.
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=this.TmpC
              endif
            else
              if this.w_CICLI
                if EMPTY(this.oParentObject.w_DPRIFCAR) AND EMPTY(this.oParentObject.w_DPRIFSCA) and EMPTY(this.oParentObject.w_DPRIFSER) AND this.oParentObject.w_DPQTAPRO=0 AND this.oParentObject.w_DPQTASCA=0 and nvl(this.oParentObject.w_DPFLEVAS,"")="S" 
                  this.w_ODTFIN = cp_CharToDate("  -  -  ")
                  * --- ODL chiuso a zero, lo riapro
                  * --- Select from ODL_MAST
                  i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select ODL_MAST.OLTQTOD1-ODL_MAST.OLTQTOE1 as QTASAL,OLTPERAS,OLTKEYSA,OLTCOMAG,OLTCOMME  from "+i_cTable+" ODL_MAST ";
                        +" where OLCODODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+"";
                         ,"_Curs_ODL_MAST")
                  else
                    select ODL_MAST.OLTQTOD1-ODL_MAST.OLTQTOE1 as QTASAL,OLTPERAS,OLTKEYSA,OLTCOMAG,OLTCOMME from (i_cTable);
                     where OLCODODL=this.oParentObject.w_DPCODODL;
                      into cursor _Curs_ODL_MAST
                  endif
                  if used('_Curs_ODL_MAST')
                    select _Curs_ODL_MAST
                    locate for 1=1
                    do while not(eof())
                    this.w_SALQTA = QTASAL
                    this.w_OKEYSA = _Curs_ODL_MAST.OLTKEYSA
                    this.w_PERASS = _Curs_ODL_MAST.OLTPERAS
                    this.w_OCOMAG = _Curs_ODL_MAST.OLTCOMAG
                    this.w_OLTCOMME = _Curs_ODL_MAST.OLTCOMME
                    if this.w_SALQTA>0
                      * --- Write into ODL_MAST
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
                        +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_SALQTA),'ODL_MAST','OLTQTSAL');
                        +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC("L"),'ODL_MAST','OLTSTATO');
                        +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
                            +i_ccchkf ;
                        +" where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                               )
                      else
                        update (i_cTable) set;
                            OLTFLEVA = " ";
                            ,OLTQTSAL = this.w_SALQTA;
                            ,OLTSTATO = "L";
                            ,OLTDTFIN = this.w_ODTFIN;
                            &i_ccchkf. ;
                         where;
                            OLCODODL = this.oParentObject.w_DPCODODL;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura ODL_MAST'
                        return
                      endif
                      * --- Aggiorna saldi MPS
                      * --- Write into MPS_TFOR
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_SALQTA);
                        +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.w_SALQTA);
                            +i_ccchkf ;
                        +" where ";
                            +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                            +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                               )
                      else
                        update (i_cTable) set;
                            FMMPSLAN = FMMPSLAN + this.w_SALQTA;
                            ,FMMPSTOT = FMMPSTOT + this.w_SALQTA;
                            &i_ccchkf. ;
                         where;
                            FMCODART = this.w_OKEYSA;
                            and FMPERASS = this.w_PERASS;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                      * --- Storna Ordinato dai Saldi
                      * --- Write into SALDIART
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDIART_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_SALQTA);
                            +i_ccchkf ;
                        +" where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                               )
                      else
                        update (i_cTable) set;
                            SLQTOPER = SLQTOPER + this.w_SALQTA;
                            &i_ccchkf. ;
                         where;
                            SLCODICE = this.w_OKEYSA;
                            and SLCODMAG = this.w_OCOMAG;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura SALDIART (3)'
                        return
                      endif
                      * --- Saldi commessa
                      if this.w_SALCOM="S"
                        if empty(nvl(this.w_OLTCOMME,""))
                          this.w_COMMAPPO = this.w_COMMDEFA
                        else
                          this.w_COMMAPPO = this.w_OLTCOMME
                        endif
                        * --- Write into SALDICOM
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDICOM_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_SALQTA);
                              +i_ccchkf ;
                          +" where ";
                              +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                              +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                 )
                        else
                          update (i_cTable) set;
                              SCQTOPER = SCQTOPER + this.w_SALQTA;
                              &i_ccchkf. ;
                           where;
                              SCCODICE = this.w_OKEYSA;
                              and SCCODMAG = this.w_OCOMAG;
                              and SCCODCAN = this.w_COMMAPPO;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore in scrittura SALDICOM'
                          return
                        endif
                      endif
                    endif
                      select _Curs_ODL_MAST
                      continue
                    enddo
                    use
                  endif
                  if this.oParentObject.w_DPULTFAS="S" and !Empty(this.oParentObject.w_DPODLFAS)
                    * --- Riapro anche il padre (per gli ODL di fase)
                    * --- Select from ODL_MAST
                    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select ODL_MAST.OLTQTOD1-ODL_MAST.OLTQTOE1 as QTASAL,OLTPERAS,OLTKEYSA,OLTCOMAG,OLTCOMME  from "+i_cTable+" ODL_MAST ";
                          +" where OLCODODL="+cp_ToStrODBC(this.oParentObject.w_DPODLFAS)+"";
                           ,"_Curs_ODL_MAST")
                    else
                      select ODL_MAST.OLTQTOD1-ODL_MAST.OLTQTOE1 as QTASAL,OLTPERAS,OLTKEYSA,OLTCOMAG,OLTCOMME from (i_cTable);
                       where OLCODODL=this.oParentObject.w_DPODLFAS;
                        into cursor _Curs_ODL_MAST
                    endif
                    if used('_Curs_ODL_MAST')
                      select _Curs_ODL_MAST
                      locate for 1=1
                      do while not(eof())
                      this.w_SALQTA = QTASAL
                      this.w_OKEYSA = _Curs_ODL_MAST.OLTKEYSA
                      this.w_PERASS = _Curs_ODL_MAST.OLTPERAS
                      this.w_OCOMAG = _Curs_ODL_MAST.OLTCOMAG
                      this.w_FASCOMME = _Curs_ODL_MAST.OLTCOMME
                      if this.w_SALQTA>0
                        * --- Write into ODL_MAST
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
                          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_SALQTA),'ODL_MAST','OLTQTSAL');
                          +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC("L"),'ODL_MAST','OLTSTATO');
                          +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
                              +i_ccchkf ;
                          +" where ";
                              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                                 )
                        else
                          update (i_cTable) set;
                              OLTFLEVA = " ";
                              ,OLTQTSAL = this.w_SALQTA;
                              ,OLTSTATO = "L";
                              ,OLTDTFIN = this.w_ODTFIN;
                              &i_ccchkf. ;
                           where;
                              OLCODODL = this.oParentObject.w_DPODLFAS;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore in scrittura ODL_MAST'
                          return
                        endif
                        * --- Aggiorna saldi MPS
                        * --- Write into MPS_TFOR
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_SALQTA);
                          +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.w_SALQTA);
                              +i_ccchkf ;
                          +" where ";
                              +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                                 )
                        else
                          update (i_cTable) set;
                              FMMPSLAN = FMMPSLAN + this.w_SALQTA;
                              ,FMMPSTOT = FMMPSTOT + this.w_SALQTA;
                              &i_ccchkf. ;
                           where;
                              FMCODART = this.w_OKEYSA;
                              and FMPERASS = this.w_PERASS;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error=MSG_WRITE_ERROR
                          return
                        endif
                        * --- Storna Ordinato dai Saldi
                        * --- Write into SALDIART
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDIART_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_SALQTA);
                              +i_ccchkf ;
                          +" where ";
                              +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                              +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                                 )
                        else
                          update (i_cTable) set;
                              SLQTOPER = SLQTOPER + this.w_SALQTA;
                              &i_ccchkf. ;
                           where;
                              SLCODICE = this.w_OKEYSA;
                              and SLCODMAG = this.w_OCOMAG;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore in scrittura SALDIART (3)'
                          return
                        endif
                        * --- Saldi commessa
                        if this.w_SALCOM="S"
                          if empty(nvl(this.w_FASCOMME,""))
                            this.w_COMMAPPO = this.w_COMMDEFA
                          else
                            this.w_COMMAPPO = this.w_FASCOMME
                          endif
                          * --- Write into SALDICOM
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.SALDICOM_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_SALQTA);
                                +i_ccchkf ;
                            +" where ";
                                +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                                +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                   )
                          else
                            update (i_cTable) set;
                                SCQTOPER = SCQTOPER + this.w_SALQTA;
                                &i_ccchkf. ;
                             where;
                                SCCODICE = this.w_OKEYSA;
                                and SCCODMAG = this.w_OCOMAG;
                                and SCCODCAN = this.w_COMMAPPO;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error='Errore in scrittura SALDICOM'
                            return
                          endif
                        endif
                      endif
                        select _Curs_ODL_MAST
                        continue
                      enddo
                      use
                    endif
                  endif
                endif
                if EMPTY(this.oParentObject.w_DPRIFCAR) AND EMPTY(this.oParentObject.w_DPRIFSCA) and EMPTY(this.oParentObject.w_DPRIFSER) AND this.oParentObject.w_DPQTAPRO=0 AND this.oParentObject.w_DPQTASCA=0 and nvl(this.oParentObject.w_DPFLEVAS,"")="S" 
                  * --- ODL chiuso a zero, lo riapro
                  * --- Select from ODL_DETT
                  i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
                        +" where OLCODODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+"";
                         ,"_Curs_ODL_DETT")
                  else
                    select * from (i_cTable);
                     where OLCODODL=this.oParentObject.w_DPCODODL;
                      into cursor _Curs_ODL_DETT
                  endif
                  if used('_Curs_ODL_DETT')
                    select _Curs_ODL_DETT
                    locate for 1=1
                    do while not(eof())
                    this.w_QTASAL = _Curs_ODL_DETT.OLQTAUM1-_Curs_ODL_DETT.OLQTAEV1
                    if this.w_QTASAL>0
                      this.w_MAGCOD = _Curs_ODL_DETT.OLCODMAG
                      this.w_SALKEY = _Curs_ODL_DETT.OLKEYSAL
                      * --- Aggiorna Qta Evasa sull ODL
                      * --- Write into ODL_DETT
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLEVAS');
                        +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_QTASAL),'ODL_DETT','OLQTASAL');
                            +i_ccchkf ;
                        +" where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                               )
                      else
                        update (i_cTable) set;
                            OLFLEVAS = " ";
                            ,OLQTASAL = this.w_QTASAL;
                            &i_ccchkf. ;
                         where;
                            OLCODODL = this.oParentObject.w_DPCODODL;
                            and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura ODL_DETT'
                        return
                      endif
                      * --- Se Nettificabile, Storna Impegnato dai Saldi
                      this.w_DISMAG = " "
                      * --- Read from MAGAZZIN
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "MGDISMAG"+;
                          " from "+i_cTable+" MAGAZZIN where ";
                              +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGCOD);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          MGDISMAG;
                          from (i_cTable) where;
                              MGCODMAG = this.w_MAGCOD;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_DISMAG="S"
                        * --- Inverto i flags
                        this.w_FLORDD = iif(_Curs_ODL_DETT.OLFLORDI="+","-",iif(_Curs_ODL_DETT.OLFLORDI="-","+"," "))
                        this.w_FLIMPD = iif(_Curs_ODL_DETT.OLFLIMPE="+","-",iif(_Curs_ODL_DETT.OLFLIMPE="-","+"," "))
                        * --- Write into SALDIART
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDIART_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                          i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SLQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                          i_cOp2=cp_SetTrsOp(this.w_FLIMPD,'SLQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                              +i_ccchkf ;
                          +" where ";
                              +"SLCODMAG = "+cp_ToStrODBC(this.w_MAGCOD);
                              +" and SLCODICE = "+cp_ToStrODBC(this.w_SALKEY);
                                 )
                        else
                          update (i_cTable) set;
                              SLQTOPER = &i_cOp1.;
                              ,SLQTIPER = &i_cOp2.;
                              &i_ccchkf. ;
                           where;
                              SLCODMAG = this.w_MAGCOD;
                              and SLCODICE = this.w_SALKEY;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error=MSG_WRITE_ERROR
                          return
                        endif
                        * --- Saldi commessa
                        * --- Read from ART_ICOL
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "ARSALCOM"+;
                            " from "+i_cTable+" ART_ICOL where ";
                                +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            ARSALCOM;
                            from (i_cTable) where;
                                ARCODART = _Curs_ODL_DETT.OLKEYSAL;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        if this.w_SALCOM="S"
                          if empty(nvl(this.w_OLTCOMME,""))
                            this.w_COMMAPPO = this.w_COMMDEFA
                          else
                            this.w_COMMAPPO = this.w_OLTCOMME
                          endif
                          * --- Write into SALDICOM
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.SALDICOM_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                            i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SCQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                            i_cOp2=cp_SetTrsOp(this.w_FLIMPD,'SCQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                                +i_ccchkf ;
                            +" where ";
                                +"SCCODICE = "+cp_ToStrODBC(this.w_SALKEY);
                                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGCOD);
                                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                   )
                          else
                            update (i_cTable) set;
                                SCQTOPER = &i_cOp1.;
                                ,SCQTIPER = &i_cOp2.;
                                &i_ccchkf. ;
                             where;
                                SCCODICE = this.w_SALKEY;
                                and SCCODMAG = this.w_MAGCOD;
                                and SCCODCAN = this.w_COMMAPPO;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error='Errore in scrittura SALDICOM'
                            return
                          endif
                        endif
                      endif
                    endif
                      select _Curs_ODL_DETT
                      continue
                    enddo
                    use
                  endif
                endif
              endif
            endif
            if this.w_CICLI
              * --- Read from DIC_PROD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIC_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DPQTAPRO,DPQTAPR1,DPQTASCA,DPQTASC1"+;
                  " from "+i_cTable+" DIC_PROD where ";
                      +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DPQTAPRO,DPQTAPR1,DPQTASCA,DPQTASC1;
                  from (i_cTable) where;
                      DPSERIAL = this.oParentObject.w_DPSERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_oQTAMOV = NVL(cp_ToDate(_read_.DPQTAPRO),cp_NullValue(_read_.DPQTAPRO))
                this.w_oQTAUM1 = NVL(cp_ToDate(_read_.DPQTAPR1),cp_NullValue(_read_.DPQTAPR1))
                this.w_oQTASCA = NVL(cp_ToDate(_read_.DPQTASCA),cp_NullValue(_read_.DPQTASCA))
                this.w_oQTASC1 = NVL(cp_ToDate(_read_.DPQTASC1),cp_NullValue(_read_.DPQTASC1))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from ODL_CICL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CLQTAAVA,CLAVAUM1,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1,CLFASEVA"+;
                  " from "+i_cTable+" ODL_CICL where ";
                      +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                      +" and CLROWORD = "+cp_ToStrODBC(this.oParentObject.w_FASE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CLQTAAVA,CLAVAUM1,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1,CLFASEVA;
                  from (i_cTable) where;
                      CLCODODL = this.oParentObject.w_DPODLFAS;
                      and CLROWORD = this.oParentObject.w_FASE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CLQTAAVA = NVL(cp_ToDate(_read_.CLQTAAVA),cp_NullValue(_read_.CLQTAAVA))
                this.w_CLAVAUM1 = NVL(cp_ToDate(_read_.CLAVAUM1),cp_NullValue(_read_.CLAVAUM1))
                this.w_CLQTASCA = NVL(cp_ToDate(_read_.CLQTASCA),cp_NullValue(_read_.CLQTASCA))
                this.w_CLSCAUM1 = NVL(cp_ToDate(_read_.CLSCAUM1),cp_NullValue(_read_.CLSCAUM1))
                this.w_CLQTADIC = NVL(cp_ToDate(_read_.CLQTADIC),cp_NullValue(_read_.CLQTADIC))
                this.w_CLDICUM1 = NVL(cp_ToDate(_read_.CLDICUM1),cp_NullValue(_read_.CLDICUM1))
                this.w_CLFASEVA = NVL(cp_ToDate(_read_.CLFASEVA),cp_NullValue(_read_.CLFASEVA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_CLFASEVA="S"
                * --- Ripristina la quantita dichiarabile
                * --- Write into ODL_CICL
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_CICL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CLQTADIC ="+cp_NullLink(cp_ToStrODBC(this.w_CLQTADIC),'ODL_CICL','CLQTADIC');
                  +",CLDICUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_CLDICUM1),'ODL_CICL','CLDICUM1');
                      +i_ccchkf ;
                  +" where ";
                      +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                      +" and CLROWORD > "+cp_ToStrODBC(this.oParentObject.w_FASE);
                         )
                else
                  update (i_cTable) set;
                      CLQTADIC = this.w_CLQTADIC;
                      ,CLDICUM1 = this.w_CLDICUM1;
                      &i_ccchkf. ;
                   where;
                      CLCODODL = this.oParentObject.w_DPODLFAS;
                      and CLROWORD > this.oParentObject.w_FASE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              * --- Write into ODL_CICL
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CLQTAAVA =CLQTAAVA- "+cp_ToStrODBC(this.w_oQTAMOV);
                +",CLAVAUM1 =CLAVAUM1- "+cp_ToStrODBC(this.w_oQTAUM1);
                +",CLQTASCA =CLQTASCA- "+cp_ToStrODBC(this.w_oQTASCA);
                +",CLSCAUM1 =CLSCAUM1- "+cp_ToStrODBC(this.w_oQTASC1);
                +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASEVA');
                    +i_ccchkf ;
                +" where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                    +" and CLBFRIFE = "+cp_ToStrODBC(this.oParentObject.w_FASE);
                       )
              else
                update (i_cTable) set;
                    CLQTAAVA = CLQTAAVA - this.w_oQTAMOV;
                    ,CLAVAUM1 = CLAVAUM1 - this.w_oQTAUM1;
                    ,CLQTASCA = CLQTASCA - this.w_oQTASCA;
                    ,CLSCAUM1 = CLSCAUM1 - this.w_oQTASC1;
                    ,CLFASEVA = "N";
                    &i_ccchkf. ;
                 where;
                    CLCODODL = this.oParentObject.w_DPODLFAS;
                    and CLBFRIFE = this.oParentObject.w_FASE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.TmpC
          endif
        endif
      case this.TipOpe $ "CARI-SCAR-SERV-MOUT"
        this.w_MVSERIAL = IIF(this.TipOpe="CARI", this.oParentObject.w_DPRIFCAR, IIF(this.TipOpe="SCAR", this.oParentObject.w_DPRIFSCA, IIF(this.TipOpe="MOUT", this.oParentObject.w_DPRIFMOU, this.oParentObject.w_DPRIFSER)))
        if NOT EMPTY(this.w_MVSERIAL)
          gsar_bzm(this,this.w_MVSERIAL,-20)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.TipOpe = "SETMATR"
        * --- Abilita il bottone per lo scarico delle matricole (componenti)
        this.oParentObject.w_DPCOMMAT = "N"
        * --- Select from GSCO_BTA3
        do vq_exec with 'GSCO_BTA3',this,'_Curs_GSCO_BTA3','',.f.,.t.
        if used('_Curs_GSCO_BTA3')
          select _Curs_GSCO_BTA3
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_DPCOMMAT = "S"
            select _Curs_GSCO_BTA3
            continue
          enddo
          use
        endif
      case this.TipOpe = "UPDMATR"
        * --- Aggiorna le quantit� delle matricole
        this.GSCO_MCO = this.GSCO_ADP.GSCO_MCO
        if g_MATR="S" and this.oParentObject.w_DPCOMMAT="S" and not this.oParentObject.w_EDCOMMAT
          * --- Cursore dei Componenti da scaricare
          nc = this.GSCO_MCO.cnt.cTrsname
          if used(nc)
            if RecCount(nc)=0
              * --- Carica il dettaglio matricole componenti da scaricare
              this.GSCO_MCO.LinkPCClick()     
              this.GSCO_MCO.ecpquit()     
            endif
            if RecCount(nc)>0
              With this.GSCO_ADP
              CALQTA(.w_DPQTAPRO,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, "N", .w_MODUM2, "", .w_UNMIS3, .w_OPERA3, .w_MOLTI3) 
 CALQTA(.w_DPQTASCA,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, "N", .w_MODUM2, "", .w_UNMIS3, .w_OPERA3, .w_MOLTI3)
              Endwith
              Select (nc)
              Scan
              replace t_mtqtamov with t_mtcoeimp * ( this.oParentObject.w_DPQTAPR1 + this.oParentObject.w_DPQTASC1 )
              if this.GSCO_ADP.cFunction="Edit"
                replace i_srv with "U"
              endif
              EndScan
              goto this.GSCO_MCO.cnt.nCurrentRow
              this.GSCO_MCO.cnt.WorkFromTrs()     
              this.GSCO_MCO.cnt.bUpdated = True
            endif
          endif
        endif
      case this.TipOpe = "F3PRESSED"
        * --- Controllo che non ci siano dichiarazioni che evadono anche solo un materiale
        vq_exec ("GSCO5BDP", this, "TEST")
        if reccount("TEST")>0
          Select TEST 
 GO TOP 
 SCAN
          this.w_Msg = alltrim(this.w_Msg)+iif(EMPTY(this.w_Msg),"",",") +chr(13)+"Dichiarazione "+DPSERIAL
          ENDSCAN
          ah_ErrorMsg("Esiste almeno una dichiarazione che evade i materiali, impossibile modificare: %0%1","","",this.w_Msg)
          this.GSCO_ADP.ecpQuery()     
        else
          * --- Blocco l'ODL perch� lo sto modificando (valorizzo il flag OLTSOSPE) - Modifica
          *     Operazione eseguita per evitare che l'ODL venga processato in contemporanea da pi� utenti che creerebbero una situazione inconsistente
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTSOSPE ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTSOSPE');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                   )
          else
            update (i_cTable) set;
                OLTSOSPE = "S";
                &i_ccchkf. ;
             where;
                OLCODODL = this.oParentObject.w_DPCODODL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if g_MATR="S" and this.oParentObject.w_DPCOMMAT="S" and this.oParentObject.w_EDCOMMAT
            * --- Carica il dettaglio matricole componenti da scaricare
            this.GSCO_MCO = this.GSCO_ADP.GSCO_MCO
            this.GSCO_MCO.LinkPCClick()     
            this.GSCO_MCO = this.GSCO_ADP.GSCO_MCO
            this.GSCO_MCO.ecpquit()     
          endif
          this.oParentObject.w_EDCOMMAT = False
          NC = this.PUNMAT.cTrsName
          vq_exec ("..\COLA\EXE\QUERY\GSCO1BDP", this, "MATPRO")
          if USED("MATPRO")
            SELECT MATPRO
            GO TOP
            UPDATE (NC) SET T_QTARES=NVL(MATPRO.QTARES,0) FROM MATPRO WHERE &NC..T_MPSERODL=MATPRO.MPSERODL AND &NC..T_MPROWODL=MATPRO.MPROWODL
            * --- Rinfrescamenti vari
            SELECT (NC) 
 GO TOP 
 With this.PUNMAT 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
            USE IN SELECT("MATPRO")
          endif
        endif
      case this.TipOpe = "CHKUPDATED"
        * --- Controlli finali alla fine della transazione
        if this.oParentObject.w_DPQTAPR1+this.oParentObject.w_DPQTASC1>0
          * --- Controlla le matricole
          this.w_MATRKO = False
          if this.oParentObject.w_GESMAT="S"
            * --- Controlla le Matricole di carico
            this.w_MATFOUND = False
            this.w_CntMat = 0
            * --- Select from GSCO_BTA4
            do vq_exec with 'GSCO_BTA4',this,'_Curs_GSCO_BTA4','',.f.,.t.
            if used('_Curs_GSCO_BTA4')
              select _Curs_GSCO_BTA4
              locate for 1=1
              do while not(eof())
              this.w_CntMat = this.w_CntMat+MTCODMAT
              this.w_MATFOUND = True
              do case
                case _Curs_GSCO_BTA4.MTMAGCAR=this.oParentObject.w_DPCODMAG
                  if this.oParentObject.w_DPQTAPR1 <> _Curs_GSCO_BTA4.MTCODMAT
                    this.w_MATRKO = True
                    this.w_Msg = ah_Msgformat("Quantit� dichiarata su magazzino %1 = %2%0Matricole dichiarate: %3",alltrim(_Curs_GSCO_BTA4.MTMAGCAR), alltrim(trans(this.oParentObject.w_DPQTAPR1,v_pv(32))), alltrim(str(_Curs_GSCO_BTA4.MTCODMAT)) ) 
                  endif
                case _Curs_GSCO_BTA4.MTMAGCAR=this.oParentObject.w_MGSCAR
                  if this.oParentObject.w_DPQTASC1 <> _Curs_GSCO_BTA4.MTCODMAT
                    this.w_MATRKO = True
                    this.w_Msg = Ah_Msgformat("Quantit� dichiarata su magazzino %1 = %2%0Matricole dichiarate: %3",alltrim(_Curs_GSCO_BTA4.MTMAGCAR), alltrim(trans(this.oParentObject.w_DPQTASC1,v_pv(32))), alltrim(str(_Curs_GSCO_BTA4.MTCODMAT)) )
                  endif
              endcase
                select _Curs_GSCO_BTA4
                continue
              enddo
              use
            endif
            if this.oParentObject.w_DPQTAPR1+this.oParentObject.w_DPQTASC1-this.w_CntMat>0
              this.w_MATFOUND = False
            endif
            if not this.w_MATFOUND
              this.w_MATRKO = True
              this.w_Msg = ah_Msgformat("Dichiarare le matricole da caricare")
            endif
          endif
          if not this.w_MATRKO and this.oParentObject.w_DPCOMMAT="S"
            * --- Controlla le matricole da scaricare
            * --- Select from GSCO_BTA5
            do vq_exec with 'GSCO_BTA5',this,'_Curs_GSCO_BTA5','',.f.,.t.
            if used('_Curs_GSCO_BTA5')
              select _Curs_GSCO_BTA5
              locate for 1=1
              do while not(eof())
              this.w_MATFOUND = True
              this.w_MATRKO = True
              this.w_Msg = ah_Msgformat("Per il componente '%1' sono state dichiarate %2 matricole anzich� %3", alltrim(_Curs_GSCO_BTA5.MPCODICE), alltrim(str(_Curs_GSCO_BTA5.MTCODMAT)), alltrim(trans(_Curs_GSCO_BTA5.DPQTASC1,v_pv(32))) )
                select _Curs_GSCO_BTA5
                continue
              enddo
              use
            endif
          endif
          if this.w_MATRKO
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_Msg
          endif
        endif
      case this.TipOpe="CTRLMAT"
        if this.oParentObject.w_DPFLEVAS="S"
          * --- Devo aggiornare la qta saldo e sbiancare il flag evaso sui materiali dell'ODL 
          *     che non sono stati scaricati dalla dichiarazione
          this.w_DELETING = "S"
          this.w_OLCODODL = this.oParentObject.w_DPCODODL
          this.w_SDIC = this.oParentObject.w_DPSERIAL
          * --- Select from gsco2bgd
          do vq_exec with 'gsco2bgd',this,'_Curs_gsco2bgd','',.f.,.t.
          if used('_Curs_gsco2bgd')
            select _Curs_gsco2bgd
            locate for 1=1
            do while not(eof())
            this.w_OLCODODL = _Curs_gsco2bgd.OLCODODL
            this.w_CPROWNUM = _Curs_gsco2bgd.CPROWNUM
            this.w_FLIMPE = iif(_Curs_gsco2bgd.OLFLIMPE="+","-",iif(_Curs_gsco2bgd.OLFLIMPE="-","+"," "))
            this.w_FLORDI = iif(_Curs_gsco2bgd.OLFLORDI="+","-",iif(_Curs_gsco2bgd.OLFLORDI="-","+"," "))
            this.w_OQTOR1 = _Curs_gsco2bgd.OLQTAUM1
            this.w_OQTEV1 = _Curs_gsco2bgd.OLQTAEV1
            this.w_CODMAG = _Curs_gsco2bgd.OLCODMAG
            this.w_KEYSAL = _Curs_gsco2bgd.OLKEYSAL
            this.w_OLTCOMME = _Curs_gsco2bgd.OLTCOMME
            * --- Try
            local bErr_04F62C40
            bErr_04F62C40=bTrsErr
            this.Try_04F62C40()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04F62C40
            * --- End
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OQTOR1',this.w_OQTOR1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OQTOR1',this.w_OQTOR1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_KEYSAL;
                  and SLCODMAG = this.w_CODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Saldi commessa
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_KEYSAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_KEYSAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_OLTCOMME,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_OLTCOMME
              endif
              * --- Try
              local bErr_04F666C0
              bErr_04F666C0=bTrsErr
              this.Try_04F666C0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04F666C0
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_OQTOR1',this.w_OQTOR1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OQTOR1',this.w_OQTOR1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_KEYSAL;
                    and SCCODMAG = this.w_CODMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM'
                return
              endif
            endif
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OQTOR1-this.w_OQTEV1),'ODL_DETT','OLQTASAL');
              +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLEVAS');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  OLQTASAL = this.w_OQTOR1-this.w_OQTEV1;
                  ,OLFLEVAS = " ";
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_OLCODODL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
              select _Curs_gsco2bgd
              continue
            enddo
            use
          endif
        endif
        if this.w_CICLI
          * --- Select from RIS_DICH
          i_nConn=i_TableProp[this.RIS_DICH_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_DICH_idx,2],.t.,this.RIS_DICH_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select RFRIFFAS,RFRIFODL,RFTCONSS  from "+i_cTable+" RIS_DICH ";
                +" where RFSERIAL="+cp_ToStrODBC(this.oParentObject.w_DPSERIAL)+"";
                 ,"_Curs_RIS_DICH")
          else
            select RFRIFFAS,RFRIFODL,RFTCONSS from (i_cTable);
             where RFSERIAL=this.oParentObject.w_DPSERIAL;
              into cursor _Curs_RIS_DICH
          endif
          if used('_Curs_RIS_DICH')
            select _Curs_RIS_DICH
            locate for 1=1
            do while not(eof())
            * --- Write into ODL_RISF
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_RISF_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_RISF_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RFTCONSS =RFTCONSS- "+cp_ToStrODBC(_Curs_RIS_DICH.RFTCONSS);
              +",RFTCORIS =RFTCORIS- "+cp_ToStrODBC(_Curs_RIS_DICH.RFTCONSS);
                  +i_ccchkf ;
              +" where ";
                  +"RFCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                  +" and RFRIFFAS = "+cp_ToStrODBC(_Curs_RIS_DICH.RFRIFFAS);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_RIS_DICH.RFRIFODL);
                     )
            else
              update (i_cTable) set;
                  RFTCONSS = RFTCONSS - _Curs_RIS_DICH.RFTCONSS;
                  ,RFTCORIS = RFTCORIS - _Curs_RIS_DICH.RFTCONSS;
                  &i_ccchkf. ;
               where;
                  RFCODODL = this.oParentObject.w_DPCODODL;
                  and RFRIFFAS = _Curs_RIS_DICH.RFRIFFAS;
                  and CPROWNUM = _Curs_RIS_DICH.RFRIFODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Write into ODL_RISO
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_RISO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_RISO_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RLTCONSS =RLTCONSS- "+cp_ToStrODBC(_Curs_RIS_DICH.RFTCONSS);
              +",RLTCORIS =RLTCORIS- "+cp_ToStrODBC(_Curs_RIS_DICH.RFTCONSS);
                  +i_ccchkf ;
              +" where ";
                  +"RLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                  +" and RLROWNUM = "+cp_ToStrODBC(_Curs_RIS_DICH.RFRIFFAS);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_RIS_DICH.RFRIFODL);
                     )
            else
              update (i_cTable) set;
                  RLTCONSS = RLTCONSS - _Curs_RIS_DICH.RFTCONSS;
                  ,RLTCORIS = RLTCORIS - _Curs_RIS_DICH.RFTCONSS;
                  &i_ccchkf. ;
               where;
                  RLCODODL = this.oParentObject.w_DPODLFAS;
                  and RLROWNUM = _Curs_RIS_DICH.RFRIFFAS;
                  and CPROWNUM = _Curs_RIS_DICH.RFRIFODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
              select _Curs_RIS_DICH
              continue
            enddo
            use
          endif
        endif
      case this.TipOpe="USCITA"
        * --- Sblocco l'ODL (sbianco il flag OLTSOSPE)
        *     Uscita senza salvataggio
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSOSPE ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTSOSPE');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                 )
        else
          update (i_cTable) set;
              OLTSOSPE = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.oParentObject.w_DPCODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc
  proc Try_04E99248()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TFOR
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TFOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FMCODICE"+",FMPERASS"+",FMCODART"+",FMCODVAR"+",FMKEYSAL"+",FMFAMPRO"+",FMUNIMIS"+",FMMPSCON"+",FMMPSDAP"+",FMMPSPIA"+",FMMPSLAN"+",FMMPSSUG"+",FMMPSTOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERASS),'MPS_TFOR','FMPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMCODART');
      +","+cp_NullLink(cp_ToStrODBC("                    "),'MPS_TFOR','FMCODVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMPRO),'MPS_TFOR','FMFAMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'MPS_TFOR','FMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSDAP');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSPIA');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSLAN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSSUG');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSTOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FMCODICE',this.w_OKEYSA,'FMPERASS',this.w_PERASS,'FMCODART',this.w_OKEYSA,'FMCODVAR',"                    ",'FMKEYSAL',this.w_OKEYSA,'FMFAMPRO',this.w_FAMPRO,'FMUNIMIS',this.w_UNIMIS,'FMMPSCON',0,'FMMPSDAP',0,'FMMPSPIA',0,'FMMPSLAN',0,'FMMPSSUG',0)
      insert into (i_cTable) (FMCODICE,FMPERASS,FMCODART,FMCODVAR,FMKEYSAL,FMFAMPRO,FMUNIMIS,FMMPSCON,FMMPSDAP,FMMPSPIA,FMMPSLAN,FMMPSSUG,FMMPSTOT &i_ccchkf. );
         values (;
           this.w_OKEYSA;
           ,this.w_PERASS;
           ,this.w_OKEYSA;
           ,"                    ";
           ,this.w_OKEYSA;
           ,this.w_FAMPRO;
           ,this.w_UNIMIS;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04EB79A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TFOR
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TFOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FMCODICE"+",FMPERASS"+",FMCODART"+",FMCODVAR"+",FMKEYSAL"+",FMFAMPRO"+",FMUNIMIS"+",FMMPSCON"+",FMMPSDAP"+",FMMPSPIA"+",FMMPSLAN"+",FMMPSSUG"+",FMMPSTOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERASS),'MPS_TFOR','FMPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMCODART');
      +","+cp_NullLink(cp_ToStrODBC("                    "),'MPS_TFOR','FMCODVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMPRO),'MPS_TFOR','FMFAMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'MPS_TFOR','FMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSDAP');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSPIA');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSLAN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSSUG');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSTOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FMCODICE',this.w_OKEYSA,'FMPERASS',this.w_PERASS,'FMCODART',this.w_OKEYSA,'FMCODVAR',"                    ",'FMKEYSAL',this.w_OKEYSA,'FMFAMPRO',this.w_FAMPRO,'FMUNIMIS',this.w_UNIMIS,'FMMPSCON',0,'FMMPSDAP',0,'FMMPSPIA',0,'FMMPSLAN',0,'FMMPSSUG',0)
      insert into (i_cTable) (FMCODICE,FMPERASS,FMCODART,FMCODVAR,FMKEYSAL,FMFAMPRO,FMUNIMIS,FMMPSCON,FMMPSDAP,FMMPSPIA,FMMPSLAN,FMMPSSUG,FMMPSTOT &i_ccchkf. );
         values (;
           this.w_OKEYSA;
           ,this.w_PERASS;
           ,this.w_OKEYSA;
           ,"                    ";
           ,this.w_OKEYSA;
           ,this.w_FAMPRO;
           ,this.w_UNIMIS;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F62C40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_OLCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_KEYSAL,'SLCODMAG',w_OLCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,w_OLCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDIART (1)'
      return
    endif
    return
  proc Try_04F666C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_OLCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_KEYSAL,'SCCODMAG',w_OLCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_KEYSAL)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,w_OLCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_KEYSAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDICOM'
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Lista Materiali
    if this.w_TATTIV<>"T"
      vq_exec ("..\COLA\EXE\QUERY\GSCO1BDP", this, "MATPRO")
      if this.TipOpe="MATERIALI"
        this.w_QTAUM1 = this.oParentObject.w_OLTQTOD1-this.oParentObject.w_OLTQTOE1
      else
        this.w_QTAUM1 = this.oParentObject.w_DPQTAPRO+this.oParentObject.w_DPQTASCA
        if this.oParentObject.w_DPUNIMIS<>this.oParentObject.w_UNMIS1
          this.w_QTAUM1 = CALQTA(this.w_QTAUM1,this.oParentObject.w_DPUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
        endif
      endif
    endif
    * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
    NC = this.PUNMAT.cTrsName
    SELECT (NC)
    GO TOP
    DELETE ALL
    if this.w_TATTIV<>"T"
      if USED("MATPRO")
        this.w_MPCODMAG = SPACE(5)
        this.w_ROWORD = 0
        SELECT MATPRO
        GO TOP
        SCAN FOR NOT EMPTY(NVL(MPCODICE,""))
        * --- leggo il Magazzino da Scaricare e controllo se � nettificabile
        this.w_MPCODMAG = iif(NVL(ARTIPPRE,"")="N",NVL(MPCODMAG, SPACE(5)),iif(empty(NVL(OLMAGWIP,SPACE(5))),NVL(MPCODMAG, SPACE(5)),NVL(OLMAGWIP,SPACE(5))))
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGDISMAG,MGFLUBIC"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_MPCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGDISMAG,MGFLUBIC;
            from (i_cTable) where;
                MGCODMAG = this.w_MPCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_WIPNETT = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
          this.w_FLUBI = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT MATPRO
        * --- NVL(OLFLEVAS, SPACE(1))<>"S" or nvl(w_WIPNETT,SPACE(1))<>"S"
        if .t.
          * --- E' un fantasma?
          SELECT MATPRO
          if not empty(nvl(OLPROFAN," ")) and ARTIPART="PH"
            this.w_PDSERIAL = MPSERODL
            this.w_PDROWNUM = MPROWODL
            this.w_TIPART = ARTIPART
            * --- Mi memorizzo la radice della catena del fantasma
            this.radice = OLPROFAN
            * --- Select from MAT_PROD
            i_nConn=i_TableProp[this.MAT_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select sum(MPQTAMOV) as QTAMOV  from "+i_cTable+" MAT_PROD ";
                  +" where MPSERODL = "+cp_ToStrODBC(this.w_PDSERIAL)+" AND MPROWODL = "+cp_ToStrODBC(this.w_PDROWNUM)+"";
                   ,"_Curs_MAT_PROD")
            else
              select sum(MPQTAMOV) as QTAMOV from (i_cTable);
               where MPSERODL = this.w_PDSERIAL AND MPROWODL = this.w_PDROWNUM;
                into cursor _Curs_MAT_PROD
            endif
            if used('_Curs_MAT_PROD')
              select _Curs_MAT_PROD
              locate for 1=1
              do while not(eof())
              this.w_QTATOT = nvl(_Curs_MAT_PROD.QTAMOV,0)
                select _Curs_MAT_PROD
                continue
              enddo
              use
            endif
            SELECT MATPRO
            this.riga = recno()
            * --- Se l'articolo in esame � un fantasma (o figlio di un fantasma) deve essere consumato finch� ce n'� disponibile a magazzino, poi si passa 
            *     l'impegno ai suoi figli.
            * --- Verifico se la quantit� disponibile del fantasma � sufficiente a coprire l'impegno residuo
            if (NVL(MPCOEIMP, 0)* iif(phqtares<>0,phqtares,this.w_QTAUM1))>(OLQTAMOV-this.w_QTATOT) and OLFLELAB $ "S-F"
              * --- La quantit� disponibile non � sufficiente, devo considerare anche i/il figlio
              * --- Quantit� che resta da evadere
              this.w_QTARES = max((MPCOEIMP * iif(phqtares<>0,phqtares,this.w_QTAUM1))-(OLQTAMOV-this.w_QTATOT),0)/MPCOEIMP
              replace phqtares with this.w_QTARES
              * --- Setto la corretta quantit� anche per tutti i figli
              update MATPRO set OLFLELAB="F" ,OLQTAMOV= iif(artipart="PH",min(this.w_qtares*MPCOEIMP,OLQTAMOV),this.w_qtares*MPCOEIMP),PHQTARES=this.w_QTARES where OLPROFAN=alltrim(this.radice)+"."
              * --- Mi riposiziono sulla riga corretta
              SELECT MATPRO 
 go this.riga
              if min(MPCOEIMP * this.w_QTAUM1, (OLQTAMOV-this.w_QTATOT))>0
                update MATPRO set OLFLELAB="F" ,OLQTAMOV=min(MPCOEIMP * this.w_QTAUM1,(OLQTAMOV-this.w_QTATOT)) where OLPROFAN==alltrim(this.radice)
              else
                update MATPRO set OLFLELAB="N" ,OLQTAMOV=min(MPCOEIMP * this.w_QTAUM1,(OLQTAMOV-this.w_QTATOT)) where OLPROFAN==alltrim(this.radice)
              endif
            else
              * --- La quantit� disponibile � sufficiente, non devo considerare anche i/il figlio
              update MATPRO set OLFLELAB="N" where OLPROFAN=alltrim(this.radice)+"."
            endif
            SELECT MATPRO 
 go this.riga
          endif
          if OLFLELAB $ "S-F"
            * --- Nuova Riga del Temporaneo
            this.PUNMAT.InitRow()     
            * --- Valorizza Variabili di Riga ...
            SELECT MATPRO
            this.w_ROWORD = this.w_ROWORD + 10
            this.PUNMAT.w_CPROWORD = this.w_ROWORD
            this.PUNMAT.w_MPCODICE = MPCODICE
            this.w_MPUNIMIS = NVL(MPUNIMIS, "   ")
            this.PUNMAT.w_MPUNIMIS = this.w_MPUNIMIS
            this.w_COEIMP = NVL(MPCOEIMP, 0)
            this.PUNMAT.w_MPCOEIMP = this.w_COEIMP
            this.PUNMAT.w_QTARES = NVL(QTARES,0)
            * --- Calcolo quantit�
            if OLFLELAB = "F"
              * --- Fantasmi
              this.w_MPQTAMOV = NVL(OLQTAMOV , 0)
              this.w_MPQTAUM1 = NVL(OLQTAMOV , 0)
            else
              this.w_MPQTAMOV = this.w_COEIMP * this.w_QTAUM1
              this.w_MPQTAUM1 = this.w_COEIMP * this.w_QTAUM1
            endif
            this.PUNMAT.w_MPQTAMOV = this.w_MPQTAMOV
            this.PUNMAT.w_MPQTAUM1 = this.w_MPQTAUM1
            this.PUNMAT.w_MPCODMAG = this.w_MPCODMAG
            this.PUNMAT.w_DESART = NVL(DESART, " ")
            this.PUNMAT.w_MPCODART = NVL(MPCODART, " ")
            this.w_UM1 = NVL(UNMIS1, "   ")
            this.w_UM2 = NVL(UNMIS2, "   ")
            this.w_UM3 = NVL(UNMIS3, "   ")
            this.w_OP = NVL(OPERAT, " ")
            this.w_MOLT = NVL(MOLTIP, 0)
            this.w_OP3 = NVL(OPERA3, " ")
            this.w_MOLT3 = NVL(MOLTI3, 0)
            this.PUNMAT.w_UNMIS1 = this.w_UM1
            this.PUNMAT.w_UNMIS2 = this.w_UM2
            this.PUNMAT.w_UNMIS3 = this.w_UM3
            this.PUNMAT.w_OPERAT = this.w_OP
            this.PUNMAT.w_OPERA3 = this.w_OP3
            this.PUNMAT.w_MOLTIP = this.w_MOLT
            this.PUNMAT.w_MOLTI3 = this.w_MOLT3
            if this.w_MPUNIMIS<>this.w_UM1
              this.w_MPQTAUM1 = CALQTA(this.w_MPQTAMOV, this.w_MPUNIMIS,this.w_UM2,this.w_OP, this.w_MOLT, this.PUNMAT.w_FLUSEP, "N", this.PUNMAT.w_MODUM2, "", this.w_UM3, this.w_OP3, this.w_MOLT3)
              this.PUNMAT.w_MPQTAUM1 = this.w_MPQTAUM1
            endif
            this.PUNMAT.w_MPSERODL = NVL(MPSERODL, SPACE(15))
            this.PUNMAT.w_MPROWODL = NVL(MPROWODL, 0)
            this.PUNMAT.w_MATCOM = NVL(ARGESMAT, " ")
            this.PUNMAT.w_FLLOTT = NVL(ARFLLOTT, " ")
            this.PUNMAT.w_FLUBI = NVL(this.w_FLUBI, " ")
            this.PUNMAT.w_FLCOMM = NVL(ARSALCOM, "N")
            this.PUNMAT.w_MPCODCOM = this.oParentObject.w_DPCODCOM
            this.w_ODLRIG = MATPRO.MPROWODL
            this.w_DIFFMOV = 0
            this.w_QTASAL = 0
            if this.GSCO_ADP.cFunction="Edit"
              * --- Select from ..\cola\exe\query\gsco_bdp1
              do vq_exec with '..\cola\exe\query\gsco_bdp1',this,'_Curs__d__d__cola_exe_query_gsco_bdp1','',.f.,.t.
              if used('_Curs__d__d__cola_exe_query_gsco_bdp1')
                select _Curs__d__d__cola_exe_query_gsco_bdp1
                locate for 1=1
                do while not(eof())
                * --- Totale quantit� richiesta da ODL
                this.w_QTARIC = nvl(QTARIC,0)
                * --- Totale quantit� gi� evasa (tutte le dichiarazioni esclusa la presente)
                this.w_TOTMOV = nvl(QTATOT,0)
                  select _Curs__d__d__cola_exe_query_gsco_bdp1
                  continue
                enddo
                use
              endif
              SELECT MATPRO
              * --- Differenza tra quantit� precedente ed attuale, mi serve per vedere se devo riaprire l'ODL e ricalcolare la quantit� evasa
              this.w_DIFFMOV = (this.GSCO_ADP.w_QTAUM1*(NVL(MPCOEIMP, 0)))-this.PUNMAT.w_MPQTAMOV
              if this.w_DIFFMOV>0
                * --- C'� stata una modifica, ricalcolo i dati
                this.w_QTASAL = min(this.PUNMAT.w_MPQTAMOV,this.w_QTARIC-this.w_TOTMOV)
              else
                * --- Tengo buona la vecchia quantit�
                this.w_QTASAL = this.PUNMAT.w_QTARES
              endif
            else
              this.w_QTASAL = MATPRO.OLQTASAL
            endif
            this.w_SALCNV = CALQTA(this.w_QTASAL, this.w_MPUNIMIS,iif(this.w_MPUNIMIS=this.w_UM2,this.w_UM2,space(3)),IIF(this.w_OP="/","*","/"), this.w_MOLT, this.PUNMAT.w_FLUSEP, "N", this.PUNMAT.w_MODUM2, "", this.w_UM3, IIF(this.w_OP3="/","*","/"), this.w_MOLT3)
            SELECT MATPRO
            * --- Controlli sulla qta evasa rispetto al magazzino se Nettificabile
            if this.w_WIPNETT="S" or MATPRO.OLQTAPR1=0
              this.w_QTAPPO = NVL(MPCOEIMP, 0)
              * --- Magazzino nettificabile o non nettificabile per il quale non ho eseguito alcun trasferimento di materiale.
              if OLFLELAB = "F"
                * --- Fantasmi
                this.PUNMAT.w_MPQTAEVA = iif(NVL(OLFLEVAS, SPACE(1))<>"S" or this.w_QTASAL>0 and this.GSCO_ADP.cFunction="Edit" ,min(OLQTAMOV,this.w_SALCNV),0)
                if this.w_MPUNIMIS<>this.w_UM1
                  this.PUNMAT.w_MPQTAEV1 = CALQTA(iif(NVL(OLFLEVAS, SPACE(1))<>"S" or this.w_QTASAL>0 and this.GSCO_ADP.cFunction="Edit",min(OLQTAMOV,this.w_SALCNV),0), this.w_MPUNIMIS,this.w_UM2,this.w_OP, this.w_MOLT, this.PUNMAT.w_FLUSEP, "N", this.PUNMAT.w_MODUM2, "", this.w_UM3, this.w_OP3, this.w_MOLT3)
                else
                  this.PUNMAT.w_MPQTAEV1 = iif(NVL(OLFLEVAS, SPACE(1))<>"S" or this.w_QTASAL>0 and this.GSCO_ADP.cFunction="Edit",min(OLQTAMOV,this.w_QTASAL),0)
                endif
              else
                this.PUNMAT.w_MPQTAEVA = iif(NVL(OLFLEVAS, SPACE(1))<>"S" or this.w_QTASAL>0 and this.GSCO_ADP.cFunction="Edit",min(NVL(MPCOEIMP, 0)* this.w_QTAUM1,nvl(this.w_SALCNV,0)),0)
                if this.w_MPUNIMIS<>this.w_UM1
                  this.PUNMAT.w_MPQTAEV1 = CALQTA(iif(NVL(OLFLEVAS, SPACE(1))<>"S" or this.w_QTASAL>0 and this.GSCO_ADP.cFunction="Edit",min(NVL(MPCOEIMP, 0)* this.w_QTAUM1,nvl(this.w_SALCNV,0)),0), this.w_MPUNIMIS,this.w_UM2,this.w_OP, this.w_MOLT, this.PUNMAT.w_FLUSEP, "N", this.PUNMAT.w_MODUM2, "", this.w_UM3, this.w_OP3, this.w_MOLT3)
                else
                  this.PUNMAT.w_MPQTAEV1 = iif(NVL(OLFLEVAS, SPACE(1))<>"S" or this.w_QTASAL>0 and this.GSCO_ADP.cFunction="Edit",min(NVL(MPCOEIMP, 0)* this.w_QTAUM1,nvl(this.w_QTASAL,0)),0)
                endif
              endif
            else
              * --- Magazzino non nettificabile per il quale ho effettuato almeno un trasferimento, � necessario 
              *     verificare le quantit� gi� trasferite e scaricate per valutare la quantit� da evadere.
              * --- Select from MAT_PROD
              i_nConn=i_TableProp[this.MAT_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select sum(MPQTAMOV) as QTAMOV  from "+i_cTable+" MAT_PROD ";
                    +" where MPSERODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" and MPROWODL="+cp_ToStrODBC(this.w_ODLRIG)+"";
                     ,"_Curs_MAT_PROD")
              else
                select sum(MPQTAMOV) as QTAMOV from (i_cTable);
                 where MPSERODL=this.oParentObject.w_DPCODODL and MPROWODL=this.w_ODLRIG;
                  into cursor _Curs_MAT_PROD
              endif
              if used('_Curs_MAT_PROD')
                select _Curs_MAT_PROD
                locate for 1=1
                do while not(eof())
                * --- Totale scarichi
                this.w_QTAMOV = nvl(_Curs_MAT_PROD.QTAMOV,0)
                  select _Curs_MAT_PROD
                  continue
                enddo
                use
              endif
              SELECT MATPRO
              if OLFLELAB = "F"
                * --- Fantasmi
                this.w_QTAPPO = min(max(NVL(OLQTAMOV, 0)-(MATPRO.OLQTAMOV-this.w_SALCNV-this.w_QTAMOV),0),this.w_SALCNV)
              else
                this.w_QTAPPO = min(max(NVL(MPCOEIMP, 0)* this.w_QTAUM1-(MATPRO.OLQTAMOV-this.w_SALCNV-this.w_QTAMOV),0),this.w_SALCNV)
              endif
              this.PUNMAT.w_MPQTAEVA = this.w_QTAPPO
              if this.w_MPUNIMIS<>this.w_UM1
                this.PUNMAT.w_MPQTAEV1 = CALQTA(this.w_QTAPPO, this.w_MPUNIMIS,this.w_UM2,this.w_OP, this.w_MOLT, this.PUNMAT.w_FLUSEP, "N", this.PUNMAT.w_MODUM2, "", this.w_UM3, this.w_OP3, this.w_MOLT3)
              else
                this.PUNMAT.w_MPQTAEV1 = this.w_QTAPPO
              endif
            endif
            this.PUNMAT.w_MPFLEVAS = IIF(this.oParentObject.w_DPFLEVAS="S" or (NVL(OLFLEVAS, SPACE(1))="S" and this.PUNMAT.w_MPQTAMOV>this.w_QTARIC-this.w_TOTMOV ), "S", " ")
            * --- Carica il Temporaneo dei Dati e skippa al record successivo
            this.PUNMAT.TrsFromWork()     
          endif
        else
          * --- Record con flag evasione attivo su ODL_DETT con WIP nettificabile.
          *     Questo record non deve essere considerato, proseguo con il successivo.
        endif
        SELECT MATPRO
        ENDSCAN
        * --- Rinfrescamenti vari
        SELECT (NC) 
 GO TOP 
 With this.PUNMAT 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
        USE IN SELECT("MATPRO")
      endif
    else
      this.oParentObject.w_DPUNIMIS = "   "
      this.oParentObject.w_DPQTAPRO = 0
      this.oParentObject.w_DPQTASCA = 0
      this.oParentObject.w_DPQTAPR1 = 0
      this.oParentObject.w_DPQTASC1 = 0
    endif
    * --- Provo a caricare le risorse dall'ODL 
    *     se sull'ODL non sono presenti verifico se esiste un ciclo associato all'ODL
    *     se esiste allora carico la lista delle risorse dal ciclo semplificato
    * --- Azzero temporaneo risorse legate alla dichiarazione
    NC = this.PUNRIS.cTrsName
    SELECT (NC)
    GO TOP
    DELETE ALL
    * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
    this.w_TOTALE = 0
    this.w_ROWORD = 0
    if this.w_CICLI
      if this.w_TATTIV<>"Q"
        * --- Modulo produzione funzioni avanzate attivo, variabile che attiva i cicli di lavorazione attiva e dichiarazione ODL di fase
        * --- ODL di fase
        this.PUNRIS.MarkPos()     
        this.PUNRIS.InitRow()     
        * --- Select from GSCO9BDP
        do vq_exec with 'GSCO9BDP',this,'_Curs_GSCO9BDP','',.f.,.t.
        if used('_Curs_GSCO9BDP')
          select _Curs_GSCO9BDP
          locate for 1=1
          do while not(eof())
          if NOT EMPTY(NVL(_Curs_GSCO9BDP.RFCODICE,""))
            this.PUNRIS.AddRow()     
            this.PUNRIS.w_RFCODODL = _Curs_GSCO9BDP.RFCODODL
            this.PUNRIS.w_RFRIFODL = _Curs_GSCO9BDP.CPROWNUM
            this.PUNRIS.w_CPROWNUM = _Curs_GSCO9BDP.CPROWNUM
            this.PUNRIS.w_CPROWORD = _Curs_GSCO9BDP.CPROWORD
            this.PUNRIS.w_RFCODCLA = NVL(_Curs_GSCO9BDP.RFCODCLA,SPACE(5))
            this.PUNRIS.w_RF__TIPO = NVL(_Curs_GSCO9BDP.RF__TIPO,SPACE(2))
            this.PUNRIS.w_RFCODICE = _Curs_GSCO9BDP.RFCODICE
            this.PUNRIS.w_RFQTARIS = NVL(_Curs_GSCO9BDP.RFQTARIS,1)
            this.PUNRIS.w_RFUMTRIS = NVL(_Curs_GSCO9BDP.RFUMTRIS,SPACE(3))
            if NVL(_Curs_GSCO9BDP.RFTIPTEM,SPACE(1))="L"
              * --- Solo lavorazione moltiplica i tempi per quantit�
              this.PUNRIS.w_RFTPREVS = NVL(_Curs_GSCO9BDP.RFTPRRIU,0)*this.w_QTAUM1
              this.PUNRIS.w_RFTCONSS = NVL(_Curs_GSCO9BDP.RFTPRRIU,0)*this.w_QTAUM1
            else
              this.PUNRIS.w_RFTPREVS = NVL(_Curs_GSCO9BDP.RFTPRRIU,0)
              this.PUNRIS.w_RFTCONSS = NVL(_Curs_GSCO9BDP.RFTPRRIU,0)
            endif
            this.PUNRIS.w_RFTPRRIS = NVL(_Curs_GSCO9BDP.RFTPRRIS,0)
            this.PUNRIS.w_RFTPRRIU = NVL(_Curs_GSCO9BDP.RFTPRRIU,0)
            this.PUNRIS.w_RFTCORIS = NVL(_Curs_GSCO9BDP.RFTCORIS,0)
            this.PUNRIS.w_RFCODPAD = NVL(_Curs_GSCO9BDP.RFCODPAD,SPACE(20))
            this.PUNRIS.w_RFTMPCIC = NVL(_Curs_GSCO9BDP.RFTMPCIC,0)
            this.PUNRIS.w_RFTMPSEC = NVL(_Curs_GSCO9BDP.RFTMPSEC,0)
            this.PUNRIS.w_RFTIPTEM = NVL(_Curs_GSCO9BDP.RFTIPTEM,SPACE(1))
            this.PUNRIS.w_RFRIFFAS = NVL(_Curs_GSCO9BDP.RFRIFFAS,0)
            * --- Read from RIS_ORSE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP"+;
                " from "+i_cTable+" RIS_ORSE where ";
                    +"RLCODICE = "+cp_ToStrODBC(_Curs_GSCO9BDP.RFCODICE);
                    +" and RL__TIPO = "+cp_ToStrODBC(_Curs_GSCO9BDP.RF__TIPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP;
                from (i_cTable) where;
                    RLCODICE = _Curs_GSCO9BDP.RFCODICE;
                    and RL__TIPO = _Curs_GSCO9BDP.RF__TIPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DSCFAS = NVL(cp_ToDate(_read_.RLDESCRI),cp_NullValue(_read_.RLDESCRI))
              this.w_UMTFAS = NVL(cp_ToDate(_read_.RLUMTDEF),cp_NullValue(_read_.RLUMTDEF))
              this.w_QTAFAS = NVL(cp_ToDate(_read_.RLQTARIS),cp_NullValue(_read_.RLQTARIS))
              this.w_INTESFAS = NVL(cp_ToDate(_read_.RLINTEST),cp_NullValue(_read_.RLINTEST))
              this.w_MWIPFAS = NVL(cp_ToDate(_read_.RLMAGWIP),cp_NullValue(_read_.RLMAGWIP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.PUNRIS.w_RFDESCRI = this.w_DSCFAS
            this.PUNRIS.w_UMTDEF = this.w_UMTFAS
            this.PUNRIS.w_QTARIS = this.w_QTAFAS
            this.PUNRIS.w_INTEST = NVL(this.w_INTESFAS,SPACE(1))
            this.PUNRIS.w_RFMAGWIP = NVL(this.w_MWIPFAS,SPACE(5))
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMDURSEC"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(_Curs_GSCO9BDP.RFUMTRIS);
                    +" and UMFLTEMP = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMDURSEC;
                from (i_cTable) where;
                    UMCODICE = _Curs_GSCO9BDP.RFUMTRIS;
                    and UMFLTEMP = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TMPSEC = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.PUNRIS.w_CONSEC = NVL(this.w_TMPSEC,0)
            * --- Read from CLR_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CLR_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CLR_MAST_idx,2],.t.,this.CLR_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CSDESCLA"+;
                " from "+i_cTable+" CLR_MAST where ";
                    +"CSCODCLA = "+cp_ToStrODBC(_Curs_GSCO9BDP.RFCODCLA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CSDESCLA;
                from (i_cTable) where;
                    CSCODCLA = _Curs_GSCO9BDP.RFCODCLA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DSCLFAS = NVL(cp_ToDate(_read_.CSDESCLA),cp_NullValue(_read_.CSDESCLA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.PUNRIS.w_DESCLA = NVL(this.w_DSCLFAS,"")
            do case
              case this.w_TATTIV="E"
                if this.w_QTAUM1=0
                  * --- Al primo giro non � ancora scattata la mcalc quindi il valore di DPQTAPRO potrebbe essere erroneamente a 0
                  if this.TipOpe="MATERIALI"
                    this.w_QTAUM1 = iif(this.oParentObject.w_DPFLPBSC $ "B-E" and this.w_TATTIV $ "Q-E",MAX(0, this.oParentObject.w_OLTQTODL-this.oParentObject.w_OLTQTOEV),0)
                  else
                    this.w_QTAUM1 = this.oParentObject.w_DPQTAPRO+this.oParentObject.w_DPQTASCA
                  endif
                  if this.oParentObject.w_DPUNIMIS<>this.oParentObject.w_UNMIS1
                    this.w_QTAUM1 = CALQTA(this.w_QTAUM1,this.oParentObject.w_DPUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
                  endif
                endif
                * --- Dichiarazione quantit� e tempi riproporziono in base alla quantit� del padre il valore inserito a preventivo nelle risorse dell'ODL
                if NVL(_Curs_GSCO9BDP.RFTIPTEM,SPACE(1))="L"
                  * --- Solo lavorazione moltiplica i tempi per quantit�
                  this.PUNRIS.w_RFTEMRIS = NVL(_Curs_GSCO9BDP.RFTEMRIS,0)*this.w_QTAUM1
                  this.w_TEMSEC = NVL(_Curs_GSCO9BDP.RFTEMSEC,0)*this.w_QTAUM1
                  this.PUNRIS.w_RFTEMSEC = this.w_TEMSEC
                else
                  this.PUNRIS.w_RFTEMRIS = NVL(_Curs_GSCO9BDP.RFTEMRIS,0)
                  this.w_TEMSEC = NVL(_Curs_GSCO9BDP.RFTEMSEC,0)
                  this.PUNRIS.w_RFTEMSEC = this.w_TEMSEC
                endif
              case this.w_TATTIV="T"
                * --- Dichiarazione solo tempi, uso DPQTATEM inserita manualmente dall'utente (ignoro il preventivo perch� sto dichiarando quantit� 0)
                * --- Read from UNIMIS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.UNIMIS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "UMDURSEC"+;
                    " from "+i_cTable+" UNIMIS where ";
                        +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_DPUMTEMP);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    UMDURSEC;
                    from (i_cTable) where;
                        UMCODICE = this.oParentObject.w_DPUMTEMP;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CONVE = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if NVL(this.oParentObject.w_DPUMTEMP,"")=NVL(_Curs_GSCO9BDP.RFUMTRIS,"")
                  this.PUNRIS.w_RFTEMRIS = this.oParentObject.w_DPQTATEM
                  this.w_VALCONV = cp_round(this.oParentObject.w_DPQTATEM*this.w_CONVE,3)
                  this.w_TEMSEC = this.w_VALCONV
                  this.PUNRIS.w_RFTEMSEC = this.w_VALCONV
                else
                  this.w_VALCONV = cp_round(this.oParentObject.w_DPQTATEM*this.w_CONVE/this.w_TMPSEC,3)
                  this.PUNRIS.w_RFTEMRIS = this.w_VALCONV
                  this.w_VALCONV = cp_round(this.oParentObject.w_DPQTATEM*this.w_CONVE,3)
                  this.w_TEMSEC = this.w_VALCONV
                  this.PUNRIS.w_RFTEMSEC = this.w_VALCONV
                endif
            endcase
            this.PUNRIS.w_RFTCONSS = this.w_TEMSEC
            this.PUNRIS.SaveRow()     
          endif
            select _Curs_GSCO9BDP
            continue
          enddo
          use
        endif
        this.PUNRIS.RePos()     
      else
        * --- Azzero quantit� tempi
        this.oParentObject.w_DPQTATEM = 0
        this.oParentObject.w_DPUMTEMP = "   "
      endif
    else
      * --- Select from GSCO7BDP
      do vq_exec with 'GSCO7BDP',this,'_Curs_GSCO7BDP','',.f.,.t.
      if used('_Curs_GSCO7BDP')
        select _Curs_GSCO7BDP
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(_Curs_GSCO7BDP.RPCODRIS,""))
          * --- Nuova Riga del Temporaneo
          this.PUNRIS.InitRow()     
          * --- Valorizza Variabili di Riga ...
          this.PUNRIS.w_CPROWNUM = _Curs_GSCO7BDP.CPROWNUM
          this.PUNRIS.w_RPCODRIS = _Curs_GSCO7BDP.RPCODRIS
          this.PUNRIS.w_DESRIS = NVL(_Curs_GSCO7BDP.DESRIS, " ")
          this.w_RPUNIMIS = NVL(_Curs_GSCO7BDP.RPUNIMIS, SPACE(3))
          this.PUNRIS.w_RPUNIMIS = this.w_RPUNIMIS
          this.w_RPSETUP = NVL(_Curs_GSCO7BDP.SETUP,"N")
          this.PUNRIS.w_RPSETUP = this.w_RPSETUP
          this.w_RPQTAUNI = NVL(_Curs_GSCO7BDP.RPQTAMOV, 0)
          this.w_RPQTAMOV = NVL(_Curs_GSCO7BDP.RPQTAMOV, 0) * IIF(this.w_RPSETUP<> "S", this.w_QTAUM1, 1)
          this.PUNRIS.w_RPQTAUNI = this.w_RPQTAUNI
          this.PUNRIS.w_RPQTAMOV = this.w_RPQTAMOV
          this.w_UM1 = NVL(_Curs_GSCO7BDP.UNMIS1, "   ")
          this.w_UM2 = NVL(_Curs_GSCO7BDP.UNMIS2, "   ")
          this.w_OP = NVL(_Curs_GSCO7BDP.OPERAT, " ")
          this.w_MOLT = NVL(_Curs_GSCO7BDP.MOLTIP, 0)
          this.w_CODVAL = NVL(_Curs_GSCO7BDP.CODVAL, SPACE(3))
          this.w_CAOVAL = GETCAM(this.w_CODVAL, i_DATSYS, 0)
          this.w_PREZZO = NVL(_Curs_GSCO7BDP.PREZZO, 0)
          this.w_PRENAZ = IIF(this.w_CODVAL=g_PERVAL, this.w_PREZZO, VAL2MON(this.w_PREZZO,this.w_CAOVAL,g_CAOVAL,i_DATSYS))
          this.w_PREUM = CALMMLIS(this.w_PRENAZ, this.w_UM1+this.w_UM2+"   "+this.w_RPUNIMIS+this.w_OP+"  P"+ALLTRIM(STR(g_PERPUL)),this.w_MOLT,0, 0)
          this.w_TOTDAR = cp_ROUND(this.w_PREUM*this.w_RPQTAMOV, g_PERPVL)
          this.w_TOTALE = this.w_TOTALE + this.w_TOTDAR
          this.PUNRIS.w_UNMIS1 = this.w_UM1
          this.PUNRIS.w_UNMIS2 = this.w_UM2
          this.PUNRIS.w_OPERAT = this.w_OP
          this.PUNRIS.w_MOLTIP = this.w_MOLT
          * --- ---------
          this.PUNRIS.w_CODVAL = this.w_CODVAL
          this.PUNRIS.w_PREZZO = this.w_PREZZO
          this.PUNRIS.w_CAOVAL = this.w_CAOVAL
          this.PUNRIS.w_PRENAZ = this.w_PRENAZ
          this.PUNRIS.w_PREUM = this.w_PREUM
          this.PUNRIS.w_RPIMTORI = this.w_TOTDAR
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.PUNRIS.TrsFromWork()     
          this.w_RISODL = this.w_RISODL + 1
        endif
          select _Curs_GSCO7BDP
          continue
        enddo
        use
      endif
      * --- Carica Elenco Risorse
      if this.w_RISODL=0 and NOT EMPTY(this.oParentObject.w_OLTCICLO)
        vq_exec ("..\COLA\EXE\QUERY\GSCO2BDP", this, "RISORSE")
        * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
        NC = this.PUNRIS.cTrsName
        SELECT (NC)
        GO TOP
        DELETE ALL
        if USED("RISORSE")
          this.w_TOTALE = 0
          this.w_ROWORD = 0
          SELECT RISORSE
          GO TOP
          SCAN FOR NOT EMPTY(NVL(RPCODRIS,""))
          * --- Nuova Riga del Temporaneo
          this.PUNRIS.InitRow()     
          * --- Valorizza Variabili di Riga ...
          SELECT RISORSE
          this.PUNRIS.w_CPROWNUM = CPROWNUM
          this.PUNRIS.w_RPCODRIS = RPCODRIS
          this.PUNRIS.w_DESRIS = NVL(DESRIS, " ")
          this.w_RPUNIMIS = NVL(RPUNIMIS, SPACE(3))
          this.PUNRIS.w_RPUNIMIS = this.w_RPUNIMIS
          this.w_RPSETUP = NVL(SETUP,"N")
          this.PUNRIS.w_RPSETUP = this.w_RPSETUP
          this.w_RPQTAUNI = NVL(RPQTAMOV, 0)
          this.w_RPQTAMOV = NVL(RPQTAMOV, 0) * IIF(this.w_RPSETUP<> "S", this.w_QTAUM1, 1)
          this.PUNRIS.w_RPQTAUNI = this.w_RPQTAUNI
          this.PUNRIS.w_RPQTAMOV = this.w_RPQTAMOV
          this.w_UM1 = NVL(UNMIS1, "   ")
          this.w_UM2 = NVL(UNMIS2, "   ")
          this.w_OP = NVL(OPERAT, " ")
          this.w_MOLT = NVL(MOLTIP, 0)
          this.w_CODVAL = NVL(CODVAL, SPACE(3))
          this.w_CAOVAL = GETCAM(this.w_CODVAL, i_DATSYS, 0)
          this.w_PREZZO = NVL(PREZZO, 0)
          this.w_PRENAZ = IIF(this.w_CODVAL=g_PERVAL, this.w_PREZZO, VAL2MON(this.w_PREZZO,this.w_CAOVAL,g_CAOVAL,i_DATSYS))
          this.w_PREUM = CALMMLIS(this.w_PRENAZ, this.w_UM1+this.w_UM2+"   "+this.w_RPUNIMIS+this.w_OP+"  P"+ALLTRIM(STR(g_PERPUL)),this.w_MOLT,0, 0)
          this.w_TOTDAR = cp_ROUND(this.w_PREUM*this.w_RPQTAMOV, g_PERPVL)
          this.w_TOTALE = this.w_TOTALE + this.w_TOTDAR
          this.PUNRIS.w_UNMIS1 = this.w_UM1
          this.PUNRIS.w_UNMIS2 = this.w_UM2
          this.PUNRIS.w_OPERAT = this.w_OP
          this.PUNRIS.w_MOLTIP = this.w_MOLT
          * --- ---------
          this.PUNRIS.w_CODVAL = this.w_CODVAL
          this.PUNRIS.w_PREZZO = this.w_PREZZO
          this.PUNRIS.w_CAOVAL = this.w_CAOVAL
          this.PUNRIS.w_PRENAZ = this.w_PRENAZ
          this.PUNRIS.w_PREUM = this.w_PREUM
          this.PUNRIS.w_RPIMTORI = this.w_TOTDAR
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.PUNRIS.TrsFromWork()     
          this.w_RISODL = this.w_RISODL + 1
          SELECT RISORSE
          ENDSCAN
          USE IN SELECT("RISORSE")
        endif
      endif
      if this.w_RISODL > 0
        this.PUNRIS.w_QTAUM1 = this.w_QTAUM1
        * --- Rinfrescamenti vari
        this.PUNRIS.w_TOTALE = this.w_TOTALE
        SELECT (NC) 
 GO TOP 
 With this.PUNRIS 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
      endif
    endif
    * --- Provo a caricare i materiali di output dall'ODL
    if this.w_TATTIV<>"T"
      * --- Azzero temporaneo risorse legate alla dichiarazione
      NC = this.PUNMOU.cTrsName
      SELECT (NC)
      GO TOP
      DELETE ALL
      * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
      * --- Nuova Riga del Temporaneo
      this.PUNMOU.MarkPos()     
      this.PUNMOU.InitRow()     
      * --- Select from GSCO11BDP
      do vq_exec with 'GSCO11BDP',this,'_Curs_GSCO11BDP','',.f.,.t.
      if used('_Curs_GSCO11BDP')
        select _Curs_GSCO11BDP
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(_Curs_GSCO11BDP.MDCODICE,""))
          * --- Valorizza Variabili di Riga ...
          this.PUNMOU.AddRow()     
          this.w_MDCODICE = _Curs_GSCO11BDP.MDCODICE
          this.w_MDCODART = _Curs_GSCO11BDP.MDCODART
          this.w_MDCODMAG = _Curs_GSCO11BDP.MDCODMAG
          this.PUNMOU.w_MDCODICE = this.w_MDCODICE
          this.PUNMOU.w_MDCODART = this.w_MDCODART
          this.PUNMOU.w_MDUNIMIS = _Curs_GSCO11BDP.MDUNIMIS
          this.PUNMOU.w_MDCOEIMP = _Curs_GSCO11BDP.MDQTAMOV
          this.PUNMOU.w_MDCOEIM1 = _Curs_GSCO11BDP.MDQTAUM1
          this.PUNMOU.w_MDQTAMOV = NVL(_Curs_GSCO11BDP.MDQTAMOV*this.w_QTAUM1,0)
          this.PUNMOU.w_MDQTAUM1 = NVL(_Curs_GSCO11BDP.MDQTAUM1*this.w_QTAUM1,0)
          this.PUNMOU.w_MDCODMAG = this.w_MDCODMAG
          this.PUNMOU.w_MDFASRIF = NVL(_Curs_GSCO11BDP.MDFASRIF,0)
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_MDCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP;
              from (i_cTable) where;
                  CACODICE = this.w_MDCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
            this.w_UNIMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.w_OPERAT3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.w_MOLTIP3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGESMAT,ARFLUSEP,ARFLLOTT,ARMAGPRE"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_MDCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGESMAT,ARFLUSEP,ARFLLOTT,ARMAGPRE;
              from (i_cTable) where;
                  ARCODART = this.w_MDCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNIMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_OPERAT1 = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_MOLTIP1 = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            this.w_UNIMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            this.w_MATCOMME = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
            this.w_FLUMSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
            this.w_FLOTTI = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
            this.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGFLUBIC"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.w_MDCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGFLUBIC;
              from (i_cTable) where;
                  MGCODMAG = this.w_MDCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.PUNMOU.w_DESART = this.w_DESART
          this.PUNMOU.w_UNMIS3 = this.w_UNIMIS3
          this.PUNMOU.w_OPERA3 = this.w_OPERAT3
          this.PUNMOU.w_MOLTI3 = this.w_MOLTIP3
          this.PUNMOU.w_UNMIS1 = this.w_UNIMIS1
          this.PUNMOU.w_OPERAT = this.w_OPERAT1
          this.PUNMOU.w_MOLTIP = this.w_MOLTIP1
          this.PUNMOU.w_UNMIS2 = this.w_UNIMIS2
          this.PUNMOU.w_MATCOM = this.w_MATCOMME
          this.PUNMOU.w_FLUSEP = this.w_FLUMSEP
          this.PUNMOU.w_FLLOTT = this.w_FLOTTI
          this.PUNMOU.w_MAGPRE = this.w_MAGPRE
          this.PUNMOU.w_FLUBI = this.w_FLUBIC
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.PUNMOU.SaveRow()     
          this.w_MOUODL = this.w_MOUODL + 1
        endif
          select _Curs_GSCO11BDP
          continue
        enddo
        use
      endif
      this.PUNMOU.RePos()     
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Qta su Materiali e Risorse
    this.w_AGGMAT = .F.
    this.w_QTAUM1 = this.oParentObject.w_DPQTAPRO+this.oParentObject.w_DPQTASCA
    if this.oParentObject.w_DPUNIMIS<>this.oParentObject.w_UNMIS1
      this.w_QTAUM1 = CALQTA(this.w_QTAUM1,this.oParentObject.w_DPUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
    endif
    NC = this.PUNMAT.cTrsName
    SELECT (NC)
    GO TOP
    if NOT (RECCOUNT()=1 AND EMPTY(t_MPCODICE))
      * --- Aggiorna Qta Cursore ...
      SCAN FOR (NOT DELETED()) AND (NOT EMPTY(t_MPCODICE))
      this.w_COEIMP = t_MPCOEIMP
      this.w_MPQTAMOV = this.w_COEIMP * this.w_QTAUM1
      this.w_MPQTAUM1 = this.w_COEIMP * this.w_QTAUM1
      this.w_MPFLEVAS = this.oParentObject.w_DPFLEVAS
      this.w_MPUNIMIS = t_MPUNIMIS
      this.w_UM1 = t_UNMIS1
      this.w_UM2 = t_UNMIS2
      this.w_UM3 = t_UNMIS3
      this.w_OP = t_OPERAT
      this.w_MOLT = t_MOLTIP
      this.w_OP3 = t_OPERA3
      this.w_MOLT3 = t_MOLTI3
      if this.w_MPUNIMIS<>this.w_UM1
        this.w_MPQTAUM1 = CALQTA(this.w_MPQTAMOV, this.w_MPUNIMIS,this.w_UM2,this.w_OP, this.w_MOLT, this.PUNMAT.w_FLUSEP, "N", this.PUNMAT.w_MODUM2, "", this.w_UM3, this.w_OP3, this.w_MOLT3)
      endif
      this.PUNMAT.w_MPQTAMOV = this.w_MPQTAMOV
      this.PUNMAT.w_MPQTAUM1 = this.w_MPQTAUM1
      * --- Controlli sulla qta evasa rispetto al magazzino se Nettificabile
      this.w_MPQTAEVA = this.w_MPQTAMOV
      this.w_MPQTAEV1 = this.w_MPQTAUM1
      this.w_MPCODMAG = t_MPCODMAG
      this.w_ODLRIG = t_MPROWODL
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGDISMAG"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_MPCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGDISMAG;
          from (i_cTable) where;
              MGCODMAG = this.w_MPCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_WIPNETT = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Select from ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
            +" where OLCODODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" and CPROWNUM="+cp_ToStrODBC(this.w_ODLRIG)+"";
             ,"_Curs_ODL_DETT")
      else
        select * from (i_cTable);
         where OLCODODL=this.oParentObject.w_DPCODODL and CPROWNUM=this.w_ODLRIG;
          into cursor _Curs_ODL_DETT
      endif
      if used('_Curs_ODL_DETT')
        select _Curs_ODL_DETT
        locate for 1=1
        do while not(eof())
        this.w_MPFLEVAS = IIF(this.oParentObject.w_DPFLEVAS="S" or NVL(OLFLEVAS, SPACE(1))="S", "S", " ")
        if this.w_WIPNETT="S" or _Curs_ODL_DETT.OLQTAPR1=0
          this.w_QTAPPO = NVL(OLCOEIMP, 0)
          * --- Magazzino nettificabile o non nettificabile per il quale non ho eseguito alcun trasferimento di materiale.
          this.w_MPQTAEVA = NVL(OLCOEIMP, 0)* this.w_QTAUM1
          this.w_MPQTAEV1 = NVL(OLCOEIMP, 0)* this.w_QTAUM1
        else
          * --- Magazzino non nettificabile per il quale ho effettuato almeno un trasferimento, � necessario 
          *     verificare le quantit� gi� trasferite e scaricate per valutare la quantit� da evadere.
          * --- Select from MAT_PROD
          i_nConn=i_TableProp[this.MAT_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select sum(MPQTAMOV) as QTAMOV  from "+i_cTable+" MAT_PROD ";
                +" where MPSERODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+" and MPROWODL="+cp_ToStrODBC(this.w_ODLRIG)+"";
                 ,"_Curs_MAT_PROD")
          else
            select sum(MPQTAMOV) as QTAMOV from (i_cTable);
             where MPSERODL=this.oParentObject.w_DPCODODL and MPROWODL=this.w_ODLRIG;
              into cursor _Curs_MAT_PROD
          endif
          if used('_Curs_MAT_PROD')
            select _Curs_MAT_PROD
            locate for 1=1
            do while not(eof())
            * --- Totale scarichi
            this.w_QTAMOV = nvl(_Curs_MAT_PROD.QTAMOV,0)
              select _Curs_MAT_PROD
              continue
            enddo
            use
          endif
          SELECT _Curs_ODL_DETT
          this.w_QTAPPO = max(NVL(OLCOEIMP, 0)* this.w_QTAUM1-(OLQTAMOV-OLQTASAL-this.w_QTAMOV),0)
          this.w_MPQTAEVA = this.w_QTAPPO
          this.w_MPQTAEV1 = this.w_QTAPPO
        endif
          select _Curs_ODL_DETT
          continue
        enddo
        use
      endif
      this.PUNMAT.w_MPFLEVAS = this.w_MPFLEVAS
      this.PUNMAT.w_MPQTAEVA = this.w_MPQTAEVA
      this.PUNMAT.w_MPQTAEV1 = this.w_MPQTAEVA
      SELECT (NC)
      REPLACE t_MPQTAMOV WITH this.w_MPQTAMOV
      REPLACE t_MPQTAUM1 WITH this.w_MPQTAUM1
      REPLACE t_MPQTAEVA WITH this.w_MPQTAEVA
      REPLACE t_MPQTAEV1 WITH this.w_MPQTAEV1
      REPLACE t_MPFLEVAS WITH IIF(this.w_MPFLEVAS="S", 1, 0)
      if EMPTY(i_SRV)
        REPLACE i_SRV WITH "U"
      endif
      this.w_AGGMAT = .T.
      ENDSCAN
      * --- Rinfrescamenti vari
      SELECT (NC) 
 GO TOP 
 With this.PUNMAT 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
      if this.w_AGGMAT=.T.
        ah_Msg("Aggiornata lista materiali",.T.)
      endif
    endif
    this.w_AGGMAT = .F.
    this.w_QTAUM1 = this.oParentObject.w_DPQTAPRO+this.oParentObject.w_DPQTASCA
    this.PUNRIS.w_QTAUM1 = this.w_QTAUM1
    this.w_TOTALE = 0
    NC = this.PUNRIS.cTrsName
    SELECT (NC)
    GO TOP
    if NOT (RECCOUNT()=1 AND EMPTY(t_RPCODRIS))
      * --- Aggiorna Qta Cursore ...
      SCAN FOR (NOT DELETED()) AND (NOT EMPTY(t_RPCODRIS))
      this.w_RPQTAMOV = IIF(t_RPQTAUNI=0, 1, t_RPQTAUNI) * IIF(t_RPSETUP=0, this.w_QTAUM1, 1)
      this.PUNRIS.w_RPQTAMOV = this.w_RPQTAMOV
      this.w_TOTDAR = cp_ROUND(t_PREUM*this.w_RPQTAMOV, g_PERPVL)
      this.w_TOTALE = this.w_TOTALE + this.w_TOTDAR
      REPLACE t_RPQTAMOV WITH this.w_RPQTAMOV
      REPLACE t_RPIMTORI WITH this.w_TOTDAR
      if EMPTY(i_SRV)
        REPLACE i_SRV WITH "U"
      endif
      this.w_AGGMAT = .T.
      ENDSCAN
      * --- Rinfrescamenti vari
      this.PUNRIS.w_TOTALE = this.w_TOTALE
      SELECT (NC) 
 GO TOP 
 With this.PUNRIS 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
      if this.w_AGGMAT=.T.
        ah_Msg("Aggiornata lista risorse",.T.)
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documenti di Produzione
    * --- Log Errori
    this.w_LNumErr = 0
    this.w_nRecSel = 0
    this.w_nRecEla = 0
    this.w_nRecDoc = 0
    this.w_DOCCAR = SPACE(10)
    this.w_DOCSCA = SPACE(10)
    this.w_DOCSER = SPACE(10)
    this.w_DOCMOU = SPACE(10)
    * --- Crea il File delle Messaggistiche di Errore
    CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
    * --- Calcola i Costi Unitari e Globali su Ultimo Costo Standard Articolo
    if this.w_CICLI
      this.w_NUMINV = this.oParentObject.w_DPNUMINV
      this.w_ESEINV = this.oParentObject.w_DPESERCZ
      if this.oParentObject.w_CRIVAL="US"
        vq_exec("..\COLA\EXE\QUERY\GSCOXQIC.VQR", this,"APPOVALO")
      endif
      if this.oParentObject.w_CRIVAL="CL"
        * --- Leggo il Listino Qta Scaglione e Data di Attivaziione piu' Prossimi
        vq_exec("..\COLA\EXE\QUERY\GSCOLQIC.VQR", this,"APPOVALO")
      endif
      if this.oParentObject.w_CRIVAL="UA"
        * --- Cerco il Costo dell'Articolo nei Saldi Articoli
        vq_exec("..\COLA\EXE\QUERY\GSCOSQIC.VQR", this,"APPOVALO")
      endif
      if this.oParentObject.w_CRIVAL $ "CS-CM-UC-UP"
        * --- Cerco il Costo dell'Articolo nel Cursore calcolato sull'Inventario
        vq_exec("..\COLA\EXE\QUERY\GSCOIQIC.VQR", this,"APPOVALO")
      endif
      vq_exec("..\COLA\EXE\QUERY\GSCOZQIC.VQR", this,"APPORISO")
    else
      vq_exec("..\COLA\EXE\QUERY\GSCOXQIC.VQR", this,"APPOVALO")
    endif
    if this.w_CREADOC
      this.w_OPERAZ = "CA"
      this.w_nRecSel = this.w_nRecSel + 1
      * --- Generazione Documento di Carico
      vq_exec("..\COLA\EXE\QUERY\GSCO_BCP.VQR",this,"GENEORDI")
      ah_Msg("Generazione carico di produzione...",.T.)
      this.w_MVTIPDOC = this.oParentObject.w_CAUCAR
      this.w_MVDATDOC = this.oParentObject.w_DPDATREG
      this.w_MVDATDIV = this.w_MVDATDOC
      this.w_MVDATREG = this.w_MVDATDOC
      this.w_MVDATCIV = this.w_MVDATDOC
      this.w_MVPRP = "NN"
      this.w_MVNUMDOC = 0
      this.w_MVFLPROV = "N"
      this.w_MVNUMREG = 0
      this.w_MVNUMEST = 0
      this.w_MVALFEST = Space(10)
      this.w_MVSERIAL = SPACE(10)
      this.w_MVCODESE = g_CODESE
      this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
        this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
        this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
        this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
        this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
        this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
        this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
        this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
        this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVTIPCON = IIF(this.w_FLINTE $ "CF", this.w_FLINTE, " ")
      this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
      this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      if this.w_OLDCOM
        * --- La quantit� gi� trasferita (se presente) deve essere legata al magazzino di trasferimento
        this.w_QTADAB = 0
        * --- Select from gsdb1bad
        do vq_exec with 'gsdb1bad',this,'_Curs_gsdb1bad','',.f.,.t.
        if used('_Curs_gsdb1bad')
          select _Curs_gsdb1bad
          locate for 1=1
          do while not(eof())
          * --- Read from ODL_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLQTAPR1,OLCODMAG"+;
              " from "+i_cTable+" ODL_DETT where ";
                  +"OLCODODL = "+cp_ToStrODBC(_Curs_gsdb1bad.PERIFODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_gsdb1bad.PERIGORD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLQTAPR1,OLCODMAG;
              from (i_cTable) where;
                  OLCODODL = _Curs_gsdb1bad.PERIFODL;
                  and CPROWNUM = _Curs_gsdb1bad.PERIGORD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_QTAPR1 = NVL(cp_ToDate(_read_.OLQTAPR1),cp_NullValue(_read_.OLQTAPR1))
            this.w_MAGTRA = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Deve essere garantita la continuit� del WIP, cio� il wip di un odl deve sempre essere lo stesso
          this.w_RIFMAG = " Magazz.: " + this.w_MAGTRA
          if this.w_QTAPR1>0
            this.w_QTADAB = this.w_QTADAB+this.w_QTAPR1
          endif
            select _Curs_gsdb1bad
            continue
          enddo
          use
        endif
        Select GENEORDI 
 scan
        * --- Primo giro, quantit� abbinata al WIP
        this.w_PGCODODL = this.oParentObject.w_DPCODODL
        this.w_PECODART = GENEORDI.PDCODART
        this.w_PEQTAUM1 = min(GENEORDI.PDQTAUM1,this.w_QTADAB)
        this.w_PEQTAOLD = this.w_PEQTAUM1
        if this.w_PEQTAUM1>0
          this.w_PECODODL = this.w_RIFMAG
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_PEQTAUM1 = GENEORDI.PDQTAUM1-this.w_PEQTAOLD
        * --- Secondo giro, quantit� abbinata a magazzino originale di riferimento (solo materiale non trasferito)
        if this.w_PEQTAUM1>0
          this.w_PECODODL = " Magazz.: "+GENEORDI.PDCODMAG
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        endscan
      endif
      if this.oParentObject.w_DPFLEVAS<>"S"
        Select GENEORDI 
 scan for PDFLEVAS="S"
        * --- Write into DIC_PROD
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DIC_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_PROD_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DPFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DIC_PROD','DPFLEVAS');
              +i_ccchkf ;
          +" where ";
              +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERIAL);
                 )
        else
          update (i_cTable) set;
              DPFLEVAS = "S";
              &i_ccchkf. ;
           where;
              DPSERIAL = this.oParentObject.w_DPSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        exit
        endscan
      endif
      * --- Lancia Batch di Generazione Documenti di Carico
      do GSCO_BGD with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SERIAL = this.w_DOCCAR
      this.w_CLADOC = "DI"
      this.w_FLVEAC = "A"
      this.w_DATMAT = g_DATMAT
      if not empty(nvl(this.w_SERIAL,""))
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Aggiorna saldi lotti  prima di eliminare i documenti
        if NOT EMPTY (this.w_SERIAL) AND g_MADV="S" AND (g_PERLOT="S" OR g_PERUBI="S")
          GSMD_BRL (this, this.w_SERIAL , "V" , "+" , ,.T.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if this.oParentObject.w_STADOC="S"
        if NOT EMPTY(this.w_DOCCAR)
          * --- Lancia il Report Documenti di Carico
          ah_Msg("Stampa documento di carico",.T.)
          this.w_MVSERIAL = this.w_DOCCAR
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      this.w_OPERAZ = "SC"
      this.w_nRecSel = this.w_nRecSel + 1
      * --- Generazione Documento di Scarico
      vq_exec("..\COLA\EXE\QUERY\GSCO_BSP.VQR",this,"GENEORDI")
      ah_Msg("Generazione scarico materiali per produzione...",.T.)
      this.w_MVTIPDOC = this.oParentObject.w_CAUSCA
      this.w_MVDATDOC = this.oParentObject.w_DPDATREG
      this.w_MVDATDIV = this.w_MVDATDOC
      this.w_MVDATREG = this.w_MVDATDOC
      this.w_MVDATCIV = this.w_MVDATDOC
      this.w_MVPRP = "NN"
      this.w_MVNUMDOC = 0
      this.w_MVFLPROV = "N"
      this.w_MVNUMREG = 0
      this.w_MVNUMEST = 0
      this.w_MVALFEST = Space(10)
      this.w_MVSERIAL = SPACE(10)
      this.w_MVCODESE = g_CODESE
      this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
        this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
        this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
        this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
        this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
        this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
        this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
        this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
        this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVTIPCON = IIF(this.w_FLINTE $ "CF", this.w_FLINTE, " ")
      this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
      this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      if this.w_OLDCOM
        * --- Aggiorna Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- La quantit� gi� trasferita (se presente) deve essere legata al magazzino di trasferimento
        this.w_QTADAB = 0
        Select GENEORDI 
 scan
        * --- Read from ODL_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLQTAPR1,OLCODMAG,OLMAGPRE"+;
            " from "+i_cTable+" ODL_DETT where ";
                +"OLCODODL = "+cp_ToStrODBC(GENEORDI.PDSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(GENEORDI.PDROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLQTAPR1,OLCODMAG,OLMAGPRE;
            from (i_cTable) where;
                OLCODODL = GENEORDI.PDSERIAL;
                and CPROWNUM = GENEORDI.PDROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_QTAPR1 = NVL(cp_ToDate(_read_.OLQTAPR1),cp_NullValue(_read_.OLQTAPR1))
          this.w_MAGTRA = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
          this.w_MAGRIF = NVL(cp_ToDate(_read_.OLMAGPRE),cp_NullValue(_read_.OLMAGPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_GESWIP="S"
          if this.w_QTAPR1>0
            * --- Sottraggo alla quantit� trasferita per l'ODL quella gi� utilizzata
            this.w_QTAPR1 = max(this.w_QTAPR1+GENEORDI.PDQTAUM1,0)
            this.w_QTADAB = this.w_QTADAB+this.w_QTAPR1
          endif
          this.w_PEQTAUM1 = min(GENEORDI.PDQTAUM1,this.w_QTADAB)
        else
          * --- Se non gestisco il magazzino WIP la quantit� da considerare � sempre quella dichiarata
          this.w_PEQTAUM1 = GENEORDI.PDQTAUM1
        endif
        this.w_PEQTAOLD = this.w_PEQTAUM1
        this.w_RIFMAG = " Magazz.: " + this.w_MAGTRA
        this.w_PGSERIAL = GENEORDI.PDSERIAL
        this.w_PGROWNUM = GENEORDI.PDROWNUM
        * --- Primo giro quantit� abbinata al WIP
        * --- Select from PEG_SELI
        i_nConn=i_TableProp[this.PEG_SELI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB  from "+i_cTable+" PEG_SELI ";
              +" where PERIFODL="+cp_ToStrODBC(this.w_PGSERIAL)+" AND PERIGORD="+cp_ToStrODBC(this.w_PGROWNUM)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_RIFMAG)+"";
               ,"_Curs_PEG_SELI")
        else
          select PESERIAL,PEQTAABB from (i_cTable);
           where PERIFODL=this.w_PGSERIAL AND PERIGORD=this.w_PGROWNUM AND PETIPRIF="M" AND PESERODL=this.w_RIFMAG;
            into cursor _Curs_PEG_SELI
        endif
        if used('_Curs_PEG_SELI')
          select _Curs_PEG_SELI
          locate for 1=1
          do while not(eof())
          if this.w_PEQTAUM1<_Curs_PEG_SELI.PEQTAABB
            * --- Write into PEG_SELI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(_Curs_PEG_SELI.PEQTAABB-this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                  +i_ccchkf ;
              +" where ";
                  +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                     )
            else
              update (i_cTable) set;
                  PEQTAABB = _Curs_PEG_SELI.PEQTAABB-this.w_PEQTAUM1;
                  &i_ccchkf. ;
               where;
                  PESERIAL = _Curs_PEG_SELI.PESERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_PEQTAUM1 = 0
          else
            this.w_PEQTAUM1 = this.w_PEQTAUM1-_Curs_PEG_SELI.PEQTAABB
            * --- Delete from PEG_SELI?
            * --- Write into PEG_SELI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PEQTAABB');
                  +i_ccchkf ;
              +" where ";
                  +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                     )
            else
              update (i_cTable) set;
                  PEQTAABB = 0;
                  &i_ccchkf. ;
               where;
                  PESERIAL = _Curs_PEG_SELI.PESERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_PEG_SELI
            continue
          enddo
          use
        endif
        this.w_PEQTAUM1 = GENEORDI.PDQTAUM1-this.w_PEQTAOLD
        this.w_RIFMAG = " Magazz.: " + this.w_MAGRIF
        * --- Secondo giro, quantit� abbinata a magazzino originale di riferimento
        if this.w_PEQTAUM1>0
          * --- Select from PEG_SELI
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB  from "+i_cTable+" PEG_SELI ";
                +" where PERIFODL="+cp_ToStrODBC(this.w_PGSERIAL)+" AND PERIGORD="+cp_ToStrODBC(this.w_PGROWNUM)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_RIFMAG)+"";
                 ,"_Curs_PEG_SELI")
          else
            select PESERIAL,PEQTAABB from (i_cTable);
             where PERIFODL=this.w_PGSERIAL AND PERIGORD=this.w_PGROWNUM AND PETIPRIF="M" AND PESERODL=this.w_RIFMAG;
              into cursor _Curs_PEG_SELI
          endif
          if used('_Curs_PEG_SELI')
            select _Curs_PEG_SELI
            locate for 1=1
            do while not(eof())
            if this.w_PEQTAUM1<_Curs_PEG_SELI.PEQTAABB
              * --- Write into PEG_SELI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(_Curs_PEG_SELI.PEQTAABB-this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +i_ccchkf ;
                +" where ";
                    +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                       )
              else
                update (i_cTable) set;
                    PEQTAABB = _Curs_PEG_SELI.PEQTAABB-this.w_PEQTAUM1;
                    &i_ccchkf. ;
                 where;
                    PESERIAL = _Curs_PEG_SELI.PESERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_PEQTAUM1 = 0
            else
              this.w_PEQTAUM1 = this.w_PEQTAUM1-_Curs_PEG_SELI.PEQTAABB
              * --- Delete from PEG_SELI?
              * --- Write into PEG_SELI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PEQTAABB');
                    +i_ccchkf ;
                +" where ";
                    +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                       )
              else
                update (i_cTable) set;
                    PEQTAABB = 0;
                    &i_ccchkf. ;
                 where;
                    PESERIAL = _Curs_PEG_SELI.PESERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
              select _Curs_PEG_SELI
              continue
            enddo
            use
          endif
        endif
        endscan
      endif
      * --- Lancia Batch di Generazione Documenti di Scarico
      do GSCO_BGD with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SERIAL = this.w_DOCSCA
      this.w_CLADOC = "DI"
      this.w_FLVEAC = "V"
      this.w_DATMAT = g_DATMAT
      if not empty(nvl(this.w_SERIAL,""))
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Aggiorna saldi lotti  prima di eliminare i documenti
        if NOT EMPTY (this.w_SERIAL) AND g_MADV="S" AND (g_PERLOT="S" OR g_PERUBI="S")
          GSMD_BRL (this, this.w_SERIAL , "V" , "+" , ,.T.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if this.oParentObject.w_STADOC="S"
        if NOT EMPTY(this.w_DOCSCA)
          * --- Lancia il Report Documenti di Carico
          ah_Msg("Stampa documento di scarico materiali",.T.)
          this.w_MVSERIAL = this.w_DOCSCA
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if this.w_CICLI
      if this.w_TATTIV<>"Q"
        this.w_OPERAZ = "RI"
        this.w_nRecSel = this.w_nRecSel + 1
        * --- Generazione Documento di Scarico
        vq_exec("..\COLA\EXE\QUERY\GSCO_BRP.VQR",this,"GENEORDI")
        ah_Msg("Generazione scarico materiali per produzione...",.T.)
        this.w_MVTIPDOC = this.oParentObject.w_CAUSER
        this.w_MVDATDOC = this.oParentObject.w_DPDATREG
        this.w_MVDATDIV = this.w_MVDATDOC
        this.w_MVDATREG = this.w_MVDATDOC
        this.w_MVDATCIV = this.w_MVDATDOC
        this.w_MVPRP = "NN"
        this.w_MVNUMDOC = 0
        this.w_MVFLPROV = "N"
        this.w_MVNUMREG = 0
        this.w_MVNUMEST = 0
        this.w_MVALFEST = Space(10)
        this.w_MVSERIAL = SPACE(10)
        this.w_MVCODESE = g_CODESE
        this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO;
            from (i_cTable) where;
                TDTIPDOC = this.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
          this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
          this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
          this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
          this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
          this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
          this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
          this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVTIPCON = IIF(this.w_FLINTE $ "CF", this.w_FLINTE, " ")
        this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
        this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
        * --- Lancia Batch di Generazione Documenti
        do GSCO_BGD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_SERIAL = this.w_DOCSER
        this.w_CLADOC = "DI"
        this.w_FLVEAC = "V"
        this.w_DATMAT = g_DATMAT
        if this.oParentObject.w_STADOC="S"
          if NOT EMPTY(this.w_DOCSER)
            * --- Lancia il Report Documenti di Carico
            ah_Msg("Stampa documento",.T.)
            this.w_MVSERIAL = this.w_DOCSER
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
    endif
    this.w_OPERAZ = "MO"
    this.w_nRecSel = this.w_nRecSel + 1
    * --- Generazione Documento di Carico materiali di output
    vq_exec("..\COLA\EXE\QUERY\GSCO_BOP.VQR",this,"GENEORDI")
    ah_Msg("Generazione carico di produzione...",.T.)
    this.w_MVTIPDOC = this.oParentObject.w_CAUMOU
    this.w_MVDATDOC = this.oParentObject.w_DPDATREG
    this.w_MVDATDIV = this.w_MVDATDOC
    this.w_MVDATREG = this.w_MVDATDOC
    this.w_MVDATCIV = this.w_MVDATDOC
    this.w_MVPRP = "NN"
    this.w_MVNUMDOC = 0
    this.w_MVFLPROV = "N"
    this.w_MVNUMREG = 0
    this.w_MVNUMEST = 0
    this.w_MVALFEST = Space(10)
    this.w_MVSERIAL = SPACE(10)
    this.w_MVCODESE = g_CODESE
    this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO;
        from (i_cTable) where;
            TDTIPDOC = this.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
      this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
      this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
      this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
      this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
      this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
      this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
      this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
      this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
      this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
      this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
      this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVTIPCON = IIF(this.w_FLINTE $ "CF", this.w_FLINTE, " ")
    this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
    this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
    if this.w_OLDCOM and .f.
      * --- La quantit� gi� trasferita (se presente) deve essere legata al magazzino di trasferimento
      this.w_QTADAB = 0
      * --- Select from gsdb1bad
      do vq_exec with 'gsdb1bad',this,'_Curs_gsdb1bad','',.f.,.t.
      if used('_Curs_gsdb1bad')
        select _Curs_gsdb1bad
        locate for 1=1
        do while not(eof())
        * --- Read from ODL_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLQTAPR1,OLCODMAG"+;
            " from "+i_cTable+" ODL_DETT where ";
                +"OLCODODL = "+cp_ToStrODBC(_Curs_gsdb1bad.PERIFODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_gsdb1bad.PERIGORD);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLQTAPR1,OLCODMAG;
            from (i_cTable) where;
                OLCODODL = _Curs_gsdb1bad.PERIFODL;
                and CPROWNUM = _Curs_gsdb1bad.PERIGORD;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_QTAPR1 = NVL(cp_ToDate(_read_.OLQTAPR1),cp_NullValue(_read_.OLQTAPR1))
          this.w_MAGTRA = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Deve essere garantita la continuit� del WIP, cio� il wip di un odl deve sempre essere lo stesso
        this.w_RIFMAG = " Magazz.: " + this.w_MAGTRA
        if this.w_QTAPR1>0
          this.w_QTADAB = this.w_QTADAB+this.w_QTAPR1
        endif
          select _Curs_gsdb1bad
          continue
        enddo
        use
      endif
      Select GENEORDI 
 scan
      * --- Primo giro, quantit� abbinata al WIP
      this.w_PGCODODL = this.oParentObject.w_DPCODODL
      this.w_PECODART = GENEORDI.PDCODART
      this.w_PEQTAUM1 = min(GENEORDI.PDQTAUM1,this.w_QTADAB)
      this.w_PEQTAOLD = this.w_PEQTAUM1
      if this.w_PEQTAUM1>0
        this.w_PECODODL = this.w_RIFMAG
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_PEQTAUM1 = GENEORDI.PDQTAUM1-this.w_PEQTAOLD
      * --- Secondo giro, quantit� abbinata a magazzino originale di riferimento (solo materiale non trasferito)
      if this.w_PEQTAUM1>0
        this.w_PECODODL = " Magazz.: "+GENEORDI.PDCODMAG
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      endscan
    endif
    * --- Lancia Batch di Generazione Documenti di Carico
    do GSCO_BGD with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_SERIAL = this.w_DOCMOU
    this.w_CLADOC = "DI"
    this.w_FLVEAC = "A"
    this.w_DATMAT = g_DATMAT
    if not empty(nvl(this.w_SERIAL,"")) and .f.
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorna saldi lotti  prima di eliminare i documenti
      if NOT EMPTY (this.w_SERIAL) AND g_MADV="S" AND (g_PERLOT="S" OR g_PERUBI="S")
        GSMD_BRL (this, this.w_SERIAL , "V" , "+" , ,.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_STADOC="S"
      if NOT EMPTY(this.w_DOCMOU)
        * --- Lancia il Report Documenti di Carico
        ah_Msg("Stampa documento di carico",.T.)
        this.w_MVSERIAL = this.w_DOCMOU
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_CICLI
      if this.oParentObject.w_DPQTAPRO=0 AND this.oParentObject.w_DPQTASCA=0 and nvl(this.oParentObject.w_DPFLEVAS,"")="S" 
        * --- Caso particolare, non genero documenti ma evado
        * --- Read from ODL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOMME,OLTFLORD"+;
            " from "+i_cTable+" ODL_MAST where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOMME,OLTFLORD;
            from (i_cTable) where;
                OLCODODL = this.oParentObject.w_DPCODODL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
          this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
          this.w_OCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
          this.w_OSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
          this.w_ODTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
          this.w_ODTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
          this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
          this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
          this.w_FLORDD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ODTFIN = this.oParentObject.w_DPDATREG
        this.w_FLORDD = iif(this.w_FLORDD="+","-",iif(this.w_FLORDD="-","+",""))
        if empty(this.w_ODTINI)
          this.w_ODTINI = this.w_ODTFIN
        endif
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
          +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
          +",OLTDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_ODTINI),'ODL_MAST','OLTDTINI');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                 )
        else
          update (i_cTable) set;
              OLTFLEVA = "S";
              ,OLTQTSAL = 0;
              ,OLTSTATO = "F";
              ,OLTDTFIN = this.w_ODTFIN;
              ,OLTDTINI = this.w_ODTINI;
              &i_ccchkf. ;
           where;
              OLCODODL = this.oParentObject.w_DPCODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura ODL_MAST'
          return
        endif
        * --- Aggiorna saldi MPS
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODFAM,ARUNMIS1,ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODFAM,ARUNMIS1,ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_OKEYSA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FAMPRO = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
          this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORDD,'FMMPSLAN','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLORDD,'FMMPSTOT','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSLAN ="+cp_NullLink(i_cOp1,'MPS_TFOR','FMMPSLAN');
          +",FMMPSTOT ="+cp_NullLink(i_cOp2,'MPS_TFOR','FMMPSTOT');
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSLAN = &i_cOp1.;
              ,FMMPSTOT = &i_cOp2.;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_OKEYSA;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Storna Ordinato dai Saldi
        if this.w_OQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OCOMAG)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SLQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OKEYSA;
                and SLCODMAG = this.w_OCOMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (3)'
            return
          endif
          * --- Saldi commessa
          if this.w_SALCOM="S"
            if empty(nvl(this.w_OLTCOMME,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_OLTCOMME
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SCQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OKEYSA;
                  and SCCODMAG = this.w_OCOMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDICOM'
              return
            endif
          endif
        endif
        if this.oParentObject.w_DPULTFAS="S" and !Empty(this.oParentObject.w_DPODLFAS)
          * --- Aggiorno anche il padre (per gli ODL di fase)
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOMME,OLTFLORD"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTKEYSA,OLTQTSAL,OLTCOMAG,OLTSTATO,OLTDTINI,OLTDTFIN,OLTPERAS,OLTCOMME,OLTFLORD;
              from (i_cTable) where;
                  OLCODODL = this.oParentObject.w_DPODLFAS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
            this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
            this.w_OCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
            this.w_OSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
            this.w_ODTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
            this.w_ODTFIN = NVL(cp_ToDate(_read_.OLTDTFIN),cp_NullValue(_read_.OLTDTFIN))
            this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
            this.w_FASCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
            this.w_FLORDD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ODTFIN = this.oParentObject.w_DPDATREG
          this.w_FLORDD = iif(this.w_FLORDD="+","-",iif(this.w_FLORDD="-","+",""))
          if empty(this.w_ODTINI)
            this.w_ODTINI = this.w_ODTFIN
          endif
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
            +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
            +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
            +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
            +",OLTDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_ODTINI),'ODL_MAST','OLTDTINI');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                   )
          else
            update (i_cTable) set;
                OLTFLEVA = "S";
                ,OLTQTSAL = 0;
                ,OLTSTATO = "F";
                ,OLTDTFIN = this.w_ODTFIN;
                ,OLTDTINI = this.w_ODTINI;
                &i_ccchkf. ;
             where;
                OLCODODL = this.oParentObject.w_DPODLFAS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura ODL_MAST'
            return
          endif
          * --- Aggiorna saldi MPS
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODFAM,ARUNMIS1,ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODFAM,ARUNMIS1,ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OKEYSA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FAMPRO = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
            this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDD,'FMMPSLAN','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLORDD,'FMMPSTOT','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSLAN ="+cp_NullLink(i_cOp1,'MPS_TFOR','FMMPSLAN');
            +",FMMPSTOT ="+cp_NullLink(i_cOp2,'MPS_TFOR','FMMPSTOT');
                +i_ccchkf ;
            +" where ";
                +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                   )
          else
            update (i_cTable) set;
                FMMPSLAN = &i_cOp1.;
                ,FMMPSTOT = &i_cOp2.;
                &i_ccchkf. ;
             where;
                FMCODART = this.w_OKEYSA;
                and FMPERASS = this.w_PERASS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Storna Ordinato dai Saldi
          if this.w_OQTSAL<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OCOMAG)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SLQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OKEYSA;
                  and SLCODMAG = this.w_OCOMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDIART (3)'
              return
            endif
            * --- Saldi commessa
            if this.w_SALCOM="S"
              if empty(nvl(this.w_FASCOMME,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_FASCOMME
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SCQTOPER','this.w_OQTSAL',this.w_OQTSAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OKEYSA;
                    and SCCODMAG = this.w_OCOMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM'
                return
              endif
            endif
          endif
        endif
        * --- Storna impegnato
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.oParentObject.w_DPCODODL)+"";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.oParentObject.w_DPCODODL;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          this.w_MAGCOD = _Curs_ODL_DETT.OLCODMAG
          this.w_SALKEY = _Curs_ODL_DETT.OLKEYSAL
          * --- Aggiorna Qta Evasa sull ODL
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
            +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                OLFLEVAS = "S";
                ,OLQTASAL = 0;
                &i_ccchkf. ;
             where;
                OLCODODL = this.oParentObject.w_DPCODODL;
                and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura ODL_DETT'
            return
          endif
          * --- Se Nettificabile, Storna Impegnato dai Saldi
          this.w_DISMAG = " "
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = this.w_MAGCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_DISMAG="S" AND _Curs_ODL_DETT.OLQTASAL<>0 AND NOT EMPTY(_Curs_ODL_DETT.OLKEYSAL) AND NOT EMPTY(_Curs_ODL_DETT.OLCODMAG)
            * --- Inverto i flags
            this.w_FLORDD = iif(_Curs_ODL_DETT.OLFLORDI="+","-",iif(_Curs_ODL_DETT.OLFLORDI="-","+"," "))
            this.w_FLIMPD = iif(_Curs_ODL_DETT.OLFLIMPE="+","-",iif(_Curs_ODL_DETT.OLFLIMPE="-","+"," "))
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPD,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODMAG = "+cp_ToStrODBC(this.w_MAGCOD);
                  +" and SLCODICE = "+cp_ToStrODBC(this.w_SALKEY);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SLCODMAG = this.w_MAGCOD;
                  and SLCODICE = this.w_SALKEY;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Saldi commessa
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = _Curs_ODL_DETT.OLKEYSAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_OLTCOMME,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_OLTCOMME
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMPD,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_SALKEY);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGCOD);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_SALKEY;
                    and SCCODMAG = this.w_MAGCOD;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM'
                return
              endif
            endif
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
        this.w_CLFASEVA = IIF(this.oParentObject.w_DPFLEVAS="S", this.oParentObject.w_DPFLEVAS, "N")
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLFASEVA),'ODL_CICL','CLFASEVA');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
              +" and CLBFRIFE = "+cp_ToStrODBC(this.oParentObject.w_FASE);
                 )
        else
          update (i_cTable) set;
              CLFASEVA = this.w_CLFASEVA;
              &i_ccchkf. ;
           where;
              CLCODODL = this.oParentObject.w_DPODLFAS;
              and CLBFRIFE = this.oParentObject.w_FASE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Caso particolare fase di count point ma non output, non genero il documento ma devo evadere la parte dichiarata sull'ODL di fase
        if ! this.w_CREADOC and this.oParentObject.w_COUPOI="S" and this.oParentObject.w_DPFASOUT<>"S"
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTQTSAL,OLTKEYSA,OLTPERAS,OLTFLORD,OLTCOMAG,OLTCOMME"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTQTSAL,OLTKEYSA,OLTPERAS,OLTFLORD,OLTCOMAG,OLTCOMME;
              from (i_cTable) where;
                  OLCODODL = this.oParentObject.w_DPCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OQTASAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
            this.w_OKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
            this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
            this.w_FLORDD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
            this.w_OCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
            this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_NQTEVA = this.oParentObject.w_DPQTAPRO+this.oParentObject.w_DPQTASCA
          this.w_NQTEV1 = this.oParentObject.w_DPQTAPR1+this.oParentObject.w_DPQTASC1
          this.w_NQTPRO = this.oParentObject.w_DPQTAPRO
          this.w_NQTPR1 = this.oParentObject.w_DPQTAPR1
          this.w_NQTOSC = this.oParentObject.w_DPQTASCA
          this.w_NQTOS1 = this.oParentObject.w_DPQTASC1
          * --- Aggiorna Qta Evasa sull ODL
          this.w_NQTSAL = iif(nvl(this.oParentObject.w_DPFLEVAS,"")="S",0,iif(this.w_OQTASAL-this.w_NQTEVA>0,this.w_OQTASAL-this.w_NQTEVA,0))
          this.w_OEVA = iif(this.w_NQTSAL=0,"S","N")
          * --- Ripristina lo Stato
          this.w_OSTATO = IIF(this.w_NQTSAL=0, "F", "L")
          this.w_ODTFIN = IIF(this.w_OSTATO="L", cp_CharToDate("  -  -  "), this.oParentObject.w_DPDATREG)
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTQTOEV =OLTQTOEV+ "+cp_ToStrODBC(this.w_NQTEVA);
            +",OLTQTOE1 =OLTQTOE1+ "+cp_ToStrODBC(this.w_NQTEV1);
            +",OLTQTPRO =OLTQTPRO+ "+cp_ToStrODBC(this.w_NQTPRO);
            +",OLTQTPR1 =OLTQTPR1+ "+cp_ToStrODBC(this.w_NQTPR1);
            +",OLTQTOSC =OLTQTOSC+ "+cp_ToStrODBC(this.w_NQTOSC);
            +",OLTQTOS1 =OLTQTOS1+ "+cp_ToStrODBC(this.w_NQTOS1);
            +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OEVA),'ODL_MAST','OLTFLEVA');
            +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTSAL),'ODL_MAST','OLTQTSAL');
            +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OSTATO),'ODL_MAST','OLTSTATO');
            +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ODTFIN),'ODL_MAST','OLTDTFIN');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                   )
          else
            update (i_cTable) set;
                OLTQTOEV = OLTQTOEV + this.w_NQTEVA;
                ,OLTQTOE1 = OLTQTOE1 + this.w_NQTEV1;
                ,OLTQTPRO = OLTQTPRO + this.w_NQTPRO;
                ,OLTQTPR1 = OLTQTPR1 + this.w_NQTPR1;
                ,OLTQTOSC = OLTQTOSC + this.w_NQTOSC;
                ,OLTQTOS1 = OLTQTOS1 + this.w_NQTOS1;
                ,OLTFLEVA = this.w_OEVA;
                ,OLTQTSAL = this.w_NQTSAL;
                ,OLTSTATO = this.w_OSTATO;
                ,OLTDTFIN = this.w_ODTFIN;
                &i_ccchkf. ;
             where;
                OLCODODL = this.oParentObject.w_DPCODODL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura ODL_MAST'
            return
          endif
          * --- Aggiorna saldi MPS
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODFAM,ARUNMIS1,ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODFAM,ARUNMIS1,ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OKEYSA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FAMPRO = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
            this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_FLORDD = iif(this.w_FLORDD="+","-",iif(this.w_FLORDD="-","+",""))
          * --- Try
          local bErr_0526D270
          bErr_0526D270=bTrsErr
          this.Try_0526D270()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0526D270
          * --- End
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDD,'FMMPSLAN','this.w_NQTEV1',this.w_NQTEV1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLORDD,'FMMPSTOT','this.w_NQTEV1',this.w_NQTEV1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSLAN ="+cp_NullLink(i_cOp1,'MPS_TFOR','FMMPSLAN');
            +",FMMPSTOT ="+cp_NullLink(i_cOp2,'MPS_TFOR','FMMPSTOT');
                +i_ccchkf ;
            +" where ";
                +"FMCODART = "+cp_ToStrODBC(this.w_OKEYSA);
                +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                   )
          else
            update (i_cTable) set;
                FMMPSLAN = &i_cOp1.;
                ,FMMPSTOT = &i_cOp2.;
                &i_ccchkf. ;
             where;
                FMCODART = this.w_OKEYSA;
                and FMPERASS = this.w_PERASS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorna saldi
          if this.w_NQTEV1<>0 AND NOT EMPTY(this.w_OKEYSA) AND NOT EMPTY(this.w_OCOMAG)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SLQTOPER','this.w_NQTEV1',this.w_NQTEV1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OKEYSA;
                  and SLCODMAG = this.w_OCOMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDIART (3)'
              return
            endif
            * --- Saldi commessa
            if this.w_SALCOM="S"
              if empty(nvl(this.w_OLTCOMME,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_OLTCOMME
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDD,'SCQTOPER','this.w_NQTEV1',this.w_NQTEV1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OKEYSA);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OCOMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OKEYSA;
                    and SCCODMAG = this.w_OCOMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM'
                return
              endif
            endif
          endif
          if !Empty(this.oParentObject.w_DPODLFAS)
            * --- Aggiorna anche le fasi del padre
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTQTOE1,OLTQTOD1"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTQTOE1,OLTQTOD1;
                from (i_cTable) where;
                    OLCODODL = this.oParentObject.w_DPCODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLDICUM1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
              this.w_QTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_NQTEVA = this.oParentObject.w_DPQTAPRO+this.oParentObject.w_DPQTASCA
            this.w_NQTEV1 = this.oParentObject.w_DPQTAPR1+this.oParentObject.w_DPQTASC1
            this.w_CLFASEVA = IIF(this.oParentObject.w_DPFLEVAS="S" or this.w_CLDICUM1 >= this.w_QTOD1 , "S", "N")
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLQTAAVA =CLQTAAVA+ "+cp_ToStrODBC(this.oParentObject.w_DPQTAPRO);
              +",CLAVAUM1 =CLAVAUM1+ "+cp_ToStrODBC(this.oParentObject.w_DPQTAPR1);
              +",CLQTASCA =CLQTASCA+ "+cp_ToStrODBC(this.oParentObject.w_DPQTASCA);
              +",CLSCAUM1 =CLSCAUM1+ "+cp_ToStrODBC(this.oParentObject.w_DPQTASC1);
              +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLFASEVA),'ODL_CICL','CLFASEVA');
              +",CLQTADIC =CLQTADIC+ "+cp_ToStrODBC(this.w_NQTEVA);
              +",CLDICUM1 =CLDICUM1+ "+cp_ToStrODBC(this.w_NQTEV1);
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPODLFAS);
                  +" and CLBFRIFE = "+cp_ToStrODBC(this.oParentObject.w_FASE);
                     )
            else
              update (i_cTable) set;
                  CLQTAAVA = CLQTAAVA + this.oParentObject.w_DPQTAPRO;
                  ,CLAVAUM1 = CLAVAUM1 + this.oParentObject.w_DPQTAPR1;
                  ,CLQTASCA = CLQTASCA + this.oParentObject.w_DPQTASCA;
                  ,CLSCAUM1 = CLSCAUM1 + this.oParentObject.w_DPQTASC1;
                  ,CLFASEVA = this.w_CLFASEVA;
                  ,CLQTADIC = CLQTADIC + this.w_NQTEVA;
                  ,CLDICUM1 = CLDICUM1 + this.w_NQTEV1;
                  &i_ccchkf. ;
               where;
                  CLCODODL = this.oParentObject.w_DPODLFAS;
                  and CLBFRIFE = this.oParentObject.w_FASE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    if this.w_CICLI and this.w_CREADOC
      if this.oParentObject.w_DPCRIVAL="P"
        this.w_PRZTOT = 0
        * --- Sommo il valore totale di risorse e scarichi
        vq_exec("..\COLA\EXE\QUERY\GSCOPQIC.VQR", this,"CARVALO")
        select CARVALO
        scan
        this.w_PRZTOT = this.w_PRZTOT+NVL(CARVALO.IMPNAZ,0)
        endscan
        * --- Quantit� totale (buoni pi� scarti) carico
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select sum(MVQTAMOV) as MOVTOT  from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL="+cp_ToStrODBC(this.w_DOCCAR)+"";
               ,"_Curs_DOC_DETT")
        else
          select sum(MVQTAMOV) as MOVTOT from (i_cTable);
           where MVSERIAL=this.w_DOCCAR;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          this.w_TOTQTA = NVL(MOVTOT,0)
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
        if this.w_TOTQTA>0
          * --- Valore unitario
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLVEAC,MVCODVAL,MVCAOVAL,MVDATDOC,MVVALNAZ"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_DOCCAR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLVEAC,MVCODVAL,MVCAOVAL,MVDATDOC,MVVALNAZ;
              from (i_cTable) where;
                  MVSERIAL = this.w_DOCCAR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLSTRN = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_VALCOD = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
            this.w_CAMBIO = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
            this.w_DATDCM = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_VALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT,VADECUNI"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_VALCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT,VADECUNI;
              from (i_cTable) where;
                  VACODVAL = this.w_VALCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PRZUNI = cp_Round(this.w_PRZTOT/this.w_TOTQTA, this.w_DECUNI)
          * --- Gestione commessa
          if g_COMM="S"
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDFLCOMM"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUCAR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDFLCOMM;
                from (i_cTable) where;
                    TDTIPDOC = this.oParentObject.w_CAUCAR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVTFLCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_MVTFLCOM = SPACE(1)
          endif
          * --- Aggiorno carico
          * --- Select from DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MVSERIAL,CPROWNUM,MVQTAMOV,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVIMPSCO,MVIMPACC,MVCODCOM,MVCODCEN,MVCODCOS,MVIMPCOM,MVCODATT,MVTIPATT,MVCAUMAG,MVFLORCO,MVFLCOCO  from "+i_cTable+" DOC_DETT ";
                +" where MVSERIAL="+cp_ToStrODBC(this.w_DOCCAR)+"";
                 ,"_Curs_DOC_DETT")
          else
            select MVSERIAL,CPROWNUM,MVQTAMOV,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVIMPSCO,MVIMPACC,MVCODCOM,MVCODCEN,MVCODCOS,MVIMPCOM,MVCODATT,MVTIPATT,MVCAUMAG,MVFLORCO,MVFLCOCO from (i_cTable);
             where MVSERIAL=this.w_DOCCAR;
              into cursor _Curs_DOC_DETT
          endif
          if used('_Curs_DOC_DETT')
            select _Curs_DOC_DETT
            locate for 1=1
            do while not(eof())
            this.w_RIGA = CPROWNUM
            this.w_MOVQTA = NVL(MVQTAMOV,0)
            this.w_SCONT1 = NVL(MVSCONT1,0)
            this.w_SCONT2 = NVL(MVSCONT2,0)
            this.w_SCONT3 = NVL(MVSCONT3,0)
            this.w_SCONT4 = NVL(MVSCONT4,0)
            this.w_IMPSCO = NVL(MVIMPSCO,0)
            this.w_IMPACC = NVL(MVIMPACC,0)
            this.w_OLDIMPCOM = NVL(MVIMPCOM,0)
            this.w_CODCOM = NVL(MVCODCOM, SPACE(15))
            this.w_CODCEN = NVL(MVCODCEN, SPACE(15))
            this.w_VOCCOS = NVL(MVCODCOS, SPACE(5))
            this.w_TIPATT = NVL(MVTIPATT,"A")
            this.w_CODATT = NVL(MVCODATT, SPACE(15))
            this.w_CAUMAGA = NVL(MVCAUMAG, SPACE(5))
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLCOMM"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAGA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLCOMM;
                from (i_cTable) where;
                    CMCODICE = this.w_CAUMAGA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FLORCO = NVL(MVFLORCO,"")
            this.w_FLCOCO = NVL(MVFLCOCO,"")
            * --- Flag invertiti (per storno)
            this.w_IFLORCO = iif(this.w_FLORCO="+","-",iif(this.w_FLORCO="-","+"," "))
            this.w_IFLCOCO = iif(this.w_FLCOCO="+","-",iif(this.w_FLCOCO="-","+"," "))
            * --- Scorporo IVA non gestito (il flag � sul cliente e sui documenti di produzione non c'� intestatario)
            this.w_FLSCOR = "N"
            this.w_CODIVA = "     "
            this.w_PERIVA = 0
            this.w_PERIVE = 0
            this.w_INDIVA = 0
            this.w_INDIVE = 0
            this.w_VALRIG = CAVALRIG(this.w_PRZUNI,this.w_MOVQTA, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
            this.w_VALMAG = CAVALMAG(this.w_FLSCOR,this.w_VALRIG, this.w_IMPSCO, this.w_IMPACC, this.w_PERIVA, this.w_DECTOT, this.w_CODIVA, this.w_PERIVE )
            this.w_IMPNAZ = CAIMPNAZ(this.w_FLSTRN, this.w_VALMAG, this.w_CAMBIO, g_CAOVAL, this.w_DATDCM, this.w_VALNAZ, this.w_VALCOD, this.w_CODIVA, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
            if Not Empty(this.w_CODCOM)
              * --- Leggo valuta della Commessa e suoi decimali
              * --- Read from CAN_TIER
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAN_TIER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CNCODVAL"+;
                  " from "+i_cTable+" CAN_TIER where ";
                      +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CNCODVAL;
                  from (i_cTable) where;
                      CNCODCAN = this.w_CODCOM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from VALUTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VALUTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VADECTOT"+;
                  " from "+i_cTable+" VALUTE where ";
                      +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VADECTOT;
                  from (i_cTable) where;
                      VACODVAL = this.w_COCODVAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_IMPCOM = CAIMPCOM( IIF(Empty(this.w_CODCOM),"S", " "), this.w_VALCOD, this.w_COCODVAL, this.w_IMPNAZ, this.w_CAMBIO, this.w_DATDCM, this.w_DECCOM )
            endif
            if g_COMM="S"
              this.w_OK_COMM = g_COMM="S" And this.w_MFLCOMM<>"N" And not empty(this.w_CODCOM) and not empty(this.w_CODATT)
            endif
            * --- Costi di Commessa
            if this.w_OK_COMM and nvl(this.w_MVTFLCOM, " ") = "S"
              * --- Storno vecchi costi
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_IFLCOCO,'CSCONSUN','this.w_OLDIMPCOM',this.w_OLDIMPCOM,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_IFLORCO,'CSORDIN','this.w_OLDIMPCOM',this.w_OLDIMPCOM,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                    +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(w_CODCOS);
                       )
              else
                update (i_cTable) set;
                    CSCONSUN = &i_cOp1.;
                    ,CSORDIN = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.w_CODCOM;
                    and CSCODMAT = this.w_CODATT;
                    and CSCODCOS = w_CODCOS;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_FLORCO = IIF(g_COMM="S" AND this.w_FLSTRN $ "AV" ,iif(this.w_MFLCOMM="I","+",IIF(this.w_MFLCOMM="D","-"," "))," ")
              this.w_FLCOCO = IIF(g_COMM="S" AND this.w_FLSTRN $ "AV" ,iif(this.w_MFLCOMM="C","+",IIF(this.w_MFLCOMM="S","-"," "))," ")
              * --- Inserisco nuovi costi
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLCOCO,'CSCONSUN','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLORCO,'CSORDIN','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                    +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(w_CODCOS);
                       )
              else
                update (i_cTable) set;
                    CSCONSUN = &i_cOp1.;
                    ,CSORDIN = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.w_CODCOM;
                    and CSCODMAT = this.w_CODATT;
                    and CSCODCOS = w_CODCOS;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PRZUNI),'DOC_DETT','MVPREZZO');
              +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
              +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_VALRIG),'DOC_DETT','MVVALRIG');
              +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'DOC_DETT','MVVALMAG');
              +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCOM),'DOC_DETT','MVIMPCOM');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_DOCCAR);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGA);
                     )
            else
              update (i_cTable) set;
                  MVPREZZO = this.w_PRZUNI;
                  ,MVIMPNAZ = this.w_IMPNAZ;
                  ,MVVALRIG = this.w_VALRIG;
                  ,MVVALMAG = this.w_VALMAG;
                  ,MVIMPCOM = this.w_IMPCOM;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_DOCCAR;
                  and CPROWNUM = this.w_RIGA;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
              select _Curs_DOC_DETT
              continue
            enddo
            use
          endif
          * --- Ricalcolo i totali
          gsar_brd(this,this.w_DOCCAR)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    this.w_oPart = this.w_oMess.AddMsgPartNL("Elaborazione terminata%0N.%1 documenti generati%0Su %2 documenti da generare%0N.%3 articoli movimentati")
    this.w_oPart.AddParam(alltrim(str(this.w_nRecDoc,5,0)))     
    this.w_oPart.AddParam(alltrim(str(this.w_nRecSel,5,0)))     
    this.w_oPart.AddParam(alltrim(str(this.w_nRecEla,5,0)))     
    if USED("MessErr") AND this.w_LNumErr>0
      this.w_oPart = this.w_oMess.AddMsgPartNL("%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?")
      this.w_oPart.AddParam(alltrim(str(this.w_LNumErr,5,0)))     
      if this.w_oMess.ah_YesNo()
        SELECT * FROM MessErr INTO CURSOR __TMP__
        CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
      endif
    else
      this.w_oMess.ah_ErrorMsg()
    endif
    * --- Chiusura Cursori
    USE IN SELECT("MATRERRO")
    USE IN SELECT("__TMP__")
    USE IN SELECT("GENEORDI")
    USE IN SELECT("APPOVALO")
    USE IN SELECT("CARVALO")
    USE IN SELECT("APPORISO")
    USE IN SELECT("MessErr")
  endproc
  proc Try_0526D270()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TFOR
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TFOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FMCODICE"+",FMPERASS"+",FMCODART"+",FMCODVAR"+",FMKEYSAL"+",FMFAMPRO"+",FMUNIMIS"+",FMMPSCON"+",FMMPSDAP"+",FMMPSPIA"+",FMMPSLAN"+",FMMPSSUG"+",FMMPSTOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERASS),'MPS_TFOR','FMPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMCODART');
      +","+cp_NullLink(cp_ToStrODBC("                    "),'MPS_TFOR','FMCODVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OKEYSA),'MPS_TFOR','FMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FAMPRO),'MPS_TFOR','FMFAMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'MPS_TFOR','FMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSDAP');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSPIA');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSLAN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSSUG');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSTOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FMCODICE',this.w_OKEYSA,'FMPERASS',this.w_PERASS,'FMCODART',this.w_OKEYSA,'FMCODVAR',"                    ",'FMKEYSAL',this.w_OKEYSA,'FMFAMPRO',this.w_FAMPRO,'FMUNIMIS',this.w_UNIMIS,'FMMPSCON',0,'FMMPSDAP',0,'FMMPSPIA',0,'FMMPSLAN',0,'FMMPSSUG',0)
      insert into (i_cTable) (FMCODICE,FMPERASS,FMCODART,FMCODVAR,FMKEYSAL,FMFAMPRO,FMUNIMIS,FMMPSCON,FMMPSDAP,FMMPSPIA,FMMPSLAN,FMMPSSUG,FMMPSTOT &i_ccchkf. );
         values (;
           this.w_OKEYSA;
           ,this.w_PERASS;
           ,this.w_OKEYSA;
           ,"                    ";
           ,this.w_OKEYSA;
           ,this.w_FAMPRO;
           ,this.w_UNIMIS;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifiche di Congruita'
    if this.w_CICLI
      do case
        case this.w_TATTIV="E"
          * --- Dichiarazione quantit� e tempi
          if EMPTY(this.oParentObject.w_CAUCAR) OR EMPTY(this.oParentObject.w_CAUSCA)
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Causali di carico/scarico non definite nei parametri",,"")
            else
              this.TmpC = "Causali di carico/scarico non definite nei parametri"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
          if EMPTY(this.oParentObject.w_CAUSER) and this.oParentObject.w_TESTFORM
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Causali dei tempi risorse non definita",,"")
            else
              this.TmpC = "Causali dei tempi risorse non definita"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
        case this.w_TATTIV="Q"
          * --- Dichiarazione solo quantit�
          if EMPTY(this.oParentObject.w_CAUCAR) OR EMPTY(this.oParentObject.w_CAUSCA)
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Causali di carico/scarico non definite nei parametri",,"")
            else
              this.TmpC = "Causali di carico/scarico non definite nei parametri"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
          if this.oParentObject.w_DPQTAPRO=0 AND this.oParentObject.w_DPQTASCA=0 and this.oParentObject.w_DPFLEVAS<>"S" and this.oParentObject.w_TESTFORM
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Indicare la qta da produrre e/o la qta scartata o la chiusura a 0 della fase",,"")
            else
              this.TmpC = "Indicare la qta da produrre e/o la qta scartata o la chiusura a 0 della fase"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
        case this.w_TATTIV="T"
          * --- Dichiarazione solo tempi
          if EMPTY(this.oParentObject.w_CAUSER)
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Causali dei tempi risorse non definita",,"")
            else
              this.TmpC = "Causali dei tempi risorse non definita"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
          if this.oParentObject.w_DPQTATEM=0 and this.oParentObject.w_TESTFORM
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Indicare il tempo della fase",,"")
            else
              this.TmpC = "Indicare il tempo della fase"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
      endcase
    else
      if EMPTY(this.oParentObject.w_CAUCAR) OR EMPTY(this.oParentObject.w_CAUSCA)
        if this.w_NOMSG<>"S"
          ah_ErrorMsg("Causali di carico/scarico non definite nei parametri",,"")
        else
          this.TmpC = "Causali di carico/scarico non definite nei parametri"
        endif
        this.oParentObject.w_TESTFORM = .F.
      endif
    endif
    if this.oParentObject.w_TESTFORM
      * --- Verifica Congruita' causali Documenti e Magazzino
      if NOT EMPTY(this.oParentObject.w_CAUCAR)
        this.w_FLINTE = " "
        this.w_CAUMAG = SPACE(5)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLINTE,TDCAUMAG,TDFLVEAC,TDFLANAL"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUCAR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLINTE,TDCAUMAG,TDFLVEAC,TDFLANAL;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_CAUCAR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_CAUMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_LFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_CAUMAG) OR this.w_FLINTE<>"N"
          if this.w_NOMSG<>"S"
            ah_ErrorMsg("Causale documento di carico incongruente o inesistente",,"")
          else
            this.TmpC = "Causale documento di carico incongruente o inesistente"
          endif
          this.oParentObject.w_TESTFORM = .F.
        else
          this.w_FLCASC = " "
          this.w_FLRISE = " "
          this.w_FLORDI = " "
          this.w_FLIMPE = " "
          this.w_CAUCOL = SPACE(5)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLCASC<>"+" OR NOT EMPTY(this.w_FLORDI+this.w_FLIMPE+this.w_FLRISE) OR NOT EMPTY(this.w_CAUCOL)
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Causale magazzino associata al documento di carico incongruente o inesistente",,"")
            else
              this.TmpC = "Causale magazzino associata al documento di carico incongruente o inesistente"
            endif
            this.oParentObject.w_TESTFORM = .F.
          else
            if NOT EMPTY(this.oParentObject.w_CAUMOU)
              this.w_FLINTM = " "
              this.w_CAUMGM = SPACE(5)
              * --- Read from TIP_DOCU
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TDFLINTE,TDCAUMAG"+;
                  " from "+i_cTable+" TIP_DOCU where ";
                      +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUMOU);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TDFLINTE,TDCAUMAG;
                  from (i_cTable) where;
                      TDTIPDOC = this.oParentObject.w_CAUMOU;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLINTM = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
                this.w_CAUMGM = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if EMPTY(this.w_CAUMGM) OR this.w_FLINTM<>"N"
                if this.w_NOMSG<>"S"
                  ah_ErrorMsg("Causale magazzino associata al documento di carico materiali di output incongruente o inesistente",,"")
                else
                  this.TmpC = "Causale magazzino associata al documento di carico materiali di output incongruente o inesistente"
                endif
                this.oParentObject.w_TESTFORM = .F.
              endif
            else
              if this.w_NOMSG<>"S"
                ah_ErrorMsg("Causale documento associata al documento di carico dei materiali di output incongruente o inesistente",,"")
              else
                this.TmpC = "Causale documento associata al documento di carico dei materiali di output incongruente o inesistente"
              endif
              this.oParentObject.w_TESTFORM = .F.
            endif
          endif
        endif
      endif
    endif
    if this.oParentObject.w_TESTFORM AND Not Empty(CHKCONS(this.w_LFLVEAC+"M"+IIF(this.w_FLANAL="S","C",""),this.oParentObject.w_DPDATREG,"B","N")) and NOT EMPTY(this.oParentObject.w_CAUCAR)
      * --- Controllo Date Consolidamento Carichi
      this.TmpC = CHKCONS(this.w_LFLVEAC+"M"+IIF(this.w_FLANAL="S","C",""),this.oParentObject.w_DPDATREG,"B","N")+"!"
      if this.w_NOMSG<>"S"
        ah_ErrorMsg("%1",,"", this.TmpC)
      endif
      this.oParentObject.w_TESTFORM = .F.
    endif
    if this.oParentObject.w_TESTFORM AND this.oParentObject.w_DPQTASCA<>0
      * --- Se Esiste Qta Scartata Verifica Congruita' Magazzino Scarti
      if EMPTY(this.oParentObject.w_MGSCAR) 
        if this.w_NOMSG<>"S"
          ah_ErrorMsg("Codice magazzino scarti non definito nei parametri",,"")
        else
          this.TmpC = "Codice magazzino scarti non definito nei parametri"
        endif
        this.oParentObject.w_TESTFORM = .F.
      else
        this.w_WIPSCA = "W"
        this.w_NETSCA = "S"
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGTIPMAG,MGDISMAG"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MGSCAR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGTIPMAG,MGDISMAG;
            from (i_cTable) where;
                MGCODMAG = this.oParentObject.w_MGSCAR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_WIPSCA = NVL(cp_ToDate(_read_.MGTIPMAG),cp_NullValue(_read_.MGTIPMAG))
          this.w_NETSCA = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_NETSCA="S" OR this.w_WIPSCA="W"
          if this.w_NOMSG<>"S"
            ah_ErrorMsg("Codice magazzino scarti incongruente",,"")
          else
            this.TmpC = "Codice magazzino scarti incongruente"
          endif
          this.oParentObject.w_TESTFORM = .F.
        endif
      endif
    endif
    if this.oParentObject.w_TESTFORM
      * --- Verifica Causale di Scarico
      if NOT EMPTY(this.oParentObject.w_CAUSCA)
        this.w_FLINTE = " "
        this.w_CAUMAG = SPACE(5)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLINTE,TDCAUMAG,TDFLVEAC,TDFLANAL"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUSCA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLINTE,TDCAUMAG,TDFLVEAC,TDFLANAL;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_CAUSCA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_CAUMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_LFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_CAUMAG) OR this.w_FLINTE<>"N"
          if this.w_NOMSG<>"S"
            ah_ErrorMsg("Causale documento di scarico incongruente o inesistente",,"")
          else
            this.TmpC = "Causale documento di scarico incongruente o inesistente"
          endif
          this.oParentObject.w_TESTFORM = .F.
        else
          this.w_FLCASC = " "
          this.w_FLRISE = " "
          this.w_FLORDI = " "
          this.w_FLIMPE = " "
          this.w_CAUCOL = SPACE(5)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLCASC<>"-" OR NOT EMPTY(this.w_FLORDI+this.w_FLIMPE+this.w_FLRISE) OR NOT EMPTY(this.w_CAUCOL)
            if this.w_NOMSG<>"S"
              ah_ErrorMsg("Causale magazzino associata al documento di scarico incongruente o inesistente",,"")
            else
              this.TmpC = "Causale magazzino associata al documento di scarico incongruente o inesistente"
            endif
            this.oParentObject.w_TESTFORM = .F.
          endif
        endif
      endif
    endif
    if this.oParentObject.w_TESTFORM AND Not Empty(CHKCONS(this.w_LFLVEAC+"M"+IIF(this.w_FLANAL="S","C",""),this.oParentObject.w_DPDATREG,"B","N")) and NOT EMPTY(this.oParentObject.w_CAUSCA)
      * --- Controllo Date Consolidamento Scarichi
      this.TmpC = CHKCONS(this.w_LFLVEAC+"M"+IIF(this.w_FLANAL="S","C",""),this.oParentObject.w_DPDATREG,"B","N")+"!"
      if this.w_NOMSG<>"S"
        ah_ErrorMsg("%1",,"", this.TmpC)
      endif
      this.oParentObject.w_TESTFORM = .F.
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Documenti 
    * --- Variabili da passare alle Stampe
    this.w_SPUNTA = "N"
    this.w_CODESE = "    "
    this.w_FLEVAS = " "
    this.w_TIPOIN = this.w_MVTIPDOC
    this.w_CLIFOR = SPACE(15)
    this.w_DATAIN = i_INIDAT
    this.w_DATA1 = i_INIDAT
    this.w_DATAFI = i_FINDAT
    this.w_DATA2 = i_FINDAT
    this.w_NUMINI = 1
    this.w_NUMFIN = 999999
    this.w_serie1 = ""
    this.w_serie2 = ""
    this.w_STAMPA = 0
    this.w_MVRIFFAD = SPACE(10)
    this.w_MVRIFODL = SPACE(10)
    this.w_TIPCON = this.w_MVTIPCON
    if EMPTY(this.w_TIPOIN)
      ah_ErrorMsg("Causale documento inesistente",,"")
    else
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDCATDOC,TDPRGSTA"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPOIN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDCATDOC,TDPRGSTA;
          from (i_cTable) where;
              TDTIPDOC = this.w_TIPOIN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        this.w_STAMPA = NVL(cp_ToDate(_read_.TDPRGSTA),cp_NullValue(_read_.TDPRGSTA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      GSVE_BRD(this,"F")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Documento, Storna Saldi e verifica se non Evaso
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
          +" where MVSERIAL="+cp_ToStrODBC(this.w_SERDOC)+"";
           ,"_Curs_DOC_DETT")
    else
      select * from (i_cTable);
       where MVSERIAL=this.w_SERDOC;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      * --- Aggiorna Saldi
      if this.w_OK=.T.
        if NVL(_Curs_DOC_DETT.MVQTAEVA,0)<>0 OR NOT EMPTY(NVL(_Curs_DOC_DETT.MVFLEVAS," ")) OR NOT EMPTY(CP_TODATE(_Curs_DOC_DETT.MVDATGEN))
          this.w_OK = .F.
          this.TmpC = ah_Msgformat("Documento: %1 del %2%0Gi� evaso da altri documenti; impossibile eliminare", ALLTR(STR(NVL(_Curs_DOC_DETT.MVNUMDOC,0),15))+" "+IIF(Not Empty(NVL(_Curs_DOC_DETT.MVALFDOC," ")),Alltrim(NVL(_Curs_DOC_DETT.MVALFDOC," ")),""), DTOC(CP_TODATE(_Curs_DOC_DETT.MVDATDOC)) )
        else
          this.w_ROWRIF = NVL(CPROWNUM, 0)
          this.w_QTAPR1 = NVL(MVQTASAL, 0)
          this.w_KEYSAL = NVL(MVKEYSAL, " ")
          this.w_CODMAG = NVL(MVCODMAG, " ")
          this.w_CODMAT = NVL(MVCODMAT, " ")
          this.w_MFLCASC = NVL(MVFLCASC, " ")
          this.w_MFLORDI = NVL(MVFLORDI, " ")
          this.w_MFLIMPE = NVL(MVFLIMPE, " ")
          this.w_MFLRISE = NVL(MVFLRISE, " ")
          this.w_MF2CASC = NVL(MVF2CASC, " ")
          this.w_MF2ORDI = NVL(MVF2ORDI, " ")
          this.w_MF2IMPE = NVL(MVF2IMPE, " ")
          this.w_MF2RISE = NVL(MVF2RISE, " ")
          this.w_MF2COCO = NVL(MVFLCOCO, " ")
          this.w_MF2ORCO = NVL(MVFLORCO, " ")
          this.w_MFCODCOS = NVL(MVCODCOS, " ")
          this.w_MFCODCOM = NVL(MVCODCOM, " ")
          this.w_MFCODATT = NVL(MVCODATT, " ")
          if this.w_QTAPR1<>0 AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG)
            if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
              this.w_MFLCASC = IIF(this.w_MFLCASC="+", "-", IIF(this.w_MFLCASC="-", "+", " "))
              this.w_MFLORDI = IIF(this.w_MFLORDI="+", "-", IIF(this.w_MFLORDI="-", "+", " "))
              this.w_MFLIMPE = IIF(this.w_MFLIMPE="+", "-", IIF(this.w_MFLIMPE="-", "+", " "))
              this.w_MFLRISE = IIF(this.w_MFLRISE="+", "-", IIF(this.w_MFLRISE="-", "+", " "))
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
                +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
                +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTAPER = &i_cOp1.;
                    ,SLQTRPER = &i_cOp2.;
                    ,SLQTOPER = &i_cOp3.;
                    ,SLQTIPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART (1)'
                return
              endif
              * --- Saldi commessa
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_KEYSAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_KEYSAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_MFCODCOM,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_MFCODCOM
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                  i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                  +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                  +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTAPER = &i_cOp1.;
                      ,SCQTRPER = &i_cOp2.;
                      ,SCQTOPER = &i_cOp3.;
                      ,SCQTIPER = &i_cOp4.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM'
                  return
                endif
              endif
            endif
            if (not empty(this.w_MF2COCO) or not empty(this.w_MF2ORCO)) and not empty(this.w_MFCODCOS) and not empty(this.w_MFCODCOM) and not empty(this.w_MFCODATT)
              this.w_MVFLORCO = IIF(this.w_MF2ORCO="+", "-", IIF(this.w_MF2ORCO="-","+", " "))
              this.w_MVFLCOCO = IIF(this.w_MF2COCO="+", "-", IIF(this.w_MF2COCO="-","+", " "))
              this.w_MVIMPCOM = NVL(MVIMPCOM,0)
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_MVFLCOCO,'CSCONSUN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.w_MFCODCOM);
                    +" and CSCODMAT = "+cp_ToStrODBC(this.w_MFCODATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(this.w_MFCODCOS);
                       )
              else
                update (i_cTable) set;
                    CSCONSUN = &i_cOp1.;
                    ,CSORDIN = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.w_MFCODCOM;
                    and CSCODMAT = this.w_MFCODATT;
                    and CSCODCOS = this.w_MFCODCOS;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Eventuale Causale Collegata 
            if NOT EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE) AND NOT EMPTY(this.w_CODMAT)
              this.w_MF2CASC = IIF(this.w_MF2CASC="+", "-", IIF(this.w_MF2CASC="-", "+", " "))
              this.w_MF2ORDI = IIF(this.w_MF2ORDI="+", "-", IIF(this.w_MF2ORDI="-", "+", " "))
              this.w_MF2IMPE = IIF(this.w_MF2IMPE="+", "-", IIF(this.w_MF2IMPE="-", "+", " "))
              this.w_MF2RISE = IIF(this.w_MF2RISE="+", "-", IIF(this.w_MF2RISE="-", "+", " "))
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SLQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SLQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SLQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SLQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
                +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
                +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                       )
              else
                update (i_cTable) set;
                    SLQTAPER = &i_cOp1.;
                    ,SLQTRPER = &i_cOp2.;
                    ,SLQTOPER = &i_cOp3.;
                    ,SLQTIPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAT;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART (2)'
                return
              endif
              * --- Saldi commessa
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_KEYSAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_KEYSAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_MFCODCOM,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_MFCODCOM
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SCQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SCQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SCQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                  i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SCQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                  +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                  +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTAPER = &i_cOp1.;
                      ,SCQTRPER = &i_cOp2.;
                      ,SCQTOPER = &i_cOp3.;
                      ,SCQTIPER = &i_cOp4.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAT;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM'
                  return
                endif
              endif
            endif
          endif
        endif
      endif
      if this.w_OK=.T.
        * --- Gestione Matricole
        this.w_STORNAMAT = False
        this.w_MTCODMAT = ""
        this.w_MSGMATRI = ""
        this.w_MVNUMRIF = -20
        * --- Select from MOVIMATR
        i_nConn=i_TableProp[this.MOVIMATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MTKEYSAL,MTCODMAT,MT_SALDO  from "+i_cTable+" MOVIMATR ";
              +" where MTSERIAL="+cp_ToStrODBC(this.w_SERDOC)+" AND MTROWNUM="+cp_ToStrODBC(this.w_ROWRIF)+" AND MTNUMRIF="+cp_ToStrODBC(this.w_MVNUMRIF)+"";
               ,"_Curs_MOVIMATR")
        else
          select MTKEYSAL,MTCODMAT,MT_SALDO from (i_cTable);
           where MTSERIAL=this.w_SERDOC AND MTROWNUM=this.w_ROWRIF AND MTNUMRIF=this.w_MVNUMRIF;
            into cursor _Curs_MOVIMATR
        endif
        if used('_Curs_MOVIMATR')
          select _Curs_MOVIMATR
          locate for 1=1
          do while not(eof())
          this.w_STORNAMAT = True
          this.w_MTCODMAT = iif(_Curs_MOVIMATR.MT_SALDO>0, _Curs_MOVIMATR.MTCODMAT, this.w_MTCODMAT)
            select _Curs_MOVIMATR
            continue
          enddo
          use
        endif
        if not empty(this.w_MTCODMAT)
          this.w_OK = False
          this.oParentObject.w_TESTFORM = .F.
          * --- Verifica se qualche matricola � stata usata
          this.w_MSGMATRI = ah_Msgformat("Impossibile eliminare documento: movimentata matricola %1", alltrim(this.w_MTCODMAT) )
          this.TmpC = this.w_MSGMATRI
        endif
        if this.w_STORNAMAT and this.w_OK
          * --- Storna i saldi su MOVIMATR
          * --- Verifica se la matricola � stata nuovamente dichiarata in produzione
          this.w_MTCODMAT = ""
          this.w_MT__FLAG = " "
          this.w_AM_PRENO = 1
          * --- Select from GSCO_BAD6
          do vq_exec with 'GSCO_BAD6',this,'_Curs_GSCO_BAD6','',.f.,.t.
          if used('_Curs_GSCO_BAD6')
            select _Curs_GSCO_BAD6
            locate for 1=1
            do while not(eof())
            this.w_MTCODMAT = _Curs_GSCO_BAD6.AMCODICE
              select _Curs_GSCO_BAD6
              continue
            enddo
            use
          endif
          if not empty(this.w_MTCODMAT)
            this.w_OK = False
            this.oParentObject.w_TESTFORM = .F.
            * --- Impossibile cancellare Mov. di Magazzino
            this.w_MSGMATRI = ah_Msgformat("Impossibile stornare: matricola %1 dichiarata in produzione", alltrim(this.w_MTCODMAT) )
            this.TmpC = this.w_MSGMATRI
          endif
          * --- Reimposta il Flag Prenotato da Produzione
          this.w_AM_PRENO = 0
          if this.w_SERDOC=this.oParentObject.w_DPRIFSCA
            * --- Read from DIC_COMP
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIC_COMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_COMP_idx,2],.t.,this.DIC_COMP_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" DIC_COMP where ";
                    +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    MTSERIAL = this.oParentObject.w_DPSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_rows>0 or this.TipOpe="ELIMINA"
              this.w_CHECKMATR = .t.
            endif
          endif
          if this.w_CHECKMATR or this.w_SERDOC=this.oParentObject.w_DPRIFCAR
            * --- Write into MATRICOL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MATRICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="AMKEYSAL,AMCODICE"
              do vq_exec with 'GSCO_BAD6',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MATRICOL_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                      +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AM_PRENO =MATRICOL.AM_PRENO+ "+cp_ToStrODBC(1);
                  +i_ccchkf;
                  +" from "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                      +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 set ";
              +"MATRICOL.AM_PRENO =MATRICOL.AM_PRENO+ "+cp_ToStrODBC(1);
                  +Iif(Empty(i_ccchkf),"",",MATRICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="MATRICOL.AMKEYSAL = t2.AMKEYSAL";
                      +" and "+"MATRICOL.AMCODICE = t2.AMCODICE";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set (";
                  +"AM_PRENO";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"MATRICOL.AM_PRENO+"+cp_ToStrODBC(1)+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                      +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set ";
              +"AM_PRENO =MATRICOL.AM_PRENO+ "+cp_ToStrODBC(1);
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".AMKEYSAL = "+i_cQueryTable+".AMKEYSAL";
                      +" and "+i_cTable+".AMCODICE = "+i_cQueryTable+".AMCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AM_PRENO =AM_PRENO+ "+cp_ToStrODBC(1);
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Impossibile aggiornare anagrafica matricole'
              return
            endif
          endif
          * --- Storna i saldi su MOVIMATR del movimento collegato
          this.w_MT__FLAG = "+"
          * --- Write into MOVIMATR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"
            do vq_exec with 'GSCO_BAD7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                    +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                    +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                    +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                    +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
                +i_ccchkf;
                +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                    +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                    +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                    +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                    +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
            +"MOVIMATR.MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
                +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                    +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                    +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
                    +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                    +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
                +"MT_SALDO";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"MOVIMATR.MT_SALDO-"+cp_ToStrODBC(1)+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                    +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                    +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                    +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                    +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
            +"MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                    +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                    +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
                    +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                    +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MT_SALDO =MT_SALDO- "+cp_ToStrODBC(1);
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Impossibile aggiornare dettaglio matricole'
            return
          endif
        endif
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    if this.w_OK
      this.w_NUMRIF = -20
      * --- Aggiorna saldi lotti  prima di eliminare i documenti
      if g_MADV="S" AND (g_PERLOT="S" OR g_PERUBI="S")
        GSMD_BRL (this, this.w_SERDOC , "V" , "-" , ,.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Elimina Documento in Cascata
      GSAR_BED(this,this.w_SERDOC, this.w_NUMRIF)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Lotti/Ubicazioni/Matricole, ripresa da GSMD_BVM
    this.w_DATDOC = cp_CharToDate("  -  -  ")
    this.w_DATREG = cp_CharToDate("  -  -  ")
    this.w_NUMDOC = 0
    this.w_ALFDOC = Space(10)
    this.w_CAUDOC = SPACE(5)
    this.w_DESDOC = SPACE(40)
    this.w_TIPCON = " "
    this.w_GENROWDOC = 0
    this.w_CODCON = SPACE(15)
    vq_exec("..\COLA\EXE\QUERY\GSCO_QDP.VQR", this,"AGGIORNA")
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON;
        from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_CAUDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDDESDOC"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDDESDOC;
        from (i_cTable) where;
            TDTIPDOC = this.w_CAUDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESDOC = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROV = .F.
    if USED("AGGIORNA")
      * --- Azzerro Archivi di Appoggio
      * --- Delete from AGG_LOTT
      i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione AGG_LOTT'
        return
      endif
      * --- Delete from SPL_LOTT
      i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              SPSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione SPL_LOTT'
        return
      endif
      * --- Inserisco Dati in Archivi di Appoggio
      ah_Msg("Fase 1: inserimento dati...",.T.)
      SELECT AGGIORNA
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(CPROWNUM, 0)>0
        this.w_CPROWNUM = CPROWNUM
        this.w_MVNUMRIF = MVNUMRIF
        this.w_CPROWORD = CPROWORD
        this.w_MVCODICE = NVL(MVCODICE," ")
        this.w_MVCODART = NVL(MVCODART, " ")
        this.w_MVDESART = NVL(MVDESART," ")
        this.w_MVUNIMIS = NVL(MVUNIMIS," ")
        this.w_MVQTAMOV = NVL(MVQTAMOV, 0)
        this.w_MVQTAUM1 = NVL(MVQTAUM1, 0)
        this.w_MVCODMAG = NVL(MVCODMAG," ")
        this.w_MVCODMAT = NVL(MVCODMAT," ")
        this.w_MVCODLOT = NVL(MVCODLOT," ")
        this.w_MVCODUBI = NVL(MVCODUBI," ")
        this.w_MVCODUB2 = NVL(MVCODUB2," ")
        this.w_MVFLCASC = NVL(MVFLCASC," ")
        this.w_MVFLRISE = NVL(MVFLRISE," ")
        this.w_MVF2CASC = NVL(MVF2CASC," ")
        this.w_MVF2RISE = NVL(MVF2RISE," ")
        this.w_MVFLUBIC = NVL(FLUBIC, " ")
        this.w_MVFLUBI2 = NVL(FLUBI2, " ")
        this.w_MVARTLOT = NVL(FLLOTT, " ")
        this.w_MVSERRIF = NVL(MVSERRIF, " ")
        this.w_MVROWRIF = NVL(MVROWRIF, 0)
        this.w_MVFLLOTT = IIF((g_PERLOT="S" AND this.w_MVARTLOT$ "SC") OR (g_PERUBI="S" AND this.w_MVFLUBIC="S"), LEFT(ALLTRIM(this.w_MVFLCASC)+IIF(this.w_MVFLRISE="+", "-", IIF(this.w_MVFLRISE="-", "+", " ")), 1), " ")
        this.w_MVF2LOTT = IIF((g_PERLOT="S" AND this.w_MVARTLOT$ "SC") OR (g_PERUBI="S" AND this.w_MVFLUBI2="S"), LEFT(ALLTRIM(this.w_MVF2CASC)+IIF(this.w_MVF2RISE="+", "-", IIF(this.w_MVF2RISE="-", "+", " ")), 1), " ")
        this.w_MVFLELAN = NVL(MVFLELAN," ")
        this.w_MV_SEGNO = NVL(MV_SEGNO," ")
        this.w_MVCODODL = NVL(MVCODODL, " ")
        this.w_MVRIGMAT = NVL(MVRIGMAT, 0)
        this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
        this.w_MVLOTMAG = iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUBI ) , SPACE(5) , this.w_MVCODMAG )
        this.w_MVLOTMAT = Iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUB2 ) , Space(5) , this.w_MVCODMAT )
        if NOT EMPTY(this.w_MVCODICE) AND this.w_MVQTAUM1>0
          * --- Try
          local bErr_054BBE80
          bErr_054BBE80=bTrsErr
          this.Try_054BBE80()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_054BBE80
          * --- End
        endif
        if this.w_OPERAZ="CA"
          * --- Temporaneo per split lotti per movimenti di carico da produzione
          vq_exec("..\COLA\EXE\QUERY\GSCO3BDP.VQR", this,"tmpsplot")
        else
          * --- Temporaneo per split lotti per movimenti di scarico da produzione
          vq_exec("..\COLA\EXE\QUERY\GSCO4BDP.VQR", this,"tmpsplot")
          if reccount("tmpsplot")=0
            this.w_SERODL = this.w_MVCODODL
            this.w_ROWODL = this.w_MVRIGMAT
            this.w_LCODMAG = this.w_MVCODMAG
            this.w_OKRIG = .f.
            this.w_BASE = this.w_MVQTAUM1
            this.w_CONTAREC = 0
            if g_PERLOT="S" AND this.w_MVARTLOT<>"N"
              if !empty(this.w_MVCODODL) and this.w_MVARTLOT<>"C"
                * --- Select from GSCO_BLZ
                do vq_exec with 'GSCO_BLZ',this,'_Curs_GSCO_BLZ','',.f.,.t.
                if used('_Curs_GSCO_BLZ')
                  select _Curs_GSCO_BLZ
                  locate for 1=1
                  do while not(eof())
                  this.w_CODLOT = NVL(_Curs_GSCO_BLZ.MVCODLOT,SPACE(20))
                  if !EMPTY(this.w_CODLOT)
                    this.w_OKRIG = .T.
                  endif
                  if g_PERUBI="S" AND this.w_MVFLUBIC="S"
                    this.w_CODUBI = NVL(_Curs_GSCO_BLZ.MVCODUBI,SPACE(20))
                  endif
                  this.w_QTALOT = NVL(_Curs_GSCO_BLZ.QTALOT,0)
                  this.w_MOVLOT = iif(this.w_BASE>this.w_QTALOT, this.w_QTALOT, this.w_BASE)
                  this.w_BASE = this.w_BASE - this.w_MOVLOT
                  if this.w_MOVLOT > 0
                    this.w_CONTAREC = this.w_CONTAREC+1
                    * --- Inserisco la riga del lotto
                    m.spserial=this.w_SERIAL 
 m.sproword=this.w_CPROWNUM 
 m.cprownum=this.w_CONTAREC 
 m.spcodlot=this.w_CODLOT 
 m.spcodubi=this.w_CODUBI 
 m.spcodub2=SPACE(20) 
 m.spqtamov=this.w_MOVLOT 
 m.spqtaum1=this.w_MVQTAUM1 
 m.spnumrif=this.w_MVNUMRIF 
 m.mtmagsca=this.w_MVCODMAG 
 m.mtmagcar=this.w_MVCODMAT 
 m.mtkeysal=this.w_MVCODART
                    Insert into tmpsplot from memvar
                  endif
                    select _Curs_GSCO_BLZ
                    continue
                  enddo
                  use
                endif
              endif
              if this.w_OKRIG=.F.
                if this.w_MVARTLOT="C"
                  * --- Consumo automatico
                  this.w_CODMAG = this.w_MVCODMAG
                  this.w_CODART = this.w_MVCODART
                  this.w_CONTAREC = 0
                  vq_exec("GSMD1LOT.VQR",this,"LOTTI")
                  Select * from Lotti into cursor Lotti NoFilter
                  * --- Quantit� effettiva su cui eseguire proporzione quantit� importata
                  =WRCURSOR("LOTTI")
                  do while this.w_BASE>0
                    this.w_CODLOT = CERCLOT(this.w_DATREG,this.w_CODART,"LOTTI")
                    if NOT EMPTY(this.w_CODLOT)
                      SELECT LOTTI
                      GO TOP
                      LOCATE FOR this.w_CODART=NVL(CODART,SPACE(20)) AND this.w_CODLOT=NVL(LOTTI.LOCODICE,SPACE(20))
                      if FOUND()
                        * --- Assegno informazioni della riga documento  che variano 
                        this.w_QTALOT = NVL(LOTTI.LOQTAESI,0)
                        this.w_LOTESI = NVL(LOTTI.LOQTAESI,0)
                        this.w_CODLOT = NVL(LOTTI.LOCODICE,SPACE(20))
                        this.w_CODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
                        this.w_CODMAG = NVL(LOTTI.CODMAG,SPACE(5))
                        this.w_LOFLSTAT = NVL(LOTTI.LOFLSTAT,"")
                        if this.w_BASE>this.w_QTALOT
                          this.w_BASE = this.w_BASE - this.w_QTALOT
                          * --- Elimino il lotto per intero
                          SELECT LOTTI
                          DELETE
                        else
                          * --- Consumo in parte la quantit� del lotto quindi creo una riga pari
                          *     alla quantit� del documento di origine
                          this.w_QTARES = this.w_QTALOT - this.w_BASE
                          this.w_QTALOT = this.w_BASE
                          this.w_BASE = 0
                        endif
                      endif
                    else
                      * --- Creo Riga con lotto vuoto
                      this.w_QTALOT = this.w_BASE
                      this.w_BASE = 0
                    endif
                    this.w_OKRIG = .T.
                    this.w_CONTAREC = this.w_CONTAREC+1
                    * --- Inserisco la riga del lotto
                    m.spserial=this.w_SERIAL 
 m.sproword=this.w_CPROWNUM 
 m.cprownum=this.w_CONTAREC 
 m.spcodlot=this.w_CODLOT 
 m.spcodubi=this.w_CODUBI 
 m.spcodub2=SPACE(20) 
 m.spqtamov=this.w_QTALOT 
 m.spqtaum1=this.w_MVQTAUM1 
 m.spnumrif=this.w_MVNUMRIF 
 m.mtmagsca=this.w_MVCODMAG 
 m.mtmagcar=this.w_MVCODMAT 
 m.mtkeysal=this.w_MVCODART
                    Insert into tmpsplot from memvar
                  enddo
                  Use in Select("Lotti")
                endif
              endif
            else
              if g_PERUBI="S" AND this.w_MVFLUBIC="S"
                this.w_BASE = this.w_MVQTAUM1
                * --- Select from DOC_DETT
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select MVCODLOT,MVCODUBI,MVQTAUM1  from "+i_cTable+" DOC_DETT ";
                      +" where MVCODODL="+cp_ToStrODBC(this.w_SERODL)+" and MVRIGMAT="+cp_ToStrODBC(this.w_ROWODL)+"";
                       ,"_Curs_DOC_DETT")
                else
                  select MVCODLOT,MVCODUBI,MVQTAUM1 from (i_cTable);
                   where MVCODODL=this.w_SERODL and MVRIGMAT=this.w_ROWODL;
                    into cursor _Curs_DOC_DETT
                endif
                if used('_Curs_DOC_DETT')
                  select _Curs_DOC_DETT
                  locate for 1=1
                  do while not(eof())
                  this.w_OKRIG = .T.
                  this.w_CODUBI = NVL(_Curs_DOC_DETT.MVCODUBI,SPACE(20))
                  this.w_QTALOT = NVL(_Curs_DOC_DETT.MVQTAUM1,0)
                  this.w_MOVLOT = iif(this.w_BASE>this.w_QTALOT, this.w_QTALOT, this.w_BASE)
                  this.w_BASE = this.w_BASE - this.w_MOVLOT
                  if this.w_MOVLOT > 0
                    * --- Inserisco la riga del lotto
                    this.w_CONTAREC = this.w_CONTAREC+1
                    * --- Inserisco la riga del lotto
                    m.spserial=this.w_SERIAL 
 m.sproword=this.w_CPROWNUM 
 m.cprownum=this.w_CONTAREC 
 m.spcodlot=this.w_CODLOT 
 m.spcodubi=this.w_CODUBI 
 m.spcodub2=SPACE(20) 
 m.spqtamov=this.w_QTALOT 
 m.spqtaum1=this.w_MVQTAUM1 
 m.spnumrif=this.w_MVNUMRIF 
 m.mtmagsca=this.w_MVCODMAG 
 m.mtmagcar=this.w_MVCODMAT 
 m.mtkeysal=this.w_MVCODART
                    Insert into tmpsplot from memvar
                  endif
                    select _Curs_DOC_DETT
                    continue
                  enddo
                  use
                endif
              endif
            endif
            if this.w_BASE>0
              * --- Incrementa numero errori
              this.w_LNumErr = this.w_LNumErr + 1
              this.w_LOggErr = this.oParentObject.w_DPCODODL
              this.w_LErrore = ah_Msgformat("Riferimenti lotti/ubicazioni mancanti, eseguire aggiornamento differito")
              this.w_LTesMes = ""
              * --- Scrive LOG
              INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
              (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LTesMes)
              * --- Inserisco la riga del lotto
              m.spserial=this.w_SERIAL 
 m.sproword=this.w_CPROWNUM 
 m.cprownum=this.w_CONTAREC 
 m.spcodlot=SPACE(20) 
 m.spcodubi=SPACE(20) 
 m.spcodub2=SPACE(20) 
 m.spqtamov=this.w_BASE 
 m.spqtaum1=this.w_BASE 
 m.spnumrif=this.w_MVNUMRIF 
 m.mtmagsca=this.w_MVCODMAG 
 m.mtmagcar=this.w_MVCODMAT 
 m.mtkeysal=this.w_MVCODART
              Insert into tmpsplot from memvar
            endif
          endif
        endif
        if used("spllott") and reccount("spllott")>0
          * --- La count serve ad aggiornare il cursore che altrimenti perdeva l'ultimo record
           Select tmpsplot 
 count to reclot 
 scan 
 scatter memvar 
 Select spllott 
 append blank 
 gather memvar 
 endscan
        else
          * --- Il cursore potrebbe esistere ed essere vuoto, per sicurezza verifico se lo devo droppare
          if used("spllott")
            select spllott 
 use
          endif
          * --- La count serve ad aggiornare il cursore che altrimenti perdeva l'ultimo record
          Select tmpsplot 
 count to reclot
          select * from tmpsplot into cursor spllott order by 1,2
          cur=wrcursor("spllott")
        endif
        * --- Rilascio il temporaneo di appoggio
        if used("tmpsplot")
          select tmpsplot 
 use
        endif
        SELECT AGGIORNA
        ENDSCAN
      endif
      if this.w_TROV=.T.
        if (g_PERLOT="S" OR g_PERUBI="S")
          * --- Inizializzo la variabile di riferimento alla riga ogni volta che cambio documento
          this.w_OLDROW = 0
          * --- Try
          local bErr_05479C18
          bErr_05479C18=bTrsErr
          this.Try_05479C18()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg(i_ErrMsg,,"")
          endif
          bTrsErr=bTrsErr or bErr_05479C18
          * --- End
          * --- Chiude i Cursori che non servono piu'
          USE IN SELECT("AGGIORNA")
        else
          ah_Msg("Fase 2: aggiornamento matricole...",.T.)
          * --- Aggiungo righe matricole alle righe documento aggiornate
          * --- Try
          local bErr_0547DA88
          bErr_0547DA88=bTrsErr
          this.Try_0547DA88()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg(i_ErrMsg,,"")
          endif
          bTrsErr=bTrsErr or bErr_0547DA88
          * --- End
        endif
        * --- Ripulisce le tabelle temporanee
        * --- Delete from SPL_LOTT
        i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                SPSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione SPL_LOTT'
          return
        endif
        * --- Delete from AGG_LOTT
        i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione AGG_LOTT'
          return
        endif
      endif
      USE IN SELECT("AGGIORNA")
    endif
    if this.w_CONFERMA
      this.GSCO_ADP.NotifyEvent("Esegui")
    endif
  endproc
  proc Try_054BBE80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into AGG_LOTT
    i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AGG_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVCODICE"+",MVDESART"+",MVUNIMIS"+",MVQTAMOV"+",MVQTAUM1"+",MVCODMAG"+",MVCODMAT"+",MVCODLOT"+",MVCODUBI"+",MVCODUB2"+",MVFLLOTT"+",MVF2LOTT"+",MVFLUBIC"+",MVFLUBI2"+",MVARTLOT"+",MVCODART"+",MVSERRIF"+",MVROWRIF"+",MVFLCASC"+",MVFLRISE"+",MVF2CASC"+",MVF2RISE"+",MVCODCOM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'AGG_LOTT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'AGG_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'AGG_LOTT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'AGG_LOTT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'AGG_LOTT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'AGG_LOTT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'AGG_LOTT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'AGG_LOTT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'AGG_LOTT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'AGG_LOTT','MVCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'AGG_LOTT','MVCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AGG_LOTT','MVCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'AGG_LOTT','MVCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'AGG_LOTT','MVCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'AGG_LOTT','MVFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'AGG_LOTT','MVF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLUBIC),'AGG_LOTT','MVFLUBIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLUBI2),'AGG_LOTT','MVFLUBI2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVARTLOT),'AGG_LOTT','MVARTLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'AGG_LOTT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'AGG_LOTT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'AGG_LOTT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'AGG_LOTT','MVFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'AGG_LOTT','MVFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'AGG_LOTT','MVF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'AGG_LOTT','MVF2RISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'AGG_LOTT','MVCODCOM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_CPROWORD,'MVCODICE',this.w_MVCODICE,'MVDESART',this.w_MVDESART,'MVUNIMIS',this.w_MVUNIMIS,'MVQTAMOV',this.w_MVQTAMOV,'MVQTAUM1',this.w_MVQTAUM1,'MVCODMAG',this.w_MVCODMAG,'MVCODMAT',this.w_MVCODMAT,'MVCODLOT',this.w_MVCODLOT)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVCODICE,MVDESART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODMAG,MVCODMAT,MVCODLOT,MVCODUBI,MVCODUB2,MVFLLOTT,MVF2LOTT,MVFLUBIC,MVFLUBI2,MVARTLOT,MVCODART,MVSERRIF,MVROWRIF,MVFLCASC,MVFLRISE,MVF2CASC,MVF2RISE,MVCODCOM &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MVCODICE;
           ,this.w_MVDESART;
           ,this.w_MVUNIMIS;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVCODMAG;
           ,this.w_MVCODMAT;
           ,this.w_MVCODLOT;
           ,this.w_MVCODUBI;
           ,this.w_MVCODUB2;
           ,this.w_MVFLLOTT;
           ,this.w_MVF2LOTT;
           ,this.w_MVFLUBIC;
           ,this.w_MVFLUBI2;
           ,this.w_MVARTLOT;
           ,this.w_MVCODART;
           ,this.w_MVSERRIF;
           ,this.w_MVROWRIF;
           ,this.w_MVFLCASC;
           ,this.w_MVFLRISE;
           ,this.w_MVF2CASC;
           ,this.w_MVF2RISE;
           ,this.w_MVCODCOM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento AGG_LOTT'
      return
    endif
    this.w_TROV = .T.
    return
  proc Try_05479C18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Fase 2: aggiornamento righe documento...",.T.)
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0547DA88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT AGGIORNA 
 go top 
 scan
    this.w_CPROWNUM = CPROWNUM
    this.w_MVCODMAG = NVL(MVCODMAG, " ")
    this.w_MVNUMRIF = -20
    this.Page_10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    endscan
    * --- commit
    cp_EndTrs(.t.)
    USE IN SELECT("AGGIORNA")
    return


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Documenti di Evasione
    this.CONTA = 0
    select spllott 
 scan
    this.CONTA = this.CONTA+1
    this.SERDOC = spllott.SPSERIAL
    this.RIGDOC = spllott.SPROWORD
    this.NUMROW = spllott.CPROWNUM+this.conta
    this.LOTTO = spllott.SPCODLOT
    this.UBICA = spllott.SPCODUBI
    this.UBICA2 = spllott.SPCODUB2
    this.QTA1 = spllott.SPQTAMOV
    this.QTA2 = spllott.SPQTAUM1
    this.RIFRIGA = spllott.SPNUMRIF
    * --- Try
    local bErr_0553DFB8
    bErr_0553DFB8=bTrsErr
    this.Try_0553DFB8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0553DFB8
    * --- End
    endscan
    * --- Carica Temporaneo Evasioni
    vq_exec("..\MADV\EXE\QUERY\GSMD1QDO.VQR", this,"AGGIORNA")
    * --- Rilascio il temporaneo per lo split dei lotti
    if used("spllott")
      select spllott 
 use
    endif
    if USED("AGGIORNA")
      SELECT AGGIORNA
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(CPROWNUM, 0)>0 AND NVL(MVQTAMOV, 0)>0
        * --- Esegue Update su riga gi� presente.
        this.w_CPROWNUM = CPROWNUM
        if this.w_OLDROW<>this.w_CPROWNUM
          * --- Informazioni Mantenute dalla riga originaria
          this.w_MVDESART = NVL(MVDESART, " ")
          this.w_MVCODART = NVL(MVCODART, " ")
          this.w_MVCODICE = NVL(MVCODICE, " ")
          this.w_MVCODMAG = NVL(MVCODMAG, " ")
          this.w_MVCODMAT = NVL(MVCODMAT, " ")
          this.w_MVFLLOTT = NVL(MVFLLOTT, " ")
          this.w_MVF2LOTT = NVL(MVF2LOTT, " ")
          this.w_MVARTLOT = NVL(MVARTLOT, " ")
          this.w_MVFLUBIC = NVL(MVFLUBIC, " ")
          this.w_MVFLUBI2 = NVL(MVFLUBI2, " ")
          this.w_MVCODLOT = SPACE(20)
          this.w_MVCODUBI = SPACE(20)
          this.w_MVCODUB2 = SPACE(20)
          this.w_MVLOTMAG = SPACE(5)
          this.w_MVLOTMAT = SPACE(5)
          this.w_MVCODMAG = NVL(MVCODMAG,SPACE(5))
          this.w_MVCODMAT = NVL(MVCODMAT,SPACE(5))
          this.w_MVFLCASC = NVL(MVFLCASC, " ")
          this.w_MVFLRISE = NVL(MVFLRISE, " ")
          this.w_MVF2CASC = NVL(MVF2CASC, " ")
          this.w_MVF2RISE = NVL(MVF2RISE, " ")
          this.w_MVSERRIF = NVL(MVSERRIF,SPACE(10))
          this.w_MVROWRIF = NVL(MVROWRIF,0)
          this.w_MVTIPRIG = NVL(MVTIPRIG," ")
          this.w_MVCAUMAG = NVL(MVCAUMAG,SPACE(5))
          this.w_MVCATCON = NVL(MVCATCON,SPACE(5))
          this.w_MVCONTRA = NVL(MVCONTRA,SPACE(15))
          this.w_MVDESSUP = NVL(MVDESSUP,SPACE(10))
          this.w_MVCODLIS = NVL(MVCODLIS,SPACE(5))
          this.w_MVCODCLA = NVL(MVCODCLA,SPACE(5))
          this.w_MVPREZZO = NVL(MVPREZZO,0)
          this.w_MVSCONT1 = NVL(MVSCONT1,0)
          this.w_MVSCONT2 = NVL(MVSCONT2,0)
          this.w_MVSCONT3 = NVL(MVSCONT3,0)
          this.w_MVSCONT4 = NVL(MVSCONT4,0)
          this.w_MVFLOMAG = NVL(MVFLOMAG," ")
          this.w_MVIMPACC = 0
          this.w_MVIMPSCO = 0
          this.w_MVPESNET = NVL(MVPESNET,0)
          this.w_MVFLRAGG = NVL(MVFLRAGG," ")
          this.w_MVDATEVA = CP_TODATE(MVDATEVA)
          this.w_MVCODIVA = NVL(MVCODIVA,SPACE(5))
          this.w_MVCONIND = NVL(MVCONIND,SPACE(15))
          this.w_MVNOMENC = NVL(MVNOMENC,SPACE(8))
          this.w_MVFLTRAS = NVL(MVFLTRAS,SPACE(1))
          this.w_MVMOLSUP = NVL(MVMOLSUP,0)
          this.w_MVCAUCOL = NVL(MVCAUCOL,SPACE(5))
          this.w_MVCODATT = NVL(MVCODATT,SPACE(15))
          this.w_MVCODCEN = NVL(MVCODCEN,SPACE(15))
          this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
          this.w_MVCODODL = NVL(MVCODODL,SPACE(15))
          this.w_MVF2ORDI = NVL(MVF2ORDI," ")
          this.w_MVF2IMPE = NVL(MVF2IMPE," ")
          this.w_MVFLORDI = NVL(MVFLORDI," ")
          this.w_MVFLELGM = NVL(MVFLELGM," ")
          this.w_MVFLIMPE = NVL(MVFLIMPE," ")
          this.w_MVKEYSAL = NVL(MVKEYSAL,SPACE(20))
          this.w_MVTIPATT = NVL(MVTIPATT," ")
          this.w_MVRIGMAT = NVL(MVRIGMAT,0)
          this.w_MVVOCCEN = NVL(MVVOCCEN,SPACE(15))
          this.w_MVIMPNAZ = 0
          this.w_MVVALMAG = 0
          this.w_MVVALRIG = 0
          this.w_MVINICOM = CP_TODATE(MVINICOM)
          this.w_MVFINCOM = CP_TODATE(MVFINCOM)
          this.w_MVUNIMIS = NVL(MVUNIMIS,"   ")
        endif
        this.w_MVQTAUM1 = NVL(MVQTAMOV,0)
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAUNIMIS,CAMOLTIP,CAOPERAT"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(AGGIORNA.MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAUNIMIS,CAMOLTIP,CAOPERAT;
            from (i_cTable) where;
                CACODICE = AGGIORNA.MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.w_MOLT3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          this.w_OP3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARFLUSEP"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(AGGIORNA.MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARFLUSEP;
            from (i_cTable) where;
                ARCODART = AGGIORNA.MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.w_MOLT = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.w_OP = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_FLUSEPD = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT AGGIORNA
        if this.w_MVUNIMIS<>this.w_UM1
          msg=" "
          this.w_MVQTAMOV = CALQTA(this.w_MVQTAUM1, this.w_MVUNIMIS,iif(this.w_MVUNIMIS=this.w_UM2,this.w_UM2,space(3)),IIF(this.w_OP="/","*","/"), this.w_MOLT, this.w_FLUSEPD, "N", this.w_MODUM2D, @msg, this.w_UM3, IIF(this.w_OP3="/","*","/"), this.w_MOLT3)
          if not empty(msg)
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=msg
          endif
        else
          this.w_MVQTAMOV = NVL(this.w_MVQTAUM1, 0)
        endif
        if g_PERLOT="S" AND this.w_MVARTLOT$ "SC" AND (this.w_MVFLLOTT $ "+-" OR this.w_MVF2LOTT $ "+-")
          this.w_MVCODLOT = NVL(MVCODLOT, SPACE(20))
        endif
        if g_PERUBI="S" AND NOT EMPTY(this.w_MVCODMAG) AND this.w_MVFLUBIC="S" AND this.w_MVFLLOTT $ "+-"
          this.w_MVCODUBI = NVL(MVCODUBI, SPACE(20))
        endif
        if NOT EMPTY(this.w_MVCODMAT) AND g_PERUBI="S" AND this.w_MVFLUBI2="S" AND this.w_MVF2LOTT $ "+-"
          this.w_MVCODUB2 = NVL(MVCODUB2, SPACE(20))
        endif
        this.w_MVLOTMAG = iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUBI ) , SPACE(5) , this.w_MVCODMAG )
        this.w_MVLOTMAT = Iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUB2 ) , Space(5) , this.w_MVCODMAT )
        this.w_VALUNI = cp_ROUND(this.w_MVPREZZO * (1+this.w_MVSCONT1/100)*(1+this.w_MVSCONT2/100)*(1+this.w_MVSCONT3/100)*(1+this.w_MVSCONT4/100),5)
        this.w_MVVALRIG = cp_ROUND(this.w_MVQTAMOV*this.w_VALUNI, 5)
        this.w_ROWMAT = SPROWNUM
        * --- Riferimento di riga dello split del lotto passato come parametro alla 
        *     query per la determinazione delle matricole da inserire.
        *     Nel caso di righe aggiuntive la variabile w_ROWNUM viene passato come 
        *     parametro alla query senza filtro poich� sar� effettivamente l'espressione del 
        *     campo MTROWNUM da inserire in MOVIMATR
        this.w_MVNUMRIF = -20
        if this.w_OLDROW<>this.w_CPROWNUM
          * --- Modifico riga gi� esistente
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
            +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
            +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
            +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
            +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
            +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
            +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
            +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
            +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVCODLOT = this.w_MVCODLOT;
                ,MVCODUBI = this.w_MVCODUBI;
                ,MVCODUB2 = this.w_MVCODUB2;
                ,MVQTAUM1 = this.w_MVQTAUM1;
                ,MVQTASAL = this.w_MVQTAUM1;
                ,MVQTAMOV = this.w_MVQTAMOV;
                ,MVVALRIG = this.w_MVVALRIG;
                ,MVLOTMAG = this.w_MVLOTMAG;
                ,MVLOTMAT = this.w_MVLOTMAT;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_CPROWNUM;
                and MVNUMRIF = this.w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorno Righe Piano Produzione collegato
          Gsmd_bap(this,"A")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Determino il Max Numero di riga del documento
          this.w_ROWNUM = this.w_CPROWNUM
          this.Page_10()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          vq_exec("..\MADV\EXE\QUERY\GSMD2QDO.VQR", this,"MAXRIG")
          SELECT MAXRIG
          this.w_ROWNUM = NVL(MAXRIG.NUMRIG,0)
          this.w_ROWORD = NVL(MAXRIG.RIFRIG,0)
        else
          * --- Inserisco Righe nuove
          this.w_MVCODODL = IIF(EMPTY(this.w_MVCODODL),SPACE(15),this.w_MVCODODL)
          this.w_ROWNUM = this.w_ROWNUM + 1
          this.w_ROWORD = this.w_ROWORD + 10
          * --- Insert into DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCONTRO"+",MVCAUMAG"+",MVCODCLA"+",MVCONTRA"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVFLOMAG"+",MVCODIVA"+",MVCONIND"+",MVVALRIG"+",MVIMPACC"+",MVIMPSCO"+",MVVALMAG"+",MVIMPNAZ"+",MVPERPRO"+",MVIMPPRO"+",MVPESNET"+",MVNOMENC"+",MVMOLSUP"+",MVNUMCOL"+",MVFLTRAS"+",MVFLRAGG"+",MVSERRIF"+",MVROWRIF"+",MVFLARIF"+",MVQTASAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_DETT','MVSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DOC_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DOC_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLRAGG),'DOC_DETT','MVFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'CPROWNUM',this.w_ROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_ROWORD,'MVTIPRIG',this.w_MVTIPRIG,'MVCODICE',this.w_MVCODICE,'MVCODART',this.w_MVCODART,'MVDESART',this.w_MVDESART,'MVDESSUP',this.w_MVDESSUP,'MVUNIMIS',this.w_MVUNIMIS,'MVCATCON',this.w_MVCATCON,'MVCONTRO',SPACE(15))
            insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCONTRO,MVCAUMAG,MVCODCLA,MVCONTRA,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVFLOMAG,MVCODIVA,MVCONIND,MVVALRIG,MVIMPACC,MVIMPSCO,MVVALMAG,MVIMPNAZ,MVPERPRO,MVIMPPRO,MVPESNET,MVNOMENC,MVMOLSUP,MVNUMCOL,MVFLTRAS,MVFLRAGG,MVSERRIF,MVROWRIF,MVFLARIF,MVQTASAL &i_ccchkf. );
               values (;
                 this.w_SERIAL;
                 ,this.w_ROWNUM;
                 ,this.w_MVNUMRIF;
                 ,this.w_ROWORD;
                 ,this.w_MVTIPRIG;
                 ,this.w_MVCODICE;
                 ,this.w_MVCODART;
                 ,this.w_MVDESART;
                 ,this.w_MVDESSUP;
                 ,this.w_MVUNIMIS;
                 ,this.w_MVCATCON;
                 ,SPACE(15);
                 ,this.w_MVCAUMAG;
                 ,this.w_MVCODCLA;
                 ,this.w_MVCONTRA;
                 ,this.w_MVCODLIS;
                 ,this.w_MVQTAMOV;
                 ,this.w_MVQTAUM1;
                 ,this.w_MVPREZZO;
                 ,this.w_MVSCONT1;
                 ,this.w_MVSCONT2;
                 ,this.w_MVSCONT3;
                 ,this.w_MVSCONT4;
                 ,this.w_MVFLOMAG;
                 ,this.w_MVCODIVA;
                 ,this.w_MVCONIND;
                 ,this.w_MVVALRIG;
                 ,this.w_MVIMPACC;
                 ,this.w_MVIMPSCO;
                 ,this.w_MVVALMAG;
                 ,this.w_MVIMPNAZ;
                 ,0;
                 ,0;
                 ,this.w_MVPESNET;
                 ,this.w_MVNOMENC;
                 ,this.w_MVMOLSUP;
                 ,0;
                 ,this.w_MVFLTRAS;
                 ,this.w_MVFLRAGG;
                 ,SPACE(10);
                 ,0;
                 ," ";
                 ,this.w_MVQTAUM1;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Errore in inserimento DOC_DETT'
            return
          endif
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
            +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
            +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
            +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
            +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
            +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
            +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
            +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCOL),'DOC_DETT','MVCAUCOL');
            +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
            +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
            +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
            +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
            +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
            +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
            +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'DOC_DETT','MVCODMAT');
            +",MVCODODL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODODL),'DOC_DETT','MVCODODL');
            +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
            +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
            +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
            +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'DOC_DETT','MVF2CASC');
            +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2IMPE),'DOC_DETT','MVF2IMPE');
            +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
            +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2ORDI),'DOC_DETT','MVF2ORDI');
            +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'DOC_DETT','MVF2RISE');
            +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFINCOM),'DOC_DETT','MVFINCOM');
            +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'DOC_DETT','MVFLCASC');
            +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELGM),'DOC_DETT','MVFLELGM');
            +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLIMPE),'DOC_DETT','MVFLIMPE');
            +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
            +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORDI),'DOC_DETT','MVFLORDI');
            +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'DOC_DETT','MVFLRISE');
            +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVINICOM),'DOC_DETT','MVINICOM');
            +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
            +",MVRIGMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIGMAT),'DOC_DETT','MVRIGMAT');
            +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
            +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
            +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
            +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
            +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
            +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVFLARIF = " ";
                ,MVFLRIPA = " ";
                ,MVFLEVAS = " ";
                ,MVFLERIF = " ";
                ,MVQTAEVA = 0;
                ,MVQTAIMP = 0;
                ,MVQTAEV1 = 0;
                ,MVIMPEVA = 0;
                ,MVQTAIM1 = 0;
                ,MVCAUCOL = this.w_MVCAUCOL;
                ,MVCAUMAG = this.w_MVCAUMAG;
                ,MVCODATT = this.w_MVCODATT;
                ,MVCODCEN = this.w_MVCODCEN;
                ,MVCODCOM = this.w_MVCODCOM;
                ,MVCODLOT = this.w_MVCODLOT;
                ,MVCODMAG = this.w_MVCODMAG;
                ,MVCODMAT = this.w_MVCODMAT;
                ,MVCODODL = this.w_MVCODODL;
                ,MVCODUB2 = this.w_MVCODUB2;
                ,MVCODUBI = this.w_MVCODUBI;
                ,MVDATEVA = this.w_MVDATEVA;
                ,MVF2CASC = this.w_MVF2CASC;
                ,MVF2IMPE = this.w_MVF2IMPE;
                ,MVF2LOTT = this.w_MVF2LOTT;
                ,MVF2ORDI = this.w_MVF2ORDI;
                ,MVF2RISE = this.w_MVF2RISE;
                ,MVFINCOM = this.w_MVFINCOM;
                ,MVFLCASC = this.w_MVFLCASC;
                ,MVFLELGM = this.w_MVFLELGM;
                ,MVFLIMPE = this.w_MVFLIMPE;
                ,MVFLLOTT = this.w_MVFLLOTT;
                ,MVFLORDI = this.w_MVFLORDI;
                ,MVFLRISE = this.w_MVFLRISE;
                ,MVINICOM = this.w_MVINICOM;
                ,MVKEYSAL = this.w_MVKEYSAL;
                ,MVRIGMAT = this.w_MVRIGMAT;
                ,MVTIPATT = this.w_MVTIPATT;
                ,MVVOCCEN = this.w_MVVOCCEN;
                ,MV_SEGNO = this.w_MV_SEGNO;
                ,MVFLELAN = this.w_MVFLELAN;
                ,MVLOTMAG = this.w_MVLOTMAG;
                ,MVLOTMAT = this.w_MVLOTMAT;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;
                and MVNUMRIF = this.w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura DOC_DETT'
            return
          endif
          * --- Aggiorno il riferimento sulle Dichiarazioni matricole
          if empty(nvl(this.w_MVCODUBI,space(10))) or empty(nvl(this.w_MVCODLOT,space(10)))
            if empty(nvl(this.w_MVCODUBI,space(10)))
              if this.w_OPERAZ="CA"
                * --- Write into DIC_MATR
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DIC_MATR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MATR','MTROWDIS');
                      +i_ccchkf ;
                  +" where ";
                      +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                      +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                         )
                else
                  update (i_cTable) set;
                      MTROWDIS = this.w_ROWNUM;
                      &i_ccchkf. ;
                   where;
                      MTRIFDIS = this.w_SERIAL;
                      and MTROWDIS = this.w_CPROWNUM;
                      and MTCODLOT = this.w_MVCODLOT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Write into DIC_MCOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MCOM','MTROWDIS');
                      +i_ccchkf ;
                  +" where ";
                      +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                      +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                         )
                else
                  update (i_cTable) set;
                      MTROWDIS = this.w_ROWNUM;
                      &i_ccchkf. ;
                   where;
                      MTRIFDIS = this.w_SERIAL;
                      and MTROWDIS = this.w_CPROWNUM;
                      and MTCODLOT = this.w_MVCODLOT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              if empty(nvl(this.w_MVCODLOT,space(10)))
                if this.w_OPERAZ="CA"
                  * --- Write into DIC_MATR
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DIC_MATR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MATR','MTROWDIS');
                        +i_ccchkf ;
                    +" where ";
                        +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                        +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                        +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                           )
                  else
                    update (i_cTable) set;
                        MTROWDIS = this.w_ROWNUM;
                        &i_ccchkf. ;
                     where;
                        MTRIFDIS = this.w_SERIAL;
                        and MTROWDIS = this.w_CPROWNUM;
                        and MTCODUBI = this.w_MVCODUBI;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  * --- Write into DIC_MCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MCOM','MTROWDIS');
                        +i_ccchkf ;
                    +" where ";
                        +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                        +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                        +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                           )
                  else
                    update (i_cTable) set;
                        MTROWDIS = this.w_ROWNUM;
                        &i_ccchkf. ;
                     where;
                        MTRIFDIS = this.w_SERIAL;
                        and MTROWDIS = this.w_CPROWNUM;
                        and MTCODUBI = this.w_MVCODUBI;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                * --- Non devo aggiornare niente
              endif
            endif
          else
            if this.w_OPERAZ="CA"
              * --- Write into DIC_MATR
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIC_MATR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MATR','MTROWDIS');
                    +i_ccchkf ;
                +" where ";
                    +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                    +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                    +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                       )
              else
                update (i_cTable) set;
                    MTROWDIS = this.w_ROWNUM;
                    &i_ccchkf. ;
                 where;
                    MTRIFDIS = this.w_SERIAL;
                    and MTROWDIS = this.w_CPROWNUM;
                    and MTCODLOT = this.w_MVCODLOT;
                    and MTCODUBI = this.w_MVCODUBI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Write into DIC_MCOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MCOM','MTROWDIS');
                    +i_ccchkf ;
                +" where ";
                    +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                    +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                    +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                       )
              else
                update (i_cTable) set;
                    MTROWDIS = this.w_ROWNUM;
                    &i_ccchkf. ;
                 where;
                    MTRIFDIS = this.w_SERIAL;
                    and MTROWDIS = this.w_CPROWNUM;
                    and MTCODLOT = this.w_MVCODLOT;
                    and MTCODUBI = this.w_MVCODUBI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          * --- Inserisce nuova riga nel piano produzione
          Gsmd_bap(this,"B")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_10()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_OLDROW = this.w_CPROWNUM
        SELECT AGGIORNA
        ENDSCAN 
        * --- Rieseguo ricalcoli Totali documenti ed eventuali Ripartizioni (Sconti,Spese Accessorie)
        gsar_brd(this,this.w_SERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc
  proc Try_0553DFB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SPL_LOTT
    i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SPL_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SPSERIAL"+",SPROWORD"+",CPROWNUM"+",SPCODLOT"+",SPCODUBI"+",SPCODUB2"+",SPQTAMOV"+",SPQTAUM1"+",SPNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.serdoc),'SPL_LOTT','SPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.rigdoc),'SPL_LOTT','SPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.numrow),'SPL_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.lotto),'SPL_LOTT','SPCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.ubica),'SPL_LOTT','SPCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.ubica2),'SPL_LOTT','SPCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.qta1),'SPL_LOTT','SPQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.qta2),'SPL_LOTT','SPQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.rifriga),'SPL_LOTT','SPNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SPSERIAL',this.serdoc,'SPROWORD',this.rigdoc,'CPROWNUM',this.numrow,'SPCODLOT',this.lotto,'SPCODUBI',this.ubica,'SPCODUB2',this.ubica2,'SPQTAMOV',this.qta1,'SPQTAUM1',this.qta2,'SPNUMRIF',this.rifriga)
      insert into (i_cTable) (SPSERIAL,SPROWORD,CPROWNUM,SPCODLOT,SPCODUBI,SPCODUB2,SPQTAMOV,SPQTAUM1,SPNUMRIF &i_ccchkf. );
         values (;
           this.serdoc;
           ,this.rigdoc;
           ,this.numrow;
           ,this.lotto;
           ,this.ubica;
           ,this.ubica2;
           ,this.qta1;
           ,this.qta2;
           ,this.rifriga;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SPL_LOTT'
      return
    endif
    return


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo Matricole nel documento selezionato
    if (g_PERLOT="S" OR g_PERUBI="S")
      if this.w_OPERAZ="CA"
        * --- Create temporary table DELTA
        i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCO_QAM.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.DELTA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table DELTA
        i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCO3QAM.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.DELTA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    else
      if this.w_OPERAZ="CA"
        * --- Create temporary table DELTA
        i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCO1QAM.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.DELTA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table DELTA
        i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCO2QAM.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.DELTA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    * --- Insert into MOVIMATR
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.DELTA_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOVIMATR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento matricole'
      return
    endif
    if Inlist(this.GSCO_ADP.cFunction, "Load", "Edit")
      * --- Write into MATRICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MATRICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="AMKEYSAL,AMCODICE"
        do vq_exec with 'GSCO4QAM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MATRICOL_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
            +i_ccchkf;
            +" from "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 set ";
        +"MATRICOL.AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
            +Iif(Empty(i_ccchkf),"",",MATRICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MATRICOL.AMKEYSAL = t2.AMKEYSAL";
                +" and "+"MATRICOL.AMCODICE = t2.AMCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set (";
            +"AM_PRENO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set ";
        +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".AMKEYSAL = "+i_cQueryTable+".AMKEYSAL";
                +" and "+i_cTable+".AMCODICE = "+i_cQueryTable+".AMCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_OPERAZ<>"CA"
      * --- Aggiorna il saldo Movimenti Matricole Matricole (blocco sul semaforo)
      * --- Write into MOVIMATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"
        do vq_exec with 'GSCO5QAM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO =MOVIMATR.MT_SALDO+ "+cp_ToStrODBC(1);
            +i_ccchkf;
            +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
        +"MOVIMATR.MT_SALDO =MOVIMATR.MT_SALDO+ "+cp_ToStrODBC(1);
            +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
            +"MT_SALDO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"MOVIMATR.MT_SALDO+"+cp_ToStrODBC(1)+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
        +"MT_SALDO =MOVIMATR.MT_SALDO+ "+cp_ToStrODBC(1);
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
                +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO =MT_SALDO+ "+cp_ToStrODBC(1);
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Pegging
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLCOMM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_PECODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLCOMM;
        from (i_cTable) where;
            ARCODART = this.w_PECODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PEFLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTCOMME"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_DPCODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTCOMME;
        from (i_cTable) where;
            OLCODODL = this.oParentObject.w_DPCODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Sposta gli abbinamenti dagli ODL al Magazzino (MRP)
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
    pegging=.F.
    * --- Select from GSDB2BAM
    do vq_exec with 'GSDB2BAM',this,'_Curs_GSDB2BAM','',.f.,.t.
    if used('_Curs_GSDB2BAM')
      select _Curs_GSDB2BAM
      locate for 1=1
      do while not(eof())
      pegging=.T.
      if this.w_PEQTAUM1>0
        this.w_PESERIAL = _Curs_GSDB2BAM.PESERIAL
        this.w_PEQTAABB = _Curs_GSDB2BAM.PEQTAABB
        this.w_PECODCOM = _Curs_GSDB2BAM.PECODCOM
        this.w_PECODRIC = _Curs_GSDB2BAM.PECODRIC
        if this.w_PEQTAUM1 >= this.w_PEQTAABB
          * --- Porta tutto l'abbinamento a Magazzino
          if this.w_PGCODODL=" Magazz"
            * --- Storna Dichiarazione
            this.w_PETIPRIF = iif(empty(nvl(_Curs_GSDB2BAM.PERIFODL,"")), "D", "O")
          else
            this.w_PETIPRIF = "M"
          endif
          if empty(nvl(_Curs_GSDB2BAM.PERIFODL," ")) and empty(nvl(_Curs_GSDB2BAM.PESERORD," ")) and empty(nvl(_Curs_GSDB2BAM.PERIFTRS," "))
            * --- Elimino la riga
            * --- Delete from PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                     )
            else
              delete from (i_cTable) where;
                    PESERIAL = this.w_PESERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            this.w_PEQTAUM1 = this.w_PEQTAUM1 - this.w_PEQTAABB
          else
            if this.oParentObject.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S"
              do case
                case this.w_PETIPRIF="M"
                  do case
                    case _Curs_GSDB2BAM.PETIPRIF="M"
                      this.w_PERIFTRS = space(20)
                    case _Curs_GSDB2BAM.PETIPRIF="D"
                      this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PESERORD)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
                    case _Curs_GSDB2BAM.PETIPRIF="O"
                      this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PERIFODL)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
                  endcase
                  * --- Write into PEG_SELI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                    +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                    +",PERIFODL ="+cp_NullLink(cp_ToStrODBC(space(15)),'PEG_SELI','PERIFODL');
                    +",PESERORD ="+cp_NullLink(cp_ToStrODBC(space(10)),'PEG_SELI','PESERORD');
                    +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PERIGORD');
                    +",PERIFTRS ="+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                        +i_ccchkf ;
                    +" where ";
                        +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                           )
                  else
                    update (i_cTable) set;
                        PESERODL = this.w_PECODODL;
                        ,PETIPRIF = this.w_PETIPRIF;
                        ,PECODCOM = this.oParentObject.w_DPCODCOM;
                        ,PERIFODL = space(15);
                        ,PESERORD = space(10);
                        ,PERIGORD = 0;
                        ,PERIFTRS = this.w_PERIFTRS;
                        &i_ccchkf. ;
                     where;
                        PESERIAL = this.w_PESERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.w_PETIPRIF="D"
                  this.w_TRSSERORD = left(_Curs_GSDB2BAM.PERIFTRS,10)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Write into PEG_SELI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                    +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +",PESERORD ="+cp_NullLink(cp_ToStrODBC(this.w_TRSSERORD),'PEG_SELI','PESERORD');
                    +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +",PERIFTRS ="+cp_NullLink(cp_ToStrODBC(space(20)),'PEG_SELI','PERIFTRS');
                        +i_ccchkf ;
                    +" where ";
                        +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                           )
                  else
                    update (i_cTable) set;
                        PESERODL = this.w_PECODODL;
                        ,PETIPRIF = this.w_PETIPRIF;
                        ,PECODCOM = this.w_OLTCOMME;
                        ,PESERORD = this.w_TRSSERORD;
                        ,PERIGORD = this.w_TRSRIGORD;
                        ,PERIFTRS = space(20);
                        &i_ccchkf. ;
                     where;
                        PESERIAL = this.w_PESERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.w_PETIPRIF="O"
                  this.w_TRSRIFODL = left(_Curs_GSDB2BAM.PERIFTRS,15)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Write into PEG_SELI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                    +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +",PERIFODL ="+cp_NullLink(cp_ToStrODBC(this.w_TRSRIFODL),'PEG_SELI','PERIFODL');
                    +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +",PERIFTRS ="+cp_NullLink(cp_ToStrODBC(space(20)),'PEG_SELI','PERIFTRS');
                        +i_ccchkf ;
                    +" where ";
                        +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                           )
                  else
                    update (i_cTable) set;
                        PESERODL = this.w_PECODODL;
                        ,PETIPRIF = this.w_PETIPRIF;
                        ,PECODCOM = this.w_OLTCOMME;
                        ,PERIFODL = this.w_TRSRIFODL;
                        ,PERIGORD = this.w_TRSRIGORD;
                        ,PERIFTRS = space(20);
                        &i_ccchkf. ;
                     where;
                        PESERIAL = this.w_PESERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
              endcase
            else
              * --- Write into PEG_SELI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                    +i_ccchkf ;
                +" where ";
                    +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                       )
              else
                update (i_cTable) set;
                    PESERODL = this.w_PECODODL;
                    ,PETIPRIF = this.w_PETIPRIF;
                    ,PECODCOM = this.oParentObject.w_DPCODCOM;
                    &i_ccchkf. ;
                 where;
                    PESERIAL = this.w_PESERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            this.w_PEQTAUM1 = this.w_PEQTAUM1 - this.w_PEQTAABB
            if this.w_PETIPRIF="M" and this.w_PEQTAUM1>0
              * --- Se dichiaro pi� del dovuto inserisco a magazzino la parte non abbinata
              pegging=.F.
            endif
          endif
        else
          * --- Divide l'abbinamento in due parti: una rimane all'ODL e l'altra va a Magazzino
          * --- Write into PEG_SELI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PEQTAABB =PEQTAABB- "+cp_ToStrODBC(this.w_PEQTAUM1);
                +i_ccchkf ;
            +" where ";
                +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                   )
          else
            update (i_cTable) set;
                PEQTAABB = PEQTAABB - this.w_PEQTAUM1;
                &i_ccchkf. ;
             where;
                PESERIAL = this.w_PESERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_PROGR = max(this.w_PROGR + 1,1)
          this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
          do case
            case _Curs_GSDB2BAM.PETIPRIF="O"
              if this.oParentObject.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S"
                this.w_PESERORD = space(10)
                this.w_PERIGORD = 0
                this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PERIFODL)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
              else
                this.w_PESERORD = _Curs_GSDB2BAM.PERIFODL
                this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                this.w_PERIFTRS = space(20)
              endif
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+",PERIFTRS"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PERIFODL',this.w_PESERORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_PERIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_PECODRIC,'PERIFTRS',this.w_PERIFTRS)
                insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC,PERIFTRS &i_ccchkf. );
                   values (;
                     this.w_PESERIAL;
                     ,this.w_PECODODL;
                     ,"M";
                     ,this.w_PESERORD;
                     ,this.w_PEQTAUM1;
                     ,this.oParentObject.w_DPCODODL;
                     ,this.w_PERIGORD;
                     ,this.oParentObject.w_DPCODCOM;
                     ,this.w_PECODRIC;
                     ,this.w_PERIFTRS;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            case _Curs_GSDB2BAM.PETIPRIF="D"
              if this.oParentObject.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S"
                this.w_PESERORD = space(10)
                this.w_PERIGORD = 0
                this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PESERORD)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
              else
                this.w_PESERORD = _Curs_GSDB2BAM.PESERORD
                this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                this.w_PERIFTRS = space(20)
              endif
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+",PERIFTRS"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PESERORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PESERORD',this.w_PESERORD,'PERIGORD',this.w_PERIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_PECODRIC,'PERIFTRS',this.w_PERIFTRS)
                insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC,PERIFTRS &i_ccchkf. );
                   values (;
                     this.w_PESERIAL;
                     ,this.w_PECODODL;
                     ,"M";
                     ,this.w_PESERORD;
                     ,this.w_PERIGORD;
                     ,this.w_PEQTAUM1;
                     ,this.oParentObject.w_DPCODODL;
                     ,this.oParentObject.w_DPCODCOM;
                     ,this.w_PECODRIC;
                     ,this.w_PERIFTRS;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            case _Curs_GSDB2BAM.PETIPRIF="M"
              if empty(nvl(_Curs_GSDB2BAM.PERIFODL,""))
                * --- Storna pegging di documento
                if this.oParentObject.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S" and ! empty(nvl(_Curs_GSDB2BAM.PERIFTRS," "))
                  this.w_TRSSERORD = left(_Curs_GSDB2BAM.PERIFTRS,10)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSSERORD),'PEG_SELI','PESERORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"D",'PESERORD',this.w_TRSSERORD,'PERIGORD',this.w_TRSRIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"D";
                         ,this.w_TRSSERORD;
                         ,this.w_TRSRIGORD;
                         ,this.w_PEQTAUM1;
                         ,this.oParentObject.w_DPCODODL;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                else
                  this.w_PESERORD = _Curs_GSDB2BAM.PESERORD
                  this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PESERORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"D",'PESERORD',this.w_PESERORD,'PERIGORD',this.w_PERIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"D";
                         ,this.w_PESERORD;
                         ,this.w_PERIGORD;
                         ,this.w_PEQTAUM1;
                         ,this.oParentObject.w_DPCODODL;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              else
                * --- Storna pegging di impegno ODL
                if this.oParentObject.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S" and ! empty(nvl(_Curs_GSDB2BAM.PERIFTRS," "))
                  this.w_TRSRIFODL = left(_Curs_GSDB2BAM.PERIFTRS,15)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSRIFODL),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"O",'PERIFODL',this.w_TRSRIFODL,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_TRSRIGORD,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"O";
                         ,this.w_TRSRIFODL;
                         ,this.w_PEQTAUM1;
                         ,this.oParentObject.w_DPCODODL;
                         ,this.w_TRSRIGORD;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                else
                  this.w_PESERORD = _Curs_GSDB2BAM.PERIFODL
                  this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"O",'PERIFODL',this.w_PESERORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_PERIGORD,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"O";
                         ,this.w_PESERORD;
                         ,this.w_PEQTAUM1;
                         ,this.oParentObject.w_DPCODODL;
                         ,this.w_PERIGORD;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              endif
          endcase
          this.w_PEQTAUM1 = 0
        endif
      endif
        select _Curs_GSDB2BAM
        continue
      enddo
      use
    endif
    if ! pegging and not empty (this.oParentObject.w_DPCODCOM) and (this.w_PEFLCOMM="S")
      * --- Inserisco sul pegging la quantit� non abbinata inserita sul magazzino per la commessa
      this.w_PETIPRIF = "M"
      this.w_PROGR = max(this.w_PROGR + 1,1)
      this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
      * --- Insert into PEG_SELI
      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
        +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODODL),'PEG_SELI','PEODLORI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DPCODCOM),'PEG_SELI','PECODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PECODART),'PEG_SELI','PECODRIC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PERIFODL',this.w_PESERORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.oParentObject.w_DPCODODL,'PERIGORD',this.w_PERIGORD,'PECODCOM',this.oParentObject.w_DPCODCOM,'PECODRIC',this.w_PECODART)
        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
           values (;
             this.w_PESERIAL;
             ,this.w_PECODODL;
             ,"M";
             ,this.w_PESERORD;
             ,this.w_PEQTAUM1;
             ,this.oParentObject.w_DPCODODL;
             ,this.w_PERIGORD;
             ,this.oParentObject.w_DPCODCOM;
             ,this.w_PECODART;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,TipOpe)
    this.TipOpe=TipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,41)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='DOC_RATE'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='*DELTA'
    this.cWorkTables[8]='ODL_MAST'
    this.cWorkTables[9]='ODL_DETT'
    this.cWorkTables[10]='MAGAZZIN'
    this.cWorkTables[11]='DIC_PROD'
    this.cWorkTables[12]='AGG_LOTT'
    this.cWorkTables[13]='SPL_LOTT'
    this.cWorkTables[14]='DIC_MATR'
    this.cWorkTables[15]='DIC_MCOM'
    this.cWorkTables[16]='*DELTA'
    this.cWorkTables[17]='MOVIMATR'
    this.cWorkTables[18]='MATRICOL'
    this.cWorkTables[19]='KEY_ARTI'
    this.cWorkTables[20]='ART_ICOL'
    this.cWorkTables[21]='MA_COSTI'
    this.cWorkTables[22]='MAT_PROD'
    this.cWorkTables[23]='UNIMIS'
    this.cWorkTables[24]='ART_PROD'
    this.cWorkTables[25]='PEG_SELI'
    this.cWorkTables[26]='PAR_PROD'
    this.cWorkTables[27]='ODL_SMPL'
    this.cWorkTables[28]='LOTTIART'
    this.cWorkTables[29]='MPS_TFOR'
    this.cWorkTables[30]='SALDICOM'
    this.cWorkTables[31]='RIS_ORSE'
    this.cWorkTables[32]='CLR_MAST'
    this.cWorkTables[33]='VALUTE'
    this.cWorkTables[34]='CAN_TIER'
    this.cWorkTables[35]='VOC_COST'
    this.cWorkTables[36]='VOCIIVA'
    this.cWorkTables[37]='ODL_RISF'
    this.cWorkTables[38]='ODL_CICL'
    this.cWorkTables[39]='ODL_RISO'
    this.cWorkTables[40]='RIS_DICH'
    this.cWorkTables[41]='DIC_COMP'
    return(this.OpenAllTables(41))

  proc CloseCursors()
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_DIC_PROD')
      use in _Curs_DIC_PROD
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_GSCO10BDP')
      use in _Curs_GSCO10BDP
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_GSCO10BDP')
      use in _Curs_GSCO10BDP
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_GSCO10BDP')
      use in _Curs_GSCO10BDP
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_GSCO10BDP')
      use in _Curs_GSCO10BDP
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_GSCO_BTA3')
      use in _Curs_GSCO_BTA3
    endif
    if used('_Curs_GSCO_BTA4')
      use in _Curs_GSCO_BTA4
    endif
    if used('_Curs_GSCO_BTA5')
      use in _Curs_GSCO_BTA5
    endif
    if used('_Curs_gsco2bgd')
      use in _Curs_gsco2bgd
    endif
    if used('_Curs_RIS_DICH')
      use in _Curs_RIS_DICH
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs__d__d__cola_exe_query_gsco_bdp1')
      use in _Curs__d__d__cola_exe_query_gsco_bdp1
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_GSCO9BDP')
      use in _Curs_GSCO9BDP
    endif
    if used('_Curs_GSCO7BDP')
      use in _Curs_GSCO7BDP
    endif
    if used('_Curs_GSCO11BDP')
      use in _Curs_GSCO11BDP
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_gsdb1bad')
      use in _Curs_gsdb1bad
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_gsdb1bad')
      use in _Curs_gsdb1bad
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_MOVIMATR')
      use in _Curs_MOVIMATR
    endif
    if used('_Curs_GSCO_BAD6')
      use in _Curs_GSCO_BAD6
    endif
    if used('_Curs_GSCO_BLZ')
      use in _Curs_GSCO_BLZ
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_GSDB2BAM')
      use in _Curs_GSDB2BAM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="TipOpe"
endproc
