* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_agm                                                        *
*              Archivio generazione MRP                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-10                                                      *
* Last revis.: 2018-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_agm"))

* --- Class definition
define class tgsmr_agm as StdForm
  Top    = 5
  Left   = 10

  * --- Standard Properties
  Width  = 829
  Height = 600+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-19"
  HelpContextID=225875095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=97

  * --- Constant Properties
  SEL__MRP_IDX = 0
  CPUSERS_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  CAN_TIER_IDX = 0
  MARCHI_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "SEL__MRP"
  cKeySelect = "GMSERIAL"
  cKeyWhere  = "GMSERIAL=this.w_GMSERIAL"
  cKeyWhereODBC = '"GMSERIAL="+cp_ToStrODBC(this.w_GMSERIAL)';

  cKeyWhereODBCqualified = '"SEL__MRP.GMSERIAL="+cp_ToStrODBC(this.w_GMSERIAL)';

  cPrg = "gsmr_agm"
  cComment = "Archivio generazione MRP"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPCON = space(1)
  w_GMSERIAL = space(10)
  w_GMDATELA = ctod('  /  /  ')
  w_GMORAELA = space(8)
  w_GMUTEELA = 0
  w_GMPATHEL = space(200)
  w_DESUTE = space(20)
  w_GMFLULEL = space(1)
  w_GMTIPDOC = space(0)
  w_GFPATHFILE = space(10)
  w_GFNOMEFILE = space(10)
  w_GMMODELA = space(1)
  w_GMCHKDAT = space(1)
  w_GMMRPLOG = space(1)
  w_GMMSGRIP = space(1)
  w_GMMICOMP = space(1)
  w_GMMAGFOR = space(5)
  w_GMMRFODL = space(1)
  w_GMMAGFOC = space(5)
  w_GMMRFOCL = space(1)
  w_GMMAGFOL = space(5)
  w_GMGENPDA = space(1)
  w_GMMRFODA = space(1)
  w_GMMAGFOA = space(5)
  w_GMGENODA = space(1)
  w_GMDISMAG = space(1)
  w_GMGIANEG = space(1)
  w_GMORDMPS = space(1)
  w_GMSTAORD = space(1)
  w_DESMFR = space(30)
  w_DESMOC = space(30)
  w_DESMOL = space(30)
  w_DESMOA = space(30)
  w_GMCRIFOR = space(1)
  w_GMCRIELA = space(1)
  o_GMCRIELA = space(1)
  w_GMLISMAG = space(0)
  w_GMELABID = space(1)
  w_GMCODINI = space(20)
  w_GMCODFIN = space(20)
  w_GMLLCINI = 0
  w_GMLLCFIN = 0
  w_GMFAMAIN = space(5)
  w_GMFAMAFI = space(5)
  w_GMGRUINI = space(5)
  w_GMGRUFIN = space(5)
  w_GMCATINI = space(5)
  w_GMCATFIN = space(5)
  w_GMMAGINI = space(5)
  w_GMMAGFIN = space(5)
  w_GMMARINI = space(5)
  w_GMMARFIN = space(5)
  w_GMPROFIN = space(2)
  w_GMSEMLAV = space(2)
  w_GMMATPRI = space(2)
  w_GMELACAT = space(1)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESMARI = space(35)
  w_DESMARF = space(35)
  w_GMCOMINI = space(15)
  w_GMCOMFIN = space(15)
  w_GMCOMODL = space(1)
  w_GMNUMINI = 0
  w_GMSERIE1 = space(10)
  w_GMNUMFIN = 0
  w_GMSERIE2 = space(10)
  w_GMDOCINI = ctod('  /  /  ')
  w_GMDOCFIN = ctod('  /  /  ')
  w_GMINICLI = space(15)
  w_GMFINCLI = space(15)
  w_GMINIELA = ctod('  /  /  ')
  w_GMFINELA = ctod('  /  /  ')
  w_GMORIODL = space(1)
  w_DESCLII = space(40)
  w_DESCLIF = space(40)
  w_DESCOMI = space(30)
  w_DESCOMF = space(30)
  w_GMSELIMP = space(1)
  w_GMNOTEEL = space(0)
  w_GMPERPIA = space(1)
  w_GMPIAPUN = space(1)
  w_GMORDPRD = space(1)
  w_GMIMPPRD = space(1)
  w_RET = 0
  w_GMDELORD = space(1)
  w_nColXCHK = 0
  w_GUFLAROB = space(1)
  w_GUELPDAS = space(1)
  w_GUELPDAF = space(1)
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  w_SZOOM1 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsmr_agm
  w_KEYRIF=''
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SEL__MRP','gsmr_agm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_agmPag1","gsmr_agm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modalit� di elaborazione")
      .Pages(1).HelpContextID = 165295779
      .Pages(2).addobject("oPag","tgsmr_agmPag2","gsmr_agm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).HelpContextID = 24984028
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGMSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZOOMMAGA = this.oPgFrm.Pages(1).oPag.ZOOMMAGA
      this.w_LBLMAGA = this.oPgFrm.Pages(1).oPag.LBLMAGA
      this.w_SZOOM1 = this.oPgFrm.Pages(2).oPag.SZOOM1
      DoDefault()
    proc Destroy()
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      this.w_SZOOM1 = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='MARCHI'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='SEL__MRP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SEL__MRP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SEL__MRP_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GMSERIAL = NVL(GMSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_24_joined
    link_1_24_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_18_joined
    link_2_18_joined=.f.
    local link_2_50_joined
    link_2_50_joined=.f.
    local link_2_51_joined
    link_2_51_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SEL__MRP where GMSERIAL=KeySet.GMSERIAL
    *
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SEL__MRP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SEL__MRP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SEL__MRP '
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_18_joined=this.AddJoinedLink_2_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_50_joined=this.AddJoinedLink_2_50(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_51_joined=this.AddJoinedLink_2_51(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GMSERIAL',this.w_GMSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPCON = 'C'
        .w_DESUTE = space(20)
        .w_DESMFR = space(30)
        .w_DESMOC = space(30)
        .w_DESMOL = space(30)
        .w_DESMOA = space(30)
        .w_DESMAGI = space(30)
        .w_DESMAGF = space(30)
        .w_DESINI = space(40)
        .w_DESFIN = space(40)
        .w_DESFAMAI = space(35)
        .w_DESGRUI = space(35)
        .w_DESCATI = space(35)
        .w_DESFAMAF = space(35)
        .w_DESGRUF = space(35)
        .w_DESCATF = space(35)
        .w_DESMARI = space(35)
        .w_DESMARF = space(35)
        .w_DESCLII = space(40)
        .w_DESCLIF = space(40)
        .w_DESCOMI = space(30)
        .w_DESCOMF = space(30)
        .w_RET = 0
        .w_nColXCHK = 0
        .w_GMSERIAL = NVL(GMSERIAL,space(10))
        .w_GMDATELA = NVL(cp_ToDate(GMDATELA),ctod("  /  /  "))
        .w_GMORAELA = NVL(GMORAELA,space(8))
        .w_GMUTEELA = NVL(GMUTEELA,0)
          .link_1_5('Load')
        .w_GMPATHEL = NVL(GMPATHEL,space(200))
        .w_GMFLULEL = NVL(GMFLULEL,space(1))
        .w_GMTIPDOC = NVL(GMTIPDOC,space(0))
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .w_GFPATHFILE = alltrim(.w_GMPATHEL)+"elab"+alltrim(i_CODAZI)+alltrim(.w_GMSERIAL)+".log"
        .w_GFNOMEFILE = alltrim(.w_GFPATHFILE)+"elab"+alltrim(i_CODAZI)+alltrim(.w_GMSERIAL)+".log"
        .w_GMMODELA = NVL(GMMODELA,space(1))
        .w_GMCHKDAT = NVL(GMCHKDAT,space(1))
        .w_GMMRPLOG = NVL(GMMRPLOG,space(1))
        .w_GMMSGRIP = NVL(GMMSGRIP,space(1))
        .w_GMMICOMP = NVL(GMMICOMP,space(1))
        .w_GMMAGFOR = NVL(GMMAGFOR,space(5))
          if link_1_22_joined
            this.w_GMMAGFOR = NVL(MGCODMAG122,NVL(this.w_GMMAGFOR,space(5)))
            this.w_DESMFR = NVL(MGDESMAG122,space(30))
          else
          .link_1_22('Load')
          endif
        .w_GMMRFODL = NVL(GMMRFODL,space(1))
        .w_GMMAGFOC = NVL(GMMAGFOC,space(5))
          if link_1_24_joined
            this.w_GMMAGFOC = NVL(MGCODMAG124,NVL(this.w_GMMAGFOC,space(5)))
            this.w_DESMOC = NVL(MGDESMAG124,space(30))
          else
          .link_1_24('Load')
          endif
        .w_GMMRFOCL = NVL(GMMRFOCL,space(1))
        .w_GMMAGFOL = NVL(GMMAGFOL,space(5))
          if link_1_26_joined
            this.w_GMMAGFOL = NVL(MGCODMAG126,NVL(this.w_GMMAGFOL,space(5)))
            this.w_DESMOL = NVL(MGDESMAG126,space(30))
          else
          .link_1_26('Load')
          endif
        .w_GMGENPDA = NVL(GMGENPDA,space(1))
        .w_GMMRFODA = NVL(GMMRFODA,space(1))
        .w_GMMAGFOA = NVL(GMMAGFOA,space(5))
          if link_1_29_joined
            this.w_GMMAGFOA = NVL(MGCODMAG129,NVL(this.w_GMMAGFOA,space(5)))
            this.w_DESMOA = NVL(MGDESMAG129,space(30))
          else
          .link_1_29('Load')
          endif
        .w_GMGENODA = NVL(GMGENODA,space(1))
        .w_GMDISMAG = NVL(GMDISMAG,space(1))
        .w_GMGIANEG = NVL(GMGIANEG,space(1))
        .w_GMORDMPS = NVL(GMORDMPS,space(1))
        .w_GMSTAORD = NVL(GMSTAORD,space(1))
        .w_GMCRIFOR = NVL(GMCRIFOR,space(1))
        .w_GMCRIELA = NVL(GMCRIELA,space(1))
        .w_GMLISMAG = NVL(GMLISMAG,space(0))
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_GMCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .w_GMELABID = NVL(GMELABID,space(1))
        .w_GMCODINI = NVL(GMCODINI,space(20))
          if link_2_5_joined
            this.w_GMCODINI = NVL(ARCODART205,NVL(this.w_GMCODINI,space(20)))
            this.w_DESINI = NVL(ARDESART205,space(40))
          else
          .link_2_5('Load')
          endif
        .w_GMCODFIN = NVL(GMCODFIN,space(20))
          if link_2_6_joined
            this.w_GMCODFIN = NVL(ARCODART206,NVL(this.w_GMCODFIN,space(20)))
            this.w_DESFIN = NVL(ARDESART206,space(40))
          else
          .link_2_6('Load')
          endif
        .w_GMLLCINI = NVL(GMLLCINI,0)
        .w_GMLLCFIN = NVL(GMLLCFIN,0)
        .w_GMFAMAIN = NVL(GMFAMAIN,space(5))
          if link_2_9_joined
            this.w_GMFAMAIN = NVL(FACODICE209,NVL(this.w_GMFAMAIN,space(5)))
            this.w_DESFAMAI = NVL(FADESCRI209,space(35))
          else
          .link_2_9('Load')
          endif
        .w_GMFAMAFI = NVL(GMFAMAFI,space(5))
          if link_2_10_joined
            this.w_GMFAMAFI = NVL(FACODICE210,NVL(this.w_GMFAMAFI,space(5)))
            this.w_DESFAMAF = NVL(FADESCRI210,space(35))
          else
          .link_2_10('Load')
          endif
        .w_GMGRUINI = NVL(GMGRUINI,space(5))
          if link_2_11_joined
            this.w_GMGRUINI = NVL(GMCODICE211,NVL(this.w_GMGRUINI,space(5)))
            this.w_DESGRUI = NVL(GMDESCRI211,space(35))
          else
          .link_2_11('Load')
          endif
        .w_GMGRUFIN = NVL(GMGRUFIN,space(5))
          if link_2_12_joined
            this.w_GMGRUFIN = NVL(GMCODICE212,NVL(this.w_GMGRUFIN,space(5)))
            this.w_DESGRUF = NVL(GMDESCRI212,space(35))
          else
          .link_2_12('Load')
          endif
        .w_GMCATINI = NVL(GMCATINI,space(5))
          if link_2_13_joined
            this.w_GMCATINI = NVL(OMCODICE213,NVL(this.w_GMCATINI,space(5)))
            this.w_DESCATI = NVL(OMDESCRI213,space(35))
          else
          .link_2_13('Load')
          endif
        .w_GMCATFIN = NVL(GMCATFIN,space(5))
          if link_2_14_joined
            this.w_GMCATFIN = NVL(OMCODICE214,NVL(this.w_GMCATFIN,space(5)))
            this.w_DESCATF = NVL(OMDESCRI214,space(35))
          else
          .link_2_14('Load')
          endif
        .w_GMMAGINI = NVL(GMMAGINI,space(5))
          if link_2_15_joined
            this.w_GMMAGINI = NVL(MGCODMAG215,NVL(this.w_GMMAGINI,space(5)))
            this.w_DESMAGI = NVL(MGDESMAG215,space(30))
          else
          .link_2_15('Load')
          endif
        .w_GMMAGFIN = NVL(GMMAGFIN,space(5))
          if link_2_16_joined
            this.w_GMMAGFIN = NVL(MGCODMAG216,NVL(this.w_GMMAGFIN,space(5)))
            this.w_DESMAGF = NVL(MGDESMAG216,space(30))
          else
          .link_2_16('Load')
          endif
        .w_GMMARINI = NVL(GMMARINI,space(5))
          if link_2_17_joined
            this.w_GMMARINI = NVL(MACODICE217,NVL(this.w_GMMARINI,space(5)))
            this.w_DESMARI = NVL(MADESCRI217,space(35))
          else
          .link_2_17('Load')
          endif
        .w_GMMARFIN = NVL(GMMARFIN,space(5))
          if link_2_18_joined
            this.w_GMMARFIN = NVL(MACODICE218,NVL(this.w_GMMARFIN,space(5)))
            this.w_DESMARF = NVL(MADESCRI218,space(35))
          else
          .link_2_18('Load')
          endif
        .w_GMPROFIN = NVL(GMPROFIN,space(2))
        .w_GMSEMLAV = NVL(GMSEMLAV,space(2))
        .w_GMMATPRI = NVL(GMMATPRI,space(2))
        .w_GMELACAT = NVL(GMELACAT,space(1))
        .w_GMCOMINI = NVL(GMCOMINI,space(15))
          if link_2_50_joined
            this.w_GMCOMINI = NVL(CNCODCAN250,NVL(this.w_GMCOMINI,space(15)))
            this.w_DESCOMI = NVL(CNDESCAN250,space(30))
          else
          .link_2_50('Load')
          endif
        .w_GMCOMFIN = NVL(GMCOMFIN,space(15))
          if link_2_51_joined
            this.w_GMCOMFIN = NVL(CNCODCAN251,NVL(this.w_GMCOMFIN,space(15)))
            this.w_DESCOMF = NVL(CNDESCAN251,space(30))
          else
          .link_2_51('Load')
          endif
        .w_GMCOMODL = NVL(GMCOMODL,space(1))
        .w_GMNUMINI = NVL(GMNUMINI,0)
        .w_GMSERIE1 = NVL(GMSERIE1,space(10))
        .w_GMNUMFIN = NVL(GMNUMFIN,0)
        .w_GMSERIE2 = NVL(GMSERIE2,space(10))
        .w_GMDOCINI = NVL(cp_ToDate(GMDOCINI),ctod("  /  /  "))
        .w_GMDOCFIN = NVL(cp_ToDate(GMDOCFIN),ctod("  /  /  "))
        .w_GMINICLI = NVL(GMINICLI,space(15))
          .link_2_59('Load')
        .w_GMFINCLI = NVL(GMFINCLI,space(15))
          .link_2_60('Load')
        .w_GMINIELA = NVL(cp_ToDate(GMINIELA),ctod("  /  /  "))
        .w_GMFINELA = NVL(cp_ToDate(GMFINELA),ctod("  /  /  "))
        .w_GMORIODL = NVL(GMORIODL,space(1))
        .oPgFrm.Page2.oPag.SZOOM1.Calculate()
        .w_GMSELIMP = NVL(GMSELIMP,space(1))
        .w_GMNOTEEL = NVL(GMNOTEEL,space(0))
        .w_GMPERPIA = NVL(GMPERPIA,space(1))
        .w_GMPIAPUN = NVL(GMPIAPUN,space(1))
        .w_GMORDPRD = NVL(GMORDPRD,space(1))
        .w_GMIMPPRD = NVL(GMIMPPRD,space(1))
        .w_GMDELORD = NVL(GMDELORD,space(1))
        .w_GUFLAROB = NVL(GUFLAROB,space(1))
        .w_GUELPDAS = NVL(GUELPDAS,space(1))
        .w_GUELPDAF = NVL(GUELPDAF,space(1))
        cp_LoadRecExtFlds(this,'SEL__MRP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON = space(1)
      .w_GMSERIAL = space(10)
      .w_GMDATELA = ctod("  /  /  ")
      .w_GMORAELA = space(8)
      .w_GMUTEELA = 0
      .w_GMPATHEL = space(200)
      .w_DESUTE = space(20)
      .w_GMFLULEL = space(1)
      .w_GMTIPDOC = space(0)
      .w_GFPATHFILE = space(10)
      .w_GFNOMEFILE = space(10)
      .w_GMMODELA = space(1)
      .w_GMCHKDAT = space(1)
      .w_GMMRPLOG = space(1)
      .w_GMMSGRIP = space(1)
      .w_GMMICOMP = space(1)
      .w_GMMAGFOR = space(5)
      .w_GMMRFODL = space(1)
      .w_GMMAGFOC = space(5)
      .w_GMMRFOCL = space(1)
      .w_GMMAGFOL = space(5)
      .w_GMGENPDA = space(1)
      .w_GMMRFODA = space(1)
      .w_GMMAGFOA = space(5)
      .w_GMGENODA = space(1)
      .w_GMDISMAG = space(1)
      .w_GMGIANEG = space(1)
      .w_GMORDMPS = space(1)
      .w_GMSTAORD = space(1)
      .w_DESMFR = space(30)
      .w_DESMOC = space(30)
      .w_DESMOL = space(30)
      .w_DESMOA = space(30)
      .w_GMCRIFOR = space(1)
      .w_GMCRIELA = space(1)
      .w_GMLISMAG = space(0)
      .w_GMELABID = space(1)
      .w_GMCODINI = space(20)
      .w_GMCODFIN = space(20)
      .w_GMLLCINI = 0
      .w_GMLLCFIN = 0
      .w_GMFAMAIN = space(5)
      .w_GMFAMAFI = space(5)
      .w_GMGRUINI = space(5)
      .w_GMGRUFIN = space(5)
      .w_GMCATINI = space(5)
      .w_GMCATFIN = space(5)
      .w_GMMAGINI = space(5)
      .w_GMMAGFIN = space(5)
      .w_GMMARINI = space(5)
      .w_GMMARFIN = space(5)
      .w_GMPROFIN = space(2)
      .w_GMSEMLAV = space(2)
      .w_GMMATPRI = space(2)
      .w_GMELACAT = space(1)
      .w_DESMAGI = space(30)
      .w_DESMAGF = space(30)
      .w_DESINI = space(40)
      .w_DESFIN = space(40)
      .w_DESFAMAI = space(35)
      .w_DESGRUI = space(35)
      .w_DESCATI = space(35)
      .w_DESFAMAF = space(35)
      .w_DESGRUF = space(35)
      .w_DESCATF = space(35)
      .w_DESMARI = space(35)
      .w_DESMARF = space(35)
      .w_GMCOMINI = space(15)
      .w_GMCOMFIN = space(15)
      .w_GMCOMODL = space(1)
      .w_GMNUMINI = 0
      .w_GMSERIE1 = space(10)
      .w_GMNUMFIN = 0
      .w_GMSERIE2 = space(10)
      .w_GMDOCINI = ctod("  /  /  ")
      .w_GMDOCFIN = ctod("  /  /  ")
      .w_GMINICLI = space(15)
      .w_GMFINCLI = space(15)
      .w_GMINIELA = ctod("  /  /  ")
      .w_GMFINELA = ctod("  /  /  ")
      .w_GMORIODL = space(1)
      .w_DESCLII = space(40)
      .w_DESCLIF = space(40)
      .w_DESCOMI = space(30)
      .w_DESCOMF = space(30)
      .w_GMSELIMP = space(1)
      .w_GMNOTEEL = space(0)
      .w_GMPERPIA = space(1)
      .w_GMPIAPUN = space(1)
      .w_GMORDPRD = space(1)
      .w_GMIMPPRD = space(1)
      .w_RET = 0
      .w_GMDELORD = space(1)
      .w_nColXCHK = 0
      .w_GUFLAROB = space(1)
      .w_GUELPDAS = space(1)
      .w_GUELPDAF = space(1)
      if .cFunction<>"Filter"
        .w_TIPCON = 'C'
        .DoRTCalc(2,5,.f.)
          if not(empty(.w_GMUTEELA))
          .link_1_5('Full')
          endif
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
          .DoRTCalc(6,9,.f.)
        .w_GFPATHFILE = alltrim(.w_GMPATHEL)+"elab"+alltrim(i_CODAZI)+alltrim(.w_GMSERIAL)+".log"
        .w_GFNOMEFILE = alltrim(.w_GFPATHFILE)+"elab"+alltrim(i_CODAZI)+alltrim(.w_GMSERIAL)+".log"
        .DoRTCalc(12,17,.f.)
          if not(empty(.w_GMMAGFOR))
          .link_1_22('Full')
          endif
        .DoRTCalc(18,19,.f.)
          if not(empty(.w_GMMAGFOC))
          .link_1_24('Full')
          endif
        .DoRTCalc(20,21,.f.)
          if not(empty(.w_GMMAGFOL))
          .link_1_26('Full')
          endif
        .w_GMGENPDA = 'N'
        .DoRTCalc(23,24,.f.)
          if not(empty(.w_GMMAGFOA))
          .link_1_29('Full')
          endif
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_GMCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .DoRTCalc(25,38,.f.)
          if not(empty(.w_GMCODINI))
          .link_2_5('Full')
          endif
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_GMCODFIN))
          .link_2_6('Full')
          endif
        .DoRTCalc(40,42,.f.)
          if not(empty(.w_GMFAMAIN))
          .link_2_9('Full')
          endif
        .DoRTCalc(43,43,.f.)
          if not(empty(.w_GMFAMAFI))
          .link_2_10('Full')
          endif
        .DoRTCalc(44,44,.f.)
          if not(empty(.w_GMGRUINI))
          .link_2_11('Full')
          endif
        .DoRTCalc(45,45,.f.)
          if not(empty(.w_GMGRUFIN))
          .link_2_12('Full')
          endif
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_GMCATINI))
          .link_2_13('Full')
          endif
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_GMCATFIN))
          .link_2_14('Full')
          endif
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_GMMAGINI))
          .link_2_15('Full')
          endif
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_GMMAGFIN))
          .link_2_16('Full')
          endif
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_GMMARINI))
          .link_2_17('Full')
          endif
        .DoRTCalc(51,51,.f.)
          if not(empty(.w_GMMARFIN))
          .link_2_18('Full')
          endif
        .DoRTCalc(52,68,.f.)
          if not(empty(.w_GMCOMINI))
          .link_2_50('Full')
          endif
        .DoRTCalc(69,69,.f.)
          if not(empty(.w_GMCOMFIN))
          .link_2_51('Full')
          endif
        .DoRTCalc(70,77,.f.)
          if not(empty(.w_GMINICLI))
          .link_2_59('Full')
          endif
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_GMFINCLI))
          .link_2_60('Full')
          endif
        .oPgFrm.Page2.oPag.SZOOM1.Calculate()
          .DoRTCalc(79,95,.f.)
        .w_GUELPDAS = 'S'
        .w_GUELPDAF = 'F'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SEL__MRP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGMSERIAL_1_2.enabled = !i_bVal
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
      .Page1.oPag.oBtn_1_66.enabled = .Page1.oPag.oBtn_1_66.mCond()
      .Page2.oPag.oObj_2_2.enabled = i_bVal
      .Page1.oPag.ZOOMMAGA.enabled = i_bVal
      .Page1.oPag.LBLMAGA.enabled = i_bVal
      .Page2.oPag.SZOOM1.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'SEL__MRP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMSERIAL,"GMSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMDATELA,"GMDATELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMORAELA,"GMORAELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMUTEELA,"GMUTEELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMPATHEL,"GMPATHEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMFLULEL,"GMFLULEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMTIPDOC,"GMTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMODELA,"GMMODELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCHKDAT,"GMCHKDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMRPLOG,"GMMRPLOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMSGRIP,"GMMSGRIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMICOMP,"GMMICOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMAGFOR,"GMMAGFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMRFODL,"GMMRFODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMAGFOC,"GMMAGFOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMRFOCL,"GMMRFOCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMAGFOL,"GMMAGFOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMGENPDA,"GMGENPDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMRFODA,"GMMRFODA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMAGFOA,"GMMAGFOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMGENODA,"GMGENODA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMDISMAG,"GMDISMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMGIANEG,"GMGIANEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMORDMPS,"GMORDMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMSTAORD,"GMSTAORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCRIFOR,"GMCRIFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCRIELA,"GMCRIELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMLISMAG,"GMLISMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMELABID,"GMELABID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCODINI,"GMCODINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCODFIN,"GMCODFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMLLCINI,"GMLLCINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMLLCFIN,"GMLLCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMFAMAIN,"GMFAMAIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMFAMAFI,"GMFAMAFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMGRUINI,"GMGRUINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMGRUFIN,"GMGRUFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCATINI,"GMCATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCATFIN,"GMCATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMAGINI,"GMMAGINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMAGFIN,"GMMAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMARINI,"GMMARINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMARFIN,"GMMARFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMPROFIN,"GMPROFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMSEMLAV,"GMSEMLAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMMATPRI,"GMMATPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMELACAT,"GMELACAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCOMINI,"GMCOMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCOMFIN,"GMCOMFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMCOMODL,"GMCOMODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMNUMINI,"GMNUMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMSERIE1,"GMSERIE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMNUMFIN,"GMNUMFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMSERIE2,"GMSERIE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMDOCINI,"GMDOCINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMDOCFIN,"GMDOCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMINICLI,"GMINICLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMFINCLI,"GMFINCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMINIELA,"GMINIELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMFINELA,"GMFINELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMORIODL,"GMORIODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMSELIMP,"GMSELIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMNOTEEL,"GMNOTEEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMPERPIA,"GMPERPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMPIAPUN,"GMPIAPUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMORDPRD,"GMORDPRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMIMPPRD,"GMIMPPRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GMDELORD,"GMDELORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GUFLAROB,"GUFLAROB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GUELPDAS,"GUELPDAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GUELPDAF,"GUELPDAF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
    i_lTable = "SEL__MRP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SEL__MRP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SEL__MRP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SEL__MRP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SEL__MRP')
        i_extval=cp_InsertValODBCExtFlds(this,'SEL__MRP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GMSERIAL,GMDATELA,GMORAELA,GMUTEELA,GMPATHEL"+;
                  ",GMFLULEL,GMTIPDOC,GMMODELA,GMCHKDAT,GMMRPLOG"+;
                  ",GMMSGRIP,GMMICOMP,GMMAGFOR,GMMRFODL,GMMAGFOC"+;
                  ",GMMRFOCL,GMMAGFOL,GMGENPDA,GMMRFODA,GMMAGFOA"+;
                  ",GMGENODA,GMDISMAG,GMGIANEG,GMORDMPS,GMSTAORD"+;
                  ",GMCRIFOR,GMCRIELA,GMLISMAG,GMELABID,GMCODINI"+;
                  ",GMCODFIN,GMLLCINI,GMLLCFIN,GMFAMAIN,GMFAMAFI"+;
                  ",GMGRUINI,GMGRUFIN,GMCATINI,GMCATFIN,GMMAGINI"+;
                  ",GMMAGFIN,GMMARINI,GMMARFIN,GMPROFIN,GMSEMLAV"+;
                  ",GMMATPRI,GMELACAT,GMCOMINI,GMCOMFIN,GMCOMODL"+;
                  ",GMNUMINI,GMSERIE1,GMNUMFIN,GMSERIE2,GMDOCINI"+;
                  ",GMDOCFIN,GMINICLI,GMFINCLI,GMINIELA,GMFINELA"+;
                  ",GMORIODL,GMSELIMP,GMNOTEEL,GMPERPIA,GMPIAPUN"+;
                  ",GMORDPRD,GMIMPPRD,GMDELORD,GUFLAROB,GUELPDAS"+;
                  ",GUELPDAF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GMSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GMDATELA)+;
                  ","+cp_ToStrODBC(this.w_GMORAELA)+;
                  ","+cp_ToStrODBCNull(this.w_GMUTEELA)+;
                  ","+cp_ToStrODBC(this.w_GMPATHEL)+;
                  ","+cp_ToStrODBC(this.w_GMFLULEL)+;
                  ","+cp_ToStrODBC(this.w_GMTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_GMMODELA)+;
                  ","+cp_ToStrODBC(this.w_GMCHKDAT)+;
                  ","+cp_ToStrODBC(this.w_GMMRPLOG)+;
                  ","+cp_ToStrODBC(this.w_GMMSGRIP)+;
                  ","+cp_ToStrODBC(this.w_GMMICOMP)+;
                  ","+cp_ToStrODBCNull(this.w_GMMAGFOR)+;
                  ","+cp_ToStrODBC(this.w_GMMRFODL)+;
                  ","+cp_ToStrODBCNull(this.w_GMMAGFOC)+;
                  ","+cp_ToStrODBC(this.w_GMMRFOCL)+;
                  ","+cp_ToStrODBCNull(this.w_GMMAGFOL)+;
                  ","+cp_ToStrODBC(this.w_GMGENPDA)+;
                  ","+cp_ToStrODBC(this.w_GMMRFODA)+;
                  ","+cp_ToStrODBCNull(this.w_GMMAGFOA)+;
                  ","+cp_ToStrODBC(this.w_GMGENODA)+;
                  ","+cp_ToStrODBC(this.w_GMDISMAG)+;
                  ","+cp_ToStrODBC(this.w_GMGIANEG)+;
                  ","+cp_ToStrODBC(this.w_GMORDMPS)+;
                  ","+cp_ToStrODBC(this.w_GMSTAORD)+;
                  ","+cp_ToStrODBC(this.w_GMCRIFOR)+;
                  ","+cp_ToStrODBC(this.w_GMCRIELA)+;
                  ","+cp_ToStrODBC(this.w_GMLISMAG)+;
                  ","+cp_ToStrODBC(this.w_GMELABID)+;
                  ","+cp_ToStrODBCNull(this.w_GMCODINI)+;
                  ","+cp_ToStrODBCNull(this.w_GMCODFIN)+;
                  ","+cp_ToStrODBC(this.w_GMLLCINI)+;
                  ","+cp_ToStrODBC(this.w_GMLLCFIN)+;
                  ","+cp_ToStrODBCNull(this.w_GMFAMAIN)+;
                  ","+cp_ToStrODBCNull(this.w_GMFAMAFI)+;
                  ","+cp_ToStrODBCNull(this.w_GMGRUINI)+;
                  ","+cp_ToStrODBCNull(this.w_GMGRUFIN)+;
                  ","+cp_ToStrODBCNull(this.w_GMCATINI)+;
                  ","+cp_ToStrODBCNull(this.w_GMCATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_GMMAGINI)+;
                  ","+cp_ToStrODBCNull(this.w_GMMAGFIN)+;
                  ","+cp_ToStrODBCNull(this.w_GMMARINI)+;
                  ","+cp_ToStrODBCNull(this.w_GMMARFIN)+;
                  ","+cp_ToStrODBC(this.w_GMPROFIN)+;
                  ","+cp_ToStrODBC(this.w_GMSEMLAV)+;
                  ","+cp_ToStrODBC(this.w_GMMATPRI)+;
                  ","+cp_ToStrODBC(this.w_GMELACAT)+;
                  ","+cp_ToStrODBCNull(this.w_GMCOMINI)+;
                  ","+cp_ToStrODBCNull(this.w_GMCOMFIN)+;
                  ","+cp_ToStrODBC(this.w_GMCOMODL)+;
                  ","+cp_ToStrODBC(this.w_GMNUMINI)+;
                  ","+cp_ToStrODBC(this.w_GMSERIE1)+;
                  ","+cp_ToStrODBC(this.w_GMNUMFIN)+;
                  ","+cp_ToStrODBC(this.w_GMSERIE2)+;
                  ","+cp_ToStrODBC(this.w_GMDOCINI)+;
                  ","+cp_ToStrODBC(this.w_GMDOCFIN)+;
                  ","+cp_ToStrODBCNull(this.w_GMINICLI)+;
                  ","+cp_ToStrODBCNull(this.w_GMFINCLI)+;
                  ","+cp_ToStrODBC(this.w_GMINIELA)+;
                  ","+cp_ToStrODBC(this.w_GMFINELA)+;
                  ","+cp_ToStrODBC(this.w_GMORIODL)+;
                  ","+cp_ToStrODBC(this.w_GMSELIMP)+;
                  ","+cp_ToStrODBC(this.w_GMNOTEEL)+;
                  ","+cp_ToStrODBC(this.w_GMPERPIA)+;
                  ","+cp_ToStrODBC(this.w_GMPIAPUN)+;
                  ","+cp_ToStrODBC(this.w_GMORDPRD)+;
                  ","+cp_ToStrODBC(this.w_GMIMPPRD)+;
                  ","+cp_ToStrODBC(this.w_GMDELORD)+;
                  ","+cp_ToStrODBC(this.w_GUFLAROB)+;
                  ","+cp_ToStrODBC(this.w_GUELPDAS)+;
                  ","+cp_ToStrODBC(this.w_GUELPDAF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SEL__MRP')
        i_extval=cp_InsertValVFPExtFlds(this,'SEL__MRP')
        cp_CheckDeletedKey(i_cTable,0,'GMSERIAL',this.w_GMSERIAL)
        INSERT INTO (i_cTable);
              (GMSERIAL,GMDATELA,GMORAELA,GMUTEELA,GMPATHEL,GMFLULEL,GMTIPDOC,GMMODELA,GMCHKDAT,GMMRPLOG,GMMSGRIP,GMMICOMP,GMMAGFOR,GMMRFODL,GMMAGFOC,GMMRFOCL,GMMAGFOL,GMGENPDA,GMMRFODA,GMMAGFOA,GMGENODA,GMDISMAG,GMGIANEG,GMORDMPS,GMSTAORD,GMCRIFOR,GMCRIELA,GMLISMAG,GMELABID,GMCODINI,GMCODFIN,GMLLCINI,GMLLCFIN,GMFAMAIN,GMFAMAFI,GMGRUINI,GMGRUFIN,GMCATINI,GMCATFIN,GMMAGINI,GMMAGFIN,GMMARINI,GMMARFIN,GMPROFIN,GMSEMLAV,GMMATPRI,GMELACAT,GMCOMINI,GMCOMFIN,GMCOMODL,GMNUMINI,GMSERIE1,GMNUMFIN,GMSERIE2,GMDOCINI,GMDOCFIN,GMINICLI,GMFINCLI,GMINIELA,GMFINELA,GMORIODL,GMSELIMP,GMNOTEEL,GMPERPIA,GMPIAPUN,GMORDPRD,GMIMPPRD,GMDELORD,GUFLAROB,GUELPDAS,GUELPDAF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GMSERIAL;
                  ,this.w_GMDATELA;
                  ,this.w_GMORAELA;
                  ,this.w_GMUTEELA;
                  ,this.w_GMPATHEL;
                  ,this.w_GMFLULEL;
                  ,this.w_GMTIPDOC;
                  ,this.w_GMMODELA;
                  ,this.w_GMCHKDAT;
                  ,this.w_GMMRPLOG;
                  ,this.w_GMMSGRIP;
                  ,this.w_GMMICOMP;
                  ,this.w_GMMAGFOR;
                  ,this.w_GMMRFODL;
                  ,this.w_GMMAGFOC;
                  ,this.w_GMMRFOCL;
                  ,this.w_GMMAGFOL;
                  ,this.w_GMGENPDA;
                  ,this.w_GMMRFODA;
                  ,this.w_GMMAGFOA;
                  ,this.w_GMGENODA;
                  ,this.w_GMDISMAG;
                  ,this.w_GMGIANEG;
                  ,this.w_GMORDMPS;
                  ,this.w_GMSTAORD;
                  ,this.w_GMCRIFOR;
                  ,this.w_GMCRIELA;
                  ,this.w_GMLISMAG;
                  ,this.w_GMELABID;
                  ,this.w_GMCODINI;
                  ,this.w_GMCODFIN;
                  ,this.w_GMLLCINI;
                  ,this.w_GMLLCFIN;
                  ,this.w_GMFAMAIN;
                  ,this.w_GMFAMAFI;
                  ,this.w_GMGRUINI;
                  ,this.w_GMGRUFIN;
                  ,this.w_GMCATINI;
                  ,this.w_GMCATFIN;
                  ,this.w_GMMAGINI;
                  ,this.w_GMMAGFIN;
                  ,this.w_GMMARINI;
                  ,this.w_GMMARFIN;
                  ,this.w_GMPROFIN;
                  ,this.w_GMSEMLAV;
                  ,this.w_GMMATPRI;
                  ,this.w_GMELACAT;
                  ,this.w_GMCOMINI;
                  ,this.w_GMCOMFIN;
                  ,this.w_GMCOMODL;
                  ,this.w_GMNUMINI;
                  ,this.w_GMSERIE1;
                  ,this.w_GMNUMFIN;
                  ,this.w_GMSERIE2;
                  ,this.w_GMDOCINI;
                  ,this.w_GMDOCFIN;
                  ,this.w_GMINICLI;
                  ,this.w_GMFINCLI;
                  ,this.w_GMINIELA;
                  ,this.w_GMFINELA;
                  ,this.w_GMORIODL;
                  ,this.w_GMSELIMP;
                  ,this.w_GMNOTEEL;
                  ,this.w_GMPERPIA;
                  ,this.w_GMPIAPUN;
                  ,this.w_GMORDPRD;
                  ,this.w_GMIMPPRD;
                  ,this.w_GMDELORD;
                  ,this.w_GUFLAROB;
                  ,this.w_GUELPDAS;
                  ,this.w_GUELPDAF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SEL__MRP_IDX,i_nConn)
      *
      * update SEL__MRP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SEL__MRP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GMDATELA="+cp_ToStrODBC(this.w_GMDATELA)+;
             ",GMORAELA="+cp_ToStrODBC(this.w_GMORAELA)+;
             ",GMUTEELA="+cp_ToStrODBCNull(this.w_GMUTEELA)+;
             ",GMPATHEL="+cp_ToStrODBC(this.w_GMPATHEL)+;
             ",GMFLULEL="+cp_ToStrODBC(this.w_GMFLULEL)+;
             ",GMTIPDOC="+cp_ToStrODBC(this.w_GMTIPDOC)+;
             ",GMMODELA="+cp_ToStrODBC(this.w_GMMODELA)+;
             ",GMCHKDAT="+cp_ToStrODBC(this.w_GMCHKDAT)+;
             ",GMMRPLOG="+cp_ToStrODBC(this.w_GMMRPLOG)+;
             ",GMMSGRIP="+cp_ToStrODBC(this.w_GMMSGRIP)+;
             ",GMMICOMP="+cp_ToStrODBC(this.w_GMMICOMP)+;
             ",GMMAGFOR="+cp_ToStrODBCNull(this.w_GMMAGFOR)+;
             ",GMMRFODL="+cp_ToStrODBC(this.w_GMMRFODL)+;
             ",GMMAGFOC="+cp_ToStrODBCNull(this.w_GMMAGFOC)+;
             ",GMMRFOCL="+cp_ToStrODBC(this.w_GMMRFOCL)+;
             ",GMMAGFOL="+cp_ToStrODBCNull(this.w_GMMAGFOL)+;
             ",GMGENPDA="+cp_ToStrODBC(this.w_GMGENPDA)+;
             ",GMMRFODA="+cp_ToStrODBC(this.w_GMMRFODA)+;
             ",GMMAGFOA="+cp_ToStrODBCNull(this.w_GMMAGFOA)+;
             ",GMGENODA="+cp_ToStrODBC(this.w_GMGENODA)+;
             ",GMDISMAG="+cp_ToStrODBC(this.w_GMDISMAG)+;
             ",GMGIANEG="+cp_ToStrODBC(this.w_GMGIANEG)+;
             ",GMORDMPS="+cp_ToStrODBC(this.w_GMORDMPS)+;
             ",GMSTAORD="+cp_ToStrODBC(this.w_GMSTAORD)+;
             ",GMCRIFOR="+cp_ToStrODBC(this.w_GMCRIFOR)+;
             ",GMCRIELA="+cp_ToStrODBC(this.w_GMCRIELA)+;
             ",GMLISMAG="+cp_ToStrODBC(this.w_GMLISMAG)+;
             ",GMELABID="+cp_ToStrODBC(this.w_GMELABID)+;
             ",GMCODINI="+cp_ToStrODBCNull(this.w_GMCODINI)+;
             ",GMCODFIN="+cp_ToStrODBCNull(this.w_GMCODFIN)+;
             ",GMLLCINI="+cp_ToStrODBC(this.w_GMLLCINI)+;
             ",GMLLCFIN="+cp_ToStrODBC(this.w_GMLLCFIN)+;
             ",GMFAMAIN="+cp_ToStrODBCNull(this.w_GMFAMAIN)+;
             ",GMFAMAFI="+cp_ToStrODBCNull(this.w_GMFAMAFI)+;
             ",GMGRUINI="+cp_ToStrODBCNull(this.w_GMGRUINI)+;
             ",GMGRUFIN="+cp_ToStrODBCNull(this.w_GMGRUFIN)+;
             ",GMCATINI="+cp_ToStrODBCNull(this.w_GMCATINI)+;
             ",GMCATFIN="+cp_ToStrODBCNull(this.w_GMCATFIN)+;
             ",GMMAGINI="+cp_ToStrODBCNull(this.w_GMMAGINI)+;
             ",GMMAGFIN="+cp_ToStrODBCNull(this.w_GMMAGFIN)+;
             ",GMMARINI="+cp_ToStrODBCNull(this.w_GMMARINI)+;
             ",GMMARFIN="+cp_ToStrODBCNull(this.w_GMMARFIN)+;
             ",GMPROFIN="+cp_ToStrODBC(this.w_GMPROFIN)+;
             ",GMSEMLAV="+cp_ToStrODBC(this.w_GMSEMLAV)+;
             ",GMMATPRI="+cp_ToStrODBC(this.w_GMMATPRI)+;
             ",GMELACAT="+cp_ToStrODBC(this.w_GMELACAT)+;
             ",GMCOMINI="+cp_ToStrODBCNull(this.w_GMCOMINI)+;
             ",GMCOMFIN="+cp_ToStrODBCNull(this.w_GMCOMFIN)+;
             ",GMCOMODL="+cp_ToStrODBC(this.w_GMCOMODL)+;
             ",GMNUMINI="+cp_ToStrODBC(this.w_GMNUMINI)+;
             ",GMSERIE1="+cp_ToStrODBC(this.w_GMSERIE1)+;
             ",GMNUMFIN="+cp_ToStrODBC(this.w_GMNUMFIN)+;
             ",GMSERIE2="+cp_ToStrODBC(this.w_GMSERIE2)+;
             ",GMDOCINI="+cp_ToStrODBC(this.w_GMDOCINI)+;
             ",GMDOCFIN="+cp_ToStrODBC(this.w_GMDOCFIN)+;
             ",GMINICLI="+cp_ToStrODBCNull(this.w_GMINICLI)+;
             ",GMFINCLI="+cp_ToStrODBCNull(this.w_GMFINCLI)+;
             ",GMINIELA="+cp_ToStrODBC(this.w_GMINIELA)+;
             ",GMFINELA="+cp_ToStrODBC(this.w_GMFINELA)+;
             ",GMORIODL="+cp_ToStrODBC(this.w_GMORIODL)+;
             ",GMSELIMP="+cp_ToStrODBC(this.w_GMSELIMP)+;
             ",GMNOTEEL="+cp_ToStrODBC(this.w_GMNOTEEL)+;
             ",GMPERPIA="+cp_ToStrODBC(this.w_GMPERPIA)+;
             ",GMPIAPUN="+cp_ToStrODBC(this.w_GMPIAPUN)+;
             ",GMORDPRD="+cp_ToStrODBC(this.w_GMORDPRD)+;
             ",GMIMPPRD="+cp_ToStrODBC(this.w_GMIMPPRD)+;
             ",GMDELORD="+cp_ToStrODBC(this.w_GMDELORD)+;
             ",GUFLAROB="+cp_ToStrODBC(this.w_GUFLAROB)+;
             ",GUELPDAS="+cp_ToStrODBC(this.w_GUELPDAS)+;
             ",GUELPDAF="+cp_ToStrODBC(this.w_GUELPDAF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SEL__MRP')
        i_cWhere = cp_PKFox(i_cTable  ,'GMSERIAL',this.w_GMSERIAL  )
        UPDATE (i_cTable) SET;
              GMDATELA=this.w_GMDATELA;
             ,GMORAELA=this.w_GMORAELA;
             ,GMUTEELA=this.w_GMUTEELA;
             ,GMPATHEL=this.w_GMPATHEL;
             ,GMFLULEL=this.w_GMFLULEL;
             ,GMTIPDOC=this.w_GMTIPDOC;
             ,GMMODELA=this.w_GMMODELA;
             ,GMCHKDAT=this.w_GMCHKDAT;
             ,GMMRPLOG=this.w_GMMRPLOG;
             ,GMMSGRIP=this.w_GMMSGRIP;
             ,GMMICOMP=this.w_GMMICOMP;
             ,GMMAGFOR=this.w_GMMAGFOR;
             ,GMMRFODL=this.w_GMMRFODL;
             ,GMMAGFOC=this.w_GMMAGFOC;
             ,GMMRFOCL=this.w_GMMRFOCL;
             ,GMMAGFOL=this.w_GMMAGFOL;
             ,GMGENPDA=this.w_GMGENPDA;
             ,GMMRFODA=this.w_GMMRFODA;
             ,GMMAGFOA=this.w_GMMAGFOA;
             ,GMGENODA=this.w_GMGENODA;
             ,GMDISMAG=this.w_GMDISMAG;
             ,GMGIANEG=this.w_GMGIANEG;
             ,GMORDMPS=this.w_GMORDMPS;
             ,GMSTAORD=this.w_GMSTAORD;
             ,GMCRIFOR=this.w_GMCRIFOR;
             ,GMCRIELA=this.w_GMCRIELA;
             ,GMLISMAG=this.w_GMLISMAG;
             ,GMELABID=this.w_GMELABID;
             ,GMCODINI=this.w_GMCODINI;
             ,GMCODFIN=this.w_GMCODFIN;
             ,GMLLCINI=this.w_GMLLCINI;
             ,GMLLCFIN=this.w_GMLLCFIN;
             ,GMFAMAIN=this.w_GMFAMAIN;
             ,GMFAMAFI=this.w_GMFAMAFI;
             ,GMGRUINI=this.w_GMGRUINI;
             ,GMGRUFIN=this.w_GMGRUFIN;
             ,GMCATINI=this.w_GMCATINI;
             ,GMCATFIN=this.w_GMCATFIN;
             ,GMMAGINI=this.w_GMMAGINI;
             ,GMMAGFIN=this.w_GMMAGFIN;
             ,GMMARINI=this.w_GMMARINI;
             ,GMMARFIN=this.w_GMMARFIN;
             ,GMPROFIN=this.w_GMPROFIN;
             ,GMSEMLAV=this.w_GMSEMLAV;
             ,GMMATPRI=this.w_GMMATPRI;
             ,GMELACAT=this.w_GMELACAT;
             ,GMCOMINI=this.w_GMCOMINI;
             ,GMCOMFIN=this.w_GMCOMFIN;
             ,GMCOMODL=this.w_GMCOMODL;
             ,GMNUMINI=this.w_GMNUMINI;
             ,GMSERIE1=this.w_GMSERIE1;
             ,GMNUMFIN=this.w_GMNUMFIN;
             ,GMSERIE2=this.w_GMSERIE2;
             ,GMDOCINI=this.w_GMDOCINI;
             ,GMDOCFIN=this.w_GMDOCFIN;
             ,GMINICLI=this.w_GMINICLI;
             ,GMFINCLI=this.w_GMFINCLI;
             ,GMINIELA=this.w_GMINIELA;
             ,GMFINELA=this.w_GMFINELA;
             ,GMORIODL=this.w_GMORIODL;
             ,GMSELIMP=this.w_GMSELIMP;
             ,GMNOTEEL=this.w_GMNOTEEL;
             ,GMPERPIA=this.w_GMPERPIA;
             ,GMPIAPUN=this.w_GMPIAPUN;
             ,GMORDPRD=this.w_GMORDPRD;
             ,GMIMPPRD=this.w_GMIMPPRD;
             ,GMDELORD=this.w_GMDELORD;
             ,GUFLAROB=this.w_GUFLAROB;
             ,GUELPDAS=this.w_GUELPDAS;
             ,GUELPDAF=this.w_GUELPDAF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SEL__MRP_IDX,i_nConn)
      *
      * delete SEL__MRP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GMSERIAL',this.w_GMSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsmr_agm
        l_DBFdacanc=(alltrim(this.w_GMPATHEL)+'elab'+alltrim(i_CODAZI)+alltrim(this.w_GMSERIAL)+'.DBF')
        l_DBFbisdacanc=(alltrim(this.w_GMPATHEL)+'elab'+alltrim(i_CODAZI)+alltrim(this.w_GMSERIAL)+'_bis.DBF')
        l_LOGdacanc=(alltrim(this.w_GMPATHEL)+'elab'+alltrim(i_CODAZI)+alltrim(this.w_GMSERIAL)+'.LOG')
        l_PEGdacanc=(alltrim(this.w_GMPATHEL)+'elab'+alltrim(i_CODAZI)+alltrim(this.w_GMSERIAL)+'_PEG.LOG')
        l_Filename=''
        l_ErrorDel=.f.
        l_OldOnError=ON('Error')
        if file(l_DBFdacanc)
           ah_msg('Eliminazione file %1 in corso',.t.,.f.,.f.,l_DBFdacanc)
           ON Error l_ErrorDel=.t.
           l_Filename = l_DBFdacanc
           delete file (l_DBFdacanc)
           if file(l_DBFbisdacanc)
              ah_msg('Eliminazione file %1 in corso',.t.,.f.,.f.,l_DBFbisdacanc)
              ON Error l_ErrorDel=.t.
              l_Filename = l_DBFbisdacanc
              delete file (l_DBFbisdacanc)
           endif
           if this.w_GMMRPLOG='S'
            if file(l_LOGdacanc) or file(l_PEGdacanc)
             if ah_yesno("Elimino anche i file di log dell'elaborazione?")
              if file(l_LOGdacanc)
                ah_msg('Eliminazione file %1 in corso',.t.,.f.,.f.,l_LOGdacanc)
                l_Filename = l_LOGdacanc
                delete file(l_LOGdacanc)
              endif
              if file(l_PEGdacanc)
                ah_msg('Eliminazione file %1 in corso',.t.,.f.,.f.,l_PEGdacanc)
                l_Filename = l_PEGdacanc
                delete file(l_PEGdacanc)
              endif
             endif
            endif
           endif
           * Ripristina ON ERROR
           if not(empty(l_OldOnError))
              ON Error &l_OldOnError
             else
              ON Error
           endif
           if l_ErrorDel
             i_TrsMsg=AH_Msgformat("Impossibile eliminare il file %1. %0Verificare che tale file non sia in uso. Operazione sospesa", alltrim(l_Filename))
             btrserr=.t.
           endif
         else
           if ah_yesno("File %1 non trovato. %0Si vuole eliminare comunque il seriale di elaborazione?",'', l_DBFdacanc)
              *Elimino comunque il Seriale di Elaborazione
            else
              *Blocco l'elaborazione
              i_TrsMsg=AH_Msgformat("Operazione sospesa dall'utente")
              btrserr=.t.
           endif
        endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .DoRTCalc(6,9,.t.)
            .w_GFPATHFILE = alltrim(.w_GMPATHEL)+"elab"+alltrim(i_CODAZI)+alltrim(.w_GMSERIAL)+".log"
            .w_GFNOMEFILE = alltrim(.w_GFPATHFILE)+"elab"+alltrim(i_CODAZI)+alltrim(.w_GMSERIAL)+".log"
        .DoRTCalc(12,16,.t.)
          .link_1_22('Full')
        .DoRTCalc(18,18,.t.)
          .link_1_24('Full')
        .DoRTCalc(20,20,.t.)
          .link_1_26('Full')
        .DoRTCalc(22,23,.t.)
          .link_1_29('Full')
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_GMCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .DoRTCalc(25,37,.t.)
          .link_2_5('Full')
          .link_2_6('Full')
        .DoRTCalc(40,41,.t.)
          .link_2_9('Full')
          .link_2_10('Full')
          .link_2_11('Full')
          .link_2_12('Full')
          .link_2_13('Full')
          .link_2_14('Full')
          .link_2_15('Full')
          .link_2_16('Full')
          .link_2_17('Full')
          .link_2_18('Full')
        .DoRTCalc(52,67,.t.)
          .link_2_50('Full')
          .link_2_51('Full')
        .DoRTCalc(70,76,.t.)
          .link_2_59('Full')
          .link_2_60('Full')
        .oPgFrm.Page2.oPag.SZOOM1.Calculate()
        if .o_GMCRIELA<>.w_GMCRIELA
          .Calculate_AHEHLWIYIW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(79,97,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_GMCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .oPgFrm.Page2.oPag.SZOOM1.Calculate()
    endwith
  return

  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          .w_ZOOMMAGA.cZoomFile = IIF(.w_GMCRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_GMCRIELA = 'G', "QUERY\GSVEGKGF" , "QUERY\GSVEFKGF")
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_GMCRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_AHEHLWIYIW()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_GMCRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_GMCRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_GMCRIELA $ 'G-M'
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_CYXNTVWJYH()
    with this
          * --- Assegna w_KEYRIF
          .w_KEYRIF = sys(2015)
    endwith
  endproc
  proc Calculate_GBXSIMSAOR()
    with this
          * --- GSMA_BFM - Load
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_GMCRIELA;
              ,"N";
             )
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_QYNAVQZUKT()
    with this
          * --- GSMA_BFM - Load
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_GMCRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_GMCRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_GMCRIELA $ 'G-M'
          .w_ZOOMMAGA.cZoomFile = IIF(.w_GMCRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_GMCRIELA = 'G', "QUERY\GSVEGNKGF" , "QUERY\GSVEFNKGF")
      if .w_GMCRIELA $ 'G-M' AND !empty(.w_GMLISMAG)
          GSAR_BFM(this;
              ,"CARICADATO";
              ,.w_KEYRIF;
              ,.w_GMCRIELA;
              ,"N";
              ,.w_GMLISMAG;
             )
      endif
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_FTUTTITKPH()
    with this
          * --- GSMA_BFM - Load
          .w_nColXCHK = .w_ZOOMMAGA.Grd.ColumnCount
      if .w_nColXCHK > 0
          .W_ZOOMMAGA.grd.Columns(.w_nColXCHK).Enabled = Not Inlist(.cFunction, "Query", "FIlter")
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.visible=!this.oPgFrm.Page1.oPag.oBtn_1_16.mHide()
    this.oPgFrm.Page1.oPag.oGMMRFODA_1_28.visible=!this.oPgFrm.Page1.oPag.oGMMRFODA_1_28.mHide()
    this.oPgFrm.Page1.oPag.oGMMAGFOA_1_29.visible=!this.oPgFrm.Page1.oPag.oGMMAGFOA_1_29.mHide()
    this.oPgFrm.Page1.oPag.oGMGENODA_1_30.visible=!this.oPgFrm.Page1.oPag.oGMGENODA_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDESMOA_1_52.visible=!this.oPgFrm.Page1.oPag.oDESMOA_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page1.oPag.oGUFLAROB_1_83.visible=!this.oPgFrm.Page1.oPag.oGUFLAROB_1_83.mHide()
    this.oPgFrm.Page1.oPag.oGUELPDAS_1_84.visible=!this.oPgFrm.Page1.oPag.oGUELPDAS_1_84.mHide()
    this.oPgFrm.Page1.oPag.oGUELPDAF_1_85.visible=!this.oPgFrm.Page1.oPag.oGUELPDAF_1_85.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_2.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLMAGA.Event(cEvent)
      .oPgFrm.Page2.oPag.SZOOM1.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Load")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init") or lower(cEvent)==lower("Load")
          .Calculate_AHEHLWIYIW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_CYXNTVWJYH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_GBXSIMSAOR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_QYNAVQZUKT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Edit Aborted") or lower(cEvent)==lower("Edit Restarted") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Load") or lower(cEvent)==lower("New record") or lower(cEvent)==lower("New record aborted") or lower(cEvent)==lower("SetEnabledZoom")
          .Calculate_FTUTTITKPH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsmr_agm
    if upper(cEvent)='FORMLOAD'
    *Abilito la sola barra di scorrimento Vericale
    this.w_SZoom1.GRD.SCROLLBARS=2
    this.w_ZOOMMAGA.GRD.SCROLLBARS=2
    *Disabilito la colonna XCHK per evitare di selezionare/deselezionare
    this.w_SZoom1.grd.column3.enabled=.F.
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GMUTEELA
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMUTEELA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMUTEELA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_GMUTEELA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_GMUTEELA)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMUTEELA = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_GMUTEELA = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMUTEELA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GMMAGFOR
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMAGFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMAGFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GMMAGFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GMMAGFOR)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMAGFOR = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMFR = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMMAGFOR = space(5)
      endif
      this.w_DESMFR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMAGFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.MGCODMAG as MGCODMAG122"+ ",link_1_22.MGDESMAG as MGDESMAG122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on SEL__MRP.GMMAGFOR=link_1_22.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMAGFOR=link_1_22.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMAGFOC
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMAGFOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMAGFOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GMMAGFOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GMMAGFOC)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMAGFOC = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMOC = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMMAGFOC = space(5)
      endif
      this.w_DESMOC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMAGFOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.MGCODMAG as MGCODMAG124"+ ",link_1_24.MGDESMAG as MGDESMAG124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on SEL__MRP.GMMAGFOC=link_1_24.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMAGFOC=link_1_24.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMAGFOL
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMAGFOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMAGFOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GMMAGFOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GMMAGFOL)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMAGFOL = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMOL = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMMAGFOL = space(5)
      endif
      this.w_DESMOL = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMAGFOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.MGCODMAG as MGCODMAG126"+ ",link_1_26.MGDESMAG as MGDESMAG126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on SEL__MRP.GMMAGFOL=link_1_26.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMAGFOL=link_1_26.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMAGFOA
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMAGFOA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMAGFOA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GMMAGFOA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GMMAGFOA)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMAGFOA = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMOA = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMMAGFOA = space(5)
      endif
      this.w_DESMOA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMAGFOA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.MGCODMAG as MGCODMAG129"+ ",link_1_29.MGDESMAG as MGDESMAG129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on SEL__MRP.GMMAGFOA=link_1_29.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMAGFOA=link_1_29.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMCODINI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_GMCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_GMCODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GMCODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ARCODART as ARCODART205"+ ",link_2_5.ARDESART as ARDESART205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on SEL__MRP.GMCODINI=link_2_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and SEL__MRP.GMCODINI=link_2_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMCODFIN
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_GMCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_GMCODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GMCODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.ARCODART as ARCODART206"+ ",link_2_6.ARDESART as ARDESART206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on SEL__MRP.GMCODFIN=link_2_6.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and SEL__MRP.GMCODFIN=link_2_6.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMFAMAIN
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMFAMAIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMFAMAIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_GMFAMAIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_GMFAMAIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMFAMAIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMFAMAIN = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMFAMAIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.FACODICE as FACODICE209"+ ","+cp_TransLinkFldName('link_2_9.FADESCRI')+" as FADESCRI209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on SEL__MRP.GMFAMAIN=link_2_9.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and SEL__MRP.GMFAMAIN=link_2_9.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMFAMAFI
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMFAMAFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMFAMAFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_GMFAMAFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_GMFAMAFI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMFAMAFI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMFAMAFI = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMFAMAFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.FACODICE as FACODICE210"+ ","+cp_TransLinkFldName('link_2_10.FADESCRI')+" as FADESCRI210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on SEL__MRP.GMFAMAFI=link_2_10.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and SEL__MRP.GMFAMAFI=link_2_10.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMGRUINI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GMGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GMGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMGRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.GMCODICE as GMCODICE211"+ ","+cp_TransLinkFldName('link_2_11.GMDESCRI')+" as GMDESCRI211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on SEL__MRP.GMGRUINI=link_2_11.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and SEL__MRP.GMGRUINI=link_2_11.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMGRUFIN
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GMGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GMGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.GMCODICE as GMCODICE212"+ ","+cp_TransLinkFldName('link_2_12.GMDESCRI')+" as GMDESCRI212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on SEL__MRP.GMGRUFIN=link_2_12.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and SEL__MRP.GMGRUFIN=link_2_12.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMCATINI
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_GMCATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_GMCATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMCATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.OMCODICE as OMCODICE213"+ ","+cp_TransLinkFldName('link_2_13.OMDESCRI')+" as OMDESCRI213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on SEL__MRP.GMCATINI=link_2_13.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and SEL__MRP.GMCATINI=link_2_13.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMCATFIN
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_GMCATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_GMCATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMCATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.OMCODICE as OMCODICE214"+ ","+cp_TransLinkFldName('link_2_14.OMDESCRI')+" as OMDESCRI214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on SEL__MRP.GMCATFIN=link_2_14.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and SEL__MRP.GMCATFIN=link_2_14.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMAGINI
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GMMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GMMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.MGCODMAG as MGCODMAG215"+ ",link_2_15.MGDESMAG as MGDESMAG215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on SEL__MRP.GMMAGINI=link_2_15.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMAGINI=link_2_15.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMAGFIN
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GMMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GMMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.MGCODMAG as MGCODMAG216"+ ",link_2_16.MGDESMAG as MGDESMAG216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on SEL__MRP.GMMAGFIN=link_2_16.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMAGFIN=link_2_16.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMARINI
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_GMMARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_GMMARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMMARINI = space(5)
      endif
      this.w_DESMARI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MARCHI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.MACODICE as MACODICE217"+ ",link_2_17.MADESCRI as MADESCRI217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on SEL__MRP.GMMARINI=link_2_17.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMARINI=link_2_17.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMMARFIN
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMMARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMMARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_GMMARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_GMMARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMMARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GMMARFIN = space(5)
      endif
      this.w_DESMARF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMMARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MARCHI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_18.MACODICE as MACODICE218"+ ",link_2_18.MADESCRI as MADESCRI218"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_18 on SEL__MRP.GMMARFIN=link_2_18.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_18"
          i_cKey=i_cKey+'+" and SEL__MRP.GMMARFIN=link_2_18.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMCOMINI
  func Link_2_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_GMCOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_GMCOMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCOMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMI = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMCOMINI = space(15)
      endif
      this.w_DESCOMI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_50(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_50.CNCODCAN as CNCODCAN250"+ ",link_2_50.CNDESCAN as CNDESCAN250"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_50 on SEL__MRP.GMCOMINI=link_2_50.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_50"
          i_cKey=i_cKey+'+" and SEL__MRP.GMCOMINI=link_2_50.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMCOMFIN
  func Link_2_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCOMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCOMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_GMCOMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_GMCOMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCOMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_GMCOMFIN = space(15)
      endif
      this.w_DESCOMF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCOMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_51(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_51.CNCODCAN as CNCODCAN251"+ ",link_2_51.CNDESCAN as CNDESCAN251"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_51 on SEL__MRP.GMCOMFIN=link_2_51.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_51"
          i_cKey=i_cKey+'+" and SEL__MRP.GMCOMFIN=link_2_51.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GMINICLI
  func Link_2_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMINICLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMINICLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GMINICLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_GMINICLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMINICLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLII = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GMINICLI = space(15)
      endif
      this.w_DESCLII = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMINICLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GMFINCLI
  func Link_2_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMFINCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMFINCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GMFINCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_GMFINCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMFINCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLIF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GMFINCLI = space(15)
      endif
      this.w_DESCLIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMFINCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGMSERIAL_1_2.value==this.w_GMSERIAL)
      this.oPgFrm.Page1.oPag.oGMSERIAL_1_2.value=this.w_GMSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGMDATELA_1_3.value==this.w_GMDATELA)
      this.oPgFrm.Page1.oPag.oGMDATELA_1_3.value=this.w_GMDATELA
    endif
    if not(this.oPgFrm.Page1.oPag.oGMORAELA_1_4.value==this.w_GMORAELA)
      this.oPgFrm.Page1.oPag.oGMORAELA_1_4.value=this.w_GMORAELA
    endif
    if not(this.oPgFrm.Page1.oPag.oGMUTEELA_1_5.value==this.w_GMUTEELA)
      this.oPgFrm.Page1.oPag.oGMUTEELA_1_5.value=this.w_GMUTEELA
    endif
    if not(this.oPgFrm.Page1.oPag.oGMPATHEL_1_6.value==this.w_GMPATHEL)
      this.oPgFrm.Page1.oPag.oGMPATHEL_1_6.value=this.w_GMPATHEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_11.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_11.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMODELA_1_17.RadioValue()==this.w_GMMODELA)
      this.oPgFrm.Page1.oPag.oGMMODELA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMCHKDAT_1_18.RadioValue()==this.w_GMCHKDAT)
      this.oPgFrm.Page1.oPag.oGMCHKDAT_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMRPLOG_1_19.RadioValue()==this.w_GMMRPLOG)
      this.oPgFrm.Page1.oPag.oGMMRPLOG_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMSGRIP_1_20.RadioValue()==this.w_GMMSGRIP)
      this.oPgFrm.Page1.oPag.oGMMSGRIP_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMICOMP_1_21.RadioValue()==this.w_GMMICOMP)
      this.oPgFrm.Page1.oPag.oGMMICOMP_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMAGFOR_1_22.value==this.w_GMMAGFOR)
      this.oPgFrm.Page1.oPag.oGMMAGFOR_1_22.value=this.w_GMMAGFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMRFODL_1_23.RadioValue()==this.w_GMMRFODL)
      this.oPgFrm.Page1.oPag.oGMMRFODL_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMAGFOC_1_24.value==this.w_GMMAGFOC)
      this.oPgFrm.Page1.oPag.oGMMAGFOC_1_24.value=this.w_GMMAGFOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMRFOCL_1_25.RadioValue()==this.w_GMMRFOCL)
      this.oPgFrm.Page1.oPag.oGMMRFOCL_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMAGFOL_1_26.value==this.w_GMMAGFOL)
      this.oPgFrm.Page1.oPag.oGMMAGFOL_1_26.value=this.w_GMMAGFOL
    endif
    if not(this.oPgFrm.Page1.oPag.oGMGENPDA_1_27.RadioValue()==this.w_GMGENPDA)
      this.oPgFrm.Page1.oPag.oGMGENPDA_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMRFODA_1_28.RadioValue()==this.w_GMMRFODA)
      this.oPgFrm.Page1.oPag.oGMMRFODA_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMMAGFOA_1_29.value==this.w_GMMAGFOA)
      this.oPgFrm.Page1.oPag.oGMMAGFOA_1_29.value=this.w_GMMAGFOA
    endif
    if not(this.oPgFrm.Page1.oPag.oGMGENODA_1_30.RadioValue()==this.w_GMGENODA)
      this.oPgFrm.Page1.oPag.oGMGENODA_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMDISMAG_1_31.RadioValue()==this.w_GMDISMAG)
      this.oPgFrm.Page1.oPag.oGMDISMAG_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMGIANEG_1_32.RadioValue()==this.w_GMGIANEG)
      this.oPgFrm.Page1.oPag.oGMGIANEG_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMORDMPS_1_33.RadioValue()==this.w_GMORDMPS)
      this.oPgFrm.Page1.oPag.oGMORDMPS_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMSTAORD_1_34.RadioValue()==this.w_GMSTAORD)
      this.oPgFrm.Page1.oPag.oGMSTAORD_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMFR_1_49.value==this.w_DESMFR)
      this.oPgFrm.Page1.oPag.oDESMFR_1_49.value=this.w_DESMFR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOC_1_50.value==this.w_DESMOC)
      this.oPgFrm.Page1.oPag.oDESMOC_1_50.value=this.w_DESMOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOL_1_51.value==this.w_DESMOL)
      this.oPgFrm.Page1.oPag.oDESMOL_1_51.value=this.w_DESMOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOA_1_52.value==this.w_DESMOA)
      this.oPgFrm.Page1.oPag.oDESMOA_1_52.value=this.w_DESMOA
    endif
    if not(this.oPgFrm.Page1.oPag.oGMCRIFOR_1_55.RadioValue()==this.w_GMCRIFOR)
      this.oPgFrm.Page1.oPag.oGMCRIFOR_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMCRIELA_1_56.RadioValue()==this.w_GMCRIELA)
      this.oPgFrm.Page1.oPag.oGMCRIELA_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMELABID_1_61.RadioValue()==this.w_GMELABID)
      this.oPgFrm.Page1.oPag.oGMELABID_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCODINI_2_5.value==this.w_GMCODINI)
      this.oPgFrm.Page2.oPag.oGMCODINI_2_5.value=this.w_GMCODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCODFIN_2_6.value==this.w_GMCODFIN)
      this.oPgFrm.Page2.oPag.oGMCODFIN_2_6.value=this.w_GMCODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMLLCINI_2_7.value==this.w_GMLLCINI)
      this.oPgFrm.Page2.oPag.oGMLLCINI_2_7.value=this.w_GMLLCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMLLCFIN_2_8.value==this.w_GMLLCFIN)
      this.oPgFrm.Page2.oPag.oGMLLCFIN_2_8.value=this.w_GMLLCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMFAMAIN_2_9.value==this.w_GMFAMAIN)
      this.oPgFrm.Page2.oPag.oGMFAMAIN_2_9.value=this.w_GMFAMAIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMFAMAFI_2_10.value==this.w_GMFAMAFI)
      this.oPgFrm.Page2.oPag.oGMFAMAFI_2_10.value=this.w_GMFAMAFI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMGRUINI_2_11.value==this.w_GMGRUINI)
      this.oPgFrm.Page2.oPag.oGMGRUINI_2_11.value=this.w_GMGRUINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMGRUFIN_2_12.value==this.w_GMGRUFIN)
      this.oPgFrm.Page2.oPag.oGMGRUFIN_2_12.value=this.w_GMGRUFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCATINI_2_13.value==this.w_GMCATINI)
      this.oPgFrm.Page2.oPag.oGMCATINI_2_13.value=this.w_GMCATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCATFIN_2_14.value==this.w_GMCATFIN)
      this.oPgFrm.Page2.oPag.oGMCATFIN_2_14.value=this.w_GMCATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMMAGINI_2_15.value==this.w_GMMAGINI)
      this.oPgFrm.Page2.oPag.oGMMAGINI_2_15.value=this.w_GMMAGINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMMAGFIN_2_16.value==this.w_GMMAGFIN)
      this.oPgFrm.Page2.oPag.oGMMAGFIN_2_16.value=this.w_GMMAGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMMARINI_2_17.value==this.w_GMMARINI)
      this.oPgFrm.Page2.oPag.oGMMARINI_2_17.value=this.w_GMMARINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMMARFIN_2_18.value==this.w_GMMARFIN)
      this.oPgFrm.Page2.oPag.oGMMARFIN_2_18.value=this.w_GMMARFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMPROFIN_2_19.RadioValue()==this.w_GMPROFIN)
      this.oPgFrm.Page2.oPag.oGMPROFIN_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMSEMLAV_2_20.RadioValue()==this.w_GMSEMLAV)
      this.oPgFrm.Page2.oPag.oGMSEMLAV_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMMATPRI_2_21.RadioValue()==this.w_GMMATPRI)
      this.oPgFrm.Page2.oPag.oGMMATPRI_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMELACAT_2_22.RadioValue()==this.w_GMELACAT)
      this.oPgFrm.Page2.oPag.oGMELACAT_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGI_2_24.value==this.w_DESMAGI)
      this.oPgFrm.Page2.oPag.oDESMAGI_2_24.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGF_2_26.value==this.w_DESMAGF)
      this.oPgFrm.Page2.oPag.oDESMAGF_2_26.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESINI_2_27.value==this.w_DESINI)
      this.oPgFrm.Page2.oPag.oDESINI_2_27.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_2_29.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_2_29.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAI_2_31.value==this.w_DESFAMAI)
      this.oPgFrm.Page2.oPag.oDESFAMAI_2_31.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUI_2_32.value==this.w_DESGRUI)
      this.oPgFrm.Page2.oPag.oDESGRUI_2_32.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATI_2_33.value==this.w_DESCATI)
      this.oPgFrm.Page2.oPag.oDESCATI_2_33.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAF_2_37.value==this.w_DESFAMAF)
      this.oPgFrm.Page2.oPag.oDESFAMAF_2_37.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUF_2_38.value==this.w_DESGRUF)
      this.oPgFrm.Page2.oPag.oDESGRUF_2_38.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATF_2_39.value==this.w_DESCATF)
      this.oPgFrm.Page2.oPag.oDESCATF_2_39.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARI_2_47.value==this.w_DESMARI)
      this.oPgFrm.Page2.oPag.oDESMARI_2_47.value=this.w_DESMARI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARF_2_49.value==this.w_DESMARF)
      this.oPgFrm.Page2.oPag.oDESMARF_2_49.value=this.w_DESMARF
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCOMINI_2_50.value==this.w_GMCOMINI)
      this.oPgFrm.Page2.oPag.oGMCOMINI_2_50.value=this.w_GMCOMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCOMFIN_2_51.value==this.w_GMCOMFIN)
      this.oPgFrm.Page2.oPag.oGMCOMFIN_2_51.value=this.w_GMCOMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMCOMODL_2_52.RadioValue()==this.w_GMCOMODL)
      this.oPgFrm.Page2.oPag.oGMCOMODL_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMNUMINI_2_53.value==this.w_GMNUMINI)
      this.oPgFrm.Page2.oPag.oGMNUMINI_2_53.value=this.w_GMNUMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMSERIE1_2_54.value==this.w_GMSERIE1)
      this.oPgFrm.Page2.oPag.oGMSERIE1_2_54.value=this.w_GMSERIE1
    endif
    if not(this.oPgFrm.Page2.oPag.oGMNUMFIN_2_55.value==this.w_GMNUMFIN)
      this.oPgFrm.Page2.oPag.oGMNUMFIN_2_55.value=this.w_GMNUMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMSERIE2_2_56.value==this.w_GMSERIE2)
      this.oPgFrm.Page2.oPag.oGMSERIE2_2_56.value=this.w_GMSERIE2
    endif
    if not(this.oPgFrm.Page2.oPag.oGMDOCINI_2_57.value==this.w_GMDOCINI)
      this.oPgFrm.Page2.oPag.oGMDOCINI_2_57.value=this.w_GMDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMDOCFIN_2_58.value==this.w_GMDOCFIN)
      this.oPgFrm.Page2.oPag.oGMDOCFIN_2_58.value=this.w_GMDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGMINICLI_2_59.value==this.w_GMINICLI)
      this.oPgFrm.Page2.oPag.oGMINICLI_2_59.value=this.w_GMINICLI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMFINCLI_2_60.value==this.w_GMFINCLI)
      this.oPgFrm.Page2.oPag.oGMFINCLI_2_60.value=this.w_GMFINCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oGMINIELA_2_61.value==this.w_GMINIELA)
      this.oPgFrm.Page2.oPag.oGMINIELA_2_61.value=this.w_GMINIELA
    endif
    if not(this.oPgFrm.Page2.oPag.oGMFINELA_2_62.value==this.w_GMFINELA)
      this.oPgFrm.Page2.oPag.oGMFINELA_2_62.value=this.w_GMFINELA
    endif
    if not(this.oPgFrm.Page2.oPag.oGMORIODL_2_63.RadioValue()==this.w_GMORIODL)
      this.oPgFrm.Page2.oPag.oGMORIODL_2_63.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLII_2_70.value==this.w_DESCLII)
      this.oPgFrm.Page2.oPag.oDESCLII_2_70.value=this.w_DESCLII
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLIF_2_71.value==this.w_DESCLIF)
      this.oPgFrm.Page2.oPag.oDESCLIF_2_71.value=this.w_DESCLIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMI_2_73.value==this.w_DESCOMI)
      this.oPgFrm.Page2.oPag.oDESCOMI_2_73.value=this.w_DESCOMI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMF_2_75.value==this.w_DESCOMF)
      this.oPgFrm.Page2.oPag.oDESCOMF_2_75.value=this.w_DESCOMF
    endif
    if not(this.oPgFrm.Page2.oPag.oGMSELIMP_2_83.RadioValue()==this.w_GMSELIMP)
      this.oPgFrm.Page2.oPag.oGMSELIMP_2_83.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMPERPIA_1_67.RadioValue()==this.w_GMPERPIA)
      this.oPgFrm.Page1.oPag.oGMPERPIA_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMPIAPUN_1_68.RadioValue()==this.w_GMPIAPUN)
      this.oPgFrm.Page1.oPag.oGMPIAPUN_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMORDPRD_2_88.RadioValue()==this.w_GMORDPRD)
      this.oPgFrm.Page2.oPag.oGMORDPRD_2_88.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGMIMPPRD_2_89.RadioValue()==this.w_GMIMPPRD)
      this.oPgFrm.Page2.oPag.oGMIMPPRD_2_89.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGMDELORD_1_75.RadioValue()==this.w_GMDELORD)
      this.oPgFrm.Page1.oPag.oGMDELORD_1_75.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGUFLAROB_1_83.RadioValue()==this.w_GUFLAROB)
      this.oPgFrm.Page1.oPag.oGUFLAROB_1_83.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGUELPDAS_1_84.RadioValue()==this.w_GUELPDAS)
      this.oPgFrm.Page1.oPag.oGUELPDAS_1_84.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGUELPDAF_1_85.RadioValue()==this.w_GUELPDAF)
      this.oPgFrm.Page1.oPag.oGUELPDAF_1_85.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'SEL__MRP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.f.)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GMNUMINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGMNUMINI_2_53.SetFocus()
            i_bnoObbl = !empty(.w_GMNUMINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GMNUMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGMNUMFIN_2_55.SetFocus()
            i_bnoObbl = !empty(.w_GMNUMFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GMINIELA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGMINIELA_2_61.SetFocus()
            i_bnoObbl = !empty(.w_GMINIELA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data vuota o maggiore della data di fine elaborazione")
          case   (empty(.w_GMFINELA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGMFINELA_2_62.SetFocus()
            i_bnoObbl = !empty(.w_GMFINELA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data vuota o minore della data di fine elaborazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GMCRIELA = this.w_GMCRIELA
    return

  func CanEdit()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg('MSG_CANNOT_EDIT')
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg('MSG_CANNOT_ADD')
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsmr_agmPag1 as StdContainer
  Width  = 825
  height = 600
  stdWidth  = 825
  stdheight = 600
  resizeXpos=470
  resizeYpos=400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGMSERIAL_1_2 as StdField with uid="AXIPHUMVEA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GMSERIAL", cQueryName = "GMSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 69134670,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=126, Top=19, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oGMDATELA_1_3 as StdField with uid="NNXXIBSTCO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GMDATELA", cQueryName = "GMDATELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 133965479,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=281, Top=19

  add object oGMORAELA_1_4 as StdField with uid="LVPWUIBFOX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GMORAELA", cQueryName = "GMORAELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 115201703,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=414, Top=19, InputMask=replicate('X',8)

  add object oGMUTEELA_1_5 as StdField with uid="IHPVCHVGVB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GMUTEELA", cQueryName = "GMUTEELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 119551655,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=569, Top=21, cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_GMUTEELA"

  func oGMUTEELA_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMPATHEL_1_6 as StdField with uid="SLOJNMQLEF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GMPATHEL", cQueryName = "GMPATHEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 84089166,;
   bGlobalFont=.t.,;
    Height=21, Width=660, Left=126, Top=47, InputMask=replicate('X',200)

  add object oDESUTE_1_11 as StdField with uid="XXRKGFCXKH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 133099978,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=611, Top=21, InputMask=replicate('X',20)


  add object oBtn_1_15 as StdButton with uid="IDQLKXKZHR",left=770, top=551, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa elaborazione MRP";
    , HelpContextID = 220049898;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      do GSCO_SEM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="DBKXRVJYQY",left=719, top=551, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log dell'elaborazione";
    , HelpContextID = 226187446;
    , caption='\<Log elab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        gsut_bgf(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GMMRPLOG<>'S')
     endwith
    endif
  endfunc

  add object oGMMODELA_1_17 as StdRadio with uid="IAYNGAFPBC",rtseq=12,rtrep=.f.,left=28, top=84, width=108,height=35, enabled=.f.;
    , ToolTipText = "Modalit� di elaborazione";
    , cFormVar="w_GMMODELA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oGMMODELA_1_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Rigenerativa"
      this.Buttons(1).HelpContextID = 118142631
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Net-change"
      this.Buttons(2).HelpContextID = 118142631
      this.Buttons(2).Top=16
      this.SetAll("Width",106)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modalit� di elaborazione")
      StdRadio::init()
    endproc

  func oGMMODELA_1_17.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oGMMODELA_1_17.GetRadio()
    this.Parent.oContained.w_GMMODELA = this.RadioValue()
    return .t.
  endfunc

  func oGMMODELA_1_17.SetRadio()
    this.Parent.oContained.w_GMMODELA=trim(this.Parent.oContained.w_GMMODELA)
    this.value = ;
      iif(this.Parent.oContained.w_GMMODELA=='R',1,;
      iif(this.Parent.oContained.w_GMMODELA=='N',2,;
      0))
  endfunc

  add object oGMCHKDAT_1_18 as StdCheck with uid="LHIUVIMPMY",rtseq=13,rtrep=.f.,left=28, top=400, caption="Test correttezza dati", enabled=.f.,;
    ToolTipText = "Verifica di correttezza dei dati necessari per l'elaborazione",;
    HelpContextID = 160229702,;
    cFormVar="w_GMCHKDAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMCHKDAT_1_18.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oGMCHKDAT_1_18.GetRadio()
    this.Parent.oContained.w_GMCHKDAT = this.RadioValue()
    return .t.
  endfunc

  func oGMCHKDAT_1_18.SetRadio()
    this.Parent.oContained.w_GMCHKDAT=trim(this.Parent.oContained.w_GMCHKDAT)
    this.value = ;
      iif(this.Parent.oContained.w_GMCHKDAT=="S",1,;
      0)
  endfunc

  add object oGMMRPLOG_1_19 as StdCheck with uid="KYWQCBJRJS",rtseq=14,rtrep=.f.,left=28, top=425, caption="Attiva scrittura log elaborazione", enabled=.f.,;
    ToolTipText = "Attiva la scrittura del log di elaborazione MRP",;
    HelpContextID = 248362669,;
    cFormVar="w_GMMRPLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMMRPLOG_1_19.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oGMMRPLOG_1_19.GetRadio()
    this.Parent.oContained.w_GMMRPLOG = this.RadioValue()
    return .t.
  endfunc

  func oGMMRPLOG_1_19.SetRadio()
    this.Parent.oContained.w_GMMRPLOG=trim(this.Parent.oContained.w_GMMRPLOG)
    this.value = ;
      iif(this.Parent.oContained.w_GMMRPLOG=="S",1,;
      0)
  endfunc

  add object oGMMSGRIP_1_20 as StdCheck with uid="UOOBFZJPIG",rtseq=15,rtrep=.f.,left=28, top=561, caption="Messaggi ripianificazione", enabled=.f.,;
    ToolTipText = "Abilita i messaggi ripianificazione",;
    HelpContextID = 71218870,;
    cFormVar="w_GMMSGRIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMMSGRIP_1_20.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oGMMSGRIP_1_20.GetRadio()
    this.Parent.oContained.w_GMMSGRIP = this.RadioValue()
    return .t.
  endfunc

  func oGMMSGRIP_1_20.SetRadio()
    this.Parent.oContained.w_GMMSGRIP=trim(this.Parent.oContained.w_GMMSGRIP)
    this.value = ;
      iif(this.Parent.oContained.w_GMMSGRIP=="S",1,;
      0)
  endfunc


  add object oGMMICOMP_1_21 as StdCombo with uid="SRNAAGBEFD",rtseq=16,rtrep=.f.,left=229,top=125,width=157,height=21, enabled=.f.;
    , ToolTipText = "Magazzino di impegno componenti";
    , HelpContextID = 16037558;
    , cFormVar="w_GMMICOMP",RowSource=""+"Magazzino preferenziale,"+"Da anagrafica articoli,"+"Da testata odine,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMMICOMP_1_21.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oGMMICOMP_1_21.GetRadio()
    this.Parent.oContained.w_GMMICOMP = this.RadioValue()
    return .t.
  endfunc

  func oGMMICOMP_1_21.SetRadio()
    this.Parent.oContained.w_GMMICOMP=trim(this.Parent.oContained.w_GMMICOMP)
    this.value = ;
      iif(this.Parent.oContained.w_GMMICOMP=='M',1,;
      iif(this.Parent.oContained.w_GMMICOMP=='A',2,;
      iif(this.Parent.oContained.w_GMMICOMP=='T',3,;
      iif(this.Parent.oContained.w_GMMICOMP=='F',4,;
      0))))
  endfunc

  add object oGMMAGFOR_1_22 as StdField with uid="JKCCZXUOVE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GMMAGFOR", cQueryName = "GMMAGFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di impegno per componenti",;
    HelpContextID = 137148088,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=516, Top=125, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GMMAGFOR"

  func oGMMAGFOR_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oGMMRFODL_1_23 as StdCombo with uid="AUAHSRLOHO",rtseq=18,rtrep=.f.,left=229,top=150,width=157,height=21, enabled=.f.;
    , ToolTipText = "Magazzino di riferimento ODL";
    , HelpContextID = 248662350;
    , cFormVar="w_GMMRFODL",RowSource=""+"Magazzino preferenziale,"+"Criterio di pianificazione,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMMRFODL_1_23.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oGMMRFODL_1_23.GetRadio()
    this.Parent.oContained.w_GMMRFODL = this.RadioValue()
    return .t.
  endfunc

  func oGMMRFODL_1_23.SetRadio()
    this.Parent.oContained.w_GMMRFODL=trim(this.Parent.oContained.w_GMMRFODL)
    this.value = ;
      iif(this.Parent.oContained.w_GMMRFODL=='M',1,;
      iif(this.Parent.oContained.w_GMMRFODL=='C',2,;
      iif(this.Parent.oContained.w_GMMRFODL=='F',3,;
      0)))
  endfunc

  add object oGMMAGFOC_1_24 as StdField with uid="HAVLAOFVAV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GMMAGFOC", cQueryName = "GMMAGFOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di riferimento ODL",;
    HelpContextID = 137148073,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=516, Top=150, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GMMAGFOC"

  func oGMMAGFOC_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oGMMRFOCL_1_25 as StdCombo with uid="BBLKKVFXWJ",rtseq=20,rtrep=.f.,left=229,top=175,width=157,height=21, enabled=.f.;
    , ToolTipText = "Magazzino di riferimento OCL";
    , HelpContextID = 248662350;
    , cFormVar="w_GMMRFOCL",RowSource=""+"Magazzino preferenziale,"+"Criterio di pianificazione,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMMRFOCL_1_25.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oGMMRFOCL_1_25.GetRadio()
    this.Parent.oContained.w_GMMRFOCL = this.RadioValue()
    return .t.
  endfunc

  func oGMMRFOCL_1_25.SetRadio()
    this.Parent.oContained.w_GMMRFOCL=trim(this.Parent.oContained.w_GMMRFOCL)
    this.value = ;
      iif(this.Parent.oContained.w_GMMRFOCL=='M',1,;
      iif(this.Parent.oContained.w_GMMRFOCL=='C',2,;
      iif(this.Parent.oContained.w_GMMRFOCL=='F',3,;
      0)))
  endfunc

  add object oGMMAGFOL_1_26 as StdField with uid="RNISWUMNCL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_GMMAGFOL", cQueryName = "GMMAGFOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di riferimento OCL",;
    HelpContextID = 137148082,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=516, Top=175, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GMMAGFOL"

  func oGMMAGFOL_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oGMGENPDA_1_27 as StdCombo with uid="NKJOLKZOYZ",rtseq=22,rtrep=.f.,left=28,top=220,width=96,height=21, enabled=.f.;
    , ToolTipText = "Se attivo: genera proposte d'ordine per materiali d'acquisto";
    , HelpContextID = 224373081;
    , cFormVar="w_GMGENPDA",RowSource=""+"Elabora PDA,"+"Elabora ODA,"+"Non elaborare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMGENPDA_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'O',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oGMGENPDA_1_27.GetRadio()
    this.Parent.oContained.w_GMGENPDA = this.RadioValue()
    return .t.
  endfunc

  func oGMGENPDA_1_27.SetRadio()
    this.Parent.oContained.w_GMGENPDA=trim(this.Parent.oContained.w_GMGENPDA)
    this.value = ;
      iif(this.Parent.oContained.w_GMGENPDA=='S',1,;
      iif(this.Parent.oContained.w_GMGENPDA=='O',2,;
      iif(this.Parent.oContained.w_GMGENPDA=='N',3,;
      0)))
  endfunc


  add object oGMMRFODA_1_28 as StdCombo with uid="FDINBJKTUF",rtseq=23,rtrep=.f.,left=229,top=220,width=157,height=21, enabled=.f.;
    , ToolTipText = "Magazzino di riferimento ODA";
    , HelpContextID = 248662361;
    , cFormVar="w_GMMRFODA",RowSource=""+"Magazzino preferenziale,"+"Criterio di pianificazione,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMMRFODA_1_28.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oGMMRFODA_1_28.GetRadio()
    this.Parent.oContained.w_GMMRFODA = this.RadioValue()
    return .t.
  endfunc

  func oGMMRFODA_1_28.SetRadio()
    this.Parent.oContained.w_GMMRFODA=trim(this.Parent.oContained.w_GMMRFODA)
    this.value = ;
      iif(this.Parent.oContained.w_GMMRFODA=='M',1,;
      iif(this.Parent.oContained.w_GMMRFODA=='C',2,;
      iif(this.Parent.oContained.w_GMMRFODA=='F',3,;
      0)))
  endfunc

  func oGMMRFODA_1_28.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'O')
    endwith
  endfunc

  add object oGMMAGFOA_1_29 as StdField with uid="XEHOGPYVZN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_GMMAGFOA", cQueryName = "GMMAGFOA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di riferimento ODA",;
    HelpContextID = 137148071,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=516, Top=221, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GMMAGFOA"

  func oGMMAGFOA_1_29.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'O')
    endwith
  endfunc

  func oGMMAGFOA_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMGENODA_1_30 as StdCheck with uid="XQDNSFTQLZ",rtseq=25,rtrep=.f.,left=28, top=245, caption="Genera esclusivamente gli ODA", enabled=.f.,;
    ToolTipText = "Genera esclusivamente gli ODA",;
    HelpContextID = 241150297,;
    cFormVar="w_GMGENODA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMGENODA_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGMGENODA_1_30.GetRadio()
    this.Parent.oContained.w_GMGENODA = this.RadioValue()
    return .t.
  endfunc

  func oGMGENODA_1_30.SetRadio()
    this.Parent.oContained.w_GMGENODA=trim(this.Parent.oContained.w_GMGENODA)
    this.value = ;
      iif(this.Parent.oContained.w_GMGENODA=='S',1,;
      0)
  endfunc

  func oGMGENODA_1_30.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'O')
    endwith
  endfunc

  add object oGMDISMAG_1_31 as StdCheck with uid="BILRKZZCAA",rtseq=26,rtrep=.f.,left=28, top=475, caption="Considera disponibilit� magazzino", enabled=.f.,;
    ToolTipText = "Considera disponibilit� magazzino",;
    HelpContextID = 776531,;
    cFormVar="w_GMDISMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMDISMAG_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGMDISMAG_1_31.GetRadio()
    this.Parent.oContained.w_GMDISMAG = this.RadioValue()
    return .t.
  endfunc

  func oGMDISMAG_1_31.SetRadio()
    this.Parent.oContained.w_GMDISMAG=trim(this.Parent.oContained.w_GMDISMAG)
    this.value = ;
      iif(this.Parent.oContained.w_GMDISMAG=='S',1,;
      0)
  endfunc

  add object oGMGIANEG_1_32 as StdCheck with uid="FKREULHMYC",rtseq=27,rtrep=.f.,left=28, top=450, caption="Considera giacenze negative", enabled=.f.,;
    ToolTipText = "Considera le giacenze negative",;
    HelpContextID = 2861395,;
    cFormVar="w_GMGIANEG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMGIANEG_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGMGIANEG_1_32.GetRadio()
    this.Parent.oContained.w_GMGIANEG = this.RadioValue()
    return .t.
  endfunc

  func oGMGIANEG_1_32.SetRadio()
    this.Parent.oContained.w_GMGIANEG=trim(this.Parent.oContained.w_GMGIANEG)
    this.value = ;
      iif(this.Parent.oContained.w_GMGIANEG=='S',1,;
      0)
  endfunc

  add object oGMORDMPS_1_33 as StdCheck with uid="NUTKYGIZWK",rtseq=28,rtrep=.f.,left=28, top=501, caption="Considera ordini MPS da pianificare", enabled=.f.,;
    ToolTipText = "Considera ordini MPS da pianificare",;
    HelpContextID = 15870279,;
    cFormVar="w_GMORDMPS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMORDMPS_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGMORDMPS_1_33.GetRadio()
    this.Parent.oContained.w_GMORDMPS = this.RadioValue()
    return .t.
  endfunc

  func oGMORDMPS_1_33.SetRadio()
    this.Parent.oContained.w_GMORDMPS=trim(this.Parent.oContained.w_GMORDMPS)
    this.value = ;
      iif(this.Parent.oContained.w_GMORDMPS=='S',1,;
      0)
  endfunc


  add object oGMSTAORD_1_34 as StdCombo with uid="GFGQGKCOSW",rtseq=29,rtrep=.f.,left=229,top=349,width=157,height=21, enabled=.f.;
    , ToolTipText = "Stato ordini generati da MRP";
    , HelpContextID = 253749590;
    , cFormVar="w_GMSTAORD",RowSource=""+"Suggerito,"+"Pianificato,"+"Pianificato (da articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMSTAORD_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'P',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oGMSTAORD_1_34.GetRadio()
    this.Parent.oContained.w_GMSTAORD = this.RadioValue()
    return .t.
  endfunc

  func oGMSTAORD_1_34.SetRadio()
    this.Parent.oContained.w_GMSTAORD=trim(this.Parent.oContained.w_GMSTAORD)
    this.value = ;
      iif(this.Parent.oContained.w_GMSTAORD=='S',1,;
      iif(this.Parent.oContained.w_GMSTAORD=='P',2,;
      iif(this.Parent.oContained.w_GMSTAORD=='A',3,;
      0)))
  endfunc

  add object oDESMFR_1_49 as StdField with uid="PGNQOTCUXR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESMFR", cQueryName = "DESMFR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 198635978,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=578, Top=125, InputMask=replicate('X',30)

  add object oDESMOC_1_50 as StdField with uid="CDATHAQKUR",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESMOC", cQueryName = "DESMOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 172421578,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=578, Top=150, InputMask=replicate('X',30)

  add object oDESMOL_1_51 as StdField with uid="GEBWEFFQVC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESMOL", cQueryName = "DESMOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 21426634,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=578, Top=175, InputMask=replicate('X',30)

  add object oDESMOA_1_52 as StdField with uid="XCJRMLCMJC",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESMOA", cQueryName = "DESMOA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 205976010,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=578, Top=221, InputMask=replicate('X',30)

  func oDESMOA_1_52.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'O')
    endwith
  endfunc


  add object oGMCRIFOR_1_55 as StdCombo with uid="XRETRZSGJB",rtseq=34,rtrep=.f.,left=666,top=247,width=142,height=21, enabled=.f.;
    , ToolTipText = "Criterio di scelta del miglior fornitore in base ai contratti in essere";
    , HelpContextID = 140318392;
    , cFormVar="w_GMCRIFOR",RowSource=""+"Priorit�,"+"Tempo,"+"Prezzo,"+"Affidabilit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMCRIFOR_1_55.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oGMCRIFOR_1_55.GetRadio()
    this.Parent.oContained.w_GMCRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oGMCRIFOR_1_55.SetRadio()
    this.Parent.oContained.w_GMCRIFOR=trim(this.Parent.oContained.w_GMCRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_GMCRIFOR=='I',1,;
      iif(this.Parent.oContained.w_GMCRIFOR=='T',2,;
      iif(this.Parent.oContained.w_GMCRIFOR=='Z',3,;
      iif(this.Parent.oContained.w_GMCRIFOR=='A',4,;
      0))))
  endfunc


  add object oGMCRIELA_1_56 as StdCombo with uid="VSDAZQQBWZ",rtseq=35,rtrep=.f.,left=229,top=301,width=157,height=21, enabled=.f.;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 123541159;
    , cFormVar="w_GMCRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMCRIELA_1_56.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oGMCRIELA_1_56.GetRadio()
    this.Parent.oContained.w_GMCRIELA = this.RadioValue()
    return .t.
  endfunc

  func oGMCRIELA_1_56.SetRadio()
    this.Parent.oContained.w_GMCRIELA=trim(this.Parent.oContained.w_GMCRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_GMCRIELA=='A',1,;
      iif(this.Parent.oContained.w_GMCRIELA=='M',2,;
      iif(this.Parent.oContained.w_GMCRIELA=='G',3,;
      0)))
  endfunc


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=441, top=291, width=373,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=1;
    , HelpContextID = 132998170


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=446, top=281, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=1;
    , HelpContextID = 132998170

  add object oGMELABID_1_61 as StdCheck with uid="LAPRLXLQTW",rtseq=37,rtrep=.f.,left=28, top=526, caption="Rigenera i bidoni temporali per tutti gli ordini", enabled=.f.,;
    ToolTipText = "Rigenera i bidoni temporali per tutti gli ordini",;
    HelpContextID = 64435882,;
    cFormVar="w_GMELABID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGMELABID_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGMELABID_1_61.GetRadio()
    this.Parent.oContained.w_GMELABID = this.RadioValue()
    return .t.
  endfunc

  func oGMELABID_1_61.SetRadio()
    this.Parent.oContained.w_GMELABID=trim(this.Parent.oContained.w_GMELABID)
    this.value = ;
      iif(this.Parent.oContained.w_GMELABID=='S',1,;
      0)
  endfunc


  add object oBtn_1_66 as StdButton with uid="LUXLLMGPOJ",left=657, top=551, width=48,height=45,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=1;
    , ToolTipText = "Parametri messaggi MRP";
    , HelpContextID = 232999126;
    , Caption='\<Note elab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      do GSMR_KNE with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oGMPERPIA_1_67 as StdCombo with uid="DRHNEVIUIN",rtseq=88,rtrep=.f.,left=229,top=277,width=157,height=21, enabled=.f.;
    , ToolTipText = "Pianifica per periodo";
    , HelpContextID = 48293543;
    , cFormVar="w_GMPERPIA",RowSource=""+"Giorno,"+"Settimana,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMPERPIA_1_67.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'F',;
    iif(this.value =6,'S',;
    iif(this.value =7,'A',;
    space(1)))))))))
  endfunc
  func oGMPERPIA_1_67.GetRadio()
    this.Parent.oContained.w_GMPERPIA = this.RadioValue()
    return .t.
  endfunc

  func oGMPERPIA_1_67.SetRadio()
    this.Parent.oContained.w_GMPERPIA=trim(this.Parent.oContained.w_GMPERPIA)
    this.value = ;
      iif(this.Parent.oContained.w_GMPERPIA=='D',1,;
      iif(this.Parent.oContained.w_GMPERPIA=='W',2,;
      iif(this.Parent.oContained.w_GMPERPIA=='M',3,;
      iif(this.Parent.oContained.w_GMPERPIA=='Q',4,;
      iif(this.Parent.oContained.w_GMPERPIA=='F',5,;
      iif(this.Parent.oContained.w_GMPERPIA=='S',6,;
      iif(this.Parent.oContained.w_GMPERPIA=='A',7,;
      0)))))))
  endfunc


  add object oGMPIAPUN_1_68 as StdCombo with uid="QHVQIVCNUN",rtseq=89,rtrep=.f.,left=229,top=325,width=157,height=21, enabled=.f.;
    , ToolTipText = "Pianificazione puntuale";
    , HelpContextID = 237705548;
    , cFormVar="w_GMPIAPUN",RowSource=""+"Si,"+"No,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMPIAPUN_1_68.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oGMPIAPUN_1_68.GetRadio()
    this.Parent.oContained.w_GMPIAPUN = this.RadioValue()
    return .t.
  endfunc

  func oGMPIAPUN_1_68.SetRadio()
    this.Parent.oContained.w_GMPIAPUN=trim(this.Parent.oContained.w_GMPIAPUN)
    this.value = ;
      iif(this.Parent.oContained.w_GMPIAPUN=='S',1,;
      iif(this.Parent.oContained.w_GMPIAPUN=='N',2,;
      iif(this.Parent.oContained.w_GMPIAPUN=='A',3,;
      0)))
  endfunc


  add object oGMDELORD_1_75 as StdCombo with uid="JFOLGBXAQP",rtseq=93,rtrep=.f.,left=229,top=373,width=157,height=21, enabled=.f.;
    , ToolTipText = "Indica quali ordini suggeriti dall'MRP deve eliminare";
    , HelpContextID = 243259734;
    , cFormVar="w_GMDELORD",RowSource=""+"Tutti gli ordini,"+"Da criterio di pianificazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGMDELORD_1_75.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oGMDELORD_1_75.GetRadio()
    this.Parent.oContained.w_GMDELORD = this.RadioValue()
    return .t.
  endfunc

  func oGMDELORD_1_75.SetRadio()
    this.Parent.oContained.w_GMDELORD=trim(this.Parent.oContained.w_GMDELORD)
    this.value = ;
      iif(this.Parent.oContained.w_GMDELORD=='T',1,;
      iif(this.Parent.oContained.w_GMDELORD=='P',2,;
      0))
  endfunc

  add object oGUFLAROB_1_83 as StdCheck with uid="TITLJSWEZG",rtseq=95,rtrep=.f.,left=517, top=221, caption="Pianifica articoli obsoleti", enabled=.f.,;
    ToolTipText = "Pianifica proposte di acquisto per articoli obsoleti",;
    HelpContextID = 203993432,;
    cFormVar="w_GUFLAROB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGUFLAROB_1_83.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGUFLAROB_1_83.GetRadio()
    this.Parent.oContained.w_GUFLAROB = this.RadioValue()
    return .t.
  endfunc

  func oGUFLAROB_1_83.SetRadio()
    this.Parent.oContained.w_GUFLAROB=trim(this.Parent.oContained.w_GUFLAROB)
    this.value = ;
      iif(this.Parent.oContained.w_GUFLAROB=='S',1,;
      0)
  endfunc

  func oGUFLAROB_1_83.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'S')
    endwith
  endfunc

  add object oGUELPDAS_1_84 as StdCheck with uid="FGYUVIBBUI",rtseq=96,rtrep=.f.,left=230, top=247, caption="a scorta", enabled=.f.,;
    ToolTipText = "Elabora articoli a quantit� costante",;
    HelpContextID = 154714439,;
    cFormVar="w_GUELPDAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGUELPDAS_1_84.RadioValue()
    return(iif(this.value =1,'S',;
    '.'))
  endfunc
  func oGUELPDAS_1_84.GetRadio()
    this.Parent.oContained.w_GUELPDAS = this.RadioValue()
    return .t.
  endfunc

  func oGUELPDAS_1_84.SetRadio()
    this.Parent.oContained.w_GUELPDAS=trim(this.Parent.oContained.w_GUELPDAS)
    this.value = ;
      iif(this.Parent.oContained.w_GUELPDAS=='S',1,;
      0)
  endfunc

  func oGUELPDAS_1_84.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'S')
    endwith
  endfunc

  add object oGUELPDAF_1_85 as StdCheck with uid="HKUREFTMVX",rtseq=97,rtrep=.f.,left=230, top=221, caption="a fabbisogno", enabled=.f.,;
    ToolTipText = "Elabora articoli a fabbisogno",;
    HelpContextID = 154714452,;
    cFormVar="w_GUELPDAF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGUELPDAF_1_85.RadioValue()
    return(iif(this.value =1,'F',;
    '.'))
  endfunc
  func oGUELPDAF_1_85.GetRadio()
    this.Parent.oContained.w_GUELPDAF = this.RadioValue()
    return .t.
  endfunc

  func oGUELPDAF_1_85.SetRadio()
    this.Parent.oContained.w_GUELPDAF=trim(this.Parent.oContained.w_GUELPDAF)
    this.value = ;
      iif(this.Parent.oContained.w_GUELPDAF=='F',1,;
      0)
  endfunc

  func oGUELPDAF_1_85.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'S')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="OCRXPYTBTY",Visible=.t., Left=19, Top=22,;
    Alignment=1, Width=102, Height=18,;
    Caption="Seriale di elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="IJBXXSGOHG",Visible=.t., Left=216, Top=22,;
    Alignment=1, Width=58, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="VXEJPDYDAN",Visible=.t., Left=360, Top=22,;
    Alignment=1, Width=48, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="ZQNOZWMOYQ",Visible=.t., Left=490, Top=22,;
    Alignment=1, Width=72, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="AMFPNRBASE",Visible=.t., Left=19, Top=48,;
    Alignment=1, Width=102, Height=18,;
    Caption="Path di elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="CAOFDPYAQW",Visible=.t., Left=143, Top=84,;
    Alignment=0, Width=438, Height=15,;
    Caption="(Elaborazione completa di tutti ODP/ODR da pianificare presenti nel MPS)"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WJWUAYAAWL",Visible=.t., Left=143, Top=100,;
    Alignment=0, Width=422, Height=15,;
    Caption="(Elaborazione limitata ai soli ODP/ODR da pianificare e ODL variati)"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="IXROEWEKDY",Visible=.t., Left=125, Top=350,;
    Alignment=1, Width=99, Height=15,;
    Caption="Stato ordini:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MKNVPNHKGR",Visible=.t., Left=23, Top=198,;
    Alignment=0, Width=272, Height=15,;
    Caption="Proposte d'ordine per materiali d'acquisto"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="XDQOYSXVSN",Visible=.t., Left=481, Top=249,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di scelta fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="MHFQZKQMOH",Visible=.t., Left=16, Top=125,;
    Alignment=1, Width=208, Height=15,;
    Caption="Magazzino di impegno componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="TVPYODRMUO",Visible=.t., Left=105, Top=150,;
    Alignment=1, Width=119, Height=15,;
    Caption="Magazzino ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="QVSIYQLJNF",Visible=.t., Left=105, Top=175,;
    Alignment=1, Width=119, Height=15,;
    Caption="Magazzino OCL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="OEOKFOXMGZ",Visible=.t., Left=392, Top=150,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="KAQYZWMMPQ",Visible=.t., Left=392, Top=175,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="KNLRCHTZXJ",Visible=.t., Left=392, Top=125,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="NTDKUOUTZZ",Visible=.t., Left=392, Top=221,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'O')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="KDGLPCVNDJ",Visible=.t., Left=131, Top=222,;
    Alignment=1, Width=93, Height=15,;
    Caption="Magazzino ODA:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'O')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=42, Top=303,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="VSKSWONFNJ",Visible=.t., Left=42, Top=280,;
    Alignment=1, Width=182, Height=18,;
    Caption="Periodo di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="UYQPTYMPSQ",Visible=.t., Left=42, Top=327,;
    Alignment=1, Width=182, Height=17,;
    Caption="Pianificazione puntuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="MAGTHPKFQX",Visible=.t., Left=81, Top=375,;
    Alignment=1, Width=143, Height=18,;
    Caption="Elimina ordini suggeriti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="YIGIVNUWEN",Visible=.t., Left=130, Top=221,;
    Alignment=1, Width=95, Height=18,;
    Caption="Elabora articoli:"  ;
  , bGlobalFont=.t.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (.w_GMGENPDA<>'S')
    endwith
  endfunc

  add object oBox_1_13 as StdBox with uid="ZBTZLAQQAJ",left=22, top=73, width=785,height=2

  add object oBox_1_37 as StdBox with uid="AUQPTDFFZX",left=22, top=120, width=785,height=1

  add object oBox_1_39 as StdBox with uid="UMKYACLDHV",left=22, top=272, width=785,height=1

  add object oBox_1_41 as StdBox with uid="LVPJKZGHJE",left=22, top=214, width=785,height=1

  add object oBox_1_62 as StdBox with uid="NMBAMDFHHV",left=22, top=446, width=410,height=1

  add object oBox_1_63 as StdBox with uid="UHZJXIBASP",left=22, top=522, width=410,height=1

  add object oBox_1_64 as StdBox with uid="CFNEYTNLYK",left=22, top=396, width=410,height=1
enddefine
define class tgsmr_agmPag2 as StdContainer
  Width  = 825
  height = 600
  stdWidth  = 825
  stdheight = 600
  resizeXpos=458
  resizeYpos=541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_2 as cp_runprogram with uid="IQXQPNKAVZ",left=826, top=-48, width=98,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMR2BGP("Z")',;
    cEvent = "Load",;
    nPag=2;
    , HelpContextID = 132998170

  add object oGMCODINI_2_5 as StdField with uid="DZDJHVMUJE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_GMCODINI", cQueryName = "GMCODINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo di inizio selezione",;
    HelpContextID = 185210543,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=103, Top=36, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_GMCODINI"

  func oGMCODINI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMCODFIN_2_6 as StdField with uid="UNDISPFWGH",rtseq=39,rtrep=.f.,;
    cFormVar = "w_GMCODFIN", cQueryName = "GMCODFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articoli di fine selezione",;
    HelpContextID = 134878900,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=103, Top=61, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_GMCODFIN"

  func oGMCODFIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMLLCINI_2_7 as StdField with uid="RKYWPPNDHG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_GMLLCINI", cQueryName = "GMLLCINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di inizio selezione",;
    HelpContextID = 184002223,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=764, Top=35, cSayPict='"999"', cGetPict='"999"'

  add object oGMLLCFIN_2_8 as StdField with uid="ACQHHCIYYT",rtseq=41,rtrep=.f.,;
    cFormVar = "w_GMLLCFIN", cQueryName = "GMLLCFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di fine selezione",;
    HelpContextID = 133670580,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=764, Top=58, cSayPict='"999"', cGetPict='"999"'

  add object oGMFAMAIN_2_9 as StdField with uid="BAUZFWPTWC",rtseq=42,rtrep=.f.,;
    cFormVar = "w_GMFAMAIN", cQueryName = "GMFAMAIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 59524788,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=103, Top=86, InputMask=replicate('X',5), cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_GMFAMAIN"

  func oGMFAMAIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMFAMAFI_2_10 as StdField with uid="GHDDKOKVIG",rtseq=43,rtrep=.f.,;
    cFormVar = "w_GMFAMAFI", cQueryName = "GMFAMAFI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 208910673,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=506, Top=86, InputMask=replicate('X',5), cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_GMFAMAFI"

  func oGMFAMAFI_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMGRUINI_2_11 as StdField with uid="VYQYSTJFCH",rtseq=44,rtrep=.f.,;
    cFormVar = "w_GMGRUINI", cQueryName = "GMGRUINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 203249327,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=103, Top=111, InputMask=replicate('X',5), cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GMGRUINI"

  func oGMGRUINI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMGRUFIN_2_12 as StdField with uid="BKTBROMCMD",rtseq=45,rtrep=.f.,;
    cFormVar = "w_GMGRUFIN", cQueryName = "GMGRUFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 152917684,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=506, Top=111, InputMask=replicate('X',5), cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GMGRUFIN"

  func oGMGRUFIN_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMCATINI_2_13 as StdField with uid="WQNLFQEXDD",rtseq=46,rtrep=.f.,;
    cFormVar = "w_GMCATINI", cQueryName = "GMCATINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 201070255,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=103, Top=136, InputMask=replicate('X',5), cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_GMCATINI"

  func oGMCATINI_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMCATFIN_2_14 as StdField with uid="WHQQEZYTLE",rtseq=47,rtrep=.f.,;
    cFormVar = "w_GMCATFIN", cQueryName = "GMCATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 150738612,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=506, Top=136, InputMask=replicate('X',5), cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_GMCATFIN"

  func oGMCATFIN_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMMAGINI_2_15 as StdField with uid="WWAXPPIMPZ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_GMMAGINI", cQueryName = "GMMAGINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di inizio selezione",;
    HelpContextID = 187479727,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=103, Top=161, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GMMAGINI"

  func oGMMAGINI_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMMAGFIN_2_16 as StdField with uid="IPBLQAUROZ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_GMMAGFIN", cQueryName = "GMMAGFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di fine selezione",;
    HelpContextID = 137148084,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=506, Top=161, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GMMAGFIN"

  func oGMMAGFIN_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMMARINI_2_17 as StdField with uid="YZOGKGSXPU",rtseq=50,rtrep=.f.,;
    cFormVar = "w_GMMARINI", cQueryName = "GMMARINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di inizio selezione",;
    HelpContextID = 199014063,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=103, Top=186, InputMask=replicate('X',5), cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_GMMARINI"

  func oGMMARINI_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMMARFIN_2_18 as StdField with uid="NRZQAZMNEF",rtseq=51,rtrep=.f.,;
    cFormVar = "w_GMMARFIN", cQueryName = "GMMARFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di fine selezione",;
    HelpContextID = 148682420,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=506, Top=186, InputMask=replicate('X',5), cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_GMMARFIN"

  func oGMMARFIN_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMPROFIN_2_19 as StdCheck with uid="IHWROQAYED",rtseq=52,rtrep=.f.,left=103, top=211, caption="Prodotti finiti", enabled=.f.,;
    ToolTipText = "Se attivo, elabora i prodotti finiti",;
    HelpContextID = 146663092,;
    cFormVar="w_GMPROFIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGMPROFIN_2_19.RadioValue()
    return(iif(this.value =1,'PF',;
    'XX'))
  endfunc
  func oGMPROFIN_2_19.GetRadio()
    this.Parent.oContained.w_GMPROFIN = this.RadioValue()
    return .t.
  endfunc

  func oGMPROFIN_2_19.SetRadio()
    this.Parent.oContained.w_GMPROFIN=trim(this.Parent.oContained.w_GMPROFIN)
    this.value = ;
      iif(this.Parent.oContained.w_GMPROFIN=='PF',1,;
      0)
  endfunc

  add object oGMSEMLAV_2_20 as StdCheck with uid="KTWFDIHQED",rtseq=53,rtrep=.f.,left=260, top=211, caption="Semilavorati", enabled=.f.,;
    ToolTipText = "Se attivo, elabora i semilavorati",;
    HelpContextID = 24045892,;
    cFormVar="w_GMSEMLAV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGMSEMLAV_2_20.RadioValue()
    return(iif(this.value =1,'SE',;
    'XX'))
  endfunc
  func oGMSEMLAV_2_20.GetRadio()
    this.Parent.oContained.w_GMSEMLAV = this.RadioValue()
    return .t.
  endfunc

  func oGMSEMLAV_2_20.SetRadio()
    this.Parent.oContained.w_GMSEMLAV=trim(this.Parent.oContained.w_GMSEMLAV)
    this.value = ;
      iif(this.Parent.oContained.w_GMSEMLAV=='SE',1,;
      0)
  endfunc

  add object oGMMATPRI_2_21 as StdCheck with uid="SHIEYEJPCW",rtseq=54,rtrep=.f.,left=381, top=211, caption="Materie prime", enabled=.f.,;
    ToolTipText = "Se attivo, elabora le materie prime",;
    HelpContextID = 218319185,;
    cFormVar="w_GMMATPRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGMMATPRI_2_21.RadioValue()
    return(iif(this.value =1,'MP',;
    'XX'))
  endfunc
  func oGMMATPRI_2_21.GetRadio()
    this.Parent.oContained.w_GMMATPRI = this.RadioValue()
    return .t.
  endfunc

  func oGMMATPRI_2_21.SetRadio()
    this.Parent.oContained.w_GMMATPRI=trim(this.Parent.oContained.w_GMMATPRI)
    this.value = ;
      iif(this.Parent.oContained.w_GMMATPRI=='MP',1,;
      0)
  endfunc


  add object oGMELACAT_2_22 as StdCombo with uid="HLYKAPXQGN",rtseq=55,rtrep=.f.,left=605,top=211,width=68,height=21, enabled=.f.;
    , ToolTipText = "Elabora tutta la catena degli impegni degli ordini che rispettano i filtri di selezione sull'articolo";
    , HelpContextID = 187222342;
    , cFormVar="w_GMELACAT",RowSource=""+"S�,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oGMELACAT_2_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oGMELACAT_2_22.GetRadio()
    this.Parent.oContained.w_GMELACAT = this.RadioValue()
    return .t.
  endfunc

  func oGMELACAT_2_22.SetRadio()
    this.Parent.oContained.w_GMELACAT=trim(this.Parent.oContained.w_GMELACAT)
    this.value = ;
      iif(this.Parent.oContained.w_GMELACAT=='S',1,;
      iif(this.Parent.oContained.w_GMELACAT=='N',2,;
      0))
  endfunc

  add object oDESMAGI_2_24 as StdField with uid="QONUNYBHAK",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 148442678,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=170, Top=161, InputMask=replicate('X',30)

  add object oDESMAGF_2_26 as StdField with uid="EBXVFVWORM",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 119992778,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=572, Top=161, InputMask=replicate('X',30)

  add object oDESINI_2_27 as StdField with uid="SSNEPWHMKY",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 73069002,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=279, Top=36, InputMask=replicate('X',40)

  add object oDESFIN_2_29 as StdField with uid="LMJMLCZYUA",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 263057866,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=279, Top=61, InputMask=replicate('X',40)

  add object oDESFAMAI_2_31 as StdField with uid="AOYBYAMKXI",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19788161,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=170, Top=86, InputMask=replicate('X',35)

  add object oDESGRUI_2_32 as StdField with uid="ZABORREMDH",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 132320822,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=170, Top=111, InputMask=replicate('X',35)

  add object oDESCATI_2_33 as StdField with uid="ZFWYARIWSP",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 97455670,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=170, Top=136, InputMask=replicate('X',35)

  add object oDESFAMAF_2_37 as StdField with uid="WOCDSQXKSA",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19788164,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=572, Top=86, InputMask=replicate('X',35)

  add object oDESGRUF_2_38 as StdField with uid="PYGQUHDJMJ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 136114634,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=572, Top=111, InputMask=replicate('X',35)

  add object oDESCATF_2_39 as StdField with uid="SXSGGSHCII",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 170979786,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=572, Top=136, InputMask=replicate('X',35)

  add object oDESMARI_2_47 as StdField with uid="MXNLJGFEDS",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESMARI", cQueryName = "DESMARI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 64556598,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=170, Top=185, InputMask=replicate('X',35)

  add object oDESMARF_2_49 as StdField with uid="UFBSQRHGVN",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESMARF", cQueryName = "DESMARF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203878858,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=572, Top=185, InputMask=replicate('X',35)

  add object oGMCOMINI_2_50 as StdField with uid="YEMXFLGGUY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_GMCOMINI", cQueryName = "GMCOMINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 194647727,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=255, InputMask=replicate('X',15), cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_GMCOMINI"

  func oGMCOMINI_2_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMCOMFIN_2_51 as StdField with uid="TQLURNPFWN",rtseq=69,rtrep=.f.,;
    cFormVar = "w_GMCOMFIN", cQueryName = "GMCOMFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 144316084,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=280, InputMask=replicate('X',15), cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_GMCOMFIN"

  func oGMCOMFIN_2_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMCOMODL_2_52 as StdRadio with uid="FZFMTLUTAJ",rtseq=70,rtrep=.f.,left=574, top=255, width=153,height=51, enabled=.f.;
    , ToolTipText = "Validit� filtro commessa";
    , cFormVar="w_GMCOMODL", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oGMCOMODL_2_52.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo su ODL"
      this.Buttons(1).HelpContextID = 241559886
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo su documenti"
      this.Buttons(2).HelpContextID = 241559886
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 241559886
      this.Buttons(3).Top=32
      this.SetAll("Width",151)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Validit� filtro commessa")
      StdRadio::init()
    endproc

  func oGMCOMODL_2_52.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oGMCOMODL_2_52.GetRadio()
    this.Parent.oContained.w_GMCOMODL = this.RadioValue()
    return .t.
  endfunc

  func oGMCOMODL_2_52.SetRadio()
    this.Parent.oContained.w_GMCOMODL=trim(this.Parent.oContained.w_GMCOMODL)
    this.value = ;
      iif(this.Parent.oContained.w_GMCOMODL=='O',1,;
      iif(this.Parent.oContained.w_GMCOMODL=='D',2,;
      iif(this.Parent.oContained.w_GMCOMODL=='T',3,;
      0)))
  endfunc

  add object oGMNUMINI_2_53 as StdField with uid="YWFWDUKRHP",rtseq=71,rtrep=.f.,;
    cFormVar = "w_GMNUMINI", cQueryName = "GMNUMINI",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 195085999,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=134, Top=305, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  add object oGMSERIE1_2_54 as StdField with uid="VCUBODDNSC",rtseq=72,rtrep=.f.,;
    cFormVar = "w_GMSERIE1", cQueryName = "GMSERIE1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento di inzio selezione",;
    HelpContextID = 69134697,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=281, Top=305, InputMask=replicate('X',10)

  add object oGMNUMFIN_2_55 as StdField with uid="RCINERZEKD",rtseq=73,rtrep=.f.,;
    cFormVar = "w_GMNUMFIN", cQueryName = "GMNUMFIN",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 144754356,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=134, Top=330, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  add object oGMSERIE2_2_56 as StdField with uid="HJOOAQPZOB",rtseq=74,rtrep=.f.,;
    cFormVar = "w_GMSERIE2", cQueryName = "GMSERIE2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento  di fine selezione",;
    HelpContextID = 69134696,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=281, Top=329, InputMask=replicate('X',10)

  add object oGMDOCINI_2_57 as StdField with uid="FSPDAHXDHY",rtseq=75,rtrep=.f.,;
    cFormVar = "w_GMDOCINI", cQueryName = "GMDOCINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 184166063,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=477, Top=305

  add object oGMDOCFIN_2_58 as StdField with uid="JBFVSEPHWX",rtseq=76,rtrep=.f.,;
    cFormVar = "w_GMDOCFIN", cQueryName = "GMDOCFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 133834420,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=477, Top=330

  add object oGMINICLI_2_59 as StdField with uid="XPINBWSWBI",rtseq=77,rtrep=.f.,;
    cFormVar = "w_GMINICLI", cQueryName = "GMINICLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente intestatario dell'impegno di inizio selezione",;
    HelpContextID = 89749167,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=355, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_GMINICLI"

  func oGMINICLI_2_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMFINCLI_2_60 as StdField with uid="DGDVSYNEUI",rtseq=78,rtrep=.f.,;
    cFormVar = "w_GMFINCLI", cQueryName = "GMFINCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente intestatario dell'impegno di fine selezione",;
    HelpContextID = 94652079,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=380, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_GMFINCLI"

  func oGMFINCLI_2_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oGMINIELA_2_61 as StdField with uid="MDRRUIPSZB",rtseq=79,rtrep=.f.,;
    cFormVar = "w_GMINIELA", cQueryName = "GMINIELA",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data vuota o maggiore della data di fine elaborazione",;
    ToolTipText = "Data di inizio dell'orizzonte temporale di elaborazione",;
    HelpContextID = 123303591,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=406

  add object oGMFINELA_2_62 as StdField with uid="NPUVVATDRF",rtseq=80,rtrep=.f.,;
    cFormVar = "w_GMFINELA", cQueryName = "GMFINELA",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data vuota o minore della data di fine elaborazione",;
    ToolTipText = "Data di fine dell'orizzonte temporale di elaborazione",;
    HelpContextID = 128206503,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=431

  add object oGMORIODL_2_63 as StdRadio with uid="GLGTRBWTXT",rtseq=81,rtrep=.f.,left=222, top=406, width=156,height=50, enabled=.f.;
    , ToolTipText = "Validit� filtro orizzonte di elaborazione";
    , cFormVar="w_GMORIODL", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oGMORIODL_2_63.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo su ODL"
      this.Buttons(1).HelpContextID = 245508430
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo su documenti"
      this.Buttons(2).HelpContextID = 245508430
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 245508430
      this.Buttons(3).Top=32
      this.SetAll("Width",154)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Validit� filtro orizzonte di elaborazione")
      StdRadio::init()
    endproc

  func oGMORIODL_2_63.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oGMORIODL_2_63.GetRadio()
    this.Parent.oContained.w_GMORIODL = this.RadioValue()
    return .t.
  endfunc

  func oGMORIODL_2_63.SetRadio()
    this.Parent.oContained.w_GMORIODL=trim(this.Parent.oContained.w_GMORIODL)
    this.value = ;
      iif(this.Parent.oContained.w_GMORIODL=='O',1,;
      iif(this.Parent.oContained.w_GMORIODL=='D',2,;
      iif(this.Parent.oContained.w_GMORIODL=='T',3,;
      0)))
  endfunc


  add object SZOOM1 as cp_szoombox with uid="JCMGIWLCXL",left=403, top=402, width=402,height=196,;
    caption='SZOOM1',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCO_KGP",cTable="TIP_DOCU",bOptions=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=2;
    , ToolTipText = "Causali documento di impegno";
    , HelpContextID = 60482598

  add object oDESCLII_2_70 as StdField with uid="QWFZIJERXR",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESCLII", cQueryName = "DESCLII",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192876086,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=281, Top=355, InputMask=replicate('X',40)

  add object oDESCLIF_2_71 as StdField with uid="BPAUTKZZUX",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESCLIF", cQueryName = "DESCLIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 75559370,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=281, Top=380, InputMask=replicate('X',40)

  add object oDESCOMI_2_73 as StdField with uid="ZUHXZOJHTP",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESCOMI", cQueryName = "DESCOMI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 263130678,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=281, Top=255, InputMask=replicate('X',30)

  add object oDESCOMF_2_75 as StdField with uid="GOPFERZBTM",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DESCOMF", cQueryName = "DESCOMF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 5304778,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=281, Top=280, InputMask=replicate('X',30)


  add object oGMSELIMP_2_83 as StdCombo with uid="OHXXBCHADO",rtseq=86,rtrep=.f.,left=134,top=510,width=146,height=21, enabled=.f.;
    , HelpContextID = 193009334;
    , cFormVar="w_GMSELIMP",RowSource=""+"Impegni e articoli,"+"Impegni,"+"Articoli", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oGMSELIMP_2_83.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oGMSELIMP_2_83.GetRadio()
    this.Parent.oContained.w_GMSELIMP = this.RadioValue()
    return .t.
  endfunc

  func oGMSELIMP_2_83.SetRadio()
    this.Parent.oContained.w_GMSELIMP=trim(this.Parent.oContained.w_GMSELIMP)
    this.value = ;
      iif(this.Parent.oContained.w_GMSELIMP=='E',1,;
      iif(this.Parent.oContained.w_GMSELIMP=='I',2,;
      iif(this.Parent.oContained.w_GMSELIMP=='A',3,;
      0)))
  endfunc

  add object oGMORDPRD_2_88 as StdCheck with uid="VPCQFAZNXC",rtseq=90,rtrep=.f.,left=134, top=458, caption="Considera l'ordinato da produzione", enabled=.f.,;
    ToolTipText = "Considera l'ordinato durante l'elaborazione",;
    HelpContextID = 233974102,;
    cFormVar="w_GMORDPRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGMORDPRD_2_88.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGMORDPRD_2_88.GetRadio()
    this.Parent.oContained.w_GMORDPRD = this.RadioValue()
    return .t.
  endfunc

  func oGMORDPRD_2_88.SetRadio()
    this.Parent.oContained.w_GMORDPRD=trim(this.Parent.oContained.w_GMORDPRD)
    this.value = ;
      iif(this.Parent.oContained.w_GMORDPRD=='S',1,;
      0)
  endfunc

  add object oGMIMPPRD_2_89 as StdCheck with uid="RYJDILNBMU",rtseq=91,rtrep=.f.,left=134, top=480, caption="Considera l'impegnato da produzione", enabled=.f.,;
    ToolTipText = "Considera l'ordinato durante l'elaborazione",;
    HelpContextID = 221743446,;
    cFormVar="w_GMIMPPRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGMIMPPRD_2_89.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGMIMPPRD_2_89.GetRadio()
    this.Parent.oContained.w_GMIMPPRD = this.RadioValue()
    return .t.
  endfunc

  func oGMIMPPRD_2_89.SetRadio()
    this.Parent.oContained.w_GMIMPPRD=trim(this.Parent.oContained.w_GMIMPPRD)
    this.value = ;
      iif(this.Parent.oContained.w_GMIMPPRD=='S',1,;
      0)
  endfunc

  add object oStr_2_23 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=3, Top=161,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=416, Top=161,;
    Alignment=1, Width=89, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="NMLJSVRZGE",Visible=.t., Left=3, Top=36,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="XRBSMYKOYR",Visible=.t., Left=3, Top=61,;
    Alignment=1, Width=99, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=3, Top=86,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=3, Top=111,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=3, Top=136,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="BETNLUNOYI",Visible=.t., Left=416, Top=86,;
    Alignment=1, Width=89, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=416, Top=111,;
    Alignment=1, Width=89, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=413, Top=136,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="GKIMPOMLOZ",Visible=.t., Left=660, Top=37,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="EUAAIOZOQT",Visible=.t., Left=710, Top=60,;
    Alignment=1, Width=49, Height=15,;
    Caption="A LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="QTYVSGEGTT",Visible=.t., Left=504, Top=214,;
    Alignment=1, Width=99, Height=15,;
    Caption="Elabora catena:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="SGSTIMCHVV",Visible=.t., Left=3, Top=185,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="ZYLPGFCGXX",Visible=.t., Left=416, Top=185,;
    Alignment=1, Width=89, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="XMTZKDDJKA",Visible=.t., Left=13, Top=406,;
    Alignment=1, Width=117, Height=15,;
    Caption="Data inizio elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_66 as StdString with uid="ZFEHJZWTEA",Visible=.t., Left=7, Top=431,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data fine elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_67 as StdString with uid="ZEAIRXNLNG",Visible=.t., Left=291, Top=580,;
    Alignment=1, Width=105, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_68 as StdString with uid="LRYBARYNYB",Visible=.t., Left=25, Top=355,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="UVHEDKVHXW",Visible=.t., Left=25, Top=380,;
    Alignment=1, Width=105, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_72 as StdString with uid="FRFBQXOYXD",Visible=.t., Left=25, Top=255,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="FWPPGBGFDX",Visible=.t., Left=25, Top=280,;
    Alignment=1, Width=105, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_76 as StdString with uid="QPHXLBLWMU",Visible=.t., Left=268, Top=329,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_77 as StdString with uid="QUWNGCOYRD",Visible=.t., Left=381, Top=307,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_78 as StdString with uid="FSAEEGKAFC",Visible=.t., Left=388, Top=332,;
    Alignment=1, Width=88, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="TGVEUCHXYZ",Visible=.t., Left=25, Top=305,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_80 as StdString with uid="VZMOFHLMWU",Visible=.t., Left=25, Top=330,;
    Alignment=1, Width=105, Height=15,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_81 as StdString with uid="KLVNNVVWGD",Visible=.t., Left=268, Top=305,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_82 as StdString with uid="JUEPGIFVRE",Visible=.t., Left=6, Top=512,;
    Alignment=1, Width=124, Height=18,;
    Caption="Filtri impegni/articoli:"  ;
  , bGlobalFont=.t.

  add object oStr_2_85 as StdString with uid="NTWHWPMLOA",Visible=.t., Left=15, Top=9,;
    Alignment=0, Width=331, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_2_87 as StdString with uid="CGZLNFUULM",Visible=.t., Left=17, Top=234,;
    Alignment=0, Width=331, Height=18,;
    Caption="Ulteriori selezioni"  ;
  , bGlobalFont=.t.

  add object oBox_2_84 as StdBox with uid="UDTMWKFXRP",left=8, top=26, width=785,height=1

  add object oBox_2_86 as StdBox with uid="MVBPXCGGGS",left=8, top=250, width=785,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_agm','SEL__MRP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GMSERIAL=SEL__MRP.GMSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
