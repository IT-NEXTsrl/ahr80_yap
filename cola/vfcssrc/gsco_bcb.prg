* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bcb                                                        *
*              Costruisce bidoni temporali                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-21                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bcb",oParentObject)
return(i_retval)

define class tgsco_bcb as StdBatch
  * --- Local variables
  w_PPNMAXPE = 0
  nPerGio = 0
  nPerMes = 0
  nPerTri = 0
  nPerSet = 0
  w_PDPERASS = space(3)
  w_PDDATEVA = ctod("  /  /  ")
  w_Year = 0
  w_gDatIni = ctod("  /  /  ")
  w_gDatFin = ctod("  /  /  ")
  w_sDatIni = ctod("  /  /  ")
  w_sDatFin = ctod("  /  /  ")
  w_mDatIni = ctod("  /  /  ")
  w_mDatFin = ctod("  /  /  ")
  w_tDatIni = ctod("  /  /  ")
  w_tDatFin = ctod("  /  /  ")
  TotPer = 0
  TempN = 0
  TmpC = space(10)
  TmpD = space(10)
  DatProgr = ctod("  /  /  ")
  DataDiBase = ctod("  /  /  ")
  NumPer = 0
  TmpN1 = 0
  TmpN = 0
  TmpC1 = space(3)
  TmpD1 = ctod("  /  /  ")
  cicla = .f.
  TmpN2 = 0
  TmpC2 = space(4)
  TmpD2 = ctod("  /  /  ")
  TmpD3 = ctod("  /  /  ")
  gDI = ctod("  /  /  ")
  gDF = ctod("  /  /  ")
  sDI = ctod("  /  /  ")
  sDF = ctod("  /  /  ")
  mDI = ctod("  /  /  ")
  mDF = ctod("  /  /  ")
  tDI = ctod("  /  /  ")
  tDF = ctod("  /  /  ")
  i = 0
  w_ORAELA = space(5)
  * --- WorkFile variables
  MPS_TPER_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Bidoni Temporali (da GSMR_BGP)
    * --- Legge da tabella parametri
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPNMAXPE"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPNMAXPE;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.nPerGio = NVL(cp_ToDate(_read_.PPNUMGIO),cp_NullValue(_read_.PPNUMGIO))
      this.nPerSet = NVL(cp_ToDate(_read_.PPNUMSET),cp_NullValue(_read_.PPNUMSET))
      this.nPerMes = NVL(cp_ToDate(_read_.PPNUMMES),cp_NullValue(_read_.PPNUMMES))
      this.nPerTri = NVL(cp_ToDate(_read_.PPNUMTRI),cp_NullValue(_read_.PPNUMTRI))
      this.w_PPNMAXPE = NVL(cp_ToDate(_read_.PPNMAXPE),cp_NullValue(_read_.PPNMAXPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PPNMAXPE = iif(this.w_PPNMAXPE=0,54,this.w_PPNMAXPE)
    * --- Variabili relativi ai bidoni temporali
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Crea Array con periodi da esaminare
    this.TotPer = 100
    dimension Periodi(this.TotPer)
    dimension Datini(this.TotPer)
    dimension Datfin(this.TotPer)
    this.gDI = this.w_gDatIni
    this.gDF = this.w_gDatFin
    this.sDI = this.w_sDatIni
    this.sDF = this.w_sDatFin
    this.mDI = this.w_mDatIni
    this.mDF = this.w_mDatFin
    this.tDI = this.w_tDatIni
    this.tDF = this.w_tDatFin
    * --- Giorni
    this.DatProgr = this.gDI
    * --- Legge Calendario
    this.DataDiBase = this.gDI
    vq_exec("..\COLA\exe\query\GSCO1BLT", this, "__Calend__")
    * --- Copia tutto su un array
    dimension CalAzie(1)
    select cp_TODATE(__Calend__.CAGIORNO) from __Calend__ into array CalAzie
    use in __Calend__
    this.TmpN1 = this.DatProgr - Date(year(this.DatProgr),1,1) + 1
    do while this.DatProgr <= this.gDF
      this.NumPer = this.NumPer +1
      Periodi(this.NumPer) = "G"+right("000"+alltrim(str(this.TmpN1,3,0)),3)
      Datini(this.NumPer) = this.DatProgr
      Datfin(this.NumPer) = this.DatProgr
      this.DatProgr = this.DatProgr +1
      this.TmpN1 = this.TmpN1+1
      if this.TmpN1 = 366+iif(day(cp_CharToDate("28-02-"+str(year(this.DatProgr)+1,4,0)))=29,1,0)
        this.TmpN1 = 1
      endif
    enddo
    * --- Settimane
    * --- La settimana inizia Luned� e contiene sette giorni
    this.TmpN1 = week(this.DatProgr, 2, 2)
    do while this.DatProgr <= this.sDF
      this.NumPer = this.NumPer +1
      * --- Aggiungo 6 gg in modo da utilizzare la week() con la data della fine della settimana
      *     cos� calcola sempre la sett 52 e 53 riferita al'anno corretto (caso di settimana a cavallo tra due anni)
      do case
        case week(this.DatProgr + 6 ,2,2)=52 Or week(this.DatProgr + 6,2,2)=53
          this.w_Year = year(this.DatProgr)
        otherwise
          this.w_Year = year(this.DatProgr+ 6)
      endcase
      Periodi(this.NumPer) = "S"+right(str(this.w_Year,4,0),1) +right("0"+alltrim(str(week(this.DatProgr + 6, 2, 2),2,0)),2)
      DatIni(this.NumPer) = this.DatProgr
      Datfin(this.NumPer) = this.DatProgr+6
      * --- Data inizio settimana successiva
      this.DatProgr = this.DatProgr +7
    enddo
    * --- Mesi
    do while this.DatProgr <= this.mDF
      this.NumPer = this.NumPer +1
      Datini(this.NumPer) = this.DatProgr
      Periodi(this.NumPer) = "M"+right(str(year(this.DatProgr),4,0),1) + right("0"+alltrim(str(month(this.DatProgr),2,0)),2)
      if month(this.DatProgr)=12
        this.DatProgr = Date(year(this.DatProgr)+1, 1, 1)
      else
        this.DatProgr = Date(year(this.DatProgr), month(this.DatProgr)+1, 1)
      endif
      Datfin(this.NumPer) = this.DatProgr-1
    enddo
    * --- Trimestri
    do while this.DatProgr <= this.tDF
      this.NumPer = this.NumPer +1
      Datini(this.NumPer) = this.DatProgr
      Periodi(this.NumPer) = "T" + right(str(year(this.DatProgr),4,0),1) + "0" + str((month(this.DatProgr)+2)/3,1,0)
      if month(this.DatProgr)=10
        this.DatProgr = Date(year(this.DatProgr)+1, 1, 1)
      else
        this.DatProgr = Date(year(this.DatProgr), month(this.DatProgr)+3, 1)
      endif
      Datfin(this.NumPer) = this.DatProgr-1
    enddo
    if this.NumPer > this.w_PPNMAXPE
      ah_ErrorMsg("Il numero di time buckets (%1) supera il limite consentito (%2)%0Impossibile generare il nuovo orizzonte temporale","STOP","", alltrim(str(this.NumPer,10,0)), alltrim(str(this.w_PPNMAXPE,10,0)) )
      i_retcode = 'stop'
      return
    else
      * --- Aggiorna Tabella Periodi
      this.TmpC1 = "000"
      this.TmpC2 = "SCAD"
      this.TmpD1 = i_INIDAT
      this.TmpD2 = cp_ToDate(Datini(1)) -1
      * --- Try
      local bErr_0261AE28
      bErr_0261AE28=bTrsErr
      this.Try_0261AE28()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into MPS_TPER
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TPER_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TPPERREL ="+cp_NullLink(cp_ToStrODBC(this.TmpC2),'MPS_TPER','TPPERREL');
          +",TPDATINI ="+cp_NullLink(cp_ToStrODBC(this.TmpD1),'MPS_TPER','TPDATINI');
          +",TPDATFIN ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'MPS_TPER','TPDATFIN');
          +",TPPRILAV ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'MPS_TPER','TPPRILAV');
              +i_ccchkf ;
          +" where ";
              +"TPPERASS = "+cp_ToStrODBC(this.TmpC1);
                 )
        else
          update (i_cTable) set;
              TPPERREL = this.TmpC2;
              ,TPDATINI = this.TmpD1;
              ,TPDATFIN = this.TmpD2;
              ,TPPRILAV = this.TmpD2;
              &i_ccchkf. ;
           where;
              TPPERASS = this.TmpC1;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_0261AE28
      * --- End
      this.i = 1
      do while this.i <= this.TotPer
        this.TmpC1 = right("000"+alltrim(Str(this.i,3,0)),3)
        if this.i <= this.NumPer
          this.TmpC2 = Periodi(this.i)
          this.TmpD1 = Datini(this.i)
          this.TmpD2 = Datfin(this.i)
          * --- Determina prima giorno lavorativo del periodo
          this.TmpD3 = cp_CharToDate("  -  -  ")
          this.TmpD = cp_ToDate(this.TmpD1)
          do while this.TmpD<=this.TmpD2 and empty(this.TmpD3)
            this.TmpN = AScan(CalAzie,this.TmpD)
            if this.TmpN<>0
              * --- Giorno lavorativo !
              this.TmpD3 = CalAzie(this.TmpN)
            endif
            this.TmpD = this.TmpD +1
          enddo
        else
          this.TmpC2 = "XXXX"
          this.TmpD1 = cp_CharToDate("  -  -  ")
          this.TmpD2 = cp_CharToDate("  -  -  ")
          this.TmpD3 = cp_CharToDate("  -  -  ")
        endif
        * --- Try
        local bErr_03670748
        bErr_03670748=bTrsErr
        this.Try_03670748()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into MPS_TPER
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TPER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TPER_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TPPERREL ="+cp_NullLink(cp_ToStrODBC(this.TmpC2),'MPS_TPER','TPPERREL');
            +",TPDATINI ="+cp_NullLink(cp_ToStrODBC(this.TmpD1),'MPS_TPER','TPDATINI');
            +",TPDATFIN ="+cp_NullLink(cp_ToStrODBC(this.TmpD2),'MPS_TPER','TPDATFIN');
            +",TPPRILAV ="+cp_NullLink(cp_ToStrODBC(this.TmpD3),'MPS_TPER','TPPRILAV');
                +i_ccchkf ;
            +" where ";
                +"TPPERASS = "+cp_ToStrODBC(this.TmpC1);
                   )
          else
            update (i_cTable) set;
                TPPERREL = this.TmpC2;
                ,TPDATINI = this.TmpD1;
                ,TPDATFIN = this.TmpD2;
                ,TPPRILAV = this.TmpD3;
                &i_ccchkf. ;
             where;
                TPPERASS = this.TmpC1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03670748
        * --- End
        this.i = this.i + 1
      enddo
      * --- Aggiorna Periodi in PAR_PROD
      this.w_ORAELA = left(time(),5)
      * --- Write into PAR_PROD
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PPGDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_gDatIni),'PAR_PROD','PPGDTINI');
        +",PPTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_tDatFin),'PAR_PROD','PPTDTFIN');
        +",PPELAVER ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PAR_PROD','PPELAVER');
        +",PPORAVER ="+cp_NullLink(cp_ToStrODBC(this.w_ORAELA),'PAR_PROD','PPORAVER');
        +",PPNUMPER ="+cp_NullLink(cp_ToStrODBC(this.NumPer),'PAR_PROD','PPNUMPER');
        +",PPOPEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_PROD','PPOPEELA');
            +i_ccchkf ;
        +" where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
               )
      else
        update (i_cTable) set;
            PPGDTINI = this.w_gDatIni;
            ,PPTDTFIN = this.w_tDatFin;
            ,PPELAVER = i_DATSYS;
            ,PPORAVER = this.w_ORAELA;
            ,PPNUMPER = this.NumPer;
            ,PPOPEELA = i_CODUTE;
            &i_ccchkf. ;
         where;
            PPCODICE = "PP";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Messaggio di avvenuta generazione solo se da GSCO_KCB
      if UPPER(this.oParentObject.class) = "TGSCO_KCB"
        ah_ErrorMsg("Generazione eseguita con successo","","")
        i_retcode = 'stop'
        return
      endif
    endif
  endproc
  proc Try_0261AE28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TPER
    i_nConn=i_TableProp[this.MPS_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TPER_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TPPERASS"+",TPPERREL"+",TPDATINI"+",TPDATFIN"+",TPPRILAV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.TmpC1),'MPS_TPER','TPPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpC2),'MPS_TPER','TPPERREL');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD1),'MPS_TPER','TPDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'MPS_TPER','TPDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'MPS_TPER','TPPRILAV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TPPERASS',this.TmpC1,'TPPERREL',this.TmpC2,'TPDATINI',this.TmpD1,'TPDATFIN',this.TmpD2,'TPPRILAV',this.TmpD2)
      insert into (i_cTable) (TPPERASS,TPPERREL,TPDATINI,TPDATFIN,TPPRILAV &i_ccchkf. );
         values (;
           this.TmpC1;
           ,this.TmpC2;
           ,this.TmpD1;
           ,this.TmpD2;
           ,this.TmpD2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03670748()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TPER
    i_nConn=i_TableProp[this.MPS_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TPER_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TPPERASS"+",TPPERREL"+",TPDATINI"+",TPDATFIN"+",TPPRILAV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.TmpC1),'MPS_TPER','TPPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpC2),'MPS_TPER','TPPERREL');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD1),'MPS_TPER','TPDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD2),'MPS_TPER','TPDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpD3),'MPS_TPER','TPPRILAV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TPPERASS',this.TmpC1,'TPPERREL',this.TmpC2,'TPDATINI',this.TmpD1,'TPDATFIN',this.TmpD2,'TPPRILAV',this.TmpD3)
      insert into (i_cTable) (TPPERASS,TPPERREL,TPDATINI,TPDATFIN,TPPRILAV &i_ccchkf. );
         values (;
           this.TmpC1;
           ,this.TmpC2;
           ,this.TmpD1;
           ,this.TmpD2;
           ,this.TmpD3;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.nPerGio*this.nPerSet*this.nPerMes*this.nPerTri=0
      ah_ErrorMsg("Verificare parametri time buckets. impossibile creare orizzonte temporale","STOP","")
      i_retcode = 'stop'
      return
    endif
    * --- Bidone periodi GIORNALIERI
    this.w_gDatIni = i_DATSYS
    this.w_gDatFin = this.w_gDatIni + this.nPerGio - 1
    do while dow(this.w_gDatFin)<>1
      this.w_gDatFin = this.w_gDatFin + 1
    enddo
    * --- Bidone periodi SETTIMANALI
    this.w_sDatIni = this.w_gDatFin +1
    this.w_sDatFin = this.w_sDatIni + this.nPerSet*7 -1
    * --- Verifica se e' l'ultimo giorno del mese
    if month(this.w_sDatFin) = month(this.w_sDatFin+1)
      * --- Cerca la prima domenica del mese successivo
      this.TempN = month(this.w_sDatFin) +1
      if this.TempN > 12
        this.TempN = this.TempN - 12
        this.w_sDatFin = cp_CharToDate("01-"+str(this.TempN,2,0)+"-"+str(year(this.w_sDatFin)+1,4,0))
      else
        this.w_sDatFin = cp_CharToDate("01-"+str(this.TempN,2,0)+"-"+str(year(this.w_sDatFin),4,0))
      endif
      * --- Cerca la prima domenica del  mese
      this.w_sDatFin = this.w_sDatFin -1
      do while dow(this.w_sDatFin)<>1
        this.w_sDatFin = this.w_sDatFin + 1
      enddo
    endif
    * --- Bidone periodi MENSILI
    this.w_mDatIni = this.w_sDatFin +1
    this.TempN = month(this.w_mDatIni) +this.nPerMes
    this.w_mDatFin = Date(year(this.w_mDatIni)+int((this.TempN-1)/12), mod(this.TempN-1,12)+1, 1) -1
    * --- E' l'ultimo mese del trimestre di appartenenza ?
    if mod(month(this.w_mDatFin),3) <>0
      this.TempN = int(month(this.w_mDatFin)/3)*3+4
      this.w_mDatFin = Date(year(this.w_mDatFin)+int(this.TempN/12)+iif(mod(this.TempN,12)=0,1,0), mod(this.TempN-1,12)+1, 1) -1
    endif
    * --- Bidone periodi TRIMESTRALI
    this.w_tDatIni = this.w_mDatFin +1
    this.TempN = month(this.w_tDatIni) + 3*this.nPerTri
    this.w_tDatFin = Date(year(this.w_tDatIni)+int(this.TempN/12)+iif(mod(this.TempN,12)=0,1,0), mod(this.TempN-1,12)+1, 1) -1
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MPS_TPER'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='PAR_PROD'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
