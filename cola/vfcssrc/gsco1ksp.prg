* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco1ksp                                                        *
*              Importa ordini                                                  *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-12                                                      *
* Last revis.: 2016-06-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco1ksp",oParentObject))

* --- Class definition
define class tgsco1ksp as StdForm
  Top    = -2
  Left   = 5

  * --- Standard Properties
  Width  = 850
  Height = 490
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-06-06"
  HelpContextID=166222487
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  MAGAZZIN_IDX = 0
  ODL_MAST_IDX = 0
  RIS_ORSE_IDX = 0
  TAB_RISO_IDX = 0
  cPrg = "gsco1ksp"
  cComment = "Importa ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SCCODRIS = space(20)
  w_SCTIPPIA = space(1)
  w_SCTIPSCA = space(3)
  w_SCPIAINI = ctod('  /  /  ')
  w_SCPIAFIN = ctod('  /  /  ')
  w_SCODLSUG = space(1)
  w_SCODLPIA = space(1)
  w_SCODLLAN = space(1)
  w_SELEZI = space(1)
  w_RLDESCRI = space(40)
  w_OPZVIS = space(1)
  w_ZoomMast = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco1kspPag1","gsco1ksp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Lista scaletta")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCCODRIS_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='TAB_RISO'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SCCODRIS=space(20)
      .w_SCTIPPIA=space(1)
      .w_SCTIPSCA=space(3)
      .w_SCPIAINI=ctod("  /  /  ")
      .w_SCPIAFIN=ctod("  /  /  ")
      .w_SCODLSUG=space(1)
      .w_SCODLPIA=space(1)
      .w_SCODLLAN=space(1)
      .w_SELEZI=space(1)
      .w_RLDESCRI=space(40)
      .w_OPZVIS=space(1)
        .w_SCCODRIS = this.oParentObject.oParentObject.w_SCTABRIS
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SCCODRIS))
          .link_1_1('Full')
        endif
        .w_SCTIPPIA = this.oparentobject.w_SCTIPPIA
        .w_SCTIPSCA = this.oparentobject.w_SCTIPSCA
        .w_SCPIAINI = this.oparentobject.w_DATINI
        .w_SCPIAFIN = this.oparentobject.w_DATFIN
        .w_SCODLSUG = this.oParentObject.oParentObject.w_SCODLSUG
        .w_SCODLPIA = this.oParentObject.oParentObject.w_SCODLPIA
        .w_SCODLLAN = this.oParentObject.oParentObject.w_SCODLLAN
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .w_SELEZI = "D"
          .DoRTCalc(10,10,.f.)
        .w_OPZVIS = 'T'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsco1ksp
    if alltrim(lower(cEvent))='w_zoommast after query'
        TrsCurs=this.oParentObject.oParentObject.cTrsName
        TrsZoom=this.w_zoommast.cCursor
    
             *--- escludo dallo zoom le fasi di ordini gi� inserite se la qta inserita � minore della qta della fase
             select t_sccododl, sum(t_scqtaric) as scqtaric from (TrsCurs) into cursor tmpodl group by 1
        if this.w_OPZVIS='N'
             DELETE (TrsZoom) FROM tmpodl WHERE OLCODODL=t_sccododl 
          else
             UPDATE (TrsZoom) SET INSERITO='S' FROM tmpodl WHERE OLCODODL=t_sccododl 
        endif
         use in select("tmpodl")
         select (TrsZoom)
         Go top
         this.w_zoommast.grd.refresh()
    endif
    
    
    if alltrim(lower(cEvent))='update start'
       TrsZoom=this.w_zoommast.cCursor
       select * from (TrsZoom) where xchk=1 into cursor LstTmp
    endif
    
    if alltrim(lower(cEvent))='w_selezi changed'
      if reccount(this.w_zoommast.cCursor)>0
         SELECT (this.w_zoommast.cCursor)
         l_pos=RECNO()
         if this.w_SELEZI='S'
           update (this.w_zoommast.cCursor) set xchk=1
         else
           update (this.w_zoommast.cCursor) set xchk=0
         endif
         SELECT (this.w_zoommast.cCursor)
         GO l_pos
     endif
     endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCCODRIS
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_lTable = "TAB_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2], .t., this.TAB_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_SCCODRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_SCCODRIS))
          select RICODICE,RIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODRIS)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_RISO','*','RICODICE',cp_AbsName(oSource.parent,'oSCCODRIS_1_1'),i_cWhere,'',"",'GSCO_ZLR.TAB_RISO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_SCCODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_SCCODRIS)
            select RICODICE,RIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODRIS = NVL(_Link_.RICODICE,space(20))
      this.w_RLDESCRI = NVL(_Link_.RIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODRIS = space(20)
      endif
      this.w_RLDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCCODRIS_1_1.value==this.w_SCCODRIS)
      this.oPgFrm.Page1.oPag.oSCCODRIS_1_1.value=this.w_SCCODRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCPIAINI_1_4.value==this.w_SCPIAINI)
      this.oPgFrm.Page1.oPag.oSCPIAINI_1_4.value=this.w_SCPIAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCPIAFIN_1_5.value==this.w_SCPIAFIN)
      this.oPgFrm.Page1.oPag.oSCPIAFIN_1_5.value=this.w_SCPIAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCODLSUG_1_6.RadioValue()==this.w_SCODLSUG)
      this.oPgFrm.Page1.oPag.oSCODLSUG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCODLPIA_1_7.RadioValue()==this.w_SCODLPIA)
      this.oPgFrm.Page1.oPag.oSCODLPIA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCODLLAN_1_8.RadioValue()==this.w_SCODLLAN)
      this.oPgFrm.Page1.oPag.oSCODLLAN_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_15.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_15.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oOPZVIS_1_17.RadioValue()==this.w_OPZVIS)
      this.oPgFrm.Page1.oPag.oOPZVIS_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco1kspPag1 as StdContainer
  Width  = 846
  height = 490
  stdWidth  = 846
  stdheight = 490
  resizeXpos=513
  resizeYpos=273
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCODRIS_1_1 as StdField with uid="ZQPKHRTOJH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SCCODRIS", cQueryName = "SCCODRIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 8115065,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=110, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="TAB_RISO", oKey_1_1="RICODICE", oKey_1_2="this.w_SCCODRIS"

  func oSCCODRIS_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODRIS_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODRIS_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_RISO','*','RICODICE',cp_AbsName(this.parent,'oSCCODRIS_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCO_ZLR.TAB_RISO_VZM',this.parent.oContained
  endproc

  add object oSCPIAINI_1_4 as StdField with uid="ZNIJUUEPOK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCPIAINI", cQueryName = "SCPIAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 146365585,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=610, Top=14

  add object oSCPIAFIN_1_5 as StdField with uid="AKBNYOWGLC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCPIAFIN", cQueryName = "SCPIAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 71738228,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=764, Top=14

  add object oSCODLSUG_1_6 as StdCheck with uid="QZKNLJRRYM",rtseq=6,rtrep=.f.,left=261, top=437, caption="Suggerito MRP",;
    HelpContextID = 32609133,;
    cFormVar="w_SCODLSUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCODLSUG_1_6.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oSCODLSUG_1_6.GetRadio()
    this.Parent.oContained.w_SCODLSUG = this.RadioValue()
    return .t.
  endfunc

  func oSCODLSUG_1_6.SetRadio()
    this.Parent.oContained.w_SCODLSUG=trim(this.Parent.oContained.w_SCODLSUG)
    this.value = ;
      iif(this.Parent.oContained.w_SCODLSUG=='M',1,;
      0)
  endfunc

  add object oSCODLPIA_1_7 as StdCheck with uid="LPXUOLRLFD",rtseq=7,rtrep=.f.,left=405, top=437, caption="Pianificato",;
    HelpContextID = 250712935,;
    cFormVar="w_SCODLPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCODLPIA_1_7.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oSCODLPIA_1_7.GetRadio()
    this.Parent.oContained.w_SCODLPIA = this.RadioValue()
    return .t.
  endfunc

  func oSCODLPIA_1_7.SetRadio()
    this.Parent.oContained.w_SCODLPIA=trim(this.Parent.oContained.w_SCODLPIA)
    this.value = ;
      iif(this.Parent.oContained.w_SCODLPIA=='P',1,;
      0)
  endfunc

  add object oSCODLLAN_1_8 as StdCheck with uid="BRCIWEXSZW",rtseq=8,rtrep=.f.,left=514, top=437, caption="Lanciato",;
    HelpContextID = 84831372,;
    cFormVar="w_SCODLLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCODLLAN_1_8.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oSCODLLAN_1_8.GetRadio()
    this.Parent.oContained.w_SCODLLAN = this.RadioValue()
    return .t.
  endfunc

  func oSCODLLAN_1_8.SetRadio()
    this.Parent.oContained.w_SCODLLAN=trim(this.Parent.oContained.w_SCODLLAN)
    this.value = ;
      iif(this.Parent.oContained.w_SCODLLAN=='L',1,;
      0)
  endfunc


  add object ZoomMast as cp_szoombox with uid="FNBDDSEHEU",left=-1, top=40, width=841,height=395,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ODL_MAST",cZoomFile="GSCO_ZFO",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Blank,w_SCCODRIS Changed,w_SCPIAINI Changed,w_SCPIAFIN Changed,w_SCODLSUG Changed,w_SCODLPIA Changed,w_SCODLLAN Changed,w_OPZVIS Changed",;
    nPag=1;
    , HelpContextID = 192650778


  add object oBtn_1_10 as StdButton with uid="DERMGDMCBN",left=741, top=440, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per importare le righe dei materiali";
    , HelpContextID = 11267050;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="CNWSZLWHAD",left=792, top=440, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare la selezione";
    , HelpContextID = 11267050;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_12 as StdRadio with uid="XBGVNIVMJH",rtseq=9,rtrep=.f.,left=15, top=438, width=183,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 120429274
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 120429274
      this.Buttons(2).Top=15
      this.SetAll("Width",181)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_12.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oRLDESCRI_1_15 as StdField with uid="UKBIRZCQRJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228463521,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=222, Top=13, InputMask=replicate('X',40)

  add object oOPZVIS_1_17 as StdRadio with uid="JZNGYUDVPP",rtseq=11,rtrep=.f.,left=329, top=466, width=294,height=20;
    , cFormVar="w_OPZVIS", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oOPZVIS_1_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="tutti gli ordini"
      this.Buttons(1).HelpContextID = 237744154
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("tutti gli ordini","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="solo ordini non presenti"
      this.Buttons(2).HelpContextID = 237744154
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("solo ordini non presenti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oOPZVIS_1_17.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oOPZVIS_1_17.GetRadio()
    this.Parent.oContained.w_OPZVIS = this.RadioValue()
    return .t.
  endfunc

  func oOPZVIS_1_17.SetRadio()
    this.Parent.oContained.w_OPZVIS=trim(this.Parent.oContained.w_OPZVIS)
    this.value = ;
      iif(this.Parent.oContained.w_OPZVIS=='T',1,;
      iif(this.Parent.oContained.w_OPZVIS=='N',2,;
      0))
  endfunc

  add object oStr_1_13 as StdString with uid="BJDVDXKKSP",Visible=.t., Left=521, Top=17,;
    Alignment=1, Width=85, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HZEPCIJIHP",Visible=.t., Left=690, Top=17,;
    Alignment=1, Width=68, Height=18,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="UYTOATCKDN",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=100, Height=18,;
    Caption="Codice risorsa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="EAFFMZTXSJ",Visible=.t., Left=258, Top=465,;
    Alignment=1, Width=64, Height=18,;
    Caption="Mostra"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco1ksp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
