* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bsp                                                        *
*              Gestione scaletta di produzione                                 *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-01                                                      *
* Last revis.: 2017-04-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bsp",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_bsp as StdBatch
  * --- Local variables
  w_LRowNum = 0
  pAzione = space(15)
  w_Dettaglio = .f.
  w_QTASUP = .f.
  w_RET = .f.
  GSCI_MSP = .NULL.
  w_i = space(0)
  w_NRVARIATA = 0
  ngg = 0
  w_QTAPER = 0
  w_FOUND = .f.
  w_PERIODI = 0
  w_CODODL = space(20)
  w_ROWNUM = 0
  w_SCLAVSEC = 0
  w_CODRIS = space(20)
  w_OLCODODL = space(15)
  w_OLTDTINI = ctod("  /  /  ")
  w_OLTSEODL = space(15)
  w_OLTFAODL = 0
  w_obj = .NULL.
  w_OLTDTINI = ctod("  /  /  ")
  w_OLTCILAV = space(20)
  w_NREC = 0
  w_SCRISODL = space(20)
  w_SCRISFAS = space(20)
  w_SCTPRSFA = space(2)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATIN1 = ctod("  /  /  ")
  w_CODRIS = space(20)
  w_CODRIS1 = space(20)
  w_SERIAL = space(15)
  w_SERIAL1 = space(15)
  w_RL__TIPO = space(2)
  w_RL1_TIPO = space(2)
  w_LCLCODODL = space(15)
  w_LSCFASODL = 0
  w_LSCFASSEL = space(1)
  w_LQTAPRE = 0
  w_LQTAAVA = 0
  w_LQTASCA = 0
  w_LQTABCF = 0
  w_CONTCAPT = 0
  objColumn = .NULL.
  w_HdrColumn = .NULL.
  w_ForeColor = 0
  w_QTAORAODL = 0
  w_SCQTAORA = 0
  w_SCQTAODL = 0
  w_SCPROORA = 0
  w_APPO = 0
  w_LSerial = space(10)
  w_LOggErr = space(41)
  w_LErrore = space(80)
  w_LOra = space(8)
  w_LMess = space(0)
  w_LNumErr = 0
  w_LEERR = space(250)
  TmpC = space(100)
  w_OLTSEODL = space(15)
  w_OLTFAODL = 0
  w_OLTSECPR = space(15)
  GSCI_MSP = .NULL.
  GSCI_MSP = .NULL.
  oBody = .NULL.
  w_CTRSNAME = space(10)
  w_CONTCAP = 0
  w_CONT = 0
  w_QTAODL = 0
  MyChekRow = .f.
  w_ORECOOLD = 0
  w_ORECO = 0
  w_nAbsRow = 0
  w_nRelRow = 0
  w_QTAPER = 0
  w_ANNO = 0
  w_MESE = 0
  w_WEEK = 0
  w_SETTTOT = 0
  SfieldCount = 0
  fCount = 0
  fName = space(20)
  ffound = .f.
  w_SCTIPSCA = space(3)
  w_SCTIPPIA = space(1)
  w_SCTIPRIS = space(2)
  w_SCCODRIS = space(20)
  w_INCDAT = 0
  w_LTIPGES = space(3)
  w_MESS = space(200)
  w_PPCALSTA = space(5)
  w_DRQTARIS = 0
  w_DRCOEEFF = 0
  w_DRPERRID = 0
  w_bLockScreen = .f.
  w_SCDATINI = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_SCCODODL = space(15)
  w_SCFASODL = 0
  w_SCCODCIC = space(15)
  w_CLROWORD = 0
  w_SCQTARIC = 0
  w_SCQTAODL = 0
  w_SCCODICE = space(20)
  w_ARUNMIS = space(3)
  w_OLTCOART = space(20)
  w_ARUNMIS1 = space(3)
  w_FLCOUPOI = space(1)
  w_OLTSTATO = space(1)
  w_SCLAVSEC = 0
  w_SCPROORA = 0
  w_CODODL = space(20)
  w_ROWNUM = 0
  w_SCLAVSEC = 0
  w_CODRIS = space(20)
  w_SCDESFAS = space(40)
  w_SCDINRIC = ctod("  /  /  ")
  w_SCQTAORA = 0
  w_SLQTARES = space(1)
  w_OLTQTODL = 0
  w_OLTQTOEV = 0
  w_OLTQTOSC = 0
  w_OLTQTOBF = 0
  w_SCTPSLAV = 0
  w_SCQTMORA = 0
  w_SCQTMODL = 0
  w_RLDESCRI = space(40)
  w_OLTRESID = 0
  w_CLQTASCA = 0
  w_GRUPPO = 0
  w_CODCIC = space(5)
  w_DESCIC = space(40)
  w_CFUNC = space(10)
  w_INSFIRSTR = .f.
  w_SCTABRIS = space(20)
  w_OLDTPQTOR = space(1)
  w_PPCOLSUM = 0
  w_PPCOLPIA = 0
  w_PPCOLLAN = 0
  w_RLCODICE = space(20)
  w_SCROWNUM = 0
  w_CLDESFAS = space(40)
  w_CLQTAPRE = 0
  w_CLPREUM1 = 0
  w_OLTUNMIS = space(3)
  w_CLULTFAS = space(1)
  w_CLQTAAVA = 0
  w_CLFASEVA = space(1)
  w_CLQTABCF = 0
  w_CLBCFUM1 = 0
  w_CLAVAUM1 = 0
  w_RLTPSLAU = 0
  w_RLTPSLAV = 0
  w_OLTQTOD1 = 0
  w_SCDESFAS = space(40)
  w_NUMRECOR = 0
  w_GIORNO = ctod("  /  /  ")
  w_SETTIMA = ctod("  /  /  ")
  w_MESE = ctod("  /  /  ")
  w_BIMESTRE = ctod("  /  /  ")
  w_TRIMESTR = ctod("  /  /  ")
  w_QUADRIME = ctod("  /  /  ")
  w_SEMESTRE = ctod("  /  /  ")
  w_ANNO = ctod("  /  /  ")
  w_SCRISODL = space(15)
  w_SCTSEODL = space(15)
  w_OLTCODIC = space(20)
  w_OLDESART = space(40)
  w_SCDESART = space(40)
  * --- WorkFile variables
  PAR_PROD_idx=0
  ODL_CICL_idx=0
  ODL_RISO_idx=0
  ODL_MAST_idx=0
  PRD_ERRO_idx=0
  SCALETTA_idx=0
  ART_ICOL_idx=0
  CIC_MAST_idx=0
  RIS_ORSE_idx=0
  SCALETTM_idx=0
  TABMCICL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera scaletta di Produzione (da GSCI_MSP)
    this.GSCI_MSP = this.oParentObject
    this.oBody = this.GSCI_MSP.oPgFrm.Page1.oPag.oBody
    this.w_CTRSNAME = this.GSCI_MSP.cTrsname
    this.w_SCDATINI = this.GSCI_MSP.w_SCDATINI
    * --- Dati Comuni alle gestioni
    this.w_SCTIPSCA = this.GSCI_MSP.w_SCTIPSCA
    this.w_SCTIPPIA = this.GSCI_MSP.w_SCTIPPIA
    * --- Dati piano cicli semplificati
    this.w_SCTABRIS = this.GSCI_MSP.w_SCTABRIS
    * --- Dati piano cicli di lavorazione
    this.w_SCTIPRIS = this.GSCI_MSP.w_SCTIPRIS
    this.w_SCCODRIS = this.GSCI_MSP.w_SCCODRIS
    do case
      case this.pAzione="Importa"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.pAzione="Riordina"
      case this.pAzione="CHND_QO"
        SELECT (this.w_CTRSNAME)
        if this.oParentObject.w_SCTPQTOR="Q"
          UPDATE (this.w_CTRSNAME) Set t_SCQTARIC = t_SCQTMODL WHERE t_SCQTARIC <> t_SCQTAORA
          UPDATE (this.w_CTRSNAME) Set t_SCQTARIC = t_SCQTAODL WHERE t_SCQTARIC = t_SCQTAORA
          this.pAzione="Riordina"
        else
          UPDATE (this.w_CTRSNAME) Set t_SCQTARIC = t_SCQTMORA WHERE t_SCQTARIC <> t_SCQTAODL
          UPDATE (this.w_CTRSNAME) Set t_SCQTARIC = t_SCQTAORA WHERE t_SCQTARIC = t_SCQTAODL
          this.pAzione="Riordina"
        endif
      case this.pAzione="QTAVAR"
        this.w_SCQTARIC = this.GSCI_MSP.w_SCQTARIC
        this.w_SCQTAODL = this.GSCI_MSP.w_SCQTAODL
        if this.w_SCTIPSCA <> "CSQ"
          this.w_SCPROORA = this.GSCI_MSP.w_SCPROORA
          this.w_SCQTAORA = this.GSCI_MSP.w_SCQTAORA
          this.w_MESS = "La quantit� impostata � superiore alla quantit� della fase%0Continuare lo stesso?"
        else
          this.w_MESS = "La quantit� impostata � superiore alla quantit� dell'ordine%0Continuare lo stesso?"
        endif
        this.w_QTASUP = False
        this.w_RET = False
        do case
          case this.w_SCTIPSCA == "CLO"
            if this.oParentObject.w_SCTPQTOR="Q"
              if this.w_SCQTARIC > this.w_SCQTAODL
                this.w_QTASUP = True
              endif
            else
              if this.w_SCQTARIC > this.w_SCQTAORA
                this.w_QTASUP = True
              endif
            endif
          otherwise
            if this.w_SCQTARIC > this.w_SCQTAODL
              this.w_QTASUP = True
            endif
        endcase
        if this.w_QTASUP
          this.w_RET = ah_YESNO(this.w_MESS)
        endif
        if Not this.w_QTASUP or (this.w_QTASUP and this.w_RET)
          do case
            case this.w_SCTIPSCA == "CLO"
              if this.oParentObject.w_SCTPQTOR="Q"
                this.w_SCQTMORA = this.w_SCQTARIC / this.w_SCPROORA
                this.w_SCQTMODL = this.w_SCQTARIC
              else
                this.w_SCQTMORA = this.w_SCQTARIC * this.w_SCPROORA
                this.w_SCQTMODL = this.w_SCQTARIC
              endif
            otherwise
              this.w_SCQTMORA = this.w_SCQTARIC / this.w_SCPROORA
              this.w_SCQTMODL = this.w_SCQTARIC
              this.w_SCQTMORA = 0
          endcase
        else
          do case
            case this.w_SCTIPSCA == "CLO"
              this.w_SCQTMORA = this.w_SCQTAORA
              this.w_SCQTMODL = this.w_SCQTAODL
              if this.oParentObject.w_SCTPQTOR="Q"
                this.w_SCQTARIC = this.w_SCQTMODL
              else
                this.w_SCQTARIC = this.w_SCQTMORA
              endif
            otherwise
              this.w_SCQTMORA = this.w_SCQTAODL / this.w_SCPROORA
              this.w_SCQTMODL = this.w_SCQTAODL
              this.w_SCQTARIC = this.w_SCQTMODL
              this.w_SCQTMORA = 0
          endcase
          this.GSCI_MSP.w_SCQTARIC = this.w_SCQTARIC
        endif
        if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
          this.GSCI_MSP.w_SCQTMORA = this.w_SCQTMORA
        endif
        this.GSCI_MSP.w_SCQTMODL = this.w_SCQTMODL
        this.GSCI_MSP.SetControlsValue()     
        this.GSCI_MSP.SaveDependsOn()     
        this.GSCI_MSP.TrsFromWork()     
    endcase
    do case
      case this.pAzione=="Riordina"
        if used("RecDel")
          use in RecDel 
        endif
        if used("Scaletta")
          use in Scaletta
        endif
        this.w_MESS = "Aggiornamento sequenza in corso..."
        ah_Msg (this.w_MESS)
        this.GSCI_MSP.LockScreen = .t.
        this.w_bLockScreen = this.GSCI_MSP.__dummy__.enabled
        this.GSCI_MSP.__dummy__.enabled = .t.
        this.GSCI_MSP.__dummy__.SetFocus()     
        this.GSCI_MSP.w_VARIATA = .f.
        this.w_nRelRow = 0
        this.w_nAbsRow = 0
        SELECT (this.w_CTRSNAME)
        this.w_ORECO = CPROWNUM
        this.w_NRVARIATA = this.GSCI_MSP.w_NRVARIATA
        SELECT (this.w_CTRSNAME)
        GO TOP
        * --- Azzero Quantit� consumata nel periodo prima di iniziare l'elaborazione
        SCAN FOR !DELETED()
        this.w_CONT = 1
        do while this.w_CONT<=14
          icap = padl(alltrim(str(this.w_CONT)), 2, "0")
          if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
            replace t_SCQTAP&icap with 0, t_SCQTAH&icap with 0, i_srv WITH IIF(i_srv<> "A","U", i_srv)
          else
            replace t_SCQTAP&icap with 0, i_srv WITH IIF(i_srv<> "A","U", i_srv)
          endif
          this.w_CONT = this.w_CONT+1
        enddo
        SELECT (this.w_CTRSNAME) 
 REPLACE t_QTARES WITH 0
        SELECT (this.w_CTRSNAME)
        ENDSCAN
        * --- Metto le righe buone in un cursore d'appoggio Scaletta
        *     gi� ordinate correttamente
        Local l_cWhere, l_cOrderBy 
 Store "" to l_cWhere, l_cOrderBy
        l_cWhere = ' (!empty(nvl(t_SCCODODL," ")) and !empty(nvl(t_SCCODCIC," ")) ) AND !DELETED()'
        l_cOrderBy = " t_CPROWORD, t_SCCODODL"
        if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
          l_cOrderBy = l_cOrderBy + ", t_SCFASODL"
        endif
        this.GSCI_MSP.Exec_Select("Scaletta", "*", l_cWhere, l_cOrderby)     
        i_olddel=SET ("DELETED")
        SET DELETED OFF
        * --- Metto le righe cancellate in un cursore di appoggio RecDel
        SELECT * from (this.w_CTRSNAME) INTO CURSOR RecDel WHERE DELETED()
        SELECT (this.w_CTRSNAME)
        * --- Elimino tutti i dati dal transitorio per ripopolarlo con i dati aggiornati
        ZAP
        SET DELETED ON
        SET DELETED &i_olddel
        if used("RecDel")
          if reccount("RecDel") > 0
            SELECT (this.w_CTRSNAME)
            APPEND FROM DBF("RecDel")
            DELETE ALL
          endif
        endif
        if used ("Scaletta")
          if reccount("Scaletta")>0
            SELECT (this.w_CTRSNAME)
            APPEND FROM DBF("Scaletta")
          endif
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GSCI_MSP.InitRow()     
        this.GSCI_MSP.__dummy__.SetFocus()     
        this.w_MESS = "Preparazione della visualizzazione in corso..."
        ah_Msg(this.w_MESS)
        SELECT (this.w_CTRSNAME) 
 GO TOP
        i_r=recno() 
 i_del=set("DELETED") 
 set deleted off 
 go top
        do while deleted() and !eof()
          skip
        enddo
        this.GSCI_MSP.nFirstRow = recno()
        set deleted &i_del
        SELECT (this.w_CTRSNAME) 
 GO TOP
        go i_r
        this.GSCI_MSP.oPgFrm.Page1.oPag.oBody.nAbsRow = i_r
        this.GSCI_MSP.oPgFrm.Page1.oPag.oBody.nRelRow = 0
        SELECT (this.w_CTRSNAME)
        GO TOP
        * --- CVerco di riposizionarmi sulla riga che stavo modificando
        LOCATE FOR CPROWNUM = this.w_NRVARIATA
        if !FOUND()
          * --- se non ci riesco mi metto sul primo record
          SELECT (this.w_CTRSNAME)
          GO TOP
        endif
        * --- Aggiorno il dettaglio della Movimentaziione
        With this.GSCI_MSP 
 .WorkFromTrs() 
 .bupdated=.t. 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow = RECNO() 
 * .oPgFrm.Page1.oPag.oBody.nRelRow = 0 
 endwith
        this.GSCI_MSP.__dummy__.enabled=.f.
        this.GSCI_MSP.__dummy__.enabled = this.w_bLockScreen
        this.oBody.Refresh()     
        this.oBody.SetFocus()     
        if used("RecDel")
          use in RecDel 
        endif
        if used("Scaletta")
          use in Scaletta
        endif
      case this.pAzione=="Intestazione"
        * --- Assegna titolo a gestione
        do case
          case this.w_SCTIPSCA == "CLQ"
            this.w_LTIPGES = "CLQ"
            do case
              case this.oParentObject.w_TIPGES=="LQA"
                this.GSCI_MSP.cComment = AH_MsgFormat("Piano di produzione quantita / articolo")
              case this.oParentObject.w_TIPGES=="LQO"
                this.GSCI_MSP.cComment = AH_MsgFormat("Piano di produzione quantita / ODL")
            endcase
          case this.w_SCTIPSCA == "CLO"
            this.w_LTIPGES = "CLO"
            do case
              case this.oParentObject.w_TIPGES=="LOA"
                this.GSCI_MSP.cComment = AH_MsgFormat("Piano di produzione ore / articolo")
              case this.oParentObject.w_TIPGES=="LOO"
                this.GSCI_MSP.cComment = AH_MsgFormat("Piano di produzione ore / ODL")
            endcase
          case this.w_SCTIPSCA == "CSQ"
            this.w_LTIPGES = "CLQ"
            do case
              case this.oParentObject.w_TIPGES=="LQA"
                this.GSCI_MSP.cComment = AH_MsgFormat("Piano di produzione quantita / articolo")
              case this.oParentObject.w_TIPGES=="LQO"
                this.GSCI_MSP.cComment = AH_MsgFormat("Piano di produzione quantita / ODL")
            endcase
        endcase
        if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
          this.GSCI_MSP.cAutozoom = "GSCI"+this.w_LTIPGES+ "MSP"
        else
          this.GSCI_MSP.cAutozoom = "GSCO"+this.w_LTIPGES+ "MSP"
        endif
        this.GSCI_MSP.SetCaption()     
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPCOLSUM,PPCOLPIA,PPCOLLAN,PPCALSTA"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPCOLSUM,PPCOLPIA,PPCOLLAN,PPCALSTA;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PPCOLSUM = NVL(cp_ToDate(_read_.PPCOLSUM),cp_NullValue(_read_.PPCOLSUM))
          this.w_PPCOLPIA = NVL(cp_ToDate(_read_.PPCOLPIA),cp_NullValue(_read_.PPCOLPIA))
          this.w_PPCOLLAN = NVL(cp_ToDate(_read_.PPCOLLAN),cp_NullValue(_read_.PPCOLLAN))
          this.w_PPCALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_CALSTA = NVL(this.w_PPCALSTA, SPACE(5))
        this.oParentObject.w_BASSUG = mod(this.w_PPCOLSUM, (rgb(255,255,255)+1))
        this.oParentObject.w_BASCON = mod(this.w_PPCOLPIA, (rgb(255,255,255)+1))
        this.oParentObject.w_BASLAN = mod(this.w_PPCOLLAN, (rgb(255,255,255)+1))
        this.oParentObject.w_TESSUG = int(this.w_PPCOLSUM/(rgb(255,255,255)+1))
        this.oParentObject.w_TESCON = int(this.w_PPCOLPIA/(rgb(255,255,255)+1))
        this.oParentObject.w_TESLAN = int(this.w_PPCOLLAN/(rgb(255,255,255)+1))
        this.GSCI_MSP.mcalc(.t.)     
        this.GSCI_MSP.Refresh()     
      case this.pAzione=="CapPeriodo"
        do case
          case INLIST(this.w_SCTIPSCA , "CLO" , "CLQ", "CSQ")
            * --- Se la scaletta � settimanale deve iniziare sempre di luned�
            if this.w_SCTIPPIA = "S"
              this.w_DATINI = this.w_SCDATINI
              if dow(this.w_DATINI)<>2
                ah_ERRORMSG("La settimana deve iniziare sempre di luned�%0La procedura ricalcoler� in automatico il luned� pi� vicino", 48)
                this.w_DATINI = this.w_DATINI - (dow(this.w_DATINI)-2)
                if dow(this.w_DATINI)=2
                  this.w_SCDATINI = this.w_DATINI
                  this.GSCI_MSP.w_SCDATINI = this.w_SCDATINI
                endif
              endif
            endif
        endcase
        * --- Calcolo la disponibilit� della risorsa
        this.w_DRQTARIS = 0
        this.w_DRCOEEFF = 0
        this.w_DRPERRID = 0
        if this.w_SCTIPSCA = "CSQ"
          this.w_DRQTARIS = 1
          this.w_DRQTARIS = 100
        else
          * --- Select from ..\PRFA\EXE\QUERY\GSCICBSP
          do vq_exec with '..\PRFA\EXE\QUERY\GSCICBSP',this,'_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP','',.f.,.t.
          if used('_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP')
            select _Curs__d__d__PRFA_EXE_QUERY_GSCICBSP
            locate for 1=1
            do while not(eof())
            this.w_DRQTARIS = NVL(_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP.DRQTARIS, 0)
            this.w_DRCOEEFF = NVL(_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP.DRCOEEFF, 0)
            this.w_DRPERRID = NVL(_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP.DRPERRID, 0)
              select _Curs__d__d__PRFA_EXE_QUERY_GSCICBSP
              continue
            enddo
            use
          endif
          this.w_DRQTARIS = 1 * (this.w_DRQTARIS * this.w_DRCOEEFF * (100-this.w_DRPERRID)/100)
        endif
        this.w_CONTCAP = 1
        do while this.w_CONTCAP<=14
          icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
          this.GSCI_MSP.w_SCQTAC&icap = 0
          this.w_CONTCAP = this.w_CONTCAP+1
        enddo
        do case
          case this.w_SCTIPSCA == "CLO"
            do case
              case this.w_SCTIPPIA = "G"
                this.oParentObject.w_SCDATFIN = this.w_SCDATINI+13
                if not empty(this.w_SCDATINI) and not empty(this.oParentObject.w_SCCALRIS)
                  CREATE CURSOR RngDate (SCGIORNO D)
                  SELECT RngDate
                  FOR this.ngg = 0 TO 13 
 APPEND BLANK 
 REPLACE SCGIORNO WITH this.w_SCDATINI+this.ngg 
 ENDFOR
                  vq_exec("..\PRFA\EXE\QUERY\GSCIGBSP",this,"Calend")
                  select distinct Rngdate.SCGIORNO, Calend.CANUMORE from RngDate left join Calend; 
 on (Calend.CAGIORNO=RngDate.SCGIORNO) into cursor CapPer order by 1
                  SELECT CapPer
                  GO TOP
                  this.w_CONTCAP = 1
                  this.w_QTAPER = 0
                  do while not eof("CapPer") and this.w_CONTCAP<=14
                    icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
                    this.w_QTAPER = NVL(CapPer.CANUMORE, 0) * this.w_DRQTARIS
                    this.GSCI_MSP.w_SCQTAC&icap = this.w_QTAPER
                    this.w_CONTCAP = this.w_CONTCAP+1
                    SELECT CapPer
                    skip +1
                  enddo
                  use in RngDate
                  use in Calend
                  use in CapPer
                  this.w_CONTCAP = 1
                  this.GSCI_MSP.SaveDependsOn()     
                  this.GSCI_MSP.SetControlsValue()     
                  this.GSCI_MSP.bHeaderUpdated = .t.
                endif
              case this.w_SCTIPPIA = "S"
                * --- Recuper 14 periodi settimanali
                this.oParentObject.w_SCDATFIN = this.w_SCDATINI + (13*7)
                if not empty(this.w_SCDATINI) and not empty(this.oParentObject.w_SCCALRIS)
                  this.w_ANNO = YEAR(this.w_SCDATINI)
                  * --- Il formato WEEK(CTOD(sData), 2, 2) � standard ISO week 
                  this.w_WEEK = WEEK(this.w_SCDATINI, 2, 2)
                  * --- Faccio La WEEK passando i parametri che indicano che la settimana
                  *     � sempre di 7 giorni e inizia sempre di luned�
                  *     In questo modo riesco a sapere se l'anno ha 52 oppure 53 settimane
                  sData = "31-12-" + alltrim(str(this.w_ANNO))
                  * --- Trucco per prendere sempre l'ultima settimana dell'anno
                  sData = CTOD(sData) - (DOW(CTOD(sData))-1)
                  this.w_SETTTOT = WEEK(sData, 2, 2)
                  vq_exec("..\PRFA\EXE\QUERY\GSCISBSP",this,"CapPerS")
                  SELECT CACODCAL AS CACODCAL, MIN(SETTIMANA) AS DATAI, YEAR(MIN(SETTIMANA)) AS ANNO, WEEK(SETTIMANA,2,2) AS SETTIMANA, SUM(PERIODO) AS NUMEROORE ; 
 FROM CapPerS INTO CURSOR CapPerS GROUP BY 1,4 ORDER BY 1,2,3,4
                  if used("CapPerS")
                    =WRCURSOR("CapPerS") 
 INDEX ON SETTIMANA TAG SETTIMANA 
 SET ORDER TO SETTIMANA
                    sett=this.w_WEEK
                    this.w_PERIODI = 0
                    this.w_CONTCAP = 1
                    SELECT CapPerS
                    do while not eof ("CapPerS") and this.w_CONTCAP <=14
                      SELECT CapPerS
                      if this.w_WEEK + this.w_PERIODI > this.w_SETTTOT
                        this.w_WEEK = 0
                        this.w_PERIODI = 1
                      endif
                      this.w_FOUND = SEEK(this.w_WEEK + this.w_PERIODI)
                      icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
                      if this.w_FOUND
                        SELECT CapPerS
                        this.w_QTAPER = NVL(CapPerS.NumeroOre, 0) * this.w_DRQTARIS
                        this.GSCI_MSP.w_SCQTAC&icap = this.w_QTAPER
                      else
                        this.w_QTAPER = 0
                        this.GSCI_MSP.w_SCQTAC&icap = 0
                      endif
                      this.w_PERIODI = this.w_PERIODI + 1
                      this.w_CONTCAP = this.w_CONTCAP + 1
                    enddo
                  endif
                  use in CapPerS
                  this.w_CONTCAP = 1
                  this.GSCI_MSP.SaveDependsOn()     
                  this.GSCI_MSP.SetControlsValue()     
                  this.GSCI_MSP.bHeaderUpdated = .t.
                endif
              case this.w_SCTIPPIA = "M"
                this.oParentObject.w_SCDATFIN = GOMONTH(this.w_SCDATINI , 13)
                if not empty(this.w_SCDATINI) and not empty(this.oParentObject.w_SCCALRIS)
                  this.w_ANNO = YEAR(this.w_SCDATINI)
                  this.w_MESE = MONTH(this.w_SCDATINI)
                  vq_exec("..\PRFA\EXE\QUERY\GSCIMBSP",this,"CapPerM")
                  mese=this.w_MESE
                  this.SfieldCount = AFIELDS(ArrayFields, "CapPerM")
                  this.w_CONTCAP = 1
                  SELECT CapPerM
                  GO TOP
                  locate for Anno = this.w_ANNO
                  if found()
                    do while not eof ("CapPerM") and this.w_CONTCAP <=14
                      icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
                      if mese=13 and this.w_CONTCAP <=14
                        mese=1
                        this.w_ANNO = this.w_ANNO + 1
                        locate for Anno = this.w_ANNO
                        if found()
                          smese = alltrim(str(mese))
                          this.ffound=.f.
                          FOR this.fCount = 1 TO this.SfieldCount 
 this.fName=upper(alltrim(ArrayFields(this.fCount,1))) && recupero nome del campo 
 if this.fName==upper("Periodo_" + smese) 
 this.ffound=.t. 
 exit 
 endif 
 ENDFOR
                          if this.ffound
                            this.w_QTAPER = NVL(CapPerM.Periodo_&smese, 0) * this.w_DRQTARIS
                          else
                            this.w_QTAPER = 0
                          endif
                        endif
                      else
                        smese = alltrim(str(mese))
                        this.ffound=.f.
                        FOR this.fCount = 1 TO this.SfieldCount 
 this.fName=upper(alltrim(ArrayFields(this.fCount,1))) && recupero nome del campo 
 if this.fName==upper("Periodo_" + smese) 
 this.ffound=.t. 
 exit 
 endif 
 ENDFOR
                        if this.ffound
                          this.w_QTAPER = NVL(CapPerM.Periodo_&smese, 0) * this.w_DRQTARIS
                        else
                          this.w_QTAPER = 0
                        endif
                      endif
                      this.GSCI_MSP.w_SCQTAC&icap = this.w_QTAPER
                      this.w_CONTCAP = this.w_CONTCAP + 1
                      mese=mese+1
                      SELECT CapPerM
                    enddo
                  endif
                  use in CapPerM
                  this.w_CONTCAP = 1
                  this.GSCI_MSP.SaveDependsOn()     
                  this.GSCI_MSP.SetControlsValue()     
                  this.GSCI_MSP.bHeaderUpdated = .t.
                endif
              case this.w_SCTIPPIA = "A"
                this.oParentObject.w_SCDATFIN = cp_CharToDate("31-12-" + alltrim(str(YEAR(this.w_SCDATINI)+13)))
                if not empty(this.w_SCDATINI) and not empty(this.oParentObject.w_SCCALRIS)
                  this.w_ANNO = YEAR(this.w_SCDATINI)
                  vq_exec("..\PRFA\EXE\QUERY\GSCIABSP",this,"CapPerA")
                  if used("CapPerA")
                    yanno=this.w_ANNO
                    this.w_CONTCAP = 1
                    SELECT CapPerA
                    GO TOP
                    do while not eof ("CapPerA") and this.w_CONTCAP <=14
                      icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
                      locate for Anno = yanno
                      if found()
                        sanno = alltrim(str(yanno))
                        this.w_QTAPER = NVL(CapPerA.Periodo_&sanno, 0) * this.w_DRQTARIS
                      else
                        this.w_QTAPER = 0
                      endif
                      this.GSCI_MSP.w_SCQTAC&icap = this.w_QTAPER
                      yanno = yanno + 1
                      this.w_CONTCAP = this.w_CONTCAP + 1
                      SELECT CapPerA
                    enddo
                  endif
                  use in CapPerA
                  this.w_CONTCAP = 1
                  yanno = 1
                  this.GSCI_MSP.SaveDependsOn()     
                  this.GSCI_MSP.SetControlsValue()     
                  this.GSCI_MSP.bHeaderUpdated = .t.
                endif
            endcase
          case this.w_SCTIPSCA $ "CLQ-CSQ"
            this.w_CONTCAP = 1
            this.w_INCDAT = 0
            this.w_CONTCAP = 1
            do case
              case this.w_SCTIPPIA = "G"
                this.oParentObject.w_SCDATFIN = this.w_SCDATINI+13
              case this.w_SCTIPPIA = "S"
                this.oParentObject.w_SCDATFIN = this.w_SCDATINI + (13*7)
              case this.w_SCTIPPIA = "M"
                this.oParentObject.w_SCDATFIN = GOMONTH(this.w_SCDATINI , 13)
              case this.w_SCTIPPIA = "A"
                this.oParentObject.w_SCDATFIN = cp_CharToDate("31-12-" + alltrim(str(YEAR(this.w_SCDATINI)+13)))
            endcase
            if this.w_SCTIPPIA ="G"
              do while this.w_CONTCAP<=14
                icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
                this.w_QTAPER = iif(DOW(this.w_SCDATINI+ this.w_INCDAT)>=2 AND DOW(this.w_SCDATINI+ this.w_INCDAT)<=6 ,this.oParentObject.w_SCQTCPDF,0)
                this.GSCI_MSP.w_SCQTAC&icap = this.w_QTAPER
                this.w_CONTCAP = this.w_CONTCAP+1
                this.w_INCDAT = this.w_INCDAT + 1
              enddo
            else
              do while this.w_CONTCAP<=14
                icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
                this.w_QTAPER = this.oParentObject.w_SCQTCPDF
                this.GSCI_MSP.w_SCQTAC&icap = this.w_QTAPER
                this.w_CONTCAP = this.w_CONTCAP+1
              enddo
            endif
        endcase
        this.GSCI_MSP.SaveDependsOn()     
        this.GSCI_MSP.SetControlsValue()     
        this.GSCI_MSP.bHeaderUpdated = .t.
      case this.pAzione=="TempiLav"
        do case
          case this.w_SCTIPSCA == "CLO"
            this.w_CODODL = this.GSCI_MSP.w_SCCODODL
            this.w_ROWNUM = this.GSCI_MSP.w_SCROWNUM
            this.w_CODRIS = alltrim(this.w_SCCODRIS)
            * --- Read from ODL_RISO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_RISO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2],.t.,this.ODL_RISO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RLTPSLAU"+;
                " from "+i_cTable+" ODL_RISO where ";
                    +"RLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                    +" and RLROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and RLCODICE = "+cp_ToStrODBC(this.w_CODRIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RLTPSLAU;
                from (i_cTable) where;
                    RLCODODL = this.w_CODODL;
                    and RLROWNUM = this.w_ROWNUM;
                    and RLCODICE = this.w_CODRIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SCLAVSEC = NVL(cp_ToDate(_read_.RLTPSLAU),cp_NullValue(_read_.RLTPSLAU))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_SCQTAODL = NVL(this.GSCI_MSP.w_SCQTAODL, 0)
            this.w_SCQTARIC = cp_ROUND(IIF((this.w_SCQTAODL * this.w_SCLAVSEC)>0, (this.w_SCQTAODL * this.w_SCLAVSEC) / 3600, 0), 5)
            this.GSCI_MSP.w_SCLAVSEC = this.w_SCLAVSEC
            this.GSCI_MSP.w_SCQTARIC = this.w_SCQTARIC
            this.GSCI_MSP.TrsFromWork()     
          case INLIST(this.w_SCTIPSCA , "CLQ", "CSQ")
            this.w_SCQTAODL = NVL(this.GSCI_MSP.w_SCQTAODL, 0)
            this.GSCI_MSP.w_SCQTARIC = this.w_SCQTAODL
        endcase
      case this.pAzione == "ZINPUTODL" or this.pAzione == "ZOOMODL"
        this.w_SCROWNUM = 0
        this.w_CLROWORD = 0
        this.w_CLDESFAS = " "
        this.w_CLBCFUM1 = " "
        this.w_RLTPSLAU = 0
        this.w_RLTPSLAV = 0
        this.w_SCCODICE = " "
        this.w_SCCODODL = " "
        this.w_OLCODODL = " "
        do case
          case INLIST(this.w_SCTIPSCA , "CLQ", "CLO")
            if this.pAzione == "ZOOMODL"
              this.w_OLCODODL = this.GSCI_MSP.w_SCCODODL
            endif
            vx_exec("..\PRFA\EXE\QUERY\GSCI_ZFO.VZM",this)
          case this.w_SCTIPSCA = "CSQ"
            vx_exec("..\COLA\EXE\QUERY\GSCO_ZFO.VZM",this)
        endcase
        if !empty(this.w_OLCODODL)
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV,OLTSEODL,OLTFAODL"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV,OLTSEODL,OLTFAODL;
              from (i_cTable) where;
                  OLCODODL = this.w_OLCODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SCCODODL = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
            this.w_SCCODICE = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
            this.w_SCDINRIC = NVL(cp_ToDate(_read_.OLTDINRIC),cp_NullValue(_read_.OLTDINRIC))
            this.w_OLTUNMIS = NVL(cp_ToDate(_read_.OLTUNMIS),cp_NullValue(_read_.OLTUNMIS))
            this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
            this.w_SCCODCIC = NVL(cp_ToDate(_read_.OLTCICLO),cp_NullValue(_read_.OLTCICLO))
            this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
            this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
            this.w_OLTQTODL = NVL(cp_ToDate(_read_.OLTQTODL),cp_NullValue(_read_.OLTQTODL))
            this.w_OLTDTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
            this.w_OLTQTOEV = NVL(cp_ToDate(_read_.OLTQTOEV),cp_NullValue(_read_.OLTQTOEV))
            this.w_OLTSEODL = NVL(cp_ToDate(_read_.OLTSEODL),cp_NullValue(_read_.OLTSEODL))
            this.w_OLTFAODL = NVL(cp_ToDate(_read_.OLTFAODL),cp_NullValue(_read_.OLTFAODL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.GSCI_MSP.w_SCCODODL = this.w_OLCODODL
          if INLIST(this.w_SCTIPSCA , "CLQ", "CLO")
            * --- Leggo i dati dall'ardine di riferimento del ciclo di lavorazione
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTSECIC"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTSECIC;
                from (i_cTable) where;
                    OLCODODL = this.w_OLTSEODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SCCODCIC = NVL(cp_ToDate(_read_.OLTSECIC),cp_NullValue(_read_.OLTSECIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.GSCI_MSP.w_SCDINRIC = this.w_SCDINRIC
          this.GSCI_MSP.w_OLTUNMIS = this.w_OLTUNMIS
          this.GSCI_MSP.w_SCCODICE = this.w_SCCODICE
          this.GSCI_MSP.w_OLTCOART = this.w_OLTCOART
          this.GSCI_MSP.w_SCCODCIC = this.w_SCCODCIC
          this.GSCI_MSP.w_OLTSTATO = this.w_OLTSTATO
          this.GSCI_MSP.w_OLTQTOD1 = this.w_OLTQTOD1
          this.GSCI_MSP.w_OLTQTODL = this.w_OLTQTODL
          this.GSCI_MSP.w_OLTDTINI = this.w_OLTDTINI
          this.GSCI_MSP.w_OLTQTOEV = this.w_OLTQTOEV
          this.w_SCQTAODL = NVL(this.w_OLTQTODL, 0) - NVL(this.w_OLTQTOEV, 0)
          this.w_OLTRESID = NVL(this.w_OLTQTODL, 0) - NVL(this.w_OLTQTOEV, 0)
          this.GSCI_MSP.w_SCQTAODL = NVL(this.w_OLTQTODL, 0) - NVL(this.w_OLTQTOEV, 0)
          this.GSCI_MSP.w_SCQTARIC = this.w_SCQTAODL
          do case
            case INLIST(this.w_SCTIPSCA , "CLQ", "CLO")
              * --- Read from CIC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CIC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CLCODCIC,CLDESCIC"+;
                  " from "+i_cTable+" CIC_MAST where ";
                      +"CLSERIAL = "+cp_ToStrODBC(this.w_SCCODCIC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CLCODCIC,CLDESCIC;
                  from (i_cTable) where;
                      CLSERIAL = this.w_SCCODCIC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_OLTCILAV = NVL(cp_ToDate(_read_.CLCODCIC),cp_NullValue(_read_.CLCODCIC))
                this.w_DESCIC = NVL(cp_ToDate(_read_.CLDESCIC),cp_NullValue(_read_.CLDESCIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.GSCI_MSP.w_OLTCILAV = this.w_OLTCILAV
            case this.w_SCTIPSCA = "CSQ"
              * --- Read from TABMCICL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TABMCICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2],.t.,this.TABMCICL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CSDESCRI"+;
                  " from "+i_cTable+" TABMCICL where ";
                      +"CSCODICE = "+cp_ToStrODBC(this.w_SCCODCIC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CSDESCRI;
                  from (i_cTable) where;
                      CSCODICE = this.w_SCCODCIC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DESCIC = NVL(cp_ToDate(_read_.CSDESCRI),cp_NullValue(_read_.CSDESCRI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
          endcase
          this.GSCI_MSP.w_SCCODCIC = this.w_SCCODCIC
          this.GSCI_MSP.w_DESCIC = this.w_DESCIC
          this.GSCI_MSP.w_INPUTODL = .f.
          if INLIST(this.w_SCTIPSCA , "CLQ", "CLO")
            * --- Calcolo la produzione oraria
            this.w_SCLAVSEC = NVL(this.w_RLTPSLAU, 0)
            this.w_SCTPSLAV = NVL(this.w_RLTPSLAV, 0)
            this.w_SCPROORA = iif(this.w_SCLAVSEC=0, 0, 3600/this.w_SCLAVSEC)
            this.w_SCQTAODL = NVL(this.w_CLQTAPRE, 0) - NVL(this.w_CLQTAAVA, 0) - NVL(this.w_CLQTASCA, 0) - NVL(this.w_CLQTABCF, 0)
            this.w_OLTRESID = NVL(this.w_OLTQTODL, 0) - NVL(this.w_OLTQTOEV, 0) - NVL(this.w_OLTQTOSC, 0) - NVL(this.w_OLTQTOBF, 0)
            if this.w_OLTSTATO="M"
              this.w_RLTPSLAV = NVL(this.w_RLTPSLAU, 0) * NVL(this.w_OLTQTOD1, 0)
            endif
            this.w_SCQTAORA = cp_ROUND(IIF(NVL(this.w_RLTPSLAV, 0) > 0, this.w_RLTPSLAV / 3600, 0), 5)
            do case
              case this.w_SCTIPSCA == "CLO"
                this.w_SCQTARIC = this.w_SCQTAORA
              case this.w_SCTIPSCA == "CLQ"
                this.w_SCQTARIC = this.w_SCQTAODL
            endcase
            this.GSCI_MSP.w_SCTSEODL = this.w_OLTSEODL
            this.GSCI_MSP.w_SCROWNUM = this.w_SCROWNUM
            this.GSCI_MSP.w_SCFASODL = this.w_CLROWORD
            this.GSCI_MSP.w_CLROWORD = this.w_CLROWORD
            this.GSCI_MSP.w_SCDESFAS = this.w_CLDESFAS
            this.GSCI_MSP.w_SCRISODL = this.w_RLCODICE
            this.GSCI_MSP.w_SCQTAODL = this.w_SCQTAODL
            this.GSCI_MSP.w_SCQTMODL = this.w_SCQTAODL
            this.GSCI_MSP.w_SCQTAORA = this.w_SCQTAORA
            this.GSCI_MSP.w_SCQTMORA = this.w_SCQTAORA
            this.GSCI_MSP.w_SCDESFAS = this.w_CLDESFAS
            this.GSCI_MSP.w_SCLAVSEC = NVL(this.w_RLTPSLAU, 0)
            this.GSCI_MSP.w_SCPROORA = this.w_SCPROORA
            this.GSCI_MSP.w_SCQTARIC = IIF(this.oParentObject.w_SCTPQTOR="Q", this.w_SCQTAODL, this.w_SCQTAORA)
            this.GSCI_MSP.w_INPUTODL = .f.
            this.GSCI_MSP.w_INPUTODL = False
          endif
          this.GSCI_MSP.SaveDependsOn()     
          this.GSCI_MSP.SetControlsValue()     
          this.GSCI_MSP.TrsFromWork()     
        endif
      case this.pAzione == "CINPUTODL"
        this.w_OLCODODL = this.GSCI_MSP.w_SCCODODL
        if !empty(this.w_OLCODODL)
          do case
            case INLIST(this.w_SCTIPSCA , "CLQ", "CLO")
              vq_exec("..\PRFA\EXE\QUERY\GSCIOMSP", this, "_Fasi_")
              if used("_Fasi_")
                this.w_NREC = RECCOUNT("_Fasi_")
                do case
                  case this.w_NREC < 1
                    this.GSCI_MSP.w_SCROWNUM = 0
                    this.GSCI_MSP.w_SCFASODL = 0
                    this.GSCI_MSP.w_CLROWORD = 0
                    this.GSCI_MSP.w_SCRISODL = SPACE(20)
                    this.GSCI_MSP.w_SCLAVSEC = 0
                    this.GSCI_MSP.w_SCQTARIC = 0
                    this.GSCI_MSP.w_SCQTARIC = 0
                    ah_ERRORMSG("Valore non ammesso", 48)
                  case this.w_NREC > 1
                    * --- Se la query restituisce pi� di un record allora eseguo lo zoom 
                    *     direttamente dalla maschera come se facessi F9
                    use in select("_Fasi_")
                    this.GSCI_MSP.NotifyEvent("ZoomODL")     
                  otherwise
                    * --- In questo caso ho solo un record e quindi leggo i valori 
                    *     dal cursore e li assegno alla maschera
                    this.w_SCROWNUM = NVL(SCROWNUM, 0)
                    this.w_CLROWORD = NVL(CLROWORD, 0)
                    this.w_RLCODICE = NVL(RLCODICE, SPACE(20))
                    this.w_RLTPSLAU = NVL(RLTPSLAU,0)
                    this.w_RLTPSLAV = NVL(RLTPSLAV,0)
                    this.w_CLDESFAS = NVL(CLDESFAS, SPACE(40))
                    this.w_SCQTAODL = NVL(CLQTAPRE, 0) - NVL(CLQTAAVA, 0) - NVL(CLQTASCA, 0) - NVL(CLQTABCF, 0)
                    * --- Assegno i valori alla gestione
                    this.GSCI_MSP.w_SCROWNUM = this.w_SCROWNUM
                    this.GSCI_MSP.w_SCFASODL = this.w_CLROWORD
                    this.GSCI_MSP.w_SCDESFAS = this.w_SCDESFAS
                    this.GSCI_MSP.w_CLROWORD = this.w_CLROWORD
                    this.GSCI_MSP.w_SCRISODL = this.w_RLCODICE
                    this.GSCI_MSP.w_SCLAVSEC = this.w_SCLAVSEC
                    this.GSCI_MSP.w_SCPROORA = this.w_SCPROORA
                    * --- Tempo di Lavorazione Oraria
                    if this.w_OLTSTATO="M"
                      this.w_RLTPSLAV = NVL(this.w_RLTPSLAU, 0) * NVL(this.w_OLTQTOD1, 0)
                    endif
                    this.w_SCQTAORA = cp_ROUND(IIF(NVL(this.w_RLTPSLAV, 0) > 0, this.w_RLTPSLAV / 3600, 0), 5)
                    do case
                      case this.w_SCTIPSCA == "CLO"
                        this.w_SCQTARIC = this.w_SCQTAORA
                      case this.w_SCTIPSCA == "CLQ"
                        this.w_SCQTARIC = this.w_SCQTAODL
                    endcase
                    this.GSCI_MSP.w_SCQTAODL = this.w_SCQTAODL
                    this.GSCI_MSP.w_SCQTMODL = this.w_SCQTAODL
                    this.GSCI_MSP.w_FQTARES = this.w_SCQTAODL
                    this.GSCI_MSP.w_FQTAEVA = NVL(this.w_CLQTAAVA, 0) + NVL(this.w_CLQTABCF, 0)
                    this.GSCI_MSP.w_SCQTAORA = this.w_SCQTAORA
                    this.GSCI_MSP.w_SCQTMORA = this.w_SCQTAORA
                    this.GSCI_MSP.w_SCDESFAS = this.w_CLDESFAS
                    this.GSCI_MSP.w_SCLAVSEC = NVL(this.w_RLTPSLAU, 0)
                    this.GSCI_MSP.w_SCQTARIC = IIF(this.oParentObject.w_SCTPQTOR="Q", this.w_SCQTAODL, this.w_SCQTAORA)
                endcase
              endif
            case this.w_SCTIPSCA = "CSQ"
              this.w_SCQTAODL = NVL(this.GSCI_MSP.w_OLTQTODL, 0) - NVL(this.GSCI_MSP.w_OLTQTOEV, 0)
              this.w_OLTRESID = NVL(this.GSCI_MSP.w_OLTQTODL, 0) - NVL(this.GSCI_MSP.w_OLTQTOEV, 0)
              this.GSCI_MSP.w_SCQTAODL = NVL(this.w_OLTQTODL, 0) - NVL(this.w_OLTQTOEV, 0)
              this.GSCI_MSP.w_SCQTARIC = this.w_SCQTAODL
          endcase
          this.GSCI_MSP.w_INPUTODL = .f.
          this.GSCI_MSP.SaveDependsOn()     
          this.GSCI_MSP.SetControlsValue()     
          this.GSCI_MSP.TrsFromWork()     
        endif
    endcase
    do case
      case this.pAzione="Elabora"
        this.GSCI_MSP.LockScreen = .t.
        SELECT (this.w_CTRSNAME)
        this.w_ORECO = RECNO()
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT (this.w_CTRSNAME)
        GOTO this.w_ORECO
         With this.GSCI_MSP 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_ORECO 
 EndWith
        SELECT (this.w_CTRSNAME)
        this.GSCI_MSP.LockScreen = .f.
      case this.pAzione="Dettaglio"
        this.w_bLockScreen = this.GSCI_MSP.LockScreen
        this.GSCI_MSP.LockScreen = .t.
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT (this.w_CTRSNAME)
        this.oBody.SetFocus()     
        this.GSCI_MSP.LockScreen = .f.
        this.GSCI_MSP.LockScreen = this.w_bLockScreen
      case this.pAzione="Stampa"
        this.w_DATINI = this.w_SCDATINI
        this.w_DATIN1 = this.w_SCDATINI
        this.w_SERIAL = this.oParentObject.w_SCSERIAL
        this.w_SERIAL1 = this.oParentObject.w_SCSERIAL
        if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
          do GSCI_SSC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_CODRIS = this.w_SCCODRIS
          this.w_CODRIS1 = this.w_SCCODRIS
          do GSCO_SSC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAzione="ControlloDati"
        this.w_bLockScreen = this.GSCI_MSP.LockScreen
        this.GSCI_MSP.LockScreen = .t.
        SELECT (this.w_CTRSNAME)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oBody.SetFocus()     
        this.w_LCLCODODL = this.GSCI_MSP.w_SCCODODL
        this.GSCI_MSP.SaveDependsOn()     
        this.GSCI_MSP.SetControlsValue()     
        this.GSCI_MSP.TrsFromWork()     
        this.GSCI_MSP.LockScreen = .f.
        this.GSCI_MSP.LockScreen = this.w_bLockScreen
      case this.pAzione="Calendario"
        do case
          case INLIST(this.w_SCTIPSCA , "CSO" , "CSQ")
            * --- Assegna il calendario della risorsa
            * --- Read from PAR_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PPCALSTA"+;
                " from "+i_cTable+" PAR_PROD where ";
                    +"PPCODICE = "+cp_ToStrODBC("PP");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PPCALSTA;
                from (i_cTable) where;
                    PPCODICE = "PP";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_CALRIS = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          case INLIST(this.w_SCTIPSCA , "CLO" , "CLQ")
            * --- Assegna il calendario della risorsa
            * --- Select from ..\PRFA\EXE\QUERY\GSCICBSP
            do vq_exec with '..\PRFA\EXE\QUERY\GSCICBSP',this,'_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP','',.f.,.t.
            if used('_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP')
              select _Curs__d__d__PRFA_EXE_QUERY_GSCICBSP
              locate for 1=1
              do while not(eof())
              this.oParentObject.w_CALRIS = NVL(_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP.DRCODCAL, SPACE(5))
              if !Empty(this.oParentObject.w_CALRIS)
                exit
              endif
                select _Curs__d__d__PRFA_EXE_QUERY_GSCICBSP
                continue
              enddo
              use
            endif
        endcase
        if !Empty(this.oParentObject.w_CALRIS)
          this.oParentObject.w_SCCALRIS = this.oParentObject.w_CALRIS
          this.GSCI_MSP.mCalc(.t.)     
        endif
      case this.pAzione="DatiPeriodo"
        Local L_NameHdrColumn, N
        this.w_CONTCAP = 1
        this.w_CONTCAPT = 0
        do while this.w_CONTCAP<=14
          this.w_DATINI = this.w_SCDATINI
          this.w_CONTCAPT = this.w_CONTCAP - 1
          this.w_ForeColor = 0
          do case
            case this.w_SCTIPPIA = "G"
              this.w_DATINI = this.w_DATINI + this.w_CONTCAPT
            case this.w_SCTIPPIA = "S"
              this.w_DATINI = this.w_DATINI + (this.w_CONTCAPT * 7)
            case this.w_SCTIPPIA = "M"
              this.w_DATINI = GOMONTH(this.w_DATINI , this.w_CONTCAPT)
            case this.w_SCTIPPIA = "A"
              this.w_DATINI = cp_CharToDate("31-12-" + alltrim(str(YEAR(this.w_DATINI)+this.w_CONTCAPT)))
          endcase
          icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
          N = alltrim(str(this.w_CONTCAP + 3))
          L_NameHdrColumn = "hdr_" + this.GSCI_MSP.w_oHeaderDetail.Field&N
          this.objColumn = this.GSCI_MSP.w_oHeaderDetail.&L_NameHdrColumn
          this.w_HdrColumn = this.objColumn.Header1
          if Empty(this.w_DATINI) or !UPPER(this.w_SCTIPPIA) $ "A-G-M-S"
            this.GSCI_MSP.w_SCHPER&icap = ""
          else
            do case
              case this.w_SCTIPPIA = "G"
                this.GSCI_MSP.w_SCHPER&icap = left(cdow(this.w_DATINI),1) + " " + left(dtoc(this.w_DATINI),5)
                this.w_ForeColor = IIF(left(cdow(this.w_DATINI),1) $ "S-D", RGB(255,0,0), RGB(0,0,0))
              case this.w_SCTIPPIA = "S"
                this.GSCI_MSP.w_SCHPER&icap = "W" + padl(alltrim(str(WEEK(this.w_DATINI,2,2))),2,"0") + "-" + CalcYear(this.w_DATINI)
              case this.w_SCTIPPIA = "M"
                this.GSCI_MSP.w_SCHPER&icap = "M" + padl(alltrim(str(MONTH(this.w_DATINI))),2,"0") + "-" + alltrim(str(YEAR(this.w_DATINI)))
              case this.w_SCTIPPIA = "A"
                this.GSCI_MSP.w_SCHPER&icap = "Y " + alltrim(str(YEAR(this.w_DATINI)))
            endcase
          endif
          this.w_HdrColumn.Caption = this.GSCI_MSP.w_SCHPER&icap
          this.w_HdrColumn.ForeColor = this.w_ForeColor
          this.objColumn.GetTxtSize()     
          this.w_CONTCAP = this.w_CONTCAP+1
        enddo
        * --- Eseguo la calculate su geader per assegnare caption dell'header
        this.GSCI_MSP.w_oHeaderDetail.Calculate(.T.)     
    endcase
    WAIT CLEAR
    this.bupdateparentobject=.F.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.MyChekRow = .t.
    do case
      case (empty(this.w_SCCODODL) or not(not empty(nvl(this.w_SCCODCIC," ")))) and (not(Empty(this.w_SCCODODL)) and not(Empty(this.w_SCFASODL)))
        this.MyChekRow = .f.
      case not(PRDCHKFA(this.w_SCCODODL,this.w_SCFASODL, "GSRA_ADO", this.oparentobject.w_SCCOUPOI)) and (not(Empty(this.w_SCCODODL)) and not(Empty(this.w_SCFASODL)))
        this.MyChekRow = .f.
    endcase
    RETURN (this.MyChekRow)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia le capacit� in un array
    this.w_MESS = "Ricalcolo quantit� del periodo in corso..."
    ah_Msg (this.w_MESS)
    Declare Capac(14)
    SELECT (this.w_CTRSNAME)
    GO TOP
    SCAN
    this.w_CONT = 1
    do while this.w_CONT<=14
      icap = padl(alltrim(str(this.w_CONT)), 2, "0")
      SELECT (this.w_CTRSNAME)
      if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
        replace t_SCQTAP&icap with 0, t_SCQTAH&icap with 0
      else
        replace t_SCQTAP&icap with 0
      endif
      this.GSCI_MSP.w_TOQTAC&icap = 0
      this.GSCI_MSP.w_QTARES = 0
      this.w_CONT = this.w_CONT+1
    enddo
    ENDSCAN
    this.w_CONTCAP = 1
    do while this.w_CONTCAP<=14
      icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
      Capac(this.w_CONTCAP) = this.GSCI_MSP.w_SCQTAC&icap
      this.w_CONTCAP = this.w_CONTCAP+1
      this.GSCI_MSP.w_TOQTAC&icap = 0
    enddo
    this.w_CONTCAP = 1
    if !used("Scaletta")
      * --- Query su Transitorio
      Local l_cWhere, l_cOrderBy 
 Store "" to l_cWhere, l_cOrderBy
      l_cWhere = "not(Empty(t_SCCODODL))"
      if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
        l_cWhere = l_cWhere + " and not(Empty(t_SCFASODL))"
      endif
      l_cOrderBy = "t_CPROWORD"
      this.GSCI_MSP.Exec_Select("Scaletta", "*", l_cWhere, l_cOrderby)     
    endif
    =wrcursor("Scaletta")
    UPDATE SCALETTA SET I_SRV="U" WHERE I_SRV<>"A" AND !DELETED() 
 SELECT SCALETTA 
 GO TOP
    this.w_CONT = 1
    do while this.w_CONT<=14
      icap = padl(alltrim(str(this.w_CONT)), 2, "0")
      if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
        replace t_SCQTAP&icap with 0, t_SCQTAH&icap with 0
      else
        replace t_SCQTAP&icap with 0
      endif
      this.w_CONT = this.w_CONT+1
    enddo
    do while not eof("Scaletta") and this.w_CONTCAP<=14
      this.w_SCQTAODL = NVL(Scaletta.t_SCQTARIC, 0)
      this.w_QTAODL = NVL(Scaletta.t_SCQTARIC, 0)
      if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
        this.w_SCQTAORA = IIF(NVL(Scaletta.t_SCQTAORA, 0)=0, 1, NVL(Scaletta.t_SCQTAORA, 0))
        this.w_SCPROORA = NVL(Scaletta.t_SCPROORA, 0)
        if this.oParentObject.w_SCTPQTOR="Q"
          this.w_QTAODL = this.w_SCQTAODL / this.w_SCPROORA
        endif
      endif
      do while this.w_CONTCAP<=14 and this.w_QTAODL>Capac(this.w_CONTCAP)
        * --- Spalma l'ODL su pi� giorni, consuma tutta la capacit� del periodo
        icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
        if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
          if this.w_SCTIPSCA="CLO" and this.oParentObject.w_SCTPQTOR="Q"
            if Capac(this.w_CONTCAP) >= this.w_QTAODL
              this.w_QTAORAODL = this.w_SCQTAODL
              Replace t_SCQTAH&icap with Capac(this.w_CONTCAP)
              Replace t_SCQTAP&icap with this.w_QTAORAODL
            else
              this.w_QTAORAODL = Capac(this.w_CONTCAP) * this.w_SCPROORA
              Replace t_SCQTAH&icap with Capac(this.w_CONTCAP)
              Replace t_SCQTAP&icap with this.w_QTAORAODL
            endif
          else
            this.w_QTAORAODL = Capac(this.w_CONTCAP) / this.w_SCPROORA
            Replace t_SCQTAH&icap with this.w_QTAORAODL
            Replace t_SCQTAP&icap with Capac(this.w_CONTCAP)
          endif
        else
          Replace t_SCQTAP&icap with Capac(this.w_CONTCAP)
        endif
        this.GSCI_MSP.w_TOQTAC&icap = this.GSCI_MSP.w_TOQTAC&icap + Capac(this.w_CONTCAP)
        this.w_QTAODL = this.w_QTAODL - Capac(this.w_CONTCAP)
        this.w_SCQTAODL = this.w_SCQTAODL - this.w_QTAORAODL
        Capac(this.w_CONTCAP) = 0
        Select Scaletta
        REPLACE t_QTARES WITH this.w_QTAODL
        this.w_CONTCAP = this.w_CONTCAP+1
      enddo
      if (this.w_QTAODL>=0 and this.w_CONTCAP<=14 and Capac(this.w_CONTCAP)>0) or (this.w_QTAODL>=0 and this.w_CONTCAP<=14 and Capac(this.w_CONTCAP)=0)
        * --- Consuma la capacit� del periodo
        icap = padl(alltrim(str(this.w_CONTCAP)), 2, "0")
        this.w_QTAORAODL = this.w_SCQTAODL
        do case
          case this.w_SCTIPSCA="CSQ"
            Replace t_SCQTAP&icap with this.w_QTAODL
          case this.w_SCTIPSCA="CLQ"
            this.w_QTAORAODL = this.w_SCQTAODL
            this.w_QTAORAODL = this.w_QTAODL / this.w_SCPROORA
            Replace t_SCQTAH&icap with this.w_QTAORAODL
            Replace t_SCQTAP&icap with this.w_QTAODL
          case this.w_SCTIPSCA="CLO"
            if this.oParentObject.w_SCTPQTOR="Q"
              this.w_QTAORAODL = this.w_SCQTAODL
              Replace t_SCQTAH&icap with this.w_QTAODL
              Replace t_SCQTAP&icap with this.w_QTAORAODL
            else
              this.w_QTAORAODL = this.w_SCQTAODL
              this.w_QTAORAODL = this.w_QTAODL * this.w_SCPROORA
              Replace t_SCQTAH&icap with this.w_QTAORAODL
              Replace t_SCQTAP&icap with this.w_QTAODL
            endif
        endcase
        this.w_SCQTAODL = this.w_SCQTAODL - this.w_QTAORAODL
        this.GSCI_MSP.w_TOQTAC&icap = this.GSCI_MSP.w_TOQTAC&icap + this.w_QTAODL
        Capac(this.w_CONTCAP) = Capac(this.w_CONTCAP) - this.w_QTAODL
        with this.GSCI_MSP
        .w_QTARES = .w_SCQTARIC-(nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0)+nvl(.w_SCQTAP&icap,0))
        endwith
        this.w_QTAODL = 0
        REPLACE t_QTARES WITH this.w_QTAODL
        Scatter memvar 
 Select (this.w_CTRSNAME) 
 locate for cprownum = m.cprownum 
 gather memvar
        Select Scaletta
        if not eof("Scaletta")
          skip
          this.w_CONT = 1
          do while this.w_CONT<=14
            icap = padl(alltrim(str(this.w_CONT)), 2, "0")
            if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
              replace t_SCQTAP&icap with 0, t_SCQTAH&icap with 0
            else
              replace t_SCQTAP&icap with 0
            endif
            this.w_CONT = this.w_CONT+1
          enddo
        endif
      endif
      if this.w_CONTCAP<=14 and Capac(this.w_CONTCAP)=0
        * --- Periodo consumato completamente
        Scatter memvar 
 Select (this.w_CTRSNAME)
        if m.cprownum>0
          locate for cprownum=m.cprownum
          if found()
            gather memvar
          endif
        endif
        Select Scaletta
        this.w_CONTCAP = this.w_CONTCAP+1
      endif
    enddo
    do while not eof("Scaletta") and this.w_CONTCAP=15
      this.w_QTAODL = 0
      Select Scaletta
      this.w_CONT = 1
      this.w_APPO = NVL(t_SCQTARIC, 0)
      do while this.w_CONT<=14
        icap = padl(alltrim(str(this.w_CONT)), 2, "0")
        Select Scaletta
        this.w_APPO = this.w_APPO - nvl(t_SCQTAP&icap,0)
        this.w_CONT = this.w_CONT + 1
      enddo
      replace t_QTARES with this.w_APPO
      Select Scaletta
      Scatter memvar 
 Select (this.w_CTRSNAME) 
 locate for cprownum = m.cprownum 
 gather memvar
      Select Scaletta
      if not eof("Scaletta")
        skip
      endif
    enddo
    if used("Scaletta")
      use in Scaletta
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_SERIAL = this.oParentObject.w_SCSERIAL
    * --- Creo Cursore di Appoggio Per stampa Log Errori
    if used("_MsgErr_")
      use in _MsgErr_
    endif
    Create Cursor _MsgErr_ (LESERIAL C(10), LEROWNUM N(5), LEDATOPE D, LEORAOPE C(8), LECODICE C(41), LEERRORE C(80), LEMESSAG M) 
    private l_Titolo,l_DettTec, l_Rec
    l_DettTec="S"
    ah_Msg("Controllo dati scaletta di produzione in corso...")
    vq_exec("..\COLA\EXE\QUERY\GSCOLBSP",this,"CursLogErr")
    select CursLogErr
    go top
    l_Rec = reccount()
    if l_Rec > 0
      * --- Try
      local bErr_04BB3B50
      bErr_04BB3B50=bTrsErr
      this.Try_04BB3B50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Impossibile aggiornare scaletta di prdouzione",16)
      endif
      bTrsErr=bTrsErr or bErr_04BB3B50
      * --- End
    endif
    if used("_MsgErr_")
      use in _MsgErr_
    endif
    if used("CursLogErr")
      use in CursLogErr
    endif
    if used("__tmp__")
      use in __tmp__
    endif
  endproc
  proc Try_04BB3B50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Determina progressivo log
    this.w_LSerial = space(10)
    this.w_LSerial = cp_GetProg("PRD_ERRO","PRERR",this.w_LSerial,i_CODAZI)
    this.w_LEERR = " "
    this.w_LMess = " "
    * --- Scrive LOG
    do while not eof("CursLogErr")
      this.w_LNumErr = 0
      this.w_LNumErr = this.w_LNumErr + 1
      this.w_LOggErr = alltrim(CursLogErr.scserial)
      this.w_LRowNum = CursLogErr.cprownum
      this.w_LErrore = ah_msgformat("Sequenza %1 riferimento ODL %2 non pi� valida", alltrim(str(CursLogErr.cproword)) , alltrim(CursLogErr.sccododl) )
      this.w_LOra = time()
      if not empty(this.w_LSerial)
        select _MsgErr_
        append blank
        replace LESERIAL with this.w_LSerial
        replace LEROWNUM with this.w_LNumErr
        replace LEDATOPE with i_datsys
        replace LEORAOPE with this.w_LOra
        replace LECODICE with this.w_LOggErr
        replace LEERRORE with this.w_LErrore
        replace LEMESSAG with this.w_LMess
        * --- Delete from SCALETTA
        i_nConn=i_TableProp[this.SCALETTA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SCALETTA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_LRowNum);
                 )
        else
          delete from (i_cTable) where;
                SCSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_LRowNum;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      select CursLogErr
      skip +1
    enddo
    this.TmpC = "La procedura ha rilevato %1 righe di dettaglio con ODL non pi� validi%0Desideri la stampa dell'elenco degli errori?"
    if ah_YesNo(this.TmpC,"",alltrim(str(l_Rec,5,0)))
      * --- Lancia La Stampa
      select * from _MsgErr_ into cursor __tmp__
      l_Titolo = "CHECK SCALETTA DI PRODUZIONE"
      CP_CHPRN("..\COLA\EXE\QUERY\GSCO_KLE")
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.GSCI_MSP.EcpFilter()     
    this.GSCI_MSP.w_SCSERIAL = this.w_SERIAL
    this.GSCI_MSP.EcpSave()     
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegno la variabile w_DATINI comme appoggio da passare alla GSCI_KSP
    this.w_DATINI = this.w_SCDATINI
    do GSCO_KSP with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.GSCI_MSP.Refresh()     
    if used("GeneApp")
      if reccount("GeneApp") > 0
        this.w_MESS = "Aggiornamento in corso..."
        ah_MSG(this.w_MESS)
        if this.w_SLQTARES = "S"
          Select * from GeneApp where SCQTARES>0 order by CPROWORD into cursor TmpScaletta
        else
          Select * from GeneApp where SCRESODL>0 order by CPROWORD into cursor TmpScaletta
        endif
        * --- Disabiltita il bottone importa
        select (this.w_CTRSNAME)
        delete all for empty(nvl(t_SCCODODL," ")) and empty(nvl(t_SCCODCIC," ")) 
        Select TmpScaletta
        SCAN
        * --- Seleziona ODL corrente ...
        this.w_SERIAL = nvl(TmpScaletta.SCSERIAL , space(15))
        this.w_CPROWNUM = nvl(tmpScaletta.CPROWNUM , 0)
        this.w_SCQTARIC = NVL(tmpScaletta.SCQTARES, 0)
        * --- Questa Select estrae una sola riga
        * --- Select from GSCO5BSP
        do vq_exec with 'GSCO5BSP',this,'_Curs_GSCO5BSP','',.f.,.t.
        if used('_Curs_GSCO5BSP')
          select _Curs_GSCO5BSP
          locate for 1=1
          do while not(eof())
          this.w_CPROWORD = NVL(_Curs_GSCO5BSP.CPROWORD, 0)
          this.w_SCCODODL = NVL(_Curs_GSCO5BSP.SCCODODL, SPACE(15))
          this.w_SCFASODL = NVL(_Curs_GSCO5BSP.SCFASODL, SPACE(5))
          this.w_SCROWNUM = NVL(_Curs_GSCO5BSP.SCROWNUM, 0)
          this.w_SCRISODL = NVL(_Curs_GSCO5BSP.SCRISODL, SPACE(20))
          this.w_SCLAVSEC = NVL(_Curs_GSCO5BSP.SCLAVSEC , 0)
          this.w_SCPROORA = NVL(_Curs_GSCO5BSP.SCPROORA , 0)
          this.w_OLTQTOEV = NVL(_Curs_GSCO5BSP.CLQTAAVA, 0)
          this.w_SCDESART = NVL(CADESART, SPACE(40))
          * --- Controllo Stato ODL
          do case
            case INLIST(this.w_SCTIPSCA , "CLQ", "CLO")
              this.w_OLDESART = NVL(CADESPAD, SPACE(40))
              * --- Read from ODL_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV,OLTSEODL,OLTFAODL,OLTSECPR"+;
                  " from "+i_cTable+" ODL_MAST where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_SCCODODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV,OLTSEODL,OLTFAODL,OLTSECPR;
                  from (i_cTable) where;
                      OLCODODL = this.w_SCCODODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SCCODODL = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
                this.w_SCCODICE = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
                this.w_SCDINRIC = NVL(cp_ToDate(_read_.OLTDINRIC),cp_NullValue(_read_.OLTDINRIC))
                this.w_OLTUNMIS = NVL(cp_ToDate(_read_.OLTUNMIS),cp_NullValue(_read_.OLTUNMIS))
                this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
                this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
                this.w_OLTQTODL = NVL(cp_ToDate(_read_.OLTQTODL),cp_NullValue(_read_.OLTQTODL))
                this.w_OLTDTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
                this.w_OLTQTOEV = NVL(cp_ToDate(_read_.OLTQTOEV),cp_NullValue(_read_.OLTQTOEV))
                this.w_OLTSEODL = NVL(cp_ToDate(_read_.OLTSEODL),cp_NullValue(_read_.OLTSEODL))
                this.w_OLTFAODL = NVL(cp_ToDate(_read_.OLTFAODL),cp_NullValue(_read_.OLTFAODL))
                this.w_OLTSECPR = NVL(cp_ToDate(_read_.OLTSECPR),cp_NullValue(_read_.OLTSECPR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Leggo i dati dall'ardine di riferimento
              * --- Read from ODL_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLTSECIC"+;
                  " from "+i_cTable+" ODL_MAST where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLTSECIC;
                  from (i_cTable) where;
                      OLCODODL = this.w_OLTSEODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SCCODCIC = NVL(cp_ToDate(_read_.OLTSECIC),cp_NullValue(_read_.OLTSECIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from CIC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CIC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CLCODCIC,CLDESCIC"+;
                  " from "+i_cTable+" CIC_MAST where ";
                      +"CLSERIAL = "+cp_ToStrODBC(this.w_SCCODCIC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CLCODCIC,CLDESCIC;
                  from (i_cTable) where;
                      CLSERIAL = this.w_SCCODCIC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_OLTCILAV = NVL(cp_ToDate(_read_.CLCODCIC),cp_NullValue(_read_.CLCODCIC))
                this.w_DESCIC = NVL(cp_ToDate(_read_.CLDESCIC),cp_NullValue(_read_.CLDESCIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case this.w_SCTIPSCA = "CSQ"
              * --- Read from ODL_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLTCICLO,OLTCODIC,OLTUNMIS,OLTSTATO,OLTDINRIC"+;
                  " from "+i_cTable+" ODL_MAST where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_SCCODODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLTCICLO,OLTCODIC,OLTUNMIS,OLTSTATO,OLTDINRIC;
                  from (i_cTable) where;
                      OLCODODL = this.w_SCCODODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SCCODCIC = NVL(cp_ToDate(_read_.OLTCICLO),cp_NullValue(_read_.OLTCICLO))
                this.w_SCCODICE = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
                this.w_ARUNMIS = NVL(cp_ToDate(_read_.OLTUNMIS),cp_NullValue(_read_.OLTUNMIS))
                this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                this.w_SCDINRIC = NVL(cp_ToDate(_read_.OLTDINRIC),cp_NullValue(_read_.OLTDINRIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
          endcase
          * --- Read from SCALETTM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SCALETTM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SCALETTM_idx,2],.t.,this.SCALETTM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SCTPQTOR"+;
              " from "+i_cTable+" SCALETTM where ";
                  +"SCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SCTPQTOR;
              from (i_cTable) where;
                  SCSERIAL = this.w_SERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OLDTPQTOR = NVL(cp_ToDate(_read_.SCTPQTOR),cp_NullValue(_read_.SCTPQTOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !Empty(this.w_SCCODODL)
            this.w_SCCODICE = NVL(_Curs_GSCO5BSP.SCCODICE, SPACE(5))
            do case
              case Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
                * --- Calcolo la produzione oraria
                if this.w_SLQTARES = "S"
                  if this.w_OLDTPQTOR=this.oParentObject.w_SCTPQTOR
                    this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCQTARES, 0)
                  else
                    if this.oParentObject.w_SCTPQTOR="Q"
                      this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCQTARES, 0) * 3600 / this.w_SCLAVSEC
                    else
                      this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCQTARES, 0) * this.w_SCLAVSEC / 3600
                    endif
                  endif
                else
                  if this.w_OLDTPQTOR=this.oParentObject.w_SCTPQTOR
                    this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCRESODL, 0)
                  else
                    if this.oParentObject.w_SCTPQTOR="Q"
                      this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCRESODL, 0) * 3600 / this.w_SCLAVSEC
                    else
                      this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCRESODL, 0) * this.w_SCLAVSEC / 3600
                    endif
                  endif
                endif
                this.w_SCQTAORA = this.w_SCQTAODL * this.w_SCLAVSEC / 3600
              case Inlist(this.w_SCTIPSCA, "CSQ", "CSO")
                if this.w_SLQTARES = "S"
                  this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCQTARES, 0)
                else
                  this.w_SCQTAODL = NVL(_Curs_GSCO5BSP.SCRESODL, 0) 
                endif
            endcase
            do case
              case this.w_SCTIPSCA == "CLO"
                this.w_SCQTARIC = this.w_SCQTAORA
              case this.w_SCTIPSCA $ "CLQ-CSQ"
                this.w_SCQTARIC = this.w_SCQTAODL
            endcase
            this.GSCI_MSP.InitRow()     
            this.GSCI_MSP.w_CPROWORD = this.w_CPROWORD
            this.GSCI_MSP.w_SCCODODL = this.w_SCCODODL
            this.GSCI_MSP.w_SCCODICE = this.w_SCCODICE
            this.GSCI_MSP.w_SCCODCIC = this.w_SCCODCIC
            this.GSCI_MSP.w_OLTCOART = this.w_OLTCOART
            this.GSCI_MSP.w_OLTUNMIS = this.w_ARUNMIS
            this.GSCI_MSP.w_OLTSTATO = this.w_OLTSTATO
            this.GSCI_MSP.w_OLTQTORE = this.w_SCQTAODL
            this.GSCI_MSP.w_OLTQTOEV = this.w_OLTQTOEV
            this.GSCI_MSP.w_OLTQTODL = this.w_OLTQTODL
            this.GSCI_MSP.w_SCRISODL = this.w_SCRISODL
            this.GSCI_MSP.w_SCQTAODL = this.w_SCQTAODL
            this.GSCI_MSP.w_SCQTMODL = this.w_SCQTAODL
            this.GSCI_MSP.w_SCQTARIC = this.w_SCQTAODL
            this.GSCI_MSP.w_SCDESART = this.w_SCDESART
            if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
              this.GSCI_MSP.w_SCTSEODL = this.w_OLTSEODL
              this.GSCI_MSP.w_OLTCODIC = this.w_OLTCODIC
              this.GSCI_MSP.w_OLDESART = this.w_OLDESART
              this.GSCI_MSP.w_OLTCILAV = this.w_OLTCILAV
              this.GSCI_MSP.w_DESCIC = this.w_DESCIC
              this.GSCI_MSP.w_SCROWNUM = this.w_SCROWNUM
              this.GSCI_MSP.w_CLROWORD = this.w_SCFASODL
              this.GSCI_MSP.w_SCFASODL = this.w_SCFASODL
              this.GSCI_MSP.w_SCLAVSEC = this.w_SCLAVSEC
              this.GSCI_MSP.w_SCPROORA = this.w_SCPROORA
              this.GSCI_MSP.w_SCRISODL = this.w_SCRISODL
              this.GSCI_MSP.w_SCDESFAS = this.w_SCDESFAS
              this.GSCI_MSP.w_SCQTAORA = this.w_SCQTAORA
              this.GSCI_MSP.w_SCQTAODL = this.w_SCQTAODL
              this.GSCI_MSP.w_SCQTMORA = this.w_SCQTAORA
              this.GSCI_MSP.w_SCQTMODL = this.w_SCQTAODL
              this.GSCI_MSP.w_SCQTARIC = IIF(this.oParentObject.w_SCTPQTOR="Q", this.w_SCQTAODL, this.w_SCQTAORA)
              this.GSCI_MSP.w_OLTSECPR = this.w_OLTSECPR
            endif
            Replace I_srv with "A"
            this.GSCI_MSP.TrsFromWork()     
            this.GSCI_MSP.mCalc()     
          endif
            select _Curs_GSCO5BSP
            continue
          enddo
          use
        endif
        ENDSCAN
        use in tmpScaletta
        * --- Rinfrescamenti vari
        With this.GSCI_MSP 
 SELECT (.cTrsName) 
 GO TOP 
 .WorkFromTrs() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=0 
 EndWith
      else
        ah_ErrorMsg("Non sono stati selezionate piani di produzione da elaborare",48)
      endif
      * --- Chiude cursore aperto dalla maschera GSDB_KGM
      use in GeneApp
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Declare ArrayFields (1, 16)
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- In base al tipo di scaletta verifico se posso effettuare il caricamento rapido
    if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
      this.w_Dettaglio = not empty(this.w_SCCODRIS) and not empty(this.w_SCDATINI)
    else
      this.w_Dettaglio = not empty(this.w_SCTABRIS) and not empty(this.w_SCDATINI)
    endif
    if this.w_Dettaglio
      this.w_CFUNC = this.GSCI_MSP.cFunction
      this.w_CPROWORD = 0
      this.w_CLROWORD = 0
      this.w_DATINI = this.oParentObject.w_SCPIAINI
      this.w_DATFIN = this.oParentObject.w_SCPIAFIN
      USE IN SELECT("LstTmp")
      this.GSCI_MSP.LockScreen = .f.
      if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
        do GSCI1KSP with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        do GSCO1KSP with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.GSCI_MSP.LockScreen = .t.
      if used ("LstTmp")
        if RECCOUNT ("LstTmp") > 0
          if this.w_CFUNC="Load" And this.GSCI_MSP.NumRow()=0
            SELECT (this.w_CTRSNAME)
            ZAP
          else
            this.GSCI_MSP.LastRow()     
            this.GSCI_MSP.SetRow()     
            if !this.GSCI_MSP.FullRow()
              SELECT (this.w_CTRSNAME)
              delete
              this.GSCI_MSP.LastRow()     
              this.GSCI_MSP.SetRow()     
            endif
            this.w_CPROWORD = this.GSCI_MSP.w_CPROWORD
          endif
        endif
      endif
      if used ("LstTmp")
        SELECT LstTmp
        GO TOP
        if reccount("LstTmp") > 0
          this.w_INSFIRSTR = .T.
          SCAN FOR NOT EMPTY(OLCODODL)
          SELECT LstTmp
          this.w_CPROWORD = this.w_CPROWORD + 10
          this.w_SCCODODL = NVL(OLCODODL, SPACE(15))
          if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
            this.w_SCRISODL = NVL(RLCODICE, SPACE(20))
          else
            this.w_SCRISODL = NVL(RICODICE, SPACE(15))
          endif
          this.w_SCCODICE = NVL(OLTCODIC, SPACE(20))
          this.w_OLTCOART = NVL(OLTCOART, SPACE(20))
          this.w_SCCODCIC = NVL(CLCODCIC, SPACE(5))
          this.w_ARUNMIS = NVL(OLTUNMIS, SPACE(3))
          this.w_OLTQTODL = NVL(OLTQTODL ,0)
          this.w_OLTSTATO = NVL(OLTSTATO, SPACE(1))
          this.w_SCDESART = NVL(CADESART, SPACE(40))
          if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
            this.w_OLTCILAV = NVL(OLTCILAV, SPACE(10))
            this.w_DESCIC = NVL(CLDESCIC, SPACE(40))
            this.w_SCTSEODL = NVL(OLTSEODL, SPACE(15))
            this.w_OLTCODIC = NVL(OLTCODPAD, SPACE(20))
            this.w_OLDESART = NVL(CADESPAD, SPACE(40))
            this.w_SCROWNUM = NVL(CPROWNUM, 0)
            this.w_SCFASODL = NVL(CLROWORD, SPACE(5))
            this.w_OLTSECPR = NVL(OLTSECPR, SPACE(15))
          else
            * --- Read from TABMCICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TABMCICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2],.t.,this.TABMCICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CSDESCRI"+;
                " from "+i_cTable+" TABMCICL where ";
                    +"CSCODICE = "+cp_ToStrODBC(this.w_SCCODCIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CSDESCRI;
                from (i_cTable) where;
                    CSCODICE = this.w_SCCODCIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCIC = NVL(cp_ToDate(_read_.CSDESCRI),cp_NullValue(_read_.CSDESCRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Calcolo la produzione oraria
          this.w_OLTQTOEV = NVL(CLQTAAVA, 0)
          this.w_SCQTAODL = NVL(CLQTARES, 0)
          if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
            this.w_SCDESFAS = NVL(CLDESFAS, "")
            this.w_SCLAVSEC = NVL(RLTPSLAU, 0)
            this.w_SCTPSLAV = NVL(RLTPSLAV, 0)
            this.w_SCPROORA = iif(this.w_SCLAVSEC=0, 0, 3600/this.w_SCLAVSEC)
            if this.w_SCTIPSCA == "CLO"
              this.w_SCQTARIC = cp_ROUND(IIF(this.w_SCTPSLAV>0, this.w_SCTPSLAV / 3600, 0), 5)
              this.w_SCQTAORA = this.w_SCQTARIC
            else
              * --- Recuper la quantit� residua dell'ODL
              this.w_SCQTARIC = NVL(CLQTARES, 0)
              this.w_SCQTAORA = this.w_SCQTARIC / this.w_SCPROORA
            endif
          else
            * --- Recuper la quantit� residua dell'ODL
            this.w_SCQTARIC = NVL(CLQTARES, 0)
          endif
          this.GSCI_MSP.InitRow()     
          if this.w_INSFIRSTR
            * --- nel caso sono in caricamento e sto inserendo la prima riga per evitare di pertire da 20
            this.GSCI_MSP.w_CPROWORD = this.w_CPROWORD
            if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
              this.GSCI_MSP.w_SCROWNUM = this.w_SCROWNUM
            endif
            this.w_INSFIRSTR = .F.
          endif
          this.GSCI_MSP.w_SCCODODL = this.w_SCCODODL
          this.GSCI_MSP.w_SCCODICE = this.w_SCCODICE
          this.GSCI_MSP.w_SCCODCIC = this.w_SCCODCIC
          this.GSCI_MSP.w_OLTCOART = this.w_OLTCOART
          this.GSCI_MSP.w_OLTUNMIS = this.w_ARUNMIS
          this.GSCI_MSP.w_OLTQTODL = this.w_OLTQTODL
          this.GSCI_MSP.w_OLTQTORE = this.w_SCQTAODL
          this.GSCI_MSP.w_OLTQTOEV = this.w_OLTQTOEV
          this.GSCI_MSP.w_OLTSTATO = this.w_OLTSTATO
          this.GSCI_MSP.w_SCRISODL = this.w_SCRISODL
          this.GSCI_MSP.w_SCQTAODL = this.w_SCQTAODL
          this.GSCI_MSP.w_SCQTMODL = this.w_SCQTAODL
          this.GSCI_MSP.w_SCQTARIC = this.w_SCQTAODL
          this.GSCI_MSP.w_SCDESART = this.w_SCDESART
          if Inlist(this.w_SCTIPSCA, "CLQ", "CLO")
            this.GSCI_MSP.w_SCTSEODL = this.w_SCTSEODL
            this.GSCI_MSP.w_OLTCODIC = this.w_OLTCODIC
            this.GSCI_MSP.w_OLDESART = this.w_OLDESART
            this.GSCI_MSP.w_OLTCILAV = this.w_OLTCILAV
            this.GSCI_MSP.w_DESCIC = this.w_DESCIC
            this.GSCI_MSP.w_SCROWNUM = this.w_SCROWNUM
            this.GSCI_MSP.w_CLROWORD = this.w_SCFASODL
            this.GSCI_MSP.w_SCFASODL = this.w_SCFASODL
            this.GSCI_MSP.w_SCLAVSEC = this.w_SCLAVSEC
            this.GSCI_MSP.w_SCPROORA = this.w_SCPROORA
            this.GSCI_MSP.w_SCRISODL = this.w_SCRISODL
            this.GSCI_MSP.w_SCDESFAS = this.w_SCDESFAS
            this.GSCI_MSP.w_SCQTAORA = this.w_SCQTAORA
            this.GSCI_MSP.w_SCQTAODL = this.w_SCQTAODL
            this.GSCI_MSP.w_SCQTMORA = this.w_SCQTAORA
            this.GSCI_MSP.w_SCQTMODL = this.w_SCQTAODL
            this.GSCI_MSP.w_SCQTARIC = IIF(this.oParentObject.w_SCTPQTOR="Q", this.w_SCQTAODL, this.w_SCQTAORA)
            this.GSCI_MSP.w_OLTSECPR = this.w_OLTSECPR
          endif
          this.GSCI_MSP.w_INPUTODL = .t.
          this.GSCI_MSP.TrsFromWork()     
          this.GSCI_MSP.mCalc()     
          ENDSCAN
        else
          this.GSCI_MSP.InitRow()     
        endif
      endif
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.GSCI_MSP.w_VARIATA = .f.
       With this.GSCI_MSP 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 ** .oPgFrm.Page1.oPag.oBody.nAbsRow=0 
 ** .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = .t. 
 EndWith
      USE IN SELECT("LstTmp")
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='ODL_CICL'
    this.cWorkTables[3]='ODL_RISO'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='PRD_ERRO'
    this.cWorkTables[6]='SCALETTA'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='CIC_MAST'
    this.cWorkTables[9]='RIS_ORSE'
    this.cWorkTables[10]='SCALETTM'
    this.cWorkTables[11]='TABMCICL'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP')
      use in _Curs__d__d__PRFA_EXE_QUERY_GSCICBSP
    endif
    if used('_Curs__d__d__PRFA_EXE_QUERY_GSCICBSP')
      use in _Curs__d__d__PRFA_EXE_QUERY_GSCICBSP
    endif
    if used('_Curs_GSCO5BSP')
      use in _Curs_GSCO5BSP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
