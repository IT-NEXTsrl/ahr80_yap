* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bbm                                                        *
*              Esportazione su MS-PROJECT                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-19                                                      *
* Last revis.: 2015-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bbm",oParentObject,m.pParam)
return(i_retval)

define class tgsco_bbm as StdBatch
  * --- Local variables
  w_CURODL = space(15)
  w_NUMREC = 0
  pParam = space(1)
  w_oMSP = .NULL.
  w_VERSION = 0
  w_oPRJ = .NULL.
  w_oRESOURCE = .NULL.
  w_oEXCEPT = .NULL.
  w_oTASK = .NULL.
  w_oERRORLOG = .NULL.
  w_GIORNO = ctod("  /  /  ")
  w_DESGIO = space(40)
  w_NUMORE = 0
  w_YEAR = 0
  w_MONTH = 0
  w_DAY = 0
  w_GAP1 = 0
  w_GAP2 = 0
  w_GAPTOT = 0
  w_Handle = space(10)
  w_TOWRITE = space(10)
  w_DAOW = 0
  w_ISEXCEPTION = .f.
  w_STIME1 = space(5)
  w_ETIME1 = space(5)
  w_STIME2 = space(5)
  w_ETIME2 = space(5)
  w_HPERDAY = 0
  w_FIRSTLOOP = .f.
  w_DISPONI = 0
  w_MINDATE = ctod("  /  /  ")
  w_MAXDATE = ctod("  /  /  ")
  w_RLCODICE = space(20)
  w_RLDESCRI = space(40)
  w_RLCALRIS = space(5)
  w_DRDATINI = ctod("  /  /  ")
  w_DRCALRIS = space(5)
  w_DRQTARIS = 0
  w_DRCOEFF = 0
  w_DRPERRID = 0
  w_DRDATFIN = ctod("  /  /  ")
  w_TASKMASTER = space(50)
  w_PPELAODL = ctod("  /  /  ")
  w_PPORAODL = space(8)
  w_PPBLOMRP = space(1)
  w_KEYMRP = space(20)
  w_NUMREC = 0
  w_RESOURCE = .NULL.
  w_CODICE = space(20)
  w_DESCRI = space(40)
  w_UID = 0
  w_ID = 0
  w_RESCOUNT = 0
  w_OLCODODL = space(15)
  w_OLTSECIC = space(10)
  w_OLTCODIC = space(41)
  w_OLDESCRI = space(40)
  w_OLTPROVE = space(1)
  w_TASKNAME = space(254)
  w_OLTDINRIC = ctod("  /  /  ")
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLTQTODL = 0
  w_OLTLEMPS = 0
  w_OLTQTOD1 = 0
  w_OLTQTSAL = 0
  w_OLTSTATO = space(1)
  w_CURUID = 0
  w_SONCOUNT = 0
  w_RESUID = 0
  w_MSPINI = ctod("  /  /  ")
  w_MSPFIN = ctod("  /  /  ")
  w_TODL = 0
  w_CURODL = 0
  w_CHIAVE = space(20)
  w_CURCHIAVE = space(20)
  w_CHIAVEDETT = space(21)
  w_CURCHIAVEDETT = space(21)
  w_UID = 0
  w_CURUID = 0
  w_PREDEC = space(20)
  w_AGGIORNA = .f.
  w_PADRE = .NULL.
  w_TIMEMAX = 0
  w_OWRK = 0
  w_ORECOUNT = 0
  w_TEXT1 = space(100)
  w_TEXT2 = space(100)
  w_TEXT3 = space(100)
  w_TEXT4 = space(100)
  w_TEXT5 = space(100)
  w_TEXT6 = space(100)
  w_TEXT7 = space(100)
  w_TEXT8 = space(100)
  w_TEXT9 = space(100)
  w_TEXT10 = space(100)
  w_TEXT11 = space(100)
  w_TEXT12 = space(100)
  w_TEXT13 = space(100)
  w_TEXT14 = space(100)
  w_TEXT15 = space(100)
  w_TEXT16 = space(100)
  w_TEXT17 = space(100)
  w_TEXT18 = space(100)
  w_TEXT19 = space(100)
  w_TEXT20 = space(100)
  w_TEXT21 = space(100)
  w_TEXT22 = space(100)
  w_TEXT23 = space(100)
  w_TEXT24 = space(100)
  w_TEXT25 = space(100)
  w_TEXT26 = space(100)
  w_TEXT27 = space(100)
  w_TEXT28 = space(100)
  w_TEXT29 = space(100)
  w_TEXT30 = space(100)
  w_CLROWNUM = 0
  w_CLROWORD = 0
  w_CL__FASE = space(5)
  w_CLDESFAS = space(20)
  w_PRIORITY = 0
  w_RLCODICE = space(20)
  w_RLTPSATT = 0
  w_RLTPSAVV = 0
  w_RLTPSLAV = 0
  w_OSETUP = .NULL.
  w_OWARMUP = .NULL.
  w_OWORK = .NULL.
  w_TUID = 0
  w_RLQPSATT = 0
  w_RLQPSAVV = 0
  w_RLQPSLAV = 0
  w_ASSCOUNT = 0
  w_SETUPCREATED = .f.
  w_WARMUPCREATED = .f.
  w_WORKCREATED = .f.
  w_RESID = 0
  w_CURASSIGN = .NULL.
  w_COUNTER = 0
  w_ARREL = 0
  w_CLPREUM1 = 0
  w_CLAVAUM1 = 0
  w_CLSCAUM1 = 0
  w_CLBCFUM1 = 0
  w_CLFASEVA = space(1)
  w_CURDID = 0
  w_RDDATRIL = ctod("  /  /  ")
  w_RDQTAUM1 = 0
  w_FLDICH = .f.
  w_DATAMIN = ctod("  /  /  ")
  w_TIMESCALE = .NULL.
  w_RLTPRLAV = 0
  w_CURASSIGN = .NULL.
  w_TMPN = 0
  w_MAXTIME = 0
  w_MAXID = 0
  w_PERCEN = 0
  w_RECNO = 0
  w_CHIAVE = space(30)
  w_OLTQTODL = 0
  w_THEQUERY = space(10)
  w_ORETOT = 0
  w_DTIMEIN = ctot("")
  w_DTIMEFI = ctot("")
  w_DPCOUNT = 0
  w_RESMIN = 0
  w_OLDWORK = 0
  w_QTATOT = 0
  w_FULLDIC = .f.
  w_NUMDIC = 0
  w_SECDIC = 0
  w_MINDIC = 0
  w_SECFORQTA = 0
  w_ARRC = 0
  w_CURID = 0
  w_SONODL = 0
  w_NUMREC = 0
  w_PREDEC = space(254)
  w_CURTASK = .NULL.
  w_CODL = 0
  w_CICLO = 0
  w_FASE = 0
  w_DETTFASE = 0
  w_UID = 0
  w_LOggErr = space(41)
  w_LOperaz = space(20)
  w_LErrore = space(80)
  w_LOra = space(8)
  w_LMessage = space(0)
  w_LSelOpe = space(1)
  w_LNumErr = 0
  w_LSerial = space(10)
  w_LSelOpe = space(1)
  w_LDettTec = space(1)
  w_LDatini = ctod("  /  /  ")
  w_LDatfin = ctod("  /  /  ")
  w_LEERR = space(250)
  * --- WorkFile variables
  ODL_MAST_idx=0
  PRD_ERRO_idx=0
  PAR_PROD_idx=0
  TMPODL_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esportazione Risorse, ODL ed OCL per MSProject 2007
    *     ----------------------------------------------------------------------------------------
    *     pParam:'E' Esportazione, 'I' Importazione
    *     
    *     Parametrizzazioni:
    *     -----------------------------------------------------------------------
    *     (GSDB2BBM) Propriet� delle ATTIVITA' in MSProject
    *     
    *     TEXT1 Codice ODL/OCL
    *     TEXT2 Codice Articolo
    *     TEXT3 Descrizione Articolo
    *     TEXT4 Gruppo Merceologico
    *     TEXT5 Categoria omogenea
    *     TEXT6 Pianificatore
    *     TEXT7 Magazzino
    *     TEXT8 Famiglia Articolo
    *     TEXT9 Famiglia Prodizione
    *     TEXT10 Quantit�
    *     TEXT11 Descrizione provenienza (ODL/OCL)
    *     TEXT12 Stato
    *     TEXT13 Commessa
    *     TEXT14 Fornitore
    *     TEXT15 Numero documento
    *     TEXT16 Provenienza (i/l)
    *     TEXT17 Wip out
    *     TEXT30 Codice azienda
    *     
    *     NUMERO1 INT(CODICE ODL/OCL)
    *     NUMERO2 Numero FASE
    *     NUMERO3 Tipo FASE
    *     
    *     ----------------------------------------------------------------------------------------
    *     Propriet� delle RISORSE in MSProject
    *     TEXT20 Codice Stabilimento
    *     TEXT21 Reparto
    *     TEXT22 Area
    *     
    * --- Variabili di appoggio per ODL\OCL
    * --- Variabili per il Task
    this.w_PADRE = this.oParentObject
    * --- Istanzia Application di MS-Project
    if !FILE(this.oParentObject.w_FILEMODEL)
      ah_errormsg("Il file %1 non esiste, specificare un file di modello valido.",48,"",alltrim(this.oParentObject.w_FILEMODEL))
      i_retcode = 'stop'
      return
    endif
    this.w_oMSP = CreateObject("MsProject.Application")
    this.w_oMSP.Visible = .t.
    this.w_VERSION = VAL(this.w_oMSP.Version)
    this.w_oMSP.FileOpen(this.oParentObject.w_FILEMODEL)     
    this.w_oPRJ = this.w_oMSP.ActiveProject
    i_olderr=on("ERROR") 
 i_err=.f.
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPELAODL,PPORAODL,PPBLOMRP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPELAODL,PPORAODL,PPBLOMRP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPELAODL = NVL(cp_ToDate(_read_.PPELAODL),cp_NullValue(_read_.PPELAODL))
      this.w_PPORAODL = NVL(cp_ToDate(_read_.PPORAODL),cp_NullValue(_read_.PPORAODL))
      this.w_PPBLOMRP = NVL(cp_ToDate(_read_.PPBLOMRP),cp_NullValue(_read_.PPBLOMRP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PPBLOMRP == "S"
      ah_ErrorMsg("Impossibile eseguire l'esportazione mentre si sta effettuando l'elaborazione MRP.",16)
      i_retcode = 'stop'
      return
    endif
    this.w_KEYMRP = DTOS(this.w_PPELAODL)+STRTRAN(STRTRAN(STRTRAN(this.w_PPORAODL,":",""),"-",""),".","")
    if this.w_oPRJ.Text10 <> this.w_KEYMRP
      this.w_PADRE.oPgFrm.ActivePage = 2
      * --- Istanzia Application di MS-Project
      if !FILE(this.oParentObject.w_FILEMODEL)
        ah_errormsg("Il file %1 non esiste, specificare un file di modello valido.",48,"",alltrim(this.oParentObject.w_FILEMODEL))
        i_retcode = 'stop'
        return
      endif
      this.w_oMSP = CreateObject("MsProject.Application")
      this.w_oMSP.Visible = .t.
      this.w_VERSION = VAL(this.w_oMSP.Version)
      this.w_oMSP.FileOpen(this.oParentObject.w_FILEMODEL)     
      this.w_oPRJ = this.w_oMSP.ActiveProject
      this.w_oMSP.LevelingOptions(.f.)     
      this.w_oMSP.LevelNow(.f.)     
      this.w_oPRJ.Activate()     
      this.w_oMSP.ScreenUpdating = False
      AddMsgNL("%0Cancellazione ordini suggeriti%0",this.w_PADRE)
      this.w_oPRJ.Text10 = this.w_KEYMRP
      if !EMPTY(this.w_oPRJ.Text10)
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.w_PADRE.oPgFrm.ActivePage = 2
    if this.pParam="E"
      * --- Esportazione
      AddMsgNL("Esportazione piano di produzione iniziata alle %1",this.w_PADRE,time()) 
 AddMsgNL(REPL("-",40),this.w_PADRE)
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='OLCODODL'
        cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL,OLTDINRIC,OLTDTRIC "," from "+i_cQueryTable+" where OLTMSPINI is null AND OLTMSPFIN is null",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTMSPINI = ODL_MAST.OLTDINRIC";
            +",OLTMSPFIN = ODL_MAST.OLTDTRIC";
            +i_ccchkf;
            +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
            +"ODL_MAST.OLTMSPINI = ODL_MAST.OLTDINRIC";
            +",ODL_MAST.OLTMSPFIN = ODL_MAST.OLTDTRIC";
            +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
            +"OLTMSPINI = ODL_MAST.OLTDINRIC";
            +",OLTMSPFIN = ODL_MAST.OLTDTRIC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTMSPINI = (select OLTDINRIC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",OLTMSPFIN = (select OLTDTRIC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_STIME1 = "9.00"
      this.w_ETIME1 = "13.00"
      this.w_STIME2 = "14.00"
      this.w_ETIME2 = "18.00"
      this.w_TASKMASTER = "Piano di produzione azienda "+alltrim(i_CODAZI)
      this.w_oERRORLOG=createobject("AH_ERRORLOG")
      this.w_oMSP.LevelingOptions(.f.)     
      this.w_LOperaz = "EP"
      * --- Pulizia Log Errori
      * --- Try
      local bErr_03743AA0
      bErr_03743AA0=bTrsErr
      this.Try_03743AA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03743AA0
      * --- End
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_ODL="I" or this.oParentObject.w_OCL="L"
        if this.oParentObject.w_CALENDAR<>"S"
          * --- Se il calendario selezionato � diverso dallo standard di MS Project allora lo esporto
          AddMsgNL("%0Fase 1: Esportazione calendari aziendali e disponibilit�%0",this.w_PADRE)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        AddMsgNL("%0Fase 2: Esportazione attivit� ODL/OCL",this.w_PADRE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_FLPRESUC="S"
          AddMsgNL("%0Fase 3: Esportazione dipendenze attivit� ODL/OCL (Pegging)",this.w_PADRE)
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if USED("ODLCURSOR")
        Select ODLCURSOR
        SCAN FOR EMPTY(NVL(STATO,"")) AND NVL(ODL,0)<>0
        this.w_CURUID = UID
        this.w_CURTASK = this.w_oPRJ.tasks.UniqueID(this.w_CURUID)
        if this.w_CURTASK.Text12<>"F"
          this.w_LOggErr = ah_msgformat("%1", iif(this.w_CURTASK.Text16="I","ODL","OCL")+" "+alltrim(this.w_CURTASK.Text1)+" "+alltrim(this.w_CURTASK.Name))
          this.w_LErrore = ah_msgformat("Cancellato")
          this.w_LMessage = ah_msgformat("Cancellato")
          this.w_CURTASK.Delete()     
          DELETE FROM ODLCURSOR WHERE UID=this.w_CURUID
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        ENDSCAN
        Select ODLCURSOR
        SCAN FOR NVL(ODL,0)<>0
        this.w_CURUID = UID
        this.w_CURTASK = this.w_oPRJ.tasks.UniqueID(this.w_CURUID)
        this.w_LOggErr = ah_msgformat("%1", iif(this.w_CURTASK.Text16="I","ODL","OCL")+" "+alltrim(this.w_CURTASK.Text1)+" "+alltrim(this.w_CURTASK.Name))
        this.w_LErrore = iif(STATO="P",ah_msgformat("Inserito"),ah_msgformat("Aggiornato"))
        this.w_LMessage = iif(STATO="P",ah_msgformat("Inserito"),ah_msgformat("Aggiornato"))
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ENDSCAN
      endif
      this.w_oMSP.UpdateProject(.T.,Date(),2)     
      this.w_oMSP.ScreenUpdating = True
      if ah_YesNo("Si desidera stampare il resoconto dell'esportazione?")
        * --- Lancia Stampa
        do GSCO_BLE with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.oParentObject.w_EXPORTED = "S"
      if USED("RESCURSOR")
        Select RESCURSOR 
 USE
      endif
      if USED("ODLCURSOR")
        Select ODLCURSOR 
 USE
      endif
      * --- Drop temporary table TMPODL_MAST
      i_nIdx=cp_GetTableDefIdx('TMPODL_MAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPODL_MAST')
      endif
      * --- Drop temporary table TMPCNLAV
      i_nIdx=cp_GetTableDefIdx('TMPCNLAV')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPCNLAV')
      endif
    else
      * --- Importazione
      this.w_LOperaz = "IP"
      * --- Pulizia Log Errori
      * --- Try
      local bErr_037566E8
      bErr_037566E8=bTrsErr
      this.Try_037566E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_037566E8
      * --- End
      this.w_NUMREC = 2
      this.w_CURODL = ""
      this.w_OLCODODL = ""
      this.w_MINDATE = Ctod("  -  -    ")
      this.w_MAXDATE = Ctod("  -  -    ")
      this.w_AGGIORNA = .f.
      do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
        this.w_CURTASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
        this.w_CURODL = this.w_CURTASK.TEXT1
        if this.w_CURODL<>this.w_OLCODODL or empty(this.w_OLCODODL)
          if this.w_AGGIORNA
            * --- begin transaction
            cp_BeginTrs()
            * --- Try
            local bErr_037A0020
            bErr_037A0020=bTrsErr
            this.Try_037A0020()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              this.w_LErrore = ah_msgformat("Non aggiornato")
              this.w_LMessage = message()
            endif
            bTrsErr=bTrsErr or bErr_037A0020
            * --- End
          endif
          this.w_AGGIORNA = .f.
          this.w_MINDATE = Ctod("  -  -    ")
          this.w_MAXDATE = Ctod("  -  -    ")
        endif
        if TTOD(this.w_CURTASK.Start) <> TTOD(this.w_CURTASK.Date1)
          this.w_AGGIORNA = .t.
          if this.w_MINDATE>this.w_CURTASK.Start
            this.w_MINDATE = this.w_CURTASK.Start
          endif
        endif
        if EMPTY(this.w_MINDATE)
          this.w_MINDATE = this.w_CURTASK.Start
        endif
        if TTOD(this.w_CURTASK.Finish) <> TTOD(this.w_CURTASK.Date2)
          this.w_AGGIORNA = .t.
          if this.w_MAXDATE<this.w_CURTASK.Finish
            this.w_MAXDATE = this.w_CURTASK.Finish
          endif
        endif
        if EMPTY(this.w_MAXDATE)
          this.w_MAXDATE = this.w_CURTASK.Finish
        endif
        this.w_OLCODODL = this.w_CURODL
        this.w_NUMREC = this.w_NUMREC+1
      enddo
      if this.w_AGGIORNA
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_03787AF0
        bErr_03787AF0=bTrsErr
        this.Try_03787AF0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_LErrore = ah_msgformat("Non aggiornato")
          this.w_LMessage = message()
        endif
        bTrsErr=bTrsErr or bErr_03787AF0
        * --- End
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if ah_YesNo("Si desidera stampare il resoconto dell'importazione?")
        * --- Lancia Stampa
        l_Titolo = "Resoconto errori export MS Project"
        do GSCO_BLE with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    on error &i_olderr
    if i_err
      AddMsgNL("Non � possibile %1 il piano di produzione: %2",this.w_PADRE,iif(this.pParam="E","esportazione","importazione"),i_err)
    endif
  endproc
  proc Try_03743AA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRD_ERRO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRD_ERRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LESTORIC ="+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTORIC');
          +i_ccchkf ;
      +" where ";
          +"LECODUTE = "+cp_ToStrODBC(i_CODUTE);
          +" and LEOPERAZ = "+cp_ToStrODBC(this.w_LOperaz);
          +" and LESTORIC = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          LESTORIC = "N";
          &i_ccchkf. ;
       where;
          LECODUTE = i_CODUTE;
          and LEOPERAZ = this.w_LOperaz;
          and LESTORIC = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_037566E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRD_ERRO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRD_ERRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LESTORIC ="+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTORIC');
          +i_ccchkf ;
      +" where ";
          +"LECODUTE = "+cp_ToStrODBC(i_CODUTE);
          +" and LEOPERAZ = "+cp_ToStrODBC(this.w_LOperaz);
          +" and LESTORIC = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          LESTORIC = "N";
          &i_ccchkf. ;
       where;
          LECODUTE = i_CODUTE;
          and LEOPERAZ = this.w_LOperaz;
          and LESTORIC = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_037A0020()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTMSPINI ="+cp_NullLink(cp_ToStrODBC(this.w_MINDATE),'ODL_MAST','OLTMSPINI');
      +",OLTMSPFIN ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDATE),'ODL_MAST','OLTMSPFIN');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      update (i_cTable) set;
          OLTMSPINI = this.w_MINDATE;
          ,OLTMSPFIN = this.w_MAXDATE;
          &i_ccchkf. ;
       where;
          OLCODODL = this.w_OLCODODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_LErrore = ah_msgformat("Aggiornato")
    this.w_LMessage = ah_msgformat("Aggiornato")
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03787AF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTMSPINI ="+cp_NullLink(cp_ToStrODBC(this.w_MINDATE),'ODL_MAST','OLTMSPINI');
      +",OLTMSPFIN ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDATE),'ODL_MAST','OLTMSPFIN');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      update (i_cTable) set;
          OLTMSPINI = this.w_MINDATE;
          ,OLTMSPFIN = this.w_MAXDATE;
          &i_ccchkf. ;
       where;
          OLCODODL = this.w_OLCODODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_LErrore = ah_msgformat("Aggiornato")
    this.w_LMessage = ah_msgformat("Aggiornato")
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sulle date del calendario della risorsa e gestisco le eccezioni...
    this.w_DRCALRIS = this.oParentObject.w_PPCALSTA
    this.w_HPERDAY = 8
    * --- Le date devono essere comprese tra 01/01/1984 e il 31/12/2049 altrimenti Microsoft project va in errore OLE dispatch... (suo limite del calendario)
    this.w_DRDATINI = cp_todate(ctod("01-01-1984"))
    this.w_DRDATINI = iif(EMPTY(this.oParentObject.w_DATACALIN),this.w_DRDATINI,this.oParentObject.w_DATACALIN)
    this.w_DRDATFIN = cp_todate(ctod("31-12-2049"))
    this.w_DRDATFIN = iif(EMPTY(this.oParentObject.w_DATACALFI),this.w_DRDATFIN,this.oParentObject.w_DATACALFI)
    * --- Select from GSCO1BBM
    do vq_exec with 'GSCO1BBM',this,'_Curs_GSCO1BBM','',.f.,.t.
    if used('_Curs_GSCO1BBM')
      select _Curs_GSCO1BBM
      locate for 1=1
      do while not(eof())
      this.w_GAP1 = 0
      this.w_GAP2 = 0
      this.w_GIORNO = NVL(_Curs_GSCO1BBM.CAGIORNO,ctod("  -  -    "))
      this.w_NUMORE = NVL(_Curs_GSCO1BBM.CANUMORE,0)
      this.w_YEAR = year(this.w_GIORNO)
      this.w_MONTH = month(this.w_GIORNO)
      this.w_DAY = day(this.w_GIORNO)
      this.w_DAOW = DOW(this.w_GIORNO)
      this.w_STIME1 = ALLTRIM(NVL(_Curs_GSCO1BBM.CAORAIN1, "9.00"))
      this.w_ETIME1 = ALLTRIM(NVL(_Curs_GSCO1BBM.CAORAFI1, "13.00"))
      this.w_STIME2 = ALLTRIM(NVL(_Curs_GSCO1BBM.CAORAIN2, "14.00"))
      this.w_ETIME2 = ALLTRIM(NVL(_Curs_GSCO1BBM.CAORAFI2, "18.00"))
      * --- Se le ore giornaliere sono > 14 allora devo iniziare prima delle 9.00
      if this.w_NUMORE = 24
        this.w_oEXCEPT = this.w_oPRJ.Calendar.Period(dtoc(this.w_GIORNO),dtoc(this.w_GIORNO))
        this.w_oEXCEPT.Shift1.Start = "00.00"
        this.w_oEXCEPT.Shift1.Finish = "23.59"
        this.w_oEXCEPT.Shift2.Clear()     
        this.w_oEXCEPT.Shift3.Clear()     
        this.w_oEXCEPT.Shift4.Clear()     
        this.w_oEXCEPT.Shift5.Clear()     
      else
        if this.w_NUMORE=0
          * --- Setto il giorno festivo
          this.w_oEXCEPT = this.w_oPRJ.Calendar.Period(dtoc(this.w_GIORNO),dtoc(this.w_GIORNO))
          this.w_oEXCEPT.Working = .f.
        else
          this.w_GAP1 = this.w_NUMORE-this.w_HPERDAY
          if this.w_GAP1 > 0
            * --- Ore lavorative > 8
            if this.w_GAP1 > 6
              this.w_GAP2 = this.w_GAP1 - 6
              this.w_GAP1 = 6
            endif
            if this.w_GAP1=6
              this.w_GAP1 = 5.59
            endif
            this.w_oEXCEPT = this.w_oPRJ.Calendar.Period(dtoc(this.w_GIORNO),dtoc(this.w_GIORNO))
            this.w_oEXCEPT.Shift1.Start = STRTRAN(STR(VAL(this.w_STIME1)-(this.w_GAP2),5,2),",",".")
            this.w_oEXCEPT.Shift1.Finish = this.w_ETIME1
            this.w_oEXCEPT.Shift2.Start = this.w_STIME2
            this.w_oEXCEPT.Shift2.Finish = STRTRAN(STR(VAL(this.w_ETIME2)+this.w_GAP1,5,2),",",".")
            this.w_oEXCEPT.Shift3.Clear()     
            this.w_oEXCEPT.Shift4.Clear()     
            this.w_oEXCEPT.Shift5.Clear()     
          else
            * --- Ore lavorative < 8
            if -this.w_GAP1 > 4
              this.w_GAP2 = this.w_GAP1 + 4
              this.w_GAP1 = -4
            endif
            this.w_oEXCEPT = this.w_oPRJ.Calendar.Period(dtoc(this.w_GIORNO),dtoc(this.w_GIORNO))
            this.w_oEXCEPT.Shift1.Start = this.w_STIME1
            this.w_oEXCEPT.Shift1.Finish = STR(VAL(this.w_ETIME1)+this.w_GAP2,5,2)
            if this.w_GAP2 <> 0
              this.w_oEXCEPT.Shift2.Clear()     
            else
              this.w_oEXCEPT.Shift2.Start = this.w_STIME2
              this.w_oEXCEPT.Shift2.Finish = STR(VAL(this.w_ETIME2)+this.w_GAP1,5,2)
            endif
            this.w_oEXCEPT.Shift3.Clear()     
            this.w_oEXCEPT.Shift4.Clear()     
            this.w_oEXCEPT.Shift5.Clear()     
          endif
        endif
      endif
        select _Curs_GSCO1BBM
        continue
      enddo
      use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CLROWORD = 0
    this.w_PRIORITY = 1000
    this.w_OLTDTRIC = ctod("  -  -    ")
    this.w_NUMREC = 1
    do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
      this.w_CURTASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
      this.w_oPRJ.Tasks.Item(this.w_NUMREC).UniqueIDPredecessors = ""
      this.w_NUMREC = this.w_NUMREC+1
    enddo
    * --- Create temporary table TMPODL_MAST
    i_nIdx=cp_AddTableDef('TMPODL_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCO10BBM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPODL_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_CHIAVE = "XXXXXXXXXXXXXXX"
    * --- Select from GSCO12BBM
    do vq_exec with 'GSCO12BBM',this,'_Curs_GSCO12BBM','',.f.,.t.
    if used('_Curs_GSCO12BBM')
      select _Curs_GSCO12BBM
      locate for 1=1
      do while not(eof())
      this.w_OLCODODL = NVL(_Curs_GSCO12BBM.OLCODODL,"")
      if ALLTRIM(this.w_OLCODODL)<> this.w_CHIAVE
        if this.w_OLTDTRIC < NVL(OLTDTRIC,ctod("  -  -    ")) AND this.w_PRIORITY > 0
          this.w_PRIORITY = this.w_PRIORITY-1
        endif
        if TYPE("ResWork[1,1]") <>"U"
          this.w_ARRC = 1
          do while this.w_ARRC < this.w_RESCOUNT
            ResWork[this.w_ARRC,1]=.f.
            ResWork[this.w_ARRC,2]=.f.
            ResWork[this.w_ARRC,3]=.f.
            ResWork[this.w_ARRC,4]=.f.
            this.w_ARRC = this.w_ARRC + 1
          enddo
        endif
        this.w_FLDICH = .f.
        this.w_FULLDIC = .f.
        if this.w_OLCODODL<>NVL(_Curs_GSCO12BBM.OLCODODL,"")
          AddMsgNL(" - %1 %2 (%3)",this.w_PADRE,iif(_Curs_GSCO12BBM.OLTPROVE="l"," OCL", "ODL"), _Curs_GSCO12BBM.OLCODODL,alltrim(_Curs_GSCO12BBM.TEXT2)+": "+alltrim(NVL(_Curs_GSCO12BBM.TEXT3,"")) )
        endif
        this.w_OLTCODIC = NVL(_Curs_GSCO12BBM.OLTCODIC,"")
        this.w_OLDESCRI = NVL(_Curs_GSCO12BBM.CADESART,"")
        this.w_OLTPROVE = NVL(_Curs_GSCO12BBM.OLTPROVE,"")
        this.w_OLTDINRIC = NVL(_Curs_GSCO12BBM.OLTDINRIC,ctod("  -  -    "))
        this.w_OLTDTRIC = NVL(_Curs_GSCO12BBM.OLTDTRIC,ctod("  -  -    "))
        this.w_OLTQTODL = NVL(_Curs_GSCO12BBM.OLTQTODL,1)
        this.w_OLTLEMPS = NVL(_Curs_GSCO12BBM.OLTEMLAV,0)
        this.w_OLTSTATO = NVL(_Curs_GSCO12BBM.OLTSTATO,"")
        this.w_OLTQTOD1 = NVL(_Curs_GSCO12BBM.OLTQTOD1,0)
        this.w_OLTQTSAL = NVL(_Curs_GSCO12BBM.OLTQTSAL,0)
        this.w_OLTSECIC = NVL(_Curs_GSCO12BBM.OLTSECIC,"")
        this.w_OLTQTODL = NVL(_Curs_GSCO12BBM.OLTQTODL,0)
        this.w_RLCODICE = NVL(RLCODICE,"")
        this.w_TEXT1 = NVL(_Curs_GSCO12BBM.TEXT1,"")
        this.w_TEXT2 = NVL(_Curs_GSCO12BBM.TEXT2,"")
        this.w_TEXT3 = NVL(_Curs_GSCO12BBM.TEXT3,"")
        this.w_TEXT4 = NVL(_Curs_GSCO12BBM.TEXT4,"")
        this.w_TEXT5 = NVL(_Curs_GSCO12BBM.TEXT5,"")
        this.w_TEXT6 = NVL(_Curs_GSCO12BBM.TEXT6,"")
        this.w_TEXT7 = NVL(_Curs_GSCO12BBM.TEXT7,"")
        this.w_TEXT8 = NVL(_Curs_GSCO12BBM.TEXT8,"")
        this.w_TEXT9 = NVL(_Curs_GSCO12BBM.TEXT9,"")
        this.w_TEXT10 = STR(NVL(_Curs_GSCO12BBM.TEXT10,0))
        this.w_TEXT11 = NVL(_Curs_GSCO12BBM.TEXT11,"")
        this.w_TEXT12 = NVL(_Curs_GSCO12BBM.TEXT12,"")
        this.w_TEXT13 = NVL(_Curs_GSCO12BBM.TEXT13,"")
        this.w_TEXT14 = NVL(_Curs_GSCO12BBM.TEXT14,"")
        this.w_TEXT15 = NVL(_Curs_GSCO12BBM.TEXT15,"")
        this.w_TEXT16 = NVL(_Curs_GSCO12BBM.TEXT16,"")
        this.w_TEXT17 = NVL(_Curs_GSCO12BBM.TEXT17,"")
        this.w_TEXT18 = NVL(_Curs_GSCO12BBM.TEXT18,"")
        this.w_TEXT19 = NVL(_Curs_GSCO12BBM.TEXT19,"")
        this.w_TEXT20 = NVL(_Curs_GSCO12BBM.TEXT20,"")
        this.w_TEXT21 = NVL(_Curs_GSCO12BBM.TEXT21,"")
        this.w_TEXT22 = NVL(_Curs_GSCO12BBM.TEXT22,"")
        this.w_TEXT23 = NVL(_Curs_GSCO12BBM.TEXT23,"")
        this.w_TEXT24 = NVL(_Curs_GSCO12BBM.TEXT24,"")
        this.w_TEXT25 = NVL(_Curs_GSCO12BBM.TEXT25,"")
        this.w_TEXT26 = NVL(_Curs_GSCO12BBM.TEXT26,"")
        this.w_TEXT27 = NVL(_Curs_GSCO12BBM.TEXT27,"")
        this.w_TEXT28 = NVL(_Curs_GSCO12BBM.TEXT28,"")
        this.w_TEXT29 = NVL(_Curs_GSCO12BBM.TEXT29,"")
        this.w_TEXT30 = NVL(_Curs_GSCO12BBM.TEXT30,"")
        this.w_CL__FASE = NVL(_Curs_GSCO12BBM.CL__FASE,"")
        * --- Le date devono essere comprese tra 01/01/1984 e il 31/12/2049 altrimenti Microsoft project va in errore OLE dispatch... (suo limite del calendario)
        if BETWEEN(this.w_OLTDINRIC , cp_chartodate("01-01-1984") , cp_chartodate("31-12-2049")) and BETWEEN(this.w_OLTDTRIC , cp_chartodate("01-01-1984") , cp_chartodate("31-12-2049"))
          * --- Aggiungo una fase fittizia...
          this.w_CURDID = this.CheckDettFase(this.w_OLCODODL)
          if this.w_CURDID<>0
            * --- Nel progetto � gi� presente la fase
            this.w_oWORK = this.w_oPRJ.tasks.UniqueID(this.w_CURDID)
            UPDATE ODLCURSOR SET STATO="A" where ODL=int(val(this.w_OLCODODL))
          else
            * --- Inserisco il nuovo task per la fase
            this.w_oWORK = this.w_oPRJ.tasks.add("Ordine di lavorazione")
            INSERT INTO ODLCURSOR (ODL,UID,STATO) values (int(val(this.w_OLCODODL)),this.w_oWORK.UniqueID,"P")
          endif
          this.w_oWORK.OutlineLevel = 1
          this.w_oWORK.Start = dtoc(this.w_OLTDINRIC)
          this.w_oWORK.Duration = int(this.w_OLTLEMPS * 480)
          this.w_oWORK.ConstraintType = 4
          this.w_oWORK.ConstraintDate = dtoc(this.w_OLTDINRIC)
          this.w_oWORK.DeadLine = dtoc(this.w_OLTDTRIC)
          this.w_oWORK.PercentComplete = iif(this.w_OLTQTOD1=0,0,(this.w_OLTQTOD1-this.w_OLTQTSAL)*100/this.w_OLTQTOD1)
          this.w_oWORK.Number3 = 99
          this.w_oWORK.Priority = this.w_PRIORITY
          this.SetContrass(this.w_oWORK,5)
          this.SetTextValues(this.w_oWORK)
          this.w_oWORK.Number2 = int(val(this.w_OLTSECIC))
          this.w_oWORK.Date1 = dtoc(this.w_OLTDINRIC)
          this.w_oWORK.Date2 = this.w_oWORK.Finish
        endif
        if USED("_DETTFASI_")
          USE IN _DETTFASI_
        endif
        if USED("_DICHIARAZ_")
          USE IN _DICHIARAZ_
        endif
      endif
      this.w_CHIAVE = ALLTRIM(this.w_OLCODODL)
        select _Curs_GSCO12BBM
        continue
      enddo
      use
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select * from ODLCURSOR order by ODL into cursor ODLCURSORORD
    this.w_UID = 0
    this.w_CHIAVE = ""
    SELECT ODLCURSORORD 
 GO TOP 
 SCAN FOR ODL<>0
    this.w_CURODL = NVL(ODL,0)
    this.w_CURCHIAVE = RIGHT("000000000000000"+ALLTRIM(STR(INT(ODL))),15)
    this.w_CURCHIAVEDETT = RIGHT("000000000000000"+ALLTRIM(STR(INT(ODL))),15)
    this.w_CURUID = this.CheckDettFase(this.w_CURCHIAVEDETT)
    if this.w_CURODL=this.w_TODL and this.w_CURCHIAVE<>this.w_CHIAVE
      this.w_PREDEC = this.w_oPRJ.Tasks.UniqueID(this.w_CURUID).UniqueIDPredecessors
      this.w_oPRJ.Tasks.UniqueID(this.w_CURUID).UniqueIDPredecessors = iif(EMPTY(this.w_PREDEC),"",this.w_oPRJ.Tasks.UniqueID(this.w_CURUID).UniqueIDPredecessors+",")+alltrim(str(this.w_UID))
    endif
    this.w_UID = this.w_CURUID
    this.w_CHIAVE = this.w_CURCHIAVE
    this.w_TODL = this.w_CURODL
    ENDSCAN
    if USED("ODLCURSORORD")
      Select ODLCURSORORD 
 USE
    endif
    Select MIN(UID) as UID, ODL from ODLCURSOR group by ODL into cursor FASEMIN
    Select MAX(UID) as UID, ODL from ODLCURSOR group by ODL into cursor FASEMAX
    Select FASEMIN 
 GO TOP 
 SCAN
    this.w_NUMREC = Recno()
    this.w_CURODL = RIGHT(repl("0",15)+ALLTRIM(STR(NVL(ODL,0))),15)
    this.w_CURID = NVL(UID,0)
    this.w_PREDEC = ""
    * --- Select from GSCO5BBM
    do vq_exec with 'GSCO5BBM',this,'_Curs_GSCO5BBM','',.f.,.t.
    if used('_Curs_GSCO5BBM')
      select _Curs_GSCO5BBM
      locate for 1=1
      do while not(eof())
      this.w_SONODL = NVL(INT(VAL(_Curs_GSCO5BBM.PESERODL)),0)
      Select FASEMAX 
 LOCATE FOR ODL=this.w_SONODL
      if found()
        this.w_PREDEC = iif(EMPTY(this.w_PREDEC),"",alltrim(this.w_PREDEC)+";")+alltrim(str(int(UID)))
      endif
        select _Curs_GSCO5BBM
        continue
      enddo
      use
    endif
    if !EMPTY(this.w_PREDEC)
      this.w_oPRJ.Tasks.UniqueID(this.w_CURID).UniqueIDPredecessors = this.w_PREDEC
    endif
    Select FASEMIN 
 GO this.w_NUMREC
    ENDSCAN
    if USED("FASEMAX")
      Select FASEMAX 
 USE
    endif
    if USED("FASEMIN")
      Select FASEMIN 
 USE
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ah_msg("Analisi attivit� in corso...")
    CREATE CURSOR ODLCURSOR (ODL N(15), UID N(10), STATO C(1))
    this.w_NUMREC = 1
    do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
      this.w_CURTASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
      this.w_UID = this.w_CURTASK.UniqueID
      this.w_CODL = INT(VAL(this.w_CURTASK.Text1))
      INSERT INTO ODLCURSOR (ODL, UID,STATO) values (this.w_CODL,this.w_UID," ")
      this.w_NUMREC = this.w_NUMREC+1
    enddo
    Select ODLCURSOR 
 =WRCURSOR("ODLCURSOR") 
 INDEX ON ODL TAG IDX1
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Log
    this.w_LEERR = ""
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    if empty(this.w_LSerial)
      * --- Determina progressivo log
      this.w_LSerial = cp_GetProg("PRD_ERRO","PRERR",this.w_LSerial,i_CODAZI)
    endif
    * --- Scrive LOG
    if not empty(this.w_LSerial)
      * --- Scrive LOG
      this.w_LOra = time()
      if empty(this.w_LMessage) and this.w_LDettTec="S"
        this.w_LMessage = "Message()= "+message()
      endif
      * --- Try
      local bErr_038F9380
      bErr_038F9380=bTrsErr
      this.Try_038F9380()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_038F9380
      * --- End
    endif
    this.w_LMessage = ""
  endproc
  proc Try_038F9380()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRD_ERRO
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRD_ERRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LESERIAL"+",LEROWNUM"+",LEOPERAZ"+",LEDATOPE"+",LEORAOPE"+",LECODUTE"+",LECODICE"+",LEERRORE"+",LEMESSAG"+",LESTORIC"+",LESTAMPA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSerial),'PRD_ERRO','LESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LNumErr),'PRD_ERRO','LEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOperaz),'PRD_ERRO','LEOPERAZ');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PRD_ERRO','LEDATOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOra),'PRD_ERRO','LEORAOPE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PRD_ERRO','LECODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LErrore),'PRD_ERRO','LECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOggErr),'PRD_ERRO','LEERRORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMessage),'PRD_ERRO','LEMESSAG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PRD_ERRO','LESTORIC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTAMPA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LESERIAL',this.w_LSerial,'LEROWNUM',this.w_LNumErr,'LEOPERAZ',this.w_LOperaz,'LEDATOPE',i_DATSYS,'LEORAOPE',this.w_LOra,'LECODUTE',i_CODUTE,'LECODICE',this.w_LErrore,'LEERRORE',this.w_LOggErr,'LEMESSAG',this.w_LMessage,'LESTORIC',"S",'LESTAMPA',"N")
      insert into (i_cTable) (LESERIAL,LEROWNUM,LEOPERAZ,LEDATOPE,LEORAOPE,LECODUTE,LECODICE,LEERRORE,LEMESSAG,LESTORIC,LESTAMPA &i_ccchkf. );
         values (;
           this.w_LSerial;
           ,this.w_LNumErr;
           ,this.w_LOperaz;
           ,i_DATSYS;
           ,this.w_LOra;
           ,i_CODUTE;
           ,this.w_LErrore;
           ,this.w_LOggErr;
           ,this.w_LMessage;
           ,"S";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NUMREC = 1
    do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
      this.w_CURTASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
      if this.w_CURTASK.TEXT12 == "M"
        this.w_CURTASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
        AddMsgNL(" - %1",this.w_PADRE,this.w_CURTASK.TEXT11+" "+this.w_CURTASK.TEXT1+" "+this.w_CURTASK.TEXT2+" ("+this.w_CURTASK.TEXT3+")")
        this.w_CURTASK.Delete()     
      endif
      this.w_NUMREC = this.w_NUMREC+1
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='PRD_ERRO'
    this.cWorkTables[3]='PAR_PROD'
    this.cWorkTables[4]='*TMPODL_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSCO1BBM')
      use in _Curs_GSCO1BBM
    endif
    if used('_Curs_GSCO12BBM')
      use in _Curs_GSCO12BBM
    endif
    if used('_Curs_GSCO5BBM')
      use in _Curs_GSCO5BBM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsco_bbm
  function OpenTag
     parameters sTag,rc
     return '<'+alltrim(sTag)+'>'+iif(!rc,chr(13),'')
  endfunc
  
  function CloseTag
     parameters sTag
     return '</'+alltrim(sTag)+'>'+chr(13)
  endfunc
  
  function SetTag
     parameters sTag, sValue
     local ret
     return this.OpenTag(sTag,.t.)+sValue+this.CloseTag(sTag)
  endfunc
  
  function SetTextValues
    PARAMETERS Task
    *** Text1 � un campo calcolato come formula.....
    Task.Text1=this.w_TEXT1
    Task.Text2=this.w_TEXT2
    Task.Text3=this.w_TEXT3
    Task.Text4=this.w_TEXT4
    Task.Text5=this.w_TEXT5
    Task.Text6=this.w_TEXT6
    Task.Text7=this.w_TEXT7
    Task.Text8=this.w_TEXT8
    Task.Text9=this.w_TEXT9
    Task.Text10=this.w_TEXT10
    Task.Text11=this.w_TEXT11
    Task.Text12=this.w_TEXT12
    Task.Text13=this.w_TEXT13
    Task.Text14=this.w_TEXT14
    Task.Text15=this.w_TEXT15
    Task.Text16=this.w_TEXT16
    Task.Text17=this.w_TEXT17
    Task.Text18=this.w_TEXT18
    Task.Text19=this.w_TEXT19
    Task.Text20=this.w_TEXT20
    Task.Text21=this.w_TEXT21
    Task.Text22=this.w_TEXT22
    Task.Text23=this.w_TEXT23
    Task.Text24=this.w_TEXT24
    Task.Text25=this.w_TEXT25
    Task.Text26=this.w_TEXT26
    Task.Text27=this.w_TEXT27
    Task.Text28=this.w_TEXT28
    Task.Text29=this.w_TEXT29
    Task.Text30=i_CODAZI
  endfunc
  
  function SetContrass
    Parameters Task, tipo
    do case
      case tipo=1
        Task.Flag1=.t.
        Task.Flag2=.f.
        Task.Flag3=.f.
        Task.Flag4=.f.
        Task.Flag5=.f.      
      case tipo=2
        Task.Flag1=.f.
        Task.Flag2=.t.
        Task.Flag3=.f.
        Task.Flag4=.f.
        Task.Flag5=.f.      
      case tipo=3
        Task.Flag1=.f.
        Task.Flag2=.f.
        Task.Flag3=.t.
        Task.Flag4=.f.
        Task.Flag5=.f.      
      case tipo=4
        Task.Flag1=.f.
        Task.Flag2=.f.
        Task.Flag3=.f.
        Task.Flag4=.t.
        Task.Flag5=.f.          
      case tipo=5    
        Task.Flag1=.f.
        Task.Flag2=.f.
        Task.Flag3=.f.
        Task.Flag4=.f.
        Task.Flag5=.t.          
    endcase
  endfunc
  
  function CheckFase
  parameters pstring
  local retval
    retval=0
    if USED('ODLCURSOR')
      Select ODLCURSOR
      SET ORDER TO IDX1
      seek pstring
      if found()
        retval=UID
      endif
    endif
    return retval
  endfunc
  
  function CheckDettFase
  parameters pstring
  local retval
    retval=0
    if USED('ODLCURSOR')
      Select ODLCURSOR
      SET ORDER TO IDX1
      If seek(int(val(nvl(pstring, ""))))
        retval=UID
      endif
    endif
    return retval
  endfunc
  
  
  function CheckResource
  parameters pcodice, ptipo
  local retval
    retval=0
    if USED('RESCURSOR')
      Select RESCURSOR
      SET ORDER TO IDX4
      seek pcodice
      if found()
        if ptipo=1
          retval=UID
        else
          retval=ID
        endif
      endif
    endif
    return retval  
  endfunc
  
  function DeleteODL
  parameters serodl
    if USED('CURSORODL')
      delete from CURSODL where ODL=serodl
    endif
  endfunc
  
  function getFather
    parameters codice, livello
    local retval, riskey
    retval=''
    if USED('_TmpCoSiPr_')
      Select _TmpCoSiPr_ 
      Locate for rlcodice=codice
      if found()
        riskey = alltrim(lvlkey)
        Select _TmpCoSiPr_ 
        locate for lvlkey = SUBSTR(riskey,1,AT('.',riskey,livello)-1)
        if found()
          retval = rlcodice
        endif    
      endif 
    endif
  return retval  
  endfunc
  
  
  
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
