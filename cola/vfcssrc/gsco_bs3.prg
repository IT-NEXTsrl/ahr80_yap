* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bs3                                                        *
*              Ritorna progressivo documento                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-08                                                      *
* Last revis.: 2011-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bs3",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsco_bs3 as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_MVANNDOC = space(4)
  w_MVPRD = space(2)
  w_MVALFDOC = space(10)
  w_MVNUMDOC = 0
  w_TIPGES = space(1)
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Torna Progressivo Documento (da GSCL_KGL, GSCO_KGI, GSCL_KGT, GSCO_KGB)
    this.w_TIPGES = SPACE(1)
    if Type("this.oParentObject.w_TIPGES")="C"
      this.w_TIPGES = this.oParentObject.w_TIPGES
    endif
    if this.pTipOpe $ "OD" AND (g_PROD<>"S" OR (g_COLA<>"S" AND this.w_TIPGES="L"))
      ah_ErrorMsg("Modulo non installato, impossibile proseguire",,"")
      this.oParentObject.ecpQuit()
      Keyboard "{ESC}"
      i_retcode = 'stop'
      return
    endif
    if this.pTipOpe $ "BL" AND this.oParentObject.w_GESWIP=" "
      if this.pTipOpe = "B"
        ah_ErrorMsg("Magazzino WIP non gestito, impossibile generare buoni di prelievo",,"")
        this.oParentObject.ecpQuit()
        Keyboard "{ESC}"
      endif
      i_retcode = 'stop'
      return
    endif
    this.w_MVANNDOC = this.oParentObject.w_PPANNDOC
    * --- Se il batch � lanciato da GSCL_KGT -Generazione DDT di trasferimento- se la causale lo prevede adotto la numerazione delle vendite,
    *     altrimenti uso quella collegata al tipo della causale scelta.
    this.w_MVPRD = iif(this.oParentObject.w_FLPDOC<>"S",this.oParentObject.w_PPPRD,"DV")
    this.w_MVALFDOC = this.oParentObject.w_PPALFDOC
    this.w_MVNUMDOC = 0
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_AskTableProg(this, i_Conn, "PRDOC", "i_CODAZI,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    this.oParentObject.w_PPNUMDOC = this.w_MVNUMDOC
  endproc


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
