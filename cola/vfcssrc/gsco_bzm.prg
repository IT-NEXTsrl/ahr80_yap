* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bzm                                                        *
*              Manutenzione rivalorizzazione DDT C/Lavoro                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_61]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-06                                                      *
* Last revis.: 2015-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pSERIAL,pPARAME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bzm",oParentObject,m.pAzione,m.pSERIAL,m.pPARAME)
return(i_retval)

define class tgsco_bzm as StdBatch
  * --- Local variables
  pAzione = space(10)
  pSERIAL = space(10)
  pPARAME = space(3)
  w_PROG = .NULL.
  NC = space(10)
  GSCO_SZM = .NULL.
  I = 0
  w_ZOOM = .NULL.
  w_CHKOK = .f.
  w_NR = 0
  w_CODCONTO = space(15)
  zCursor = space(10)
  tCursor = space(10)
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_IMPNAZ = 0
  w_SERDDT = space(10)
  w_ROWDDT = 0
  w_CODODL = space(15)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_ULTFAS = space(1)
  w_VALMAG = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  TMPMOVIDETT_idx=0
  DOC_MAST_idx=0
  ODL_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamata da GSCO_SZM
    * --- Parametro operazione da eseguire
    * --- Puntatore al padre
    this.GSCO_SZM = this.oParentObject
    this.w_ZOOM = this.GSCO_SZM.w_ZoomDoc
    this.NC = this.w_ZOOM.cCursor
    do case
      case this.pAzione="SEL_DESEL"
        * --- Seleziona/Deseleziona tutte le righe dello zoom
        * --- Nome cursore collegato allo zoom
        if used(this.NC)
          * --- Gestisco l'errore nel caso in cui non riesca a riposizionarmi sul currsore
          *     Caso in cui alla requery ho meno righe
          w_olderr=ON("ERROR")
          w_err =.f.
          ON ERROR w_err=.t.
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET XCHK=0
          endif
          ON ERROR &w_olderr
        endif
      case this.pAzione="UPD"
        if used(this.NC)
          * --- Try
          local bErr_0252D5A0
          bErr_0252D5A0=bTrsErr
          this.Try_0252D5A0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore durante l'aggiornamento dei dati: operazione annullata","STOP","")
          endif
          bTrsErr=bTrsErr or bErr_0252D5A0
          * --- End
          if used ("dtcl")
            use in dtcl
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_SELEZI = "D"
        endif
      case this.pAzione="DON"
        * --- Drop temporary table TMPMOVIDETT
        i_nIdx=cp_GetTableDefIdx('TMPMOVIDETT')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPMOVIDETT')
        endif
      case this.pAzione = "VISDETT"
        this.w_PROG = .NULL.
        if RIGHT(this.pPARAME, 2) $ "DT-FA" and LEFT(this.pPARAME,1)="A"
          this.w_PROG = GSAC_MDV(RIGHT(this.pPARAME, 2))
        endif
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_MVSERIAL = this.pSERIAL
        this.w_PROG.QueryKeySet("MVSERIAL='"+this.pSERIAL+ "'","")     
        * --- mi metto in interrogazione
        this.w_PROG.LoadRec()     
      case this.pAzione = "DEL"
        if used(this.NC)
          select (this.NC)
          ZAP
        endif
      case this.pAzione="RICERCA"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_SELEZI = "D"
    endcase
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc
  proc Try_0252D5A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    select SERIAL, CPROWNUM, IMPNAZFA from (this.NC) into Cursor dtcl where xchk=1
    select dtcl
    go top
    SCAN
    this.w_SERIAL = NVL(SERIAL, SPACE(10))
    this.w_CPROWNUM = NVL(CPROWNUM,0)
    this.w_IMPNAZ = NVL(IMPNAZFA, 0)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
      +",MVFLRVCL ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_DETT','MVFLRVCL');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MVFLRVCL = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          MVIMPNAZ = this.w_IMPNAZ;
          ,MVFLRVCL = "N";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERIAL;
          and CPROWNUM = this.w_CPROWNUM;
          and MVFLRVCL = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with 'GSCO9SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLBLOC ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLBLOC');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
      +"DOC_MAST.MVFLBLOC ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLBLOC');
          +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
          +"MVFLBLOC";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLBLOC')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
      +"MVFLBLOC ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLBLOC');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLBLOC ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLBLOC');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if g_PRFA="S" AND g_CICLILAV="S"
      * --- Sull'ultima fase devo aggiornare il codice del padre (Documento interno associato al DDT)
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERRIF,MVROWRIF"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERRIF,MVROWRIF;
          from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_CPROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERRIF = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
        this.w_ROWRIF = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODODL"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODODL;
          from (i_cTable) where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODODL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTULFAS"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTULFAS;
          from (i_cTable) where;
              OLCODODL = this.w_CODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ULTFAS = NVL(cp_ToDate(_read_.OLTULFAS),cp_NullValue(_read_.OLTULFAS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NVL(this.w_ULTFAS,"N")="S"
        this.w_SERDDT = space(10)
        this.w_ROWDDT = 0
        * --- Dovrebbe restituirmi una sola riga
        vq_exec("..\COLA\EXE\QUERY\GSCL_BRC.VQR",this,"SWITCH")
        if USED("SWITCH")
          SELECT SWITCH
          if RECCOUNT()>0
            SELECT SWITCH
            GO TOP
            SCAN FOR NOT EMPTY(NVL(MVSERIAL,"")) AND NVL(CPROWNUM, 0)<>0
            this.w_SERDDT = MVSERIAL
            this.w_ROWDDT = CPROWNUM
            this.w_VALMAG = NVL(MVVALMAG,0)
            ENDSCAN
          endif
          SELECT SWITCH
          USE
        endif
        if NOT EMPTY(NVL(this.w_SERDDT,"")) AND NVL(this.w_ROWDDT, 0)<>0
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'DOC_DETT','MVIMPNAZ');
            +",MVFLRVCL ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_DETT','MVFLRVCL');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDDT);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWDDT);
                   )
          else
            update (i_cTable) set;
                MVIMPNAZ = this.w_VALMAG;
                ,MVFLRVCL = "N";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERDDT;
                and CPROWNUM = this.w_ROWDDT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura DOC_DETT (DDT)'
            return
          endif
        endif
      endif
    endif
    select dtcl
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione completata con successo","i","")
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ah_Msg("Preparazione dati in corso...",.T.)
    * --- Create temporary table TMPMOVIDETT
    i_nIdx=cp_AddTableDef('TMPMOVIDETT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\COLA\EXE\QUERY\GSCO2SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPMOVIDETT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Nella Write effettuo i calcoli tenendo conto dell'indetraibilitÓ dell'IVA
    *     e delle evntuali Ripartizioni Spese
    * --- Write into TMPMOVIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMOVIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIDETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERIALE,CPROWNUM"
      do vq_exec with 'GSCO4SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIDETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPMOVIDETT.SERIALE = _t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPNAZOR = _t2.MVIMPNAZ";
          +i_ccchkf;
          +" from "+i_cTable+" TMPMOVIDETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPMOVIDETT.SERIALE = _t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIDETT, "+i_cQueryTable+" _t2 set ";
          +"TMPMOVIDETT.IMPNAZOR = _t2.MVIMPNAZ";
          +Iif(Empty(i_ccchkf),"",",TMPMOVIDETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPMOVIDETT.SERIALE = t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIDETT set (";
          +"IMPNAZOR";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVIMPNAZ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPMOVIDETT.SERIALE = _t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIDETT set ";
          +"IMPNAZOR = _t2.MVIMPNAZ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIALE = "+i_cQueryTable+".SERIALE";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPNAZOR = (select MVIMPNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TMPMOVIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMOVIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIDETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERIALE,CPROWNUM"
      do vq_exec with 'GSCO6SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIDETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPMOVIDETT.SERIALE = _t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPNAZOR = TMPMOVIDETT.IMPNAZOR+_t2.MVIMPNAZ";
          +i_ccchkf;
          +" from "+i_cTable+" TMPMOVIDETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPMOVIDETT.SERIALE = _t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIDETT, "+i_cQueryTable+" _t2 set ";
          +"TMPMOVIDETT.IMPNAZOR = TMPMOVIDETT.IMPNAZOR+_t2.MVIMPNAZ";
          +Iif(Empty(i_ccchkf),"",",TMPMOVIDETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPMOVIDETT.SERIALE = t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIDETT set (";
          +"IMPNAZOR";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"IMPNAZOR+t2.MVIMPNAZ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPMOVIDETT.SERIALE = _t2.SERIALE";
              +" and "+"TMPMOVIDETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIDETT set ";
          +"IMPNAZOR = TMPMOVIDETT.IMPNAZOR+_t2.MVIMPNAZ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIALE = "+i_cQueryTable+".SERIALE";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPNAZOR = (select "+i_cTable+".IMPNAZOR+MVIMPNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_ZOOM.cCpQueryName = "..\COLA\EXE\QUERY\GSCO5SZM"
    this.GSCO_SZM.notifyevent("Esegui")     
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione,pSERIAL,pPARAME)
    this.pAzione=pAzione
    this.pSERIAL=pSERIAL
    this.pPARAME=pPARAME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='*TMPMOVIDETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='ODL_MAST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pSERIAL,pPARAME"
endproc
