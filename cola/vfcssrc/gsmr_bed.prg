* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bed                                                        *
*              Filtri distinta base                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-05-21                                                      *
* Last revis.: 2016-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bed",oParentObject)
return(i_retval)

define class tgsmr_bed as StdBatch
  * --- Local variables
  w_FILDOC = space(1)
  w_SUGG = space(1)
  Padre = .NULL.
  Nonno = .NULL.
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(3)
  w_FILSTAT = space(1)
  w_DATFIL = ctod("  /  /  ")
  w_TIPART = space(20)
  w_PROPRE = space(10)
  w_LIVELLO = 0
  w_ROWS = 0
  w_ERR = space(1)
  w_EXIT = .f.
  w_PROVEI = space(10)
  w_PROVEL = space(10)
  w_PROVEE = space(10)
  w_TIPARTPF = space(20)
  w_TIPARTSE = space(20)
  w_TIPARTPH = space(20)
  w_TIPARTMP = space(20)
  * --- WorkFile variables
  ART_TEMP_idx=0
  TMPEXPDB_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.Padre = this.oParentObject
    this.Nonno = this.Padre.oParentObject
    this.w_DBCODINI = SPACE(20)
    this.w_DBCODFIN = SPACE(20)
    this.w_FILSTAT = " "
    this.w_DATFIL = i_DATSYS
    this.w_VERIFICA = "ST"
    this.w_MAXLEVEL = 999
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_LIVELLO = 0
    USE IN SELECT("ELABREC")
    ah_msg("Esplosione livello 0 in corso ...", .t., .t.)
    * --- Create temporary table TMPEXPDB
    i_nIdx=cp_AddTableDef('TMPEXPDB') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\COLA\EXE\QUERY\GSMRABED',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPEXPDB_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_ROWS = 1
    do while this.w_ROWS > 0
      * --- Try
      local bErr_02C7C1D0
      bErr_02C7C1D0=bTrsErr
      this.Try_02C7C1D0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_ERR = MESSAGE()
        this.w_EXIT = .T.
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02C7C1D0
      * --- End
      this.w_LIVELLO = this.w_LIVELLO + 1
      * --- Sicurezza al livello 40 esco dal ciclo
      if this.w_LIVELLO = 40
        this.w_VERIFICA = "ZZZ"
        this.w_EXIT = .T.
      endif
      if this.w_EXIT Or bTrsErr
        exit
      endif
    enddo
    if this.w_VERIFICA="ZZZ"
      if TYPE("this.Padre.w_VERIFICA")="C"
        this.Padre.w_VERIFICA = this.w_VERIFICA
      endif
      * --- Drop temporary table TMPEXPDB
      i_nIdx=cp_GetTableDefIdx('TMPEXPDB')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPEXPDB')
      endif
      * --- Errore in fase di esplosione
      i_retcode = 'stop'
      return
    endif
    if (this.Padre.w_GENPODA="O" or (this.Padre.w_GENPODA="S" and this.Padre.w_SELEZ="S")) and not empty(this.Nonno.w_CRITFORN)
      * --- Inserisce nella tabella ART_TEMP tutti gli articoli delle distinte
      this.w_PROVEI = "I"
      this.w_PROVEL = "L"
      this.w_PROVEE = "E"
      this.w_TIPARTPF = "PF"
      this.w_TIPARTSE = "SE"
      this.w_TIPARTPH = "PH"
      this.w_TIPARTMP = "MP"
    else
      * --- Inserisce nella tabella ART_TEMP gli articoli interni (semilavorati) delle distinte
      this.w_PROVEI = "I"
      this.w_PROVEL = "L"
      this.w_PROVEE = " "
      this.w_TIPARTPF = "PF"
      this.w_TIPARTSE = "SE"
      this.w_TIPARTPH = "PH"
      this.w_TIPARTMP = "  "
    endif
    * --- Try
    local bErr_02C7FBF0
    bErr_02C7FBF0=bTrsErr
    this.Try_02C7FBF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_02C7FBF0
    * --- End
    * --- Drop temporary table TMPEXPDB
    i_nIdx=cp_GetTableDefIdx('TMPEXPDB')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPEXPDB')
    endif
  endproc
  proc Try_02C7C1D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPEXPDB
    i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRBBED",this.TMPEXPDB_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_ROWS = i_ROWS
    if this.w_ROWS <= 0
      this.w_EXIT = .T.
    endif
    ah_msg("Esplosione livello %1 %0 Righe esplose %2...",.t.,.t.,.f.,alltrim(str(this.w_LIVELLO+1)) , alltrim(str(this.w_ROWS)) )
    return
  proc Try_02C7FBF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMRCBED",this.ART_TEMP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='*TMPEXPDB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
