* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mcs                                                        *
*              Ciclo semplificato                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-11-03                                                      *
* Last revis.: 2015-10-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mcs")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mcs")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mcs")
  return

* --- Class definition
define class tgsco_mcs as StdPCForm
  Width  = 852
  Height = 450
  Top    = 10
  Left   = 10
  cComment = "Ciclo semplificato"
  cPrg = "gsco_mcs"
  HelpContextID=97101161
  add object cnt as tcgsco_mcs
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mcs as PCContext
  w_CPROWORD = 0
  w_SMDESFAS = space(40)
  w_SM_SETUP = space(1)
  w_SMCODRIS = space(15)
  w_DESRIS = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CODVAL = space(3)
  w_PREZZO = 0
  w_CAOVAL = 0
  w_PRENAZ = 0
  w_SMUNIMIS = space(3)
  w_SMQTAMOV = 0
  w_PREUM = 0
  w_CALCPICT = 0
  w_CODREP = space(15)
  w_DESREP = space(40)
  w_SMCODODL = space(15)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_FLFRAZ = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_OPERA3 = space(1)
  w_SMQTAMO1 = 0
  w_QTAODL = 0
  w_TIPREP = space(2)
  proc Save(i_oFrom)
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_SMDESFAS = i_oFrom.w_SMDESFAS
    this.w_SM_SETUP = i_oFrom.w_SM_SETUP
    this.w_SMCODRIS = i_oFrom.w_SMCODRIS
    this.w_DESRIS = i_oFrom.w_DESRIS
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_PREZZO = i_oFrom.w_PREZZO
    this.w_CAOVAL = i_oFrom.w_CAOVAL
    this.w_PRENAZ = i_oFrom.w_PRENAZ
    this.w_SMUNIMIS = i_oFrom.w_SMUNIMIS
    this.w_SMQTAMOV = i_oFrom.w_SMQTAMOV
    this.w_PREUM = i_oFrom.w_PREUM
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_CODREP = i_oFrom.w_CODREP
    this.w_DESREP = i_oFrom.w_DESREP
    this.w_SMCODODL = i_oFrom.w_SMCODODL
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_FLFRAZ = i_oFrom.w_FLFRAZ
    this.w_NOFRAZ = i_oFrom.w_NOFRAZ
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_SMQTAMO1 = i_oFrom.w_SMQTAMO1
    this.w_QTAODL = i_oFrom.w_QTAODL
    this.w_TIPREP = i_oFrom.w_TIPREP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_SMDESFAS = this.w_SMDESFAS
    i_oTo.w_SM_SETUP = this.w_SM_SETUP
    i_oTo.w_SMCODRIS = this.w_SMCODRIS
    i_oTo.w_DESRIS = this.w_DESRIS
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_PREZZO = this.w_PREZZO
    i_oTo.w_CAOVAL = this.w_CAOVAL
    i_oTo.w_PRENAZ = this.w_PRENAZ
    i_oTo.w_SMUNIMIS = this.w_SMUNIMIS
    i_oTo.w_SMQTAMOV = this.w_SMQTAMOV
    i_oTo.w_PREUM = this.w_PREUM
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_CODREP = this.w_CODREP
    i_oTo.w_DESREP = this.w_DESREP
    i_oTo.w_SMCODODL = this.w_SMCODODL
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_FLFRAZ = this.w_FLFRAZ
    i_oTo.w_NOFRAZ = this.w_NOFRAZ
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_SMQTAMO1 = this.w_SMQTAMO1
    i_oTo.w_QTAODL = this.w_QTAODL
    i_oTo.w_TIPREP = this.w_TIPREP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mcs as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 852
  Height = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-06"
  HelpContextID=97101161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ODL_SMPL_IDX = 0
  ODL_MAST_IDX = 0
  TAB_RISO_IDX = 0
  RIS_ORSE_IDX = 0
  UNIMIS_IDX = 0
  cFile = "ODL_SMPL"
  cKeySelect = "SMCODODL"
  cKeyWhere  = "SMCODODL=this.w_SMCODODL"
  cKeyDetail  = "SMCODODL=this.w_SMCODODL"
  cKeyWhereODBC = '"SMCODODL="+cp_ToStrODBC(this.w_SMCODODL)';

  cKeyDetailWhereODBC = '"SMCODODL="+cp_ToStrODBC(this.w_SMCODODL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ODL_SMPL.SMCODODL="+cp_ToStrODBC(this.w_SMCODODL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ODL_SMPL.CPROWORD'
  cPrg = "gsco_mcs"
  cComment = "Ciclo semplificato"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPROWORD = 0
  w_SMDESFAS = space(40)
  w_SM_SETUP = space(1)
  w_SMCODRIS = space(15)
  o_SMCODRIS = space(15)
  w_DESRIS = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CODVAL = space(3)
  w_PREZZO = 0
  w_CAOVAL = 0
  w_PRENAZ = 0
  w_SMUNIMIS = space(3)
  o_SMUNIMIS = space(3)
  w_SMQTAMOV = 0
  o_SMQTAMOV = 0
  w_PREUM = 0
  w_CALCPICT = 0
  w_CODREP = space(15)
  w_DESREP = space(40)
  w_SMCODODL = space(15)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_FLFRAZ = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_OPERA3 = space(1)
  w_SMQTAMO1 = 0
  w_QTAODL = 0
  w_TIPREP = space(2)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mcsPag1","gsco_mcs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='TAB_RISO'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='ODL_SMPL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ODL_SMPL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ODL_SMPL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mcs'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ODL_SMPL where SMCODODL=KeySet.SMCODODL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_SMPL_IDX,2],this.bLoadRecFilter,this.ODL_SMPL_IDX,"gsco_mcs")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ODL_SMPL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ODL_SMPL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ODL_SMPL '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SMCODODL',this.w_SMCODODL  )
      select * from (i_cTable) ODL_SMPL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPREP = 'RE'
        .w_CALCPICT = DEFPIU(g_PERPUL)
        .w_SMCODODL = NVL(SMCODODL,space(15))
        .w_QTAODL = this.oParentObject.w_OLTQTODL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ODL_SMPL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESRIS = space(40)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_CODVAL = space(3)
          .w_PREZZO = 0
          .w_DESREP = space(40)
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_FLFRAZ = space(1)
          .w_NOFRAZ = space(1)
          .w_MODUM2 = space(1)
          .w_FLUSEP = space(1)
          .w_OPERA3 = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_SMDESFAS = NVL(SMDESFAS,space(40))
          .w_SM_SETUP = NVL(SM_SETUP,space(1))
          .w_SMCODRIS = NVL(SMCODRIS,space(15))
          if link_2_4_joined
            this.w_SMCODRIS = NVL(RICODICE204,NVL(this.w_SMCODRIS,space(15)))
            this.w_DESRIS = NVL(RIDESCRI204,space(40))
            this.w_UNMIS1 = NVL(RIUNMIS1204,space(3))
            this.w_UNMIS2 = NVL(RIUNMIS2204,space(3))
            this.w_OPERAT = NVL(RIOPERAT204,space(1))
            this.w_MOLTIP = NVL(RIMOLTIP204,0)
            this.w_PREZZO = NVL(RIPREZZO204,0)
            this.w_CODVAL = NVL(RICODVAL204,space(3))
            this.w_CODREP = NVL(RICODREP204,space(15))
          else
          .link_2_4('Load')
          endif
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
          .w_SMUNIMIS = NVL(SMUNIMIS,space(3))
          if link_2_14_joined
            this.w_SMUNIMIS = NVL(UMCODICE214,NVL(this.w_SMUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ214,space(1))
          else
          .link_2_14('Load')
          endif
          .w_SMQTAMOV = NVL(SMQTAMOV,0)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_SMUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_CODREP = .w_CODREP
          .link_2_18('Load')
          .link_2_23('Load')
          .w_SMQTAMO1 = NVL(SMQTAMO1,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_QTAODL = this.oParentObject.w_OLTQTODL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CPROWORD=10
      .w_SMDESFAS=space(40)
      .w_SM_SETUP=space(1)
      .w_SMCODRIS=space(15)
      .w_DESRIS=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_CODVAL=space(3)
      .w_PREZZO=0
      .w_CAOVAL=0
      .w_PRENAZ=0
      .w_SMUNIMIS=space(3)
      .w_SMQTAMOV=0
      .w_PREUM=0
      .w_CALCPICT=0
      .w_CODREP=space(15)
      .w_DESREP=space(40)
      .w_SMCODODL=space(15)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_FLFRAZ=space(1)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_OPERA3=space(1)
      .w_SMQTAMO1=0
      .w_QTAODL=0
      .w_TIPREP=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_SMCODRIS))
         .link_2_4('Full')
        endif
        .DoRTCalc(5,11,.f.)
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        .w_SMUNIMIS = .w_UNMIS1
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_SMUNIMIS))
         .link_2_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_SMUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_CALCPICT = DEFPIU(g_PERPUL)
        .w_CODREP = .w_CODREP
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODREP))
         .link_2_18('Full')
        endif
        .DoRTCalc(19,23,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_23('Full')
        endif
        .DoRTCalc(24,31,.f.)
        .w_SMQTAMO1 = CALQTAADV(.w_SMQTAMOV,.w_SMUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "SMQTAMOV")
        .w_QTAODL = this.oParentObject.w_OLTQTODL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_TIPREP = 'RE'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ODL_SMPL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ODL_SMPL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMCODODL,"SMCODODL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_SMDESFAS C(40);
      ,t_SM_SETUP N(3);
      ,t_SMCODRIS C(15);
      ,t_DESRIS C(40);
      ,t_SMUNIMIS C(3);
      ,t_SMQTAMOV N(12,3);
      ,t_CODREP C(15);
      ,t_DESREP C(40);
      ,CPROWNUM N(10);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_CODVAL C(3);
      ,t_PREZZO N(18,5);
      ,t_CAOVAL N(12,7);
      ,t_PRENAZ N(18,5);
      ,t_PREUM N(18,5);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_FLFRAZ C(1);
      ,t_NOFRAZ C(1);
      ,t_MODUM2 C(1);
      ,t_FLUSEP C(1);
      ,t_OPERA3 C(1);
      ,t_SMQTAMO1 N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mcsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMDESFAS_2_2.controlsource=this.cTrsName+'.t_SMDESFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSM_SETUP_2_3.controlsource=this.cTrsName+'.t_SM_SETUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODRIS_2_4.controlsource=this.cTrsName+'.t_SMCODRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_5.controlsource=this.cTrsName+'.t_DESRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMUNIMIS_2_14.controlsource=this.cTrsName+'.t_SMUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMQTAMOV_2_15.controlsource=this.cTrsName+'.t_SMQTAMOV'
    this.oPgFRm.Page1.oPag.oCODREP_2_18.controlsource=this.cTrsName+'.t_CODREP'
    this.oPgFRm.Page1.oPag.oDESREP_2_19.controlsource=this.cTrsName+'.t_DESREP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(50)
    this.AddVLine(341)
    this.AddVLine(366)
    this.AddVLine(490)
    this.AddVLine(703)
    this.AddVLine(747)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_SMPL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_SMPL_IDX,2])
      *
      * insert into ODL_SMPL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ODL_SMPL')
        i_extval=cp_InsertValODBCExtFlds(this,'ODL_SMPL')
        i_cFldBody=" "+;
                  "(CPROWORD,SMDESFAS,SM_SETUP,SMCODRIS,SMUNIMIS"+;
                  ",SMQTAMOV,SMCODODL,SMQTAMO1,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_SMDESFAS)+","+cp_ToStrODBC(this.w_SM_SETUP)+","+cp_ToStrODBCNull(this.w_SMCODRIS)+","+cp_ToStrODBCNull(this.w_SMUNIMIS)+;
             ","+cp_ToStrODBC(this.w_SMQTAMOV)+","+cp_ToStrODBC(this.w_SMCODODL)+","+cp_ToStrODBC(this.w_SMQTAMO1)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ODL_SMPL')
        i_extval=cp_InsertValVFPExtFlds(this,'ODL_SMPL')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SMCODODL',this.w_SMCODODL)
        INSERT INTO (i_cTable) (;
                   CPROWORD;
                  ,SMDESFAS;
                  ,SM_SETUP;
                  ,SMCODRIS;
                  ,SMUNIMIS;
                  ,SMQTAMOV;
                  ,SMCODODL;
                  ,SMQTAMO1;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CPROWORD;
                  ,this.w_SMDESFAS;
                  ,this.w_SM_SETUP;
                  ,this.w_SMCODRIS;
                  ,this.w_SMUNIMIS;
                  ,this.w_SMQTAMOV;
                  ,this.w_SMCODODL;
                  ,this.w_SMQTAMO1;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_SMPL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 and !Empty(t_SMCODRIS)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_SMPL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_SMPL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and !Empty(t_SMCODRIS)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ODL_SMPL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_SMPL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SMDESFAS="+cp_ToStrODBC(this.w_SMDESFAS)+;
                     ",SM_SETUP="+cp_ToStrODBC(this.w_SM_SETUP)+;
                     ",SMCODRIS="+cp_ToStrODBCNull(this.w_SMCODRIS)+;
                     ",SMUNIMIS="+cp_ToStrODBCNull(this.w_SMUNIMIS)+;
                     ",SMQTAMOV="+cp_ToStrODBC(this.w_SMQTAMOV)+;
                     ",SMQTAMO1="+cp_ToStrODBC(this.w_SMQTAMO1)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_SMPL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SMDESFAS=this.w_SMDESFAS;
                     ,SM_SETUP=this.w_SM_SETUP;
                     ,SMCODRIS=this.w_SMCODRIS;
                     ,SMUNIMIS=this.w_SMUNIMIS;
                     ,SMQTAMOV=this.w_SMQTAMOV;
                     ,SMQTAMO1=this.w_SMQTAMO1;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_SMPL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and !Empty(t_SMCODRIS)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ODL_SMPL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and !Empty(t_SMCODRIS)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODL_SMPL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_SMPL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_SMCODRIS<>.w_SMCODRIS
          .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        endif
        if .o_SMCODRIS<>.w_SMCODRIS
          .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        endif
        if .o_SMCODRIS<>.w_SMCODRIS
          .w_SMUNIMIS = .w_UNMIS1
          .link_2_14('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_SMCODRIS<>.w_SMCODRIS.or. .o_SMUNIMIS<>.w_SMUNIMIS
          .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_SMUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        endif
          .w_CALCPICT = DEFPIU(g_PERPUL)
          .w_CODREP = .w_CODREP
          .link_2_18('Full')
        .DoRTCalc(19,22,.t.)
          .link_2_23('Full')
        .DoRTCalc(24,31,.t.)
        if .o_SMQTAMOV<>.w_SMQTAMOV.or. .o_SMUNIMIS<>.w_SMUNIMIS
          .w_SMQTAMO1 = CALQTAADV(.w_SMQTAMOV,.w_SMUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "SMQTAMOV")
        endif
          .w_QTAODL = this.oParentObject.w_OLTQTODL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_CODVAL with this.w_CODVAL
      replace t_PREZZO with this.w_PREZZO
      replace t_CAOVAL with this.w_CAOVAL
      replace t_PRENAZ with this.w_PRENAZ
      replace t_PREUM with this.w_PREUM
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_NOFRAZ with this.w_NOFRAZ
      replace t_MODUM2 with this.w_MODUM2
      replace t_FLUSEP with this.w_FLUSEP
      replace t_OPERA3 with this.w_OPERA3
      replace t_SMQTAMO1 with this.w_SMQTAMO1
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSMUNIMIS_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSMUNIMIS_2_14.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SMCODRIS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_lTable = "TAB_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2], .t., this.TAB_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ARI',True,'TAB_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_SMCODRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_SMCODRIS))
          select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMCODRIS)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMCODRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_RISO','*','RICODICE',cp_AbsName(oSource.parent,'oSMCODRIS_2_4'),i_cWhere,'GSDS_ARI',"Elenco risorse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_SMCODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_SMCODRIS)
            select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMCODRIS = NVL(_Link_.RICODICE,space(15))
      this.w_DESRIS = NVL(_Link_.RIDESCRI,space(40))
      this.w_UNMIS1 = NVL(_Link_.RIUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.RIUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.RIOPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.RIMOLTIP,0)
      this.w_PREZZO = NVL(_Link_.RIPREZZO,0)
      this.w_CODVAL = NVL(_Link_.RICODVAL,space(3))
      this.w_CODREP = NVL(_Link_.RICODREP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_SMCODRIS = space(15)
      endif
      this.w_DESRIS = space(40)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_PREZZO = 0
      this.w_CODVAL = space(3)
      this.w_CODREP = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_RISO_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.RICODICE as RICODICE204"+ ",link_2_4.RIDESCRI as RIDESCRI204"+ ",link_2_4.RIUNMIS1 as RIUNMIS1204"+ ",link_2_4.RIUNMIS2 as RIUNMIS2204"+ ",link_2_4.RIOPERAT as RIOPERAT204"+ ",link_2_4.RIMOLTIP as RIMOLTIP204"+ ",link_2_4.RIPREZZO as RIPREZZO204"+ ",link_2_4.RICODVAL as RICODVAL204"+ ",link_2_4.RICODREP as RICODREP204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on ODL_SMPL.SMCODRIS=link_2_4.RICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and ODL_SMPL.SMCODRIS=link_2_4.RICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SMUNIMIS
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_SMUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_SMUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oSMUNIMIS_2_14'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSDS_MCS.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_SMUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_SMUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SMUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_SMUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_SMUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.UMCODICE as UMCODICE214"+ ",link_2_14.UMFLFRAZ as UMFLFRAZ214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on ODL_SMPL.SMUNIMIS=link_2_14.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and ODL_SMPL.SMUNIMIS=link_2_14.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODREP
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODREP);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPREP;
                       ,'RLCODICE',this.w_CODREP)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODREP = NVL(_Link_.RLCODICE,space(15))
      this.w_DESREP = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODREP = space(15)
      endif
      this.w_DESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCODREP_2_18.value==this.w_CODREP)
      this.oPgFrm.Page1.oPag.oCODREP_2_18.value=this.w_CODREP
      replace t_CODREP with this.oPgFrm.Page1.oPag.oCODREP_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_2_19.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_2_19.value=this.w_DESREP
      replace t_DESREP with this.oPgFrm.Page1.oPag.oDESREP_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMDESFAS_2_2.value==this.w_SMDESFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMDESFAS_2_2.value=this.w_SMDESFAS
      replace t_SMDESFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMDESFAS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSM_SETUP_2_3.RadioValue()==this.w_SM_SETUP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSM_SETUP_2_3.SetRadio()
      replace t_SM_SETUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSM_SETUP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODRIS_2_4.value==this.w_SMCODRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODRIS_2_4.value=this.w_SMCODRIS
      replace t_SMCODRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODRIS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_5.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_5.value=this.w_DESRIS
      replace t_DESRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMUNIMIS_2_14.value==this.w_SMUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMUNIMIS_2_14.value=this.w_SMUNIMIS
      replace t_SMUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMUNIMIS_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMQTAMOV_2_15.value==this.w_SMQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMQTAMOV_2_15.value=this.w_SMQTAMOV
      replace t_SMQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMQTAMOV_2_15.value
    endif
    cp_SetControlsValueExtFlds(this,'ODL_SMPL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_SMUNIMIS) or not(CHKUNIMI(.w_SMUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (NOT EMPTY(.w_SMCODRIS)) and (.w_CPROWORD<>0 and !Empty(.w_SMCODRIS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMUNIMIS_2_14
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      if .w_CPROWORD<>0 and !Empty(.w_SMCODRIS)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SMCODRIS = this.w_SMCODRIS
    this.o_SMUNIMIS = this.w_SMUNIMIS
    this.o_SMQTAMOV = this.w_SMQTAMOV
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and !Empty(t_SMCODRIS))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_SMDESFAS=space(40)
      .w_SM_SETUP=space(1)
      .w_SMCODRIS=space(15)
      .w_DESRIS=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_CODVAL=space(3)
      .w_PREZZO=0
      .w_CAOVAL=0
      .w_PRENAZ=0
      .w_SMUNIMIS=space(3)
      .w_SMQTAMOV=0
      .w_PREUM=0
      .w_CODREP=space(15)
      .w_DESREP=space(40)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_FLFRAZ=space(1)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_OPERA3=space(1)
      .w_SMQTAMO1=0
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_SMCODRIS))
        .link_2_4('Full')
      endif
      .DoRTCalc(5,11,.f.)
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        .w_SMUNIMIS = .w_UNMIS1
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_SMUNIMIS))
        .link_2_14('Full')
      endif
      .DoRTCalc(15,15,.f.)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_SMUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
      .DoRTCalc(17,17,.f.)
        .w_CODREP = .w_CODREP
      .DoRTCalc(18,18,.f.)
      if not(empty(.w_CODREP))
        .link_2_18('Full')
      endif
      .DoRTCalc(19,23,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_23('Full')
      endif
      .DoRTCalc(24,31,.f.)
        .w_SMQTAMO1 = CALQTAADV(.w_SMQTAMOV,.w_SMUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "SMQTAMOV")
    endwith
    this.DoRTCalc(33,34,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SMDESFAS = t_SMDESFAS
    this.w_SM_SETUP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSM_SETUP_2_3.RadioValue(.t.)
    this.w_SMCODRIS = t_SMCODRIS
    this.w_DESRIS = t_DESRIS
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_CODVAL = t_CODVAL
    this.w_PREZZO = t_PREZZO
    this.w_CAOVAL = t_CAOVAL
    this.w_PRENAZ = t_PRENAZ
    this.w_SMUNIMIS = t_SMUNIMIS
    this.w_SMQTAMOV = t_SMQTAMOV
    this.w_PREUM = t_PREUM
    this.w_CODREP = t_CODREP
    this.w_DESREP = t_DESREP
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_FLFRAZ = t_FLFRAZ
    this.w_NOFRAZ = t_NOFRAZ
    this.w_MODUM2 = t_MODUM2
    this.w_FLUSEP = t_FLUSEP
    this.w_OPERA3 = t_OPERA3
    this.w_SMQTAMO1 = t_SMQTAMO1
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SMDESFAS with this.w_SMDESFAS
    replace t_SM_SETUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSM_SETUP_2_3.ToRadio()
    replace t_SMCODRIS with this.w_SMCODRIS
    replace t_DESRIS with this.w_DESRIS
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_CODVAL with this.w_CODVAL
    replace t_PREZZO with this.w_PREZZO
    replace t_CAOVAL with this.w_CAOVAL
    replace t_PRENAZ with this.w_PRENAZ
    replace t_SMUNIMIS with this.w_SMUNIMIS
    replace t_SMQTAMOV with this.w_SMQTAMOV
    replace t_PREUM with this.w_PREUM
    replace t_CODREP with this.w_CODREP
    replace t_DESREP with this.w_DESREP
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_NOFRAZ with this.w_NOFRAZ
    replace t_MODUM2 with this.w_MODUM2
    replace t_FLUSEP with this.w_FLUSEP
    replace t_OPERA3 with this.w_OPERA3
    replace t_SMQTAMO1 with this.w_SMQTAMO1
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mcsPag1 as StdContainer
  Width  = 848
  height = 450
  stdWidth  = 848
  stdheight = 450
  resizeXpos=155
  resizeYpos=196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=10, width=832,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Fase",Field2="SMDESFAS",Label2="Descrizione operazione",Field3="SM_SETUP",Label3="Setup",Field4="SMCODRIS",Label4="Risorsa",Field5="DESRIS",Label5="Descrizione risorsa",Field6="SMUNIMIS",Label6="U.M.",Field7="SMQTAMOV",Label7="Quantit�",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218989690

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=29,;
    width=829+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=30,width=828+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TAB_RISO|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCODREP_2_18.Refresh()
      this.Parent.oDESREP_2_19.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TAB_RISO'
        oDropInto=this.oBodyCol.oRow.oSMCODRIS_2_4
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oSMUNIMIS_2_14
    endcase
    return(oDropInto)
  EndFunc


  add object oCODREP_2_18 as StdTrsField with uid="NMAGVORIYH",rtseq=18,rtrep=.t.,;
    cFormVar="w_CODREP",value=space(15),enabled=.f.,;
    ToolTipText = "Codice reparto associato alla risorsa",;
    HelpContextID = 19075546,;
    cTotal="", bFixedPos=.t., cQueryName = "CODREP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=108, Top=418, InputMask=replicate('X',15), cLinkFile="RIS_ORSE", cZoomOnZoom="GSDB_ARL", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPREP", oKey_2_1="RLCODICE", oKey_2_2="this.w_CODREP"

  func oCODREP_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESREP_2_19 as StdTrsField with uid="VOHRRCCFNX",rtseq=19,rtrep=.t.,;
    cFormVar="w_DESREP",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione reparto",;
    HelpContextID = 19016650,;
    cTotal="", bFixedPos=.t., cQueryName = "DESREP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=228, Top=418, InputMask=replicate('X',40)

  add object oStr_2_20 as StdString with uid="XHBVODZMXB",Visible=.t., Left=10, Top=420,;
    Alignment=1, Width=93, Height=16,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsco_mcsBodyRow as CPBodyRowCnt
  Width=819
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="YIKODDOLMS",rtseq=1,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza temporale di elaborazione del ciclo semplificato",;
    HelpContextID = 251318122,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=40, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oSMDESFAS_2_2 as StdTrsField with uid="OBPICASBEI",rtseq=2,rtrep=.t.,;
    cFormVar="w_SMDESFAS",value=space(40),;
    HelpContextID = 173019783,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=286, Left=43, Top=0, InputMask=replicate('X',40)

  add object oSM_SETUP_2_3 as StdTrsCheck with uid="YKIEGKTGBO",rtrep=.t.,;
    cFormVar="w_SM_SETUP",  caption="",;
    ToolTipText = "Se attivo: la fase si intende a costo fisso",;
    HelpContextID = 48209270,;
    Left=333, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oSM_SETUP_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SM_SETUP,&i_cF..t_SM_SETUP),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oSM_SETUP_2_3.GetRadio()
    this.Parent.oContained.w_SM_SETUP = this.RadioValue()
    return .t.
  endfunc

  func oSM_SETUP_2_3.ToRadio()
    this.Parent.oContained.w_SM_SETUP=trim(this.Parent.oContained.w_SM_SETUP)
    return(;
      iif(this.Parent.oContained.w_SM_SETUP=='S',1,;
      0))
  endfunc

  func oSM_SETUP_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSMCODRIS_2_4 as StdTrsField with uid="FMJVHAAQYN",rtseq=4,rtrep=.t.,;
    cFormVar="w_SMCODRIS",value=space(15),;
    ToolTipText = "Codice della risorsa impegnata",;
    HelpContextID = 255206023,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=360, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TAB_RISO", cZoomOnZoom="GSDS_ARI", oKey_1_1="RICODICE", oKey_1_2="this.w_SMCODRIS"

  func oSMCODRIS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMCODRIS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSMCODRIS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_RISO','*','RICODICE',cp_AbsName(this.parent,'oSMCODRIS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ARI',"Elenco risorse",'',this.parent.oContained
  endproc
  proc oSMCODRIS_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ARI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RICODICE=this.parent.oContained.w_SMCODRIS
    i_obj.ecpSave()
  endproc

  add object oDESRIS_2_5 as StdTrsField with uid="ILSXFZEPEV",rtseq=5,rtrep=.t.,;
    cFormVar="w_DESRIS",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione risorsa",;
    HelpContextID = 232926154,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=209, Left=482, Top=0, InputMask=replicate('X',40)

  add object oSMUNIMIS_2_14 as StdTrsField with uid="NNLDJJETXR",rtseq=14,rtrep=.t.,;
    cFormVar="w_SMUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura associata alla risorsa",;
    HelpContextID = 203029881,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=695, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_SMUNIMIS"

  func oSMUNIMIS_2_14.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_SMCODRIS))
    endwith
  endfunc

  func oSMUNIMIS_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMUNIMIS_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSMUNIMIS_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oSMUNIMIS_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSDS_MCS.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oSMUNIMIS_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_SMUNIMIS
    i_obj.ecpSave()
  endproc

  add object oSMQTAMOV_2_15 as StdTrsField with uid="PTAACKZZQF",rtseq=15,rtrep=.t.,;
    cFormVar="w_SMQTAMOV",value=0,;
    HelpContextID = 73417348,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=75, Left=739, Top=0, cSayPict=[v_PQ(10)], cGetPict=[v_GQ(10)]
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mcs','ODL_SMPL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SMCODODL=ODL_SMPL.SMCODODL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
