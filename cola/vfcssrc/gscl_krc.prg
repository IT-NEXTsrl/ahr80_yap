* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscl_krc                                                        *
*              Rivalorizzazione DDT conto lavoro                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_60]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-09                                                      *
* Last revis.: 2011-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscl_krc",oParentObject))

* --- Class definition
define class tgscl_krc as StdForm
  Top    = 15
  Left   = 20

  * --- Standard Properties
  Width  = 552
  Height = 327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-01-07"
  HelpContextID=115987817
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  INVENTAR_IDX = 0
  cPrg = "gscl_krc"
  cComment = "Rivalorizzazione DDT conto lavoro"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_CRIVAL = space(2)
  o_CRIVAL = space(2)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_NUMINV = space(6)
  w_DATINV = ctod('  /  /  ')
  w_DATSYS = ctod('  /  /  ')
  w_MAGCOL = space(1)
  w_TIPINV = space(1)
  w_MAGINV = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_STAINV = space(1)
  w_DESINV = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscl_krcPag1","gscl_krc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCRIVAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='INVENTAR'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_CRIVAL=space(2)
      .w_CODESE=space(4)
      .w_NUMINV=space(6)
      .w_DATINV=ctod("  /  /  ")
      .w_DATSYS=ctod("  /  /  ")
      .w_MAGCOL=space(1)
      .w_TIPINV=space(1)
      .w_MAGINV=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_STAINV=space(1)
      .w_DESINV=space(40)
        .w_AZIENDA = i_CODAZI
        .w_CRIVAL = 'CS'
        .w_CODESE = IIF(.w_CRIVAL='US', SPACE(4), g_CODESE)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODESE))
          .link_1_3('Full')
        endif
        .w_NUMINV = IIF(.w_CRIVAL='US', SPACE(6), IIF(.w_CODESE<>.o_CODESE, SPACE(6), .w_NUMINV))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_NUMINV))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_DATSYS = i_DATSYS
          .DoRTCalc(7,9,.f.)
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(Ah_MsgFormat("Questa funzione ricalcola il valore dei DDT di acquisto per articoli in C/Lavoro"))
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate(Ah_MsgFormat("La procedura utilizza l'inventario o l'ultimo costo standard come punto di riferimento iniziale"))
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate(Ah_MsgFormat("Procede nella valorizzazione considerando i movimenti nella corretta sequenza%0e rivalorizzando quelli dello stesso esercizio e raggruppamento fiscale"))
    endwith
    this.DoRTCalc(11,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CRIVAL<>.w_CRIVAL
            .w_CODESE = IIF(.w_CRIVAL='US', SPACE(4), g_CODESE)
          .link_1_3('Full')
        endif
        if .o_CODESE<>.w_CODESE.or. .o_CRIVAL<>.w_CRIVAL
            .w_NUMINV = IIF(.w_CRIVAL='US', SPACE(6), IIF(.w_CODESE<>.o_CODESE, SPACE(6), .w_NUMINV))
          .link_1_4('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(Ah_MsgFormat("Questa funzione ricalcola il valore dei DDT di acquisto per articoli in C/Lavoro"))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(Ah_MsgFormat("La procedura utilizza l'inventario o l'ultimo costo standard come punto di riferimento iniziale"))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(Ah_MsgFormat("Procede nella valorizzazione considerando i movimenti nella corretta sequenza%0e rivalorizzando quelli dello stesso esercizio e raggruppamento fiscale"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(Ah_MsgFormat("Questa funzione ricalcola il valore dei DDT di acquisto per articoli in C/Lavoro"))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(Ah_MsgFormat("La procedura utilizza l'inventario o l'ultimo costo standard come punto di riferimento iniziale"))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(Ah_MsgFormat("Procede nella valorizzazione considerando i movimenti nella corretta sequenza%0e rivalorizzando quelli dello stesso esercizio e raggruppamento fiscale"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODESE_1_3.enabled = this.oPgFrm.Page1.oPag.oCODESE_1_3.mCond()
    this.oPgFrm.Page1.oPag.oNUMINV_1_4.enabled = this.oPgFrm.Page1.oPag.oNUMINV_1_4.mCond()
    this.oPgFrm.Page1.oPag.oDATINV_1_5.enabled = this.oPgFrm.Page1.oPag.oDATINV_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_3'),i_cWhere,'GSAR_KES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINV
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_CODESE;
                     ,'INNUMINV',trim(this.w_NUMINV))
          select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINV_1_4'),i_cWhere,'GSMA_AIN',"Elenco inventari",'GSCO_KRC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_CODESE;
                       ,'INNUMINV',this.w_NUMINV)
            select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INMAGCOL,INTIPINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_DESINV = NVL(_Link_.INDESINV,space(40))
      this.w_DATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_STAINV = NVL(_Link_.INSTAINV,space(1))
      this.w_MAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_TIPINV = NVL(_Link_.INTIPINV,space(1))
      this.w_MAGINV = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINV = space(6)
      endif
      this.w_DESINV = space(40)
      this.w_DATINV = ctod("  /  /  ")
      this.w_STAINV = space(1)
      this.w_MAGCOL = space(1)
      this.w_TIPINV = space(1)
      this.w_MAGINV = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_STAINV<>'P' AND .w_TIPINV='G' AND EMPTY(nvl(looktab('MAGAZZIN','MGMAGRAG','MGCODMAG',.w_MAGINV),""))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NUMINV = space(6)
        this.w_DESINV = space(40)
        this.w_DATINV = ctod("  /  /  ")
        this.w_STAINV = space(1)
        this.w_MAGCOL = space(1)
        this.w_TIPINV = space(1)
        this.w_MAGINV = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCRIVAL_1_2.RadioValue()==this.w_CRIVAL)
      this.oPgFrm.Page1.oPag.oCRIVAL_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_3.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_3.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINV_1_4.value==this.w_NUMINV)
      this.oPgFrm.Page1.oPag.oNUMINV_1_4.value=this.w_NUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINV_1_5.value==this.w_DATINV)
      this.oPgFrm.Page1.oPag.oDATINV_1_5.value=this.w_DATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSYS_1_6.value==this.w_DATSYS)
      this.oPgFrm.Page1.oPag.oDATSYS_1_6.value=this.w_DATSYS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINV_1_22.value==this.w_DESINV)
      this.oPgFrm.Page1.oPag.oDESINV_1_22.value=this.w_DESINV
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))  and (.w_CRIVAL<>'US')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMINV)) or not(.w_STAINV<>'P' AND .w_TIPINV='G' AND EMPTY(nvl(looktab('MAGAZZIN','MGMAGRAG','MGCODMAG',.w_MAGINV),""))))  and (.w_CRIVAL<>'US')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINV_1_4.SetFocus()
            i_bnoObbl = !empty(.w_NUMINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATSYS)) or not(.w_DATSYS>=.w_DATINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSYS_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATSYS)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscl_krc
      if i_bRes=.t. and not (.w_CRIVAL='US'or not empty(.w_NUMINV))
         i_cErrorMsg = ah_MsgFormat("Numero inventario non definito")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CRIVAL = this.w_CRIVAL
    this.o_CODESE = this.w_CODESE
    return

enddefine

* --- Define pages as container
define class tgscl_krcPag1 as StdContainer
  Width  = 548
  height = 327
  stdWidth  = 548
  stdheight = 327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCRIVAL_1_2 as StdCombo with uid="SVYFDQOGHY",rtseq=2,rtrep=.f.,left=127,top=126,width=157,height=21;
    , ToolTipText = "Criterio di valorizzazione dei DDT";
    , HelpContextID = 108981978;
    , cFormVar="w_CRIVAL",RowSource=""+"Costo standard,"+"Costo medio periodo,"+"Costo medio esercizio,"+"Ultimo costo,"+"Ultimo costo standard (articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIVAL_1_2.RadioValue()
    return(iif(this.value =1,'CS',;
    iif(this.value =2,'CM',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'UC',;
    iif(this.value =5,'US',;
    space(2)))))))
  endfunc
  func oCRIVAL_1_2.GetRadio()
    this.Parent.oContained.w_CRIVAL = this.RadioValue()
    return .t.
  endfunc

  func oCRIVAL_1_2.SetRadio()
    this.Parent.oContained.w_CRIVAL=trim(this.Parent.oContained.w_CRIVAL)
    this.value = ;
      iif(this.Parent.oContained.w_CRIVAL=='CS',1,;
      iif(this.Parent.oContained.w_CRIVAL=='CM',2,;
      iif(this.Parent.oContained.w_CRIVAL=='CA',3,;
      iif(this.Parent.oContained.w_CRIVAL=='UC',4,;
      iif(this.Parent.oContained.w_CRIVAL=='US',5,;
      0)))))
  endfunc

  add object oCODESE_1_3 as StdField with uid="OVDUECVFCN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 208683482,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=154, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIVAL<>'US')
    endwith
   endif
  endfunc

  func oCODESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_NUMINV)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODESE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"",'',this.parent.oContained
  endproc
  proc oCODESE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_CODESE
     i_obj.ecpSave()
  endproc

  add object oNUMINV_1_4 as StdField with uid="JVHAJQAKFC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMINV", cQueryName = "NUMINV",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero inventario",;
    HelpContextID = 71587030,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=184, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_CODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINV"

  func oNUMINV_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIVAL<>'US')
    endwith
   endif
  endfunc

  func oNUMINV_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINV_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINV_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_CODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_CODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINV_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Elenco inventari",'GSCO_KRC.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oNUMINV_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_CODESE
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINV
     i_obj.ecpSave()
  endproc

  add object oDATINV_1_5 as StdField with uid="QWAXDWPXYX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINV", cQueryName = "DATINV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale vengono considerati i movimenti",;
    HelpContextID = 71610422,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=127, Top=244

  func oDATINV_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIVAL='US')
    endwith
   endif
  endfunc

  add object oDATSYS_1_6 as StdField with uid="LVSDEVFTCC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATSYS", cQueryName = "DATSYS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale devono essere considerati i movimenti",;
    HelpContextID = 33468470,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=242, Top=244

  func oDATSYS_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATSYS>=.w_DATINV)
    endwith
    return bRes
  endfunc


  add object oBtn_1_19 as StdButton with uid="XAOFRYAIID",left=440, top=275, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la rivalorizzazione";
    , HelpContextID = 115959066;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="KYQESPJULE",left=492, top=275, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108670394;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESINV_1_22 as StdField with uid="IPEKRTXBGS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESINV", cQueryName = "DESINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 71607350,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=188, Top=184, InputMask=replicate('X',40)


  add object oObj_1_23 as cp_runprogram with uid="LYNJMNQSPV",left=359, top=333, width=159,height=32,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCL_BRC",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 62009830


  add object oObj_1_24 as cp_calclbl with uid="DSANYOPGAT",left=17, top=24, width=525,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 62009830


  add object oObj_1_25 as cp_calclbl with uid="VVBSLHJNXG",left=17, top=49, width=525,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 62009830


  add object oObj_1_26 as cp_calclbl with uid="LXGIVYYAHU",left=17, top=74, width=525,height=34,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 62009830

  add object oStr_1_7 as StdString with uid="YRUPFVRTDB",Visible=.t., Left=18, Top=9,;
    Alignment=0, Width=104, Height=15,;
    Caption="ATTENZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="CZNCPYNCOP",Visible=.t., Left=12, Top=184,;
    Alignment=1, Width=111, Height=15,;
    Caption="Inventario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="OMRGUPPIXW",Visible=.t., Left=74, Top=244,;
    Alignment=1, Width=49, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="AOOCZHJSPQ",Visible=.t., Left=12, Top=154,;
    Alignment=1, Width=111, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WAXKZGIXEK",Visible=.t., Left=210, Top=244,;
    Alignment=1, Width=28, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="SWNHFYFXSE",Visible=.t., Left=10, Top=213,;
    Alignment=0, Width=81, Height=15,;
    Caption="Movimenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="LCKWCROMZJ",Visible=.t., Left=12, Top=125,;
    Alignment=1, Width=111, Height=18,;
    Caption="Valorizzazione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="NMKAUCHJVD",left=7, top=232, width=532,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscl_krc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
