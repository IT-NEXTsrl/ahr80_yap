* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco1bto                                                        *
*              Tracciabilita OCL                                               *
*                                                                              *
*      Author: ZUCCHETTI TAM SPA CF                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-17                                                      *
* Last revis.: 2016-01-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco1bto",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsco1bto as StdBatch
  * --- Local variables
  w_PARAM = space(10)
  w_PUNPAD = .NULL.
  w_NODITV = .NULL.
  Albero = .NULL.
  w_PROG = .NULL.
  w_OGGETTO = .NULL.
  w_cCursor = space(20)
  w_LDettTec = space(1)
  w_PPCENCOS = space(15)
  w_ODL = space(150)
  w_OCL = space(150)
  w_OCF = space(150)
  w_ORD = space(150)
  w_MDM = space(150)
  w_DTA = space(150)
  w_DTV = space(150)
  w_FAT = space(150)
  w_DPI = space(150)
  w_PARAM = space(1)
  w_PARAM1 = space(1)
  w_PARAM2 = space(1)
  w_SELEZI = space(1)
  TmpC = space(100)
  w_EXPANDED = space(1)
  w_DOCSER = space(10)
  pstipo = space(2)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_ANCATCOM = space(3)
  w_CONUMERO = space(5)
  w_COFLGCIC = space(1)
  w_CI = 0
  w_CJ = 0
  w_CK = 0
  w_CX = 0
  w_CY = 0
  w_LVLKEY = space(240)
  w_LVLKEY1 = space(240)
  w_LVLKEY2 = space(240)
  w_LVLKEY3 = space(240)
  w_LVLKEY4 = space(240)
  CHKKEY = .f.
  w_SINGOLO = space(1)
  w_ODLSEL = space(15)
  w_ARTICOLO = space(20)
  w_VARIANTE = space(20)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_ODLPAD = space(15)
  w_ODL1 = space(15)
  w_SERRIF1 = space(10)
  w_ROWRIF1 = 0
  w_TROVATO = space(10)
  w_STAMPFAS = space(1)
  w_STAMPODL = space(1)
  w_LABELREG = space(50)
  w_LABELTDOC = space(50)
  w_LABELDOC = space(50)
  w_LABELCMAG = space(50)
  w_LABELART = space(50)
  w_NUMRIF = 0
  w_PROVE = space(1)
  * --- WorkFile variables
  TMPOCL_MAST_idx=0
  KEY_ARTI_idx=0
  DOC_DETT_idx=0
  MVM_MAST_idx=0
  PAR_PROD_idx=0
  DOC_MAST_idx=0
  RIF_GODL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tracciabilit� OCL (da GSCL_KTO,GSDB_AOL)
    * --- Parametro di elaborazione
    * --- Caller
    * --- Locali
    * --- Assegnamenti
    *     -----------------------
    * --- Puntatore alla Maschera
    this.w_PUNPAD = this.oParentObject
    * --- Tree-view
    this.Albero = this.oParentObject.w_TREEV
    * --- Cursore Tree-view
    this.w_cCursor = this.Albero.cCursor
    * --- Nodi Tree-view
    this.w_NODITV = this.Albero.oTree
    * --- Tipo Esplosione
    this.w_SINGOLO = "N"
    * --- Gestione apertura multipla maschera GSCL_kto (cambio il nome del cursore tutte le volte che apro la tree-view)
    this.w_LABELREG = "Reg. n.: %1 del %2 (%3)"
    this.w_LABELTDOC = "Tipo doc.: %1"
    this.w_LABELDOC = "Doc. n.: %1"
    this.w_LABELCMAG = "Causale mag.: %1"
    this.w_LABELART = "Art: %1 (%2)"
    if this.w_PARAM="Tree-View" or this.w_PARAM="Page2"
      if used(this.w_cCursor)
        use in (this.w_cCursor)
      endif
      this.w_cCursor = sys(2015)
      this.Albero.cCursor = this.w_cCursor
      * --- Inizializzo la variabile di GSCL_KTO
      this.oParentObject.w_FirstTime = .T.
    endif
    * --- Bitmap per tree-view
    this.w_ODL = padr(".\bmp\odll.bmp",150)
    this.w_OCL = padr(".\bmp\odll.bmp",150)
    this.w_OCF = padr(".\bmp\fase.bmp",150)
    this.w_ORD = padr(".\bmp\documenti.bmp",150)
    this.w_MDM = padr(".\bmp\mdm.bmp",150)
    this.w_DTA = padr(".\bmp\ocl.bmp",150)
    this.w_DTV = padr(".\bmp\ddttra.bmp",150)
    this.w_FAT = padr(".\bmp\fattura1.bmp",150)
    this.w_DPI = padr(".\bmp\batch.bmp",150)
    * --- Variabili utilizzate da GSDB_BCS
    this.w_STAMPFAS = "N"
    this.w_STAMPODL = "N"
    do case
      case this.w_PARAM="Tree-View" or this.w_PARAM="Page2"
        * --- Drop temporary table TMPOCL_MAST
        i_nIdx=cp_GetTableDefIdx('TMPOCL_MAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPOCL_MAST')
        endif
        * --- Scelgo la query da utilizzare.
        if this.oParentObject.w_FLVEAC="T" and this.oParentObject.w_CATDOC="XX" and empty(this.oParentObject.w_TIPDOC) and empty(this.oParentObject.w_TIPMAG) and ((this.oParentObject.w_NUMINI=0 and this.oParentObject.w_NUMFIN=0) ; 
 or (this.oParentObject.w_NUMINI=1 and this.oParentObject.w_NUMFIN=999999)) and empty(this.oParentObject.w_DOCINI) and empty(this.oParentObject.w_DOCFIN) and empty(this.oParentObject.w_DATINI) and ; 
 empty(this.oParentObject.w_DATFIN) and ((this.oParentObject.w_NUMREI=0 and this.oParentObject.w_NUMREF=0) or (this.oParentObject.w_NUMREI=1 and this.oParentObject.w_NUMREF=999999)) and empty(this.oParentObject.w_CODMAG) 
          * --- Esguo la query senza alcun filtro su Documenti e Movimenti di Magazzino
          * --- Create temporary table TMPOCL_MAST
          i_nIdx=cp_AddTableDef('TMPOCL_MAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_SDSZNASCLS[1]
          indexes_SDSZNASCLS[1]='OLCODODL'
          vq_exec('GSCO1KTO',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_SDSZNASCLS,.f.)
          this.TMPOCL_MAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Ho dei filtri su Documenti/Movimenti di Magazzino
          do case
            case this.oParentObject.w_CATDOC="OR"
              * --- Solo filtri sugli Ordini
              * --- Create temporary table TMPOCL_MAST
              i_nIdx=cp_AddTableDef('TMPOCL_MAST') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              declare indexes_HGPDQSSGAY[1]
              indexes_HGPDQSSGAY[1]='OLCODODL'
              vq_exec('GSCO1KTO1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_HGPDQSSGAY,.f.)
              this.TMPOCL_MAST_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.oParentObject.w_CATDOC="DT"
              * --- Solo filtro su DDT
              * --- Acquisto
              * --- Create temporary table TMPOCL_MAST
              i_nIdx=cp_AddTableDef('TMPOCL_MAST') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              declare indexes_VHNHDXQIBK[1]
              indexes_VHNHDXQIBK[1]='OLCODODL'
              vq_exec('GSCO1KTO2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_VHNHDXQIBK,.f.)
              this.TMPOCL_MAST_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.oParentObject.w_CATDOC="FA"
              * --- Solo filtro su Fatture
              * --- Create temporary table TMPOCL_MAST
              i_nIdx=cp_AddTableDef('TMPOCL_MAST') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              declare indexes_QJTLZQWQWI[1]
              indexes_QJTLZQWQWI[1]='OLCODODL'
              vq_exec('GSCO1KTO3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_QJTLZQWQWI,.f.)
              this.TMPOCL_MAST_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.oParentObject.w_CATDOC="XX"
              * --- Ordini
              * --- Create temporary table TMPOCL_MAST
              i_nIdx=cp_AddTableDef('TMPOCL_MAST') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              declare indexes_ACZGWUKZLT[1]
              indexes_ACZGWUKZLT[1]='OLCODODL'
              vq_exec('GSCO1KTO1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ACZGWUKZLT,.f.)
              this.TMPOCL_MAST_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              * --- DDT Acq
              * --- Insert into TMPOCL_MAST
              i_nConn=i_TableProp[this.TMPOCL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPOCL_MAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCO1KTO2",this.TMPOCL_MAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              * --- Fatture
              * --- Insert into TMPOCL_MAST
              i_nConn=i_TableProp[this.TMPOCL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPOCL_MAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCO1KTO3",this.TMPOCL_MAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
        endif
      case this.w_PARAM="Done" and used(this.w_cCursor)
        * --- Rilascio la tabella temporanea
        * --- Drop temporary table TMPOCL_MAST
        i_nIdx=cp_GetTableDefIdx('TMPOCL_MAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPOCL_MAST')
        endif
        * --- Chiudo il cursore della tree-view
        use in (this.w_cCursor)
        * --- Rilascio anche popmenu
        DEACTIVATE POPUP MenuTV
        RELEASE POPUPS MenuTV EXTENDED
      case this.w_PARAM="PopUp" or this.w_PARAM="PopRid"
        Private Azione 
 store space(10) to Azione
        if .F.
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_PARAM="EXPNODE"
        this.w_SINGOLO = "S"
        select (this.w_cCursor)
        if not empty(nvl(this.Albero.nKeyExpanded," "))
          if seek(substr(this.Albero.nKeyExpanded,2))
            this.w_EXPANDED = collapsed
            this.w_ODLSEL = OLCODODL
            this.w_ODL1 = OLCODODL1
            this.w_LVLKEY = LVLKEY
            this.pstipo = TIPO
            this.w_SERRIF = MVSERIAL
            this.w_ROWRIF = CPROWNUM
            this.w_ODLPAD = ORDINE
            if this.w_EXPANDED<>"+"
              select (this.w_cCursor) 
 replace collapsed with "+"
            endif
          endif
        endif
        if this.oParentObject.w_FirstTime and this.w_EXPANDED<>"+" and this.pstipo $"OCL-OCF-ORD-DTA-ODA"
          * --- Esplosione livello per livello - come distinta base
          do case
            case this.pstipo $"OCL-OCF-ODA"
              vq_exec("..\COLA\exe\query\GSCL8bto", this, "_dett_")
              * --- Viene simulata una riga vuota per abilitare il pulsante '+' per tutti gli Ordini
              select _dett_ 
 scan
              if tipo="ORD"
                * --- Il + lo aggiungo solo se serve, verifico che ci sia almeno un DDT di rientro legato all'ordine in esame
                this.w_TROVATO = space(10)
                this.w_SERRIF1 = MVSERIAL
                this.w_ROWRIF1 = CPROWNUM
                * --- Read from DOC_DETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVSERIAL"+;
                    " from "+i_cTable+" DOC_DETT where ";
                        +"MVSERRIF = "+cp_ToStrODBC(this.w_SERRIF1);
                        +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWRIF1);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVSERIAL;
                    from (i_cTable) where;
                        MVSERRIF = this.w_SERRIF1;
                        and MVROWRIF = this.w_ROWRIF1;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TROVATO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if not empty(this.w_TROVATO)
                  CODL=OLCODODL 
 POSIZ=ORDINE 
 ODL1=OLCODODL1 
 RCD=recno() 
 append blank
                  replace ORDINE with POSIZ, OLCODODL with CODL, OLCODODL1 with ODL1, MVFLVEAC with "A", LIVELLO with 3, TIPO with "DTA" , ORDINA with "A" 
 go RCD
                endif
              endif
              endscan
            case this.pstipo="ORD"
              vq_exec("..\COLA\exe\query\GSCL9bto", this, "_dett_")
              * --- Viene simulata una riga vuota per abilitare il pulsante '+' per tutti i DTA
              select _dett_ 
 scan
              if tipo="DTA"
                * --- Il + lo aggiungo solo se serve, verifico che ci sia almeno un MVM (o una Fattura) legato all'ordine in esame
                this.w_TROVATO = space(10)
                this.w_SERRIF1 = MVSERIAL
                this.w_ROWRIF1 = CPROWNUM
                * --- Read from DOC_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVSERIAL"+;
                    " from "+i_cTable+" DOC_MAST where ";
                        +"MVSERDDT = "+cp_ToStrODBC(this.w_SERRIF1);
                        +" and MVROWDDT = "+cp_ToStrODBC(this.w_ROWRIF1);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVSERIAL;
                    from (i_cTable) where;
                        MVSERDDT = this.w_SERRIF1;
                        and MVROWDDT = this.w_ROWRIF1;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TROVATO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Se c'� almeno un MVM non serve verificare la presenza della fattura
                if empty(this.w_TROVATO)
                  * --- Read from DOC_DETT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "MVSERIAL"+;
                      " from "+i_cTable+" DOC_DETT where ";
                          +"MVSERRIF = "+cp_ToStrODBC(this.w_SERRIF1);
                          +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWRIF1);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      MVSERIAL;
                      from (i_cTable) where;
                          MVSERRIF = this.w_SERRIF1;
                          and MVROWRIF = this.w_ROWRIF1;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_TROVATO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if not empty(this.w_TROVATO)
                  CODL=OLCODODL 
 POSIZ=ORDINE 
 ODL1=OLCODODL1 
 RCD=recno() 
 append blank
                  replace ORDINE with POSIZ, OLCODODL with CODL, OLCODODL1 with ODL1, MVFLVEAC with "A", LIVELLO with 3, TIPO with "FAT" , ORDINA with "A" 
 go RCD
                endif
              endif
              endscan
            case this.pstipo="DTA"
              vq_exec("..\COLA\exe\query\GSCL10bto", this, "_dett_")
          endcase
          select (this.w_cCursor) 
 scan for OLCODODL=this.w_ODLSEL and empty (OLTCODIC) and tipo<>"DPI" and iif(tipo="FAT",OLCODODL1=this.w_ODL1,.t.)
          * --- Cancello l'elemento se c'� gi�
          this.Albero.DelElement(alltrim("k"+lvlkey))     
          delete 
 endscan
          if Used("_dett_") and RecCount("_dett_")>0
            select *, space(200) as lvlkey, space(150) as CPBmpName, space(250) as Descri, "-" as collapsed from _dett_ order by ; 
 1,ORDINA,26,3,25 into cursor _tree_ 
 =wrcursor("_tree_")
            if this.pstipo $"OCL-OCF-ODA"
              * --- Elimino righe duplicate del Piano di generazione (sono riferite a due righe diverse, eliminandole tutte esclusa la prima che trovo le raggruppo)
              select _tree_ 
 scan for tipo="DPI" 
 odl=olcododl 
 odl1=olcododl1 
 seriale=mvserial 
 riga=recno() 
 go top 
 delete for olcododl=odl and mvserial=seriale and olcododl1<>odl1 and tipo="DPI" 
 go riga 
 endscan
            endif
            this.w_CK = 0
            activecursor=this.w_cCursor
            if used("_dett_")
              use in _dett_
            endif
            select _tree_ 
 scan
            do case
              case livello=2
                if tipo="DTV"
                  this.w_LVLKEY3 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CX)),9)
                  replace lvlkey with this.w_LVLKEY3
                  this.w_CX = this.w_CX + 1
                else
                  if this.w_CK=0
                    this.w_CK = this.w_CK + 1
                  endif
                  this.w_LVLKEY2 = Alltrim(this.w_LVLKEY)+"."+right("00000000"+alltrim(str(this.w_CK)),9)
                  replace lvlkey with this.w_LVLKEY2
                  this.w_CK = this.w_CK + 1
                  this.w_CX = 1
                endif
              case livello=3
                if tipo $ "MDM-FAT"
                  if empty(nvl(this.w_LVLKEY3," ")) and this.w_CY=0
                    this.w_CY = 1
                    this.w_LVLKEY3 = Alltrim(this.w_LVLKEY)
                  endif
                  this.w_LVLKEY4 = this.w_LVLKEY3+"."+right("00000000"+alltrim(str(this.w_CY)),9)
                  replace lvlkey with this.w_LVLKEY4
                  this.w_CY = this.w_CY + 1
                else
                  if empty(nvl(this.w_LVLKEY2," ")) and this.w_CX=0
                    this.w_CX = 1
                    this.w_LVLKEY2 = Alltrim(this.w_LVLKEY)
                  endif
                  this.w_LVLKEY3 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CX)),9)
                  replace lvlkey with this.w_LVLKEY3
                  this.w_CX = this.w_CX + 1
                  this.w_CY = 1
                  this.CHKKEY = .F.
                endif
              case livello=4
                this.w_LVLKEY4 = this.w_LVLKEY3+"."+right("00000000"+alltrim(str(this.w_CY)),9)
                replace lvlkey with this.w_LVLKEY4
                this.w_CY = this.w_CY + 1
            endcase
            endscan
            update _tree_ set Descri=iif(tipo="DPI",mvserial+SPACE(1)+; 
 AH_MsgFormat("Piano di spedizione a terzista"),; 
 iif(livello=0,iif(tipo="ODL",olcododl1,olcododl),iif(livello=1,olcododl,; 
 iif(livello>1,mvserial+iif(tipo<>"MDM"," "+; 
 AH_MsgFormat(this.w_LABELTDOC,MVTIPDOC),"")+" "+; 
 AH_MsgFormat(this.w_LABELDOC,allt(trans(MVNUMDOC,v_pq(26))))+iif(empty(MVALFDOC),"","/")+MVALFDOC+" "+; 
 AH_MsgFormat(this.w_LABELCMAG,MVCAUMAG),; 
 "")))+" ("+allt(trans(OLTQTODL,v_pq(12)))+" "+nvl(UNIMIS,"")+") "+; 
 AH_MsgFormat(this.w_LABELART,allt(OLTCODIC), allt(ARDESART))),; 
 CPBmpName = iif(tipo="OCF",this.w_OCF,iif(tipo="ORD",this.w_ORD,iif(tipo="MDM",this.w_MDM,iif(tipo="DTA",this.w_DTA,iif(tipo="DTV",; 
 this.w_DTV,iif(tipo="FAT",this.w_FAT,iif(tipo="ODL",this.w_ODL,iif(tipo="OCL",this.w_OCL,iif(tipo="DPI",this.w_DPI,space(150)))))))))),; 
 ORDINE=iif(empty(nvl(ORDINE," ")),OLCODODL,ORDINE)
            select _tree_ 
 scan 
 scatter memvar 
 riga=recno() 
 insert into &activecursor from memvar 
 select &activecursor
            this.Albero.AddElement(alltrim("k"+left(lvlkey,rat(".",lvlkey)-1)),alltrim("k"+lvlkey))     
            select _tree_ 
 go riga
            endscan
            if used("_tree_")
              use in _tree_
            endif
          else
            * --- Cursore Vuoto
          endif
        endif
    endcase
    do case
      case this.w_PARAM="Gestione"
        if this.oParentObject.w_LEVEL>1
          if this.oParentObject.w_TYPE="DPI"
            * --- Apertura Piano Generazione.
            this.w_PROG = GSCO_AMG("DT")
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carico il Record.
            this.w_PROG.ecpFilter()     
            this.w_PROG.w_PDSERIAL = this.oParentObject.w_CODODL1
            this.w_PROG.w_PDTIPGEN = "DT"
            this.w_PROG.ecpSave()     
          else
            * --- Numrif -20 documento
            this.w_NUMRIF = -20
            GSAR_BZM(this,this.oParentObject.w_CODODL1, this.w_NUMRIF)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          GSCO_BOR(this,this.oParentObject.w_CODODL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_PARAM="Esplosione"
        if this.oParentObject.w_FirstTime
          * --- L'esplosione avviene per la prima volta, devo calcolare tutti i sottolivelli dell'albero
          if used(this.w_cCursor)
            use in (this.w_cCursor)
          endif
          this.w_cCursor = sys(2015)
          this.Albero.cCursor = this.w_cCursor
          vq_exec("..\COLA\exe\query\GSCO1BTO1", this, "_curs_")
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Albero.ExpandAll(.T.)
          if used(this.w_cCursor)
            update (this.w_cCursor) set collapsed="+" where collapsed="-"
          endif
        else
          * --- L'albero � gi� pronto, mi limito ad espanderne le foglie
          this.Albero.ExpandAll(.T.)
          if used(this.w_cCursor)
            update (this.w_cCursor) set collapsed="+" where collapsed="-"
          endif
        endif
      case this.w_PARAM="Implosione"
        this.Albero.ExpandAll(.F.)
      case this.w_PARAM="PGen"
        * --- Apertura Piano Generazione.
        if this.oParentObject.w_TYPE="ORD"
          * --- Read from RIF_GODL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RIF_GODL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2],.t.,this.RIF_GODL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PDSERIAL"+;
              " from "+i_cTable+" RIF_GODL where ";
                  +"PDTIPGEN = "+cp_ToStrODBC("OR");
                  +" and PDSERDOC = "+cp_ToStrODBC(this.oParentObject.w_CODODL1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PDSERIAL;
              from (i_cTable) where;
                  PDTIPGEN = "OR";
                  and PDSERDOC = this.oParentObject.w_CODODL1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DOCSER = NVL(cp_ToDate(_read_.PDSERIAL),cp_NullValue(_read_.PDSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PROVE = "E"
          this.pstipo = "OR"
        else
          * --- Read from RIF_GODL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RIF_GODL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2],.t.,this.RIF_GODL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PDSERIAL"+;
              " from "+i_cTable+" RIF_GODL where ";
                  +"PDTIPGEN = "+cp_ToStrODBC("DT");
                  +" and PDSERDOC = "+cp_ToStrODBC(this.oParentObject.w_CODODL1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PDSERIAL;
              from (i_cTable) where;
                  PDTIPGEN = "DT";
                  and PDSERDOC = this.oParentObject.w_CODODL1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DOCSER = NVL(cp_ToDate(_read_.PDSERIAL),cp_NullValue(_read_.PDSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PROVE = "E"
          this.pstipo = "DT"
        endif
        this.w_PROG = GSCO_AMG(this.w_PROVE, this.pstipo)
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il Record.
        this.w_PROG.ecpfilter()     
        this.w_PROG.w_PDSERIAL = this.w_DOCSER
        if this.oParentObject.w_TYPE="ORD"
          this.w_PROG.w_PDTIPGEN = "OR"
        else
          this.w_PROG.w_PDTIPGEN = "DT"
        endif
        this.w_PROG.ecpSave()     
      case this.w_PARAM="Chiudi"
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPLOGTEC,PPCENCOS"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPLOGTEC,PPCENCOS;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LDettTec = NVL(cp_ToDate(_read_.PPLOGTEC),cp_NullValue(_read_.PPLOGTEC))
          this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PARAM = "U"
        * --- OCL
        this.TmpC = "Si � scelto di chiudere l'ODA n. %1%0Si � certi di voler proseguire l'operazione?"
        this.w_PARAM1 = " "
        this.w_PARAM2 = " "
        this.w_SELEZI = "Z"
        if ah_YesNo(this.TmpC,"",this.oParentObject.w_CODODL)
          vq_exec("..\COLA\exe\query\gsco_kpe", this, "GSCOSBCL")
          GSCO_BCL(this,"AG", "E")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        USE IN SELECT("GSCOSBCL")
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Tree-View
    * --- Elabora l'albero per la visualizzazione
    vq_exec("..\COLA\EXE\QUERY\GSCO1BTO1", this, "_curs_")
    if this.oParentObject.w_FirstTime
      * --- Prendo solo i padri ed esplodo volta per volta
      if Used("_curs_") and RecCount("_curs_")>0
        * --- Viene simulata una riga vuota per abilitare il pulsante '+' per tutti gli OCL
        select _curs_ 
 scan
        if tipo<>"ODL"
          CODL=OLCODODL 
 POSIZ=ORDINE 
 RCD=recno() 
 append blank
          replace ORDINE with POSIZ, OLCODODL with CODL, LIVELLO with 2, TIPO with "ODL" 
 go RCD
        endif
        endscan
        select *, space(200) as lvlkey, space(150) as CPBmpName, space(250) as Descri from _curs_ order by 1,2,26,3,25 into cursor _tree_ 
 =wrcursor("_tree_")
        * --- Calcolo il lvlkey
        SCAN
        do case
          case livello=0
            * --- ODL padre di un OCL di fase oppure OCL di salto codice
            this.w_CI = this.w_CI + 1
            this.w_CJ = 1
            this.w_CK = 0
            this.CHKKEY = .F.
            this.w_LVLKEY = right("00000000"+alltrim(str(this.w_CI)),9)
            replace lvlkey with this.w_LVLKEY
          case livello=1
            * --- OCL di fase (questo � l'unico livello che pu� non esserci anche se c'� il livello successivo!)
            this.w_LVLKEY1 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
            replace lvlkey with this.w_LVLKEY1
            this.w_CJ = this.w_CJ + 1
            this.w_CK = 1
            this.CHKKEY = .T.
          case livello=2
            if this.w_CK=0 or ! this.CHKKEY
              * --- Se il livello 1 non esiste
              this.w_CK = this.w_CK + 1
              this.w_LVLKEY2 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CK)),9)
            else
              * --- Se il livello 1 esiste
              this.w_LVLKEY2 = this.w_LVLKEY1+"."+right("00000000"+alltrim(str(this.w_CK)),9)
            endif
            replace lvlkey with this.w_LVLKEY2
            this.w_CK = this.w_CK + 1
            this.w_CX = 1
        endcase
        ENDSCAN
        use in _curs_ 
 update _tree_ set Descri=iif(tipo="DPI",mvserial+" "+; 
 AH_MsgFormat(this.w_LABELREG,allt(trans(MVNUMREG,v_pq(26))), alltrim(dtoc(oltdinric)), allt(ARDESART)),; 
 iif(livello=0,iif(tipo="ODL",olcododl1,olcododl),iif(livello=1,olcododl,iif(livello>1,mvserial+iif(tipo<>"MDM",; 
 AH_MsgFormat(this.w_LABELTDOC,MVTIPDOC),"")+" "+; 
 AH_MsgFormat(this.w_LABELDOC, allt(trans(MVNUMDOC,v_pq(26))))+iif(empty(MVALFDOC),"","/")+MVALFDOC+" "+; 
 AH_MsgFormat(this.w_LABELCMAG, MVCAUMAG),"")))+; 
 " ("+allt(trans(OLTQTODL,v_pq(12)))+" "+nvl(UNIMIS,"")+") "+; 
 AH_MsgFormat(this.w_LABELART,allt(OLTCODIC), allt(ARDESART))), ; 
 CPBmpName = iif(tipo="OCF",this.w_OCF,iif(tipo="ORD",this.w_ORD,iif(tipo="MDM",this.w_MDM,iif(tipo="DTA",this.w_DTA,iif(tipo="DTV",; 
 this.w_DTV,iif(tipo="FAT",this.w_FAT,iif(tipo="ODL",this.w_ODL,iif(tipo="OCL",this.w_OCL,iif(tipo="DPI",this.w_DPI,space(150))))))))))
      else
        * --- Cursore Vuoto
        if used("_curs_")
          use in _curs_
        endif
        ah_ErrorMsg("Non ci sono dati da visualizzare",48)
        * --- Sbianco la tree-view
        this.w_NODITV.Nodes.Clear
      endif
      activecursor=this.w_cCursor
      if used("_tree_")
        Select *,"-" as collapsed from _tree_ order by lvlkey into cursor &activecursor
        use in _tree_
        if INLIST(alltrim(upper(this.w_PUNPAD.Class)), "TGSCL_KTO", "TGSCO1KTO")
          this.w_PunPad.oPgFrm.ActivePage = 2
        else
          this.w_PunPad.oPgFrm.ActivePage = 9
        endif
      endif
      * --- Indicizzo il cursore e faccio il refresh della tree-view sulla maschera
      if used(this.w_cCursor)
        =wrcursor(this.w_cCursor) 
 index on lvlkey tag lvlkey COLLATE "MACHINE"
      else
        * --- Sbianco la tree-view
        this.w_NODITV.Nodes.Clear
        * --- Sbianco le variabili della maschera
      endif
    endif
    this.w_PUNPAD.NotifyEvent("updtreev")     
    if !INLIST(alltrim(upper(this.w_PUNPAD.Class)), "TGSCL_KTO", "TGSCO1KTO")
      this.w_PunPad.mhidecontrols()     
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce SHORTCUT
    DEFINE POPUP MenuTV from MRow()+1,MCol()+1 shortcut margin
    if this.w_PARAM="PopUp"
      DEFINE BAR 1 OF MenuTV prompt AH_Msgformat("Apertura gestione")
      DEFINE BAR 2 OF MenuTV prompt AH_Msgformat("Esplosione albero")
      DEFINE BAR 3 OF MenuTV prompt AH_Msgformat("Implosione albero")
      DEFINE BAR 4 OF MenuTV prompt "\-"
      if (this.oParentObject.w_ST $ "L-P" and this.oParentObject.w_TYPE="ODL") or (this.oParentObject.w_TYPE="OCL" and this.oParentObject.w_ST="L") or (this.oParentObject.w_TYPE="OCF" and this.oParentObject.w_ST="L")
        * --- La chiusura pu� essere effettuata solo su OCL Lanciati
        DEFINE BAR 5 OF MenuTV prompt AH_Msgformat("Chiudi OCL")
      else
        DEFINE BAR 5 OF MenuTV prompt AH_Msgformat("Chiudi OCL") skip
      endif
      if this.oParentObject.w_TYPE $ "DTV-ORD"
        DEFINE BAR 6 OF MenuTV prompt AH_Msgformat("Piano generazione")
      else
        DEFINE BAR 6 OF MenuTV prompt AH_Msgformat("Piano generazione") skip
      endif
    else
      * --- Pop-up Ridotto
      DEFINE BAR 1 OF MenuTV prompt AH_Msgformat("Esplosione albero")
      DEFINE BAR 2 OF MenuTV prompt AH_Msgformat("Implosione albero")
    endif
    if this.w_PARAM="PopUp"
      ON SELE BAR 1 OF MenuTV Azione="Gestione"
      ON SELE BAR 2 OF MenuTV Azione="Esplosione"
      ON SELE BAR 3 OF MenuTV Azione="Implosione"
      ON SELE BAR 5 OF MenuTV Azione="Chiudi"
      ON SELE BAR 6 OF MenuTV Azione="PGen"
    else
      * --- Pop-up Ridotto
      ON SELE BAR 1 OF MenuTV Azione="Esplosione"
      ON SELE BAR 2 OF MenuTV Azione="Implosione"
    endif
    ACTI POPUP MenuTV
    DEACTIVATE POPUP MenuTV
    RELEASE POPUPS MenuTV EXTENDED
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplodo tutto l'albero
    if Used("_curs_") and RecCount("_curs_")>0
      vq_exec("..\COLA\exe\query\GSCL1bto", this, "_dett_")
      select *, space(200) as lvlkey, space(150) as CPBmpName, space(250) as Descri from _curs_ union ; 
 select iif(empty(nvl(Ordine," ")),Olcododl,Ordine) as Ordine,Olcododl,Olcododl1,Oltstato,Oltcodic,Ardesart,Oltdinric,; 
 Oltdtric,Unimis,Oltqtodl,Oltqtoev,Oltqtobf,Oltcomag,Oltcofor,Oltfleva,Mvserial,CPROWNUM,Mvnumreg,Mvtipdoc,; 
 Mvcladoc,Mvcaumag,Mvflveac,Mvnumdoc,Mvalfdoc,Tipo,Livello,space(200) as lvlkey, space(150) as CPBmpName,; 
 space(250) as Descri, ordina as ordina from _dett_ order by 1,2,ORDINA, 26,3,25 into cursor _tree_ 
 =wrcursor("_tree_")
      * --- Elimino righe duplicate del Piano di generazione (sono riferite a due righe diverse, eliminandole tutte esclusa la prima che trovo le raggruppo)
      select _tree_ 
 scan for tipo="DPI" 
 odl=olcododl 
 odl1=olcododl1 
 seriale=mvserial 
 riga=recno() 
 go top 
 delete for olcododl=odl and mvserial=seriale and olcododl1<>odl1 and tipo="DPI" 
 go riga 
 endscan
      scan
      * --- Calcolo la chiave della tree-view (lvlkey)
      do case
        case livello=0
          * --- ODL padre di un OCL di fase oppure OCL di salto codice
          this.w_CI = this.w_CI + 1
          this.w_CJ = 1
          this.w_CK = 0
          this.CHKKEY = .F.
          this.w_LVLKEY = right("00000000"+alltrim(str(this.w_CI)),9)
          replace lvlkey with this.w_LVLKEY
        case livello=1
          * --- OCL di fase (questo � l'unico livello che pu� non esserci anche se c'� il livello successivo!)
          this.w_LVLKEY1 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
          replace lvlkey with this.w_LVLKEY1
          this.w_CJ = this.w_CJ + 1
          this.w_CK = 1
          this.CHKKEY = .T.
        case livello=2
          if tipo="DTV"
            this.w_LVLKEY3 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CX)),9)
            replace lvlkey with this.w_LVLKEY3
            this.w_CX = this.w_CX + 1
          else
            if this.w_CK=0 or (! this.CHKKEY and len(alltrim(this.w_LVLKEY2))=19)
              * --- Se il livello 1 non esiste
              if this.w_CK=0
                this.w_CK = this.w_CK + 1
              endif
              this.w_LVLKEY2 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CK)),9)
            else
              * --- Se il livello 1 esiste
              this.w_LVLKEY2 = this.w_LVLKEY1+"."+right("00000000"+alltrim(str(this.w_CK)),9)
            endif
            replace lvlkey with this.w_LVLKEY2
            this.w_CK = this.w_CK + 1
            this.w_CX = 1
          endif
        case livello=3
          if tipo $ "MDM-FAT"
            this.w_LVLKEY4 = this.w_LVLKEY3+"."+right("00000000"+alltrim(str(this.w_CY)),9)
            replace lvlkey with this.w_LVLKEY4
            this.w_CY = this.w_CY + 1
          else
            this.w_LVLKEY3 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CX)),9)
            replace lvlkey with this.w_LVLKEY3
            this.w_CX = this.w_CX + 1
            this.w_CY = 1
            this.CHKKEY = .F.
          endif
        case livello=4
          this.w_LVLKEY4 = this.w_LVLKEY3+"."+right("00000000"+alltrim(str(this.w_CY)),9)
          replace lvlkey with this.w_LVLKEY4
          this.w_CY = this.w_CY + 1
      endcase
      ENDSCAN
      if used("_dett_")
        use in _dett_
      endif
      use in _curs_ 
 update _tree_ set Descri=iif(tipo="DPI",mvserial+" "+; 
 AH_MsgFormat("Piano di spedizione a terzista"),; 
 iif(livello=0,iif(tipo="ODL",olcododl1,olcododl),iif(livello=1,olcododl,iif(livello>1,mvserial+iif(tipo<>"MDM",; 
 AH_MsgFormat(this.w_LABELTDOC,MVTIPDOC),"")+" "+; 
 AH_MsgFormat(this.w_LABELDOC, allt(trans(MVNUMDOC,v_pq(26))))+iif(empty(MVALFDOC),"","/")+MVALFDOC+" "+; 
 AH_MsgFormat(this.w_LABELCMAG, MVCAUMAG),"")))+; 
 " ("+allt(trans(OLTQTODL,v_pq(12)))+" "+nvl(UNIMIS,"")+") "+; 
 AH_MsgFormat(this.w_LABELART,allt(OLTCODIC), allt(ARDESART))), ; 
 CPBmpName = iif(tipo="OCF",this.w_OCF,iif(tipo="ORD",this.w_ORD,iif(tipo="MDM",this.w_MDM,iif(tipo="DTA",this.w_DTA,iif(tipo="DTV",; 
 this.w_DTV,iif(tipo="FAT",this.w_FAT,iif(tipo="ODL",this.w_ODL,iif(tipo="OCL",this.w_OCL,iif(tipo="DPI",this.w_DPI,space(150))))))))))
      activecursor=this.w_cCursor
      Select *,"-" as collapsed from _tree_ order by lvlkey into cursor &activecursor
    else
      * --- Cursore Vuoto
      if used("_curs_")
        use in _curs_
      endif
      ah_ErrorMsg("Non ci sono dati da visualizzare",48)
      * --- Sbianco la tree-view
      this.w_NODITV.Nodes.Clear
    endif
    if used("_tree_")
      use in _tree_
    endif
    if alltrim(upper(this.w_PUNPAD.Class))="TGSCO1KTO"
      this.w_PunPad.oPgFrm.ActivePage = 2
    else
      this.w_PunPad.oPgFrm.ActivePage = 9
    endif
    * --- Indicizzo il cursore e faccio il refresh della tree-view sulla maschera
    if used(this.w_cCursor)
      =wrcursor(this.w_cCursor) 
 index on lvlkey tag lvlkey COLLATE "MACHINE"
    else
      this.w_NODITV.Nodes.Clear
    endif
    this.oParentObject.w_FirstTime = .F.
    this.w_PUNPAD.NotifyEvent("updtreev")     
    if alltrim(upper(this.w_PUNPAD.Class))<>"TGSCO1KTO"
      this.w_PunPad.mhidecontrols()     
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='*TMPOCL_MAST'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='MVM_MAST'
    this.cWorkTables[5]='PAR_PROD'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='RIF_GODL'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
