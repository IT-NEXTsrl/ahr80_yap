* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_kcl                                                        *
*              Caricamento lotti/ubicazioni                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-15                                                      *
* Last revis.: 2014-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_kcl",oParentObject))

* --- Class definition
define class tgsmr_kcl as StdForm
  Top    = 10
  Left   = 8

  * --- Standard Properties
  Width  = 777
  Height = 513
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-02"
  HelpContextID=169251991
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  cPrg = "gsmr_kcl"
  cComment = "Caricamento lotti/ubicazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MMCODICE = space(20)
  w_MMCODART = space(20)
  w_MMKEYSAL = space(20)
  w_MMCODVAR = space(20)
  w_DESART = space(40)
  w_OBTEST = space(10)
  w_CODUBI = space(20)
  w_UM1 = space(3)
  w_MMQTAUM1 = 0
  w_NPROG = 0
  w_FLLOTT = space(1)
  w_CONTA = 0
  w_MAGCAR = space(5)
  w_MAGSCA = space(5)
  w_MGUBICAR = space(1)
  w_MGUBISCA = space(1)
  w_USCITA = .F.
  w_COMME = space(15)
  w_CALCOLA = .F.
  w_ZoomCar = .NULL.
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_kclPag1","gsmr_kcl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUBI_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCar = this.oPgFrm.Pages(1).oPag.ZoomCar
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomCar = .NULL.
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='UBICAZIO'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSMR_BCL(this,"L")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MMCODICE=space(20)
      .w_MMCODART=space(20)
      .w_MMKEYSAL=space(20)
      .w_MMCODVAR=space(20)
      .w_DESART=space(40)
      .w_OBTEST=space(10)
      .w_CODUBI=space(20)
      .w_UM1=space(3)
      .w_MMQTAUM1=0
      .w_NPROG=0
      .w_FLLOTT=space(1)
      .w_CONTA=0
      .w_MAGCAR=space(5)
      .w_MAGSCA=space(5)
      .w_MGUBICAR=space(1)
      .w_MGUBISCA=space(1)
      .w_USCITA=.f.
      .w_COMME=space(15)
      .w_CALCOLA=.f.
      .w_MMCODICE=oParentObject.w_MMCODICE
      .w_MMCODART=oParentObject.w_MMCODART
      .w_MMKEYSAL=oParentObject.w_MMKEYSAL
      .w_MMCODVAR=oParentObject.w_MMCODVAR
      .w_DESART=oParentObject.w_DESART
      .w_UM1=oParentObject.w_UM1
      .w_MMQTAUM1=oParentObject.w_MMQTAUM1
      .w_FLLOTT=oParentObject.w_FLLOTT
      .w_MAGCAR=oParentObject.w_MAGCAR
      .w_MAGSCA=oParentObject.w_MAGSCA
      .w_MGUBICAR=oParentObject.w_MGUBICAR
      .w_MGUBISCA=oParentObject.w_MGUBISCA
          .DoRTCalc(1,5,.f.)
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODUBI))
          .link_1_8('Full')
        endif
          .DoRTCalc(8,9,.f.)
        .w_NPROG = .w_MMQTAUM1
      .oPgFrm.Page1.oPag.ZoomCar.Calculate()
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
          .DoRTCalc(11,16,.f.)
        .w_USCITA = .F.
        .w_COMME = iif(inlist(UPPER(.oParentObject.oParentObject.oParentObject.class),'TGSMR_BSC'),.oParentObject.oParentObject.w_CODCOM,' ')
        .w_CALCOLA = .f.
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MMCODICE=.w_MMCODICE
      .oParentObject.w_MMCODART=.w_MMCODART
      .oParentObject.w_MMKEYSAL=.w_MMKEYSAL
      .oParentObject.w_MMCODVAR=.w_MMCODVAR
      .oParentObject.w_DESART=.w_DESART
      .oParentObject.w_UM1=.w_UM1
      .oParentObject.w_MMQTAUM1=.w_MMQTAUM1
      .oParentObject.w_FLLOTT=.w_FLLOTT
      .oParentObject.w_MAGCAR=.w_MAGCAR
      .oParentObject.w_MAGSCA=.w_MAGSCA
      .oParentObject.w_MGUBICAR=.w_MGUBICAR
      .oParentObject.w_MGUBISCA=.w_MGUBISCA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.ZoomCar.Calculate()
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        * --- Area Manuale = Calculate
        * --- gsmr_kcl
        if .w_calcola
          .SaveDependsOn()
          .NotifyEvent('CheckStato')
        endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.ZoomCar.Calculate()
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODUBI_1_8.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMAGCAR_1_23.visible=!this.oPgFrm.Page1.oPag.oMAGCAR_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oMAGSCA_1_27.visible=!this.oPgFrm.Page1.oPag.oMAGSCA_1_27.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomCar.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUBI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGCAR);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MAGCAR;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_8'),i_cWhere,'GSAR_AUB',"ELENCO UBICAZIONI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGCAR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGCAR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGCAR;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESART_1_5.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_5.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_8.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_8.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oUM1_1_10.value==this.w_UM1)
      this.oPgFrm.Page1.oPag.oUM1_1_10.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page1.oPag.oMMQTAUM1_1_12.value==this.w_MMQTAUM1)
      this.oPgFrm.Page1.oPag.oMMQTAUM1_1_12.value=this.w_MMQTAUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oNPROG_1_13.value==this.w_NPROG)
      this.oPgFrm.Page1.oPag.oNPROG_1_13.value=this.w_NPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTA_1_21.value==this.w_CONTA)
      this.oPgFrm.Page1.oPag.oCONTA_1_21.value=this.w_CONTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGCAR_1_23.value==this.w_MAGCAR)
      this.oPgFrm.Page1.oPag.oMAGCAR_1_23.value=this.w_MAGCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGSCA_1_27.value==this.w_MAGSCA)
      this.oPgFrm.Page1.oPag.oMAGSCA_1_27.value=this.w_MAGSCA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MMQTAUM1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMMQTAUM1_1_12.SetFocus()
            i_bnoObbl = !empty(.w_MMQTAUM1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NPROG >= 0 and .w_NPROG <= .w_MMQTAUM1-.w_CONTA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNPROG_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsmr_kclPag1 as StdContainer
  Width  = 773
  height = 513
  stdWidth  = 773
  stdheight = 513
  resizeXpos=452
  resizeYpos=226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESART_1_5 as StdField with uid="ZLXWOVIZXZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209908170,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=98, Top=8, InputMask=replicate('X',40)


  add object oObj_1_7 as cp_runprogram with uid="IKTKTRDRQJ",left=3, top=542, width=254,height=19,;
    caption='GSMR_BCL(C)',;
   bGlobalFont=.t.,;
    prg="GSMR_BCL('C')",;
    cEvent = "ZOOMSEL row checked",;
    nPag=1;
    , HelpContextID = 228552142

  add object oCODUBI_1_8 as StdField with uid="MHUQLNNIPJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione selezionata per l'inserimento",;
    HelpContextID = 126887974,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=239, Top=69, InputMask=replicate('X',20), bHasZoom = .t. , ForeColor=RGB(0,0,255), cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MAGCAR", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI='S' AND .w_MGUBICAR='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_MAGCAR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_MAGCAR)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUB',"ELENCO UBICAZIONI",'',this.parent.oContained
  endproc
  proc oCODUBI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MAGCAR
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oUM1_1_10 as StdField with uid="GRGGGLKVDJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 169473862,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=542, Top=9, InputMask=replicate('X',3)

  add object oMMQTAUM1_1_12 as StdField with uid="PWZTAHGMOE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MMQTAUM1", cQueryName = "MMQTAUM1",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nella prima unit� di misura",;
    HelpContextID = 58717943,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=631, Top=9, cSayPict="v_pv(28)", cGetPict='"99999999"'

  add object oNPROG_1_13 as StdField with uid="RFHTMKRDST",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NPROG", cQueryName = "NPROG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero matricole da selezionare",;
    HelpContextID = 249235926,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=278, Top=469, cSayPict='"999999999999"', cGetPict='"999999999999"'

  func oNPROG_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NPROG >= 0 and .w_NPROG <= .w_MMQTAUM1-.w_CONTA)
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="QNLSAIHLGU",left=658, top=137, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 86746042;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSVE_BCR(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomCar as cp_zoombox with uid="AWXTJLPKWG",left=10, top=296, width=750,height=156,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="SALDILOT",cZoomFile="GSMR1ZCL",bOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,cMenuFile="",bAdvOptions=.t.,bRetriveAllRows=.f.,cZoomOnZoom="",;
    nPag=1;
    , HelpContextID = 189621274


  add object ZoomSel as cp_szoombox with uid="CZSVCBIDGT",left=10, top=116, width=750,height=164,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="SALDILOT",cZoomFile="GSMR_ZCL",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bReadOnly=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 189621274


  add object oBtn_1_17 as StdButton with uid="HZUGTMEYRQ",left=660, top=455, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare movimenti lotii/ubicazioni";
    , HelpContextID = 86746042;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSMR_BCL(this.Parent.oContained,"L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CONTA>0 and .w_NPROG=0)
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="FRSAGQFIZA",left=712, top=455, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176569414;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_20 as cp_runprogram with uid="HZTIPOIXTU",left=3, top=565, width=254,height=19,;
    caption='GSMR_BCL(U)',;
   bGlobalFont=.t.,;
    prg="GSMR_BCL('U')",;
    cEvent = "ZOOMSEL row unchecked",;
    nPag=1;
    , HelpContextID = 228547534

  add object oCONTA_1_21 as StdField with uid="RCAQEGOCXS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CONTA", cQueryName = "CONTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 243255334,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=111, Top=469, cSayPict='"999999"', cGetPict='"999999"'

  add object oMAGCAR_1_23 as StdField with uid="MKSVILDVZA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MAGCAR", cQueryName = "MAGCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 261207354,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=98, Top=69, InputMask=replicate('X',5)

  func oMAGCAR_1_23.mHide()
    with this.Parent.oContained
      return (Empty(.w_MAGCAR))
    endwith
  endfunc

  func oMAGCAR_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oMAGSCA_1_27 as StdField with uid="RLPRITDPHT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MAGSCA", cQueryName = "MAGSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 262032070,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=98, Top=43, InputMask=replicate('X',5)

  func oMAGSCA_1_27.mHide()
    with this.Parent.oContained
      return (Empty(.w_MAGSCA))
    endwith
  endfunc


  add object oBtn_1_29 as StdButton with uid="IWDUZKSZIR",left=712, top=57, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 86746042;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSMR_BCL(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_37 as cp_runprogram with uid="BPCRIWNKUU",left=3, top=519, width=254,height=19,;
    caption='GSMR_BCL(R)',;
   bGlobalFont=.t.,;
    prg="GSMR_BCL('R')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 228548302


  add object oObj_1_38 as cp_runprogram with uid="RWHOLMQHRG",left=3, top=588, width=254,height=19,;
    caption='GSMR_BCL(X)',;
   bGlobalFont=.t.,;
    prg="GSMR_BCL('X')",;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 228546766


  add object oObj_1_42 as cp_runprogram with uid="QJYMFEFTVH",left=271, top=520, width=254,height=19,;
    caption='GSMR_BCL(V)',;
   bGlobalFont=.t.,;
    prg="GSMR_BCL('V')",;
    cEvent = "CheckStato",;
    nPag=1;
    , HelpContextID = 228547278


  add object oObj_1_43 as cp_runprogram with uid="QTXIRZDKEA",left=270, top=543, width=254,height=19,;
    caption='GSMR_BCL(Q)',;
   bGlobalFont=.t.,;
    prg="GSMR_BCL('Q')",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 228548558

  add object oStr_1_9 as StdString with uid="ROOUJJSJCM",Visible=.t., Left=499, Top=12,;
    Alignment=1, Width=39, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="TTSQTOOQQT",Visible=.t., Left=591, Top=12,;
    Alignment=1, Width=38, Height=20,;
    Caption="Qt�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="KEPFNBBYWH",Visible=.t., Left=106, Top=452,;
    Alignment=0, Width=131, Height=18,;
    Caption="Totale selezionato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="VUMMUAMHTQ",Visible=.t., Left=26, Top=104,;
    Alignment=0, Width=300, Height=18,;
    Caption="Lotti/ubicazioni da selezionare (da scaricare)"    , forecolor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="BUGFQWKMXZ",Visible=.t., Left=4, Top=73,;
    Alignment=1, Width=89, Height=18,;
    Caption="Mag.carico:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (Empty(.w_MAGCAR))
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="EQXKJFWQDI",Visible=.t., Left=3, Top=47,;
    Alignment=1, Width=90, Height=18,;
    Caption="Mag.scarico:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (Empty(.w_MAGSCA))
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="OGJJZWNSXX",Visible=.t., Left=28, Top=284,;
    Alignment=0, Width=267, Height=18,;
    Caption="Lotti/ubicazioni selezionati (da caricare)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="QZWLKFSNTD",Visible=.t., Left=244, Top=452,;
    Alignment=0, Width=140, Height=18,;
    Caption="Totale da selezionare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="TVVIZVMLKA",Visible=.t., Left=234, Top=473,;
    Alignment=1, Width=43, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KBFKZCCBAM",Visible=.t., Left=34, Top=12,;
    Alignment=1, Width=59, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="EKCQIYYMXR",Visible=.t., Left=154, Top=73,;
    Alignment=1, Width=79, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_31 as StdBox with uid="OIDSMVCXDZ",left=107, top=465, width=270,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_kcl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
