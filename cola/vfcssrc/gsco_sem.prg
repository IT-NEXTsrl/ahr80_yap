* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_sem                                                        *
*              Stampa elaborazione fabbisogni                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_24]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-05                                                      *
* Last revis.: 2014-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_sem",oParentObject))

* --- Class definition
define class tgsco_sem as StdForm
  Top    = 2
  Left   = 3

  * --- Standard Properties
  Width  = 754
  Height = 503
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-16"
  HelpContextID=211180183
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  _IDX = 0
  GRUMERC_IDX = 0
  MAGAZZIN_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  CONTI_IDX = 0
  CATEGOMO_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  SEL__MRP_IDX = 0
  cPrg = "gsco_sem"
  cComment = "Stampa elaborazione fabbisogni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_PARAM = space(1)
  w_LLCINI = 0
  w_LLCFIN = 0
  w_SOLOELA = .F.
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_DESINI = space(40)
  w_CODFIN = space(20)
  w_DESFIN = space(40)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_CODCON = space(15)
  w_PROPRE = space(1)
  w_FLIMPE = space(1)
  w_SALTOP = space(1)
  w_TESTO = space(1)
  w_DESMAGI = space(30)
  w_TIPATT = space(1)
  w_DESMAGF = space(30)
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_TIPCON = space(1)
  w_DESFOR = space(40)
  w_MAGTER = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_SALINI = space(40)
  w_SALFIN = space(40)
  w_STAREG = space(1)
  w_ISUGG = space(1)
  w_IPIA = space(1)
  w_ILAN = space(1)
  w_LSUGG = space(1)
  w_LPIA = space(1)
  w_LLAN = space(1)
  w_ESUGG = space(1)
  w_EPIA = space(1)
  w_ELAN = space(1)
  w_FLULELA = space(1)
  o_FLULELA = space(1)
  w_GIANEG = space(1)
  w_CODSER = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_semPag1","gsco_sem",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLLCINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='GRUMERC'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='SEL__MRP'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCO_BEM with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_PARAM=space(1)
      .w_LLCINI=0
      .w_LLCFIN=0
      .w_SOLOELA=.f.
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_CODCON=space(15)
      .w_PROPRE=space(1)
      .w_FLIMPE=space(1)
      .w_SALTOP=space(1)
      .w_TESTO=space(1)
      .w_DESMAGI=space(30)
      .w_TIPATT=space(1)
      .w_DESMAGF=space(30)
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_TIPCON=space(1)
      .w_DESFOR=space(40)
      .w_MAGTER=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_SALINI=space(40)
      .w_SALFIN=space(40)
      .w_STAREG=space(1)
      .w_ISUGG=space(1)
      .w_IPIA=space(1)
      .w_ILAN=space(1)
      .w_LSUGG=space(1)
      .w_LPIA=space(1)
      .w_LLAN=space(1)
      .w_ESUGG=space(1)
      .w_EPIA=space(1)
      .w_ELAN=space(1)
      .w_FLULELA=space(1)
      .w_GIANEG=space(1)
      .w_CODSER=space(10)
        .w_OBTEST = i_DATSYS
        .w_PARAM = this.oParentObject
          .DoRTCalc(3,3,.f.)
        .w_LLCFIN = 999
        .w_SOLOELA = .T.
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODINI))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CODFIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_14('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_15('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_GRUINI))
          .link_1_16('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_17('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CATINI))
          .link_1_18('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CATFIN))
          .link_1_19('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MAGINI))
          .link_1_20('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_21('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODCOM))
          .link_1_22('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODATT))
          .link_1_23('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODCON))
          .link_1_24('Full')
        endif
        .w_PROPRE = 'T'
        .w_FLIMPE = 'T'
        .w_SALTOP = 'N'
        .w_TESTO = 'X'
          .DoRTCalc(25,25,.f.)
        .w_TIPATT = 'A'
          .DoRTCalc(27,29,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(31,41,.f.)
        .w_STAREG = 'V'
        .w_ISUGG = 'M'
        .w_IPIA = 'P'
        .w_ILAN = 'L'
        .w_LSUGG = 'M'
        .w_LPIA = 'P'
        .w_LLAN = 'L'
        .w_ESUGG = 'M'
        .w_EPIA = 'P'
        .w_ELAN = 'L'
        .w_FLULELA = iif(type('.w_PARAM')='O',iif(alltrim(upper(this.oparentobject.Class))="TGSMR_AGM",this.oparentobject.w_GMFLULEL,'S'),'S')
        .w_GIANEG = "S"
        .w_CODSER = iif(type('.w_PARAM')='O',iif(alltrim(upper(this.oparentobject.Class))="TGSMR_AGM",this.oparentobject.w_GMSERIAL,looktab('SEL__MRP','GMSERIAL','GMFLULEL','S')),looktab('SEL__MRP','GMSERIAL','GMFLULEL','S'))
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CODSER))
          .link_1_82('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_80.enabled = this.oPgFrm.Page1.oPag.oBtn_1_80.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_CODINI<>.w_CODINI
          .link_1_9('Full')
        endif
        .DoRTCalc(9,42,.t.)
        if .o_FLULELA<>.w_FLULELA
            .w_ISUGG = 'M'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_IPIA = 'P'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_ILAN = 'L'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_LSUGG = 'M'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_LPIA = 'P'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_LLAN = 'L'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_ESUGG = 'M'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_EPIA = 'P'
        endif
        if .o_FLULELA<>.w_FLULELA
            .w_ELAN = 'L'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(52,54,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_22.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_23.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_24.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCON_1_24.visible=!this.oPgFrm.Page1.oPag.oCODCON_1_24.mHide()
    this.oPgFrm.Page1.oPag.oTESTO_1_28.visible=!this.oPgFrm.Page1.oPag.oTESTO_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESFOR_1_44.visible=!this.oPgFrm.Page1.oPag.oDESFOR_1_44.mHide()
    this.oPgFrm.Page1.oPag.oSTAREG_1_61.visible=!this.oPgFrm.Page1.oPag.oSTAREG_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oISUGG_1_65.visible=!this.oPgFrm.Page1.oPag.oISUGG_1_65.mHide()
    this.oPgFrm.Page1.oPag.oIPIA_1_66.visible=!this.oPgFrm.Page1.oPag.oIPIA_1_66.mHide()
    this.oPgFrm.Page1.oPag.oILAN_1_67.visible=!this.oPgFrm.Page1.oPag.oILAN_1_67.mHide()
    this.oPgFrm.Page1.oPag.oLSUGG_1_68.visible=!this.oPgFrm.Page1.oPag.oLSUGG_1_68.mHide()
    this.oPgFrm.Page1.oPag.oLPIA_1_69.visible=!this.oPgFrm.Page1.oPag.oLPIA_1_69.mHide()
    this.oPgFrm.Page1.oPag.oLLAN_1_70.visible=!this.oPgFrm.Page1.oPag.oLLAN_1_70.mHide()
    this.oPgFrm.Page1.oPag.oESUGG_1_71.visible=!this.oPgFrm.Page1.oPag.oESUGG_1_71.mHide()
    this.oPgFrm.Page1.oPag.oEPIA_1_72.visible=!this.oPgFrm.Page1.oPag.oEPIA_1_72.mHide()
    this.oPgFrm.Page1.oPag.oELAN_1_73.visible=!this.oPgFrm.Page1.oPag.oELAN_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_6'),i_cWhere,'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_9'),i_cWhere,'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_14'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_15'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_16'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_17'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_18'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_19'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_20'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_21'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_22'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_23'),i_cWhere,'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_24'),i_cWhere,'',"Fornitori",'GSCO1KCS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un fornitore di conto lavoro")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTER = NVL(_Link_.ANMAGTER,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_MAGTER = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and not empty(.w_MAGTER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un fornitore di conto lavoro")
        endif
        this.w_CODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_MAGTER = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_82(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_lTable = "SEL__MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2], .t., this.SEL__MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEL__MRP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMSERIAL like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMSERIAL,GMFLULEL,GMGIANEG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMSERIAL',trim(this.w_CODSER))
          select GMSERIAL,GMFLULEL,GMGIANEG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.GMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('SEL__MRP','*','GMSERIAL',cp_AbsName(oSource.parent,'oCODSER_1_82'),i_cWhere,'',"Archivio generazione MRP",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMSERIAL,GMFLULEL,GMGIANEG";
                     +" from "+i_cTable+" "+i_lTable+" where GMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMSERIAL',oSource.xKey(1))
            select GMSERIAL,GMFLULEL,GMGIANEG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMSERIAL,GMFLULEL,GMGIANEG";
                   +" from "+i_cTable+" "+i_lTable+" where GMSERIAL="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMSERIAL',this.w_CODSER)
            select GMSERIAL,GMFLULEL,GMGIANEG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.GMSERIAL,space(10))
      this.w_FLULELA = NVL(_Link_.GMFLULEL,space(1))
      this.w_GIANEG = NVL(_Link_.GMGIANEG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(10)
      endif
      this.w_FLULELA = space(1)
      this.w_GIANEG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])+'\'+cp_ToStr(_Link_.GMSERIAL,1)
      cp_ShowWarn(i_cKey,this.SEL__MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLLCINI_1_3.value==this.w_LLCINI)
      this.oPgFrm.Page1.oPag.oLLCINI_1_3.value=this.w_LLCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLLCFIN_1_4.value==this.w_LLCFIN)
      this.oPgFrm.Page1.oPag.oLLCFIN_1_4.value=this.w_LLCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSOLOELA_1_5.RadioValue()==this.w_SOLOELA)
      this.oPgFrm.Page1.oPag.oSOLOELA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_6.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_6.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_7.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_7.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_9.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_9.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_10.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_10.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_14.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_14.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_15.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_15.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_16.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_16.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_17.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_17.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_18.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_18.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_19.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_19.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_20.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_20.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_21.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_21.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_22.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_22.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_23.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_23.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_24.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_24.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPROPRE_1_25.RadioValue()==this.w_PROPRE)
      this.oPgFrm.Page1.oPag.oPROPRE_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLIMPE_1_26.RadioValue()==this.w_FLIMPE)
      this.oPgFrm.Page1.oPag.oFLIMPE_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSALTOP_1_27.RadioValue()==this.w_SALTOP)
      this.oPgFrm.Page1.oPag.oSALTOP_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTO_1_28.RadioValue()==this.w_TESTO)
      this.oPgFrm.Page1.oPag.oTESTO_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_30.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_30.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_33.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_33.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_39.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_39.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_41.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_41.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_44.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_44.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_47.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_47.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_48.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_48.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_49.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_49.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_53.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_53.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_54.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_54.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_55.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_55.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAREG_1_61.RadioValue()==this.w_STAREG)
      this.oPgFrm.Page1.oPag.oSTAREG_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oISUGG_1_65.RadioValue()==this.w_ISUGG)
      this.oPgFrm.Page1.oPag.oISUGG_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIPIA_1_66.RadioValue()==this.w_IPIA)
      this.oPgFrm.Page1.oPag.oIPIA_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oILAN_1_67.RadioValue()==this.w_ILAN)
      this.oPgFrm.Page1.oPag.oILAN_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLSUGG_1_68.RadioValue()==this.w_LSUGG)
      this.oPgFrm.Page1.oPag.oLSUGG_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLPIA_1_69.RadioValue()==this.w_LPIA)
      this.oPgFrm.Page1.oPag.oLPIA_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLLAN_1_70.RadioValue()==this.w_LLAN)
      this.oPgFrm.Page1.oPag.oLLAN_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESUGG_1_71.RadioValue()==this.w_ESUGG)
      this.oPgFrm.Page1.oPag.oESUGG_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEPIA_1_72.RadioValue()==this.w_EPIA)
      this.oPgFrm.Page1.oPag.oEPIA_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELAN_1_73.RadioValue()==this.w_ELAN)
      this.oPgFrm.Page1.oPag.oELAN_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_82.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_82.value=this.w_CODSER
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_LLCINI<=.w_LLCFIN and .w_LLCINI>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLLCINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LLCINI<=.w_LLCFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLLCFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN)  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and not empty(.w_MAGTER))  and not(g_COLA<>"S")  and (g_COLA="S")  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un fornitore di conto lavoro")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_FLULELA = this.w_FLULELA
    return

enddefine

* --- Define pages as container
define class tgsco_semPag1 as StdContainer
  Width  = 750
  height = 503
  stdWidth  = 750
  stdheight = 503
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLLCINI_1_3 as StdField with uid="RKYWPPNDHG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LLCINI", cQueryName = "LLCINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di inizio selezione",;
    HelpContextID = 87827530,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=125, Top=4, cSayPict='"99"', cGetPict='"99"'

  func oLLCINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LLCINI<=.w_LLCFIN and .w_LLCINI>=0)
    endwith
    return bRes
  endfunc

  add object oLLCFIN_1_4 as StdField with uid="ACQHHCIYYT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LLCFIN", cQueryName = "LLCFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di fine selezione",;
    HelpContextID = 9380938,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=311, Top=4, cSayPict='"999"', cGetPict='"999"'

  func oLLCFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LLCINI<=.w_LLCFIN)
    endwith
    return bRes
  endfunc

  add object oSOLOELA_1_5 as StdCheck with uid="SQDWIIJHCW",rtseq=5,rtrep=.f.,left=531, top=3, caption="Stampa articoli elaborati",;
    ToolTipText = "Stampa solo gli articoli elaborati",;
    HelpContextID = 46502106,;
    cFormVar="w_SOLOELA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOLOELA_1_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oSOLOELA_1_5.GetRadio()
    this.Parent.oContained.w_SOLOELA = this.RadioValue()
    return .t.
  endfunc

  func oSOLOELA_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SOLOELA==.T.,1,;
      0)
  endfunc

  add object oCODINI_1_6 as StdField with uid="DZDJHVMUJE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca di inizio selezione",;
    HelpContextID = 87822810,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=54, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_7 as StdField with uid="SSNEPWHMKY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 87763914,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=295, Top=54, InputMask=replicate('X',40)

  add object oCODFIN_1_9 as StdField with uid="UNDISPFWGH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca di fine selezione",;
    HelpContextID = 9376218,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=79, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  proc oCODFIN_1_9.mDefault
    with this.Parent.oContained
      if empty(.w_CODFIN)
        .w_CODFIN = .w_CODINI
      endif
    endwith
  endproc

  func oCODFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCO_ZAR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESFIN_1_10 as StdField with uid="LMJMLCZYUA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9317322,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=295, Top=79, InputMask=replicate('X',40)

  add object oFAMAINI_1_14 as StdField with uid="BAUZFWPTWC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 258764886,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_15 as StdField with uid="GHDDKOKVIG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 171733078,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_15.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_16 as StdField with uid="VYQYSTJFCH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 87752346,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_17 as StdField with uid="BKTBROMCMD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 9305754,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=175, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_17.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_18 as StdField with uid="WQNLFQEXDD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 87760858,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=198, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_19 as StdField with uid="WHQQEZYTLE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 9314266,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_19.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oMAGINI_1_20 as StdField with uid="WWAXPPIMPZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 87813946,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=272, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMAGFIN_1_21 as StdField with uid="IPBLQAUROZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 9367354,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=295, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_21.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oCODCOM_1_22 as StdField with uid="GGQGKFAWGY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 20058586,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=320, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
   endif
  endfunc

  func oCODCOM_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_23 as StdField with uid="BJCGFCEHYO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 165941722,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=343, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCOM) and g_COMM='S')
    endwith
   endif
  endfunc

  func oCODATT_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oCODCON_1_24 as StdField with uid="GRXGWWUYUW",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un fornitore di conto lavoro",;
    ToolTipText = "Codice fornitore conto lavoro...",;
    HelpContextID = 3281370,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=368, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COLA="S")
    endwith
   endif
  endfunc

  func oCODCON_1_24.mHide()
    with this.Parent.oContained
      return (g_COLA<>"S")
    endwith
  endfunc

  func oCODCON_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fornitori",'GSCO1KCS.CONTI_VZM',this.parent.oContained
  endproc


  add object oPROPRE_1_25 as StdCombo with uid="RPLQSXINOY",rtseq=21,rtrep=.f.,left=135,top=393,width=116,height=21;
    , ToolTipText = "Abilita la selezione sulla provenienza dell'articolo";
    , HelpContextID = 150232586;
    , cFormVar="w_PROPRE",RowSource=""+"Interna,"+"Conto lavoro,"+"Esterna,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROPRE_1_25.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'L',;
    iif(this.value =3,'E',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oPROPRE_1_25.GetRadio()
    this.Parent.oContained.w_PROPRE = this.RadioValue()
    return .t.
  endfunc

  func oPROPRE_1_25.SetRadio()
    this.Parent.oContained.w_PROPRE=trim(this.Parent.oContained.w_PROPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PROPRE=='I',1,;
      iif(this.Parent.oContained.w_PROPRE=='L',2,;
      iif(this.Parent.oContained.w_PROPRE=='E',3,;
      iif(this.Parent.oContained.w_PROPRE=='T',4,;
      0))))
  endfunc


  add object oFLIMPE_1_26 as StdCombo with uid="ONFOJRPBAD",rtseq=22,rtrep=.f.,left=423,top=393,width=135,height=21;
    , ToolTipText = "Saldi articolo";
    , HelpContextID = 152552618;
    , cFormVar="w_FLIMPE",RowSource=""+"Con impegno,"+"Senza impegno,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLIMPE_1_26.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLIMPE_1_26.GetRadio()
    this.Parent.oContained.w_FLIMPE = this.RadioValue()
    return .t.
  endfunc

  func oFLIMPE_1_26.SetRadio()
    this.Parent.oContained.w_FLIMPE=trim(this.Parent.oContained.w_FLIMPE)
    this.value = ;
      iif(this.Parent.oContained.w_FLIMPE=='C',1,;
      iif(this.Parent.oContained.w_FLIMPE=='S',2,;
      iif(this.Parent.oContained.w_FLIMPE=='T',3,;
      0)))
  endfunc


  add object oSALTOP_1_27 as StdCombo with uid="KFNRWRWZXU",rtseq=23,rtrep=.f.,left=423,top=449,width=135,height=21;
    , ToolTipText = "Salto pagina";
    , HelpContextID = 237018842;
    , cFormVar="w_SALTOP",RowSource=""+"Per articolo,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSALTOP_1_27.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSALTOP_1_27.GetRadio()
    this.Parent.oContained.w_SALTOP = this.RadioValue()
    return .t.
  endfunc

  func oSALTOP_1_27.SetRadio()
    this.Parent.oContained.w_SALTOP=trim(this.Parent.oContained.w_SALTOP)
    this.value = ;
      iif(this.Parent.oContained.w_SALTOP=='P',1,;
      iif(this.Parent.oContained.w_SALTOP=='N',2,;
      0))
  endfunc

  add object oTESTO_1_28 as StdCheck with uid="RZOOXPKEEV",rtseq=24,rtrep=.f.,left=135, top=421, caption="Stampa solo testo",;
    ToolTipText = "Stampa solo testo",;
    HelpContextID = 31446326,;
    cFormVar="w_TESTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTESTO_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    'X'))
  endfunc
  func oTESTO_1_28.GetRadio()
    this.Parent.oContained.w_TESTO = this.RadioValue()
    return .t.
  endfunc

  func oTESTO_1_28.SetRadio()
    this.Parent.oContained.w_TESTO=trim(this.Parent.oContained.w_TESTO)
    this.value = ;
      iif(this.Parent.oContained.w_TESTO=='S',1,;
      0)
  endfunc

  func oTESTO_1_28.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oDESMAGI_1_30 as StdField with uid="QONUNYBHAK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 133747766,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=202, Top=272, InputMask=replicate('X',30)

  add object oDESMAGF_1_33 as StdField with uid="EBXVFVWORM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 134687690,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=202, Top=295, InputMask=replicate('X',30)

  add object oDESCOM_1_39 as StdField with uid="BCJHQPAVGJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 19999690,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=272, Top=320, InputMask=replicate('X',30)

  add object oDESATT_1_41 as StdField with uid="TTNMSCEJIW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 165882826,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=272, Top=343, InputMask=replicate('X',30)

  add object oDESFOR_1_44 as StdField with uid="RJLIHNJXRO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 204352458,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=272, Top=368, InputMask=replicate('X',40)

  func oDESFOR_1_44.mHide()
    with this.Parent.oContained
      return (g_COLA<>"S")
    endwith
  endfunc

  add object oDESFAMAI_1_47 as StdField with uid="AOYBYAMKXI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34483073,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=202, Top=104, InputMask=replicate('X',35)

  add object oDESGRUI_1_48 as StdField with uid="ZABORREMDH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117625910,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=202, Top=152, InputMask=replicate('X',35)

  add object oDESCATI_1_49 as StdField with uid="ZFWYARIWSP",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 82760758,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=202, Top=198, InputMask=replicate('X',35)

  add object oDESFAMAF_1_53 as StdField with uid="WOCDSQXKSA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34483076,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=202, Top=128, InputMask=replicate('X',35)

  add object oDESGRUF_1_54 as StdField with uid="PYGQUHDJMJ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 150809546,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=202, Top=175, InputMask=replicate('X',35)

  add object oDESCATF_1_55 as StdField with uid="SXSGGSHCII",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 185674698,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=202, Top=221, InputMask=replicate('X',35)


  add object oSTAREG_1_61 as StdCombo with uid="ODCBMECLIH",rtseq=42,rtrep=.f.,left=423,top=421,width=135,height=21;
    , ToolTipText = "Formato dei moduli di stampa";
    , HelpContextID = 130235354;
    , cFormVar="w_STAREG",RowSource=""+"Orizzontale,"+"Verticale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAREG_1_61.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oSTAREG_1_61.GetRadio()
    this.Parent.oContained.w_STAREG = this.RadioValue()
    return .t.
  endfunc

  func oSTAREG_1_61.SetRadio()
    this.Parent.oContained.w_STAREG=trim(this.Parent.oContained.w_STAREG)
    this.value = ;
      iif(this.Parent.oContained.w_STAREG=='O',1,;
      iif(this.Parent.oContained.w_STAREG=='V',2,;
      0))
  endfunc

  func oSTAREG_1_61.mHide()
    with this.Parent.oContained
      return (.w_TESTO<>'S')
    endwith
  endfunc

  add object oISUGG_1_65 as StdCheck with uid="JPFHADGZDW",rtseq=43,rtrep=.f.,left=639, top=189, caption="Suggerito",;
    ToolTipText = "Stampa articolo con almeno un ODL suggerito MRP",;
    HelpContextID = 22217350,;
    cFormVar="w_ISUGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oISUGG_1_65.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oISUGG_1_65.GetRadio()
    this.Parent.oContained.w_ISUGG = this.RadioValue()
    return .t.
  endfunc

  func oISUGG_1_65.SetRadio()
    this.Parent.oContained.w_ISUGG=trim(this.Parent.oContained.w_ISUGG)
    this.value = ;
      iif(this.Parent.oContained.w_ISUGG=='M',1,;
      0)
  endfunc

  func oISUGG_1_65.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oIPIA_1_66 as StdCheck with uid="PJLOOGMDHT",rtseq=44,rtrep=.f.,left=639, top=214, caption="Pianificato",;
    ToolTipText = "Stampa articolo con almeno un ODL pianificato",;
    HelpContextID = 215760774,;
    cFormVar="w_IPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIPIA_1_66.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oIPIA_1_66.GetRadio()
    this.Parent.oContained.w_IPIA = this.RadioValue()
    return .t.
  endfunc

  func oIPIA_1_66.SetRadio()
    this.Parent.oContained.w_IPIA=trim(this.Parent.oContained.w_IPIA)
    this.value = ;
      iif(this.Parent.oContained.w_IPIA=='P',1,;
      0)
  endfunc

  func oIPIA_1_66.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oILAN_1_67 as StdCheck with uid="ZEKXBWMRCA",rtseq=45,rtrep=.f.,left=639, top=239, caption="Lanciato",;
    ToolTipText = "Stampa articolo con almeno un ODL lanciato",;
    HelpContextID = 216578950,;
    cFormVar="w_ILAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oILAN_1_67.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oILAN_1_67.GetRadio()
    this.Parent.oContained.w_ILAN = this.RadioValue()
    return .t.
  endfunc

  func oILAN_1_67.SetRadio()
    this.Parent.oContained.w_ILAN=trim(this.Parent.oContained.w_ILAN)
    this.value = ;
      iif(this.Parent.oContained.w_ILAN=='L',1,;
      0)
  endfunc

  func oILAN_1_67.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oLSUGG_1_68 as StdCheck with uid="EDNHJQSAAJ",rtseq=46,rtrep=.f.,left=639, top=272, caption="Suggerito",;
    ToolTipText = "Stampa articolo con almeno un OCL suggerito MRP",;
    HelpContextID = 22217398,;
    cFormVar="w_LSUGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLSUGG_1_68.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oLSUGG_1_68.GetRadio()
    this.Parent.oContained.w_LSUGG = this.RadioValue()
    return .t.
  endfunc

  func oLSUGG_1_68.SetRadio()
    this.Parent.oContained.w_LSUGG=trim(this.Parent.oContained.w_LSUGG)
    this.value = ;
      iif(this.Parent.oContained.w_LSUGG=='M',1,;
      0)
  endfunc

  func oLSUGG_1_68.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oLPIA_1_69 as StdCheck with uid="VGAGXJJVWA",rtseq=47,rtrep=.f.,left=639, top=297, caption="Da ordinare",;
    ToolTipText = "Stampa articolo con almeno un OCL da ordinare",;
    HelpContextID = 215760822,;
    cFormVar="w_LPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLPIA_1_69.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oLPIA_1_69.GetRadio()
    this.Parent.oContained.w_LPIA = this.RadioValue()
    return .t.
  endfunc

  func oLPIA_1_69.SetRadio()
    this.Parent.oContained.w_LPIA=trim(this.Parent.oContained.w_LPIA)
    this.value = ;
      iif(this.Parent.oContained.w_LPIA=='P',1,;
      0)
  endfunc

  func oLPIA_1_69.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oLLAN_1_70 as StdCheck with uid="YBSVWNQBFK",rtseq=48,rtrep=.f.,left=639, top=322, caption="Ordinato",;
    ToolTipText = "Stampa articolo con almeno un OCL ordinato",;
    HelpContextID = 216578998,;
    cFormVar="w_LLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLLAN_1_70.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oLLAN_1_70.GetRadio()
    this.Parent.oContained.w_LLAN = this.RadioValue()
    return .t.
  endfunc

  func oLLAN_1_70.SetRadio()
    this.Parent.oContained.w_LLAN=trim(this.Parent.oContained.w_LLAN)
    this.value = ;
      iif(this.Parent.oContained.w_LLAN=='L',1,;
      0)
  endfunc

  func oLLAN_1_70.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oESUGG_1_71 as StdCheck with uid="JUWJLWIMCI",rtseq=49,rtrep=.f.,left=639, top=352, caption="Suggerito",;
    ToolTipText = "Stampa articolo con almeno un ODA suggerito MRP",;
    HelpContextID = 22217286,;
    cFormVar="w_ESUGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESUGG_1_71.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oESUGG_1_71.GetRadio()
    this.Parent.oContained.w_ESUGG = this.RadioValue()
    return .t.
  endfunc

  func oESUGG_1_71.SetRadio()
    this.Parent.oContained.w_ESUGG=trim(this.Parent.oContained.w_ESUGG)
    this.value = ;
      iif(this.Parent.oContained.w_ESUGG=='M',1,;
      0)
  endfunc

  func oESUGG_1_71.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oEPIA_1_72 as StdCheck with uid="SYZDLCQSVG",rtseq=50,rtrep=.f.,left=639, top=377, caption="Da ordinare",;
    ToolTipText = "Stampa articolo con almeno un ODA da ordinare",;
    HelpContextID = 215760710,;
    cFormVar="w_EPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEPIA_1_72.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oEPIA_1_72.GetRadio()
    this.Parent.oContained.w_EPIA = this.RadioValue()
    return .t.
  endfunc

  func oEPIA_1_72.SetRadio()
    this.Parent.oContained.w_EPIA=trim(this.Parent.oContained.w_EPIA)
    this.value = ;
      iif(this.Parent.oContained.w_EPIA=='P',1,;
      0)
  endfunc

  func oEPIA_1_72.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oELAN_1_73 as StdCheck with uid="CRBQKKISIM",rtseq=51,rtrep=.f.,left=639, top=402, caption="Ordinato",;
    ToolTipText = "Stampa articolo con almeno un ODA ordinato",;
    HelpContextID = 216578886,;
    cFormVar="w_ELAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELAN_1_73.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oELAN_1_73.GetRadio()
    this.Parent.oContained.w_ELAN = this.RadioValue()
    return .t.
  endfunc

  func oELAN_1_73.SetRadio()
    this.Parent.oContained.w_ELAN=trim(this.Parent.oContained.w_ELAN)
    this.value = ;
      iif(this.Parent.oContained.w_ELAN=='L',1,;
      0)
  endfunc

  func oELAN_1_73.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc


  add object oBtn_1_79 as StdButton with uid="XAOFRYAIID",left=644, top=453, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa elaborazione fabbisogni";
    , HelpContextID = 211208934;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_79.Click()
      with this.Parent.oContained
        do GSCO_BEM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_80 as StdButton with uid="MJNXVZGNOX",left=695, top=452, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 218497606;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_80.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODSER_1_82 as StdField with uid="OKMFTILHMM",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un seriale valido",;
    ToolTipText = "Seriale generazione MRP",;
    HelpContextID = 214045146,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=639, Top=104, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEL__MRP", oKey_1_1="GMSERIAL", oKey_1_2="this.w_CODSER"

  func oCODSER_1_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_82('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_82.ecpDrop(oSource)
    this.Parent.oContained.link_1_82('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_82.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEL__MRP','*','GMSERIAL',cp_AbsName(this.parent,'oCODSER_1_82'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivio generazione MRP",'',this.parent.oContained
  endproc

  add object oStr_1_8 as StdString with uid="NMLJSVRZGE",Visible=.t., Left=8, Top=57,;
    Alignment=1, Width=123, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="XRBSMYKOYR",Visible=.t., Left=8, Top=82,;
    Alignment=1, Width=123, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GKIMPOMLOZ",Visible=.t., Left=63, Top=4,;
    Alignment=1, Width=60, Height=15,;
    Caption="Da LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="EUAAIOZOQT",Visible=.t., Left=242, Top=4,;
    Alignment=1, Width=64, Height=15,;
    Caption="A LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=3, Top=272,;
    Alignment=1, Width=128, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=3, Top=295,;
    Alignment=1, Width=128, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="WHGWHEWFXM",Visible=.t., Left=6, Top=28,;
    Alignment=0, Width=146, Height=18,;
    Caption="Codice articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="UXMZEKGIAI",Visible=.t., Left=5, Top=250,;
    Alignment=0, Width=115, Height=15,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="ODMTRHWNGJ",Visible=.t., Left=3, Top=320,;
    Alignment=1, Width=128, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MSVOMVFWHL",Visible=.t., Left=3, Top=342,;
    Alignment=1, Width=128, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="UZFXVUFHLH",Visible=.t., Left=3, Top=368,;
    Alignment=1, Width=128, Height=15,;
    Caption="Fornitore C/Lavoro:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (g_COLA<>"S")
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=8, Top=107,;
    Alignment=1, Width=123, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=8, Top=156,;
    Alignment=1, Width=123, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=8, Top=201,;
    Alignment=1, Width=123, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="BETNLUNOYI",Visible=.t., Left=8, Top=131,;
    Alignment=1, Width=123, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=8, Top=178,;
    Alignment=1, Width=123, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=8, Top=223,;
    Alignment=1, Width=123, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="IZCTKFJGKB",Visible=.t., Left=320, Top=421,;
    Alignment=1, Width=97, Height=15,;
    Caption="Modulo stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_TESTO<>'S')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="QHPMXNUDVR",Visible=.t., Left=565, Top=191,;
    Alignment=1, Width=70, Height=18,;
    Caption="Stato ODL:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="FUVIOTZRMF",Visible=.t., Left=566, Top=274,;
    Alignment=1, Width=69, Height=18,;
    Caption="Stato OCL:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="LOCXEGFZJL",Visible=.t., Left=328, Top=449,;
    Alignment=1, Width=89, Height=18,;
    Caption="Salto pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="SSUPOBTUZA",Visible=.t., Left=3, Top=393,;
    Alignment=1, Width=128, Height=15,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="GHEXLDLPXT",Visible=.t., Left=328, Top=393,;
    Alignment=1, Width=89, Height=18,;
    Caption="Stampa articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="YMHPNIZCVM",Visible=.t., Left=565, Top=352,;
    Alignment=1, Width=70, Height=18,;
    Caption="Stato ODA:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (.w_FLULELA<>'S')
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="LWBYTEJDZN",Visible=.t., Left=534, Top=106,;
    Alignment=1, Width=101, Height=18,;
    Caption="Seriale di elab.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_35 as StdBox with uid="AVMMROXYXX",left=6, top=48, width=739,height=1

  add object oBox_1_37 as StdBox with uid="XNHQMLLQVI",left=4, top=264, width=565,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_sem','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
