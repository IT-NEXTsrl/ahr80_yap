* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kbm                                                        *
*              Esportazione/Importazione MS-PROJECT                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-16                                                      *
* Last revis.: 2012-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kbm",oParentObject))

* --- Class definition
define class tgsco_kbm as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 614
  Height = 331+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-22"
  HelpContextID=152459927
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  TAB_CALE_IDX = 0
  cPrg = "gsco_kbm"
  cComment = "Esportazione/Importazione MS-PROJECT"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PP = space(2)
  w_MODEL = space(254)
  w_FILEMODEL = space(254)
  w_DATAELAB = ctod('  /  /  ')
  w_ODL = space(1)
  w_OCL = space(2)
  w_FLPRESUC = space(1)
  w_DATACALIN = ctod('  /  /  ')
  w_TIPODATA = space(1)
  w_LAN = space(1)
  w_PIA = space(1)
  w_SUG = space(1)
  w_FIN = space(1)
  w_NOCIC = space(1)
  w_EXPORTED = space(1)
  w_MSG = space(0)
  w_CALENDAR = space(1)
  w_DATACALFI = ctod('  /  /  ')
  w_PPCALSTA = space(5)
  w_DESCAL = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kbmPag1","gsco_kbm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni")
      .Pages(2).addobject("oPag","tgsco_kbmPag2","gsco_kbm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILEMODEL_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='TAB_CALE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PP=space(2)
      .w_MODEL=space(254)
      .w_FILEMODEL=space(254)
      .w_DATAELAB=ctod("  /  /  ")
      .w_ODL=space(1)
      .w_OCL=space(2)
      .w_FLPRESUC=space(1)
      .w_DATACALIN=ctod("  /  /  ")
      .w_TIPODATA=space(1)
      .w_LAN=space(1)
      .w_PIA=space(1)
      .w_SUG=space(1)
      .w_FIN=space(1)
      .w_NOCIC=space(1)
      .w_EXPORTED=space(1)
      .w_MSG=space(0)
      .w_CALENDAR=space(1)
      .w_DATACALFI=ctod("  /  /  ")
      .w_PPCALSTA=space(5)
      .w_DESCAL=space(40)
        .w_PP = 'PP'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PP))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_FILEMODEL = .w_MODEL
        .w_DATAELAB = i_DATSYS
        .w_ODL = 'I'
        .w_OCL = 'L'
        .w_FLPRESUC = 'S'
        .w_DATACALIN = g_INIESE
        .w_TIPODATA = 'E'
        .w_LAN = 'L'
        .w_PIA = 'P'
        .w_SUG = 'M'
        .w_FIN = 'F'
        .w_NOCIC = 'S'
        .w_EXPORTED = 'N'
          .DoRTCalc(16,16,.f.)
        .w_CALENDAR = "S"
        .w_DATACALFI = g_FINESE
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_PPCALSTA))
          .link_1_32('Full')
        endif
    endwith
    this.DoRTCalc(20,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_PP = 'PP'
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATACALIN_1_9.enabled = this.oPgFrm.Page1.oPag.oDATACALIN_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDATACALFI_1_30.enabled = this.oPgFrm.Page1.oPag.oDATACALFI_1_30.mCond()
    this.oPgFrm.Page1.oPag.oPPCALSTA_1_32.enabled = this.oPgFrm.Page1.oPag.oPPCALSTA_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPPRJMOD,PPCALSTA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PP);
                   +" and PPCODICE="+cp_ToStrODBC(this.w_PP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PP;
                       ,'PPCODICE',this.w_PP)
            select PPCODICE,PPPRJMOD,PPCALSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PP = NVL(_Link_.PPCODICE,space(2))
      this.w_MODEL = NVL(_Link_.PPPRJMOD,space(254))
      this.w_PPCALSTA = NVL(_Link_.PPCALSTA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PP = space(2)
      endif
      this.w_MODEL = space(254)
      this.w_PPCALSTA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCALSTA
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCALSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_PPCALSTA)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_PPCALSTA))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCALSTA)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCALSTA) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oPPCALSTA_1_32'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCALSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_PPCALSTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_PPCALSTA)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCALSTA = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PPCALSTA = space(5)
      endif
      this.w_DESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCALSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILEMODEL_1_3.value==this.w_FILEMODEL)
      this.oPgFrm.Page1.oPag.oFILEMODEL_1_3.value=this.w_FILEMODEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAELAB_1_5.value==this.w_DATAELAB)
      this.oPgFrm.Page1.oPag.oDATAELAB_1_5.value=this.w_DATAELAB
    endif
    if not(this.oPgFrm.Page1.oPag.oODL_1_6.RadioValue()==this.w_ODL)
      this.oPgFrm.Page1.oPag.oODL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOCL_1_7.RadioValue()==this.w_OCL)
      this.oPgFrm.Page1.oPag.oOCL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPRESUC_1_8.RadioValue()==this.w_FLPRESUC)
      this.oPgFrm.Page1.oPag.oFLPRESUC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATACALIN_1_9.value==this.w_DATACALIN)
      this.oPgFrm.Page1.oPag.oDATACALIN_1_9.value=this.w_DATACALIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODATA_1_10.RadioValue()==this.w_TIPODATA)
      this.oPgFrm.Page1.oPag.oTIPODATA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLAN_1_11.RadioValue()==this.w_LAN)
      this.oPgFrm.Page1.oPag.oLAN_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPIA_1_12.RadioValue()==this.w_PIA)
      this.oPgFrm.Page1.oPag.oPIA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSUG_1_13.RadioValue()==this.w_SUG)
      this.oPgFrm.Page1.oPag.oSUG_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIN_1_14.RadioValue()==this.w_FIN)
      this.oPgFrm.Page1.oPag.oFIN_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page1.oPag.oCALENDAR_1_29.RadioValue()==this.w_CALENDAR)
      this.oPgFrm.Page1.oPag.oCALENDAR_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATACALFI_1_30.value==this.w_DATACALFI)
      this.oPgFrm.Page1.oPag.oDATACALFI_1_30.value=this.w_DATACALFI
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCALSTA_1_32.value==this.w_PPCALSTA)
      this.oPgFrm.Page1.oPag.oPPCALSTA_1_32.value=this.w_PPCALSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAL_1_33.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_1_33.value=this.w_DESCAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PPCALSTA))  and (.w_CALENDAR<>"S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPCALSTA_1_32.SetFocus()
            i_bnoObbl = !empty(.w_PPCALSTA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco_kbmPag1 as StdContainer
  Width  = 610
  height = 331
  stdWidth  = 610
  stdheight = 331
  resizeXpos=432
  resizeYpos=204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILEMODEL_1_3 as StdField with uid="GJRUGFGYKQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FILEMODEL", cQueryName = "FILEMODEL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Modello utilizzato per l'esportazione/importazione",;
    HelpContextID = 47157925,;
   bGlobalFont=.t.,;
    Height=21, Width=439, Left=131, Top=6, InputMask=replicate('X',254)


  add object oBtn_1_4 as StdButton with uid="AMLHRKNJUB",left=574, top=7, width=21,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona il file su cui eseguire l'esportazione";
    , HelpContextID = 152660950;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .w_FILEMODEL = GETFILE('mp?')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATAELAB_1_5 as StdField with uid="SDOKLQRWMP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAELAB", cQueryName = "DATAELAB",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data elaborazione",;
    HelpContextID = 106110856,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=132, Top=34

  add object oODL_1_6 as StdCheck with uid="ELUFFMXXNL",rtseq=5,rtrep=.f.,left=16, top=92, caption="ODL",;
    ToolTipText = "Se attivo vengono esportati gli ODL",;
    HelpContextID = 152789990,;
    cFormVar="w_ODL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oODL_1_6.RadioValue()
    return(iif(this.value =1,'I',;
    'X'))
  endfunc
  func oODL_1_6.GetRadio()
    this.Parent.oContained.w_ODL = this.RadioValue()
    return .t.
  endfunc

  func oODL_1_6.SetRadio()
    this.Parent.oContained.w_ODL=trim(this.Parent.oContained.w_ODL)
    this.value = ;
      iif(this.Parent.oContained.w_ODL=='I',1,;
      0)
  endfunc

  add object oOCL_1_7 as StdCheck with uid="WKKIVHNXSF",rtseq=6,rtrep=.f.,left=16, top=121, caption="OCL",;
    ToolTipText = "Se attivo vengono esportati gli OCL",;
    HelpContextID = 152789734,;
    cFormVar="w_OCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOCL_1_7.RadioValue()
    return(iif(this.value =1,'L',;
    'X'))
  endfunc
  func oOCL_1_7.GetRadio()
    this.Parent.oContained.w_OCL = this.RadioValue()
    return .t.
  endfunc

  func oOCL_1_7.SetRadio()
    this.Parent.oContained.w_OCL=trim(this.Parent.oContained.w_OCL)
    this.value = ;
      iif(this.Parent.oContained.w_OCL=='L',1,;
      0)
  endfunc

  add object oFLPRESUC_1_8 as StdCheck with uid="OGVJBSTJIX",rtseq=7,rtrep=.f.,left=16, top=149, caption="Crea predecessori",;
    ToolTipText = "Se attivo vengono create le dipendenze tra gli ODL/OCL",;
    HelpContextID = 256005223,;
    cFormVar="w_FLPRESUC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPRESUC_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLPRESUC_1_8.GetRadio()
    this.Parent.oContained.w_FLPRESUC = this.RadioValue()
    return .t.
  endfunc

  func oFLPRESUC_1_8.SetRadio()
    this.Parent.oContained.w_FLPRESUC=trim(this.Parent.oContained.w_FLPRESUC)
    this.value = ;
      iif(this.Parent.oContained.w_FLPRESUC=='S',1,;
      0)
  endfunc

  add object oDATACALIN_1_9 as StdField with uid="HRNBFORCEJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATACALIN", cQueryName = "DATACALIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inzio calendari per esportazione",;
    HelpContextID = 244114783,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=364, Top=149

  func oDATACALIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CALENDAR<>"S")
    endwith
   endif
  endfunc


  add object oTIPODATA_1_10 as StdCombo with uid="VSSASJBWGV",rtseq=9,rtrep=.f.,left=443,top=178,width=152,height=21;
    , ToolTipText = "Determina quali date verranno esportate in MSProject";
    , HelpContextID = 22369929;
    , cFormVar="w_TIPODATA",RowSource=""+"Date gestionali,"+"Date schedulate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPODATA_1_10.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPODATA_1_10.GetRadio()
    this.Parent.oContained.w_TIPODATA = this.RadioValue()
    return .t.
  endfunc

  func oTIPODATA_1_10.SetRadio()
    this.Parent.oContained.w_TIPODATA=trim(this.Parent.oContained.w_TIPODATA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPODATA=='E',1,;
      iif(this.Parent.oContained.w_TIPODATA=='P',2,;
      0))
  endfunc

  add object oLAN_1_11 as StdCheck with uid="WLWJZGRSAC",rtseq=10,rtrep=.f.,left=218, top=237, caption="Lanciati",;
    ToolTipText = "Se attivo verranno esportati gli ordini lanciati",;
    HelpContextID = 152797366,;
    cFormVar="w_LAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLAN_1_11.RadioValue()
    return(iif(this.value =1,'L',;
    'X'))
  endfunc
  func oLAN_1_11.GetRadio()
    this.Parent.oContained.w_LAN = this.RadioValue()
    return .t.
  endfunc

  func oLAN_1_11.SetRadio()
    this.Parent.oContained.w_LAN=trim(this.Parent.oContained.w_LAN)
    this.value = ;
      iif(this.Parent.oContained.w_LAN=='L',1,;
      0)
  endfunc

  add object oPIA_1_12 as StdCheck with uid="LZVOYADPAX",rtseq=11,rtrep=.f.,left=122, top=237, caption="Pianificati",;
    ToolTipText = "Se attivo verranno esportati gli ordini pianificati",;
    HelpContextID = 152746230,;
    cFormVar="w_PIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPIA_1_12.RadioValue()
    return(iif(this.value =1,'P',;
    'X'))
  endfunc
  func oPIA_1_12.GetRadio()
    this.Parent.oContained.w_PIA = this.RadioValue()
    return .t.
  endfunc

  func oPIA_1_12.SetRadio()
    this.Parent.oContained.w_PIA=trim(this.Parent.oContained.w_PIA)
    this.value = ;
      iif(this.Parent.oContained.w_PIA=='P',1,;
      0)
  endfunc

  add object oSUG_1_13 as StdCheck with uid="EXVMSOJDSI",rtseq=12,rtrep=.f.,left=8, top=237, caption="Suggeriti",;
    ToolTipText = "Se attivo verranno esportati gli ordini suggeriti",;
    HelpContextID = 152773926,;
    cFormVar="w_SUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSUG_1_13.RadioValue()
    return(iif(this.value =1,'M',;
    'X'))
  endfunc
  func oSUG_1_13.GetRadio()
    this.Parent.oContained.w_SUG = this.RadioValue()
    return .t.
  endfunc

  func oSUG_1_13.SetRadio()
    this.Parent.oContained.w_SUG=trim(this.Parent.oContained.w_SUG)
    this.value = ;
      iif(this.Parent.oContained.w_SUG=='M',1,;
      0)
  endfunc

  add object oFIN_1_14 as StdCheck with uid="PPQQHRLRZA",rtseq=13,rtrep=.f.,left=303, top=237, caption="Finiti",;
    ToolTipText = "Se attivo verranno esportati gli ordini finiti",;
    HelpContextID = 152799318,;
    cFormVar="w_FIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFIN_1_14.RadioValue()
    return(iif(this.value =1,'F',;
    'X'))
  endfunc
  func oFIN_1_14.GetRadio()
    this.Parent.oContained.w_FIN = this.RadioValue()
    return .t.
  endfunc

  func oFIN_1_14.SetRadio()
    this.Parent.oContained.w_FIN=trim(this.Parent.oContained.w_FIN)
    this.value = ;
      iif(this.Parent.oContained.w_FIN=='F',1,;
      0)
  endfunc


  add object oBtn_1_16 as StdButton with uid="LVMWVKGNMM",left=443, top=282, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per importare";
    , HelpContextID = 60820458;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSCO_BBM(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="KMIQYENGGV",left=495, top=282, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esportare";
    , HelpContextID = 60820458;
    , Caption='\<Esporta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSCO_BBM(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="MWXKLVEWEK",left=547, top=282, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 25029610;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oCALENDAR_1_29 as StdCombo with uid="GWSFQRDTYL",rtseq=17,rtrep=.f.,left=417,top=92,width=181,height=22;
    , HelpContextID = 230662024;
    , cFormVar="w_CALENDAR",RowSource=""+"Standard MS Project,"+"Calendario gestionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCALENDAR_1_29.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"A",;
    space(1))))
  endfunc
  func oCALENDAR_1_29.GetRadio()
    this.Parent.oContained.w_CALENDAR = this.RadioValue()
    return .t.
  endfunc

  func oCALENDAR_1_29.SetRadio()
    this.Parent.oContained.w_CALENDAR=trim(this.Parent.oContained.w_CALENDAR)
    this.value = ;
      iif(this.Parent.oContained.w_CALENDAR=="S",1,;
      iif(this.Parent.oContained.w_CALENDAR=="A",2,;
      0))
  endfunc

  add object oDATACALFI_1_30 as StdField with uid="IEMDTCHQNJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATACALFI", cQueryName = "DATACALFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inzio calendari per esportazione",;
    HelpContextID = 244114700,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=522, Top=149

  func oDATACALFI_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CALENDAR<>"S")
    endwith
   endif
  endfunc

  add object oPPCALSTA_1_32 as StdField with uid="NHMLPATPJR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PPCALSTA", cQueryName = "PPCALSTA",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario di stabilimento",;
    HelpContextID = 249831369,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=257, Top=121, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_PPCALSTA"

  func oPPCALSTA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CALENDAR<>"S")
    endwith
   endif
  endfunc

  func oPPCALSTA_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCALSTA_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCALSTA_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oPPCALSTA_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oPPCALSTA_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_PPCALSTA
     i_obj.ecpSave()
  endproc

  add object oDESCAL_1_33 as StdField with uid="VQWOQOUELG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 110177226,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=324, Top=121, InputMask=replicate('X',40)

  add object oStr_1_19 as StdString with uid="SBLBFAXFMN",Visible=.t., Left=8, Top=10,;
    Alignment=1, Width=119, Height=18,;
    Caption="Modello MSProject:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ARMQSDVDEC",Visible=.t., Left=13, Top=66,;
    Alignment=0, Width=121, Height=18,;
    Caption="Opzioni esportazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="LYASVKXPLO",Visible=.t., Left=10, Top=36,;
    Alignment=1, Width=117, Height=18,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BVMTKLAMPX",Visible=.t., Left=238, Top=180,;
    Alignment=1, Width=200, Height=18,;
    Caption="Date esportazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PLSVPBGUKB",Visible=.t., Left=2, Top=213,;
    Alignment=0, Width=115, Height=18,;
    Caption="Selezione ordini"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="CVFJJAYYHL",Visible=.t., Left=292, Top=151,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QRVXMHZWPJ",Visible=.t., Left=325, Top=94,;
    Alignment=1, Width=88, Height=18,;
    Caption="Tipo calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="QGMROAGPDC",Visible=.t., Left=449, Top=151,;
    Alignment=1, Width=70, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YMTGNEIBBP",Visible=.t., Left=158, Top=123,;
    Alignment=1, Width=96, Height=18,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="SBDPSULRMY",left=15, top=85, width=583,height=1

  add object oBox_1_26 as StdBox with uid="VKBPSJLADQ",left=1, top=232, width=415,height=1
enddefine
define class tgsco_kbmPag2 as StdContainer
  Width  = 610
  height = 331
  stdWidth  = 610
  stdheight = 331
  resizeXpos=396
  resizeYpos=157
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="DKECUIXAZZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 152773318,;
   bGlobalFont=.t.,;
    Height=322, Width=600, Left=3, Top=7, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kbm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
