* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bgs                                                        *
*              Generazione a scorta                                            *
*                                                                              *
*      Author: TAM Software Srl (SM)                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-08-16                                                      *
* Last revis.: 2017-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPELA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bgs",oParentObject,m.pTIPELA)
return(i_retval)

define class tgsco_bgs as StdBatch
  * --- Local variables
  w_OLCODMAG = space(5)
  pTIPELA = space(1)
  w_GFATHER = .NULL.
  w_CATFIN = space(5)
  w_CATINI = space(5)
  w_CODFIN = space(41)
  w_CODINI = space(41)
  w_FAMAFIN = space(5)
  w_FAMAINI = space(5)
  w_FAMFIN = space(5)
  w_FAMINI = space(5)
  w_GRUFIN = space(5)
  w_GRUINI = space(5)
  w_MAGFIN = space(5)
  w_MAGINI = space(5)
  w_MARFIN = space(5)
  w_MARINI = space(5)
  w_PIAFIN = space(5)
  w_PIAINI = space(5)
  tmpN = 0
  cPERASS = space(3)
  w_OLTDISBA = space(20)
  w_WIPFASPRE = space(5)
  w_WIPFASOLD = space(5)
  w_CPROWORD = 0
  w_CLWIPFPR = space(41)
  w_WIPFPR = space(41)
  w_CLFASCLA = space(1)
  w_CLFPROVE = space(1)
  w_PRRIFFAS = 0
  w_PRCPRIFE = 0
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLCODODL = space(15)
  w_MAXFASE = 0
  w_nRecFasi = 0
  w_CLCOUPOI = space(1)
  w_CLFASOUT = space(1)
  w_CLCODFAS = space(41)
  w_CLFASCOS = space(1)
  w_CLCODODL = space(15)
  w_SERODL = space(15)
  w_OLTSTATODP = space(1)
  w_LOggErr = space(20)
  w_LErrore = space(80)
  w_LMessage = space(0)
  w_KEYRIF = space(10)
  w_CALPRZ = 0
  w_CLKEYRIF = space(10)
  w_DataElab = ctod("  /  /  ")
  cDatIniMPS = ctod("  /  /  ")
  w_NumOrd = 0
  w_LNumErr = 0
  PunPAD = .NULL.
  cDatFinMPS = ctod("  /  /  ")
  w_LSerial = space(10)
  w_LEERR = space(250)
  w_PPNMAXPE = 0
  cDatIniPRO = ctod("  /  /  ")
  w_LSelOpe = space(1)
  w_CALSTA = space(5)
  cMPSSUG = 0
  w_LDettTec = space(1)
  w_CAUORD = space(5)
  w_OLSERIAL = space(15)
  w_LDatini = ctod("  /  /  ")
  w_CAUIMP = space(5)
  w_LDatfin = ctod("  /  /  ")
  w_CAUO_OR = space(1)
  cCODICE = space(20)
  cCODDIS = space(20)
  cCODRIS = space(15)
  w_GENPERMPS = .f.
  w_CAUO_IM = space(1)
  cCODART = space(20)
  w_GENPERODL = .f.
  w_CAUI_OR = space(1)
  cCODVAR = space(20)
  w_GENPEROCL = .f.
  w_CAUI_IM = space(1)
  cKEYSAL = space(20)
  w_OLTFLDAP = space(1)
  w_CAUI_RI = space(1)
  cUNIMIS = space(3)
  i = 0
  cFAMPRO = space(5)
  w_tDATFIN = ctod("  /  /  ")
  w_MAGPRO = space(5)
  NumPer = 0
  w_OLTSTATO = space(1)
  w_OLTSECIC = space(10)
  TmpC = space(100)
  w_OLTFLCON = space(1)
  w_CODICE = space(41)
  cQTASAL = 0
  w_OLTFLDAP = space(1)
  w_OLTPROVE = space(1)
  pCodice = space(40)
  w_OLTFLORD = space(1)
  w_OLTCOFOR = space(15)
  pDatIni = ctod("  /  /  ")
  w_OLTFLIMP = space(1)
  cPROPRE = space(1)
  pDatFin = ctod("  /  /  ")
  w_OLTCOMAG = space(5)
  w_CODCENTRO = space(20)
  pMPSIni = ctod("  /  /  ")
  cPUNLOT = 0
  cPUNRIO = 0
  cLEAFIS = 0
  cCOEFLT = 0
  cLOTMED = 0
  cLEAPRO = 0
  cMAGPRE = space(5)
  cLEAMPS = 0
  w_OLTCONTR = space(15)
  w_DATORD = ctod("  /  /  ")
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_DATFIL = ctod("  /  /  ")
  w_ROWNUM = 0
  w_ROWORD = 0
  w_OLCODICE = space(20)
  w_OLCODART = space(20)
  w_OLUNIMIS = space(3)
  w_OLCOEIMP = 0
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OLTIPPRE = space(1)
  w_OLMAGPRE = space(5)
  w_ARTIPGES = space(1)
  w_OLCAUMAG = space(5)
  w_OLFLORDI = space(1)
  w_OLFLIMPE = space(1)
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_OLFLEVAS = space(1)
  w_OLEVAAUT = space(1)
  w_OLQTASAL = 0
  w_RIFFAS = 0
  w_CPRFAS = 0
  w_ULTFAS = 0
  w_PIANIF = .f.
  w_GENPERODA = .f.
  w_ARUNMIS2 = space(2)
  w_AROPERAT = space(1)
  w_UMLIPREZZO = space(3)
  w_OLTUNMIS = space(5)
  w_LIUNIMIS = space(3)
  w_ARMOLTIP = 0
  w_APQTAUM1 = 0
  w_ARPREZUM = space(1)
  w_APQTAUM1 = 0
  w_OLTQTOD1 = 0
  w_OLTQTODL = 0
  w_CONUMROW = space(10)
  w_TIPGES = space(1)
  w_PRCRIFOR = space(1)
  w_LOTSMUL = 0
  w_QTAMIN = 0
  w_LOTRIO = 0
  w_QTAMINDEF = 0
  w_LOTRIODEF = 0
  w_GIOAPP = 0
  w_ARFLCOMM = space(1)
  w_CENCOS = space(15)
  w_VOCCOS = space(15)
  w_PPCENCOS = space(15)
  w_PPCCSOCL = space(15)
  w_PPCCSODA = space(15)
  w_QTARES = 0
  cQTAMAX = 0
  w_MICOM = space(1)
  w_MIODL = space(1)
  w_MIOCL = space(1)
  w_MIODA = space(1)
  w_MAGFOR = space(5)
  w_MAGFOC = space(5)
  w_MAGFOL = space(5)
  w_MAGFOA = space(5)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_OLTCICLO = space(15)
  w_CLINDCIC = space(2)
  w_CORLEA = 0
  w_COROWNUM = 0
  w_DISPAD = space(20)
  w_NUMLEV = 0
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_TIPART = space(1)
  w_Dateva = ctod("  /  /  ")
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_TIPLISBP = space(1)
  w_TIPLISPP = space(1)
  w_FLPROMO = space(1)
  w_SELEZ = space(1)
  w_OLTCOART = space(20)
  w_DULTIFAS = space(40)
  w_CPULTFAS = 0
  w_PPMATINP = space(1)
  w_PPMATOUP = space(1)
  w_MATOUP = space(1)
  w_DATFAS = ctod("  /  /  ")
  w_OLTDINRIC = ctod("  /  /  ")
  w_ULTIMAFASE = 0
  w_CPULTFASE = 0
  w_WIPFASEPREC = space(41)
  w_WIPFASEOLD = space(41)
  w_PRIMOGIRO = .f.
  w_FASCOS = space(1)
  w_CLFASSEL = space(1)
  QTC = 0
  w_appo1 = space(10)
  w_PZ = 0
  w_S1 = 0
  w_S2 = 0
  w_S3 = 0
  w_S4 = 0
  w_VAL = space(3)
  w_CAO = 0
  * --- WorkFile variables
  ART_PROD_idx=0
  CAM_AGAZ_idx=0
  DISMBASE_idx=0
  MPS_TPER_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  PAR_RIOR_idx=0
  PRD_ERRO_idx=0
  SALDIART_idx=0
  CONTI_idx=0
  CON_TRAM_idx=0
  CON_TRAD_idx=0
  ODL_RISO_idx=0
  TAB_CICL_idx=0
  ODL_DETT_idx=0
  ODL_SMPL_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  ODLMRISO_idx=0
  ODL_MOUT_idx=0
  CIC_DETT_idx=0
  ODL_CICL_idx=0
  TMPGSDB_MRL_idx=0
  TMPGSCO_MMO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- PIANIFICAZIONE MATERIALI A SCORTA
    * --- M=Multipla (non da messaggio di completamento), S=Singola
    if TYPE("this.oParentObject.oParentObject.class")="C" and UPPER(this.oParentObject.oParentObject.class) = "TGSDB_KGF"
      this.w_GFATHER = this.oParentObject.oParentObject
    else
      if TYPE("this.oParentObject.class")="C" and UPPER(this.oParentObject.class) $ "TGSCO_KGS-TGSCO_KGA"
        this.w_GFATHER = this.oParentObject
      endif
    endif
    if VARTYPE(this.w_GFATHER.class) = "C"
      this.w_CATFIN = this.w_GFATHER.w_CATFIN
      this.w_CATINI = this.w_GFATHER.w_CATINI
      this.w_CODFIN = this.w_GFATHER.w_CODFIN
      this.w_CODINI = this.w_GFATHER.w_CODINI
      this.w_FAMAFIN = this.w_GFATHER.w_FAMAFIN
      this.w_FAMAINI = this.w_GFATHER.w_FAMAINI
      this.w_GRUFIN = this.w_GFATHER.w_GRUFIN
      this.w_GRUINI = this.w_GFATHER.w_GRUINI
      this.w_MAGFIN = this.w_GFATHER.w_MAGFIN
      this.w_MAGINI = this.w_GFATHER.w_MAGINI
      this.w_MARFIN = this.w_GFATHER.w_MARFIN
      this.w_MARINI = this.w_GFATHER.w_MARINI
    endif
    * --- w_ODA='S': la procedura genera esclusivamente le ODA per gli articoli di provenienza preferenziale esterna, 
    *     w_ODA='N': la procedura genera ODL/OCL per articoli pref. interni/conto lavoro ignorando gli articoli pref. esterni.
    * --- Punta al padre
    this.PunPAD = this.oParentObject
    this.w_MICOM = this.PunPAD.w_MICOM
    this.w_MIODL = this.PunPAD.w_MIODL
    this.w_MIOCL = this.PunPAD.w_MIOCL
    this.w_MIODA = this.PunPAD.w_MIODA
    this.w_MAGFOR = this.PunPAD.w_MAGFOR
    this.w_MAGFOC = this.PunPAD.w_MAGFOC
    this.w_MAGFOL = this.PunPAD.w_MAGFOL
    this.w_MAGFOA = this.PunPAD.w_MAGFOA
    if this.pTIPELA="S"
      * --- Verifico campi obbligatori
      if (this.w_MICOM="F" and empty (this.w_MAGFOR)) or (this.w_MIODL="F" and empty (this.w_MAGFOC)) or (this.w_MIOCL="F" and empty (this.w_MAGFOL)) or (this.w_MIODA="F" and empty (this.w_MAGFOA))
        ah_errormsg("Campo obbligatorio!",48)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Data di partenza elaborazione
    this.w_SELEZ = "N"
    this.w_DataElab = i_datsys
    this.w_DATORD = i_DATSYS
    * --- Gestione errori
    this.w_LNumErr = 0
    this.w_LSerial = space(10)
    this.w_LSelOpe = "U"
    this.w_LDettTec = "N"
    this.w_LDatini = i_DATSYS
    this.w_LDatfin = i_DATSYS
    * --- Flags riassegnazione periodi MPS
    this.w_GENPERMPS = .F.
    this.w_GENPERODL = .F.
    this.w_GENPEROCL = .F.
    * --- Legge da tabella parametri il calendario std
    this.w_MAGPRO = " "
    this.w_PPNMAXPE = 0
    this.w_CALSTA = " "
    this.w_CAUORD = " "
    this.w_CAUIMP = " "
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUORD,PPCAUIMP,PPCALSTA,PPNMAXPE,PPMAGPRO,PPCENCOS,PPCCSODA"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUORD,PPCAUIMP,PPCALSTA,PPNMAXPE,PPMAGPRO,PPCENCOS,PPCCSODA;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.w_CAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      this.w_CALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
      this.w_PPNMAXPE = NVL(cp_ToDate(_read_.PPNMAXPE),cp_NullValue(_read_.PPNMAXPE))
      this.w_MAGPRO = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      this.w_PPCCSODA = NVL(cp_ToDate(_read_.PPCCSODA),cp_NullValue(_read_.PPCCSODA))
      this.w_PPCCSOCL = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NumOrd = 0
    this.w_PPNMAXPE = iif(this.w_PPNMAXPE=0,54,this.w_PPNMAXPE)
    if empty(this.w_CALSTA)
      ah_ErrorMSG("Calendario di stabilimento non specificato (tabella parametri)",16)
      i_retcode = 'stop'
      return
    endif
    if empty(this.w_CAUORD) or empty(this.w_CAUIMP)
      this.TmpC = "Causali di default non definite in tabella parametri"
      ah_ErrorMsg(this.TmpC,16)
      i_retcode = 'stop'
      return
    endif
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Crea il File delle Messaggistiche di Errore
    CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(20), ERRORE C(80), TESMES M(10))
    * --- Legge FLAGS Causali
    this.w_CAUO_OR = " "
    this.w_CAUO_IM = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLORDI,CMFLIMPE"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_CAUORD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLORDI,CMFLIMPE;
        from (i_cTable) where;
            CMCODICE = this.w_CAUORD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUO_OR = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_CAUO_IM = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAUI_OR = " "
    this.w_CAUI_IM = " "
    this.w_CAUI_RI = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLORDI,CMFLIMPE,CMFLRISE"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_CAUIMP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLORDI,CMFLIMPE,CMFLRISE;
        from (i_cTable) where;
            CMCODICE = this.w_CAUIMP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUI_OR = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_CAUI_IM = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_CAUI_RI = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from MPS_TPER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MPS_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" MPS_TPER where ";
            +"1 = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            1 = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows = 0
      * --- Calcola bidoni temporali ....
      do GSCO_BCB with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_KEYRIF = SYS(2015)
    this.w_CLKEYRIF = this.w_KEYRIF
    * --- Pagina 2
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_ODA="N"
      * --- Eventuale generazione conto lavoro
      if g_PRFA="S" and g_CICLILAV="S"
        * --- Lancia pianificazione del conto lavoro per gli articoli gestiti a scorta ma non Oggetti MPS o Ricambi
        GSCI_BCO(this,"MRP")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Controllo errori
    if this.pTipEla="S"
      if this.w_LNumErr=0
        if this.w_NumOrd=0
          this.TmpC = "Elaborazione completata%0Nessun ordine generato"
        else
          this.TmpC = "Elaborazione completata%0Totale n. %1 ordini generati"
        endif
        ah_ErrorMsg(this.TmpC,"!","", ALLTRIM(STR(this.w_NumOrd)))
      else
        this.TmpC = "Pianificazione materiali gestiti a scorta%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?"
        if ah_YesNo(this.TmpC,"", alltrim(str(this.w_LNumErr,5,0)) )
          SELECT * FROM MessErr INTO CURSOR __TMP__
          CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
        endif
      endif
      * --- Chiude Padre
      this.PunPAD.ECPQuit()     
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea array periodi
    dimension PeriodiMPS(100)
    vq_exec ("..\COLA\EXE\QUERY\GSCO_BRI", this, "_PeriodiMPS_")
    Cur=WrCursor("_PeriodiMPS_")
    select * from _PeriodiMPS_ into array PeriodiMPS
    use in _PeriodiMPS_
    if this.oParentObject.w_ODA="N"
      if g_PRFA="S" and g_CICLILAV="S"
        ah_msg("Lettura cicli di lavorazione... ")
        this.w_CLINDCIC = "00"
        vq_exec("..\PRFA\EXE\QUERY\GSCIDBCS", this, "_CicloPref_")
      endif
      if g_PRFA="S" and g_CICLILAV="S" and g_COLA = "S"
        vq_exec("..\COLA\EXE\QUERY\GSCOFBCS", this, "_CicExtMono_")
        vq_exec("..\COLA\EXE\QUERY\GSCOEBCS", this, "Terzista")
      endif
    endif
    * --- Cursore con gli articoli gestiti a scorta ...<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    ah_msg("Lettura dati gestione a scorta in corso...")
    if this.oParentObject.w_ODA="N"
      vq_exec ("..\COLA\EXE\QUERY\GSCO1BGS", this, "_Globale_")
    else
      vq_exec ("..\COLA\EXE\QUERY\GSCO4BGS", this, "_Globale_")
    endif
    this.w_PIANIF = FALSE
    * --- Scan
    if used("_Globale_")
      select _Globale_
      Scan for nvl(QTASAL,0) < nvl(PUNRIO,0) and not empty(nvl(CODART,""))
      * --- Per query ...
      this.pCodice = _Globale_.CODART
      this.pDatIni = i_DATSYS
      this.pDatFin = cp_CharToDate("  -  -  ")
      this.pMPSIni = cp_CharToDate("  -  -  ")
      * --- Prepara dati ...
      this.cCODICE = nvl(_Globale_.CODART,space(20))
      this.cCODART = nvl(_Globale_.CODART,space(20))
      this.cCODDIS = nvl(_Globale_.DISTINTA,space(20))
      this.cCODRIS = nvl(_Globale_.CODRIS,space(15))
      this.w_OLTCICLO = nvl(_Globale_.CODRIS,space(15))
      * --- REM: gli articoli con varianti non possono essere gestiti a scorta
      this.cKEYSAL = this.cCODART
      this.cUNIMIS = nvl(_Globale_.UNMIS1,space(3))
      this.cQTASAL = nvl(_Globale_.QTASAL,0)
      * --- Conteggia Lead Time di approvvigionamento (produzione)
      this.cPROPRE = nvl(_Globale_.PROPRE,"I")
      this.cLEAMPS = nvl(_Globale_.LEAMPS,0)
      this.cPUNLOT = nvl(_Globale_.PUNLOT,0)
      this.cPUNRIO = nvl(_Globale_.PUNRIO,0)
      this.cQTAMAX = nvl(_Globale_.QTAMAX,0)
      this.cLEAFIS = nvl(_Globale_.LEAFIS,0)
      this.cCOEFLT = nvl(_Globale_.COEFLT,0)
      this.cLOTMED = nvl(_Globale_.LOTMED,0)
      this.cMAGPRE = nvl(_Globale_.MAGPRE,space(5))
      this.w_ARFLCOMM = nvl(_Globale_.ARFLCOMM,space(1))
      this.w_CENCOS = iif(!empty(_Globale_.ARCENCOS),_Globale_.ARCENCOS,iif(_Globale_.propre="I",this.w_PPCENCOS,iif(_Globale_.propre="L",this.w_PPCCSOCL,this.w_PPCCSODA)))
      this.w_VOCCOS = nvl(_Globale_.ARVOCCEN,space(15))
      * --- --Dati per calcolo listino
      this.w_ARUNMIS2 = nvl(_Globale_.ARUNMIS2,space(3))
      this.w_AROPERAT = nvl(_Globale_.AROPERAT,space(1))
      this.w_ARMOLTIP = nvl(_Globale_.ARMOLTIP,0)
      this.w_ARPREZUM = nvl(_Globale_.ARPREZUM,space(1))
      this.w_OLTUNMIS = this.cUNIMIS
      * --- Cerca il ciclo preferenziale
      this.w_OLTCICLO = space(15)
      this.w_OLTSECIC = SPACE(10)
      if this.oParentObject.w_ODA="N"
        if g_PRFA="S" and g_CICLILAV="S" and used("_CicloPref_")
          this.w_CODICE = nvl(_Globale_.DISTINTA,SPACE(20))
          Select _CicloPref_ 
 go top
          locate for clcoddis=this.cCODDIS AND clcodart=this.cCODART
          this.w_OLTSECIC = iif(found(), _CicloPref_.CLSERIAL, space(10))
        endif
      endif
      if this.oParentObject.w_ODA="N"
        * --- Determina provenienza
        this.w_OLTPROVE = "I"
        this.w_OLTCOFOR = space(15)
        if g_COLA = "S"
          * --- Articolo con provenienza preferenziale CL
          if g_PRFA="S" and g_CICLILAV="S" and not empty(this.w_OLTSECIC)
            * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno, da trattare come salto codice
            * --- Usa come filtro w_OLTSECIC
            if used("_CicExtMono_")
              select _CicExtMono_ 
 go top 
 locate for clserial=this.w_OLTSECIC
              if found()
                this.w_OLTPROVE = "L"
                this.w_CODCENTRO = nvl(_CicExtMono_.LRCODRIS ," ")
                this.w_OLTCOFOR = space(15)
                if used("Terzista")
                  Select Terzista 
 go top 
 locate for lfcodice=this.w_CODCENTRO
                  this.w_OLTCOFOR = iif(found(), lfcodfor, space(15))
                endif
              endif
            endif
          else
            * --- Il modulo Cicli non c'� o non � abilitato => E' un articolo di CL con salto di codice => mette prov. CL
            if this.cPROPRE = "L"
              * --- Read from PAR_RIOR
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PRCODFOR"+;
                  " from "+i_cTable+" PAR_RIOR where ";
                      +"PRCODART = "+cp_ToStrODBC(this.cCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PRCODFOR;
                  from (i_cTable) where;
                      PRCODART = this.cCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_OLTPROVE = "L"
            endif
          endif
        endif
      else
        * --- Determina provenienza
        this.w_OLTPROVE = "E"
        this.w_OLTCOFOR = space(15)
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRCODFOR,PRLOTRIO,PRQTAMIN,PRGIOAPP"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.cCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRCODFOR,PRLOTRIO,PRQTAMIN,PRGIOAPP;
            from (i_cTable) where;
                PRCODART = this.cCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
          this.w_LOTRIODEF = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
          this.w_QTAMINDEF = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
          this.w_GIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Calcola LT
      this.cLEAPRO = this.cLEAFIS + iif(this.cLOTMED=0, 0, this.cLEAFIS * this.cCOEFLT * (this.cPUNLOT/this.cLOTMED-1))
      this.pDatFin = COCALCLT(this.pDatIni,this.cLEAPRO,"A",.F. , "")
      this.pMPSIni = COCALCLT(this.pDatFin,this.cLEAMPS,"I",.F. , "")
      * --- Lancia query per estrarre ordini di produzione da oggi a oggi+LT
      vq_exec ("..\COLA\EXE\QUERY\GSCO2BGS", this, "_TempODP_")
      * --- Legge Valore
      if used("_TempODP_")
        * --- Aggiorna cQTASAL
        select _TempODP_
        SCAN
        this.cQTASAL = this.cQTASAL + nvl(_TempODP_.ORDPRO, 0) + nvl(_TempODP_.ORDFOR, 0)
        ENDSCAN
        * --- Chiude Cursore
        USE IN _TempODP_
      endif
      * --- Se il saldo a 'oggi+LT' � ancora sotto il punto di riordino, allora suggerisce ODP
      if this.cQTASAL < this.cPUNRIO
        * --- Suggerimento
        this.cMPSSUG = this.cPUNLOT
        * --- Emette ODP
        if this.cMPSSUG > 0
          * --- Determina a quale periodo appartiene la data di fine lavorazione
          this.tmpN = 1
          do while this.tmpN <= ALen(PeriodiMPS,1)
            if PeriodiMPS(this.tmpN,3) <= this.pDATFIN and this.pDATFIN <=PeriodiMPS(this.tmpN,4)
              this.cPERASS = PeriodiMPS(this.tmpN,1)
              this.tmpN = 99999
            endif
            this.tmpN = this.tmpN+1
          enddo
          * --- Conteggia Lead Time MPS
          this.cDatIniMPS = this.pMPSINI
          this.cDatFinMPS = this.pDATFIN
          this.cDatIniPRO = this.pDATINI
          if this.oParentObject.w_ODA="N"
            if g_COLA = "S" and this.cPROPRE = "L"
              * --- Articolo con provenienza preferenziale CL
              this.w_OLTPROVE = "L"
            endif
            if this.oParentObject.w_PRVMPS="S" and this.cPROPRE = "E"
              this.w_OLTPROVE = "E"
            endif
          else
            this.w_OLTPROVE = "E"
          endif
          this.w_OLTCOMAG = iif(empty(this.cMAGPRE),iif(empty(this.w_MAGPRO),g_MAGAZI,this.w_MAGPRO),this.cMAGPRE)
          this.w_OLTCOMAG = iif(this.w_OLTPROVE="I",iif(this.w_MIODL="M",this.w_OLTCOMAG,this.w_MAGFOC),iif(this.w_OLTPROVE="L",iif(this.w_MIOCL="M",this.w_OLTCOMAG,this.w_MAGFOL),iif(this.w_MIODA="M",this.w_OLTCOMAG,this.w_MAGFOA)))
          this.w_OLTSTATO = iif( nvl(_Globale_.OGGMPS, "N") $ "SR", "C", "P")
          if this.w_OLTSTATO="C"
            * --- Flag MPS
            this.w_OLTFLCON = iif(this.w_OLTPROVE<>"E","+","")
            this.w_OLTFLDAP = iif(this.w_OLTPROVE="E","+","")
          endif
          this.w_OLTFLORD = this.w_CAUO_OR
          this.w_OLTFLIMP = this.w_CAUO_IM
          * --- --Determinazione del listino applicabile
          this.w_QTAMIN = 0
          this.w_LOTRIO = 0
          this.w_OLTPROVE = "I"
          this.w_OLTCONTR = SPACE(15)
          if this.oParentObject.w_ODA="N"
            this.w_OLTCOFOR = SPACE(15)
            if g_COLA = "S"
              * --- Articolo con provenienza preferenziale CL
              if this.cPROPRE = "L"
                * --- Read from PAR_RIOR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PRCODFOR"+;
                    " from "+i_cTable+" PAR_RIOR where ";
                        +"PRCODART = "+cp_ToStrODBC(this.cCODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PRCODFOR;
                    from (i_cTable) where;
                        PRCODART = this.cCODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_OLTPROVE = "L"
              endif
              if this.w_OLTPROVE="L" AND NOT EMPTY(this.w_OLTCOFOR)
                * --- Seleziona il contratto
                * --- Select from CON_TRAM
                i_nConn=i_TableProp[this.CON_TRAM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select CONUMERO  from "+i_cTable+" CON_TRAM ";
                      +" where COTIPCLF='F' and COCODCLF="+cp_ToStrODBC(this.w_OLTCOFOR)+" and CODATINI<="+cp_ToStrODBC(this.w_DATORD)+" and "+cp_ToStrODBC(this.w_DATORD)+"<=CODATFIN";
                      +" order by CODATINI";
                       ,"_Curs_CON_TRAM")
                else
                  select CONUMERO from (i_cTable);
                   where COTIPCLF="F" and COCODCLF=this.w_OLTCOFOR and CODATINI<=this.w_DATORD and this.w_DATORD<=CODATFIN;
                   order by CODATINI;
                    into cursor _Curs_CON_TRAM
                endif
                if used('_Curs_CON_TRAM')
                  select _Curs_CON_TRAM
                  locate for 1=1
                  do while not(eof())
                  * --- Select from CON_TRAD
                  i_nConn=i_TableProp[this.CON_TRAD_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2],.t.,this.CON_TRAD_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select CONUMERO  from "+i_cTable+" CON_TRAD ";
                        +" where CONUMERO="+cp_ToStrODBC(_Curs_CON_TRAM.CONUMERO)+" and COCODART="+cp_ToStrODBC(this.cCODART)+"";
                         ,"_Curs_CON_TRAD")
                  else
                    select CONUMERO from (i_cTable);
                     where CONUMERO=_Curs_CON_TRAM.CONUMERO and COCODART=this.cCODART;
                      into cursor _Curs_CON_TRAD
                  endif
                  if used('_Curs_CON_TRAD')
                    select _Curs_CON_TRAD
                    locate for 1=1
                    do while not(eof())
                    this.w_OLTCONTR = _Curs_CON_TRAD.CONUMERO
                      select _Curs_CON_TRAD
                      continue
                    enddo
                    use
                  endif
                    select _Curs_CON_TRAM
                    continue
                  enddo
                  use
                endif
              endif
            endif
            if this.oParentObject.w_PRVMPS="S" and this.cPROPRE = "E"
              this.w_OLTPROVE = "E"
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            this.w_OLTPROVE = "E"
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Try
          local bErr_0517D660
          bErr_0517D660=bTrsErr
          this.Try_0517D660()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_LOggErr = this.cCODICE
            this.w_LErrore = AH_MSGFORMAT("Gestione a scorta: impossibile generare ordini")
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_0517D660
          * --- End
        endif
      endif
      EndScan
      * --- Chiude Cursori
      USE IN SELECT("_Globale_")
    endif
    if this.w_PIANIF and this.oParentObject.w_ODA="N"
      * --- Se quando esce ci sono ancora degli ODL in stato K, devono essere cancellati con i relativi saldi
      * --- Try
      local bErr_0515BCA0
      bErr_0515BCA0=bTrsErr
      this.Try_0515BCA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_0515BCA0
      * --- End
    endif
    * --- Rilascia array periodi MPS
    Release PeriodiMPS
    USE IN SELECT("_MatTer_")
    if used("Terzista")
      Use in Terzista
    endif
  endproc
  proc Try_0517D660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- EMETTE ODP
    this.w_QTARES = this.cMPSSUG
    do while this.w_QTARES>0
      * --- Questo ciclo serve a spezzare gli ordini che non rispettano il vincolo di quantit� massima, se il vincolo non c'� la procedura fa sempre un solo 'giro'
      if this.cQTAMAX<>0 and this.w_QTARES>this.cQTAMAX
        * --- Applico politica di lottizzazione quantit� massima
        this.cMPSSUG = this.cQTAMAX
        * --- Fabbisogno residuo (da valutare al prossimo giro)
        this.w_QTARES = this.w_QTARES-this.cMPSSUG
      else
        if this.cQTAMAX=0
          * --- Se la quantit� massima non � gestita faccio un solo giro nello while
          this.w_QTARES = 0
        else
          * --- Se sono all'ultimo giro tengo l'avanzo
          this.cMPSSUG = this.w_QTARES
          this.w_QTARES = 0
        endif
      endif
      * --- Assegna Progressivo ODP
      this.w_OLSERIAL = space(15)
      this.w_OLSERIAL = cp_GetProg("ODL_MAST","PRODL",this.w_OLSERIAL,i_CODAZI)
      * --- Inserisce ordine pianificato
      if this.oParentObject.w_ODA="N" and this.w_OLTPROVE<>"E"
        if g_PRFA="S" and g_CICLILAV="S"
          * --- Insert into ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"OLCODODL"+",OLDATODL"+",OLDATODP"+",OLOPEODL"+",OLOPEODP"+",OLTCAMAG"+",OLTCOART"+",OLTCODIC"+",OLTCOFOR"+",OLTCOMAG"+",OLTCONTR"+",OLTDINRIC"+",OLTDISBA"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLEVA"+",OLTFLIMP"+",OLTFLORD"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",OLTVARIA"+",UTCC"+",UTDC"+",OLTFLCON"+",OLTSECIC"+",OLTDTCON"+",OLTSAFLT"+",OLTCOCEN"+",OLTVOCEN"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_MAST','OLCODODL');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'ODL_MAST','OLTCOART');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODICE),'ODL_MAST','OLTCODIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOFOR),'ODL_MAST','OLTCOFOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'ODL_MAST','OLTCOMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCONTR),'ODL_MAST','OLTCONTR');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatIniPRO),'ODL_MAST','OLTDINRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODDIS),'ODL_MAST','OLTDISBA');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatIniMPS),'ODL_MAST','OLTDTMPS');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.cLEAPRO),'ODL_MAST','OLTEMLAV');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFCOCO');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMP),'ODL_MAST','OLTFLIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLORD),'ODL_MAST','OLTFLORD');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFORCO');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTIMCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'ODL_MAST','OLTKEYSA');
            +","+cp_NullLink(cp_ToStrODBC(this.cLEAMPS),'ODL_MAST','OLTLEMPS');
            +","+cp_NullLink(cp_ToStrODBC(this.cPERASS),'ODL_MAST','OLTPERAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
            +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTOD1');
            +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTODL');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
            +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
            +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
            +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
            +","+cp_NullLink(cp_ToStrODBC(this.cUNIMIS),'ODL_MAST','OLTUNMIS');
            +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLCON),'ODL_MAST','OLTFLCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSECIC),'ODL_MAST','OLTSECIC');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTCON');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTSAFLT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CENCOS),'ODL_MAST','OLTCOCEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCOS),'ODL_MAST','OLTVOCEN');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'OLDATODL',i_DATSYS,'OLDATODP',i_DATSYS,'OLOPEODL',i_CODUTE,'OLOPEODP',i_CODUTE,'OLTCAMAG',this.w_CAUORD,'OLTCOART',this.cCODART,'OLTCODIC',this.cCODICE,'OLTCOFOR',this.w_OLTCOFOR,'OLTCOMAG',this.w_OLTCOMAG,'OLTCONTR',this.w_OLTCONTR,'OLTDINRIC',this.cDatIniPRO)
            insert into (i_cTable) (OLCODODL,OLDATODL,OLDATODP,OLOPEODL,OLOPEODP,OLTCAMAG,OLTCOART,OLTCODIC,OLTCOFOR,OLTCOMAG,OLTCONTR,OLTDINRIC,OLTDISBA,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLEVA,OLTFLIMP,OLTFLORD,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,OLTVARIA,UTCC,UTDC,OLTFLCON,OLTSECIC,OLTDTCON,OLTSAFLT,OLTCOCEN,OLTVOCEN &i_ccchkf. );
               values (;
                 this.w_OLSERIAL;
                 ,i_DATSYS;
                 ,i_DATSYS;
                 ,i_CODUTE;
                 ,i_CODUTE;
                 ,this.w_CAUORD;
                 ,this.cCODART;
                 ,this.cCODICE;
                 ,this.w_OLTCOFOR;
                 ,this.w_OLTCOMAG;
                 ,this.w_OLTCONTR;
                 ,this.cDatIniPRO;
                 ,this.cCODDIS;
                 ,this.cDatIniMPS;
                 ,this.cDatFinMPS;
                 ,this.cLEAPRO;
                 ," ";
                 ," ";
                 ,this.w_OLTFLIMP;
                 ,this.w_OLTFLORD;
                 ," ";
                 ,0;
                 ,this.cKEYSAL;
                 ,this.cLEAMPS;
                 ,this.cPERASS;
                 ,this.w_OLTPROVE;
                 ,this.cMPSSUG;
                 ,this.cMPSSUG;
                 ,0;
                 ,0;
                 ,this.cMPSSUG;
                 ,this.w_OLTSTATO;
                 ,"F";
                 ,"A";
                 ,this.cUNIMIS;
                 ,"N";
                 ,i_CODUTE;
                 ,SetInfoDate( g_CALUTD );
                 ,this.w_OLTFLCON;
                 ,this.w_OLTSECIC;
                 ,this.cDatFinMPS;
                 ,0;
                 ,this.w_CENCOS;
                 ,this.w_VOCCOS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"OLCODODL"+",OLDATODL"+",OLDATODP"+",OLOPEODL"+",OLOPEODP"+",OLTCAMAG"+",OLTCICLO"+",OLTCOART"+",OLTCODIC"+",OLTCOFOR"+",OLTCOMAG"+",OLTCONTR"+",OLTDINRIC"+",OLTDISBA"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLEVA"+",OLTFLIMP"+",OLTFLORD"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",OLTVARIA"+",UTCC"+",UTDC"+",OLTFLCON"+",OLTDTCON"+",OLTSAFLT"+",OLTCOCEN"+",OLTVOCEN"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_MAST','OLCODODL');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODRIS),'ODL_MAST','OLTCICLO');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'ODL_MAST','OLTCOART');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODICE),'ODL_MAST','OLTCODIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOFOR),'ODL_MAST','OLTCOFOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'ODL_MAST','OLTCOMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCONTR),'ODL_MAST','OLTCONTR');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatIniPRO),'ODL_MAST','OLTDINRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.cCODDIS),'ODL_MAST','OLTDISBA');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatIniMPS),'ODL_MAST','OLTDTMPS');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.cLEAPRO),'ODL_MAST','OLTEMLAV');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFCOCO');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMP),'ODL_MAST','OLTFLIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLORD),'ODL_MAST','OLTFLORD');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFORCO');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTIMCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'ODL_MAST','OLTKEYSA');
            +","+cp_NullLink(cp_ToStrODBC(this.cLEAMPS),'ODL_MAST','OLTLEMPS');
            +","+cp_NullLink(cp_ToStrODBC(this.cPERASS),'ODL_MAST','OLTPERAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
            +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTOD1');
            +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTODL');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
            +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
            +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
            +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
            +","+cp_NullLink(cp_ToStrODBC(this.cUNIMIS),'ODL_MAST','OLTUNMIS');
            +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLCON),'ODL_MAST','OLTFLCON');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTCON');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTSAFLT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CENCOS),'ODL_MAST','OLTCOCEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCOS),'ODL_MAST','OLTVOCEN');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'OLDATODL',i_DATSYS,'OLDATODP',i_DATSYS,'OLOPEODL',i_CODUTE,'OLOPEODP',i_CODUTE,'OLTCAMAG',this.w_CAUORD,'OLTCICLO',this.cCODRIS,'OLTCOART',this.cCODART,'OLTCODIC',this.cCODICE,'OLTCOFOR',this.w_OLTCOFOR,'OLTCOMAG',this.w_OLTCOMAG,'OLTCONTR',this.w_OLTCONTR)
            insert into (i_cTable) (OLCODODL,OLDATODL,OLDATODP,OLOPEODL,OLOPEODP,OLTCAMAG,OLTCICLO,OLTCOART,OLTCODIC,OLTCOFOR,OLTCOMAG,OLTCONTR,OLTDINRIC,OLTDISBA,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLEVA,OLTFLIMP,OLTFLORD,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,OLTVARIA,UTCC,UTDC,OLTFLCON,OLTDTCON,OLTSAFLT,OLTCOCEN,OLTVOCEN &i_ccchkf. );
               values (;
                 this.w_OLSERIAL;
                 ,i_DATSYS;
                 ,i_DATSYS;
                 ,i_CODUTE;
                 ,i_CODUTE;
                 ,this.w_CAUORD;
                 ,this.cCODRIS;
                 ,this.cCODART;
                 ,this.cCODICE;
                 ,this.w_OLTCOFOR;
                 ,this.w_OLTCOMAG;
                 ,this.w_OLTCONTR;
                 ,this.cDatIniPRO;
                 ,this.cCODDIS;
                 ,this.cDatIniMPS;
                 ,this.cDatFinMPS;
                 ,this.cLEAPRO;
                 ," ";
                 ," ";
                 ,this.w_OLTFLIMP;
                 ,this.w_OLTFLORD;
                 ," ";
                 ,0;
                 ,this.cKEYSAL;
                 ,this.cLEAMPS;
                 ,this.cPERASS;
                 ,this.w_OLTPROVE;
                 ,this.cMPSSUG;
                 ,this.cMPSSUG;
                 ,0;
                 ,0;
                 ,this.cMPSSUG;
                 ,this.w_OLTSTATO;
                 ,"F";
                 ,"A";
                 ,this.cUNIMIS;
                 ,"N";
                 ,i_CODUTE;
                 ,SetInfoDate( g_CALUTD );
                 ,this.w_OLTFLCON;
                 ,this.cDatFinMPS;
                 ,0;
                 ,this.w_CENCOS;
                 ,this.w_VOCCOS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        * --- Insert into ODL_MAST
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"OLCODODL"+",OLDATODL"+",OLDATODP"+",OLOPEODL"+",OLOPEODP"+",OLTCAMAG"+",OLTCODIC"+",OLTCOART"+",OLTCOFOR"+",OLTCOMAG"+",OLTCONTR"+",OLTDINRIC"+",OLTDISBA"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLEVA"+",OLTFLIMP"+",OLTFLORD"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",OLTVARIA"+",UTCC"+",UTDC"+",OLCRIFOR"+",OLTCOCEN"+",OLTVOCEN"+",OLTFLDAP"+",OLTSECIC"+",OLTDTCON"+",OLTSAFLT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_MAST','OLCODODL');
          +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
          +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAUORD),'ODL_MAST','OLTCAMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.cCODICE),'ODL_MAST','OLTCODIC');
          +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'ODL_MAST','OLTCOART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOFOR),'ODL_MAST','OLTCOFOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'ODL_MAST','OLTCOMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCONTR),'ODL_MAST','OLTCONTR');
          +","+cp_NullLink(cp_ToStrODBC(this.cDatIniPRO),'ODL_MAST','OLTDINRIC');
          +","+cp_NullLink(cp_ToStrODBC(this.cCODDIS),'ODL_MAST','OLTDISBA');
          +","+cp_NullLink(cp_ToStrODBC(this.cDatIniMPS),'ODL_MAST','OLTDTMPS');
          +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTRIC');
          +","+cp_NullLink(cp_ToStrODBC(this.cLEAPRO),'ODL_MAST','OLTEMLAV');
          +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFCOCO');
          +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLEVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMP),'ODL_MAST','OLTFLIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLORD),'ODL_MAST','OLTFLORD');
          +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFORCO');
          +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTIMCOM');
          +","+cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'ODL_MAST','OLTKEYSA');
          +","+cp_NullLink(cp_ToStrODBC(this.cLEAMPS),'ODL_MAST','OLTLEMPS');
          +","+cp_NullLink(cp_ToStrODBC(this.cPERASS),'ODL_MAST','OLTPERAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
          +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTOD1');
          +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTODL');
          +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
          +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
          +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTSAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
          +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
          +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
          +","+cp_NullLink(cp_ToStrODBC(this.cUNIMIS),'ODL_MAST','OLTUNMIS');
          +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRCRIFOR),'ODL_MAST','OLCRIFOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CENCOS),'ODL_MAST','OLTCOCEN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCOS),'ODL_MAST','OLTVOCEN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLDAP),'ODL_MAST','OLTFLDAP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSECIC),'ODL_MAST','OLTSECIC');
          +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTCON');
          +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTSAFLT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'OLDATODL',i_DATSYS,'OLDATODP',i_DATSYS,'OLOPEODL',i_CODUTE,'OLOPEODP',i_CODUTE,'OLTCAMAG',this.w_CAUORD,'OLTCODIC',this.cCODICE,'OLTCOART',this.cCODART,'OLTCOFOR',this.w_OLTCOFOR,'OLTCOMAG',this.w_OLTCOMAG,'OLTCONTR',this.w_OLTCONTR,'OLTDINRIC',this.cDatIniPRO)
          insert into (i_cTable) (OLCODODL,OLDATODL,OLDATODP,OLOPEODL,OLOPEODP,OLTCAMAG,OLTCODIC,OLTCOART,OLTCOFOR,OLTCOMAG,OLTCONTR,OLTDINRIC,OLTDISBA,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLEVA,OLTFLIMP,OLTFLORD,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,OLTVARIA,UTCC,UTDC,OLCRIFOR,OLTCOCEN,OLTVOCEN,OLTFLDAP,OLTSECIC,OLTDTCON,OLTSAFLT &i_ccchkf. );
             values (;
               this.w_OLSERIAL;
               ,i_DATSYS;
               ,i_DATSYS;
               ,i_CODUTE;
               ,i_CODUTE;
               ,this.w_CAUORD;
               ,this.cCODICE;
               ,this.cCODART;
               ,this.w_OLTCOFOR;
               ,this.w_OLTCOMAG;
               ,this.w_OLTCONTR;
               ,this.cDatIniPRO;
               ,this.cCODDIS;
               ,this.cDatIniMPS;
               ,this.cDatFinMPS;
               ,this.cLEAPRO;
               ," ";
               ," ";
               ,this.w_OLTFLIMP;
               ,this.w_OLTFLORD;
               ," ";
               ,0;
               ,this.cKEYSAL;
               ,this.cLEAMPS;
               ,this.cPERASS;
               ,this.w_OLTPROVE;
               ,this.cMPSSUG;
               ,this.cMPSSUG;
               ,0;
               ,0;
               ,this.cMPSSUG;
               ,this.w_OLTSTATO;
               ,"F";
               ,"A";
               ,this.cUNIMIS;
               ,"N";
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,this.w_PRCRIFOR;
               ,this.w_CENCOS;
               ,this.w_VOCCOS;
               ,this.w_OLTFLDAP;
               ,this.w_OLTSECIC;
               ,this.cDatFinMPS;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Inserisce ciclo su figlio dettaglio
      if this.oParentObject.w_ODA="N" and this.w_OLTSTATO<>"C" and this.w_OLTPROVE<>"E"
        this.w_ULTIMAFASE = 0
        this.w_CPULTFASE = 0
        this.w_WIPFASEPREC = SPACE(20)
        this.w_WIPFASEOLD = SPACE(20)
        this.w_PRIMOGIRO = .T.
        if g_PRFA="S" and g_CICLILAV="S" and this.w_OLTSTATO="P" and not empty(this.w_OLTSECIC)
          this.w_OLTDINRIC = this.cDatIniPRO
          this.w_OLTDTRIC = this.cDatFinMPS
          vq_exec("..\PRFA\EXE\QUERY\GSCI_BCS", this, "Cicli")
          Select Cicli
          index on clcoddis tag clcoddis 
 index on clserial tag clserial
          this.w_nRecFasi = 0
          * --- Seleziona il ciclo preferenziale (del materiale preferenziale)
          Select Cicli 
 set order to clcoddis
          select distinct clserial, clcoddis, cprownum, cproword, clindfas, cl__fase, clfasdes, clcoupoi, clfasout, clultfas, clcprife, clcodfas, clwipfpr, ; 
 clbfrife, clfascos, clfascla, clfasclo, clmagwip, clprifas, clltfase from Cicli order by cproword desc into cursor TmpCic
          this.w_nRecFasi = RecCount("TmpCic")
          Select Cicli 
 set order to clserial
          if Used("TmpCic")
            Select * from TmpCic into cursor TmpCic READWRITE 
 Select TmpCic
            if this.w_nRecFasi > 0
              Select TmpCic
              go top
              replace clultfas with "S", clcoupoi with "S", clfasout with "S"
              * --- Verifico che sia presente il flag prima fase
              *     La fase con il flag ppotrebbe non essere valida e quindi lo dovrei ricalcolare
              Select TmpCic
              go top
              locate for clprifas="S"
              if Found()
                replace clprifas with "S", clcoupoi with "S", clfasout with "S"
              else
                Select TmpCic
                go bottom
                do while not bof()
                  if clcoupoi="S" and clfasout="S"
                    replace clprifas with "S"
                    exit
                  endif
                  Select TmpCic
                  skip-1
                enddo
              endif
              if this.w_PPMATINP="P"
                * --- Prima fase: i materiali non associati a nessuna fase sono associati alla prima
                this.w_PRRIFFAS = cproword
                this.w_PRCPRIFE = clcprife
              endif
            endif
            * --- Imposta il Codice output della fase precedente
            this.w_OLTDISBA = SPACE(20)
            this.w_WIPFASPRE = SPACE(5)
            this.w_WIPFASOLD = SPACE(5)
            go bottom
            do while not bof()
              if this.w_PRIMOGIRO
                this.w_OLTDISBA = clcoddis
                this.w_CPROWORD = cproword
                this.w_CLWIPFPR = SPACE(20)
                this.w_WIPFPR = nvl(clcodfas,SPACE(20))
                this.w_PRIMOGIRO = .F.
              endif
              if this.w_CPROWORD<>cproword
                this.w_CPROWORD = cproword
                this.w_CLWIPFPR = this.w_WIPFPR
                this.w_WIPFPR = nvl(clcodfas,SPACE(20))
              endif
              replace clwipfpr with this.w_CLWIPFPR
              skip-1
            enddo
          endif
          Select TmpCic
          go top
          if this.w_nRecFasi = 1
            this.w_CLFASCLA = NVL(CLFASCLA, "N")
            if NVL(CLFASCLA, "N")<>NVL(CLFASCLO, "N")
              this.w_CLFASCLA = NVL(CLFASCLO, "N")
            endif
            this.w_CLFPROVE = IIF(this.w_CLFASCLA="S", "L", "I")
            this.w_OLTPROVE = this.w_CLFPROVE
          endif
          Select TmpCic
          if this.w_PPMATINP<>"P"
            * --- Ultima Fase: i materiali non associati a nessuna fase sono associati all'ultima
            this.w_PRRIFFAS = cproword
            this.w_PRCPRIFE = clcprife
            this.w_WIPFASPRE = this.w_WIPFASOLD
          endif
          if Used("TmpCic")
            select TmpCic
            scan
            this.w_CLFASSEL = iif(clindfas="00","S","N")
            this.w_CLCOUPOI = iif(clultfas="S", "S", clcoupoi)
            this.w_CLFASOUT = nvl(clfasout, "N")
            this.w_CLFASCLA = NVL(CLFASCLA, "N")
            if this.w_nRecFasi = 1 AND NVL(CLFASCLA, "N")<>NVL(CLFASCLO, "N")
              this.w_CLFASCLA = NVL(CLFASCLO, "N")
            endif
            this.w_CLCODFAS = NVL(CLCODFAS, SPACE(20))
            this.w_CLFASCOS = iif(this.w_CLCOUPOI="S","S",clfascos)
            this.w_WIPFASOLD = nvl(clmagwip, space(5))
            * --- Insert into ODL_CICL
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CLDESFAS"+",CLQTAPRE"+",CLPREUM1"+",CLQTAAVA"+",CLAVAUM1"+",CLFASEVA"+",CLCOUPOI"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLWIPOUT"+",CLWIPFPR"+",CLBFRIFE"+",CL__FASE"+",CLFASCOS"+",CLCODFAS"+",CLFASOUT"+",CLFASCLA"+",CLWIPDES"+",CLPRIFAS"+",CLLTFASE"+",CLMAGWIP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_CICL','CLCODODL');
              +","+cp_NullLink(cp_ToStrODBC(cprownum),'ODL_CICL','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(cprownum),'ODL_CICL','CLROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(cproword),'ODL_CICL','CLROWORD');
              +","+cp_NullLink(cp_ToStrODBC(clindfas),'ODL_CICL','CLINDPRE');
              +","+cp_NullLink(cp_ToStrODBC(clfasdes),'ODL_CICL','CLDESFAS');
              +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_CICL','CLQTAPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_CICL','CLPREUM1');
              +","+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLQTAAVA');
              +","+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLAVAUM1');
              +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_CICL','CLFASEVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCOUPOI),'ODL_CICL','CLCOUPOI');
              +","+cp_NullLink(cp_ToStrODBC(clultfas),'ODL_CICL','CLULTFAS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASSEL),'ODL_CICL','CLFASSEL');
              +","+cp_NullLink(cp_ToStrODBC(clcprife),'ODL_CICL','CLCPRIFE');
              +","+cp_NullLink(cp_ToStrODBC(clcodfas),'ODL_CICL','CLWIPOUT');
              +","+cp_NullLink(cp_ToStrODBC(clwipfpr),'ODL_CICL','CLWIPFPR');
              +","+cp_NullLink(cp_ToStrODBC(clbfrife),'ODL_CICL','CLBFRIFE');
              +","+cp_NullLink(cp_ToStrODBC(cl__fase),'ODL_CICL','CL__FASE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCOS),'ODL_CICL','CLFASCOS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODFAS),'ODL_CICL','CLCODFAS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASOUT),'ODL_CICL','CLFASOUT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLFASCLA),'ODL_CICL','CLFASCLA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_WIPFASPRE),'ODL_CICL','CLWIPDES');
              +","+cp_NullLink(cp_ToStrODBC(clprifas),'ODL_CICL','CLPRIFAS');
              +","+cp_NullLink(cp_ToStrODBC(clltfase),'ODL_CICL','CLLTFASE');
              +","+cp_NullLink(cp_ToStrODBC(clmagwip),'ODL_CICL','CLMAGWIP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CLCODODL',this.w_OLSERIAL,'CPROWNUM',cprownum,'CLROWNUM',cprownum,'CLROWORD',cproword,'CLINDPRE',clindfas,'CLDESFAS',clfasdes,'CLQTAPRE',this.cMPSSUG,'CLPREUM1',this.cMPSSUG,'CLQTAAVA',0,'CLAVAUM1',0,'CLFASEVA'," ",'CLCOUPOI',this.w_CLCOUPOI)
              insert into (i_cTable) (CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CLDESFAS,CLQTAPRE,CLPREUM1,CLQTAAVA,CLAVAUM1,CLFASEVA,CLCOUPOI,CLULTFAS,CLFASSEL,CLCPRIFE,CLWIPOUT,CLWIPFPR,CLBFRIFE,CL__FASE,CLFASCOS,CLCODFAS,CLFASOUT,CLFASCLA,CLWIPDES,CLPRIFAS,CLLTFASE,CLMAGWIP &i_ccchkf. );
                 values (;
                   this.w_OLSERIAL;
                   ,cprownum;
                   ,cprownum;
                   ,cproword;
                   ,clindfas;
                   ,clfasdes;
                   ,this.cMPSSUG;
                   ,this.cMPSSUG;
                   ,0;
                   ,0;
                   ," ";
                   ,this.w_CLCOUPOI;
                   ,clultfas;
                   ,this.w_CLFASSEL;
                   ,clcprife;
                   ,clcodfas;
                   ,clwipfpr;
                   ,clbfrife;
                   ,cl__fase;
                   ,this.w_CLFASCOS;
                   ,this.w_CLCODFAS;
                   ,this.w_CLFASOUT;
                   ,this.w_CLFASCLA;
                   ,this.w_WIPFASPRE;
                   ,clprifas;
                   ,clltfase;
                   ,clmagwip;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            this.w_WIPFASPRE = this.w_WIPFASOLD
            endscan
          endif
          * --- Cursore per il riferimento dei materiali alle fasi
          select midibrow, clinival, clfinval, cproword, clcprife from Cicli into cursor TmpCic
          this.w_MAXFASE = 0
          this.w_OLCODODL = this.w_OLSERIAL
          this.w_CLCODODL = this.w_OLSERIAL
          ah_msg("Assegnamento risorse di fase in corso...")
          this.w_SERODL = this.w_OLSERIAL
          this.w_OLTSTATODP = "*"
          * --- Inserisco Risorse Master
          * --- Create temporary table TMPGSDB_MRL
          i_nIdx=cp_AddTableDef('TMPGSDB_MRL') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_RNYBTLIZJR[2]
          indexes_RNYBTLIZJR[1]='OLTSECIC,CLROWNUM'
          indexes_RNYBTLIZJR[2]='CLCODODL'
          vq_exec('..\PRFA\EXE\QUERY\GSCI1MRL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_RNYBTLIZJR,.f.)
          this.TMPGSDB_MRL_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Insert into ODLMRISO
          i_nConn=i_TableProp[this.ODLMRISO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCI3MRL",this.ODLMRISO_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPGSDB_MRL
          i_nIdx=cp_GetTableDefIdx('TMPGSDB_MRL')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPGSDB_MRL')
          endif
          * --- Inserisco Risorse Detail
          * --- Create temporary table TMPGSDB_MRL
          i_nIdx=cp_AddTableDef('TMPGSDB_MRL') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_KFRQLSZAKO[2]
          indexes_KFRQLSZAKO[1]='OLTSECIC,CLROWNUM'
          indexes_KFRQLSZAKO[2]='CLCODODL'
          vq_exec('..\PRFA\EXE\QUERY\GSCI2MRL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_KFRQLSZAKO,.f.)
          this.TMPGSDB_MRL_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Insert into ODL_RISO
          i_nConn=i_TableProp[this.ODL_RISO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCI4MRL",this.ODL_RISO_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPGSDB_MRL
          i_nIdx=cp_GetTableDefIdx('TMPGSDB_MRL')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPGSDB_MRL')
          endif
          * --- Materiali di output
          ah_msg("Assegnamento materiali di output in corso...")
          this.w_OLTSTATODP = "*"
          this.w_MATOUP = this.w_PPMATOUP
          * --- Inserisco Materiali di output
          * --- Create temporary table TMPGSCO_MMO
          i_nIdx=cp_AddTableDef('TMPGSCO_MMO') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_OAZGRYBTHU[2]
          indexes_OAZGRYBTHU[1]='MOSERIAL,MOROWNUM'
          indexes_OAZGRYBTHU[2]='MOCODODL'
          vq_exec('GSMR_MOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_OAZGRYBTHU,.f.)
          this.TMPGSCO_MMO_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Insert into ODL_MOUT
          i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR1MOU",this.ODL_MOUT_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPGSCO_MMO
          i_nIdx=cp_GetTableDefIdx('TMPGSCO_MMO')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPGSCO_MMO')
          endif
          if g_PRFA="S" and g_CICLILAV="S"
            * --- Associo il materiale di output alla fase, solo se l'ODL ha il ciclo e se la fase non � gi� stata assegnata manualmente nei materiali di output del ciclo stesso.
            *     Se w_MATOUP � 'P' saranno tutti associati alla prima fase, altrimenti all'ultima fase. Se w_MATOUP � 'N' i materiali di output non vengono gestiti a livello di fase
            *     e sono gi� filtrati dalla query GSMR_MOU
            * --- Write into ODL_MOUT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MOCODODL,CPROWNUM"
              do vq_exec with 'GSMR2MOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MOUT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MOROWNUM = _t2.CPROWFS";
                  +",MOFASRIF = _t2.ROWORDFS ";
                  +i_ccchkf;
                  +" from "+i_cTable+" ODL_MOUT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT, "+i_cQueryTable+" _t2 set ";
                  +"ODL_MOUT.MOROWNUM = _t2.CPROWFS";
                  +",ODL_MOUT.MOFASRIF = _t2.ROWORDFS ";
                  +Iif(Empty(i_ccchkf),"",",ODL_MOUT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ODL_MOUT.MOCODODL = t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = t2.CPROWNUM";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT set (";
                  +"MOROWNUM,";
                  +"MOFASRIF";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.CPROWFS,";
                  +"t2.ROWORDFS ";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT set ";
                  +"MOROWNUM = _t2.CPROWFS";
                  +",MOFASRIF = _t2.ROWORDFS ";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MOCODODL = "+i_cQueryTable+".MOCODODL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MOROWNUM = (select CPROWFS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MOFASRIF = (select ROWORDFS  from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          this.w_SERODL = this.w_OLSERIAL
          * --- Inserisco cicli semplificati sull'odl
          * --- Insert into ODL_SMPL
          i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS_BGP",this.ODL_SMPL_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Materiali di output
          ah_msg("Assegnamento materiali di output in corso...")
          this.w_OLTSTATODP = "*"
          this.w_MATOUP = this.w_PPMATOUP
          * --- Inserisco Materiali di output
          * --- Create temporary table TMPGSCO_MMO
          i_nIdx=cp_AddTableDef('TMPGSCO_MMO') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_SWVRTHLMMD[2]
          indexes_SWVRTHLMMD[1]='MOSERIAL,MOROWNUM'
          indexes_SWVRTHLMMD[2]='MOCODODL'
          vq_exec('..\COLA\EXE\QUERY\GSMRDMOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_SWVRTHLMMD,.f.)
          this.TMPGSCO_MMO_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Insert into ODL_MOUT
          i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSMR1MOU",this.ODL_MOUT_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPGSCO_MMO
          i_nIdx=cp_GetTableDefIdx('TMPGSCO_MMO')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPGSCO_MMO')
          endif
          if g_PRFA="S" and g_CICLILAV="S"
            * --- Associo il materiale di output alla fase, solo se l'ODL ha il ciclo e se la fase non � gi� stata assegnata manualmente nei materiali di output del ciclo stesso.
            *     Se w_MATOUP � 'P' saranno tutti associati alla prima fase, altrimenti all'ultima fase. Se w_MATOUP � 'N' i materiali di output non vengono gestiti a livello di fase
            *     e sono gi� filtrati dalla query GSMR_MOU
            * --- Write into ODL_MOUT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MOCODODL,CPROWNUM"
              do vq_exec with 'GSMR2MOU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MOUT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MOROWNUM = _t2.CPROWFS";
                  +",MOFASRIF = _t2.ROWORDFS ";
                  +i_ccchkf;
                  +" from "+i_cTable+" ODL_MOUT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT, "+i_cQueryTable+" _t2 set ";
                  +"ODL_MOUT.MOROWNUM = _t2.CPROWFS";
                  +",ODL_MOUT.MOFASRIF = _t2.ROWORDFS ";
                  +Iif(Empty(i_ccchkf),"",",ODL_MOUT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ODL_MOUT.MOCODODL = t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = t2.CPROWNUM";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT set (";
                  +"MOROWNUM,";
                  +"MOFASRIF";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.CPROWFS,";
                  +"t2.ROWORDFS ";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ODL_MOUT.MOCODODL = _t2.MOCODODL";
                      +" and "+"ODL_MOUT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MOUT set ";
                  +"MOROWNUM = _t2.CPROWFS";
                  +",MOFASRIF = _t2.ROWORDFS ";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MOCODODL = "+i_cQueryTable+".MOCODODL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MOROWNUM = (select CPROWFS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MOFASRIF = (select ROWORDFS  from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
      * --- Aggiorna saldi magazzino
      * --- Try
      local bErr_0518B398
      bErr_0518B398=bTrsErr
      this.Try_0518B398()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0518B398
      * --- End
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.cKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.cKEYSAL;
            and SLCODMAG = this.w_OLTCOMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Saldi commessa
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.cCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.cCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SALCOM="S"
        * --- Try
        local bErr_0518EE48
        bErr_0518EE48=bTrsErr
        this.Try_0518EE48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0518EE48
        * --- End
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SCQTOPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SCQTIPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.cKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.cKEYSAL;
              and SCCODMAG = this.w_OLTCOMAG;
              and SCCODCAN = this.w_COMMDEFA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa'
          return
        endif
      endif
      if this.oParentObject.w_ODA="N" and this.w_OLTSTATO<>"C" and this.w_OLTPROVE<>"E"
        * --- Lancia procedura di generazione dettaglio ODL
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    enddo
    * --- Setta periodo ODL
    this.w_GENPERODL = iif(this.w_OLTSTATO="K",.T.,this.w_GENPERODL)
    * --- commit
    cp_EndTrs(.t.)
    this.w_NumOrd = this.w_NumOrd + 1
    this.w_PIANIF = TRUE
    return
  proc Try_0515BCA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
          +" where OLTSTATO='K'";
           ,"_Curs_ODL_MAST")
    else
      select * from (i_cTable);
       where OLTSTATO="K";
        into cursor _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_MAST')
      select _Curs_ODL_MAST
      locate for 1=1
      do while not(eof())
      this.w_OLTFLORD = this.w_CAUO_OR
      this.w_OLTFLIMP = this.w_CAUO_IM
      this.w_OLTCOMAG = _Curs_ODL_MAST.OLTCOMAG
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTKEYSA);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SLCODICE = _Curs_ODL_MAST.OLTKEYSA;
            and SLCODMAG = this.w_OLTCOMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore aggiornamento saldi'
        return
      endif
      * --- Delete from ODL_MAST
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
               )
      else
        delete from (i_cTable) where;
              OLCODODL = _Curs_ODL_MAST.OLCODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore cancellazione ODL'
        return
      endif
        select _Curs_ODL_MAST
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0518B398()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+",SLCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'SALDIART','SLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'SALDIART','SLCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.cKEYSAL,'SLCODMAG',this.w_OLTCOMAG,'SLCODART',this.cCODART)
      insert into (i_cTable) (SLCODICE,SLCODMAG,SLCODART &i_ccchkf. );
         values (;
           this.cKEYSAL;
           ,this.w_OLTCOMAG;
           ,this.cCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0518EE48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.cKEYSAL,'SCCODMAG',this.w_OLTCOMAG,'SCCODCAN',this.w_COMMDEFA,'SCCODART',this.cCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.cKEYSAL;
           ,this.w_OLTCOMAG;
           ,this.w_COMMDEFA;
           ,this.cCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    this.w_LMessage = "Message()= "+message()
    INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
    (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LMessage)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili
    * --- --Variabili Listini
    * --- --Politiche di lottizzazione
    * --- --Variabili per la determinazione del listino per filtro query
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Dettaglio ODL
    * --- Eplode Distinta Selezionata
    this.w_DBCODINI = this.cCODART
    this.w_DBCODFIN = this.cCODART
    this.w_OLTCOART = this.cCODART
    this.w_MAXLEVEL = 1
    this.w_VERIFICA = "SN"
    this.w_FILSTAT = " "
    this.w_DATFIL = i_DATSYS
    USE IN SELECT("TES_PLOS")
    gsar_bde(this,"A")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if USED("TES_PLOS")
      if this.w_OLTPROVE="L" AND NOT EMPTY(this.w_OLTCONTR)
        * --- Legge Eventuali Materiali del Terzista
        vq_exec ("..\COLA\EXE\QUERY\GSCO_QMT", this, "_MatTer_")
      endif
      this.w_ULTFAS = 0
      if g_PRFA="S" and g_CICLILAV="S" and not empty(this.w_OLTSECIC)
        * --- Inserisco nel temporanero le fasi associate alla distinta
        this.w_DATFAS = this.cDatIniPRO
        this.w_OLTDINRIC = this.cDatIniPRO
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPMATINP,PPMATOUP"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPMATINP,PPMATOUP;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PPMATINP = NVL(cp_ToDate(_read_.PPMATINP),cp_NullValue(_read_.PPMATINP))
          this.w_PPMATOUP = NVL(cp_ToDate(_read_.PPMATOUP),cp_NullValue(_read_.PPMATOUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Determina prima fase ciclo (per assegnazione rif. fase a lista materiali stimati)
        this.w_ULTFAS = 0
        this.w_DULTIFAS = " "
        this.w_CPULTFAS = 0
        * --- Select from GSCO_BOL1
        do vq_exec with 'GSCO_BOL1',this,'_Curs_GSCO_BOL1','',.f.,.t.
        if used('_Curs_GSCO_BOL1')
          select _Curs_GSCO_BOL1
          locate for 1=1
          do while not(eof())
          * --- Determino ultima fase
          this.w_ULTFAS = _Curs_GSCO_BOL1.CPROWORD
          this.w_DULTIFAS = _Curs_GSCO_BOL1.CLFASDES
          this.w_CPULTFAS = _Curs_GSCO_BOL1.CPROWNUM
          if this.w_PPMATINP="P"
            exit
          endif
            select _Curs_GSCO_BOL1
            continue
          enddo
          use
        endif
      else
        if NOT EMPTY(this.cCODRIS)
          * --- Legge Ultima Fase del Ciclo (Per Default)
          * --- Select from TAB_CICL
          i_nConn=i_TableProp[this.TAB_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2],.t.,this.TAB_CICL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CPROWORD  from "+i_cTable+" TAB_CICL ";
                +" where CSCODICE="+cp_ToStrODBC(this.cCODRIS)+"";
                +" order by CPROWORD";
                 ,"_Curs_TAB_CICL")
          else
            select CPROWORD from (i_cTable);
             where CSCODICE=this.cCODRIS;
             order by CPROWORD;
              into cursor _Curs_TAB_CICL
          endif
          if used('_Curs_TAB_CICL')
            select _Curs_TAB_CICL
            locate for 1=1
            do while not(eof())
            this.w_ULTFAS = NVL(_Curs_TAB_CICL.CPROWORD, 0)
              select _Curs_TAB_CICL
              continue
            enddo
            use
          endif
        endif
      endif
      DIMENSION QTC[99,2]
      * --- Cursore di Elaborazione
      SELECT TES_PLOS
      GO TOP
      * --- Cicla sulla Distinta Base Esplosa
      this.w_ROWNUM = 0
      this.w_ROWORD = 0
      SCAN FOR NOT EMPTY(DISPAD) AND NOT EMPTY(NVL(CODCOM,""))
      this.w_NUMLEV = NUMLEV
      this.w_OLCODICE = CODCOM
      this.w_OLCODART = ARTCOM
      this.w_OLUNIMIS = UNMIS0
      this.w_OLCOEIMP = QTAMOV
      this.w_OLQTAMOV = cp_ROUND(this.cMPSSUG * QTAMOV, 3)
      this.w_OLQTAUM1 = cp_ROUND(this.cMPSSUG * IIF(QTAUM1=0, QTAMOV, QTAUM1), 3)
      if VAL(this.w_NUMLEV) > 0
        this.w_QTAMOV = QTAMOV
        this.w_QTAUM1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
        * --- Componente: Prodotto Finito o Semilavorato
        * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
        QTC[VAL(this.w_NUMLEV), 1] = this.w_QTAMOV
        * --- Viene calcolala anche la Qta relativa alla U.M. Principale, per il calcolo del prezzo Unitario 
        QTC[VAL(this.w_NUMLEV), 2] = this.w_QTAUM1
        this.w_QTAMOV = 1
        this.w_QTAUM1 = 1
        FOR L_i = 1 TO VAL(this.w_NUMLEV)
        this.w_QTAMOV = this.w_QTAMOV * QTC[L_i, 1]
        this.w_QTAUM1 = this.w_QTAUM1 * QTC[L_i, 2]
        ENDFOR
        this.w_OLCOEIMP = this.w_QTAMOV
        this.w_OLQTAMOV = cp_ROUND(this.cMPSSUG * this.w_QTAMOV, 3)
        this.w_OLQTAUM1 = cp_ROUND(this.cMPSSUG * this.w_QTAUM1, 3)
      endif
      this.w_OLQTAEVA = 0
      this.w_OLQTAEV1 = 0
      this.w_OLFLEVAS = " "
      this.w_OLEVAAUT = " "
      this.w_OLCAUMAG = this.w_CAUIMP
      this.w_OLFLORDI = this.w_CAUI_OR
      this.w_OLFLIMPE = this.w_CAUI_IM
      this.w_OLTIPPRE = IIF(EMPTY(NVL(FLPREV, " ")), "N", NVL(FLPREV, "N"))
      this.w_OLMAGPRE = IIF(EMPTY(NVL(MAGPRE, " ")), this.w_MAGPRO, MAGPRE)
      * --- Magazzino di riga
      this.w_OLCODMAG = iif(this.w_MICOM="M",NVL( this.w_OLMAGPRE, " "),this.w_MAGFOR)
      this.w_OLCODMAG = iif(empty(this.w_OLCODMAG),iif(empty(this.w_MAGPRO),g_MAGAZI,this.w_MAGPRO),this.w_OLCODMAG)
      this.w_ARTIPGES = NVL(TIPGES , " ")
      this.w_TIPART = NVL(TIPART, SPACE(2))
      this.w_DISPAD = DISPAD
      this.w_COROWNUM = CPROWNUM
      this.w_RIFFAS = 0
      this.w_CORLEA = NVL(CORLEA,0)
      if this.w_CORLEA <> 0
        * --- Ricalcola data di inizio (Produzione)
        this.cDatIniPRO = COCALCLT(this.cDatIniPRO,Ceiling(this.w_CORLEA),"C", .F. ,"")
      endif
      if g_PRFA="S" and g_CICLILAV="S" and not empty(this.w_OLTSECIC)
        * --- Determina fase di utilizzo
        if VAL(this.w_NUMLEV) < 2
          if Used("TmpCic")
            select TmpCic
            locate for midibrow=this.w_COROWNUM and clinival<=this.cDatIniPRO and this.cDatIniPRO<=clfinval
            if FOUND()
              this.w_RIFFAS = cproword
              this.w_CPRFAS = clcprife
            else
              * --- I materiali senza riferimento sono prelevati nella prima fase
              this.w_RIFFAS = 0
              this.w_CPRFAS = 0
            endif
          endif
        endif
        if this.w_RIFFAS=0
          this.w_RIFFAS = this.w_PRRIFFAS
          this.w_CPRFAS = this.w_PRCPRIFE
        endif
      else
        if NOT EMPTY(this.cCODRIS)
          this.w_RIFFAS = IIF(NVL(RIFFAS, 0)=0, this.w_PRRIFFAS, RIFFAS)
        endif
      endif
      * --- Salta i materiali gestiti a consumo 
      if this.w_ARTIPGES <> "C" And this.w_TIPART<>"PH"
        this.w_ROWNUM = this.w_ROWNUM + 1
        this.w_ROWORD = this.w_ROWNUM * 10
        if this.w_OLTPROVE="L" AND NOT EMPTY(this.w_OLTCONTR) AND USED("_MatTer_")
          * --- Legge Eventuali Materiali del Terzista
          select _MatTer_
          go top
          LOCATE FOR NVL(MCCODICE, SPACE(20))=this.w_OLCODICE
          if FOUND()
            * --- Se il componente e' presente nei materiali del Terzista non deve eseguire alcun Impegno ne Trasferimento
            this.w_OLQTAEVA = this.w_OLQTAMOV
            this.w_OLQTAEV1 = this.w_OLQTAUM1
            this.w_OLFLEVAS = "S"
            this.w_OLTIPPRE = "N"
            this.w_OLEVAAUT = "S"
          endif
        endif
        this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S", 0, this.w_OLQTAUM1 - this.w_OLQTAEV1)
        if g_PRFA="S" and g_CICLILAV="S"
          * --- Insert into ODL_DETT
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLFLORDI"+",OLFLIMPE"+",OLFLRISE"+",OLCODMAG"+",OLCODICE"+",OLCODART"+",OLUNIMIS"+",OLCOEIMP"+",OLQTAMOV"+",OLQTAUM1"+",OLQTAEVA"+",OLQTAEV1"+",OLFLEVAS"+",OLKEYSAL"+",OLQTASAL"+",OLQTAPRE"+",OLQTAPR1"+",OLFLPREV"+",OLDATRIC"+",OLMAGPRE"+",OLMAGWIP"+",OLTIPPRE"+",OLRIFDOC"+",OLROWDOC"+",OLEVAAUT"+",OLFASRIF"+",OLCPRFAS"+",OLRIFFAS"+",OLCORRLT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_DETT','OLCODODL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ODL_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'ODL_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCAUMAG),'ODL_DETT','OLCAUMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLORDI),'ODL_DETT','OLFLORDI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLIMPE),'ODL_DETT','OLFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLRISE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'ODL_DETT','OLCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODICE),'ODL_DETT','OLCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'ODL_DETT','OLCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLUNIMIS),'ODL_DETT','OLUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCOEIMP),'ODL_DETT','OLCOEIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'ODL_DETT','OLKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatIniPRO),'ODL_DETT','OLDATRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'ODL_DETT','OLMAGPRE');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'ODL_DETT','OLMAGWIP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ODL_DETT','OLRIFDOC');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLROWDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFAS),'ODL_DETT','OLFASRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPRFAS),'ODL_DETT','OLCPRFAS');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLRIFFAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CORLEA),'ODL_DETT','OLCORRLT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'OLCAUMAG',this.w_OLCAUMAG,'OLFLORDI',this.w_OLFLORDI,'OLFLIMPE',this.w_OLFLIMPE,'OLFLRISE'," ",'OLCODMAG',this.w_OLCODMAG,'OLCODICE',this.w_OLCODICE,'OLCODART',this.w_OLCODART,'OLUNIMIS',this.w_OLUNIMIS,'OLCOEIMP',this.w_OLCOEIMP)
            insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLFLORDI,OLFLIMPE,OLFLRISE,OLCODMAG,OLCODICE,OLCODART,OLUNIMIS,OLCOEIMP,OLQTAMOV,OLQTAUM1,OLQTAEVA,OLQTAEV1,OLFLEVAS,OLKEYSAL,OLQTASAL,OLQTAPRE,OLQTAPR1,OLFLPREV,OLDATRIC,OLMAGPRE,OLMAGWIP,OLTIPPRE,OLRIFDOC,OLROWDOC,OLEVAAUT,OLFASRIF,OLCPRFAS,OLRIFFAS,OLCORRLT &i_ccchkf. );
               values (;
                 this.w_OLSERIAL;
                 ,this.w_ROWNUM;
                 ,this.w_ROWORD;
                 ,this.w_OLCAUMAG;
                 ,this.w_OLFLORDI;
                 ,this.w_OLFLIMPE;
                 ," ";
                 ,this.w_OLCODMAG;
                 ,this.w_OLCODICE;
                 ,this.w_OLCODART;
                 ,this.w_OLUNIMIS;
                 ,this.w_OLCOEIMP;
                 ,this.w_OLQTAMOV;
                 ,this.w_OLQTAUM1;
                 ,this.w_OLQTAEVA;
                 ,this.w_OLQTAEV1;
                 ,this.w_OLFLEVAS;
                 ,this.w_OLCODART;
                 ,this.w_OLQTASAL;
                 ,0;
                 ,0;
                 ," ";
                 ,this.cDatIniPRO;
                 ,this.w_OLCODMAG;
                 ,SPACE(5);
                 ,this.w_OLTIPPRE;
                 ,SPACE(10);
                 ,0;
                 ,this.w_OLEVAAUT;
                 ,this.w_RIFFAS;
                 ,this.w_CPRFAS;
                 ,0;
                 ,this.w_CORLEA;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into ODL_DETT
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLFLORDI"+",OLFLIMPE"+",OLFLRISE"+",OLCODMAG"+",OLCODICE"+",OLCODART"+",OLUNIMIS"+",OLCOEIMP"+",OLQTAMOV"+",OLQTAUM1"+",OLQTAEVA"+",OLQTAEV1"+",OLFLEVAS"+",OLKEYSAL"+",OLQTASAL"+",OLQTAPRE"+",OLQTAPR1"+",OLFLPREV"+",OLDATRIC"+",OLMAGPRE"+",OLMAGWIP"+",OLTIPPRE"+",OLRIFDOC"+",OLROWDOC"+",OLEVAAUT"+",OLRIFFAS"+",OLFASRIF"+",OLCORRLT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_DETT','OLCODODL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ODL_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'ODL_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCAUMAG),'ODL_DETT','OLCAUMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLORDI),'ODL_DETT','OLFLORDI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLIMPE),'ODL_DETT','OLFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLRISE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'ODL_DETT','OLCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODICE),'ODL_DETT','OLCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'ODL_DETT','OLCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLUNIMIS),'ODL_DETT','OLUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCOEIMP),'ODL_DETT','OLCOEIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'ODL_DETT','OLKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
            +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
            +","+cp_NullLink(cp_ToStrODBC(this.cDatIniPRO),'ODL_DETT','OLDATRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'ODL_DETT','OLMAGPRE');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'ODL_DETT','OLMAGWIP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ODL_DETT','OLRIFDOC');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLROWDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFAS),'ODL_DETT','OLRIFFAS');
            +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLFASRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CORLEA),'ODL_DETT','OLCORRLT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLSERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'OLCAUMAG',this.w_OLCAUMAG,'OLFLORDI',this.w_OLFLORDI,'OLFLIMPE',this.w_OLFLIMPE,'OLFLRISE'," ",'OLCODMAG',this.w_OLCODMAG,'OLCODICE',this.w_OLCODICE,'OLCODART',this.w_OLCODART,'OLUNIMIS',this.w_OLUNIMIS,'OLCOEIMP',this.w_OLCOEIMP)
            insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLFLORDI,OLFLIMPE,OLFLRISE,OLCODMAG,OLCODICE,OLCODART,OLUNIMIS,OLCOEIMP,OLQTAMOV,OLQTAUM1,OLQTAEVA,OLQTAEV1,OLFLEVAS,OLKEYSAL,OLQTASAL,OLQTAPRE,OLQTAPR1,OLFLPREV,OLDATRIC,OLMAGPRE,OLMAGWIP,OLTIPPRE,OLRIFDOC,OLROWDOC,OLEVAAUT,OLRIFFAS,OLFASRIF,OLCORRLT &i_ccchkf. );
               values (;
                 this.w_OLSERIAL;
                 ,this.w_ROWNUM;
                 ,this.w_ROWORD;
                 ,this.w_OLCAUMAG;
                 ,this.w_OLFLORDI;
                 ,this.w_OLFLIMPE;
                 ," ";
                 ,this.w_OLCODMAG;
                 ,this.w_OLCODICE;
                 ,this.w_OLCODART;
                 ,this.w_OLUNIMIS;
                 ,this.w_OLCOEIMP;
                 ,this.w_OLQTAMOV;
                 ,this.w_OLQTAUM1;
                 ,this.w_OLQTAEVA;
                 ,this.w_OLQTAEV1;
                 ,this.w_OLFLEVAS;
                 ,this.w_OLCODART;
                 ,this.w_OLQTASAL;
                 ,0;
                 ,0;
                 ," ";
                 ,this.cDatIniPRO;
                 ,this.w_OLCODMAG;
                 ,SPACE(5);
                 ,this.w_OLTIPPRE;
                 ,SPACE(10);
                 ,0;
                 ,this.w_OLEVAAUT;
                 ,this.w_RIFFAS;
                 ,0;
                 ,this.w_CORLEA;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Aggiorna saldi magazzino
        if this.w_OLQTASAL<>0 AND (NOT EMPTY(this.w_OLFLORDI) OR NOT EMPTY(this.w_OLFLIMPE))
          * --- Try
          local bErr_053163B8
          bErr_053163B8=bTrsErr
          this.Try_053163B8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_053163B8
          * --- End
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_OLFLORDI,'SLQTOPER','this.w_OLQTAUM1',this.w_OLQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_OLFLIMPE,'SLQTIPER','this.w_OLQTAUM1',this.w_OLQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLCODART);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLCODART;
                and SLCODMAG = this.w_OLCODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Saldi commessa
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OLCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OLCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            * --- Try
            local bErr_05308978
            bErr_05308978=bTrsErr
            this.Try_05308978()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_05308978
            * --- End
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_OLFLORDI,'SCQTOPER','this.w_OLQTAUM1',this.w_OLQTAUM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_OLFLIMPE,'SCQTIPER','this.w_OLQTAUM1',this.w_OLQTAUM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLCODART);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLCODART;
                  and SCCODMAG = this.w_OLCODMAG;
                  and SCCODCAN = this.w_COMMDEFA;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
        endif
      endif
      select TES_PLOS
      ENDSCAN
      * --- Chiude Cursore Esplosione
      select TES_PLOS
      use
      if USED("_MatTer_")
        select _MatTer_
      endif
    endif
  endproc
  proc Try_053163B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLCODART,'SLCODMAG',this.w_OLCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLCODART;
           ,this.w_OLCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05308978()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLCODART,'SCCODMAG',this.w_OLCODMAG,'SCCODCAN',this.w_COMMDEFA,'SCCODART',this.w_OLCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_OLCODART;
           ,this.w_OLCODMAG;
           ,this.w_COMMDEFA;
           ,this.w_OLCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PRCRIFOR = this.oParentObject.w_CRITFORN
    * --- In teoria ricalcolare punto e lotto di riordino qui + altri dati tipo voce di costo e fave varie
    *     Ma a quel punto come agisco su quantit� e data? Vedi anche AHE anche se l� � sui listini
    vq_exec("..\COLA\exe\query\GSCO_BGS", this, "ContrattiA")
    * --- Cerca il miglior fornitore dai ContrattiA
    * --- Seleziona Criterio di scelta
    do case
      case this.oParentObject.w_CRITFORN="A"
        * --- Affidabilit�
        OrdinaPer = "Order by coaffida desc"
      case this.oParentObject.w_CRITFORN="T"
        * --- Tempo = giorni di approvvigionamento
        OrdinaPer = "Order by cogioapp"
      case this.oParentObject.w_CRITFORN="R"
        * --- Ripartizione
        OrdinaPer = "Order by coperrip"
      case this.oParentObject.w_CRITFORN="I"
        * --- Priorit�
        OrdinaPer = "Order by copriori desc"
      otherwise
        OrdinaPer = ""
    endcase
    if this.oParentObject.w_CRITFORN<>"Z"
      * --- Seleziona i ContrattiA validi e li ordina per il criterio di scelta
      Select * from ContrattiA ;
      where fbcodart=this.cCODART and codatini<=i_DatSys and i_DatSys<=codatfin and (cocodclf=this.w_OLTCOFOR or empty(nvl(this.w_OLTCOFOR,"")));
      &OrdinaPer into cursor tempor
    else
      * --- Calcola Prezzo da Contratto
      * --- Prima genera un temporaneo contenente le righe articolo interessate dei ContrattiA validi
      * --- ordinate per Fornitore + Qta Scaglione
      * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
      Select * from ContrattiA ;
      where fbcodart=this.cCODART and codatini<=i_DatSys and i_DatSys<=codatfin ;
      and nvl(coquanti,0)>=this.cMPSSUG and not empty(nvl(cocodclf," ")) and (cocodclf=this.w_OLTCOFOR or empty(nvl(this.w_OLTCOFOR,"")));
      order by cocodclf, coquanti into cursor Contprez
      * --- Calcola Prezzo da Contratto
      * --- Elabora Criterio per Miglior Prezzo
      =WrCursor("Contprez")
      Index On cocodclf+str(coquanti,12,3) Tag Codice
      * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
      this.w_appo1 = "######"
      select Contprez
      go top
      scan
      if cocodclf <> this.w_APPO1
        * --- Nuovo Fornitore, Calcola Prezzo da Contratto
        this.w_PZ = NVL(COPREZZO, 0)
        this.w_S1 = NVL(COSCONT1, 0)
        this.w_S2 = NVL(COSCONT2, 0)
        this.w_S3 = NVL(COSCONT3, 0)
        this.w_S4 = NVL(COSCONT4, 0)
        this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
        this.w_VAL = NVL(COCODVAL, g_PERVAL)
        this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
        if this.w_VAL<>g_PERVAL
          * --- Se altra valuta, riporta alla Valuta di conto
          if this.w_CAO<>0
            this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
            replace coprezzo with this.w_PZ
          else
            * --- Cambio incongruente, record non utilizzabile
            this.w_PZ = -999
          endif
          select Contprez
        endif
        if this.w_PZ=-999
          delete
        else
          this.w_appo1 = cocodclf
        endif
      else
        * --- Elimina record
        delete
      endif
      endscan
      * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
      * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
      * --- Ordino per il prezzo piu' basso
      Select * from ContPrez where not deleted() ;
      Order By Coprezzo into cursor tempor
      if used("ContPrez")
        Select contPrez
        use
      endif
    endif
    Select tempor
    if reccount()=0
      * --- Nessun contratto valido trovato
      if empty(this.w_OLTCOFOR)
        this.w_OLTCOFOR = space(15)
        this.w_OLTCONTR = space(15)
      endif
    else
      go top
      this.w_OLTCOFOR = nvl(cocodclf,space(15))
      this.w_OLTCONTR = nvl(conumero,space(15))
      this.w_QTAMIN = nvl(coqtamin,0)
      this.w_LOTRIO = nvl(colotmul,0)
      if this.w_QTAMIN>0 and this.w_QTAMIN>this.cMPSSUG
        this.cMPSSUG = this.w_QTAMIN
      endif
      if this.w_LOTRIO>0
        this.cMPSSUG = iif( Mod(this.cMPSSUG,this.w_LOTRIO)=0, this.cMPSSUG, (Int(this.cMPSSUG/this.w_LOTRIO)+1)*this.w_LOTRIO)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTIPELA)
    this.pTIPELA=pTIPELA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,24)]
    this.cWorkTables[1]='ART_PROD'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='DISMBASE'
    this.cWorkTables[4]='MPS_TPER'
    this.cWorkTables[5]='ODL_MAST'
    this.cWorkTables[6]='PAR_PROD'
    this.cWorkTables[7]='PAR_RIOR'
    this.cWorkTables[8]='PRD_ERRO'
    this.cWorkTables[9]='SALDIART'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='CON_TRAM'
    this.cWorkTables[12]='CON_TRAD'
    this.cWorkTables[13]='ODL_RISO'
    this.cWorkTables[14]='TAB_CICL'
    this.cWorkTables[15]='ODL_DETT'
    this.cWorkTables[16]='ODL_SMPL'
    this.cWorkTables[17]='ART_ICOL'
    this.cWorkTables[18]='SALDICOM'
    this.cWorkTables[19]='ODLMRISO'
    this.cWorkTables[20]='ODL_MOUT'
    this.cWorkTables[21]='CIC_DETT'
    this.cWorkTables[22]='ODL_CICL'
    this.cWorkTables[23]='*TMPGSDB_MRL'
    this.cWorkTables[24]='*TMPGSCO_MMO'
    return(this.OpenAllTables(24))

  proc CloseCursors()
    if used('_Curs_CON_TRAM')
      use in _Curs_CON_TRAM
    endif
    if used('_Curs_CON_TRAD')
      use in _Curs_CON_TRAD
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_GSCO_BOL1')
      use in _Curs_GSCO_BOL1
    endif
    if used('_Curs_TAB_CICL')
      use in _Curs_TAB_CICL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPELA"
endproc
