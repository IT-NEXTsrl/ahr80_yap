* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kmp                                                        *
*              Piano ODL/OCL/ODA/ODP                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_492]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-04                                                      *
* Last revis.: 2017-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsco_kmp
* Array per periodi ODL/OCL
dimension PeriodiODP(100)
* --- Fine Area Manuale
return(createobject("tgsco_kmp",oParentObject))

* --- Class definition
define class tgsco_kmp as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 790
  Height = 510+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-21"
  HelpContextID=68573847
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=126

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  cpusers_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  PAR_RIOR_IDX = 0
  FAM_ARTI_IDX = 0
  MARCHI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  MAGAZZIN_IDX = 0
  CATEGOMO_IDX = 0
  GRUMERC_IDX = 0
  cPrg = "gsco_kmp"
  cComment = "Piano ODL/OCL/ODA/ODP"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  w_ReadPar = space(2)
  o_ReadPar = space(2)
  w_LARGCOL = 0
  w_DIMFONT = 0
  w_NUMPER = 0
  w_NMAXPE = 0
  w_PPCOLFES = 0
  w_PPCOLSUG = 0
  w_PPCOLCON = 0
  w_PPCOLMLA = 0
  w_PPCOLMIS = 0
  w_PPCOLMCO = 0
  w_BASSUG = 0
  w_TESSUG = 0
  w_BASCON = 0
  w_BASLAN = 0
  w_TESCON = 0
  w_TESLAN = 0
  w_BASMIS = 0
  w_TESMIS = 0
  w_BASSUM = 0
  w_BASPIA = 0
  w_BASMDP = 0
  w_TESSUM = 0
  w_TESPIA = 0
  w_TESMDP = 0
  w_ZoomEspanso = .F.
  w_Zoom1Height = 0
  w_OBTEST = ctod('  /  /  ')
  w_PSELA = space(3)
  w_CODSEL = space(20)
  w_DESSEL = space(40)
  w_PSELR = space(4)
  w_TipoPeriodo = space(20)
  w_DIP = ctod('  /  /  ')
  w_DFP = ctod('  /  /  ')
  w_ODPSEL = space(15)
  w_sCODART = space(20)
  w_sDESART = space(40)
  w_DInEff = ctod('  /  /  ')
  w_Dlancio = ctod('  /  /  ')
  w_FORSEL = space(15)
  w_DESFOR = space(40)
  w_CODICE = space(20)
  o_CODICE = space(20)
  w_COLSEL = 0
  w_CODSEL = space(41)
  w_sCODART = space(20)
  w_UMARTSE = space(3)
  w_sCODART = space(20)
  w_DESCSEL = space(40)
  w_sTIPGES = space(1)
  w_sPUNRIO = 0
  w_sSCOMIN = 0
  w_sLOTMIN = 0
  w_sLOTRIO = 0
  w_PPTIPDTF = space(1)
  w_PP___DTF = 0
  w_sSCARTO = 0
  w_ACTIVECOL = 0
  w_MPS_ODL = space(3)
  w_DataEsp = ctod('  /  /  ')
  w_OraEsp = space(8)
  w_DESUTE = space(20)
  w_OpeEsp = 0
  w_CACODICE = space(20)
  w_BSCROLL = .F.
  w_TIPCON = space(1)
  w_PPCOLSUM = 0
  w_PPCOLPIA = 0
  w_PPCOLMDP = 0
  w_cPERDTF = space(3)
  w_TIPATT = space(1)
  w_TIPGES = space(1)
  w_fPROFIN = space(2)
  w_fSEMLAV = space(2)
  w_fMATPRI = space(2)
  w_fMATFAS = space(2)
  w_fDISINI = space(20)
  w_fDISFIN = space(20)
  w_fCODFAM = space(5)
  w_fCODFAF = space(5)
  w_CRIELA = space(1)
  o_CRIELA = space(1)
  w_fGRUMER = space(5)
  w_fGRUMEF = space(5)
  w_fCATOMO = space(5)
  w_fCATOMF = space(5)
  w_fCODMAR = space(5)
  w_fCODMAF = space(5)
  w_fMAGINI = space(5)
  w_fMAGFIN = space(5)
  w_fCODCOM = space(15)
  o_fCODCOM = space(15)
  w_fCODCOF = space(15)
  o_fCODCOF = space(15)
  w_fCODATT = space(15)
  w_fCODATF = space(15)
  w_fCODFOR = space(15)
  w_fCODFOF = space(15)
  w_fCODFOR = space(15)
  w_fCODFOF = space(15)
  w_fCODFASI = space(66)
  w_fCODFASF = space(66)
  w_DESINI = space(40)
  w_SELEZM = space(1)
  o_SELEZM = space(1)
  w_fTIPGES = space(1)
  w_DESFIN = space(40)
  w_DESGRU = space(35)
  w_DESCAT = space(35)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_DESFORN = space(40)
  w_DESFAMA = space(35)
  w_DESMAR = space(35)
  w_DESGRF = space(35)
  w_DESCAF = space(35)
  w_DESFAMF = space(35)
  w_DESMAF = space(35)
  w_DESFOF = space(40)
  w_DESCANF = space(30)
  w_DESATF = space(30)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_MAGTEF = space(5)
  w_MAGTER = space(5)
  w_RIGALIS = 0
  w_KEYRIF = space(10)
  w_PPCRIELA = space(1)
  w_RET = .F.
  w_MIST = .NULL.
  w_ZoomODP = .NULL.
  w_DettODP = .NULL.
  w_PeriodoODP = .NULL.
  w_LANC = .NULL.
  w_SUGGM = .NULL.
  w_SUGGE = .NULL.
  w_PIAN = .NULL.
  w_CONF = .NULL.
  w_DPIA = .NULL.
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_kmp
  o_ACTIVECOL = 0
  o_CACODICE = space(20)
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kmpPag1","gsco_kmp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("ODP")
      .Pages(2).addobject("oPag","tgsco_kmpPag2","gsco_kmp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio periodo")
      .Pages(3).addobject("oPag","tgsco_kmpPag3","gsco_kmp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Informazioni")
      .Pages(4).addobject("oPag","tgsco_kmpPag4","gsco_kmp",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Selezioni")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPPTIPDTF_1_38
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_MIST = this.oPgFrm.Pages(3).oPag.MIST
    this.w_ZoomODP = this.oPgFrm.Pages(1).oPag.ZoomODP
    this.w_DettODP = this.oPgFrm.Pages(1).oPag.DettODP
    this.w_PeriodoODP = this.oPgFrm.Pages(2).oPag.PeriodoODP
    this.w_LANC = this.oPgFrm.Pages(3).oPag.LANC
    this.w_SUGGM = this.oPgFrm.Pages(3).oPag.SUGGM
    this.w_SUGGE = this.oPgFrm.Pages(3).oPag.SUGGE
    this.w_PIAN = this.oPgFrm.Pages(3).oPag.PIAN
    this.w_CONF = this.oPgFrm.Pages(3).oPag.CONF
    this.w_DPIA = this.oPgFrm.Pages(3).oPag.DPIA
    this.w_ZOOMMAGA = this.oPgFrm.Pages(4).oPag.ZOOMMAGA
    this.w_LBLMAGA = this.oPgFrm.Pages(4).oPag.LBLMAGA
    DoDefault()
    proc Destroy()
      this.w_MIST = .NULL.
      this.w_ZoomODP = .NULL.
      this.w_DettODP = .NULL.
      this.w_PeriodoODP = .NULL.
      this.w_LANC = .NULL.
      this.w_SUGGM = .NULL.
      this.w_SUGGE = .NULL.
      this.w_PIAN = .NULL.
      this.w_CONF = .NULL.
      this.w_DPIA = .NULL.
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='PAR_RIOR'
    this.cWorkTables[7]='FAM_ARTI'
    this.cWorkTables[8]='MARCHI'
    this.cWorkTables[9]='CAN_TIER'
    this.cWorkTables[10]='ATTIVITA'
    this.cWorkTables[11]='MAGAZZIN'
    this.cWorkTables[12]='CATEGOMO'
    this.cWorkTables[13]='GRUMERC'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_ReadPar=space(2)
      .w_LARGCOL=0
      .w_DIMFONT=0
      .w_NUMPER=0
      .w_NMAXPE=0
      .w_PPCOLFES=0
      .w_PPCOLSUG=0
      .w_PPCOLCON=0
      .w_PPCOLMLA=0
      .w_PPCOLMIS=0
      .w_PPCOLMCO=0
      .w_BASSUG=0
      .w_TESSUG=0
      .w_BASCON=0
      .w_BASLAN=0
      .w_TESCON=0
      .w_TESLAN=0
      .w_BASMIS=0
      .w_TESMIS=0
      .w_BASSUM=0
      .w_BASPIA=0
      .w_BASMDP=0
      .w_TESSUM=0
      .w_TESPIA=0
      .w_TESMDP=0
      .w_ZoomEspanso=.f.
      .w_Zoom1Height=0
      .w_OBTEST=ctod("  /  /  ")
      .w_PSELA=space(3)
      .w_CODSEL=space(20)
      .w_DESSEL=space(40)
      .w_PSELR=space(4)
      .w_TipoPeriodo=space(20)
      .w_DIP=ctod("  /  /  ")
      .w_DFP=ctod("  /  /  ")
      .w_ODPSEL=space(15)
      .w_sCODART=space(20)
      .w_sDESART=space(40)
      .w_DInEff=ctod("  /  /  ")
      .w_Dlancio=ctod("  /  /  ")
      .w_FORSEL=space(15)
      .w_DESFOR=space(40)
      .w_CODICE=space(20)
      .w_COLSEL=0
      .w_CODSEL=space(41)
      .w_sCODART=space(20)
      .w_UMARTSE=space(3)
      .w_sCODART=space(20)
      .w_DESCSEL=space(40)
      .w_sTIPGES=space(1)
      .w_sPUNRIO=0
      .w_sSCOMIN=0
      .w_sLOTMIN=0
      .w_sLOTRIO=0
      .w_PPTIPDTF=space(1)
      .w_PP___DTF=0
      .w_sSCARTO=0
      .w_ACTIVECOL=0
      .w_MPS_ODL=space(3)
      .w_DataEsp=ctod("  /  /  ")
      .w_OraEsp=space(8)
      .w_DESUTE=space(20)
      .w_OpeEsp=0
      .w_CACODICE=space(20)
      .w_BSCROLL=.f.
      .w_TIPCON=space(1)
      .w_PPCOLSUM=0
      .w_PPCOLPIA=0
      .w_PPCOLMDP=0
      .w_cPERDTF=space(3)
      .w_TIPATT=space(1)
      .w_TIPGES=space(1)
      .w_fPROFIN=space(2)
      .w_fSEMLAV=space(2)
      .w_fMATPRI=space(2)
      .w_fMATFAS=space(2)
      .w_fDISINI=space(20)
      .w_fDISFIN=space(20)
      .w_fCODFAM=space(5)
      .w_fCODFAF=space(5)
      .w_CRIELA=space(1)
      .w_fGRUMER=space(5)
      .w_fGRUMEF=space(5)
      .w_fCATOMO=space(5)
      .w_fCATOMF=space(5)
      .w_fCODMAR=space(5)
      .w_fCODMAF=space(5)
      .w_fMAGINI=space(5)
      .w_fMAGFIN=space(5)
      .w_fCODCOM=space(15)
      .w_fCODCOF=space(15)
      .w_fCODATT=space(15)
      .w_fCODATF=space(15)
      .w_fCODFOR=space(15)
      .w_fCODFOF=space(15)
      .w_fCODFOR=space(15)
      .w_fCODFOF=space(15)
      .w_fCODFASI=space(66)
      .w_fCODFASF=space(66)
      .w_DESINI=space(40)
      .w_SELEZM=space(1)
      .w_fTIPGES=space(1)
      .w_DESFIN=space(40)
      .w_DESGRU=space(35)
      .w_DESCAT=space(35)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_DESFORN=space(40)
      .w_DESFAMA=space(35)
      .w_DESMAR=space(35)
      .w_DESGRF=space(35)
      .w_DESCAF=space(35)
      .w_DESFAMF=space(35)
      .w_DESMAF=space(35)
      .w_DESFOF=space(40)
      .w_DESCANF=space(30)
      .w_DESATF=space(30)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
      .w_MAGTEF=space(5)
      .w_MAGTER=space(5)
      .w_RIGALIS=0
      .w_KEYRIF=space(10)
      .w_PPCRIELA=space(1)
      .w_RET=.f.
        .w_PARAM = This.oParentObject
        .w_ReadPar = 'PP'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ReadPar))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,12,.f.)
        .w_BASSUG = mod(.w_PPCOLSUG, (rgb(255,255,255)+1))
        .w_TESSUG = int(.w_PPCOLSUG/(rgb(255,255,255)+1))
        .w_BASCON = mod(.w_PPCOLCON, (rgb(255,255,255)+1))
        .w_BASLAN = mod(.w_PPCOLCON, (rgb(255,255,255)+1))
        .w_TESCON = int(.w_PPCOLCON/(rgb(255,255,255)+1))
        .w_TESLAN = int(.w_PPCOLCON/(rgb(255,255,255)+1))
        .w_BASMIS = mod(.w_PPCOLMIS, (rgb(255,255,255)+1))
        .w_TESMIS = int(.w_PPCOLMIS/(rgb(255,255,255)+1))
        .w_BASSUM = mod(.w_PPCOLSUM, (rgb(255,255,255)+1))
        .w_BASPIA = mod(.w_PPCOLPIA, (rgb(255,255,255)+1))
        .w_BASMDP = mod(.w_PPCOLMDP, (rgb(255,255,255)+1))
        .w_TESSUM = int(.w_PPCOLSUM/(rgb(255,255,255)+1))
        .w_TESPIA = int(.w_PPCOLPIA/(rgb(255,255,255)+1))
        .w_TESMDP = int(.w_PPCOLMDP/(rgb(255,255,255)+1))
      .oPgFrm.Page3.oPag.MIST.Calculate('      1234567890     ',.w_TESMIS,.w_BASMIS)
      .oPgFrm.Page3.oPag.oObj_3_23.Calculate()
        .w_ZoomEspanso = .t.
      .oPgFrm.Page1.oPag.ZoomODP.Calculate()
      .oPgFrm.Page1.oPag.DettODP.Calculate()
          .DoRTCalc(28,28,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .DoRTCalc(30,30,.f.)
        .w_CODSEL = .w_CODSEL
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CODSEL))
          .link_2_2('Full')
        endif
        .w_DESSEL = .w_DESSEL
      .oPgFrm.Page2.oPag.PeriodoODP.Calculate()
          .DoRTCalc(33,36,.f.)
        .w_ODPSEL = Nvl( .w_PeriodoODP.GetVar("OLCODODL") , Space(15))
        .w_sCODART = Nvl(.w_PeriodoODP.GetVar("OLTCOART"),Space(20))
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_sCODART))
          .link_2_17('Full')
        endif
          .DoRTCalc(39,39,.f.)
        .w_DInEff = Nvl( .w_PeriodoODP.GetVar("OLTDTINI") , cp_CharToDate('  /  /    '))
        .w_Dlancio = Nvl( .w_PeriodoODP.GetVar("OLTDTLAN") ,cp_CharToDate('  /  /    '))
        .w_FORSEL = Nvl(.w_PeriodoODP.GetVar("OLTCOFOR"),Space(15))
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_FORSEL))
          .link_2_23('Full')
        endif
          .DoRTCalc(43,44,.f.)
        .w_COLSEL = iif(.w_ZoomODP.GRD.ActiveColumn=0,.w_COLSEL,.w_ZoomODP.GRD.ActiveColumn)
        .w_CODSEL = Nvl( .w_ZoomODP.GetVar('CACODICE') , Space(iif(g_APPLICATION="ADHOC REVOLUTION",20,41)))
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_CODSEL))
          .link_1_22('Full')
        endif
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_sCODART))
          .link_1_23('Full')
        endif
        .DoRTCalc(48,49,.f.)
        if not(empty(.w_sCODART))
          .link_1_25('Full')
        endif
          .DoRTCalc(50,55,.f.)
        .w_PPTIPDTF = 'F'
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
          .DoRTCalc(57,58,.f.)
        .w_ACTIVECOL = .w_ZoomODP.GRD.ActiveColumn
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_MPS_ODL = .w_PARAM
      .oPgFrm.Page2.oPag.oObj_2_26.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_28.Calculate()
        .DoRTCalc(61,64,.f.)
        if not(empty(.w_OpeEsp))
          .link_3_35('Full')
        endif
        .w_CACODICE = .w_ZoomODP.GETVAR('CACODICE')
      .oPgFrm.Page3.oPag.LANC.Calculate('      1234567890     ',.w_TESLAN,.w_BASLAN)
        .w_BSCROLL = .T.
        .w_TIPCON = 'F'
      .oPgFrm.Page3.oPag.SUGGM.Calculate('      1234567890     ',.w_TESSUM,.w_BASSUM)
      .oPgFrm.Page3.oPag.SUGGE.Calculate('      1234567890     ',.w_TESSUG,.w_BASSUG)
      .oPgFrm.Page3.oPag.PIAN.Calculate('      1234567890     ',.w_TESPIA,.w_BASPIA)
      .oPgFrm.Page3.oPag.CONF.Calculate('      1234567890     ',.w_TESCON,.w_BASCON)
      .oPgFrm.Page3.oPag.DPIA.Calculate('      1234567890     ',.w_TESMDP,.w_BASMDP)
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
          .DoRTCalc(68,70,.f.)
        .w_cPERDTF = '000'
        .w_TIPATT = "A"
          .DoRTCalc(73,73,.f.)
        .w_fPROFIN = 'PF'
        .w_fSEMLAV = 'SE'
        .w_fMATPRI = 'MP'
        .w_fMATFAS = iif(g_PRFA='S' AND g_CICLILAV='S' AND !.w_MPS_ODL$'E-C-F-T' ,"FS","XX")
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_fDISINI))
          .link_4_8('Full')
        endif
        .DoRTCalc(79,79,.f.)
        if not(empty(.w_fDISFIN))
          .link_4_9('Full')
        endif
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_fCODFAM))
          .link_4_10('Full')
        endif
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_fCODFAF))
          .link_4_11('Full')
        endif
        .w_CRIELA = "A"
        .DoRTCalc(83,83,.f.)
        if not(empty(.w_fGRUMER))
          .link_4_13('Full')
        endif
        .DoRTCalc(84,84,.f.)
        if not(empty(.w_fGRUMEF))
          .link_4_14('Full')
        endif
        .DoRTCalc(85,85,.f.)
        if not(empty(.w_fCATOMO))
          .link_4_15('Full')
        endif
        .DoRTCalc(86,86,.f.)
        if not(empty(.w_fCATOMF))
          .link_4_16('Full')
        endif
        .DoRTCalc(87,87,.f.)
        if not(empty(.w_fCODMAR))
          .link_4_17('Full')
        endif
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_fCODMAF))
          .link_4_18('Full')
        endif
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_fMAGINI))
          .link_4_19('Full')
        endif
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_fMAGFIN))
          .link_4_20('Full')
        endif
        .DoRTCalc(91,91,.f.)
        if not(empty(.w_fCODCOM))
          .link_4_21('Full')
        endif
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_fCODCOF))
          .link_4_22('Full')
        endif
        .w_fCODATT = " "
        .DoRTCalc(93,93,.f.)
        if not(empty(.w_fCODATT))
          .link_4_23('Full')
        endif
        .w_fCODATF = .w_fCODATT
        .DoRTCalc(94,94,.f.)
        if not(empty(.w_fCODATF))
          .link_4_24('Full')
        endif
        .DoRTCalc(95,95,.f.)
        if not(empty(.w_fCODFOR))
          .link_4_25('Full')
        endif
        .DoRTCalc(96,96,.f.)
        if not(empty(.w_fCODFOF))
          .link_4_26('Full')
        endif
        .DoRTCalc(97,97,.f.)
        if not(empty(.w_fCODFOR))
          .link_4_27('Full')
        endif
        .DoRTCalc(98,98,.f.)
        if not(empty(.w_fCODFOF))
          .link_4_28('Full')
        endif
        .DoRTCalc(99,99,.f.)
        if not(empty(.w_fCODFASI))
          .link_4_29('Full')
        endif
        .DoRTCalc(100,100,.f.)
        if not(empty(.w_fCODFASF))
          .link_4_30('Full')
        endif
      .oPgFrm.Page4.oPag.ZOOMMAGA.Calculate()
          .DoRTCalc(101,101,.f.)
        .w_SELEZM = "S"
        .w_fTIPGES = 'X'
      .oPgFrm.Page4.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
          .DoRTCalc(104,123,.f.)
        .w_KEYRIF = SYS(2015)
    endwith
    this.DoRTCalc(125,126,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page3.oPag.oBtn_3_24.enabled = this.oPgFrm.Page3.oPag.oBtn_3_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_29.enabled = this.oPgFrm.Page2.oPag.oBtn_2_29.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_30.enabled = this.oPgFrm.Page2.oPag.oBtn_2_30.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_31.enabled = this.oPgFrm.Page2.oPag.oBtn_2_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_7.enabled = this.oPgFrm.Page4.oPag.oBtn_4_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsco_kmp
      if this.w_CRIELA $ 'G-M'
        this.w_ZOOMMAGA.grd.enabled=.T.
        this.w_ZOOMMAGA.enabled=.T.
        this.w_SELEZM='S'
        this.notifyevent('w_SELEZM Changed')
      else
        this.w_ZOOMMAGA.grd.enabled=.F.
        this.w_ZOOMMAGA.enabled=.F.
        this.w_SELEZM='D'
        this.notifyevent('w_SELEZM Changed')
      endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_ReadPar<>.w_ReadPar
          .link_1_2('Full')
        endif
        .oPgFrm.Page3.oPag.MIST.Calculate('      1234567890     ',.w_TESMIS,.w_BASMIS)
        .oPgFrm.Page3.oPag.oObj_3_23.Calculate()
        .oPgFrm.Page1.oPag.ZoomODP.Calculate()
        .oPgFrm.Page1.oPag.DettODP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(3,30,.t.)
        if .o_CODICE<>.w_CODICE
            .w_CODSEL = .w_CODSEL
          .link_2_2('Full')
        endif
            .w_DESSEL = .w_DESSEL
        .oPgFrm.Page2.oPag.PeriodoODP.Calculate()
        .DoRTCalc(33,36,.t.)
            .w_ODPSEL = Nvl( .w_PeriodoODP.GetVar("OLCODODL") , Space(15))
            .w_sCODART = Nvl(.w_PeriodoODP.GetVar("OLTCOART"),Space(20))
          .link_2_17('Full')
        .DoRTCalc(39,39,.t.)
            .w_DInEff = Nvl( .w_PeriodoODP.GetVar("OLTDTINI") , cp_CharToDate('  /  /    '))
            .w_Dlancio = Nvl( .w_PeriodoODP.GetVar("OLTDTLAN") ,cp_CharToDate('  /  /    '))
            .w_FORSEL = Nvl(.w_PeriodoODP.GetVar("OLTCOFOR"),Space(15))
          .link_2_23('Full')
        .DoRTCalc(43,44,.t.)
            .w_COLSEL = iif(.w_ZoomODP.GRD.ActiveColumn=0,.w_COLSEL,.w_ZoomODP.GRD.ActiveColumn)
            .w_CODSEL = Nvl( .w_ZoomODP.GetVar('CACODICE') , Space(iif(g_APPLICATION="ADHOC REVOLUTION",20,41)))
          .link_1_22('Full')
          .link_1_23('Full')
        .DoRTCalc(48,48,.t.)
          .link_1_25('Full')
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .DoRTCalc(50,58,.t.)
            .w_ACTIVECOL = .w_ZoomODP.GRD.ActiveColumn
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_28.Calculate()
        .DoRTCalc(60,63,.t.)
          .link_3_35('Full')
            .w_CACODICE = .w_ZoomODP.GETVAR('CACODICE')
        .oPgFrm.Page3.oPag.LANC.Calculate('      1234567890     ',.w_TESLAN,.w_BASLAN)
        .oPgFrm.Page3.oPag.SUGGM.Calculate('      1234567890     ',.w_TESSUM,.w_BASSUM)
        .oPgFrm.Page3.oPag.SUGGE.Calculate('      1234567890     ',.w_TESSUG,.w_BASSUG)
        .oPgFrm.Page3.oPag.PIAN.Calculate('      1234567890     ',.w_TESPIA,.w_BASPIA)
        .oPgFrm.Page3.oPag.CONF.Calculate('      1234567890     ',.w_TESCON,.w_BASCON)
        .oPgFrm.Page3.oPag.DPIA.Calculate('      1234567890     ',.w_TESMDP,.w_BASMDP)
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(66,92,.t.)
        if .o_fCODCOM<>.w_fCODCOM.or. .o_fCODCOF<>.w_fCODCOF
            .w_fCODATT = " "
          .link_4_23('Full')
        endif
        if .o_fCODCOM<>.w_fCODCOM.or. .o_fCODCOF<>.w_fCODCOF
            .w_fCODATF = .w_fCODATT
          .link_4_24('Full')
        endif
        if .o_CRIELA<>.w_CRIELA
          .Calculate_VASGIKTUPU()
        endif
        if .o_SELEZM<>.w_SELEZM
          .Calculate_LOGQKNSSGF()
        endif
        .oPgFrm.Page4.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page4.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
        * --- Area Manuale = Calculate
        * --- gsco_kmp
              .w_ACTIVECOL = .w_ZoomODP.GRD.ActiveColumn
                if .w_ACTIVECOL <> .o_ACTIVECOL and .oPgFrm.ActivePage=1
                    .NotifyEvent('w_zoomodp scrolled')
                    .O_ACTIVECOL = .w_ACTIVECOL
                    select(.w_ZoomODP.ccursor)
                    local l_Row
                    l_Row=recno()
                    if type('i_CurForm')='O' and !IsNull(i_CurForm)
                      if upper(i_CurForm.class) = upper(.Class)
                        Try
                          .w_ZoomODP.GRD.SetFocus()
                        CATCH TO oException
                        EndTry
                      endif
                    endif
                endif 
        
                if .w_CACODICE<>.o_CACODICE and .w_ZoomEspanso
                  .o_CACODICE = .w_CACODICE
                endif
                
                if .w_CACODICE<>.o_CACODICE and Not .w_ZoomEspanso
                  .o_CACODICE = .w_CACODICE
                  .NotifyEvent('SHOWDETTC')
                endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(95,126,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.MIST.Calculate('      1234567890     ',.w_TESMIS,.w_BASMIS)
        .oPgFrm.Page3.oPag.oObj_3_23.Calculate()
        .oPgFrm.Page1.oPag.ZoomODP.Calculate()
        .oPgFrm.Page1.oPag.DettODP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page2.oPag.PeriodoODP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page3.oPag.LANC.Calculate('      1234567890     ',.w_TESLAN,.w_BASLAN)
        .oPgFrm.Page3.oPag.SUGGM.Calculate('      1234567890     ',.w_TESSUM,.w_BASSUM)
        .oPgFrm.Page3.oPag.SUGGE.Calculate('      1234567890     ',.w_TESSUG,.w_BASSUG)
        .oPgFrm.Page3.oPag.PIAN.Calculate('      1234567890     ',.w_TESPIA,.w_BASPIA)
        .oPgFrm.Page3.oPag.CONF.Calculate('      1234567890     ',.w_TESCON,.w_BASCON)
        .oPgFrm.Page3.oPag.DPIA.Calculate('      1234567890     ',.w_TESMDP,.w_BASMDP)
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page4.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page4.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di elaborazione")))
    endwith
  return

  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          .w_ZOOMMAGA.cZoomFile = IIF(.w_CRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_CRIELA = 'G', "QUERY\GSVEGKGF" , "QUERY\GSVEFKGF")
    endwith
  endproc
  proc Calculate_XOHCHPGRKK()
    with this
          * --- Condizione di editing Label e zoom
          .w_LBLMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.GRD.Enabled = .w_ZOOMMAGA.Enabled
    endwith
  endproc
  proc Calculate_LRBMCECHHD()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_VASGIKTUPU()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_CRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_CRIELA $ 'G-M'
          .w_RET = .NotifyEvent("InterrogaMaga")
          .w_SELEZM = IIF(.w_CRIELA $ 'G-M' , 'S', SPACE(1) )
      if .w_CRIELA $ 'G-M'
          .w_RET = .w_ZOOMMAGA.CheckAll()
      endif
      if .w_CRIELA = 'A'
          .w_RET = .w_ZOOMMAGA.UnCheckAll()
      endif
    endwith
  endproc
  proc Calculate_LOGQKNSSGF()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
      if .w_SELEZM='S'
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
      if .w_SELEZM='D'
          .w_RET = .w_ZOOMMAGA.uncheckall()
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPPTIPDTF_1_38.enabled = this.oPgFrm.Page1.oPag.oPPTIPDTF_1_38.mCond()
    this.oPgFrm.Page1.oPag.oPP___DTF_1_39.enabled = this.oPgFrm.Page1.oPag.oPP___DTF_1_39.mCond()
    this.oPgFrm.Page4.oPag.ofMATFAS_4_6.enabled = this.oPgFrm.Page4.oPag.ofMATFAS_4_6.mCond()
    this.oPgFrm.Page4.oPag.ofCODCOM_4_21.enabled = this.oPgFrm.Page4.oPag.ofCODCOM_4_21.mCond()
    this.oPgFrm.Page4.oPag.ofCODCOF_4_22.enabled = this.oPgFrm.Page4.oPag.ofCODCOF_4_22.mCond()
    this.oPgFrm.Page4.oPag.ofCODATT_4_23.enabled = this.oPgFrm.Page4.oPag.ofCODATT_4_23.mCond()
    this.oPgFrm.Page4.oPag.ofCODATF_4_24.enabled = this.oPgFrm.Page4.oPag.ofCODATF_4_24.mCond()
    this.oPgFrm.Page4.oPag.ofCODFOR_4_25.enabled = this.oPgFrm.Page4.oPag.ofCODFOR_4_25.mCond()
    this.oPgFrm.Page4.oPag.ofCODFOF_4_26.enabled = this.oPgFrm.Page4.oPag.ofCODFOF_4_26.mCond()
    this.oPgFrm.Page4.oPag.ofCODFOR_4_27.enabled = this.oPgFrm.Page4.oPag.ofCODFOR_4_27.mCond()
    this.oPgFrm.Page4.oPag.ofCODFOF_4_28.enabled = this.oPgFrm.Page4.oPag.ofCODFOF_4_28.mCond()
    this.oPgFrm.Page4.oPag.ofCODFASI_4_29.enabled = this.oPgFrm.Page4.oPag.ofCODFASI_4_29.mCond()
    this.oPgFrm.Page4.oPag.ofCODFASF_4_30.enabled = this.oPgFrm.Page4.oPag.ofCODFASF_4_30.mCond()
    this.oPgFrm.Page4.oPag.oSELEZM_4_39.enabled_(this.oPgFrm.Page4.oPag.oSELEZM_4_39.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page3.oPag.oStr_3_19.visible=!this.oPgFrm.Page3.oPag.oStr_3_19.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_20.visible=!this.oPgFrm.Page3.oPag.oStr_3_20.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_21.visible=!this.oPgFrm.Page3.oPag.oStr_3_21.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page2.oPag.oFORSEL_2_23.visible=!this.oPgFrm.Page2.oPag.oFORSEL_2_23.mHide()
    this.oPgFrm.Page2.oPag.oDESFOR_2_24.visible=!this.oPgFrm.Page2.oPag.oDESFOR_2_24.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page1.oPag.osPUNRIO_1_29.visible=!this.oPgFrm.Page1.oPag.osPUNRIO_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.osLOTMIN_1_35.visible=!this.oPgFrm.Page1.oPag.osLOTMIN_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_33.visible=!this.oPgFrm.Page3.oPag.oStr_3_33.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_36.visible=!this.oPgFrm.Page3.oPag.oStr_3_36.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_37.visible=!this.oPgFrm.Page3.oPag.oStr_3_37.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_38.visible=!this.oPgFrm.Page3.oPag.oStr_3_38.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_40.visible=!this.oPgFrm.Page3.oPag.oStr_3_40.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_41.visible=!this.oPgFrm.Page3.oPag.oStr_3_41.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_42.visible=!this.oPgFrm.Page3.oPag.oStr_3_42.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_48.visible=!this.oPgFrm.Page3.oPag.oStr_3_48.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_49.visible=!this.oPgFrm.Page3.oPag.oStr_3_49.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_50.visible=!this.oPgFrm.Page3.oPag.oStr_3_50.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_51.visible=!this.oPgFrm.Page3.oPag.oStr_3_51.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_52.visible=!this.oPgFrm.Page3.oPag.oStr_3_52.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_53.visible=!this.oPgFrm.Page3.oPag.oStr_3_53.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_54.visible=!this.oPgFrm.Page3.oPag.oStr_3_54.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_55.visible=!this.oPgFrm.Page3.oPag.oStr_3_55.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_56.visible=!this.oPgFrm.Page3.oPag.oStr_3_56.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_57.visible=!this.oPgFrm.Page3.oPag.oStr_3_57.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_58.visible=!this.oPgFrm.Page3.oPag.oStr_3_58.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_59.visible=!this.oPgFrm.Page3.oPag.oStr_3_59.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_60.visible=!this.oPgFrm.Page3.oPag.oStr_3_60.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_61.visible=!this.oPgFrm.Page3.oPag.oStr_3_61.mHide()
    this.oPgFrm.Page4.oPag.ofMATFAS_4_6.visible=!this.oPgFrm.Page4.oPag.ofMATFAS_4_6.mHide()
    this.oPgFrm.Page4.oPag.ofCODFOR_4_27.visible=!this.oPgFrm.Page4.oPag.ofCODFOR_4_27.mHide()
    this.oPgFrm.Page4.oPag.ofCODFOF_4_28.visible=!this.oPgFrm.Page4.oPag.ofCODFOF_4_28.mHide()
    this.oPgFrm.Page4.oPag.ofCODFASI_4_29.visible=!this.oPgFrm.Page4.oPag.ofCODFASI_4_29.mHide()
    this.oPgFrm.Page4.oPag.ofCODFASF_4_30.visible=!this.oPgFrm.Page4.oPag.ofCODFASF_4_30.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_65.visible=!this.oPgFrm.Page4.oPag.oStr_4_65.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_66.visible=!this.oPgFrm.Page4.oPag.oStr_4_66.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsco_kmp
    if inlist(cEvent , 'w_SELEZM Changed' , 'w_CRIELA Changed')
        if this.w_SELEZM='S'
          Select (this.w_zoommaga.cCursor)
          this.w_RIGALIS = Recno()
          update (this.w_zoommaga.cCursor) set XCHK=1
          local m_olderr
          m_olderr=on('ERROR')
          on error =.t.
          Select (this.w_zoommaga.cCursor)
          GO this.w_RIGALIS
          on error &m_olderr
        else
          Select (this.w_zoommaga.cCursor)
          this.w_RIGALIS = Recno()
          update (this.w_zoommaga.cCursor) set XCHK=0
          local m_olderr
          m_olderr=on('ERROR')
          on error =.t.
          Select (this.w_zoommaga.cCursor)
          GO this.w_RIGALIS
          on error &m_olderr    
        endif
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.MIST.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_23.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomODP.Event(cEvent)
      .oPgFrm.Page1.oPag.DettODP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page2.oPag.PeriodoODP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_26.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_27.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_28.Event(cEvent)
      .oPgFrm.Page3.oPag.LANC.Event(cEvent)
      .oPgFrm.Page3.oPag.SUGGM.Event(cEvent)
      .oPgFrm.Page3.oPag.SUGGE.Event(cEvent)
      .oPgFrm.Page3.oPag.PIAN.Event(cEvent)
      .oPgFrm.Page3.oPag.CONF.Event(cEvent)
      .oPgFrm.Page3.oPag.DPIA.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed") or lower(cEvent)==lower("w_ZOOMMAGA after query")
          .Calculate_XOHCHPGRKK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMMAGA menucheck") or lower(cEvent)==lower("w_ZOOMMAGA row checked") or lower(cEvent)==lower("w_ZOOMMAGA row unchecked") or lower(cEvent)==lower("w_zoommaga rowcheckall") or lower(cEvent)==lower("w_zoommaga rowuncheckall")
          .Calculate_LRBMCECHHD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_VASGIKTUPU()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page4.oPag.LBLMAGA.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsco_kmp
    if upper(cEvent)='FORMLOAD'
      this.w_ZOOMMAGA.GRD.SCROLLBARS=2
    endif
    if upper(cEvent)='W_CRIELA CHANGED'
      if this.w_CRIELA $ 'G-M'
        this.w_ZOOMMAGA.enabled=.T.
        this.w_SELEZM='S'
        this.notifyevent('w_SELEZM Changed')
      else
        this.w_ZOOMMAGA.enabled=.F.
        this.w_SELEZM=' '
        this.notifyevent('w_SELEZM Changed')
      endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadPar
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPar) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPar)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCOLMPS,PPFONMPS,PPNUMPER,PPCOLSUM,PPCOLPIA,PPCOLLAN,PPCOLMIS,PPCOLMCO,PPCOLFES,PPNMAXPE,PPOPEODL,PPELAODL,PPORAODL,PPCOLSUG,PPCOLCON,PPCOLMDP,PP___DTF,PPCRIELA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ReadPar);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ReadPar)
            select PPCODICE,PPCOLMPS,PPFONMPS,PPNUMPER,PPCOLSUM,PPCOLPIA,PPCOLLAN,PPCOLMIS,PPCOLMCO,PPCOLFES,PPNMAXPE,PPOPEODL,PPELAODL,PPORAODL,PPCOLSUG,PPCOLCON,PPCOLMDP,PP___DTF,PPCRIELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPar = NVL(_Link_.PPCODICE,space(2))
      this.w_LARGCOL = NVL(_Link_.PPCOLMPS,0)
      this.w_DIMFONT = NVL(_Link_.PPFONMPS,0)
      this.w_NUMPER = NVL(_Link_.PPNUMPER,0)
      this.w_PPCOLSUM = NVL(_Link_.PPCOLSUM,0)
      this.w_PPCOLPIA = NVL(_Link_.PPCOLPIA,0)
      this.w_PPCOLMLA = NVL(_Link_.PPCOLLAN,0)
      this.w_PPCOLMIS = NVL(_Link_.PPCOLMIS,0)
      this.w_PPCOLMCO = NVL(_Link_.PPCOLMCO,0)
      this.w_PPCOLFES = NVL(_Link_.PPCOLFES,0)
      this.w_NMAXPE = NVL(_Link_.PPNMAXPE,0)
      this.w_OpeEsp = NVL(_Link_.PPOPEODL,0)
      this.w_DataEsp = NVL(cp_ToDate(_Link_.PPELAODL),ctod("  /  /  "))
      this.w_OraEsp = NVL(_Link_.PPORAODL,space(8))
      this.w_PPCOLSUG = NVL(_Link_.PPCOLSUG,0)
      this.w_PPCOLCON = NVL(_Link_.PPCOLCON,0)
      this.w_PPCOLMDP = NVL(_Link_.PPCOLMDP,0)
      this.w_PP___DTF = NVL(_Link_.PP___DTF,0)
      this.w_PPCRIELA = NVL(_Link_.PPCRIELA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPar = space(2)
      endif
      this.w_LARGCOL = 0
      this.w_DIMFONT = 0
      this.w_NUMPER = 0
      this.w_PPCOLSUM = 0
      this.w_PPCOLPIA = 0
      this.w_PPCOLMLA = 0
      this.w_PPCOLMIS = 0
      this.w_PPCOLMCO = 0
      this.w_PPCOLFES = 0
      this.w_NMAXPE = 0
      this.w_OpeEsp = 0
      this.w_DataEsp = ctod("  /  /  ")
      this.w_OraEsp = space(8)
      this.w_PPCOLSUG = 0
      this.w_PPCOLCON = 0
      this.w_PPCOLMDP = 0
      this.w_PP___DTF = 0
      this.w_PPCRIELA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPar Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSEL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSEL)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSEL = NVL(_Link_.CACODICE,space(20))
      this.w_DESSEL = NVL(_Link_.CADESART,space(40))
      this.w_sCODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODSEL = space(20)
      endif
      this.w_DESSEL = space(40)
      this.w_sCODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=sCODART
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_sCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_sCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_sCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_sCODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_sCODART = NVL(_Link_.ARCODART,space(20))
      this.w_sDESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_sCODART = space(20)
      endif
      this.w_sDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_sCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSEL
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSEL)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSEL = NVL(_Link_.CACODICE,space(41))
      this.w_sCODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODSEL = space(41)
      endif
      this.w_sCODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=sCODART
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_sCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_sCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPGES,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_sCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_sCODART)
            select ARCODART,ARDESART,ARTIPGES,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_sCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESCSEL = NVL(_Link_.ARDESART,space(40))
      this.w_sTIPGES = NVL(_Link_.ARTIPGES,space(1))
      this.w_UMARTSE = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_sCODART = space(20)
      endif
      this.w_DESCSEL = space(40)
      this.w_sTIPGES = space(1)
      this.w_UMARTSE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_sCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=sCODART
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_lTable = "PAR_RIOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2], .t., this.PAR_RIOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_sCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_sCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRSCOMIN,PRPERSCA,PRLOTRIO,PRQTAMIN,PRPUNRIO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODART="+cp_ToStrODBC(this.w_sCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_sCODART)
            select PRCODART,PRSCOMIN,PRPERSCA,PRLOTRIO,PRQTAMIN,PRPUNRIO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_sCODART = NVL(_Link_.PRCODART,space(20))
      this.w_sSCOMIN = NVL(_Link_.PRSCOMIN,0)
      this.w_sSCARTO = NVL(_Link_.PRPERSCA,0)
      this.w_sLOTRIO = NVL(_Link_.PRLOTRIO,0)
      this.w_sLOTMIN = NVL(_Link_.PRQTAMIN,0)
      this.w_sPUNRIO = NVL(_Link_.PRPUNRIO,0)
    else
      if i_cCtrl<>'Load'
        this.w_sCODART = space(20)
      endif
      this.w_sSCOMIN = 0
      this.w_sSCARTO = 0
      this.w_sLOTRIO = 0
      this.w_sLOTMIN = 0
      this.w_sPUNRIO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)
      cp_ShowWarn(i_cKey,this.PAR_RIOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_sCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OpeEsp
  func Link_3_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OpeEsp) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OpeEsp)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OpeEsp);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OpeEsp)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OpeEsp = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OpeEsp = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OpeEsp Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fDISINI
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fDISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_fDISINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_fDISINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fDISINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fDISINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'ofDISINI_4_8'),i_cWhere,'GSMA_ACA',"Codici articoli",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fDISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_fDISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_fDISINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fDISINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_fDISINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_FDISINI) AND (.w_fDISINI<=.w_fDISFIN or empty(.w_fDISFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_fDISINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fDISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fDISFIN
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fDISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_fDISFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_fDISFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fDISFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fDISFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'ofDISFIN_4_9'),i_cWhere,'GSMA_ACA',"Codici articoli",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fDISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_fDISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_fDISFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fDISFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_fDISFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_FDISFIN) AND (.w_fDISINI<=.w_fDISFIN or empty(.w_fDISINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_fDISFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fDISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFAM
  func Link_4_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_fCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_fCODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'ofCODFAM_4_10'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_fCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_fCODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMA = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFAM = space(5)
      endif
      this.w_DESFAMA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODFAM = space(5)
        this.w_DESFAMA = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFAF
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_fCODFAF)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_fCODFAF))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFAF)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFAF) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'ofCODFAF_4_11'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_fCODFAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_fCODFAF)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFAF = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFAF = space(5)
      endif
      this.w_DESFAMF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODFAF = space(5)
        this.w_DESFAMF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fGRUMER
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_fGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_fGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ofGRUMER_4_13'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_fGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_fGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fGRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMEF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fGRUMER = space(5)
        this.w_DESGRU = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fGRUMEF
  func Link_4_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fGRUMEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_fGRUMEF)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_fGRUMEF))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fGRUMEF)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fGRUMEF) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ofGRUMEF_4_14'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fGRUMEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_fGRUMEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_fGRUMEF)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fGRUMEF = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fGRUMEF = space(5)
      endif
      this.w_DESGRF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fGRUMEF = space(5)
        this.w_DESGRF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fGRUMEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCATOMO
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_fCATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_fCATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'ofCATOMO_4_15'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_fCATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_fCATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCATOMO = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCATOMO = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCATOMO = space(5)
        this.w_DESCAT = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCATOMF
  func Link_4_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCATOMF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_fCATOMF)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_fCATOMF))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCATOMF)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCATOMF) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'ofCATOMF_4_16'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCATOMF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_fCATOMF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_fCATOMF)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCATOMF = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCATOMF = space(5)
      endif
      this.w_DESCAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCATOMF = space(5)
        this.w_DESCAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCATOMF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODMAR
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_fCODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_fCODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'ofCODMAR_4_17'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_fCODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_fCODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODMAR = space(5)
        this.w_DESMAR = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODMAF
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODMAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_fCODMAF)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_fCODMAF))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODMAF)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODMAF) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'ofCODMAF_4_18'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODMAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_fCODMAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_fCODMAF)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODMAF = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_fCODMAF = space(5)
      endif
      this.w_DESMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODMAF = space(5)
        this.w_DESMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODMAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fMAGINI
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_fMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_fMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'ofMAGINI_4_19'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_fMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_fMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fMAGINI <= .w_fMAGFIN OR EMPTY(.w_fMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fMAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fMAGFIN
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_fMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_fMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'ofMAGFIN_4_20'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_fMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_fMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fMAGINI <= .w_fMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fMAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODCOM
  func Link_4_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_fCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_fCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_fCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_fCODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'ofCODCOM_4_21'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_fCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_fCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODCOM = space(15)
        this.w_DESCAN = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODCOF
  func Link_4_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODCOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_fCODCOF)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_fCODCOF))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODCOF)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_fCODCOF)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_fCODCOF)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODCOF) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'ofCODCOF_4_22'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODCOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_fCODCOF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_fCODCOF)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODCOF = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCANF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODCOF = space(15)
      endif
      this.w_DESCANF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODCOF = space(15)
        this.w_DESCANF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODCOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODATT
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_fCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_fCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_fCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_fCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_fCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'ofCODATT_4_23'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_fCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_fCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_fCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_fCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODATT<=.w_fCODATF or empty(.w_fCODATF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODATT = space(15)
        this.w_DESATT = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODATF
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODATF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_fCODATF)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_fCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_fCODATF))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODATF)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_fCODATF)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_fCODATF)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_fCODATF) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'ofCODATF_4_24'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_fCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODATF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_fCODATF);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_fCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_fCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_fCODATF)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODATF = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATF = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_fCODATF = space(15)
      endif
      this.w_DESATF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODATT<=.w_fCODATF or empty(.w_fCODATT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODATF = space(15)
        this.w_DESATF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODATF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFOR
  func Link_4_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_fCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_fCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ofCODFOR_4_25'),i_cWhere,'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_fCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_fCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_fCODFOR) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTER) OR .w_MPS_ODL $ 'E-T') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        endif
        this.w_fCODFOR = space(15)
        this.w_DESFOR = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFOF
  func Link_4_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_fCODFOF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_fCODFOF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFOF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFOF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ofCODFOF_4_26'),i_cWhere,'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_fCODFOF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_fCODFOF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFOF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFOF = space(15)
      endif
      this.w_DESFOF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_fCODFOF) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTEF) OR .w_MPS_ODL $ 'E-T') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente")
        endif
        this.w_fCODFOF = space(15)
        this.w_DESFOF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFOR
  func Link_4_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_fCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_fCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ofCODFOR_4_27'),i_cWhere,'',"Elenco fornitori",'gscozscs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_fCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_fCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFORN = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTER = NVL(_Link_.ANMAGTER,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFOR = space(15)
      endif
      this.w_DESFORN = space(40)
      this.w_MAGTER = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_fCODFOR) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTER) OR .w_MPS_ODL='E') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        endif
        this.w_fCODFOR = space(15)
        this.w_DESFORN = space(40)
        this.w_MAGTER = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFOF
  func Link_4_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_fCODFOF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_fCODFOF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFOF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFOF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ofCODFOF_4_28'),i_cWhere,'',"Elenco fornitori",'gscozscs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_fCODFOF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_fCODFOF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFOF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOF = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTEF = NVL(_Link_.ANMAGTER,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFOF = space(15)
      endif
      this.w_DESFOF = space(40)
      this.w_MAGTEF = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_fCODFOF) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTEF) OR .w_MPS_ODL='E') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        endif
        this.w_fCODFOF = space(15)
        this.w_DESFOF = space(40)
        this.w_MAGTEF = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFASI
  func Link_4_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFASI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODFAS like "+cp_ToStrODBC(trim(this.w_fCODFASI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODFAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODFAS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODFAS',trim(this.w_fCODFASI))
          select CACODFAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODFAS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFASI)==trim(_Link_.CACODFAS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFASI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODFAS',cp_AbsName(oSource.parent,'ofCODFASI_4_29'),i_cWhere,'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                     +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',oSource.xKey(1))
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFASI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(this.w_fCODFASI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',this.w_fCODFASI)
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFASI = NVL(_Link_.CACODFAS,space(66))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFASI = space(66)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODFASI<=.w_fCODFASF or empty(.w_fCODFASF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODFASI = space(66)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODFAS,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFASI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fCODFASF
  func Link_4_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fCODFASF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODFAS like "+cp_ToStrODBC(trim(this.w_fCODFASF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODFAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODFAS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODFAS',trim(this.w_fCODFASF))
          select CACODFAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODFAS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fCODFASF)==trim(_Link_.CACODFAS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fCODFASF) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODFAS',cp_AbsName(oSource.parent,'ofCODFASF_4_30'),i_cWhere,'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                     +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',oSource.xKey(1))
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fCODFASF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODFAS="+cp_ToStrODBC(this.w_fCODFASF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODFAS',this.w_fCODFASF)
            select CACODFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fCODFASF = NVL(_Link_.CACODFAS,space(66))
    else
      if i_cCtrl<>'Load'
        this.w_fCODFASF = space(66)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_fCODFASI<=.w_fCODFASF or empty(.w_fCODFASI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_fCODFASF = space(66)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODFAS,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fCODFASF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oCODSEL_2_2.value==this.w_CODSEL)
      this.oPgFrm.Page2.oPag.oCODSEL_2_2.value=this.w_CODSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSEL_2_3.value==this.w_DESSEL)
      this.oPgFrm.Page2.oPag.oDESSEL_2_3.value=this.w_DESSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oPSELR_2_4.value==this.w_PSELR)
      this.oPgFrm.Page2.oPag.oPSELR_2_4.value=this.w_PSELR
    endif
    if not(this.oPgFrm.Page2.oPag.oTipoPeriodo_2_5.value==this.w_TipoPeriodo)
      this.oPgFrm.Page2.oPag.oTipoPeriodo_2_5.value=this.w_TipoPeriodo
    endif
    if not(this.oPgFrm.Page2.oPag.oDIP_2_6.value==this.w_DIP)
      this.oPgFrm.Page2.oPag.oDIP_2_6.value=this.w_DIP
    endif
    if not(this.oPgFrm.Page2.oPag.oDFP_2_7.value==this.w_DFP)
      this.oPgFrm.Page2.oPag.oDFP_2_7.value=this.w_DFP
    endif
    if not(this.oPgFrm.Page2.oPag.oODPSEL_2_16.value==this.w_ODPSEL)
      this.oPgFrm.Page2.oPag.oODPSEL_2_16.value=this.w_ODPSEL
    endif
    if not(this.oPgFrm.Page2.oPag.osCODART_2_17.value==this.w_sCODART)
      this.oPgFrm.Page2.oPag.osCODART_2_17.value=this.w_sCODART
    endif
    if not(this.oPgFrm.Page2.oPag.osDESART_2_18.value==this.w_sDESART)
      this.oPgFrm.Page2.oPag.osDESART_2_18.value=this.w_sDESART
    endif
    if not(this.oPgFrm.Page2.oPag.oDInEff_2_20.value==this.w_DInEff)
      this.oPgFrm.Page2.oPag.oDInEff_2_20.value=this.w_DInEff
    endif
    if not(this.oPgFrm.Page2.oPag.oDlancio_2_22.value==this.w_Dlancio)
      this.oPgFrm.Page2.oPag.oDlancio_2_22.value=this.w_Dlancio
    endif
    if not(this.oPgFrm.Page2.oPag.oFORSEL_2_23.value==this.w_FORSEL)
      this.oPgFrm.Page2.oPag.oFORSEL_2_23.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_24.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_24.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCSEL_1_26.value==this.w_DESCSEL)
      this.oPgFrm.Page1.oPag.oDESCSEL_1_26.value=this.w_DESCSEL
    endif
    if not(this.oPgFrm.Page1.oPag.osTIPGES_1_28.RadioValue()==this.w_sTIPGES)
      this.oPgFrm.Page1.oPag.osTIPGES_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.osPUNRIO_1_29.value==this.w_sPUNRIO)
      this.oPgFrm.Page1.oPag.osPUNRIO_1_29.value=this.w_sPUNRIO
    endif
    if not(this.oPgFrm.Page1.oPag.osSCOMIN_1_33.value==this.w_sSCOMIN)
      this.oPgFrm.Page1.oPag.osSCOMIN_1_33.value=this.w_sSCOMIN
    endif
    if not(this.oPgFrm.Page1.oPag.osLOTMIN_1_35.value==this.w_sLOTMIN)
      this.oPgFrm.Page1.oPag.osLOTMIN_1_35.value=this.w_sLOTMIN
    endif
    if not(this.oPgFrm.Page1.oPag.osLOTRIO_1_37.value==this.w_sLOTRIO)
      this.oPgFrm.Page1.oPag.osLOTRIO_1_37.value=this.w_sLOTRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oPPTIPDTF_1_38.RadioValue()==this.w_PPTIPDTF)
      this.oPgFrm.Page1.oPag.oPPTIPDTF_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPP___DTF_1_39.value==this.w_PP___DTF)
      this.oPgFrm.Page1.oPag.oPP___DTF_1_39.value=this.w_PP___DTF
    endif
    if not(this.oPgFrm.Page3.oPag.oDataEsp_3_27.value==this.w_DataEsp)
      this.oPgFrm.Page3.oPag.oDataEsp_3_27.value=this.w_DataEsp
    endif
    if not(this.oPgFrm.Page3.oPag.oOraEsp_3_28.value==this.w_OraEsp)
      this.oPgFrm.Page3.oPag.oOraEsp_3_28.value=this.w_OraEsp
    endif
    if not(this.oPgFrm.Page3.oPag.oDESUTE_3_30.value==this.w_DESUTE)
      this.oPgFrm.Page3.oPag.oDESUTE_3_30.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page3.oPag.oOpeEsp_3_35.value==this.w_OpeEsp)
      this.oPgFrm.Page3.oPag.oOpeEsp_3_35.value=this.w_OpeEsp
    endif
    if not(this.oPgFrm.Page4.oPag.ofPROFIN_4_3.RadioValue()==this.w_fPROFIN)
      this.oPgFrm.Page4.oPag.ofPROFIN_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofSEMLAV_4_4.RadioValue()==this.w_fSEMLAV)
      this.oPgFrm.Page4.oPag.ofSEMLAV_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofMATPRI_4_5.RadioValue()==this.w_fMATPRI)
      this.oPgFrm.Page4.oPag.ofMATPRI_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofMATFAS_4_6.RadioValue()==this.w_fMATFAS)
      this.oPgFrm.Page4.oPag.ofMATFAS_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofDISINI_4_8.value==this.w_fDISINI)
      this.oPgFrm.Page4.oPag.ofDISINI_4_8.value=this.w_fDISINI
    endif
    if not(this.oPgFrm.Page4.oPag.ofDISFIN_4_9.value==this.w_fDISFIN)
      this.oPgFrm.Page4.oPag.ofDISFIN_4_9.value=this.w_fDISFIN
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFAM_4_10.value==this.w_fCODFAM)
      this.oPgFrm.Page4.oPag.ofCODFAM_4_10.value=this.w_fCODFAM
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFAF_4_11.value==this.w_fCODFAF)
      this.oPgFrm.Page4.oPag.ofCODFAF_4_11.value=this.w_fCODFAF
    endif
    if not(this.oPgFrm.Page4.oPag.oCRIELA_4_12.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page4.oPag.oCRIELA_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofGRUMER_4_13.value==this.w_fGRUMER)
      this.oPgFrm.Page4.oPag.ofGRUMER_4_13.value=this.w_fGRUMER
    endif
    if not(this.oPgFrm.Page4.oPag.ofGRUMEF_4_14.value==this.w_fGRUMEF)
      this.oPgFrm.Page4.oPag.ofGRUMEF_4_14.value=this.w_fGRUMEF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCATOMO_4_15.value==this.w_fCATOMO)
      this.oPgFrm.Page4.oPag.ofCATOMO_4_15.value=this.w_fCATOMO
    endif
    if not(this.oPgFrm.Page4.oPag.ofCATOMF_4_16.value==this.w_fCATOMF)
      this.oPgFrm.Page4.oPag.ofCATOMF_4_16.value=this.w_fCATOMF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODMAR_4_17.value==this.w_fCODMAR)
      this.oPgFrm.Page4.oPag.ofCODMAR_4_17.value=this.w_fCODMAR
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODMAF_4_18.value==this.w_fCODMAF)
      this.oPgFrm.Page4.oPag.ofCODMAF_4_18.value=this.w_fCODMAF
    endif
    if not(this.oPgFrm.Page4.oPag.ofMAGINI_4_19.value==this.w_fMAGINI)
      this.oPgFrm.Page4.oPag.ofMAGINI_4_19.value=this.w_fMAGINI
    endif
    if not(this.oPgFrm.Page4.oPag.ofMAGFIN_4_20.value==this.w_fMAGFIN)
      this.oPgFrm.Page4.oPag.ofMAGFIN_4_20.value=this.w_fMAGFIN
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODCOM_4_21.value==this.w_fCODCOM)
      this.oPgFrm.Page4.oPag.ofCODCOM_4_21.value=this.w_fCODCOM
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODCOF_4_22.value==this.w_fCODCOF)
      this.oPgFrm.Page4.oPag.ofCODCOF_4_22.value=this.w_fCODCOF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODATT_4_23.value==this.w_fCODATT)
      this.oPgFrm.Page4.oPag.ofCODATT_4_23.value=this.w_fCODATT
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODATF_4_24.value==this.w_fCODATF)
      this.oPgFrm.Page4.oPag.ofCODATF_4_24.value=this.w_fCODATF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFOR_4_25.value==this.w_fCODFOR)
      this.oPgFrm.Page4.oPag.ofCODFOR_4_25.value=this.w_fCODFOR
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFOF_4_26.value==this.w_fCODFOF)
      this.oPgFrm.Page4.oPag.ofCODFOF_4_26.value=this.w_fCODFOF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFOR_4_27.value==this.w_fCODFOR)
      this.oPgFrm.Page4.oPag.ofCODFOR_4_27.value=this.w_fCODFOR
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFOF_4_28.value==this.w_fCODFOF)
      this.oPgFrm.Page4.oPag.ofCODFOF_4_28.value=this.w_fCODFOF
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFASI_4_29.value==this.w_fCODFASI)
      this.oPgFrm.Page4.oPag.ofCODFASI_4_29.value=this.w_fCODFASI
    endif
    if not(this.oPgFrm.Page4.oPag.ofCODFASF_4_30.value==this.w_fCODFASF)
      this.oPgFrm.Page4.oPag.ofCODFASF_4_30.value=this.w_fCODFASF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESINI_4_31.value==this.w_DESINI)
      this.oPgFrm.Page4.oPag.oDESINI_4_31.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page4.oPag.oSELEZM_4_39.RadioValue()==this.w_SELEZM)
      this.oPgFrm.Page4.oPag.oSELEZM_4_39.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.ofTIPGES_4_40.RadioValue()==this.w_fTIPGES)
      this.oPgFrm.Page4.oPag.ofTIPGES_4_40.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFIN_4_41.value==this.w_DESFIN)
      this.oPgFrm.Page4.oPag.oDESFIN_4_41.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESGRU_4_42.value==this.w_DESGRU)
      this.oPgFrm.Page4.oPag.oDESGRU_4_42.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAT_4_43.value==this.w_DESCAT)
      this.oPgFrm.Page4.oPag.oDESCAT_4_43.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAN_4_44.value==this.w_DESCAN)
      this.oPgFrm.Page4.oPag.oDESCAN_4_44.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESATT_4_45.value==this.w_DESATT)
      this.oPgFrm.Page4.oPag.oDESATT_4_45.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFORN_4_46.value==this.w_DESFORN)
      this.oPgFrm.Page4.oPag.oDESFORN_4_46.value=this.w_DESFORN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFAMA_4_47.value==this.w_DESFAMA)
      this.oPgFrm.Page4.oPag.oDESFAMA_4_47.value=this.w_DESFAMA
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAR_4_48.value==this.w_DESMAR)
      this.oPgFrm.Page4.oPag.oDESMAR_4_48.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page4.oPag.oDESGRF_4_49.value==this.w_DESGRF)
      this.oPgFrm.Page4.oPag.oDESGRF_4_49.value=this.w_DESGRF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAF_4_50.value==this.w_DESCAF)
      this.oPgFrm.Page4.oPag.oDESCAF_4_50.value=this.w_DESCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFAMF_4_51.value==this.w_DESFAMF)
      this.oPgFrm.Page4.oPag.oDESFAMF_4_51.value=this.w_DESFAMF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAF_4_52.value==this.w_DESMAF)
      this.oPgFrm.Page4.oPag.oDESMAF_4_52.value=this.w_DESMAF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESFOF_4_53.value==this.w_DESFOF)
      this.oPgFrm.Page4.oPag.oDESFOF_4_53.value=this.w_DESFOF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCANF_4_54.value==this.w_DESCANF)
      this.oPgFrm.Page4.oPag.oDESCANF_4_54.value=this.w_DESCANF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESATF_4_55.value==this.w_DESATF)
      this.oPgFrm.Page4.oPag.oDESATF_4_55.value=this.w_DESATF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAGI_4_57.value==this.w_DESMAGI)
      this.oPgFrm.Page4.oPag.oDESMAGI_4_57.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAGF_4_58.value==this.w_DESMAGF)
      this.oPgFrm.Page4.oPag.oDESMAGF_4_58.value=this.w_DESMAGF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(COCHKAR(.w_FDISINI) AND (.w_fDISINI<=.w_fDISFIN or empty(.w_fDISFIN)))  and not(empty(.w_fDISINI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofDISINI_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not(COCHKAR(.w_FDISFIN) AND (.w_fDISINI<=.w_fDISFIN or empty(.w_fDISINI)))  and not(empty(.w_fDISFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofDISFIN_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not(.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAF))  and not(empty(.w_fCODFAM))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFAM_4_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODFAM<=.w_fCODFAF or empty(.w_fCODFAM))  and not(empty(.w_fCODFAF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFAF_4_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMEF))  and not(empty(.w_fGRUMER))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofGRUMER_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fGRUMER<=.w_fGRUMEF or empty(.w_fGRUMER))  and not(empty(.w_fGRUMEF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofGRUMEF_4_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMF))  and not(empty(.w_fCATOMO))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCATOMO_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCATOMO<=.w_fCATOMF or empty(.w_fCATOMO))  and not(empty(.w_fCATOMF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCATOMF_4_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAF))  and not(empty(.w_fCODMAR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODMAR_4_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODMAR<=.w_fCODMAF or empty(.w_fCODMAR))  and not(empty(.w_fCODMAF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODMAF_4_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fMAGINI <= .w_fMAGFIN OR EMPTY(.w_fMAGFIN))  and not(empty(.w_fMAGINI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofMAGINI_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fMAGINI <= .w_fMAGFIN)  and not(empty(.w_fMAGFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofMAGFIN_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOF))  and (g_COMM="S" OR g_PERCAN="S")  and not(empty(.w_fCODCOM))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODCOM_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODCOM<=.w_fCODCOF or empty(.w_fCODCOM))  and (g_COMM="S" OR g_PERCAN="S")  and not(empty(.w_fCODCOF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODCOF_4_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODATT<=.w_fCODATF or empty(.w_fCODATF))  and (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)  and not(empty(.w_fCODATT))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODATT_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODATT<=.w_fCODATF or empty(.w_fCODATT))  and (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)  and not(empty(.w_fCODATF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODATF_4_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(NOT EMPTY(.w_fCODFOR) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTER) OR .w_MPS_ODL $ 'E-T') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOF))  and (.w_MPS_ODL $ 'E-T')  and not(empty(.w_fCODFOR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFOR_4_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente")
          case   not(NOT EMPTY(.w_fCODFOF) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTEF) OR .w_MPS_ODL $ 'E-T') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOR))  and (.w_MPS_ODL $ 'E-T')  and not(empty(.w_fCODFOF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFOF_4_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente")
          case   not(NOT EMPTY(.w_fCODFOR) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTER) OR .w_MPS_ODL='E') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOF))  and not(.w_MPS_ODL <> 'L')  and (.w_MPS_ODL = 'L')  and not(empty(.w_fCODFOR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFOR_4_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
          case   not(NOT EMPTY(.w_fCODFOF) AND (.w_MPS_ODL='L' AND NOT EMPTY(.w_MAGTEF) OR .w_MPS_ODL='E') and .w_fCODFOR<=.w_fCODFOF or empty(.w_fCODFOR))  and not(.w_MPS_ODL <> 'L')  and (.w_MPS_ODL = 'L')  and not(empty(.w_fCODFOF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFOF_4_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
          case   not(.w_fCODFASI<=.w_fCODFASF or empty(.w_fCODFASF))  and not(.w_MPS_ODL$'E-C-F-T')  and (g_PRFA='S' AND g_CICLILAV='S')  and not(empty(.w_fCODFASI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFASI_4_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_fCODFASI<=.w_fCODFASF or empty(.w_fCODFASI))  and not(.w_MPS_ODL$'E-C-F-T')  and (g_PRFA='S' AND g_CICLILAV='S')  and not(empty(.w_fCODFASF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.ofCODFASF_4_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ReadPar = this.w_ReadPar
    this.o_CODICE = this.w_CODICE
    this.o_CRIELA = this.w_CRIELA
    this.o_fCODCOM = this.w_fCODCOM
    this.o_fCODCOF = this.w_fCODCOF
    this.o_SELEZM = this.w_SELEZM
    return

enddefine

* --- Define pages as container
define class tgsco_kmpPag1 as StdContainer
  Width  = 786
  height = 510
  stdWidth  = 786
  stdheight = 510
  resizeXpos=262
  resizeYpos=67
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomODP as cp_zoombox with uid="ZAZTYPHGDA",left=-1, top=3, width=788,height=380,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSCO_KMP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 21863962


  add object DettODP as cp_zoombox with uid="MNXEYPRUED",left=-1, top=222, width=788,height=161,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSCODKMP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bRetriveAllRows=.f.,cMenuFile="FALSE",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    nPag=1;
    , HelpContextID = 21863962


  add object oBtn_1_18 as StdButton with uid="MJNXVZGNOX",left=735, top=464, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75891270;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_19 as cp_runprogram with uid="XBFDIZSRRU",left=4, top=525, width=170,height=19,;
    caption='GSCO_BMP',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('INTESTAZIONI')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 61218122

  add object oDESCSEL_1_26 as StdField with uid="OPXSFABDQV",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCSEL", cQueryName = "DESCSEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 244241462,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=248, Left=79, Top=390, InputMask=replicate('X',40)

  add object osTIPGES_1_28 as StdRadio with uid="YXTIDHPPCK",rtseq=51,rtrep=.f.,left=408, top=390, width=85,height=30, enabled=.f.;
    , ToolTipText = "Modalit� di gestione dell'articolo";
    , cFormVar="w_sTIPGES", ButtonCount=2, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.f., FntItalic=.f., FntUnderline=.f., FntStrikeThru=.f.

    proc osTIPGES_1_28.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Fabbisogno"
      this.Buttons(1).HelpContextID = 35961306
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Scorta"
      this.Buttons(2).HelpContextID = 35961306
      this.Buttons(2).Top=14
      this.SetAll("Width",83)
      this.SetAll("Height",16)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modalit� di gestione dell'articolo")
      StdRadio::init()
    endproc

  func osTIPGES_1_28.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func osTIPGES_1_28.GetRadio()
    this.Parent.oContained.w_sTIPGES = this.RadioValue()
    return .t.
  endfunc

  func osTIPGES_1_28.SetRadio()
    this.Parent.oContained.w_sTIPGES=trim(this.Parent.oContained.w_sTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_sTIPGES=='F',1,;
      iif(this.Parent.oContained.w_sTIPGES=='S',2,;
      0))
  endfunc

  add object osPUNRIO_1_29 as StdField with uid="URVYSDLOGA",rtseq=52,rtrep=.f.,;
    cFormVar = "w_sPUNRIO", cQueryName = "sPUNRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Punto di riordino",;
    HelpContextID = 225836506,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=583, Top=417, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func osPUNRIO_1_29.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES <>'S')
    endwith
  endfunc

  add object osSCOMIN_1_33 as StdField with uid="CTOYQNCUMP",rtseq=53,rtrep=.f.,;
    cFormVar = "w_sSCOMIN", cQueryName = "sSCOMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scorta sicurezza",;
    HelpContextID = 231086810,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=583, Top=390, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object osLOTMIN_1_35 as StdField with uid="SMUCUYFPAS",rtseq=54,rtrep=.f.,;
    cFormVar = "w_sLOTMIN", cQueryName = "sLOTMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� minima di riordino",;
    HelpContextID = 230711770,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=583, Top=417, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func osLOTMIN_1_35.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES <>'F')
    endwith
  endfunc

  add object osLOTRIO_1_37 as StdField with uid="FKDKMEPIXQ",rtseq=55,rtrep=.f.,;
    cFormVar = "w_sLOTRIO", cQueryName = "sLOTRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lotto di riordino",;
    HelpContextID = 225468890,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=80, Left=702, Top=390, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"


  add object oPPTIPDTF_1_38 as StdCombo with uid="CAWZZMJXMN",rtseq=56,rtrep=.f.,left=408,top=445,width=153,height=22;
    , ToolTipText = "Indica la modalit� di gestione del DTF (demand time fence) dando la possibilit� di gestire il parametro per articolo oppure generico per elaborazione";
    , HelpContextID = 43716548;
    , cFormVar="w_PPTIPDTF",RowSource=""+"Da anagrafica articolo,"+"Forza valore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPTIPDTF_1_38.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPPTIPDTF_1_38.GetRadio()
    this.Parent.oContained.w_PPTIPDTF = this.RadioValue()
    return .t.
  endfunc

  func oPPTIPDTF_1_38.SetRadio()
    this.Parent.oContained.w_PPTIPDTF=trim(this.Parent.oContained.w_PPTIPDTF)
    this.value = ;
      iif(this.Parent.oContained.w_PPTIPDTF=='A',1,;
      iif(this.Parent.oContained.w_PPTIPDTF=='F',2,;
      0))
  endfunc

  func oPPTIPDTF_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PARAM $ 'C-F-T' and g_MMPS='S')
    endwith
   endif
  endfunc

  add object oPP___DTF_1_39 as StdField with uid="ZNDOLPSFFQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_PP___DTF", cQueryName = "PP___DTF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 26501060,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=702, Top=445

  func oPP___DTF_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PARAM $ 'C-F-T' and g_MMPS='S')
    endwith
   endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="MZXVNEFMNO",left=4, top=464, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il piano di produzione";
    , HelpContextID = 22939370;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_42 as cp_runprogram with uid="VFLVJLASUD",left=188, top=543, width=231,height=19,;
    caption='GSCO_BMP',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('SELEZIONE')",;
    cEvent = "w_zoomodp selected",;
    nPag=1;
    , HelpContextID = 61218122


  add object oObj_1_44 as cp_runprogram with uid="ALRRGZHEMA",left=188, top=525, width=231,height=19,;
    caption='GSCO_BMP',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('XSCROLL')",;
    cEvent = "w_zoomodp scrolled",;
    nPag=1;
    , HelpContextID = 61218122


  add object oBtn_1_45 as StdButton with uid="JWEXAPSWKJ",left=102, top=464, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per nascondere il dettaglio manutenzione ordini";
    , HelpContextID = 158396735;
    , Caption='\<Nascondi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"HIDEDETT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not .w_ZOOMEspanso)
      endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="PWOLDYBHMJ",left=53, top=464, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il dettaglio manutenzione ordini";
    , HelpContextID = 241040225;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"SHOWDETT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_47 as StdButton with uid="XZCIGHXAQG",left=151, top=464, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare il piano degli ordini";
    , HelpContextID = 58072026;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      do GSCO_SMP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_49 as StdButton with uid="DFRIDKMVRP",left=200, top=464, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la verifica temporale";
    , HelpContextID = 65463735;
    , Caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"VERIF_TEMP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_56 as cp_runprogram with uid="CRAHBFWYOQ",left=188, top=565, width=231,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('SHOWDETTC')",;
    cEvent = "SHOWDETTC",;
    nPag=1;
    , HelpContextID = 21863962

  add object oStr_1_27 as StdString with uid="HWNKQYJWCX",Visible=.t., Left=4, Top=390,;
    Alignment=1, Width=72, Height=13,;
    Caption="Descrizione:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="ZRJFRHKCPC",Visible=.t., Left=467, Top=417,;
    Alignment=1, Width=114, Height=13,;
    Caption="Punto riordino:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES <> 'S')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="ZXYUKTFWOH",Visible=.t., Left=666, Top=390,;
    Alignment=1, Width=35, Height=13,;
    Caption="Lotto:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="ZPJJBJLKSD",Visible=.t., Left=331, Top=390,;
    Alignment=1, Width=75, Height=13,;
    Caption="Tipo gestione:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="MGJBFRMBLY",Visible=.t., Left=494, Top=390,;
    Alignment=1, Width=87, Height=13,;
    Caption="Scorta minima:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="ZJDQMFQQSP",Visible=.t., Left=467, Top=417,;
    Alignment=1, Width=114, Height=13,;
    Caption="Qta minima:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_sTIPGES <> 'F')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="LSCCHYBDBU",Visible=.t., Left=255, Top=448,;
    Alignment=1, Width=143, Height=18,;
    Caption="Tipo di gestione DTF:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="VXOPVPCUQD",Visible=.t., Left=633, Top=447,;
    Alignment=1, Width=68, Height=18,;
    Caption="Giorni DTF:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsco_kmpPag2 as StdContainer
  Width  = 786
  height = 510
  stdWidth  = 786
  stdheight = 510
  resizeXpos=363
  resizeYpos=238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODSEL_2_2 as StdField with uid="BODRMRSIHR",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CODSEL", cQueryName = "CODSEL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 188879322,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=160, Left=138, Top=13, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSEL"

  func oCODSEL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESSEL_2_3 as StdField with uid="TLILEPZQPV",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESSEL", cQueryName = "DESSEL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 188820426,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=301, Top=13, InputMask=replicate('X',40)

  add object oPSELR_2_4 as StdField with uid="OSPYXIRNCH",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PSELR", cQueryName = "PSELR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 159843062,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=138, Top=36, InputMask=replicate('X',4)

  add object oTipoPeriodo_2_5 as StdField with uid="KKNQCQLBLX",rtseq=34,rtrep=.f.,;
    cFormVar = "w_TipoPeriodo", cQueryName = "TipoPeriodo",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 23845745,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=267, Top=36, InputMask=replicate('X',20)

  add object oDIP_2_6 as StdField with uid="AUCHYPCWKM",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DIP", cQueryName = "DIP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 68921398,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=523, Top=36

  add object oDFP_2_7 as StdField with uid="UOJAWAEHVO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DFP", cQueryName = "DFP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 68920630,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=704, Top=36


  add object PeriodoODP as cp_zoombox with uid="AMOEMBNTTT",left=-1, top=63, width=789,height=325,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ODL_MAST",cZoomFile="GSCO2KMP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "ListaODP",;
    nPag=2;
    , HelpContextID = 21863962

  add object oODPSEL_2_16 as StdField with uid="JSUCSKQIMX",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ODPSEL", cQueryName = "ODPSEL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Ordine selezionato",;
    HelpContextID = 188832794,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=112, Top=391, InputMask=replicate('X',15)

  add object osCODART_2_17 as StdField with uid="WTHANYMBYM",rtseq=38,rtrep=.f.,;
    cFormVar = "w_sCODART", cQueryName = "sCODART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 175084838,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=112, Top=414, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_sCODART"

  func osCODART_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object osDESART_2_18 as StdField with uid="WINZLDWYHK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_sDESART", cQueryName = "sDESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 176027174,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=266, Top=414, InputMask=replicate('X',40)

  add object oDInEff_2_20 as StdField with uid="ALKJUBEFIO",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DInEff", cQueryName = "DInEff",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 255686602,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=707, Top=391

  add object oDlancio_2_22 as StdField with uid="GXRGUPEFVW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_Dlancio", cQueryName = "Dlancio",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 205857994,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=503, Top=391

  add object oFORSEL_2_23 as StdField with uid="TIACWRZKSJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Fornitore C/Lavoro",;
    HelpContextID = 188821930,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=112, Top=437, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_2_23.mHide()
    with this.Parent.oContained
      return (.w_PARAM='O')
    endwith
  endfunc

  func oFORSEL_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESFOR_2_24 as StdField with uid="NFRFCFVMEJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78523338,;
   bGlobalFont=.t.,;
    Height=21, Width=315, Left=240, Top=437, InputMask=replicate('X',40)

  func oDESFOR_2_24.mHide()
    with this.Parent.oContained
      return (.w_PARAM='O')
    endwith
  endfunc


  add object oObj_2_26 as cp_runprogram with uid="IYACFXDFAY",left=15, top=539, width=271,height=19,;
    caption='GSCO_BMP',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('VISUALIZZA_ODP')",;
    cEvent = "w_periodoodp selected",;
    nPag=2;
    , HelpContextID = 61218122


  add object oObj_2_27 as cp_runprogram with uid="RYQERVKBYT",left=15, top=521, width=271,height=19,;
    caption='GSCO_BMP',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('MENU_LISTAODP')",;
    cEvent = "w_periodoodp MouseRightClick",;
    nPag=2;
    , HelpContextID = 61218122


  add object oObj_2_28 as cp_runprogram with uid="XTWVVRKIEH",left=15, top=557, width=271,height=19,;
    caption='GSCO_BMP',;
   bGlobalFont=.t.,;
    prg="GSCO_BMP('SELEZIONE')",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 61218122


  add object oBtn_2_29 as StdButton with uid="HZFZLLGEOC",left=477, top=464, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per caricare un nuovo ordine";
    , HelpContextID = 71901146;
    , Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_29.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"CARICA_ODP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_30 as StdButton with uid="SOFRLFQUUN",left=525, top=464, width=48,height=45,;
    CpPicture="bmp\modifica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per modificare l'ordine selezionato";
    , HelpContextID = 177660246;
    , Caption='\<Varia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_30.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"MODIFICA_ODP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_31 as StdButton with uid="VTFRAXPECG",left=573, top=464, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per cancellare l'ordine selezionato";
    , HelpContextID = 115713210;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_31.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"ELIMINA_ODP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_32 as StdButton with uid="LCIZCVPRLY",left=735, top=464, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75891270;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_9 as StdString with uid="AWPZTOYEGM",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=132, Height=15,;
    Caption="Codice selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="TVNVAXXJIE",Visible=.t., Left=2, Top=36,;
    Alignment=1, Width=132, Height=15,;
    Caption="Periodo selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="LQQSGCFBSU",Visible=.t., Left=190, Top=36,;
    Alignment=1, Width=73, Height=15,;
    Caption="Tipo periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="CSUDQKLXHQ",Visible=.t., Left=382, Top=36,;
    Alignment=1, Width=137, Height=15,;
    Caption="Data inizio periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="QVXSTVYWUB",Visible=.t., Left=611, Top=36,;
    Alignment=1, Width=89, Height=15,;
    Caption="Fine periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="FGNPWTJPVF",Visible=.t., Left=22, Top=391,;
    Alignment=1, Width=88, Height=15,;
    Caption="Ordine sel.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="QQKOBPSKXO",Visible=.t., Left=22, Top=415,;
    Alignment=1, Width=88, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="XFHOOHCLGJ",Visible=.t., Left=585, Top=391,;
    Alignment=1, Width=118, Height=15,;
    Caption="Data inizio effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="VURLPIQDFX",Visible=.t., Left=410, Top=391,;
    Alignment=1, Width=89, Height=15,;
    Caption="Data lancio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="LNMZUUAWWX",Visible=.t., Left=22, Top=437,;
    Alignment=1, Width=88, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (.w_PARAM='O')
    endwith
  endfunc
enddefine
define class tgsco_kmpPag3 as StdContainer
  Width  = 786
  height = 510
  stdWidth  = 786
  stdheight = 510
  resizeYpos=270
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object MIST as cp_calclbl with uid="DMQIBOGZUI",left=255, top=224, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962


  add object oObj_3_23 as cp_zoombox with uid="ETYLMDKPKI",left=522, top=61, width=243,height=444,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MPS_TPER",cZoomFile="GSCO_KMP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Periodi",;
    nPag=3;
    , HelpContextID = 21863962


  add object oBtn_3_24 as StdButton with uid="SZYOXXWAPH",left=717, top=9, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare il dettaglio dei periodi elaborati";
    , HelpContextID = 259450102;
    , Caption='\<Periodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_24.Click()
      with this.Parent.oContained
        .notifyevent("Periodi")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDataEsp_3_27 as StdField with uid="AKEXXZBPCI",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DataEsp", cQueryName = "DataEsp",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 70320074,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=162, Top=330

  add object oOraEsp_3_28 as StdField with uid="WSFQZILJKD",rtseq=62,rtrep=.f.,;
    cFormVar = "w_OraEsp", cQueryName = "OraEsp",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 74325530,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=289, Top=330, InputMask=replicate('X',8)

  add object oDESUTE_3_30 as StdField with uid="PNLFLRNRMA",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 21965770,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=205, Top=362, InputMask=replicate('X',20)

  add object oOpeEsp_3_35 as StdField with uid="HZRFNELLLQ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_OpeEsp", cQueryName = "OpeEsp",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 74309658,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=162, Top=362, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OpeEsp"

  func oOpeEsp_3_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object LANC as cp_calclbl with uid="KKCXNPFOSW",left=255, top=194, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962


  add object SUGGM as cp_calclbl with uid="TLHFBDWUEE",left=255, top=134, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962


  add object SUGGE as cp_calclbl with uid="WHVDTETLZN",left=255, top=44, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962


  add object PIAN as cp_calclbl with uid="KJGKLJBZGJ",left=255, top=164, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962


  add object CONF as cp_calclbl with uid="HLVWJHTWKZ",left=255, top=74, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962


  add object DPIA as cp_calclbl with uid="NESDLGCIKE",left=255, top=104, width=106,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=3;
    , HelpContextID = 21863962

  add object oStr_3_16 as StdString with uid="JFSHBDTAYD",Visible=.t., Left=39, Top=224,;
    Alignment=1, Width=209, Height=15,;
    Caption="Composizione mista:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="SWLKCTBDXQ",Visible=.t., Left=6, Top=17,;
    Alignment=0, Width=215, Height=15,;
    Caption="Legenda colori"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="QSQNCDWNWY",Visible=.t., Left=39, Top=134,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODL suggeriti:"  ;
  , bGlobalFont=.t.

  func oStr_3_19.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'O')
    endwith
  endfunc

  add object oStr_3_20 as StdString with uid="PUDCZDVZTI",Visible=.t., Left=39, Top=134,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo OCL suggeriti:"  ;
  , bGlobalFont=.t.

  func oStr_3_20.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'L')
    endwith
  endfunc

  add object oStr_3_21 as StdString with uid="AOXEABMCTQ",Visible=.t., Left=39, Top=164,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODL pianificati:"  ;
  , bGlobalFont=.t.

  func oStr_3_21.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'O')
    endwith
  endfunc

  add object oStr_3_22 as StdString with uid="ZWWRPAYMVP",Visible=.t., Left=39, Top=164,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo OCL Da ordinare:"  ;
  , bGlobalFont=.t.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'L')
    endwith
  endfunc

  add object oStr_3_26 as StdString with uid="OJEBVAAFOC",Visible=.t., Left=518, Top=40,;
    Alignment=0, Width=157, Height=15,;
    Caption="Tabella periodi"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="WRAXRDGYOC",Visible=.t., Left=8, Top=330,;
    Alignment=1, Width=148, Height=15,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="IBOINDGDHN",Visible=.t., Left=242, Top=330,;
    Alignment=1, Width=43, Height=15,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_3_32 as StdString with uid="STVTUCMKYX",Visible=.t., Left=8, Top=362,;
    Alignment=1, Width=148, Height=15,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_33 as StdString with uid="KCKZUBZABK",Visible=.t., Left=6, Top=298,;
    Alignment=0, Width=215, Height=18,;
    Caption="Elaborazione OCL"  ;
  , bGlobalFont=.t.

  func oStr_3_33.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'L')
    endwith
  endfunc

  add object oStr_3_36 as StdString with uid="OFKWRLJITV",Visible=.t., Left=6, Top=298,;
    Alignment=0, Width=215, Height=18,;
    Caption="Elaborazione ODL"  ;
  , bGlobalFont=.t.

  func oStr_3_36.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'O')
    endwith
  endfunc

  add object oStr_3_37 as StdString with uid="WJLRILUDEJ",Visible=.t., Left=39, Top=134,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODA suggeriti:"  ;
  , bGlobalFont=.t.

  func oStr_3_37.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'E')
    endwith
  endfunc

  add object oStr_3_38 as StdString with uid="QZXZAYDEQR",Visible=.t., Left=39, Top=164,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODA Da ordinare:"  ;
  , bGlobalFont=.t.

  func oStr_3_38.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'E')
    endwith
  endfunc

  add object oStr_3_40 as StdString with uid="IMEKUHACFC",Visible=.t., Left=39, Top=194,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODL Lanciati:"  ;
  , bGlobalFont=.t.

  func oStr_3_40.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'O')
    endwith
  endfunc

  add object oStr_3_41 as StdString with uid="DPPYZOLUWF",Visible=.t., Left=39, Top=194,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo OCL Ordinati:"  ;
  , bGlobalFont=.t.

  func oStr_3_41.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'L')
    endwith
  endfunc

  add object oStr_3_42 as StdString with uid="WNWATTMGON",Visible=.t., Left=39, Top=194,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODA Ordinati:"  ;
  , bGlobalFont=.t.

  func oStr_3_42.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'E')
    endwith
  endfunc

  add object oStr_3_48 as StdString with uid="OCKNROXZRG",Visible=.t., Left=39, Top=44,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODF suggeriti:"  ;
  , bGlobalFont=.t.

  func oStr_3_48.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'T')
    endwith
  endfunc

  add object oStr_3_49 as StdString with uid="JNLCSNEKBX",Visible=.t., Left=39, Top=44,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODR suggeriti:"  ;
  , bGlobalFont=.t.

  func oStr_3_49.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'F')
    endwith
  endfunc

  add object oStr_3_50 as StdString with uid="LHLYHVCWOW",Visible=.t., Left=39, Top=74,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODF confermati:"  ;
  , bGlobalFont=.t.

  func oStr_3_50.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'T')
    endwith
  endfunc

  add object oStr_3_51 as StdString with uid="MKIRMDHMMY",Visible=.t., Left=39, Top=74,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODR confermati:"  ;
  , bGlobalFont=.t.

  func oStr_3_51.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'F')
    endwith
  endfunc

  add object oStr_3_52 as StdString with uid="XLWXNRDKDB",Visible=.t., Left=39, Top=44,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODP suggeriti:"  ;
  , bGlobalFont=.t.

  func oStr_3_52.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  add object oStr_3_53 as StdString with uid="UETUQLEPAN",Visible=.t., Left=39, Top=74,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODP confermati:"  ;
  , bGlobalFont=.t.

  func oStr_3_53.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  add object oStr_3_54 as StdString with uid="XTLVJDVUSI",Visible=.t., Left=39, Top=104,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODP da pianificare:"  ;
  , bGlobalFont=.t.

  func oStr_3_54.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  add object oStr_3_55 as StdString with uid="RWDJDVFBTL",Visible=.t., Left=39, Top=104,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODR da pianificare:"  ;
  , bGlobalFont=.t.

  func oStr_3_55.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'F')
    endwith
  endfunc

  add object oStr_3_56 as StdString with uid="EFXSMHEDDC",Visible=.t., Left=39, Top=104,;
    Alignment=1, Width=209, Height=18,;
    Caption="Solo ODF da pianificare:"  ;
  , bGlobalFont=.t.

  func oStr_3_56.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'T')
    endwith
  endfunc

  add object oStr_3_57 as StdString with uid="YCROAUYAQO",Visible=.t., Left=39, Top=164,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODP pianificati:"  ;
  , bGlobalFont=.t.

  func oStr_3_57.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  add object oStr_3_58 as StdString with uid="SERFQBXPDE",Visible=.t., Left=39, Top=194,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODP lanciati:"  ;
  , bGlobalFont=.t.

  func oStr_3_58.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  add object oStr_3_59 as StdString with uid="SILHFTOYPT",Visible=.t., Left=39, Top=164,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODR pianificati:"  ;
  , bGlobalFont=.t.

  func oStr_3_59.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'F')
    endwith
  endfunc

  add object oStr_3_60 as StdString with uid="UPIWBGTMAA",Visible=.t., Left=39, Top=194,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODR lanciati:"  ;
  , bGlobalFont=.t.

  func oStr_3_60.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'F')
    endwith
  endfunc

  add object oStr_3_61 as StdString with uid="GGGMEAIHKZ",Visible=.t., Left=39, Top=164,;
    Alignment=1, Width=209, Height=15,;
    Caption="Solo ODF pianificati:"  ;
  , bGlobalFont=.t.

  func oStr_3_61.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'T')
    endwith
  endfunc

  add object oBox_3_17 as StdBox with uid="IXGDICOKNH",left=6, top=34, width=446,height=2

  add object oBox_3_25 as StdBox with uid="IXQRTNIXHC",left=518, top=57, width=252,height=2

  add object oBox_3_34 as StdBox with uid="ZXZEXACBJI",left=5, top=317, width=446,height=2
enddefine
define class tgsco_kmpPag4 as StdContainer
  Width  = 786
  height = 510
  stdWidth  = 786
  stdheight = 510
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ofPROFIN_4_3 as StdCheck with uid="IHWROQAYED",rtseq=74,rtrep=.f.,left=59, top=6, caption="Prodotti finiti",;
    ToolTipText = "Se attivo, considera i prodotti finiti",;
    HelpContextID = 238366378,;
    cFormVar="w_fPROFIN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofPROFIN_4_3.RadioValue()
    return(iif(this.value =1,'PF',;
    'XX'))
  endfunc
  func ofPROFIN_4_3.GetRadio()
    this.Parent.oContained.w_fPROFIN = this.RadioValue()
    return .t.
  endfunc

  func ofPROFIN_4_3.SetRadio()
    this.Parent.oContained.w_fPROFIN=trim(this.Parent.oContained.w_fPROFIN)
    this.value = ;
      iif(this.Parent.oContained.w_fPROFIN=='PF',1,;
      0)
  endfunc

  add object ofSEMLAV_4_4 as StdCheck with uid="KTWFDIHQED",rtseq=75,rtrep=.f.,left=212, top=6, caption="Semilavorati",;
    ToolTipText = "Se attivo, considera i semilavorati",;
    HelpContextID = 170394710,;
    cFormVar="w_fSEMLAV", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofSEMLAV_4_4.RadioValue()
    return(iif(this.value =1,'SE',;
    'XX'))
  endfunc
  func ofSEMLAV_4_4.GetRadio()
    this.Parent.oContained.w_fSEMLAV = this.RadioValue()
    return .t.
  endfunc

  func ofSEMLAV_4_4.SetRadio()
    this.Parent.oContained.w_fSEMLAV=trim(this.Parent.oContained.w_fSEMLAV)
    this.value = ;
      iif(this.Parent.oContained.w_fSEMLAV=='SE',1,;
      0)
  endfunc

  add object ofMATPRI_4_5 as StdCheck with uid="SHIEYEJPCW",rtseq=76,rtrep=.f.,left=365, top=6, caption="Materie prime",;
    ToolTipText = "Se attivo, considera le materie prime",;
    HelpContextID = 191807062,;
    cFormVar="w_fMATPRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofMATPRI_4_5.RadioValue()
    return(iif(this.value =1,'MP',;
    'XX'))
  endfunc
  func ofMATPRI_4_5.GetRadio()
    this.Parent.oContained.w_fMATPRI = this.RadioValue()
    return .t.
  endfunc

  func ofMATPRI_4_5.SetRadio()
    this.Parent.oContained.w_fMATPRI=trim(this.Parent.oContained.w_fMATPRI)
    this.value = ;
      iif(this.Parent.oContained.w_fMATPRI=='MP',1,;
      0)
  endfunc

  add object ofMATFAS_4_6 as StdCheck with uid="LYJGJELUFR",rtseq=77,rtrep=.f.,left=518, top=6, caption="Visualizza ordini di fase",;
    ToolTipText = "Se attivo, considera gli ordini di fase",;
    HelpContextID = 103891370,;
    cFormVar="w_fMATFAS", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func ofMATFAS_4_6.RadioValue()
    return(iif(this.value =1,'FS',;
    'XX'))
  endfunc
  func ofMATFAS_4_6.GetRadio()
    this.Parent.oContained.w_fMATFAS = this.RadioValue()
    return .t.
  endfunc

  func ofMATFAS_4_6.SetRadio()
    this.Parent.oContained.w_fMATFAS=trim(this.Parent.oContained.w_fMATFAS)
    this.value = ;
      iif(this.Parent.oContained.w_fMATFAS=='FS',1,;
      0)
  endfunc

  func ofMATFAS_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' AND g_CICLILAV='S')
    endwith
   endif
  endfunc

  func ofMATFAS_4_6.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL$'E-C-F-T')
    endwith
  endfunc


  add object oBtn_4_7 as StdButton with uid="THRFVLAJCD",left=734, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per visualizzare il piano principale di produzione";
    , HelpContextID = 108915690;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_7.Click()
      with this.Parent.oContained
        GSCO_BMP(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ofDISINI_4_8 as StdField with uid="KHKMPFSALM",rtseq=78,rtrep=.f.,;
    cFormVar = "w_fDISINI", cQueryName = "fDISINI",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 117323094,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=126, Top=29, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_fDISINI"

  func ofDISINI_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc ofDISINI_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofDISINI_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'ofDISINI_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici articoli",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc ofDISINI_4_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_fDISINI
     i_obj.ecpSave()
  endproc

  add object ofDISFIN_4_9 as StdField with uid="FVVRYKENZQ",rtseq=79,rtrep=.f.,;
    cFormVar = "w_fDISFIN", cQueryName = "fDISFIN",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 238144170,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=126, Top=53, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_fDISFIN"

  proc ofDISFIN_4_9.mDefault
    with this.Parent.oContained
      if empty(.w_fDISFIN)
        .w_fDISFIN = .w_fDISINI
      endif
    endwith
  endproc

  func ofDISFIN_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc ofDISFIN_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofDISFIN_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'ofDISFIN_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici articoli",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc ofDISFIN_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_fDISFIN
     i_obj.ecpSave()
  endproc

  add object ofCODFAM_4_10 as StdField with uid="XGZLMVEIRB",rtseq=80,rtrep=.f.,;
    cFormVar = "w_fCODFAM", cQueryName = "fCODFAM",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 104885162,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_fCODFAM"

  func ofCODFAM_4_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_10('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFAM_4_10.ecpDrop(oSource)
    this.Parent.oContained.link_4_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFAM_4_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'ofCODFAM_4_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object ofCODFAF_4_11 as StdField with uid="XFEHNCKXQA",rtseq=81,rtrep=.f.,;
    cFormVar = "w_fCODFAF", cQueryName = "fCODFAF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 163550294,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=101, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_fCODFAF"

  proc ofCODFAF_4_11.mDefault
    with this.Parent.oContained
      if empty(.w_fCODFAF)
        .w_fCODFAF = .w_fCODFAM
      endif
    endwith
  endproc

  func ofCODFAF_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFAF_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFAF_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'ofCODFAF_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc


  add object oCRIELA_4_12 as StdCombo with uid="VSDAZQQBWZ",rtseq=82,rtrep=.f.,left=624,top=77,width=157,height=21;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 98549466;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCRIELA_4_12.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_4_12.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_4_12.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc

  add object ofGRUMER_4_13 as StdField with uid="YCLNJRBZDM",rtseq=83,rtrep=.f.,;
    cFormVar = "w_fGRUMER", cQueryName = "fGRUMER",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di inizio selezione",;
    HelpContextID = 29308842,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=126, Top=125, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_fGRUMER"

  func ofGRUMER_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc ofGRUMER_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofGRUMER_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ofGRUMER_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object ofGRUMEF_4_14 as StdField with uid="QRNYKYSQLT",rtseq=84,rtrep=.f.,;
    cFormVar = "w_fGRUMEF", cQueryName = "fGRUMEF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di fine selezione",;
    HelpContextID = 239126614,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=126, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_fGRUMEF"

  proc ofGRUMEF_4_14.mDefault
    with this.Parent.oContained
      if empty(.w_fGRUMEF)
        .w_fGRUMEF = .w_fGRUMER
      endif
    endwith
  endproc

  func ofGRUMEF_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_14('Part',this)
    endwith
    return bRes
  endfunc

  proc ofGRUMEF_4_14.ecpDrop(oSource)
    this.Parent.oContained.link_4_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofGRUMEF_4_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ofGRUMEF_4_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object ofCATOMO_4_15 as StdField with uid="JUJNKWFJYM",rtseq=85,rtrep=.f.,;
    cFormVar = "w_fCATOMO", cQueryName = "fCATOMO",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di inizio selezione",;
    HelpContextID = 161565610,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=126, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_fCATOMO"

  func ofCATOMO_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCATOMO_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCATOMO_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'ofCATOMO_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object ofCATOMF_4_16 as StdField with uid="HXLMXBDRQP",rtseq=86,rtrep=.f.,;
    cFormVar = "w_fCATOMF", cQueryName = "fCATOMF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di fine selezione",;
    HelpContextID = 106869846,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=126, Top=197, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_fCATOMF"

  proc ofCATOMF_4_16.mDefault
    with this.Parent.oContained
      if empty(.w_fCATOMF)
        .w_fCATOMF = .w_fCATOMO
      endif
    endwith
  endproc

  func ofCATOMF_4_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_16('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCATOMF_4_16.ecpDrop(oSource)
    this.Parent.oContained.link_4_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCATOMF_4_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'ofCATOMF_4_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object ofCODMAR_4_17 as StdField with uid="NLIAPNFUHE",rtseq=87,rtrep=.f.,;
    cFormVar = "w_fCODMAR", cQueryName = "fCODMAR",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di inizio selezione",;
    HelpContextID = 97545130,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_fCODMAR"

  func ofCODMAR_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODMAR_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODMAR_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'ofCODMAR_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object ofCODMAF_4_18 as StdField with uid="VPVQKOFTGH",rtseq=88,rtrep=.f.,;
    cFormVar = "w_fCODMAF", cQueryName = "fCODMAF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di fine selezione",;
    HelpContextID = 170890326,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=245, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_fCODMAF"

  proc ofCODMAF_4_18.mDefault
    with this.Parent.oContained
      if empty(.w_fCODMAF)
        .w_fCODMAF = .w_fCODMAR
      endif
    endwith
  endproc

  func ofCODMAF_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODMAF_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODMAF_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'ofCODMAF_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object ofMAGINI_4_19 as StdField with uid="SLNUVWMBLE",rtseq=89,rtrep=.f.,;
    cFormVar = "w_fMAGINI", cQueryName = "fMAGINI",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 116506198,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=269, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_fMAGINI"

  func ofMAGINI_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc ofMAGINI_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofMAGINI_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'ofMAGINI_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object ofMAGFIN_4_20 as StdField with uid="ASPAKVFGCX",rtseq=90,rtrep=.f.,;
    cFormVar = "w_fMAGFIN", cQueryName = "fMAGFIN",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 238961066,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=293, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_fMAGFIN"

  proc ofMAGFIN_4_20.mDefault
    with this.Parent.oContained
      if empty(.w_fMAGFIN)
        .w_fMAGFIN = .w_fMAGINI
      endif
    endwith
  endproc

  func ofMAGFIN_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
    endwith
    return bRes
  endfunc

  proc ofMAGFIN_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofMAGFIN_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'ofMAGFIN_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object ofCODCOM_4_21 as StdField with uid="PVVMICHNRA",rtseq=91,rtrep=.f.,;
    cFormVar = "w_fCODCOM", cQueryName = "fCODCOM",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 141585322,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=126, Top=317, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_fCODCOM"

  func ofCODCOM_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" OR g_PERCAN="S")
    endwith
   endif
  endfunc

  func ofCODCOM_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_21('Part',this)
      if .not. empty(.w_fCODATT)
        bRes2=.link_4_23('Full')
      endif
      if .not. empty(.w_fCODATF)
        bRes2=.link_4_24('Full')
      endif
    endwith
    return bRes
  endfunc

  proc ofCODCOM_4_21.ecpDrop(oSource)
    this.Parent.oContained.link_4_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODCOM_4_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'ofCODCOM_4_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object ofCODCOF_4_22 as StdField with uid="BJEAZWHKXG",rtseq=92,rtrep=.f.,;
    cFormVar = "w_fCODCOF", cQueryName = "fCODCOF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 126850134,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=126, Top=341, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_fCODCOF"

  proc ofCODCOF_4_22.mDefault
    with this.Parent.oContained
      if empty(.w_fCODCOF)
        .w_fCODCOF = .w_fCODCOM
      endif
    endwith
  endproc

  func ofCODCOF_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" OR g_PERCAN="S")
    endwith
   endif
  endfunc

  func ofCODCOF_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_22('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODCOF_4_22.ecpDrop(oSource)
    this.Parent.oContained.link_4_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODCOF_4_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'ofCODCOF_4_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object ofCODATT_4_23 as StdField with uid="ZIKVMSSXJZ",rtseq=93,rtrep=.f.,;
    cFormVar = "w_fCODATT", cQueryName = "fCODATT",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita di inizio selezione",;
    HelpContextID = 208639062,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=126, Top=365, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_fCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_fCODATT"

  func ofCODATT_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)
    endwith
   endif
  endfunc

  func ofCODATT_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODATT_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODATT_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'ofCODATT_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object ofCODATF_4_24 as StdField with uid="LNKODMPNRF",rtseq=94,rtrep=.f.,;
    cFormVar = "w_fCODATF", cQueryName = "fCODATF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita di fine selezione",;
    HelpContextID = 208639062,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=126, Top=389, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_fCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_fCODATF"

  func ofCODATF_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(nvl(.w_fCODCOM,"")) and !empty(nvl(.w_fCODCOF,"")) and .w_fCODCOM=.w_fCODCOF)
    endwith
   endif
  endfunc

  func ofCODATF_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODATF_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODATF_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_fCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'ofCODATF_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object ofCODFOR_4_25 as StdField with uid="FRYOZPVPMZ",rtseq=95,rtrep=.f.,;
    cFormVar = "w_fCODFOR", cQueryName = "fCODFOR",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente",;
    ToolTipText = "Codice fornitore conto lavoro di inizio selezione",;
    HelpContextID = 138439594,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=126, Top=413, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_fCODFOR"

  func ofCODFOR_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MPS_ODL $ 'E-T')
    endwith
   endif
  endfunc

  func ofCODFOR_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_25('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFOR_4_25.ecpDrop(oSource)
    this.Parent.oContained.link_4_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFOR_4_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ofCODFOR_4_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this.parent.oContained
  endproc

  add object ofCODFOF_4_26 as StdField with uid="HNEKKHWJXJ",rtseq=96,rtrep=.f.,;
    cFormVar = "w_fCODFOF", cQueryName = "fCODFOF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente",;
    ToolTipText = "Codice fornitore conto lavoro di fine selezione",;
    HelpContextID = 129995862,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=126, Top=437, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_fCODFOF"

  proc ofCODFOF_4_26.mDefault
    with this.Parent.oContained
      if empty(.w_fCODFOF)
        .w_fCODFOF = .w_fCODFOR
      endif
    endwith
  endproc

  func ofCODFOF_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MPS_ODL $ 'E-T')
    endwith
   endif
  endfunc

  func ofCODFOF_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_26('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFOF_4_26.ecpDrop(oSource)
    this.Parent.oContained.link_4_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFOF_4_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ofCODFOF_4_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gsco1kcs.CONTI_VZM',this.parent.oContained
  endproc

  add object ofCODFOR_4_27 as StdField with uid="ONJNVXIODO",rtseq=97,rtrep=.f.,;
    cFormVar = "w_fCODFOR", cQueryName = "fCODFOR",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o magazzino terzista non definito",;
    ToolTipText = "Codice fornitore conto lavoro di inizio selezione",;
    HelpContextID = 138439594,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=126, Top=413, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_fCODFOR"

  func ofCODFOR_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MPS_ODL = 'L')
    endwith
   endif
  endfunc

  func ofCODFOR_4_27.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL <> 'L')
    endwith
  endfunc

  func ofCODFOR_4_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_27('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFOR_4_27.ecpDrop(oSource)
    this.Parent.oContained.link_4_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFOR_4_27.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ofCODFOR_4_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gscozscs.CONTI_VZM',this.parent.oContained
  endproc

  add object ofCODFOF_4_28 as StdField with uid="AHFBCNTOCV",rtseq=98,rtrep=.f.,;
    cFormVar = "w_fCODFOF", cQueryName = "fCODFOF",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o magazzino terzista non definito",;
    ToolTipText = "Codice fornitore conto lavoro di fine selezione",;
    HelpContextID = 129995862,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=126, Top=437, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_fCODFOF"

  func ofCODFOF_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MPS_ODL = 'L')
    endwith
   endif
  endfunc

  func ofCODFOF_4_28.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL <> 'L')
    endwith
  endfunc

  func ofCODFOF_4_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_28('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFOF_4_28.ecpDrop(oSource)
    this.Parent.oContained.link_4_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFOF_4_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ofCODFOF_4_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gscozscs.CONTI_VZM',this.parent.oContained
  endproc

  add object ofCODFASI_4_29 as StdField with uid="IDUNPUJVAF",rtseq=99,rtrep=.f.,;
    cFormVar = "w_fCODFASI", cQueryName = "fCODFASI",;
    bObbl = .f. , nPag = 4, value=space(66), bMultilanguage =  .f.,;
    ToolTipText = "Codice di fase di inizio selezione",;
    HelpContextID = 104885089,;
   bGlobalFont=.t.,;
    Height=21, Width=475, Left=126, Top=461, InputMask=replicate('X',66), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODFAS", oKey_1_2="this.w_fCODFASI"

  func ofCODFASI_4_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' AND g_CICLILAV='S')
    endwith
   endif
  endfunc

  func ofCODFASI_4_29.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL$'E-C-F-T')
    endwith
  endfunc

  func ofCODFASI_4_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_29('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFASI_4_29.ecpDrop(oSource)
    this.Parent.oContained.link_4_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFASI_4_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODFAS',cp_AbsName(this.parent,'ofCODFASI_4_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object ofCODFASF_4_30 as StdField with uid="PXXEHJTQRD",rtseq=100,rtrep=.f.,;
    cFormVar = "w_fCODFASF", cQueryName = "fCODFASF",;
    bObbl = .f. , nPag = 4, value=space(66), bMultilanguage =  .f.,;
    ToolTipText = "Codice di fase di fine selezione",;
    HelpContextID = 104885092,;
   bGlobalFont=.t.,;
    Height=21, Width=475, Left=126, Top=485, InputMask=replicate('X',66), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODFAS", oKey_1_2="this.w_fCODFASF"

  proc ofCODFASF_4_30.mDefault
    with this.Parent.oContained
      if empty(.w_fCODFASF)
        .w_fCODFASF = .w_fCODFASI
      endif
    endwith
  endproc

  func ofCODFASF_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' AND g_CICLILAV='S')
    endwith
   endif
  endfunc

  func ofCODFASF_4_30.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL$'E-C-F-T')
    endwith
  endfunc

  func ofCODFASF_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_30('Part',this)
    endwith
    return bRes
  endfunc

  proc ofCODFASF_4_30.ecpDrop(oSource)
    this.Parent.oContained.link_4_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofCODFASF_4_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODFAS',cp_AbsName(this.parent,'ofCODFASF_4_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di fase",'GSCO_ZFL.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESINI_4_31 as StdField with uid="KNHKVOWXPR",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 230370250,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=280, Top=29, InputMask=replicate('X',40)


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=434, top=120, width=352,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=4;
    , HelpContextID = 21863962

  add object oSELEZM_4_39 as StdRadio with uid="LTQWYYFJNE",rtseq=102,rtrep=.f.,left=546, top=373, width=239,height=20;
    , cFormVar="w_SELEZM", ButtonCount=2, bObbl=.f., nPag=4;
  , bGlobalFont=.t.

    proc oSELEZM_4_39.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 150969050
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 150969050
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZM_4_39.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZM_4_39.GetRadio()
    this.Parent.oContained.w_SELEZM = this.RadioValue()
    return .t.
  endfunc

  func oSELEZM_4_39.SetRadio()
    this.Parent.oContained.w_SELEZM=trim(this.Parent.oContained.w_SELEZM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZM=="S",1,;
      iif(this.Parent.oContained.w_SELEZM=="D",2,;
      0))
  endfunc

  func oSELEZM_4_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIELA $ 'G-M')
    endwith
   endif
  endfunc


  add object ofTIPGES_4_40 as StdCombo with uid="QFVODWEYFZ",rtseq=103,rtrep=.f.,left=672,top=412,width=109,height=21;
    , ToolTipText = "Tipo gestione articolo";
    , HelpContextID = 35961514;
    , cFormVar="w_fTIPGES",RowSource=""+"Fabbisogno,"+"Scorta,"+"Tutti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func ofTIPGES_4_40.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func ofTIPGES_4_40.GetRadio()
    this.Parent.oContained.w_fTIPGES = this.RadioValue()
    return .t.
  endfunc

  func ofTIPGES_4_40.SetRadio()
    this.Parent.oContained.w_fTIPGES=trim(this.Parent.oContained.w_fTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_fTIPGES=='F',1,;
      iif(this.Parent.oContained.w_fTIPGES=='S',2,;
      iif(this.Parent.oContained.w_fTIPGES=='X',3,;
      0)))
  endfunc

  add object oDESFIN_4_41 as StdField with uid="DEJRYNJSQL",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151923658,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=280, Top=53, InputMask=replicate('X',40)

  add object oDESGRU_4_42 as StdField with uid="OMNXRCVTQU",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 24980426,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=178, Top=125, InputMask=replicate('X',35)

  add object oDESCAT_4_43 as StdField with uid="TWDGWFYFEP",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59845578,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=178, Top=173, InputMask=replicate('X',35)

  add object oDESCAN_4_44 as StdField with uid="PJHERXXTYJ",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 160508874,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=237, Top=317, InputMask=replicate('X',30)

  add object oDESATT_4_45 as StdField with uid="JRUPUPZCWV",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 40053706,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=237, Top=365, InputMask=replicate('X',30)

  add object oDESFORN_4_46 as StdField with uid="QITDWTVWPI",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DESFORN", cQueryName = "DESFORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78523338,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=280, Top=413, InputMask=replicate('X',40)

  add object oDESFAMA_4_47 as StdField with uid="AOYBYAMKXI",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESFAMA", cQueryName = "DESFAMA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177089482,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=189, Top=77, InputMask=replicate('X',35)

  add object oDESMAR_4_48 as StdField with uid="MXNLJGFEDS",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92744650,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=189, Top=221, InputMask=replicate('X',35)

  add object oDESGRF_4_49 as StdField with uid="AJBTWZFPAJ",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DESGRF", cQueryName = "DESGRF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 8203210,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=178, Top=149, InputMask=replicate('X',35)

  add object oDESCAF_4_50 as StdField with uid="AVFSGGPYLJ",rtseq=113,rtrep=.f.,;
    cFormVar = "w_DESCAF", cQueryName = "DESCAF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 26291146,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=178, Top=197, InputMask=replicate('X',35)

  add object oDESFAMF_4_51 as StdField with uid="ETEOEUZMNA",rtseq=114,rtrep=.f.,;
    cFormVar = "w_DESFAMF", cQueryName = "DESFAMF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 91345974,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=189, Top=101, InputMask=replicate('X',35)

  add object oDESMAF_4_52 as StdField with uid="EAHDNETTTB",rtseq=115,rtrep=.f.,;
    cFormVar = "w_DESMAF", cQueryName = "DESMAF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25635786,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=189, Top=245, InputMask=replicate('X',35)

  add object oDESFOF_4_53 as StdField with uid="HZPHPWHVCM",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DESFOF", cQueryName = "DESFOF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11414474,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=280, Top=437, InputMask=replicate('X',40)

  add object oDESCANF_4_54 as StdField with uid="HXSGZCDNIS",rtseq=117,rtrep=.f.,;
    cFormVar = "w_DESCANF", cQueryName = "DESCANF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 107926582,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=237, Top=341, InputMask=replicate('X',30)

  add object oDESATF_4_55 as StdField with uid="DQYXKCQONM",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESATF", cQueryName = "DESATF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 6499274,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=237, Top=389, InputMask=replicate('X',30)

  add object oDESMAGI_4_57 as StdField with uid="QONUNYBHAK",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 259576886,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=189, Top=269, InputMask=replicate('X',30)

  add object oDESMAGF_4_58 as StdField with uid="EBXVFVWORM",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 259576886,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=189, Top=293, InputMask=replicate('X',30)


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=441, top=110, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=4;
    , HelpContextID = 21863962

  add object oStr_4_56 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=437, Top=77,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_60 as StdString with uid="CJIPWEAGWS",Visible=.t., Left=561, Top=413,;
    Alignment=1, Width=107, Height=15,;
    Caption="Tipo gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_63 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=5, Top=293,;
    Alignment=1, Width=118, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_64 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=5, Top=269,;
    Alignment=1, Width=118, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_65 as StdString with uid="VBDGHHTYIN",Visible=.t., Left=16, Top=485,;
    Alignment=1, Width=107, Height=15,;
    Caption="A codice di fase:"  ;
  , bGlobalFont=.t.

  func oStr_4_65.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL$'E-C-F-T')
    endwith
  endfunc

  add object oStr_4_66 as StdString with uid="XMQKHQVLBA",Visible=.t., Left=12, Top=461,;
    Alignment=1, Width=111, Height=15,;
    Caption="Da codice di fase:"  ;
  , bGlobalFont=.t.

  func oStr_4_66.mHide()
    with this.Parent.oContained
      return (.w_MPS_ODL$'E-C-F-T')
    endwith
  endfunc

  add object oStr_4_67 as StdString with uid="EAPXXFINWA",Visible=.t., Left=16, Top=389,;
    Alignment=1, Width=107, Height=15,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_68 as StdString with uid="XKQOJLGOQV",Visible=.t., Left=16, Top=341,;
    Alignment=1, Width=107, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_69 as StdString with uid="LEZVOAVKGD",Visible=.t., Left=13, Top=437,;
    Alignment=1, Width=110, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_70 as StdString with uid="YOAHAQTVAM",Visible=.t., Left=38, Top=245,;
    Alignment=1, Width=85, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_4_71 as StdString with uid="QCTHNSNMNF",Visible=.t., Left=20, Top=101,;
    Alignment=1, Width=103, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_4_72 as StdString with uid="RMFZDVMSDO",Visible=.t., Left=0, Top=197,;
    Alignment=1, Width=123, Height=15,;
    Caption="A cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_4_73 as StdString with uid="TRPYWGVGAK",Visible=.t., Left=13, Top=149,;
    Alignment=1, Width=110, Height=15,;
    Caption="A gr.merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_4_74 as StdString with uid="SGSTIMCHVV",Visible=.t., Left=38, Top=221,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_4_75 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=20, Top=77,;
    Alignment=1, Width=103, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_4_76 as StdString with uid="EAQXXFQMDK",Visible=.t., Left=13, Top=413,;
    Alignment=1, Width=110, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_77 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=16, Top=365,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_78 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=16, Top=317,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_79 as StdString with uid="OTZSPAQGTS",Visible=.t., Left=0, Top=173,;
    Alignment=1, Width=123, Height=15,;
    Caption="Da cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_4_80 as StdString with uid="FNLHDDRZDW",Visible=.t., Left=2, Top=125,;
    Alignment=1, Width=121, Height=15,;
    Caption="Da gr.merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_4_81 as StdString with uid="GSERYQDUJG",Visible=.t., Left=16, Top=53,;
    Alignment=1, Width=107, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_4_82 as StdString with uid="QSJPSSNCLY",Visible=.t., Left=12, Top=29,;
    Alignment=1, Width=111, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kmp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
