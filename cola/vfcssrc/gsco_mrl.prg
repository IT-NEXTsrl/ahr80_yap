* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mrl                                                        *
*              Risorse della fase                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-10                                                      *
* Last revis.: 2016-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mrl")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mrl")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mrl")
  return

* --- Class definition
define class tgsco_mrl as StdPCForm
  Width  = 831
  Height = 196+35
  Top    = 10
  Left   = 10
  cComment = "Risorse della fase"
  cPrg = "gsco_mrl"
  HelpContextID=154557079
  add object cnt as tcgsco_mrl
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mrl as PCContext
  w_RLCODODL = space(15)
  w_RLROWNUM = 0
  w_CSFLAPRE = space(1)
  w_EDITROW = space(1)
  w_EDITCHK = space(1)
  w_CPROWORD = 0
  w_RLCODCLA = space(5)
  w_RLCODCLA = space(5)
  w_RL__TIPO = space(2)
  w_RLCODICE = space(20)
  w_DESCLA = space(40)
  w_RLTCORIS = 0
  w_RLTCOEXT = 0
  w_RLCODPAD = space(20)
  w_RLQTARIS = 0
  w_RLUMTRIS = space(5)
  w_CONSEC = 0
  w_RLTEMRIS = 0
  w_RLTEMSEC = 0
  w_RLTPRRIU = 0
  w_RLTPRRIS = 0
  w_CLARIS = space(20)
  w_RLTPSRIU = 0
  w_QTARIS = 0
  w_UMTDEF = space(5)
  w_RLTPSRIS = 0
  w_RLTPSTDS = 0
  w_RLQPSRIS = 0
  w_RLDESCRI = space(40)
  w_RLTPREVS = 0
  w_RLNUMIMP = 0
  w_RLTCONSS = 0
  w_RLINTEST = space(1)
  w_RLTMPCIC = 0
  w_RLTMPSEC = 0
  w_RLPROORA = 0
  w_RLMAGWIP = space(5)
  w_RLTIPTEM = space(1)
  w_UMFLTEMP = space(1)
  w_RL1TIPO = space(2)
  w_RLTIPTET = space(1)
  w_RLTEMCOD = 0
  w_RLISTLAV = space(10)
  proc Save(i_oFrom)
    this.w_RLCODODL = i_oFrom.w_RLCODODL
    this.w_RLROWNUM = i_oFrom.w_RLROWNUM
    this.w_CSFLAPRE = i_oFrom.w_CSFLAPRE
    this.w_EDITROW = i_oFrom.w_EDITROW
    this.w_EDITCHK = i_oFrom.w_EDITCHK
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_RLCODCLA = i_oFrom.w_RLCODCLA
    this.w_RLCODCLA = i_oFrom.w_RLCODCLA
    this.w_RL__TIPO = i_oFrom.w_RL__TIPO
    this.w_RLCODICE = i_oFrom.w_RLCODICE
    this.w_DESCLA = i_oFrom.w_DESCLA
    this.w_RLTCORIS = i_oFrom.w_RLTCORIS
    this.w_RLTCOEXT = i_oFrom.w_RLTCOEXT
    this.w_RLCODPAD = i_oFrom.w_RLCODPAD
    this.w_RLQTARIS = i_oFrom.w_RLQTARIS
    this.w_RLUMTRIS = i_oFrom.w_RLUMTRIS
    this.w_CONSEC = i_oFrom.w_CONSEC
    this.w_RLTEMRIS = i_oFrom.w_RLTEMRIS
    this.w_RLTEMSEC = i_oFrom.w_RLTEMSEC
    this.w_RLTPRRIU = i_oFrom.w_RLTPRRIU
    this.w_RLTPRRIS = i_oFrom.w_RLTPRRIS
    this.w_CLARIS = i_oFrom.w_CLARIS
    this.w_RLTPSRIU = i_oFrom.w_RLTPSRIU
    this.w_QTARIS = i_oFrom.w_QTARIS
    this.w_UMTDEF = i_oFrom.w_UMTDEF
    this.w_RLTPSRIS = i_oFrom.w_RLTPSRIS
    this.w_RLTPSTDS = i_oFrom.w_RLTPSTDS
    this.w_RLQPSRIS = i_oFrom.w_RLQPSRIS
    this.w_RLDESCRI = i_oFrom.w_RLDESCRI
    this.w_RLTPREVS = i_oFrom.w_RLTPREVS
    this.w_RLNUMIMP = i_oFrom.w_RLNUMIMP
    this.w_RLTCONSS = i_oFrom.w_RLTCONSS
    this.w_RLINTEST = i_oFrom.w_RLINTEST
    this.w_RLTMPCIC = i_oFrom.w_RLTMPCIC
    this.w_RLTMPSEC = i_oFrom.w_RLTMPSEC
    this.w_RLPROORA = i_oFrom.w_RLPROORA
    this.w_RLMAGWIP = i_oFrom.w_RLMAGWIP
    this.w_RLTIPTEM = i_oFrom.w_RLTIPTEM
    this.w_UMFLTEMP = i_oFrom.w_UMFLTEMP
    this.w_RL1TIPO = i_oFrom.w_RL1TIPO
    this.w_RLTIPTET = i_oFrom.w_RLTIPTET
    this.w_RLTEMCOD = i_oFrom.w_RLTEMCOD
    this.w_RLISTLAV = i_oFrom.w_RLISTLAV
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_RLCODODL = this.w_RLCODODL
    i_oTo.w_RLROWNUM = this.w_RLROWNUM
    i_oTo.w_CSFLAPRE = this.w_CSFLAPRE
    i_oTo.w_EDITROW = this.w_EDITROW
    i_oTo.w_EDITCHK = this.w_EDITCHK
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_RLCODCLA = this.w_RLCODCLA
    i_oTo.w_RLCODCLA = this.w_RLCODCLA
    i_oTo.w_RL__TIPO = this.w_RL__TIPO
    i_oTo.w_RLCODICE = this.w_RLCODICE
    i_oTo.w_DESCLA = this.w_DESCLA
    i_oTo.w_RLTCORIS = this.w_RLTCORIS
    i_oTo.w_RLTCOEXT = this.w_RLTCOEXT
    i_oTo.w_RLCODPAD = this.w_RLCODPAD
    i_oTo.w_RLQTARIS = this.w_RLQTARIS
    i_oTo.w_RLUMTRIS = this.w_RLUMTRIS
    i_oTo.w_CONSEC = this.w_CONSEC
    i_oTo.w_RLTEMRIS = this.w_RLTEMRIS
    i_oTo.w_RLTEMSEC = this.w_RLTEMSEC
    i_oTo.w_RLTPRRIU = this.w_RLTPRRIU
    i_oTo.w_RLTPRRIS = this.w_RLTPRRIS
    i_oTo.w_CLARIS = this.w_CLARIS
    i_oTo.w_RLTPSRIU = this.w_RLTPSRIU
    i_oTo.w_QTARIS = this.w_QTARIS
    i_oTo.w_UMTDEF = this.w_UMTDEF
    i_oTo.w_RLTPSRIS = this.w_RLTPSRIS
    i_oTo.w_RLTPSTDS = this.w_RLTPSTDS
    i_oTo.w_RLQPSRIS = this.w_RLQPSRIS
    i_oTo.w_RLDESCRI = this.w_RLDESCRI
    i_oTo.w_RLTPREVS = this.w_RLTPREVS
    i_oTo.w_RLNUMIMP = this.w_RLNUMIMP
    i_oTo.w_RLTCONSS = this.w_RLTCONSS
    i_oTo.w_RLINTEST = this.w_RLINTEST
    i_oTo.w_RLTMPCIC = this.w_RLTMPCIC
    i_oTo.w_RLTMPSEC = this.w_RLTMPSEC
    i_oTo.w_RLPROORA = this.w_RLPROORA
    i_oTo.w_RLMAGWIP = this.w_RLMAGWIP
    i_oTo.w_RLTIPTEM = this.w_RLTIPTEM
    i_oTo.w_UMFLTEMP = this.w_UMFLTEMP
    i_oTo.w_RL1TIPO = this.w_RL1TIPO
    i_oTo.w_RLTIPTET = this.w_RLTIPTET
    i_oTo.w_RLTEMCOD = this.w_RLTEMCOD
    i_oTo.w_RLISTLAV = this.w_RLISTLAV
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mrl as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 831
  Height = 196+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-06-07"
  HelpContextID=154557079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  ODLMRISO_IDX = 0
  ODL_RISO_IDX = 0
  RIS_ORSE_IDX = 0
  ODL_MOUT_IDX = 0
  UNIMIS_IDX = 0
  CLR_DETT_IDX = 0
  CLR_MAST_IDX = 0
  cFile = "ODLMRISO"
  cFileDetail = "ODL_RISO"
  cKeySelect = "RLCODODL,RLROWNUM"
  cKeyWhere  = "RLCODODL=this.w_RLCODODL and RLROWNUM=this.w_RLROWNUM"
  cKeyDetail  = "RLCODODL=this.w_RLCODODL and RLROWNUM=this.w_RLROWNUM"
  cKeyWhereODBC = '"RLCODODL="+cp_ToStrODBC(this.w_RLCODODL)';
      +'+" and RLROWNUM="+cp_ToStrODBC(this.w_RLROWNUM)';

  cKeyDetailWhereODBC = '"RLCODODL="+cp_ToStrODBC(this.w_RLCODODL)';
      +'+" and RLROWNUM="+cp_ToStrODBC(this.w_RLROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"ODL_RISO.RLCODODL="+cp_ToStrODBC(this.w_RLCODODL)';
      +'+" and ODL_RISO.RLROWNUM="+cp_ToStrODBC(this.w_RLROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ODL_RISO.CPROWORD'
  cPrg = "gsco_mrl"
  cComment = "Risorse della fase"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RLCODODL = space(15)
  w_RLROWNUM = 0
  w_CSFLAPRE = space(1)
  w_EDITROW = .F.
  w_EDITCHK = .F.
  w_CPROWORD = 0
  w_RLCODCLA = space(5)
  o_RLCODCLA = space(5)
  w_RLCODCLA = space(5)
  w_RL__TIPO = space(2)
  w_RLCODICE = space(20)
  o_RLCODICE = space(20)
  w_DESCLA = space(40)
  w_RLTCORIS = 0
  w_RLTCOEXT = 0
  w_RLCODPAD = space(20)
  w_RLQTARIS = 0
  w_RLUMTRIS = space(5)
  w_CONSEC = 0
  w_RLTEMRIS = 0
  o_RLTEMRIS = 0
  w_RLTEMSEC = 0
  w_RLTPRRIU = 0
  w_RLTPRRIS = 0
  w_CLARIS = space(20)
  w_RLTPSRIU = 0
  w_QTARIS = 0
  w_UMTDEF = space(5)
  w_RLTPSRIS = 0
  w_RLTPSTDS = 0
  w_RLQPSRIS = 0
  w_RLDESCRI = space(40)
  w_RLTPREVS = 0
  w_RLNUMIMP = 0
  o_RLNUMIMP = 0
  w_RLTCONSS = 0
  w_RLINTEST = space(1)
  w_RLTMPCIC = 0
  o_RLTMPCIC = 0
  w_RLTMPSEC = 0
  w_RLPROORA = 0
  w_RLMAGWIP = space(5)
  w_RLTIPTEM = space(1)
  w_UMFLTEMP = space(1)
  w_RL1TIPO = space(2)
  w_RLTIPTET = space(1)
  w_RLTEMCOD = 0
  w_RLISTLAV = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_mrl
   * - Riedefinita eliminazione riga
   PROC F6()
     if inlist(this.cFunction,'Edit','Load') and this.w_EDITROW
       dodefault()
     endif
   ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mrlPag1","gsco_mrl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Risorse")
      .Pages(2).addobject("oPag","tgsco_mrlPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Note fase")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='ODL_MOUT'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='CLR_DETT'
    this.cWorkTables[5]='CLR_MAST'
    this.cWorkTables[6]='ODLMRISO'
    this.cWorkTables[7]='ODL_RISO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ODLMRISO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ODLMRISO_IDX,3]
  return

    procedure NewContext()
      return(createobject('tsgsco_mrl'))


  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from ODLMRISO where RLCODODL=KeySet.RLCODODL
    *                            and RLROWNUM=KeySet.RLROWNUM
    *
    i_nConn = i_TableProp[this.ODLMRISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODLMRISO_IDX,2],this.bLoadRecFilter,this.ODLMRISO_IDX,"gsco_mrl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ODLMRISO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ODLMRISO.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"ODL_RISO.","ODLMRISO.")
      i_cTable = i_cTable+' ODLMRISO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RLCODODL',this.w_RLCODODL  ,'RLROWNUM',this.w_RLROWNUM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CSFLAPRE = 'S'
        .w_UMFLTEMP = 'S'
        .w_RLCODODL = NVL(RLCODODL,space(15))
        .w_RLROWNUM = NVL(RLROWNUM,0)
        .w_EDITROW = this.oParentObject.w_EDITROW
        .w_EDITCHK = this.oParentObject.w_EDITCHK
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_RLISTLAV = NVL(RLISTLAV,space(0))
        cp_LoadRecExtFlds(this,'ODLMRISO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from ODL_RISO where RLCODODL=KeySet.RLCODODL
      *                            and RLROWNUM=KeySet.RLROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.ODL_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISO_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('ODL_RISO')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "ODL_RISO.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" ODL_RISO"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'RLCODODL',this.w_RLCODODL  ,'RLROWNUM',this.w_RLROWNUM  )
        select * from (i_cTable) ODL_RISO where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCLA = space(40)
          .w_CONSEC = 0
          .w_CLARIS = space(20)
          .w_QTARIS = 0
          .w_UMTDEF = space(5)
          .w_RLDESCRI = space(40)
          .w_RLINTEST = space(1)
          .w_RLMAGWIP = space(5)
          .w_RLTEMCOD = 0
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RLCODCLA = NVL(RLCODCLA,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_RLCODCLA = NVL(RLCODCLA,space(5))
          if link_2_3_joined
            this.w_RLCODCLA = NVL(CSCODCLA203,NVL(this.w_RLCODCLA,space(5)))
            this.w_RL__TIPO = NVL(CSTIPCLA203,space(2))
            this.w_DESCLA = NVL(CSDESCLA203,space(40))
          else
          .link_2_3('Load')
          endif
          .w_RL__TIPO = NVL(RL__TIPO,space(2))
          .w_RLCODICE = NVL(RLCODICE,space(20))
          .link_2_5('Load')
          .w_RLTCORIS = NVL(RLTCORIS,0)
          .w_RLTCOEXT = NVL(RLTCOEXT,0)
          .w_RLCODPAD = NVL(RLCODPAD,space(20))
          .w_RLQTARIS = NVL(RLQTARIS,0)
          .w_RLUMTRIS = NVL(RLUMTRIS,space(5))
          .link_2_11('Load')
          .w_RLTEMRIS = NVL(RLTEMRIS,0)
          .w_RLTEMSEC = NVL(RLTEMSEC,0)
          .w_RLTPRRIU = NVL(RLTPRRIU,0)
          .w_RLTPRRIS = NVL(RLTPRRIS,0)
          .w_RLTPSRIU = NVL(RLTPSRIU,0)
          .w_RLTPSRIS = NVL(RLTPSRIS,0)
          .w_RLTPSTDS = NVL(RLTPSTDS,0)
          .w_RLQPSRIS = NVL(RLQPSRIS,0)
          .w_RLTPREVS = NVL(RLTPREVS,0)
          .w_RLNUMIMP = NVL(RLNUMIMP,0)
          .w_RLTCONSS = NVL(RLTCONSS,0)
          .w_RLTMPCIC = NVL(RLTMPCIC,0)
          .w_RLTMPSEC = NVL(RLTMPSEC,0)
          .w_RLPROORA = NVL(RLPROORA,0)
          .w_RLTIPTEM = NVL(RLTIPTEM,space(1))
        .w_RL1TIPO = .w_RL__TIPO
        .w_RLTIPTET = .w_RLTIPTEM
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_EDITROW = this.oParentObject.w_EDITROW
        .w_EDITCHK = this.oParentObject.w_EDITCHK
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsco_mrl
    EndProc
    
    Proc SetEditableRow()
         with this
          .w_EDITROW = .oParentObject.w_EDITROW
          .w_EDITCHK = .oParentObject.w_EDITCHK
          .mEnableControls()
         endwith
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RLCODODL=space(15)
      .w_RLROWNUM=0
      .w_CSFLAPRE=space(1)
      .w_EDITROW=.f.
      .w_EDITCHK=.f.
      .w_CPROWORD=10
      .w_RLCODCLA=space(5)
      .w_RLCODCLA=space(5)
      .w_RL__TIPO=space(2)
      .w_RLCODICE=space(20)
      .w_DESCLA=space(40)
      .w_RLTCORIS=0
      .w_RLTCOEXT=0
      .w_RLCODPAD=space(20)
      .w_RLQTARIS=0
      .w_RLUMTRIS=space(5)
      .w_CONSEC=0
      .w_RLTEMRIS=0
      .w_RLTEMSEC=0
      .w_RLTPRRIU=0
      .w_RLTPRRIS=0
      .w_CLARIS=space(20)
      .w_RLTPSRIU=0
      .w_QTARIS=0
      .w_UMTDEF=space(5)
      .w_RLTPSRIS=0
      .w_RLTPSTDS=0
      .w_RLQPSRIS=0
      .w_RLDESCRI=space(40)
      .w_RLTPREVS=0
      .w_RLNUMIMP=0
      .w_RLTCONSS=0
      .w_RLINTEST=space(1)
      .w_RLTMPCIC=0
      .w_RLTMPSEC=0
      .w_RLPROORA=0
      .w_RLMAGWIP=space(5)
      .w_RLTIPTEM=space(1)
      .w_UMFLTEMP=space(1)
      .w_RL1TIPO=space(2)
      .w_RLTIPTET=space(1)
      .w_RLTEMCOD=0
      .w_RLISTLAV=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CSFLAPRE = 'S'
        .w_EDITROW = this.oParentObject.w_EDITROW
        .w_EDITCHK = this.oParentObject.w_EDITCHK
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_RLCODCLA))
         .link_2_2('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_RLCODCLA))
         .link_2_3('Full')
        endif
        .w_RL__TIPO = 'CL'
        .w_RLCODICE = iif(empty(.w_CLARIS),.w_RLCODICE,.w_CLARIS)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_RLCODICE))
         .link_2_5('Full')
        endif
        .DoRTCalc(11,13,.f.)
        .w_RLCODPAD = .w_RLCODICE
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_RLUMTRIS))
         .link_2_11('Full')
        endif
        .DoRTCalc(17,17,.f.)
        .w_RLTEMRIS = iif(.w_RLTMPCIC > 0 , .w_RLTMPCIC / .w_RLNUMIMP , 0)
        .w_RLTEMSEC = .w_RLTEMRIS*.w_CONSEC
        .w_RLTPRRIU = .w_RLTEMSEC*.w_RLQTARIS
        .w_RLTPRRIS = .w_RLTPRRIU*iif(.w_RLTIPTEM='L',.oParentObject.oParentObject.w_OLTQTOD1,1)
        .DoRTCalc(22,22,.f.)
        .w_RLTPSRIU = .w_RLTEMSEC*.w_RLQTARIS
        .DoRTCalc(24,25,.f.)
        .w_RLTPSRIS = .w_RLTPSRIU*.oParentObject.oParentObject.w_OLTQTOD1
        .w_RLTPSTDS = .w_RLTPRRIS
        .w_RLQPSRIS = .w_RLQTARIS
        .DoRTCalc(29,29,.f.)
        .w_RLTPREVS = .w_RLTPRRIS
        .w_RLNUMIMP = 1
        .w_RLTCONSS = .w_RLTCORIS
        .DoRTCalc(33,33,.f.)
        .w_RLTMPCIC = iif(.w_RLTEMRIS>0 , .w_RLTEMRIS * .w_RLNUMIMP, 0)
        .w_RLTMPSEC = .w_RLTMPCIC*.w_CONSEC
        .w_RLPROORA = iif(.w_RLTEMSEC=0 and .w_RLTMPSEC=0 , 0, 3600/IIF(.w_RLTMPSEC<>0 , .w_RLTMPSEC, .w_RLTEMSEC)) * IIF(.w_RLTMPSEC<>0,.w_RLNUMIMP,1)
        .DoRTCalc(37,37,.f.)
        .w_RLTIPTEM = 'L'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UMFLTEMP = 'S'
        .w_RL1TIPO = .w_RL__TIPO
        .w_RLTIPTET = .w_RLTIPTEM
      endif
    endwith
    cp_BlankRecExtFlds(this,'ODLMRISO')
    this.DoRTCalc(42,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oRLNUMIMP_2_26.enabled = i_bVal
      .Page1.oPag.oRLTMPCIC_2_29.enabled = i_bVal
      .Page2.oPag.oRLISTLAV_4_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ODLMRISO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ODLMRISO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODODL,"RLCODODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLROWNUM,"RLROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLISTLAV,"RLISTLAV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_RLCODCLA C(5);
      ,t_RL__TIPO N(3);
      ,t_RLCODICE C(20);
      ,t_DESCLA C(40);
      ,t_RLQTARIS N(8,2);
      ,t_RLUMTRIS C(5);
      ,t_RLTEMRIS N(18,4);
      ,t_RLTEMSEC N(18,4);
      ,t_RLDESCRI C(40);
      ,t_RLTPREVS N(18,4);
      ,t_RLNUMIMP N(4);
      ,t_RLTCONSS N(18,4);
      ,t_RLINTEST N(3);
      ,t_RLTMPCIC N(18,4);
      ,t_RLPROORA N(18,4);
      ,t_RLTIPTEM N(3);
      ,CPROWNUM N(10);
      ,t_RLTCORIS N(18,4);
      ,t_RLTCOEXT N(18,4);
      ,t_RLCODPAD C(20);
      ,t_CONSEC N(9,5);
      ,t_RLTPRRIU N(18,4);
      ,t_RLTPRRIS N(18,4);
      ,t_CLARIS C(20);
      ,t_RLTPSRIU N(18,4);
      ,t_QTARIS N(12,5);
      ,t_UMTDEF C(5);
      ,t_RLTPSRIS N(18,4);
      ,t_RLTPSTDS N(18,4);
      ,t_RLQPSRIS N(5);
      ,t_RLTMPSEC N(18,4);
      ,t_RLMAGWIP C(5);
      ,t_RL1TIPO C(2);
      ,t_RLTIPTET C(1);
      ,t_RLTEMCOD N(12,5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mrlbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODCLA_2_2.controlsource=this.cTrsName+'.t_RLCODCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRL__TIPO_2_4.controlsource=this.cTrsName+'.t_RL__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODICE_2_5.controlsource=this.cTrsName+'.t_RLCODICE'
    this.oPgFRm.Page1.oPag.oDESCLA_2_6.controlsource=this.cTrsName+'.t_DESCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLQTARIS_2_10.controlsource=this.cTrsName+'.t_RLQTARIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLUMTRIS_2_11.controlsource=this.cTrsName+'.t_RLUMTRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMRIS_2_13.controlsource=this.cTrsName+'.t_RLTEMRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMSEC_2_14.controlsource=this.cTrsName+'.t_RLTEMSEC'
    this.oPgFRm.Page1.oPag.oRLDESCRI_2_24.controlsource=this.cTrsName+'.t_RLDESCRI'
    this.oPgFRm.Page1.oPag.oRLTPREVS_2_25.controlsource=this.cTrsName+'.t_RLTPREVS'
    this.oPgFRm.Page1.oPag.oRLNUMIMP_2_26.controlsource=this.cTrsName+'.t_RLNUMIMP'
    this.oPgFRm.Page1.oPag.oRLTCONSS_2_27.controlsource=this.cTrsName+'.t_RLTCONSS'
    this.oPgFRm.Page1.oPag.oRLINTEST_2_28.controlsource=this.cTrsName+'.t_RLINTEST'
    this.oPgFRm.Page1.oPag.oRLTMPCIC_2_29.controlsource=this.cTrsName+'.t_RLTMPCIC'
    this.oPgFRm.Page1.oPag.oRLPROORA_2_31.controlsource=this.cTrsName+'.t_RLPROORA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRLTIPTEM_2_33.controlsource=this.cTrsName+'.t_RLTIPTEM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(48)
    this.AddVLine(114)
    this.AddVLine(229)
    this.AddVLine(387)
    this.AddVLine(520)
    this.AddVLine(558)
    this.AddVLine(610)
    this.AddVLine(695)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODLMRISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODLMRISO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ODLMRISO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ODLMRISO')
        i_extval=cp_InsertValODBCExtFlds(this,'ODLMRISO')
        local i_cFld
        i_cFld=" "+;
                  "(RLCODODL,RLROWNUM,RLISTLAV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_RLCODODL)+;
                    ","+cp_ToStrODBC(this.w_RLROWNUM)+;
                    ","+cp_ToStrODBC(this.w_RLISTLAV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ODLMRISO')
        i_extval=cp_InsertValVFPExtFlds(this,'ODLMRISO')
        cp_CheckDeletedKey(i_cTable,0,'RLCODODL',this.w_RLCODODL,'RLROWNUM',this.w_RLROWNUM)
        INSERT INTO (i_cTable);
              (RLCODODL,RLROWNUM,RLISTLAV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_RLCODODL;
                  ,this.w_RLROWNUM;
                  ,this.w_RLISTLAV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODL_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISO_IDX,2])
      *
      * insert into ODL_RISO
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(RLCODODL,RLROWNUM,CPROWORD,RLCODCLA,RL__TIPO"+;
                  ",RLCODICE,RLTCORIS,RLTCOEXT,RLCODPAD,RLQTARIS"+;
                  ",RLUMTRIS,RLTEMRIS,RLTEMSEC,RLTPRRIU,RLTPRRIS"+;
                  ",RLTPSRIU,RLTPSRIS,RLTPSTDS,RLQPSRIS,RLTPREVS"+;
                  ",RLNUMIMP,RLTCONSS,RLTMPCIC,RLTMPSEC,RLPROORA"+;
                  ",RLTIPTEM,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RLCODODL)+","+cp_ToStrODBC(this.w_RLROWNUM)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_RLCODCLA)+","+cp_ToStrODBC(this.w_RL__TIPO)+;
             ","+cp_ToStrODBCNull(this.w_RLCODICE)+","+cp_ToStrODBC(this.w_RLTCORIS)+","+cp_ToStrODBC(this.w_RLTCOEXT)+","+cp_ToStrODBC(this.w_RLCODPAD)+","+cp_ToStrODBC(this.w_RLQTARIS)+;
             ","+cp_ToStrODBCNull(this.w_RLUMTRIS)+","+cp_ToStrODBC(this.w_RLTEMRIS)+","+cp_ToStrODBC(this.w_RLTEMSEC)+","+cp_ToStrODBC(this.w_RLTPRRIU)+","+cp_ToStrODBC(this.w_RLTPRRIS)+;
             ","+cp_ToStrODBC(this.w_RLTPSRIU)+","+cp_ToStrODBC(this.w_RLTPSRIS)+","+cp_ToStrODBC(this.w_RLTPSTDS)+","+cp_ToStrODBC(this.w_RLQPSRIS)+","+cp_ToStrODBC(this.w_RLTPREVS)+;
             ","+cp_ToStrODBC(this.w_RLNUMIMP)+","+cp_ToStrODBC(this.w_RLTCONSS)+","+cp_ToStrODBC(this.w_RLTMPCIC)+","+cp_ToStrODBC(this.w_RLTMPSEC)+","+cp_ToStrODBC(this.w_RLPROORA)+;
             ","+cp_ToStrODBC(this.w_RLTIPTEM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RLCODODL',this.w_RLCODODL,'RLROWNUM',this.w_RLROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_RLCODODL,this.w_RLROWNUM,this.w_CPROWORD,this.w_RLCODCLA,this.w_RL__TIPO"+;
                ",this.w_RLCODICE,this.w_RLTCORIS,this.w_RLTCOEXT,this.w_RLCODPAD,this.w_RLQTARIS"+;
                ",this.w_RLUMTRIS,this.w_RLTEMRIS,this.w_RLTEMSEC,this.w_RLTPRRIU,this.w_RLTPRRIS"+;
                ",this.w_RLTPSRIU,this.w_RLTPSRIS,this.w_RLTPSTDS,this.w_RLQPSRIS,this.w_RLTPREVS"+;
                ",this.w_RLNUMIMP,this.w_RLTCONSS,this.w_RLTMPCIC,this.w_RLTMPSEC,this.w_RLPROORA"+;
                ",this.w_RLTIPTEM,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ODLMRISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODLMRISO_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update ODLMRISO
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'ODLMRISO')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " RLISTLAV="+cp_ToStrODBC(this.w_RLISTLAV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'ODLMRISO')
          i_cWhere = cp_PKFox(i_cTable  ,'RLCODODL',this.w_RLCODODL  ,'RLROWNUM',this.w_RLROWNUM  )
          UPDATE (i_cTable) SET;
              RLISTLAV=this.w_RLISTLAV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and not(Empty(t_RL__TIPO)) and not(Empty(t_RLCODICE)) and not(Empty(t_RLTIPTEM)) and t_RLQTARIS > 0 and not(Empty(t_RLUMTRIS)) and t_RLTEMRIS > 0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.ODL_RISO_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISO_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from ODL_RISO
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ODL_RISO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",RLCODCLA="+cp_ToStrODBCNull(this.w_RLCODCLA)+;
                     ",RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)+;
                     ",RLCODICE="+cp_ToStrODBCNull(this.w_RLCODICE)+;
                     ",RLTCORIS="+cp_ToStrODBC(this.w_RLTCORIS)+;
                     ",RLTCOEXT="+cp_ToStrODBC(this.w_RLTCOEXT)+;
                     ",RLCODPAD="+cp_ToStrODBC(this.w_RLCODPAD)+;
                     ",RLQTARIS="+cp_ToStrODBC(this.w_RLQTARIS)+;
                     ",RLUMTRIS="+cp_ToStrODBCNull(this.w_RLUMTRIS)+;
                     ",RLTEMRIS="+cp_ToStrODBC(this.w_RLTEMRIS)+;
                     ",RLTEMSEC="+cp_ToStrODBC(this.w_RLTEMSEC)+;
                     ",RLTPRRIU="+cp_ToStrODBC(this.w_RLTPRRIU)+;
                     ",RLTPRRIS="+cp_ToStrODBC(this.w_RLTPRRIS)+;
                     ",RLTPSRIU="+cp_ToStrODBC(this.w_RLTPSRIU)+;
                     ",RLTPSRIS="+cp_ToStrODBC(this.w_RLTPSRIS)+;
                     ",RLTPSTDS="+cp_ToStrODBC(this.w_RLTPSTDS)+;
                     ",RLQPSRIS="+cp_ToStrODBC(this.w_RLQPSRIS)+;
                     ",RLTPREVS="+cp_ToStrODBC(this.w_RLTPREVS)+;
                     ",RLNUMIMP="+cp_ToStrODBC(this.w_RLNUMIMP)+;
                     ",RLTCONSS="+cp_ToStrODBC(this.w_RLTCONSS)+;
                     ",RLTMPCIC="+cp_ToStrODBC(this.w_RLTMPCIC)+;
                     ",RLTMPSEC="+cp_ToStrODBC(this.w_RLTMPSEC)+;
                     ",RLPROORA="+cp_ToStrODBC(this.w_RLPROORA)+;
                     ",RLTIPTEM="+cp_ToStrODBC(this.w_RLTIPTEM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,RLCODCLA=this.w_RLCODCLA;
                     ,RL__TIPO=this.w_RL__TIPO;
                     ,RLCODICE=this.w_RLCODICE;
                     ,RLTCORIS=this.w_RLTCORIS;
                     ,RLTCOEXT=this.w_RLTCOEXT;
                     ,RLCODPAD=this.w_RLCODPAD;
                     ,RLQTARIS=this.w_RLQTARIS;
                     ,RLUMTRIS=this.w_RLUMTRIS;
                     ,RLTEMRIS=this.w_RLTEMRIS;
                     ,RLTEMSEC=this.w_RLTEMSEC;
                     ,RLTPRRIU=this.w_RLTPRRIU;
                     ,RLTPRRIS=this.w_RLTPRRIS;
                     ,RLTPSRIU=this.w_RLTPSRIU;
                     ,RLTPSRIS=this.w_RLTPSRIS;
                     ,RLTPSTDS=this.w_RLTPSTDS;
                     ,RLQPSRIS=this.w_RLQPSRIS;
                     ,RLTPREVS=this.w_RLTPREVS;
                     ,RLNUMIMP=this.w_RLNUMIMP;
                     ,RLTCONSS=this.w_RLTCONSS;
                     ,RLTMPCIC=this.w_RLTMPCIC;
                     ,RLTMPSEC=this.w_RLTMPSEC;
                     ,RLPROORA=this.w_RLPROORA;
                     ,RLTIPTEM=this.w_RLTIPTEM;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and not(Empty(t_RL__TIPO)) and not(Empty(t_RLCODICE)) and not(Empty(t_RLTIPTEM)) and t_RLQTARIS > 0 and not(Empty(t_RLUMTRIS)) and t_RLTEMRIS > 0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.ODL_RISO_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISO_IDX,2])
        *
        * delete ODL_RISO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.ODLMRISO_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ODLMRISO_IDX,2])
        *
        * delete ODLMRISO
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and not(Empty(t_RL__TIPO)) and not(Empty(t_RLCODICE)) and not(Empty(t_RLTIPTEM)) and t_RLQTARIS > 0 and not(Empty(t_RLUMTRIS)) and t_RLTEMRIS > 0) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODLMRISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODLMRISO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_EDITROW = this.oParentObject.w_EDITROW
          .w_EDITCHK = this.oParentObject.w_EDITCHK
        .DoRTCalc(6,7,.t.)
        if .o_RLCODCLA<>.w_RLCODCLA
          .link_2_3('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_RLCODCLA<>.w_RLCODCLA.or. .o_RLCODICE<>.w_RLCODICE
          .w_RLCODICE = iif(empty(.w_CLARIS),.w_RLCODICE,.w_CLARIS)
          .link_2_5('Full')
        endif
        .DoRTCalc(11,13,.t.)
        if .o_RLCODICE<>.w_RLCODICE
          .w_RLCODPAD = .w_RLCODICE
        endif
        .DoRTCalc(15,17,.t.)
        if .o_RLTMPCIC<>.w_RLTMPCIC.or. .o_RLNUMIMP<>.w_RLNUMIMP
          .w_RLTEMRIS = iif(.w_RLTMPCIC > 0 , .w_RLTMPCIC / .w_RLNUMIMP , 0)
        endif
          .w_RLTEMSEC = .w_RLTEMRIS*.w_CONSEC
          .w_RLTPRRIU = .w_RLTEMSEC*.w_RLQTARIS
          .w_RLTPRRIS = .w_RLTPRRIU*iif(.w_RLTIPTEM='L',.oParentObject.oParentObject.w_OLTQTOD1,1)
        .DoRTCalc(22,22,.t.)
          .w_RLTPSRIU = .w_RLTEMSEC*.w_RLQTARIS
        .DoRTCalc(24,25,.t.)
          .w_RLTPSRIS = .w_RLTPSRIU*.oParentObject.oParentObject.w_OLTQTOD1
          .w_RLTPSTDS = .w_RLTPRRIS
          .w_RLQPSRIS = .w_RLQTARIS
        .DoRTCalc(29,29,.t.)
          .w_RLTPREVS = .w_RLTPRRIS
        .DoRTCalc(31,31,.t.)
          .w_RLTCONSS = .w_RLTCORIS
        .DoRTCalc(33,33,.t.)
        if .o_RLTEMRIS<>.w_RLTEMRIS.or. .o_RLNUMIMP<>.w_RLNUMIMP
          .w_RLTMPCIC = iif(.w_RLTEMRIS>0 , .w_RLTEMRIS * .w_RLNUMIMP, 0)
        endif
          .w_RLTMPSEC = .w_RLTMPCIC*.w_CONSEC
          .w_RLPROORA = iif(.w_RLTEMSEC=0 and .w_RLTMPSEC=0 , 0, 3600/IIF(.w_RLTMPSEC<>0 , .w_RLTMPSEC, .w_RLTEMSEC)) * IIF(.w_RLTMPSEC<>0,.w_RLNUMIMP,1)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(37,39,.t.)
          .w_RL1TIPO = .w_RL__TIPO
          .w_RLTIPTET = .w_RLTIPTEM
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_RLCODCLA with this.w_RLCODCLA
      replace t_RLTCORIS with this.w_RLTCORIS
      replace t_RLTCOEXT with this.w_RLTCOEXT
      replace t_RLCODPAD with this.w_RLCODPAD
      replace t_CONSEC with this.w_CONSEC
      replace t_RLTPRRIU with this.w_RLTPRRIU
      replace t_RLTPRRIS with this.w_RLTPRRIS
      replace t_CLARIS with this.w_CLARIS
      replace t_RLTPSRIU with this.w_RLTPSRIU
      replace t_QTARIS with this.w_QTARIS
      replace t_UMTDEF with this.w_UMTDEF
      replace t_RLTPSRIS with this.w_RLTPSRIS
      replace t_RLTPSTDS with this.w_RLTPSTDS
      replace t_RLQPSRIS with this.w_RLQPSRIS
      replace t_RLTMPSEC with this.w_RLTMPSEC
      replace t_RLMAGWIP with this.w_RLMAGWIP
      replace t_RL1TIPO with this.w_RL1TIPO
      replace t_RLTIPTET with this.w_RLTIPTET
      replace t_RLTEMCOD with this.w_RLTEMCOD
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_LDNFVOYMOP()
    with this
          * --- Recupera l'editabilit� della riga dal padre
          .w_EDITROW = this.oParentObject.w_EDITROW
          .w_EDITCHK = this.oParentObject.w_EDITCHK
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLCODCLA_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLCODCLA_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRL__TIPO_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRL__TIPO_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLCODICE_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLCODICE_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLQTARIS_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLQTARIS_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLUMTRIS_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLUMTRIS_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLTEMRIS_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLTEMRIS_2_13.mCond()
    this.oPgFrm.Page1.oPag.oRLNUMIMP_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oRLNUMIMP_2_26.mCond()
    this.oPgFrm.Page1.oPag.oRLTMPCIC_2_29.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oRLTMPCIC_2_29.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLTIPTEM_2_33.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRLTIPTEM_2_33.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("SetEditableRow") or lower(cEvent)==lower("w_CPROWORD GotFocus")
          .Calculate_LDNFVOYMOP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsco_mrl
       if cEvent=='SetEditableRow' or lower(alltrim(cEvent))=='w_cproword gotfocus'
           this.SetEditableRow()
       endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RLCODCLA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_DETT_IDX,3]
    i_lTable = "CLR_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2], .t., this.CLR_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLR_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCLA like "+cp_ToStrODBC(trim(this.w_RLCODCLA)+"%");
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);

          i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSFLAPRE,CSCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSFLAPRE',this.w_CSFLAPRE;
                     ,'CSCODCLA',trim(this.w_RLCODCLA))
          select CSFLAPRE,CSCODCLA,CSCODRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSFLAPRE,CSCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCODCLA)==trim(_Link_.CSCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(oSource.parent,'oRLCODCLA_2_2'),i_cWhere,'',"",'GSCI_MCS.CLR_DETT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CSFLAPRE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',oSource.xKey(1);
                       ,'CSCODCLA',oSource.xKey(2))
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_RLCODCLA);
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',this.w_CSFLAPRE;
                       ,'CSCODCLA',this.w_RLCODCLA)
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_RLCODICE = NVL(_Link_.CSCODRIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCLA = space(5)
      endif
      this.w_RLCODICE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])+'\'+cp_ToStr(_Link_.CSFLAPRE,1)+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLCODCLA
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_MAST_IDX,3]
    i_lTable = "CLR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2], .t., this.CLR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSTIPCLA,CSDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_RLCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',this.w_RLCODCLA)
            select CSCODCLA,CSTIPCLA,CSDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_RL__TIPO = NVL(_Link_.CSTIPCLA,space(2))
      this.w_DESCLA = NVL(_Link_.CSDESCLA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCLA = space(5)
      endif
      this.w_RL__TIPO = space(2)
      this.w_DESCLA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLR_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CSCODCLA as CSCODCLA203"+ ",link_2_3.CSTIPCLA as CSTIPCLA203"+ ",link_2_3.CSDESCLA as CSDESCLA203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on ODL_RISO.RLCODCLA=link_2_3.CSCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and ODL_RISO.RLCODCLA=link_2_3.CSCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLCODICE
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_RLCODICE)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_RL__TIPO;
                     ,'RLCODICE',trim(this.w_RLCODICE))
          select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCODICE)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_RLCODICE)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_RLCODICE)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_RL__TIPO);

            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RLCODICE) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oRLCODICE_2_5'),i_cWhere,'',"RISORSE",'GSCO_ZLR.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RL__TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_RLCODICE);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_RL__TIPO;
                       ,'RLCODICE',this.w_RLCODICE)
            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP,RLTEMCOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODICE = NVL(_Link_.RLCODICE,space(20))
      this.w_RLDESCRI = NVL(_Link_.RLDESCRI,space(40))
      this.w_UMTDEF = NVL(_Link_.RLUMTDEF,space(5))
      this.w_QTARIS = NVL(_Link_.RLQTARIS,0)
      this.w_RLINTEST = NVL(_Link_.RLINTEST,space(1))
      this.w_RLMAGWIP = NVL(_Link_.RLMAGWIP,space(5))
      this.w_RLTEMCOD = NVL(_Link_.RLTEMCOD,0)
    else
      if i_cCtrl<>'Load'
        this.w_RLCODICE = space(20)
      endif
      this.w_RLDESCRI = space(40)
      this.w_UMTDEF = space(5)
      this.w_QTARIS = 0
      this.w_RLINTEST = space(1)
      this.w_RLMAGWIP = space(5)
      this.w_RLTEMCOD = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!.w_RL__TIPO $ 'UP-AR-RE'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inesistente")
        endif
        this.w_RLCODICE = space(20)
        this.w_RLDESCRI = space(40)
        this.w_UMTDEF = space(5)
        this.w_QTARIS = 0
        this.w_RLINTEST = space(1)
        this.w_RLMAGWIP = space(5)
        this.w_RLTEMCOD = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLUMTRIS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLUMTRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RLUMTRIS)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_RLUMTRIS))
          select UMFLTEMP,UMCODICE,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLUMTRIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLUMTRIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oRLUMTRIS_2_11'),i_cWhere,'GSAR_AUM',"Unit� di misura tempo",'GSCO_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLUMTRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RLUMTRIS);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_RLUMTRIS)
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLUMTRIS = NVL(_Link_.UMCODICE,space(5))
      this.w_CONSEC = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_RLUMTRIS = space(5)
      endif
      this.w_CONSEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLUMTRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCLA_2_6.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_2_6.value=this.w_DESCLA
      replace t_DESCLA with this.oPgFrm.Page1.oPag.oDESCLA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_2_24.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_2_24.value=this.w_RLDESCRI
      replace t_RLDESCRI with this.oPgFrm.Page1.oPag.oRLDESCRI_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLTPREVS_2_25.value==this.w_RLTPREVS)
      this.oPgFrm.Page1.oPag.oRLTPREVS_2_25.value=this.w_RLTPREVS
      replace t_RLTPREVS with this.oPgFrm.Page1.oPag.oRLTPREVS_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLNUMIMP_2_26.value==this.w_RLNUMIMP)
      this.oPgFrm.Page1.oPag.oRLNUMIMP_2_26.value=this.w_RLNUMIMP
      replace t_RLNUMIMP with this.oPgFrm.Page1.oPag.oRLNUMIMP_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLTCONSS_2_27.value==this.w_RLTCONSS)
      this.oPgFrm.Page1.oPag.oRLTCONSS_2_27.value=this.w_RLTCONSS
      replace t_RLTCONSS with this.oPgFrm.Page1.oPag.oRLTCONSS_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLINTEST_2_28.RadioValue()==this.w_RLINTEST)
      this.oPgFrm.Page1.oPag.oRLINTEST_2_28.SetRadio()
      replace t_RLINTEST with this.oPgFrm.Page1.oPag.oRLINTEST_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLTMPCIC_2_29.value==this.w_RLTMPCIC)
      this.oPgFrm.Page1.oPag.oRLTMPCIC_2_29.value=this.w_RLTMPCIC
      replace t_RLTMPCIC with this.oPgFrm.Page1.oPag.oRLTMPCIC_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLPROORA_2_31.value==this.w_RLPROORA)
      this.oPgFrm.Page1.oPag.oRLPROORA_2_31.value=this.w_RLPROORA
      replace t_RLPROORA with this.oPgFrm.Page1.oPag.oRLPROORA_2_31.value
    endif
    if not(this.oPgFrm.Page2.oPag.oRLISTLAV_4_1.value==this.w_RLISTLAV)
      this.oPgFrm.Page2.oPag.oRLISTLAV_4_1.value=this.w_RLISTLAV
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODCLA_2_2.value==this.w_RLCODCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODCLA_2_2.value=this.w_RLCODCLA
      replace t_RLCODCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODCLA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRL__TIPO_2_4.RadioValue()==this.w_RL__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRL__TIPO_2_4.SetRadio()
      replace t_RL__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRL__TIPO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODICE_2_5.value==this.w_RLCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODICE_2_5.value=this.w_RLCODICE
      replace t_RLCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODICE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLQTARIS_2_10.value==this.w_RLQTARIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLQTARIS_2_10.value=this.w_RLQTARIS
      replace t_RLQTARIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLQTARIS_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLUMTRIS_2_11.value==this.w_RLUMTRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLUMTRIS_2_11.value=this.w_RLUMTRIS
      replace t_RLUMTRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLUMTRIS_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMRIS_2_13.value==this.w_RLTEMRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMRIS_2_13.value=this.w_RLTEMRIS
      replace t_RLTEMRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMRIS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMSEC_2_14.value==this.w_RLTEMSEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMSEC_2_14.value=this.w_RLTEMSEC
      replace t_RLTEMSEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTEMSEC_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTIPTEM_2_33.RadioValue()==this.w_RLTIPTEM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTIPTEM_2_33.SetRadio()
      replace t_RLTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTIPTEM_2_33.value
    endif
    cp_SetControlsValueExtFlds(this,'ODLMRISO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(!.w_RL__TIPO $ 'UP-AR-RE') and ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK)) and not(empty(.w_RLCODICE)) and (.w_CPROWORD<>0 and not(Empty(.w_RL__TIPO)) and not(Empty(.w_RLCODICE)) and not(Empty(.w_RLTIPTEM)) and .w_RLQTARIS > 0 and not(Empty(.w_RLUMTRIS)) and .w_RLTEMRIS > 0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLCODICE_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo risorsa non valido oppure risorsa inesistente")
        case   not(((.w_RL__TIPO<>'CL' and .w_RLQTARIS<=.w_QTARIS) or (.w_RL__TIPO='CL' and .w_RLQTARIS<=1))) and ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK)) and (.w_CPROWORD<>0 and not(Empty(.w_RL__TIPO)) and not(Empty(.w_RLCODICE)) and not(Empty(.w_RLTIPTEM)) and .w_RLQTARIS > 0 and not(Empty(.w_RLUMTRIS)) and .w_RLTEMRIS > 0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLQTARIS_2_10
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_CPROWORD<>0 and not(Empty(.w_RL__TIPO)) and not(Empty(.w_RLCODICE)) and not(Empty(.w_RLTIPTEM)) and .w_RLQTARIS > 0 and not(Empty(.w_RLUMTRIS)) and .w_RLTEMRIS > 0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RLCODCLA = this.w_RLCODCLA
    this.o_RLCODICE = this.w_RLCODICE
    this.o_RLTEMRIS = this.w_RLTEMRIS
    this.o_RLNUMIMP = this.w_RLNUMIMP
    this.o_RLTMPCIC = this.w_RLTMPCIC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and not(Empty(t_RL__TIPO)) and not(Empty(t_RLCODICE)) and not(Empty(t_RLTIPTEM)) and t_RLQTARIS > 0 and not(Empty(t_RLUMTRIS)) and t_RLTEMRIS > 0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_RLCODCLA=space(5)
      .w_RLCODCLA=space(5)
      .w_RL__TIPO=space(2)
      .w_RLCODICE=space(20)
      .w_DESCLA=space(40)
      .w_RLTCORIS=0
      .w_RLTCOEXT=0
      .w_RLCODPAD=space(20)
      .w_RLQTARIS=0
      .w_RLUMTRIS=space(5)
      .w_CONSEC=0
      .w_RLTEMRIS=0
      .w_RLTEMSEC=0
      .w_RLTPRRIU=0
      .w_RLTPRRIS=0
      .w_CLARIS=space(20)
      .w_RLTPSRIU=0
      .w_QTARIS=0
      .w_UMTDEF=space(5)
      .w_RLTPSRIS=0
      .w_RLTPSTDS=0
      .w_RLQPSRIS=0
      .w_RLDESCRI=space(40)
      .w_RLTPREVS=0
      .w_RLNUMIMP=0
      .w_RLTCONSS=0
      .w_RLINTEST=space(1)
      .w_RLTMPCIC=0
      .w_RLTMPSEC=0
      .w_RLPROORA=0
      .w_RLMAGWIP=space(5)
      .w_RLTIPTEM=space(1)
      .w_RL1TIPO=space(2)
      .w_RLTIPTET=space(1)
      .w_RLTEMCOD=0
      .DoRTCalc(1,7,.f.)
      if not(empty(.w_RLCODCLA))
        .link_2_2('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_RLCODCLA))
        .link_2_3('Full')
      endif
        .w_RL__TIPO = 'CL'
        .w_RLCODICE = iif(empty(.w_CLARIS),.w_RLCODICE,.w_CLARIS)
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_RLCODICE))
        .link_2_5('Full')
      endif
      .DoRTCalc(11,13,.f.)
        .w_RLCODPAD = .w_RLCODICE
      .DoRTCalc(15,16,.f.)
      if not(empty(.w_RLUMTRIS))
        .link_2_11('Full')
      endif
      .DoRTCalc(17,17,.f.)
        .w_RLTEMRIS = iif(.w_RLTMPCIC > 0 , .w_RLTMPCIC / .w_RLNUMIMP , 0)
        .w_RLTEMSEC = .w_RLTEMRIS*.w_CONSEC
        .w_RLTPRRIU = .w_RLTEMSEC*.w_RLQTARIS
        .w_RLTPRRIS = .w_RLTPRRIU*iif(.w_RLTIPTEM='L',.oParentObject.oParentObject.w_OLTQTOD1,1)
      .DoRTCalc(22,22,.f.)
        .w_RLTPSRIU = .w_RLTEMSEC*.w_RLQTARIS
      .DoRTCalc(24,25,.f.)
        .w_RLTPSRIS = .w_RLTPSRIU*.oParentObject.oParentObject.w_OLTQTOD1
        .w_RLTPSTDS = .w_RLTPRRIS
        .w_RLQPSRIS = .w_RLQTARIS
      .DoRTCalc(29,29,.f.)
        .w_RLTPREVS = .w_RLTPRRIS
        .w_RLNUMIMP = 1
        .w_RLTCONSS = .w_RLTCORIS
      .DoRTCalc(33,33,.f.)
        .w_RLTMPCIC = iif(.w_RLTEMRIS>0 , .w_RLTEMRIS * .w_RLNUMIMP, 0)
        .w_RLTMPSEC = .w_RLTMPCIC*.w_CONSEC
        .w_RLPROORA = iif(.w_RLTEMSEC=0 and .w_RLTMPSEC=0 , 0, 3600/IIF(.w_RLTMPSEC<>0 , .w_RLTMPSEC, .w_RLTEMSEC)) * IIF(.w_RLTMPSEC<>0,.w_RLNUMIMP,1)
      .DoRTCalc(37,37,.f.)
        .w_RLTIPTEM = 'L'
      .DoRTCalc(39,39,.f.)
        .w_RL1TIPO = .w_RL__TIPO
        .w_RLTIPTET = .w_RLTIPTEM
    endwith
    this.DoRTCalc(42,43,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RLCODCLA = t_RLCODCLA
    this.w_RLCODCLA = t_RLCODCLA
    this.w_RL__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRL__TIPO_2_4.RadioValue(.t.)
    this.w_RLCODICE = t_RLCODICE
    this.w_DESCLA = t_DESCLA
    this.w_RLTCORIS = t_RLTCORIS
    this.w_RLTCOEXT = t_RLTCOEXT
    this.w_RLCODPAD = t_RLCODPAD
    this.w_RLQTARIS = t_RLQTARIS
    this.w_RLUMTRIS = t_RLUMTRIS
    this.w_CONSEC = t_CONSEC
    this.w_RLTEMRIS = t_RLTEMRIS
    this.w_RLTEMSEC = t_RLTEMSEC
    this.w_RLTPRRIU = t_RLTPRRIU
    this.w_RLTPRRIS = t_RLTPRRIS
    this.w_CLARIS = t_CLARIS
    this.w_RLTPSRIU = t_RLTPSRIU
    this.w_QTARIS = t_QTARIS
    this.w_UMTDEF = t_UMTDEF
    this.w_RLTPSRIS = t_RLTPSRIS
    this.w_RLTPSTDS = t_RLTPSTDS
    this.w_RLQPSRIS = t_RLQPSRIS
    this.w_RLDESCRI = t_RLDESCRI
    this.w_RLTPREVS = t_RLTPREVS
    this.w_RLNUMIMP = t_RLNUMIMP
    this.w_RLTCONSS = t_RLTCONSS
    this.w_RLINTEST = this.oPgFrm.Page1.oPag.oRLINTEST_2_28.RadioValue(.t.)
    this.w_RLTMPCIC = t_RLTMPCIC
    this.w_RLTMPSEC = t_RLTMPSEC
    this.w_RLPROORA = t_RLPROORA
    this.w_RLMAGWIP = t_RLMAGWIP
    this.w_RLTIPTEM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTIPTEM_2_33.RadioValue(.t.)
    this.w_RL1TIPO = t_RL1TIPO
    this.w_RLTIPTET = t_RLTIPTET
    this.w_RLTEMCOD = t_RLTEMCOD
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RLCODCLA with this.w_RLCODCLA
    replace t_RLCODCLA with this.w_RLCODCLA
    replace t_RL__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRL__TIPO_2_4.ToRadio()
    replace t_RLCODICE with this.w_RLCODICE
    replace t_DESCLA with this.w_DESCLA
    replace t_RLTCORIS with this.w_RLTCORIS
    replace t_RLTCOEXT with this.w_RLTCOEXT
    replace t_RLCODPAD with this.w_RLCODPAD
    replace t_RLQTARIS with this.w_RLQTARIS
    replace t_RLUMTRIS with this.w_RLUMTRIS
    replace t_CONSEC with this.w_CONSEC
    replace t_RLTEMRIS with this.w_RLTEMRIS
    replace t_RLTEMSEC with this.w_RLTEMSEC
    replace t_RLTPRRIU with this.w_RLTPRRIU
    replace t_RLTPRRIS with this.w_RLTPRRIS
    replace t_CLARIS with this.w_CLARIS
    replace t_RLTPSRIU with this.w_RLTPSRIU
    replace t_QTARIS with this.w_QTARIS
    replace t_UMTDEF with this.w_UMTDEF
    replace t_RLTPSRIS with this.w_RLTPSRIS
    replace t_RLTPSTDS with this.w_RLTPSTDS
    replace t_RLQPSRIS with this.w_RLQPSRIS
    replace t_RLDESCRI with this.w_RLDESCRI
    replace t_RLTPREVS with this.w_RLTPREVS
    replace t_RLNUMIMP with this.w_RLNUMIMP
    replace t_RLTCONSS with this.w_RLTCONSS
    replace t_RLINTEST with this.oPgFrm.Page1.oPag.oRLINTEST_2_28.ToRadio()
    replace t_RLTMPCIC with this.w_RLTMPCIC
    replace t_RLTMPSEC with this.w_RLTMPSEC
    replace t_RLPROORA with this.w_RLPROORA
    replace t_RLMAGWIP with this.w_RLMAGWIP
    replace t_RLTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRLTIPTEM_2_33.ToRadio()
    replace t_RL1TIPO with this.w_RL1TIPO
    replace t_RLTIPTET with this.w_RLTIPTET
    replace t_RLTEMCOD with this.w_RLTEMCOD
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mrlPag1 as StdContainer
  Width  = 827
  height = 196
  stdWidth  = 827
  stdheight = 196
  resizeXpos=173
  resizeYpos=83
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="RZLAJPDTIT",left=1, top=5, width=822,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Posiz.",Field2="RLCODCLA",Label2="Classe",Field3="RL__TIPO",Label3="Tipo risorsa",Field4="RLCODICE",Label4="Risorsa",Field5="RLTIPTEM",Label5="Tipo tempo",Field6="RLQTARIS",Label6="Q.t�",Field7="RLUMTRIS",Label7="U.M.T.",Field8="RLTEMRIS",Label8="Tempo risorsa",Field9="RLTEMSEC",Label9="Tempo risorsa sec.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 66222982

  add object oStr_1_3 as StdString with uid="GCBCHWOCCZ",Visible=.t., Left=317, Top=143,;
    Alignment=1, Width=62, Height=18,;
    Caption="T. a prev."  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="IBRCMDSFHK",Visible=.t., Left=7, Top=143,;
    Alignment=1, Width=56, Height=18,;
    Caption="Classe"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EWBGXQPFBM",Visible=.t., Left=471, Top=143,;
    Alignment=1, Width=55, Height=18,;
    Caption="T. a cons."  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YKKOXWOFCH",Visible=.t., Left=5, Top=166,;
    Alignment=1, Width=58, Height=18,;
    Caption="Risorsa:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="UGWAJPCAWG",Visible=.t., Left=614, Top=142,;
    Alignment=1, Width=96, Height=13,;
    Caption="Num. impronte:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="LJNEHBYSJT",Visible=.t., Left=615, Top=166,;
    Alignment=1, Width=96, Height=13,;
    Caption="Prod. oraria:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-9,top=24,;
    width=819+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-8,top=25,width=818+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CLR_DETT|RIS_ORSE|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCLA_2_6.Refresh()
      this.Parent.oRLDESCRI_2_24.Refresh()
      this.Parent.oRLTPREVS_2_25.Refresh()
      this.Parent.oRLNUMIMP_2_26.Refresh()
      this.Parent.oRLTCONSS_2_27.Refresh()
      this.Parent.oRLINTEST_2_28.Refresh()
      this.Parent.oRLTMPCIC_2_29.Refresh()
      this.Parent.oRLPROORA_2_31.Refresh()
      * --- Area Manuale = Set Current Row
      * --- gsco_mrl
      this.Parent.oContained.NotifyEvent('SetEditableRow')
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CLR_DETT'
        oDropInto=this.oBodyCol.oRow.oRLCODCLA_2_2
      case cFile='RIS_ORSE'
        oDropInto=this.oBodyCol.oRow.oRLCODICE_2_5
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oRLUMTRIS_2_11
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESCLA_2_6 as StdTrsField with uid="UALQAOAXQO",rtseq=11,rtrep=.t.,;
    cFormVar="w_DESCLA",value=space(40),enabled=.f.,;
    HelpContextID = 255775798,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCLA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=246, Left=67, Top=142, InputMask=replicate('X',40)

  add object oRLDESCRI_2_24 as StdTrsField with uid="RURJNZQHGC",rtseq=29,rtrep=.t.,;
    cFormVar="w_RLDESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 240128929,;
    cTotal="", bFixedPos=.t., cQueryName = "RLDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=246, Left=67, Top=165, InputMask=replicate('X',40)

  add object oRLTPREVS_2_25 as StdTrsField with uid="GAFTUADHIE",rtseq=30,rtrep=.t.,;
    cFormVar="w_RLTPREVS",value=0,enabled=.f.,;
    HelpContextID = 206836631,;
    cTotal="", bFixedPos=.t., cQueryName = "RLTPREVS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=384, Top=142, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oRLNUMIMP_2_26 as StdTrsField with uid="UJDMQNUYIR",rtseq=31,rtrep=.t.,;
    cFormVar="w_RLNUMIMP",value=0,;
    ToolTipText = "Numero impronte dell'attrezzatura",;
    HelpContextID = 123767910,;
    cTotal="", bFixedPos=.t., cQueryName = "RLNUMIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=32, Left=714, Top=141, cSayPict=["@Z 9999"], cGetPict=["9999"]

  func oRLNUMIMP_2_26.mCond()
    with this.Parent.oContained
      return (.w_RLQTARIS>0 and not empty(.w_RLUMTRIS) AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  add object oRLTCONSS_2_27 as StdTrsField with uid="OLJITDGLBF",rtseq=32,rtrep=.t.,;
    cFormVar="w_RLTCONSS",value=0,enabled=.f.,;
    HelpContextID = 59839383,;
    cTotal="", bFixedPos=.t., cQueryName = "RLTCONSS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=528, Top=142, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oRLINTEST_2_28 as StdTrsCheck with uid="PDUQVLZNNN",rtrep=.t.,;
    cFormVar="w_RLINTEST", enabled=.f., caption="Centro esterno",;
    HelpContextID = 204915606,;
    Left=384, Top=165,;
    cTotal="", cQueryName = "RLINTEST",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , DisabledBackColor=rgb(255,255,255);
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oRLINTEST_2_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RLINTEST,&i_cF..t_RLINTEST),this.value)
    return(iif(xVal =1,'E',;
    'I'))
  endfunc
  func oRLINTEST_2_28.GetRadio()
    this.Parent.oContained.w_RLINTEST = this.RadioValue()
    return .t.
  endfunc

  func oRLINTEST_2_28.ToRadio()
    this.Parent.oContained.w_RLINTEST=trim(this.Parent.oContained.w_RLINTEST)
    return(;
      iif(this.Parent.oContained.w_RLINTEST=='E',1,;
      0))
  endfunc

  func oRLINTEST_2_28.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oRLTMPCIC_2_29 as StdTrsField with uid="RGDDLKHAPS",rtseq=34,rtrep=.t.,;
    cFormVar="w_RLTMPCIC",value=0,;
    ToolTipText = "Tempo di battuta",;
    HelpContextID = 25750617,;
    cTotal="", bFixedPos=.t., cQueryName = "RLTMPCIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=74, Left=748, Top=141

  func oRLTMPCIC_2_29.mCond()
    with this.Parent.oContained
      return (.w_RLQTARIS>0 and not empty(.w_RLUMTRIS) AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  add object oRLPROORA_2_31 as StdTrsField with uid="NUQRJHKBPH",rtseq=36,rtrep=.t.,;
    cFormVar="w_RLPROORA",value=0,enabled=.f.,;
    ToolTipText = "Produzione oraria della risorsa",;
    HelpContextID = 42095529,;
    cTotal="", bFixedPos=.t., cQueryName = "RLPROORA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=107, Left=714, Top=165, cSayPict=["@Z 9999999999.9999"], cGetPict=["@Z 9999999999.9999"]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsco_mrlPag2 as StdContainer
    Width  = 827
    height = 196
    stdWidth  = 827
    stdheight = 196
  resizeXpos=433
  resizeYpos=100
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRLISTLAV_4_1 as StdMemo with uid="MGRYOLDDRM",rtseq=43,rtrep=.f.,;
    cFormVar = "w_RLISTLAV", cQueryName = "RLISTLAV",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Istruzioni di lavorazione della fase",;
    HelpContextID = 87147412,;
   bGlobalFont=.t.,;
    Height=182, Width=814, Left=5, Top=6
enddefine

* --- Defining Body row
define class tgsco_mrlBodyRow as CPBodyRowCnt
  Width=809
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="TYPMNYNACZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 33894550,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=46, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oRLCODCLA_2_2 as StdTrsField with uid="MRBTJIDYNA",rtseq=7,rtrep=.t.,;
    cFormVar="w_RLCODCLA",value=space(5),;
    ToolTipText = "Classe risorsa",;
    HelpContextID = 13229143,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=47, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLR_DETT", oKey_1_1="CSFLAPRE", oKey_1_2="this.w_CSFLAPRE", oKey_2_1="CSCODCLA", oKey_2_2="this.w_RLCODCLA"

  func oRLCODCLA_2_2.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRLCODCLA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCODCLA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRLCODCLA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLR_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStrODBC(this.Parent.oContained.w_CSFLAPRE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStr(this.Parent.oContained.w_CSFLAPRE)
    endif
    do cp_zoom with 'CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(this.parent,'oRLCODCLA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCI_MCS.CLR_DETT_VZM',this.parent.oContained
  endproc

  add object oRL__TIPO_2_4 as stdTrsTableCombo with uid="HAVEZSUYDN",rtrep=.t.,;
    cFormVar="w_RL__TIPO", tablefilter="" , ;
    HelpContextID = 136602523,;
    Height=22, Width=112, Left=113, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , cDescEmptyElement='Seleziona il tipo';
, bIsInHeader=.f.;
    , cTable='..\PRFA\EXE\QUERY\GSCI_QLT.vqr',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.



  func oRL__TIPO_2_4.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK) and empty(nvl(.w_RLCODCLA,' ')))
    endwith
  endfunc

  func oRL__TIPO_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_RLCODICE)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oRLCODICE_2_5 as StdTrsField with uid="SYMJOZRMZL",rtseq=10,rtrep=.t.,;
    cFormVar="w_RLCODICE",value=space(20),;
    HelpContextID = 154543013,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inesistente",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=228, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_RL__TIPO", oKey_2_1="RLCODICE", oKey_2_2="this.w_RLCODICE"

  func oRLCODICE_2_5.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRLCODICE_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCODICE_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRLCODICE_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_RL__TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_RL__TIPO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oRLCODICE_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"RISORSE",'GSCO_ZLR.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oRLQTARIS_2_10 as StdTrsField with uid="JSATEIPKTK",rtseq=15,rtrep=.t.,;
    cFormVar="w_RLQTARIS",value=0,;
    HelpContextID = 262126697,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=33, Left=519, Top=0, cSayPict=["@Z 999.99"], cGetPict=["99999.99"]

  func oRLQTARIS_2_10.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRLQTARIS_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_RL__TIPO<>'CL' and .w_RLQTARIS<=.w_QTARIS) or (.w_RL__TIPO='CL' and .w_RLQTARIS<=1)))
    endwith
    return bRes
  endfunc

  add object oRLUMTRIS_2_11 as StdTrsField with uid="ZEILBJIQDR",rtseq=16,rtrep=.t.,;
    cFormVar="w_RLUMTRIS",value=space(5),;
    HelpContextID = 13171817,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=48, Left=558, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_RLUMTRIS"

  proc oRLUMTRIS_2_11.mDefault
    with this.Parent.oContained
      if empty(.w_RLUMTRIS)
        .w_RLUMTRIS = .w_UMTDEF
      endif
    endwith
  endproc

  func oRLUMTRIS_2_11.mCond()
    with this.Parent.oContained
      return (.w_RLQTARIS>0 AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRLUMTRIS_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLUMTRIS_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRLUMTRIS_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oRLUMTRIS_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura tempo",'GSCO_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oRLUMTRIS_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UMFLTEMP=w_UMFLTEMP
     i_obj.w_UMCODICE=this.parent.oContained.w_RLUMTRIS
    i_obj.ecpSave()
  endproc

  add object oRLTEMRIS_2_13 as StdTrsField with uid="QQJNIFPZKS",rtseq=18,rtrep=.t.,;
    cFormVar="w_RLTEMRIS",value=0,;
    HelpContextID = 5303401,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=609, Top=0, cSayPict=["@Z 99,999.999"], cGetPict=["@Z 99999.999"]

  func oRLTEMRIS_2_13.mCond()
    with this.Parent.oContained
      return (.w_RLQTARIS>0 and not empty(.w_RLUMTRIS) AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  add object oRLTEMSEC_2_14 as StdTrsField with uid="YPBYPFNLSU",rtseq=19,rtrep=.t.,;
    cFormVar="w_RLTEMSEC",value=0,enabled=.f.,;
    HelpContextID = 246354855,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=107, Left=697, Top=0, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oRLTIPTEM_2_33 as StdTrsCombo with uid="XCVGKBHHNF",rtrep=.t.,;
    cFormVar="w_RLTIPTEM", RowSource=""+"Lavorazione,"+"Setup,"+"Warm Up" , ;
    ToolTipText = "Tipo di tempo della risorsa",;
    HelpContextID = 226169757,;
    Height=21, Width=127, Left=388, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRLTIPTEM_2_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RLTIPTEM,&i_cF..t_RLTIPTEM),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'S',;
    iif(xVal =3,'W',;
    space(1)))))
  endfunc
  func oRLTIPTEM_2_33.GetRadio()
    this.Parent.oContained.w_RLTIPTEM = this.RadioValue()
    return .t.
  endfunc

  func oRLTIPTEM_2_33.ToRadio()
    this.Parent.oContained.w_RLTIPTEM=trim(this.Parent.oContained.w_RLTIPTEM)
    return(;
      iif(this.Parent.oContained.w_RLTIPTEM=='L',1,;
      iif(this.Parent.oContained.w_RLTIPTEM=='S',2,;
      iif(this.Parent.oContained.w_RLTIPTEM=='W',3,;
      0))))
  endfunc

  func oRLTIPTEM_2_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRLTIPTEM_2_33.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mrl','ODLMRISO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RLCODODL=ODLMRISO.RLCODODL";
  +" and "+i_cAliasName2+".RLROWNUM=ODLMRISO.RLROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
