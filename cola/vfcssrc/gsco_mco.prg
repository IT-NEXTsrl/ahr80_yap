* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mco                                                        *
*              Componenti matricole                                            *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_341]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-03                                                      *
* Last revis.: 2015-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mco")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mco")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mco")
  return

* --- Class definition
define class tgsco_mco as StdPCForm
  Width  = 502
  Height = 423
  Top    = 10
  Left   = 10
  cComment = "Componenti matricole"
  cPrg = "gsco_mco"
  HelpContextID=171334295
  add object cnt as tcgsco_mco
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mco as PCContext
  w_MTSERIAL = space(15)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_CPROWORD = 0
  w_MTKEYSAL = space(20)
  w_MTROWODL = 0
  w_MTCODRIC = space(20)
  w_CODART = space(20)
  w_DESART = space(40)
  w_FLLOTT = space(1)
  w_MTMAGCAR = space(5)
  w_MTMAGSCA = space(5)
  w_MTFLSCAR = space(1)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_FLRISE = space(1)
  w_MOVMAT = space(1)
  w_DATREG = space(5)
  w_FLPRG = space(1)
  w_MTUNIMIS = space(3)
  w_MTCOEIMP = 0
  w_MTQTAMOV = 0
  w_RIGMOVLOT = space(1)
  w_MTCARI = space(1)
  w_PPCODICE = space(2)
  w_PPCAUSCA = space(5)
  w_CODLOTTES = space(20)
  w_CODUBITES = space(20)
  w_CODUBI = space(20)
  w_MTCODCOM = space(15)
  proc Save(i_oFrom)
    this.w_MTSERIAL = i_oFrom.w_MTSERIAL
    this.w_MTROWNUM = i_oFrom.w_MTROWNUM
    this.w_MTNUMRIF = i_oFrom.w_MTNUMRIF
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MTKEYSAL = i_oFrom.w_MTKEYSAL
    this.w_MTROWODL = i_oFrom.w_MTROWODL
    this.w_MTCODRIC = i_oFrom.w_MTCODRIC
    this.w_CODART = i_oFrom.w_CODART
    this.w_DESART = i_oFrom.w_DESART
    this.w_FLLOTT = i_oFrom.w_FLLOTT
    this.w_MTMAGCAR = i_oFrom.w_MTMAGCAR
    this.w_MTMAGSCA = i_oFrom.w_MTMAGSCA
    this.w_MTFLSCAR = i_oFrom.w_MTFLSCAR
    this.w_MTFLCARI = i_oFrom.w_MTFLCARI
    this.w_CODMAG = i_oFrom.w_CODMAG
    this.w_ESIRIS = i_oFrom.w_ESIRIS
    this.w_FLRISE = i_oFrom.w_FLRISE
    this.w_MOVMAT = i_oFrom.w_MOVMAT
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_FLPRG = i_oFrom.w_FLPRG
    this.w_MTUNIMIS = i_oFrom.w_MTUNIMIS
    this.w_MTCOEIMP = i_oFrom.w_MTCOEIMP
    this.w_MTQTAMOV = i_oFrom.w_MTQTAMOV
    this.w_RIGMOVLOT = i_oFrom.w_RIGMOVLOT
    this.w_MTCARI = i_oFrom.w_MTCARI
    this.w_PPCODICE = i_oFrom.w_PPCODICE
    this.w_PPCAUSCA = i_oFrom.w_PPCAUSCA
    this.w_CODLOTTES = i_oFrom.w_CODLOTTES
    this.w_CODUBITES = i_oFrom.w_CODUBITES
    this.w_CODUBI = i_oFrom.w_CODUBI
    this.w_MTCODCOM = i_oFrom.w_MTCODCOM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MTSERIAL = this.w_MTSERIAL
    i_oTo.w_MTROWNUM = this.w_MTROWNUM
    i_oTo.w_MTNUMRIF = this.w_MTNUMRIF
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MTKEYSAL = this.w_MTKEYSAL
    i_oTo.w_MTROWODL = this.w_MTROWODL
    i_oTo.w_MTCODRIC = this.w_MTCODRIC
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_FLLOTT = this.w_FLLOTT
    i_oTo.w_MTMAGCAR = this.w_MTMAGCAR
    i_oTo.w_MTMAGSCA = this.w_MTMAGSCA
    i_oTo.w_MTFLSCAR = this.w_MTFLSCAR
    i_oTo.w_MTFLCARI = this.w_MTFLCARI
    i_oTo.w_CODMAG = this.w_CODMAG
    i_oTo.w_ESIRIS = this.w_ESIRIS
    i_oTo.w_FLRISE = this.w_FLRISE
    i_oTo.w_MOVMAT = this.w_MOVMAT
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_FLPRG = this.w_FLPRG
    i_oTo.w_MTUNIMIS = this.w_MTUNIMIS
    i_oTo.w_MTCOEIMP = this.w_MTCOEIMP
    i_oTo.w_MTQTAMOV = this.w_MTQTAMOV
    i_oTo.w_RIGMOVLOT = this.w_RIGMOVLOT
    i_oTo.w_MTCARI = this.w_MTCARI
    i_oTo.w_PPCODICE = this.w_PPCODICE
    i_oTo.w_PPCAUSCA = this.w_PPCAUSCA
    i_oTo.w_CODLOTTES = this.w_CODLOTTES
    i_oTo.w_CODUBITES = this.w_CODUBITES
    i_oTo.w_CODUBI = this.w_CODUBI
    i_oTo.w_MTCODCOM = this.w_MTCODCOM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mco as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 502
  Height = 423
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-16"
  HelpContextID=171334295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DIC_COMP_IDX = 0
  MATRICOL_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  PAR_PROD_IDX = 0
  CAM_AGAZ_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "DIC_COMP"
  cKeySelect = "MTSERIAL,MTROWNUM"
  cKeyWhere  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM"
  cKeyDetail  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM and MTKEYSAL=this.w_MTKEYSAL and MTROWODL=this.w_MTROWODL"
  cKeyWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';

  cKeyDetailWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)';
      +'+" and MTROWODL="+cp_ToStrODBC(this.w_MTROWODL)';

  cKeyWhereODBCqualified = '"DIC_COMP.MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and DIC_COMP.MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DIC_COMP.CPROWORD'
  cPrg = "gsco_mco"
  cComment = "Componenti matricole"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MTSERIAL = space(15)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_CPROWORD = 0
  w_MTKEYSAL = space(20)
  w_MTROWODL = 0
  w_MTCODRIC = space(20)
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_FLLOTT = space(1)
  w_MTMAGCAR = space(5)
  w_MTMAGSCA = space(5)
  w_MTFLSCAR = space(1)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_FLRISE = space(1)
  w_MOVMAT = .F.
  w_DATREG = space(5)
  w_FLPRG = space(1)
  w_MTUNIMIS = space(3)
  w_MTCOEIMP = 0
  w_MTQTAMOV = 0
  w_RIGMOVLOT = .F.
  w_MTCARI = space(1)
  w_PPCODICE = space(2)
  w_PPCAUSCA = space(5)
  w_CODLOTTES = space(20)
  o_CODLOTTES = space(20)
  w_CODUBITES = space(20)
  o_CODUBITES = space(20)
  w_CODUBI = space(20)
  o_CODUBI = space(20)
  w_MTCODCOM = space(15)

  * --- Children pointers
  GSCO_MCM = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_mco
  PROC F6()
     * --- disabilita F6
  EndProc
  
  Proc ecpQuit()
     this.ecpSave()
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCO_MCM additive
    with this
      .Pages(1).addobject("oPag","tgsco_mcoPag1","gsco_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCO_MCM
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='MATRICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='UBICAZIO'
    this.cWorkTables[4]='PAR_PROD'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='DIC_COMP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_COMP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_COMP_IDX,3]
  return

  function CreateChildren()
    this.GSCO_MCM = CREATEOBJECT('stdDynamicChild',this,'GSCO_MCM',this.oPgFrm.Page1.oPag.oLinkPC_2_13)
    this.GSCO_MCM.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsco_mco'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCO_MCM)
      this.GSCO_MCM.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCO_MCM.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCO_MCM.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCO_MCM)
      this.GSCO_MCM.DestroyChildrenChain()
      this.GSCO_MCM=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_13')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCO_MCM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCO_MCM.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCO_MCM.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCO_MCM.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_MTSERIAL,"MTSERIAL";
             ,.w_MTROWNUM,"MTROWNUM";
             ,.w_MTROWODL,"MTROWODL";
             ,.w_MTKEYSAL,"MTKEYSAL";
             )
      .WriteTo_GSCO_MCM()
    endwith
    select (i_cOldSel)
    return

procedure WriteTo_GSCO_MCM()
  if at('gsco_mcm',lower(this.GSCO_MCM.class))<>0
    if this.GSCO_MCM.w_CODLOTTES<>this.w_CODLOTTES or this.GSCO_MCM.w_CODUBITES<>this.w_CODUBITES or this.GSCO_MCM.w_CODUBI<>this.w_CODUBI
      this.GSCO_MCM.w_CODLOTTES = this.w_CODLOTTES
      this.GSCO_MCM.w_CODUBITES = this.w_CODUBITES
      this.GSCO_MCM.w_CODUBI = this.w_CODUBI
      this.GSCO_MCM.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DIC_COMP where MTSERIAL=KeySet.MTSERIAL
    *                            and MTROWNUM=KeySet.MTROWNUM
    *                            and MTKEYSAL=KeySet.MTKEYSAL
    *                            and MTROWODL=KeySet.MTROWODL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_COMP_IDX,2],this.bLoadRecFilter,this.DIC_COMP_IDX,"gsco_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_COMP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_COMP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_COMP '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MTSERIAL',this.w_MTSERIAL  ,'MTROWNUM',this.w_MTROWNUM  )
      select * from (i_cTable) DIC_COMP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODMAG = space(5)
        .w_ESIRIS = space(1)
        .w_FLRISE = space(1)
        .w_MOVMAT = .t.
        .w_DATREG = space(5)
        .w_FLPRG = space(1)
        .w_RIGMOVLOT = .f.
        .w_MTCARI = space(1)
        .w_PPCODICE = "PP"
        .w_PPCAUSCA = space(5)
        .w_CODLOTTES = space(20)
        .w_CODUBITES = space(20)
        .w_CODUBI = space(20)
        .w_MTSERIAL = NVL(MTSERIAL,space(15))
        .w_MTROWNUM = NVL(MTROWNUM,0)
        .w_MTNUMRIF = NVL(MTNUMRIF,0)
        .w_MTFLSCAR = NVL(MTFLSCAR,space(1))
        .w_MTFLCARI = NVL(MTFLCARI,space(1))
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
          .link_1_15('Load')
          .link_1_16('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DIC_COMP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESART = space(40)
          .w_FLLOTT = space(1)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MTKEYSAL = NVL(MTKEYSAL,space(20))
          .w_MTROWODL = NVL(MTROWODL,0)
          .w_MTCODRIC = NVL(MTCODRIC,space(20))
          if link_2_4_joined
            this.w_MTCODRIC = NVL(CACODICE204,NVL(this.w_MTCODRIC,space(20)))
            this.w_CODART = NVL(CACODART204,space(20))
          else
          .link_2_4('Load')
          endif
        .w_CODART = .w_CODART
          .link_2_5('Load')
          .w_MTMAGCAR = NVL(MTMAGCAR,space(5))
          .w_MTMAGSCA = NVL(MTMAGSCA,space(5))
          .w_MTUNIMIS = NVL(MTUNIMIS,space(3))
          .w_MTCOEIMP = NVL(MTCOEIMP,0)
          .w_MTQTAMOV = NVL(MTQTAMOV,0)
          .w_MTCODCOM = NVL(MTCODCOM,space(15))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace MTKEYSAL with .w_MTKEYSAL
          replace MTROWODL with .w_MTROWODL
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MTSERIAL=space(15)
      .w_MTROWNUM=0
      .w_MTNUMRIF=0
      .w_CPROWORD=10
      .w_MTKEYSAL=space(20)
      .w_MTROWODL=0
      .w_MTCODRIC=space(20)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_FLLOTT=space(1)
      .w_MTMAGCAR=space(5)
      .w_MTMAGSCA=space(5)
      .w_MTFLSCAR=space(1)
      .w_MTFLCARI=space(1)
      .w_CODMAG=space(5)
      .w_ESIRIS=space(1)
      .w_FLRISE=space(1)
      .w_MOVMAT=.f.
      .w_DATREG=space(5)
      .w_FLPRG=space(1)
      .w_MTUNIMIS=space(3)
      .w_MTCOEIMP=0
      .w_MTQTAMOV=0
      .w_RIGMOVLOT=.f.
      .w_MTCARI=space(1)
      .w_PPCODICE=space(2)
      .w_PPCAUSCA=space(5)
      .w_CODLOTTES=space(20)
      .w_CODUBITES=space(20)
      .w_CODUBI=space(20)
      .w_MTCODCOM=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,7,.f.)
        if not(empty(.w_MTCODRIC))
         .link_2_4('Full')
        endif
        .w_CODART = .w_CODART
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODART))
         .link_2_5('Full')
        endif
        .DoRTCalc(9,17,.f.)
        .w_MOVMAT = .t.
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(19,25,.f.)
        .w_PPCODICE = "PP"
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_PPCODICE))
         .link_1_15('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_PPCAUSCA))
         .link_1_16('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_COMP')
    this.DoRTCalc(28,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSCO_MCM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIC_COMP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCO_MCM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTSERIAL,"MTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTROWNUM,"MTROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTNUMRIF,"MTNUMRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTFLSCAR,"MTFLSCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTFLCARI,"MTFLCARI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MTCODRIC C(20);
      ,t_MTUNIMIS C(3);
      ,t_MTQTAMOV N(12,3);
      ,MTKEYSAL C(20);
      ,MTROWODL N(4);
      ,t_MTKEYSAL C(20);
      ,t_MTROWODL N(4);
      ,t_CODART C(20);
      ,t_DESART C(40);
      ,t_FLLOTT C(1);
      ,t_MTMAGCAR C(5);
      ,t_MTMAGSCA C(5);
      ,t_MTCOEIMP N(12,5);
      ,t_MTCODCOM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODRIC_2_4.controlsource=this.cTrsName+'.t_MTCODRIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTUNIMIS_2_10.controlsource=this.cTrsName+'.t_MTUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTQTAMOV_2_12.controlsource=this.cTrsName+'.t_MTQTAMOV'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(52)
    this.AddVLine(325)
    this.AddVLine(371)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_COMP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_COMP_IDX,2])
      *
      * insert into DIC_COMP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_COMP')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_COMP')
        i_cFldBody=" "+;
                  "(MTSERIAL,MTROWNUM,MTNUMRIF,CPROWORD,MTKEYSAL"+;
                  ",MTROWODL,MTCODRIC,MTMAGCAR,MTMAGSCA,MTFLSCAR"+;
                  ",MTFLCARI,MTUNIMIS,MTCOEIMP,MTQTAMOV,MTCODCOM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MTSERIAL)+","+cp_ToStrODBC(this.w_MTROWNUM)+","+cp_ToStrODBC(this.w_MTNUMRIF)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MTKEYSAL)+;
             ","+cp_ToStrODBC(this.w_MTROWODL)+","+cp_ToStrODBCNull(this.w_MTCODRIC)+","+cp_ToStrODBC(this.w_MTMAGCAR)+","+cp_ToStrODBC(this.w_MTMAGSCA)+","+cp_ToStrODBC(this.w_MTFLSCAR)+;
             ","+cp_ToStrODBC(this.w_MTFLCARI)+","+cp_ToStrODBC(this.w_MTUNIMIS)+","+cp_ToStrODBC(this.w_MTCOEIMP)+","+cp_ToStrODBC(this.w_MTQTAMOV)+","+cp_ToStrODBC(this.w_MTCODCOM)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_COMP')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_COMP')
        cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MTSERIAL,'MTROWNUM',this.w_MTROWNUM,'MTKEYSAL',this.w_MTKEYSAL,'MTROWODL',this.w_MTROWODL)
        INSERT INTO (i_cTable) (;
                   MTSERIAL;
                  ,MTROWNUM;
                  ,MTNUMRIF;
                  ,CPROWORD;
                  ,MTKEYSAL;
                  ,MTROWODL;
                  ,MTCODRIC;
                  ,MTMAGCAR;
                  ,MTMAGSCA;
                  ,MTFLSCAR;
                  ,MTFLCARI;
                  ,MTUNIMIS;
                  ,MTCOEIMP;
                  ,MTQTAMOV;
                  ,MTCODCOM;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MTSERIAL;
                  ,this.w_MTROWNUM;
                  ,this.w_MTNUMRIF;
                  ,this.w_CPROWORD;
                  ,this.w_MTKEYSAL;
                  ,this.w_MTROWODL;
                  ,this.w_MTCODRIC;
                  ,this.w_MTMAGCAR;
                  ,this.w_MTMAGSCA;
                  ,this.w_MTFLSCAR;
                  ,this.w_MTFLCARI;
                  ,this.w_MTUNIMIS;
                  ,this.w_MTCOEIMP;
                  ,this.w_MTQTAMOV;
                  ,this.w_MTCODCOM;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_COMP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not empty(t_MTKEYSAL)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_COMP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)+;
                 ",MTFLSCAR="+cp_ToStrODBC(this.w_MTFLSCAR)+;
                 ",MTFLCARI="+cp_ToStrODBC(this.w_MTFLCARI)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                 " and MTROWODL="+cp_ToStrODBC(&i_TN.->MTROWODL)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_COMP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  MTNUMRIF=this.w_MTNUMRIF;
                 ,MTFLSCAR=this.w_MTFLSCAR;
                 ,MTFLCARI=this.w_MTFLCARI;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                      and MTROWODL=&i_TN.->MTROWODL;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not empty(t_MTKEYSAL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSCO_MCM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_MTSERIAL,"MTSERIAL";
                     ,this.w_MTROWNUM,"MTROWNUM";
                     ,this.w_MTROWODL,"MTROWODL";
                     ,this.w_MTKEYSAL,"MTKEYSAL";
                     )
              this.GSCO_MCM.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTROWODL="+cp_ToStrODBC(&i_TN.->MTROWODL)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MTKEYSAL=&i_TN.->MTKEYSAL;
                            and MTROWODL=&i_TN.->MTROWODL;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MTKEYSAL with this.w_MTKEYSAL
              replace MTROWODL with this.w_MTROWODL
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DIC_COMP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_COMP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MTCODRIC="+cp_ToStrODBCNull(this.w_MTCODRIC)+;
                     ",MTMAGCAR="+cp_ToStrODBC(this.w_MTMAGCAR)+;
                     ",MTMAGSCA="+cp_ToStrODBC(this.w_MTMAGSCA)+;
                     ",MTFLSCAR="+cp_ToStrODBC(this.w_MTFLSCAR)+;
                     ",MTFLCARI="+cp_ToStrODBC(this.w_MTFLCARI)+;
                     ",MTUNIMIS="+cp_ToStrODBC(this.w_MTUNIMIS)+;
                     ",MTCOEIMP="+cp_ToStrODBC(this.w_MTCOEIMP)+;
                     ",MTQTAMOV="+cp_ToStrODBC(this.w_MTQTAMOV)+;
                     ",MTCODCOM="+cp_ToStrODBC(this.w_MTCODCOM)+;
                     ",MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)+;
                     ",MTROWODL="+cp_ToStrODBC(this.w_MTROWODL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MTKEYSAL="+cp_ToStrODBC(MTKEYSAL)+;
                             " and MTROWODL="+cp_ToStrODBC(MTROWODL)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_COMP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MTNUMRIF=this.w_MTNUMRIF;
                     ,CPROWORD=this.w_CPROWORD;
                     ,MTCODRIC=this.w_MTCODRIC;
                     ,MTMAGCAR=this.w_MTMAGCAR;
                     ,MTMAGSCA=this.w_MTMAGSCA;
                     ,MTFLSCAR=this.w_MTFLSCAR;
                     ,MTFLCARI=this.w_MTFLCARI;
                     ,MTUNIMIS=this.w_MTUNIMIS;
                     ,MTCOEIMP=this.w_MTCOEIMP;
                     ,MTQTAMOV=this.w_MTQTAMOV;
                     ,MTCODCOM=this.w_MTCODCOM;
                     ,MTKEYSAL=this.w_MTKEYSAL;
                     ,MTROWODL=this.w_MTROWODL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                                      and MTROWODL=&i_TN.->MTROWODL;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not empty(t_MTKEYSAL))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSCO_MCM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MTSERIAL,"MTSERIAL";
               ,this.w_MTROWNUM,"MTROWNUM";
               ,this.w_MTROWODL,"MTROWODL";
               ,this.w_MTKEYSAL,"MTKEYSAL";
               )
          this.GSCO_MCM.mReplace()
          this.GSCO_MCM.bSaveContext=.f.
        endif
      endscan
     this.GSCO_MCM.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_COMP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not empty(t_MTKEYSAL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCO_MCM : Deleting
        this.GSCO_MCM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MTSERIAL,"MTSERIAL";
               ,this.w_MTROWNUM,"MTROWNUM";
               ,this.w_MTROWODL,"MTROWODL";
               ,this.w_MTKEYSAL,"MTKEYSAL";
               )
        this.GSCO_MCM.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DIC_COMP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTROWODL="+cp_ToStrODBC(&i_TN.->MTROWODL)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MTKEYSAL=&i_TN.->MTKEYSAL;
                              and MTROWODL=&i_TN.->MTROWODL;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not empty(t_MTKEYSAL)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_COMP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .link_2_4('Full')
          .w_CODART = .w_CODART
          .link_2_5('Full')
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        if  .o_CODLOTTES<>.w_CODLOTTES.or. .o_CODUBITES<>.w_CODUBITES.or. .o_CODUBI<>.w_CODUBI
          .WriteTo_GSCO_MCM()
        endif
        .DoRTCalc(9,25,.t.)
          .link_1_15('Full')
          .link_1_16('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MTKEYSAL with this.w_MTKEYSAL
      replace t_MTROWODL with this.w_MTROWODL
      replace t_CODART with this.w_CODART
      replace t_DESART with this.w_DESART
      replace t_FLLOTT with this.w_FLLOTT
      replace t_MTMAGCAR with this.w_MTMAGCAR
      replace t_MTMAGSCA with this.w_MTMAGSCA
      replace t_MTCOEIMP with this.w_MTCOEIMP
      replace t_MTCODCOM with this.w_MTCODCOM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.GSCO_MCM.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MTCODRIC
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MTCODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MTCODRIC)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODRIC = NVL(_Link_.CACODICE,space(20))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODRIC = space(20)
      endif
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CACODICE as CACODICE204"+ ",link_2_4.CACODART as CACODART204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DIC_COMP.MTCODRIC=link_2_4.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DIC_COMP.MTCODRIC=link_2_4.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_FLLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCODICE
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCAUSCA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPCAUSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_PPCAUSCA = NVL(_Link_.PPCAUSCA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_PPCAUSCA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUSCA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMMTCARI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PPCAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PPCAUSCA)
            select CMCODICE,CMMTCARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUSCA = NVL(_Link_.CMCODICE,space(5))
      this.w_MTCARI = NVL(_Link_.CMMTCARI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUSCA = space(5)
      endif
      this.w_MTCARI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODRIC_2_4.value==this.w_MTCODRIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODRIC_2_4.value=this.w_MTCODRIC
      replace t_MTCODRIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODRIC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTUNIMIS_2_10.value==this.w_MTUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTUNIMIS_2_10.value=this.w_MTUNIMIS
      replace t_MTUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTUNIMIS_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTQTAMOV_2_12.value==this.w_MTQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTQTAMOV_2_12.value=this.w_MTQTAMOV
      replace t_MTQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTQTAMOV_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'DIC_COMP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSCO_MCM.CheckForm()
      if not empty(.w_MTKEYSAL)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_CODLOTTES = this.w_CODLOTTES
    this.o_CODUBITES = this.w_CODUBITES
    this.o_CODUBI = this.w_CODUBI
    * --- GSCO_MCM : Depends On
    this.GSCO_MCM.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not empty(t_MTKEYSAL))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MTKEYSAL=space(20)
      .w_MTROWODL=0
      .w_MTCODRIC=space(20)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_FLLOTT=space(1)
      .w_MTMAGCAR=space(5)
      .w_MTMAGSCA=space(5)
      .w_MTUNIMIS=space(3)
      .w_MTCOEIMP=0
      .w_MTQTAMOV=0
      .w_MTCODCOM=space(15)
      .DoRTCalc(1,7,.f.)
      if not(empty(.w_MTCODRIC))
        .link_2_4('Full')
      endif
        .w_CODART = .w_CODART
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_CODART))
        .link_2_5('Full')
      endif
    endwith
    this.DoRTCalc(9,31,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MTKEYSAL = t_MTKEYSAL
    this.w_MTROWODL = t_MTROWODL
    this.w_MTCODRIC = t_MTCODRIC
    this.w_CODART = t_CODART
    this.w_DESART = t_DESART
    this.w_FLLOTT = t_FLLOTT
    this.w_MTMAGCAR = t_MTMAGCAR
    this.w_MTMAGSCA = t_MTMAGSCA
    this.w_MTUNIMIS = t_MTUNIMIS
    this.w_MTCOEIMP = t_MTCOEIMP
    this.w_MTQTAMOV = t_MTQTAMOV
    this.w_MTCODCOM = t_MTCODCOM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MTKEYSAL with this.w_MTKEYSAL
    replace t_MTROWODL with this.w_MTROWODL
    replace t_MTCODRIC with this.w_MTCODRIC
    replace t_CODART with this.w_CODART
    replace t_DESART with this.w_DESART
    replace t_FLLOTT with this.w_FLLOTT
    replace t_MTMAGCAR with this.w_MTMAGCAR
    replace t_MTMAGSCA with this.w_MTMAGSCA
    replace t_MTUNIMIS with this.w_MTUNIMIS
    replace t_MTCOEIMP with this.w_MTCOEIMP
    replace t_MTQTAMOV with this.w_MTQTAMOV
    replace t_MTCODCOM with this.w_MTCODCOM
    if i_srv='A'
      replace MTKEYSAL with this.w_MTKEYSAL
      replace MTROWODL with this.w_MTROWODL
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mcoPag1 as StdContainer
  Width  = 498
  height = 423
  stdWidth  = 498
  stdheight = 423
  resizeXpos=268
  resizeYpos=75
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_12 as cp_runprogram with uid="URSTUWEHUQ",left=6, top=426, width=135,height=20,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('CREATEROW')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 226893133


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=6, width=477,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Posiz.",Field2="MTCODRIC",Label2="Codice articolo",Field3="MTUNIMIS",Label3="UM",Field4="MTQTAMOV",Label4="Q.ta movimentata",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49445766

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=26,;
    width=473+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=27,width=472+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_13 as stdDynamicChildContainer with uid="XIPHNDRDEO",bOnScreen=.t.,width=480,height=274,;
   left=-1, top=143;


  func oLinkPC_2_13.mCond()
    with this.Parent.oContained
      return (Not Empty( .w_MTCODRIC ))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsco_mcoBodyRow as CPBodyRowCnt
  Width=463
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="THFARSIPRZ",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17117334,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oMTCODRIC_2_4 as StdTrsField with uid="SPOHKTCFXW",rtseq=7,rtrep=.t.,;
    cFormVar="w_MTCODRIC",value=space(20),enabled=.f.,;
    HelpContextID = 13231113,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=265, Left=42, Top=0, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MTCODRIC"

  func oMTCODRIC_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMTUNIMIS_2_10 as StdTrsField with uid="XNVADGFRMD",rtseq=21,rtrep=.t.,;
    cFormVar="w_MTUNIMIS",value=space(3),enabled=.f.,;
    HelpContextID = 203031577,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=320, Top=-1, InputMask=replicate('X',3)

  add object oMTQTAMOV_2_12 as StdTrsField with uid="RQWVFHBYHF",rtseq=23,rtrep=.t.,;
    cFormVar="w_MTQTAMOV",value=0,enabled=.f.,;
    HelpContextID = 73415652,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=361, Top=-1, cSayPict=[v_pq(12)]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mco','DIC_COMP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MTSERIAL=DIC_COMP.MTSERIAL";
  +" and "+i_cAliasName2+".MTROWNUM=DIC_COMP.MTROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsco_mco
* --- Class definition
define class tgsco_mco as StdPCForm
  Width  = 501
  Height = 420
  Top    = 10
  Left   = 10
  cComment = "Componenti matricole"
  HelpContextID=171334295
  add object cnt as tcgsco_mco

  proc ecpQuit()
    if !this.cnt.bF10
      if !this.cnt.bUpdated
        this.cnt.bDontReportError=.t.
        this.TerminateEdit()
        this.cnt.bUpdated=.f.
      endif
    endif
    *this.cnt.bUpdated=iif(this.cnt.bF10,this.cnt.bUpdated,.f.)
    this.LinkPCClick()
  Endproc

enddefine
* --- Fine Area Manuale
