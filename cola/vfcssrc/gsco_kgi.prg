* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kgi                                                        *
*              Lancio ODL                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_215]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-07                                                      *
* Last revis.: 2015-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kgi",oParentObject))

* --- Class definition
define class tgsco_kgi as StdForm
  Top    = 10
  Left   = 11

  * --- Standard Properties
  Width  = 863
  Height = 534+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-11"
  HelpContextID=32089449
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=96

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  ODL_MAST_IDX = 0
  DISMBASE_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  KEY_ARTI_IDX = 0
  cpusers_IDX = 0
  SEL__MRP_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  cPrg = "gsco_kgi"
  cComment = "Lancio ODL"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_READPAR = space(2)
  w_PPTIPDOC = space(5)
  w_DESRIF = space(35)
  w_PPFLPROV = space(1)
  w_PPNUMDOC = 0
  w_PPALFDOC = space(10)
  w_PPDATDOC = ctod('  /  /  ')
  o_PPDATDOC = ctod('  /  /  ')
  w_PPDATDIV = ctod('  /  /  ')
  w_PPDATTRA = ctod('  /  /  ')
  w_PPORATRA = space(2)
  w_PPMINTRA = space(2)
  w_STADOC = space(1)
  w_PPNOTAGG = space(40)
  w_PPCHKPRE = space(1)
  w_ODLINI = space(15)
  w_ODLFIN = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_SELEZI = space(1)
  w_TIPATT = space(1)
  w_TIPCON = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATFINE = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_DISINI = space(20)
  w_DISFIN = space(20)
  w_DESDISI = space(40)
  w_DESDISF = space(40)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MAGINI = space(5)
  w_DESMAGI = space(30)
  w_MAGFIN = space(5)
  w_DESMAGF = space(30)
  w_CODCOM = space(15)
  w_DESCOM = space(30)
  w_CODATT = space(15)
  w_SERMRP = space(10)
  w_DESATT = space(30)
  w_MAGTER = space(5)
  w_DISMAG1 = space(1)
  w_FLWIP1 = space(1)
  w_DISMAG2 = space(1)
  w_FLWIP2 = space(1)
  w_PPFLVEAC = space(1)
  w_PPPRD = space(2)
  w_PPCLADOC = space(2)
  w_PPFLINTE = space(1)
  w_PPTCAMAG = space(5)
  w_PPTFRAGG = space(1)
  w_PPCAUCON = space(5)
  w_FLPPRO = space(1)
  w_PPNUMSCO = 0
  w_PPFLACCO = space(1)
  w_PPDATREG = ctod('  /  /  ')
  w_PPCODESE = space(4)
  w_PPANNPRO = space(4)
  w_PPANNDOC = space(4)
  w_GESWIP = space(1)
  w_FLPDOC = space(1)
  w_PPEMERIC = space(1)
  w_FATTODL = space(1)
  w_DTINI = ctod('  /  /  ')
  w_DTFIN = ctod('  /  /  ')
  w_TIPVER = space(1)
  w_DATVER = ctod('  /  /  ')
  w_ORAVER = space(5)
  w_OPEVER = 0
  w_DESUTE = space(20)
  w_TIPGES = space(1)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_STAMPODL = space(1)
  w_STAMPFAS = space(1)
  w_FLSTAM = space(1)
  w_CLFASEVA = space(1)
  w_FLMATINP = space(1)
  w_CODINI = space(10)
  w_CODFIN = space(10)
  w_DAFINI = ctod('  /  /  ')
  w_DAFFIN = ctod('  /  /  ')
  w_FLSUG = space(1)
  w_FLPIA = space(1)
  w_FLLAN = space(1)
  w_DALINI = ctod('  /  /  ')
  w_DALFIN = ctod('  /  /  ')
  w_ZoomSel = .NULL.
  w_OBJPRNSOL = .NULL.
  w_OBJPRNSFL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kgiPag1","gsco_kgi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsco_kgiPag2","gsco_kgi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco ordini")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPPTIPDOC_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(2).oPag.ZoomSel
    this.w_OBJPRNSOL = this.oPgFrm.Pages(2).oPag.OBJPRNSOL
    this.w_OBJPRNSFL = this.oPgFrm.Pages(2).oPag.OBJPRNSFL
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      this.w_OBJPRNSOL = .NULL.
      this.w_OBJPRNSFL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='DISMBASE'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='cpusers'
    this.cWorkTables[11]='SEL__MRP'
    this.cWorkTables[12]='FAM_ARTI'
    this.cWorkTables[13]='GRUMERC'
    this.cWorkTables[14]='CATEGOMO'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSCO_BGI(this,"AG")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_READPAR=space(2)
      .w_PPTIPDOC=space(5)
      .w_DESRIF=space(35)
      .w_PPFLPROV=space(1)
      .w_PPNUMDOC=0
      .w_PPALFDOC=space(10)
      .w_PPDATDOC=ctod("  /  /  ")
      .w_PPDATDIV=ctod("  /  /  ")
      .w_PPDATTRA=ctod("  /  /  ")
      .w_PPORATRA=space(2)
      .w_PPMINTRA=space(2)
      .w_STADOC=space(1)
      .w_PPNOTAGG=space(40)
      .w_PPCHKPRE=space(1)
      .w_ODLINI=space(15)
      .w_ODLFIN=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_TIPATT=space(1)
      .w_TIPCON=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATFINE=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_DISINI=space(20)
      .w_DISFIN=space(20)
      .w_DESDISI=space(40)
      .w_DESDISF=space(40)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_MAGINI=space(5)
      .w_DESMAGI=space(30)
      .w_MAGFIN=space(5)
      .w_DESMAGF=space(30)
      .w_CODCOM=space(15)
      .w_DESCOM=space(30)
      .w_CODATT=space(15)
      .w_SERMRP=space(10)
      .w_DESATT=space(30)
      .w_MAGTER=space(5)
      .w_DISMAG1=space(1)
      .w_FLWIP1=space(1)
      .w_DISMAG2=space(1)
      .w_FLWIP2=space(1)
      .w_PPFLVEAC=space(1)
      .w_PPPRD=space(2)
      .w_PPCLADOC=space(2)
      .w_PPFLINTE=space(1)
      .w_PPTCAMAG=space(5)
      .w_PPTFRAGG=space(1)
      .w_PPCAUCON=space(5)
      .w_FLPPRO=space(1)
      .w_PPNUMSCO=0
      .w_PPFLACCO=space(1)
      .w_PPDATREG=ctod("  /  /  ")
      .w_PPCODESE=space(4)
      .w_PPANNPRO=space(4)
      .w_PPANNDOC=space(4)
      .w_GESWIP=space(1)
      .w_FLPDOC=space(1)
      .w_PPEMERIC=space(1)
      .w_FATTODL=space(1)
      .w_DTINI=ctod("  /  /  ")
      .w_DTFIN=ctod("  /  /  ")
      .w_TIPVER=space(1)
      .w_DATVER=ctod("  /  /  ")
      .w_ORAVER=space(5)
      .w_OPEVER=0
      .w_DESUTE=space(20)
      .w_TIPGES=space(1)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_STAMPODL=space(1)
      .w_STAMPFAS=space(1)
      .w_FLSTAM=space(1)
      .w_CLFASEVA=space(1)
      .w_FLMATINP=space(1)
      .w_CODINI=space(10)
      .w_CODFIN=space(10)
      .w_DAFINI=ctod("  /  /  ")
      .w_DAFFIN=ctod("  /  /  ")
      .w_FLSUG=space(1)
      .w_FLPIA=space(1)
      .w_FLLAN=space(1)
      .w_DALINI=ctod("  /  /  ")
      .w_DALFIN=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .w_READPAR = 'PP'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READPAR))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PPTIPDOC))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_PPFLPROV = 'N'
          .DoRTCalc(6,7,.f.)
        .w_PPDATDOC = i_DATSYS
        .w_PPDATDIV = i_datsys
        .w_PPDATTRA = .w_PPDATDOC
        .w_PPORATRA = left(time(),2)
        .w_PPMINTRA = substr(time(),4,2)
          .DoRTCalc(13,14,.f.)
        .w_PPCHKPRE = ' '
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_ODLINI))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_ODLFIN))
          .link_1_17('Full')
        endif
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .w_SELEZI = "D"
      .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .w_TIPATT = 'A'
        .w_TIPCON = 'F'
        .DoRTCalc(22,27,.f.)
        if not(empty(.w_DISINI))
          .link_1_32('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_DISFIN))
          .link_1_33('Full')
        endif
        .DoRTCalc(29,31,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_37('Full')
        endif
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_38('Full')
        endif
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_GRUINI))
          .link_1_39('Full')
        endif
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_40('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CATINI))
          .link_1_41('Full')
        endif
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_CATFIN))
          .link_1_42('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_MAGINI))
          .link_1_43('Full')
        endif
        .DoRTCalc(38,39,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_46('Full')
        endif
        .DoRTCalc(40,41,.f.)
        if not(empty(.w_CODCOM))
          .link_1_49('Full')
        endif
        .DoRTCalc(42,43,.f.)
        if not(empty(.w_CODATT))
          .link_1_52('Full')
        endif
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_SERMRP))
          .link_1_53('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
          .DoRTCalc(45,60,.f.)
        .w_PPDATREG = .w_PPDATDOC
        .w_PPCODESE = g_CODESE
        .w_PPANNPRO = CALPRO(.w_PPDATREG,.w_PPCODESE,.w_FLPPRO)
        .w_PPANNDOC = STR(YEAR(.w_PPDATDOC), 4, 0)
          .DoRTCalc(65,65,.f.)
        .w_FLPDOC = 'N'
          .DoRTCalc(67,67,.f.)
        .w_FATTODL = 'T'
          .DoRTCalc(69,73,.f.)
        .w_OPEVER = .w_OPEVER
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_OPEVER))
          .link_1_106('Full')
        endif
      .oPgFrm.Page2.oPag.OBJPRNSOL.Calculate('GSCO_SOL')
          .DoRTCalc(75,82,.f.)
        .w_STAMPODL = "N"
        .w_STAMPFAS = "N"
      .oPgFrm.Page2.oPag.OBJPRNSFL.Calculate('GSCI_SSF')
        .w_FLSTAM = "O"
        .w_CLFASEVA = " "
        .w_FLMATINP = "S"
        .w_CODINI = .w_DISINI
        .w_CODFIN = .w_DISFIN
        .w_DAFINI = .w_DATFINE
        .w_DAFFIN = .w_DATFIN1
        .w_FLSUG = 'X'
        .w_FLPIA = 'X'
        .w_FLLAN = 'L'
    endwith
    this.DoRTCalc(95,96,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_READPAR = 'PP'
          .link_1_2('Full')
        endif
        .DoRTCalc(3,9,.t.)
        if .o_PPDATDOC<>.w_PPDATDOC
            .w_PPDATTRA = .w_PPDATDOC
        endif
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .DoRTCalc(11,60,.t.)
            .w_PPDATREG = .w_PPDATDOC
            .w_PPCODESE = g_CODESE
            .w_PPANNPRO = CALPRO(.w_PPDATREG,.w_PPCODESE,.w_FLPPRO)
            .w_PPANNDOC = STR(YEAR(.w_PPDATDOC), 4, 0)
        .DoRTCalc(65,73,.t.)
          .link_1_106('Full')
        .oPgFrm.Page2.oPag.OBJPRNSOL.Calculate('GSCO_SOL')
        .oPgFrm.Page2.oPag.OBJPRNSFL.Calculate('GSCI_SSF')
        .DoRTCalc(75,87,.t.)
            .w_CODINI = .w_DISINI
            .w_CODFIN = .w_DISFIN
            .w_DAFINI = .w_DATFINE
            .w_DAFFIN = .w_DATFIN1
        * --- Area Manuale = Calculate
        * --- gsco_kgi
        This.w_OBJPRNSFL.Visible = Not(g_PRFA<>"S" or g_CICLILAV <> "S")
        This.w_OBJPRNSFL.Enabled = This.w_STAMPFAS="S" and g_PRFA="S" and g_CICLILAV = "S"
        
        This.w_OBJPRNSOL.Enabled = This.w_STAMPODL="S"
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(92,96,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page2.oPag.OBJPRNSOL.Calculate('GSCO_SOL')
        .oPgFrm.Page2.oPag.OBJPRNSFL.Calculate('GSCI_SSF')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPPTIPDOC_1_3.enabled = this.oPgFrm.Page1.oPag.oPPTIPDOC_1_3.mCond()
    this.oPgFrm.Page1.oPag.oPPFLPROV_1_5.enabled = this.oPgFrm.Page1.oPag.oPPFLPROV_1_5.mCond()
    this.oPgFrm.Page1.oPag.oPPNUMDOC_1_6.enabled = this.oPgFrm.Page1.oPag.oPPNUMDOC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPPALFDOC_1_7.enabled = this.oPgFrm.Page1.oPag.oPPALFDOC_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPPDATDOC_1_8.enabled = this.oPgFrm.Page1.oPag.oPPDATDOC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPPDATDIV_1_9.enabled = this.oPgFrm.Page1.oPag.oPPDATDIV_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPPDATTRA_1_10.enabled = this.oPgFrm.Page1.oPag.oPPDATTRA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPPORATRA_1_11.enabled = this.oPgFrm.Page1.oPag.oPPORATRA_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPPMINTRA_1_12.enabled = this.oPgFrm.Page1.oPag.oPPMINTRA_1_12.mCond()
    this.oPgFrm.Page1.oPag.oSTADOC_1_13.enabled = this.oPgFrm.Page1.oPag.oSTADOC_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPPNOTAGG_1_14.enabled = this.oPgFrm.Page1.oPag.oPPNOTAGG_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPPCHKPRE_1_15.enabled = this.oPgFrm.Page1.oPag.oPPCHKPRE_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCODCOM_1_49.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_49.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_52.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_52.mCond()
    this.oPgFrm.Page2.oPag.oSTAMPFAS_2_9.enabled = this.oPgFrm.Page2.oPag.oSTAMPFAS_2_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTADOC_1_13.visible=!this.oPgFrm.Page1.oPag.oSTADOC_1_13.mHide()
    this.oPgFrm.Page1.oPag.oPPCHKPRE_1_15.visible=!this.oPgFrm.Page1.oPag.oPPCHKPRE_1_15.mHide()
    this.oPgFrm.Page2.oPag.oSTAMPFAS_2_9.visible=!this.oPgFrm.Page2.oPag.oSTAMPFAS_2_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_3.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page2.oPag.OBJPRNSOL.Event(cEvent)
      .oPgFrm.Page2.oPag.OBJPRNSFL.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCODCAU,PPGESWIP,PPOPEVER,PPELAVER,PPORAVER,PPDINVER,PPDFIVER,PPTIPVER";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPCODCAU,PPGESWIP,PPOPEVER,PPELAVER,PPORAVER,PPDINVER,PPDFIVER,PPTIPVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(2))
      this.w_PPTIPDOC = NVL(_Link_.PPCODCAU,space(5))
      this.w_GESWIP = NVL(_Link_.PPGESWIP,space(1))
      this.w_OPEVER = NVL(_Link_.PPOPEVER,0)
      this.w_DATVER = NVL(cp_ToDate(_Link_.PPELAVER),ctod("  /  /  "))
      this.w_ORAVER = NVL(_Link_.PPORAVER,space(5))
      this.w_DTINI = NVL(cp_ToDate(_Link_.PPDINVER),ctod("  /  /  "))
      this.w_DTFIN = NVL(cp_ToDate(_Link_.PPDFIVER),ctod("  /  /  "))
      this.w_TIPVER = NVL(_Link_.PPTIPVER,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(2)
      endif
      this.w_PPTIPDOC = space(5)
      this.w_GESWIP = space(1)
      this.w_OPEVER = 0
      this.w_DATVER = ctod("  /  /  ")
      this.w_ORAVER = space(5)
      this.w_DTINI = ctod("  /  /  ")
      this.w_DTFIN = ctod("  /  /  ")
      this.w_TIPVER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPTIPDOC
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDEMERIC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDEMERIC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPTIPDOC_1_3'),i_cWhere,'',"Causali documenti",'GSCO_KGI.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDEMERIC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDEMERIC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDEMERIC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDEMERIC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIF = NVL(_Link_.TDDESDOC,space(35))
      this.w_PPFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_PPEMERIC = NVL(_Link_.TDEMERIC,space(1))
      this.w_PPALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_PPPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_PPCLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_PPFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_PPFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_PPTCAMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_PPTFRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_PPCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO = NVL(_Link_.TDFLPPRO,space(1))
      this.w_PPNUMSCO = NVL(_Link_.TDNUMSCO,0)
    else
      if i_cCtrl<>'Load'
        this.w_PPTIPDOC = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_PPFLVEAC = space(1)
      this.w_PPEMERIC = space(1)
      this.w_PPALFDOC = space(10)
      this.w_PPPRD = space(2)
      this.w_PPCLADOC = space(2)
      this.w_PPFLACCO = space(1)
      this.w_PPFLINTE = space(1)
      this.w_PPTCAMAG = space(5)
      this.w_PPTFRAGG = space(1)
      this.w_PPCAUCON = space(5)
      this.w_FLPPRO = space(1)
      this.w_PPNUMSCO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GESWIP=' ' OR (.w_PPFLINTE='N' AND .w_PPCLADOC = 'DI')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_PPTIPDOC = space(5)
        this.w_DESRIF = space(35)
        this.w_PPFLVEAC = space(1)
        this.w_PPEMERIC = space(1)
        this.w_PPALFDOC = space(10)
        this.w_PPPRD = space(2)
        this.w_PPCLADOC = space(2)
        this.w_PPFLACCO = space(1)
        this.w_PPFLINTE = space(1)
        this.w_PPTCAMAG = space(5)
        this.w_PPTFRAGG = space(1)
        this.w_PPCAUCON = space(5)
        this.w_FLPPRO = space(1)
        this.w_PPNUMSCO = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLINI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLINI_1_16'),i_cWhere,'',"Elenco ordini di lavorazione",'GSCO_KGI.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ODLINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLFIN_1_17'),i_cWhere,'',"Elenco ordini di lavorazione",'GSCO_KGI.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ODLFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISINI
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DISINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DISINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDISINI_1_32'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DISINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DISINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_DISINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_DISINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISFIN
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DISFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DISFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDISFIN_1_33'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DISFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DISFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_DISFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_DISFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_37'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_38'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_39'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_40'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_41'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_42'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_43'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG1 = NVL(_Link_.MGDISMAG,space(1))
      this.w_FLWIP1 = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
      this.w_DISMAG1 = space(1)
      this.w_FLWIP1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MAGINI) OR (.w_DISMAG1='S' AND .w_FLWIP1<>'W')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o maggiore del magazzino finale.")
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
        this.w_DISMAG1 = space(1)
        this.w_FLWIP1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_46'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG2 = NVL(_Link_.MGDISMAG,space(1))
      this.w_FLWIP2 = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
      this.w_DISMAG2 = space(1)
      this.w_FLWIP2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MAGFIN) OR (.w_DISMAG2='S' AND .w_FLWIP2<>'W' AND .w_MAGINI <= .w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o minore del magazzino iniziale.")
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
        this.w_DISMAG2 = space(1)
        this.w_FLWIP2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_49'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_52'),i_cWhere,'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERMRP
  func Link_1_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEL__MRP_IDX,3]
    i_lTable = "SEL__MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2], .t., this.SEL__MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERMRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEL__MRP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMSERIAL like "+cp_ToStrODBC(trim(this.w_SERMRP)+"%");

          i_ret=cp_SQL(i_nConn,"select GMSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMSERIAL',trim(this.w_SERMRP))
          select GMSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERMRP)==trim(_Link_.GMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERMRP) and !this.bDontReportError
            deferred_cp_zoom('SEL__MRP','*','GMSERIAL',cp_AbsName(oSource.parent,'oSERMRP_1_53'),i_cWhere,'',"Archivio generazione MRP",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where GMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMSERIAL',oSource.xKey(1))
            select GMSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERMRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where GMSERIAL="+cp_ToStrODBC(this.w_SERMRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMSERIAL',this.w_SERMRP)
            select GMSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERMRP = NVL(_Link_.GMSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SERMRP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEL__MRP_IDX,2])+'\'+cp_ToStr(_Link_.GMSERIAL,1)
      cp_ShowWarn(i_cKey,this.SEL__MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERMRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPEVER
  func Link_1_106(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPEVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPEVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OPEVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OPEVER)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPEVER = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OPEVER = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPEVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPPTIPDOC_1_3.value==this.w_PPTIPDOC)
      this.oPgFrm.Page1.oPag.oPPTIPDOC_1_3.value=this.w_PPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_4.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_4.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oPPFLPROV_1_5.RadioValue()==this.w_PPFLPROV)
      this.oPgFrm.Page1.oPag.oPPFLPROV_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMDOC_1_6.value==this.w_PPNUMDOC)
      this.oPgFrm.Page1.oPag.oPPNUMDOC_1_6.value=this.w_PPNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPPALFDOC_1_7.value==this.w_PPALFDOC)
      this.oPgFrm.Page1.oPag.oPPALFDOC_1_7.value=this.w_PPALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPPDATDOC_1_8.value==this.w_PPDATDOC)
      this.oPgFrm.Page1.oPag.oPPDATDOC_1_8.value=this.w_PPDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPPDATDIV_1_9.value==this.w_PPDATDIV)
      this.oPgFrm.Page1.oPag.oPPDATDIV_1_9.value=this.w_PPDATDIV
    endif
    if not(this.oPgFrm.Page1.oPag.oPPDATTRA_1_10.value==this.w_PPDATTRA)
      this.oPgFrm.Page1.oPag.oPPDATTRA_1_10.value=this.w_PPDATTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPPORATRA_1_11.value==this.w_PPORATRA)
      this.oPgFrm.Page1.oPag.oPPORATRA_1_11.value=this.w_PPORATRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMINTRA_1_12.value==this.w_PPMINTRA)
      this.oPgFrm.Page1.oPag.oPPMINTRA_1_12.value=this.w_PPMINTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADOC_1_13.RadioValue()==this.w_STADOC)
      this.oPgFrm.Page1.oPag.oSTADOC_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNOTAGG_1_14.value==this.w_PPNOTAGG)
      this.oPgFrm.Page1.oPag.oPPNOTAGG_1_14.value=this.w_PPNOTAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCHKPRE_1_15.RadioValue()==this.w_PPCHKPRE)
      this.oPgFrm.Page1.oPag.oPPCHKPRE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oODLINI_1_16.value==this.w_ODLINI)
      this.oPgFrm.Page1.oPag.oODLINI_1_16.value=this.w_ODLINI
    endif
    if not(this.oPgFrm.Page1.oPag.oODLFIN_1_17.value==this.w_ODLFIN)
      this.oPgFrm.Page1.oPag.oODLFIN_1_17.value=this.w_ODLFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_2.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_24.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_24.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_25.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_25.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINE_1_26.value==this.w_DATFINE)
      this.oPgFrm.Page1.oPag.oDATFINE_1_26.value=this.w_DATFINE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_27.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_27.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oDISINI_1_32.value==this.w_DISINI)
      this.oPgFrm.Page1.oPag.oDISINI_1_32.value=this.w_DISINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDISFIN_1_33.value==this.w_DISFIN)
      this.oPgFrm.Page1.oPag.oDISFIN_1_33.value=this.w_DISFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISI_1_34.value==this.w_DESDISI)
      this.oPgFrm.Page1.oPag.oDESDISI_1_34.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISF_1_35.value==this.w_DESDISF)
      this.oPgFrm.Page1.oPag.oDESDISF_1_35.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_37.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_37.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_38.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_38.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_39.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_39.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_40.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_40.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_41.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_41.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_42.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_42.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_43.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_43.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_44.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_44.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_46.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_46.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_47.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_47.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_49.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_49.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_50.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_50.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_52.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_52.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSERMRP_1_53.value==this.w_SERMRP)
      this.oPgFrm.Page1.oPag.oSERMRP_1_53.value=this.w_SERMRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_54.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_54.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oFATTODL_1_92.RadioValue()==this.w_FATTODL)
      this.oPgFrm.Page1.oPag.oFATTODL_1_92.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDTINI_1_96.value==this.w_DTINI)
      this.oPgFrm.Page1.oPag.oDTINI_1_96.value=this.w_DTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDTFIN_1_97.value==this.w_DTFIN)
      this.oPgFrm.Page1.oPag.oDTFIN_1_97.value=this.w_DTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVER_1_98.RadioValue()==this.w_TIPVER)
      this.oPgFrm.Page1.oPag.oTIPVER_1_98.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVER_1_103.value==this.w_DATVER)
      this.oPgFrm.Page1.oPag.oDATVER_1_103.value=this.w_DATVER
    endif
    if not(this.oPgFrm.Page1.oPag.oORAVER_1_104.value==this.w_ORAVER)
      this.oPgFrm.Page1.oPag.oORAVER_1_104.value=this.w_ORAVER
    endif
    if not(this.oPgFrm.Page1.oPag.oOPEVER_1_106.value==this.w_OPEVER)
      this.oPgFrm.Page1.oPag.oOPEVER_1_106.value=this.w_OPEVER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_107.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_107.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_113.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_113.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_114.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_114.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_115.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_115.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_119.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_119.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_120.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_120.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_121.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_121.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oSTAMPODL_2_8.RadioValue()==this.w_STAMPODL)
      this.oPgFrm.Page2.oPag.oSTAMPODL_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSTAMPFAS_2_9.RadioValue()==this.w_STAMPFAS)
      this.oPgFrm.Page2.oPag.oSTAMPFAS_2_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PPTIPDOC)) or not(.w_GESWIP=' ' OR (.w_PPFLINTE='N' AND .w_PPCLADOC = 'DI')))  and (.w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPTIPDOC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_PPTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   (empty(.w_PPNUMDOC))  and (.w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMDOC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPDATDOC))  and (.w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPDATDOC_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PPDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPDATDIV))  and (.w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPDATDIV_1_9.SetFocus()
            i_bnoObbl = !empty(.w_PPDATDIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PPDATTRA>=.w_PPDATDOC) OR EMPTY(.w_PPDATTRA))  and (.w_PPFLACCO='S' AND .w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPDATTRA_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di trasporto non pu� precedere la data del documento")
          case   not(VAL(.w_PPORATRA)<24)  and (.w_PPFLACCO='S' AND .w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPORATRA_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora trasporto non corretta")
          case   not(VAL(.w_PPMINTRA)<60)  and (.w_PPFLACCO='S' AND .w_GESWIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPMINTRA_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti trasporto non corretti")
          case   not(.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN))  and not(empty(.w_ODLINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLINI<=.w_ODLFIN)  and not(empty(.w_ODLFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(COCHKAR(.w_DISINI))  and not(empty(.w_DISINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISINI_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(COCHKAR(.w_DISFIN))  and not(empty(.w_DISFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISFIN_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_MAGINI) OR (.w_DISMAG1='S' AND .w_FLWIP1<>'W'))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o maggiore del magazzino finale.")
          case   not(EMPTY(.w_MAGFIN) OR (.w_DISMAG2='S' AND .w_FLWIP2<>'W' AND .w_MAGINI <= .w_MAGFIN))  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o minore del magazzino iniziale.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PPDATDOC = this.w_PPDATDOC
    return

enddefine

* --- Define pages as container
define class tgsco_kgiPag1 as StdContainer
  Width  = 859
  height = 534
  stdWidth  = 859
  stdheight = 534
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPTIPDOC_1_3 as StdField with uid="HJNMKCAPBA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PPTIPDOC", cQueryName = "PPTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale documento interno da utilizzarsi per la generazione del trasferimento a magazzino",;
    HelpContextID = 124055609,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=162, Top=11, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPTIPDOC"

  func oPPTIPDOC_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  func oPPTIPDOC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPTIPDOC_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPTIPDOC_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPTIPDOC_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSCO_KGI.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oDESRIF_1_4 as StdField with uid="NZLUHJPELA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 150852662,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=224, Top=11, InputMask=replicate('X',35)


  add object oPPFLPROV_1_5 as StdCombo with uid="AQKGHLCJVS",rtseq=5,rtrep=.f.,left=763,top=11,width=88,height=21;
    , ToolTipText = "Status dei documenti da generare";
    , HelpContextID = 90640460;
    , cFormVar="w_PPFLPROV",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPFLPROV_1_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPPFLPROV_1_5.GetRadio()
    this.Parent.oContained.w_PPFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oPPFLPROV_1_5.SetRadio()
    this.Parent.oContained.w_PPFLPROV=trim(this.Parent.oContained.w_PPFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_PPFLPROV=='N',1,;
      iif(this.Parent.oContained.w_PPFLPROV=='S',2,;
      0))
  endfunc

  func oPPFLPROV_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  add object oPPNUMDOC_1_6 as StdField with uid="ZXXUTHULLA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PPNUMDOC", cQueryName = "PPNUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero di ordine da generare disponibile",;
    HelpContextID = 121671737,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=162, Top=39, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue= 999999999999999

  func oPPNUMDOC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  add object oPPALFDOC_1_7 as StdField with uid="LGHDPDHADI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PPALFDOC", cQueryName = "PPALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 113688633,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=292, Top=39, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  func oPPALFDOC_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  add object oPPDATDOC_1_8 as StdField with uid="SRXRGALYSN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PPDATDOC", cQueryName = "PPDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 127660089,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=763, Top=39

  func oPPDATDOC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  add object oPPDATDIV_1_9 as StdField with uid="WNWCHHHDLF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PPDATDIV", cQueryName = "PPDATDIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 140775348,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=763, Top=67

  func oPPDATDIV_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  add object oPPDATTRA_1_10 as StdField with uid="PYWPFVHHRA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PPDATTRA", cQueryName = "PPDATTRA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di trasporto non pu� precedere la data del documento",;
    ToolTipText = "Data di trasporto",;
    HelpContextID = 127660087,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=162, Top=67

  func oPPDATTRA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPFLACCO='S' AND .w_GESWIP='S')
    endwith
   endif
  endfunc

  func oPPDATTRA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PPDATTRA>=.w_PPDATDOC) OR EMPTY(.w_PPDATTRA))
    endwith
    return bRes
  endfunc

  add object oPPORATRA_1_11 as StdField with uid="MTUTTTLUTO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PPORATRA", cQueryName = "PPORATRA",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora trasporto non corretta",;
    ToolTipText = "Ora del trasporto",;
    HelpContextID = 108896311,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=292, Top=67, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPPORATRA_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPFLACCO='S' AND .w_GESWIP='S')
    endwith
   endif
  endfunc

  func oPPORATRA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PPORATRA)<24)
    endwith
    return bRes
  endfunc

  add object oPPMINTRA_1_12 as StdField with uid="LZLJREZMQM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PPMINTRA", cQueryName = "PPMINTRA",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti trasporto non corretti",;
    ToolTipText = "Minuto del trasporto",;
    HelpContextID = 121929783,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=327, Top=67, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPPMINTRA_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPFLACCO='S' AND .w_GESWIP='S')
    endwith
   endif
  endfunc

  func oPPMINTRA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PPMINTRA)<60)
    endwith
    return bRes
  endfunc

  add object oSTADOC_1_13 as StdCheck with uid="AYOEVPGGTM",rtseq=13,rtrep=.f.,left=353, top=122, caption="Stampa immediata",;
    ToolTipText = "Se attivo esegue la stampa dei documenti generati",;
    HelpContextID = 105825318,;
    cFormVar="w_STADOC", bObbl = .f. , nPag = 1;
    , tabstop = .f.;
   , bGlobalFont=.t.


  func oSTADOC_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTADOC_1_13.GetRadio()
    this.Parent.oContained.w_STADOC = this.RadioValue()
    return .t.
  endfunc

  func oSTADOC_1_13.SetRadio()
    this.Parent.oContained.w_STADOC=trim(this.Parent.oContained.w_STADOC)
    this.value = ;
      iif(this.Parent.oContained.w_STADOC=='S',1,;
      0)
  endfunc

  func oSTADOC_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  func oSTADOC_1_13.mHide()
    with this.Parent.oContained
      return (.w_GESWIP<>'S')
    endwith
  endfunc

  add object oPPNOTAGG_1_14 as StdField with uid="FARSCKUZZQ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PPNOTAGG", cQueryName = "PPNOTAGG",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive sul trasporto",;
    HelpContextID = 190148547,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=162, Top=94, InputMask=replicate('X',40)

  func oPPNOTAGG_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPFLACCO='S' AND .w_GESWIP='S')
    endwith
   endif
  endfunc

  add object oPPCHKPRE_1_15 as StdCheck with uid="GVSDVAUNTF",rtseq=15,rtrep=.f.,left=162, top=122, caption="Controllo disponibilit�",;
    ToolTipText = "Se attivo abilita il controllo disponibilit� sul magazzino di prelievo",;
    HelpContextID = 51568699,;
    cFormVar="w_PPCHKPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPPCHKPRE_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPCHKPRE_1_15.GetRadio()
    this.Parent.oContained.w_PPCHKPRE = this.RadioValue()
    return .t.
  endfunc

  func oPPCHKPRE_1_15.SetRadio()
    this.Parent.oContained.w_PPCHKPRE=trim(this.Parent.oContained.w_PPCHKPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PPCHKPRE=='S',1,;
      0)
  endfunc

  func oPPCHKPRE_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESWIP='S')
    endwith
   endif
  endfunc

  func oPPCHKPRE_1_15.mHide()
    with this.Parent.oContained
      return (.w_GESWIP<>'S')
    endwith
  endfunc

  add object oODLINI_1_16 as StdField with uid="TMVOMSBJOT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ODLINI", cQueryName = "ODLINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL di inizio selezione (vuoto=no selezione)",;
    HelpContextID = 205808614,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=136, Top=166, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLINI"

  func oODLINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLINI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLINI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLINI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ordini di lavorazione",'GSCO_KGI.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oODLFIN_1_17 as StdField with uid="KGBHZSHPWV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ODLFIN", cQueryName = "ODLFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL di fine selezione (vuoto=no selezione)",;
    HelpContextID = 15819750,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=136, Top=190, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLFIN"

  proc oODLFIN_1_17.mDefault
    with this.Parent.oContained
      if empty(.w_ODLFIN)
        .w_ODLFIN = .w_ODLINI
      endif
    endwith
  endproc

  func oODLFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ordini di lavorazione",'GSCO_KGI.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oDATINI_1_24 as StdField with uid="BZZJIUDNZK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 205840438,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=349, Top=166

  add object oDATFIN_1_25 as StdField with uid="CRHOXZPXDK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 15851574,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=349, Top=190

  add object oDATFINE_1_26 as StdField with uid="FAXQJUJEXU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATFINE", cQueryName = "DATFINE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 252583882,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=511, Top=166

  add object oDATFIN1_1_27 as StdField with uid="QWARCSPHHS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 15851574,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=511, Top=190

  add object oDISINI_1_32 as StdField with uid="COEWNKTBFQ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DISINI", cQueryName = "DISINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di inizio selezione (vuota=no selezione)",;
    HelpContextID = 205838390,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=136, Top=214, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DISINI"

  func oDISINI_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISINI_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISINI_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDISINI_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDISINI_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DISINI
     i_obj.ecpSave()
  endproc

  add object oDISFIN_1_33 as StdField with uid="ZYGJAJAVLU",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DISFIN", cQueryName = "DISFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di fine selezione (vuota=no selezione)",;
    HelpContextID = 15849526,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=136, Top=238, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DISFIN"

  proc oDISFIN_1_33.mDefault
    with this.Parent.oContained
      if empty(.w_DISFIN)
        .w_DISFIN = .w_DISINI
      endif
    endwith
  endproc

  func oDISFIN_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISFIN_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISFIN_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDISFIN_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDISFIN_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DISFIN
     i_obj.ecpSave()
  endproc

  add object oDESDISI_1_34 as StdField with uid="QSEMNVVIFP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168831946,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=292, Top=214, InputMask=replicate('X',40)

  add object oDESDISF_1_35 as StdField with uid="BLVZKOKGVG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168831946,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=292, Top=238, InputMask=replicate('X',40)

  add object oFAMAINI_1_37 as StdField with uid="BAUZFWPTWC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 252940202,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=264, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_38 as StdField with uid="GHDDKOKVIG",rtseq=32,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 196898902,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=288, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_38.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_39 as StdField with uid="VYQYSTJFCH",rtseq=33,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 205848934,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=313, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_40 as StdField with uid="BKTBROMCMD",rtseq=34,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 15860070,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=337, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_40.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_41 as StdField with uid="WQNLFQEXDD",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 205840422,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=362, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_42 as StdField with uid="WHQQEZYTLE",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 15851558,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=386, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_42.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oMAGINI_1_43 as StdField with uid="WWAXPPIMPZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente o non nettificabile o di tipo WIP o maggiore del magazzino finale.",;
    ToolTipText = "Codice magazzino di inizio selezione (vuoto=no selezione)",;
    HelpContextID = 205787334,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=410, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMAGINI_1_43.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGINI
     i_obj.ecpSave()
  endproc

  add object oDESMAGI_1_44 as StdField with uid="QONUNYBHAK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 109521866,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=205, Top=410, InputMask=replicate('X',30)

  add object oMAGFIN_1_46 as StdField with uid="IPBLQAUROZ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente o non nettificabile o di tipo WIP o minore del magazzino iniziale.",;
    ToolTipText = "Codice magazzino di fine selezione (vuoto=no selezione)",;
    HelpContextID = 15798470,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=434, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_46.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_46.ecpDrop(oSource)
    this.Parent.oContained.link_1_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_46'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMAGFIN_1_46.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGFIN
     i_obj.ecpSave()
  endproc

  add object oDESMAGF_1_47 as StdField with uid="EBXVFVWORM",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 109521866,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=205, Top=434, InputMask=replicate('X',30)

  add object oCODCOM_1_49 as StdField with uid="GGQGKFAWGY",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di selezione (vuota =no selezione)",;
    HelpContextID = 5107238,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=136, Top=458, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
   endif
  endfunc

  func oCODCOM_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_52('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCODCOM_1_49.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDESCOM_1_50 as StdField with uid="BCJHQPAVGJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 5166134,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=262, Top=458, InputMask=replicate('X',30)

  add object oCODATT_1_52 as StdField with uid="BJCGFCEHYO",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di selezione (vuota =no selezione)",;
    HelpContextID = 127659558,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=136, Top=482, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODCOM) AND g_COMM='S')
    endwith
   endif
  endfunc

  func oCODATT_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODATT_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc

  add object oSERMRP_1_53 as StdField with uid="EEAADUFPEU",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SERMRP", cQueryName = "SERMRP",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale di elaborazione MRP",;
    HelpContextID = 59295014,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=136, Top=506, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEL__MRP", oKey_1_1="GMSERIAL", oKey_1_2="this.w_SERMRP"

  func oSERMRP_1_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERMRP_1_53.ecpDrop(oSource)
    this.Parent.oContained.link_1_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERMRP_1_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEL__MRP','*','GMSERIAL',cp_AbsName(this.parent,'oSERMRP_1_53'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivio generazione MRP",'',this.parent.oContained
  endproc

  add object oDESATT_1_54 as StdField with uid="TTNMSCEJIW",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 127718454,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=262, Top=482, InputMask=replicate('X',30)


  add object oBtn_1_55 as StdButton with uid="PPJFABIUST",left=751, top=486, width=48,height=45,;
    CpPicture="bmp\odll.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza lista ODL che soddisfano le selezioni impostate";
    , HelpContextID = 128127997;
    , tabstop=.f., Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      with this.Parent.oContained
        GSCO_BGI(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_56 as StdButton with uid="MJNXVZGNOX",left=804, top=486, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24772026;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_67 as cp_runprogram with uid="XPIOQBGYPF",left=9, top=552, width=552,height=17,;
    caption='GSCO_BS3',;
   bGlobalFont=.t.,;
    prg="GSCO_BS3('L')",;
    cEvent = "w_PPALFDOC Changed,w_PPTIPDOC Changed,w_PPDATDOC Changed,Init,NewProg",;
    nPag=1;
    , HelpContextID = 106554009


  add object oFATTODL_1_92 as StdCombo with uid="WVKRVNMDTR",rtseq=68,rtrep=.f.,left=754,top=324,width=97,height=21;
    , HelpContextID = 123723862;
    , cFormVar="w_FATTODL",RowSource=""+"Tutti,"+"Fattibili,"+"Non fattibili", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFATTODL_1_92.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFATTODL_1_92.GetRadio()
    this.Parent.oContained.w_FATTODL = this.RadioValue()
    return .t.
  endfunc

  func oFATTODL_1_92.SetRadio()
    this.Parent.oContained.w_FATTODL=trim(this.Parent.oContained.w_FATTODL)
    this.value = ;
      iif(this.Parent.oContained.w_FATTODL=='T',1,;
      iif(this.Parent.oContained.w_FATTODL=='D',2,;
      iif(this.Parent.oContained.w_FATTODL=='N',3,;
      0)))
  endfunc

  add object oDTINI_1_96 as StdField with uid="MIMWMOLVUL",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DTINI", cQueryName = "DTINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio del periodo elaborato",;
    HelpContextID = 49890102,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=754, Top=272

  add object oDTFIN_1_97 as StdField with uid="UVWTRCHKIX",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DTFIN", cQueryName = "DTFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine del periodo elaborato",;
    HelpContextID = 54793014,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=754, Top=295

  add object oTIPVER_1_98 as StdRadio with uid="JABPVGDPPQ",rtseq=71,rtrep=.f.,left=684, top=215, width=145,height=32, enabled=.f.;
    , cFormVar="w_TIPVER", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPVER_1_98.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Su disponibilit�"
      this.Buttons(1).HelpContextID = 79800630
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Su disp. contabile"
      this.Buttons(2).HelpContextID = 79800630
      this.Buttons(2).Top=15
      this.SetAll("Width",143)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPVER_1_98.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPVER_1_98.GetRadio()
    this.Parent.oContained.w_TIPVER = this.RadioValue()
    return .t.
  endfunc

  func oTIPVER_1_98.SetRadio()
    this.Parent.oContained.w_TIPVER=trim(this.Parent.oContained.w_TIPVER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVER=='D',1,;
      iif(this.Parent.oContained.w_TIPVER=='C',2,;
      0))
  endfunc

  add object oDATVER_1_103 as StdField with uid="VIRRLCYMZG",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DATVER", cQueryName = "DATVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 79814710,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=652, Top=164

  add object oORAVER_1_104 as StdField with uid="DSJEXZOHCH",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ORAVER", cQueryName = "ORAVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Ora ultima elaborazione",;
    HelpContextID = 79741414,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=802, Top=164, InputMask=replicate('X',5)

  add object oOPEVER_1_106 as StdField with uid="ONGZGCMGJS",rtseq=74,rtrep=.f.,;
    cFormVar = "w_OPEVER", cQueryName = "OPEVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha eseguito l'ultima verifica materiali",;
    HelpContextID = 79757286,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=652, Top=187, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OPEVER"

  func oOPEVER_1_106.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTE_1_107 as StdField with uid="EJGTDEDPGF",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 145806390,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=691, Top=187, InputMask=replicate('X',20)

  add object oDESFAMAI_1_113 as StdField with uid="AOYBYAMKXI",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259118207,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=205, Top=264, InputMask=replicate('X',35)

  add object oDESGRUI_1_114 as StdField with uid="ZABORREMDH",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125643722,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=205, Top=313, InputMask=replicate('X',35)

  add object oDESCATI_1_115 as StdField with uid="ZFWYARIWSP",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160508874,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=205, Top=362, InputMask=replicate('X',35)

  add object oDESFAMAF_1_119 as StdField with uid="WOCDSQXKSA",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259118204,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=205, Top=288, InputMask=replicate('X',35)

  add object oDESGRUF_1_120 as StdField with uid="PYGQUHDJMJ",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125643722,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=205, Top=337, InputMask=replicate('X',35)

  add object oDESCATF_1_121 as StdField with uid="SXSGGSHCII",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160508874,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=205, Top=386, InputMask=replicate('X',35)

  add object oStr_1_22 as StdString with uid="WVAVYFBMFI",Visible=.t., Left=-14, Top=167,;
    Alignment=1, Width=148, Height=15,;
    Caption="Da codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="QMCNFQOCKF",Visible=.t., Left=-14, Top=191,;
    Alignment=1, Width=148, Height=15,;
    Caption="A codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="BISAHFOFSR",Visible=.t., Left=266, Top=167,;
    Alignment=1, Width=80, Height=18,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="MEPBCWUJDL",Visible=.t., Left=271, Top=191,;
    Alignment=1, Width=75, Height=18,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="EVNJJSWPZN",Visible=.t., Left=5, Top=144,;
    Alignment=0, Width=120, Height=15,;
    Caption="Selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=-14, Top=411,;
    Alignment=1, Width=148, Height=15,;
    Caption="Da magazzino prelievo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=-14, Top=437,;
    Alignment=1, Width=148, Height=15,;
    Caption="A magazzino prelievo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ODMTRHWNGJ",Visible=.t., Left=-14, Top=461,;
    Alignment=1, Width=148, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="MSVOMVFWHL",Visible=.t., Left=-14, Top=485,;
    Alignment=1, Width=148, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="RQIHYRQPQJ",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=154, Height=15,;
    Caption="Causale buoni di prelievo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="HOVDNAQJWB",Visible=.t., Left=281, Top=39,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="LTTIBSJSQS",Visible=.t., Left=600, Top=69,;
    Alignment=1, Width=158, Height=15,;
    Caption="Scadenze in data diversa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="JNOBILKAHU",Visible=.t., Left=-1, Top=39,;
    Alignment=1, Width=161, Height=15,;
    Caption="Primo numero documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="FQFFGXFEAF",Visible=.t., Left=634, Top=41,;
    Alignment=1, Width=124, Height=15,;
    Caption="Data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="KXLMVYGVLS",Visible=.t., Left=682, Top=12,;
    Alignment=1, Width=76, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="HUERZBVWUA",Visible=.t., Left=6, Top=67,;
    Alignment=1, Width=154, Height=15,;
    Caption="Data trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="VQOEJAOMRV",Visible=.t., Left=246, Top=67,;
    Alignment=1, Width=43, Height=15,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="PVYTKNMCYC",Visible=.t., Left=6, Top=94,;
    Alignment=1, Width=154, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="NEYNREMDGG",Visible=.t., Left=315, Top=67,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=-14, Top=214,;
    Alignment=1, Width=148, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=-14, Top=238,;
    Alignment=1, Width=148, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="ZDABOVSXET",Visible=.t., Left=701, Top=250,;
    Alignment=0, Width=123, Height=15,;
    Caption="Periodo elaborato"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="WHDCASGAFN",Visible=.t., Left=685, Top=274,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="AVIRFQUXDP",Visible=.t., Left=688, Top=297,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="WMANKQZAWF",Visible=.t., Left=618, Top=215,;
    Alignment=1, Width=64, Height=15,;
    Caption="Tipo ver.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="ZCYWZVDZGO",Visible=.t., Left=647, Top=325,;
    Alignment=1, Width=104, Height=16,;
    Caption="Fattibilit� ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="TPHDNISVFM",Visible=.t., Left=606, Top=165,;
    Alignment=1, Width=42, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="HQKLISBKAT",Visible=.t., Left=754, Top=164,;
    Alignment=1, Width=43, Height=15,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="FIPDMOMWTM",Visible=.t., Left=590, Top=188,;
    Alignment=1, Width=58, Height=15,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="OGYKNRKNCP",Visible=.t., Left=567, Top=144,;
    Alignment=0, Width=284, Height=15,;
    Caption="Ultima elaborazione verifica fattibilit�"  ;
  , bGlobalFont=.t.

  add object oStr_1_110 as StdString with uid="DTJYHAGEOT",Visible=.t., Left=436, Top=167,;
    Alignment=1, Width=72, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="LKUZJNOGIO",Visible=.t., Left=436, Top=191,;
    Alignment=1, Width=72, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="AGNAIFECYP",Visible=.t., Left=36, Top=509,;
    Alignment=1, Width=97, Height=15,;
    Caption="Seriale MRP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=31, Top=266,;
    Alignment=1, Width=103, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=31, Top=315,;
    Alignment=1, Width=103, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_118 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=31, Top=365,;
    Alignment=1, Width=103, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_122 as StdString with uid="BETNLUNOYI",Visible=.t., Left=39, Top=290,;
    Alignment=1, Width=95, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_123 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=39, Top=339,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_124 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=39, Top=389,;
    Alignment=1, Width=95, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_31 as StdBox with uid="FCEXGVXXNY",left=4, top=158, width=847,height=2
enddefine
define class tgsco_kgiPag2 as StdContainer
  Width  = 859
  height = 534
  stdWidth  = 859
  stdheight = 534
  resizeXpos=453
  resizeYpos=225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomSel as cp_szoombox with uid="AEPLHZWOQV",left=0, top=3, width=858,height=437,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ODL_MAST",cZoomFile="GSCO_ZGI",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 145908198

  add object oSELEZI_2_2 as StdRadio with uid="KCMBJLDROQ",rtseq=19,rtrep=.f.,left=6, top=440, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 218129702
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 218129702
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_2.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_2.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_2.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oObj_2_3 as cp_runprogram with uid="BHANWDYKJR",left=3, top=553, width=219,height=22,;
    caption='GSCO_BGI',;
   bGlobalFont=.t.,;
    prg="GSCO_BGI('SS')",;
    cEvent = "w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 161881425


  add object oObj_2_4 as cp_runprogram with uid="EBVGCFPGTX",left=224, top=553, width=203,height=22,;
    caption='GSCO_BGI',;
   bGlobalFont=.t.,;
    prg="GSCO_BGI('INTERROGA')",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 161881425


  add object oBtn_2_5 as StdButton with uid="WQVAJMKWHR",left=752, top=487, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire gli aggiornamenti richiesti";
    , HelpContextID = 32060698;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSCO_BGI(this.Parent.oContained,"AG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_6 as StdButton with uid="IKSWHZXFAE",left=803, top=487, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24772026;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object OBJPRNSOL as cp_outputCombo with uid="ZLERXIVPWT",left=250, top=508, width=475,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cNoDefSep="",cDefSep="",;
    nPag=2;
    , HelpContextID = 145908198

  add object oSTAMPODL_2_8 as StdCheck with uid="MOUXBNGQZN",rtseq=83,rtrep=.f.,left=5, top=509, caption="Stampa immediata ordini",;
    ToolTipText = "Se attivo al termine dell'elaborazione lancia la stampa degli ordini",;
    HelpContextID = 228080526,;
    cFormVar="w_STAMPODL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSTAMPODL_2_8.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oSTAMPODL_2_8.GetRadio()
    this.Parent.oContained.w_STAMPODL = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPODL_2_8.SetRadio()
    this.Parent.oContained.w_STAMPODL=trim(this.Parent.oContained.w_STAMPODL)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPODL=="S",1,;
      0)
  endfunc

  add object oSTAMPFAS_2_9 as StdCheck with uid="AIWMJZNDGQ",rtseq=84,rtrep=.f.,left=5, top=483, caption="Stampa fasi di lavorazione",;
    ToolTipText = "Se attivo al termine dell'elaborazione lancia la stampa fasi di lavorazione",;
    HelpContextID = 157795449,;
    cFormVar="w_STAMPFAS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSTAMPFAS_2_9.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oSTAMPFAS_2_9.GetRadio()
    this.Parent.oContained.w_STAMPFAS = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPFAS_2_9.SetRadio()
    this.Parent.oContained.w_STAMPFAS=trim(this.Parent.oContained.w_STAMPFAS)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPFAS=="S",1,;
      0)
  endfunc

  func oSTAMPFAS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA="S" and g_CICLILAV = "S")
    endwith
   endif
  endfunc

  func oSTAMPFAS_2_9.mHide()
    with this.Parent.oContained
      return (g_PRFA<>"S" or g_CICLILAV <> "S")
    endwith
  endfunc


  add object OBJPRNSFL as cp_outputCombo with uid="CRONXOEAKI",left=250, top=482, width=475,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cNoDefSep="",cDefSep="",;
    nPag=2;
    , HelpContextID = 145908198

  add object oBox_2_11 as StdBox with uid="SVDPHZINFT",left=5, top=475, width=849,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kgi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
