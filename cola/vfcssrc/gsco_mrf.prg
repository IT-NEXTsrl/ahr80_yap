* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mrf                                                        *
*              Risorse di fase                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-13                                                      *
* Last revis.: 2016-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mrf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mrf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mrf")
  return

* --- Class definition
define class tgsco_mrf as StdPCForm
  Width  = 844
  Height = 464
  Top    = 10
  Left   = 10
  cComment = "Risorse di fase"
  cPrg = "gsco_mrf"
  HelpContextID=113878377
  add object cnt as tcgsco_mrf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mrf as PCContext
  w_RFCODODL = space(15)
  w_CSFLAPRE = space(1)
  w_EDITROW = space(1)
  w_EDITCHK = space(1)
  w_UMFLTEMP = space(1)
  w_CPROWORD = 0
  w_RFCODCLA = space(5)
  w_RF__TIPO = space(2)
  w_RFCODPAD = space(20)
  w_CLARIS = space(20)
  w_QTARIS = 0
  w_UMTDEF = space(3)
  w_RFCODCLA = space(5)
  w_CLARIS = space(20)
  w_RFCODICE = space(20)
  w_DESCLA = space(40)
  w_RFTCOEXT = 0
  w_RFQTARIS = 0
  w_RFUMTRIS = space(3)
  w_RFTEMRIS = 0
  w_RFTEMSEC = 0
  w_RFTPRRIU = 0
  w_RFTPRRIS = 0
  w_RFTPSRIS = 0
  w_RFTPSRIU = 0
  w_RFTPSTDS = 0
  w_RFTCORIS = 0
  w_RFQPSRIS = 0
  w_RFDESCRI = space(40)
  w_RFTPREVS = 0
  w_RFNUMIMP = 0
  w_RFTCONSS = 0
  w_RFINTEST = space(1)
  w_RFTMPCIC = 0
  w_RFTMPSEC = 0
  w_RFPROORA = 0
  w_RFMAGWIP = space(5)
  w_RFTIPTEM = space(1)
  w_CONSEC = 0
  w_RFRIFFAS = 0
  proc Save(i_oFrom)
    this.w_RFCODODL = i_oFrom.w_RFCODODL
    this.w_CSFLAPRE = i_oFrom.w_CSFLAPRE
    this.w_EDITROW = i_oFrom.w_EDITROW
    this.w_EDITCHK = i_oFrom.w_EDITCHK
    this.w_UMFLTEMP = i_oFrom.w_UMFLTEMP
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_RFCODCLA = i_oFrom.w_RFCODCLA
    this.w_RF__TIPO = i_oFrom.w_RF__TIPO
    this.w_RFCODPAD = i_oFrom.w_RFCODPAD
    this.w_CLARIS = i_oFrom.w_CLARIS
    this.w_QTARIS = i_oFrom.w_QTARIS
    this.w_UMTDEF = i_oFrom.w_UMTDEF
    this.w_RFCODCLA = i_oFrom.w_RFCODCLA
    this.w_CLARIS = i_oFrom.w_CLARIS
    this.w_RFCODICE = i_oFrom.w_RFCODICE
    this.w_DESCLA = i_oFrom.w_DESCLA
    this.w_RFTCOEXT = i_oFrom.w_RFTCOEXT
    this.w_RFQTARIS = i_oFrom.w_RFQTARIS
    this.w_RFUMTRIS = i_oFrom.w_RFUMTRIS
    this.w_RFTEMRIS = i_oFrom.w_RFTEMRIS
    this.w_RFTEMSEC = i_oFrom.w_RFTEMSEC
    this.w_RFTPRRIU = i_oFrom.w_RFTPRRIU
    this.w_RFTPRRIS = i_oFrom.w_RFTPRRIS
    this.w_RFTPSRIS = i_oFrom.w_RFTPSRIS
    this.w_RFTPSRIU = i_oFrom.w_RFTPSRIU
    this.w_RFTPSTDS = i_oFrom.w_RFTPSTDS
    this.w_RFTCORIS = i_oFrom.w_RFTCORIS
    this.w_RFQPSRIS = i_oFrom.w_RFQPSRIS
    this.w_RFDESCRI = i_oFrom.w_RFDESCRI
    this.w_RFTPREVS = i_oFrom.w_RFTPREVS
    this.w_RFNUMIMP = i_oFrom.w_RFNUMIMP
    this.w_RFTCONSS = i_oFrom.w_RFTCONSS
    this.w_RFINTEST = i_oFrom.w_RFINTEST
    this.w_RFTMPCIC = i_oFrom.w_RFTMPCIC
    this.w_RFTMPSEC = i_oFrom.w_RFTMPSEC
    this.w_RFPROORA = i_oFrom.w_RFPROORA
    this.w_RFMAGWIP = i_oFrom.w_RFMAGWIP
    this.w_RFTIPTEM = i_oFrom.w_RFTIPTEM
    this.w_CONSEC = i_oFrom.w_CONSEC
    this.w_RFRIFFAS = i_oFrom.w_RFRIFFAS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_RFCODODL = this.w_RFCODODL
    i_oTo.w_CSFLAPRE = this.w_CSFLAPRE
    i_oTo.w_EDITROW = this.w_EDITROW
    i_oTo.w_EDITCHK = this.w_EDITCHK
    i_oTo.w_UMFLTEMP = this.w_UMFLTEMP
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_RFCODCLA = this.w_RFCODCLA
    i_oTo.w_RF__TIPO = this.w_RF__TIPO
    i_oTo.w_RFCODPAD = this.w_RFCODPAD
    i_oTo.w_CLARIS = this.w_CLARIS
    i_oTo.w_QTARIS = this.w_QTARIS
    i_oTo.w_UMTDEF = this.w_UMTDEF
    i_oTo.w_RFCODCLA = this.w_RFCODCLA
    i_oTo.w_CLARIS = this.w_CLARIS
    i_oTo.w_RFCODICE = this.w_RFCODICE
    i_oTo.w_DESCLA = this.w_DESCLA
    i_oTo.w_RFTCOEXT = this.w_RFTCOEXT
    i_oTo.w_RFQTARIS = this.w_RFQTARIS
    i_oTo.w_RFUMTRIS = this.w_RFUMTRIS
    i_oTo.w_RFTEMRIS = this.w_RFTEMRIS
    i_oTo.w_RFTEMSEC = this.w_RFTEMSEC
    i_oTo.w_RFTPRRIU = this.w_RFTPRRIU
    i_oTo.w_RFTPRRIS = this.w_RFTPRRIS
    i_oTo.w_RFTPSRIS = this.w_RFTPSRIS
    i_oTo.w_RFTPSRIU = this.w_RFTPSRIU
    i_oTo.w_RFTPSTDS = this.w_RFTPSTDS
    i_oTo.w_RFTCORIS = this.w_RFTCORIS
    i_oTo.w_RFQPSRIS = this.w_RFQPSRIS
    i_oTo.w_RFDESCRI = this.w_RFDESCRI
    i_oTo.w_RFTPREVS = this.w_RFTPREVS
    i_oTo.w_RFNUMIMP = this.w_RFNUMIMP
    i_oTo.w_RFTCONSS = this.w_RFTCONSS
    i_oTo.w_RFINTEST = this.w_RFINTEST
    i_oTo.w_RFTMPCIC = this.w_RFTMPCIC
    i_oTo.w_RFTMPSEC = this.w_RFTMPSEC
    i_oTo.w_RFPROORA = this.w_RFPROORA
    i_oTo.w_RFMAGWIP = this.w_RFMAGWIP
    i_oTo.w_RFTIPTEM = this.w_RFTIPTEM
    i_oTo.w_CONSEC = this.w_CONSEC
    i_oTo.w_RFRIFFAS = this.w_RFRIFFAS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mrf as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 844
  Height = 464
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-06-07"
  HelpContextID=113878377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ODL_RISF_IDX = 0
  RIS_ORSE_IDX = 0
  CLR_DETT_IDX = 0
  CLR_MAST_IDX = 0
  UNIMIS_IDX = 0
  cFile = "ODL_RISF"
  cKeySelect = "RFCODODL"
  cKeyWhere  = "RFCODODL=this.w_RFCODODL"
  cKeyDetail  = "RFCODODL=this.w_RFCODODL"
  cKeyWhereODBC = '"RFCODODL="+cp_ToStrODBC(this.w_RFCODODL)';

  cKeyDetailWhereODBC = '"RFCODODL="+cp_ToStrODBC(this.w_RFCODODL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ODL_RISF.RFCODODL="+cp_ToStrODBC(this.w_RFCODODL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ODL_RISF.CPROWORD '
  cPrg = "gsco_mrf"
  cComment = "Risorse di fase"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RFCODODL = space(15)
  w_CSFLAPRE = space(1)
  w_EDITROW = .F.
  w_EDITCHK = .F.
  w_UMFLTEMP = space(1)
  w_CPROWORD = 0
  w_RFCODCLA = space(5)
  o_RFCODCLA = space(5)
  w_RF__TIPO = space(2)
  w_RFCODPAD = space(20)
  w_CLARIS = space(20)
  w_QTARIS = 0
  w_UMTDEF = space(3)
  w_RFCODCLA = space(5)
  w_CLARIS = space(20)
  w_RFCODICE = space(20)
  o_RFCODICE = space(20)
  w_DESCLA = space(40)
  w_RFTCOEXT = 0
  w_RFQTARIS = 0
  w_RFUMTRIS = space(3)
  w_RFTEMRIS = 0
  o_RFTEMRIS = 0
  w_RFTEMSEC = 0
  w_RFTPRRIU = 0
  w_RFTPRRIS = 0
  w_RFTPSRIS = 0
  w_RFTPSRIU = 0
  w_RFTPSTDS = 0
  w_RFTCORIS = 0
  w_RFQPSRIS = 0
  w_RFDESCRI = space(40)
  w_RFTPREVS = 0
  w_RFNUMIMP = 0
  o_RFNUMIMP = 0
  w_RFTCONSS = 0
  w_RFINTEST = space(1)
  w_RFTMPCIC = 0
  o_RFTMPCIC = 0
  w_RFTMPSEC = 0
  w_RFPROORA = 0
  w_RFMAGWIP = space(5)
  w_RFTIPTEM = space(1)
  w_CONSEC = 0
  w_RFRIFFAS = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mrfPag1","gsco_mrf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='CLR_DETT'
    this.cWorkTables[3]='CLR_MAST'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='ODL_RISF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ODL_RISF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ODL_RISF_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mrf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_8_joined
    link_2_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ODL_RISF where RFCODODL=KeySet.RFCODODL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISF_IDX,2],this.bLoadRecFilter,this.ODL_RISF_IDX,"gsco_mrf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ODL_RISF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ODL_RISF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ODL_RISF '
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RFCODODL',this.w_RFCODODL  )
      select * from (i_cTable) ODL_RISF where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CSFLAPRE = 'S'
        .w_UMFLTEMP = 'S'
        .w_RFCODODL = NVL(RFCODODL,space(15))
        .w_EDITROW = .T.
        .w_EDITCHK = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ODL_RISF')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CLARIS = space(20)
          .w_QTARIS = 0
          .w_UMTDEF = space(3)
          .w_CLARIS = space(20)
          .w_DESCLA = space(40)
          .w_RFDESCRI = space(40)
          .w_RFINTEST = space(1)
          .w_RFMAGWIP = space(5)
          .w_CONSEC = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RFCODCLA = NVL(RFCODCLA,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_RF__TIPO = NVL(RF__TIPO,space(2))
          .w_RFCODPAD = NVL(RFCODPAD,space(20))
          .w_RFCODCLA = NVL(RFCODCLA,space(5))
          if link_2_8_joined
            this.w_RFCODCLA = NVL(CSCODCLA208,NVL(this.w_RFCODCLA,space(5)))
            this.w_DESCLA = NVL(CSDESCLA208,space(40))
          else
          .link_2_8('Load')
          endif
          .w_RFCODICE = NVL(RFCODICE,space(20))
          .link_2_10('Load')
          .w_RFTCOEXT = NVL(RFTCOEXT,0)
          .w_RFQTARIS = NVL(RFQTARIS,0)
          .w_RFUMTRIS = NVL(RFUMTRIS,space(3))
          .link_2_14('Load')
          .w_RFTEMRIS = NVL(RFTEMRIS,0)
          .w_RFTEMSEC = NVL(RFTEMSEC,0)
          .w_RFTPRRIU = NVL(RFTPRRIU,0)
          .w_RFTPRRIS = NVL(RFTPRRIS,0)
          .w_RFTPSRIS = NVL(RFTPSRIS,0)
          .w_RFTPSRIU = NVL(RFTPSRIU,0)
          .w_RFTPSTDS = NVL(RFTPSTDS,0)
          .w_RFTCORIS = NVL(RFTCORIS,0)
          .w_RFQPSRIS = NVL(RFQPSRIS,0)
          .w_RFTPREVS = NVL(RFTPREVS,0)
          .w_RFNUMIMP = NVL(RFNUMIMP,0)
          .w_RFTCONSS = NVL(RFTCONSS,0)
          .w_RFTMPCIC = NVL(RFTMPCIC,0)
          .w_RFTMPSEC = NVL(RFTMPSEC,0)
          .w_RFPROORA = NVL(RFPROORA,0)
          .w_RFTIPTEM = NVL(RFTIPTEM,space(1))
          .w_RFRIFFAS = NVL(RFRIFFAS,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_EDITROW = .T.
        .w_EDITCHK = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RFCODODL=space(15)
      .w_CSFLAPRE=space(1)
      .w_EDITROW=.f.
      .w_EDITCHK=.f.
      .w_UMFLTEMP=space(1)
      .w_CPROWORD=10
      .w_RFCODCLA=space(5)
      .w_RF__TIPO=space(2)
      .w_RFCODPAD=space(20)
      .w_CLARIS=space(20)
      .w_QTARIS=0
      .w_UMTDEF=space(3)
      .w_RFCODCLA=space(5)
      .w_CLARIS=space(20)
      .w_RFCODICE=space(20)
      .w_DESCLA=space(40)
      .w_RFTCOEXT=0
      .w_RFQTARIS=0
      .w_RFUMTRIS=space(3)
      .w_RFTEMRIS=0
      .w_RFTEMSEC=0
      .w_RFTPRRIU=0
      .w_RFTPRRIS=0
      .w_RFTPSRIS=0
      .w_RFTPSRIU=0
      .w_RFTPSTDS=0
      .w_RFTCORIS=0
      .w_RFQPSRIS=0
      .w_RFDESCRI=space(40)
      .w_RFTPREVS=0
      .w_RFNUMIMP=0
      .w_RFTCONSS=0
      .w_RFINTEST=space(1)
      .w_RFTMPCIC=0
      .w_RFTMPSEC=0
      .w_RFPROORA=0
      .w_RFMAGWIP=space(5)
      .w_RFTIPTEM=space(1)
      .w_CONSEC=0
      .w_RFRIFFAS=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CSFLAPRE = 'S'
        .w_EDITROW = .T.
        .w_EDITCHK = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UMFLTEMP = 'S'
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_RFCODCLA))
         .link_2_2('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_RFCODPAD = .w_RFCODICE
        .DoRTCalc(10,13,.f.)
        if not(empty(.w_RFCODCLA))
         .link_2_8('Full')
        endif
        .DoRTCalc(14,14,.f.)
        .w_RFCODICE = iif(empty(.w_CLARIS),.w_RFCODICE,.w_CLARIS)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_RFCODICE))
         .link_2_10('Full')
        endif
        .DoRTCalc(16,19,.f.)
        if not(empty(.w_RFUMTRIS))
         .link_2_14('Full')
        endif
        .w_RFTEMRIS = iif(.w_RFTMPCIC > 0 , .w_RFTMPCIC / .w_RFNUMIMP , 0)
        .w_RFTEMSEC = .w_RFTEMRIS*.w_CONSEC
        .w_RFTPRRIU = .w_RFTEMSEC*.w_RFQTARIS
        .w_RFTPRRIS = .w_RFTPRRIU*iif(.w_RFTIPTEM='L',.oParentObject.w_OLTQTOD1,1)
        .w_RFTPSRIS = .w_RFTPSRIU*.oParentObject.w_OLTQTOD1
        .w_RFTPSRIU = .w_RFTEMSEC*.w_RFQTARIS
        .w_RFTPSTDS = .w_RFTPRRIS
        .DoRTCalc(27,27,.f.)
        .w_RFQPSRIS = .w_RFQTARIS
        .DoRTCalc(29,29,.f.)
        .w_RFTPREVS = .w_RFTPRRIS
        .w_RFNUMIMP = 1
        .DoRTCalc(32,33,.f.)
        .w_RFTMPCIC = iif(.w_RFTEMRIS>0 , .w_RFTEMRIS * .w_RFNUMIMP, 0)
        .w_RFTMPSEC = .w_RFTMPCIC*.w_CONSEC
        .w_RFPROORA = iif(.w_RFTEMSEC=0 and .w_RFTMPSEC=0 , 0, 3600/IIF(.w_RFTMPSEC<>0 , .w_RFTMPSEC, .w_RFTEMSEC)) * IIF(.w_RFTMPSEC<>0,.w_RFNUMIMP,1)
        .DoRTCalc(37,37,.f.)
        .w_RFTIPTEM = 'L'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ODL_RISF')
    this.DoRTCalc(39,40,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oRFNUMIMP_2_26.enabled = i_bVal
      .Page1.oPag.oRFTMPCIC_2_29.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ODL_RISF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCODODL,"RFCODODL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_RFCODCLA C(5);
      ,t_RF__TIPO N(3);
      ,t_RFCODICE C(20);
      ,t_DESCLA C(40);
      ,t_RFQTARIS N(8,2);
      ,t_RFUMTRIS C(3);
      ,t_RFTEMRIS N(18,4);
      ,t_RFTEMSEC N(18,4);
      ,t_RFDESCRI C(40);
      ,t_RFTPREVS N(18,4);
      ,t_RFNUMIMP N(4);
      ,t_RFTCONSS N(18,4);
      ,t_RFINTEST N(3);
      ,t_RFTMPCIC N(18,4);
      ,t_RFPROORA N(18,4);
      ,t_RFTIPTEM N(3);
      ,CPROWNUM N(10);
      ,t_RFCODPAD C(20);
      ,t_CLARIS C(20);
      ,t_QTARIS N(12,5);
      ,t_UMTDEF C(3);
      ,t_RFTCOEXT N(18,4);
      ,t_RFTPRRIU N(18,4);
      ,t_RFTPRRIS N(18,4);
      ,t_RFTPSRIS N(18,4);
      ,t_RFTPSRIU N(18,4);
      ,t_RFTPSTDS N(18,4);
      ,t_RFTCORIS N(18,4);
      ,t_RFQPSRIS N(5);
      ,t_RFTMPSEC N(18,4);
      ,t_RFMAGWIP C(5);
      ,t_CONSEC N(9,5);
      ,t_RFRIFFAS N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mrfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODCLA_2_2.controlsource=this.cTrsName+'.t_RFCODCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRF__TIPO_2_3.controlsource=this.cTrsName+'.t_RF__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODICE_2_10.controlsource=this.cTrsName+'.t_RFCODICE'
    this.oPgFRm.Page1.oPag.oDESCLA_2_11.controlsource=this.cTrsName+'.t_DESCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFQTARIS_2_13.controlsource=this.cTrsName+'.t_RFQTARIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFUMTRIS_2_14.controlsource=this.cTrsName+'.t_RFUMTRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMRIS_2_15.controlsource=this.cTrsName+'.t_RFTEMRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMSEC_2_16.controlsource=this.cTrsName+'.t_RFTEMSEC'
    this.oPgFRm.Page1.oPag.oRFDESCRI_2_24.controlsource=this.cTrsName+'.t_RFDESCRI'
    this.oPgFRm.Page1.oPag.oRFTPREVS_2_25.controlsource=this.cTrsName+'.t_RFTPREVS'
    this.oPgFRm.Page1.oPag.oRFNUMIMP_2_26.controlsource=this.cTrsName+'.t_RFNUMIMP'
    this.oPgFRm.Page1.oPag.oRFTCONSS_2_27.controlsource=this.cTrsName+'.t_RFTCONSS'
    this.oPgFRm.Page1.oPag.oRFINTEST_2_28.controlsource=this.cTrsName+'.t_RFINTEST'
    this.oPgFRm.Page1.oPag.oRFTMPCIC_2_29.controlsource=this.cTrsName+'.t_RFTMPCIC'
    this.oPgFRm.Page1.oPag.oRFPROORA_2_31.controlsource=this.cTrsName+'.t_RFPROORA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRFTIPTEM_2_33.controlsource=this.cTrsName+'.t_RFTIPTEM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(56)
    this.AddVLine(122)
    this.AddVLine(237)
    this.AddVLine(395)
    this.AddVLine(527)
    this.AddVLine(567)
    this.AddVLine(620)
    this.AddVLine(706)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISF_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISF_IDX,2])
      *
      * insert into ODL_RISF
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ODL_RISF')
        i_extval=cp_InsertValODBCExtFlds(this,'ODL_RISF')
        i_cFldBody=" "+;
                  "(RFCODODL,CPROWORD,RFCODCLA,RF__TIPO,RFCODPAD"+;
                  ",RFCODICE,RFTCOEXT,RFQTARIS,RFUMTRIS,RFTEMRIS"+;
                  ",RFTEMSEC,RFTPRRIU,RFTPRRIS,RFTPSRIS,RFTPSRIU"+;
                  ",RFTPSTDS,RFTCORIS,RFQPSRIS,RFTPREVS,RFNUMIMP"+;
                  ",RFTCONSS,RFTMPCIC,RFTMPSEC,RFPROORA,RFTIPTEM"+;
                  ",RFRIFFAS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RFCODODL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_RFCODCLA)+","+cp_ToStrODBC(this.w_RF__TIPO)+","+cp_ToStrODBC(this.w_RFCODPAD)+;
             ","+cp_ToStrODBCNull(this.w_RFCODICE)+","+cp_ToStrODBC(this.w_RFTCOEXT)+","+cp_ToStrODBC(this.w_RFQTARIS)+","+cp_ToStrODBCNull(this.w_RFUMTRIS)+","+cp_ToStrODBC(this.w_RFTEMRIS)+;
             ","+cp_ToStrODBC(this.w_RFTEMSEC)+","+cp_ToStrODBC(this.w_RFTPRRIU)+","+cp_ToStrODBC(this.w_RFTPRRIS)+","+cp_ToStrODBC(this.w_RFTPSRIS)+","+cp_ToStrODBC(this.w_RFTPSRIU)+;
             ","+cp_ToStrODBC(this.w_RFTPSTDS)+","+cp_ToStrODBC(this.w_RFTCORIS)+","+cp_ToStrODBC(this.w_RFQPSRIS)+","+cp_ToStrODBC(this.w_RFTPREVS)+","+cp_ToStrODBC(this.w_RFNUMIMP)+;
             ","+cp_ToStrODBC(this.w_RFTCONSS)+","+cp_ToStrODBC(this.w_RFTMPCIC)+","+cp_ToStrODBC(this.w_RFTMPSEC)+","+cp_ToStrODBC(this.w_RFPROORA)+","+cp_ToStrODBC(this.w_RFTIPTEM)+;
             ","+cp_ToStrODBC(this.w_RFRIFFAS)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ODL_RISF')
        i_extval=cp_InsertValVFPExtFlds(this,'ODL_RISF')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RFCODODL',this.w_RFCODODL)
        INSERT INTO (i_cTable) (;
                   RFCODODL;
                  ,CPROWORD;
                  ,RFCODCLA;
                  ,RF__TIPO;
                  ,RFCODPAD;
                  ,RFCODICE;
                  ,RFTCOEXT;
                  ,RFQTARIS;
                  ,RFUMTRIS;
                  ,RFTEMRIS;
                  ,RFTEMSEC;
                  ,RFTPRRIU;
                  ,RFTPRRIS;
                  ,RFTPSRIS;
                  ,RFTPSRIU;
                  ,RFTPSTDS;
                  ,RFTCORIS;
                  ,RFQPSRIS;
                  ,RFTPREVS;
                  ,RFNUMIMP;
                  ,RFTCONSS;
                  ,RFTMPCIC;
                  ,RFTMPSEC;
                  ,RFPROORA;
                  ,RFTIPTEM;
                  ,RFRIFFAS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RFCODODL;
                  ,this.w_CPROWORD;
                  ,this.w_RFCODCLA;
                  ,this.w_RF__TIPO;
                  ,this.w_RFCODPAD;
                  ,this.w_RFCODICE;
                  ,this.w_RFTCOEXT;
                  ,this.w_RFQTARIS;
                  ,this.w_RFUMTRIS;
                  ,this.w_RFTEMRIS;
                  ,this.w_RFTEMSEC;
                  ,this.w_RFTPRRIU;
                  ,this.w_RFTPRRIS;
                  ,this.w_RFTPSRIS;
                  ,this.w_RFTPSRIU;
                  ,this.w_RFTPSTDS;
                  ,this.w_RFTCORIS;
                  ,this.w_RFQPSRIS;
                  ,this.w_RFTPREVS;
                  ,this.w_RFNUMIMP;
                  ,this.w_RFTCONSS;
                  ,this.w_RFTMPCIC;
                  ,this.w_RFTMPSEC;
                  ,this.w_RFPROORA;
                  ,this.w_RFTIPTEM;
                  ,this.w_RFRIFFAS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISF_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_RFCODICE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_RISF')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_RISF')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_RFCODICE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ODL_RISF
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_RISF')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",RFCODCLA="+cp_ToStrODBCNull(this.w_RFCODCLA)+;
                     ",RF__TIPO="+cp_ToStrODBC(this.w_RF__TIPO)+;
                     ",RFCODPAD="+cp_ToStrODBC(this.w_RFCODPAD)+;
                     ",RFCODICE="+cp_ToStrODBCNull(this.w_RFCODICE)+;
                     ",RFTCOEXT="+cp_ToStrODBC(this.w_RFTCOEXT)+;
                     ",RFQTARIS="+cp_ToStrODBC(this.w_RFQTARIS)+;
                     ",RFUMTRIS="+cp_ToStrODBCNull(this.w_RFUMTRIS)+;
                     ",RFTEMRIS="+cp_ToStrODBC(this.w_RFTEMRIS)+;
                     ",RFTEMSEC="+cp_ToStrODBC(this.w_RFTEMSEC)+;
                     ",RFTPRRIU="+cp_ToStrODBC(this.w_RFTPRRIU)+;
                     ",RFTPRRIS="+cp_ToStrODBC(this.w_RFTPRRIS)+;
                     ",RFTPSRIS="+cp_ToStrODBC(this.w_RFTPSRIS)+;
                     ",RFTPSRIU="+cp_ToStrODBC(this.w_RFTPSRIU)+;
                     ",RFTPSTDS="+cp_ToStrODBC(this.w_RFTPSTDS)+;
                     ",RFTCORIS="+cp_ToStrODBC(this.w_RFTCORIS)+;
                     ",RFQPSRIS="+cp_ToStrODBC(this.w_RFQPSRIS)+;
                     ",RFTPREVS="+cp_ToStrODBC(this.w_RFTPREVS)+;
                     ",RFNUMIMP="+cp_ToStrODBC(this.w_RFNUMIMP)+;
                     ",RFTCONSS="+cp_ToStrODBC(this.w_RFTCONSS)+;
                     ",RFTMPCIC="+cp_ToStrODBC(this.w_RFTMPCIC)+;
                     ",RFTMPSEC="+cp_ToStrODBC(this.w_RFTMPSEC)+;
                     ",RFPROORA="+cp_ToStrODBC(this.w_RFPROORA)+;
                     ",RFTIPTEM="+cp_ToStrODBC(this.w_RFTIPTEM)+;
                     ",RFRIFFAS="+cp_ToStrODBC(this.w_RFRIFFAS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_RISF')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,RFCODCLA=this.w_RFCODCLA;
                     ,RF__TIPO=this.w_RF__TIPO;
                     ,RFCODPAD=this.w_RFCODPAD;
                     ,RFCODICE=this.w_RFCODICE;
                     ,RFTCOEXT=this.w_RFTCOEXT;
                     ,RFQTARIS=this.w_RFQTARIS;
                     ,RFUMTRIS=this.w_RFUMTRIS;
                     ,RFTEMRIS=this.w_RFTEMRIS;
                     ,RFTEMSEC=this.w_RFTEMSEC;
                     ,RFTPRRIU=this.w_RFTPRRIU;
                     ,RFTPRRIS=this.w_RFTPRRIS;
                     ,RFTPSRIS=this.w_RFTPSRIS;
                     ,RFTPSRIU=this.w_RFTPSRIU;
                     ,RFTPSTDS=this.w_RFTPSTDS;
                     ,RFTCORIS=this.w_RFTCORIS;
                     ,RFQPSRIS=this.w_RFQPSRIS;
                     ,RFTPREVS=this.w_RFTPREVS;
                     ,RFNUMIMP=this.w_RFNUMIMP;
                     ,RFTCONSS=this.w_RFTCONSS;
                     ,RFTMPCIC=this.w_RFTMPCIC;
                     ,RFTMPSEC=this.w_RFTMPSEC;
                     ,RFPROORA=this.w_RFPROORA;
                     ,RFTIPTEM=this.w_RFTIPTEM;
                     ,RFRIFFAS=this.w_RFRIFFAS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISF_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_RFCODICE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ODL_RISF
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_RFCODICE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODL_RISF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_RISF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_EDITROW = .T.
          .w_EDITCHK = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(5,8,.t.)
        if .o_RFCODICE<>.w_RFCODICE
          .w_RFCODPAD = .w_RFCODICE
        endif
        .DoRTCalc(10,12,.t.)
          .link_2_8('Full')
        .DoRTCalc(14,14,.t.)
        if .o_RFCODCLA<>.w_RFCODCLA.or. .o_RFCODICE<>.w_RFCODICE
          .w_RFCODICE = iif(empty(.w_CLARIS),.w_RFCODICE,.w_CLARIS)
          .link_2_10('Full')
        endif
        .DoRTCalc(16,19,.t.)
        if .o_RFTMPCIC<>.w_RFTMPCIC.or. .o_RFNUMIMP<>.w_RFNUMIMP
          .w_RFTEMRIS = iif(.w_RFTMPCIC > 0 , .w_RFTMPCIC / .w_RFNUMIMP , 0)
        endif
          .w_RFTEMSEC = .w_RFTEMRIS*.w_CONSEC
          .w_RFTPRRIU = .w_RFTEMSEC*.w_RFQTARIS
          .w_RFTPRRIS = .w_RFTPRRIU*iif(.w_RFTIPTEM='L',.oParentObject.w_OLTQTOD1,1)
          .w_RFTPSRIS = .w_RFTPSRIU*.oParentObject.w_OLTQTOD1
          .w_RFTPSRIU = .w_RFTEMSEC*.w_RFQTARIS
          .w_RFTPSTDS = .w_RFTPRRIS
        .DoRTCalc(27,27,.t.)
          .w_RFQPSRIS = .w_RFQTARIS
        .DoRTCalc(29,29,.t.)
          .w_RFTPREVS = .w_RFTPRRIS
        .DoRTCalc(31,33,.t.)
        if .o_RFTEMRIS<>.w_RFTEMRIS.or. .o_RFNUMIMP<>.w_RFNUMIMP
          .w_RFTMPCIC = iif(.w_RFTEMRIS>0 , .w_RFTEMRIS * .w_RFNUMIMP, 0)
        endif
          .w_RFTMPSEC = .w_RFTMPCIC*.w_CONSEC
          .w_RFPROORA = iif(.w_RFTEMSEC=0 and .w_RFTMPSEC=0 , 0, 3600/IIF(.w_RFTMPSEC<>0 , .w_RFTMPSEC, .w_RFTEMSEC)) * IIF(.w_RFTMPSEC<>0,.w_RFNUMIMP,1)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(37,40,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_RFCODPAD with this.w_RFCODPAD
      replace t_CLARIS with this.w_CLARIS
      replace t_QTARIS with this.w_QTARIS
      replace t_UMTDEF with this.w_UMTDEF
      replace t_RFCODCLA with this.w_RFCODCLA
      replace t_CLARIS with this.w_CLARIS
      replace t_RFTCOEXT with this.w_RFTCOEXT
      replace t_RFTPRRIU with this.w_RFTPRRIU
      replace t_RFTPRRIS with this.w_RFTPRRIS
      replace t_RFTPSRIS with this.w_RFTPSRIS
      replace t_RFTPSRIU with this.w_RFTPSRIU
      replace t_RFTPSTDS with this.w_RFTPSTDS
      replace t_RFTCORIS with this.w_RFTCORIS
      replace t_RFQPSRIS with this.w_RFQPSRIS
      replace t_RFTMPSEC with this.w_RFTMPSEC
      replace t_RFMAGWIP with this.w_RFMAGWIP
      replace t_CONSEC with this.w_CONSEC
      replace t_RFRIFFAS with this.w_RFRIFFAS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFCODICE_2_10
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFCODCLA_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFCODCLA_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRF__TIPO_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRF__TIPO_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFQTARIS_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFQTARIS_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFUMTRIS_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFUMTRIS_2_14.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFTEMRIS_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRFTEMRIS_2_15.mCond()
    this.oPgFrm.Page1.oPag.oRFNUMIMP_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oRFNUMIMP_2_26.mCond()
    this.oPgFrm.Page1.oPag.oRFTMPCIC_2_29.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oRFTMPCIC_2_29.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RFCODCLA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_DETT_IDX,3]
    i_lTable = "CLR_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2], .t., this.CLR_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLR_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCLA like "+cp_ToStrODBC(trim(this.w_RFCODCLA)+"%");
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);

          i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSFLAPRE,CSCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSFLAPRE',this.w_CSFLAPRE;
                     ,'CSCODCLA',trim(this.w_RFCODCLA))
          select CSFLAPRE,CSCODCLA,CSCODRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSFLAPRE,CSCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCODCLA)==trim(_Link_.CSCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RFCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(oSource.parent,'oRFCODCLA_2_2'),i_cWhere,'',"",'GSCI_MCS.CLR_DETT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CSFLAPRE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',oSource.xKey(1);
                       ,'CSCODCLA',oSource.xKey(2))
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_RFCODCLA);
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',this.w_CSFLAPRE;
                       ,'CSCODCLA',this.w_RFCODCLA)
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_RFCODICE = NVL(_Link_.CSCODRIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_RFCODCLA = space(5)
      endif
      this.w_RFCODICE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])+'\'+cp_ToStr(_Link_.CSFLAPRE,1)+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RFCODCLA
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_MAST_IDX,3]
    i_lTable = "CLR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2], .t., this.CLR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_RFCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',this.w_RFCODCLA)
            select CSCODCLA,CSDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_DESCLA = NVL(_Link_.CSDESCLA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RFCODCLA = space(5)
      endif
      this.w_DESCLA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLR_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.CSCODCLA as CSCODCLA208"+ ",link_2_8.CSDESCLA as CSDESCLA208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on ODL_RISF.RFCODCLA=link_2_8.CSCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and ODL_RISF.RFCODCLA=link_2_8.CSCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RFCODICE
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_RFCODICE)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_RF__TIPO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_RF__TIPO;
                     ,'RLCODICE',trim(this.w_RFCODICE))
          select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCODICE)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_RFCODICE)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_RF__TIPO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_RFCODICE)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_RF__TIPO);

            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RFCODICE) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oRFCODICE_2_10'),i_cWhere,'',"RISORSE",'GSCO_ZLR.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RF__TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_RF__TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_RFCODICE);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_RF__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_RF__TIPO;
                       ,'RLCODICE',this.w_RFCODICE)
            select RL__TIPO,RLCODICE,RLDESCRI,RLUMTDEF,RLQTARIS,RLINTEST,RLMAGWIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCODICE = NVL(_Link_.RLCODICE,space(20))
      this.w_RFDESCRI = NVL(_Link_.RLDESCRI,space(40))
      this.w_UMTDEF = NVL(_Link_.RLUMTDEF,space(3))
      this.w_QTARIS = NVL(_Link_.RLQTARIS,0)
      this.w_RFINTEST = NVL(_Link_.RLINTEST,space(1))
      this.w_RFMAGWIP = NVL(_Link_.RLMAGWIP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RFCODICE = space(20)
      endif
      this.w_RFDESCRI = space(40)
      this.w_UMTDEF = space(3)
      this.w_QTARIS = 0
      this.w_RFINTEST = space(1)
      this.w_RFMAGWIP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!.w_RF__TIPO $ 'UP-AR-RE'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inesistente")
        endif
        this.w_RFCODICE = space(20)
        this.w_RFDESCRI = space(40)
        this.w_UMTDEF = space(3)
        this.w_QTARIS = 0
        this.w_RFINTEST = space(1)
        this.w_RFMAGWIP = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RFUMTRIS
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFUMTRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RFUMTRIS)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_RFUMTRIS))
          select UMFLTEMP,UMCODICE,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFUMTRIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RFUMTRIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oRFUMTRIS_2_14'),i_cWhere,'',"UNITA' DI MISURA TEMPO",'GSCO_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFUMTRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RFUMTRIS);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_RFUMTRIS)
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFUMTRIS = NVL(_Link_.UMCODICE,space(3))
      this.w_CONSEC = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_RFUMTRIS = space(3)
      endif
      this.w_CONSEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFUMTRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCLA_2_11.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_2_11.value=this.w_DESCLA
      replace t_DESCLA with this.oPgFrm.Page1.oPag.oDESCLA_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFDESCRI_2_24.value==this.w_RFDESCRI)
      this.oPgFrm.Page1.oPag.oRFDESCRI_2_24.value=this.w_RFDESCRI
      replace t_RFDESCRI with this.oPgFrm.Page1.oPag.oRFDESCRI_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFTPREVS_2_25.value==this.w_RFTPREVS)
      this.oPgFrm.Page1.oPag.oRFTPREVS_2_25.value=this.w_RFTPREVS
      replace t_RFTPREVS with this.oPgFrm.Page1.oPag.oRFTPREVS_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFNUMIMP_2_26.value==this.w_RFNUMIMP)
      this.oPgFrm.Page1.oPag.oRFNUMIMP_2_26.value=this.w_RFNUMIMP
      replace t_RFNUMIMP with this.oPgFrm.Page1.oPag.oRFNUMIMP_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFTCONSS_2_27.value==this.w_RFTCONSS)
      this.oPgFrm.Page1.oPag.oRFTCONSS_2_27.value=this.w_RFTCONSS
      replace t_RFTCONSS with this.oPgFrm.Page1.oPag.oRFTCONSS_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFINTEST_2_28.RadioValue()==this.w_RFINTEST)
      this.oPgFrm.Page1.oPag.oRFINTEST_2_28.SetRadio()
      replace t_RFINTEST with this.oPgFrm.Page1.oPag.oRFINTEST_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFTMPCIC_2_29.value==this.w_RFTMPCIC)
      this.oPgFrm.Page1.oPag.oRFTMPCIC_2_29.value=this.w_RFTMPCIC
      replace t_RFTMPCIC with this.oPgFrm.Page1.oPag.oRFTMPCIC_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRFPROORA_2_31.value==this.w_RFPROORA)
      this.oPgFrm.Page1.oPag.oRFPROORA_2_31.value=this.w_RFPROORA
      replace t_RFPROORA with this.oPgFrm.Page1.oPag.oRFPROORA_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODCLA_2_2.value==this.w_RFCODCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODCLA_2_2.value=this.w_RFCODCLA
      replace t_RFCODCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODCLA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRF__TIPO_2_3.RadioValue()==this.w_RF__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRF__TIPO_2_3.SetRadio()
      replace t_RF__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRF__TIPO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODICE_2_10.value==this.w_RFCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODICE_2_10.value=this.w_RFCODICE
      replace t_RFCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODICE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFQTARIS_2_13.value==this.w_RFQTARIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFQTARIS_2_13.value=this.w_RFQTARIS
      replace t_RFQTARIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFQTARIS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFUMTRIS_2_14.value==this.w_RFUMTRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFUMTRIS_2_14.value=this.w_RFUMTRIS
      replace t_RFUMTRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFUMTRIS_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMRIS_2_15.value==this.w_RFTEMRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMRIS_2_15.value=this.w_RFTEMRIS
      replace t_RFTEMRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMRIS_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMSEC_2_16.value==this.w_RFTEMSEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMSEC_2_16.value=this.w_RFTEMSEC
      replace t_RFTEMSEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTEMSEC_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTIPTEM_2_33.RadioValue()==this.w_RFTIPTEM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTIPTEM_2_33.SetRadio()
      replace t_RFTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTIPTEM_2_33.value
    endif
    cp_SetControlsValueExtFlds(this,'ODL_RISF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_RFCODICE) or not(!.w_RF__TIPO $ 'UP-AR-RE')) and (not(Empty(.w_RFCODICE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFCODICE_2_10
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo risorsa non valido oppure risorsa inesistente")
        case   not(((.w_RF__TIPO<>'CL' and .w_RFQTARIS<=.w_QTARIS) or (.w_RF__TIPO='CL' and .w_RFQTARIS<=1))) and (.w_EDITROW Or .w_EDITCHK) and (not(Empty(.w_RFCODICE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFQTARIS_2_13
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_RFCODICE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RFCODCLA = this.w_RFCODCLA
    this.o_RFCODICE = this.w_RFCODICE
    this.o_RFTEMRIS = this.w_RFTEMRIS
    this.o_RFNUMIMP = this.w_RFNUMIMP
    this.o_RFTMPCIC = this.w_RFTMPCIC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_RFCODICE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_RFCODCLA=space(5)
      .w_RF__TIPO=space(2)
      .w_RFCODPAD=space(20)
      .w_CLARIS=space(20)
      .w_QTARIS=0
      .w_UMTDEF=space(3)
      .w_RFCODCLA=space(5)
      .w_CLARIS=space(20)
      .w_RFCODICE=space(20)
      .w_DESCLA=space(40)
      .w_RFTCOEXT=0
      .w_RFQTARIS=0
      .w_RFUMTRIS=space(3)
      .w_RFTEMRIS=0
      .w_RFTEMSEC=0
      .w_RFTPRRIU=0
      .w_RFTPRRIS=0
      .w_RFTPSRIS=0
      .w_RFTPSRIU=0
      .w_RFTPSTDS=0
      .w_RFTCORIS=0
      .w_RFQPSRIS=0
      .w_RFDESCRI=space(40)
      .w_RFTPREVS=0
      .w_RFNUMIMP=0
      .w_RFTCONSS=0
      .w_RFINTEST=space(1)
      .w_RFTMPCIC=0
      .w_RFTMPSEC=0
      .w_RFPROORA=0
      .w_RFMAGWIP=space(5)
      .w_RFTIPTEM=space(1)
      .w_CONSEC=0
      .w_RFRIFFAS=0
      .DoRTCalc(1,7,.f.)
      if not(empty(.w_RFCODCLA))
        .link_2_2('Full')
      endif
      .DoRTCalc(8,8,.f.)
        .w_RFCODPAD = .w_RFCODICE
      .DoRTCalc(10,13,.f.)
      if not(empty(.w_RFCODCLA))
        .link_2_8('Full')
      endif
      .DoRTCalc(14,14,.f.)
        .w_RFCODICE = iif(empty(.w_CLARIS),.w_RFCODICE,.w_CLARIS)
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_RFCODICE))
        .link_2_10('Full')
      endif
      .DoRTCalc(16,19,.f.)
      if not(empty(.w_RFUMTRIS))
        .link_2_14('Full')
      endif
        .w_RFTEMRIS = iif(.w_RFTMPCIC > 0 , .w_RFTMPCIC / .w_RFNUMIMP , 0)
        .w_RFTEMSEC = .w_RFTEMRIS*.w_CONSEC
        .w_RFTPRRIU = .w_RFTEMSEC*.w_RFQTARIS
        .w_RFTPRRIS = .w_RFTPRRIU*iif(.w_RFTIPTEM='L',.oParentObject.w_OLTQTOD1,1)
        .w_RFTPSRIS = .w_RFTPSRIU*.oParentObject.w_OLTQTOD1
        .w_RFTPSRIU = .w_RFTEMSEC*.w_RFQTARIS
        .w_RFTPSTDS = .w_RFTPRRIS
      .DoRTCalc(27,27,.f.)
        .w_RFQPSRIS = .w_RFQTARIS
      .DoRTCalc(29,29,.f.)
        .w_RFTPREVS = .w_RFTPRRIS
        .w_RFNUMIMP = 1
      .DoRTCalc(32,33,.f.)
        .w_RFTMPCIC = iif(.w_RFTEMRIS>0 , .w_RFTEMRIS * .w_RFNUMIMP, 0)
        .w_RFTMPSEC = .w_RFTMPCIC*.w_CONSEC
        .w_RFPROORA = iif(.w_RFTEMSEC=0 and .w_RFTMPSEC=0 , 0, 3600/IIF(.w_RFTMPSEC<>0 , .w_RFTMPSEC, .w_RFTEMSEC)) * IIF(.w_RFTMPSEC<>0,.w_RFNUMIMP,1)
      .DoRTCalc(37,37,.f.)
        .w_RFTIPTEM = 'L'
    endwith
    this.DoRTCalc(39,40,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RFCODCLA = t_RFCODCLA
    this.w_RF__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRF__TIPO_2_3.RadioValue(.t.)
    this.w_RFCODPAD = t_RFCODPAD
    this.w_CLARIS = t_CLARIS
    this.w_QTARIS = t_QTARIS
    this.w_UMTDEF = t_UMTDEF
    this.w_RFCODCLA = t_RFCODCLA
    this.w_CLARIS = t_CLARIS
    this.w_RFCODICE = t_RFCODICE
    this.w_DESCLA = t_DESCLA
    this.w_RFTCOEXT = t_RFTCOEXT
    this.w_RFQTARIS = t_RFQTARIS
    this.w_RFUMTRIS = t_RFUMTRIS
    this.w_RFTEMRIS = t_RFTEMRIS
    this.w_RFTEMSEC = t_RFTEMSEC
    this.w_RFTPRRIU = t_RFTPRRIU
    this.w_RFTPRRIS = t_RFTPRRIS
    this.w_RFTPSRIS = t_RFTPSRIS
    this.w_RFTPSRIU = t_RFTPSRIU
    this.w_RFTPSTDS = t_RFTPSTDS
    this.w_RFTCORIS = t_RFTCORIS
    this.w_RFQPSRIS = t_RFQPSRIS
    this.w_RFDESCRI = t_RFDESCRI
    this.w_RFTPREVS = t_RFTPREVS
    this.w_RFNUMIMP = t_RFNUMIMP
    this.w_RFTCONSS = t_RFTCONSS
    this.w_RFINTEST = this.oPgFrm.Page1.oPag.oRFINTEST_2_28.RadioValue(.t.)
    this.w_RFTMPCIC = t_RFTMPCIC
    this.w_RFTMPSEC = t_RFTMPSEC
    this.w_RFPROORA = t_RFPROORA
    this.w_RFMAGWIP = t_RFMAGWIP
    this.w_RFTIPTEM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTIPTEM_2_33.RadioValue(.t.)
    this.w_CONSEC = t_CONSEC
    this.w_RFRIFFAS = t_RFRIFFAS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RFCODCLA with this.w_RFCODCLA
    replace t_RF__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRF__TIPO_2_3.ToRadio()
    replace t_RFCODPAD with this.w_RFCODPAD
    replace t_CLARIS with this.w_CLARIS
    replace t_QTARIS with this.w_QTARIS
    replace t_UMTDEF with this.w_UMTDEF
    replace t_RFCODCLA with this.w_RFCODCLA
    replace t_CLARIS with this.w_CLARIS
    replace t_RFCODICE with this.w_RFCODICE
    replace t_DESCLA with this.w_DESCLA
    replace t_RFTCOEXT with this.w_RFTCOEXT
    replace t_RFQTARIS with this.w_RFQTARIS
    replace t_RFUMTRIS with this.w_RFUMTRIS
    replace t_RFTEMRIS with this.w_RFTEMRIS
    replace t_RFTEMSEC with this.w_RFTEMSEC
    replace t_RFTPRRIU with this.w_RFTPRRIU
    replace t_RFTPRRIS with this.w_RFTPRRIS
    replace t_RFTPSRIS with this.w_RFTPSRIS
    replace t_RFTPSRIU with this.w_RFTPSRIU
    replace t_RFTPSTDS with this.w_RFTPSTDS
    replace t_RFTCORIS with this.w_RFTCORIS
    replace t_RFQPSRIS with this.w_RFQPSRIS
    replace t_RFDESCRI with this.w_RFDESCRI
    replace t_RFTPREVS with this.w_RFTPREVS
    replace t_RFNUMIMP with this.w_RFNUMIMP
    replace t_RFTCONSS with this.w_RFTCONSS
    replace t_RFINTEST with this.oPgFrm.Page1.oPag.oRFINTEST_2_28.ToRadio()
    replace t_RFTMPCIC with this.w_RFTMPCIC
    replace t_RFTMPSEC with this.w_RFTMPSEC
    replace t_RFPROORA with this.w_RFPROORA
    replace t_RFMAGWIP with this.w_RFMAGWIP
    replace t_RFTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRFTIPTEM_2_33.ToRadio()
    replace t_CONSEC with this.w_CONSEC
    replace t_RFRIFFAS with this.w_RFRIFFAS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mrfPag1 as StdContainer
  Width  = 840
  height = 464
  stdWidth  = 840
  stdheight = 464
  resizeXpos=296
  resizeYpos=166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="RZLAJPDTIT",left=7, top=4, width=804,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Posiz.",Field2="RFCODCLA",Label2="Classe",Field3="RF__TIPO",Label3="Tipo risorsa",Field4="RFCODICE",Label4="Risorsa",Field5="RFTIPTEM",Label5="Tipo tempo",Field6="RFQTARIS",Label6="Q.t�",Field7="RFUMTRIS",Label7="U.M.T.",Field8="RFTEMRIS",Label8="Tempo risorsa",Field9="RFTEMSEC",Label9="Tempo risorsa sec.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202212474

  add object oStr_1_2 as StdString with uid="GCBCHWOCCZ",Visible=.t., Left=333, Top=413,;
    Alignment=1, Width=62, Height=18,;
    Caption="T. a prev."  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="IBRCMDSFHK",Visible=.t., Left=10, Top=413,;
    Alignment=1, Width=56, Height=18,;
    Caption="Classe"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="EWBGXQPFBM",Visible=.t., Left=483, Top=414,;
    Alignment=1, Width=55, Height=18,;
    Caption="T. a cons."  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="YKKOXWOFCH",Visible=.t., Left=8, Top=437,;
    Alignment=1, Width=58, Height=18,;
    Caption="Risorsa:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="UGWAJPCAWG",Visible=.t., Left=625, Top=413,;
    Alignment=1, Width=96, Height=13,;
    Caption="Num. impronte:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="LJNEHBYSJT",Visible=.t., Left=626, Top=436,;
    Alignment=1, Width=96, Height=13,;
    Caption="Prod. oraria:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="FGOCLMCRBX",Visible=.t., Left=33, Top=-270,;
    Alignment=1, Width=85, Height=18,;
    Caption="Conv. Sec.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=24,;
    width=798+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=25,width=797+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CLR_DETT|RIS_ORSE|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCLA_2_11.Refresh()
      this.Parent.oRFDESCRI_2_24.Refresh()
      this.Parent.oRFTPREVS_2_25.Refresh()
      this.Parent.oRFNUMIMP_2_26.Refresh()
      this.Parent.oRFTCONSS_2_27.Refresh()
      this.Parent.oRFINTEST_2_28.Refresh()
      this.Parent.oRFTMPCIC_2_29.Refresh()
      this.Parent.oRFPROORA_2_31.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CLR_DETT'
        oDropInto=this.oBodyCol.oRow.oRFCODCLA_2_2
      case cFile='RIS_ORSE'
        oDropInto=this.oBodyCol.oRow.oRFCODICE_2_10
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oRFUMTRIS_2_14
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESCLA_2_11 as StdTrsField with uid="UALQAOAXQO",rtseq=16,rtrep=.t.,;
    cFormVar="w_DESCLA",value=space(40),enabled=.f.,;
    HelpContextID = 255775798,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCLA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=246, Left=72, Top=412, InputMask=replicate('X',40)

  add object oRFDESCRI_2_24 as StdTrsField with uid="RURJNZQHGC",rtseq=29,rtrep=.t.,;
    cFormVar="w_RFDESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 28304991,;
    cTotal="", bFixedPos=.t., cQueryName = "RFDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=246, Left=72, Top=436, InputMask=replicate('X',40)

  add object oRFTPREVS_2_25 as StdTrsField with uid="GAFTUADHIE",rtseq=30,rtrep=.t.,;
    cFormVar="w_RFTPREVS",value=0,enabled=.f.,;
    HelpContextID = 206838167,;
    cTotal="", bFixedPos=.t., cQueryName = "RFTPREVS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=82, Left=398, Top=412, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oRFNUMIMP_2_26 as StdTrsField with uid="UJDMQNUYIR",rtseq=31,rtrep=.t.,;
    cFormVar="w_RFNUMIMP",value=0,;
    ToolTipText = "Numero impronte dell'attrezzatura",;
    HelpContextID = 144669082,;
    cTotal="", bFixedPos=.t., cQueryName = "RFNUMIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=32, Left=727, Top=412, cSayPict=["@Z 9999"], cGetPict=["9999"]

  func oRFNUMIMP_2_26.mCond()
    with this.Parent.oContained
      return (.w_RFQTARIS>0 and not empty(.w_RFUMTRIS) AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  add object oRFTCONSS_2_27 as StdTrsField with uid="OLJITDGLBF",rtseq=32,rtrep=.t.,;
    cFormVar="w_RFTCONSS",value=0,enabled=.f.,;
    HelpContextID = 208594537,;
    cTotal="", bFixedPos=.t., cQueryName = "RFTCONSS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=82, Left=540, Top=412, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oRFINTEST_2_28 as StdTrsCheck with uid="PDUQVLZNNN",rtrep=.t.,;
    cFormVar="w_RFINTEST", enabled=.f., caption="Centro esterno",;
    HelpContextID = 63518314,;
    Left=429, Top=436,;
    cTotal="", cQueryName = "RFINTEST",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , DisabledBackColor=rgb(255,255,255);
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oRFINTEST_2_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RFINTEST,&i_cF..t_RFINTEST),this.value)
    return(iif(xVal =1,'E',;
    'I'))
  endfunc
  func oRFINTEST_2_28.GetRadio()
    this.Parent.oContained.w_RFINTEST = this.RadioValue()
    return .t.
  endfunc

  func oRFINTEST_2_28.ToRadio()
    this.Parent.oContained.w_RFINTEST=trim(this.Parent.oContained.w_RFINTEST)
    return(;
      iif(this.Parent.oContained.w_RFINTEST=='E',1,;
      0))
  endfunc

  func oRFINTEST_2_28.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oRFTMPCIC_2_29 as StdTrsField with uid="RGDDLKHAPS",rtseq=34,rtrep=.t.,;
    cFormVar="w_RFTMPCIC",value=0,;
    ToolTipText = "Tempo di battuta",;
    HelpContextID = 242686375,;
    cTotal="", bFixedPos=.t., cQueryName = "RFTMPCIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=74, Left=761, Top=412

  func oRFTMPCIC_2_29.mCond()
    with this.Parent.oContained
      return (.w_RFQTARIS>0 and not empty(.w_RFUMTRIS) AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  add object oRFPROORA_2_31 as StdTrsField with uid="NUQRJHKBPH",rtseq=36,rtrep=.t.,;
    cFormVar="w_RFPROORA",value=0,enabled=.f.,;
    ToolTipText = "Produzione oraria della risorsa",;
    HelpContextID = 226338391,;
    cTotal="", bFixedPos=.t., cQueryName = "RFPROORA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=107, Left=727, Top=436, cSayPict=["@Z 9999999999.9999"], cGetPict=["@Z 9999999999.9999"]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsco_mrfBodyRow as CPBodyRowCnt
  Width=788
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="TYPMNYNACZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Ordinamento",;
    HelpContextID = 234540906,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=46, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oRFCODCLA_2_2 as StdTrsField with uid="MRBTJIDYNA",rtseq=7,rtrep=.t.,;
    cFormVar="w_RFCODCLA",value=space(5),;
    ToolTipText = "Classe risorsa",;
    HelpContextID = 255207849,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=47, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLR_DETT", oKey_1_1="CSFLAPRE", oKey_1_2="this.w_CSFLAPRE", oKey_2_1="CSCODCLA", oKey_2_2="this.w_RFCODCLA"

  func oRFCODCLA_2_2.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRFCODCLA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCODCLA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRFCODCLA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLR_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStrODBC(this.Parent.oContained.w_CSFLAPRE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStr(this.Parent.oContained.w_CSFLAPRE)
    endif
    do cp_zoom with 'CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(this.parent,'oRFCODCLA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCI_MCS.CLR_DETT_VZM',this.parent.oContained
  endproc

  add object oRF__TIPO_2_3 as stdTrsTableCombo with uid="HAVEZSUYDN",rtrep=.t.,;
    cFormVar="w_RF__TIPO", tablefilter="" , ;
    ToolTipText = "Tipo risorsa",;
    HelpContextID = 131831397,;
    Height=22, Width=112, Left=113, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , cDescEmptyElement='Seleziona il tipo';
, bIsInHeader=.f.;
    , cTable='..\PRFA\EXE\QUERY\GSCI_QLT.vqr',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.



  func oRF__TIPO_2_3.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRF__TIPO_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_RFCODICE)
        bRes2=.link_2_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oRFCODICE_2_10 as StdTrsField with uid="SYMJOZRMZL",rtseq=15,rtrep=.t.,;
    cFormVar="w_RFCODICE",value=space(20),;
    ToolTipText = "Codice risorsa",;
    HelpContextID = 113890907,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inesistente",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=228, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_RF__TIPO", oKey_2_1="RLCODICE", oKey_2_2="this.w_RFCODICE"

  func oRFCODICE_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  func oRFCODICE_2_10.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=!Empty(.w_RF__TIPO)
    endwith
    return i_bres
  endfunc

  proc oRFCODICE_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRFCODICE_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_RF__TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_RF__TIPO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oRFCODICE_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"RISORSE",'GSCO_ZLR.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oRFQTARIS_2_13 as StdTrsField with uid="JSATEIPKTK",rtseq=18,rtrep=.t.,;
    cFormVar="w_RFQTARIS",value=0,;
    ToolTipText = "Quantit�",;
    HelpContextID = 6310295,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=33, Left=519, Top=0, cSayPict=["@Z 999.99"], cGetPict=["99999.99"]

  func oRFQTARIS_2_13.mCond()
    with this.Parent.oContained
      return (.w_EDITROW Or .w_EDITCHK)
    endwith
  endfunc

  func oRFQTARIS_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_RF__TIPO<>'CL' and .w_RFQTARIS<=.w_QTARIS) or (.w_RF__TIPO='CL' and .w_RFQTARIS<=1)))
    endwith
    return bRes
  endfunc

  add object oRFUMTRIS_2_14 as StdTrsField with uid="ZEILBJIQDR",rtseq=19,rtrep=.t.,;
    cFormVar="w_RFUMTRIS",value=space(3),;
    ToolTipText = "U.M.",;
    HelpContextID = 255265175,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=48, Left=560, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_RFUMTRIS"

  proc oRFUMTRIS_2_14.mDefault
    with this.Parent.oContained
      if empty(.w_RFUMTRIS)
        .w_RFUMTRIS = .w_UMTDEF
      endif
    endwith
  endproc

  func oRFUMTRIS_2_14.mCond()
    with this.Parent.oContained
      return (.w_RFQTARIS>0 AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oRFUMTRIS_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFUMTRIS_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRFUMTRIS_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oRFUMTRIS_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"UNITA' DI MISURA TEMPO",'GSCO_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oRFTEMRIS_2_15 as StdTrsField with uid="QQJNIFPZKS",rtseq=20,rtrep=.t.,;
    cFormVar="w_RFTEMRIS",value=0,;
    ToolTipText = "Tempo",;
    HelpContextID = 263133591,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=611, Top=0, cSayPict=["@Z 99,999.999"], cGetPict=["@Z 99999.999"]

  func oRFTEMRIS_2_15.mCond()
    with this.Parent.oContained
      return (.w_RFQTARIS>0 and not empty(.w_RFUMTRIS) AND (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  add object oRFTEMSEC_2_16 as StdTrsField with uid="YPBYPFNLSU",rtseq=21,rtrep=.t.,;
    cFormVar="w_RFTEMSEC",value=0,enabled=.f.,;
    HelpContextID = 246356391,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=700, Top=0, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oRFTIPTEM_2_33 as StdTrsCombo with uid="NDAIYCUQEF",rtrep=.t.,;
    cFormVar="w_RFTIPTEM", RowSource=""+"Lavorazione,"+"Setup,"+"Warm Up" , ;
    ToolTipText = "Tipo di tempo della risorsa",;
    HelpContextID = 226171293,;
    Height=21, Width=127, Left=387, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRFTIPTEM_2_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RFTIPTEM,&i_cF..t_RFTIPTEM),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'S',;
    iif(xVal =3,'W',;
    space(1)))))
  endfunc
  func oRFTIPTEM_2_33.GetRadio()
    this.Parent.oContained.w_RFTIPTEM = this.RadioValue()
    return .t.
  endfunc

  func oRFTIPTEM_2_33.ToRadio()
    this.Parent.oContained.w_RFTIPTEM=trim(this.Parent.oContained.w_RFTIPTEM)
    return(;
      iif(this.Parent.oContained.w_RFTIPTEM=='L',1,;
      iif(this.Parent.oContained.w_RFTIPTEM=='S',2,;
      iif(this.Parent.oContained.w_RFTIPTEM=='W',3,;
      0))))
  endfunc

  func oRFTIPTEM_2_33.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mrf','ODL_RISF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RFCODODL=ODL_RISF.RFCODODL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
