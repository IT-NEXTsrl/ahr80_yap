* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_stm                                                        *
*              Tracciabilità matricole produzione                              *
*                                                                              *
*      Author: ZUCCHETTI SPA (CF)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_150]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-17                                                      *
* Last revis.: 2008-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_stm",oParentObject))

* --- Class definition
define class tgsco_stm as StdForm
  Top    = 2
  Left   = 7

  * --- Standard Properties
  Width  = 557
  Height = 247+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-18"
  HelpContextID=194402967
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  MATRICOL_IDX = 0
  LOTTIART_IDX = 0
  ODL_MAST_IDX = 0
  DIC_PROD_IDX = 0
  KEY_ARTI_IDX = 0
  DOC_MAST_IDX = 0
  PAR_PROD_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsco_stm"
  cComment = "Tracciabilità matricole produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_DESINI = space(40)
  w_CODFIN = space(20)
  o_CODFIN = space(20)
  w_DESFIN = space(40)
  w_MATINI = space(40)
  o_MATINI = space(40)
  w_MATFIN = space(40)
  o_MATFIN = space(40)
  w_LOTINI = space(20)
  o_LOTINI = space(20)
  w_LOTFIN = space(20)
  w_ODLINI = space(15)
  o_ODLINI = space(15)
  w_ODLFIN = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_COCINI = space(20)
  o_COCINI = space(20)
  w_COCFIN = space(20)
  o_COCFIN = space(20)
  w_MACINI = space(40)
  o_MACINI = space(40)
  w_MACFIN = space(40)
  o_MACFIN = space(40)
  w_LOCINI = space(20)
  o_LOCINI = space(20)
  w_LOCFIN = space(20)
  w_DECINI = space(40)
  w_DECFIN = space(40)
  w_DICINI = space(10)
  o_DICINI = space(10)
  w_DICFIN = space(10)
  w_MOVINI = 0
  o_MOVINI = 0
  w_MOVFIN = 0
  w_DTDINI = ctod('  /  /  ')
  o_DTDINI = ctod('  /  /  ')
  w_DTDFIN = ctod('  /  /  ')
  w_READPAR = space(10)
  w_MGSCAR = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_stmPag1","gsco_stm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni sugli ODL")
      .Pages(2).addobject("oPag","tgsco_stmPag2","gsco_stm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni sui componenti")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='MATRICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='ODL_MAST'
    this.cWorkTables[4]='DIC_PROD'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='PAR_PROD'
    this.cWorkTables[8]='ART_ICOL'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_MATINI=space(40)
      .w_MATFIN=space(40)
      .w_LOTINI=space(20)
      .w_LOTFIN=space(20)
      .w_ODLINI=space(15)
      .w_ODLFIN=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_COCINI=space(20)
      .w_COCFIN=space(20)
      .w_MACINI=space(40)
      .w_MACFIN=space(40)
      .w_LOCINI=space(20)
      .w_LOCFIN=space(20)
      .w_DECINI=space(40)
      .w_DECFIN=space(40)
      .w_DICINI=space(10)
      .w_DICFIN=space(10)
      .w_MOVINI=0
      .w_MOVFIN=0
      .w_DTDINI=ctod("  /  /  ")
      .w_DTDFIN=ctod("  /  /  ")
      .w_READPAR=space(10)
      .w_MGSCAR=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODFIN = .w_CODINI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_MATINI = space(40)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_MATINI))
          .link_1_5('Full')
        endif
        .w_MATFIN = .w_MATINI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MATFIN))
          .link_1_6('Full')
        endif
        .w_LOTINI = space(20)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_LOTINI))
          .link_1_7('Full')
        endif
        .w_LOTFIN = .w_LOTINI
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_LOTFIN))
          .link_1_8('Full')
        endif
        .w_ODLINI = space(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ODLINI))
          .link_1_9('Full')
        endif
        .w_ODLFIN = .w_ODLINI
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ODLFIN))
          .link_1_10('Full')
        endif
        .w_OBTEST = i_datsys
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_COCINI))
          .link_2_1('Full')
        endif
        .w_COCFIN = .w_COCINI
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_COCFIN))
          .link_2_2('Full')
        endif
        .w_MACINI = space(40)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MACINI))
          .link_2_3('Full')
        endif
        .w_MACFIN = .w_MACINI
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MACFIN))
          .link_2_4('Full')
        endif
        .w_LOCINI = space(20)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_LOCINI))
          .link_2_5('Full')
        endif
        .w_LOCFIN = .w_LOCINI
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_LOCFIN))
          .link_2_6('Full')
        endif
        .DoRTCalc(18,20,.f.)
        if not(empty(.w_DICINI))
          .link_2_15('Full')
        endif
        .w_DICFIN = .w_DICINI
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_DICFIN))
          .link_2_16('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MOVINI))
          .link_2_17('Full')
        endif
        .w_MOVFIN = .w_MOVINI
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_MOVFIN))
          .link_2_18('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_DTDFIN = .w_DTDINI
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_READPAR = 'PP'
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_READPAR))
          .link_1_24('Full')
        endif
    endwith
    this.DoRTCalc(27,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.t.)
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN
            .w_MATINI = space(40)
          .link_1_5('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_MATINI<>.w_MATINI
            .w_MATFIN = .w_MATINI
          .link_1_6('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_MATINI<>.w_MATINI.or. .o_MATFIN<>.w_MATFIN
            .w_LOTINI = space(20)
          .link_1_7('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_MATINI<>.w_MATINI.or. .o_MATFIN<>.w_MATFIN.or. .o_LOTINI<>.w_LOTINI
            .w_LOTFIN = .w_LOTINI
          .link_1_8('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN
            .w_ODLINI = space(15)
          .link_1_9('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_ODLINI<>.w_ODLINI
            .w_ODLFIN = .w_ODLINI
          .link_1_10('Full')
        endif
        .DoRTCalc(11,12,.t.)
        if .o_COCINI<>.w_COCINI
            .w_COCFIN = .w_COCINI
          .link_2_2('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN
            .w_MACINI = space(40)
          .link_2_3('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN.or. .o_MACINI<>.w_MACINI
            .w_MACFIN = .w_MACINI
          .link_2_4('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN.or. .o_MACFIN<>.w_MACFIN.or. .o_MACINI<>.w_MACINI
            .w_LOCINI = space(20)
          .link_2_5('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN.or. .o_MACINI<>.w_MACINI.or. .o_MACFIN<>.w_MACFIN.or. .o_LOCINI<>.w_LOCINI
            .w_LOCFIN = .w_LOCINI
          .link_2_6('Full')
        endif
        .DoRTCalc(18,20,.t.)
        if .o_DICINI<>.w_DICINI
            .w_DICFIN = .w_DICINI
          .link_2_16('Full')
        endif
        .DoRTCalc(22,22,.t.)
        if .o_MOVINI<>.w_MOVINI
            .w_MOVFIN = .w_MOVINI
          .link_2_18('Full')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_DTDINI<>.w_DTDINI
            .w_DTDFIN = .w_DTDINI
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
            .w_READPAR = 'PP'
          .link_1_24('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOTINI_1_7.enabled = this.oPgFrm.Page1.oPag.oLOTINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLOTFIN_1_8.enabled = this.oPgFrm.Page1.oPag.oLOTFIN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'',"Codici di ricerca",'TRACPROD.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'',"Codici di ricerca",'TRACPROD.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATINI))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATINI)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATINI) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATINI_1_5'),i_cWhere,'',"Matricole",'gsco1ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATINI)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATINI = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATINI = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MATINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATFIN))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATFIN)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATFIN) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATFIN_1_6'),i_cWhere,'',"Matricole",'gsco1ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATFIN)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATFIN = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATFIN = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MATFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTINI)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODINI;
                     ,'LOCODICE',trim(this.w_LOTINI))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTINI)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTINI) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTINI_1_7'),i_cWhere,'',"Lotti",'gsco3ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTINI);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODINI;
                       ,'LOCODICE',this.w_LOTINI)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTINI = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTINI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOTINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTFIN)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODINI;
                     ,'LOCODICE',trim(this.w_LOTFIN))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTFIN)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTFIN) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTFIN_1_8'),i_cWhere,'',"Lotti",'gsco3ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTFIN);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODINI;
                       ,'LOCODICE',this.w_LOTFIN)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTFIN = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTFIN = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOTFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLINI_1_9'),i_cWhere,'',"ODL",'gsco_ztm.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ODLINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI <= .w_ODLFIN OR EMPTY(.w_ODLFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLFIN_1_10'),i_cWhere,'',"ODL",'gsco_ztm.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ODLFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI <= .w_ODLFIN OR EMPTY(.w_ODLINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCINI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COCINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_COCINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_COCINI)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOCINI_2_1'),i_cWhere,'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COCINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCINI = NVL(_Link_.CACODICE,space(20))
      this.w_DECINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCINI = space(20)
      endif
      this.w_DECINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COCINI<=.w_COCFIN or empty(.w_COCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_COCINI = space(20)
        this.w_DECINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCFIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COCFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_COCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_COCFIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOCFIN_2_2'),i_cWhere,'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COCFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DECFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCFIN = space(20)
      endif
      this.w_DECFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COCINI<=.w_COCFIN or empty(.w_COCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_COCFIN = space(20)
        this.w_DECFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACINI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MACINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MACINI))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACINI)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACINI) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMACINI_2_3'),i_cWhere,'',"Matricole",'gsco2ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MACINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MACINI)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACINI = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MACINI = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MACINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACFIN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MACFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MACFIN))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACFIN)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACFIN) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMACFIN_2_4'),i_cWhere,'',"Matricole",'gsco2ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MACFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MACFIN)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACFIN = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MACFIN = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MACFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCINI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_LOCINI))
          select LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOCINI)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOCINI) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oLOCINI_2_5'),i_cWhere,'',"Lotti",'gsco4ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_LOCINI)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCINI = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOCINI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOCINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCFIN
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_LOCFIN))
          select LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOCFIN)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOCFIN) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oLOCFIN_2_6'),i_cWhere,'',"Lotti",'gsco4ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_LOCFIN)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCFIN = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOCFIN = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOCFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICINI
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_lTable = "DIC_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2], .t., this.DIC_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIC_PROD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPSERIAL like "+cp_ToStrODBC(trim(this.w_DICINI)+"%");

          i_ret=cp_SQL(i_nConn,"select DPSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPSERIAL',trim(this.w_DICINI))
          select DPSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICINI)==trim(_Link_.DPSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICINI) and !this.bDontReportError
            deferred_cp_zoom('DIC_PROD','*','DPSERIAL',cp_AbsName(oSource.parent,'oDICINI_2_15'),i_cWhere,'',"Dichiarazioni di produzione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where DPSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPSERIAL',oSource.xKey(1))
            select DPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where DPSERIAL="+cp_ToStrODBC(this.w_DICINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPSERIAL',this.w_DICINI)
            select DPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICINI = NVL(_Link_.DPSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DICINI = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DICINI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])+'\'+cp_ToStr(_Link_.DPSERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICFIN
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_lTable = "DIC_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2], .t., this.DIC_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIC_PROD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPSERIAL like "+cp_ToStrODBC(trim(this.w_DICFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DPSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPSERIAL',trim(this.w_DICFIN))
          select DPSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICFIN)==trim(_Link_.DPSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICFIN) and !this.bDontReportError
            deferred_cp_zoom('DIC_PROD','*','DPSERIAL',cp_AbsName(oSource.parent,'oDICFIN_2_16'),i_cWhere,'',"Dichiarazioni di produzione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where DPSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPSERIAL',oSource.xKey(1))
            select DPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where DPSERIAL="+cp_ToStrODBC(this.w_DICFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPSERIAL',this.w_DICFIN)
            select DPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICFIN = NVL(_Link_.DPSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DICFIN = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DICFIN = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])+'\'+cp_ToStr(_Link_.DPSERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOVINI
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOVINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DOC_MAST')
        if i_nConn<>0
          i_cWhere = " MVNUMREG="+cp_ToStrODBC(this.w_MOVINI);

          i_ret=cp_SQL(i_nConn,"select MVNUMREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MVNUMREG',this.w_MOVINI)
          select MVNUMREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_MOVINI) and !this.bDontReportError
            deferred_cp_zoom('DOC_MAST','*','MVNUMREG',cp_AbsName(oSource.parent,'oMOVINI_2_17'),i_cWhere,'',"Documenti interni",'gsco5ztm.DOC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVNUMREG";
                     +" from "+i_cTable+" "+i_lTable+" where MVNUMREG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVNUMREG',oSource.xKey(1))
            select MVNUMREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOVINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVNUMREG";
                   +" from "+i_cTable+" "+i_lTable+" where MVNUMREG="+cp_ToStrODBC(this.w_MOVINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVNUMREG',this.w_MOVINI)
            select MVNUMREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOVINI = NVL(_Link_.MVNUMREG,0)
    else
      if i_cCtrl<>'Load'
        this.w_MOVINI = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MOVINI <= .w_MOVFIN OR EMPTY(.w_MOVFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOVINI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVNUMREG,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOVINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOVFIN
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOVFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DOC_MAST')
        if i_nConn<>0
          i_cWhere = " MVNUMREG="+cp_ToStrODBC(this.w_MOVFIN);

          i_ret=cp_SQL(i_nConn,"select MVNUMREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MVNUMREG',this.w_MOVFIN)
          select MVNUMREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_MOVFIN) and !this.bDontReportError
            deferred_cp_zoom('DOC_MAST','*','MVNUMREG',cp_AbsName(oSource.parent,'oMOVFIN_2_18'),i_cWhere,'',"Documenti interni",'gsco5ztm.DOC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVNUMREG";
                     +" from "+i_cTable+" "+i_lTable+" where MVNUMREG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVNUMREG',oSource.xKey(1))
            select MVNUMREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOVFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVNUMREG";
                   +" from "+i_cTable+" "+i_lTable+" where MVNUMREG="+cp_ToStrODBC(this.w_MOVFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVNUMREG',this.w_MOVFIN)
            select MVNUMREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOVFIN = NVL(_Link_.MVNUMREG,0)
    else
      if i_cCtrl<>'Load'
        this.w_MOVFIN = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MOVINI <= .w_MOVFIN OR EMPTY(.w_MOVINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOVFIN = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVNUMREG,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOVFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPMAGSCA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPMAGSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(10))
      this.w_MGSCAR = NVL(_Link_.PPMAGSCA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_MGSCAR = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_2.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_2.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATINI_1_5.value==this.w_MATINI)
      this.oPgFrm.Page1.oPag.oMATINI_1_5.value=this.w_MATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMATFIN_1_6.value==this.w_MATFIN)
      this.oPgFrm.Page1.oPag.oMATFIN_1_6.value=this.w_MATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTINI_1_7.value==this.w_LOTINI)
      this.oPgFrm.Page1.oPag.oLOTINI_1_7.value=this.w_LOTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTFIN_1_8.value==this.w_LOTFIN)
      this.oPgFrm.Page1.oPag.oLOTFIN_1_8.value=this.w_LOTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oODLINI_1_9.value==this.w_ODLINI)
      this.oPgFrm.Page1.oPag.oODLINI_1_9.value=this.w_ODLINI
    endif
    if not(this.oPgFrm.Page1.oPag.oODLFIN_1_10.value==this.w_ODLFIN)
      this.oPgFrm.Page1.oPag.oODLFIN_1_10.value=this.w_ODLFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCINI_2_1.value==this.w_COCINI)
      this.oPgFrm.Page2.oPag.oCOCINI_2_1.value=this.w_COCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCFIN_2_2.value==this.w_COCFIN)
      this.oPgFrm.Page2.oPag.oCOCFIN_2_2.value=this.w_COCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMACINI_2_3.value==this.w_MACINI)
      this.oPgFrm.Page2.oPag.oMACINI_2_3.value=this.w_MACINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMACFIN_2_4.value==this.w_MACFIN)
      this.oPgFrm.Page2.oPag.oMACFIN_2_4.value=this.w_MACFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCINI_2_5.value==this.w_LOCINI)
      this.oPgFrm.Page2.oPag.oLOCINI_2_5.value=this.w_LOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCFIN_2_6.value==this.w_LOCFIN)
      this.oPgFrm.Page2.oPag.oLOCFIN_2_6.value=this.w_LOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDECINI_2_7.value==this.w_DECINI)
      this.oPgFrm.Page2.oPag.oDECINI_2_7.value=this.w_DECINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDECFIN_2_8.value==this.w_DECFIN)
      this.oPgFrm.Page2.oPag.oDECFIN_2_8.value=this.w_DECFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDICINI_2_15.value==this.w_DICINI)
      this.oPgFrm.Page2.oPag.oDICINI_2_15.value=this.w_DICINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDICFIN_2_16.value==this.w_DICFIN)
      this.oPgFrm.Page2.oPag.oDICFIN_2_16.value=this.w_DICFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMOVINI_2_17.value==this.w_MOVINI)
      this.oPgFrm.Page2.oPag.oMOVINI_2_17.value=this.w_MOVINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMOVFIN_2_18.value==this.w_MOVFIN)
      this.oPgFrm.Page2.oPag.oMOVFIN_2_18.value=this.w_MOVFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDTDINI_2_19.value==this.w_DTDINI)
      this.oPgFrm.Page2.oPag.oDTDINI_2_19.value=this.w_DTDINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDTDFIN_2_20.value==this.w_DTDFIN)
      this.oPgFrm.Page2.oPag.oDTDFIN_2_20.value=this.w_DTDFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATFIN))  and not(empty(.w_MATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMATINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATINI))  and not(empty(.w_MATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMATFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTFIN))  and (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)  and not(empty(.w_LOTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTINI))  and (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)  and not(empty(.w_LOTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLINI <= .w_ODLFIN OR EMPTY(.w_ODLFIN))  and not(empty(.w_ODLINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLINI <= .w_ODLFIN OR EMPTY(.w_ODLINI))  and not(empty(.w_ODLFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COCINI<=.w_COCFIN or empty(.w_COCFIN))  and not(empty(.w_COCINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCINI_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_COCINI<=.w_COCFIN or empty(.w_COCINI))  and not(empty(.w_COCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCFIN_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACFIN))  and not(empty(.w_MACINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMACINI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACINI))  and not(empty(.w_MACFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMACFIN_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCFIN))  and not(empty(.w_LOCINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLOCINI_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCINI))  and not(empty(.w_LOCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLOCFIN_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICFIN))  and not(empty(.w_DICINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDICINI_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICINI))  and not(empty(.w_DICFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDICFIN_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MOVINI <= .w_MOVFIN OR EMPTY(.w_MOVFIN))  and not(empty(.w_MOVINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOVINI_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MOVINI <= .w_MOVFIN OR EMPTY(.w_MOVINI))  and not(empty(.w_MOVFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOVFIN_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDTDINI_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDTDFIN_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_MATINI = this.w_MATINI
    this.o_MATFIN = this.w_MATFIN
    this.o_LOTINI = this.w_LOTINI
    this.o_ODLINI = this.w_ODLINI
    this.o_COCINI = this.w_COCINI
    this.o_COCFIN = this.w_COCFIN
    this.o_MACINI = this.w_MACINI
    this.o_MACFIN = this.w_MACFIN
    this.o_LOCINI = this.w_LOCINI
    this.o_DICINI = this.w_DICINI
    this.o_MOVINI = this.w_MOVINI
    this.o_DTDINI = this.w_DTDINI
    return

enddefine

* --- Define pages as container
define class tgsco_stmPag1 as StdContainer
  Width  = 553
  height = 247
  stdWidth  = 553
  stdheight = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="SZEKJHCVBQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 104600026,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=99, Top=17, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_LOTINI)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_LOTFIN)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACPROD.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_2 as StdField with uid="UWDLPXXTHQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104541130,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=262, Top=17, InputMask=replicate('X',40)

  add object oCODFIN_1_3 as StdField with uid="KAOZXLFOUP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 26153434,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=99, Top=41, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACPROD.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESFIN_1_4 as StdField with uid="MYLSJYYKJE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26094538,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=262, Top=41, InputMask=replicate('X',40)

  add object oMATINI_1_5 as StdField with uid="BAUZFWPTWC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MATINI", cQueryName = "MATINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di inizio selezione",;
    HelpContextID = 104537914,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=99, Top=65, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATINI"

  func oMATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco1ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oMATFIN_1_6 as StdField with uid="GHDDKOKVIG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MATFIN", cQueryName = "MATFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di fine selezione",;
    HelpContextID = 26091322,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=99, Top=89, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATFIN"

  func oMATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco1ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oLOTINI_1_7 as StdField with uid="VYQYSTJFCH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LOTINI", cQueryName = "LOTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di inizio selezione",;
    HelpContextID = 104534346,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=99, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODINI", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTINI"

  func oLOTINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)
    endwith
   endif
  endfunc

  func oLOTINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODINI)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco3ztm.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oLOTFIN_1_8 as StdField with uid="BKTBROMCMD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LOTFIN", cQueryName = "LOTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di fine selezione",;
    HelpContextID = 26087754,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=388, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODINI", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTFIN"

  func oLOTFIN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)
    endwith
   endif
  endfunc

  func oLOTFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODINI)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco3ztm.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oODLINI_1_9 as StdField with uid="YGJOJOEPWB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ODLINI", cQueryName = "ODLINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ODL di inizio selezione",;
    HelpContextID = 104569882,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=99, Top=137, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLINI"

  func oODLINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ODL",'gsco_ztm.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oODLFIN_1_10 as StdField with uid="CJSJLEAEBM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ODLFIN", cQueryName = "ODLFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ODL di fine selezione",;
    HelpContextID = 26123290,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=388, Top=137, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLFIN"

  func oODLFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ODL",'gsco_ztm.ODL_MAST_VZM',this.parent.oContained
  endproc


  add object oBtn_1_21 as StdButton with uid="HZUGTMEYRQ",left=452, top=199, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare movimenti matricole";
    , HelpContextID = 200678362;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oObj_1_22 as cp_outputCombo with uid="ZLERXIVPWT",left=99, top=169, width=414,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 164470298


  add object oBtn_1_23 as StdButton with uid="FRSAGQFIZA",left=502, top=199, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 201720390;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="JHACHFBEZX",Visible=.t., Left=0, Top=17,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CGICWDOWMU",Visible=.t., Left=0, Top=41,;
    Alignment=1, Width=96, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=0, Top=65,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=0, Top=113,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BETNLUNOYI",Visible=.t., Left=0, Top=89,;
    Alignment=1, Width=96, Height=15,;
    Caption="A matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=313, Top=113,;
    Alignment=1, Width=71, Height=15,;
    Caption="A lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PGZKNKALVF",Visible=.t., Left=0, Top=169,;
    Alignment=1, Width=96, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RKDHEXEJHY",Visible=.t., Left=0, Top=137,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="VMGPYDJXMT",Visible=.t., Left=313, Top=137,;
    Alignment=1, Width=71, Height=15,;
    Caption="A ODL:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsco_stmPag2 as StdContainer
  Width  = 553
  height = 247
  stdWidth  = 553
  stdheight = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCINI_2_1 as StdField with uid="IQFGQMWCZO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COCINI", cQueryName = "COCINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 104604122,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=100, Top=17, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_COCINI"

  func oCOCINI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCINI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCINI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOCINI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oCOCFIN_2_2 as StdField with uid="TBUXLZLSFY",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COCFIN", cQueryName = "COCFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 26157530,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=100, Top=41, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_COCFIN"

  func oCOCFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCFIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCFIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOCFIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oMACINI_2_3 as StdField with uid="MXBHNACTEW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MACINI", cQueryName = "MACINI",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di inizio selezione",;
    HelpContextID = 104607546,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=100, Top=65, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MACINI"

  func oMACINI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACINI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACINI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMACINI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco2ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oMACFIN_2_4 as StdField with uid="EXIOWJGCBL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MACFIN", cQueryName = "MACFIN",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di fine selezione",;
    HelpContextID = 26160954,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=100, Top=89, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MACFIN"

  func oMACFIN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACFIN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACFIN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMACFIN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco2ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oLOCINI_2_5 as StdField with uid="YGCZPUZYWL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_LOCINI", cQueryName = "LOCINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di inizio selezione",;
    HelpContextID = 104603978,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=100, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODICE", oKey_1_2="this.w_LOCINI"

  func oLOCINI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCINI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCINI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOTTIART','*','LOCODICE',cp_AbsName(this.parent,'oLOCINI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco4ztm.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oLOCFIN_2_6 as StdField with uid="TGZPADMXNT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_LOCFIN", cQueryName = "LOCFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di fine selezione",;
    HelpContextID = 26157386,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=389, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODICE", oKey_1_2="this.w_LOCFIN"

  func oLOCFIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCFIN_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCFIN_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOTTIART','*','LOCODICE',cp_AbsName(this.parent,'oLOCFIN_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco4ztm.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oDECINI_2_7 as StdField with uid="ANDVBHJWDX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DECINI", cQueryName = "DECINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104606666,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=263, Top=17, InputMask=replicate('X',40)

  add object oDECFIN_2_8 as StdField with uid="NWKNSBGIQH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DECFIN", cQueryName = "DECFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26160074,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=263, Top=41, InputMask=replicate('X',40)

  add object oDICINI_2_15 as StdField with uid="WQNLFQEXDD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DICINI", cQueryName = "DICINI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Dichiarazione di produzione di inizio selezione",;
    HelpContextID = 104605642,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=100, Top=170, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="DIC_PROD", oKey_1_1="DPSERIAL", oKey_1_2="this.w_DICINI"

  func oDICINI_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICINI_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICINI_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIC_PROD','*','DPSERIAL',cp_AbsName(this.parent,'oDICINI_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dichiarazioni di produzione",'',this.parent.oContained
  endproc

  add object oDICFIN_2_16 as StdField with uid="WHQQEZYTLE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DICFIN", cQueryName = "DICFIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Dichiarazione di produzione di fine selezione",;
    HelpContextID = 26159050,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=100, Top=192, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="DIC_PROD", oKey_1_1="DPSERIAL", oKey_1_2="this.w_DICFIN"

  func oDICFIN_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICFIN_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICFIN_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIC_PROD','*','DPSERIAL',cp_AbsName(this.parent,'oDICFIN_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dichiarazioni di produzione",'',this.parent.oContained
  endproc

  add object oMOVINI_2_17 as StdField with uid="HQHVDGLJKG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MOVINI", cQueryName = "MOVINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Documento interno di inizio selezione",;
    HelpContextID = 104526138,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=273, Top=170, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="DOC_MAST", oKey_1_1="MVNUMREG", oKey_1_2="this.w_MOVINI"

  func oMOVINI_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOVINI_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOVINI_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DOC_MAST','*','MVNUMREG',cp_AbsName(this.parent,'oMOVINI_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Documenti interni",'gsco5ztm.DOC_MAST_VZM',this.parent.oContained
  endproc

  add object oMOVFIN_2_18 as StdField with uid="ETHPIEMRQR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MOVFIN", cQueryName = "MOVFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Documento interno di fine selezione",;
    HelpContextID = 26079546,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=273, Top=192, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="DOC_MAST", oKey_1_1="MVNUMREG", oKey_1_2="this.w_MOVFIN"

  func oMOVFIN_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOVFIN_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOVFIN_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DOC_MAST','*','MVNUMREG',cp_AbsName(this.parent,'oMOVFIN_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Documenti interni",'gsco5ztm.DOC_MAST_VZM',this.parent.oContained
  endproc

  add object oDTDINI_2_19 as StdField with uid="AUCHYPCWKM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DTDINI", cQueryName = "DTDINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dichiarazione di produzione di inizio selezione",;
    HelpContextID = 104598730,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=463, Top=170

  func oDTDINI_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDFIN))
    endwith
    return bRes
  endfunc

  add object oDTDFIN_2_20 as StdField with uid="UOJAWAEHVO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DTDFIN", cQueryName = "DTDFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dichiarazione di produzione di fine selezione",;
    HelpContextID = 26152138,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=463, Top=192

  func oDTDFIN_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDINI))
    endwith
    return bRes
  endfunc

  add object oStr_2_9 as StdString with uid="HGLLLTOCVE",Visible=.t., Left=2, Top=17,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="CTZMIIHWUH",Visible=.t., Left=2, Top=41,;
    Alignment=1, Width=94, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="VKKHKJCAEV",Visible=.t., Left=2, Top=65,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="INDZOCTWXR",Visible=.t., Left=325, Top=113,;
    Alignment=1, Width=60, Height=15,;
    Caption="A lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="ICHJGVJLAW",Visible=.t., Left=2, Top=89,;
    Alignment=1, Width=94, Height=15,;
    Caption="A matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="PNYCXUFOTG",Visible=.t., Left=2, Top=113,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=2, Top=170,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da dich. prod.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=2, Top=192,;
    Alignment=1, Width=94, Height=15,;
    Caption="A dich. prod.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="CSUDQKLXHQ",Visible=.t., Left=369, Top=170,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da data dich.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="QVXSTVYWUB",Visible=.t., Left=369, Top=192,;
    Alignment=1, Width=90, Height=15,;
    Caption="A data dich.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="LTENIIBGWH",Visible=.t., Left=190, Top=170,;
    Alignment=1, Width=79, Height=15,;
    Caption="Da doc.int:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="IKFKEWCTKN",Visible=.t., Left=190, Top=192,;
    Alignment=1, Width=79, Height=15,;
    Caption="A doc.int.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="WMRGDURNBL",Visible=.t., Left=20, Top=143,;
    Alignment=0, Width=157, Height=18,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oBox_2_28 as StdBox with uid="NJTNMJACPG",left=9, top=162, width=542,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_stm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
