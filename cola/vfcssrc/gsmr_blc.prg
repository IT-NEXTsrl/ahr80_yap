* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_blc                                                        *
*              Calcolo LLC                                                     *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_76]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-07-26                                                      *
* Last revis.: 2014-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_blc",oParentObject)
return(i_retval)

define class tgsmr_blc as StdBatch
  * --- Local variables
  w_MAXLLC = 0
  w_LLC = 0
  w_MESS = space(10)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_KEYVAR = space(20)
  w_KEYSAL = space(40)
  w_NRECELAB = 0
  w_NRECCUR = 0
  Padre = .NULL.
  w_PathRel = space(50)
  w_PARAM = space(10)
  w_bTITLE = space(40)
  w_ROOTCLASS = space(5)
  w_UseProgBar = .f.
  w_MESS1 = space(10)
  w_KEYRIF = space(10)
  OutCursor = space(10)
  w_QTAEXP = 0
  w_first = .f.
  * --- WorkFile variables
  ART_PROD_idx=0
  KEY_ARTI_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Calcolo del Low Level Code
    this.Padre = this.oParentObject
    this.w_ROOTCLASS = IIF(IsAhe(), "TGSMR", "TGSCO")
    do case
      case type("this.Padre")="O"
        this.w_PARAM = alltrim(upper(this.Padre.Class))
      otherwise
        i_retcode = 'stop'
        return
    endcase
    if this.w_PARAM = "TGSMR_BGP"
      this.w_UseProgBar = g_UseProgBar and vartype(this.Padre.w_UseProgBar)="L" and this.Padre.w_UseProgBar
      this.w_bTITLE = "Calcolo low level code in corso..."
    else
      this.w_UseProgBar = g_UseProgBar and vartype(this.Padre.w_PROGBAR)="O"
      if this.w_UseProgBar
        * --- Assegno alla variabile pubblica l'oggetto Padre chiamante
        i_PROGBAR = this.Padre
      endif
    endif
    if this.w_UseProgBar
      LOCAL L_SETCURSOR
      L_SETCURSOR = SET("CURSOR")
      SET CURSOR OFF
    endif
    this.w_PathRel = IIF( IsAhe() , "..\DIBA\EXE\QUERY\", "..\COLA\EXE\QUERY\")
    this.w_MAXLLC = 200
    this.w_LLC = 1
    if g_MMRP="S"
      if this.w_UseProgBar
        * --- Imposta La Progress Bar
        GesProgBar("M", this, 0, 0, this.w_bTITLE )
        this.w_NRECELAB = 15
      else
        ah_Msg("Calcolo low level code in corso...",.T.)
      endif
      * --- Legami delle distinte basi
      vq_exec( this.w_PathRel + "gsmr_blc", this, "Legami")
      * --- Valore di LLC attualmente assegnato
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
      endif
      vq_exec( this.w_PathRel + "gsmr1blc", this, "Art_prod")
      * --- Articoli con LLC=0 in anagrafica
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
      endif
      vq_exec( this.w_PathRel + "gsmr2blc", this, "LLCzero")
      * --- Codici con LLC=0 (tutti quei codici che non hanno un padre)
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
      endif
      CREATE CURSOR Val_Def (vdkeysal Char(40))
      * --- Creo il Cursore contenente tutti gli Articoli per il quale verranno
      *     inseriti i valori di default in ART_PROD (solo AHE)
      Select distinct CORIFDIS as COCODCOM from Legami ; 
 where CORIFDIS not in (select distinct COCODCOM from Legami) ; 
 union Select COCODCOM from LLCzero into cursor Art_temp 
 Select COCODCOM, 100*0 as LOWLVC from Art_temp into cursor LLC
      if g_UseProgBar
        this.w_NRECCUR = this.w_NRECCUR + 1
        GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
      endif
      do while RecCount("Art_temp")>0 and this.w_LLC<this.w_MAXLLC
        select distinct Legami.COCODCOM as COCODCOM from Legami inner join Art_temp ; 
 on (Art_temp.cocodcom=Legami.corifdis) into cursor tempor 
 Select * from LLC Union Select *, this.w_LLC as LOWLVC from tempor into cursor art_temp 
 Select * from art_temp into cursor LLC 
 Select cocodcom from tempor into cursor art_temp
        this.w_LLC = this.w_LLC + 1
        this.w_MESS1 = alltrim(str(this.W_LLC))
        ah_msg(this.w_MESS1)
        if g_UseProgBar
          this.w_NRECCUR = this.w_NRECCUR + 1
          GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
        endif
      enddo
      if g_UseProgBar
        this.w_NRECCUR = INT((this.w_NRECCUR / this.w_NRECELAB) * 100)
        this.w_NRECELAB = this.w_LLC + 4
        this.w_NRECCUR = INT((this.w_NRECCUR * 0.01) * this.w_NRECELAB)
        GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
      endif
      if this.w_LLC>=this.w_MAXLLC
        if g_UseProgBar
          * --- lo devo eseguire con parametro e perch� lo abbiamo lanciato da un altro batch
          GesProgBar("E", this)
        endif
        this.w_MESS = "Raggiunto il valore massimo di LLC (%1)%0Probabile esistenza di loop all'interno delle distinte basi%0Elaborazione sospesa" 
        this.oParentObject.w_ErrorLLC = .T.
        if IsAhe()
          this.OutCursor = "DistEspl"
          this.w_QTAEXP = 1
           l_chkLoop = "S" 
 l_chkDibaNoleg="N" 
 L_bUnionCurs="N" 
 L_LatoServer="S"
          if FALSE
            * --- Serve per far capire che � una tabella temporanea non rimuovere questo IF
            * --- Create temporary table TMPEXPDB
            i_nIdx=cp_AddTableDef('TMPEXPDB') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPEXPDB_proto';
                  )
            this.TMPEXPDB_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          vq_exec("..\diba\exe\query\gsmr3blc", this, "Repo") 
 Select Repo.* from Repo inner join llc on cocodcom=dbkeysal where lowlvc=this.w_MAXLLC-1 into cursor InCursor
          GSDB_BTV(this,"L", "InCursor"," "," ","  ","GNNS","S"," "," ",i_datsys,this.OutCursor , this.w_QTAEXP,l_chkLoop,L_bUnionCurs, l_chkDibaNoleg, L_LatoServer)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          vx_exec("..\DIBA\EXE\QUERY\GSMR_SCL.vqr,..\DIBA\EXE\QUERY\GSMR_SCL.FRX",this)
          * --- Drop temporary table TMPEXPDB
          i_nIdx=cp_GetTableDefIdx('TMPEXPDB')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPEXPDB')
          endif
        else
          ah_ErrorMsg(this.w_MESS,"STOP","", alltrim(str(this.w_MAXLLC)) ) 
 vq_exec( this.w_PathRel + "gsmr3blc", this, "Repo") 
 Select Repo.* from Repo inner join llc on cocodcom=arcodart where lowlvc=this.w_MAXLLC-1 into cursor __tmp__
          cp_chprn("..\cola\exe\query\gsmr3blc", " ", this)
          USE IN SELECT("Repo")
        endif
      else
        if Used("LLC")
          Select cocodcom, lowlvc from LLC into cursor Art_temp
          Select cocodcom, max(lowlvc) as lowlvc from Art_temp group by cocodcom into cursor Tempor
          * --- Seleziona solo gli articoli dove � variato il LLC
          Select cocodcom, lowlvc, nvl(dplowlew,-1) as dplowlew from (Tempor left outer join Art_prod ; 
 on cocodcom=dpcodart) having lowlvc<>nvl(dplowlew,-1) into cursor LLC
          * --- Aggiorna il database
          * --- begin transaction
          cp_BeginTrs()
          Select LLC
          if g_UseProgBar
            this.w_NRECELAB = this.w_NRECELAB + reccount()
          else
            ah_Msg("Aggiornamento LLC sul database in corso...",.T.)
          endif
          scan
          * --- Try
          local bErr_03B14358
          bErr_03B14358=bTrsErr
          this.Try_03B14358()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03B14358
          * --- End
          if IsAhe() and dplowlew=-1
            * --- Se non ci sono dati in ART_PROD inserisco i Valori di default
            * --- Try
            local bErr_03B41800
            bErr_03B41800=bTrsErr
            this.Try_03B41800()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03B41800
            * --- End
          endif
          if g_UseProgBar
            this.w_NRECCUR = this.w_NRECCUR + 1
            if MOD(this.w_NRECCUR,1*iif(this.w_NRECELAB>1000,10,1)*iif(this.w_NRECELAB>10000,50,1))=0
              GesProgBar("S", this, this.w_NRECCUR, this.w_NRECELAB)
            endif
          endif
          Select LLC
          endscan
          * --- Imposta a -1 i codici WIP di fase
          this.w_first = .f.
          if IsAhe()
            * --- Insert into PAR_RIOR
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\diba\exe\query\gsmr4blc.vqr",this.PAR_RIOR_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Write into PAR_RIOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
              i_cTempTable=cp_GetTempTableName(i_nConn)
              i_aIndex[1]='PRKEYSAL'
              cp_CreateTempTable(i_nConn,i_cTempTable,"CAKEYSAL as PRKEYSAL "," from "+i_cQueryTable+" where NOT (CARIFCLA IS NULL)",.f.,@i_aIndex)
              i_cQueryTable=i_cTempTable
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="PAR_RIOR.PRKEYSAL = _t2.PRKEYSAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
                  +i_ccchkf;
                  +" from "+i_cTable+" PAR_RIOR, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="PAR_RIOR.PRKEYSAL = _t2.PRKEYSAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_RIOR, "+i_cQueryTable+" _t2 set ";
              +"PAR_RIOR.PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
                  +Iif(Empty(i_ccchkf),"",",PAR_RIOR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="PAR_RIOR.PRKEYSAL = _t2.PRKEYSAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_RIOR set ";
              +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".PRKEYSAL = "+i_cQueryTable+".PRKEYSAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- commit
          cp_EndTrs(.t.)
          if IsAhe()
            Select cocodcom, lowlvc from llc Inner join Val_def on cocodcom=vdkeysal where dplowlew=-1 into cursor tmpLLC
          else
            Select cocodcom, lowlvc from LLC where dplowlew=-1 into cursor tmpLLC
          endif
        endif
      endif
      if g_UseProgBar
        GesProgBar("S", this, this.w_NRECELAB, this.w_NRECELAB)
      endif
      USE IN SELECT("LLC") 
 USE IN SELECT("Tempor") 
 USE IN SELECT("Legami") 
 USE IN SELECT("Art_temp") 
 USE IN SELECT("Art_prod") 
 USE IN SELECT("LLCzero") 
 USE IN SELECT("Val_Def")
    else
      * --- Se il modulo MRP non � installato il LLC deve essere uguale a zero
      * --- (Caso Impossibile se AHR)
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRLOWLEV');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            PRLOWLEV = 0;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_UseProgBar
      GesProgBar("Q", this)
    endif
    if UPPER(ALLTRIM(this.Padre.CLASS))=this.w_ROOTCLASS + "_KLC"
      ah_ErrorMsg("Elaborazione completata",64)
      * --- Chiude Padre
      this.Padre.ecpQuit()     
    endif
  endproc
  proc Try_03B14358()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if dplowlew=-1
      if IsAhe()
        this.w_CODART = left(cocodcom,20)
        this.w_CODVAR = IIF(RIGHT(cocodcom,20)=repl("#",20),"#", RIGHT(cocodcom,20))
        this.w_KEYVAR = IIF(RIGHT(cocodcom,20)=repl("#",20), space(20), RIGHT(cocodcom,20))
        * --- Insert into PAR_RIOR
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PRCODART"+",PRCODVAR"+",PRKEYVAR"+",PRKEYSAL"+",PRLOWLEV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODART),'PAR_RIOR','PRCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAR),'PAR_RIOR','PRCODVAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_KEYVAR),'PAR_RIOR','PRKEYVAR');
          +","+cp_NullLink(cp_ToStrODBC(cocodcom),'PAR_RIOR','PRKEYSAL');
          +","+cp_NullLink(cp_ToStrODBC(lowlvc),'PAR_RIOR','PRLOWLEV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_CODART,'PRCODVAR',this.w_CODVAR,'PRKEYVAR',this.w_KEYVAR,'PRKEYSAL',cocodcom,'PRLOWLEV',lowlvc)
          insert into (i_cTable) (PRCODART,PRCODVAR,PRKEYVAR,PRKEYSAL,PRLOWLEV &i_ccchkf. );
             values (;
               this.w_CODART;
               ,this.w_CODVAR;
               ,this.w_KEYVAR;
               ,cocodcom;
               ,lowlvc;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Insert into PAR_RIOR
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PRCODART"+",PRLOWLEV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(cocodcom),'PAR_RIOR','PRCODART');
          +","+cp_NullLink(cp_ToStrODBC(lowlvc),'PAR_RIOR','PRLOWLEV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PRCODART',cocodcom,'PRLOWLEV',lowlvc)
          insert into (i_cTable) (PRCODART,PRLOWLEV &i_ccchkf. );
             values (;
               cocodcom;
               ,lowlvc;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    else
      if IsAhe()
        * --- Write into PAR_RIOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(lowlvc),'PAR_RIOR','PRLOWLEV');
              +i_ccchkf ;
          +" where ";
              +"PRKEYSAL = "+cp_ToStrODBC(cocodcom);
                 )
        else
          update (i_cTable) set;
              PRLOWLEV = lowlvc;
              &i_ccchkf. ;
           where;
              PRKEYSAL = cocodcom;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into PAR_RIOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(lowlvc),'PAR_RIOR','PRLOWLEV');
              +i_ccchkf ;
          +" where ";
              +"PRCODART = "+cp_ToStrODBC(cocodcom);
                 )
        else
          update (i_cTable) set;
              PRLOWLEV = lowlvc;
              &i_ccchkf. ;
           where;
              PRCODART = cocodcom;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    return
  proc Try_03B41800()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_PROD
    i_nConn=i_TableProp[this.ART_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DPCODART"+",DPOGGMPS"+",DPPOLPRO"+",DPMODPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODART),'ART_PROD','DPCODART');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ART_PROD','DPOGGMPS');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ART_PROD','DPPOLPRO');
      +","+cp_NullLink(cp_ToStrODBC("M"),'ART_PROD','DPMODPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DPCODART',this.w_CODART,'DPOGGMPS',"N",'DPPOLPRO',"F",'DPMODPRE',"M")
      insert into (i_cTable) (DPCODART,DPOGGMPS,DPPOLPRO,DPMODPRE &i_ccchkf. );
         values (;
           this.w_CODART;
           ,"N";
           ,"F";
           ,"M";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_KEYSAL = llc.cocodcom
    insert into Val_Def (vdkeysal) Values (this.w_KEYSAL)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_PROD'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='PAR_RIOR'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
