* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bgl                                                        *
*              Generazione ordini da OCL                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_719]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-07                                                      *
* Last revis.: 2011-06-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bgl",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_bgl as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_nRecEla = 0
  w_nRecDoc = 0
  w_OPERAZ = space(2)
  w_ERRORE = .f.
  TmpC = space(100)
  w_PPCENCOS = space(15)
  w_PPCAUORD = space(5)
  w_PPMAGPRO = space(5)
  w_PPCAUIMP = space(5)
  w_FLORDI = space(1)
  w_APPO = 0
  w_APPO1 = 0
  w_CAUCOL = space(5)
  w_LNumErr = 0
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_CODODL = space(15)
  w_QTAODL = 0
  w_QTAOD1 = 0
  w_QTAEVA = 0
  w_QTAEV1 = 0
  w_QTASAL = 0
  w_UNMIS = space(3)
  w_FLEVAS = space(1)
  w_KEYSAL = space(20)
  w_CODART = space(20)
  w_CODICE = space(20)
  w_PROVEN = space(1)
  w_COFOR = space(15)
  w_CODMAG = space(5)
  w_CODCOM = space(15)
  w_TIPATT = space(1)
  w_CODATT = space(15)
  w_CODCOS = space(5)
  w_FLORCO = space(1)
  w_FLCOCO = space(1)
  w_IMPCOM = 0
  w_COCEN = space(15)
  w_VOCEN = space(15)
  w_FLIMCO = space(1)
  w_DINRIC = ctod("  /  /  ")
  w_DTRIC = ctod("  /  /  ")
  w_OMAG = space(5)
  w_PDORDINE = space(15)
  w_MVTIPDOC = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_MVFLPROV = space(1)
  w_NUMSCO = 0
  w_MVEMERIC = space(1)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  DISMBASE_idx=0
  MA_COSTI_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  PAR_RIOR_idx=0
  SALDIART_idx=0
  MAGAZZIN_idx=0
  TIP_DOCU_idx=0
  TMPMODL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Generazione ordini da OCL-ODA (da GSCO_KGL)
    * --- Log Errori
    * --- Variabili Cursore
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_OPERAZ = "OR"
    * --- Legge da Parametri Causali
    this.w_PPCAUORD = " "
    this.w_PPMAGPRO = " "
    this.w_PPCAUIMP = " "
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUORD,PPMAGPRO,PPCENCOS,PPCAUIMP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUORD,PPMAGPRO,PPCENCOS,PPCAUIMP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPCAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.w_PPMAGPRO = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      this.w_PPCAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controlla
    do case
      case EMPTY(this.w_PPMAGPRO)
        ah_ErrorMsg("Magazzino produzione non definito in tabella parametri","STOP","")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_PPCAUORD)
        ah_ErrorMsg("Causale di default (ordinato) non definita in tabella parametri","STOP","")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_PPCAUIMP)
        ah_ErrorMsg("Causale di default (impegnato) non definita in tabella parametri","STOP","")
        i_retcode = 'stop'
        return
    endcase
    * --- Legge FLAGS Causali
    this.w_TIPATT = "A"
    this.w_nRecSel = 0
    this.w_nRecEla = 0
    this.w_nRecDoc = 0
    this.w_LNumErr = 0
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione="INTERROGA"
        * --- Visualizza Zoom Elendo OCL periodo
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
        this.oParentObject.w_SELEZI = "D"
      case this.pAzione="AG"
        * --- Generazione OCL
        * --- Controlli Preliminari
        if EMPTY(this.oParentObject.w_PPTIPDOC)
          ah_ErrorMsg("Causale ordine a terzista non definita","STOP","")
          i_retcode = 'stop'
          return
        endif
        this.w_APPO = " "
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDCAUMAG"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDCAUMAG;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_PPTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_APPO)
          ah_ErrorMsg("Causale magazzino associata a documento di ordine a terzista non definita","STOP","")
          i_retcode = 'stop'
          return
        else
          this.w_FLORDI = " "
          this.w_CAUCOL = SPACE(5)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLORDI,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_APPO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLORDI,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = this.w_APPO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLORDI<>"+" OR NOT EMPTY(this.w_CAUCOL)
            ah_ErrorMsg("Causale magazzino associata a documento di ordine a terzista incongruente","STOP","")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Crea tabella Temporanea dati ODL_MAST
        * --- Create temporary table TMPMODL
        i_nIdx=cp_AddTableDef('TMPMODL') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\COLA\EXE\QUERY\GSCOTBGL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPMODL_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Controlla selezioni
        SELECT (this.NC)
        GO TOP
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel>0
          this.w_nRecSel = 0
          this.w_nRecEla = 0
          this.w_LNumErr = 0
          * --- Crea il File delle Messaggistiche di Errore
          CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
          * --- Legge cursore di selezione ...
          * --- Cicla sui codici selezionati ...
          SELECT (this.NC)
          GO TOP
          SCAN FOR xChk=1 AND NOT EMPTY(NVL(OLCODODL,"")) 
          * --- Legge dati di interesse ....
          this.w_CODODL = OLCODODL
          this.w_CODICE = OLTCODIC
          this.w_CODART = NVL(OLTCOART, SPACE(15))
          this.w_CODMAG = NVL(OLTCOMAG, SPACE(5))
          this.w_CODATT = NVL(OLTCOATT, SPACE(15))
          this.w_CODCOS = NVL(OLTCOCOS, SPACE(5))
          this.w_CODCOM = NVL(OLTCOMME, SPACE(15))
          this.w_COCEN = NVL(OLTCOCEN, SPACE(15))
          this.w_VOCEN = NVL(OLTVOCEN, SPACE(15))
          this.w_IMPCOM = NVL(OLTIMCOM, 0)
          this.w_FLIMCO = NVL(OLTFLIMC, " ")
          this.w_KEYSAL = IIF(EMPTY(NVL(OLTKEYSA,"")), this.w_CODART, OLTKEYSA)
          this.w_PROVEN = NVL(OLTPROVE, " ")
          this.w_COFOR = NVL(OLTCOFOR, SPACE(15))
          this.w_QTAODL = NVL(OLTQTODL, 0)
          this.w_QTAOD1 = NVL(OLTQTOD1, 0)
          this.w_QTAEVA = NVL(OLTQTOEV, 0)
          this.w_QTAEV1 = NVL(OLTQTOE1, 0)
          this.w_FLEVAS = NVL(OLTFLEVA, " ")
          this.w_UNMIS = NVL(OLTUNMIS, "   ")
          this.w_DINRIC = CP_TODATE(OLTDINRIC)
          this.w_DTRIC = CP_TODATE(OLTDTRIC)
          this.w_PDORDINE = this.w_COFOR
          * --- Azione ...
          this.w_nRecSel = this.w_nRecSel + 1
          do case
            case EMPTY(this.w_COFOR)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice fornitore C/Lavoro non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_CODART)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice articolo non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_CODMAG)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice magazzino non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            otherwise
              * --- Generazione OCL
              * --- Insert into TMPMODL
              i_nConn=i_TableProp[this.TMPMODL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMODL_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPMODL_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PDSERIAL"+",PDROWNUM"+",PDCODICE"+",PDCODART"+",PDDATEVA"+",PDCODMAG"+",PDTIPCON"+",PDCODCON"+",PDUMORDI"+",PDQTAORD"+",PDQTAUM1"+",PDCODCOM"+",PDTIPATT"+",PDCODATT"+",PDCODCEN"+",PDVOCCOS"+",PDDATORD"+",PDORDINE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODODL),'TMPMODL','PDSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(0),'TMPMODL','PDROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'TMPMODL','PDCODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'TMPMODL','PDCODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DTRIC),'TMPMODL','PDDATEVA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'TMPMODL','PDCODMAG');
                +","+cp_NullLink(cp_ToStrODBC("F"),'TMPMODL','PDTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_COFOR),'TMPMODL','PDCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_UNMIS),'TMPMODL','PDUMORDI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAODL),'TMPMODL','PDQTAORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAOD1),'TMPMODL','PDQTAUM1');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'TMPMODL','PDCODCOM');
                +","+cp_NullLink(cp_ToStrODBC("A"),'TMPMODL','PDTIPATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'TMPMODL','PDCODATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_COCEN),'TMPMODL','PDCODCEN');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VOCEN),'TMPMODL','PDVOCCOS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DINRIC),'TMPMODL','PDDATORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PDORDINE),'TMPMODL','PDORDINE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_CODODL,'PDROWNUM',0,'PDCODICE',this.w_CODICE,'PDCODART',this.w_CODART,'PDDATEVA',this.w_DTRIC,'PDCODMAG',this.w_CODMAG,'PDTIPCON',"F",'PDCODCON',this.w_COFOR,'PDUMORDI',this.w_UNMIS,'PDQTAORD',this.w_QTAODL,'PDQTAUM1',this.w_QTAOD1,'PDCODCOM',this.w_CODCOM)
                insert into (i_cTable) (PDSERIAL,PDROWNUM,PDCODICE,PDCODART,PDDATEVA,PDCODMAG,PDTIPCON,PDCODCON,PDUMORDI,PDQTAORD,PDQTAUM1,PDCODCOM,PDTIPATT,PDCODATT,PDCODCEN,PDVOCCOS,PDDATORD,PDORDINE &i_ccchkf. );
                   values (;
                     this.w_CODODL;
                     ,0;
                     ,this.w_CODICE;
                     ,this.w_CODART;
                     ,this.w_DTRIC;
                     ,this.w_CODMAG;
                     ,"F";
                     ,this.w_COFOR;
                     ,this.w_UNMIS;
                     ,this.w_QTAODL;
                     ,this.w_QTAOD1;
                     ,this.w_CODCOM;
                     ,"A";
                     ,this.w_CODATT;
                     ,this.w_COCEN;
                     ,this.w_VOCEN;
                     ,this.w_DINRIC;
                     ,this.w_PDORDINE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
          endcase
          SELECT (this.NC)
          ENDSCAN
          vq_exec("..\COLA\EXE\QUERY\GSCO_BGL.VQR",this,"GENEORDI")
          * --- Elimina Tabella temporanea
          * --- Drop temporary table TMPMODL
          i_nIdx=cp_GetTableDefIdx('TMPMODL')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPMODL')
          endif
          * --- Generazione OCL
          ah_Msg("Generazione ordini a terzisti...",.T.)
          this.w_MVTIPDOC = this.oParentObject.w_PPTIPDOC
          this.w_MVDATDOC = this.oParentObject.w_PPDATDOC
          this.w_MVDATDIV = this.oParentObject.w_PPDATDIV
          this.w_MVNUMDOC = this.oParentObject.w_PPNUMDOC
          this.w_MVALFDOC = this.oParentObject.w_PPALFDOC
          this.w_MVPRD = this.oParentObject.w_PPPRD
          this.w_MVPRP = "NN"
          this.w_MVFLVEAC = this.oParentObject.w_PPFLVEAC
          this.w_MVEMERIC = this.oParentObject.w_PPEMERIC
          this.w_MVCLADOC = this.oParentObject.w_PPCLADOC
          this.w_MVFLINTE = this.oParentObject.w_PPFLINTE
          this.w_MVTCAMAG = this.oParentObject.w_PPTCAMAG
          this.w_MVTFRAGG = this.oParentObject.w_PPTFRAGG
          this.w_MVCAUCON = this.oParentObject.w_PPCAUCON
          this.w_NUMSCO = this.oParentObject.w_PPNUMSCO
          this.w_MVFLACCO = this.oParentObject.w_PPFLACCO
          this.w_MVFLPROV = this.oParentObject.w_PPFLPROV
          this.w_MVSERIAL = SPACE(10)
          this.w_MVTIPCON = "F"
          this.w_MVCODESE = g_CODESE
          this.w_MVNUMEST = 0
          this.w_MVALFEST = Space(10)
          this.w_MVANNPRO = this.oParentObject.w_PPANNPRO
          this.w_MVDATREG = this.w_MVDATDOC
          this.w_MVDATCIV = this.w_MVDATDOC
          this.w_MVNUMREG = 0
          this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
          this.w_MVANNDOC = this.oParentObject.w_PPANNDOC
          * --- Lancia Batch di Generazione Ordini a Terzisti
          do GSCO_BGD with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_oPart = this.w_oMess.AddMsgPartNL("Elaborazione terminata%0N.%1 documenti generati%0N.%2 records elaborati%0su %3 records selezionati")
          this.w_oPart.AddParam(alltrim(str(this.w_nRecDoc,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecEla,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecSel,5,0)))     
          if USED("MessErr") AND this.w_LNumErr>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?")
            this.w_oPart.AddParam(alltrim(str(this.w_LNumErr,5,0)))     
            if this.w_oMess.ah_YesNo()
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
            endif
          else
            this.w_oMess.ah_ErrorMsg()
          endif
          * --- Chiusura Cursori
          if USED("MATRERRO")
            use in MATRERRO
          endif
          if USED("MessErr")
            SELECT MessErr
            USE
          endif
          if USED("__TMP__")
            SELECT __TMP__
            USE
          endif
          if USED("GENEORDI")
            SELECT GENEORDI
            USE
          endif
          * --- Refresh nuovo Progressivo Ordine e Zoom
          this.Padre.NotifyEvent("NewProg")     
          this.Padre.NotifyEvent("Interroga")     
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare","!","")
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    * --- Scrive LOG
    if EMPTY(this.w_LTesMes)
      this.w_LTesMes = "Message()= "+message()
    endif
    INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
    (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LTesMes)
    this.w_LTesMes = ""
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='MA_COSTI'
    this.cWorkTables[4]='ODL_DETT'
    this.cWorkTables[5]='ODL_MAST'
    this.cWorkTables[6]='PAR_PROD'
    this.cWorkTables[7]='PAR_RIOR'
    this.cWorkTables[8]='SALDIART'
    this.cWorkTables[9]='MAGAZZIN'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='*TMPMODL'
    return(this.OpenAllTables(11))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
