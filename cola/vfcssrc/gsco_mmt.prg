* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mmt                                                        *
*              Dichiarazioni matricole                                         *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_351]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-03                                                      *
* Last revis.: 2015-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mmt")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mmt")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mmt")
  return

* --- Class definition
define class tgsco_mmt as StdPCForm
  Width  = 706
  Height = 439
  Top    = 4
  Left   = 10
  cComment = "Dichiarazioni matricole"
  cPrg = "gsco_mmt"
  HelpContextID=197764457
  add object cnt as tcgsco_mmt
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mmt as PCContext
  w_MTSERIAL = space(15)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_MAGCAR = space(5)
  w_QTACA1 = 0
  w_MAGSCA = space(5)
  w_FLPRD = space(1)
  w_MTKEYSAL = space(20)
  w_MTCODMAT = space(40)
  w_MTFLPREN = space(1)
  w_MTCODMAT = space(40)
  w_AMPRODU = 0
  w_MTCODLOT = space(20)
  w_QTASC1 = 0
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_CODART = space(20)
  w_CODICE = space(41)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_QTAUM1 = 0
  w_FLLOTT = space(1)
  w_RIGA = 0
  w_MTFLSCAR = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_KEYSAL = space(20)
  w_MTRIFNUM = 0
  w_MT__FLAG = space(1)
  w_MT_SALDO = 0
  w_CODFOR = space(15)
  w_ARTLOT = space(20)
  w_STALOT = space(1)
  w_LOTZOOM = space(1)
  w_MAGUBI = space(5)
  w_MAGUBI1 = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_FLRISE = space(1)
  w_NUMRIF = 0
  w_MOVMAT = space(1)
  w_DATREG = space(5)
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_MTMAGCAR = space(5)
  w_MTCODUBI = space(20)
  w_MGUBIC = space(1)
  w_MGUBIC1 = space(1)
  w_RIGACAR = 0
  w_RIGASCA = 0
  w_UNIMIS1 = space(3)
  w_CODLOTTES = space(20)
  w_CODUBITES = space(20)
  w_CODUB2TES = space(20)
  w_ARTLOTTES = space(20)
  w_FLPRG = space(1)
  w_OBTEST = space(8)
  w_TOTCAR = 0
  w_TOTSCA = 0
  w_TOTALE = 0
  w_FLSTATTES = space(1)
  w_CODUBI = space(20)
  w_MTRIFSTO = 0
  w_NOMSG = space(1)
  w_MTCODCOM = space(15)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  proc Save(i_oFrom)
    this.w_MTSERIAL = i_oFrom.w_MTSERIAL
    this.w_MTROWNUM = i_oFrom.w_MTROWNUM
    this.w_MTNUMRIF = i_oFrom.w_MTNUMRIF
    this.w_MAGCAR = i_oFrom.w_MAGCAR
    this.w_QTACA1 = i_oFrom.w_QTACA1
    this.w_MAGSCA = i_oFrom.w_MAGSCA
    this.w_FLPRD = i_oFrom.w_FLPRD
    this.w_MTKEYSAL = i_oFrom.w_MTKEYSAL
    this.w_MTCODMAT = i_oFrom.w_MTCODMAT
    this.w_MTFLPREN = i_oFrom.w_MTFLPREN
    this.w_MTCODMAT = i_oFrom.w_MTCODMAT
    this.w_AMPRODU = i_oFrom.w_AMPRODU
    this.w_MTCODLOT = i_oFrom.w_MTCODLOT
    this.w_QTASC1 = i_oFrom.w_QTASC1
    this.w_MTSERRIF = i_oFrom.w_MTSERRIF
    this.w_MTROWRIF = i_oFrom.w_MTROWRIF
    this.w_CODART = i_oFrom.w_CODART
    this.w_CODICE = i_oFrom.w_CODICE
    this.w_DESART = i_oFrom.w_DESART
    this.w_UNIMIS1 = i_oFrom.w_UNIMIS1
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_FLLOTT = i_oFrom.w_FLLOTT
    this.w_RIGA = i_oFrom.w_RIGA
    this.w_MTFLSCAR = i_oFrom.w_MTFLSCAR
    this.w_MTMAGSCA = i_oFrom.w_MTMAGSCA
    this.w_MTFLCARI = i_oFrom.w_MTFLCARI
    this.w_CODMAG = i_oFrom.w_CODMAG
    this.w_ESIRIS = i_oFrom.w_ESIRIS
    this.w_KEYSAL = i_oFrom.w_KEYSAL
    this.w_MTRIFNUM = i_oFrom.w_MTRIFNUM
    this.w_MT__FLAG = i_oFrom.w_MT__FLAG
    this.w_MT_SALDO = i_oFrom.w_MT_SALDO
    this.w_CODFOR = i_oFrom.w_CODFOR
    this.w_ARTLOT = i_oFrom.w_ARTLOT
    this.w_STALOT = i_oFrom.w_STALOT
    this.w_LOTZOOM = i_oFrom.w_LOTZOOM
    this.w_MAGUBI = i_oFrom.w_MAGUBI
    this.w_MAGUBI1 = i_oFrom.w_MAGUBI1
    this.w_SERRIF = i_oFrom.w_SERRIF
    this.w_ROWRIF = i_oFrom.w_ROWRIF
    this.w_FLRISE = i_oFrom.w_FLRISE
    this.w_NUMRIF = i_oFrom.w_NUMRIF
    this.w_MOVMAT = i_oFrom.w_MOVMAT
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_ULTPROG = i_oFrom.w_ULTPROG
    this.w_SECODUBI = i_oFrom.w_SECODUBI
    this.w_SECODLOT = i_oFrom.w_SECODLOT
    this.w_MTMAGCAR = i_oFrom.w_MTMAGCAR
    this.w_MTCODUBI = i_oFrom.w_MTCODUBI
    this.w_MGUBIC = i_oFrom.w_MGUBIC
    this.w_MGUBIC1 = i_oFrom.w_MGUBIC1
    this.w_RIGACAR = i_oFrom.w_RIGACAR
    this.w_RIGASCA = i_oFrom.w_RIGASCA
    this.w_UNIMIS1 = i_oFrom.w_UNIMIS1
    this.w_CODLOTTES = i_oFrom.w_CODLOTTES
    this.w_CODUBITES = i_oFrom.w_CODUBITES
    this.w_CODUB2TES = i_oFrom.w_CODUB2TES
    this.w_ARTLOTTES = i_oFrom.w_ARTLOTTES
    this.w_FLPRG = i_oFrom.w_FLPRG
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_TOTCAR = i_oFrom.w_TOTCAR
    this.w_TOTSCA = i_oFrom.w_TOTSCA
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_FLSTATTES = i_oFrom.w_FLSTATTES
    this.w_CODUBI = i_oFrom.w_CODUBI
    this.w_MTRIFSTO = i_oFrom.w_MTRIFSTO
    this.w_NOMSG = i_oFrom.w_NOMSG
    this.w_MTCODCOM = i_oFrom.w_MTCODCOM
    this.w_COMCAR = i_oFrom.w_COMCAR
    this.w_COMSCA = i_oFrom.w_COMSCA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MTSERIAL = this.w_MTSERIAL
    i_oTo.w_MTROWNUM = this.w_MTROWNUM
    i_oTo.w_MTNUMRIF = this.w_MTNUMRIF
    i_oTo.w_MAGCAR = this.w_MAGCAR
    i_oTo.w_QTACA1 = this.w_QTACA1
    i_oTo.w_MAGSCA = this.w_MAGSCA
    i_oTo.w_FLPRD = this.w_FLPRD
    i_oTo.w_MTKEYSAL = this.w_MTKEYSAL
    i_oTo.w_MTCODMAT = this.w_MTCODMAT
    i_oTo.w_MTFLPREN = this.w_MTFLPREN
    i_oTo.w_MTCODMAT = this.w_MTCODMAT
    i_oTo.w_AMPRODU = this.w_AMPRODU
    i_oTo.w_MTCODLOT = this.w_MTCODLOT
    i_oTo.w_QTASC1 = this.w_QTASC1
    i_oTo.w_MTSERRIF = this.w_MTSERRIF
    i_oTo.w_MTROWRIF = this.w_MTROWRIF
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_CODICE = this.w_CODICE
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_UNIMIS1 = this.w_UNIMIS1
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_FLLOTT = this.w_FLLOTT
    i_oTo.w_RIGA = this.w_RIGA
    i_oTo.w_MTFLSCAR = this.w_MTFLSCAR
    i_oTo.w_MTMAGSCA = this.w_MTMAGSCA
    i_oTo.w_MTFLCARI = this.w_MTFLCARI
    i_oTo.w_CODMAG = this.w_CODMAG
    i_oTo.w_ESIRIS = this.w_ESIRIS
    i_oTo.w_KEYSAL = this.w_KEYSAL
    i_oTo.w_MTRIFNUM = this.w_MTRIFNUM
    i_oTo.w_MT__FLAG = this.w_MT__FLAG
    i_oTo.w_MT_SALDO = this.w_MT_SALDO
    i_oTo.w_CODFOR = this.w_CODFOR
    i_oTo.w_ARTLOT = this.w_ARTLOT
    i_oTo.w_STALOT = this.w_STALOT
    i_oTo.w_LOTZOOM = this.w_LOTZOOM
    i_oTo.w_MAGUBI = this.w_MAGUBI
    i_oTo.w_MAGUBI1 = this.w_MAGUBI1
    i_oTo.w_SERRIF = this.w_SERRIF
    i_oTo.w_ROWRIF = this.w_ROWRIF
    i_oTo.w_FLRISE = this.w_FLRISE
    i_oTo.w_NUMRIF = this.w_NUMRIF
    i_oTo.w_MOVMAT = this.w_MOVMAT
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_ULTPROG = this.w_ULTPROG
    i_oTo.w_SECODUBI = this.w_SECODUBI
    i_oTo.w_SECODLOT = this.w_SECODLOT
    i_oTo.w_MTMAGCAR = this.w_MTMAGCAR
    i_oTo.w_MTCODUBI = this.w_MTCODUBI
    i_oTo.w_MGUBIC = this.w_MGUBIC
    i_oTo.w_MGUBIC1 = this.w_MGUBIC1
    i_oTo.w_RIGACAR = this.w_RIGACAR
    i_oTo.w_RIGASCA = this.w_RIGASCA
    i_oTo.w_UNIMIS1 = this.w_UNIMIS1
    i_oTo.w_CODLOTTES = this.w_CODLOTTES
    i_oTo.w_CODUBITES = this.w_CODUBITES
    i_oTo.w_CODUB2TES = this.w_CODUB2TES
    i_oTo.w_ARTLOTTES = this.w_ARTLOTTES
    i_oTo.w_FLPRG = this.w_FLPRG
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_TOTCAR = this.w_TOTCAR
    i_oTo.w_TOTSCA = this.w_TOTSCA
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_FLSTATTES = this.w_FLSTATTES
    i_oTo.w_CODUBI = this.w_CODUBI
    i_oTo.w_MTRIFSTO = this.w_MTRIFSTO
    i_oTo.w_NOMSG = this.w_NOMSG
    i_oTo.w_MTCODCOM = this.w_MTCODCOM
    i_oTo.w_COMCAR = this.w_COMCAR
    i_oTo.w_COMSCA = this.w_COMSCA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mmt as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 706
  Height = 439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-18"
  HelpContextID=197764457
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=70

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DIC_MATR_IDX = 0
  LOTTIART_IDX = 0
  MAGAZZIN_IDX = 0
  MATRICOL_IDX = 0
  MOVIMATR_IDX = 0
  UBICAZIO_IDX = 0
  cFile = "DIC_MATR"
  cKeySelect = "MTSERIAL,MTROWNUM"
  cKeyWhere  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM"
  cKeyDetail  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM and MTKEYSAL=this.w_MTKEYSAL and MTCODMAT=this.w_MTCODMAT and MTCODMAT=this.w_MTCODMAT"
  cKeyWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';

  cKeyDetailWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)';
      +'+" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)';
      +'+" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)';

  cKeyWhereODBCqualified = '"DIC_MATR.MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and DIC_MATR.MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DIC_MATR.MTCODLOT,DIC_MATR.MTCODUBI,DIC_MATR.MTCODMAT'
  cPrg = "gsco_mmt"
  cComment = "Dichiarazioni matricole"
  i_nRowNum = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MTSERIAL = space(15)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_MAGCAR = space(5)
  w_QTACA1 = 0
  w_MAGSCA = space(5)
  w_FLPRD = space(1)
  w_MTKEYSAL = space(20)
  w_MTCODMAT = space(40)
  o_MTCODMAT = space(40)
  w_MTFLPREN = space(1)
  w_MTCODMAT = space(40)
  w_AMPRODU = 0
  w_MTCODLOT = space(20)
  o_MTCODLOT = space(20)
  w_QTASC1 = 0
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_CODART = space(20)
  w_CODICE = space(41)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_QTAUM1 = 0
  w_FLLOTT = space(1)
  w_RIGA = 0
  w_MTFLSCAR = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_KEYSAL = space(20)
  w_MTRIFNUM = 0
  w_MT__FLAG = space(1)
  w_MT_SALDO = 0
  w_CODFOR = space(15)
  w_ARTLOT = space(20)
  w_STALOT = space(1)
  w_LOTZOOM = .F.
  w_MAGUBI = space(5)
  w_MAGUBI1 = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_FLRISE = space(1)
  w_NUMRIF = 0
  w_MOVMAT = .F.
  w_DATREG = space(5)
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_MTMAGCAR = space(5)
  o_MTMAGCAR = space(5)
  w_MTCODUBI = space(20)
  w_MGUBIC = space(1)
  w_MGUBIC1 = space(1)
  w_RIGACAR = 0
  w_RIGASCA = 0
  w_UNIMIS1 = space(3)
  w_CODLOTTES = space(20)
  w_CODUBITES = space(20)
  w_CODUB2TES = space(20)
  w_ARTLOTTES = space(20)
  w_FLPRG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_TOTCAR = 0
  w_TOTSCA = 0
  w_TOTALE = 0
  w_FLSTATTES = space(1)
  w_CODUBI = space(20)
  w_MTRIFSTO = 0
  w_NOMSG = .F.
  w_MTCODCOM = space(15)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_mmt
  Procedure InitSon
  * Valorizzo le variabili che arrivano dal padre
  	With This
  	  *.w_MTMAGCAR=Space(5)
  		.w_MTMAGSCA=Space(5)
  		.w_MTFLSCAR=' '
  		.w_MTFLCARI=' '
  		* Scarico da evasione
  	  .w_SERRIF=Space(10)
      .w_ROWRIF=0
      .w_FLRISE=' '
      .w_NUMRIF=0
  
  		.w_CODICE=iif(Upper(this.oParentObject.Class)='TGSCI_ADP',.oParentObject.w_DPCODPAD,.oParentObject.w_DPCODICE)
  		.w_DESART=.oParentObject.w_DESART
  		.w_CODART=iif(Upper(this.oParentObject.Class)='TGSCI_ADP',.oParentObject.w_DPARTPAD,.oParentObject.w_DPCODART)
  		.w_UNIMIS1=.oParentObject.w_UNMIS1
  		.w_KEYSAL=iif(Upper(this.oParentObject.Class)='TGSCI_ADP',.oParentObject.w_OLTKEYSA1,.oParentObject.w_OLTKEYSA)
  		.w_DATREG=.oParentObject.w_DPDATREG
  
  		* Dichiarazioni con ODL
  		.w_QTACA1=.oParentObject.w_DPQTAPR1
  		.w_QTASC1=.oParentObject.w_DPQTASC1
  		.w_QTAUM1=.w_QTACA1+.w_QTASC1
  
  		* Determino i Magazzini di Carico PF
  		.w_MAGCAR=iif(.w_QTACA1>0, .oParentObject.w_DPCODMAG, Space(5))
  		.w_CODUBITES=iif(.w_QTACA1>0, .oParentObject.w_DPCODUBI, Space(15))
      .w_CODUBI=iif(.w_QTACA1>0, .oParentObject.w_DPCODUBI, Space(15))
  		.w_MAGSCA=iif(.w_QTASC1>0, .oParentObject.w_MGSCAR, Space(5))
  		*.w_CODUB2TES=iif(.w_QTASC1>0, .oParentObject.w_DPCODUBI, Space(15))
  		.w_CODLOTTES=.oParentObject.w_DPCODLOT
      .w_COMCAR=.oParentObject.w_DPCODCOM
      .w_COMSCA=.oParentObject.w_DPCODCOM
      
      .w_FLPRD = iif(Upper(this.oParentObject.Class)$'TGSCO_ADP-TGSCI_ADP',"+"," ")
  		
      .w_ARTLOT=.w_CODART
      .w_ARTLOTTES=.w_CODART
  		.w_MTFLCARI='E'
  		*.w_MTMAGCAR=iif(.w_QTAUM1>0, .w_MAGCAR, .w_MAGSCA)
  		
  		* Magazzino di Riferimento (se presente magazzino di scarico)
  		.w_CODMAG = iif(empty(.w_MAGCAR), .w_MAGSCA, .w_MAGCAR)
  		
      * Magazzino di Riferimento ubicazioni (se presente magazzino collegato)
      .w_MAGUBI = .w_MAGCAR
  
      * Magazzino di riferimento gestito ad Ubicazioni
  		.w_MGUBIC = IIF( .w_CODMAG=.oParentObject.w_DPCODMAG , .oParentObject.w_FLUBIC,.oParentObject.w_FLUBIC1 )
  		
      .w_MTFLPREN = .w_FLPRD
  
      * Magazzino di Riferimento ubicazioni (se presente magazzino collegato)
      .w_MAGUBI1 = .w_MAGSCA
  
      * Magazzino di riferimento gestito ad Ubicazioni
  		.w_MGUBIC1 = .oParentObject.w_FLUBIC1
  
      .w_FLPRG='Z'
  	
  		* Flag articolo gestito a lotti
  		.w_FLLOTT=iif(Upper(this.oParentObject.Class)='TGSCI_ADP',.oParentObject.w_FLLOTT1,.oParentObject.w_FLLOTT)
  		
  		* tipo di operazione (Esistenza/Riservato)
  		.w_ESIRIS = IIF( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
  		
  		.w_MTKEYSAL=.w_KEYSAL
  
  		.SetControlsValue()
  		
  		* Mostro / Nascondo i Controls
  		.mHideControls()
  	
  	EndWith
  EndProc
  
  PROC F6()
  * --- disabilita F6 per righe non cancellabili
  * --- Es. matricola movimentata in seguito
  	if inlist(this.cFunction,'Edit','Load')
  		If this.w_MT_SALDO<>0
  			Ah_errormsg("Impossibile cancellare una riga succesivamente movimentata",'!',"")
  		else
  			dodefault()
  		Endif
  	Endif
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mmtPag1","gsco_mmt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODLOTTES_1_47
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='MATRICOL'
    this.cWorkTables[4]='MOVIMATR'
    this.cWorkTables[5]='UBICAZIO'
    this.cWorkTables[6]='DIC_MATR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_MATR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_MATR_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mmt'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsco_mmt
    This.InitSon()
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DIC_MATR where MTSERIAL=KeySet.MTSERIAL
    *                            and MTROWNUM=KeySet.MTROWNUM
    *                            and MTKEYSAL=KeySet.MTKEYSAL
    *                            and MTCODMAT=KeySet.MTCODMAT
    *                            and MTCODMAT=KeySet.MTCODMAT
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_MATR_IDX,2],this.bLoadRecFilter,this.DIC_MATR_IDX,"gsco_mmt")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_MATR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_MATR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_MATR '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MTSERIAL',this.w_MTSERIAL  ,'MTROWNUM',this.w_MTROWNUM  )
      select * from (i_cTable) DIC_MATR where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MAGCAR = .oParentObject.w_DPCODMAG
        .w_QTACA1 = .oParentObject.w_DPQTAPR1
        .w_MAGSCA = .oParentObject.w_MGSCAR
        .w_FLPRD = space(1)
        .w_QTASC1 = .oParentObject.w_DPQTASC1
        .w_CODICE = space(41)
        .w_DESART = space(40)
        .w_UNIMIS1 = space(3)
        .w_MOVMAT = .t.
        .w_ULTPROG = 0
        .w_SECODUBI = space(20)
        .w_SECODLOT = space(20)
        .w_UNIMIS1 = space(3)
        .w_CODLOTTES = space(20)
        .w_CODUBITES = space(20)
        .w_CODUB2TES = space(20)
        .w_ARTLOTTES = space(20)
        .w_OBTEST = ctod("  /  /  ")
        .w_TOTCAR = 0
        .w_TOTSCA = 0
        .w_TOTALE = 0
        .w_FLSTATTES = space(1)
        .w_MTSERIAL = NVL(MTSERIAL,space(15))
        .w_MTROWNUM = NVL(MTROWNUM,0)
        .w_MTNUMRIF = NVL(MTNUMRIF,0)
        .w_CODART = .w_CODART
        .w_QTAUM1 = .w_QTACA1+.w_QTASC1
        .w_FLLOTT = .w_FLLOTT
        .w_MTFLSCAR = NVL(MTFLSCAR,space(1))
        .w_MTMAGSCA = NVL(MTMAGSCA,space(5))
        .w_MTFLCARI = NVL(MTFLCARI,space(1))
        .w_CODMAG = .w_CODMAG
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .w_KEYSAL
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_CODFOR = .w_CODFOR
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_MAGUBI = .w_MAGUBI
        .w_MAGUBI1 = .w_MAGUBI1
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_DATREG = .w_DATREG
          .link_1_47('Load')
          .link_1_49('Load')
          .link_1_51('Load')
        .w_FLPRG = .w_FLPRG
        .w_CODUBI = .w_CODUBI
        .w_NOMSG = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
        cp_LoadRecExtFlds(this,'DIC_MATR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTCAR = 0
      this.w_TOTSCA = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_AMPRODU = 0
          .w_ARTLOT = space(20)
          .w_STALOT = space(1)
          .w_MTKEYSAL = NVL(MTKEYSAL,space(20))
          .w_MTCODMAT = NVL(MTCODMAT,space(40))
          .w_MTFLPREN = NVL(MTFLPREN,space(1))
          .w_MTCODMAT = NVL(MTCODMAT,space(40))
          if link_2_4_joined
            this.w_MTCODMAT = NVL(AMCODICE204,NVL(this.w_MTCODMAT,space(40)))
            this.w_AMPRODU = NVL(AM_PRODU204,0)
          else
          .link_2_4('Load')
          endif
          .w_MTCODLOT = NVL(MTCODLOT,space(20))
          .link_2_6('Load')
          .w_MTSERRIF = NVL(MTSERRIF,space(10))
          .w_MTROWRIF = NVL(MTROWRIF,0)
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
          .w_MTRIFNUM = NVL(MTRIFNUM,0)
          * evitabile
          *.link_2_10('Load')
          .w_MT__FLAG = NVL(MT__FLAG,space(1))
          .w_MT_SALDO = NVL(MT_SALDO,0)
        .w_LOTZOOM = .F.
          .w_MTMAGCAR = NVL(MTMAGCAR,space(5))
          if link_2_19_joined
            this.w_MTMAGCAR = NVL(MGCODMAG219,NVL(this.w_MTMAGCAR,space(5)))
            this.w_MGUBIC = NVL(MGFLUBIC219,space(1))
          else
          .link_2_19('Load')
          endif
          .w_MTCODUBI = NVL(MTCODUBI,space(20))
          * evitabile
          *.link_2_20('Load')
        .w_MGUBIC = .w_MGUBIC
        .w_MGUBIC1 = .w_MGUBIC1
        .w_RIGACAR = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGCAR,"        ") , .w_RIGA , 0)
        .w_RIGASCA = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGSCA,"        ") , .w_RIGA ,0 )
          .w_MTRIFSTO = NVL(MTRIFSTO,0)
          .w_MTCODCOM = NVL(MTCODCOM,space(15))
          select (this.cTrsName)
          append blank
          replace MTKEYSAL with .w_MTKEYSAL
          replace MTCODMAT with .w_MTCODMAT
          replace MTFLPREN with .w_MTFLPREN
          replace MTCODMAT with .w_MTCODMAT
          replace MTSERRIF with .w_MTSERRIF
          replace MTROWRIF with .w_MTROWRIF
          replace MTRIFNUM with .w_MTRIFNUM
          replace MT__FLAG with .w_MT__FLAG
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTCAR = .w_TOTCAR+.w_RIGACAR
          .w_TOTSCA = .w_TOTSCA+.w_RIGASCA
          .w_TOTALE = .w_TOTALE+.w_RIGA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CODART = .w_CODART
        .w_QTAUM1 = .w_QTACA1+.w_QTASC1
        .w_FLLOTT = .w_FLLOTT
        .w_CODMAG = .w_CODMAG
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .w_KEYSAL
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_CODFOR = .w_CODFOR
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_MAGUBI = .w_MAGUBI
        .w_MAGUBI1 = .w_MAGUBI1
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_DATREG = .w_DATREG
        .w_FLPRG = .w_FLPRG
        .w_CODUBI = .w_CODUBI
        .w_NOMSG = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MTSERIAL=space(15)
      .w_MTROWNUM=0
      .w_MTNUMRIF=0
      .w_MAGCAR=space(5)
      .w_QTACA1=0
      .w_MAGSCA=space(5)
      .w_FLPRD=space(1)
      .w_MTKEYSAL=space(20)
      .w_MTCODMAT=space(40)
      .w_MTFLPREN=space(1)
      .w_MTCODMAT=space(40)
      .w_AMPRODU=0
      .w_MTCODLOT=space(20)
      .w_QTASC1=0
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_CODART=space(20)
      .w_CODICE=space(41)
      .w_DESART=space(40)
      .w_UNIMIS1=space(3)
      .w_QTAUM1=0
      .w_FLLOTT=space(1)
      .w_RIGA=0
      .w_MTFLSCAR=space(1)
      .w_MTMAGSCA=space(5)
      .w_MTFLCARI=space(1)
      .w_CODMAG=space(5)
      .w_ESIRIS=space(1)
      .w_KEYSAL=space(20)
      .w_MTRIFNUM=0
      .w_MT__FLAG=space(1)
      .w_MT_SALDO=0
      .w_CODFOR=space(15)
      .w_ARTLOT=space(20)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_MAGUBI=space(5)
      .w_MAGUBI1=space(5)
      .w_SERRIF=space(10)
      .w_ROWRIF=0
      .w_FLRISE=space(1)
      .w_NUMRIF=0
      .w_MOVMAT=.f.
      .w_DATREG=space(5)
      .w_ULTPROG=0
      .w_SECODUBI=space(20)
      .w_SECODLOT=space(20)
      .w_MTMAGCAR=space(5)
      .w_MTCODUBI=space(20)
      .w_MGUBIC=space(1)
      .w_MGUBIC1=space(1)
      .w_RIGACAR=0
      .w_RIGASCA=0
      .w_UNIMIS1=space(3)
      .w_CODLOTTES=space(20)
      .w_CODUBITES=space(20)
      .w_CODUB2TES=space(20)
      .w_ARTLOTTES=space(20)
      .w_FLPRG=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_TOTCAR=0
      .w_TOTSCA=0
      .w_TOTALE=0
      .w_FLSTATTES=space(1)
      .w_CODUBI=space(20)
      .w_MTRIFSTO=0
      .w_NOMSG=.f.
      .w_MTCODCOM=space(15)
      .w_COMCAR=space(15)
      .w_COMSCA=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_MAGCAR = .oParentObject.w_DPCODMAG
        .w_QTACA1 = .oParentObject.w_DPQTAPR1
        .w_MAGSCA = .oParentObject.w_MGSCAR
        .DoRTCalc(7,7,.f.)
        .w_MTKEYSAL = .w_KEYSAL
        .DoRTCalc(9,9,.f.)
        .w_MTFLPREN = .w_FLPRD
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MTCODMAT))
         .link_2_4('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_MTCODLOT))
         .link_2_6('Full')
        endif
        .w_QTASC1 = .oParentObject.w_DPQTASC1
        .DoRTCalc(15,15,.f.)
        .w_MTROWRIF = 0
        .w_CODART = .w_CODART
        .DoRTCalc(18,20,.f.)
        .w_QTAUM1 = .w_QTACA1+.w_QTASC1
        .w_FLLOTT = .w_FLLOTT
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
        .DoRTCalc(24,26,.f.)
        .w_CODMAG = .w_CODMAG
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .w_KEYSAL
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_MTRIFNUM))
         .link_2_10('Full')
        endif
        .w_MT__FLAG = ' '
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .DoRTCalc(32,32,.f.)
        .w_CODFOR = .w_CODFOR
        .DoRTCalc(34,35,.f.)
        .w_LOTZOOM = .F.
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_MAGUBI = .w_MAGUBI
        .w_MAGUBI1 = .w_MAGUBI1
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_MOVMAT = .t.
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_DATREG = .w_DATREG
        .DoRTCalc(45,47,.f.)
        .w_MTMAGCAR = iif(.w_TOTCAR<.w_QTACA1 or empty(.w_MAGSCA), .w_MAGCAR, .w_MAGSCA)
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_MTMAGCAR))
         .link_2_19('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_MTCODUBI))
         .link_2_20('Full')
        endif
        .w_MGUBIC = .w_MGUBIC
        .w_MGUBIC1 = .w_MGUBIC1
        .w_RIGACAR = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGCAR,"        ") , .w_RIGA , 0)
        .w_RIGASCA = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGSCA,"        ") , .w_RIGA ,0 )
        .DoRTCalc(54,55,.f.)
        if not(empty(.w_CODLOTTES))
         .link_1_47('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_CODUBITES))
         .link_1_49('Full')
        endif
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_CODUB2TES))
         .link_1_51('Full')
        endif
        .DoRTCalc(58,58,.f.)
        .w_FLPRG = .w_FLPRG
        .DoRTCalc(60,64,.f.)
        .w_CODUBI = .w_CODUBI
        .DoRTCalc(66,66,.f.)
        .w_NOMSG = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(68,68,.f.)
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_MATR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsco_mmt
    * Dopo l'inserzione della riga rileggo tutto
    This.InitSon()
    * per aggiornare w_MTKEYSAL (sul transitorio)
    This.TrsFromWork()
    
    * verifico se lanciare il caricamento
    If Not Empty(This.w_MTMAGCAR) or Not Empty(This.w_MTMAGSCA)
        * sono in un caricamento verifico se riga aggiunta
        Local L_area,L_Srv
        L_Srv=iif(This.oParentObject.cFunction="Load","A"," ")
        * se riga in append e attivo flag caricamento rapido
        * sulla causale
        If L_Srv='A' And This.oParentObject.w_MTCARI='S'
          This.NotifyEvent('CarRapido')
        Endif
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gsco_mmt
    this.InitSon()
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCODLOTTES_1_47.enabled = i_bVal
      .Page1.oPag.oCODUBITES_1_49.enabled = i_bVal
      .Page1.oPag.oCODUB2TES_1_51.enabled = i_bVal
      .Page1.oPag.oBtn_1_56.enabled = i_bVal
      .Page1.oPag.oBtn_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DIC_MATR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTSERIAL,"MTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTROWNUM,"MTROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTNUMRIF,"MTNUMRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTFLSCAR,"MTFLSCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTMAGSCA,"MTMAGSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTFLCARI,"MTFLCARI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MTCODMAT C(40);
      ,t_MTCODLOT C(20);
      ,t_MTMAGCAR C(5);
      ,t_MTCODUBI C(20);
      ,MTKEYSAL C(20);
      ,MTCODMAT C(40);
      ,MTFLPREN C(1);
      ,MTSERRIF C(10);
      ,MTROWRIF N(4);
      ,MTRIFNUM N(4);
      ,MT__FLAG C(1);
      ,t_MTKEYSAL C(20);
      ,t_MTFLPREN C(1);
      ,t_AMPRODU N(1);
      ,t_MTSERRIF C(10);
      ,t_MTROWRIF N(4);
      ,t_RIGA N(10);
      ,t_MTRIFNUM N(4);
      ,t_MT__FLAG C(1);
      ,t_MT_SALDO N(1);
      ,t_ARTLOT C(20);
      ,t_STALOT C(1);
      ,t_LOTZOOM L(1);
      ,t_MGUBIC C(1);
      ,t_MGUBIC1 C(1);
      ,t_RIGACAR N(10);
      ,t_RIGASCA N(10);
      ,t_MTRIFSTO N(4);
      ,t_MTCODCOM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mmtbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.controlsource=this.cTrsName+'.t_MTCODMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_6.controlsource=this.cTrsName+'.t_MTCODLOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTMAGCAR_2_19.controlsource=this.cTrsName+'.t_MTMAGCAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODUBI_2_20.controlsource=this.cTrsName+'.t_MTCODUBI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(62)
    this.AddVLine(360)
    this.AddVLine(518)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_MATR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_MATR_IDX,2])
      *
      * insert into DIC_MATR
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_MATR')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_MATR')
        i_cFldBody=" "+;
                  "(MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"+;
                  ",MTFLPREN,MTCODLOT,MTSERRIF,MTROWRIF,MTFLSCAR"+;
                  ",MTMAGSCA,MTFLCARI,MTRIFNUM,MT__FLAG,MT_SALDO"+;
                  ",MTMAGCAR,MTCODUBI,MTRIFSTO,MTCODCOM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MTSERIAL)+","+cp_ToStrODBC(this.w_MTROWNUM)+","+cp_ToStrODBC(this.w_MTNUMRIF)+","+cp_ToStrODBC(this.w_MTKEYSAL)+","+cp_ToStrODBC(this.w_MTCODMAT)+;
             ","+cp_ToStrODBC(this.w_MTFLPREN)+","+cp_ToStrODBCNull(this.w_MTCODLOT)+","+cp_ToStrODBC(this.w_MTSERRIF)+","+cp_ToStrODBC(this.w_MTROWRIF)+","+cp_ToStrODBC(this.w_MTFLSCAR)+;
             ","+cp_ToStrODBC(this.w_MTMAGSCA)+","+cp_ToStrODBC(this.w_MTFLCARI)+","+cp_ToStrODBCNull(this.w_MTRIFNUM)+","+cp_ToStrODBC(this.w_MT__FLAG)+","+cp_ToStrODBC(this.w_MT_SALDO)+;
             ","+cp_ToStrODBCNull(this.w_MTMAGCAR)+","+cp_ToStrODBCNull(this.w_MTCODUBI)+","+cp_ToStrODBC(this.w_MTRIFSTO)+","+cp_ToStrODBC(this.w_MTCODCOM)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_MATR')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_MATR')
        cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MTSERIAL,'MTROWNUM',this.w_MTROWNUM,'MTKEYSAL',this.w_MTKEYSAL,'MTCODMAT',this.w_MTCODMAT,'MTCODMAT',this.w_MTCODMAT)
        INSERT INTO (i_cTable) (;
                   MTSERIAL;
                  ,MTROWNUM;
                  ,MTNUMRIF;
                  ,MTKEYSAL;
                  ,MTCODMAT;
                  ,MTFLPREN;
                  ,MTCODLOT;
                  ,MTSERRIF;
                  ,MTROWRIF;
                  ,MTFLSCAR;
                  ,MTMAGSCA;
                  ,MTFLCARI;
                  ,MTRIFNUM;
                  ,MT__FLAG;
                  ,MT_SALDO;
                  ,MTMAGCAR;
                  ,MTCODUBI;
                  ,MTRIFSTO;
                  ,MTCODCOM;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MTSERIAL;
                  ,this.w_MTROWNUM;
                  ,this.w_MTNUMRIF;
                  ,this.w_MTKEYSAL;
                  ,this.w_MTCODMAT;
                  ,this.w_MTFLPREN;
                  ,this.w_MTCODLOT;
                  ,this.w_MTSERRIF;
                  ,this.w_MTROWRIF;
                  ,this.w_MTFLSCAR;
                  ,this.w_MTMAGSCA;
                  ,this.w_MTFLCARI;
                  ,this.w_MTRIFNUM;
                  ,this.w_MT__FLAG;
                  ,this.w_MT_SALDO;
                  ,this.w_MTMAGCAR;
                  ,this.w_MTCODUBI;
                  ,this.w_MTRIFSTO;
                  ,this.w_MTCODCOM;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_MATR_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_MTCODMAT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_MATR')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)+;
                 ",MTFLSCAR="+cp_ToStrODBC(this.w_MTFLSCAR)+;
                 ",MTMAGSCA="+cp_ToStrODBC(this.w_MTMAGSCA)+;
                 ",MTFLCARI="+cp_ToStrODBC(this.w_MTFLCARI)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                 " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                 " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_MATR')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  MTNUMRIF=this.w_MTNUMRIF;
                 ,MTFLSCAR=this.w_MTFLSCAR;
                 ,MTMAGSCA=this.w_MTMAGSCA;
                 ,MTFLCARI=this.w_MTFLCARI;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                      and MTCODMAT=&i_TN.->MTCODMAT;
                      and MTCODMAT=&i_TN.->MTCODMAT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MTCODMAT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MTKEYSAL=&i_TN.->MTKEYSAL;
                            and MTCODMAT=&i_TN.->MTCODMAT;
                            and MTCODMAT=&i_TN.->MTCODMAT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MTKEYSAL with this.w_MTKEYSAL
              replace MTCODMAT with this.w_MTCODMAT
              replace MTCODMAT with this.w_MTCODMAT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DIC_MATR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_MATR')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)+;
                     ",MTFLPREN="+cp_ToStrODBC(this.w_MTFLPREN)+;
                     ",MTCODLOT="+cp_ToStrODBCNull(this.w_MTCODLOT)+;
                     ",MTSERRIF="+cp_ToStrODBC(this.w_MTSERRIF)+;
                     ",MTROWRIF="+cp_ToStrODBC(this.w_MTROWRIF)+;
                     ",MTFLSCAR="+cp_ToStrODBC(this.w_MTFLSCAR)+;
                     ",MTMAGSCA="+cp_ToStrODBC(this.w_MTMAGSCA)+;
                     ",MTFLCARI="+cp_ToStrODBC(this.w_MTFLCARI)+;
                     ",MTRIFNUM="+cp_ToStrODBCNull(this.w_MTRIFNUM)+;
                     ",MT__FLAG="+cp_ToStrODBC(this.w_MT__FLAG)+;
                     ",MT_SALDO="+cp_ToStrODBC(this.w_MT_SALDO)+;
                     ",MTMAGCAR="+cp_ToStrODBCNull(this.w_MTMAGCAR)+;
                     ",MTCODUBI="+cp_ToStrODBCNull(this.w_MTCODUBI)+;
                     ",MTRIFSTO="+cp_ToStrODBC(this.w_MTRIFSTO)+;
                     ",MTCODCOM="+cp_ToStrODBC(this.w_MTCODCOM)+;
                     ",MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)+;
                     ",MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MTKEYSAL="+cp_ToStrODBC(MTKEYSAL)+;
                             " and MTCODMAT="+cp_ToStrODBC(MTCODMAT)+;
                             " and MTCODMAT="+cp_ToStrODBC(MTCODMAT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_MATR')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MTNUMRIF=this.w_MTNUMRIF;
                     ,MTFLPREN=this.w_MTFLPREN;
                     ,MTCODLOT=this.w_MTCODLOT;
                     ,MTSERRIF=this.w_MTSERRIF;
                     ,MTROWRIF=this.w_MTROWRIF;
                     ,MTFLSCAR=this.w_MTFLSCAR;
                     ,MTMAGSCA=this.w_MTMAGSCA;
                     ,MTFLCARI=this.w_MTFLCARI;
                     ,MTRIFNUM=this.w_MTRIFNUM;
                     ,MT__FLAG=this.w_MT__FLAG;
                     ,MT_SALDO=this.w_MT_SALDO;
                     ,MTMAGCAR=this.w_MTMAGCAR;
                     ,MTCODUBI=this.w_MTCODUBI;
                     ,MTRIFSTO=this.w_MTRIFSTO;
                     ,MTCODCOM=this.w_MTCODCOM;
                     ,MTKEYSAL=this.w_MTKEYSAL;
                     ,MTCODMAT=this.w_MTCODMAT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                                      and MTCODMAT=&i_TN.->MTCODMAT;
                                      and MTCODMAT=&i_TN.->MTCODMAT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MTFLPREN,space(1))==this.w_MTFLPREN;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MTFLPREN,space(1)),'AM_PRENO','',1,'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MTCODMAT,space(40))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" AM_PRENO="+i_cOp1+","           +" AM_PRODU=AM_PRODU - "+cp_ToStrODBC(1)+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE AMCODICE="+cp_ToStrODBC(NVL(&i_cF..MTCODMAT,space(40)));
             +" AND AMKEYSAL="+cp_ToStrODBC(NVL(&i_cF..MTKEYSAL,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MTFLPREN,'AM_PRENO','1',1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'AMKEYSAL',&i_cF..MTKEYSAL;
                 ,'AMCODICE',&i_cF..MTCODMAT)
      UPDATE (i_cTable) SET ;
           AM_PRENO=&i_cOp1.  ,;
           AM_PRODU=AM_PRODU-1  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.MOVIMATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTRIFNUM,0)==this.w_MTRIFNUM;
              and NVL(&i_cF..MTSERRIF,space(10))==this.w_MTSERRIF;
              and NVL(&i_cF..MTROWRIF,0)==this.w_MTROWRIF;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MT__FLAG,space(1)),'MT_SALDO','',1,'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MTRIFNUM,0)
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" MT_SALDO="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE MTNUMRIF="+cp_ToStrODBC(NVL(&i_cF..MTRIFNUM,0));
             +" AND MTSERIAL="+cp_ToStrODBC(NVL(&i_cF..MTSERRIF,space(10)));
             +" AND MTROWNUM="+cp_ToStrODBC(NVL(&i_cF..MTROWRIF,0));
             +" AND MTKEYSAL="+cp_ToStrODBC(NVL(&i_cF..MTKEYSAL,space(20)));
             +" AND MTCODMAT="+cp_ToStrODBC(NVL(&i_cF..MTCODMAT,space(40)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MT__FLAG,'MT_SALDO','1',1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'MTSERIAL',&i_cF..MTSERRIF;
                 ,'MTROWNUM',&i_cF..MTROWRIF;
                 ,'MTKEYSAL',&i_cF..MTKEYSAL;
                 ,'MTCODMAT',&i_cF..MTCODMAT;
                 ,'MTNUMRIF',&i_cF..MTRIFNUM)
      UPDATE (i_cTable) SET ;
           MT_SALDO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MTFLPREN,space(1))==this.w_MTFLPREN;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;

      i_cOp1=cp_SetTrsOp(this.w_MTFLPREN,'AM_PRENO','1',1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MTCODMAT)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" AM_PRENO="+i_cOp1  +",";
         +" AM_PRODU=AM_PRODU + "+cp_ToStrODBC(1)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE AMCODICE="+cp_ToStrODBC(this.w_MTCODMAT);
           +" AND AMKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MTFLPREN,'AM_PRENO','1',1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'AMKEYSAL',this.w_MTKEYSAL;
                 ,'AMCODICE',this.w_MTCODMAT)
      UPDATE (i_cTable) SET;
           AM_PRENO=&i_cOp1.  ,;
           AM_PRODU=AM_PRODU+1  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.MOVIMATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTRIFNUM,0)==this.w_MTRIFNUM;
              and NVL(&i_cF..MTSERRIF,space(10))==this.w_MTSERRIF;
              and NVL(&i_cF..MTROWRIF,0)==this.w_MTROWRIF;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;

      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'MT_SALDO','1',1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MTRIFNUM)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" MT_SALDO="+i_cOp1  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE MTNUMRIF="+cp_ToStrODBC(this.w_MTRIFNUM);
           +" AND MTSERIAL="+cp_ToStrODBC(this.w_MTSERRIF);
           +" AND MTROWNUM="+cp_ToStrODBC(this.w_MTROWRIF);
           +" AND MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
           +" AND MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'MT_SALDO','1',1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'MTSERIAL',this.w_MTSERRIF;
                 ,'MTROWNUM',this.w_MTROWRIF;
                 ,'MTKEYSAL',this.w_MTKEYSAL;
                 ,'MTCODMAT',this.w_MTCODMAT;
                 ,'MTNUMRIF',this.w_MTRIFNUM)
      UPDATE (i_cTable) SET;
           MT_SALDO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_MATR_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MTCODMAT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DIC_MATR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MTKEYSAL=&i_TN.->MTKEYSAL;
                              and MTCODMAT=&i_TN.->MTCODMAT;
                              and MTCODMAT=&i_TN.->MTCODMAT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MTCODMAT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_MATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_MATR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
          .link_2_4('Full')
        .DoRTCalc(12,16,.t.)
          .w_CODART = .w_CODART
        .DoRTCalc(18,20,.t.)
          .w_QTAUM1 = .w_QTACA1+.w_QTASC1
          .w_FLLOTT = .w_FLLOTT
        if .o_MTCODMAT<>.w_MTCODMAT
          .w_TOTALE = .w_TOTALE-.w_riga
          .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
          .w_TOTALE = .w_TOTALE+.w_riga
        endif
        .DoRTCalc(24,26,.t.)
          .w_CODMAG = .w_CODMAG
          .w_ESIRIS = .w_ESIRIS
          .w_KEYSAL = .w_KEYSAL
          .link_2_10('Full')
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .DoRTCalc(31,32,.t.)
          .w_CODFOR = .w_CODFOR
        .DoRTCalc(34,35,.t.)
        if .o_MTCODLOT<>.w_MTCODLOT
          .w_LOTZOOM = .F.
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .w_MAGUBI = .w_MAGUBI
          .w_MAGUBI1 = .w_MAGUBI1
          .w_SERRIF = .w_SERRIF
          .w_ROWRIF = .w_ROWRIF
          .w_FLRISE = .w_FLRISE
          .w_NUMRIF = .w_NUMRIF
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .DoRTCalc(43,43,.t.)
          .w_DATREG = .w_DATREG
        .DoRTCalc(45,49,.t.)
          .w_MGUBIC = .w_MGUBIC
          .w_MGUBIC1 = .w_MGUBIC1
        if .o_MTCODMAT<>.w_MTCODMAT.or. .o_MTMAGCAR<>.w_MTMAGCAR
          .w_TOTCAR = .w_TOTCAR-.w_rigacar
          .w_RIGACAR = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGCAR,"        ") , .w_RIGA , 0)
          .w_TOTCAR = .w_TOTCAR+.w_rigacar
        endif
        if .o_MTCODMAT<>.w_MTCODMAT.or. .o_MTMAGCAR<>.w_MTMAGCAR
          .w_TOTSCA = .w_TOTSCA-.w_rigasca
          .w_RIGASCA = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGSCA,"        ") , .w_RIGA ,0 )
          .w_TOTSCA = .w_TOTSCA+.w_rigasca
        endif
        .DoRTCalc(54,58,.t.)
          .w_FLPRG = .w_FLPRG
        .DoRTCalc(60,64,.t.)
          .w_CODUBI = .w_CODUBI
        .DoRTCalc(66,66,.t.)
          .w_NOMSG = .T.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(68,68,.t.)
          .w_COMCAR = .w_COMCAR
          .w_COMSCA = .w_COMSCA
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MTKEYSAL with this.w_MTKEYSAL
      replace t_MTFLPREN with this.w_MTFLPREN
      replace t_MTCODMAT with this.w_MTCODMAT
      replace t_AMPRODU with this.w_AMPRODU
      replace t_MTSERRIF with this.w_MTSERRIF
      replace t_MTROWRIF with this.w_MTROWRIF
      replace t_RIGA with this.w_RIGA
      replace t_MTRIFNUM with this.w_MTRIFNUM
      replace t_MT__FLAG with this.w_MT__FLAG
      replace t_MT_SALDO with this.w_MT_SALDO
      replace t_ARTLOT with this.w_ARTLOT
      replace t_STALOT with this.w_STALOT
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_MGUBIC with this.w_MGUBIC
      replace t_MGUBIC1 with this.w_MGUBIC1
      replace t_RIGACAR with this.w_RIGACAR
      replace t_RIGASCA with this.w_RIGASCA
      replace t_MTRIFSTO with this.w_MTRIFSTO
      replace t_MTCODCOM with this.w_MTCODCOM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODLOTTES_1_47.enabled = this.oPgFrm.Page1.oPag.oCODLOTTES_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCODUBITES_1_49.enabled = this.oPgFrm.Page1.oPag.oCODUBITES_1_49.mCond()
    this.oPgFrm.Page1.oPag.oCODUB2TES_1_51.enabled = this.oPgFrm.Page1.oPag.oCODUB2TES_1_51.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTCODLOT_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTCODLOT_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTMAGCAR_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTMAGCAR_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTCODUBI_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTCODUBI_2_20.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_56.visible=!this.oPgFrm.Page1.oPag.oBtn_1_56.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_57.visible=!this.oPgFrm.Page1.oPag.oBtn_1_57.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MTCODMAT
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE,AM_PRODU";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MTCODMAT);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_MTKEYSAL;
                       ,'AMCODICE',this.w_MTCODMAT)
            select AMKEYSAL,AMCODICE,AM_PRODU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODMAT = NVL(_Link_.AMCODICE,space(40))
      this.w_AMPRODU = NVL(_Link_.AM_PRODU,0)
    else
      if i_cCtrl<>'Load'
        this.w_MTCODMAT = space(40)
      endif
      this.w_AMPRODU = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MATRICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.AMCODICE as AMCODICE204"+ ",link_2_4.AM_PRODU as AM_PRODU204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DIC_MATR.MTCODMAT=link_2_4.AMCODICE"+" and DIC_MATR.MTKEYSAL=link_2_4.AMKEYSAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DIC_MATR.MTCODMAT=link_2_4.AMCODICE(+)"'+'+" and DIC_MATR.MTKEYSAL=link_2_4.AMKEYSAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MTCODLOT
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_MTCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_MTCODLOT))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MTCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MTCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oMTCODLOT_2_6'),i_cWhere,'GSAR_ALO',"Elenco lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_MTCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_MTCODLOT)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTLOT=.w_CODART
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        endif
        this.w_MTCODLOT = space(20)
        this.w_ARTLOT = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTRIFNUM
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOVIMATR_IDX,3]
    i_lTable = "MOVIMATR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2], .t., this.MOVIMATR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTRIFNUM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTRIFNUM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT,MTNUMRIF";
                   +" from "+i_cTable+" "+i_lTable+" where MTNUMRIF="+cp_ToStrODBC(this.w_MTRIFNUM);
                   +" and MTSERIAL="+cp_ToStrODBC(this.w_MTSERRIF);
                   +" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWRIF);
                   +" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
                   +" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTSERIAL',this.w_MTSERRIF;
                       ,'MTROWNUM',this.w_MTROWRIF;
                       ,'MTKEYSAL',this.w_MTKEYSAL;
                       ,'MTCODMAT',this.w_MTCODMAT;
                       ,'MTNUMRIF',this.w_MTRIFNUM)
            select MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT,MTNUMRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTRIFNUM = NVL(_Link_.MTNUMRIF,0)
    else
      if i_cCtrl<>'Load'
        this.w_MTRIFNUM = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])+'\'+cp_ToStr(_Link_.MTSERIAL,1)+'\'+cp_ToStr(_Link_.MTROWNUM,1)+'\'+cp_ToStr(_Link_.MTKEYSAL,1)+'\'+cp_ToStr(_Link_.MTCODMAT,1)+'\'+cp_ToStr(_Link_.MTNUMRIF,1)
      cp_ShowWarn(i_cKey,this.MOVIMATR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTRIFNUM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTMAGCAR
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTMAGCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MTMAGCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MTMAGCAR))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MTMAGCAR)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MTMAGCAR) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMTMAGCAR_2_19'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTMAGCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MTMAGCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MTMAGCAR)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTMAGCAR = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MTMAGCAR = space(5)
      endif
      this.w_MGUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MTMAGCAR=.w_MAGCAR and .w_QTAUM1>0) or (.w_MTMAGCAR=.w_MAGSCA and .w_QTASC1>0)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MTMAGCAR = space(5)
        this.w_MGUBIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTMAGCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.MGCODMAG as MGCODMAG219"+ ",link_2_19.MGFLUBIC as MGFLUBIC219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on DIC_MATR.MTMAGCAR=link_2_19.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and DIC_MATR.MTMAGCAR=link_2_19.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MTCODUBI
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_MTCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MTMAGCAR);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MTMAGCAR;
                     ,'UBCODICE',trim(this.w_MTCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MTCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MTCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oMTCODUBI_2_20'),i_cWhere,'GSAR_AUB',"Elenco ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MTMAGCAR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MTMAGCAR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_MTCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MTMAGCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MTMAGCAR;
                       ,'UBCODICE',this.w_MTCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLOTTES
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLOTTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_CODLOTTES)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE,LOCODART,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_CODLOTTES))
          select LOCODICE,LOCODART,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLOTTES)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLOTTES) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oCODLOTTES_1_47'),i_cWhere,'GSAR_ALO',"Elenco lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE,LOCODART,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE,LOCODART,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLOTTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE,LOCODART,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODLOTTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_CODLOTTES)
            select LOCODICE,LOCODART,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLOTTES = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOTTES = NVL(_Link_.LOCODART,space(20))
      this.w_FLSTATTES = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODLOTTES = space(20)
      endif
      this.w_ARTLOTTES = space(20)
      this.w_FLSTATTES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTLOTTES=.w_CODART
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        endif
        this.w_CODLOTTES = space(20)
        this.w_ARTLOTTES = space(20)
        this.w_FLSTATTES = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLOTTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBITES
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBITES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBITES)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGCAR);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MAGCAR;
                     ,'UBCODICE',trim(this.w_CODUBITES))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBITES)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBITES) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBITES_1_49'),i_cWhere,'GSAR_AUB',"Elenco ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGCAR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGCAR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBITES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBITES);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGCAR;
                       ,'UBCODICE',this.w_CODUBITES)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBITES = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBITES = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBITES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUB2TES
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUB2TES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUB2TES)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGSCA);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MAGSCA;
                     ,'UBCODICE',trim(this.w_CODUB2TES))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUB2TES)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUB2TES) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUB2TES_1_51'),i_cWhere,'GSAR_AUB',"Elenco ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGSCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGSCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUB2TES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUB2TES);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGSCA;
                       ,'UBCODICE',this.w_CODUB2TES)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUB2TES = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUB2TES = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUB2TES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMAGCAR_1_4.value==this.w_MAGCAR)
      this.oPgFrm.Page1.oPag.oMAGCAR_1_4.value=this.w_MAGCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oQTACA1_1_5.value==this.w_QTACA1)
      this.oPgFrm.Page1.oPag.oQTACA1_1_5.value=this.w_QTACA1
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGSCA_1_6.value==this.w_MAGSCA)
      this.oPgFrm.Page1.oPag.oMAGSCA_1_6.value=this.w_MAGSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oQTASC1_1_8.value==this.w_QTASC1)
      this.oPgFrm.Page1.oPag.oQTASC1_1_8.value=this.w_QTASC1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_11.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_11.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_12.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_12.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS1_1_14.value==this.w_UNIMIS1)
      this.oPgFrm.Page1.oPag.oUNIMIS1_1_14.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS1_1_45.value==this.w_UNIMIS1)
      this.oPgFrm.Page1.oPag.oUNIMIS1_1_45.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLOTTES_1_47.value==this.w_CODLOTTES)
      this.oPgFrm.Page1.oPag.oCODLOTTES_1_47.value=this.w_CODLOTTES
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBITES_1_49.value==this.w_CODUBITES)
      this.oPgFrm.Page1.oPag.oCODUBITES_1_49.value=this.w_CODUBITES
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUB2TES_1_51.value==this.w_CODUB2TES)
      this.oPgFrm.Page1.oPag.oCODUB2TES_1_51.value=this.w_CODUB2TES
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCAR_3_3.value==this.w_TOTCAR)
      this.oPgFrm.Page1.oPag.oTOTCAR_3_3.value=this.w_TOTCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSCA_3_4.value==this.w_TOTSCA)
      this.oPgFrm.Page1.oPag.oTOTSCA_3_4.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value==this.w_MTCODMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value=this.w_MTCODMAT
      replace t_MTCODMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_6.value==this.w_MTCODLOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_6.value=this.w_MTCODLOT
      replace t_MTCODLOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTMAGCAR_2_19.value==this.w_MTMAGCAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTMAGCAR_2_19.value=this.w_MTMAGCAR
      replace t_MTMAGCAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTMAGCAR_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODUBI_2_20.value==this.w_MTCODUBI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODUBI_2_20.value=this.w_MTCODUBI
      replace t_MTCODUBI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODUBI_2_20.value
    endif
    cp_SetControlsValueExtFlds(this,'DIC_MATR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_QTACA1=.w_TOTCAR AND .w_QTASC1=.w_TOTSCA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Il numero di matricole movimentate deve coincidere con le quantit� dichiarate."))
          case   not(.w_ARTLOTTES=.w_CODART)  and (g_PERLOT='S' AND .w_FLLOTT $ 'S-C' And .w_MT_SALDO=0 And .w_ESIRIS='E' And Not Empty(.w_MTMAGCAR))  and not(empty(.w_CODLOTTES))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODLOTTES_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lotto inesistente o di un altro articolo")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsco_mmt
        IF (.w_QTACA1<>.w_TOTCAR OR .w_QTASC1<>.w_TOTSCA) and i_bRes And .bOnScreen
          i_bRes=ah_YESNO("Il numero di matricole movimentate deve coincidere con la quantit� di riga, non sar� possibile confermare la registrazione. Confermi ugualmente?")
        endif
      
        * Valorizzo con w_TOTCAR la variabile w_MATCAR
        * del padre (solo se a video)
        IF this.bOnScreen and i_bRes
          with this.oParentObject
            .w_MATCAR=this.w_TOTCAR
            .w_MATSCA=this.w_TOTSCA
            .w_DPCODLOT=this.w_CODLOTTES
            .w_ARTLOT=this.w_ARTLOTTES
            .w_FLSTAT=this.w_FLSTATTES
            .w_DPCODUBI=this.w_CODUBITES
            .bUpdated=.t.
            .mCalc(.t.)
          endwith
        endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_MTCODLOT) or not(.w_ARTLOT=.w_CODART)) and (g_PERLOT='S' AND .w_FLLOTT='S' And .w_ESIRIS='E' And .w_MT_SALDO=0) and (not(Empty(.w_MTCODMAT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_6
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Lotto inesistente o di un altro articolo")
        case   (empty(.w_MTMAGCAR) or not((.w_MTMAGCAR=.w_MAGCAR and .w_QTAUM1>0) or (.w_MTMAGCAR=.w_MAGSCA and .w_QTASC1>0))) and (.w_QTAUM1>0 AND .w_QTASC1>0) and (not(Empty(.w_MTCODMAT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTMAGCAR_2_19
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_MTCODUBI) and (g_PERUBI='S' AND .w_MGUBIC='S' And .w_MT_SALDO=0 And .w_ESIRIS='E' And Not Empty(.w_MTMAGCAR)) and (not(Empty(.w_MTCODMAT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODUBI_2_20
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_MTCODMAT))
        * --- Area Manuale = Check Row
        * --- gsco_mmt
        * Controllo che la matricola inserita non sia gia presente
        * nel dettaglio
        Local L_Posizione,L_Area,L_CodMat,L_Trovato
        
        L_Area = Select()
        
        Select (this.cTrsName)
        L_Posizione=Recno(this.cTrsName)
        L_CodMat= t_MTCODMAT
        
        if L_Posizione<>0 And Not Empty(L_CodMat)
            Go Top
            LOCATE FOR t_MTCODMAT = L_CodMat And Not Deleted()
            L_Trovato=Found()
            if L_Trovato And Recno()<>L_Posizione
              i_bRes = .f.
              i_bnoChk = .f.
              i_cErrorMsg = Ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
            ELse
              if L_Trovato
                Continue
                if Found() And Recno()<>L_Posizione
                  i_bRes = .f.
                  i_bnoChk = .f.
                  i_cErrorMsg =Ah_MsgFormat( "Matricola gi� utilizzata nel dettaglio")
                endif
              Endif
            Endif
            * mi riposiziono nella riga di partenza
            Select (this.cTrsName)
            Go L_Posizione
        endif
        
        * mi rimetto nella vecchia area
        Select (L_Area)
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MTCODMAT = this.w_MTCODMAT
    this.o_MTCODLOT = this.w_MTCODLOT
    this.o_MTMAGCAR = this.w_MTMAGCAR
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MTCODMAT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MTKEYSAL=space(20)
      .w_MTCODMAT=space(40)
      .w_MTFLPREN=space(1)
      .w_MTCODMAT=space(40)
      .w_AMPRODU=0
      .w_MTCODLOT=space(20)
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_RIGA=0
      .w_MTRIFNUM=0
      .w_MT__FLAG=space(1)
      .w_MT_SALDO=0
      .w_ARTLOT=space(20)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_MTMAGCAR=space(5)
      .w_MTCODUBI=space(20)
      .w_MGUBIC=space(1)
      .w_MGUBIC1=space(1)
      .w_RIGACAR=0
      .w_RIGASCA=0
      .w_MTRIFSTO=0
      .w_MTCODCOM=space(15)
      .DoRTCalc(1,7,.f.)
        .w_MTKEYSAL = .w_KEYSAL
      .DoRTCalc(9,9,.f.)
        .w_MTFLPREN = .w_FLPRD
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_MTCODMAT))
        .link_2_4('Full')
      endif
      .DoRTCalc(12,13,.f.)
      if not(empty(.w_MTCODLOT))
        .link_2_6('Full')
      endif
      .DoRTCalc(14,15,.f.)
        .w_MTROWRIF = 0
      .DoRTCalc(17,22,.f.)
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
      .DoRTCalc(24,30,.f.)
      if not(empty(.w_MTRIFNUM))
        .link_2_10('Full')
      endif
        .w_MT__FLAG = ' '
      .DoRTCalc(32,35,.f.)
        .w_LOTZOOM = .F.
      .DoRTCalc(37,47,.f.)
        .w_MTMAGCAR = iif(.w_TOTCAR<.w_QTACA1 or empty(.w_MAGSCA), .w_MAGCAR, .w_MAGSCA)
      .DoRTCalc(48,48,.f.)
      if not(empty(.w_MTMAGCAR))
        .link_2_19('Full')
      endif
      .DoRTCalc(49,49,.f.)
      if not(empty(.w_MTCODUBI))
        .link_2_20('Full')
      endif
        .w_MGUBIC = .w_MGUBIC
        .w_MGUBIC1 = .w_MGUBIC1
        .w_RIGACAR = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGCAR,"        ") , .w_RIGA , 0)
        .w_RIGASCA = IIF( !empty(nvl(.w_MTMAGCAR,"")) and .w_MTMAGCAR=nvl(.w_MAGSCA,"        ") , .w_RIGA ,0 )
    endwith
    this.DoRTCalc(54,70,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MTKEYSAL = t_MTKEYSAL
    this.w_MTCODMAT = t_MTCODMAT
    this.w_MTFLPREN = t_MTFLPREN
    this.w_MTCODMAT = t_MTCODMAT
    this.w_AMPRODU = t_AMPRODU
    this.w_MTCODLOT = t_MTCODLOT
    this.w_MTSERRIF = t_MTSERRIF
    this.w_MTROWRIF = t_MTROWRIF
    this.w_RIGA = t_RIGA
    this.w_MTRIFNUM = t_MTRIFNUM
    this.w_MT__FLAG = t_MT__FLAG
    this.w_MT_SALDO = t_MT_SALDO
    this.w_ARTLOT = t_ARTLOT
    this.w_STALOT = t_STALOT
    this.w_LOTZOOM = t_LOTZOOM
    this.w_MTMAGCAR = t_MTMAGCAR
    this.w_MTCODUBI = t_MTCODUBI
    this.w_MGUBIC = t_MGUBIC
    this.w_MGUBIC1 = t_MGUBIC1
    this.w_RIGACAR = t_RIGACAR
    this.w_RIGASCA = t_RIGASCA
    this.w_MTRIFSTO = t_MTRIFSTO
    this.w_MTCODCOM = t_MTCODCOM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MTKEYSAL with this.w_MTKEYSAL
    replace t_MTCODMAT with this.w_MTCODMAT
    replace t_MTFLPREN with this.w_MTFLPREN
    replace t_MTCODMAT with this.w_MTCODMAT
    replace t_AMPRODU with this.w_AMPRODU
    replace t_MTCODLOT with this.w_MTCODLOT
    replace t_MTSERRIF with this.w_MTSERRIF
    replace t_MTROWRIF with this.w_MTROWRIF
    replace t_RIGA with this.w_RIGA
    replace t_MTRIFNUM with this.w_MTRIFNUM
    replace t_MT__FLAG with this.w_MT__FLAG
    replace t_MT_SALDO with this.w_MT_SALDO
    replace t_ARTLOT with this.w_ARTLOT
    replace t_STALOT with this.w_STALOT
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_MTMAGCAR with this.w_MTMAGCAR
    replace t_MTCODUBI with this.w_MTCODUBI
    replace t_MGUBIC with this.w_MGUBIC
    replace t_MGUBIC1 with this.w_MGUBIC1
    replace t_RIGACAR with this.w_RIGACAR
    replace t_RIGASCA with this.w_RIGASCA
    replace t_MTRIFSTO with this.w_MTRIFSTO
    replace t_MTCODCOM with this.w_MTCODCOM
    if i_srv='A'
      replace MTKEYSAL with this.w_MTKEYSAL
      replace MTCODMAT with this.w_MTCODMAT
      replace MTCODMAT with this.w_MTCODMAT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_riga
        .w_TOTCAR = .w_TOTCAR-.w_rigacar
        .w_TOTSCA = .w_TOTSCA-.w_rigasca
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mmtPag1 as StdContainer
  Width  = 702
  height = 439
  stdWidth  = 702
  stdheight = 439
  resizeXpos=160
  resizeYpos=268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMAGCAR_1_4 as StdField with uid="SUQZHNWWMY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MAGCAR", cQueryName = "MAGCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 177082566,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=66, Top=27, InputMask=replicate('X',5)

  func oMAGCAR_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODUBITES)
        bRes2=.link_1_49('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oQTACA1_1_5 as StdField with uid="FONWPHGIIR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_QTACA1", cQueryName = "QTACA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nella prima unit� di misura",;
    HelpContextID = 108149754,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=276, Top=27, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oMAGSCA_1_6 as StdField with uid="RHACIWKLOL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MAGSCA", cQueryName = "MAGSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 104984378,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=66, Top=50, InputMask=replicate('X',5)

  func oMAGSCA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODUB2TES)
        bRes2=.link_1_51('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oQTASC1_1_8 as StdField with uid="RTHDOFNRAY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_QTASC1", cQueryName = "QTASC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nella prima unit� di misura",;
    HelpContextID = 105004026,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=276, Top=50, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oCODICE_1_11 as StdField with uid="TTFNZFCAPF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca",;
    HelpContextID = 38539738,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=66, Top=4, InputMask=replicate('X',41)

  add object oDESART_1_12 as StdField with uid="DZOJCGBEDM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228381750,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=379, Top=4, InputMask=replicate('X',40)

  add object oUNIMIS1_1_14 as StdField with uid="CXTKOINJOE",rtseq=20,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 202915398,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=195, Top=50, InputMask=replicate('X',3)


  add object oObj_1_27 as cp_runprogram with uid="ZRZPFWLUDD",left=-2, top=469, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('DICHCHANGED')",;
    cEvent = "w_MTCODMAT Changed",;
    nPag=1;
    , ToolTipText = "Al cambio della matricola effettua tutti i controlli pi� eventuali letture";
    , HelpContextID = 208634547


  add object oObj_1_30 as cp_runprogram with uid="ACNKDSSMKQ",left=-2, top=445, width=188,height=24,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('DELETEROW')",;
    cEvent = "Delete row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la cancellazione della matricola";
    , HelpContextID = 209314483


  add object oObj_1_31 as cp_runprogram with uid="AWQPAKNIED",left=189, top=445, width=185,height=24,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('UPDATEROW')",;
    cEvent = "Update row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la modifica della matricola";
    , HelpContextID = 209314483


  add object oObj_1_32 as cp_runprogram with uid="VECODULZZQ",left=378, top=445, width=176,height=24,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('INSERTROW')",;
    cEvent = "Insert row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la cancellazione della matricola";
    , HelpContextID = 209314483


  add object oObj_1_39 as cp_runprogram with uid="RIFVIWSAID",left=-2, top=493, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('DICHCARRAPIDO')",;
    cEvent = "CarRapido",;
    nPag=1;
    , ToolTipText = "lancia il caricamento rapido";
    , HelpContextID = 208634547


  add object oObj_1_41 as cp_runprogram with uid="DHTIGXOYWX",left=-2, top=517, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('DICHCARMAT')",;
    cEvent = "CarMat",;
    nPag=1;
    , ToolTipText = "lancia il caricamento dettaglio";
    , HelpContextID = 208634547

  add object oUNIMIS1_1_45 as StdField with uid="PWQIHJHNET",rtseq=54,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 202915398,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=195, Top=27, InputMask=replicate('X',3)

  add object oCODLOTTES_1_47 as StdField with uid="UNJHWEXEUD",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CODLOTTES", cQueryName = "CODLOTTES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Lotto inesistente o di un altro articolo",;
    ToolTipText = "Imposta lotto di default sulle matricole",;
    HelpContextID = 225899419,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=538, Top=73, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOCODICE", oKey_1_2="this.w_CODLOTTES"

  func oCODLOTTES_1_47.mCond()
    with this.Parent.oContained
      return (g_PERLOT='S' AND .w_FLLOTT $ 'S-C' And .w_MT_SALDO=0 And .w_ESIRIS='E' And Not Empty(.w_MTMAGCAR))
    endwith
  endfunc

  func oCODLOTTES_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLOTTES_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLOTTES_1_47.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"LOTTITES")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oCODLOTTES_1_47.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LOCODICE=this.parent.oContained.w_CODLOTTES
    i_obj.ecpSave()
  endproc

  add object oCODUBITES_1_49 as StdField with uid="ZYSSRPSDXT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CODUBITES", cQueryName = "CODUBITES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Imposta ubicazione di default sulle matricole",;
    HelpContextID = 28308379,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=538, Top=27, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MAGCAR", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBITES"

  func oCODUBITES_1_49.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND .oParentObject.w_FLUBIC='S' And .w_ESIRIS='E')
    endwith
  endfunc

  func oCODUBITES_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBITES_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBITES_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_MAGCAR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_MAGCAR)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBITES_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUB',"Elenco ubicazioni",'',this.parent.oContained
  endproc
  proc oCODUBITES_1_49.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MAGCAR
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBITES
    i_obj.ecpSave()
  endproc

  add object oCODUB2TES_1_51 as StdField with uid="KZQAOVWKLQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CODUB2TES", cQueryName = "CODUB2TES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Imposta ubicazione di default sulle matricole",;
    HelpContextID = 179303323,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=538, Top=50, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MAGSCA", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUB2TES"

  func oCODUB2TES_1_51.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND .oParentObject.w_FLUBIC1='S' And not empty(.w_MAGSCA) And .w_ESIRIS='E')
    endwith
  endfunc

  func oCODUB2TES_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUB2TES_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUB2TES_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_MAGSCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_MAGSCA)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUB2TES_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUB',"Elenco ubicazioni",'',this.parent.oContained
  endproc
  proc oCODUB2TES_1_51.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MAGSCA
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUB2TES
    i_obj.ecpSave()
  endproc


  add object oBtn_1_56 as StdButton with uid="HKJGWQDDOH",left=8, top=389, width=48,height=45,;
    CpPicture="bmp\lotti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per creare un nuovo lotto con i parametri impostati";
    , HelpContextID = 73265482;
    , TabStop=.f.,Caption='\<Lotto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( g_PERLOT='S' AND .w_FLLOTT='S-C' And .w_MT_SALDO=0 And .w_ESIRIS='E' And Not Empty(.w_MTMAGCAR) ))
    endwith
   endif
  endfunc


  add object oBtn_1_57 as StdButton with uid="BRHJBWXMYQ",left=58, top=389, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per attivare inserimento rapido";
    , HelpContextID = 198631462;
    , TabStop=.f.,Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        .NotifyEvent("CarRapido")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTALE>=.w_QTAUM1)
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=99, width=682,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="MTMAGCAR",Label1="Magaz.",Field2="MTCODMAT",Label2="Matricola",Field3="MTCODLOT",Label3="Lotto",Field4="MTCODUBI",Label4="Ubicazione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118326394

  add object oStr_1_9 as StdString with uid="SOYPQWKRXQ",Visible=.t., Left=17, Top=7,;
    Alignment=1, Width=45, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="MZWIFWFMHD",Visible=.t., Left=161, Top=53,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M."  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="OSJFPVSUNF",Visible=.t., Left=237, Top=53,;
    Alignment=1, Width=32, Height=18,;
    Caption="Qt�"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ICCPJRRUUQ",Visible=.t., Left=1, Top=29,;
    Alignment=1, Width=61, Height=18,;
    Caption="Prodotta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HOXNHFDEYL",Visible=.t., Left=3, Top=51,;
    Alignment=1, Width=59, Height=18,;
    Caption="Scartata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="AHQVXNXIMF",Visible=.t., Left=191, Top=475,;
    Alignment=0, Width=427, Height=18,;
    Caption="Le variabili provenienti dal padre sono inizializzate con la funzione initson"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="QDOLLFGSCR",Visible=.t., Left=191, Top=492,;
    Alignment=0, Width=412, Height=18,;
    Caption="nell'area manuale <declare>. Le variabili cos� costruite vanno calcolate"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="PPFHKRJTGP",Visible=.t., Left=191, Top=508,;
    Alignment=0, Width=204, Height=18,;
    Caption="su se stesse senza dipendenze"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="XCPNOYQRVY",Visible=.t., Left=161, Top=29,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M."  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="GEXHHLTNNX",Visible=.t., Left=237, Top=29,;
    Alignment=1, Width=32, Height=18,;
    Caption="Qt�"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="AYERNTVPDY",Visible=.t., Left=436, Top=76,;
    Alignment=1, Width=95, Height=15,;
    Caption="Lotto di default"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="RGDPJMIDGR",Visible=.t., Left=400, Top=31,;
    Alignment=1, Width=131, Height=15,;
    Caption="Ubicazione di default"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="VRUGOTMRCA",Visible=.t., Left=400, Top=54,;
    Alignment=1, Width=131, Height=15,;
    Caption="Ubicazione di default"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=118,;
    width=678+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=119,width=677+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LOTTIART|MAGAZZIN|UBICAZIO|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LOTTIART'
        oDropInto=this.oBodyCol.oRow.oMTCODLOT_2_6
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oMTMAGCAR_2_19
      case cFile='UBICAZIO'
        oDropInto=this.oBodyCol.oRow.oMTCODUBI_2_20
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTCAR_3_3 as StdField with uid="DAEUBEESEV",rtseq=61,rtrep=.f.,;
    cFormVar="w_TOTCAR",value=0,enabled=.f.,;
    ToolTipText = "Numero totale di matricole prodotte",;
    HelpContextID = 177139510,;
    cQueryName = "TOTCAR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=327, Top=392

  add object oTOTSCA_3_4 as StdField with uid="DBXZGNSDJC",rtseq=62,rtrep=.f.,;
    cFormVar="w_TOTSCA",value=0,enabled=.f.,;
    ToolTipText = "Numero totale di matricole scartate",;
    HelpContextID = 104927434,;
    cQueryName = "TOTSCA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=594, Top=392

  add object oStr_3_1 as StdString with uid="FQXVSEZGNA",Visible=.t., Left=164, Top=394,;
    Alignment=1, Width=158, Height=18,;
    Caption="Totale matricole prodotte:"  ;
  , bGlobalFont=.t.

  add object oStr_3_2 as StdString with uid="PTKMNIEUIA",Visible=.t., Left=444, Top=394,;
    Alignment=1, Width=145, Height=18,;
    Caption="Totale matricole scartate:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsco_mmtBodyRow as CPBodyRowCnt
  Width=668
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMTCODMAT_2_2 as StdTrsField with uid="SPOHKTCFXW",rtseq=9,rtrep=.t.,;
    cFormVar="w_MTCODMAT",value=space(40),isprimarykey=.t.,;
    HelpContextID = 97117210,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=53, Top=0, InputMask=replicate('X',40), bHasZoom = .t. 

  func oMTCODMAT_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_MTRIFNUM)
        bRes2=.link_2_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMTCODMAT_2_2.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"DICHZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMTCODLOT_2_6 as StdTrsField with uid="YIIZNTYANT",rtseq=13,rtrep=.t.,;
    cFormVar="w_MTCODLOT",value=space(20),;
    HelpContextID = 188095462,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Lotto inesistente o di un altro articolo",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=350, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , TabStop=.f., cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_MTCODLOT"

  func oMTCODLOT_2_6.mCond()
    with this.Parent.oContained
      return (g_PERLOT='S' AND .w_FLLOTT='S' And .w_ESIRIS='E' And .w_MT_SALDO=0)
    endwith
  endfunc

  func oMTCODLOT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMTCODLOT_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMTCODLOT_2_6.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"LOTTIZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMTCODLOT_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_MTCODLOT
    i_obj.ecpSave()
  endproc

  add object oMTMAGCAR_2_19 as StdTrsField with uid="TNFDSADGXY",rtseq=48,rtrep=.t.,;
    cFormVar="w_MTMAGCAR",value=space(5),;
    HelpContextID = 200049688,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=49, Left=-2, Top=0, InputMask=replicate('X',5), Tabstop=.f., cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MTMAGCAR"

  func oMTMAGCAR_2_19.mCond()
    with this.Parent.oContained
      return (.w_QTAUM1>0 AND .w_QTASC1>0)
    endwith
  endfunc

  func oMTMAGCAR_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
      if .not. empty(.w_MTCODUBI)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMTMAGCAR_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  add object oMTCODUBI_2_20 as StdTrsField with uid="BNADFUNXBS",rtseq=49,rtrep=.t.,;
    cFormVar="w_MTCODUBI",value=space(20),;
    HelpContextID = 231334927,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=510, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , TabStop=.f., cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MTMAGCAR", oKey_2_1="UBCODICE", oKey_2_2="this.w_MTCODUBI"

  func oMTCODUBI_2_20.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND .w_MGUBIC='S' And .w_MT_SALDO=0 And .w_ESIRIS='E' And Not Empty(.w_MTMAGCAR))
    endwith
  endfunc

  func oMTCODUBI_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMTCODUBI_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMTCODUBI_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_MTMAGCAR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_MTMAGCAR)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oMTCODUBI_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUB',"Elenco ubicazioni",'',this.parent.oContained
  endproc
  proc oMTCODUBI_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MTMAGCAR
     i_obj.w_UBCODICE=this.parent.oContained.w_MTCODUBI
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover
  * ---
  func oMTCODMAT_2_2.When()
    return(.t.)
  proc oMTCODMAT_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMTCODMAT_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mmt','DIC_MATR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MTSERIAL=DIC_MATR.MTSERIAL";
  +" and "+i_cAliasName2+".MTROWNUM=DIC_MATR.MTROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
