* --- Container for functions
* --- START CHKCOLA
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkcola                                                         *
*              Controlli doc conto lavoro                                      *
*                                                                              *
*      Author: Zucchetti TAM SPA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-28                                                      *
* Last revis.: 2003-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chkcola
param w_MVCODICE,w_MVUNIMIS,w_MVQTAMOV,w_MVQTAUM1,w_MVFLEVAS,w_MVSERIAL,w_CPROWNUM,w_MVCLVEAC,w_MVCODMAG

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_MVSEROCL
  m.w_MVSEROCL=space(15)
  private w_oMVCODICE
  m.w_oMVCODICE=space(41)
  private w_oMVUNIMIS
  m.w_oMVUNIMIS=space(3)
  private w_oMVQTAMOV
  m.w_oMVQTAMOV=0
  private w_oMVQTAUM1
  m.w_oMVQTAUM1=0
  private w_oMVFLEVAS
  m.w_oMVFLEVAS=space(1)
  private w_oMVCODMAG
  m.w_oMVCODMAG=space(5)
  private w_MVQTAEV1
  m.w_MVQTAEV1=0
  private w_OLTSEODL
  m.w_OLTSEODL=space(15)
  private w_MESS
  m.w_MESS=space(10)
  private w_QTAABB
  m.w_QTAABB=0
* --- WorkFile variables
  private DOC_DETT_idx
  DOC_DETT_idx=0
  private ODL_MAST_idx
  ODL_MAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "chkcola"
if vartype(__chkcola_hook__)='O'
  __chkcola_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chkcola('+Transform(w_MVCODICE)+','+Transform(w_MVUNIMIS)+','+Transform(w_MVQTAMOV)+','+Transform(w_MVQTAUM1)+','+Transform(w_MVFLEVAS)+','+Transform(w_MVSERIAL)+','+Transform(w_CPROWNUM)+','+Transform(w_MVCLVEAC)+','+Transform(w_MVCODMAG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkcola')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if chkcola_OpenTables()
  chkcola_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chkcola('+Transform(w_MVCODICE)+','+Transform(w_MVUNIMIS)+','+Transform(w_MVQTAMOV)+','+Transform(w_MVQTAUM1)+','+Transform(w_MVFLEVAS)+','+Transform(w_MVSERIAL)+','+Transform(w_CPROWNUM)+','+Transform(w_MVCLVEAC)+','+Transform(w_MVCODMAG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkcola')
Endif
*--- Activity log
if vartype(__chkcola_hook__)='O'
  __chkcola_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chkcola_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Chiamata da Area manuale Check-row di GSVE_MDV
  * --- Controlla che non siano stati variati i campi relativi al Conto Lavoro
  * --- Read from DOC_DETT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[DOC_DETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[DOC_DETT_idx,2],.t.,DOC_DETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "MVCODICE,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODODL,MVQTAEV1,MVFLEVAS,MVCODMAG"+;
      " from "+i_cTable+" DOC_DETT where ";
          +"MVSERIAL = "+cp_ToStrODBC(m.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(m.w_CPROWNUM);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      MVCODICE,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODODL,MVQTAEV1,MVFLEVAS,MVCODMAG;
      from (i_cTable) where;
          MVSERIAL = m.w_MVSERIAL;
          and CPROWNUM = m.w_CPROWNUM;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_oMVCODICE = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
    m.w_oMVUNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
    m.w_oMVQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
    m.w_oMVQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
    m.w_MVSEROCL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
    m.w_MVQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
    m.w_oMVFLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
    m.w_oMVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if not empty(nvl(m.w_MVSEROCL,"")) 
    if isnull(m.w_MVFLEVAS) or m.w_MVFLEVAS == ""
      m.w_MVFLEVAS = " "
    endif
    do case
      case m.w_MVCODICE<>m.w_oMVCODICE
        * --- L'anomalia � dovuta sicuramente al conto lavoro
        m.w_MESS = "Impossibile modificare codice articolo su documenti riferiti a conto lavoro"
      case m.w_oMVUNIMIS<>m.w_MVUNIMIS
        m.w_MESS = "Impossibile modificare unit� di misura su documenti riferiti a conto lavoro"
      case m.w_oMVFLEVAS<>m.w_MVFLEVAS
        if m.w_MVFLEVAS="S" and m.w_MVCLVEAC="ORA"
          m.w_MESS = "Impossibile modificare flag evaso su documenti riferiti a conto lavoro%0Utilizzare apposita funzione chiusura OCL ordinati"
        else
          m.w_MESS = "Impossibile modificare flag evaso su documenti riferiti a conto lavoro"
        endif
      case (m.w_oMVQTAMOV<>m.w_MVQTAMOV or m.w_oMVQTAUM1<>m.w_MVQTAUM1) and m.w_oMVFLEVAS="S"
        m.w_MESS = "Impossibile modificare quantit� su documenti riferiti a conto lavoro evasi"
    endcase
  endif
  if not empty(m.w_MESS)
    ah_ErrorMsg(m.w_MESS,"!","")
  endif
  i_retcode = 'stop'
  i_retval = empty(m.w_MESS)
  return
endproc


  function chkcola_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='DOC_DETT'
    i_cWorkTables[2]='ODL_MAST'
    return(cp_OpenFuncTables(2))
* --- END CHKCOLA
* --- START COCALCLT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cocalclt                                                        *
*              Gestione lead time gestionale                                   *
*                                                                              *
*      Author: TAM Software Srl (SM)                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_64]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-22                                                      *
* Last revis.: 2012-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cocalclt
param DataDiBase,LeadTime,TipoCalc,Festivi,Curs_Calen

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private Risultato
  m.Risultato=ctod("  /  /  ")
  private i
  m.i=0
  private w_EMPTYCUR
  m.w_EMPTYCUR=.f.
  private w_GIO
  m.w_GIO=ctod("  /  /  ")
  private w_GIOLAV
  m.w_GIOLAV=space(1)
  private w_ALL
  m.w_ALL=space(1)
  private OldCurs
  m.OldCurs=0
* --- WorkFile variables
  private CAL_AZIE_idx
  CAL_AZIE_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cocalclt"
if vartype(__cocalclt_hook__)='O'
  __cocalclt_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cocalclt('+Transform(DataDiBase)+','+Transform(LeadTime)+','+Transform(TipoCalc)+','+Transform(Festivi)+','+Transform(Curs_Calen)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cocalclt')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if cocalclt_OpenTables()
  cocalclt_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cocalclt('+Transform(DataDiBase)+','+Transform(LeadTime)+','+Transform(TipoCalc)+','+Transform(Festivi)+','+Transform(Curs_Calen)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cocalclt')
Endif
*--- Activity log
if vartype(__cocalclt_hook__)='O'
  __cocalclt_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cocalclt_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- GESTIONE LEAD TIME
  * --- Parametri:
  *        1 - DataDiBase D(8)    - Data di Base
  *        2 - LeadTime    N(3,0) - Lead Time in Giorni
  *        3 - TipoCalc      C(1)    - Tipo Calcolo (A)vanti ; (I)ndietro (C)orrezione LT
  *        4 - Festivi          L(1)    - .T. carica anche le giornate festive ; .F. solo giornate dove ore lavorative>0
  *        5 - Curs_Calen  C(10)  -  Cursore del Calendario: se vuoto esegue la query
  * --- Variabili locali
  * --- Tipo Calcolo (avanti/Indietro)
  m.TipoCalc = iif(Type("TipoCalc")="L" or empty(m.TipoCalc),"I",m.TipoCalc)
  m.w_ALL = iif(Type("Festivi")<>"L" ,"T", iif(m.Festivi,"T", "F"))
  if m.LeadTime=0 or not m.TipoCalc $ "AIC" or empty(m.DataDiBase) or m.DataDiBase=i_INIDAT
    i_retcode = 'stop'
    i_retval = m.DataDiBase
    return
  else
    m.LeadTime = Ceiling(m.LeadTime)
    m.Risultato = m.DataDiBase
    * --- Eventuale correzione parametro
    if m.TipoCalc="C"
      m.TipoCalc = iif(m.LeadTime<0,"A", "I")
      m.LeadTime = abs( m.LeadTime )
    endif
    m.OldCurs = Select()
    m.w_EMPTYCUR = type("Curs_Calen")<>"C" or empty(m.Curs_Calen)
    Calend = IIF(m.w_EMPTYCUR, sys(2015), m.Curs_Calen)
    * --- Applica lead-time ...
    * --- Cicla lungo la query per estrarre dal calendario i soli giorni lavorativi
    if m.TipoCalc="I"
      if m.w_EMPTYCUR
        * --- Esegue query per estrarre dal calendario i soli giorni lavorativi
        DO vq_exec WITH "..\COLA\exe\query\GSCOiBLT", createobject("cp_getfuncvar"), Calend,"",.f.,.t. 
 go BOTTOM
      else
        SELECT (Calend)
        GO BOTTOM
        Select (Calend) 
 SET NEAR ON
        if not seek(dtos(m.DataDiBase)) and not bof()
          * --- Se il giorno � festivo anticipa di un giorno
          skip -1
        endif
      endif
      lOnError = On("Error") 
 On error w_Err=TRUE 
 SKIP -m.LeadTime 
 On error &lOnError 
 Select (Calend)
      m.Risultato = cp_TODATE(CAGIORNO)
      if BOF()
        cp_ErrorMsg("Raggiunto limite inferiore calendario ",48,"")
      endif
    else
      if m.w_EMPTYCUR
        DO vq_exec WITH "..\COLA\exe\query\GSCOaBLT", createobject("cp_getfuncvar"), Calend,"",.f.,.t. 
 go TOP
      else
        Select (m.Curs_Calen) 
 SET NEAR ON 
 seek(dtos(m.DataDiBase))
      endif
      lOnError = On("Error") 
 On error w_Err=TRUE 
 SKIP m.LeadTime 
 On error &lOnError 
 Select (Calend)
      m.Risultato = cp_TODATE(CAGIORNO)
      if EOF()
        cp_ErrorMsg("Raggiunto limite superiore calendario ","!","")
      endif
    endif
    SET_NEAR = Set("NEAR")
    * --- Chiude Cursore
    if m.w_EMPTYCUR AND USED(Calend)
      Use IN (Calend)
    endif
    SET NEAR &SET_NEAR
    Select (m.OldCurs)
    * --- Ritorna risultato
    i_retcode = 'stop'
    i_retval = m.Risultato
    return
  endif
endproc


  function cocalclt_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='CAL_AZIE'
    return(cp_OpenFuncTables(1))
* --- END COCALCLT
* --- START COCHKAR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cochkar                                                         *
*              Controlli articolo produzione                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-26                                                      *
* Last revis.: 2015-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cochkar
param pCodice

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RISULT
  m.w_RISULT=.f.
  private w_CODART
  m.w_CODART=space(20)
  private w_PROVEN
  m.w_PROVEN=space(1)
  private w_CODDIS
  m.w_CODDIS=space(20)
  private w_FLSTAT
  m.w_FLSTAT=space(1)
  private w_ARTIPART
  m.w_ARTIPART=space(2)
* --- WorkFile variables
  private ART_ICOL_idx
  ART_ICOL_idx=0
  private KEY_ARTI_idx
  KEY_ARTI_idx=0
  private DISMBASE_idx
  DISMBASE_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cochkar"
if vartype(__cochkar_hook__)='O'
  __cochkar_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cochkar('+Transform(pCodice)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cochkar')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if cochkar_OpenTables()
  cochkar_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cochkar('+Transform(pCodice)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cochkar')
Endif
*--- Activity log
if vartype(__cochkar_hook__)='O'
  __cochkar_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cochkar_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Esegue dei controlli sugli articoli per il Modulo Conto Lavoro (da GSCO_AOP)
  * --- Esegue controlli ...
  m.w_RISULT = .T.
  if NOT EMPTY(m.pCodice)
    m.w_RISULT = .F.
    m.w_CODART = SPACE(20)
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[KEY_ARTI_idx,2],.t.,KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(m.pCodice);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART;
        from (i_cTable) where;
            CACODICE = m.pCodice;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(m.w_CODART)
      m.w_PROVEN = " "
      m.w_CODDIS = SPACE(20)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[ART_ICOL_idx,2],.t.,ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARPROPRE,ARCODDIS,ARTIPART"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(m.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARPROPRE,ARCODDIS,ARTIPART;
          from (i_cTable) where;
              ARCODART = m.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_PROVEN = NVL(cp_ToDate(_read_.ARPROPRE),cp_NullValue(_read_.ARPROPRE))
        m.w_CODDIS = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
        m.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(m.w_CODDIS) AND m.w_PROVEN $ "IL"
        m.w_FLSTAT = " "
        * --- Read from DISMBASE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[DISMBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[DISMBASE_idx,2],.t.,DISMBASE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DBFLSTAT"+;
            " from "+i_cTable+" DISMBASE where ";
                +"DBCODICE = "+cp_ToStrODBC(m.w_CODDIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DBFLSTAT;
            from (i_cTable) where;
                DBCODICE = m.w_CODDIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_FLSTAT = NVL(cp_ToDate(_read_.DBFLSTAT),cp_NullValue(_read_.DBFLSTAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if m.w_FLSTAT="S"
          m.w_RISULT = .T.
        endif
      else
        if m.w_PROVEN="E" OR m.w_ARTIPART="FS"
          m.w_RISULT = .T.
        endif
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RISULT
  return
endproc


  function cochkar_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='ART_ICOL'
    i_cWorkTables[2]='KEY_ARTI'
    i_cWorkTables[3]='DISMBASE'
    return(cp_OpenFuncTables(3))
* --- END COCHKAR
* --- START COSETLT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cosetlt                                                         *
*              Calcola lead-time                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_37]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-21                                                      *
* Last revis.: 2018-08-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cosetlt
param pCODART,pPROVEN,pQTAMOV,pQTAMO1,pCODFOR,pDATA,pUNIMIS

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=0
  private w_OK0
  m.w_OK0=.f.
  private w_CODART
  m.w_CODART=space(20)
  private w_OK1
  m.w_OK1=.f.
  private w_GIOAPP1
  m.w_GIOAPP1=0
  private w_OK2
  m.w_OK2=.f.
  private w_GIOAPP2
  m.w_GIOAPP2=0
  private w_CODGRU
  m.w_CODGRU=space(5)
  private w_CONUMERO
  m.w_CONUMERO=space(15)
  private w_LEAFIS
  m.w_LEAFIS=0
  private w_LOTMED
  m.w_LOTMED=0
  private w_COEFLT
  m.w_COEFLT=0
  private w_DATORD
  m.w_DATORD=ctod("  /  /  ")
  private w_OLTCOFOR
  m.w_OLTCOFOR=space(15)
  private w_PREZUM
  m.w_PREZUM=space(1)
  private w_UNMIS
  m.w_UNMIS=space(3)
  private w_UNMIS1
  m.w_UNMIS1=space(3)
  private w_UNMIS2
  m.w_UNMIS2=space(3)
  private w_OPERAT
  m.w_OPERAT=space(1)
  private w_MOLTIP
  m.w_MOLTIP=0
  private w_UNMIS3
  m.w_UNMIS3=space(3)
  private w_OPERA3
  m.w_OPERA3=space(1)
  private w_MOLTI3
  m.w_MOLTI3=0
  private w_QTAUM3
  m.w_QTAUM3=0
  private w_QTAUM2
  m.w_QTAUM2=0
  private w_QTAUM1
  m.w_QTAUM1=0
  private w_OBTEST
  m.w_OBTEST=ctod("  /  /  ")
  private w_OLTPROVE
  m.w_OLTPROVE=space(1)
  private w_ATIPGES
  m.w_ATIPGES=space(1)
* --- WorkFile variables
  private ART_ICOL_idx
  ART_ICOL_idx=0
  private CON_TRAD_idx
  CON_TRAD_idx=0
  private CON_TRAM_idx
  CON_TRAM_idx=0
  private PAR_RIOR_idx
  PAR_RIOR_idx=0
  private KEY_ARTI_idx
  KEY_ARTI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cosetlt"
if vartype(__cosetlt_hook__)='O'
  __cosetlt_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cosetlt('+Transform(pCODART)+','+Transform(pPROVEN)+','+Transform(pQTAMOV)+','+Transform(pQTAMO1)+','+Transform(pCODFOR)+','+Transform(pDATA)+','+Transform(pUNIMIS)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cosetlt')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if cosetlt_OpenTables()
  cosetlt_Page_1()
endif
cp_CloseFuncTables()
if used('_Curs_PRDSETLT')
  use in _Curs_PRDSETLT
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cosetlt('+Transform(pCODART)+','+Transform(pPROVEN)+','+Transform(pQTAMOV)+','+Transform(pQTAMO1)+','+Transform(pCODFOR)+','+Transform(pDATA)+','+Transform(pUNIMIS)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cosetlt')
Endif
*--- Activity log
if vartype(__cosetlt_hook__)='O'
  __cosetlt_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cosetlt_Page_1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Parametri
  * --- Controlli iniziali per uscire subito ...
  m.w_RESULT = 0
  m.w_OLTPROVE = NVL(m.pPROVEN, "L")
  m.w_CONUMERO = ""
  if empty(m.pCODART) or (m.w_OLTPROVE $ "E-L" and (empty(m.pDATA) or empty(m.pCODFOR)))
    * --- Esce ...
  else
    m.w_OLTCODIC = m.pCODART
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[KEY_ARTI_idx,2],.t.,KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(w_OLTCODIC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = w_OLTCODIC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      m.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      m.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      m.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[ART_ICOL_idx,2],.t.,ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARPREZUM,ARGRUMER,ARTIPGES"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(m.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARPREZUM,ARGRUMER,ARTIPGES;
        from (i_cTable) where;
            ARCODART = m.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      m.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      m.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      m.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      m.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
      m.w_CODGRU = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
      m.w_ATIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if m.w_OLTPROVE $ "E-L" and not empty(m.pCODFOR) AND m.w_ATIPGES<>"S"
      if len(m.pCODFOR) > 15
        * --- Il contratto � concatenato dopo il codice fornitore
        m.w_CONUMERO = substr(m.pCODFOR,16,15)
        m.w_OLTCOFOR = left(m.pCODFOR,15)
      else
        m.w_CONUMERO = SPACE(15)
        m.w_OLTCOFOR = m.pCODFOR
      endif
      * --- Cerca il contratto alla data ...
      m.w_OK1 = .F.
      m.w_OK2 = .F.
      m.w_DATORD = m.pDATA
      m.w_OLTCODIC = m.pCODART
      m.w_OBTEST = i_DATSYS
      if m.w_PREZUM = "S"
        m.w_UNMIS = m.pUNIMIS
        m.w_QTAUM1 = m.pQTAMOV
      else
        m.w_QTAUM1 = m.pQTAMO1
        m.w_UNMIS = m.w_UNMIS1
        m.w_QTAUM3 = CALQTA( m.pQTAMO1 ,m.w_UNMIS3, Space(3),IIF(m.w_OPERAT="/","*","/"), m.w_MOLTIP, "", "", "", , m.w_UNMIS3, IIF(m.w_OPERA3="/","*","/"), m.w_MOLTI3)
        m.w_QTAUM2 = CALQTA( m.pQTAMO1 ,m.w_UNMIS2, m.w_UNMIS2,IIF(m.w_OPERAT="/","*","/"), m.w_MOLTIP, "", "", "", , m.w_UNMIS3, IIF(m.w_OPERA3="/","*","/"), m.w_MOLTI3)
      endif
      * --- Select from PRDSETLT
      do vq_exec with 'PRDSETLT',createobject('cp_getfuncvar'),'_Curs_PRDSETLT','',.f.,.t.
      if used('_Curs_PRDSETLT')
        select _Curs_PRDSETLT
        locate for 1=1
        do while not(eof())
        do case
          case NOT EMPTY(NVL(_Curs_PRDSETLT.COCODART, " "))
            * --- Cerca il Contratto su Articolo (Preferenziale)
            m.w_GIOAPP1 = NVL(_Curs_PRDSETLT.COGIOAPP, 0)
            m.w_OK1 = .T.
          case NOT EMPTY(NVL(_Curs_PRDSETLT.COGRUMER, " "))
            * --- Oppure Quello per Gruppo Merceologico (Alternativo all'Articolo)
            m.w_GIOAPP2 = NVL(_Curs_PRDSETLT.COGIOAPP, 0)
            m.w_OK2 = .T.
        endcase
          select _Curs_PRDSETLT
          continue
        enddo
        use
      endif
      * --- Nel caso di GESTIONE A SCORTA cerco il contratto per cui le variabili w_OK1 e w_OK2 restano a false cos� leggo sempre i parametri dell'articolo
      do case
        case m.w_OK1 = .T.
          m.w_RESULT = m.w_GIOAPP1
        case m.w_OK2 = .T.
          m.w_RESULT = m.w_GIOAPP2
        otherwise
          m.w_CONUMERO = ""
      endcase
    endif
    * --- Se non Trova Articolo sul Contratto prende il valore dai Dati Articolo
    if empty(m.w_CONUMERO)
      * --- Applica LT Articolo
      * --- Read from PAR_RIOR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[PAR_RIOR_idx,2],.t.,PAR_RIOR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PRGIOAPP,PRCOEFLT,PRLOTMED"+;
          " from "+i_cTable+" PAR_RIOR where ";
              +"PRCODART = "+cp_ToStrODBC(m.pCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PRGIOAPP,PRCOEFLT,PRLOTMED;
          from (i_cTable) where;
              PRCODART = m.pCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_LEAFIS = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
        m.w_COEFLT = NVL(cp_ToDate(_read_.PRCOEFLT),cp_NullValue(_read_.PRCOEFLT))
        m.w_LOTMED = NVL(cp_ToDate(_read_.PRLOTMED),cp_NullValue(_read_.PRLOTMED))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Calcolo LT (arrotonda sempre per eccesso all'intero)
      m.w_RESULT = cp_ROUND(m.w_LEAFIS + iif(m.w_LOTMED=0 OR m.pQTAMOV=0, 0, m.w_LEAFIS * m.w_COEFLT * ((m.pQTAMOV/m.w_LOTMED) - 1)) + .499, 0)
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function cosetlt_OpenTables()
    dimension i_cWorkTables[max(1,5)]
    i_cWorkTables[1]='ART_ICOL'
    i_cWorkTables[2]='CON_TRAD'
    i_cWorkTables[3]='CON_TRAM'
    i_cWorkTables[4]='PAR_RIOR'
    i_cWorkTables[5]='KEY_ARTI'
    return(cp_OpenFuncTables(5))
* --- END COSETLT
* --- START COSTRODL
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: costrodl                                                        *
*              Prod - gest.stringhe ODL                                        *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-04                                                      *
* Last revis.: 2011-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func costrodl
param w_AZIONE,w_TIPGES,w_OLTSTATO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private TmpC
  m.TmpC=space(100)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "costrodl"
if vartype(__costrodl_hook__)='O'
  __costrodl_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'costrodl('+Transform(w_AZIONE)+','+Transform(w_TIPGES)+','+Transform(w_OLTSTATO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'costrodl')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
costrodl_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'costrodl('+Transform(w_AZIONE)+','+Transform(w_TIPGES)+','+Transform(w_OLTSTATO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'costrodl')
Endif
*--- Activity log
if vartype(__costrodl_hook__)='O'
  __costrodl_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure costrodl_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Stringe gestione ODL
  m.TmpC = " "
  do case
    case m.w_AZIONE = "STRODL"
      do case
        case m.w_TIPGES="L"
          m.TmpC = iif(m.w_OLTSTATO="L", "ODL <LANCIATO>", "ODL <COMPLETATO>")
        case m.w_TIPGES="F"
          m.TmpC = iif(m.w_OLTSTATO="L", "ODR <LANCIATO>", "ODR <COMPLETATO>")
        case m.w_TIPGES = "Z"
          m.TmpC = iif(m.w_OLTSTATO="L", "OCL <ORDINATO>", "OCL <EVASO>")
        case m.w_TIPGES="S"
          m.TmpC = iif(m.w_OLTSTATO="L", "ODS <LANCIATO>", "ODS <COMPLETATO>")
        case m.w_TIPGES="E"
          m.TmpC = iif(m.w_OLTSTATO="L", "ODA <LANCIATO>", "ODA <COMPLETATO>")
      endcase
      * --- Traduco la stringa
      m.TmpC = ah_msgFormat(m.TmpC)
    case m.w_AZIONE = "TIPOORD"
      do case
        case m.w_TIPGES="L"
          m.TmpC = ah_msgFormat("ODL")
        case m.w_TIPGES = "Z"
          m.TmpC = ah_msgFormat("OCL")
        case m.w_TIPGES="E"
          m.TmpC = ah_msgFormat("ODA")
      endcase
  endcase
  i_retcode = 'stop'
  i_retval = m.TmpC
  return
endproc


* --- END COSTRODL
* --- START GETDIRELAB
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getdirelab                                                      *
*              Gestione path file MRP (elab.DBF)                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-08-05                                                      *
* Last revis.: 2015-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getdirelab
param pParam,pArrPath,pSerial

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_SERIALE
  m.w_SERIALE=space(10)
  private w_CurDir
  m.w_CurDir=space(200)
  private w_FileName
  m.w_FileName=space(50)
  private w_PATHMRP
  m.w_PATHMRP=space(200)
  private w_cTableMRP 
  m.w_cTableMRP =space(200)
  private w_Handle
  m.w_Handle=0
  private w_PathFound
  m.w_PathFound=.f.
* --- WorkFile variables
  private PAR_PROD_idx
  PAR_PROD_idx=0
  private SEL__MRP_idx
  SEL__MRP_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getdirelab"
if vartype(__getdirelab_hook__)='O'
  __getdirelab_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getdirelab('+Transform(pParam)+','+Transform(pArrPath)+','+Transform(pSerial)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getdirelab')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if getdirelab_OpenTables()
  getdirelab_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getdirelab('+Transform(pParam)+','+Transform(pArrPath)+','+Transform(pSerial)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getdirelab')
Endif
*--- Activity log
if vartype(__getdirelab_hook__)='O'
  __getdirelab_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getdirelab_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Chiamata da 
  *     GSMR_BGP - Elaborazione MRP
  *     GSMR_BPG - Aggiornamento Pegging 2 Livello
  *     GSMR_BEM - Stampa Elaborazione MRP
  * --- ----------------------------------------------------------------------------------------
  * --- pArrPath[ 1 ] = Ritorna Percorso completo di Nome del File 
  *     oppure solo percorso a seconda del tipo di errore
  * --- pArrPath[ 2 ] = 0 Nessun Errore
  * --- pArrPath[ 2 ] = 1 restituisce errore percorso non trovato
  * --- pArrPath[ 2 ] = 2 restituisce errore lettura scrittura
  * --- pArrPath[ 2 ] = 3 restituisce errore nell'aggiornamento del Seriale di Elaborazione
  * --- pArrPath[ 3 ] = Ritorna il Seriale di elaborazione
  * --- ----------------------------------------------------------------------------------------
  * --- Parametro Lettura 'R' - Scrittura 'W'
  * --- Array che restituisce con i Dati
  *     Percorso Oppure File
  *     Eventualmente tipo di errore
  * --- Seriale di elaborazione
  * --- Azzero array risultato
   
 pArrPath[ 1 ] = "" 
 pArrPath[ 2 ] = 0 
 pArrPath[ 3 ] = ""
  * --- Lettura percorso File
  m.w_SERIALE = m.pSerial
  do case
    case m.pParam = "W"
      * --- Lettura percorso da utilizzare per la scrittura
      * --- Read from PAR_PROD
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[PAR_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[PAR_PROD_idx,2],.t.,PAR_PROD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPATHMRP"+;
          " from "+i_cTable+" PAR_PROD where ";
              +"PPCODICE = "+cp_ToStrODBC("PP");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPATHMRP;
          from (i_cTable) where;
              PPCODICE = "PP";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_PATHMRP = NVL(cp_ToDate(_read_.PPATHMRP),cp_NullValue(_read_.PPATHMRP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(m.w_SERIALE)
        * --- Se il Seriale non � passato come parametro lo ricalcolo, altrimenti uso quello passato
        * --- Try
        getdirelab_Try_02D05550()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
           
 pArrPath[ 1 ] = m.w_PATHMRP 
 pArrPath[ 2 ] = 3 
 pArrPath[ 3 ] = m.w_SERIALE
          i_retcode = 'stop'
          i_retval = 1
          return
        endif
        * --- End
      endif
    case m.pParam = "R"
      * --- Read from SEL__MRP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[SEL__MRP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[SEL__MRP_idx,2],.t.,SEL__MRP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "GMPATHEL"+;
          " from "+i_cTable+" SEL__MRP where ";
              +"GMSERIAL = "+cp_ToStrODBC(m.w_SERIALE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          GMPATHEL;
          from (i_cTable) where;
              GMSERIAL = m.w_SERIALE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_PATHMRP = NVL(cp_ToDate(_read_.GMPATHEL),cp_NullValue(_read_.GMPATHEL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
  endcase
  * --- Percorso Corrente (punta alla exe di AHE)
  m.w_CurDir = sys(5)+sys(2003)+"\"
  * --- Nome del File di Elaborazione MRP
  m.w_FileName = "elab"+alltrim(i_codazi) + alltrim(m.w_SERIALE) + ".dbf"
  m.w_PATHMRP = nvl(m.w_PATHMRP,"")
  * --- Se il Path memorizzato � lo stesso del percorso di AHE non faccio niente
  do case
    case m.pParam = "W"
      * --- Variabile HWnd del file
      m.w_Handle = 0
      if !empty(m.w_PATHMRP)
        m.w_PathFound = True
        * --- Per evitare errori Recupero FULLPATH()
        m.w_PATHMRP = FULLPATH(alltrim(nvl(m.w_PATHMRP,"")))
        * --- Se non c'� aggiungo Slash finale
        m.w_PATHMRP = m.w_PATHMRP + iif(right(m.w_PATHMRP,1)="\","","\")
        * --- Verifico che la Directory Esista
        m.w_PathFound = DIRECTORY(m.w_PATHMRP)
        if !m.w_PathFound
          * --- In questo caso ho un errore (lo memorizzo Nell'array)
           
 pArrPath[ 1 ] = m.w_PATHMRP 
 pArrPath[ 2 ] = 1 
 pArrPath[ 3 ] = m.w_SERIALE
          i_retcode = 'stop'
          i_retval = 1
          return
        endif
        m.w_cTableMRP = m.w_PATHMRP + m.w_FileName
      else
        m.w_cTableMRP = m.w_FileName
      endif
      if file(w_cTableMRP)
        * --- Provo ad aprire il file in Modalit� Read/Write
        m.w_Handle = FOPEN(w_cTableMRP,12)
      else
        * --- Provo a Creare il file
        m.w_Handle = FCREATE(w_cTableMRP)
      endif
      if m.w_Handle < 0
        * --- In questo caso ho un errore (lo memorizzo Nell'array)
         
 pArrPath[ 1 ] = w_cTableMRP 
 pArrPath[ 2 ] = 2 
 pArrPath[ 3 ] = m.w_SERIALE
      else
        * --- Tutto OK
        FCLOSE(m.w_Handle)
        DELETE FILE (w_cTableMRP)
         
 pArrPath[ 1 ] = w_cTableMRP 
 pArrPath[ 2 ] = 0 
 pArrPath[ 3 ] = m.w_SERIALE
      endif
    case m.pParam = "R"
      if !empty(m.w_PATHMRP)
        m.w_PathFound = True
        * --- Per evitare errori Recupero FULLPATH()
        m.w_PATHMRP = FULLPATH(alltrim(nvl(m.w_PATHMRP,"")))
        * --- Se non c'� aggiungo Slash finale
        m.w_PATHMRP = m.w_PATHMRP + iif(right(m.w_PATHMRP,1)="\","","\")
        * --- Verifico che la Directory Esista
        m.w_PathFound = DIRECTORY(m.w_PATHMRP)
        if !m.w_PathFound
          * --- In questo caso ho un errore (lo memorizzo Nell'array)
           
 pArrPath[ 1 ] = m.w_PATHMRP 
 pArrPath[ 2 ] = 1 
 pArrPath[ 3 ] = m.w_SERIALE
          i_retcode = 'stop'
          i_retval = 1
          return
        endif
        m.w_cTableMRP = m.w_PATHMRP + m.w_FileName
      else
        m.w_cTableMRP = m.w_FileName
      endif
      if !file(w_cTableMRP)
        * --- In questo caso ho un errore (lo memorizzo Nell'array)
         
 pArrPath[ 1 ] = w_cTableMRP 
 pArrPath[ 2 ] = 2 
 pArrPath[ 3 ] = m.w_SERIALE
      else
        * --- Tutto OK
         
 pArrPath[ 1 ] = w_cTableMRP 
 pArrPath[ 2 ] = 0 
 pArrPath[ 3 ] = m.w_SERIALE
      endif
  endcase
  i_retcode = 'stop'
  i_retval = 1
  return
endproc
  proc getdirelab_Try_02D05550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- begin transaction
  cp_BeginTrs()
  m.w_SERIALE = space(10)
  m.w_SERIALE = cp_GetProg("SEL__MRP","PRMRP",m.w_SERIALE,i_CODAZI)
  * --- commit
  cp_EndTrs(.t.)
    return


  function getdirelab_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='PAR_PROD'
    i_cWorkTables[2]='SEL__MRP'
    return(cp_OpenFuncTables(2))
* --- END GETDIRELAB
