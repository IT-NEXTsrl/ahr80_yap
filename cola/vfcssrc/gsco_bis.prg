* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bis                                                        *
*              Import piano di produzione                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-01                                                      *
* Last revis.: 2010-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bis",oParentObject,m.pEvent)
return(i_retval)

define class tgsco_bis as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_GSDB_KSP = .NULL.
  w_ZOOM = .NULL.
  w_CHKMAST = 0
  w_SERSCA = space(15)
  w_ROWOCL = 0
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue eventi da Selezione/Spostamento Riga Master (da GSDB_KGM)
    * --- Evento: MC - Master Row Checked , MU - Master Row Checked
    * --- BQD - Before Query Detail , AQD - After Query Detail
    this.w_GSDB_KSP = this.oParentObject
    this.w_CHKMAST = -1
    ND = this.w_GSDB_KSP.w_ZoomDett.cCursor
    NM = this.w_GSDB_KSP.w_ZoomMast.cCursor
    do case
      case this.pEvent = "MRC"
        * --- Eseguito il Check sul Master
        * --- Inserisce il Flag Selezionato sulle righe del Detail
        UPDATE (ND) SET XCHK = 1 && , SPEDIRE=MAX(OLQTAMOV-OLQTAPRE,0)
        this.w_CHKMAST = 1
        this.w_GSDB_KSP.NotifyEvent("ZOOMDETT before query")     
      case this.pEvent = "UPS"
        this.w_GSDB_KSP.NotifyEvent("ZOOMDETT before query")     
      case this.pEvent = "MRU"
        * --- Eseguito l' Uncheck sul Master
        * --- Toglie il Flag Selezionato sulle righe del Detail
        UPDATE (ND) SET XCHK = 2
        this.w_CHKMAST = 0
        this.w_GSDB_KSP.NotifyEvent("ZOOMDETT before query")     
      case this.pEvent = "DBQ"
        * --- Evento Before Query sul Dettaglio
        * --- Salva in RigheDoc le Variazioni Riportate sul Dettaglio
        if USED(ND) AND USED("GeneApp")
          this.w_SERSCA = this.w_GSDB_KSP.w_ZoomDett.GetVar("SCSERIAL")
          Select * from GeneApp WHERE SCSERIAL<>this.w_SERSCA Union ; 
 Select xchk, SCSERIAL, CPROWORD, CPROWNUM, SCQTARES, SCRESODL from (ND) ; 
 where XCHK=1 into cursor GeneApp 
 SELECT (ND)
        endif
      case this.pEvent = "DRC"
        this.w_ZOOM = this.w_GSDB_KSP.w_ZoomDett
        this.w_ZOOM.grd.Refresh()     
        * --- Eseguito il Check sul Dettaglio
        * --- Salva in RigheDoc le Variazioni Riportate sul Dettaglio
        if USED(ND)
          * --- Aggiorna quantita' su cursore di dettaglio
          SELECT (ND)
        endif
      case this.pEvent = "DRU"
        * --- Eseguito il UnCheck sul Dettaglio
        * --- Salva in RigheDoc le Variazioni Riportate sul Dettaglio
        if USED(ND)
          * --- Aggiorna quantita' su cursore di dettaglio
          SELECT (ND)
        endif
      case this.pEvent = "DAQ"
        * --- Evento After Query sul Dettaglio
        * --- Riporta sul Dettaglio le Variazioni precedentemente definite e salvate in GeneApp
        if USED(ND)
          this.w_CHKMAST = this.w_GSDB_KSP.w_ZoomMast.GetVar("XCHK")
          if this.w_CHKMAST=1
            UPDATE (ND) SET XCHK = 1
            * --- Toglie il Flag alle Eventuali Righe da non Importare
            if USED("GeneApp")
              SELECT (ND)
              SCAN FOR NOT EMPTY(NVL(SCSERIAL,""))
              SELECT * FROM GeneApp WHERE GeneApp.SCSERIAL = &ND..SCSERIAL ; 
 AND GeneApp.CPROWNUM = &ND..CPROWNUM INTO CURSOR APPCUR
              SELECT (ND)
              if RECCOUNT("APPCUR")>0
                REPLACE XCHK with APPCUR.XCHK &&, SCQTARES with APPCUR.SCQTARES
              endif
              ENDSCAN
            endif
          else
            UPDATE (ND) SET XCHK = 2
          endif
        endif
      case this.pEvent = "SCH"
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if USED(ND)
          if this.w_GSDB_KSP.w_ZoomMast.GetVar("XCHK") = 1
            * --- Selezionato il Documento Master
            if this.oParentObject.w_SELEZI = "S"
              * --- Seleziona Tutto
              UPDATE (ND) SET XCHK = 1 && , SPEDIRE=max(OLQTAMOV-OLQTAPRE,0)
            else
              * --- deseleziona Tutto
              UPDATE (ND) SET XCHK = 0
            endif
          else
            ah_msg("Documento da Generare non Selezionato")
            this.oParentObject.w_SELEZI = IIF(this.oParentObject.w_SELEZI = "S", "D", "S")
          endif
        endif
    endcase
    * --- Setta Proprieta' Campi del Cursore
    if this.w_CHKMAST <> -1
      this.w_ZOOM = this.w_GSDB_KSP.w_ZoomDett
      SELECT (ND)
      FOR I=1 TO this.w_Zoom.grd.ColumnCount
      NC = ALLTRIM(STR(I))
      if UPPER(this.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
        this.w_ZOOM.grd.Column&NC..DynamicForeColor = "IIF(XCHK=1, RGB(0,0,255), RGB(0,0,0))"
      else
        this.w_ZOOM.grd.Column&NC..Width = 40
        this.w_ZOOM.grd.Column&NC..Header1.caption = "Evasa"
        this.w_ZOOM.grd.Column&NC..chk.caption = ""
        this.w_ZOOM.grd.Column&NC..Enabled = (this.w_CHKMAST=1)
      endif
      ENDFOR
    endif
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
