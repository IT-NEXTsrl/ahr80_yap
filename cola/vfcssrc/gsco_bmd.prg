* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bmd                                                        *
*              Eventi da detail dichiarazioni di produzione                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_78]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-25                                                      *
* Last revis.: 2018-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,TipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bmd",oParentObject,m.TipOpe)
return(i_retval)

define class tgsco_bmd as StdBatch
  * --- Local variables
  TipOpe = space(15)
  GSCO_ADP = .NULL.
  GSCO_MCO = .NULL.
  GSCO_MMP = .NULL.
  w_DPCOMMAT = space(1)
  w_EDCOMMAT = .f.
  w_MATENABL = .f.
  w_RIGCUR = 0
  w_FLLOTTI = space(1)
  w_DESARTI = space(40)
  w_MATGES = space(1)
  w_DPCODODL = space(15)
  NUMREC = 0
  w_QTAEVA = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione di GSCO_MMP
    * --- Locals
    * --- Callers
    * --- Riferimenti agli oggetti correlati
    *     ------------------------------------------------
    * --- Padre-Dettaglio Componenti Dichiarazioni
    this.GSCO_MMP = this.oParentObject
    * --- Nonno - Testata Dichiarazioni
    this.GSCO_ADP = this.GSCO_MMP.oparentobject
    * --- Figlio del Nonno - Componenti a Matricole
    this.GSCO_MCO = this.GSCO_ADP.GSCO_MCO
    * --- ------------------------------------------------
    this.w_DPCOMMAT = this.GSCO_ADP.w_DPCOMMAT
    this.w_EDCOMMAT = this.GSCO_ADP.w_EDCOMMAT
    this.w_MATENABL = this.GSCO_ADP.w_MATENABL
    this.w_DPCODODL = this.GSCO_ADP.w_DPCODODL
    do case
      case this.TipOpe="DELMATR"
        if this.oParentObject.w_CHANGE
          if (not empty(nvl(this.oParentObject.w_MPCODICE," ")) and not empty(nvl(this.oParentObject.w_MPUNIMIS," ")) and not empty(nvl(this.oParentObject.w_MPCODMAG," ")); 
 and this.oParentObject.w_MPCOEIMP>0 and this.oParentObject.w_MPQTAMOV>0)
            select (this.GSCO_MMP.cTrsname)
            this.w_RIGCUR = cprownum
            this.w_MATGES = t_MATCOM
            * --- Aggiorna le quantit� delle matricole
            if g_MATR="S" and this.w_DPCOMMAT="S" and not this.w_EDCOMMAT and this.w_MATGES="S"
              * --- Cursore dei Componenti da scaricare
              nc = this.GSCO_MCO.cnt.cTrsname
              if used(nc)
                if RecCount(nc)=0
                  * --- Carica il dettaglio matricole componenti da scaricare
                  this.GSCO_MCO.LinkPCClick()     
                  this.GSCO_MCO.ecpsave()     
                endif
                if RecCount(nc)>0
                  Select (nc)
                  if this.TipOpe=="UPDMATR1"
                    * --- Ho eliminato un record
                    delete from (nc) where mtrowodl=this.w_RIGCUR
                    goto this.GSCO_MCO.cnt.nCurrentRow
                    this.GSCO_MCO.cnt.WorkFromTrs()     
                    this.GSCO_MCO.cnt.bUpdated = True
                  else
                    * --- Riga nuova oppure modificata
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARFLLOTT,ARDESART"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MPCODART);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARFLLOTT,ARDESART;
                        from (i_cTable) where;
                            ARCODART = this.oParentObject.w_MPCODART;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_FLLOTTI = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
                      this.w_DESARTI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    Select (nc)
                    go top
                    locate for mtrowodl=this.w_RIGCUR
                    if found()
                      * --- Riga Aggiornata
                      replace t_MTKEYSAL with left(this.oParentObject.w_MPCODART+space(40),40), t_mtcodric with this.oParentObject.w_MPCODICE, t_codart with this.oParentObject.w_MPCODART,; 
 t_desart with this.w_DESARTI, t_fllott with this.w_FLLOTTI, t_MTMAGSCA with this.oParentObject.w_MPCODMAG, t_mtunimis with this.oParentObject.w_MPUNIMIS, ; 
 t_mtcoeimp with this.oParentObject.w_MPCOEIMP,t_MTQTAMOV with this.oParentObject.w_MPCOEIMP * (this.GSCO_ADP.w_DPQTAPR1+this.GSCO_ADP.w_DPQTASC1),; 
 mtkeysal with left(this.oParentObject.w_MPCODART+space(40),40), i_srv with iif(i_srv="A",i_srv,"U" )
                      goto this.GSCO_MCO.cnt.nCurrentRow
                      this.GSCO_MCO.cnt.WorkFromTrs()     
                      this.GSCO_MCO.cnt.bUpdated = True
                    else
                      * --- Riga nuova
                      Select (nc) 
 go bottom
                      if not empty(t_MTKEYSAL) and t_MTROWODL>0
                        * --- L'utente ha gi� forzato la initrow quindi la riga bianca esiste gi�, non devo ricrearla
                        append blank
                      endif
                      replace t_MTKEYSAL with left(this.oParentObject.w_MPCODART+space(40),40), t_mtcodric with this.oParentObject.w_MPCODICE, t_codart with this.oParentObject.w_MPCODART,; 
 t_desart with this.w_DESARTI, t_fllott with this.w_FLLOTTI, t_MTMAGSCA with this.oParentObject.w_MPCODMAG, t_mtunimis with this.oParentObject.w_MPUNIMIS, ; 
 t_mtcoeimp with this.oParentObject.w_MPCOEIMP,t_MTQTAMOV with this.oParentObject.w_MPCOEIMP * (this.GSCO_ADP.w_DPQTAPR1+this.GSCO_ADP.w_DPQTASC1),; 
 mtkeysal with left(this.oParentObject.w_MPCODART+space(40),40), t_MTROWODL with this.w_RIGCUR, MTROWODL with this.w_RIGCUR; 
 t_CPROWORD with this.oParentObject.w_CPROWORD, cpccchk with sys(2015), i_srv with "A" 
                      goto this.GSCO_MCO.cnt.nCurrentRow
                      this.GSCO_MCO.cnt.WorkFromTrs()     
                      this.GSCO_MCO.cnt.bUpdated = True
                    endif
                  endif
                endif
              endif
            else
              if this.w_DPCOMMAT<>"S" and this.w_MATGES="S"
                * --- Ho aggiunto un componente a matricole su una gestione che non ne gestiva, devo 
                *     abilitare il pulsante.
                this.GSCO_ADP.w_DPCOMMAT = "S"
                this.GSCO_ADP.w_MATENABL = .T.
                this.GSCO_ADP.opgfrm.page1.opag.olinkpc_1_94.visible=.T.
                this.GSCO_ADP.opgfrm.page1.opag.olinkpc_1_94.enabled=.T.
                * --- Carica il dettaglio matricole componenti da scaricare
                nc = this.GSCO_MCO.cnt.cTrsname
                if used(nc)
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARFLLOTT,ARDESART"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MPCODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARFLLOTT,ARDESART;
                      from (i_cTable) where;
                          ARCODART = this.oParentObject.w_MPCODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_FLLOTTI = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
                    this.w_DESARTI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.GSCO_MCO.LinkPCClick()     
                  * --- Riga nuova
                  Select (nc)
                  count for not deleted() to this.NUMREC
                  Select (nc) 
 go bottom
                  if (this.NUMREC=0 or (this.NUMREC=1 and empty(t_MTKEYSAL) and t_MTROWODL=0 ))
                    if this.NUMREC=0
                      * --- In caso contrario l'utente ha gi� forzato la initrow quindi la riga bianca esiste gi�, non devo ricrearla
                      append blank
                    endif
                    replace t_MTKEYSAL with left(this.oParentObject.w_MPCODART+space(40),40), t_mtcodric with this.oParentObject.w_MPCODICE, t_codart with this.oParentObject.w_MPCODART,; 
 t_desart with this.w_DESARTI, t_fllott with this.w_FLLOTTI, t_MTMAGSCA with this.oParentObject.w_MPCODMAG, t_mtunimis with this.oParentObject.w_MPUNIMIS, ; 
 t_mtcoeimp with this.oParentObject.w_MPCOEIMP,t_MTQTAMOV with this.oParentObject.w_MPCOEIMP * (this.GSCO_ADP.w_DPQTAPR1+this.GSCO_ADP.w_DPQTASC1),; 
 mtkeysal with left(this.oParentObject.w_MPCODART+space(40),40), t_MTROWODL with this.w_RIGCUR, MTROWODL with this.w_RIGCUR; 
 t_CPROWORD with this.oParentObject.w_CPROWORD, cpccchk with sys(2015), i_srv with "A" 
                    goto this.GSCO_MCO.cnt.nCurrentRow
                    this.GSCO_MCO.cnt.WorkFromTrs()     
                    this.GSCO_MCO.cnt.bUpdated = True
                  endif
                  this.GSCO_MCO.ecpsave()     
                endif
              endif
            endif
          endif
          this.oParentObject.w_CHANGE = False
          this.bUpdateParentObject = .F.
        endif
      case this.TipOpe="DELMATR"
        if g_MATR="S" and this.w_DPCOMMAT="S"
          * --- Se cancello una riga devo riverificare la situazione dei componenti, 
          *     potrei dover disabilitare il bottone per lo scarico delle matricole.
          this.GSCO_ADP.w_DPCOMMAT = "N"
          select (this.GSCO_MMP.cTrsname)
          this.w_RIGCUR = recno()
          go top 
 locate for t_MATCOM="S"
          if found()
            this.GSCO_ADP.w_DPCOMMAT = "S"
          endif
          if this.GSCO_ADP.w_DPCOMMAT="N"
            this.GSCO_ADP.opgfrm.page1.opag.olinkpc_1_94.visible=.F.
          endif
          go this.w_RIGCUR
        endif
      case this.TipOpe="CHKQTA"
        this.w_QTAEVA = CALQTAADV(this.oParentObject.w_MPQTAEVA,this.oParentObject.w_MPUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3,,,This, "MPQTAEVA")
        if this.w_QTAEVA > this.oParentObject.w_QTARES
          ah_ErrorMsg("Quantit� specificata superiore a quella residua",,"")
          this.oParentObject.w_MPQTAEVA = this.oparentobject.o_MPQTAEVA
        endif
    endcase
  endproc


  proc Init(oParentObject,TipOpe)
    this.TipOpe=TipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="TipOpe"
endproc
