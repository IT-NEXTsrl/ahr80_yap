* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_btc                                                        *
*              Gestione tracciabilità commessa                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-18                                                      *
* Last revis.: 2014-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_btc",oParentObject,m.w_TIPO)
return(i_retval)

define class tgsmr_btc as StdBatch
  * --- Local variables
  w_TIPO = space(2)
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione GSMR_KTC - Tracciabilità commessa
    do case
      case this.w_TIPO="MM"
        * --- Visualizzazione movimenti di magazzino
        this.w_OBJECT = GSMA_SZM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.oParentObject.w_MVCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        DO GSMA_BVM WITH this.w_OBJECT
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.w_TIPO="PE"
        * --- Visualizzazione pegging di secondo livello
        this.w_OBJECT = GSMR_KPE()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_DBCODINI = this.oParentObject.w_CODICE
        this.w_PROG = this.w_OBJECT.GetcTRL("w_DBCODINI")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_OBJECT.w_DBCODFIN = this.oParentObject.w_CODICE
        this.w_PROG = this.w_OBJECT.GetcTRL("w_DBCODFIN")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        do case
          case NOT EMPTY(this.oParentObject.w_MVSERIAL)
            * --- Abilito filtro commessa magazzino
            this.w_OBJECT.w_CODCOM = this.oParentObject.w_CDCOMM
            this.w_PROG = this.w_OBJECT.GetcTRL("w_CODCOM")
            this.w_PROG.bUpd = .t.
            this.w_OBJECT.SetControlsValue()     
            this.w_PROG.Valid()     
          case NOT EMPTY(this.oParentObject.w_OLCODODL)
            * --- Abilito filtro commessa ordine di lavorazione
            this.w_OBJECT.w_COMODL = this.oParentObject.w_CDCOMM
            this.w_PROG = this.w_OBJECT.GetcTRL("w_COMODL")
            this.w_PROG.bUpd = .t.
            this.w_OBJECT.SetControlsValue()     
            this.w_PROG.Valid()     
          otherwise
            * --- Nessun filtro commessa (Riga saldo)
        endcase
        DO GSMR_BPE WITH this.w_OBJECT,"Tree-PadreFig"
        this.w_PROG = .null.
        this.w_OBJECT = .null.
    endcase
  endproc


  proc Init(oParentObject,w_TIPO)
    this.w_TIPO=w_TIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPO"
endproc
