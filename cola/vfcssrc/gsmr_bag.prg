* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bag                                                        *
*              Genera movimento di magazzino per commessa                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-06                                                      *
* Last revis.: 2016-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bag",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_bag as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_MMCODUTE = 0
  w_MMDATREG = ctod("  /  /  ")
  w_MMCODESE = space(4)
  w_MMSERIAL = space(10)
  w_MMNUMREG = 0
  w_MMVALNAZ = space(3)
  w_MMTCOMAG = space(5)
  w_MMTCAMAG = space(5)
  w_MMNUMDOC = 0
  w_MMDATDOC = ctod("  /  /  ")
  w_MMDESSUP = space(40)
  w_MMCAOVAL = 0
  w_MMTCOCEN = space(15)
  w_MMTCOMME = space(15)
  w_MMTCOATT = space(15)
  w_MMTCOLIS = space(5)
  w_MMTCOMAT = space(5)
  w_MMALFDOC = space(10)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MMNUMRIF = 0
  w_MMCODVAL = space(3)
  w_MMCAUCOL = space(5)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMFLELGM = space(1)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLCLFR = space(1)
  w_MMCAUMAG = space(5)
  w_MMCODMAT = space(1)
  w_MMCODLIS = space(1)
  w_MMCODICE = space(41)
  w_MMCODART = space(20)
  w_MMCODVAR = space(20)
  w_MMCODMAG = space(5)
  w_MMKEYSAL = space(20)
  w_MMUNIMIS = space(3)
  w_MMQTAMOV = 0
  w_MMQTAUM1 = 0
  w_MMTIPATT = space(1)
  w_MMCODCOM = space(15)
  w_MMCODATT = space(15)
  w_MMIMPTOT = 0
  w_MMVALMAG = 0
  w_MMCODCOS = space(5)
  w_MMFLLOTT = space(1)
  w_MMFLOMAG = space(1)
  w_MMIMPCOM = 0
  w_MMFLORCO = space(1)
  w_MMFLCOCO = space(1)
  PunPAD = .NULL.
  w_DECTOT = 0
  w_PPCENCOS = space(15)
  w_TIPVOC = space(1)
  w_CMFLCOMM = space(1)
  w_ARGESMAT = space(1)
  w_KEYLOTUB = space(40)
  w_SEGNO = space(1)
  w_DESART = space(40)
  w_ARCODCAA = space(0)
  w_FLLOTT = space(1)
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_MAGPRE = space(5)
  w_ARFLCOMM = space(1)
  w_CODVOC = space(15)
  w_CODVOR = space(15)
  w_FLUBIC = space(1)
  w_FLLOTT1 = space(1)
  w_COCODVAL = space(3)
  w_CODECTOT = 0
  w_DATA1 = ctod("  /  /  ")
  w_CARSCA = space(1)
  w_MAGCAR = space(5)
  w_MAGSCA = space(5)
  w_MGUBICAR = space(1)
  w_MGUBISCA = space(1)
  w_CPCAR = 0
  w_CPSCA = 0
  w_COMMAPPO = space(15)
  w_COMMDEFA = space(15)
  * --- WorkFile variables
  VALUTE_idx=0
  PAR_PROD_idx=0
  CAM_AGAZ_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  CAN_TIER_idx=0
  MA_COSTI_idx=0
  MVM_MAST_idx=0
  MVM_DETT_idx=0
  CAANARTI_idx=0
  MAGAZZIN_idx=0
  SALDIART_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera movimento di magazzino di trasferimento (da GSMR_KAG)
    * --- Parametro operazione da eseguire
    * --- Puntatore al padre
    this.PunPAD = this.oParentObject
    * --- Creo testata movimento di magazzino di trasferimento
    * --- Dati Testata
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCENCOS"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCENCOS;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    this.w_MMCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    this.w_MMDATREG = i_datsys
    this.w_MMCODESE = CALCESER(this.w_MMDATREG, g_CODESE)
    this.w_MMSERIAL = cp_GetProg("MVM_MAST","SEMVM",this.w_MMSERIAL,i_CODAZI)
    this.w_MMNUMREG = cp_GetProg("MVM_MAST","PRMVM",this.w_MMNUMREG,i_CODAZI,this.w_MMCODESE,this.w_MMCODUTE)
    this.w_MMVALNAZ = g_PERVAL
    * --- Legge decimali totali
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MMVALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_MMVALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Altri Dati Testata
    this.w_MMTCOMAG = this.oParentObject.w_NEWMAG
    this.w_MMTCAMAG = this.oParentObject.w_CAUTRA
    this.w_MMNUMDOC = 0
    this.w_MMDESSUP = left(AH_MsgFormat("Creato dalla manutenzione saldi commessa"),40)
    this.w_MMCODVAL = g_PERVAL
    this.w_MMCAOVAL = GETCAM(this.w_MMCODVAL, IIF(EMPTY(this.w_MMDATDOC), this.w_MMDATREG, this.w_MMDATDOC), 7)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARCODCEN"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODRIC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARCODCEN;
        from (i_cTable) where;
            ARCODART = this.oParentObject.w_CODRIC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MMTCOCEN = NVL(cp_ToDate(_read_.ARCODCEN),cp_NullValue(_read_.ARCODCEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(nvl(this.w_MMTCOCEN,""))
      this.w_MMTCOCEN = this.w_PPCENCOS
    endif
    this.w_MMTCOMME = this.oParentObject.w_NEWCOM
    this.w_MMTCOATT = space(15)
    this.w_MMTCOLIS = " "
    this.w_MMTCOMAT = " "
    this.w_MMALFDOC = space(10)
    * --- Inserisce testata movimento
    * --- Insert into MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",MMCODESE"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMTCOLIS"+",MMNUMDOC"+",MMALFDOC"+",MMDATDOC"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",MMSCOCL1"+",MMSCOCL2"+",MMSCOPAG"+",MMFLGIOM"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCOLIS),'MVM_MAST','MMTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMDOC),'MVM_MAST','MMNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMALFDOC),'MVM_MAST','MMALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATDOC),'MVM_MAST','MMDATDOC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(space(15)),'MVM_MAST','MMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',this.w_MMCODESE,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMTCOLIS',this.w_MMTCOLIS,'MMNUMDOC',this.w_MMNUMDOC,'MMALFDOC',this.w_MMALFDOC,'MMDATDOC',this.w_MMDATDOC,'MMFLCLFR',"N")
      insert into (i_cTable) (MMSERIAL,MMCODESE,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMTCOLIS,MMNUMDOC,MMALFDOC,MMDATDOC,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,MMSCOCL1,MMSCOCL2,MMSCOPAG,MMFLGIOM,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
         values (;
           this.w_MMSERIAL;
           ,this.w_MMCODESE;
           ,this.w_MMVALNAZ;
           ,this.w_MMCODUTE;
           ,this.w_MMNUMREG;
           ,this.w_MMDATREG;
           ,this.w_MMTCAMAG;
           ,this.w_MMTCOLIS;
           ,this.w_MMNUMDOC;
           ,this.w_MMALFDOC;
           ,this.w_MMDATDOC;
           ,"N";
           ," ";
           ,space(15);
           ,this.w_MMDESSUP;
           ,this.w_MMCODVAL;
           ,this.w_MMCAOVAL;
           ,0;
           ,0;
           ,0;
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Variabili per righe
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    this.w_MMNUMRIF = -10
    * --- Dati causali prima riga
    this.w_MMCAUCOL = " "
    this.w_MMFLIMPE = " "
    this.w_MMFLRISE = " "
    this.w_MMFLELGM = " "
    this.w_MMFLCASC = " "
    this.w_TIPVOC = " "
    this.w_MMFLORDI = " "
    this.w_MMFLCLFR = "N"
    this.w_CMFLCOMM = " "
    this.w_MMCAUMAG = this.oParentObject.w_CAUSCA
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMCAUCOL,CMFLCLFR,CMFLCOMM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMCAUCOL,CMFLCLFR,CMFLCOMM;
        from (i_cTable) where;
            CMCODICE = this.w_MMCAUMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
      this.w_MMFLCLFR = NVL(cp_ToDate(_read_.CMFLCLFR),cp_NullValue(_read_.CMFLCLFR))
      this.w_CMFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Dati specifici articolo
    this.w_MMCODMAT = " "
    this.w_MMCODLIS = " "
    this.w_MMCODICE = this.oParentObject.w_CODRIC
    this.w_MMCODART = this.oParentObject.w_CODART
    this.w_MMCODVAR = this.oParentObject.w_CODVAR
    this.w_MMCODMAG = this.oParentObject.w_CODMAG
    this.w_MMKEYSAL = this.w_MMCODART
    this.w_MMUNIMIS = this.oParentObject.w_UNIMIS
    this.w_MMQTAMOV = this.oParentObject.w_QTANUO
    this.w_MMQTAUM1 = this.oParentObject.w_QTANUO
    this.w_MMTIPATT = "A"
    this.w_MMCODCOM = this.oParentObject.w_CODCOM
    this.w_MMCODATT = space(15)
    this.w_MMIMPTOT = 0
    this.w_MMVALMAG = this.w_MMIMPTOT
    this.w_CARSCA = "S"
    this.oParentObject.w_OK = .t.
    * --- Scrive dettaglio
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_OK
      * --- Possono essere diverse dallo scarico solo causale, magazzino e commessa, le ricalcolo
      this.w_MMCAUMAG = this.oParentObject.w_CAUTRA
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMCAUCOL,CMFLCLFR,CMFLCOMM"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMCAUCOL,CMFLCLFR,CMFLCOMM;
          from (i_cTable) where;
              CMCODICE = this.w_MMCAUMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
        this.w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
        this.w_MMFLCLFR = NVL(cp_ToDate(_read_.CMFLCLFR),cp_NullValue(_read_.CMFLCLFR))
        this.w_CMFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MMCODMAG = this.oParentObject.w_NEWMAG
      this.w_MMCODCOM = this.oParentObject.w_NEWCOM
      this.w_MMQTAMOV = this.oParentObject.w_QTANUO
      this.w_MMQTAUM1 = this.oParentObject.w_QTANUO
      this.w_CARSCA = "C"
      * --- Scrive dettaglio
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if g_MADV="S"
      if (g_PERLOT="S" or g_PERUBI="S")
        * --- Select from MVM_DETT
        i_nConn=i_TableProp[this.MVM_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM, MMFLCASC  from "+i_cTable+" MVM_DETT ";
              +" where MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL)+"";
               ,"_Curs_MVM_DETT")
        else
          select CPROWNUM, MMFLCASC from (i_cTable);
           where MMSERIAL = this.w_MMSERIAL;
            into cursor _Curs_MVM_DETT
        endif
        if used('_Curs_MVM_DETT')
          select _Curs_MVM_DETT
          locate for 1=1
          do while not(eof())
          this.w_CPROWNUM = _Curs_MVM_DETT.CPROWNUM
          this.w_MMFLCASC = _Curs_MVM_DETT.MMFLCASC
          this.w_MMFLCASC = IIF(this.w_MMFLCASC="+", "-", "+")
          GSMD_BRL (this, this.w_MMSERIAL , "M" , "+", this.w_CPROWNUM ,.T. )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_MVM_DETT
            continue
          enddo
          use
        endif
      endif
    endif
    ah_ErrorMsg("Aggiornamento completato",64)
    this.PUNPAD.ecpquit()     
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce Riga su MVM_DETT
    * --- Progressivo di riga
    this.w_CPROWNUM = this.w_CPROWNUM+1
    this.w_CPROWORD = iif(this.w_CARSCA="C",10,20)
    * --- Legge dati articolo
    this.w_ARCODCAA = " "
    this.w_FLLOTT = " "
    this.w_UM1 = " "
    this.w_UM2 = " "
    this.w_OPERAT = " "
    this.w_MOLTIP = 0
    this.w_MAGPRE = " "
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLLOTT,ARMAGPRE,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGESMAT,ARSALCOM,ARDESART"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLLOTT,ARMAGPRE,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGESMAT,ARSALCOM,ARDESART;
        from (i_cTable) where;
            ARCODART = this.w_MMCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
      this.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
      this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_ARGESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
      this.w_ARFLCOMM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ARGESMAT = iif(g_MATR="S" and g_DATMAT<=i_datsys, this.w_ARGESMAT, "N")
    * --- Legge dati codice di ricerca
    this.w_UM3 = space(3)
    this.w_MOLTI3 = 0
    this.w_OPERA3 = " "
    * --- Determina Qta UM1
    if this.w_MMQTAUM1=0
      if this.w_UM1=this.w_MMUNIMIS
        this.w_MMQTAUM1 = this.w_MMQTAMOV
      else
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_MMCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAUNIMIS,CAOPERAT,CAMOLTIP;
            from (i_cTable) where;
                CACODICE = this.w_MMCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Verifica se w_UM3 � lungo 3 caratteri (per evitare errore su CALMMLIS)
        this.w_UM2 = padr(this.w_UM2,3)
        this.w_UM3 = padr(this.w_UM3,3)
        this.w_MMQTAUM1 = CALMMLIS(this.w_MMQTAMOV, this.w_UM1+this.w_UM2+this.w_UM3+this.w_MMUNIMIS+this.w_OPERAT+this.w_OPERA3+" "+"Q",this.w_MOLTIP,this.w_MOLTI3, 0)
      endif
    endif
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGFLUBIC"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGFLUBIC;
        from (i_cTable) where;
            MGCODMAG = this.w_MMCODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Altri Dati
    this.w_FLLOTT1 = IIF(this.w_FLLOTT="S" OR this.w_FLUBIC="S" and g_PERUBI="S", IIF(this.w_MMFLRISE="+", "-", this.w_MMFLCASC)," ")
    this.w_MMFLLOTT = IIF(g_PERLOT="S" OR g_PERUBI="S", this.w_FLLOTT1, " ")
    this.w_MMFLOMAG = "X"
    * --- Importo aggiornamento saldi attivita'
    this.w_MMIMPCOM = 0
    this.w_MMFLORCO = " "
    this.w_MMFLCOCO = " "
    if g_COMM="S" and not empty(this.w_MMCODATT)
      this.w_MMIMPCOM = this.w_MMVALMAG
      this.w_COCODVAL = " "
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNCODVAL"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_MMCODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNCODVAL;
          from (i_cTable) where;
              CNCODCAN = this.w_MMCODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_COCODVAL=g_PERVAL
        this.w_CODECTOT = g_PERPVL
      else
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_COCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DATA1 = IIF(EMPTY(this.w_MMDATDOC), this.w_MMDATREG, this.w_MMDATDOC)
        this.w_MMIMPCOM = VAL2CAM(this.w_MMVALMAG,this.w_MMCODVAL,this.w_COCODVAL,this.w_MMCAOVAL,this.w_DATA1,this.w_CODECTOT)
      endif
      * --- Aggiorna saldi attivita'
      this.w_MMFLORCO = iif(this.w_CMFLCOMM="I","+",IIF(this.w_CMFLCOMM="D","-"," "))
      this.w_MMFLCOCO = iif(this.w_CMFLCOMM="C","+",IIF(this.w_CMFLCOMM="S","-"," "))
      * --- Try
      local bErr_02B5A740
      bErr_02B5A740=bTrsErr
      this.Try_02B5A740()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02B5A740
      * --- End
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MMFLCOCO,'CSCONSUN','this.w_MMIMPCOM',this.w_MMIMPCOM,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMFLORCO,'CSORDIN','this.w_MMIMPCOM',this.w_MMIMPCOM,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
        +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
            +i_ccchkf ;
        +" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.w_MMCODCOM);
            +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MMTIPATT);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_MMCODATT);
            +" and CSCODCOS = "+cp_ToStrODBC(this.w_MMCODCOS);
               )
      else
        update (i_cTable) set;
            CSCONSUN = &i_cOp1.;
            ,CSORDIN = &i_cOp2.;
            &i_ccchkf. ;
         where;
            CSCODCOM = this.w_MMCODCOM;
            and CSTIPSTR = this.w_MMTIPATT;
            and CSCODMAT = this.w_MMCODATT;
            and CSCODCOS = this.w_MMCODCOS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_MMCODCOS = "     "
    endif
    * --- Inserisce Riga Dettaglio
    * --- Insert into MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",CPROWNUM"+",MMNUMRIF"+",CPROWORD"+",MMCAUMAG"+",MMCAUCOL"+",MMCODMAG"+",MMCODMAT"+",MMCODLIS"+",MMCODICE"+",MMCODART"+",MMUNIMIS"+",MMQTAMOV"+",MMQTAUM1"+",MMPREZZO"+",MMSCONT1"+",MMSCONT2"+",MMSCONT3"+",MMSCONT4"+",MMVALMAG"+",MMIMPNAZ"+",MMKEYSAL"+",MMFLELGM"+",MMFLCASC"+",MMFLORDI"+",MMFLIMPE"+",MMFLRISE"+",MMFLOMAG"+",MMFLLOTT"+",MMF2LOTT"+",MMF2RISE"+",MMF2CASC"+",MMF2ORDI"+",MMF2IMPE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_DETT','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MVM_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMRIF),'MVM_DETT','MMNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MVM_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUCOL),'MVM_DETT','MMCAUCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'MVM_DETT','MMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAT),'MVM_DETT','MMCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODLIS),'MVM_DETT','MMCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'MVM_DETT','MMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'MVM_DETT','MMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALMAG),'MVM_DETT','MMVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALMAG),'MVM_DETT','MMIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLELGM),'MVM_DETT','MMFLELGM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLCASC),'MVM_DETT','MMFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLORDI),'MVM_DETT','MMFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLRISE),'MVM_DETT','MMFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLOMAG),'MVM_DETT','MMFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2RISE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2ORDI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2IMPE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'CPROWNUM',this.w_CPROWNUM,'MMNUMRIF',this.w_MMNUMRIF,'CPROWORD',this.w_CPROWORD,'MMCAUMAG',this.w_MMCAUMAG,'MMCAUCOL',this.w_MMCAUCOL,'MMCODMAG',this.w_MMCODMAG,'MMCODMAT',this.w_MMCODMAT,'MMCODLIS',this.w_MMCODLIS,'MMCODICE',this.w_MMCODICE,'MMCODART',this.w_MMCODART,'MMUNIMIS',this.w_MMUNIMIS)
      insert into (i_cTable) (MMSERIAL,CPROWNUM,MMNUMRIF,CPROWORD,MMCAUMAG,MMCAUCOL,MMCODMAG,MMCODMAT,MMCODLIS,MMCODICE,MMCODART,MMUNIMIS,MMQTAMOV,MMQTAUM1,MMPREZZO,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4,MMVALMAG,MMIMPNAZ,MMKEYSAL,MMFLELGM,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMFLOMAG,MMFLLOTT,MMF2LOTT,MMF2RISE,MMF2CASC,MMF2ORDI,MMF2IMPE &i_ccchkf. );
         values (;
           this.w_MMSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MMNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MMCAUMAG;
           ,this.w_MMCAUCOL;
           ,this.w_MMCODMAG;
           ,this.w_MMCODMAT;
           ,this.w_MMCODLIS;
           ,this.w_MMCODICE;
           ,this.w_MMCODART;
           ,this.w_MMUNIMIS;
           ,this.w_MMQTAMOV;
           ,this.w_MMQTAUM1;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,this.w_MMVALMAG;
           ,this.w_MMVALMAG;
           ,this.w_MMKEYSAL;
           ,this.w_MMFLELGM;
           ,this.w_MMFLCASC;
           ,this.w_MMFLORDI;
           ,this.w_MMFLIMPE;
           ,this.w_MMFLRISE;
           ,this.w_MMFLOMAG;
           ,this.w_MMFLLOTT;
           ," ";
           ," ";
           ," ";
           ," ";
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MVM_DETT','MMCODCOM');
      +",MMCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MMCODATT),'MVM_DETT','MMCODATT');
      +",MMTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MMTIPATT),'MVM_DETT','MMTIPATT');
      +",MMCODCOS ="+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOS),'MVM_DETT','MMCODCOS');
      +",MMFLORCO ="+cp_NullLink(cp_ToStrODBC(this.w_MMFLORCO),'MVM_DETT','MMFLORCO');
      +",MMFLCOCO ="+cp_NullLink(cp_ToStrODBC(this.w_MMFLCOCO),'MVM_DETT','MMFLCOCO');
      +",MMIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MMIMPCOM),'MVM_DETT','MMIMPCOM');
          +i_ccchkf ;
      +" where ";
          +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MMNUMRIF = "+cp_ToStrODBC(this.w_MMNUMRIF);
             )
    else
      update (i_cTable) set;
          MMCODCOM = this.w_MMCODCOM;
          ,MMCODATT = this.w_MMCODATT;
          ,MMTIPATT = this.w_MMTIPATT;
          ,MMCODCOS = this.w_MMCODCOS;
          ,MMFLORCO = this.w_MMFLORCO;
          ,MMFLCOCO = this.w_MMFLCOCO;
          ,MMIMPCOM = this.w_MMIMPCOM;
          &i_ccchkf. ;
       where;
          MMSERIAL = this.w_MMSERIAL;
          and CPROWNUM = this.w_CPROWNUM;
          and MMNUMRIF = this.w_MMNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Saldi
    * --- Try
    local bErr_02B5B220
    bErr_02B5B220=bTrsErr
    this.Try_02B5B220()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_02B5B220
    * --- End
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
      +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
             )
    else
      update (i_cTable) set;
          SLQTAPER = &i_cOp1.;
          ,SLQTRPER = &i_cOp2.;
          ,SLQTOPER = &i_cOp3.;
          ,SLQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_MMKEYSAL;
          and SLCODMAG = this.w_MMCODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_ARFLCOMM="S"
      if empty(nvl(this.w_MMCODCOM,""))
        this.w_COMMAPPO = this.w_COMMDEFA
      else
        this.w_COMMAPPO = this.w_MMCODCOM
      endif
      * --- Aggiorna i saldi commessa
      * --- Try
      local bErr_040B2A68
      bErr_040B2A68=bTrsErr
      this.Try_040B2A68()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_040B2A68
      * --- End
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
        +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
        +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
            +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
               )
      else
        update (i_cTable) set;
            SCQTAPER = &i_cOp1.;
            ,SCQTRPER = &i_cOp2.;
            ,SCQTOPER = &i_cOp3.;
            ,SCQTIPER = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_MMKEYSAL;
            and SCCODMAG = this.w_MMCODMAG;
            and SCCODCAN = this.w_COMMAPPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Se articolo gestito a LOTTI/MATRICOLE oppure magazzino ad UBICAZIONI devo gestire immediatamente la cosa.
    this.w_MAGSCA = this.oParentObject.w_CODMAG
    this.w_MGUBISCA = " "
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGFLUBIC"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGSCA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGFLUBIC;
        from (i_cTable) where;
            MGCODMAG = this.w_MAGSCA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MGUBISCA = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MAGCAR = this.oParentObject.w_NEWMAG
    this.w_MGUBICAR = " "
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGFLUBIC"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGCAR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGFLUBIC;
        from (i_cTable) where;
            MGCODMAG = this.w_MAGCAR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MGUBICAR = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ARGESMAT="S"
      * --- Articolo Gestito a matricole
      if this.w_CARSCA="C"
        * --- Contestualmente alla riga di scarico faccio anche il carico delle matricole
        this.w_CPCAR = this.w_CPROWNUM
        do GSMR_KCM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_CPSCA = this.w_CPROWNUM
      endif
    else
      * --- Articolo non Gestito a matricole
      if (g_PERLOT="S" AND this.w_FLLOTT="S") or (g_PERUBI="S" and (this.w_MGUBICAR="S"or this.w_MGUBISCA="S"))
        if this.w_CARSCA="C"
          * --- Contestualmente alla riga di scarico faccio anche il carico di lotti /ubicazioni
          this.w_CPCAR = this.w_CPROWNUM
          do GSMR_KCL with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_CPSCA = this.w_CPROWNUM
        endif
      endif
    endif
  endproc
  proc Try_02B5A740()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTIPATT),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOS),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_MMCODCOM,'CSTIPSTR',this.w_MMTIPATT,'CSCODMAT',this.w_MMCODATT,'CSCODCOS',this.w_MMCODCOS)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           this.w_MMCODCOM;
           ,this.w_MMTIPATT;
           ,this.w_MMCODATT;
           ,this.w_MMCODCOS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02B5B220()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+",SLCODART"+",SLCODVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'SALDIART','SLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'SALDIART','SLCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAR),'SALDIART','SLCODVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODMAG',this.w_MMCODMAG,'SLCODART',this.w_MMCODART,'SLCODVAR',this.w_MMCODVAR)
      insert into (i_cTable) (SLCODICE,SLCODMAG,SLCODART,SLCODVAR &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAG;
           ,this.w_MMCODART;
           ,this.w_MMCODVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_040B2A68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MMKEYSAL,'SCCODMAG',this.w_MMCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MMCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_MMCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Usate per creazione MVM
    * --- Testata movimento
    * --- Dettaglio movimento
    * --- Locali
    * --- Caller
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='CAM_AGAZ'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='MA_COSTI'
    this.cWorkTables[8]='MVM_MAST'
    this.cWorkTables[9]='MVM_DETT'
    this.cWorkTables[10]='CAANARTI'
    this.cWorkTables[11]='MAGAZZIN'
    this.cWorkTables[12]='SALDIART'
    this.cWorkTables[13]='SALDICOM'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
