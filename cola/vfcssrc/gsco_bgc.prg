* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bgc                                                        *
*              Generazione calendario                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-21                                                      *
* Last revis.: 2001-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bgc",oParentObject)
return(i_retval)

define class tgsco_bgc as StdBatch
  * --- Local variables
  w_year = space(4)
  w_INIZIO = ctod("  /  /  ")
  w_FINE = ctod("  /  /  ")
  w_DATA = ctod("  /  /  ")
  Continua = .f.
  w_DESC = space(30)
  w_GIOLAV = space(1)
  i = 0
  w_APPO = ctod("  /  /  ")
  w_INSERTERR = .f.
  w_DATA1 = ctod("  /  /  ")
  w_DSUP1 = space(10)
  w_DATA2 = ctod("  /  /  ")
  w_DSUP2 = space(10)
  w_DATA3 = ctod("  /  /  ")
  w_DSUP3 = space(10)
  w_DATA4 = ctod("  /  /  ")
  w_DSUP4 = space(10)
  w_DATA5 = ctod("  /  /  ")
  w_DSUP5 = space(10)
  w_DATA6 = ctod("  /  /  ")
  w_DSUP6 = space(10)
  w_DATA7 = ctod("  /  /  ")
  w_DSUP7 = space(10)
  w_DATA8 = ctod("  /  /  ")
  w_DSUP8 = space(10)
  w_DATA9 = ctod("  /  /  ")
  w_DSUP9 = space(10)
  * --- WorkFile variables
  CAL_AZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione calendari (da GSCO_KGC)
    * --- Controllo parametri
    if empty(this.oParentObject.w_ANNO)
      ah_ErrorMsg("Deve essere specificato un anno solare",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Carica Calendario
    this.w_YEAR = str(this.oParentObject.w_ANNO,4,0)
    this.w_INIZIO = cp_CharToDate("01-01-"+this.w_YEAR)
    this.w_FINE = cp_CharToDate("31-12-"+this.w_YEAR)
    this.w_INSERTERR = .F.
    private k
    * --- Verifica se il calendario � gi� stato caricato
    this.w_APPO = cp_CharToDate("  -  -  ")
    * --- Select from CAL_AZIE
    i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAL_AZIE ";
          +" where CAGIORNO>="+cp_ToStrODBC(this.w_INIZIO)+" AND CAGIORNO<="+cp_ToStrODBC(this.w_FINE)+"";
           ,"_Curs_CAL_AZIE")
    else
      select * from (i_cTable);
       where CAGIORNO>=this.w_INIZIO AND CAGIORNO<=this.w_FINE;
        into cursor _Curs_CAL_AZIE
    endif
    if used('_Curs_CAL_AZIE')
      select _Curs_CAL_AZIE
      locate for 1=1
      do while not(eof())
      this.w_APPO = _Curs_CAL_AZIE.CAGIORNO
        select _Curs_CAL_AZIE
        continue
      enddo
      use
    endif
    * --- Test ...
    if not empty(this.w_APPO)
      this.Continua = ah_YesNo("Calendario gi� esistente. Sovrascrivo?")
      if this.Continua
        * --- Delete from CAL_AZIE
        i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAGIORNO >= "+cp_ToStrODBC(this.w_INIZIO);
                +" and CAGIORNO <= "+cp_ToStrODBC(this.w_FINE);
                 )
        else
          delete from (i_cTable) where;
                CAGIORNO >= this.w_INIZIO;
                and CAGIORNO <= this.w_FINE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        ah_ErrorMsg("Operazione abbandonata",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Genera ...
    * --- Try
    local bErr_026170A8
    bErr_026170A8=bTrsErr
    this.Try_026170A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Errore durante la scrittura del DB; calendario non generato",,"")
    endif
    bTrsErr=bTrsErr or bErr_026170A8
    * --- End
    if this.w_INSERTERR=.T.
      ah_ErrorMsg("Errore durante la scrittura del DB",,"")
    endif
    * --- Segna Festivita' Fisse
    ah_Msg("Inserimento festivit� fisse in corso...",.T.)
    * --- Prepara date e descrizioni ...
    this.w_DATA1 = cp_CharToDate("01-01-"+this.w_YEAR)
    this.w_DSUP1 = " " + Ah_Msgformat("(Capodanno)")
    this.w_DATA2 = cp_CharToDate("06-01-"+this.w_YEAR)
    this.w_DSUP2 = " " + Ah_Msgformat("(Epifania)")
    this.w_DATA3 = cp_CharToDate("25-04-"+this.w_YEAR)
    this.w_DSUP3 = " " + Ah_Msgformat("(Ann.liberazione)")
    this.w_DATA4 = cp_CharToDate("01-05-"+this.w_YEAR)
    this.w_DSUP4 = " " + Ah_Msgformat("(Festa del lavoro)")
    this.w_DATA5 = cp_CharToDate("15-08-"+this.w_YEAR)
    this.w_DSUP5 = " " + Ah_Msgformat("(Ferragosto)")
    this.w_DATA6 = cp_CharToDate("01-11-"+this.w_YEAR)
    this.w_DSUP6 = " " + Ah_Msgformat("(Ognissanti)")
    this.w_DATA7 = cp_CharToDate("08-12-"+this.w_YEAR)
    this.w_DSUP7 = " " + Ah_Msgformat("(Imm.Concezione)")
    this.w_DATA8 = cp_CharToDate("25-12-"+this.w_YEAR)
    this.w_DSUP8 = " " + Ah_Msgformat("(SS.Natale)")
    this.w_DATA9 = cp_CharToDate("26-12-"+this.w_YEAR)
    this.w_DSUP9 = " " + Ah_Msgformat("(S.Stefano)")
    * --- Riempie Archivio ...
    for this.i=1 to 9
    k = str(this.i,1,0)
    * --- Aggiorna
    this.w_DATA = this.w_DATA&k
    this.w_DESC = this.w_DSUP&k
    * --- Write into CAL_AZIE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAL_AZIE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CAGIOLAV ="+cp_NullLink(cp_ToStrODBC(" "),'CAL_AZIE','CAGIOLAV');
      +",CADESGIO ="+cp_NullLink(cp_ToStrODBC(this.w_DESC),'CAL_AZIE','CADESGIO');
          +i_ccchkf ;
      +" where ";
          +"CAGIORNO = "+cp_ToStrODBC(this.w_DATA);
             )
    else
      update (i_cTable) set;
          CAGIOLAV = " ";
          ,CADESGIO = this.w_DESC;
          &i_ccchkf. ;
       where;
          CAGIORNO = this.w_DATA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    next
    * --- FERIE E CHIUSURE PERIODICHE...........................................
    ah_Msg("Inserimento chiusure periodiche in corso...",.T.)
    FOR L_I=1 TO 5
    L_K = str(L_I, 1, 0)
    this.w_INIZIO = this.oParentObject.w_INPER&L_K
    this.w_FINE = this.oParentObject.w_FIPER&L_K
    if not empty(this.w_INIZIO) and not empty(this.w_FINE)
      * --- Write into CAL_AZIE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAL_AZIE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CAGIOLAV ="+cp_NullLink(cp_ToStrODBC(" "),'CAL_AZIE','CAGIOLAV');
        +",CADESGIO ="+cp_NullLink(cp_ToStrODBC("Chiuso (Ferie)"),'CAL_AZIE','CADESGIO');
            +i_ccchkf ;
        +" where ";
            +"CAGIORNO >= "+cp_ToStrODBC(this.w_INIZIO);
            +" and CAGIORNO <= "+cp_ToStrODBC(this.w_FINE);
               )
      else
        update (i_cTable) set;
            CAGIOLAV = " ";
            ,CADESGIO = "Chiuso (Ferie)";
            &i_ccchkf. ;
         where;
            CAGIORNO >= this.w_INIZIO;
            and CAGIORNO <= this.w_FINE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    NEXT
    * --- Messaggio di chiusura
    wait CLEAR
    ah_ErrorMsg("Generazione calendario completata con successo",,"")
  endproc
  proc Try_026170A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_DATA = this.w_INIZIO
    do while this.w_DATA <= this.w_FINE
      ah_Msg("Generazione calendario giorno (%1) in corso...",.T.,.F.,.F., dtoc(this.w_DATA) )
      this.w_DESC = space(30)
      * --- Conta le ore in base al giorno
      do case
        case Dow(this.w_DATA)=1
          this.w_DESC = Ah_Msgformat("Domenica")
          this.w_GIOLAV = this.oParentObject.w_DOM
        case Dow(this.w_DATA)=2
          this.w_DESC = Ah_Msgformat("Lunedi")
          this.w_GIOLAV = this.oParentObject.w_LUN
        case Dow(this.w_DATA)=3
          this.w_DESC = Ah_Msgformat("Martedi")
          this.w_GIOLAV = this.oParentObject.w_MAR
        case Dow(this.w_DATA)=4
          this.w_DESC = Ah_Msgformat("Mercoledi")
          this.w_GIOLAV = this.oParentObject.w_MER
        case Dow(this.w_DATA)=5
          this.w_DESC = Ah_Msgformat("Giovedi")
          this.w_GIOLAV = this.oParentObject.w_GIO
        case Dow(this.w_DATA)=6
          this.w_DESC = Ah_Msgformat("Venerdi")
          this.w_GIOLAV = this.oParentObject.w_VEN
        case Dow(this.w_DATA)=7
          this.w_DESC = Ah_Msgformat("Sabato")
          this.w_GIOLAV = this.oParentObject.w_SAB
      endcase
      * --- Try
      local bErr_03661C80
      bErr_03661C80=bTrsErr
      this.Try_03661C80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        this.w_INSERTERR = .T.
      endif
      bTrsErr=bTrsErr or bErr_03661C80
      * --- End
      this.w_DATA = this.w_DATA + 1
    enddo
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03661C80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAL_AZIE
    i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAL_AZIE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAGIORNO"+",CADESGIO"+",CAGIOLAV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DATA),'CAL_AZIE','CAGIORNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESC),'CAL_AZIE','CADESGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GIOLAV),'CAL_AZIE','CAGIOLAV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAGIORNO',this.w_DATA,'CADESGIO',this.w_DESC,'CAGIOLAV',this.w_GIOLAV)
      insert into (i_cTable) (CAGIORNO,CADESGIO,CAGIOLAV &i_ccchkf. );
         values (;
           this.w_DATA;
           ,this.w_DESC;
           ,this.w_GIOLAV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAL_AZIE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CAL_AZIE')
      use in _Curs_CAL_AZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
