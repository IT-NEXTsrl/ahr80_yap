* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bor                                                        *
*              Apertura anagrafica ODL/OCL                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-15                                                      *
* Last revis.: 2015-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODODL,w_NUMROW,w_TIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bor",oParentObject,m.w_CODODL,m.w_NUMROW,m.w_TIPO)
return(i_retval)

define class tgsco_bor as StdBatch
  * --- Local variables
  w_CODODL = space(15)
  w_NUMROW = 0
  w_TIPO = space(1)
  w_PROG = .NULL.
  w_PAR = space(1)
  w_RIGA = 0
  w_MPS = space(1)
  w_PROVE = space(1)
  w_STATO = space(1)
  w_CODART = space(20)
  * --- WorkFile variables
  ODL_MAST_idx=0
  ART_PROD_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione apertura Anagrafica ODL/OCL
    * --- Parametri:
    *     Codice Ordine - Parametro Obbligatorio ->ODL_MAST.OLCODODL
    * --- Riga Ordine (ODL_DETT.CPROWNUM) - su ODP-ODR non � gestita, quindi il parametro viene ignorato, su ODL/OCL si posiziona sulla riga 
    *     del dettaglio passata come parametro, se non la si vuole gestire passare 0 oppure lasciare null.
    * --- Forza la selezione del tipo ordine da aprire, 'Z' apre un OCL,'L' un ODL,'P' un ODP, 'F' un ODR, se il parametro non viene passato 
    *     o � diverso da uno di questi 4 valori gestiti viene ricalcolato automaticamente in considerazione della provenienza (ODL_MAST.OLTPROVE) 
    *     e verificando se � Oggetto MPS o Ricambio (ART_PROD.DPOGGMPS)
    * --- ----------------------------
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTSTATO,OLTPROVE,OLTCOART"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTSTATO,OLTPROVE,OLTCOART;
        from (i_cTable) where;
            OLCODODL = this.w_CODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
      this.w_PROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
      this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PAR = iif(vartype(this.w_TIPO)<>"C"," ",this.w_TIPO)
    this.w_RIGA = iif(vartype(this.w_NUMROW)<>"N",0,this.w_NUMROW)
    if ! nvl(this.w_PAR," ") $ "Z-L-P-F-E-S-T"
      * --- Non � stato passato alcun parametro oppure il parametro passato non � gestito, quindi lo ricalcolo
      if g_MMPS="S"
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PROGGMPS"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PROGGMPS;
            from (i_cTable) where;
                PRCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MPS = NVL(cp_ToDate(_read_.PROGGMPS),cp_NullValue(_read_.PROGGMPS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_MPS<>"N" AND (this.w_STATO="C" OR this.w_STATO="S" OR this.w_STATO="D")
          if this.w_PROVE="E"
            * --- E' un ODF
            this.w_PAR = "T"
          else
            if this.w_MPS="S"
              * --- E' un ODP
              this.w_PAR = "P"
            endif
            if this.w_MPS="R"
              * --- Ricambio
              this.w_PAR = "F"
            endif
          endif
        else
        endif
      endif
      do case
        case this.w_PROVE="L"
          * --- E' un OCL
          this.w_PAR = "Z"
        case this.w_PROVE="I"
          * --- E' un ODL
          this.w_PAR = "L"
        case this.w_PROVE="E"
          * --- E' un ODA
          this.w_PAR = "E"
        otherwise
          ah_Msg("Tipo archivio inesistente...")
          i_retcode = 'stop'
          return
      endcase
    endif
    * --- Dettaglio ODL/ODP/OCL
    this.w_PROG = GSCO_AOP(.null., this.w_PAR)
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_PROG.w_OLCODODL = this.w_CODODL
    this.w_PROG.QueryKeySet("OLCODODL='"+ this.w_CODODL +"'")     
    this.w_PROG.LoadRecWarn()     
    if nvl(this.w_RIGA,0)<>0 and ! this.w_PAR $ "P-F"
      * --- Mi posiziono sulla riga
      Local L_cTrsName 
 L_cTrsName=this.w_PROG.GSCO_MOL.cTrsname 
 Select (L_cTrsName) 
 Go Top 
 Locate For CPROWNUM= this.w_RIGA
      if Found()
        this.w_PROG.oPgFrm.ActivePage = 3
        this.w_PROG.GSCO_MOL.oPgFrm.Page1.oPag.oBody.Refresh()     
        this.w_PROG.GSCO_MOL.WorkFromTrs()     
        this.w_PROG.GSCO_MOL.SaveDependsOn()     
        this.w_PROG.GSCO_MOL.SetControlsValue()     
        this.w_PROG.GSCO_MOL.mHideControls()     
        this.w_PROG.GSCO_MOL.ChildrenChangeRow()     
        this.w_PROG.GSCO_MOL.oPgFRm.Page1.oPag.oBody.oBodyCol.SetFocus()     
      endif
    else
      * --- Resto posizionato sulla testata
    endif
    i_retcode = 'stop'
    i_retval = this.w_PROG
    return
  endproc


  proc Init(oParentObject,w_CODODL,w_NUMROW,w_TIPO)
    this.w_CODODL=w_CODODL
    this.w_NUMROW=w_NUMROW
    this.w_TIPO=w_TIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='ART_PROD'
    this.cWorkTables[3]='PAR_RIOR'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODODL,w_NUMROW,w_TIPO"
endproc
