* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_sol                                                        *
*              Stampa ordini di lavorazione                                    *
*                                                                              *
*      Author: TAM Software Srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_74]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-29                                                      *
* Last revis.: 2012-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_sol",oParentObject))

* --- Class definition
define class tgsco_sol as StdForm
  Top    = 13
  Left   = 13

  * --- Standard Properties
  Width  = 626
  Height = 346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-19"
  HelpContextID=110516887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ODL_MAST_IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  MAGAZZIN_IDX = 0
  cPrg = "gsco_sol"
  cComment = "Stampa ordini di lavorazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ODLINI = space(15)
  w_ODLFIN = space(15)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DAFINI = ctod('  /  /  ')
  w_DAFFIN = ctod('  /  /  ')
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_FLSUG = space(1)
  w_FLPIA = space(1)
  w_FLLAN = space(1)
  w_MPS_ODL = space(1)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_PROVEINI = space(1)
  w_PROVEFIN = space(1)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPGES = space(1)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_solPag1","gsco_sol",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oODLINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='MAGAZZIN'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ODLINI=space(15)
      .w_ODLFIN=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DAFINI=ctod("  /  /  ")
      .w_DAFFIN=ctod("  /  /  ")
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_FLSUG=space(1)
      .w_FLPIA=space(1)
      .w_FLLAN=space(1)
      .w_MPS_ODL=space(1)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_PROVEINI=space(1)
      .w_PROVEFIN=space(1)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPGES=space(1)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ODLINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ODLFIN))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,7,.f.)
        if not(empty(.w_CODINI))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODFIN))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_MAGINI))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODCOM))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODATT))
          .link_1_12('Full')
        endif
        .w_FLSUG = 'M'
        .w_FLPIA = 'P'
        .w_FLLAN = 'L'
        .w_MPS_ODL = 'O'
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
          .DoRTCalc(17,22,.f.)
        .w_TIPATT = "A"
        .w_OBTEST = i_INIDAT
    endwith
    this.DoRTCalc(25,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .DoRTCalc(1,23,.t.)
            .w_OBTEST = i_INIDAT
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_11.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_12.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ODLINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLINI))
          select OLCODODL,OLTPROVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLINI_1_1'),i_cWhere,'',"Elenco ordini di lavorazione",'GSCO_SOL.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLINI)
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLINI = NVL(_Link_.OLCODODL,space(15))
      this.w_PROVEINI = NVL(_Link_.OLTPROVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODLINI = space(15)
      endif
      this.w_PROVEINI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN) and .w_PROVEINI='I'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLINI = space(15)
        this.w_PROVEINI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLFIN))
          select OLCODODL,OLTPROVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLFIN_1_2'),i_cWhere,'',"Elenco ordini di lavorazione",'GSCO_SOL.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLFIN)
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLFIN = NVL(_Link_.OLCODODL,space(15))
      this.w_PROVEFIN = NVL(_Link_.OLTPROVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODLFIN = space(15)
      endif
      this.w_PROVEFIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN and .w_PROVEFIN='I'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLFIN = space(15)
        this.w_PROVEFIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_7'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_8'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MAGINI)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_9'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= NOT EMPTY(.w_MAGINI) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino iniziale maggiore di quello finale oppure obsoleto")
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MAGFIN)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_10'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MAGINI<=.w_MAGFIN) and NOT EMPTY(.w_MAGFIN) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino iniziale maggiore di quello finale oppure obsoleto")
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_11'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_12'),i_cWhere,'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oODLINI_1_1.value==this.w_ODLINI)
      this.oPgFrm.Page1.oPag.oODLINI_1_1.value=this.w_ODLINI
    endif
    if not(this.oPgFrm.Page1.oPag.oODLFIN_1_2.value==this.w_ODLFIN)
      this.oPgFrm.Page1.oPag.oODLFIN_1_2.value=this.w_ODLFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_3.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_3.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_4.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_4.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFINI_1_5.value==this.w_DAFINI)
      this.oPgFrm.Page1.oPag.oDAFINI_1_5.value=this.w_DAFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFFIN_1_6.value==this.w_DAFFIN)
      this.oPgFrm.Page1.oPag.oDAFFIN_1_6.value=this.w_DAFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_7.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_7.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_8.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_8.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_9.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_9.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_10.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_10.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_11.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_11.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_12.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_12.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSUG_1_13.RadioValue()==this.w_FLSUG)
      this.oPgFrm.Page1.oPag.oFLSUG_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPIA_1_14.RadioValue()==this.w_FLPIA)
      this.oPgFrm.Page1.oPag.oFLPIA_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLLAN_1_15.RadioValue()==this.w_FLLAN)
      this.oPgFrm.Page1.oPag.oFLLAN_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_23.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_23.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_24.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_24.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_35.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_35.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_37.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_37.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_42.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_42.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_43.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_43.value=this.w_DESMAGF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN) and .w_PROVEINI='I')  and not(empty(.w_ODLINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLINI<=.w_ODLFIN and .w_PROVEFIN='I')  and not(empty(.w_ODLFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAFINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAFFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(COCHKAR(.w_CODINI))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(COCHKAR(.w_CODFIN))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not( NOT EMPTY(.w_MAGINI) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino iniziale maggiore di quello finale oppure obsoleto")
          case   not((.w_MAGINI<=.w_MAGFIN) and NOT EMPTY(.w_MAGFIN) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino iniziale maggiore di quello finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco_solPag1 as StdContainer
  Width  = 622
  height = 346
  stdWidth  = 622
  stdheight = 346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oODLINI_1_1 as StdField with uid="TMVOMSBJOT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ODLINI", cQueryName = "ODLINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL di inizio selezione (vuoto=no selezione)",;
    HelpContextID = 188455962,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=118, Top=10, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLINI"

  func oODLINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ordini di lavorazione",'GSCO_SOL.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oODLFIN_1_2 as StdField with uid="KGBHZSHPWV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ODLFIN", cQueryName = "ODLFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL di fine selezione (vuoto=no selezione)",;
    HelpContextID = 110009370,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=118, Top=35, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLFIN"

  proc oODLFIN_1_2.mDefault
    with this.Parent.oContained
      if empty(.w_ODLFIN)
        .w_ODLFIN = .w_ODLINI
      endif
    endwith
  endproc

  func oODLFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ordini di lavorazione",'GSCO_SOL.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oDATINI_1_3 as StdField with uid="KADSLBXBRA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di inizio lavorazione",;
    HelpContextID = 188424138,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=370, Top=10

  func oDATINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_4 as StdField with uid="ODRUJEKWCF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di inizio lavorazione",;
    HelpContextID = 109977546,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=370, Top=35

  func oDATFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDAFINI_1_5 as StdField with uid="CKVGPKUDPP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DAFINI", cQueryName = "DAFINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di fine lavorazione",;
    HelpContextID = 188481482,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=543, Top=10

  func oDAFINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
    endwith
    return bRes
  endfunc

  add object oDAFFIN_1_6 as StdField with uid="PWYQVYUXKE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DAFFIN", cQueryName = "DAFFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di fine lavorazione",;
    HelpContextID = 110034890,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=543, Top=35

  func oDAFFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
    endwith
    return bRes
  endfunc

  add object oCODINI_1_7 as StdField with uid="COEWNKTBFQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di inizio selezione (vuota=no selezione)",;
    HelpContextID = 188486106,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=118, Top=66, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_8 as StdField with uid="ZYGJAJAVLU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di fine selezione (vuota=no selezione)",;
    HelpContextID = 110039514,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=118, Top=90, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  proc oCODFIN_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_CODFIN)
        .w_CODFIN = .w_CODINI
      endif
    endwith
  endproc

  func oCODFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oMAGINI_1_9 as StdField with uid="KJJPHJWMBL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino iniziale maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 188477242,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMAGFIN_1_10 as StdField with uid="BTOGEDJYFI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino iniziale maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 110030650,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_10.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oCODCOM_1_11 as StdField with uid="YLQVWTCUFO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 120721882,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=118, Top=173, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_12 as StdField with uid="CMRHBCMAKU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita",;
    HelpContextID = 266605018,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=118, Top=197, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" and not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oFLSUG_1_13 as StdCheck with uid="TAPGSEINNN",rtseq=13,rtrep=.f.,left=120, top=234, caption="Suggeriti",;
    ToolTipText = "Stampa ODL suggeriti",;
    HelpContextID = 190896982,;
    cFormVar="w_FLSUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSUG_1_13.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oFLSUG_1_13.GetRadio()
    this.Parent.oContained.w_FLSUG = this.RadioValue()
    return .t.
  endfunc

  func oFLSUG_1_13.SetRadio()
    this.Parent.oContained.w_FLSUG=trim(this.Parent.oContained.w_FLSUG)
    this.value = ;
      iif(this.Parent.oContained.w_FLSUG=='M',1,;
      0)
  endfunc

  add object oFLPIA_1_14 as StdCheck with uid="EDXMXNACPM",rtseq=14,rtrep=.f.,left=276, top=234, caption="Pianificati",;
    ToolTipText = "Stampa ODL pianificati",;
    HelpContextID = 183806806,;
    cFormVar="w_FLPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPIA_1_14.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oFLPIA_1_14.GetRadio()
    this.Parent.oContained.w_FLPIA = this.RadioValue()
    return .t.
  endfunc

  func oFLPIA_1_14.SetRadio()
    this.Parent.oContained.w_FLPIA=trim(this.Parent.oContained.w_FLPIA)
    this.value = ;
      iif(this.Parent.oContained.w_FLPIA=='P',1,;
      0)
  endfunc

  add object oFLLAN_1_15 as StdCheck with uid="SPHINUGANI",rtseq=15,rtrep=.f.,left=449, top=234, caption="Lanciati",;
    ToolTipText = "Stampa ODL lanciati",;
    HelpContextID = 196897622,;
    cFormVar="w_FLLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLLAN_1_15.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oFLLAN_1_15.GetRadio()
    this.Parent.oContained.w_FLLAN = this.RadioValue()
    return .t.
  endfunc

  func oFLLAN_1_15.SetRadio()
    this.Parent.oContained.w_FLLAN=trim(this.Parent.oContained.w_FLLAN)
    this.value = ;
      iif(this.Parent.oContained.w_FLLAN=='L',1,;
      0)
  endfunc

  add object oDESINI_1_23 as StdField with uid="VKRHDPMAWE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo inizio selezione",;
    HelpContextID = 188427210,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=276, Top=66, InputMask=replicate('X',40)

  add object oDESFIN_1_24 as StdField with uid="UBIUCOJSJG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo fine selezione",;
    HelpContextID = 109980618,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=276, Top=90, InputMask=replicate('X',40)


  add object oObj_1_27 as cp_outputCombo with uid="ZLERXIVPWT",left=118, top=267, width=492,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 248356378


  add object oBtn_1_28 as StdButton with uid="XAOFRYAIID",left=510, top=294, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la stampa";
    , HelpContextID = 16128986;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="MJNXVZGNOX",left=560, top=294, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 117834310;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAN_1_35 as StdField with uid="PJHERXXTYJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 118565834,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=252, Top=173, InputMask=replicate('X',30)

  add object oDESATT_1_37 as StdField with uid="JRUPUPZCWV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 266546122,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=252, Top=197, InputMask=replicate('X',30)

  add object oDESMAGI_1_42 as StdField with uid="JXPYNAXWLG",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 33084470,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=182, Top=119, InputMask=replicate('X',30)

  add object oDESMAGF_1_43 as StdField with uid="YBQEEEROIS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 235350986,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=182, Top=144, InputMask=replicate('X',30)

  add object oStr_1_16 as StdString with uid="JHWCGYNXKI",Visible=.t., Left=9, Top=267,;
    Alignment=1, Width=105, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="CGKMUWGJNM",Visible=.t., Left=255, Top=12,;
    Alignment=1, Width=113, Height=15,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="DFAYSLSKFL",Visible=.t., Left=255, Top=37,;
    Alignment=1, Width=113, Height=15,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="BQTQZXUPTH",Visible=.t., Left=9, Top=234,;
    Alignment=1, Width=105, Height=18,;
    Caption="Stato ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QZYVSVNFWE",Visible=.t., Left=446, Top=12,;
    Alignment=1, Width=94, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="GNOODMLOOO",Visible=.t., Left=446, Top=37,;
    Alignment=1, Width=94, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=9, Top=68,;
    Alignment=1, Width=105, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=9, Top=91,;
    Alignment=1, Width=105, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="WVAVYFBMFI",Visible=.t., Left=9, Top=12,;
    Alignment=1, Width=105, Height=18,;
    Caption="Da codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="QMCNFQOCKF",Visible=.t., Left=9, Top=37,;
    Alignment=1, Width=105, Height=18,;
    Caption="A codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=12, Top=175,;
    Alignment=1, Width=103, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=12, Top=199,;
    Alignment=1, Width=103, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VIKBXNZVOP",Visible=.t., Left=4, Top=121,;
    Alignment=1, Width=110, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="DLYZPVXCVE",Visible=.t., Left=4, Top=146,;
    Alignment=1, Width=110, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_sol','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
