* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_svf                                                        *
*              Lista ODL con verifica materiali                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_26]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-30                                                      *
* Last revis.: 2016-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_svf",oParentObject))

* --- Class definition
define class tgsco_svf as StdForm
  Top    = 6
  Left   = 6

  * --- Standard Properties
  Width  = 905
  Height = 505
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-02-16"
  HelpContextID=40478057
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=55

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ODL_MAST_IDX = 0
  MARCHI_IDX = 0
  CONTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  PAR_PROD_IDX = 0
  cpusers_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gsco_svf"
  cComment = "Lista ODL con verifica materiali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_READPAR = space(2)
  w_ODLINI = space(15)
  w_ODLFIN = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_CODFOR = space(15)
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MARINI = space(5)
  w_MARFIN = space(5)
  w_FLSUG = space(1)
  w_FLPIA = space(1)
  w_FLLAN = space(1)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_PROVEINI = space(1)
  w_PROVEFIN = space(1)
  w_DESFAMAI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUI = space(35)
  w_DESGRUF = space(35)
  w_DESCATI = space(35)
  w_DESCATF = space(35)
  w_TIPCON = space(1)
  w_DESFOR = space(40)
  w_MAGTER = space(5)
  w_DESMARI = space(35)
  w_DESMARF = space(35)
  w_PROPRE = space(1)
  o_PROPRE = space(1)
  w_TIPGES = space(1)
  o_TIPGES = space(1)
  w_TIPATT = space(1)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DATVER = ctod('  /  /  ')
  w_ORAVER = space(5)
  w_OPEVER = 0
  w_DESUTE = space(20)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPVER = space(1)
  w_PROINT = space(1)
  w_PROEST = space(1)
  w_ARFABB = space(1)
  w_ARSCO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_svfPag1","gsco_svf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oODLINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='MARCHI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='FAM_ARTI'
    this.cWorkTables[6]='GRUMERC'
    this.cWorkTables[7]='CATEGOMO'
    this.cWorkTables[8]='PAR_PROD'
    this.cWorkTables[9]='cpusers'
    this.cWorkTables[10]='CAN_TIER'
    this.cWorkTables[11]='ATTIVITA'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_READPAR=space(2)
      .w_ODLINI=space(15)
      .w_ODLFIN=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_CODFOR=space(15)
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_MARINI=space(5)
      .w_MARFIN=space(5)
      .w_FLSUG=space(1)
      .w_FLPIA=space(1)
      .w_FLLAN=space(1)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_PROVEINI=space(1)
      .w_PROVEFIN=space(1)
      .w_DESFAMAI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUI=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATI=space(35)
      .w_DESCATF=space(35)
      .w_TIPCON=space(1)
      .w_DESFOR=space(40)
      .w_MAGTER=space(5)
      .w_DESMARI=space(35)
      .w_DESMARF=space(35)
      .w_PROPRE=space(1)
      .w_TIPGES=space(1)
      .w_TIPATT=space(1)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATVER=ctod("  /  /  ")
      .w_ORAVER=space(5)
      .w_OPEVER=0
      .w_DESUTE=space(20)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPVER=space(1)
      .w_PROINT=space(1)
      .w_PROEST=space(1)
      .w_ARFABB=space(1)
      .w_ARSCO=space(1)
        .w_CODAZI = i_CODAZI
        .w_READPAR = 'PP'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READPAR))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ODLINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ODLFIN))
          .link_1_4('Full')
        endif
        .w_DATAINI = .w_DATINI
        .w_DATAFIN = i_DATSYS
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODCOM))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODATT))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODFOR))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODINI))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODFIN))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_GRUINI))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CATINI))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CATFIN))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_MARINI))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_MARFIN))
          .link_1_19('Full')
        endif
        .w_FLSUG = 'M'
        .w_FLPIA = 'P'
        .w_FLLAN = 'L'
          .DoRTCalc(23,32,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(34,37,.f.)
        .w_PROPRE = SPACE(1)
        .w_TIPGES = SPACE(1)
        .w_TIPATT = "A"
          .DoRTCalc(41,42,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(44,46,.f.)
        .w_OPEVER = .w_OPEVER
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_OPEVER))
          .link_1_77('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
          .DoRTCalc(48,51,.f.)
        .w_PROINT = IIF(.w_PROPRE<>'L' , 'I', 'X')
        .w_PROEST = IIF(.w_PROPRE<>'I' , 'L', 'X')
        .w_ARFABB = IIF(.w_TIPGES<>'S' , 'F', 'X')
        .w_ARSCO = IIF(.w_TIPGES<>'F' , 'S', 'X')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_READPAR = 'PP'
          .link_1_2('Full')
        endif
        .DoRTCalc(3,42,.t.)
            .w_OBTEST = i_INIDAT
        .DoRTCalc(44,46,.t.)
          .link_1_77('Full')
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .DoRTCalc(48,51,.t.)
        if .o_PROPRE<>.w_PROPRE
            .w_PROINT = IIF(.w_PROPRE<>'L' , 'I', 'X')
        endif
        if .o_PROPRE<>.w_PROPRE
            .w_PROEST = IIF(.w_PROPRE<>'I' , 'L', 'X')
        endif
        if .o_TIPGES<>.w_TIPGES
            .w_ARFABB = IIF(.w_TIPGES<>'S' , 'F', 'X')
        endif
        if .o_PROPRE<>.w_PROPRE
            .w_ARSCO = IIF(.w_TIPGES<>'F' , 'S', 'X')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_7.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_8.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_89.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPOPEVER,PPELAVER,PPORAVER,PPDINVER,PPDFIVER,PPTIPVER";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPOPEVER,PPELAVER,PPORAVER,PPDINVER,PPDFIVER,PPTIPVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(2))
      this.w_OPEVER = NVL(_Link_.PPOPEVER,0)
      this.w_DATVER = NVL(cp_ToDate(_Link_.PPELAVER),ctod("  /  /  "))
      this.w_ORAVER = NVL(_Link_.PPORAVER,space(5))
      this.w_DATINI = NVL(cp_ToDate(_Link_.PPDINVER),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.PPDFIVER),ctod("  /  /  "))
      this.w_TIPVER = NVL(_Link_.PPTIPVER,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(2)
      endif
      this.w_OPEVER = 0
      this.w_DATVER = ctod("  /  /  ")
      this.w_ORAVER = space(5)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
      this.w_TIPVER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLINI))
          select OLCODODL,OLTPROVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLINI_1_3'),i_cWhere,'GSCO_BZA',"ELENCO ORDINI DI LAVORAZIONE",'GSCO_SVF.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLINI)
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLINI = NVL(_Link_.OLCODODL,space(15))
      this.w_PROVEINI = NVL(_Link_.OLTPROVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODLINI = space(15)
      endif
      this.w_PROVEINI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLINI = space(15)
        this.w_PROVEINI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLFIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLFIN))
          select OLCODODL,OLTPROVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLFIN_1_4'),i_cWhere,'GSCO_BZA',"ELENCO ORDINI DI LAVORAZIONE",'GSCO_SVF.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTPROVE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLFIN)
            select OLCODODL,OLTPROVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLFIN = NVL(_Link_.OLCODODL,space(15))
      this.w_PROVEFIN = NVL(_Link_.OLTPROVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODLFIN = space(15)
      endif
      this.w_PROVEFIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLFIN = space(15)
        this.w_PROVEFIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_7'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_8'),i_cWhere,'',"Attivitą",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR_1_9'),i_cWhere,'',"Elenco fornitori",'gscozscs.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTER = NVL(_Link_.ANMAGTER,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_MAGTER = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODFOR) OR NOT EMPTY(.w_MAGTER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        endif
        this.w_CODFOR = space(15)
        this.w_DESFOR = space(40)
        this.w_MAGTER = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_10'),i_cWhere,'GSMA_ACA',"CODICI DI RICERCA",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_11'),i_cWhere,'GSMA_ACA',"CODICI DI RICERCA",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_12'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_13'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_14'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_15'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_16'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(_Link_.OMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_17'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(_Link_.OMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARINI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARINI_1_18'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARINI = space(5)
      endif
      this.w_DESMARI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MARINI = space(5)
        this.w_DESMARI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARFIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARFIN_1_19'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARFIN = space(5)
      endif
      this.w_DESMARF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MARFIN = space(5)
        this.w_DESMARF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPEVER
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPEVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPEVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OPEVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OPEVER)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPEVER = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OPEVER = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPEVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oODLINI_1_3.value==this.w_ODLINI)
      this.oPgFrm.Page1.oPag.oODLINI_1_3.value=this.w_ODLINI
    endif
    if not(this.oPgFrm.Page1.oPag.oODLFIN_1_4.value==this.w_ODLFIN)
      this.oPgFrm.Page1.oPag.oODLFIN_1_4.value=this.w_ODLFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_5.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_5.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_6.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_6.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_7.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_7.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_8.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_8.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFOR_1_9.value==this.w_CODFOR)
      this.oPgFrm.Page1.oPag.oCODFOR_1_9.value=this.w_CODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_10.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_10.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_11.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_11.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_12.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_12.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_13.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_13.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_14.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_14.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_15.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_15.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_16.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_16.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_17.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_17.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMARINI_1_18.value==this.w_MARINI)
      this.oPgFrm.Page1.oPag.oMARINI_1_18.value=this.w_MARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMARFIN_1_19.value==this.w_MARFIN)
      this.oPgFrm.Page1.oPag.oMARFIN_1_19.value=this.w_MARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSUG_1_20.RadioValue()==this.w_FLSUG)
      this.oPgFrm.Page1.oPag.oFLSUG_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPIA_1_21.RadioValue()==this.w_FLPIA)
      this.oPgFrm.Page1.oPag.oFLPIA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLLAN_1_22.RadioValue()==this.w_FLLAN)
      this.oPgFrm.Page1.oPag.oFLLAN_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_27.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_27.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_28.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_28.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_35.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_35.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_36.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_36.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_37.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_37.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_38.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_38.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_39.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_39.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_40.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_40.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_52.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_52.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMARI_1_55.value==this.w_DESMARI)
      this.oPgFrm.Page1.oPag.oDESMARI_1_55.value=this.w_DESMARI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMARF_1_57.value==this.w_DESMARF)
      this.oPgFrm.Page1.oPag.oDESMARF_1_57.value=this.w_DESMARF
    endif
    if not(this.oPgFrm.Page1.oPag.oPROPRE_1_60.RadioValue()==this.w_PROPRE)
      this.oPgFrm.Page1.oPag.oPROPRE_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPGES_1_62.RadioValue()==this.w_TIPGES)
      this.oPgFrm.Page1.oPag.oTIPGES_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_65.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_65.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_67.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_67.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVER_1_74.value==this.w_DATVER)
      this.oPgFrm.Page1.oPag.oDATVER_1_74.value=this.w_DATVER
    endif
    if not(this.oPgFrm.Page1.oPag.oORAVER_1_75.value==this.w_ORAVER)
      this.oPgFrm.Page1.oPag.oORAVER_1_75.value=this.w_ORAVER
    endif
    if not(this.oPgFrm.Page1.oPag.oOPEVER_1_77.value==this.w_OPEVER)
      this.oPgFrm.Page1.oPag.oOPEVER_1_77.value=this.w_OPEVER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_78.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_78.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_84.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_84.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_85.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_85.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVER_1_86.RadioValue()==this.w_TIPVER)
      this.oPgFrm.Page1.oPag.oTIPVER_1_86.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN))  and not(empty(.w_ODLINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLINI<=.w_ODLFIN)  and not(empty(.w_ODLFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATAFIN)) or not(.w_DATAINI<=.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_CODFOR) OR NOT EMPTY(.w_MAGTER))  and not(empty(.w_CODFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFOR_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
          case   not(COCHKAR(.w_CODINI))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(COCHKAR(.w_CODFIN))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN))  and not(empty(.w_MARINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN))  and not(empty(.w_MARFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PROPRE = this.w_PROPRE
    this.o_TIPGES = this.w_TIPGES
    return

enddefine

* --- Define pages as container
define class tgsco_svfPag1 as StdContainer
  Width  = 901
  height = 505
  stdWidth  = 901
  stdheight = 505
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oODLINI_1_3 as StdField with uid="TMVOMSBJOT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ODLINI", cQueryName = "ODLINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL di inizio selezione (vuoto=no selezione)",;
    HelpContextID = 197420006,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=133, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLINI"

  func oODLINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"ELENCO ORDINI DI LAVORAZIONE",'GSCO_SVF.ODL_MAST_VZM',this.parent.oContained
  endproc
  proc oODLINI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLCODODL=this.parent.oContained.w_ODLINI
     i_obj.ecpSave()
  endproc

  add object oODLFIN_1_4 as StdField with uid="KGBHZSHPWV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ODLFIN", cQueryName = "ODLFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL di fine selezione (vuoto=no selezione)",;
    HelpContextID = 7431142,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=133, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLFIN"

  proc oODLFIN_1_4.mDefault
    with this.Parent.oContained
      if empty(.w_ODLFIN)
        .w_ODLFIN = .w_ODLINI
      endif
    endwith
  endproc

  func oODLFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLFIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLFIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLFIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"ELENCO ORDINI DI LAVORAZIONE",'GSCO_SVF.ODL_MAST_VZM',this.parent.oContained
  endproc
  proc oODLFIN_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLCODODL=this.parent.oContained.w_ODLFIN
     i_obj.ecpSave()
  endproc

  add object oDATAINI_1_5 as StdField with uid="FAEQCZXKSB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data...",;
    HelpContextID = 261300170,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=418, Top=39

  add object oDATAFIN_1_6 as StdField with uid="HZZUDPJZZX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data...",;
    HelpContextID = 188538934,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=418, Top=63

  func oDATAFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINI<=.w_DATAFIN)
    endwith
    return bRes
  endfunc

  add object oCODCOM_1_7 as StdField with uid="YLQVWTCUFO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 265154086,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=133, Top=92, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_8 as StdField with uid="CMRHBCMAKU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivita",;
    HelpContextID = 119270950,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=133, Top=116, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" and not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivitą",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oCODFOR_1_9 as StdField with uid="WSTMGWLANE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODFOR", cQueryName = "CODFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o magazzino terzista non definito",;
    ToolTipText = "Codice fornitore conto lavoro",;
    HelpContextID = 80801318,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=133, Top=146, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR"

  func oCODFOR_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fornitori",'gscozscs.CONTI_VZM',this.parent.oContained
  endproc

  add object oCODINI_1_10 as StdField with uid="COEWNKTBFQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di inizio selezione (vuota=no selezione)",;
    HelpContextID = 197389862,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=125, Top=213, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"CODICI DI RICERCA",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_11 as StdField with uid="ZYGJAJAVLU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di fine selezione (vuota=no selezione)",;
    HelpContextID = 7400998,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=125, Top=237, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  proc oCODFIN_1_11.mDefault
    with this.Parent.oContained
      if empty(.w_CODFIN)
        .w_CODFIN = .w_CODINI
      endif
    endwith
  endproc

  func oCODFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"CODICI DI RICERCA",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oFAMAINI_1_12 as StdField with uid="BAUZFWPTWC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Famiglia Articolo di Inizio Selezione",;
    HelpContextID = 261328810,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=125, Top=264, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_13 as StdField with uid="GHDDKOKVIG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Famiglia Articolo di Fine Selezione",;
    HelpContextID = 188510294,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=567, Top=263, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_13.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oGRUINI_1_14 as StdField with uid="VYQYSTJFCH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Gruppo Merceologico di Inizio Selezione",;
    HelpContextID = 197460326,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=125, Top=288, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_15 as StdField with uid="BKTBROMCMD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Gruppo Merceologico di Fine Selezione",;
    HelpContextID = 7471462,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=567, Top=288, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_15.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oCATINI_1_16 as StdField with uid="WQNLFQEXDD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Categoria Omogenea di Inizio Selezione",;
    HelpContextID = 197451814,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=125, Top=316, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oCATFIN_1_17 as StdField with uid="WHQQEZYTLE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Categoria Omogenea di Fine Selezione",;
    HelpContextID = 7462950,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=567, Top=316, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_17.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oMARINI_1_18 as StdField with uid="TQUOVOZXDY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MARINI", cQueryName = "MARINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 197443782,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=125, Top=341, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARINI"

  func oMARINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARINI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARINI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARINI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARINI_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARINI
     i_obj.ecpSave()
  endproc

  add object oMARFIN_1_19 as StdField with uid="HNSRDNTWGP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MARFIN", cQueryName = "MARFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 7454918,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=567, Top=341, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARFIN"

  func oMARFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARFIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARFIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARFIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARFIN_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARFIN
     i_obj.ecpSave()
  endproc

  add object oFLSUG_1_20 as StdCheck with uid="TAPGSEINNN",rtseq=20,rtrep=.f.,left=143, top=427, caption="Suggeriti",;
    ToolTipText = "Stampa ODL suggeriti",;
    HelpContextID = 228533418,;
    cFormVar="w_FLSUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSUG_1_20.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oFLSUG_1_20.GetRadio()
    this.Parent.oContained.w_FLSUG = this.RadioValue()
    return .t.
  endfunc

  func oFLSUG_1_20.SetRadio()
    this.Parent.oContained.w_FLSUG=trim(this.Parent.oContained.w_FLSUG)
    this.value = ;
      iif(this.Parent.oContained.w_FLSUG=='M',1,;
      0)
  endfunc

  add object oFLPIA_1_21 as StdCheck with uid="EDXMXNACPM",rtseq=21,rtrep=.f.,left=295, top=427, caption="Pianificati",;
    ToolTipText = "Stampa ODL pianificati",;
    HelpContextID = 235623594,;
    cFormVar="w_FLPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPIA_1_21.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oFLPIA_1_21.GetRadio()
    this.Parent.oContained.w_FLPIA = this.RadioValue()
    return .t.
  endfunc

  func oFLPIA_1_21.SetRadio()
    this.Parent.oContained.w_FLPIA=trim(this.Parent.oContained.w_FLPIA)
    this.value = ;
      iif(this.Parent.oContained.w_FLPIA=='P',1,;
      0)
  endfunc

  add object oFLLAN_1_22 as StdCheck with uid="SPHINUGANI",rtseq=22,rtrep=.f.,left=457, top=427, caption="Lanciati",;
    ToolTipText = "Stampa ODL lanciati",;
    HelpContextID = 222532778,;
    cFormVar="w_FLLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLLAN_1_22.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oFLLAN_1_22.GetRadio()
    this.Parent.oContained.w_FLLAN = this.RadioValue()
    return .t.
  endfunc

  func oFLLAN_1_22.SetRadio()
    this.Parent.oContained.w_FLLAN=trim(this.Parent.oContained.w_FLLAN)
    this.value = ;
      iif(this.Parent.oContained.w_FLLAN=='L',1,;
      0)
  endfunc


  add object oBtn_1_23 as StdButton with uid="KTPTOSVORT",left=789, top=452, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la verifica di fattibilitą del piano ODL";
    , HelpContextID = 40449306;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="KYQESPJULE",left=841, top=452, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33160634;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESINI_1_27 as StdField with uid="VKRHDPMAWE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo inizio selezione",;
    HelpContextID = 197448758,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=283, Top=213, InputMask=replicate('X',40)

  add object oDESFIN_1_28 as StdField with uid="UBIUCOJSJG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo fine selezione",;
    HelpContextID = 7459894,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=283, Top=237, InputMask=replicate('X',40)

  add object oDESFAMAI_1_35 as StdField with uid="AOYBYAMKXI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 250729599,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=264, InputMask=replicate('X',35)

  add object oDESFAMAF_1_36 as StdField with uid="WOCDSQXKSA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 250729596,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=625, Top=263, InputMask=replicate('X',35)

  add object oDESGRUI_1_37 as StdField with uid="ZABORREMDH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134032330,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=288, InputMask=replicate('X',35)

  add object oDESGRUF_1_38 as StdField with uid="PYGQUHDJMJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134032330,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=625, Top=288, InputMask=replicate('X',35)

  add object oDESCATI_1_39 as StdField with uid="ZFWYARIWSP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 168897482,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=316, InputMask=replicate('X',35)

  add object oDESCATF_1_40 as StdField with uid="SXSGGSHCII",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 168897482,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=625, Top=316, InputMask=replicate('X',35)

  add object oDESFOR_1_52 as StdField with uid="QITDWTVWPI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 80860214,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=290, Top=146, InputMask=replicate('X',40)

  add object oDESMARI_1_55 as StdField with uid="EUHFXBJVSN",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESMARI", cQueryName = "DESMARI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 201796554,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=341, InputMask=replicate('X',35)

  add object oDESMARF_1_57 as StdField with uid="PYMQATOADO",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESMARF", cQueryName = "DESMARF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 201796554,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=625, Top=341, InputMask=replicate('X',35)


  add object oPROPRE_1_60 as StdCombo with uid="LLFMQJWOET",value=3,rtseq=38,rtrep=.f.,left=125,top=372,width=85,height=21;
    , ToolTipText = "Indica la provenienza del codice: interna, cliente o fornitore o altro";
    , HelpContextID = 134980086;
    , cFormVar="w_PROPRE",RowSource=""+"Interna,"+"Conto Lavoro,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROPRE_1_60.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'L',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oPROPRE_1_60.GetRadio()
    this.Parent.oContained.w_PROPRE = this.RadioValue()
    return .t.
  endfunc

  func oPROPRE_1_60.SetRadio()
    this.Parent.oContained.w_PROPRE=trim(this.Parent.oContained.w_PROPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PROPRE=='I',1,;
      iif(this.Parent.oContained.w_PROPRE=='L',2,;
      iif(this.Parent.oContained.w_PROPRE=='',3,;
      0)))
  endfunc


  add object oTIPGES_1_62 as StdCombo with uid="TAUGCKHSMB",value=3,rtseq=39,rtrep=.f.,left=321,top=372,width=106,height=21;
    , ToolTipText = "Indica il tipo di gestione dell'articolo";
    , HelpContextID = 87206198;
    , cFormVar="w_TIPGES",RowSource=""+"a fabbisogno,"+"a scorta,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPGES_1_62.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPGES_1_62.GetRadio()
    this.Parent.oContained.w_TIPGES = this.RadioValue()
    return .t.
  endfunc

  func oTIPGES_1_62.SetRadio()
    this.Parent.oContained.w_TIPGES=trim(this.Parent.oContained.w_TIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGES=='F',1,;
      iif(this.Parent.oContained.w_TIPGES=='S',2,;
      iif(this.Parent.oContained.w_TIPGES=='',3,;
      0)))
  endfunc

  add object oDESCAN_1_65 as StdField with uid="PJHERXXTYJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 267310134,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=269, Top=92, InputMask=replicate('X',30)

  add object oDESATT_1_67 as StdField with uid="JRUPUPZCWV",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 119329846,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=269, Top=116, InputMask=replicate('X',30)

  add object oDATVER_1_74 as StdField with uid="VIRRLCYMZG",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DATVER", cQueryName = "DATVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 71426102,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=670, Top=35

  add object oORAVER_1_75 as StdField with uid="DSJEXZOHCH",rtseq=46,rtrep=.f.,;
    cFormVar = "w_ORAVER", cQueryName = "ORAVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Ora ultima elaborazione",;
    HelpContextID = 71352806,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=823, Top=35, InputMask=replicate('X',5)

  add object oOPEVER_1_77 as StdField with uid="ONGZGCMGJS",rtseq=47,rtrep=.f.,;
    cFormVar = "w_OPEVER", cQueryName = "OPEVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha eseguito l'ultima verifica materiali",;
    HelpContextID = 71368678,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=670, Top=58, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OPEVER"

  func oOPEVER_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTE_1_78 as StdField with uid="EJGTDEDPGF",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 137417782,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=712, Top=58, InputMask=replicate('X',20)

  add object oDATINI_1_84 as StdField with uid="MIMWMOLVUL",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio del periodo elaborato",;
    HelpContextID = 197451830,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=724, Top=152

  add object oDATFIN_1_85 as StdField with uid="UVWTRCHKIX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine del periodo elaborato",;
    HelpContextID = 7462966,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=724, Top=176

  add object oTIPVER_1_86 as StdRadio with uid="JABPVGDPPQ",rtseq=51,rtrep=.f.,left=670, top=86, width=145,height=32, enabled=.f.;
    , cFormVar="w_TIPVER", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPVER_1_86.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Su disponibilitą"
      this.Buttons(1).HelpContextID = 71412022
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Su disp. contabile"
      this.Buttons(2).HelpContextID = 71412022
      this.Buttons(2).Top=15
      this.SetAll("Width",143)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPVER_1_86.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPVER_1_86.GetRadio()
    this.Parent.oContained.w_TIPVER = this.RadioValue()
    return .t.
  endfunc

  func oTIPVER_1_86.SetRadio()
    this.Parent.oContained.w_TIPVER=trim(this.Parent.oContained.w_TIPVER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVER=='D',1,;
      iif(this.Parent.oContained.w_TIPVER=='C',2,;
      0))
  endfunc


  add object oObj_1_89 as cp_outputCombo with uid="ZLERXIVPWT",left=139, top=460, width=448,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137519590

  add object oStr_1_25 as StdString with uid="ZHOWZAIPAD",Visible=.t., Left=299, Top=42,;
    Alignment=1, Width=113, Height=15,;
    Caption="Da data inizio ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="NGTAUJYIBH",Visible=.t., Left=299, Top=65,;
    Alignment=1, Width=113, Height=15,;
    Caption="A data inizio ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=16, Top=215,;
    Alignment=1, Width=105, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=16, Top=239,;
    Alignment=1, Width=105, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="WVAVYFBMFI",Visible=.t., Left=24, Top=42,;
    Alignment=1, Width=105, Height=18,;
    Caption="Da codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="QMCNFQOCKF",Visible=.t., Left=24, Top=65,;
    Alignment=1, Width=105, Height=18,;
    Caption="A codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=51, Top=267,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=38, Top=290,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da Gr. Merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=31, Top=318,;
    Alignment=1, Width=90, Height=18,;
    Caption="Da Cat. Omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="BETNLUNOYI",Visible=.t., Left=498, Top=267,;
    Alignment=1, Width=65, Height=18,;
    Caption="A Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=498, Top=290,;
    Alignment=1, Width=65, Height=18,;
    Caption="A Gr. Merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=485, Top=318,;
    Alignment=1, Width=78, Height=18,;
    Caption="A Cat. Omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="PEISJRCHUW",Visible=.t., Left=7, Top=185,;
    Alignment=0, Width=98, Height=16,;
    Caption="Selezioni Articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="MUGJTUPSPN",Visible=.t., Left=15, Top=11,;
    Alignment=0, Width=86, Height=16,;
    Caption="Selezioni ODL"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="EAQXXFQMDK",Visible=.t., Left=19, Top=148,;
    Alignment=1, Width=110, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="HPWHCHUVEZ",Visible=.t., Left=22, Top=343,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="EKPIHXYMBX",Visible=.t., Left=464, Top=343,;
    Alignment=1, Width=99, Height=18,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="NZYWHUFLLR",Visible=.t., Left=36, Top=373,;
    Alignment=1, Width=85, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="GCYHFTHGTY",Visible=.t., Left=228, Top=373,;
    Alignment=1, Width=88, Height=18,;
    Caption="Tipo di gestione"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=26, Top=94,;
    Alignment=1, Width=103, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=26, Top=118,;
    Alignment=1, Width=103, Height=15,;
    Caption="Attivitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="BQTQZXUPTH",Visible=.t., Left=11, Top=428,;
    Alignment=1, Width=105, Height=18,;
    Caption="Stato ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="OGYKNRKNCP",Visible=.t., Left=566, Top=11,;
    Alignment=0, Width=320, Height=15,;
    Caption="Parametri ultima elaborazione verifica fattibilitą"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="TPHDNISVFM",Visible=.t., Left=617, Top=35,;
    Alignment=1, Width=42, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="HQKLISBKAT",Visible=.t., Left=775, Top=35,;
    Alignment=1, Width=43, Height=15,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="FIPDMOMWTM",Visible=.t., Left=601, Top=58,;
    Alignment=1, Width=58, Height=15,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="ZDABOVSXET",Visible=.t., Left=671, Top=130,;
    Alignment=0, Width=123, Height=15,;
    Caption="Periodo elaborato"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="WHDCASGAFN",Visible=.t., Left=655, Top=152,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="AVIRFQUXDP",Visible=.t., Left=658, Top=177,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="WMANKQZAWF",Visible=.t., Left=595, Top=86,;
    Alignment=1, Width=64, Height=15,;
    Caption="Tipo ver.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="HEWHPMHQHT",Visible=.t., Left=40, Top=461,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="HSWVJBUDYK",Visible=.t., Left=11, Top=405,;
    Alignment=0, Width=70, Height=15,;
    Caption="Stato ODL"  ;
  , bGlobalFont=.t.

  add object oBox_1_48 as StdBox with uid="BQDUBJBORW",left=7, top=200, width=621,height=1

  add object oBox_1_49 as StdBox with uid="UPYCFJNTIK",left=15, top=26, width=531,height=1

  add object oBox_1_79 as StdBox with uid="TODCTBGFEP",left=566, top=26, width=317,height=1

  add object oBox_1_81 as StdBox with uid="HGOKTOZQEI",left=668, top=145, width=143,height=1

  add object oBox_1_90 as StdBox with uid="PUAEQXCCYG",left=-1, top=422, width=788,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_svf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
