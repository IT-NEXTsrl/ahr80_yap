* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco1kto                                                        *
*              Tracciabilit� ODA                                               *
*                                                                              *
*      Author: Zucchetti SPA cf                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-29                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco1kto",oParentObject))

* --- Class definition
define class tgsco1kto as StdForm
  Top    = 1
  Left   = 7

  * --- Standard Properties
  Width  = 822
  Height = 514+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-10"
  HelpContextID=182999703
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=84

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  BUSIUNIT_IDX = 0
  ODL_MAST_IDX = 0
  CAM_AGAZ_IDX = 0
  AZIENDA_IDX = 0
  KEY_ARTI_IDX = 0
  CONTI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  DOC_MAST_IDX = 0
  cPrg = "gsco1kto"
  cComment = "Tracciabilit� ODA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_CATDOC = space(2)
  o_CATDOC = space(2)
  w_TIPDOC = space(5)
  w_TIPMAG = space(5)
  w_NUMINI = 0
  w_SERIE1 = space(10)
  w_NUMFIN = 0
  w_SERIE2 = space(10)
  w_DOCINI = ctod('  /  /  ')
  o_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_NUMREI = 0
  w_NUMREF = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FORSEL = space(15)
  w_CODMAG = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_TIPOARTI = space(2)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_OLCODINI = space(15)
  w_OLCODFIN = space(15)
  w_DATODI = ctod('  /  /  ')
  w_DATODF = ctod('  /  /  ')
  w_DATFINE = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_SELOCL = space(1)
  w_DESCAN = space(30)
  w_DESATT = space(30)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESDISI = space(40)
  w_DESDISF = space(40)
  w_TIPATT = space(1)
  w_DESFOR = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_DESDOC = space(35)
  w_CODAZI = space(5)
  w_AZIENDA = space(5)
  w_gestbu = space(1)
  w_LEVELTMP = 0
  w_LEVEL = 0
  w_TMPTYPE = space(3)
  w_CLADOC = space(2)
  w_FLVEAC1 = space(1)
  w_ST = space(1)
  w_TYPE = space(3)
  w_STATO = space(10)
  w_CODODL = space(15)
  w_CODRIC = space(20)
  w_CADESAR = space(40)
  w_QTAORD = 0
  w_UNIMIS = space(3)
  w_FLEVAS = space(1)
  w_DATEVF = ctod('  /  /  ')
  w_QTAEVA = 0
  w_QTABKF = 0
  w_QTARES = 0
  w_MAGAZ = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_ROWNUM = 0
  w_CODODL2 = space(10)
  w_CODODL1 = space(10)
  w_ST1 = space(1)
  w_STATOR = space(15)
  w_DATEVA = ctod('  /  /  ')
  w_NUMREG = 0
  w_FORNIT = space(15)
  w_FirstTime = .F.
  w_TIPFOR = space(1)
  w_TIPGES = space(1)
  w_TREEV = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco1ktoPag1","gsco1kto",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsco1ktoPag2","gsco1kto",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("TreeView")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEV = this.oPgFrm.Pages(2).oPag.TREEV
    DoDefault()
    proc Destroy()
      this.w_TREEV = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='CAN_TIER'
    this.cWorkTables[10]='ATTIVITA'
    this.cWorkTables[11]='FAM_ARTI'
    this.cWorkTables[12]='GRUMERC'
    this.cWorkTables[13]='CATEGOMO'
    this.cWorkTables[14]='DOC_MAST'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      GSCO1BTO("Tree-View")
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_CATDOC=space(2)
      .w_TIPDOC=space(5)
      .w_TIPMAG=space(5)
      .w_NUMINI=0
      .w_SERIE1=space(10)
      .w_NUMFIN=0
      .w_SERIE2=space(10)
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_NUMREI=0
      .w_NUMREF=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FORSEL=space(15)
      .w_CODMAG=space(5)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_DBCODINI=space(20)
      .w_DBCODFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOARTI=space(2)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_OLCODINI=space(15)
      .w_OLCODFIN=space(15)
      .w_DATODI=ctod("  /  /  ")
      .w_DATODF=ctod("  /  /  ")
      .w_DATFINE=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_SELOCL=space(1)
      .w_DESCAN=space(30)
      .w_DESATT=space(30)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESDISI=space(40)
      .w_DESDISF=space(40)
      .w_TIPATT=space(1)
      .w_DESFOR=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESDOC=space(35)
      .w_CODAZI=space(5)
      .w_AZIENDA=space(5)
      .w_gestbu=space(1)
      .w_LEVELTMP=0
      .w_LEVEL=0
      .w_TMPTYPE=space(3)
      .w_CLADOC=space(2)
      .w_FLVEAC1=space(1)
      .w_ST=space(1)
      .w_TYPE=space(3)
      .w_STATO=space(10)
      .w_CODODL=space(15)
      .w_CODRIC=space(20)
      .w_CADESAR=space(40)
      .w_QTAORD=0
      .w_UNIMIS=space(3)
      .w_FLEVAS=space(1)
      .w_DATEVF=ctod("  /  /  ")
      .w_QTAEVA=0
      .w_QTABKF=0
      .w_QTARES=0
      .w_MAGAZ=space(5)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_ROWNUM=0
      .w_CODODL2=space(10)
      .w_CODODL1=space(10)
      .w_ST1=space(1)
      .w_STATOR=space(15)
      .w_DATEVA=ctod("  /  /  ")
      .w_NUMREG=0
      .w_FORNIT=space(15)
      .w_FirstTime=.f.
      .w_TIPFOR=space(1)
      .w_TIPGES=space(1)
        .w_FLVEAC = "A"
        .w_CATDOC = "XX"
        .w_TIPDOC = ' '
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_3('Full')
        endif
        .w_TIPMAG = ' '
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TIPMAG))
          .link_1_4('Full')
        endif
        .w_NUMINI = 1
        .w_SERIE1 = ''
        .w_NUMFIN = 999999999999999
        .w_SERIE2 = ''
          .DoRTCalc(9,9,.f.)
        .w_DOCFIN = .w_DOCINI
        .w_NUMREI = 1
        .w_NUMREF = 999999999999999
          .DoRTCalc(13,13,.f.)
        .w_DATFIN = .w_DATINI
        .w_FORSEL = ' '
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_FORSEL))
          .link_1_16('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODMAG))
          .link_1_17('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODCOM))
          .link_1_18('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODATT))
          .link_1_19('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_DBCODINI))
          .link_1_20('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_DBCODFIN))
          .link_1_21('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_22('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_23('Full')
        endif
        .w_OBTEST = i_DATSYS
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_GRUINI))
          .link_1_26('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_27('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CATINI))
          .link_1_28('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CATFIN))
          .link_1_29('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_OLCODINI))
          .link_1_30('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_OLCODFIN))
          .link_1_31('Full')
        endif
          .DoRTCalc(31,31,.f.)
        .w_DATODF = .w_DATINI
          .DoRTCalc(33,34,.f.)
        .w_SELOCL = "T"
          .DoRTCalc(36,45,.f.)
        .w_TIPATT = "A"
          .DoRTCalc(47,49,.f.)
        .w_CODAZI = i_codazi
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_76('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
        .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
        .w_TMPTYPE = .w_TREEV.getvar("TIPO")
        .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
        .w_FLVEAC1 = .w_TREEV.getvar("MVFLVEAC")
      .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
      .oPgFrm.Page2.oPag.TREEV.Calculate()
        .w_ST = .w_TREEV.getvar("OLTSTATO")
        .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
        .w_STATO = AH_MsgFormat(iif(.w_ST="M","Suggerito",iif(.w_ST="P","Pianificato",iif(.w_ST="L",iif(.w_Type="ODL","Lanciato","Ordinato"),iif(.w_ST="C","Confermato",iif(.w_ST="D","Da Pianificare",iif(.w_ST="S","Suggerito",iif(.w_ST="F","Finito",""))))))))
        .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
        .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_CODRIC))
          .link_2_12('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_16.Calculate(.w_STATO)
          .DoRTCalc(63,63,.f.)
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
        .w_FLEVAS = .w_TREEV.getvar("OLTFLEVA")
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .w_QTABKF = .w_TREEV.getvar("OLTQTOBF")
        .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) - nvl(.w_QTABKF,0),nvl(.w_QTABKF,0)),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
        .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
        .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
      .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
        .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_CODODL2))
          .link_2_37('Full')
        endif
        .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
          .DoRTCalc(77,77,.f.)
        .w_STATOR = iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' and .w_type<>'DPI','Confermato',''))
      .oPgFrm.Page2.oPag.oObj_2_41.Calculate(.w_STATOR)
      .oPgFrm.Page2.oPag.oObj_2_42.Calculate()
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_FirstTime = .T.
        .w_TIPFOR = 'F'
        .w_TIPGES = 'E'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_30.enabled = this.oPgFrm.Page2.oPag.oBtn_2_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_FLVEAC<>.w_FLVEAC.or. .o_CATDOC<>.w_CATDOC
            .w_TIPDOC = ' '
          .link_1_3('Full')
        endif
        if .o_CATDOC<>.w_CATDOC
            .w_TIPMAG = ' '
          .link_1_4('Full')
        endif
        .DoRTCalc(5,9,.t.)
        if .o_DOCINI<>.w_DOCINI
            .w_DOCFIN = .w_DOCINI
        endif
        .DoRTCalc(11,13,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = .w_DATINI
        endif
        if .o_FLVEAC<>.w_FLVEAC
            .w_FORSEL = ' '
          .link_1_16('Full')
        endif
        .DoRTCalc(16,31,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATODF = .w_DATINI
        endif
        .DoRTCalc(33,50,.t.)
          .link_1_76('Full')
        .DoRTCalc(52,52,.t.)
            .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
            .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
            .w_TMPTYPE = .w_TREEV.getvar("TIPO")
            .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
            .w_FLVEAC1 = .w_TREEV.getvar("MVFLVEAC")
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.TREEV.Calculate()
            .w_ST = .w_TREEV.getvar("OLTSTATO")
            .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
            .w_STATO = AH_MsgFormat(iif(.w_ST="M","Suggerito",iif(.w_ST="P","Pianificato",iif(.w_ST="L",iif(.w_Type="ODL","Lanciato","Ordinato"),iif(.w_ST="C","Confermato",iif(.w_ST="D","Da Pianificare",iif(.w_ST="S","Suggerito",iif(.w_ST="F","Finito",""))))))))
            .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
            .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
          .link_2_12('Full')
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate(.w_STATO)
        .DoRTCalc(63,63,.t.)
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
            .w_FLEVAS = .w_TREEV.getvar("OLTFLEVA")
            .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
            .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
            .w_QTABKF = .w_TREEV.getvar("OLTQTOBF")
            .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) - nvl(.w_QTABKF,0),nvl(.w_QTABKF,0)),0)
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
            .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
            .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
        .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
            .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
          .link_2_37('Full')
            .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
        .DoRTCalc(77,77,.t.)
            .w_STATOR = iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' and .w_type<>'DPI','Confermato',''))
        .oPgFrm.Page2.oPag.oObj_2_41.Calculate(.w_STATOR)
        .oPgFrm.Page2.oPag.oObj_2_42.Calculate()
            .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
            .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .DoRTCalc(82,82,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_TIPFOR = 'F'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(84,84,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.TREEV.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate(.w_STATO)
        .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_41.Calculate(.w_STATOR)
        .oPgFrm.Page2.oPag.oObj_2_42.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLVEAC_1_1.enabled = this.oPgFrm.Page1.oPag.oFLVEAC_1_1.mCond()
    this.oPgFrm.Page1.oPag.oCODCOM_1_18.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_19.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_3.visible=!this.oPgFrm.Page1.oPag.oTIPDOC_1_3.mHide()
    this.oPgFrm.Page1.oPag.oTIPMAG_1_4.visible=!this.oPgFrm.Page1.oPag.oTIPMAG_1_4.mHide()
    this.oPgFrm.Page1.oPag.oFORSEL_1_16.visible=!this.oPgFrm.Page1.oPag.oFORSEL_1_16.mHide()
    this.oPgFrm.Page2.oPag.oCODODL_2_11.visible=!this.oPgFrm.Page2.oPag.oCODODL_2_11.mHide()
    this.oPgFrm.Page2.oPag.oCODRIC_2_12.visible=!this.oPgFrm.Page2.oPag.oCODRIC_2_12.mHide()
    this.oPgFrm.Page2.oPag.oCADESAR_2_13.visible=!this.oPgFrm.Page2.oPag.oCADESAR_2_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    this.oPgFrm.Page2.oPag.oQTAORD_2_18.visible=!this.oPgFrm.Page2.oPag.oQTAORD_2_18.mHide()
    this.oPgFrm.Page2.oPag.oUNIMIS_2_19.visible=!this.oPgFrm.Page2.oPag.oUNIMIS_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oDATEVF_2_23.visible=!this.oPgFrm.Page2.oPag.oDATEVF_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page2.oPag.oQTAEVA_2_25.visible=!this.oPgFrm.Page2.oPag.oQTAEVA_2_25.mHide()
    this.oPgFrm.Page2.oPag.oQTARES_2_27.visible=!this.oPgFrm.Page2.oPag.oQTARES_2_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page2.oPag.oMAGAZ_2_29.visible=!this.oPgFrm.Page2.oPag.oMAGAZ_2_29.mHide()
    this.oPgFrm.Page2.oPag.oNUMDOC_2_31.visible=!this.oPgFrm.Page2.oPag.oNUMDOC_2_31.mHide()
    this.oPgFrm.Page2.oPag.oALFDOC_2_32.visible=!this.oPgFrm.Page2.oPag.oALFDOC_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_33.visible=!this.oPgFrm.Page2.oPag.oStr_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oDATEVA_2_44.visible=!this.oPgFrm.Page2.oPag.oDATEVA_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page2.oPag.oNUMREG_2_46.visible=!this.oPgFrm.Page2.oPag.oNUMREG_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oFORNIT_2_49.visible=!this.oPgFrm.Page2.oPag.oFORNIT_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page2.oPag.TREEV.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_16.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_36.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_41.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_42.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_3'),i_cWhere,'',"Causali documenti",'GSCL_ZTD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPMAG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_TIPMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_TIPMAG))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oTIPMAG_1_4'),i_cWhere,'',"Causali di magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_TIPMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_TIPMAG)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESDOC = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPMAG = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_1_16'),i_cWhere,'',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_17'),i_cWhere,'',"Magazzini",'GSDB_SCG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_18'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_19'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODINI
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODINI_1_20'),i_cWhere,'',"Codici di ricerca",'GSCL_zar.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODFIN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODFIN_1_21'),i_cWhere,'',"Codici di ricerca",'GSCL_zar.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_22'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_23'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_26'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_27'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_28'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_29'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODINI
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODINI_1_30'),i_cWhere,'',"ODA",'GSCL_zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODFIN
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODFIN_1_31'),i_cWhere,'',"ODA",'GSCL_zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZIENDA
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIC
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODRIC)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIC = NVL(_Link_.CACODICE,space(20))
      this.w_CADESAR = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIC = space(20)
      endif
      this.w_CADESAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODODL2
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODODL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODODL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_CODODL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_CODODL2)
            select MVSERIAL,MVFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODODL2 = NVL(_Link_.MVSERIAL,space(10))
      this.w_ST1 = NVL(_Link_.MVFLPROV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODODL2 = space(10)
      endif
      this.w_ST1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODODL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_1.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDOC_1_2.RadioValue()==this.w_CATDOC)
      this.oPgFrm.Page1.oPag.oCATDOC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_3.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_3.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMAG_1_4.value==this.w_TIPMAG)
      this.oPgFrm.Page1.oPag.oTIPMAG_1_4.value=this.w_TIPMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_5.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_5.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_6.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_6.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_7.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_7.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_8.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_8.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_10.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_10.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_11.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_11.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREI_1_12.value==this.w_NUMREI)
      this.oPgFrm.Page1.oPag.oNUMREI_1_12.value=this.w_NUMREI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREF_1_13.value==this.w_NUMREF)
      this.oPgFrm.Page1.oPag.oNUMREF_1_13.value=this.w_NUMREF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_14.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_14.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_15.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_15.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFORSEL_1_16.value==this.w_FORSEL)
      this.oPgFrm.Page1.oPag.oFORSEL_1_16.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_17.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_17.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_18.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_18.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_19.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_19.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODINI_1_20.value==this.w_DBCODINI)
      this.oPgFrm.Page1.oPag.oDBCODINI_1_20.value=this.w_DBCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODFIN_1_21.value==this.w_DBCODFIN)
      this.oPgFrm.Page1.oPag.oDBCODFIN_1_21.value=this.w_DBCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_22.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_22.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_23.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_23.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_26.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_26.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_27.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_27.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_28.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_28.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_29.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_29.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODINI_1_30.value==this.w_OLCODINI)
      this.oPgFrm.Page1.oPag.oOLCODINI_1_30.value=this.w_OLCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODFIN_1_31.value==this.w_OLCODFIN)
      this.oPgFrm.Page1.oPag.oOLCODFIN_1_31.value=this.w_OLCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATODI_1_32.value==this.w_DATODI)
      this.oPgFrm.Page1.oPag.oDATODI_1_32.value=this.w_DATODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATODF_1_33.value==this.w_DATODF)
      this.oPgFrm.Page1.oPag.oDATODF_1_33.value=this.w_DATODF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINE_1_34.value==this.w_DATFINE)
      this.oPgFrm.Page1.oPag.oDATFINE_1_34.value=this.w_DATFINE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_35.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_35.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oSELOCL_1_36.RadioValue()==this.w_SELOCL)
      this.oPgFrm.Page1.oPag.oSELOCL_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_43.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_43.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_45.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_45.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_46.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_46.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_47.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_47.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_48.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_48.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_52.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_52.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_53.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_53.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_54.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_54.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISI_1_58.value==this.w_DESDISI)
      this.oPgFrm.Page1.oPag.oDESDISI_1_58.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISF_1_59.value==this.w_DESDISF)
      this.oPgFrm.Page1.oPag.oDESDISF_1_59.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_66.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_66.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_69.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_69.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODODL_2_11.value==this.w_CODODL)
      this.oPgFrm.Page2.oPag.oCODODL_2_11.value=this.w_CODODL
    endif
    if not(this.oPgFrm.Page2.oPag.oCODRIC_2_12.value==this.w_CODRIC)
      this.oPgFrm.Page2.oPag.oCODRIC_2_12.value=this.w_CODRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oCADESAR_2_13.value==this.w_CADESAR)
      this.oPgFrm.Page2.oPag.oCADESAR_2_13.value=this.w_CADESAR
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAORD_2_18.value==this.w_QTAORD)
      this.oPgFrm.Page2.oPag.oQTAORD_2_18.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page2.oPag.oUNIMIS_2_19.value==this.w_UNIMIS)
      this.oPgFrm.Page2.oPag.oUNIMIS_2_19.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDATEVF_2_23.value==this.w_DATEVF)
      this.oPgFrm.Page2.oPag.oDATEVF_2_23.value=this.w_DATEVF
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAEVA_2_25.value==this.w_QTAEVA)
      this.oPgFrm.Page2.oPag.oQTAEVA_2_25.value=this.w_QTAEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oQTARES_2_27.value==this.w_QTARES)
      this.oPgFrm.Page2.oPag.oQTARES_2_27.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGAZ_2_29.value==this.w_MAGAZ)
      this.oPgFrm.Page2.oPag.oMAGAZ_2_29.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOC_2_31.value==this.w_NUMDOC)
      this.oPgFrm.Page2.oPag.oNUMDOC_2_31.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOC_2_32.value==this.w_ALFDOC)
      this.oPgFrm.Page2.oPag.oALFDOC_2_32.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDATEVA_2_44.value==this.w_DATEVA)
      this.oPgFrm.Page2.oPag.oDATEVA_2_44.value=this.w_DATEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMREG_2_46.value==this.w_NUMREG)
      this.oPgFrm.Page2.oPag.oNUMREG_2_46.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page2.oPag.oFORNIT_2_49.value==this.w_FORNIT)
      this.oPgFrm.Page2.oPag.oFORNIT_2_49.value=this.w_FORNIT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_numrei<=.w_numref)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMREI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not(.w_numrei<=.w_numref)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMREF_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(.w_TIPFOR='T')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORSEL_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN))  and not(empty(.w_DBCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODINI_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI))  and not(empty(.w_DBCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN))  and not(empty(.w_OLCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODINI_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI))  and not(empty(.w_OLCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODFIN_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATODI_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATODF_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_CATDOC = this.w_CATDOC
    this.o_DOCINI = this.w_DOCINI
    this.o_DATINI = this.w_DATINI
    return

enddefine

* --- Define pages as container
define class tgsco1ktoPag1 as StdContainer
  Width  = 818
  height = 514
  stdWidth  = 818
  stdheight = 514
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC_1_1 as StdCombo with uid="VNKCJROECU",rtseq=1,rtrep=.f.,left=109,top=27,width=74,height=21;
    , ToolTipText = "Selezione ciclo (vendite/acquisti)";
    , HelpContextID = 230487210;
    , cFormVar="w_FLVEAC",RowSource=""+"Acquisti,"+"Vendite,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_1.RadioValue()
    return(iif(this.value =1,"A",;
    iif(this.value =2,"V",;
    iif(this.value =3,"T",;
    space(1)))))
  endfunc
  func oFLVEAC_1_1.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_1.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=="A",1,;
      iif(this.Parent.oContained.w_FLVEAC=="V",2,;
      iif(this.Parent.oContained.w_FLVEAC=="T",3,;
      0)))
  endfunc

  func oFLVEAC_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=0)
    endwith
   endif
  endfunc


  add object oCATDOC_1_2 as StdCombo with uid="QUPBMCLARN",rtseq=2,rtrep=.f.,left=271,top=27,width=163,height=21;
    , ToolTipText = "Selezione categoria documento";
    , HelpContextID = 215883738;
    , cFormVar="w_CATDOC",RowSource=""+"Tutti,"+"Ordini,"+"Doc. di trasporto,"+"Fatture", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDOC_1_2.RadioValue()
    return(iif(this.value =1,"XX",;
    iif(this.value =2,"OR",;
    iif(this.value =3,"DT",;
    iif(this.value =4,"FA",;
    space(2))))))
  endfunc
  func oCATDOC_1_2.GetRadio()
    this.Parent.oContained.w_CATDOC = this.RadioValue()
    return .t.
  endfunc

  func oCATDOC_1_2.SetRadio()
    this.Parent.oContained.w_CATDOC=trim(this.Parent.oContained.w_CATDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CATDOC=="XX",1,;
      iif(this.Parent.oContained.w_CATDOC=="OR",2,;
      iif(this.Parent.oContained.w_CATDOC=="DT",3,;
      iif(this.Parent.oContained.w_CATDOC=="FA",4,;
      0))))
  endfunc

  add object oTIPDOC_1_3 as StdField with uid="FYFYYJENFJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento",;
    HelpContextID = 215897802,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=493, Top=27, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_3.mHide()
    with this.Parent.oContained
      return (.w_CATDOC='MA')
    endwith
  endfunc

  func oTIPDOC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSCL_ZTD.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oTIPMAG_1_4 as StdField with uid="GSVIPCDVAW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TIPMAG", cQueryName = "TIPMAG",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino",;
    HelpContextID = 162879178,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=493, Top=27, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", oKey_1_1="CMCODICE", oKey_1_2="this.w_TIPMAG"

  func oTIPMAG_1_4.mHide()
    with this.Parent.oContained
      return (.w_CATDOC<>'MA')
    endwith
  endfunc

  func oTIPMAG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPMAG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPMAG_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oTIPMAG_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali di magazzino",'',this.parent.oContained
  endproc

  add object oNUMINI_1_5 as StdField with uid="IKHMXRTVNM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 115964714,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=50, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_6 as StdField with uid="CPLEKOYGGO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 259603162,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=251, Top=50, InputMask=replicate('X',10)

  func oSERIE1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_7 as StdField with uid="ZQAMWANEZG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 37518122,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=73, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_8 as StdField with uid="DHTTXQQJBV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 242825946,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=251, Top=73, InputMask=replicate('X',10)

  func oSERIE2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oDOCINI_1_10 as StdField with uid="NLSHJRFBFX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 116007370,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=456, Top=50

  func oDOCINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_11 as StdField with uid="WKYDEMVJUZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 37560778,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=456, Top=73

  func oDOCFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oNUMREI_1_12 as StdField with uid="UCZIVCYRDA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMREI", cQueryName = "NUMREI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero registrazione iniziale selezionato",;
    HelpContextID = 124812074,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=95, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMREI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numrei<=.w_numref)
    endwith
    return bRes
  endfunc

  add object oNUMREF_1_13 as StdField with uid="AOTQBKOQWW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMREF", cQueryName = "NUMREF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero registrazione finale selezionato",;
    HelpContextID = 175143722,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=118, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMREF_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numrei<=.w_numref)
    endwith
    return bRes
  endfunc

  add object oDATINI_1_14 as StdField with uid="QMGDPWFMND",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 115941322,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=456, Top=95

  func oDATINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_15 as StdField with uid="VRSTSJQFDH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 37494730,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=456, Top=118

  func oDATFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oFORSEL_1_16 as StdField with uid="GRXGWWUYUW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 74396074,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=142, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPFOR='T')
    endwith
  endfunc

  func oFORSEL_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fornitori",'',this.parent.oContained
  endproc

  add object oCODMAG_1_17 as StdField with uid="WWAXPPIMPZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 162927066,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=715, Top=142, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'GSDB_SCG.MAGAZZIN_VZM',this.parent.oContained
  endproc

  add object oCODCOM_1_18 as StdField with uid="YLQVWTCUFO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 48239066,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=165, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_19('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_19 as StdField with uid="CMRHBCMAKU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 194122202,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=188, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" and not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oDBCODINI_1_20 as StdField with uid="EGVKVVQLEU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DBCODINI", cQueryName = "DBCODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 126103169,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=109, Top=228, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODINI"

  func oDBCODINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODINI_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODINI_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODINI_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCL_zar.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDBCODFIN_1_21 as StdField with uid="VCEEWLYQYH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DBCODFIN", cQueryName = "DBCODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 92000644,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=109, Top=251, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODFIN"

  proc oDBCODFIN_1_21.mDefault
    with this.Parent.oContained
      if empty(.w_DBCODFIN)
        .w_DBCODFIN = .w_DBCODINI
      endif
    endwith
  endproc

  func oDBCODFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODFIN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODFIN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODFIN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCL_zar.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_1_22 as StdField with uid="BAUZFWPTWC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 230584406,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=274, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_23 as StdField with uid="GHDDKOKVIG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 124882858,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=299, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_23.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_26 as StdField with uid="VYQYSTJFCH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 115932826,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=321, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_27 as StdField with uid="BKTBROMCMD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 37486234,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=345, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_27.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_28 as StdField with uid="WQNLFQEXDD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 115941338,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=368, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_29 as StdField with uid="WHQQEZYTLE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 37494746,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=393, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_29.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oOLCODINI_1_30 as StdField with uid="QBFYXJIXEG",rtseq=29,rtrep=.f.,;
    cFormVar = "w_OLCODINI", cQueryName = "OLCODINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ODA di inizio selezione",;
    HelpContextID = 126100433,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=435, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODINI"

  func oOLCODINI_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODINI_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODINI_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODINI_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ODA",'GSCL_zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oOLCODFIN_1_31 as StdField with uid="RYBOKFOTRV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_OLCODFIN", cQueryName = "OLCODFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ODA di fine selezione",;
    HelpContextID = 92003380,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=458, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODFIN"

  proc oOLCODFIN_1_31.mDefault
    with this.Parent.oContained
      if empty(.w_OLCODFIN)
        .w_OLCODFIN = .w_OLCODINI
      endif
    endwith
  endproc

  func oOLCODFIN_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODFIN_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODFIN_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODFIN_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ODA",'GSCL_zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oDATODI_1_32 as StdField with uid="JTIGDSWUON",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DATODI", cQueryName = "DATODI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ODA di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 126033866,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=382, Top=435

  func oDATODI_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATODF_1_33 as StdField with uid="HBIGVLINRF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DATODF", cQueryName = "DATODF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ODA di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 176365514,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=382, Top=458

  func oDATODF_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDATFINE_1_34 as StdField with uid="FAXQJUJEXU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DATFINE", cQueryName = "DATFINE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 230940726,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=584, Top=435

  add object oDATFIN1_1_35 as StdField with uid="QWARCSPHHS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 37494730,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=584, Top=458


  add object oSELOCL_1_36 as StdCombo with uid="HACJPEBNXC",rtseq=35,rtrep=.f.,left=109,top=485,width=78,height=21;
    , ToolTipText = "Selezione stato ODA";
    , HelpContextID = 76782298;
    , cFormVar="w_SELOCL",RowSource=""+"Ordinati,"+"Finiti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELOCL_1_36.RadioValue()
    return(iif(this.value =1,"L",;
    iif(this.value =2,"F",;
    iif(this.value =3,"T",;
    space(1)))))
  endfunc
  func oSELOCL_1_36.GetRadio()
    this.Parent.oContained.w_SELOCL = this.RadioValue()
    return .t.
  endfunc

  func oSELOCL_1_36.SetRadio()
    this.Parent.oContained.w_SELOCL=trim(this.Parent.oContained.w_SELOCL)
    this.value = ;
      iif(this.Parent.oContained.w_SELOCL=="L",1,;
      iif(this.Parent.oContained.w_SELOCL=="F",2,;
      iif(this.Parent.oContained.w_SELOCL=="T",3,;
      0)))
  endfunc


  add object oBtn_1_37 as StdButton with uid="WBDQHNKONY",left=688, top=463, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 262925290;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSCO1BTO(this.Parent.oContained,"Tree-View")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="MWXKLVEWEK",left=739, top=463, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 262925290;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAN_1_43 as StdField with uid="PJHERXXTYJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 46083018,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=243, Top=165, InputMask=replicate('X',30)

  add object oDESATT_1_45 as StdField with uid="JRUPUPZCWV",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 194063306,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=243, Top=188, InputMask=replicate('X',30)

  add object oDESFAMAI_1_46 as StdField with uid="AOYBYAMKXI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 62663553,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=174, Top=274, InputMask=replicate('X',35)

  add object oDESGRUI_1_47 as StdField with uid="ZABORREMDH",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89445430,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=174, Top=321, InputMask=replicate('X',35)

  add object oDESCATI_1_48 as StdField with uid="ZFWYARIWSP",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 54580278,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=174, Top=368, InputMask=replicate('X',35)

  add object oDESFAMAF_1_52 as StdField with uid="WOCDSQXKSA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 62663556,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=174, Top=299, InputMask=replicate('X',35)

  add object oDESGRUF_1_53 as StdField with uid="PYGQUHDJMJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89445430,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=174, Top=345, InputMask=replicate('X',35)

  add object oDESCATF_1_54 as StdField with uid="SXSGGSHCII",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 54580278,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=174, Top=393, InputMask=replicate('X',35)

  add object oDESDISI_1_58 as StdField with uid="QSEMNVVIFP",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 46257206,;
   bGlobalFont=.t.,;
    Height=21, Width=382, Left=286, Top=228, InputMask=replicate('X',40)

  add object oDESDISF_1_59 as StdField with uid="BLVZKOKGVG",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 46257206,;
   bGlobalFont=.t.,;
    Height=21, Width=382, Left=286, Top=251, InputMask=replicate('X',40)

  add object oDESFOR_1_66 as StdField with uid="RJLIHNJXRO",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 232532938,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=243, Top=142, InputMask=replicate('X',40)

  add object oDESDOC_1_69 as StdField with uid="RMROTHRADO",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 215886794,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=558, Top=27, InputMask=replicate('X',35)

  add object oStr_1_9 as StdString with uid="CGEEJWMTRL",Visible=.t., Left=244, Top=73,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="XRXCRISBDD",Visible=.t., Left=445, Top=27,;
    Alignment=1, Width=45, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="OLHXGJTWFV",Visible=.t., Left=44, Top=230,;
    Alignment=1, Width=64, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="IEHZXMOABQ",Visible=.t., Left=53, Top=253,;
    Alignment=1, Width=55, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=39, Top=165,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=55, Top=188,;
    Alignment=1, Width=53, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=38, Top=276,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=25, Top=323,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=18, Top=370,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="BETNLUNOYI",Visible=.t., Left=38, Top=301,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=21, Top=347,;
    Alignment=1, Width=87, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=28, Top=395,;
    Alignment=1, Width=80, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="ILMDRJPSBA",Visible=.t., Left=4, Top=5,;
    Alignment=0, Width=152, Height=18,;
    Caption="Selezioni documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="PEISJRCHUW",Visible=.t., Left=4, Top=207,;
    Alignment=0, Width=152, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="UZFXVUFHLH",Visible=.t., Left=37, Top=144,;
    Alignment=1, Width=71, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="DLAOKFKNHU",Visible=.t., Left=63, Top=27,;
    Alignment=1, Width=45, Height=15,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="HRQFQNITXV",Visible=.t., Left=366, Top=98,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="DNNAWKFMZY",Visible=.t., Left=380, Top=121,;
    Alignment=1, Width=74, Height=15,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="PYKVJJBJUN",Visible=.t., Left=366, Top=53,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="BGHQBDDCZG",Visible=.t., Left=381, Top=76,;
    Alignment=1, Width=73, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=647, Top=144,;
    Alignment=1, Width=66, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="BKVUFSLWMR",Visible=.t., Left=16, Top=435,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da codice ODA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="HGANHIHTKB",Visible=.t., Left=18, Top=458,;
    Alignment=1, Width=90, Height=15,;
    Caption="A codice ODA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="VQZWRBIBXU",Visible=.t., Left=13, Top=50,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="PMVZWORABY",Visible=.t., Left=22, Top=73,;
    Alignment=1, Width=86, Height=15,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="UKWJKYAMUK",Visible=.t., Left=244, Top=50,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="NGYZDXGMQY",Visible=.t., Left=209, Top=27,;
    Alignment=1, Width=57, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="YGQSYFZMBP",Visible=.t., Left=4, Top=413,;
    Alignment=0, Width=152, Height=18,;
    Caption="Selezioni ODA"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="MKNJKQCTUA",Visible=.t., Left=291, Top=435,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="SLVPJSANLN",Visible=.t., Left=302, Top=458,;
    Alignment=1, Width=77, Height=15,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="GMGBVESAYI",Visible=.t., Left=36, Top=485,;
    Alignment=1, Width=72, Height=15,;
    Caption="Stato ODA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="LSCPMXLBTD",Visible=.t., Left=3, Top=95,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da numero reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="FXGQBUAUJZ",Visible=.t., Left=21, Top=118,;
    Alignment=1, Width=87, Height=15,;
    Caption="A numero reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="DTJYHAGEOT",Visible=.t., Left=463, Top=435,;
    Alignment=1, Width=116, Height=15,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="LKUZJNOGIO",Visible=.t., Left=463, Top=458,;
    Alignment=1, Width=116, Height=15,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oBox_1_62 as StdBox with uid="OTTRKDEJWA",left=2, top=22, width=814,height=1

  add object oBox_1_64 as StdBox with uid="BQDUBJBORW",left=3, top=224, width=790,height=1

  add object oBox_1_85 as StdBox with uid="UPYCFJNTIK",left=4, top=430, width=814,height=1
enddefine
define class tgsco1ktoPag2 as StdContainer
  Width  = 818
  height = 514
  stdWidth  = 818
  stdheight = 514
  resizeXpos=456
  resizeYpos=259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_6 as cp_runprogram with uid="EBVGCFPGTX",left=5, top=551, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO1BTO('Page2')",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 175873562


  add object TREEV as cp_Treeview with uid="QTFEDDFLDB",left=0, top=4, width=817,height=363,;
    caption='TREEV',;
   bGlobalFont=.t.,;
    cCursor="_tree_",cShowFields="DESCRI",cNodeShowField="",cLeafShowField="",cNodeBmp="odll.bmp",cMenuFile="GSCO1KTO",bNoContextMenu=.f.,cLeafBmp="",nIndent=0,;
    cEvent = "updtreev",;
    nPag=2;
    , HelpContextID = 9568822

  add object oCODODL_2_11 as StdField with uid="EFOBYYZJFF",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CODODL", cQueryName = "CODODL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 75764186,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=65, Top=381, InputMask=replicate('X',15)

  func oCODODL_2_11.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>1)
    endwith
  endfunc

  add object oCODRIC_2_12 as StdField with uid="VXOAYLFGWV",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CODRIC", cQueryName = "CODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 221319642,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=65, Top=439, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODRIC"

  func oCODRIC_2_12.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  func oCODRIC_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCADESAR_2_13 as StdField with uid="RXUHOYJINC",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CADESAR", cQueryName = "CADESAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 245243866,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=236, Top=439, InputMask=replicate('X',40)

  func oCADESAR_2_13.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc


  add object oObj_2_16 as cp_calclbl with uid="AYCVNYJAJD",left=208, top=381, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , ToolTipText = "Stato ODL";
    , HelpContextID = 175873562

  add object oQTAORD_2_18 as StdField with uid="ZJAXUJZHTF",rtseq=64,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 195312634,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=350, Top=410, cSayPict="V_PQ(14)"

  func oQTAORD_2_18.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oUNIMIS_2_19 as StdField with uid="COHMDEMJGX",rtseq=65,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 221626810,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=234, Top=410, InputMask=replicate('X',3)

  func oUNIMIS_2_19.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oDATEVF_2_23 as StdField with uid="GYYMZDDJAC",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DATEVF", cQueryName = "DATEVF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 158146506,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=742, Top=381

  func oDATEVF_2_23.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oQTAEVA_2_25 as StdField with uid="BQYZGBVNCU",rtseq=68,rtrep=.f.,;
    cFormVar = "w_QTAEVA", cQueryName = "QTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 242105338,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=522, Top=410, cSayPict="V_PQ(14)"

  func oQTAEVA_2_25.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV')
    endwith
  endfunc

  add object oQTARES_2_27 as StdField with uid="TRUDNJFLPI",rtseq=70,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 225524730,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=704, Top=410, cSayPict="V_PQ(14)"

  func oQTARES_2_27.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV')
    endwith
  endfunc

  add object oMAGAZ_2_29 as StdField with uid="ULHQATTNMM",rtseq=71,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 13504710,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=767, Top=439, InputMask=replicate('X',5)

  func oMAGAZ_2_29.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc


  add object oBtn_2_30 as StdButton with uid="WZCLYAIBJF",left=738, top=464, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 262925290;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMDOC_2_31 as StdField with uid="LGLFEGUFHL",rtseq=72,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 215907114,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=65, Top=381, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMDOC_2_31.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or .w_TYPE='DPI')
    endwith
  endfunc

  add object oALFDOC_2_32 as StdField with uid="DJPOAMSRPN",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 215938298,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=207, Top=381, InputMask=replicate('X',10)

  func oALFDOC_2_32.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or empty(.w_ALFDOC) or .w_TYPE='DPI')
    endwith
  endfunc


  add object oObj_2_36 as cp_runprogram with uid="PFRWIUKVRB",left=4, top=585, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO1BTO('EXPNODE')",;
    cEvent = "w_treev Expanded",;
    nPag=2;
    , HelpContextID = 175873562


  add object oObj_2_41 as cp_calclbl with uid="EHWMUAAYRY",left=611, top=441, width=89,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , ToolTipText = "Statodocumento";
    , HelpContextID = 175873562


  add object oObj_2_42 as cp_runprogram with uid="GHTXBKLGSI",left=852, top=56, width=72,height=22,;
    caption='GSCO1BTO',;
   bGlobalFont=.t.,;
    prg="GSCO1BTO('Done')",;
    cEvent = "Done",;
    nPag=2;
    , HelpContextID = 263462219

  add object oDATEVA_2_44 as StdField with uid="RXYJBPTCQG",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DATEVA", cQueryName = "DATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 242032586,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=574, Top=381

  func oDATEVA_2_44.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oNUMREG_2_46 as StdField with uid="DTEXZQEJRP",rtseq=80,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 158366506,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=369, Top=381, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMREG_2_46.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or .w_TYPE='DPI')
    endwith
  endfunc

  add object oFORNIT_2_49 as StdField with uid="ENPHLDRIEX",rtseq=81,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 204747178,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=65, Top=410, InputMask=replicate('X',15)

  func oFORNIT_2_49.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_14 as StdString with uid="OHCFHJNCKJ",Visible=.t., Left=17, Top=441,;
    Alignment=1, Width=45, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_15 as StdString with uid="TYMQXOFOZF",Visible=.t., Left=5, Top=384,;
    Alignment=1, Width=57, Height=15,;
    Caption="ODA:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>1)
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="FAADZNHFCC",Visible=.t., Left=204, Top=412,;
    Alignment=1, Width=27, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="MAWHPOBZVA",Visible=.t., Left=276, Top=412,;
    Alignment=1, Width=71, Height=15,;
    Caption="Ordinato:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="VYBTMGIRDF",Visible=.t., Left=655, Top=381,;
    Alignment=1, Width=88, Height=15,;
    Caption="Evasione effet.:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF')
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="SDSTTXINGH",Visible=.t., Left=464, Top=412,;
    Alignment=1, Width=56, Height=15,;
    Caption="Evaso:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV')
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="LSEEDUNPIB",Visible=.t., Left=724, Top=441,;
    Alignment=1, Width=43, Height=15,;
    Caption="Mag.:"  ;
  , bGlobalFont=.t.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="MADMCVWCKJ",Visible=.t., Left=201, Top=384,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_33.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or empty(.w_ALFDOC) or .w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="FEOVYMXXOC",Visible=.t., Left=4, Top=384,;
    Alignment=1, Width=58, Height=15,;
    Caption="Num.doc.:"  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or .w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="ZREGTQDKVC",Visible=.t., Left=487, Top=381,;
    Alignment=1, Width=85, Height=15,;
    Caption="Evasione prev.:"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="QOOJGWSKSX",Visible=.t., Left=640, Top=412,;
    Alignment=1, Width=61, Height=15,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV')
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="XZIUPBEILQ",Visible=.t., Left=308, Top=383,;
    Alignment=1, Width=59, Height=15,;
    Caption="Num.reg.:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2  or .w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="RNLXLDMWCD",Visible=.t., Left=2, Top=412,;
    Alignment=1, Width=60, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI')
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="TFVVWXLSZD",Visible=.t., Left=675, Top=381,;
    Alignment=1, Width=68, Height=15,;
    Caption="Data reg.:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODL-OCL-OCF-DPI')
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="MROTAHJPCA",Visible=.t., Left=499, Top=381,;
    Alignment=1, Width=73, Height=15,;
    Caption="Data doc.:"  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODL-OCL-OCF-DPI')
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="YRUYCTEQFN",Visible=.t., Left=297, Top=410,;
    Alignment=1, Width=50, Height=15,;
    Caption="Q.t�:"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODL-OCL-OCF-DPI')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="MGSFUGFRKX",Visible=.t., Left=551, Top=441,;
    Alignment=1, Width=54, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2  or .w_TYPE='DPI')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco1kto','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
