* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bfl                                                        *
*              Stampa lasi di lavorazione                                      *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_78]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2017-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bfl",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_bfl as StdBatch
  * --- Local variables
  pAzione = space(1)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_OLCODODL = space(10)
  w_CODICEODL = space(10)
  w_FASE = 0
  w_CICDES = space(0)
  w_MultiRep = .NULL.
  w_FIRSTCUR = .f.
  w_TMPCUR = space(10)
  w_MultiRep = .NULL.
  w_MultiReportVarEnv = .NULL.
  * --- WorkFile variables
  TMPMODL_idx=0
  ODL_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Fasi di Lavorazione (da GSCO_SFL)
    this.w_FIRSTCUR = .T.
    this.w_TMPCUR = SYS(2015)
    * --- Punta al padre
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom ordini
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione = "SS"
        if used(this.NC)
          * --- Seleziona tutte le righe dello zoom
          UPDATE (this.NC) SET xChk=iif(this.oParentObject.w_SELEZI="S",1,0)
        endif
      case this.pAzione = "PR"
        * --- Create temporary table TMPMODL
        i_nIdx=cp_AddTableDef('TMPMODL') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.ODL_MAST_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMPMODL_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Controlla selezioni
        Select olcododl from (this.NC) Where xchk=1 into cursor CursSel nofilter
        if RecCount("CursSel")>0
          SELECT("CursSel")
          GO TOP
          do while Not Eof("CursSel")
            * --- Try
            local bErr_03381688
            bErr_03381688=bTrsErr
            this.Try_03381688()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03381688
            * --- End
            SELECT("CursSel")
            SKIP
          enddo
          USE IN SELECT(this.w_TMPCUR)
          VQ_EXEC( ALLTRIM(this.oParentObject.w_OQRY), this.Padre, this.w_TMPCUR)
          * --- Drop temporary table TMPMODL
          i_nIdx=cp_GetTableDefIdx('TMPMODL')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPMODL')
          endif
          * --- Parametri di stampa
          L_CODREP = this.oParentObject.w_CODREP
          L_ODES = Alltrim(this.oParentObject.w_ODES)
          this.w_MultiRep=createobject("Multireport")
          this.w_MultiReportVarEnv=createobject("ReportEnvironment")
          this.w_MultiReportVarEnv.AddVariable("L_CODREP" , L_CODREP)     
          this.w_MultiReportVarEnv.AddVariable("L_ODES" , L_ODES)     
          this.w_MultiRep.AddNewReport(this.oParentObject.w_OREP , this.Padre.Caption , this.w_TMPCUR, .null. , this.w_MultiReportVarEnv , .t., this.w_FIRSTCUR , .f.,"","","",1, .t.)     
          CP_CHPRN(this.w_MultiRep)
          USE IN SELECT(this.w_TMPCUR)
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare","!","")
        endif
    endcase
    use in select("CursSel")
  endproc
  proc Try_03381688()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPMODL
    i_nConn=i_TableProp[this.TMPMODL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMODL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPMODL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursSel.OLCODODL),'TMPMODL','OLCODODL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',CursSel.OLCODODL)
      insert into (i_cTable) (OLCODODL &i_ccchkf. );
         values (;
           CursSel.OLCODODL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPMODL'
    this.cWorkTables[2]='ODL_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
