* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bpv                                                        *
*              Previsioni di vendita                                           *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-04                                                      *
* Last revis.: 2017-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bpv",oParentObject,m.w_Azione)
return(i_retval)

define class tgsdb_bpv as StdBatch
  * --- Local variables
  w_Azione = space(1)
  w_nrec = 0
  w_DIMFONT = 0
  w_NUMCOL = 0
  w_pvcodart = space(40)
  w_PUNPADRE = .NULL.
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  cCursor = space(10)
  w_GRID = .NULL.
  w_PAR_CODI = space(20)
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_ANNO = 0
  w_PVANNO = 0
  w_STATO = space(10)
  w_oERRORLOG = .NULL.
  w_CUREX = space(20)
  w_CUREX2 = space(20)
  w_FILEIM = space(254)
  w_DIRIM = space(254)
  nConn = 0
  bErr = .f.
  okColumn = .f.
  tmpVar = space(10)
  w_NCICLO = 0
  w_i = 0
  w_nc = 0
  w_NUMEL = 0
  w_STATEMENT = space(100)
  w_INSERT = space(100)
  w_ELEMENT = space(20)
  w_CODART = space(20)
  w_QTAIMP = 0
  w_PVPERIOD = space(1)
  w_PVPERMES = 0
  w_PVPERSET = 0
  w_PV__ANNO = 0
  w_PV__DATA = ctod("  /  /  ")
  w_CURDATE = ctod("  /  /  ")
  w_CURDAY = 0
  w_CURMONTH = 0
  w_CURYEAR = 0
  w_MESSERR = space(100)
  w_ARTIPART = space(2)
  w_CODVAR = space(20)
  w_KEYSAL = space(40)
  w_NREC = 0
  w_LREC = 0
  w_COMPART = space(20)
  w_COMPVAR = space(20)
  w_COMPCOE = 0
  w_COMPKEYSAL = space(40)
  w_COMPQTA = 0
  w_OGGMPS = space(1)
  w_PROPRE = space(0)
  w_ARCODART = space(20)
  w_RECERR = 0
  w_TIPGES = space(1)
  * --- WorkFile variables
  PAR_PROD_idx=0
  VEN_PREV_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola le previsioni di vendita (da GSDB_KPV)
    this.w_PUNPADRE = this.oParentObject
    this.cCursor = this.w_PUNPADRE.w_Zoom.cCursor
    this.w_GRID = this.w_PUNPADRE.w_Zoom.GRD
    this.w_PAR_CODI = this.w_PUNPADRE.w_PAR_CODI
    this.w_CODINI = this.w_PUNPADRE.w_CODINI
    this.w_CODFIN = this.w_PUNPADRE.w_CODFIN
    this.w_ANNO = this.w_PUNPADRE.w_ANNO
    this.w_PVANNO = this.w_ANNO
    this.w_STATO = this.w_Azione
    * --- Utilizzata da GSDB_KPM
    do case
      case this.w_Azione $ "Query Load"
        do GSDB_KPM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_Azione = "MENU" and this.w_GRID.activecolumn<3
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_Azione $ "IR"
        * --- Imposta l'aspetto dello Zoom
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPFONMPS"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPFONMPS;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DIMFONT = NVL(cp_ToDate(_read_.PPFONMPS),cp_NullValue(_read_.PPFONMPS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_GRID.FontSize = this.w_DIMFONT
        this.w_GRID.column1.width = 120
        this.w_NUMCOL = 2
        do while this.w_NUMCOL<=13
          L_NUMCOL = ALLTRIM(STR(this.w_NUMCOL,2,0))
          this.w_GRID.column&l_numcol..width = 69
          this.w_NUMCOL = this.w_NUMCOL + 1
        enddo
        this.w_GRID.column14.width = 82
        this.w_GRID.column15.width = 107
        this.w_GRID.Refresh()     
      case this.w_Azione="Z"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_Azione="IMP"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if used("Previsioni")
      Use in Previsioni
    endif
    if used("Listino")
      Use in Listino
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce SHORTCUT
    store space(10) to Scelta
    * --- Definisce Menu
    DEFINE POPUP MENUPV from MRow()+1,MCol()+1 shortcut margin
    DEFINE BAR 1 OF MENUPV prompt AH_MsgFormat("Visualizza")
    DEFINE BAR 2 OF MENUPV prompt AH_MsgFormat("Elimina")
    DEFINE BAR 3 OF MENUPV prompt AH_MsgFormat("Stampa")
    ON SELE BAR 1 OF MENUPV Scelta="VISUALIZZA"
    ON SELE BAR 2 OF MENUPV Scelta="ELIMINA"
    ON SELE BAR 3 OF MENUPV Scelta="STAMPA"
    ACTI POPUP MENUPV
    DEACTIVATE POPUP MENUPV
    RELEASE POPUPS MENUPV EXTENDED
    do case
      case Scelta = "STAMPA"
        this.w_CODINI = this.w_PAR_CODI
        this.w_CODFIN = this.w_PAR_CODI
        this.w_DATINI = cp_CharToDate("01-01-"+alltrim(str(this.w_ANNO)))
        this.w_DATFIN = cp_CharToDate("31-12-"+alltrim(str(this.w_ANNO)))
        vx_exec("..\COLA\EXE\QUERY\GSDB_SPM.VQR",this)
      case Scelta = "VISUALIZZA"
        this.w_PAR_CODI = padr(alltrim(this.w_PAR_CODI) + iif(empty(w_PAR_VARI), "", "#") + alltrim(w_PAR_VARI), 41)
        do GSDB_KPM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case Scelta = "ELIMINA"
        if not ah_YesNo("Confermi cancellazione?")
          i_retcode = 'stop'
          return
        endif
        * --- Delete from VEN_PREV
        i_nConn=i_TableProp[this.VEN_PREV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PVCODART = "+cp_ToStrODBC(this.w_PAR_CODI);
                +" and PV__ANNO = "+cp_ToStrODBC(this.w_PVANNO);
                 )
        else
          delete from (i_cTable) where;
                PVCODART = this.w_PAR_CODI;
                and PV__ANNO = this.w_PVANNO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la query dello zoom
    Select (this.cCursor)
    zap
    * --- Controlla se ci sono delle previsioni: se il cursore della query � vuoto il Pivot da errore!
    vq_exec("..\COLA\EXE\QUERY\GSDB1BPV",this,"Previsioni")
    this.w_nrec = Previsioni.nrec
    if this.w_nrec>0
      * --- Cursore non vuoto
      * --- Query con PIVOT delle previsioni
      vq_exec("..\COLA\EXE\QUERY\GSDB_BPV",this,"Previsioni")
      if not empty(this.oParentObject.w_Listino)
        * --- Prezzi di listino degli articoli
        vq_exec("..\COLA\EXE\QUERY\GSDB2BPV",this,"Listino")
      endif
      * --- Un cursore "PIVOT" deve essere inserito nello zoom
      Select Previsioni
      SCAN
      scatter memvar
      pvcodart = left(pvcodart,20)
      pvquanti_tot = iif(type("pvquanti_1")="U",0,pvquanti_1) + iif(type("pvquanti_2")="U",0,pvquanti_2) ;
      + iif(type("pvquanti_3")="U",0,pvquanti_3) + iif(type("pvquanti_4")="U",0,pvquanti_4) ;
      + iif(type("pvquanti_5")="U",0,pvquanti_5) + iif(type("pvquanti_6")="U",0,pvquanti_6) ;
      + iif(type("pvquanti_7")="U",0,pvquanti_7) + iif(type("pvquanti_8")="U",0,pvquanti_8) ;
      + iif(type("pvquanti_9")="U",0,pvquanti_9) + iif(type("pvquanti_10")="U",0,pvquanti_10) ;
      + iif(type("pvquanti_11")="U",0,pvquanti_11) + iif(type("pvquanti_12")="U",0,pvquanti_12)
      pvquanti_lis = 0
      if not empty(this.oParentObject.w_Listino)
        this.w_pvcodart = pvcodart
        Select Listino
        Locate for Listino.pvcodart = this.w_pvcodart and nvl(liquanti,0)=0 or pvquanti_tot<=liquanti
        if found()
          pvquanti_lis = pvquanti_tot*listino.laprezzo
        endif
      endif
      Select (this.cCursor)
      append blank
      gather memvar
      ENDSCAN
    endif
    this.w_PUNPADRE.LockScreen = .t.
    this.w_GRID.Refresh()     
    this.w_PUNPADRE.LockScreen = .f.
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORTAZIONE DA FOGLIO MSExcel/OpenOffice
    *     -----------------------------------------------------------------------------------
    *     Struttura del foglio:
    *     - Previsioni mensili:
    *             CODART:codice articolo (oggetto MPS/Padre di famiglia
    *             MXXYYYY: 
    Dimension ColNames[1096]
    if g_OFFICE="M"
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      this.w_CUREX = sys(2015)
      this.w_CUREX2 = sys(2015)
      this.w_FILEIM = getfile("xls*","Nome file:","Apri",0,"Apri")
      if empty(this.w_FILEIM)
        AH_ERRORMSG("Il file %1 non esiste",48,"",this.w_FILEIM)
        i_retcode = 'stop'
        return
      endif
      this.w_DIRIM = SUBSTR(this.w_FILEIM,1,RAT(CHR(92),this.w_FILEIM)-1)
      * --- Creo la Connessione al Foglio Excel...
      if TYPE ("g_EXCELSTRINGCONN")="C" AND not empty (g_EXCELSTRINGCONN)
        this.nConn = SQLSTRINGCONNECT( STRTRAN ( STRTRAN(g_EXCELSTRINGCONN,"<NOMEFILE>", this.w_FILEIM ), "<NOMEDIR>", this.w_DIRIM ) )
      else
        if upper(right(alltrim(this.w_FILEIM),4)) == "XLSX"
          this.nConn = SQLSTRINGCONNECT("Driver=Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb);DBQ="+this.w_FILEIM+";DefaultDir="+this.w_DIRIM+";")
        else
          this.nConn = SQLSTRINGCONNECT("Driver=Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb);DBQ="+this.w_FILEIM+";DefaultDir="+this.w_DIRIM+";DriverId=790;FIL=excel 8.0;MaxBufferSize=2048;PageTimeout=5;")
        endif
      endif
      err = SQLEXEC(this.nConn, "select * from [Foglio1$]" , this.w_CUREX )
      if err = -1
        AH_ERRORMSG(MESSAGE(),48)
        i_retcode = 'stop'
        return
      endif
      * --- Chiusura della connessione al foglio Excel
      SQLCANCEL(this.nConn)
      SQLDISCONNECT(this.nConn)
    else
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if USED(this.w_CUREX)
      if reccount(this.w_CUREX)>0
        if g_OFFICE="M"
          * --- I controlli se importo da OpenOffice li ho gi� effettuati a pag.5
          this.bErr = .f.
          this.okColumn = .f.
          this.w_NUMEL = afields(farr,this.w_CUREX)
          * --- L'intestazione della prima colonna deve essere 'CODART'
          if farr(1, 1)<>"CODART"
            this.bErr = .t.
          else
            STORE "CODART" TO ColNames(1)
            this.w_i = 2
            do while this.w_i <= this.w_NUMEL and !this.bErr
              this.w_ELEMENT = alltrim(farr(this.w_i,1))
              this.okColumn = .f.
              if empty(this.w_ELEMENT)
                if this.w_i > 2
                  * --- Non ho pi� colonne 'piene' ed ho almeno una previsione caricata
                  exit
                else
                  * --- Ho la sola colonna 'CODART' senza previsioni...
                  this.bErr = .t.
                  exit
                endif
              else
                do case
                  case left(this.w_ELEMENT,1) == "M"
                    if len(this.w_ELEMENT) = 7 and val(substr(this.w_ELEMENT,2,2)) > 0 and val(substr(this.w_ELEMENT,2,2)) <= 12 and val(substr(this.w_ELEMENT,4,4)) >= 0
                      this.okColumn = .t.
                    endif
                  case left(this.w_ELEMENT,1) == "S"
                    if len(this.w_ELEMENT) = 7 and val(substr(this.w_ELEMENT,2,2)) > 0 and val(substr(this.w_ELEMENT,2,2)) <= 54 and val(substr(this.w_ELEMENT,4,4)) >= 0
                      this.okColumn = .t.
                    endif
                  case left(this.w_ELEMENT,1) == "G"
                    if len(this.w_ELEMENT) = 9 and val(right(this.w_ELEMENT,8)) > 0 
                      this.okColumn = .t.
                    endif
                endcase
              endif
              if this.okColumn
                STORE this.w_ELEMENT TO ColNames(this.w_i)
              endif
              this.bErr = !this.okColumn
              this.w_i = this.w_i + 1
            enddo
          endif
          if this.bErr
            * --- La struttura del foglio non ripecchia quella richiesta
            this.w_oERRORLOG.AddMsgLog("Il foglio excel deve rispettare la seguente struttura:")     
            this.w_oERRORLOG.AddMsgLog("   - La prima colonna deve essere CODART.")     
            this.w_oERRORLOG.AddMsgLog("   - Le colonne successive possono essere composte nel modo seguente:")     
            this.w_oERRORLOG.AddMsgLog("   ----  Formato 'MXXYYYY': indica che la previone deve essere inserita nel mese XX dell'anno YYYY")     
            this.w_oERRORLOG.AddMsgLog("   ----  Formato 'SXXYYYY': indica che la previone deve essere inserita nel XXesima settimana dell'anno YYYY")     
            this.w_oERRORLOG.AddMsgLog("   ----  Formato 'GDDMMYYYY': indica che la previone deve essere inserita nel giorno DD/MM/YYYY")     
          endif
        endif
        if !this.bErr
          * --- Non ci sono errori, procedo con l'importazione
          if g_OFFICE="M"
            * --- I campi testo vengono 'convertiti' in memo dalla select sul foglio excel...
            Select cast(CODART as char(20)) as CODART from (this.w_CUREX) into cursor (this.w_CUREX2)
          endif
          this.w_CURDATE = i_DATSYS
          this.w_CURDAY = day(this.w_CURDATE)
          this.w_CURMONTH = month(this.w_CURDATE)
          this.w_CURYEAR = year(this.w_CURDATE)
          Select (this.w_CUREX) 
 GO TOP 
 SCAN
          this.w_LREC = recno()
          this.w_RECERR = 0
          this.w_ARCODART = NVL(CODART,"")
          this.w_i = iif(g_OFFICE="M", 2, 1)
          do while TYPE("ColNames(this.w_i)")=="C" and !EMPTY(ColNames(this.w_i))
            theColumn=ColNames(this.w_i)
            Select (this.w_CUREX)
            cmd="this.w_QTAIMP=nvl("+theColumn+",0)"
            &cmd
            this.w_PV__ANNO = val(right(theColumn,4))
            this.w_PV__DATA = cp_CHarToDate("  -  -    ")
            if this.w_PVANNO >= this.w_CURYEAR
              this.w_PVPERIOD = left(theColumn,1)
              do case
                case this.w_PVPERIOD="M"
                  if this.w_PV__ANNO > this.w_CURYEAR or (this.w_PV__ANNO = this.w_CURYEAR and val(substr(theColumn,2,2)) > this.w_CURMONTH) 
                    this.w_PVPERMES = val(substr(theColumn,2,2))
                    do GSDB_BCG with this
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                case this.w_PVPERIOD="S"
                  this.w_PVPERSET = val(substr(theColumn,2,2))
                  do GSDB_BCG with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                case this.w_PVPERIOD="G"
                  this.w_PV__DATA = cp_CharToDate(SUBSTR(theColumn,2,2)+"-"+SUBSTR(theColumn,4,2)+"-"+SUBSTR(theColumn,6,4))
                  do GSDB_BCG with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
              endcase
              if this.w_PV__DATA < i_DATSYS
                * --- Mese non consentito
                this.w_oERRORLOG.AddMsgLog("La data indicata (%1) � inferiore alla data di sistema, impossibile importare la previsione per l'articolo %2.",alltrim(DTOC(this.w_PV__DATA)), alltrim(this.w_ARCODART))     
              else
                * --- Aggiorna le previsioni giornaliere
                * --- Read from KEY_ARTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CACODART"+;
                    " from "+i_cTable+" KEY_ARTI where ";
                        +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CACODART;
                    from (i_cTable) where;
                        CACODICE = this.w_ARCODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARTIPART,ARPROPRE,ARTIPGES"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARTIPART,ARPROPRE,ARTIPGES;
                    from (i_cTable) where;
                        ARCODART = this.w_CODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
                  this.w_PROPRE = NVL(cp_ToDate(_read_.ARPROPRE),cp_NullValue(_read_.ARPROPRE))
                  this.w_TIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from PAR_RIOR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PROGGMPS"+;
                    " from "+i_cTable+" PAR_RIOR where ";
                        +"PRCODART = "+cp_ToStrODBC(this.w_CODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PROGGMPS;
                    from (i_cTable) where;
                        PRCODART = this.w_CODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_OGGMPS = NVL(cp_ToDate(_read_.PROGGMPS),cp_NullValue(_read_.PROGGMPS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if !EMPTY(this.w_ARTIPART) and this.w_OGGMPS$"S-R"
                  this.w_KEYSAL = this.w_CODART
                  * --- Try
                  local bErr_04257B68
                  bErr_04257B68=bTrsErr
                  this.Try_04257B68()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- rollback
                    bTrsErr=.t.
                    cp_EndTrs(.t.)
                  endif
                  bTrsErr=bTrsErr or bErr_04257B68
                  * --- End
                else
                  * --- Articolo non oggetto MPS o Ricambio e provenienza non interna
                  this.bErr = .t.
                  if this.w_RECERR=0
                    do case
                      case EMPTY(this.w_ARTIPART)
                        this.w_oERRORLOG.AddMsgLog("Articolo %1 non presente nell'archivio.",alltrim(this.w_ARCODART))     
                      case !this.w_OGGMPS$"S-R"
                        this.w_oERRORLOG.AddMsgLog("Articolo %1 con sistema d'ordine nessuno.",alltrim(this.w_ARCODART))     
                      case this.w_TIPGES<>"F"
                        this.w_oERRORLOG.AddMsgLog("Articolo %1 con tipo gestione non a fabbisogno.",alltrim(this.w_ARCODART))     
                    endcase
                    this.w_RECERR = this.w_LREC
                  endif
                endif
                USE IN select("_tmpexpl_")
              endif
            endif
            this.w_i = this.w_i + 1
          enddo
          Select (this.w_CUREX) 
 goto this.w_LREC
          ENDSCAN
        endif
      else
        AH_ERRORMSG("Il foglio excel � vuoto, impossibile effettuare l'importazione",48)
        i_retcode = 'stop'
        return
      endif
      USE IN SELECT(this.w_CUREX)
      USE IN SELECT(this.w_CUREX2)
    endif
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.f.)     
    this.w_PUNPADRE.NotifyEvent("Interroga")     
  endproc
  proc Try_04257B68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_04258588
    bErr_04258588=bTrsErr
    this.Try_04258588()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into VEN_PREV
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VEN_PREV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_PREV_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PVQUANTI =PVQUANTI+ "+cp_ToStrODBC(this.w_QTAIMP);
            +i_ccchkf ;
        +" where ";
            +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
            +" and PVCODART = "+cp_ToStrODBC(this.w_CODART);
               )
      else
        update (i_cTable) set;
            PVQUANTI = PVQUANTI + this.w_QTAIMP;
            &i_ccchkf. ;
         where;
            PV__DATA = this.w_PV__DATA;
            and PVCODART = this.w_CODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_04258588
    * --- End
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04258588()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VEN_PREV
    i_nConn=i_TableProp[this.VEN_PREV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VEN_PREV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PV__DATA"+",PVCODART"+",PVPERIOD"+",PVPERMES"+",PVQUANTI"+",PV__ANNO"+",PVPERSET"+",PVKEYSAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PV__DATA),'VEN_PREV','PV__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'VEN_PREV','PVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERIOD),'VEN_PREV','PVPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERMES),'VEN_PREV','PVPERMES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAIMP),'VEN_PREV','PVQUANTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PV__ANNO),'VEN_PREV','PV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERSET),'VEN_PREV','PVPERSET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'VEN_PREV','PVKEYSAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PV__DATA',this.w_PV__DATA,'PVCODART',this.w_CODART,'PVPERIOD',this.w_PVPERIOD,'PVPERMES',this.w_PVPERMES,'PVQUANTI',this.w_QTAIMP,'PV__ANNO',this.w_PV__ANNO,'PVPERSET',this.w_PVPERSET,'PVKEYSAL',this.w_KEYSAL)
      insert into (i_cTable) (PV__DATA,PVCODART,PVPERIOD,PVPERMES,PVQUANTI,PV__ANNO,PVPERSET,PVKEYSAL &i_ccchkf. );
         values (;
           this.w_PV__DATA;
           ,this.w_CODART;
           ,this.w_PVPERIOD;
           ,this.w_PVPERMES;
           ,this.w_QTAIMP;
           ,this.w_PV__ANNO;
           ,this.w_PVPERSET;
           ,this.w_KEYSAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_CUREX = sys(2015)
    this.w_FILEIM = getfile("xls,sxc,ods","Nome file:","Apri",0,"Apri")
    if empty(this.w_FILEIM)
      AH_ERRORMSG("Il file %1 non esiste",48,"",this.w_FILEIM)
      i_retcode = 'stop'
      return
    else
       local loSManager, loSDesktop, loReflection, loPropertyValue, loStarCalc 
 local array NoArgs[2]
      this.w_FILEIM = "file:///"+StrTran(this.w_FILEIM,CHR(92),CHR(47))
      loSManager=CreateObject("Com.Sun.Star.ServiceManager")
      if TYPE("loSManager")="O"
        losDesktop=loSManager.createInstance("com.sun.star.frame.Desktop")
        if TYPE("loSDesktop")="O"
          loSFrame=loSManager.createInstance("com.sun.star.frame")
          if TYPE("loSFrame")="O"
            local array NoArgs[2]
            COMARRAY(loSDesktop, 10)
            loReflection=loSManager.createInstance("com.sun.star.reflection.CoreReflection" )
            COMARRAY(loSDesktop, 10)
            noArgs[1] = this.createStruct( @loReflection,"com.sun.star.beans.PropertyValue" ) 
 noArgs[1].name = "ReadOnly" 
 noArgs[1].value = .T. 
 noArgs[2] = this.createStruct(@loReflection,"com.sun.star.beans.PropertyValue" ) 
 noArgs[2].name = "Hidden" 
 noArgs[2].value = .T.
            loStarCalc=loSDesktop.loadComponentFromUrl( this.w_FILEIM, "_blank", 0, @NoArgs )
            if TYPE("loStarCalc")<>"O"
              this.bErr = .t.
            endif
          else
            this.bErr = .t.
          endif
        else
          this.bErr = .t.
        endif
      else
        this.bErr = .t.
      endif
      if this.bErr
        this.w_oERRORLOG.AddMsgLog("Impossibile inizializzare il motore OpenOffice.")     
      else
        sheets = loStarCalc.Sheets.CreateEnumeration 
 sheet = sheets.nextElement
        * --- La prima colonna deve essere CODART
        *     Al massimo processo 1096 colonne e memorizzo il nome nell'array COLNAMES
        if sheet.getCellByPosition(0,0).String == "CODART"
          STORE "CODART" TO ColNames(1)
          this.w_i = 1
          do while this.w_i < 1096 and !this.bErr
            this.w_ELEMENT = alltrim(sheet.getCellByPosition(this.w_i,0).String)
            this.okColumn = .f.
            if empty(this.w_ELEMENT)
              if this.w_i > 2
                * --- Non ho pi� colonne 'piene' ed ho almeno una previsione caricata
                exit
              else
                * --- Ho la sola colonna 'CODART' senza previsioni...
                this.bErr = .t.
                exit
              endif
            else
              do case
                case left(this.w_ELEMENT,1) == "M"
                  if len(this.w_ELEMENT) = 7 and val(substr(this.w_ELEMENT,2,2)) > 0 and val(substr(this.w_ELEMENT,2,2)) <= 12 and val(substr(this.w_ELEMENT,4,4)) >= year(i_DATSYS)
                    this.okColumn = .t.
                  endif
                case left(this.w_ELEMENT,1) == "S"
                  if len(this.w_ELEMENT) = 7 and val(substr(this.w_ELEMENT,2,2)) > 0 and val(substr(this.w_ELEMENT,2,2)) <= 54 and val(substr(this.w_ELEMENT,4,4)) >= year(i_DATSYS)
                    this.okColumn = .t.
                  endif
                case left(this.w_ELEMENT,1) == "G"
                  if len(this.w_ELEMENT) = 9 and val(right(this.w_ELEMENT,8)) > 0 
                    this.okColumn = .t.
                  endif
              endcase
            endif
            if this.okColumn
              STORE this.w_ELEMENT TO ColNames(this.w_i)
            endif
            this.bErr = !this.okCOlumn
            this.w_i = this.w_i + 1
          enddo
          this.w_ELEMENT = alltrim(sheet.getCellByPosition(this.w_i,0).String)
        else
          this.bErr = .t.
        endif
        if !this.bErr
          * --- Cotruisco la frase di create per il cursore di appoggio
          STATEMENT="CREATE CURSOR "+this.w_CUREX+" (CODART C(41)"
          this.w_i = 1
          do while TYPE("ColNames(this.w_i)")=="C" and !empty(alltrim(ColNames(this.w_i)))
            STATEMENT=STATEMENT+ "," + alltrim(ColNames(this.w_i)) + " N(10,0)"
            this.w_i = this.w_i + 1
          enddo
          this.w_NUMEL = this.w_i
          STATEMENT=STATEMENT + ")"
          &STATEMENT
          * --- Preparo la frase di insert...
          INSERT="INSERT INTO "+this.w_CUREX+" (CODART"
          this.w_i = 1
          do while TYPE("ColNames(this.w_i)")=="C" and !empty(alltrim(ColNames(this.w_i)))
            INSERT=INSERT+ "," + alltrim(ColNames(this.w_i))
            this.w_i = this.w_i + 1
          enddo
          INSERT=INSERT+") VALUES ("
          * --- Eseguo le insert nel cursore di appoggio...
          this.w_i = 1
          do while !EMPTY(alltrim(sheet.getCellByPosition(0,this.w_i).String))
            this.w_ELEMENT = alltrim(sheet.getCellByPosition(0,this.w_i).String)
            STATEMENT=INSERT
            STATEMENT=STATEMENT +"'"+alltrim(this.w_ELEMENT)+"'"
            this.w_nc = 1
            do while this.w_nc < this.w_NUMEL
              STATEMENT=STATEMENT + "," + alltrim(str(val(alltrim(sheet.getCellByPosition(this.w_nc,this.w_i).String))))
              this.w_nc = this.w_nc + 1
            enddo
            STATEMENT=STATEMENT + ")" 
            &STATEMENT
            this.w_i = this.w_i + 1
          enddo
        else
          * --- La struttura del foglio non ripecchia quella richiesta
          this.w_oERRORLOG.AddMsgLog("Il foglio excel deve rispettare la seguente struttura:")     
          this.w_oERRORLOG.AddMsgLog("   - La prima colonna deve essere CODART.")     
          this.w_oERRORLOG.AddMsgLog("   - Le colonne successive possono essere composte nel modo seguente:")     
          this.w_oERRORLOG.AddMsgLog("   ----  Formato 'MXXYYYY': indica che la previone deve essere inserita nel mese XX dell'anno YYYY")     
          this.w_oERRORLOG.AddMsgLog("   ----  Formato 'SXXYYYY': indica che la previone deve essere inserita nel XXesima settimana dell'anno YYYY")     
          this.w_oERRORLOG.AddMsgLog("   ----  Formato 'GDDMMYYYY': indica che la previone deve essere inserita nel giorno DD/MM/YYYY")     
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_Azione)
    this.w_Azione=w_Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='VEN_PREV'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='PAR_RIOR'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- gsdb_bpv
  Function createStruct( toReflection, tcTypeName )
       local loPropertyValue, loTemp
       loPropertyValue = createobject( "relation")
       toReflection.forName( tcTypeName ).createobject( @loPropertyValue )
       return( loPropertyValue )
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Azione"
endproc
