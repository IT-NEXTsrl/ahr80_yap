* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kgc                                                        *
*              Generazione calendario                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_28]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-21                                                      *
* Last revis.: 2008-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kgc",oParentObject))

* --- Class definition
define class tgsco_kgc as StdForm
  Top    = 29
  Left   = 47

  * --- Standard Properties
  Width  = 391
  Height = 255
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-18"
  HelpContextID=32089449
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsco_kgc"
  cComment = "Generazione calendario"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ANNO = 0
  w_LUN = space(1)
  w_MAR = space(1)
  w_MER = space(1)
  w_GIO = space(1)
  w_VEN = space(1)
  w_SAB = space(1)
  w_DOM = space(1)
  w_INPER1 = ctod('  /  /  ')
  w_FIPER1 = ctod('  /  /  ')
  w_INPER2 = ctod('  /  /  ')
  w_FIPER2 = ctod('  /  /  ')
  w_INPER3 = ctod('  /  /  ')
  w_FIPER3 = ctod('  /  /  ')
  w_INPER4 = ctod('  /  /  ')
  w_FIPER4 = ctod('  /  /  ')
  w_INPER5 = ctod('  /  /  ')
  w_FIPER5 = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kgcPag1","gsco_kgc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCO_BGC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=0
      .w_LUN=space(1)
      .w_MAR=space(1)
      .w_MER=space(1)
      .w_GIO=space(1)
      .w_VEN=space(1)
      .w_SAB=space(1)
      .w_DOM=space(1)
      .w_INPER1=ctod("  /  /  ")
      .w_FIPER1=ctod("  /  /  ")
      .w_INPER2=ctod("  /  /  ")
      .w_FIPER2=ctod("  /  /  ")
      .w_INPER3=ctod("  /  /  ")
      .w_FIPER3=ctod("  /  /  ")
      .w_INPER4=ctod("  /  /  ")
      .w_FIPER4=ctod("  /  /  ")
      .w_INPER5=ctod("  /  /  ")
      .w_FIPER5=ctod("  /  /  ")
        .w_ANNO = year(i_DATSYS)+1
        .w_LUN = 'S'
        .w_MAR = 'S'
        .w_MER = 'S'
        .w_GIO = 'S'
        .w_VEN = 'S'
        .w_SAB = ' '
        .w_DOM = ' '
    endwith
    this.DoRTCalc(9,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_2.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_2.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oLUN_1_9.RadioValue()==this.w_LUN)
      this.oPgFrm.Page1.oPag.oLUN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMAR_1_10.RadioValue()==this.w_MAR)
      this.oPgFrm.Page1.oPag.oMAR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMER_1_11.RadioValue()==this.w_MER)
      this.oPgFrm.Page1.oPag.oMER_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIO_1_12.RadioValue()==this.w_GIO)
      this.oPgFrm.Page1.oPag.oGIO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVEN_1_13.RadioValue()==this.w_VEN)
      this.oPgFrm.Page1.oPag.oVEN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSAB_1_14.RadioValue()==this.w_SAB)
      this.oPgFrm.Page1.oPag.oSAB_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDOM_1_15.RadioValue()==this.w_DOM)
      this.oPgFrm.Page1.oPag.oDOM_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER1_1_16.value==this.w_INPER1)
      this.oPgFrm.Page1.oPag.oINPER1_1_16.value=this.w_INPER1
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER1_1_17.value==this.w_FIPER1)
      this.oPgFrm.Page1.oPag.oFIPER1_1_17.value=this.w_FIPER1
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER2_1_18.value==this.w_INPER2)
      this.oPgFrm.Page1.oPag.oINPER2_1_18.value=this.w_INPER2
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER2_1_19.value==this.w_FIPER2)
      this.oPgFrm.Page1.oPag.oFIPER2_1_19.value=this.w_FIPER2
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER3_1_20.value==this.w_INPER3)
      this.oPgFrm.Page1.oPag.oINPER3_1_20.value=this.w_INPER3
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER3_1_21.value==this.w_FIPER3)
      this.oPgFrm.Page1.oPag.oFIPER3_1_21.value=this.w_FIPER3
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER4_1_25.value==this.w_INPER4)
      this.oPgFrm.Page1.oPag.oINPER4_1_25.value=this.w_INPER4
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER4_1_26.value==this.w_FIPER4)
      this.oPgFrm.Page1.oPag.oFIPER4_1_26.value=this.w_FIPER4
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER5_1_28.value==this.w_INPER5)
      this.oPgFrm.Page1.oPag.oINPER5_1_28.value=this.w_INPER5
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER5_1_29.value==this.w_FIPER5)
      this.oPgFrm.Page1.oPag.oFIPER5_1_29.value=this.w_FIPER5
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ANNO>1990)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INPER1) or year(.w_INPER1)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER1_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_FIPER1) or year(.w_FIPER1)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER1_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INPER2) or year(.w_INPER2)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER2_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_FIPER2) or year(.w_FIPER2)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER2_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INPER3) or year(.w_INPER3)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER3_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_FIPER3) or year(.w_FIPER3)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER3_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INPER4) or year(.w_INPER4)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER4_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_FIPER4) or year(.w_FIPER4)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER4_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INPER5) or year(.w_INPER5)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER5_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_FIPER5) or year(.w_FIPER5)=.w_ANNO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER5_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsco_kgcPag1 as StdContainer
  Width  = 387
  height = 255
  stdWidth  = 387
  stdheight = 255
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_2 as StdField with uid="FFZDYIEWMA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno solare per il quale si vuole generare il calendario",;
    HelpContextID = 26571514,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=44, Left=122, Top=9, cSayPict='"@K 9999"', cGetPict='"@K 9999"'

  func oANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANNO>1990)
    endwith
    return bRes
  endfunc

  add object oLUN_1_9 as StdCheck with uid="VNIGJLRHOB",rtseq=2,rtrep=.f.,left=17, top=59, caption="Lunedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31746890,;
    cFormVar="w_LUN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLUN_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oLUN_1_9.GetRadio()
    this.Parent.oContained.w_LUN = this.RadioValue()
    return .t.
  endfunc

  func oLUN_1_9.SetRadio()
    this.Parent.oContained.w_LUN=trim(this.Parent.oContained.w_LUN)
    this.value = ;
      iif(this.Parent.oContained.w_LUN=='S',1,;
      0)
  endfunc

  add object oMAR_1_10 as StdCheck with uid="NLBFVWVBVV",rtseq=3,rtrep=.f.,left=17, top=78, caption="Martedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31735610,;
    cFormVar="w_MAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMAR_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMAR_1_10.GetRadio()
    this.Parent.oContained.w_MAR = this.RadioValue()
    return .t.
  endfunc

  func oMAR_1_10.SetRadio()
    this.Parent.oContained.w_MAR=trim(this.Parent.oContained.w_MAR)
    this.value = ;
      iif(this.Parent.oContained.w_MAR=='S',1,;
      0)
  endfunc

  add object oMER_1_11 as StdCheck with uid="KLIIYIJSZN",rtseq=4,rtrep=.f.,left=17, top=97, caption="Mercoledi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31734586,;
    cFormVar="w_MER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMER_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMER_1_11.GetRadio()
    this.Parent.oContained.w_MER = this.RadioValue()
    return .t.
  endfunc

  func oMER_1_11.SetRadio()
    this.Parent.oContained.w_MER=trim(this.Parent.oContained.w_MER)
    this.value = ;
      iif(this.Parent.oContained.w_MER=='S',1,;
      0)
  endfunc

  add object oGIO_1_12 as StdCheck with uid="BBNCKTANRO",rtseq=5,rtrep=.f.,left=17, top=116, caption="Giovedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31745946,;
    cFormVar="w_GIO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIO_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGIO_1_12.GetRadio()
    this.Parent.oContained.w_GIO = this.RadioValue()
    return .t.
  endfunc

  func oGIO_1_12.SetRadio()
    this.Parent.oContained.w_GIO=trim(this.Parent.oContained.w_GIO)
    this.value = ;
      iif(this.Parent.oContained.w_GIO=='S',1,;
      0)
  endfunc

  add object oVEN_1_13 as StdCheck with uid="CZJAIHHKMX",rtseq=6,rtrep=.f.,left=17, top=135, caption="Venerdi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31750826,;
    cFormVar="w_VEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVEN_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVEN_1_13.GetRadio()
    this.Parent.oContained.w_VEN = this.RadioValue()
    return .t.
  endfunc

  func oVEN_1_13.SetRadio()
    this.Parent.oContained.w_VEN=trim(this.Parent.oContained.w_VEN)
    this.value = ;
      iif(this.Parent.oContained.w_VEN=='S',1,;
      0)
  endfunc

  add object oSAB_1_14 as StdCheck with uid="LQSCKNXWAL",rtseq=7,rtrep=.f.,left=17, top=154, caption="Sabato",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31801050,;
    cFormVar="w_SAB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSAB_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSAB_1_14.GetRadio()
    this.Parent.oContained.w_SAB = this.RadioValue()
    return .t.
  endfunc

  func oSAB_1_14.SetRadio()
    this.Parent.oContained.w_SAB=trim(this.Parent.oContained.w_SAB)
    this.value = ;
      iif(this.Parent.oContained.w_SAB=='S',1,;
      0)
  endfunc

  add object oDOM_1_15 as StdCheck with uid="ZVZVGLFUIG",rtseq=8,rtrep=.f.,left=17, top=173, caption="Domenica",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 31752650,;
    cFormVar="w_DOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDOM_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDOM_1_15.GetRadio()
    this.Parent.oContained.w_DOM = this.RadioValue()
    return .t.
  endfunc

  func oDOM_1_15.SetRadio()
    this.Parent.oContained.w_DOM=trim(this.Parent.oContained.w_DOM)
    this.value = ;
      iif(this.Parent.oContained.w_DOM=='S',1,;
      0)
  endfunc

  add object oINPER1_1_16 as StdField with uid="MLVMAQOKYJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_INPER1", cQueryName = "INPER1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 192893562,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=169, Top=65, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER1_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER1) or year(.w_INPER1)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oFIPER1_1_17 as StdField with uid="WBXTTMXEMQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FIPER1", cQueryName = "FIPER1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 192894890,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=289, Top=65, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER1) or year(.w_FIPER1)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oINPER2_1_18 as StdField with uid="GMTZHTUDBH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_INPER2", cQueryName = "INPER2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 176116346,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=169, Top=92, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER2_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER2) or year(.w_INPER2)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oFIPER2_1_19 as StdField with uid="GCAMYKVALR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FIPER2", cQueryName = "FIPER2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 176117674,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=289, Top=92, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER2_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER2) or year(.w_FIPER2)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oINPER3_1_20 as StdField with uid="HUFQUDYASW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_INPER3", cQueryName = "INPER3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 159339130,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=169, Top=119, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER3_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER3) or year(.w_INPER3)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oFIPER3_1_21 as StdField with uid="KVLIOAEZIO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FIPER3", cQueryName = "FIPER3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 159340458,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=289, Top=119, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER3_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER3) or year(.w_FIPER3)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oINPER4_1_25 as StdField with uid="REFDMEFCMZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_INPER4", cQueryName = "INPER4",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 142561914,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=169, Top=146, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER4_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER4) or year(.w_INPER4)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oFIPER4_1_26 as StdField with uid="NBMQLCBGIN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FIPER4", cQueryName = "FIPER4",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 142563242,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=289, Top=146, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER4_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER4) or year(.w_FIPER4)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oINPER5_1_28 as StdField with uid="UAJZSQRSFI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_INPER5", cQueryName = "INPER5",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 125784698,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=169, Top=173, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER5_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER5) or year(.w_INPER5)=.w_ANNO)
    endwith
    return bRes
  endfunc

  add object oFIPER5_1_29 as StdField with uid="OLGARTPNFH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FIPER5", cQueryName = "FIPER5",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 125786026,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=289, Top=173, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER5_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER5) or year(.w_FIPER5)=.w_ANNO)
    endwith
    return bRes
  endfunc


  add object oBtn_1_34 as StdButton with uid="XAOFRYAIID",left=275, top=203, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il calendario";
    , HelpContextID = 32060698;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        do GSCO_BGC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="KYQESPJULE",left=325, top=203, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24772026;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="UUINRNBKEV",Visible=.t., Left=8, Top=9,;
    Alignment=1, Width=108, Height=15,;
    Caption="Anno solare:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="GTFXAGPLKE",Visible=.t., Left=127, Top=65,;
    Alignment=1, Width=39, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="AYLHPSHDPF",Visible=.t., Left=247, Top=65,;
    Alignment=1, Width=36, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="VEUPGEDSMV",Visible=.t., Left=127, Top=92,;
    Alignment=1, Width=39, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="BKSPEMFIRR",Visible=.t., Left=247, Top=92,;
    Alignment=1, Width=36, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YIXPZEVHIG",Visible=.t., Left=127, Top=119,;
    Alignment=1, Width=39, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="FJUGSILXXY",Visible=.t., Left=247, Top=119,;
    Alignment=1, Width=36, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="MCMVMALXRV",Visible=.t., Left=9, Top=34,;
    Alignment=2, Width=115, Height=18,;
    Caption="Giorni lavorativi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="XDTEXXEWPU",Visible=.t., Left=122, Top=37,;
    Alignment=2, Width=254, Height=15,;
    Caption="Chiusure periodiche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="HFOGQPZWVJ",Visible=.t., Left=127, Top=146,;
    Alignment=1, Width=39, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="SOGXYVWODH",Visible=.t., Left=247, Top=146,;
    Alignment=1, Width=36, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ZCWUGGUQXZ",Visible=.t., Left=247, Top=173,;
    Alignment=1, Width=36, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="EVULMGOLFH",Visible=.t., Left=127, Top=173,;
    Alignment=1, Width=39, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oBox_1_23 as StdBox with uid="GFBRHZECCA",left=9, top=53, width=115,height=148

  add object oBox_1_33 as StdBox with uid="TECWEIOSDO",left=122, top=53, width=254,height=148
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kgc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
