* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bbt                                                        *
*              Generazione time-buckets ODL/OCL/ODA                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-10                                                      *
* Last revis.: 2010-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bbt",oParentObject)
return(i_retval)

define class tgsco_bbt as StdBatch
  * --- Local variables
  i = 0
  w_tDATFIN = ctod("  /  /  ")
  w_tDATINI = ctod("  /  /  ")
  TmpC = space(10)
  w_OLDCOD = space(41)
  NumPer = 0
  * --- WorkFile variables
  ODL_MAST_idx=0
  MPS_TPER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamato Da GSCO_KCB - GENERAZIONE TIME-BUCKETS
    do GSCO_BCB with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Eseguo aggiornamento del piano ordini
    * --- Azzera Periodo MPS
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='OLCODODL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"OLCODODL "," from "+i_cQueryTable+" where OLTQTSAL<>0",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
      +"ODL_MAST.OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Riassegnazione time-buckets MPS in corso...")
    * --- Crea Array con periodi da esaminare
    dimension Periodi(100)
    dimension Datini(100)
    dimension Datfin(100)
    dimension PriGioLav(100)
    this.i = 0
    * --- Select from MPS_TPER
    i_nConn=i_TableProp[this.MPS_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
          +" where TPPERREL <> 'XXXX'";
          +" order by TPPERASS";
           ,"_Curs_MPS_TPER")
    else
      select * from (i_cTable);
       where TPPERREL <> "XXXX";
       order by TPPERASS;
        into cursor _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      select _Curs_MPS_TPER
      locate for 1=1
      do while not(eof())
      if _Curs_MPS_TPER.TPPERASS = "000"
        this.w_tDATINI = _Curs_MPS_TPER.TPDATINI
      else
        this.i = this.i+1
        Periodi(this.i) = nvl(_Curs_MPS_TPER.TPPERREL , " ")
        Datini(this.i) = nvl(_Curs_MPS_TPER.TPDATINI , cp_CharToDate("  -  -  "))
        Datfin(this.i) = nvl(_Curs_MPS_TPER.TPDATFIN , cp_CharToDate("  -  -  "))
        this.w_tDATFIN = nvl(_Curs_MPS_TPER.TPDATFIN , cp_CharToDate("  -  -  "))
        PriGioLav(this.i) = nvl(_Curs_MPS_TPER.TPPRILAV , cp_CharToDate("  -  -  "))
      endif
        select _Curs_MPS_TPER
        continue
      enddo
      use
    endif
    this.NumPer = this.i
    * --- Try
    local bErr_0392D9E0
    bErr_0392D9E0=bTrsErr
    this.Try_0392D9E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMSG("Errore in assegnazione time-buckets",16)
    endif
    bTrsErr=bTrsErr or bErr_0392D9E0
    * --- End
    Release Periodi, DatIni, DatFin, PriGioLav
  endproc
  proc Try_0392D9E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_OLDCOD = repl("<",41)
    * --- Select from GSCO_BTB
    do vq_exec with 'GSCO_BTB',this,'_Curs_GSCO_BTB','',.f.,.t.
    if used('_Curs_GSCO_BTB')
      select _Curs_GSCO_BTB
      locate for 1=1
      do while not(eof())
      * --- Inserisce periodo zero
      if this.w_OLDCOD <> _Curs_GSCO_BTB.OLTCODIC
        * --- Ad ogni cambio codice, crea il periodo 000 nei saldi MPS
        this.w_OLDCOD = _Curs_GSCO_BTB.OLTCODIC
        this.TmpC = "000"
      endif
      * --- Ricerca Periodo da assegnare all'ODL in corso
      this.TmpC = "XXX"
      for this.i=1 to this.NumPer
      if _Curs_GSCO_BTB.OLTDTRIC < DatIni(this.i)
        this.TmpC = "000"
        Exit
      else
        if DatIni(this.i) <= _Curs_GSCO_BTB.OLTDTRIC and _Curs_GSCO_BTB.OLTDTRIC<= DatFin(this.i)
          * --- Compone periodo
          this.TmpC = right("000"+alltrim(str(this.i,3,0)),3)
          * --- Esce da ciclo for
          Exit
        endif
      endif
      next
      * --- Aggiorna Periodo
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC(this.TmpC),'ODL_MAST','OLTPERAS');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_GSCO_BTB.OLCODODL);
               )
      else
        update (i_cTable) set;
            OLTPERAS = this.TmpC;
            &i_ccchkf. ;
         where;
            OLCODODL = _Curs_GSCO_BTB.OLCODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSCO_BTB
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Messaggio di chiusura
    if lower(this.oParentObject.class) = "tgsco_kcb"
      ah_errorMsg("Elaborazione terminata",48)
      this.oParentObject.ecpquit()     
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='MPS_TPER'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_GSCO_BTB')
      use in _Curs_GSCO_BTB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
