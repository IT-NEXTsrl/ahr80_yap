* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bab                                                        *
*              Gestione abbinamento Pegging commessa                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-27                                                      *
* Last revis.: 2010-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bab",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_bab as StdBatch
  * --- Local variables
  pAzione = space(2)
  w_PESERIAL = space(10)
  w_PROGR = 0
  w_OK = .f.
  * --- WorkFile variables
  PEG_SELI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Abbinamento Pegging Commessa (da GSMR_KAB)
    do case
      case this.pAzione = "AGG"
        this.w_OK = .T.
        * --- Try
        local bErr_0385D218
        bErr_0385D218=bTrsErr
        this.Try_0385D218()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Impossibile aggiornare la tabella del Pegging di Secondo Livello.",48)
          this.w_OK = .F.
        endif
        bTrsErr=bTrsErr or bErr_0385D218
        * --- End
        if this.w_OK
          ah_ErrorMsg("Aggiornamento completato con successo.",64)
        endif
        this.oParentObject.w_QTANUO = 0
        this.oParentObject.w_NEWCOM = space(15)
        this.oparentobject.ecpquit()
    endcase
  endproc
  proc Try_0385D218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Gestione riga di PEG_SELI gi� presente sullla tabella
    if this.oParentObject.w_QTAAVA>0
      * --- La riga � ancora valida, la modifico
      * --- Write into PEG_SELI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QTAAVA),'PEG_SELI','PEQTAABB');
            +i_ccchkf ;
        +" where ";
            +"PESERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
               )
      else
        update (i_cTable) set;
            PEQTAABB = this.oParentObject.w_QTAAVA;
            &i_ccchkf. ;
         where;
            PESERIAL = this.oParentObject.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- L'abbinamento a 0 non ha senso, elimino la riga
      * --- Delete from PEG_SELI
      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PESERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              PESERIAL = this.oParentObject.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Gestione riga da aggiungere sulla tabella PEG_SELI 
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
    this.w_PROGR = max(this.w_PROGR + 1,1)
    this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
    * --- Insert into PEG_SELI
    i_nConn=i_TableProp[this.PEG_SELI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERODL),'PEG_SELI','PESERODL');
      +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
      +","+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PERIGORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QTANUO),'PEG_SELI','PEQTAABB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ODLORI),'PEG_SELI','PEODLORI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCOM),'PEG_SELI','PECODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODRIC),'PEG_SELI','PECODRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.oParentObject.w_SERODL,'PETIPRIF',"M",'PERIGORD',0,'PEQTAABB',this.oParentObject.w_QTANUO,'PEODLORI',this.oParentObject.w_ODLORI,'PECODCOM',this.oParentObject.w_NEWCOM,'PECODRIC',this.oParentObject.w_CODRIC)
      insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
         values (;
           this.w_PESERIAL;
           ,this.oParentObject.w_SERODL;
           ,"M";
           ,0;
           ,this.oParentObject.w_QTANUO;
           ,this.oParentObject.w_ODLORI;
           ,this.oParentObject.w_NEWCOM;
           ,this.oParentObject.w_CODRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PEG_SELI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
