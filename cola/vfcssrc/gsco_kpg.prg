* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kpg                                                        *
*              Aggiornamento Pegging                                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_33]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-21                                                      *
* Last revis.: 2017-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kpg",oParentObject))

* --- Class definition
define class tgsco_kpg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 435
  Height = 224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-09-27"
  HelpContextID=149529961
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  cpusers_IDX = 0
  PARA_MRP_IDX = 0
  cPrg = "gsco_kpg"
  cComment = "Aggiornamento Pegging"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PPCODICE = space(2)
  o_PPCODICE = space(2)
  w_EDITBTN = space(1)
  w_GENPDA = space(1)
  w_AACODICE = space(2)
  w_CRITFORN = space(1)
  w_MODELA = space(1)
  w_DESUTE = space(20)
  w_PEGGING2 = space(1)
  w_INTERN = .F.
  w_UPDELA = .F.
  w_LLCINI = 0
  o_LLCINI = 0
  w_LLCFIN = 0
  w_MESSRIPIA = space(1)
  w_GENPDA = space(1)
  w_MSGMRP = space(2)
  w_PMOCLORD = space(1)
  w_PMODLPIA = space(1)
  w_PMODLLAN = space(1)
  w_ELABDBF = space(1)
  w_MRPLOG = space(1)
  w_CHECKINIT = 0
  w_PPCRIELA = space(1)
  w_PPPERPIA = space(1)
  w_PPPIAPUN = space(1)
  w_PERPIA = space(1)
  w_CRIELA = space(1)
  o_CRIELA = space(1)
  w_PIAPUN = space(1)
  w_PPMATOUP = space(1)
  w_KEYRIF = space(10)
  w_oLinePB = .NULL.
  w_PROGBAR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kpgPag1","gsco_kpg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMRPLOG_1_23
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oLinePB = this.oPgFrm.Pages(1).oPag.oLinePB
    this.w_PROGBAR = this.oPgFrm.Pages(1).oPag.PROGBAR
    DoDefault()
    proc Destroy()
      this.w_oLinePB = .NULL.
      this.w_PROGBAR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='PARA_MRP'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSMR2BGP(this,"GSMR_BGP")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE=space(2)
      .w_EDITBTN=space(1)
      .w_GENPDA=space(1)
      .w_AACODICE=space(2)
      .w_CRITFORN=space(1)
      .w_MODELA=space(1)
      .w_DESUTE=space(20)
      .w_PEGGING2=space(1)
      .w_INTERN=.f.
      .w_UPDELA=.f.
      .w_LLCINI=0
      .w_LLCFIN=0
      .w_MESSRIPIA=space(1)
      .w_GENPDA=space(1)
      .w_MSGMRP=space(2)
      .w_PMOCLORD=space(1)
      .w_PMODLPIA=space(1)
      .w_PMODLLAN=space(1)
      .w_ELABDBF=space(1)
      .w_MRPLOG=space(1)
      .w_CHECKINIT=0
      .w_PPCRIELA=space(1)
      .w_PPPERPIA=space(1)
      .w_PPPIAPUN=space(1)
      .w_PERPIA=space(1)
      .w_CRIELA=space(1)
      .w_PIAPUN=space(1)
      .w_PPMATOUP=space(1)
      .w_KEYRIF=space(10)
        .w_PPCODICE = "PP"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_1('Full')
        endif
        .w_EDITBTN = 'S'
        .w_GENPDA = 'N'
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Questa funzione esegue la rigenerazione del pegging di secondo livello%0in base al piano attuale degli ODL/OCL"))
        .w_AACODICE = "AA"
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_AACODICE))
          .link_1_7('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_MODELA = 'R'
          .DoRTCalc(7,7,.f.)
        .w_PEGGING2 = "S"
        .w_INTERN = FALSE
        .w_UPDELA = FALSE
        .w_LLCINI = 0
        .w_LLCFIN = 999
        .w_MESSRIPIA = "N"
        .w_GENPDA = IIF(g_MODA="S", "S", "N")
        .w_MSGMRP = "MR"
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MSGMRP))
          .link_1_18('Full')
        endif
          .DoRTCalc(16,18,.f.)
        .w_ELABDBF = 'S'
        .w_MRPLOG = g_ATTIVAMRPLOG
        .w_CHECKINIT = 0
          .DoRTCalc(22,24,.f.)
        .w_PERPIA = .w_PPPERPIA
        .w_CRIELA = .w_PPCRIELA
        .w_PIAPUN = .w_PPPIAPUN
      .oPgFrm.Page1.oPag.oLinePB.Calculate()
      .oPgFrm.Page1.oPag.PROGBAR.Calculate()
          .DoRTCalc(28,28,.f.)
        .w_KEYRIF = SYS(2015)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Questa funzione esegue la rigenerazione del pegging di secondo livello%0in base al piano attuale degli ODL/OCL"))
        .DoRTCalc(2,3,.t.)
          .link_1_7('Full')
        .DoRTCalc(5,5,.t.)
            .w_MODELA = 'R'
        .DoRTCalc(7,12,.t.)
            .w_MESSRIPIA = "N"
        .DoRTCalc(14,14,.t.)
            .w_MSGMRP = "MR"
          .link_1_18('Full')
        .oPgFrm.Page1.oPag.oLinePB.Calculate()
        .oPgFrm.Page1.oPag.PROGBAR.Calculate()
        if .o_CRIELA<>.w_CRIELA
          .Calculate_VASGIKTUPU()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Questa funzione esegue la rigenerazione del pegging di secondo livello%0in base al piano attuale degli ODL/OCL"))
        .oPgFrm.Page1.oPag.oLinePB.Calculate()
        .oPgFrm.Page1.oPag.PROGBAR.Calculate()
    endwith
  return

  proc Calculate_VASGIKTUPU()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,.w_PEGGING2;
             )
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oLinePB.Event(cEvent)
      .oPgFrm.Page1.oPag.PROGBAR.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_VASGIKTUPU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCRIELA,PPPERPIA,PPPIAPUN,PPMATOUP";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPCRIELA,PPPERPIA,PPPIAPUN,PPMATOUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_PPCRIELA = NVL(_Link_.PPCRIELA,space(1))
      this.w_PPPERPIA = NVL(_Link_.PPPERPIA,space(1))
      this.w_PPPIAPUN = NVL(_Link_.PPPIAPUN,space(1))
      this.w_PPMATOUP = NVL(_Link_.PPMATOUP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_PPCRIELA = space(1)
      this.w_PPPERPIA = space(1)
      this.w_PPPIAPUN = space(1)
      this.w_PPMATOUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AACODICE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPFLDIGE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_AACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_AACODICE)
            select PPCODICE,PPFLDIGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AACODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_CRITFORN = NVL(_Link_.PPFLDIGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AACODICE = space(2)
      endif
      this.w_CRITFORN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MSGMRP
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARA_MRP_IDX,3]
    i_lTable = "PARA_MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2], .t., this.PARA_MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MSGMRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MSGMRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PMCODICE,PMODLPIA,PMODLLAN,PMOCLORD";
                   +" from "+i_cTable+" "+i_lTable+" where PMCODICE="+cp_ToStrODBC(this.w_MSGMRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PMCODICE',this.w_MSGMRP)
            select PMCODICE,PMODLPIA,PMODLLAN,PMOCLORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MSGMRP = NVL(_Link_.PMCODICE,space(2))
      this.w_PMODLPIA = NVL(_Link_.PMODLPIA,space(1))
      this.w_PMODLLAN = NVL(_Link_.PMODLLAN,space(1))
      this.w_PMOCLORD = NVL(_Link_.PMOCLORD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MSGMRP = space(2)
      endif
      this.w_PMODLPIA = space(1)
      this.w_PMODLLAN = space(1)
      this.w_PMOCLORD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])+'\'+cp_ToStr(_Link_.PMCODICE,1)
      cp_ShowWarn(i_cKey,this.PARA_MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MSGMRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMRPLOG_1_23.RadioValue()==this.w_MRPLOG)
      this.oPgFrm.Page1.oPag.oMRPLOG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERPIA_1_28.RadioValue()==this.w_PERPIA)
      this.oPgFrm.Page1.oPag.oPERPIA_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCRIELA_1_29.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page1.oPag.oCRIELA_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPIAPUN_1_30.RadioValue()==this.w_PIAPUN)
      this.oPgFrm.Page1.oPag.oPIAPUN_1_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PPCODICE = this.w_PPCODICE
    this.o_LLCINI = this.w_LLCINI
    this.o_CRIELA = this.w_CRIELA
    return

enddefine

* --- Define pages as container
define class tgsco_kpgPag1 as StdContainer
  Width  = 431
  height = 224
  stdWidth  = 431
  stdheight = 224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_3 as StdButton with uid="WMNAKJHMJE",left=327, top=174, width=48,height=45,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare il Pegging";
    , HelpContextID = 7747175;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSMR2BGP(this.Parent.oContained,"GSMR_BGP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="ERYVBFDDSW",left=377, top=174, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142212538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_6 as cp_calclbl with uid="FZCVVXCORO",left=3, top=4, width=422,height=41,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 28467686

  add object oMRPLOG_1_23 as StdCheck with uid="AOSDRGZMLX",rtseq=20,rtrep=.f.,left=25, top=130, caption="Attiva scrittura log elaborazione",;
    ToolTipText = "Attiva la scrittura del log di elaborazione MRP",;
    HelpContextID = 56078790,;
    cFormVar="w_MRPLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMRPLOG_1_23.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oMRPLOG_1_23.GetRadio()
    this.Parent.oContained.w_MRPLOG = this.RadioValue()
    return .t.
  endfunc

  func oMRPLOG_1_23.SetRadio()
    this.Parent.oContained.w_MRPLOG=trim(this.Parent.oContained.w_MRPLOG)
    this.value = ;
      iif(this.Parent.oContained.w_MRPLOG=="S",1,;
      0)
  endfunc


  add object oPERPIA_1_28 as StdCombo with uid="DRHNEVIUIN",rtseq=25,rtrep=.f.,left=210,top=51,width=157,height=21;
    , ToolTipText = "Pianifica per periodo";
    , HelpContextID = 217826550;
    , cFormVar="w_PERPIA",RowSource=""+"Giornaliero,"+"Settimanale,"+"Mensile,"+"Semestrale,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPERPIA_1_28.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'S',;
    iif(this.value =5,'A',;
    space(1)))))))
  endfunc
  func oPERPIA_1_28.GetRadio()
    this.Parent.oContained.w_PERPIA = this.RadioValue()
    return .t.
  endfunc

  func oPERPIA_1_28.SetRadio()
    this.Parent.oContained.w_PERPIA=trim(this.Parent.oContained.w_PERPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PERPIA=='D',1,;
      iif(this.Parent.oContained.w_PERPIA=='W',2,;
      iif(this.Parent.oContained.w_PERPIA=='M',3,;
      iif(this.Parent.oContained.w_PERPIA=='S',4,;
      iif(this.Parent.oContained.w_PERPIA=='A',5,;
      0)))))
  endfunc


  add object oCRIELA_1_29 as StdCombo with uid="RZMRXYJQNJ",rtseq=26,rtrep=.f.,left=210,top=75,width=157,height=21;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 220217638;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIELA_1_29.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_1_29.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_1_29.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc


  add object oPIAPUN_1_30 as StdCombo with uid="QHVQIVCNUN",rtseq=27,rtrep=.f.,left=210,top=100,width=157,height=21;
    , ToolTipText = "Pianificazione puntuale";
    , HelpContextID = 180009206;
    , cFormVar="w_PIAPUN",RowSource=""+"Si,"+"No,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPIAPUN_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPIAPUN_1_30.GetRadio()
    this.Parent.oContained.w_PIAPUN = this.RadioValue()
    return .t.
  endfunc

  func oPIAPUN_1_30.SetRadio()
    this.Parent.oContained.w_PIAPUN=trim(this.Parent.oContained.w_PIAPUN)
    this.value = ;
      iif(this.Parent.oContained.w_PIAPUN=='S',1,;
      iif(this.Parent.oContained.w_PIAPUN=='N',2,;
      iif(this.Parent.oContained.w_PIAPUN=='A',3,;
      0)))
  endfunc


  add object oLinePB as LineMrp with uid="GRSDEDIBQX",left=3, top=155, width=420,height=1,;
    caption='',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 149529866


  add object PROGBAR as TAM_progressbar with uid="RMEVXPDIDT",left=3, top=160, width=316,height=60,;
    caption='',;
   bGlobalFont=.t.,;
    Min=0,Max=100,Value=0,bNoInsideForm=.f.,bNoMultiProgBar=.f.,Visible=.t.,;
    nPag=1;
    , HelpContextID = 149529866

  add object oStr_1_31 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=25, Top=77,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="VSKSWONFNJ",Visible=.t., Left=25, Top=53,;
    Alignment=1, Width=182, Height=18,;
    Caption="Periodo di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="UYQPTYMPSQ",Visible=.t., Left=25, Top=102,;
    Alignment=1, Width=182, Height=17,;
    Caption="Pianificazione puntuale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kpg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsco_kpg
define class LineMrp as StdBox
  Visible=.t.
  
  proc calculate
  
  endproc

  proc event(cEvent)
  
  endproc
  
  
enddefine
* --- Fine Area Manuale
