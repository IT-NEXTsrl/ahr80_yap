* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bps                                                        *
*              Previsioni di vendita settimanali                               *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-05                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Tipope,w_settimana
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bps",oParentObject,m.Tipope,m.w_settimana)
return(i_retval)

define class tgsdb_bps as StdBatch
  * --- Local variables
  Tipope = space(1)
  w_settimana = 0
  w_PVPERIOD = space(1)
  w_PVPERMES = 0
  w_PVPERSET = 0
  w_KEYSAL = space(40)
  w_PV__DATA = ctod("  /  /  ")
  w_PVQUANTI = 0
  w_PV__ANNO = 0
  w_Numsett = 0
  w_MESS = space(1)
  w_OLDQUANT = 0
  w_DIFF = 0
  w_PUNPADRE = .NULL.
  w_PUNNONNO = .NULL.
  w_OBJS = .NULL.
  w_DESARTS = space(40)
  w_DESVARS = space(40)
  w_ANNOS = 0
  w_CODARTS = space(20)
  w_PVUNIMISS = space(3)
  w_GIOR1D = ctod("  /  /  ")
  w_SETT = 0
  w_edits = .f.
  w_PRESET = 0
  * --- WorkFile variables
  VEN_PREV_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola le previsioni di vendita mensili (da GSDB_KPM
    * --- Tipo operazione
    * --- queste variabili sono utilizzate tramite le macro di VF (non � necessario definirle)
    this.w_PVPERIOD = "S"
    this.w_PV__ANNO = this.oParentObject.w_ANNOM
    this.w_Numsett = iif(this.oParentObject.w_sett5h,4,5)
    * --- padre = maschera GSDB_KPS (previsioni settimanali)
    this.w_PUNPADRE = this.oParentObject
    * --- nonno = batch GSDB_BPM (previsioni mensili)
    this.w_PUNNONNO = this.oParentObject.oParentObject
    this.w_OBJS = this.oParentObject.oParentObject.oParentObject
    do case
      case this.Tipope = "I"
        * --- Interroga
        * --- Calcola le previsioni settimanali
        * --- Select from GSDB_BPS
        do vq_exec with 'GSDB_BPS',this,'_Curs_GSDB_BPS','',.f.,.t.
        if used('_Curs_GSDB_BPS')
          select _Curs_GSDB_BPS
          locate for 1=1
          do while not(eof())
          this.w_PVPERSET = _Curs_GSDB_BPS.PVPERSET - this.oParentObject.w_SETTINI + 1
          this.w_PVQUANTI = _Curs_GSDB_BPS.PVQUANTI
          l_SETTind=ALLTRIM(STR(this.w_PVPERSET,2,0))
          this.w_PUNPADRE.w_SETT&l_SETTind = this.w_PVQUANTI
          this.w_PUNPADRE.w_SETT&l_SETTind.O = this.w_PVQUANTI
            select _Curs_GSDB_BPS
            continue
          enddo
          use
        endif
        * --- Disabilita i textbox che contengono previsioni giornaliere
        * --- Select from GSDB1BPS
        do vq_exec with 'GSDB1BPS',this,'_Curs_GSDB1BPS','',.f.,.t.
        if used('_Curs_GSDB1BPS')
          select _Curs_GSDB1BPS
          locate for 1=1
          do while not(eof())
          this.w_PVPERSET = _Curs_GSDB1BPS.PVPERSET - this.oParentObject.w_SETTINI + 1
          l_SETTind = ALLTRIM(STR(this.w_PVPERSET,2,0))
          this.w_PUNPADRE.w_SETT&l_SETTind.E = .F.
            select _Curs_GSDB1BPS
            continue
          enddo
          use
        endif
      case this.Tipope = "A"
        * --- Aggiorna le previsioni settimanali
        if this.oParentObject.w_editm AND this.oParentObject.w_PREMES<>0 AND this.oParentObject.w_RIPARTITA <> this.oParentObject.w_PREMES
          this.w_MESS = "La somma delle previsioni settimanali � diversa da quella mensile%0%0Continuo?"
          if not ah_YesNo(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- begin transaction
        cp_BeginTrs()
        * --- Se il mese � editabile di deve cancellare la previsione mensile, che corrisponde a quella della prima settimana del mese
        if this.oParentObject.w_editm
          this.w_PVPERSET = this.oParentObject.w_SETTINI
          GSDB_BCG(this,.t.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if empty(this.w_PV__DATA)
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            i_retcode = 'stop'
            return
          else
            * --- Delete from VEN_PREV
            i_nConn=i_TableProp[this.VEN_PREV_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                    +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTM);
                     )
            else
              delete from (i_cTable) where;
                    PV__DATA = this.w_PV__DATA;
                    and PVCODART = this.oParentObject.w_CODARTM;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        endif
        * --- le settimane da aggiornare sono quelle comprese tra SettIni + Numsett = <la prima sett. del mese> + <4 oppure 5>
        this.w_PVPERSET = this.w_Numsett + this.oParentObject.w_SETTINI - 1
        do while this.w_PVPERSET>=this.oParentObject.w_SETTINI
          l_SETTind = ALLTRIM(STR(this.w_PVPERSET-this.oParentObject.w_SETTINI+1,2,0))
          * --- se la previsione settimanale contiene previsioni giornaliere allora non � editabile
          if this.oParentObject.w_SETT&l_SETTind.E
            this.w_PVQUANTI = this.w_PUNPADRE.w_SETT&l_SETTind
            this.w_OLDQUANT = this.w_PUNPADRE.w_SETT&l_SETTind.O
            if this.w_PVQUANTI <> this.w_OLDQUANT
              GSDB_BCG(this,.t.)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_PVQUANTI = 0
                * --- Delete from VEN_PREV
                i_nConn=i_TableProp[this.VEN_PREV_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                        +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTM);
                         )
                else
                  delete from (i_cTable) where;
                        PV__DATA = this.w_PV__DATA;
                        and PVCODART = this.oParentObject.w_CODARTM;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
              else
                * --- Try
                local bErr_03784668
                bErr_03784668=bTrsErr
                this.Try_03784668()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_03784668
                * --- End
                * --- Write into VEN_PREV
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.VEN_PREV_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_PREV_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PVQUANTI ="+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
                      +i_ccchkf ;
                  +" where ";
                      +"PV__DATA = "+cp_ToStrODBC(this.w_PV__DATA);
                      +" and PVCODART = "+cp_ToStrODBC(this.oParentObject.w_CODARTM);
                         )
                else
                  update (i_cTable) set;
                      PVQUANTI = this.w_PVQUANTI;
                      &i_ccchkf. ;
                   where;
                      PV__DATA = this.w_PV__DATA;
                      and PVCODART = this.oParentObject.w_CODARTM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              this.w_PUNPADRE.w_SETT&l_SETTind.O = this.w_PVQUANTI
            endif
          endif
          this.w_PVPERSET = this.w_PVPERSET - 1
        enddo
        * --- commit
        cp_EndTrs(.t.)
        this.w_OBJS.NotifyEvent("Init")     
        this.oParentObject.w_PREMES = this.oParentObject.w_RIPARTITA
        this.oParentObject.w_editm = (this.oParentObject.w_PREMES = 0)
        this.w_PUNPADRE.lockscreen = .t.
        this.w_PUNPADRE.mReplace(.t.)     
        this.w_PUNPADRE.lockscreen = .f.
        this.w_PUNPADRE.ecpquit()     
      case this.Tipope="G"
        * --- Lancia la maschera delle previoni giornaliere
        this.w_CODARTS = this.oParentObject.w_CODARTM
        this.w_DESARTS = this.oParentObject.w_DESARTM
        this.w_ANNOS = this.oParentObject.w_ANNOM
        this.w_PVUNIMISS = this.oParentObject.w_PVUNIMISM
        this.w_PVPERSET = this.w_settimana
        do GSDB_BCG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_GIOR1D = this.w_PV__DATA
        this.w_SETT = this.w_PVPERSET
        l_SETTind = alltrim(str(this.w_PVPERSET-this.oParentObject.w_SETTINI+1,2,0))
        this.w_PRESET = this.w_PUNPADRE.w_SETT&l_SETTind
        this.w_edits = this.w_PUNPADRE.w_SETT&l_SETTind.E
        do GSDB_KPG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_DIFF = this.w_PRESET - this.w_PUNPADRE.w_SETT&l_SETTind
        this.w_PUNPADRE.w_SETT&l_SETTind = this.w_PRESET
        this.w_PUNPADRE.w_SETT&l_SETTind.E = this.w_edits
        this.w_PUNNONNO.w_PREMES = this.w_PUNNONNO.w_PREMES + this.w_DIFF
        this.w_PUNNONNO.w_editm = (this.w_PUNNONNO.w_PREMES = 0)
    endcase
  endproc
  proc Try_03784668()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VEN_PREV
    i_nConn=i_TableProp[this.VEN_PREV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VEN_PREV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PV__DATA"+",PVCODART"+",PVPERIOD"+",PVPERMES"+",PVQUANTI"+",PV__ANNO"+",PVPERSET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PV__DATA),'VEN_PREV','PV__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODARTM),'VEN_PREV','PVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERIOD),'VEN_PREV','PVPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERMES),'VEN_PREV','PVPERMES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVQUANTI),'VEN_PREV','PVQUANTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PV__ANNO),'VEN_PREV','PV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PVPERSET),'VEN_PREV','PVPERSET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PV__DATA',this.w_PV__DATA,'PVCODART',this.oParentObject.w_CODARTM,'PVPERIOD',this.w_PVPERIOD,'PVPERMES',this.w_PVPERMES,'PVQUANTI',this.w_PVQUANTI,'PV__ANNO',this.w_PV__ANNO,'PVPERSET',this.w_PVPERSET)
      insert into (i_cTable) (PV__DATA,PVCODART,PVPERIOD,PVPERMES,PVQUANTI,PV__ANNO,PVPERSET &i_ccchkf. );
         values (;
           this.w_PV__DATA;
           ,this.oParentObject.w_CODARTM;
           ,this.w_PVPERIOD;
           ,this.w_PVPERMES;
           ,this.w_PVQUANTI;
           ,this.w_PV__ANNO;
           ,this.w_PVPERSET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,Tipope,w_settimana)
    this.Tipope=Tipope
    this.w_settimana=w_settimana
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VEN_PREV'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSDB_BPS')
      use in _Curs_GSDB_BPS
    endif
    if used('_Curs_GSDB1BPS')
      use in _Curs_GSDB1BPS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Tipope,w_settimana"
endproc
