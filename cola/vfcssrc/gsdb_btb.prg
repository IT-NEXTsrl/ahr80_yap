* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_btb                                                        *
*              Riassegnazione time-buckets                                     *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-08-11                                                      *
* Last revis.: 2012-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_btb",oParentObject)
return(i_retval)

define class tgsdb_btb as StdBatch
  * --- Local variables
  i = 0
  w_tDATFIN = ctod("  /  /  ")
  w_tDATINI = ctod("  /  /  ")
  TmpC = space(10)
  w_GFATHER = .NULL.
  w_CATFIN = space(5)
  w_CATINI = space(5)
  w_CODFIN = space(41)
  w_CODINI = space(41)
  w_FAMAFIN = space(5)
  w_FAMAINI = space(5)
  w_GRUFIN = space(5)
  w_GRUINI = space(5)
  w_MAGFIN = space(5)
  w_MAGINI = space(5)
  w_MARFIN = space(5)
  w_MARINI = space(5)
  w_ELAODF = space(1)
  w_ELAMPS = space(1)
  w_OLDCOD = space(20)
  NumPer = 0
  * --- WorkFile variables
  MPS_TFOR_idx=0
  ODL_MAST_idx=0
  MPS_TPER_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametri
    if TYPE("this.oParentObject.oParentObject.class") = "C" and UPPER(this.oParentObject.oParentObject.class) = "TGSDB_KGF"
      this.w_GFATHER = this.oParentObject.oParentObject
    endif
    if TYPE("this.w_GFATHER.class")="C"
      * --- Se da Generazione MPS applico i filtri della maschera GSDB_KGF
      this.w_CATFIN = this.w_GFATHER.w_CATFIN
      this.w_CATINI = this.w_GFATHER.w_CATINI
      this.w_CODFIN = this.w_GFATHER.w_CODFIN
      this.w_CODINI = this.w_GFATHER.w_CODINI
      this.w_FAMAFIN = this.w_GFATHER.w_FAMAFIN
      this.w_FAMAINI = this.w_GFATHER.w_FAMAINI
      this.w_GRUFIN = this.w_GFATHER.w_GRUFIN
      this.w_GRUINI = this.w_GFATHER.w_GRUINI
      this.w_MAGFIN = this.w_GFATHER.w_MAGFIN
      this.w_MAGINI = this.w_GFATHER.w_MAGINI
      this.w_MARFIN = this.w_GFATHER.w_MARFIN
      this.w_MARINI = this.w_GFATHER.w_MARINI
      this.w_ELAODF = this.w_GFATHER.w_ELAODF
      this.w_ELAMPS = this.w_GFATHER.w_ELAMPS
    endif
    * --- Messaggio di chiusura
    if lower(this.oParentObject.class) = "tgsdb_ktb"
      * --- Sistema eventuali ordini in stato K
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("D"),'ODL_MAST','OLTSTATO');
            +i_ccchkf ;
        +" where ";
            +"OLTSTATO = "+cp_ToStrODBC("K");
               )
      else
        update (i_cTable) set;
            OLTSTATO = "D";
            &i_ccchkf. ;
         where;
            OLTSTATO = "K";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Azzera Periodo MPS
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OLCODODL"
      do vq_exec with '..\cola\exe\query\gsdb5btb',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
      +"ODL_MAST.OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
          +"OLTPERAS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC("   "),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from MPS_TFOR
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".FMCODART = "+i_cQueryTable+".FMCODART";
            +" and "+i_cTable+".FMPERASS = "+i_cQueryTable+".FMPERASS";
    
      do vq_exec with '..\cola\exe\query\gsdb6btb',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ah_msg("Riassegnazione time-buckets MPS in corso...")
    * --- Crea Array con periodi da esaminare
    dimension Periodi(100)
    dimension Datini(100)
    dimension Datfin(100)
    dimension PriGioLav(100)
    this.i = 0
    * --- Select from MPS_TPER
    i_nConn=i_TableProp[this.MPS_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
          +" where TPPERREL <> 'XXXX'";
          +" order by TPPERASS";
           ,"_Curs_MPS_TPER")
    else
      select * from (i_cTable);
       where TPPERREL <> "XXXX";
       order by TPPERASS;
        into cursor _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      select _Curs_MPS_TPER
      locate for 1=1
      do while not(eof())
      if _Curs_MPS_TPER.TPPERASS = "000"
        this.w_tDATINI = _Curs_MPS_TPER.TPDATINI
      else
        this.i = this.i+1
        Periodi(this.i) = nvl(_Curs_MPS_TPER.TPPERREL , " ")
        Datini(this.i) = nvl(_Curs_MPS_TPER.TPDATINI , cp_CharToDate("  -  -  "))
        Datfin(this.i) = nvl(_Curs_MPS_TPER.TPDATFIN , cp_CharToDate("  -  -  "))
        this.w_tDATFIN = nvl(_Curs_MPS_TPER.TPDATFIN , cp_CharToDate("  -  -  "))
        PriGioLav(this.i) = nvl(_Curs_MPS_TPER.TPPRILAV , cp_CharToDate("  -  -  "))
      endif
        select _Curs_MPS_TPER
        continue
      enddo
      use
    endif
    this.NumPer = this.i
    * --- Try
    local bErr_039BB0D0
    bErr_039BB0D0=bTrsErr
    this.Try_039BB0D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMSG("Errore in assegnazione time-buckets MPS",16)
    endif
    bTrsErr=bTrsErr or bErr_039BB0D0
    * --- End
    Release Periodi, DatIni, DatFin, PriGioLav
    * --- Messaggio di chiusura
    if lower(this.oParentObject.class) = "tgsdb_ktb"
      ah_errorMsg("Elaborazione terminata",48)
      this.oParentObject.ecpquit()     
    endif
  endproc
  proc Try_039BB0D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_OLDCOD = repl("<",20)
    * --- Select from GSDB_BTB
    do vq_exec with 'GSDB_BTB',this,'_Curs_GSDB_BTB','',.f.,.t.
    if used('_Curs_GSDB_BTB')
      select _Curs_GSDB_BTB
      locate for 1=1
      do while not(eof())
      * --- Inserisce periodo zero
      if this.w_OLDCOD <> _Curs_GSDB_BTB.OLTCODIC
        * --- Ad ogni cambio codice, crea il periodo 000 nei saldi MPS
        this.w_OLDCOD = _Curs_GSDB_BTB.OLTCODIC
        this.TmpC = "000"
        * --- Insert into MPS_TFOR
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"FMCODICE"+",FMPERASS"+",FMCODART"+",FMCODVAR"+",FMKEYSAL"+",FMFAMPRO"+",FMUNIMIS"+",FMMPSCON"+",FMMPSDAP"+",FMMPSPIA"+",FMMPSLAN"+",FMMPSSUG"+",FMMPSTOT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTCODIC),'MPS_TFOR','FMCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.TmpC),'MPS_TFOR','FMPERASS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTCOART),'MPS_TFOR','FMCODART');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTCOVAR),'MPS_TFOR','FMCODVAR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTKEYSA),'MPS_TFOR','FMKEYSAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.DPFAMPRO),'MPS_TFOR','FMFAMPRO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.ARUNMIS1),'MPS_TFOR','FMUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSCON');
          +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSDAP');
          +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSPIA');
          +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSLAN');
          +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSSUG');
          +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSTOT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'FMCODICE',_Curs_GSDB_BTB.OLTCODIC,'FMPERASS',this.TmpC,'FMCODART',_Curs_GSDB_BTB.OLTCOART,'FMCODVAR',_Curs_GSDB_BTB.OLTCOVAR,'FMKEYSAL',_Curs_GSDB_BTB.OLTKEYSA,'FMFAMPRO',_Curs_GSDB_BTB.DPFAMPRO,'FMUNIMIS',_Curs_GSDB_BTB.ARUNMIS1,'FMMPSCON',0,'FMMPSDAP',0,'FMMPSPIA',0,'FMMPSLAN',0,'FMMPSSUG',0)
          insert into (i_cTable) (FMCODICE,FMPERASS,FMCODART,FMCODVAR,FMKEYSAL,FMFAMPRO,FMUNIMIS,FMMPSCON,FMMPSDAP,FMMPSPIA,FMMPSLAN,FMMPSSUG,FMMPSTOT &i_ccchkf. );
             values (;
               _Curs_GSDB_BTB.OLTCODIC;
               ,this.TmpC;
               ,_Curs_GSDB_BTB.OLTCOART;
               ,_Curs_GSDB_BTB.OLTCOVAR;
               ,_Curs_GSDB_BTB.OLTKEYSA;
               ,_Curs_GSDB_BTB.DPFAMPRO;
               ,_Curs_GSDB_BTB.ARUNMIS1;
               ,0;
               ,0;
               ,0;
               ,0;
               ,0;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Ricerca Periodo da assegnare all'ODL in corso
      this.TmpC = "XXX"
      for this.i=1 to this.NumPer
      if _Curs_GSDB_BTB.OLTDTRIC < DatIni(this.i)
        this.TmpC = "000"
        Exit
      else
        if DatIni(this.i) <= _Curs_GSDB_BTB.OLTDTRIC and _Curs_GSDB_BTB.OLTDTRIC<= DatFin(this.i)
          * --- Compone periodo
          this.TmpC = right("000"+alltrim(str(this.i,3,0)),3)
          * --- Esce da ciclo for
          Exit
        endif
      endif
      next
      * --- Aggiorna Periodo
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC(this.TmpC),'ODL_MAST','OLTPERAS');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_GSDB_BTB.OLCODODL);
               )
      else
        update (i_cTable) set;
            OLTPERAS = this.TmpC;
            &i_ccchkf. ;
         where;
            OLCODODL = _Curs_GSDB_BTB.OLCODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Saldi MPS
      * --- Try
      local bErr_039A6BC8
      bErr_039A6BC8=bTrsErr
      this.Try_039A6BC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_039A6BC8
      * --- End
      * --- Write into MPS_TFOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_cOp1=cp_SetTrsOp(_Curs_GSDB_BTB.OLTFLCON,'FMMPSCON','_Curs_GSDB_BTB.OLTQTSAL',_Curs_GSDB_BTB.OLTQTSAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(_Curs_GSDB_BTB.OLTFLDAP,'FMMPSDAP','_Curs_GSDB_BTB.OLTQTSAL',_Curs_GSDB_BTB.OLTQTSAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(_Curs_GSDB_BTB.OLTFLPIA,'FMMPSPIA','_Curs_GSDB_BTB.OLTQTSAL',_Curs_GSDB_BTB.OLTQTSAL,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(_Curs_GSDB_BTB.OLTFLLAN,'FMMPSLAN','_Curs_GSDB_BTB.OLTQTSAL',_Curs_GSDB_BTB.OLTQTSAL,'update',i_nConn)
        i_cOp5=cp_SetTrsOp(_Curs_GSDB_BTB.OLTFLSUG,'FMMPSSUG','_Curs_GSDB_BTB.OLTQTSAL',_Curs_GSDB_BTB.OLTQTSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FMMPSCON ="+cp_NullLink(i_cOp1,'MPS_TFOR','FMMPSCON');
        +",FMMPSDAP ="+cp_NullLink(i_cOp2,'MPS_TFOR','FMMPSDAP');
        +",FMMPSPIA ="+cp_NullLink(i_cOp3,'MPS_TFOR','FMMPSPIA');
        +",FMMPSLAN ="+cp_NullLink(i_cOp4,'MPS_TFOR','FMMPSLAN');
        +",FMMPSSUG ="+cp_NullLink(i_cOp5,'MPS_TFOR','FMMPSSUG');
        +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(_Curs_GSDB_BTB.OLTQTSAL);
            +i_ccchkf ;
        +" where ";
            +"FMCODICE = "+cp_ToStrODBC(_Curs_GSDB_BTB.OLTCODIC);
            +" and FMPERASS = "+cp_ToStrODBC(this.TmpC);
               )
      else
        update (i_cTable) set;
            FMMPSCON = &i_cOp1.;
            ,FMMPSDAP = &i_cOp2.;
            ,FMMPSPIA = &i_cOp3.;
            ,FMMPSLAN = &i_cOp4.;
            ,FMMPSSUG = &i_cOp5.;
            ,FMMPSTOT = FMMPSTOT + _Curs_GSDB_BTB.OLTQTSAL;
            &i_ccchkf. ;
         where;
            FMCODICE = _Curs_GSDB_BTB.OLTCODIC;
            and FMPERASS = this.TmpC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSDB_BTB
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_039A6BC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MPS_TFOR
    i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MPS_TFOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FMCODICE"+",FMPERASS"+",FMCODART"+",FMCODVAR"+",FMKEYSAL"+",FMFAMPRO"+",FMUNIMIS"+",FMMPSCON"+",FMMPSDAP"+",FMMPSPIA"+",FMMPSLAN"+",FMMPSSUG"+",FMMPSTOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTCODIC),'MPS_TFOR','FMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.TmpC),'MPS_TFOR','FMPERASS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTCOART),'MPS_TFOR','FMCODART');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTCOVAR),'MPS_TFOR','FMCODVAR');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.OLTKEYSA),'MPS_TFOR','FMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.DPFAMPRO),'MPS_TFOR','FMFAMPRO');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDB_BTB.ARUNMIS1),'MPS_TFOR','FMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSDAP');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSPIA');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSLAN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSSUG');
      +","+cp_NullLink(cp_ToStrODBC(0),'MPS_TFOR','FMMPSTOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FMCODICE',_Curs_GSDB_BTB.OLTCODIC,'FMPERASS',this.TmpC,'FMCODART',_Curs_GSDB_BTB.OLTCOART,'FMCODVAR',_Curs_GSDB_BTB.OLTCOVAR,'FMKEYSAL',_Curs_GSDB_BTB.OLTKEYSA,'FMFAMPRO',_Curs_GSDB_BTB.DPFAMPRO,'FMUNIMIS',_Curs_GSDB_BTB.ARUNMIS1,'FMMPSCON',0,'FMMPSDAP',0,'FMMPSPIA',0,'FMMPSLAN',0,'FMMPSSUG',0)
      insert into (i_cTable) (FMCODICE,FMPERASS,FMCODART,FMCODVAR,FMKEYSAL,FMFAMPRO,FMUNIMIS,FMMPSCON,FMMPSDAP,FMMPSPIA,FMMPSLAN,FMMPSSUG,FMMPSTOT &i_ccchkf. );
         values (;
           _Curs_GSDB_BTB.OLTCODIC;
           ,this.TmpC;
           ,_Curs_GSDB_BTB.OLTCOART;
           ,_Curs_GSDB_BTB.OLTCOVAR;
           ,_Curs_GSDB_BTB.OLTKEYSA;
           ,_Curs_GSDB_BTB.DPFAMPRO;
           ,_Curs_GSDB_BTB.ARUNMIS1;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MPS_TFOR'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='MPS_TPER'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_GSDB_BTB')
      use in _Curs_GSDB_BTB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
