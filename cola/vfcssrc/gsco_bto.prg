* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bto                                                        *
*              Tracciabilit� ODL                                               *
*                                                                              *
*      Author: ZUCCHETTI TAM SPA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-17                                                      *
* Last revis.: 2015-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bto",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsco_bto as StdBatch
  * --- Local variables
  w_PARAM = space(10)
  w_stampa = .f.
  w_STAMPODL = space(1)
  w_STAMPFAS = space(1)
  w_PunPad = .NULL.
  w_NODITV = .NULL.
  Albero = .NULL.
  w_PROG = .NULL.
  w_OGGETTO = .NULL.
  w_cCursor = space(20)
  w_LDettTec = space(1)
  w_PPCENCOS = space(15)
  w_ODL = space(150)
  w_OCL = space(150)
  w_TMOVMAG = space(150)
  w_RIGMOV = space(150)
  w_OUTPUT = space(150)
  w_DICODL = space(150)
  w_DICPROD = space(150)
  w_TRA = space(150)
  w_TMOVSCA = space(150)
  w_DETRIGHE = space(150)
  w_FACOUNT = space(150)
  w_FASEST = space(150)
  w_OCF = space(150)
  w_ORD = space(150)
  w_MDM = space(150)
  w_nNODO = 0
  w_DTA = space(150)
  w_DTV = space(150)
  w_FAT = space(150)
  w_DPI = space(150)
  w_PARAM = space(1)
  w_PARAM1 = space(1)
  w_PARAM2 = space(1)
  w_SELEZI = space(1)
  TmpC = space(100)
  w_EXPANDED = space(1)
  w_DOCSER = space(10)
  pstipo = space(2)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_ANCATCOM = space(3)
  w_CONUMERO = space(15)
  TIPODEL = space(3)
  w_CI = 0
  w_CJ = 0
  w_CK = 0
  w_CX = 0
  w_CY = 0
  w_CZ = 0
  w_CW = 0
  w_LVLKEY = space(240)
  w_LVLKEY1 = space(240)
  w_LVLKEY2 = space(240)
  w_LVLKEY2_ = space(240)
  w_LVLKEY3 = space(240)
  w_LVLKEY4 = space(240)
  w_LVLKEY5 = space(240)
  w_LVLKEY6 = space(240)
  w_APPTIPO = 0
  w_APPTIPN = space(3)
  CHKKEY = .f.
  w_SINGOLO = space(1)
  w_ODLSEL = space(15)
  w_ARTICOLO = space(20)
  w_VARIANTE = space(20)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_ODLPAD = space(15)
  w_ODL1 = space(15)
  w_SERRIF1 = space(10)
  w_ROWRIF1 = 0
  w_TROVATO = space(10)
  w_CODODL = space(15)
  w_ODLFAS = space(15)
  * --- WorkFile variables
  PAR_PROD_idx=0
  KEY_ARTI_idx=0
  DOC_DETT_idx=0
  MVM_MAST_idx=0
  TMPODL_MAST_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tracciabilit� ODL da (gsco_KTO e gsco_AOL)
    * --- Parametro di elaborazione
    this.w_STAMPODL = "N"
    this.w_STAMPFAS = "N"
    * --- Caller
    * --- Locali
    MSGM="(manuale)" 
 LABODLMSG="Art: %1 (%2)" 
 LABMMSG="Doc: %1. Tip. doc: documento. Causale mag.:%2 (%3)" 
 LABRIMSG="Riga n.: %1 (%2) art: %3 (%4)" 
 LABDDPMSG="Dichiarazione di produzione del: %1" 
 LABMOSMSG="Reg. n.:%1 del %2 n. doc. %3" 
 LABMRSMSG="Causale magazzino: %1 quantit�: %2 art: %3 (%4)"
    labelT=AH_MsgFormat("Trasferimento") 
 labelD=AH_MsgFormat("Dichiarazioni") 
 labelF=AH_MsgFormat("Gestione sottoalbero") 
 labelODL="olcododl + '('+allt(trans(OLTQTODL,v_pq(12)))+' '+nvl(OLTUNMIS,'')+')'+' '+AH_MsgFormat(LABODLMSG, allt(OLTCODIC), allt(ARDESART))" 
 labelODF="padre + '('+allt(trans(OLTQTODL,v_pq(12)))+' '+nvl(OLTUNMIS,'')+')'+' '+AH_MsgFormat(LABODLMSG, allt(OLTCODIC), allt(ARDESART))" 
 labelM="AH_MsgFormat(LABMMSG, allt(trans(MVNUMREG,v_pq(4))), MVCAUMAG, allt(ARDESART))+' '+AH_Msgformat(iif(MVTIPDOC ='M',MSGM,iif(MVTIPDOC='A' ,MSGA,' ' ) ))" 
 labelRI="AH_MsgFormat(LABRIMSG,allt(trans(CPROWNUM,v_pq(4))), allt(trans(OLTQTODL,v_pq(12)))+' '+nvl(OLTUNMIS,''), allt(OLTCODIC), allt(ARDESART))+' ' +AH_Msgformat(iif(MVTIPDOC='M',MSGM,iif(MVTIPDOC ='A' ,MSGA,' ' ) ))" 
 labelDDP="AH_MsgFormat(LABDDPMSG,alltrim(dtoc( oltdinric)))+' '+ iif(!empty(MVNUMDOC), AH_MsgFormat(LABDDP2MSG,allt(trans(MVNUMDOC,v_pq(4))),allt(cicdescri)), '' )" 
 labelDDS='AH_MsgFormat(LABDDPMSG + " (Rif dichiarazione n. %2)" ,alltrim(dtoc( oltdinric)), alltrim(padre))' 
 labelDDO="AH_MsgFormat('Dichiarazioni con ODL (raccolta dati)')+' '+ iif(!empty(MVNUMDOC), AH_MsgFormat(LABDDP2MSG,allt(trans(MVNUMDOC,v_pq(4))),allt(cicdescri)), '' )" 
 labelDRD="AH_MsgFormat('Dichiarazioni con ODL (raccolta dati)')+' '+ iif(!empty(MVNUMDOC), AH_MsgFormat(LABDDP2MSG,allt(trans(MVNUMDOC,v_pq(4))),allt(cicdescri)), '' )" 
 labelMOS= "AH_MsgFormat(LABMOSMSG,allt(trans (MVNUMREG,v_pq(4))), alltrim(dtoc( oltdinric)), allt(MVSERIAL))" 
 labelMRS ="AH_MsgFormat(LABMRSMSG, MVCAUMAG, allt(trans(oltqtodl,v_pq(12))), allt(OLTCODIC), allt(ARDESART))"
    * --- Assegnamenti
    *     -----------------------
    * --- Puntatore alla Maschera
    this.w_PUNPAD = this.oParentObject
    * --- Tree-view
    this.Albero = this.oParentObject.w_TREEV
    * --- Cursore Tree-view
    this.w_cCursor = this.Albero.cCursor
    * --- Nodi Tree-view
    this.w_NODITV = this.Albero.oTree
    * --- Tipo Esplosione
    this.w_SINGOLO = "N"
    * --- Gestione apertura multipla maschera gsco_kto (cambio il nome del cursore tutte le volte che apro la tree-view)
    if this.w_PARAM="Tree-View"
      if alltrim(upper(this.w_PUNPAD.Class))="TGSCO_KTO"
        this.w_PunPad.oPgFrm.ActivePage = 2
      endif
    else
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --gestione ODL
    if this.w_PARAM="Tree-View" or this.w_PARAM="Page2"
      if used(this.w_cCursor)
        use in (this.w_cCursor)
      endif
      this.w_cCursor = sys(2015)
      this.Albero.cCursor = this.w_cCursor
      * --- Inizializzo la variabile di gsco_KTO
      this.oParentObject.w_FirstTime = .T.
    endif
    * --- Bitmap per tree-view ODL
    * --- ------------------------------------------------------
    * --- --odl
    this.w_ODL = padr(".\bmp\odll.bmp",150)
    * --- --ocl
    this.w_OCL = padr(".\bmp\ocl.bmp",150)
    * --- --fase trasferimento
    this.w_TRA = padr(".\bmp\mdm.bmp",150)
    * --- --Fasi output
    this.w_OUTPUT = padr(".\bmp\fase.bmp",150)
    * --- --Fase count point
    this.w_FACOUNT = padr(".\bmp\batch.bmp",150)
    * --- --Fase esterna
    this.w_FASEST = padr(".\bmp\ocl.bmp",150)
    * --- ----------------------------------------------------------
    * --- --Testata movimenti di magazzino
    this.w_TMOVMAG = padr(".\bmp\documenti.bmp",150)
    * --- --Righe movimenti di trasferimento
    this.w_RIGMOV = padr(".\bmp\ddttra.bmp",150)
    * --- --Fasi dichiarazione odl raccolta
    this.w_DICODL = padr(".\bmp\fattura1.bmp",150)
    * --- --Fasi dichiarazione di produzione
    this.w_DICPROD = padr(".\bmp\mdm.bmp",150)
    * --- --Movimenti di scarico a fornitore
    this.w_TMOVSCA = padr(".\bmp\documenti.bmp",150)
    * --- --Dettaglio righe trasferimento
    this.w_DETRIGHE = padr(".\bmp\ddttra.bmp",150)
    * --- --SI CATTURA IL VERIFICARSI DEGLI EVENTI SULLA MASCHERA 
    do case
      case this.w_PARAM="Tree-View" or this.w_PARAM="Page2" 
        * --- --CHIUSURA TABELLA TEMPORANEA
        * --- Drop temporary table TMPODL_MAST
        i_nIdx=cp_GetTableDefIdx('TMPODL_MAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPODL_MAST')
        endif
        * --- Scelgo la query da utilizzare.
        * --- --NON HO NESSUN FILTRO
        * --- --Solo dichiarazioni ava_prod
        if empty(this.oParentObject.w_TIPMAG) and ((this.oParentObject.w_NUMINI=0 and this.oParentObject.w_NUMFIN=0) ; 
 or (this.oParentObject.w_NUMINI=1 and this.oParentObject.w_NUMFIN=999999)) and empty(this.oParentObject.w_DOCINI) and empty(this.oParentObject.w_DOCFIN) and empty(this.oParentObject.w_DATINI) and ; 
 empty(this.oParentObject.w_DATFIN) and ((this.oParentObject.w_NUMREI=0 and this.oParentObject.w_NUMREF=0) or (this.oParentObject.w_NUMREI=1 and this.oParentObject.w_NUMREF=999999)) and empty(this.oParentObject.w_CODMAG) ; 
 and empty(this.oParentObject.w_DBCODINI) and empty(this.oParentObject.w_DBCODFIN) ; 
 and empty(this.oParentObject.w_FAMAINI)and empty(this.oParentObject.w_FAMAFIN); 
 and empty(this.oParentObject.w_GRUINI)and empty(this.oParentObject.w_GRUFIN)and empty(this.oParentObject.w_CATINI)and empty(this.oParentObject.w_CATFIN)and empty(this.oParentObject.w_CODCOM)and empty(this.oParentObject.w_CODATT)
          * --- Eseguo la query senza alcun filtro su Documenti e Movimenti di Magazzino
          * --- Create temporary table TMPODL_MAST
          i_nIdx=cp_AddTableDef('TMPODL_MAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_NYIIOIWKVZ[1]
          indexes_NYIIOIWKVZ[1]='OLCODODL'
          vq_exec('gsco_KTOL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_NYIIOIWKVZ,.f.)
          this.TMPODL_MAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- --ho alcuni filtri
          * --- Create temporary table TMPODL_MAST
          i_nIdx=cp_AddTableDef('TMPODL_MAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_MGBJECQECN[1]
          indexes_MGBJECQECN[1]='OLCODODL'
          vq_exec('gsco2KTOL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_MGBJECQECN,.f.)
          this.TMPODL_MAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_PARAM="Done" and used(this.w_cCursor) 
        * --- Rilascio la tabella temporanea
        * --- Drop temporary table TMPODL_MAST
        i_nIdx=cp_GetTableDefIdx('TMPODL_MAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPODL_MAST')
        endif
        * --- Chiudo il cursore della tree-view
        use in (this.w_cCursor)
        * --- Rilascio anche popmenu
        DEACTIVATE POPUP MenuTV
        RELEASE POPUPS MenuTV EXTENDED
        if USED("_curs_")
          Select _curs_ 
 USE
        endif
      case this.w_PARAM="PopUp" or this.w_PARAM="PopRid" 
        * --- --Gestione del PopUp personalizzato
        Private Azione 
 store space(10) to Azione
        if .f.
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_PARAM="EXPNODE"
        select (this.w_cCursor)
        if not empty(nvl(this.Albero.nKeyExpanded," "))
          if seek(substr(this.Albero.nKeyExpanded,2))
            this.w_EXPANDED = collapsed
            this.w_ODLSEL = OLCODODL
            this.w_LVLKEY = LVLKEY
            this.pstipo = TIPO
            this.w_SERRIF = MVSERIAL
            this.w_ROWRIF = CPROWNUM
            this.w_ODLPAD = PADRE
            if this.w_EXPANDED<>"+"
              * --- --non occorre esplodre il sottoalbero
              select (this.w_cCursor) 
 replace collapsed with "+"
            endif
          endif
        endif
        if this.oParentObject.w_FirstTime and this.w_EXPANDED<>"+" and this.pstipo $ "TRA-TRF-MOV-FOT-DDO-DIC-DIM-FCP-FSE-DDP-FCP-MOS-FDO-DMT-DPC-DPF-DPT-DCC-DCF"
          * --- Esplosione livello per livello - come distinta base
          do case
            case this.pstipo ="TRA"
              * --- --DOCUMENTI DI TRASFERIMENTO
              vq_exec("..\cola\exe\query\gsco3lto", this, "_dett_")
              this.TIPODEL = "D_T"
            case this.pstipo="MOV"
              this.w_ODLFAS = iif(not empty(NVL(OLTCODIC,"")),left(OLTCODIC,15),this.w_ODLSEL)
              * --- MOVIMENTI TRASFERIMENTO
              vq_exec("..\cola\exe\query\gsco_lto2", this, "_dett_")
              this.TIPODEL = "M_V"
            case this.pstipo="DDP"
              * --- DCHIARAZIONI AVAPROD
              vq_exec("..\cola\exe\query\gsco_lto6", this, "_dett_")
              this.TIPODEL = "D_P"
            case this.pstipo="MOS"
              * --- --DICHIARAZIONE AVAPROD MOVIMENTI
              vq_exec("..\cola\exe\query\gsco_lto7", this, "_dett_")
              this.TIPODEL = "D_D"
            case this.pstipo="DPC"
              * --- --DICHIARAZIONI DI PRODUZIONI SENZA CICLI
              vq_exec("..\cola\exe\query\gsco17lto", this, "_dett_")
              this.TIPODEL = "P_C"
            case this.PSTIPO="DCC"
              * --- --Dichiarazione di produzione senza cicli
              vq_exec("..\cola\exe\query\gsco13lto", this, "_dett_")
              this.TIPODEL = "C_C"
            case this.PSTIPO="DPT"
              * --- --TESTATA DICHIARAZIONE DI PRODUZIONE
              vq_exec("..\cola\exe\query\gsco15lto", this, "_dett_")
              this.TIPODEL = "P_T"
            case this.pstipo="DPF"
              * --- --DICHIARAZIONI DI PRODUZIONI CON CICLI
              vq_exec("..\cola\exe\query\gsco19lto", this, "_dett_")
              this.TIPODEL = "P_C"
            case this.PSTIPO="DCF"
              * --- --Dichiarazione di produzione con cicli
              vq_exec("..\cola\exe\query\gsco21lto", this, "_dett_")
              this.TIPODEL = "C_C"
            case this.pstipo ="TRF"
              * --- --DOCUMENTI DI TRASFERIMENTO FASI
              vq_exec("..\cola\exe\query\gsco23lto", this, "_dett_")
              this.TIPODEL = "D_T"
          endcase
          if this.pstipo $"TRA-TRF-FOT-FCP- MOV-DDO-DIM-DDP-MOS-DMT-DRD-FSE-DPC-DPF-DPT-DCC-DCF"
            * --- Sto esplodendo il primo livello devo cancellare il figlio fittizio
            if this.pstipo="MOV"
              select (this.w_cCursor) 
 scan for OLCODODL=this.w_ODLSEL and PADRE=this.w_ODLPAD and tipo=this.tipodel and CPROWNUM=this.w_ROWRIF and MVSERIAL=this.w_SERRIF
              this.Albero.DelElement(alltrim("k"+lvlkey))     
              delete 
 endscan
            else
              select (this.w_cCursor) 
 scan for OLCODODL=this.w_ODLSEL and PADRE=this.w_ODLPAD and tipo=this.tipodel and CPROWNUM=this.w_ROWRIF
              this.Albero.DelElement(alltrim("k"+lvlkey))     
              delete 
 endscan
            endif
          endif
          if Used("_dett_") and RecCount("_dett_")>0
            select *, space(240) as lvlkey, space(150) as CPBmpName, space(250) as Descri, "-" as collapsed from _dett_ order by 1,2,15,25 into cursor _tree_ 
 =wrcursor("_tree_") 
 
            * --- --modifico l'albero aggiungendo i figli a MOV
            this.w_CK = 1
            this.w_CI = 1
            this.w_CY = 1
            this.w_CX = 1
            this.w_CW = 1
            this.w_CJ = 1
            this.w_CZ = 1
            activecursor=this.w_cCursor
            if used("_dett_")
              use in _dett_
            endif
            select _tree_ 
 go top 
 scan
            do case
              case livello=2
                if tipo $"MOV-DDP-FCP-DRD-FSC"
                  this.w_LVLKEY1 = alltrim(this.w_LVLKEY)+ "." + right("00000000"+alltrim(str(this.w_CK)),9)
                  replace lvlkey with this.w_LVLKEY1
                  this.w_CK = this.w_CK + 1
                  this.w_CW = 1
                else
                  if tipo$"DDO-FDO-DCC-DCF"
                    this.w_LVLKEY2 = Alltrim(this.w_LVLKEY)+ "." + right("00000000"+alltrim(str(this.w_Ck)),9)
                    this.w_Ck = this.w_Ck+1
                    this.w_CX = 1
                    replace lvlkey with this.w_LVLKEY2
                  endif
                endif
              case livello=3
                if tipo$"M_V-D_P-C_D"
                  this.W_LVLKEY2 = this.w_LVLKEY1+"."+right("00000000"+alltrim(str(this.w_CW)),9)
                  replace lvlkey with this.w_LVLKEY2
                  this.w_CW = this.w_CW+ 1
                else
                  if tipo$"D_O-C_C"
                    * --- --FIGLIO DDO
                    this.w_LVLKEY3 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CX)),9)
                    this.w_CX = this.w_CX+ 1
                    replace lvlkey with this.w_LVLKEY3
                  else
                    if TIPO$"RIG-MOS-DIR-DPT" 
 
                      this.w_LVLKEY3 = ALLTRIM(this.w_LVLKEY)+"."+right("00000000"+alltrim(str(this.w_CW)),9)
                      this.w_CW = this.w_CW+ 1
                      replace lvlkey with this.w_LVLKEY3
                      this.w_CZ = 1
                    else
                      if tipo="DIM"
                        this.w_LVLKEY2 = Alltrim(this.w_LVLKEY)+"."+right("00000000"+alltrim(str(this.w_CK)),9)
                        this.w_CK = this.w_CK+ 1
                        this.w_CJ = 1
                        * --- --DIM
                        replace lvlkey with this.w_LVLKEY2
                      endif
                    endif
                  endif
                endif
              case livello=4
                if TIPO$"MRS-DIR-DPM"
                  this.W_LVLKEY3 = ALLTRIM(this.w_LVLKEY)+"."+right("00000000"+alltrim(str(this.w_CY)),9)
                  replace lvlkey with this.w_LVLKEY3
                  this.w_CY = this.w_CY + 1
                else
                  if tipo="DMT"
                    this.w_LVLKEY4 = Alltrim(this.w_LVLKEY)+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
                    replace lvlkey with this.w_LVLKEY4
                    this.w_CJ = this.w_CJ + 1
                    this.w_CX = 1
                  else
                    if tipo="D_R"
                      this.w_LVLKEY4 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
                      replace lvlkey with this.w_LVLKEY4
                      this.w_CJ = this.w_CJ + 1
                    else
                      if tipo$"D_D-D_T-P_T"
                        this.w_LVLKEY4 = this.w_LVLKEY3+"."+right("00000000"+alltrim(str(this.w_CZ)),9)
                        replace lvlkey with this.w_LVLKEY4
                        this.w_CZ = this.w_CZ + 1
                      endif
                    endif
                  endif
                endif
              case livello=5
                if tipo $ "D_T"
                  this.w_LVLKEY5 = this.w_LVLKEY4+"."+right("00000000"+alltrim(str(this.w_CX)),9)
                  replace lvlkey with this.w_LVLKEY5
                  this.w_CX = this.w_CX + 1
                else
                  * --- --DMM
                  this.w_LVLKEY5 = ALLTRIM(this.w_LVLKEY)+"."+right("00000000"+alltrim(str(this.w_CX)),9)
                  replace lvlkey with this.w_LVLKEY5
                  this.w_CX = this.w_CX + 1
                endif
            endcase
            endscan
            select _tree_ 
 go top
            SCAN
            do case
              case tipo$"D_T - D_P  - M_V- D_O - D_M- D_R-D_D-C_D-D_D-P_T-C_C"
                * --- --Figlio fittizio
                replace Descri with labelF, CPBmpName with this.w_DICODL
              case tipo$"RIG-DMM-MRS-DPM"
                * --- --Righe di Movimento per trasferimento
                replace Descri with &labelRI, CPBmpName with this.w_RIGMOV
              case tipo$"DDP-DIM-FDO-DIR"
                * --- --Dichiarazione di produzione DIC_PROD
                replace Descri with &labelDDP, CPBmpName with this.w_DICPROD
              case tipo$"MOS-DMT-MOV-DPT"
                * --- --Testata di movimento di magazzino trasferimento
                replace Descri with &labelM, CPBmpName with this.w_TMOVSCA
              case tipo="DDO"
              case TIPO="FSC"
              case TIPO$"DCC-DCF"
                * --- --Dichiarazione di produzione DIC_PROD
                replace Descri with &labelDDS, CPBmpName with this.w_DICPROD
              case tipo="DRD"
            endcase
            endscan
            select _tree_ 
 scan 
 scatter memvar 
 riga=recno() 
 insert into &activecursor from memvar 
 select &activecursor
            this.Albero.AddElement(alltrim("k"+left(lvlkey,rat(".",lvlkey)-1)),alltrim("k"+lvlkey))     
            select _tree_ 
 go riga
            endscan
            if used("_tree_")
              use in _tree_
            endif
          endif
        endif
    endcase
    do case
      case this.w_PARAM="Gestione"
        if this.oParentObject.w_LEVEL>0 and this.oParentObject.w_TYPE<>"ODF"
          if this.oParentObject.w_TYPE$"MOV-MOS-RIG-MRS-DMM-DMT-DPT-DPM"
            this.w_PROG = GSAR_BZM(this, this.oParentObject.w_CODODL1, -20)
            Local L_cTrsName 
 L_cTrsName=this.w_PROG.cTrsname 
 Select (L_cTrsName) 
 Go Top 
 Locate For CPROWNUM= this.oParentObject.w_ROWNUM
            if Found()
              this.w_PROG.oPgFrm.Page1.oPag.oBody.Refresh()     
              this.w_PROG.WorkFromTrs()     
              this.w_PROG.SaveDependsOn()     
              this.w_PROG.SetControlsValue()     
              this.w_PROG.mHideControls()     
              this.w_PROG.ChildrenChangeRow()     
              l_prova=this.w_prog.cfunction
              this.w_PROG.cFunction = "Edit"
              this.w_PROG.oNewFocus = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
              this.w_PROG.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.GotFocus()     
              this.w_prog.cfunction=l_prova
            endif
          else
            if this.oParentObject.w_TYPE$"DDP-FDO-DIM-DIR-DCC-DCF"
              * --- Apertura Dichiarazione di produzione
              if this.oParentObject.w_TYPE<>"DCF"
                this.w_PROG = GSCO_ADP()
              else
                this.w_PROG = GSCI_ADP()
              endif
              * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
              if !(this.w_PROG.bSec1)
                i_retcode = 'stop'
                return
              endif
              * --- Carico il Record.
              if this.oParentObject.w_TYPE$"DIM-DIR" and not this.oParentObject.w_FirstTime
                this.w_PROG.w_DPSERIAL = this.oParentObject.w_RIF3
                this.oParentObject.w_RIF = this.oParentObject.w_RIF3
              else
                this.w_PROG.w_DPSERIAL = this.oParentObject.w_RIF
              endif
              this.w_PROG.QueryKeySet("DPSERIAL='"+ this.oParentObject.w_RIF +"'" )     
              this.w_PROG.LoadRecWarn()     
            endif
          endif
        else
          * --- --Ho un codice ODL apertura gestione
          * --- Numrif -40 odl/ocl
          GSAR_BZM(this,this.oParentObject.w_CODODL1, -40, this.oParentObject.w_CODODL_ )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_PARAM="Esplosione"
        if this.oParentObject.w_FirstTime
          * --- L'esplosione avviene per la prima volta, devo calcolare tutti i sottolivelli dell'albero
          if used(this.w_cCursor)
            use in (this.w_cCursor)
          endif
          this.w_cCursor = sys(2015)
          this.Albero.cCursor = this.w_cCursor
          vq_exec("..\COLA\exe\query\gsco_fto", this, "_curs_")
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Albero.ExpandAll(.T.)
          if used(this.w_cCursor)
            update (this.w_cCursor) set collapsed="+" where collapsed="-"
          endif
        else
          * --- L'albero � gi� pronto, mi limito ad espanderne le foglie
          this.Albero.ExpandAll(.T.)
          if used(this.w_cCursor)
            update (this.w_cCursor) set collapsed="+" where collapsed="-"
          endif
        endif
      case this.w_PARAM="Implosione"
        this.Albero.ExpandAll(.F.)
      case this.w_PARAM="Chiudi ODL"
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPLOGTEC,PPCENCOS"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPLOGTEC,PPCENCOS;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LDettTec = NVL(cp_ToDate(_read_.PPLOGTEC),cp_NullValue(_read_.PPLOGTEC))
          this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CODODL = this.oParentObject.w_cododl_
        this.w_PARAM = "K"
        this.TmpC = "Si � scelto di chiudere l'ODL n. %1%0Si � certi di voler proseguire l'operazione?"
        this.w_PARAM1 = " "
        this.w_PARAM2 = " "
        this.w_SELEZI = "Z"
        if ah_YesNo(this.TmpC,"",this.w_CODODL)
          vq_exec("..\COLA\exe\query\gsmr_kpe", this, "GSCOSBCL")
          gsco_bcl(this,"AG", "I")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          USE IN SELECT("GSCOSBCL")
        endif
      case this.w_PARAM="Piano di generazione"
        * --- Apertura Piano Generazione.
        this.w_PROG = GSCO_AMG("I" , "BP")
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il Record.
        this.w_PROG.w_PDSERIAL = LEFT(this.oParentObject.w_RIF + SPACE(10), 10)
        this.w_PROG.w_PDTIPGEN = "BP"
        this.pstipo = "BP"
        this.w_PROG.QueryKeySet("PDSERIAL='"+ LEFT(this.oParentObject.w_RIF + SPACE(10), 10)+"'"+" and PDTIPGEN='"+ this.PSTIPO +"'")     
        this.w_PROG.LoadRecWarn()     
      case this.w_PARAM="Tracciabilit�"
        * --- Apertura tracciabilit� OCL per fasi esterne
        this.w_PROG = GSCL_KTO()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_OLCODINI = this.oParentObject.w_CODODL_
        this.w_PROG.w_OLCODFIN = this.oParentObject.w_CODODL_
        gscl_bto(this.w_prog, "Tree-View")
    endcase
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Tree-View
    * --- Elabora l'albero per la visualizzazione
    vq_exec("..\COLA\exe\query\gsco_sto", this, "_curs_")
    if this.oParentObject.w_FirstTime
      if Used("_curs_") and RecCount("_curs_")>0
        select _curs_ 
 go top 
 scan
        * --- --AGGIUNGO IL +
        if Not tipo $ "ODL-ODF-D_T - D_F - D_P - D_S- P_C"
          this.w_APPTIPO = iif(tipo="TRA",2,iif(tipo="TRF",3,iif(tipo="FOT",4,iif (tipo="FCP",6,iif(tipo="FSE",8,iif(tipo="DPF",11,10))))))
          this.w_APPTIPN = iif(tipo$"TRA-TRF","D_T",iif( tipo="FOT", "D_F",iif(tipo="FCP","D_P",iif(tipo="FSE","D_S","P_C"))))
          CODL=OLCODODL 
 ORDINE=PADRE 
 RCD=recno() 
 append blank
          replace OLCODODL with CODL, PADRE with ORDINE, LIVELLO with iif(this.w_APPTIPO=3 or this.w_APPTIPO=11,3,2), TIPO with this.w_APPTIPN, POSIZIONE with this.w_APPTIPO 
 go RCD
        endif
        endscan
        select *, space(240) as lvlkey, space(150) as CPBmpName, space(250) as Descri from _curs_ order by 1,2,26 into cursor _tree_ 
 =wrcursor("_tree_")
        * --- Calcolo il lvlkey
        SCAN
        do case
          case livello=0
            * --- ODL codice si trova al livello zero
            this.w_CI = this.w_CI + 1
            this.w_CJ = 1
            this.w_CK = 0
            this.CHKKEY = .F.
            this.w_LVLKEY = right("00000000"+alltrim(str(this.w_CI)),9)
            replace lvlkey with this.w_LVLKEY
          case livello=1
            * --- TRA, FOT,FCP,FSE,DPC
            this.w_LVLKEY1 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
            replace lvlkey with this.w_LVLKEY1
            this.w_CJ = this.w_CJ + 1
            this.w_CK = 1
            this.CHKKEY = .T.
          case livello=2
            this.w_LVLKEY2 = this.w_LVLKEY1+"."+right("00000000"+alltrim(str(this.w_CK)),9)
            replace lvlkey with this.w_LVLKEY2
            this.w_CK = this.w_CK + 1
            this.w_CX = 1
          case livello=3
            this.w_LVLKEY3 = this.w_LVLKEY2+"."+right("00000000"+alltrim(str(this.w_CX)),9)
            replace lvlkey with this.w_LVLKEY3
            this.w_CX = this.w_CX + 1
            this.w_CZ = 1
        endcase
        ENDSCAN
        select _tree_ 
 go top
        SCAN
        do case
          case tipo$"D_T-D_F-D_P-D_S-P_C"
            replace Descri with labelF, CPBmpName with this.w_DICODL
          case TIPO="ODL"
            replace Descri with &labelODL, CPBmpName with this.w_ODL
          case tipo$"TRA-TRF"
            replace Descri with labelT, CPBmpName with this.w_TRA
          case tipo$"DPC-DPF"
            replace Descri with labelD, CPBmpName with this.w_DICPROD
          case TIPO="ODF"
            replace Descri with &labelODF, CPBmpName with iif(oltprove="L",this.w_OCL,this.w_ODL)
        endcase
        endscan
      else
        * --- Cursore Vuoto
        if used("_curs_")
          use in _curs_
        endif
        if used("_tree_")
          use in _tree_
        endif
        ah_ErrorMsg("Non ci sono dati da visualizzare",48)
        * --- Sbianco la tree-view
        this.w_NODITV.Nodes.Clear
        i_retcode = 'stop'
        return
      endif
      * --- Prendo solo i padri ed esplodo volta per volta
      activecursor=this.w_cCursor
      if used("_tree_")
        Select *,"-" as collapsed from _tree_ order by lvlkey into cursor &activecursor
        use in _tree_
        if alltrim(upper(this.w_PUNPAD.Class))="TGSCO_KTO"
          * --- --
        else
          this.w_PunPad.oPgFrm.ActivePage = 9
        endif
      endif
      * --- Indicizzo il cursore e faccio il refresh della tree-view sulla maschera
      if used(this.w_cCursor)
        select (this.w_cCursor) 
 =wrcursor(this.w_cCursor) 
 index on lvlkey tag lvlkey COLLATE "MACHINE"
      else
        * --- Sbianco la tree-view
        this.w_NODITV.Nodes.Clear
        * --- Sbianco le variabili della maschera
      endif
    endif
    this.w_PUNPAD.NotifyEvent("updtreev")     
    if alltrim(upper(this.w_PUNPAD.Class))<>"TGSCO_KTO"
      this.w_PunPad.mhidecontrols()     
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce SHORTCUT
    DEFINE POPUP MenuTV from MRow()+1,MCol()+1 shortcut margin
    if this.w_PARAM="PopUp"
      if this.oParentObject.w_TYPE$ "ODL-ODF-MOV-RIG-DDP-MOS-MRS-DDO-DIM-DMT-DRD-DIR-FDO-FSC-DCC-DCF-DPM-DPT-DMM"
        DEFINE BAR 1 OF MenuTV prompt AH_Msgformat("Apertura gestione")
      else
        DEFINE BAR 1 OF MenuTV prompt AH_Msgformat("Apertura gestione") skip
      endif
      DEFINE BAR 2 OF MenuTV prompt AH_Msgformat("Esplosione albero")
      DEFINE BAR 3 OF MenuTV prompt AH_Msgformat("Implosione albero")
      DEFINE BAR 4 OF MenuTV prompt "\-"
      if (this.oParentObject.w_ST $ " L - P " and this.oParentObject.w_TYPE$"ODL") 
        * --- La chiusura pu� essere effettuata solo su ODL Lanciati/Pianificati e OCL Lanciati
        DEFINE BAR 5 OF MenuTV prompt AH_Msgformat("Chiudi ODL")
      else
        DEFINE BAR 5 OF MenuTV prompt AH_Msgformat("Chiudi ODL") skip
      endif
      if this.oParentObject.w_TYPE $ "MOV-RIG" And VAL(this.oParentObject.w_CODODL1)<>VAL(this.oParentObject.w_RIF)
        DEFINE BAR 6 OF MenuTV prompt AH_Msgformat("Piano di generazione")
      else
        DEFINE BAR 6 OF MenuTV prompt AH_Msgformat("Piano di generazione") skip
      endif
      if this.oParentObject.w_TYPE="ODF" and this.oParentObject.w_PV="L"
        DEFINE BAR 7 OF MenuTV prompt AH_Msgformat("Tracciabilit� OCL")
      else
        DEFINE BAR 7 OF MenuTV prompt AH_Msgformat("Tracciabilit� OCL") skip
      endif
    else
      * --- Pop-up Ridotto
      DEFINE BAR 1 OF MenuTV prompt AH_Msgformat("Esplosione albero")
      DEFINE BAR 2 OF MenuTV prompt AH_Msgformat("Implosione albero")
    endif
    if this.w_PARAM="PopUp"
      ON SELE BAR 1 OF MenuTV Azione="Gestione"
      ON SELE BAR 2 OF MenuTV Azione="Esplosione"
      ON SELE BAR 3 OF MenuTV Azione="Implosione"
      ON SELE BAR 5 OF MenuTV Azione="Chiudi ODL"
      ON SELE BAR 6 OF MenuTV Azione="Piano di generazione"
      ON SELE BAR 7 OF MenuTV Azione="Tracciabilit�"
    else
      * --- Pop-up Ridotto
      ON SELE BAR 1 OF MenuTV Azione="Esplosione"
      ON SELE BAR 2 OF MenuTV Azione="Implosione"
    endif
    ACTI POPUP MenuTV
    DEACTIVATE POPUP MenuTV
    RELEASE POPUPS MenuTV EXTENDED
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Esplosione odl
    if Used("_curs_") and RecCount("_curs_")>0
      vq_exec("..\COLA\exe\query\gsco1fto", this, "_dett_")
      select Olcododl,Padre,Oltstato,Oltcodic,Ardesart + space(10) as Ardesart,Oltdinric,; 
 Oltdtric,Oltunmis,Oltqtodl,Oltqtoev,Oltqtobf,Oltcofor,Oltcomag,Mvserial as Mvserial,Mvnumreg,CPROWNUM,Mvtipdoc,; 
 Mvcladoc,Mvcaumag,Mvflveac,Mvnumdoc,Mvalfdoc,Tipo,Livello,Posizione, space(240) as lvlkey,; 
 space(150) as CPBmpName, space(250) as Descri, oltprove, ordfas from _curs_ union ; 
 select Olcododl,Padre,Oltstato,Oltcodic,Ardesart,Oltdinric,; 
 Oltdtric,Oltunmis,Oltqtodl,Oltqtoev,Oltqtobf,Oltcofor,Oltcomag,Mvserial,Mvnumreg,CPROWNUM,Mvtipdoc,; 
 Mvcladoc,Mvcaumag,Mvflveac,Mvnumdoc,Mvalfdoc,Tipo,Livello,Posizione, space(240) as lvlkey, space(150) as CPBmpName,; 
 space(250) as Descri, oltprove, ordfas from _dett_ order by 1,30,2,25,14,24,16 into cursor _tree_ 
 =wrcursor("_tree_")
      * --- Elimino righe duplicate del Piano di generazione (sono riferite a due righe diverse, eliminandole tutte esclusa la prima che trovo le raggruppo)
      scan
      * --- Calcolo la chiave della tree-view (lvlkey)
      do case
        case livello=0
          * --- ODL 
          this.w_CI = this.w_CI + 1
          this.w_CJ = 1
          this.w_CK = 0
          this.CHKKEY = .F.
          this.w_LVLKEY = right("00000000"+alltrim(str(this.w_CI)),9)
          replace lvlkey with this.w_LVLKEY
        case livello=1
          if tipo$ "TRA-FOT-FCP-FSE-DPC-ODF"
            * --- TRA, FOT,FCP,FSE,DPC,ODF
            this.w_LVLKEY1 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
            replace lvlkey with this.w_LVLKEY1
            this.w_CJ = this.w_CJ + 1
            this.w_CK = 1
            this.CHKKEY = .T.
          endif
          * --- TRASFERIMENTO, FASI OUTPUT, COUNT POINT, ESTERNE DICHIARAZIONI
        case livello=2
          * --- --MOV-DDO-DDP-DRD-FDO-FSC-DPP-TRF
          this.w_LVLKEY2 = alltrim(this.w_LVLKEY1)+ "." + right("00000000"+alltrim(str(this.w_CK)),9)
          replace lvlkey with this.w_LVLKEY2
          this.w_CK = this.w_CK + 1
          this.w_CW = 1
        case livello=3
          this.w_LVLKEY3 = ALLTRIM(this.w_LVLKEY2)+"."+right("00000000"+alltrim(str(this.w_CW)),9)
          this.w_CW = this.w_CW+ 1
          replace lvlkey with this.w_LVLKEY3
          this.w_CZ = 1
          * --- --RIG-DIM-MOS-DIR-DPT-DCF-MOV(solo se di fase)
        case livello=4
          * --- --DMT-MRS-DPT
          this.W_LVLKEY4 = ALLTRIM(this.w_LVLKEY3)+"."+right("00000000"+alltrim(str(this.w_CZ)),9)
          replace lvlkey with this.w_LVLKEY4
          this.w_CZ = this.w_CZ+ 1
          this.w_CX = 1
        case livello=5
          * --- --DMM-DPM
          this.w_LVLKEY5 = this.w_LVLKEY4+"."+right("00000000"+alltrim(str(this.w_CX)),9)
          replace lvlkey with this.w_LVLKEY5
          this.w_CX = this.w_CX + 1
      endcase
      ENDSCAN
      if used("_dett_")
        use in _dett_
      endif
      select _tree_ 
 go top
      SCAN
      do case
        case TIPO="ODL"
          replace Descri with &labelODL, CPBmpName with this.w_ODL
        case tipo$"TRA-TRF"
          replace Descri with labelT, CPBmpName with this.w_TRA
        case tipo$"DPC-DPF"
          replace Descri with labelD, CPBmpName with this.w_DICPROD
        case TIPO="ODF"
          replace Descri with &labelODF, CPBmpName with iif(oltprove="L",this.w_OCL,this.w_ODL)
      endcase
      do case
        case tipo$"RIG-DMM-MRS-DPM"
          * --- --Figlio fittizio
          replace Descri with &labelRI, CPBmpName with this.w_RIGMOV
        case tipo$"DDP-DIM-FDO-DIR"
          * --- --Dichiarazione di produzione AVA Prod
          replace Descri with &labelDDP, CPBmpName with this.w_DICPROD
        case tipo$"MOS-DMT-MOV-DPT"
          * --- --Testata di movimento di magazzino trasferimento
          replace Descri with &labelM, CPBmpName with this.w_TMOVSCA
        case tipo="DDO"
          * --- --DICHIARAZIONE DI PRODUZIONE RACCOLTA DATI
          replace Descri with &labelDDO, CPBmpName with this.w_DICODL
        case TIPO$"DCC-DCF"
          * --- --Dichiarazione di produzione AVA Prod senza cicli
          replace Descri with &labelDDS, CPBmpName with this.w_DICPROD
        case TIPO="DRD"
          * --- --DICHIARAZIONE DI PRODUZIONE RACCOLTA DATI
          replace Descri with &labelDRD, CPBmpName with this.w_DICODL
      endcase
      endscan
      activecursor=this.w_cCursor
      Select *,"-" as collapsed from _tree_ order by lvlkey into cursor &activecursor
    else
      * --- Cursore Vuoto
      if used("_curs_")
        use in _curs_
      endif
      if used("_tree_")
        use in _tree_
      endif
      ah_ErrorMsg("Non ci sono dati da visualizzare",48)
      * --- Sbianco la tree-view
      this.w_NODITV.Nodes.Clear
    endif
    if used("_tree_")
      use in _tree_
    endif
    if alltrim(upper(this.w_PUNPAD.Class))="TGSCO_KTO"
      this.w_PunPad.oPgFrm.ActivePage = 2
    else
      this.w_PunPad.oPgFrm.ActivePage = 9
    endif
    * --- Indicizzo il cursore e faccio il refresh della tree-view sulla maschera
    if used(this.w_cCursor)
      select (this.w_cCursor) 
 =wrcursor(this.w_cCursor) 
 index on lvlkey tag lvlkey COLLATE "MACHINE"
    else
      this.w_NODITV.Nodes.Clear
    endif
    this.oParentObject.w_FirstTime = .F.
    this.w_PUNPAD.NotifyEvent("updtreev")     
    if alltrim(upper(this.w_PUNPAD.Class))<>"TGSCO_KTO"
      this.w_PunPad.mhidecontrols()     
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='MVM_MAST'
    this.cWorkTables[5]='*TMPODL_MAST'
    this.cWorkTables[6]='DOC_MAST'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
