* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bel                                                        *
*              Eliminazione ODL/OCL/ODA                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-01-14                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bel",oParentObject,m.pPARAM)
return(i_retval)

define class tgsco_bel as StdBatch
  * --- Local variables
  pPARAM = space(1)
  PADRE = .NULL.
  w_KEYSAL = space(20)
  w_CODMAG = space(5)
  w_QTASAL = 0
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_STATO = space(1)
  w_CODODL = space(15)
  w_NUMREC = 0
  w_SERIAL = space(15)
  w_ERRORE = .f.
  w_PERASS = space(3)
  w_CODICE = space(20)
  w_COMME = space(15)
  w_TIPAT = space(1)
  w_COATT = space(15)
  w_COCOS = space(5)
  w_IMCOM = 0
  w_FCOCO = space(1)
  w_FORCO = space(1)
  w_PDTIPGEN = space(2)
  w_PDROWODL = 0
  w_PDSERDOC = space(10)
  w_PDROWDOC = 0
  w_OLTIPPRE = space(1)
  w_FLEVAS = space(1)
  w_ERRORE = .f.
  w_oERRORLOG = .NULL.
  w_OGGMPS = space(1)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_CODART = space(20)
  w_SALCOM = space(1)
  w_ODLFAS = space(15)
  w_RECFAS = 0
  w_DSERIAL = space(15)
  w_OLDODL = space(15)
  w_MVKEYSAL = space(20)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVFLCASC = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLRISE = space(1)
  w_MVF2CASC = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2RISE = space(1)
  w_FLGSAL = space(1)
  w_MVQTASAL = 0
  w_MVQTAUM1 = 0
  w_MVIMPCOM = 0
  w_MVCODATT = space(15)
  w_MVCODCOM = space(15)
  w_MVCODCOS = space(15)
  w_MVTIPATT = space(1)
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_MVCODART = space(20)
  w_SEDOC = space(10)
  w_NUMRIF = 0
  w_CHKSER = space(10)
  * --- WorkFile variables
  SALDIART_idx=0
  DIC_PROD_idx=0
  RIF_GODL_idx=0
  MAGAZZIN_idx=0
  ART_ICOL_idx=0
  ODL_DETT_idx=0
  ODL_RISO_idx=0
  SCCI_ASF_idx=0
  ODL_CICL_idx=0
  ODL_MAST_idx=0
  PEG_SELI_idx=0
  MPS_TFOR_idx=0
  SALDILOT_idx=0
  LOTTIART_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  MA_COSTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  ODL_SMPL_idx=0
  PAR_RIOR_idx=0
  MOVIMATR_idx=0
  SALDICOM_idx=0
  ODLMRISO_idx=0
  ODL_MAIN_idx=0
  ODL_MOUT_idx=0
  ODL_RISF_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione ODL/OCL/ODA (GSCO_KEL)
    *     
    *     pPARAM: 'I' Init del padre
    *     pPARAM: 'E' Eliminazione
    *     pPARAM: 'S' Seleziona deseleziona record zoom
    this.PADRE = this.oParentObject
    NC = this.PADRE.w_ZoomSel.cCursor
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    do case
      case this.pPARAM="I"
        * --- Cambio intestazione
        do case
          case this.oParentObject.w_PARAM="I"
            this.PADRE.cComment = ah_msgformat("Eliminazione ordini di lavorazione")
          case this.oParentObject.w_PARAM="L"
            this.Padre.cComment = ah_msgformat("Eliminazione ordini di c/lavoro")
          case this.oParentObject.w_PARAM="E"
            this.Padre.cComment = ah_msgformat("Eliminazione ordini di acquisto")
        endcase
        this.PADRE.Caption = this.PADRE.cComment
      case this.pPARAM="E"
        * --- Eliminazione ordini
        if used(NC)
          Select (NC) 
 Locate for xchk=1
          if found()
            * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
            this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
            Select * from (NC) where xchk=1 into cursor elab 
 Select elab 
 GO TOP 
 SCAN
            this.w_NUMREC = RECNO()
            this.w_CODODL = NVL(OLCODODL,"")
            this.w_CODICE = NVL(OLTCODIC,"")
            this.w_STATO = NVL(OLTSTATO,"")
            this.w_KEYSAL = NVL(OLTKEYSA,"")
            this.w_CODMAG = NVL(OLTCOMAG,"")
            this.w_QTASAL = NVL(OLTQTSAL,0)
            this.w_PERASS = NVL(OLTPERAS,"")
            this.w_COMME = NVL(OLTCOMME,"")
            this.w_TIPAT = NVL(OLTTIPAT,"")
            this.w_COATT = NVL(OLTCOATT,"")
            this.w_COCOS = NVL(OLTCOCOS,"")
            this.w_IMCOM = NVL(OLTIMCOM,0)
            this.w_FCOCO = iif(OLTFCOCO="+","-",iif(OLTFCOCO="-","+"," "))
            this.w_FORCO = iif(OLTFORCO="+","-",iif(OLTFORCO="-","+"," "))
            this.w_ERRORE = .f.
            * --- Controllo dichiarazioni e buoni di prelievo
            if !this.w_ERRORE
              this.w_SERIAL = SPACE(15)
              this.w_DSERIAL = SPACE(15)
              * --- Read from DIC_PROD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIC_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DPSERIAL"+;
                  " from "+i_cTable+" DIC_PROD where ";
                      +"DPCODODL = "+cp_ToStrODBC(this.w_CODODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DPSERIAL;
                  from (i_cTable) where;
                      DPCODODL = this.w_CODODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SERIAL = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              vq_exec("..\COLA\EXE\QUERY\GSCO_BEL.VQR",this,"FASCOL")
              this.w_RECFAS = reccount("FASCOL")
              if this.w_RECFAS>0
                * --- L'ODL che sto eliminando ha delle fasi, le devo verificare
                Select FASCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
                this.w_ODLFAS = FASCOL.OLCODODL
                * --- ODL/OCL di fase collegati
                * --- Read from DIC_PROD
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DIC_PROD_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DPSERIAL"+;
                    " from "+i_cTable+" DIC_PROD where ";
                        +"DPCODODL = "+cp_ToStrODBC(this.w_ODLFAS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DPSERIAL;
                    from (i_cTable) where;
                        DPCODODL = this.w_ODLFAS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_DSERIAL = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if !EMPTY(this.w_DSERIAL)
                  this.w_oERRORLOG.AddMsgLog("ODL %1 associato a dichiarazione di produzione",alltrim(this.w_ODLFAS))     
                  this.w_ERRORE = .t.
                else
                  * --- Read from DOC_DETT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "MVSERIAL"+;
                      " from "+i_cTable+" DOC_DETT where ";
                          +"MVCODODL = "+cp_ToStrODBC(this.w_ODLFAS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      MVSERIAL;
                      from (i_cTable) where;
                          MVCODODL = this.w_ODLFAS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if !EMPTY(this.w_DSERIAL)
                    this.w_oERRORLOG.AddMsgLog("Impossibile eliminare ordine %1 in presenza di buoni di prelievo o OCL ordinati",alltrim(this.w_ODLFAS))     
                    this.w_ERRORE = .t.
                  endif
                endif
                ENDSCAN
              endif
              if !EMPTY(this.w_SERIAL)
                this.w_oERRORLOG.AddMsgLog("ODL %1 associato a dichiarazione di produzione",alltrim(this.w_CODODL))     
                this.w_ERRORE = .t.
              else
                if this.w_RECFAS>0
                  this.w_OLDODL = this.w_CODODL
                  Select FASCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
                  this.w_CODODL = FASCOL.OLCODODL
                  vq_exec("..\COLA\EXE\QUERY\GSCO2KEL.VQR",this,"DOCCOL")
                  Select DOCCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
                  this.w_OLTIPPRE = NVL(OLTIPPRE,"")
                  this.w_FLEVAS = NVL(FLEVAS,"")
                  if this.w_OLTIPPRE<>"A"
                    * --- Controllo che la modalit� di prelievo sia automatica...
                    this.w_oERRORLOG.AddMsgLog("Ordine %1 associato a dichiarazione di buoni di prelievo",alltrim(this.w_CODODL))     
                    this.w_ERRORE = .t.
                  else
                    * --- Controllo che la riga del documento non sia evasa
                    if this.w_FLEVAS="S"
                      this.w_oERRORLOG.AddMsgLog("Ordine %1 associato a documento evaso",alltrim(this.w_CODODL))     
                      this.w_ERRORE = .t.
                    endif
                  endif
                  ENDSCAN
                  USE IN Select ("DOCCOL")
                  Select FASCOL
                  ENDSCAN
                  this.w_CODODL = this.w_OLDODL
                endif
                vq_exec("..\COLA\EXE\QUERY\GSCO2KEL.VQR",this,"DOCCOL")
                Select DOCCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
                this.w_PDTIPGEN = NVL(PDTIPGEN,"")
                this.w_PDROWODL = NVL(PDROWODL,0)
                this.w_PDSERDOC = NVL(PDSERDOC,"")
                this.w_PDROWDOC = NVL(PDROWDOC,0)
                this.w_OLTIPPRE = NVL(OLTIPPRE,"")
                this.w_FLEVAS = NVL(FLEVAS,"")
                if this.w_OLTIPPRE<>"A"
                  * --- Controllo che la modalit� di prelievo sia automatica...
                  this.w_oERRORLOG.AddMsgLog("Ordine %1 associato a dichiarazione di buoni di prelievo",alltrim(this.w_CODODL))     
                  this.w_ERRORE = .t.
                else
                  * --- Controllo che la riga del documento non sia evasa
                  if this.w_FLEVAS="S"
                    this.w_oERRORLOG.AddMsgLog("Ordine %1 associato a documento evaso",alltrim(this.w_CODODL))     
                    this.w_ERRORE = .t.
                  else
                    * --- Cancellazione documento associato
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    if this.w_RECFAS>0
                      this.w_OLDODL = this.w_CODODL
                      Select FASCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
                      this.w_CODODL = FASCOL.OLCODODL
                      vq_exec("..\COLA\EXE\QUERY\GSCO2KEL.VQR",this,"DOCCOL")
                      Select DOCCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
                      this.w_PDTIPGEN = NVL(PDTIPGEN,"")
                      this.w_PDROWODL = NVL(PDROWODL,0)
                      this.w_PDSERDOC = NVL(PDSERDOC,"")
                      this.w_PDROWDOC = NVL(PDROWDOC,0)
                      this.w_OLTIPPRE = NVL(OLTIPPRE,"")
                      this.w_FLEVAS = NVL(FLEVAS,"")
                      this.Page_2()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      ENDSCAN
                      USE IN Select ("DOCCOL")
                      Select FASCOL
                      ENDSCAN
                      this.w_CODODL = this.w_OLDODL
                    endif
                  endif
                endif
                ENDSCAN
                USE IN Select ("DOCCOL")
              endif
            endif
            Select Elab 
 GOTO this.w_NUMREC
            if !this.w_ERRORE
              * --- Try
              local bErr_041B8660
              bErr_041B8660=bTrsErr
              this.Try_041B8660()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                this.w_oERRORLOG.AddMsgLog("Ordine %1: impossibile cancellare",alltrim(this.w_CODODL))     
              endif
              bTrsErr=bTrsErr or bErr_041B8660
              * --- End
            endif
            Select Elab 
 GOTO this.w_NUMREC
            ENDSCAN
          else
            ah_errormsg("Selezionare almeno un ordine!",48)
          endif
          if this.w_oERRORLOG.ISFULLLOG()
            this.w_oERRORLOG.PRINTLOG(THIS , ah_msgformat("Eliminazione %1",iif(this.oParentObject.w_PARAM="I","Ordini di lavorazione",iif(this.oParentObject.w_PARAM="L","Ordini di c/lavoro","Ordini di acquisto"))))     
          else
            ah_ErrorMsg("Elaborazione terminata",,"")
          endif
          this.PADRE.NotifyEvent("ActivatePage 2")     
          USE IN Select ("Elab")
        endif
      case this.pPARAM="S"
        if used(NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (NC) SET xChk=0
          endif
        endif
    endcase
  endproc
  proc Try_041B8660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_msg("Cancellazione ordine %1 in corso...",.t.,.f.,.f.,this.w_CODODL)
    * --- Cicla sul dettaglio ODL per mettere a posto l'impegnato
    * --- Select from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
          +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+" and OLQTASAL>0";
           ,"_Curs_ODL_DETT")
    else
      select * from (i_cTable);
       where OLCODODL=this.w_CODODL and OLQTASAL>0;
        into cursor _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      select _Curs_ODL_DETT
      locate for 1=1
      do while not(eof())
      * --- Aggiorna Saldi Magazzino (storno ordinato)
      this.w_FLORDI = iif(_Curs_ODL_DETT.OLFLORDI="+","-",iif(_Curs_ODL_DETT.OLFLORDI="-","+"," "))
      this.w_FLIMPE = iif(_Curs_ODL_DETT.OLFLIMPE="+","-",iif(_Curs_ODL_DETT.OLFLIMPE="-","+"," "))
      this.w_FLRISE = iif(_Curs_ODL_DETT.OLFLRISE="+","-",iif(_Curs_ODL_DETT.OLFLRISE="-","+"," "))
      this.w_CODART = _Curs_ODL_DETT.OLCODART
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            ,SLQTRPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SLCODICE = _Curs_ODL_DETT.OLKEYSAL;
            and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SALCOM="S"
        if empty(nvl(this.w_COMME,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.w_COMME
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              ,SCQTRPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
              and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa (1)'
          return
        endif
      endif
        select _Curs_ODL_DETT
        continue
      enddo
      use
    endif
    Select Elab 
 GOTO this.w_NUMREC
    * --- Cancella lista materiali stimati
    * --- Delete from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            OLCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MAIN
    i_nConn=i_TableProp[this.ODL_MAIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MICODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            MICODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MOUT
    i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MOCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            MOCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancella dettaglio Risorse della fase
    * --- Delete from ODL_SMPL
    i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SMCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            SMCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISO
    i_nConn=i_TableProp[this.ODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            RLCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODLMRISO
    i_nConn=i_TableProp[this.ODLMRISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            RLCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISF
    i_nConn=i_TableProp[this.ODL_RISF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RFCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            RFCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancella assegnazione fasi
    * --- Delete from SCCI_ASF
    i_nConn=i_TableProp[this.SCCI_ASF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCCI_ASF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"COD_ODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            COD_ODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancella dettaglio Ciclo ODL
    * --- Delete from ODL_CICL
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            CLCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTCOART"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTCOART;
        from (i_cTable) where;
            OLCODODL = this.w_CODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Cancella Testata ODL --------------------------------------------
    * --- Delete from ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            OLCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Aggiorna Saldi Magazzino (storno ordinato)
    this.w_FLORDI = iif(OLTFLORD="+","-",iif(OLTFLORD="-","+"," "))
    this.w_FLIMPE = iif(OLTFLIMP="+","-",iif(OLTFLIMP="-","+"," "))
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_KEYSAL;
          and SLCODMAG = this.w_CODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_SALCOM="S"
      if empty(nvl(this.w_COMME,""))
        this.w_COMMAPPO = this.w_COMMDEFA
      else
        this.w_COMMAPPO = this.w_COMME
      endif
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
               )
      else
        update (i_cTable) set;
            SCQTOPER = &i_cOp1.;
            ,SCQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_KEYSAL;
            and SCCODMAG = this.w_CODMAG;
            and SCCODCAN = this.w_COMMAPPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore Aggiornamento Saldi Commessa (1)'
        return
      endif
    endif
    * --- Write into MA_COSTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FCOCO,'CSPREVEN','this.w_IMCOM',this.w_IMCOM,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FORCO,'CSCONSUN','this.w_IMCOM',this.w_IMCOM,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSPREVEN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSPREVEN');
      +",CSCONSUN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSCONSUN');
          +i_ccchkf ;
      +" where ";
          +"CSCODCOM = "+cp_ToStrODBC(this.w_COMME);
          +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPAT);
          +" and CSCODMAT = "+cp_ToStrODBC(this.w_COATT);
          +" and CSCODCOS = "+cp_ToStrODBC(this.w_COCOS);
             )
    else
      update (i_cTable) set;
          CSPREVEN = &i_cOp1.;
          ,CSCONSUN = &i_cOp2.;
          &i_ccchkf. ;
       where;
          CSCODCOM = this.w_COMME;
          and CSTIPSTR = this.w_TIPAT;
          and CSCODMAT = this.w_COATT;
          and CSCODCOS = this.w_COCOS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiornamento piano MPS
    * --- Read from PAR_RIOR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PROGGMPS"+;
        " from "+i_cTable+" PAR_RIOR where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_KEYSAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PROGGMPS;
        from (i_cTable) where;
            PRCODART = this.w_KEYSAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OGGMPS = NVL(cp_ToDate(_read_.PROGGMPS),cp_NullValue(_read_.PROGGMPS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_OGGMPS $ "S-N"
      if this.w_STATO="L"
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_QTASAL);
          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTASAL);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSLAN = FMMPSLAN - this.w_QTASAL;
              ,FMMPSTOT = FMMPSTOT - this.w_QTASAL;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_KEYSAL;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Stato = 'P'
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSPIA =FMMPSPIA- "+cp_ToStrODBC(this.w_QTASAL);
          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTASAL);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSPIA = FMMPSPIA - this.w_QTASAL;
              ,FMMPSTOT = FMMPSTOT - this.w_QTASAL;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_KEYSAL;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- Cancella movimento automatico di trasferimento a WIP (relativo al dettaglio materiali ODL)
    if this.w_RECFAS>0
      * --- Primo giro w_CODODL poi FASCOL
      Select FASCOL 
 GO TOP 
 SCAN WHILE !this.w_ERRORE
      this.w_CODODL = FASCOL.OLCODODL
      ah_msg("Cancellazione ordine %1 in corso...",.t.,.f.,.f.,this.w_CODODL)
      * --- Cicla sul dettaglio ODL per mettere a posto l'impegnato
      * --- Select from ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
            +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+" and OLQTASAL>0";
             ,"_Curs_ODL_DETT")
      else
        select * from (i_cTable);
         where OLCODODL=this.w_CODODL and OLQTASAL>0;
          into cursor _Curs_ODL_DETT
      endif
      if used('_Curs_ODL_DETT')
        select _Curs_ODL_DETT
        locate for 1=1
        do while not(eof())
        * --- Aggiorna Saldi Magazzino (storno ordinato)
        this.w_FLORDI = iif(_Curs_ODL_DETT.OLFLORDI="+","-",iif(_Curs_ODL_DETT.OLFLORDI="-","+"," "))
        this.w_FLIMPE = iif(_Curs_ODL_DETT.OLFLIMPE="+","-",iif(_Curs_ODL_DETT.OLFLIMPE="-","+"," "))
        this.w_FLRISE = iif(_Curs_ODL_DETT.OLFLRISE="+","-",iif(_Curs_ODL_DETT.OLFLRISE="-","+"," "))
        this.w_CODART = _Curs_ODL_DETT.OLCODART
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              ,SLQTRPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SLCODICE = _Curs_ODL_DETT.OLKEYSAL;
              and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_COMME,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_COMME
          endif
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                ,SCQTIPER = &i_cOp2.;
                ,SCQTRPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa (1)'
            return
          endif
        endif
          select _Curs_ODL_DETT
          continue
        enddo
        use
      endif
      Select Elab 
 GOTO this.w_NUMREC
      * --- Cancella lista materiali stimati
      * --- Delete from ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              OLCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ODL_MAIN
      i_nConn=i_TableProp[this.ODL_MAIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAIN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MICODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              MICODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ODL_MOUT
      i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MOCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              MOCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Cancella dettaglio Risorse della fase
      * --- Delete from ODL_SMPL
      i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SMCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              SMCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ODL_RISO
      i_nConn=i_TableProp[this.ODL_RISO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              RLCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ODLMRISO
      i_nConn=i_TableProp[this.ODLMRISO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              RLCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ODL_RISF
      i_nConn=i_TableProp[this.ODL_RISF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RFCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              RFCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Cancella assegnazione fasi
      * --- Delete from SCCI_ASF
      i_nConn=i_TableProp[this.SCCI_ASF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCCI_ASF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"COD_ODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              COD_ODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Cancella dettaglio Ciclo ODL
      * --- Delete from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              CLCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTCOART,OLTKEYSA,OLTCOMAG,OLTFLORD,OLTFLIMP,OLTQTSAL,OLTCOMME,OLTTIPAT,OLTCOATT,OLTCOCOS,OLTIMCOM,OLTFCOCO,OLTFORCO,OLTPERAS"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTCOART,OLTKEYSA,OLTCOMAG,OLTFLORD,OLTFLIMP,OLTQTSAL,OLTCOMME,OLTTIPAT,OLTCOATT,OLTCOCOS,OLTIMCOM,OLTFCOCO,OLTFORCO,OLTPERAS;
          from (i_cTable) where;
              OLCODODL = this.w_CODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
        this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
        this.w_CODMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
        this.w_FLORDI = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
        this.w_FLIMPE = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
        this.w_QTASAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
        this.w_COMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
        this.w_TIPAT = NVL(cp_ToDate(_read_.OLTTIPAT),cp_NullValue(_read_.OLTTIPAT))
        this.w_COATT = NVL(cp_ToDate(_read_.OLTCOATT),cp_NullValue(_read_.OLTCOATT))
        this.w_COCOS = NVL(cp_ToDate(_read_.OLTCOCOS),cp_NullValue(_read_.OLTCOCOS))
        this.w_IMCOM = NVL(cp_ToDate(_read_.OLTIMCOM),cp_NullValue(_read_.OLTIMCOM))
        this.w_FCOCO = NVL(cp_ToDate(_read_.OLTFCOCO),cp_NullValue(_read_.OLTFCOCO))
        this.w_FORCO = NVL(cp_ToDate(_read_.OLTFORCO),cp_NullValue(_read_.OLTFORCO))
        this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Cancella Testata ODL --------------------------------------------
      * --- Delete from ODL_MAST
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        delete from (i_cTable) where;
              OLCODODL = this.w_CODODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Aggiorna Saldi Magazzino (storno ordinato)
      this.w_FLORDI = iif(this.w_FLORDI="+","-",iif(this.w_FLORDI="-","+"," "))
      this.w_FLIMPE = iif(this.w_FLIMPE="+","-",iif(this.w_FLIMPE="-","+"," "))
      this.w_FCOCO = iif(this.w_FCOCO="+","-",iif(this.w_FCOCO="-","+"," "))
      this.w_FORCO = iif(this.w_FORCO="+","-",iif(this.w_FORCO="-","+"," "))
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_KEYSAL;
            and SLCODMAG = this.w_CODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SALCOM="S"
        if empty(nvl(this.w_COMME,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.w_COMME
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_KEYSAL;
              and SCCODMAG = this.w_CODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa (1)'
          return
        endif
      endif
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FCOCO,'CSPREVEN','this.w_IMCOM',this.w_IMCOM,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FORCO,'CSCONSUN','this.w_IMCOM',this.w_IMCOM,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSPREVEN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSPREVEN');
        +",CSCONSUN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSCONSUN');
            +i_ccchkf ;
        +" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.w_COMME);
            +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPAT);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_COATT);
            +" and CSCODCOS = "+cp_ToStrODBC(this.w_COCOS);
               )
      else
        update (i_cTable) set;
            CSPREVEN = &i_cOp1.;
            ,CSCONSUN = &i_cOp2.;
            &i_ccchkf. ;
         where;
            CSCODCOM = this.w_COMME;
            and CSTIPSTR = this.w_TIPAT;
            and CSCODMAT = this.w_COATT;
            and CSCODCOS = this.w_COCOS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiornamento piano MPS
      * --- Read from PAR_RIOR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PROGGMPS"+;
          " from "+i_cTable+" PAR_RIOR where ";
              +"PRCODART = "+cp_ToStrODBC(this.w_KEYSAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PROGGMPS;
          from (i_cTable) where;
              PRCODART = this.w_KEYSAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OGGMPS = NVL(cp_ToDate(_read_.PROGGMPS),cp_NullValue(_read_.PROGGMPS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_OGGMPS $ "S-N"
        if this.w_STATO="L"
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_QTASAL);
            +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTASAL);
                +i_ccchkf ;
            +" where ";
                +"FMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                   )
          else
            update (i_cTable) set;
                FMMPSLAN = FMMPSLAN - this.w_QTASAL;
                ,FMMPSTOT = FMMPSTOT - this.w_QTASAL;
                &i_ccchkf. ;
             where;
                FMCODART = this.w_KEYSAL;
                and FMPERASS = this.w_PERASS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Stato = 'P'
          * --- Write into MPS_TFOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FMMPSPIA =FMMPSPIA- "+cp_ToStrODBC(this.w_QTASAL);
            +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTASAL);
                +i_ccchkf ;
            +" where ";
                +"FMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                   )
          else
            update (i_cTable) set;
                FMMPSPIA = FMMPSPIA - this.w_QTASAL;
                ,FMMPSTOT = FMMPSTOT - this.w_QTASAL;
                &i_ccchkf. ;
             where;
                FMCODART = this.w_KEYSAL;
                and FMPERASS = this.w_PERASS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      * --- Cancella movimento automatico di trasferimento a WIP (relativo al dettaglio materiali ODL)
      ENDSCAN
      USE IN Select ("FASCOL")
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellazione documento associato
    * --- Try
    local bErr_04312818
    bErr_04312818=bTrsErr
    this.Try_04312818()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_oERRORLOG.AddMsgLog("Impossibile effettuare la cancellazione del documento associato all'ODL %1",alltrim(this.w_CODODL))     
      this.w_ERRORE = .t.
    endif
    bTrsErr=bTrsErr or bErr_04312818
    * --- End
  endproc
  proc Try_04312818()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVKEYSAL,MVCODART,MVCODMAG,MVCODMAT,MVFLCASC,MVFLIMPE,MVFLORDI,MVFLRISE,MVF2CASC,MVF2IMPE,MVF2ORDI,MVF2RISE,MVQTASAL,MVQTAUM1,MVIMPCOM,MVCODATT,MVCODCOM,MVCODCOS,MVTIPATT,MVFLORCO,MVFLCOCO  from "+i_cTable+" DOC_DETT ";
          +" where DOC_DETT.MVSERIAL="+cp_ToStrODBC(this.w_PDSERDOC)+" and DOC_DETT.CPROWNUM="+cp_ToStrODBC(this.w_PDROWDOC)+"";
           ,"_Curs_DOC_DETT")
    else
      select MVKEYSAL,MVCODART,MVCODMAG,MVCODMAT,MVFLCASC,MVFLIMPE,MVFLORDI,MVFLRISE,MVF2CASC,MVF2IMPE,MVF2ORDI,MVF2RISE,MVQTASAL,MVQTAUM1,MVIMPCOM,MVCODATT,MVCODCOM,MVCODCOS,MVTIPATT,MVFLORCO,MVFLCOCO from (i_cTable);
       where DOC_DETT.MVSERIAL=this.w_PDSERDOC and DOC_DETT.CPROWNUM=this.w_PDROWDOC;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      this.w_MVKEYSAL = _Curs_DOC_DETT.MVKEYSAL
      this.w_MVCODART = _Curs_DOC_DETT.MVCODART
      this.w_MVCODMAG = _Curs_DOC_DETT.MVCODMAG
      this.w_MVCODMAT = _Curs_DOC_DETT.MVCODMAT
      this.w_MVQTASAL = _Curs_DOC_DETT.MVQTASAL
      this.w_MVQTAUM1 = _Curs_DOC_DETT.MVQTAUM1
      this.w_MVIMPCOM = _Curs_DOC_DETT.MVIMPCOM
      this.w_MVCODATT = _Curs_DOC_DETT.MVCODATT
      this.w_MVCODCOM = _Curs_DOC_DETT.MVCODCOM
      this.w_MVCODCOS = _Curs_DOC_DETT.MVCODCOS
      this.w_MVTIPATT = _Curs_DOC_DETT.MVTIPATT
      this.w_MVFLCASC = iif(_Curs_DOC_DETT.MVFLCASC="+","-",iif(_Curs_DOC_DETT.MVFLCASC="-","+"," "))
      this.w_MVFLIMPE = iif(_Curs_DOC_DETT.MVFLIMPE="+","-",iif(_Curs_DOC_DETT.MVFLIMPE="-","+"," "))
      this.w_MVFLORDI = iif(_Curs_DOC_DETT.MVFLORDI="+","-",iif(_Curs_DOC_DETT.MVFLORDI="-","+"," "))
      this.w_MVFLRISE = iif(_Curs_DOC_DETT.MVFLRISE="+","-",iif(_Curs_DOC_DETT.MVFLRISE="-","+"," "))
      this.w_MVF2CASC = iif(_Curs_DOC_DETT.MVF2CASC="+","-",iif(_Curs_DOC_DETT.MVF2CASC="-","+"," "))
      this.w_MVF2IMPE = iif(_Curs_DOC_DETT.MVF2IMPE="+","-",iif(_Curs_DOC_DETT.MVF2IMPE="-","+"," "))
      this.w_MVF2ORDI = iif(_Curs_DOC_DETT.MVF2ORDI="+","-",iif(_Curs_DOC_DETT.MVF2ORDI="-","+"," "))
      this.w_MVF2RISE = iif(_Curs_DOC_DETT.MVF2RISE="+","-",iif(_Curs_DOC_DETT.MVF2RISE="-","+"," "))
      this.w_MVFLORCO = iif(this.w_MVFLORCO="+","-",iif(this.w_MVFLORCO="-","+"," "))
      this.w_MVFLCOCO = iif(this.w_MVFLCOCO="+","-",iif(this.w_MVFLCOCO="-","+"," "))
      * --- Storno i saldi di magazzino e di commessa
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVFLCASC,'SLQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVFLRISE,'SLQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVFLORDI,'SLQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MVFLIMPE,'SLQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
               )
      else
        update (i_cTable) set;
            SLQTAPER = &i_cOp1.;
            ,SLQTRPER = &i_cOp2.;
            ,SLQTOPER = &i_cOp3.;
            ,SLQTIPER = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVF2CASC,'SLQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVF2RISE,'SLQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVF2ORDI,'SLQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MVF2IMPE,'SLQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
               )
      else
        update (i_cTable) set;
            SLQTAPER = &i_cOp1.;
            ,SLQTRPER = &i_cOp2.;
            ,SLQTOPER = &i_cOp3.;
            ,SLQTIPER = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_SALCOM="S"
        if empty(nvl(this.w_COMME,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.w_COMME
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVFLCASC,'SCQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVFLRISE,'SCQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MVFLORDI,'SCQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MVFLIMPE,'SCQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTAPER = &i_cOp1.;
              ,SCQTRPER = &i_cOp2.;
              ,SCQTOPER = &i_cOp3.;
              ,SCQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_MVKEYSAL;
              and SCCODMAG = this.w_MVCODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa (1)'
          return
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVF2CASC,'SCQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVF2RISE,'SCQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MVF2ORDI,'SCQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MVF2IMPE,'SCQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTAPER = &i_cOp1.;
              ,SCQTRPER = &i_cOp2.;
              ,SCQTOPER = &i_cOp3.;
              ,SCQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_MVKEYSAL;
              and SCCODMAG = this.w_MVCODMAT;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa (2)'
          return
        endif
      endif
      * --- Storno i saldi commessa
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVFLCOCO,'CSCONSUN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
        +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
            +i_ccchkf ;
        +" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.w_MVCODCOM);
            +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MVTIPATT);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_MVCODATT);
            +" and CSCODCOS = "+cp_ToStrODBC(this.w_MVCODCOS);
               )
      else
        update (i_cTable) set;
            CSCONSUN = &i_cOp1.;
            ,CSORDIN = &i_cOp2.;
            &i_ccchkf. ;
         where;
            CSCODCOM = this.w_MVCODCOM;
            and CSTIPSTR = this.w_MVTIPATT;
            and CSCODMAT = this.w_MVCODATT;
            and CSCODCOS = this.w_MVCODCOS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_PDSERDOC);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_PDROWDOC);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_PDSERDOC;
              and CPROWNUM = this.w_PDROWDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Libero le matricole usate (se ce ne sono)
      this.w_SEDOC = this.w_PDSERDOC
      this.w_NUMRIF = -20
      * --- Reimposto MT_SALDO a 0 nelle Matricole di carico di quelle che sto cancellando
      * --- Write into MOVIMATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT"
        do vq_exec with 'gsar_bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
        +"MOVIMATR.MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
            +"MT_SALDO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from MOVIMATR
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MTSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
              +" and MTNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               )
      else
        delete from (i_cTable) where;
              MTSERIAL = this.w_SEDOC;
              and MTNUMRIF = this.w_NUMRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    * --- Posso cancellare la testata del documento solo se � vuota
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVSERIAL"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_PDSERDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVSERIAL;
        from (i_cTable) where;
            MVSERIAL = this.w_PDSERDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CHKSER = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(nvl(this.w_CHKSER,""))
      * --- Delete from DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_PDSERDOC);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_PDSERDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Delete from RIF_GODL
    i_nConn=i_TableProp[this.RIF_GODL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
             )
    else
      delete from (i_cTable) where;
            PDCODODL = this.w_CODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,27)]
    this.cWorkTables[1]='SALDIART'
    this.cWorkTables[2]='DIC_PROD'
    this.cWorkTables[3]='RIF_GODL'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='ODL_DETT'
    this.cWorkTables[7]='ODL_RISO'
    this.cWorkTables[8]='SCCI_ASF'
    this.cWorkTables[9]='ODL_CICL'
    this.cWorkTables[10]='ODL_MAST'
    this.cWorkTables[11]='PEG_SELI'
    this.cWorkTables[12]='MPS_TFOR'
    this.cWorkTables[13]='SALDILOT'
    this.cWorkTables[14]='LOTTIART'
    this.cWorkTables[15]='MVM_DETT'
    this.cWorkTables[16]='MVM_MAST'
    this.cWorkTables[17]='MA_COSTI'
    this.cWorkTables[18]='DOC_DETT'
    this.cWorkTables[19]='DOC_MAST'
    this.cWorkTables[20]='ODL_SMPL'
    this.cWorkTables[21]='PAR_RIOR'
    this.cWorkTables[22]='MOVIMATR'
    this.cWorkTables[23]='SALDICOM'
    this.cWorkTables[24]='ODLMRISO'
    this.cWorkTables[25]='ODL_MAIN'
    this.cWorkTables[26]='ODL_MOUT'
    this.cWorkTables[27]='ODL_RISF'
    return(this.OpenAllTables(27))

  proc CloseCursors()
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
