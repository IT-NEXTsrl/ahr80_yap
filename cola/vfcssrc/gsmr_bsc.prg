* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bsc                                                        *
*              Manutenzione saldi commessa                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-06                                                      *
* Last revis.: 2014-06-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bsc",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_bsc as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_NEWMAG = space(5)
  w_DESNMA = space(30)
  w_CODRIC = space(41)
  w_CODSAL = space(40)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_DESRIC = space(40)
  w_UNIMIS = space(3)
  w_CODCOM = space(15)
  w_DESCOM = space(30)
  w_ESI = 0
  w_RIS = 0
  w_ORD = 0
  w_IMP = 0
  w_DIS = 0
  w_DIC = 0
  w_UBICAZ = space(1)
  w_NEWUBI = space(1)
  w_LOTTI = space(1)
  w_MATRIC = space(1)
  * --- WorkFile variables
  MAGAZZIN_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di manutenzione Saldi Commessa (da GSMR_KSC)
    this.Padre = this.oParentObject
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione = "ABB"
        * --- Controlla selezioni
        select count(*) as Numero from (this.NC) where xChk=1 into cursor __temp__
        go top
        this.w_nRecSel = NVL(__temp__.Numero,0)
        use in __Temp__
        do case
          case this.w_nRecSel=0
            ah_ErrorMsg("Non sono stati selezionati elementi da elaborare",48)
          case this.w_nRecSel=1
            * --- Valorizzo le variabili di appoggio (Caller sulla Maschera)
            select(this.NC) 
 locate for xchk=1
            this.w_CODMAG = SCCODMAG
            this.w_NEWMAG = this.w_CODMAG
            this.w_CODRIC = CACODICE
            this.w_UNIMIS = ARUNMIS1
            this.w_CODCOM = SCCODCAN
            this.w_CODSAL = SCCODICE
            this.w_CODART = SCCODART
            this.w_CODVAR = SCCODVAR
            this.w_ESI = SCQTAPER
            this.w_RIS = SCQTRPER
            this.w_ORD = SCQTOPER
            this.w_IMP = SCQTIPER
            this.w_DIS = DISPOPER
            this.w_DIC = DISPCONT
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDESMAG,MGFLUBIC"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDESMAG,MGFLUBIC;
                from (i_cTable) where;
                    MGCODMAG = this.w_CODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
              this.w_UBICAZ = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_DESNMA = this.w_DESMAG
            this.w_NEWUBI = this.w_UBICAZ
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLLOTT,ARGESMAT,ARDESART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLLOTT,ARGESMAT,ARDESART;
                from (i_cTable) where;
                    ARCODART = this.w_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LOTTI = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
              this.w_MATRIC = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
              this.w_DESRIC = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Lancio la maschera di Abbinamento
            do gsmr_kag with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          otherwise
            ah_ErrorMsg("E' possibile elaborare una sola riga per volta",48)
        endcase
        * --- Riesegue l'interrogazione
        this.Padre.NotifyEvent("Interroga")     
      case this.pAzione = "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione = "INTERROGA"
        * --- Aggiorna primo zoom
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2
        this.Padre.oPgfrm.Page2.Enabled = .t.
        this.Padre.oPgfrm.Activepage = 2
        * --- Aggiorna secondo zoom posizionandosi sulla prima riga del primo zoom
        this.Padre.mcalc(.t.)     
        this.Padre.Refresh()     
        this.Padre.NotifyEvent("Interdett")     
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
