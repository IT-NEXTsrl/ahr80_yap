* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bem                                                        *
*              Stampa elaborazione fabbisogni                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_70]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-05                                                      *
* Last revis.: 2015-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bem",oParentObject)
return(i_retval)

define class tgsco_bem as StdBatch
  * --- Local variables
  w_Res = 0
  w_FileName = space(200)
  f_Error = 0
  * --- WorkFile variables
  ODL_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa il cursore di elaborazione dell'MRP (da GSCO_SEM)
    ctablemrp = "elab"+alltrim(i_codazi)+alltrim(this.oParentObject.w_CODSER)
    if Used(ctablemrp)
      use in select(ctablemrp)
    endif
    * --- Test path MRP
    * --- Azzero il Contenuto Dell'Array
    Declare ArrPath[3]
    ArrPath[1]="" 
 ArrPath[2]=0 
 ArrPath[3]=""
    this.w_Res = GetDirElab("R", @ArrPath,this.oParentObject.w_CODSER)
    this.w_FileName = alltrim(ArrPath[1])
    this.f_Error = ArrPath[2]
    do case
      case this.f_Error = 0
        use (this.w_FileName) IN 0 alias (ctablemrp)
      case this.f_Error = 1
        ah_ErrorMsg("Impossibile trovare il percorso:%0 %1%0Impossibile effettuare la stampa",16,"",this.w_FileName)
        i_retcode = 'stop'
        return
      case this.f_Error = 2
        ah_ErrorMsg("File: %1 non trovato.%0Occorre prima eseguire l'elaborazione MRP-ORDERS%0Impossibile effettuare la stampa",16,"",this.w_FileName)
        i_retcode = 'stop'
        return
    endcase
    if used(ctablemrp)
      * --- Aggiorno lo Stato ODL sul DBF
      update (ctablemrp) set odlstat=iif(inlist(tiprec,"8MRP-S","4ODL-M","8PDA-S","4PDA-M"),"M",iif(inlist(tiprec,"4ODL-L","4PDA-L"),"L",iif(inlist(tiprec,"4ODL-P","4PDA-P"),"P"," "))) where empty(nvl(odlstat," "))
      * --- Costruisce la condizione di filtro
      * --- Seleziona gli articoli da stampare
      ah_msg("Estrazione dati in corso...")
      * --- Seleziona gli articoli da stampare
      vq_exec("..\COLA\EXE\QUERY\GSCO1SEM", this, "ODL")
      vq_exec("..\COLA\EXE\QUERY\gsco2sem", this, "Documenti")
      vq_exec("..\COLA\EXE\QUERY\gsco3sem", this, "Doc")
      vq_exec("..\COLA\exe\query\gsco2bem", this, "Messaggi")
      ah_msg("Elaborazione dati in corso...")
      filtraf = "where tiprec<>'7SCO-S'  and proggmps='N' " + iif(this.oParentObject.w_SOLOELA, " and elabora ", "")
      if NVL(this.oParentObject.w_FLULELA,"N")="S"
        if this.oParentObject.w_ISUGG="." and this.oParentObject.w_IPIA="." and this.oParentObject.w_ILAN="." and this.oParentObject.w_LSUGG="." and this.oParentObject.w_LPIA="." and this.oParentObject.w_LLAN="."
          filtra = "where tiprec<>'7SCO-S' and proggmps='N' " + iif(this.oParentObject.w_SOLOELA, " and elabora ", "")
          vq_exec("..\COLA\EXE\QUERY\gsco4sem", this, "Selezione")
          filtraprove = "where 1=1" + iif(this.oParentObject.w_PROPRE="I", " and ARPROPRE='I'", iif(this.oParentObject.w_PROPRE="L", " and ARPROPRE='L'", iif(this.oParentObject.w_PROPRE="E", " and ARPROPRE='E'",""))) 
 select distinct codsal as tcakeysal from (ctablemrp) into cursor tmpSelezione &filtraprove 
 =wrcursor("tmpSelezione") 
 delete from tmpSelezione where tcakeysal in (select distinct codsal from (ctablemrp) where tiprec="4ODL" or tiprec="4PDA" or tiprec="8")
        else
          filtra = "where tiprec<>'7SCO-S' and (proggmps='N' or (tiprec='4ODL' or tiprec='4PDA')) " + iif(this.oParentObject.w_SOLOELA, " and elabora ", "")
          vq_exec("..\COLA\EXE\QUERY\gsco6sem", this, "Selezione")
          statoodl = iif(this.oParentObject.w_ISUGG="M", "'M',", "'.',") + iif(this.oParentObject.w_IPIA="P", "'P',", "'.',") + iif(this.oParentObject.w_ILAN="L", "'L',", "'.',")
          statoodl = iif(not empty(statoodl) , " and (OLTPROVE='I' and inlist(odlstat, "+ left(alltrim(statoodl), len(alltrim(statoodl))-1),"") +")"
          statoocl = iif(this.oParentObject.w_LSUGG="M", "'M',", "'.',") + iif(this.oParentObject.w_LPIA="P", "'P',", "'.',")
          statocll = iif(not empty(statoocl) and this.oParentObject.w_LLAN="L", ") or tiprec='4ORDFO'" , iif(empty(statoocl) and this.oParentObject.w_LLAN="L", " and tiprec='4ORDFO' ", ""))
          statoocl = iif(not empty(statoocl) , " (OLTPROVE='L' and inlist(odlstat, "+ left(alltrim(statoocl), len(alltrim(statoocl))-1),"") + statocll + IIF(EMPTY(statocll), ")", "") +")"
          statooda = iif(this.oParentObject.w_ESUGG="M", "'M',", "'.',") + iif(this.oParentObject.w_EPIA="P", "'P',", "'.',")
          statoodaa = iif(not empty(statooda) and this.oParentObject.w_ELAN="L", ") or tiprec='4ORDFO'" , iif(empty(statooda) and this.oParentObject.w_ELAN="L", " and tiprec='4ORDFO' ", ""))
          statooda = iif(not empty(statooda) , " (OLTPROVE='E' and inlist(odlstat, "+ left(alltrim(statooda), len(alltrim(statooda))-1),"") + statoodaa + IIF(EMPTY(statoodaa), ")", "") +")"
          do case
            case this.oParentObject.w_PROPRE = "I"
              filtraodl = " and (ARPROPRE='I'  " + statoodl + iif(empty(statoodl)," and " , " or ") + statoocl + iif(empty(statoocl)," and " , " or ") + statooda+")"
              filtrat = filtraodl + ")"
            case this.oParentObject.w_PROPRE = "L"
              filtraocl = " and (ARPROPRE='L'  " + statoodl + iif(empty(statoodl)," and " , " or ") + statoocl + iif(empty(statoocl)," and " , " or ") + statooda +")"
              filtrat = filtraocl + ")"
            case this.oParentObject.w_PROPRE = "E"
              filtraoda = " and (ARPROPRE='E'  " + statoodl + iif(empty(statoodl)," and " , " or ") + statoocl + iif(empty(statoocl)," and " , " or ") + statooda +")"
              filtrat = filtraoda + ")"
            otherwise
              filtratutto = statoodl + iif(empty(statoodl)," and " , " or ") + statoocl + iif(empty(statoocl)," and " , " or ") + statooda
              filtrat = filtratutto + ")"
          endcase
          altriFiltri = iif(not empty(this.oParentObject.w_CODCOM), " and codcom=='" + alltrim(this.oParentObject.w_CODCOM)+ "'", "")
          altriFiltri = altriFiltri + iif(not empty(this.oParentObject.w_CODATT), " and codatt=='" + alltrim(this.oParentObject.w_CODATT)+ "'", "")
          altriFiltri = altriFiltri + iif(not empty(this.oParentObject.w_CODCON), " and prcodfor=='" + alltrim(this.oParentObject.w_CODCON)+ "'", "")
          filtraodl = " where tiprec <> '7SCO-S' and proggmps='N' " + altriFiltri 
          filtraord = " where tiprec <> '7SCO-S' and proggmps='N' " + altriFiltri
          ah_msg("Elaborazione dati in corso...")
          Select DISTINCT codsal1 as tcakeysal from (ctablemrp) ; 
 left outer join odl on (padrered=olcododl) &filtraodl &filtrat; 
 union ; 
 Select codsal1 as tcakeysal from (ctablemrp) ; 
 left outer join doc on left(padrered,10)=mvserial &filtraord &filtrat; 
 into cursor tmpSelezione 
        endif
      else
        vq_exec("..\COLA\EXE\QUERY\GSCO_SEM", this, "Selezione")
      endif
      if this.oParentObject.w_FLIMPE <> "T"
        ah_msg("Verifica impegni in corso...")
        select distinct codsal as CakeySal from (ctablemrp) into cursor NRec WHERE proggmps="N" and not inlist(tiprec, "0SALDI", "7SCO-S") and Quanti < 0 
        do case
          case this.oParentObject.w_FLIMPE = "C"
            if this.oParentObject.w_SOLOELA
              if NVL(this.oParentObject.w_FLULELA,"N")="S"
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal inner join tmpselezione on cakeysal=tcakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl where ; 
 codsal in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" and elabora into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              else
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl where ; 
 codsal in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" and elabora into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              endif
            else
              if NVL(this.oParentObject.w_FLULELA,"N")="S"
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal inner join tmpselezione on cakeysal=tcakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl where ; 
 codsal in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              else
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl where ; 
 codsal in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              endif
            endif
          case this.oParentObject.w_FLIMPE = "S"
            if this.oParentObject.w_SOLOELA
              if NVL(this.oParentObject.w_FLULELA,"N")="S"
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal inner join tmpselezione on cakeysal=tcakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl ; 
 where codsal not in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" and elabora into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              else
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal left outer join odl on padrered=olcododl ; 
 left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl ; 
 where codsal not in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" and elabora into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              endif
            else
              if NVL(this.oParentObject.w_FLULELA,"N")="S"
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal inner join tmpselezione on cakeysal=tcakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl ; 
 where codsal not in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              else
                Select * from (ctablemrp) inner join Selezione on codsal=cakeysal ; 
 left outer join odl on padrered=olcododl left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl ; 
 where codsal not in(select cakeysal from Nrec) and tiprec<>"7SCO-S" and proggmps="N" into cursor __tmp__ order by prlowlev,codsal,codcom,keyidx
              endif
            endif
        endcase
        if used("NRec")
          use in select("NRec")
        endif
      else
        if NVL(this.oParentObject.w_FLULELA,"N")="S"
          Select * from (ctablemrp) inner join Selezione on codsal=cakeysal ; 
 inner join tmpselezione on cakeysal=tcakeysal ; 
 left outer join odl on padrered=olcododl ; 
 left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl ; 
 into cursor __tmp__ &filtraf order by prlowlev,codsal,codcom,keyidx
        else
          Select * from (ctablemrp) inner join Selezione on codsal=cakeysal ; 
 left outer join odl on padrered=olcododl ; 
 left outer join documenti on left(padrered,10)=mvserial ; 
 left outer join Messaggi on odlmsg=mrcododl into cursor __tmp__ &filtraf order by prlowlev,codsal,codcom,keyidx
        endif
      endif
      use in select("Doc")
      use in select("odl")
      use in select(ctablemrp)
      use in select("Selezione")
      use in select("tmpSelezione")
      use in select("Documenti")
      use in select("Messaggi")
      Select __tmp__
      if nvl(this.oParentObject.w_TESTO," ")="S"
        * --- Stampa Solo Testo
        if this.oParentObject.w_STAREG="V"
          * --- Selezionato modulo di stampa 'Verticale' - 80 colonne
          CP_CHPRN("mrp_v.fxp", "S",this.oParentObject)
        else
          * --- Selezionato modulo di stampa 'Orizzontale'
          CP_CHPRN("mrp_o.fxp", "S",this.oParentObject)
        endif
      else
        codini = this.oParentObject.w_CODINI 
 codfin = this.oParentObject.w_CODFIN
        gruini = this.oParentObject.w_GRUINI 
 grufin = this.oParentObject.w_GRUFIN
        famaini = this.oParentObject.w_FAMAINI 
 famafin = this.oParentObject.w_FAMAFIN
        catini = this.oParentObject.w_CATINI 
 catfin = this.oParentObject.w_CATFIN
        magini = this.oParentObject.w_MAGINI 
 magfin = this.oParentObject.w_MAGFIN
        isugg=this.oParentObject.w_ISUGG 
 ipia=this.oParentObject.w_IPIA 
 ilan=this.oParentObject.w_ILAN
        lsugg=this.oParentObject.w_LSUGG 
 lpia=this.oParentObject.w_LPIA 
 llan=this.oParentObject.w_LLAN
        esugg=this.oParentObject.w_ESUGG 
 epia=this.oParentObject.w_EPIA 
 elan=this.oParentObject.w_ELAN
        l_PROPRE = this.oParentObject.w_PROPRE 
 l_SALTOP = this.oParentObject.w_SALTOP 
 l_FLIMPE = this.oParentObject.w_FLIMPE
        l_flulela=NVL(this.oParentObject.w_FLULELA,"N") 
 l_codser=this.oParentObject.w_CODSER 
 l_gianeg=NVL(this.oParentObject.w_GIANEG,"S")
        l_CODSER = this.oParentObject.w_CODSER
        * --- Stampa Grafica
        if this.oParentObject.w_SALTOP="P"
          do cp_chprn with "..\COLA\EXE\QUERY\GSCO_SEM","",this.oParentObject
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do cp_chprn with "..\COLA\EXE\QUERY\gsCO1sem","",this.oParentObject
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        use in select("__tmp__")
      endif
    else
      ah_ErrorMsg("Impossibile effettuare la stampa",16)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ODL_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
