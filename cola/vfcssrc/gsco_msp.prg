* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_msp                                                        *
*              Piano di produzione                                             *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-01                                                      *
* Last revis.: 2016-06-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsco_msp
PARAMETERS pTipGes,pParentObject
* --- Fine Area Manuale
return(createobject("tgsco_msp"))

* --- Class definition
define class tgsco_msp as StdTrsForm
  Top    = 7
  Left   = 6

  * --- Standard Properties
  Width  = 1069
  Height = 580+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-06-09"
  HelpContextID=171334295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=104

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  SCALETTM_IDX = 0
  SCALETTA_IDX = 0
  ODL_MAST_IDX = 0
  RIS_ORSE_IDX = 0
  ART_ICOL_IDX = 0
  PAR_PROD_IDX = 0
  TAB_CALE_IDX = 0
  KEY_ARTI_IDX = 0
  TAB_RISO_IDX = 0
  TABMCICL_IDX = 0
  cFile = "SCALETTM"
  cFileDetail = "SCALETTA"
  cKeySelect = "SCSERIAL"
  cKeyWhere  = "SCSERIAL=this.w_SCSERIAL"
  cKeyDetail  = "SCSERIAL=this.w_SCSERIAL"
  cKeyWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cKeyDetailWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"SCALETTA.SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'SCALETTA.CPROWORD,SCALETTA. SCCODODL'
  cPrg = "gsco_msp"
  cComment = "Piano di produzione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SCSERIAL = space(15)
  w_SCTABRIS = space(20)
  o_SCTABRIS = space(20)
  w_SCDATINI = ctod('  /  /  ')
  w_SCQTCPDF = 0
  w_SCQTAC01 = 0
  w_SCQTAC02 = 0
  w_SCQTAC03 = 0
  w_SCQTAC04 = 0
  w_SCQTAC05 = 0
  w_SCQTAC06 = 0
  w_SCQTAC07 = 0
  w_SCQTAC08 = 0
  w_SCQTAC09 = 0
  w_SCQTAC10 = 0
  w_SCQTAC11 = 0
  w_SCQTAC12 = 0
  w_SCQTAC13 = 0
  w_SCQTAC14 = 0
  w_RLDESCRI = space(40)
  w_TIPGES = space(3)
  w_SCTIPSCA = space(3)
  w_CALRIS = space(5)
  w_SCCALRIS = space(5)
  w_SCDESCAL = space(40)
  w_SCFASSEL = space(1)
  w_SCTIPPIA = space(1)
  w_SCODLSUG = space(1)
  w_SCODLPIA = space(1)
  w_SCODLLAN = space(1)
  w_SCDATFIN = ctod('  /  /  ')
  w_VARIATA = .F.
  w_NRVARIATA = 0
  w_CPROWORD = 0
  o_CPROWORD = 0
  w_SCCODICE = space(20)
  o_SCCODICE = space(20)
  w_SCCODODL = space(15)
  o_SCCODODL = space(15)
  w_SCQTARIC = 0
  o_SCQTARIC = 0
  w_SCQTAP01 = 0
  o_SCQTAP01 = 0
  w_SCQTAP02 = 0
  o_SCQTAP02 = 0
  w_SCQTAP03 = 0
  o_SCQTAP03 = 0
  w_SCQTAP04 = 0
  o_SCQTAP04 = 0
  w_SCQTAP05 = 0
  o_SCQTAP05 = 0
  w_SCQTAP06 = 0
  o_SCQTAP06 = 0
  w_SCQTAP07 = 0
  o_SCQTAP07 = 0
  w_SCQTAP08 = 0
  o_SCQTAP08 = 0
  w_SCQTAP09 = 0
  o_SCQTAP09 = 0
  w_SCQTAP10 = 0
  o_SCQTAP10 = 0
  w_SCQTAP11 = 0
  o_SCQTAP11 = 0
  w_SCQTAP12 = 0
  o_SCQTAP12 = 0
  w_SCQTAP13 = 0
  o_SCQTAP13 = 0
  w_SCQTAP14 = 0
  o_SCQTAP14 = 0
  w_OLTUNMIS = space(3)
  w_OLTCOART = space(20)
  w_SCCODCIC = space(15)
  o_SCCODCIC = space(15)
  w_QTARES = 0
  w_OLTSTATO = space(1)
  w_OLTQTOD1 = 0
  w_SCSEQSCA = 0
  w_SCDINRIC = ctod('  /  /  ')
  w_SCCODICE = space(20)
  w_SCCODODL = space(15)
  w_DESCIC = space(40)
  w_OLTDTINI = ctod('  /  /  ')
  w_OLTQTODL = 0
  w_SCQTAODL = 0
  w_SCDESART = space(40)
  w_OLTQTOEV = 0
  w_OLTQTORE = 0
  w_SCRISODL = space(15)
  w_INPUTODL = .F.
  w_SCQTMODL = 0
  w_TOQTAC01 = 0
  w_TOQTAC02 = 0
  w_TOQTAC03 = 0
  w_TOQTAC04 = 0
  w_TOQTAC05 = 0
  w_TOQTAC06 = 0
  w_TOQTAC07 = 0
  w_TOQTAC08 = 0
  w_TOQTAC09 = 0
  w_TOQTAC10 = 0
  w_TOQTAC11 = 0
  w_TOQTAC12 = 0
  w_TOQTAC13 = 0
  w_TOQTAC14 = 0
  w_TOQTARES = 0
  w_SCPIAINI = ctod('  /  /  ')
  w_SCPIAFIN = ctod('  /  /  ')
  w_SCHPER01 = space(20)
  w_SCHPER02 = space(20)
  w_SCHPER03 = space(20)
  w_SCHPER04 = space(20)
  w_SCHPER05 = space(20)
  w_SCHPER06 = space(20)
  w_SCHPER07 = space(20)
  w_SCHPER08 = space(20)
  w_SCHPER09 = space(20)
  w_SCHPER10 = space(20)
  w_SCHPER11 = space(20)
  w_SCHPER12 = space(20)
  w_SCHPER13 = space(20)
  w_SCHPER14 = space(20)
  w_SCTIPRIS = space(2)
  w_SCCODRIS = space(20)
  w_SCCOUPOI = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_SCSERIAL = this.W_SCSERIAL
  w_oHeaderDetail = .NULL.
  w_SUGG = .NULL.
  w_CONFE = .NULL.
  w_LAN = .NULL.
  w_CPPER = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_msp
  pFLGEST=' '
  pPAROBJ=.NULL.
  
  w_AGGIORNATO = .f.
  
  * Dichiarazione Variabili per la Gestione dei calendari
  w_CALSTA= ''
  
  * Dichiarazione Variabili per la Gestione dei Colori
  * Variabili dello sfondo
  w_BASSUG=0
  w_BASCON=0
  w_BASLAN=0
  * Variabili del Testo
  w_TESSUG=0
  w_TESCON=0
  w_TESLAN=0
  
  * ---Variabili per la Gestione del Pop Menu sul Body
  cFileMenu=''
  gsMenu=''
  gsPopup=''
  cEvent=''
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    return(lower('GSCO_MSP,'+this.pFLGEST)+',.null.')
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SCALETTM','gsco_msp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mspPag1","gsco_msp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano di produzione")
      .Pages(1).HelpContextID = 44552008
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsco_msp
    this.parent.pFlGEST =pTipGes
    *
    this.parent.pPAROBJ=pParentObject
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    this.w_SUGG = this.oPgFrm.Pages(1).oPag.SUGG
    this.w_CONFE = this.oPgFrm.Pages(1).oPag.CONFE
    this.w_LAN = this.oPgFrm.Pages(1).oPag.LAN
    this.w_CPPER = this.oPgFrm.Pages(1).oPag.CPPER
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      this.w_SUGG = .NULL.
      this.w_CONFE = .NULL.
      this.w_LAN = .NULL.
      this.w_CPPER = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='RIS_ORSE'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='PAR_PROD'
    this.cWorkTables[5]='TAB_CALE'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='TAB_RISO'
    this.cWorkTables[8]='TABMCICL'
    this.cWorkTables[9]='SCALETTM'
    this.cWorkTables[10]='SCALETTA'
    * --- Area Manuale = Open Work Table
    * --- gsco_msp
    This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
       "IIF(EMPTY(NVL(t_OLTSTATO, SPACE(1))),RGB(255,255,255), IIF(UPPER(t_OLTSTATO)='M',this.Parent.oContained.w_BASSUG, IIF(UPPER(t_OLTSTATO)='P',this.Parent.oContained.w_BASCON, IIF(UPPER(t_OLTSTATO)='L',this.Parent.oContained.w_BASLAN, RGB(255,255,255)))))"
    
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SCALETTM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SCALETTM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SCSERIAL = NVL(SCSERIAL,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from SCALETTM where SCSERIAL=KeySet.SCSERIAL
    *
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2],this.bLoadRecFilter,this.SCALETTM_IDX,"gsco_msp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SCALETTM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SCALETTM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"SCALETTA.","SCALETTM.")
      i_cTable = i_cTable+' SCALETTM '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_RLDESCRI = space(40)
        .w_TIPGES = this.pFLGEST
        .w_CALRIS = space(5)
        .w_SCDESCAL = space(40)
        .w_SCFASSEL = "S"
        .w_VARIATA = iif(.cFunction = 'Load', False , iif(.cFunction =  'Edit', .t., .f.))
        .w_NRVARIATA = 0
        .w_TOQTAC01 = 0
        .w_TOQTAC02 = 0
        .w_TOQTAC03 = 0
        .w_TOQTAC04 = 0
        .w_TOQTAC05 = 0
        .w_TOQTAC06 = 0
        .w_TOQTAC07 = 0
        .w_TOQTAC08 = 0
        .w_TOQTAC09 = 0
        .w_TOQTAC10 = 0
        .w_TOQTAC11 = 0
        .w_TOQTAC12 = 0
        .w_TOQTAC13 = 0
        .w_TOQTAC14 = 0
        .w_TOQTARES = 0
        .w_SCHPER01 = ""
        .w_SCHPER02 = ""
        .w_SCHPER03 = ""
        .w_SCHPER04 = ""
        .w_SCHPER05 = ""
        .w_SCHPER06 = ""
        .w_SCHPER07 = ""
        .w_SCHPER08 = ""
        .w_SCHPER09 = ""
        .w_SCHPER10 = ""
        .w_SCHPER11 = ""
        .w_SCHPER12 = ""
        .w_SCHPER13 = ""
        .w_SCHPER14 = ""
        .w_SCTIPRIS = space(2)
        .w_SCCODRIS = space(20)
        .w_SCCOUPOI = space(1)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_SCSERIAL = NVL(SCSERIAL,space(15))
        .op_SCSERIAL = .w_SCSERIAL
        .w_SCTABRIS = NVL(SCTABRIS,space(20))
          if link_1_3_joined
            this.w_SCTABRIS = NVL(RICODICE103,NVL(this.w_SCTABRIS,space(20)))
            this.w_RLDESCRI = NVL(RIDESCRI103,space(40))
          else
          .link_1_3('Load')
          endif
        .w_SCDATINI = NVL(cp_ToDate(SCDATINI),ctod("  /  /  "))
        .w_SCQTCPDF = NVL(SCQTCPDF,0)
        .w_SCQTAC01 = NVL(SCQTAC01,0)
        .w_SCQTAC02 = NVL(SCQTAC02,0)
        .w_SCQTAC03 = NVL(SCQTAC03,0)
        .w_SCQTAC04 = NVL(SCQTAC04,0)
        .w_SCQTAC05 = NVL(SCQTAC05,0)
        .w_SCQTAC06 = NVL(SCQTAC06,0)
        .w_SCQTAC07 = NVL(SCQTAC07,0)
        .w_SCQTAC08 = NVL(SCQTAC08,0)
        .w_SCQTAC09 = NVL(SCQTAC09,0)
        .w_SCQTAC10 = NVL(SCQTAC10,0)
        .w_SCQTAC11 = NVL(SCQTAC11,0)
        .w_SCQTAC12 = NVL(SCQTAC12,0)
        .w_SCQTAC13 = NVL(SCQTAC13,0)
        .w_SCQTAC14 = NVL(SCQTAC14,0)
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.SUGG.Calculate(AH_MsgFormat("Suggeriti"), .w_TESSUG, .w_BASSUG)
        .oPgFrm.Page1.oPag.CONFE.Calculate(AH_MsgFormat("Pianificati"), .w_TESCON,  .w_BASCON)
        .oPgFrm.Page1.oPag.LAN.Calculate(AH_MsgFormat("Lanciati"), .w_TESLAN, .w_BASLAN)
        .w_SCTIPSCA = NVL(SCTIPSCA,space(3))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_SCCALRIS = NVL(SCCALRIS,space(5))
          .link_1_36('Load')
        .w_SCTIPPIA = NVL(SCTIPPIA,space(1))
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_SCODLSUG = NVL(SCODLSUG,space(1))
        .w_SCODLPIA = NVL(SCODLPIA,space(1))
        .w_SCODLLAN = NVL(SCODLLAN,space(1))
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_SCDATFIN = NVL(cp_ToDate(SCDATFIN),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.CPPER.Calculate(iif(.w_SCTIPSCA='CSQ', ah_MsgFormat("Cap. periodo quantit�"), ah_Msgformat("Cap. periodo ore")), RGB(0,0,0))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_23.Calculate()
        .w_SCPIAINI = NVL(cp_ToDate(SCPIAINI),ctod("  /  /  "))
        .w_SCPIAFIN = NVL(cp_ToDate(SCPIAFIN),ctod("  /  /  "))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'SCALETTM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from SCALETTA where SCSERIAL=KeySet.SCSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.SCALETTA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCALETTA_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('SCALETTA')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "SCALETTA.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" SCALETTA"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
        select * from (i_cTable) SCALETTA where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOQTAC01 = 0
      this.w_TOQTAC02 = 0
      this.w_TOQTAC03 = 0
      this.w_TOQTAC04 = 0
      this.w_TOQTAC05 = 0
      this.w_TOQTAC06 = 0
      this.w_TOQTAC07 = 0
      this.w_TOQTAC08 = 0
      this.w_TOQTAC09 = 0
      this.w_TOQTAC10 = 0
      this.w_TOQTAC11 = 0
      this.w_TOQTAC12 = 0
      this.w_TOQTAC13 = 0
      this.w_TOQTAC14 = 0
      this.w_TOQTARES = 0
      scan
        with this
          .w_OLTUNMIS = space(3)
          .w_OLTCOART = space(20)
          .w_OLTSTATO = space(1)
        .w_OLTQTOD1 = 0
          .w_SCDINRIC = ctod("  /  /  ")
          .w_SCCODICE = space(20)
          .w_SCCODODL = space(15)
          .w_DESCIC = space(40)
          .w_OLTDTINI = ctod("  /  /  ")
          .w_OLTQTODL = 0
          .w_SCDESART = space(40)
          .w_OLTQTOEV = 0
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_SCCODICE = NVL(SCCODICE,space(20))
          .link_2_2('Load')
          .w_SCCODODL = NVL(SCCODODL,space(15))
          if link_2_3_joined
            this.w_SCCODODL = NVL(OLCODODL203,NVL(this.w_SCCODODL,space(15)))
            this.w_SCCODICE = NVL(OLTCODIC203,space(20))
            this.w_SCDINRIC = NVL(cp_ToDate(OLTDINRIC203),ctod("  /  /  "))
            this.w_OLTUNMIS = NVL(OLTUNMIS203,space(3))
            this.w_OLTCOART = NVL(OLTCOART203,space(20))
            this.w_SCCODCIC = NVL(OLTCICLO203,space(15))
            this.w_OLTSTATO = NVL(OLTSTATO203,space(1))
            this.w_OLTQTOD1 = NVL(OLTQTOD1203,0)
            this.w_OLTQTODL = NVL(OLTQTODL203,0)
            this.w_OLTDTINI = NVL(cp_ToDate(OLTDTINI203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_SCQTARIC = NVL(SCQTARIC,0)
          .w_SCQTAP01 = NVL(SCQTAP01,0)
          .w_SCQTAP02 = NVL(SCQTAP02,0)
          .w_SCQTAP03 = NVL(SCQTAP03,0)
          .w_SCQTAP04 = NVL(SCQTAP04,0)
          .w_SCQTAP05 = NVL(SCQTAP05,0)
          .w_SCQTAP06 = NVL(SCQTAP06,0)
          .w_SCQTAP07 = NVL(SCQTAP07,0)
          .w_SCQTAP08 = NVL(SCQTAP08,0)
          .w_SCQTAP09 = NVL(SCQTAP09,0)
          .w_SCQTAP10 = NVL(SCQTAP10,0)
          .w_SCQTAP11 = NVL(SCQTAP11,0)
          .w_SCQTAP12 = NVL(SCQTAP12,0)
          .w_SCQTAP13 = NVL(SCQTAP13,0)
          .w_SCQTAP14 = NVL(SCQTAP14,0)
          .link_2_20('Load')
        .w_SCCODCIC = .w_SCCODCIC
          .link_2_21('Load')
        .w_QTARES = .w_SCQTARIC-(nvl(.w_SCQTAP01,0)+nvl(.w_SCQTAP02,0)+nvl(.w_SCQTAP03,0)+nvl(.w_SCQTAP04,0)+nvl(.w_SCQTAP05,0)+nvl(.w_SCQTAP06,0)+nvl(.w_SCQTAP07,0)+nvl(.w_SCQTAP08,0)+nvl(.w_SCQTAP09,0)+nvl(.w_SCQTAP10,0)+nvl(.w_SCQTAP11,0)+nvl(.w_SCQTAP12,0)+nvl(.w_SCQTAP13,0)+nvl(.w_SCQTAP14,0))
        .w_SCSEQSCA = .w_CPROWORD
          .link_2_28('Load')
          .w_SCQTAODL = NVL(SCQTAODL,0)
        .w_OLTQTORE = .w_OLTQTODL-.w_OLTQTOEV
          .w_SCRISODL = NVL(SCRISODL,space(15))
        .w_INPUTODL = .t.
          .w_SCQTMODL = NVL(SCQTMODL,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOQTAC01 = .w_TOQTAC01+.w_SCQTAP01
          .w_TOQTAC02 = .w_TOQTAC02+.w_SCQTAP02
          .w_TOQTAC03 = .w_TOQTAC03+.w_SCQTAP03
          .w_TOQTAC04 = .w_TOQTAC04+.w_SCQTAP04
          .w_TOQTAC05 = .w_TOQTAC05+.w_SCQTAP05
          .w_TOQTAC06 = .w_TOQTAC06+.w_SCQTAP06
          .w_TOQTAC07 = .w_TOQTAC07+.w_SCQTAP07
          .w_TOQTAC08 = .w_TOQTAC08+.w_SCQTAP08
          .w_TOQTAC09 = .w_TOQTAC09+.w_SCQTAP09
          .w_TOQTAC10 = .w_TOQTAC10+.w_SCQTAP10
          .w_TOQTAC11 = .w_TOQTAC11+.w_SCQTAP11
          .w_TOQTAC12 = .w_TOQTAC12+.w_SCQTAP12
          .w_TOQTAC13 = .w_TOQTAC13+.w_SCQTAP13
          .w_TOQTAC14 = .w_TOQTAC14+.w_SCQTAP14
          .w_TOQTARES = .w_TOQTARES+.w_QTARES
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.SUGG.Calculate(AH_MsgFormat("Suggeriti"), .w_TESSUG, .w_BASSUG)
        .oPgFrm.Page1.oPag.CONFE.Calculate(AH_MsgFormat("Pianificati"), .w_TESCON,  .w_BASCON)
        .oPgFrm.Page1.oPag.LAN.Calculate(AH_MsgFormat("Lanciati"), .w_TESLAN, .w_BASLAN)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.CPPER.Calculate(iif(.w_SCTIPSCA='CSQ', ah_MsgFormat("Cap. periodo quantit�"), ah_Msgformat("Cap. periodo ore")), RGB(0,0,0))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_23.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_3_19.enabled = .oPgFrm.Page1.oPag.oBtn_3_19.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsco_msp
    if inlist (this.w_TIPGES, 'LQA', 'LQO') and this.w_SCTIPSCA <> 'CSQ'
       this.BlankRec()
    endif
    
    if inlist (this.w_TIPGES, 'LOA', 'LOO') and this.w_SCTIPSCA <> 'CSO'
       this.BlankRec()
    endif
    
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_SCSERIAL=space(15)
      .w_SCTABRIS=space(20)
      .w_SCDATINI=ctod("  /  /  ")
      .w_SCQTCPDF=0
      .w_SCQTAC01=0
      .w_SCQTAC02=0
      .w_SCQTAC03=0
      .w_SCQTAC04=0
      .w_SCQTAC05=0
      .w_SCQTAC06=0
      .w_SCQTAC07=0
      .w_SCQTAC08=0
      .w_SCQTAC09=0
      .w_SCQTAC10=0
      .w_SCQTAC11=0
      .w_SCQTAC12=0
      .w_SCQTAC13=0
      .w_SCQTAC14=0
      .w_RLDESCRI=space(40)
      .w_TIPGES=space(3)
      .w_SCTIPSCA=space(3)
      .w_CALRIS=space(5)
      .w_SCCALRIS=space(5)
      .w_SCDESCAL=space(40)
      .w_SCFASSEL=space(1)
      .w_SCTIPPIA=space(1)
      .w_SCODLSUG=space(1)
      .w_SCODLPIA=space(1)
      .w_SCODLLAN=space(1)
      .w_SCDATFIN=ctod("  /  /  ")
      .w_VARIATA=.f.
      .w_NRVARIATA=0
      .w_CPROWORD=10
      .w_SCCODICE=space(20)
      .w_SCCODODL=space(15)
      .w_SCQTARIC=0
      .w_SCQTAP01=0
      .w_SCQTAP02=0
      .w_SCQTAP03=0
      .w_SCQTAP04=0
      .w_SCQTAP05=0
      .w_SCQTAP06=0
      .w_SCQTAP07=0
      .w_SCQTAP08=0
      .w_SCQTAP09=0
      .w_SCQTAP10=0
      .w_SCQTAP11=0
      .w_SCQTAP12=0
      .w_SCQTAP13=0
      .w_SCQTAP14=0
      .w_OLTUNMIS=space(3)
      .w_OLTCOART=space(20)
      .w_SCCODCIC=space(15)
      .w_QTARES=0
      .w_OLTSTATO=space(1)
      .w_OLTQTOD1=0
      .w_SCSEQSCA=0
      .w_SCDINRIC=ctod("  /  /  ")
      .w_SCCODICE=space(20)
      .w_SCCODODL=space(15)
      .w_DESCIC=space(40)
      .w_OLTDTINI=ctod("  /  /  ")
      .w_OLTQTODL=0
      .w_SCQTAODL=0
      .w_SCDESART=space(40)
      .w_OLTQTOEV=0
      .w_OLTQTORE=0
      .w_SCRISODL=space(15)
      .w_INPUTODL=.f.
      .w_SCQTMODL=0
      .w_TOQTAC01=0
      .w_TOQTAC02=0
      .w_TOQTAC03=0
      .w_TOQTAC04=0
      .w_TOQTAC05=0
      .w_TOQTAC06=0
      .w_TOQTAC07=0
      .w_TOQTAC08=0
      .w_TOQTAC09=0
      .w_TOQTAC10=0
      .w_TOQTAC11=0
      .w_TOQTAC12=0
      .w_TOQTAC13=0
      .w_TOQTAC14=0
      .w_TOQTARES=0
      .w_SCPIAINI=ctod("  /  /  ")
      .w_SCPIAFIN=ctod("  /  /  ")
      .w_SCHPER01=space(20)
      .w_SCHPER02=space(20)
      .w_SCHPER03=space(20)
      .w_SCHPER04=space(20)
      .w_SCHPER05=space(20)
      .w_SCHPER06=space(20)
      .w_SCHPER07=space(20)
      .w_SCHPER08=space(20)
      .w_SCHPER09=space(20)
      .w_SCHPER10=space(20)
      .w_SCHPER11=space(20)
      .w_SCHPER12=space(20)
      .w_SCHPER13=space(20)
      .w_SCHPER14=space(20)
      .w_SCTIPRIS=space(2)
      .w_SCCODRIS=space(20)
      .w_SCCOUPOI=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_SCTABRIS))
         .link_1_3('Full')
        endif
        .w_SCDATINI = i_datsys
        .w_SCQTCPDF = 0
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.SUGG.Calculate(AH_MsgFormat("Suggeriti"), .w_TESSUG, .w_BASSUG)
        .oPgFrm.Page1.oPag.CONFE.Calculate(AH_MsgFormat("Pianificati"), .w_TESCON,  .w_BASCON)
        .oPgFrm.Page1.oPag.LAN.Calculate(AH_MsgFormat("Lanciati"), .w_TESLAN, .w_BASLAN)
        .DoRTCalc(5,19,.f.)
        .w_TIPGES = this.pFLGEST
        .w_SCTIPSCA = IIF(inlist(.w_TIPGES, 'LQA', 'LQO'), 'CSQ', SPACES(3))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(22,22,.f.)
        .w_SCCALRIS = IIF(EMPTY(.w_CALRIS), .w_CALSTA, .w_CALRIS)
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_SCCALRIS))
         .link_1_36('Full')
        endif
        .DoRTCalc(24,24,.f.)
        .w_SCFASSEL = "S"
        .w_SCTIPPIA = "G"
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_SCODLSUG = '.'
        .w_SCODLPIA = 'P'
        .w_SCODLLAN = 'L'
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .DoRTCalc(30,30,.f.)
        .w_VARIATA = iif(.cFunction = 'Load', False , iif(.cFunction =  'Edit', .t., .f.))
        .oPgFrm.Page1.oPag.CPPER.Calculate(iif(.w_SCTIPSCA='CSQ', ah_MsgFormat("Cap. periodo quantit�"), ah_Msgformat("Cap. periodo ore")), RGB(0,0,0))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .DoRTCalc(32,34,.f.)
        if not(empty(.w_SCCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_SCCODODL))
         .link_2_3('Full')
        endif
        .w_SCQTARIC = .w_SCQTAODL
        .DoRTCalc(37,52,.f.)
        if not(empty(.w_OLTCOART))
         .link_2_20('Full')
        endif
        .w_SCCODCIC = .w_SCCODCIC
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_SCCODCIC))
         .link_2_21('Full')
        endif
        .w_QTARES = .w_SCQTARIC-(nvl(.w_SCQTAP01,0)+nvl(.w_SCQTAP02,0)+nvl(.w_SCQTAP03,0)+nvl(.w_SCQTAP04,0)+nvl(.w_SCQTAP05,0)+nvl(.w_SCQTAP06,0)+nvl(.w_SCQTAP07,0)+nvl(.w_SCQTAP08,0)+nvl(.w_SCQTAP09,0)+nvl(.w_SCQTAP10,0)+nvl(.w_SCQTAP11,0)+nvl(.w_SCQTAP12,0)+nvl(.w_SCQTAP13,0)+nvl(.w_SCQTAP14,0))
        .DoRTCalc(55,55,.f.)
        .w_OLTQTOD1 = 0
        .w_SCSEQSCA = .w_CPROWORD
        .DoRTCalc(58,59,.f.)
        if not(empty(.w_SCCODICE))
         .link_2_28('Full')
        endif
        .DoRTCalc(60,63,.f.)
        .w_SCQTAODL = 0
        .DoRTCalc(65,66,.f.)
        .w_OLTQTORE = .w_OLTQTODL-.w_OLTQTOEV
        .DoRTCalc(68,68,.f.)
        .w_INPUTODL = .t.
        .w_SCQTMODL = 0
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_23.Calculate()
        .DoRTCalc(71,85,.f.)
        .w_SCPIAINI = i_datsys
        .DoRTCalc(87,87,.f.)
        .w_SCHPER01 = ""
        .w_SCHPER02 = ""
        .w_SCHPER03 = ""
        .w_SCHPER04 = ""
        .w_SCHPER05 = ""
        .w_SCHPER06 = ""
        .w_SCHPER07 = ""
        .w_SCHPER08 = ""
        .w_SCHPER09 = ""
        .w_SCHPER10 = ""
        .w_SCHPER11 = ""
        .w_SCHPER12 = ""
        .w_SCHPER13 = ""
        .w_SCHPER14 = ""
      endif
    endwith
    cp_BlankRecExtFlds(this,'SCALETTM')
    this.DoRTCalc(102,104,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_3_19.enabled = this.oPgFrm.Page1.oPag.oBtn_3_19.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCSERIAL_1_2.enabled = i_bVal
      .Page1.oPag.oSCTABRIS_1_3.enabled = i_bVal
      .Page1.oPag.oSCDATINI_1_4.enabled = i_bVal
      .Page1.oPag.oSCQTCPDF_1_5.enabled = i_bVal
      .Page1.oPag.oSCQTAC01_1_6.enabled = i_bVal
      .Page1.oPag.oSCQTAC02_1_7.enabled = i_bVal
      .Page1.oPag.oSCQTAC03_1_8.enabled = i_bVal
      .Page1.oPag.oSCQTAC04_1_9.enabled = i_bVal
      .Page1.oPag.oSCQTAC05_1_10.enabled = i_bVal
      .Page1.oPag.oSCQTAC06_1_12.enabled = i_bVal
      .Page1.oPag.oSCQTAC07_1_13.enabled = i_bVal
      .Page1.oPag.oSCQTAC08_1_14.enabled = i_bVal
      .Page1.oPag.oSCQTAC09_1_15.enabled = i_bVal
      .Page1.oPag.oSCQTAC10_1_16.enabled = i_bVal
      .Page1.oPag.oSCQTAC11_1_17.enabled = i_bVal
      .Page1.oPag.oSCQTAC12_1_18.enabled = i_bVal
      .Page1.oPag.oSCQTAC13_1_19.enabled = i_bVal
      .Page1.oPag.oSCQTAC14_1_20.enabled = i_bVal
      .Page1.oPag.oSCCALRIS_1_36.enabled = i_bVal
      .Page1.oPag.oSCTIPPIA_1_40.enabled = i_bVal
      .Page1.oPag.oSCODLSUG_1_42.enabled = i_bVal
      .Page1.oPag.oSCODLPIA_1_43.enabled = i_bVal
      .Page1.oPag.oSCODLLAN_1_44.enabled = i_bVal
      .Page1.oPag.oSCPIAINI_1_63.enabled = i_bVal
      .Page1.oPag.oSCPIAFIN_1_64.enabled = i_bVal
      .Page1.oPag.oBtn_3_3.enabled = i_bVal
      .Page1.oPag.oBtn_3_4.enabled = i_bVal
      .Page1.oPag.oBtn_3_18.enabled = i_bVal
      .Page1.oPag.oBtn_3_19.enabled = .Page1.oPag.oBtn_3_19.mCond()
      .Page1.oPag.oBtn_3_20.enabled = i_bVal
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_47.enabled = i_bVal
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.enabled = i_bVal
      .Page1.oPag.oObj_3_21.enabled = i_bVal
      .Page1.oPag.oObj_3_22.enabled = i_bVal
      .Page1.oPag.oObj_3_23.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSCSERIAL_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSCSERIAL_1_2.enabled = .t.
        .Page1.oPag.oSCTABRIS_1_3.enabled = .t.
        .Page1.oPag.oSCDATINI_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SCALETTM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SCLTT","i_codazi,w_SCSERIAL")
      .op_codazi = .w_codazi
      .op_SCSERIAL = .w_SCSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSERIAL,"SCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTABRIS,"SCTABRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATINI,"SCDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTCPDF,"SCQTCPDF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC01,"SCQTAC01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC02,"SCQTAC02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC03,"SCQTAC03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC04,"SCQTAC04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC05,"SCQTAC05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC06,"SCQTAC06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC07,"SCQTAC07",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC08,"SCQTAC08",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC09,"SCQTAC09",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC10,"SCQTAC10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC11,"SCQTAC11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC12,"SCQTAC12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC13,"SCQTAC13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAC14,"SCQTAC14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPSCA,"SCTIPSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCALRIS,"SCCALRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPPIA,"SCTIPPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCODLSUG,"SCODLSUG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCODLPIA,"SCODLPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCODLLAN,"SCODLLAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATFIN,"SCDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPIAINI,"SCPIAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPIAFIN,"SCPIAFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
    i_lTable = "SCALETTM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SCALETTM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_SCCODICE C(20);
      ,t_SCCODODL C(15);
      ,t_SCQTARIC N(12,5);
      ,t_SCQTAP01 N(12,5);
      ,t_SCQTAP02 N(12,5);
      ,t_SCQTAP03 N(12,5);
      ,t_SCQTAP04 N(12,5);
      ,t_SCQTAP05 N(12,5);
      ,t_SCQTAP06 N(12,5);
      ,t_SCQTAP07 N(12,5);
      ,t_SCQTAP08 N(12,5);
      ,t_SCQTAP09 N(12,5);
      ,t_SCQTAP10 N(12,5);
      ,t_SCQTAP11 N(12,5);
      ,t_SCQTAP12 N(12,5);
      ,t_SCQTAP13 N(12,5);
      ,t_SCQTAP14 N(12,5);
      ,t_OLTUNMIS C(3);
      ,t_SCCODCIC C(15);
      ,t_QTARES N(12,5);
      ,t_SCDINRIC D(8);
      ,t_DESCIC C(40);
      ,t_OLTDTINI D(8);
      ,t_OLTQTODL N(12,3);
      ,t_SCDESART C(40);
      ,t_OLTQTOEV N(12,3);
      ,t_OLTQTORE N(12,3);
      ,CPROWNUM N(10);
      ,t_OLTCOART C(20);
      ,t_OLTSTATO C(1);
      ,t_OLTQTOD1 N(12,3);
      ,t_SCSEQSCA N(5);
      ,t_SCQTAODL N(18,4);
      ,t_SCRISODL C(15);
      ,t_INPUTODL L(1);
      ,t_SCQTMODL N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mspbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2.controlsource=this.cTrsName+'.t_SCCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3.controlsource=this.cTrsName+'.t_SCCODODL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTARIC_2_4.controlsource=this.cTrsName+'.t_SCQTARIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP01_2_5.controlsource=this.cTrsName+'.t_SCQTAP01'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP02_2_6.controlsource=this.cTrsName+'.t_SCQTAP02'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP03_2_7.controlsource=this.cTrsName+'.t_SCQTAP03'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP04_2_8.controlsource=this.cTrsName+'.t_SCQTAP04'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP05_2_9.controlsource=this.cTrsName+'.t_SCQTAP05'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP06_2_10.controlsource=this.cTrsName+'.t_SCQTAP06'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP07_2_11.controlsource=this.cTrsName+'.t_SCQTAP07'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP08_2_12.controlsource=this.cTrsName+'.t_SCQTAP08'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP09_2_13.controlsource=this.cTrsName+'.t_SCQTAP09'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP10_2_14.controlsource=this.cTrsName+'.t_SCQTAP10'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP11_2_15.controlsource=this.cTrsName+'.t_SCQTAP11'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP12_2_16.controlsource=this.cTrsName+'.t_SCQTAP12'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP13_2_17.controlsource=this.cTrsName+'.t_SCQTAP13'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP14_2_18.controlsource=this.cTrsName+'.t_SCQTAP14'
    this.oPgFRm.Page1.oPag.oOLTUNMIS_2_19.controlsource=this.cTrsName+'.t_OLTUNMIS'
    this.oPgFRm.Page1.oPag.oSCCODCIC_2_21.controlsource=this.cTrsName+'.t_SCCODCIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oQTARES_2_22.controlsource=this.cTrsName+'.t_QTARES'
    this.oPgFRm.Page1.oPag.oSCDINRIC_2_27.controlsource=this.cTrsName+'.t_SCDINRIC'
    this.oPgFRm.Page1.oPag.oSCCODICE_2_28.controlsource=this.cTrsName+'.t_SCCODICE'
    this.oPgFRm.Page1.oPag.oSCCODODL_2_29.controlsource=this.cTrsName+'.t_SCCODODL'
    this.oPgFRm.Page1.oPag.oDESCIC_2_30.controlsource=this.cTrsName+'.t_DESCIC'
    this.oPgFRm.Page1.oPag.oOLTDTINI_2_31.controlsource=this.cTrsName+'.t_OLTDTINI'
    this.oPgFRm.Page1.oPag.oOLTQTODL_2_32.controlsource=this.cTrsName+'.t_OLTQTODL'
    this.oPgFRm.Page1.oPag.oSCDESART_2_34.controlsource=this.cTrsName+'.t_SCDESART'
    this.oPgFRm.Page1.oPag.oOLTQTOEV_2_35.controlsource=this.cTrsName+'.t_OLTQTOEV'
    this.oPgFRm.Page1.oPag.oOLTQTORE_2_36.controlsource=this.cTrsName+'.t_OLTQTORE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(36)
    this.AddVLine(149)
    this.AddVLine(215)
    this.AddVLine(270)
    this.AddVLine(324)
    this.AddVLine(377)
    this.AddVLine(430)
    this.AddVLine(484)
    this.AddVLine(537)
    this.AddVLine(590)
    this.AddVLine(643)
    this.AddVLine(696)
    this.AddVLine(749)
    this.AddVLine(802)
    this.AddVLine(855)
    this.AddVLine(908)
    this.AddVLine(962)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SCALETTM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SCLTT","i_codazi,w_SCSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SCALETTM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SCALETTM')
        i_extval=cp_InsertValODBCExtFlds(this,'SCALETTM')
        local i_cFld
        i_cFld=" "+;
                  "(SCSERIAL,SCTABRIS,SCDATINI,SCQTCPDF,SCQTAC01"+;
                  ",SCQTAC02,SCQTAC03,SCQTAC04,SCQTAC05,SCQTAC06"+;
                  ",SCQTAC07,SCQTAC08,SCQTAC09,SCQTAC10,SCQTAC11"+;
                  ",SCQTAC12,SCQTAC13,SCQTAC14,SCTIPSCA,SCCALRIS"+;
                  ",SCTIPPIA,SCODLSUG,SCODLPIA,SCODLLAN,SCDATFIN"+;
                  ",SCPIAINI,SCPIAFIN"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_SCSERIAL)+;
                    ","+cp_ToStrODBCNull(this.w_SCTABRIS)+;
                    ","+cp_ToStrODBC(this.w_SCDATINI)+;
                    ","+cp_ToStrODBC(this.w_SCQTCPDF)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC01)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC02)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC03)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC04)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC05)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC06)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC07)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC08)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC09)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC10)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC11)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC12)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC13)+;
                    ","+cp_ToStrODBC(this.w_SCQTAC14)+;
                    ","+cp_ToStrODBC(this.w_SCTIPSCA)+;
                    ","+cp_ToStrODBCNull(this.w_SCCALRIS)+;
                    ","+cp_ToStrODBC(this.w_SCTIPPIA)+;
                    ","+cp_ToStrODBC(this.w_SCODLSUG)+;
                    ","+cp_ToStrODBC(this.w_SCODLPIA)+;
                    ","+cp_ToStrODBC(this.w_SCODLLAN)+;
                    ","+cp_ToStrODBC(this.w_SCDATFIN)+;
                    ","+cp_ToStrODBC(this.w_SCPIAINI)+;
                    ","+cp_ToStrODBC(this.w_SCPIAFIN)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SCALETTM')
        i_extval=cp_InsertValVFPExtFlds(this,'SCALETTM')
        cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.w_SCSERIAL)
        INSERT INTO (i_cTable);
              (SCSERIAL,SCTABRIS,SCDATINI,SCQTCPDF,SCQTAC01,SCQTAC02,SCQTAC03,SCQTAC04,SCQTAC05,SCQTAC06,SCQTAC07,SCQTAC08,SCQTAC09,SCQTAC10,SCQTAC11,SCQTAC12,SCQTAC13,SCQTAC14,SCTIPSCA,SCCALRIS,SCTIPPIA,SCODLSUG,SCODLPIA,SCODLLAN,SCDATFIN,SCPIAINI,SCPIAFIN &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_SCSERIAL;
                  ,this.w_SCTABRIS;
                  ,this.w_SCDATINI;
                  ,this.w_SCQTCPDF;
                  ,this.w_SCQTAC01;
                  ,this.w_SCQTAC02;
                  ,this.w_SCQTAC03;
                  ,this.w_SCQTAC04;
                  ,this.w_SCQTAC05;
                  ,this.w_SCQTAC06;
                  ,this.w_SCQTAC07;
                  ,this.w_SCQTAC08;
                  ,this.w_SCQTAC09;
                  ,this.w_SCQTAC10;
                  ,this.w_SCQTAC11;
                  ,this.w_SCQTAC12;
                  ,this.w_SCQTAC13;
                  ,this.w_SCQTAC14;
                  ,this.w_SCTIPSCA;
                  ,this.w_SCCALRIS;
                  ,this.w_SCTIPPIA;
                  ,this.w_SCODLSUG;
                  ,this.w_SCODLPIA;
                  ,this.w_SCODLLAN;
                  ,this.w_SCDATFIN;
                  ,this.w_SCPIAINI;
                  ,this.w_SCPIAFIN;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SCALETTA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCALETTA_IDX,2])
      *
      * insert into SCALETTA
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(SCSERIAL,CPROWORD,SCCODICE,SCCODODL,SCQTARIC"+;
                  ",SCQTAP01,SCQTAP02,SCQTAP03,SCQTAP04,SCQTAP05"+;
                  ",SCQTAP06,SCQTAP07,SCQTAP08,SCQTAP09,SCQTAP10"+;
                  ",SCQTAP11,SCQTAP12,SCQTAP13,SCQTAP14,SCQTAODL"+;
                  ",SCRISODL,SCQTMODL,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SCSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_SCCODICE)+","+cp_ToStrODBCNull(this.w_SCCODODL)+","+cp_ToStrODBC(this.w_SCQTARIC)+;
             ","+cp_ToStrODBC(this.w_SCQTAP01)+","+cp_ToStrODBC(this.w_SCQTAP02)+","+cp_ToStrODBC(this.w_SCQTAP03)+","+cp_ToStrODBC(this.w_SCQTAP04)+","+cp_ToStrODBC(this.w_SCQTAP05)+;
             ","+cp_ToStrODBC(this.w_SCQTAP06)+","+cp_ToStrODBC(this.w_SCQTAP07)+","+cp_ToStrODBC(this.w_SCQTAP08)+","+cp_ToStrODBC(this.w_SCQTAP09)+","+cp_ToStrODBC(this.w_SCQTAP10)+;
             ","+cp_ToStrODBC(this.w_SCQTAP11)+","+cp_ToStrODBC(this.w_SCQTAP12)+","+cp_ToStrODBC(this.w_SCQTAP13)+","+cp_ToStrODBC(this.w_SCQTAP14)+","+cp_ToStrODBC(this.w_SCQTAODL)+;
             ","+cp_ToStrODBC(this.w_SCRISODL)+","+cp_ToStrODBC(this.w_SCQTMODL)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SCSERIAL',this.w_SCSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_SCSERIAL,this.w_CPROWORD,this.w_SCCODICE,this.w_SCCODODL,this.w_SCQTARIC"+;
                ",this.w_SCQTAP01,this.w_SCQTAP02,this.w_SCQTAP03,this.w_SCQTAP04,this.w_SCQTAP05"+;
                ",this.w_SCQTAP06,this.w_SCQTAP07,this.w_SCQTAP08,this.w_SCQTAP09,this.w_SCQTAP10"+;
                ",this.w_SCQTAP11,this.w_SCQTAP12,this.w_SCQTAP13,this.w_SCQTAP14,this.w_SCQTAODL"+;
                ",this.w_SCRISODL,this.w_SCQTMODL,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- gsco_msp
    this.w_VARIATA = False
    
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.SCALETTM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update SCALETTM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'SCALETTM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " SCTABRIS="+cp_ToStrODBCNull(this.w_SCTABRIS)+;
             ",SCDATINI="+cp_ToStrODBC(this.w_SCDATINI)+;
             ",SCQTCPDF="+cp_ToStrODBC(this.w_SCQTCPDF)+;
             ",SCQTAC01="+cp_ToStrODBC(this.w_SCQTAC01)+;
             ",SCQTAC02="+cp_ToStrODBC(this.w_SCQTAC02)+;
             ",SCQTAC03="+cp_ToStrODBC(this.w_SCQTAC03)+;
             ",SCQTAC04="+cp_ToStrODBC(this.w_SCQTAC04)+;
             ",SCQTAC05="+cp_ToStrODBC(this.w_SCQTAC05)+;
             ",SCQTAC06="+cp_ToStrODBC(this.w_SCQTAC06)+;
             ",SCQTAC07="+cp_ToStrODBC(this.w_SCQTAC07)+;
             ",SCQTAC08="+cp_ToStrODBC(this.w_SCQTAC08)+;
             ",SCQTAC09="+cp_ToStrODBC(this.w_SCQTAC09)+;
             ",SCQTAC10="+cp_ToStrODBC(this.w_SCQTAC10)+;
             ",SCQTAC11="+cp_ToStrODBC(this.w_SCQTAC11)+;
             ",SCQTAC12="+cp_ToStrODBC(this.w_SCQTAC12)+;
             ",SCQTAC13="+cp_ToStrODBC(this.w_SCQTAC13)+;
             ",SCQTAC14="+cp_ToStrODBC(this.w_SCQTAC14)+;
             ",SCTIPSCA="+cp_ToStrODBC(this.w_SCTIPSCA)+;
             ",SCCALRIS="+cp_ToStrODBCNull(this.w_SCCALRIS)+;
             ",SCTIPPIA="+cp_ToStrODBC(this.w_SCTIPPIA)+;
             ",SCODLSUG="+cp_ToStrODBC(this.w_SCODLSUG)+;
             ",SCODLPIA="+cp_ToStrODBC(this.w_SCODLPIA)+;
             ",SCODLLAN="+cp_ToStrODBC(this.w_SCODLLAN)+;
             ",SCDATFIN="+cp_ToStrODBC(this.w_SCDATFIN)+;
             ",SCPIAINI="+cp_ToStrODBC(this.w_SCPIAINI)+;
             ",SCPIAFIN="+cp_ToStrODBC(this.w_SCPIAFIN)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'SCALETTM')
          i_cWhere = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
          UPDATE (i_cTable) SET;
              SCTABRIS=this.w_SCTABRIS;
             ,SCDATINI=this.w_SCDATINI;
             ,SCQTCPDF=this.w_SCQTCPDF;
             ,SCQTAC01=this.w_SCQTAC01;
             ,SCQTAC02=this.w_SCQTAC02;
             ,SCQTAC03=this.w_SCQTAC03;
             ,SCQTAC04=this.w_SCQTAC04;
             ,SCQTAC05=this.w_SCQTAC05;
             ,SCQTAC06=this.w_SCQTAC06;
             ,SCQTAC07=this.w_SCQTAC07;
             ,SCQTAC08=this.w_SCQTAC08;
             ,SCQTAC09=this.w_SCQTAC09;
             ,SCQTAC10=this.w_SCQTAC10;
             ,SCQTAC11=this.w_SCQTAC11;
             ,SCQTAC12=this.w_SCQTAC12;
             ,SCQTAC13=this.w_SCQTAC13;
             ,SCQTAC14=this.w_SCQTAC14;
             ,SCTIPSCA=this.w_SCTIPSCA;
             ,SCCALRIS=this.w_SCCALRIS;
             ,SCTIPPIA=this.w_SCTIPPIA;
             ,SCODLSUG=this.w_SCODLSUG;
             ,SCODLPIA=this.w_SCODLPIA;
             ,SCODLLAN=this.w_SCODLLAN;
             ,SCDATFIN=this.w_SCDATFIN;
             ,SCPIAINI=this.w_SCPIAINI;
             ,SCPIAFIN=this.w_SCPIAFIN;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_SCCODODL))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.SCALETTA_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.SCALETTA_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from SCALETTA
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update SCALETTA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SCCODICE="+cp_ToStrODBCNull(this.w_SCCODICE)+;
                     ",SCCODODL="+cp_ToStrODBCNull(this.w_SCCODODL)+;
                     ",SCQTARIC="+cp_ToStrODBC(this.w_SCQTARIC)+;
                     ",SCQTAP01="+cp_ToStrODBC(this.w_SCQTAP01)+;
                     ",SCQTAP02="+cp_ToStrODBC(this.w_SCQTAP02)+;
                     ",SCQTAP03="+cp_ToStrODBC(this.w_SCQTAP03)+;
                     ",SCQTAP04="+cp_ToStrODBC(this.w_SCQTAP04)+;
                     ",SCQTAP05="+cp_ToStrODBC(this.w_SCQTAP05)+;
                     ",SCQTAP06="+cp_ToStrODBC(this.w_SCQTAP06)+;
                     ",SCQTAP07="+cp_ToStrODBC(this.w_SCQTAP07)+;
                     ",SCQTAP08="+cp_ToStrODBC(this.w_SCQTAP08)+;
                     ",SCQTAP09="+cp_ToStrODBC(this.w_SCQTAP09)+;
                     ",SCQTAP10="+cp_ToStrODBC(this.w_SCQTAP10)+;
                     ",SCQTAP11="+cp_ToStrODBC(this.w_SCQTAP11)+;
                     ",SCQTAP12="+cp_ToStrODBC(this.w_SCQTAP12)+;
                     ",SCQTAP13="+cp_ToStrODBC(this.w_SCQTAP13)+;
                     ",SCQTAP14="+cp_ToStrODBC(this.w_SCQTAP14)+;
                     ",SCQTAODL="+cp_ToStrODBC(this.w_SCQTAODL)+;
                     ",SCRISODL="+cp_ToStrODBC(this.w_SCRISODL)+;
                     ",SCQTMODL="+cp_ToStrODBC(this.w_SCQTMODL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SCCODICE=this.w_SCCODICE;
                     ,SCCODODL=this.w_SCCODODL;
                     ,SCQTARIC=this.w_SCQTARIC;
                     ,SCQTAP01=this.w_SCQTAP01;
                     ,SCQTAP02=this.w_SCQTAP02;
                     ,SCQTAP03=this.w_SCQTAP03;
                     ,SCQTAP04=this.w_SCQTAP04;
                     ,SCQTAP05=this.w_SCQTAP05;
                     ,SCQTAP06=this.w_SCQTAP06;
                     ,SCQTAP07=this.w_SCQTAP07;
                     ,SCQTAP08=this.w_SCQTAP08;
                     ,SCQTAP09=this.w_SCQTAP09;
                     ,SCQTAP10=this.w_SCQTAP10;
                     ,SCQTAP11=this.w_SCQTAP11;
                     ,SCQTAP12=this.w_SCQTAP12;
                     ,SCQTAP13=this.w_SCQTAP13;
                     ,SCQTAP14=this.w_SCQTAP14;
                     ,SCQTAODL=this.w_SCQTAODL;
                     ,SCRISODL=this.w_SCRISODL;
                     ,SCQTMODL=this.w_SCQTMODL;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_SCCODODL))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.SCALETTA_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.SCALETTA_IDX,2])
        *
        * delete SCALETTA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.SCALETTM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
        *
        * delete SCALETTM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_SCCODODL))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SCALETTM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCALETTM_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.SUGG.Calculate(AH_MsgFormat("Suggeriti"), .w_TESSUG, .w_BASSUG)
        .oPgFrm.Page1.oPag.CONFE.Calculate(AH_MsgFormat("Pianificati"), .w_TESCON,  .w_BASCON)
        .oPgFrm.Page1.oPag.LAN.Calculate(AH_MsgFormat("Lanciati"), .w_TESLAN, .w_BASLAN)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(1,22,.t.)
        if .o_SCTABRIS<>.w_SCTABRIS
          .w_SCCALRIS = IIF(EMPTY(.w_CALRIS), .w_CALSTA, .w_CALRIS)
          .link_1_36('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        if .o_CPROWORD<>.w_CPROWORD
          .Calculate_HPXREJAWPI()
        endif
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.CPPER.Calculate(iif(.w_SCTIPSCA='CSQ', ah_MsgFormat("Cap. periodo quantit�"), ah_Msgformat("Cap. periodo ore")), RGB(0,0,0))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .DoRTCalc(24,35,.t.)
        if .o_SCCODICE<>.w_SCCODICE.or. .o_SCCODODL<>.w_SCCODODL
          .w_SCQTARIC = .w_SCQTAODL
        endif
        .DoRTCalc(37,51,.t.)
          .link_2_20('Full')
        if .o_SCCODODL<>.w_SCCODODL.or. .o_SCCODICE<>.w_SCCODICE.or. .o_SCCODCIC<>.w_SCCODCIC
          .w_SCCODCIC = .w_SCCODCIC
          .link_2_21('Full')
        endif
        Local l_Dep1,l_Dep2,l_Dep3
        l_Dep1= .o_SCQTARIC<>.w_SCQTARIC .or. .o_SCQTAP01<>.w_SCQTAP01 .or. .o_SCQTAP02<>.w_SCQTAP02 .or. .o_SCQTAP03<>.w_SCQTAP03 .or. .o_SCQTAP04<>.w_SCQTAP04        l_Dep2= .o_SCQTAP05<>.w_SCQTAP05 .or. .o_SCQTAP06<>.w_SCQTAP06 .or. .o_SCQTAP07<>.w_SCQTAP07 .or. .o_SCQTAP08<>.w_SCQTAP08 .or. .o_SCQTAP09<>.w_SCQTAP09        l_Dep3= .o_SCQTAP10<>.w_SCQTAP10 .or. .o_SCQTAP11<>.w_SCQTAP11 .or. .o_SCQTAP12<>.w_SCQTAP12 .or. .o_SCQTAP13<>.w_SCQTAP13 .or. .o_SCQTAP14<>.w_SCQTAP14
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3
          .w_TOQTARES = .w_TOQTARES-.w_qtares
          .w_QTARES = .w_SCQTARIC-(nvl(.w_SCQTAP01,0)+nvl(.w_SCQTAP02,0)+nvl(.w_SCQTAP03,0)+nvl(.w_SCQTAP04,0)+nvl(.w_SCQTAP05,0)+nvl(.w_SCQTAP06,0)+nvl(.w_SCQTAP07,0)+nvl(.w_SCQTAP08,0)+nvl(.w_SCQTAP09,0)+nvl(.w_SCQTAP10,0)+nvl(.w_SCQTAP11,0)+nvl(.w_SCQTAP12,0)+nvl(.w_SCQTAP13,0)+nvl(.w_SCQTAP14,0))
          .w_TOQTARES = .w_TOQTARES+.w_qtares
        endif
        if .o_SCCODODL<>.w_SCCODODL.or. .o_SCCODICE<>.w_SCCODICE
          .Calculate_MRTCNZEDNC()
        endif
        .DoRTCalc(55,56,.t.)
        if .o_CPROWORD<>.w_CPROWORD
          .w_SCSEQSCA = .w_CPROWORD
        endif
        .DoRTCalc(58,58,.t.)
          .link_2_28('Full')
        .DoRTCalc(60,66,.t.)
          .w_OLTQTORE = .w_OLTQTODL-.w_OLTQTOEV
        .DoRTCalc(68,68,.t.)
          .w_INPUTODL = .t.
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_23.Calculate()
        * --- Area Manuale = Calculate
        * --- gsco_msp
        This.NotifyEvent("DatiPeriodo")
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SCLTT","i_codazi,w_SCSERIAL")
          .op_SCSERIAL = .w_SCSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(70,104,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OLTCOART with this.w_OLTCOART
      replace t_OLTSTATO with this.w_OLTSTATO
      replace t_OLTQTOD1 with this.w_OLTQTOD1
      replace t_SCSEQSCA with this.w_SCSEQSCA
      replace t_SCQTAODL with this.w_SCQTAODL
      replace t_SCRISODL with this.w_SCRISODL
      replace t_INPUTODL with this.w_INPUTODL
      replace t_SCQTMODL with this.w_SCQTMODL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.SUGG.Calculate(AH_MsgFormat("Suggeriti"), .w_TESSUG, .w_BASSUG)
        .oPgFrm.Page1.oPag.CONFE.Calculate(AH_MsgFormat("Pianificati"), .w_TESCON,  .w_BASCON)
        .oPgFrm.Page1.oPag.LAN.Calculate(AH_MsgFormat("Lanciati"), .w_TESLAN, .w_BASLAN)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.CPPER.Calculate(iif(.w_SCTIPSCA='CSQ', ah_MsgFormat("Cap. periodo quantit�"), ah_Msgformat("Cap. periodo ore")), RGB(0,0,0))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_23.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Calculate()
    endwith
  return
  proc Calculate_HVRWLSSSBQ()
    with this
          * --- Update dettaglio
          .bUpdated = True
    endwith
  endproc
  proc Calculate_HPXREJAWPI()
    with this
          * --- Modifica ordinamento
          .w_VARIATA = iif( !empty(nvl(.w_SCCODODL," ")) , iif(.w_CPROWORD<> .o_CPROWORD, .t., .w_VARIATA), .w_VARIATA)
          .w_NRVARIATA = IIF(.w_VARIATA, CPROWNUM, 0)
    endwith
  endproc
  proc Calculate_MRTCNZEDNC()
    with this
          * --- Modifica dell'ODL
          .w_SCQTARIC = 0
          GSCO_BSP(this;
              ,'CINPUTODL';
             )
    endwith
  endproc
  proc Calculate_DMLCMGQUVP()
    with this
          * --- Valorizzazione data fine piano
          .w_SCPIAFIN = IIF(EMPTY(.w_SCPIAFIN), .w_SCDATFIN, .w_SCPIAFIN)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCTABRIS_1_3.enabled = this.oPgFrm.Page1.oPag.oSCTABRIS_1_3.mCond()
    this.oPgFrm.Page1.oPag.oSCDATINI_1_4.enabled = this.oPgFrm.Page1.oPag.oSCDATINI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSCCALRIS_1_36.enabled = this.oPgFrm.Page1.oPag.oSCCALRIS_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSCODLSUG_1_42.enabled = this.oPgFrm.Page1.oPag.oSCODLSUG_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSCODLPIA_1_43.enabled = this.oPgFrm.Page1.oPag.oSCODLPIA_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSCODLLAN_1_44.enabled = this.oPgFrm.Page1.oPag.oSCODLLAN_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_3_3.enabled = this.oPgFrm.Page1.oPag.oBtn_3_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_3_18.enabled = this.oPgFrm.Page1.oPag.oBtn_3_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_3_19.enabled = this.oPgFrm.Page1.oPag.oBtn_3_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_3_20.enabled = this.oPgFrm.Page1.oPag.oBtn_3_20.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCCODODL_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCCODODL_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCQTARIC_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCQTARIC_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSCQTCPDF_1_5.visible=!this.oPgFrm.Page1.oPag.oSCQTCPDF_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oSCCALRIS_1_36.visible=!this.oPgFrm.Page1.oPag.oSCCALRIS_1_36.mHide()
    this.oPgFrm.Page1.oPag.oSCDESCAL_1_37.visible=!this.oPgFrm.Page1.oPag.oSCDESCAL_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsco_msp
                if this.w_VARIATA
                  if lower(right(alltrim(cEvent),8))='gotfocus'
                    v = thisform.LockScreen
                    thisform.LockScreen = .t.
                    do GSCO_BSP with this, 'Riordina'
                    thisform.LockScreen = .f.
                    thisform.LockScreen = v
                  endif
                endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.SUGG.Event(cEvent)
      .oPgFrm.Page1.oPag.CONFE.Event(cEvent)
      .oPgFrm.Page1.oPag.LAN.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
        if lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("New record")
          .Calculate_HVRWLSSSBQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("LostFocus")
          .Calculate_HPXREJAWPI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.CPPER.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
        if lower(cEvent)==lower("Changed")
          .Calculate_MRTCNZEDNC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_23.Event(cEvent)
        if lower(cEvent)==lower("w_SCTABRIS Changed") or lower(cEvent)==lower("w_SCDATINI Changed") or lower(cEvent)==lower("w_SCTIPPIA Changed")
          .Calculate_DMLCMGQUVP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCTABRIS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_lTable = "TAB_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2], .t., this.TAB_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCTABRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_SCTABRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_SCTABRIS))
          select RICODICE,RIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCTABRIS)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RIDESCRI like "+cp_ToStrODBC(trim(this.w_SCTABRIS)+"%");

            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RIDESCRI like "+cp_ToStr(trim(this.w_SCTABRIS)+"%");

            select RICODICE,RIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCTABRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_RISO','*','RICODICE',cp_AbsName(oSource.parent,'oSCTABRIS_1_3'),i_cWhere,'',"Risorse",'GSCO_ZLR.TAB_RISO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCTABRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_SCTABRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_SCTABRIS)
            select RICODICE,RIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCTABRIS = NVL(_Link_.RICODICE,space(20))
      this.w_RLDESCRI = NVL(_Link_.RIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCTABRIS = space(20)
      endif
      this.w_RLDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCTABRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_RISO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.RICODICE as RICODICE103"+ ",link_1_3.RIDESCRI as RIDESCRI103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on SCALETTM.SCTABRIS=link_1_3.RICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and SCALETTM.SCTABRIS=link_1_3.RICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCALRIS
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCALRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_SCCALRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_SCCALRIS))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCALRIS)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCALRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oSCCALRIS_1_36'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCALRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_SCCALRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_SCCALRIS)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCALRIS = NVL(_Link_.TCCODICE,space(5))
      this.w_SCDESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCALRIS = space(5)
      endif
      this.w_SCDESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_SCTIPSCA='CSO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SCCALRIS = space(5)
        this.w_SCDESCAL = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCALRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLTCODIC like "+cp_ToStrODBC(trim(this.w_SCCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLTCODIC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLTCODIC',trim(this.w_SCCODICE))
          select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLTCODIC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODICE)==trim(_Link_.OLTCODIC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_SCCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStr(trim(this.w_SCCODICE)+"%");

            select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODICE) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLTCODIC',cp_AbsName(oSource.parent,'oSCCODICE_2_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV";
                     +" from "+i_cTable+" "+i_lTable+" where OLTCODIC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLTCODIC',oSource.xKey(1))
            select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV";
                   +" from "+i_cTable+" "+i_lTable+" where OLTCODIC="+cp_ToStrODBC(this.w_SCCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLTCODIC',this.w_SCCODICE)
            select OLTCODIC,OLCODODL,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI,OLTQTOEV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODICE = NVL(_Link_.OLTCODIC,space(20))
      this.w_SCCODODL = NVL(_Link_.OLCODODL,space(15))
      this.w_SCDINRIC = NVL(cp_ToDate(_Link_.OLTDINRIC),ctod("  /  /  "))
      this.w_OLTUNMIS = NVL(_Link_.OLTUNMIS,space(3))
      this.w_OLTCOART = NVL(_Link_.OLTCOART,space(20))
      this.w_SCCODCIC = NVL(_Link_.OLTCICLO,space(15))
      this.w_OLTSTATO = NVL(_Link_.OLTSTATO,space(1))
      this.w_OLTQTOD1 = NVL(_Link_.OLTQTOD1,0)
      this.w_OLTQTODL = NVL(_Link_.OLTQTODL,0)
      this.w_OLTDTINI = NVL(cp_ToDate(_Link_.OLTDTINI),ctod("  /  /  "))
      this.w_OLTQTOEV = NVL(_Link_.OLTQTOEV,0)
    else
      if i_cCtrl<>'Load'
        this.w_SCCODICE = space(20)
      endif
      this.w_SCCODODL = space(15)
      this.w_SCDINRIC = ctod("  /  /  ")
      this.w_OLTUNMIS = space(3)
      this.w_OLTCOART = space(20)
      this.w_SCCODCIC = space(15)
      this.w_OLTSTATO = space(1)
      this.w_OLTQTOD1 = 0
      this.w_OLTQTODL = 0
      this.w_OLTDTINI = ctod("  /  /  ")
      this.w_OLTQTOEV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=not empty(nvl(.w_SCCODCIC,' ')) and (.w_OLTSTATO = .w_SCODLSUG or .w_OLTSTATO = .w_SCODLPIA or .w_OLTSTATO = .w_SCODLLAN or .w_OLTSTATO = 'F') or (.w_SCDINRIC >= .w_SCDATINI or .w_SCDINRIC <= .w_SCDATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SCCODICE = space(20)
        this.w_SCCODODL = space(15)
        this.w_SCDINRIC = ctod("  /  /  ")
        this.w_OLTUNMIS = space(3)
        this.w_OLTCOART = space(20)
        this.w_SCCODCIC = space(15)
        this.w_OLTSTATO = space(1)
        this.w_OLTQTOD1 = 0
        this.w_OLTQTODL = 0
        this.w_OLTDTINI = ctod("  /  /  ")
        this.w_OLTQTOEV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLTCODIC,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODODL
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_SCCODODL)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_SCCODODL))
          select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODODL)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" OLTCODIC like "+cp_ToStrODBC(trim(this.w_SCCODODL)+"%");

            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OLTCODIC like "+cp_ToStr(trim(this.w_SCCODODL)+"%");

            select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODODL) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oSCCODODL_2_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_SCCODODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_SCCODODL)
            select OLCODODL,OLTCODIC,OLTDINRIC,OLTUNMIS,OLTCOART,OLTCICLO,OLTSTATO,OLTQTOD1,OLTQTODL,OLTDTINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODODL = NVL(_Link_.OLCODODL,space(15))
      this.w_SCCODICE = NVL(_Link_.OLTCODIC,space(20))
      this.w_SCDINRIC = NVL(cp_ToDate(_Link_.OLTDINRIC),ctod("  /  /  "))
      this.w_OLTUNMIS = NVL(_Link_.OLTUNMIS,space(3))
      this.w_OLTCOART = NVL(_Link_.OLTCOART,space(20))
      this.w_SCCODCIC = NVL(_Link_.OLTCICLO,space(15))
      this.w_OLTSTATO = NVL(_Link_.OLTSTATO,space(1))
      this.w_OLTQTOD1 = NVL(_Link_.OLTQTOD1,0)
      this.w_OLTQTODL = NVL(_Link_.OLTQTODL,0)
      this.w_OLTDTINI = NVL(cp_ToDate(_Link_.OLTDTINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODODL = space(15)
      endif
      this.w_SCCODICE = space(20)
      this.w_SCDINRIC = ctod("  /  /  ")
      this.w_OLTUNMIS = space(3)
      this.w_OLTCOART = space(20)
      this.w_SCCODCIC = space(15)
      this.w_OLTSTATO = space(1)
      this.w_OLTQTOD1 = 0
      this.w_OLTQTODL = 0
      this.w_OLTDTINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=not empty(nvl(.w_SCCODCIC,' ')) and (.w_OLTSTATO = .w_SCODLSUG or .w_OLTSTATO = .w_SCODLPIA or .w_OLTSTATO = .w_SCODLLAN or .w_OLTSTATO = 'F') or (.w_SCDINRIC >= .w_SCDATINI or .w_SCDINRIC <= .w_SCDATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SCCODODL = space(15)
        this.w_SCCODICE = space(20)
        this.w_SCDINRIC = ctod("  /  /  ")
        this.w_OLTUNMIS = space(3)
        this.w_OLTCOART = space(20)
        this.w_SCCODCIC = space(15)
        this.w_OLTSTATO = space(1)
        this.w_OLTQTOD1 = 0
        this.w_OLTQTODL = 0
        this.w_OLTDTINI = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ODL_MAST_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.OLCODODL as OLCODODL203"+ ",link_2_3.OLTCODIC as OLTCODIC203"+ ",link_2_3.OLTDINRIC as OLTDINRIC203"+ ",link_2_3.OLTUNMIS as OLTUNMIS203"+ ",link_2_3.OLTCOART as OLTCOART203"+ ",link_2_3.OLTCICLO as OLTCICLO203"+ ",link_2_3.OLTSTATO as OLTSTATO203"+ ",link_2_3.OLTQTOD1 as OLTQTOD1203"+ ",link_2_3.OLTQTODL as OLTQTODL203"+ ",link_2_3.OLTDTINI as OLTDTINI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on SCALETTA.SCCODODL=link_2_3.OLCODODL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and SCALETTA.SCCODODL=link_2_3.OLCODODL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCOART
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_OLTCOART)
            select ARCODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOART = NVL(_Link_.ARCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOART = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODCIC
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_lTable = "TABMCICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2], .t., this.TABMCICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_SCCODCIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_SCCODCIC)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCIC = NVL(_Link_.CSCODICE,space(15))
      this.w_DESCIC = NVL(_Link_.CSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCIC = space(15)
      endif
      this.w_DESCIC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.TABMCICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODICE
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_SCCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_SCCODICE)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_SCDESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODICE = space(20)
      endif
      this.w_SCDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSCSERIAL_1_2.value==this.w_SCSERIAL)
      this.oPgFrm.Page1.oPag.oSCSERIAL_1_2.value=this.w_SCSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCTABRIS_1_3.value==this.w_SCTABRIS)
      this.oPgFrm.Page1.oPag.oSCTABRIS_1_3.value=this.w_SCTABRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATINI_1_4.value==this.w_SCDATINI)
      this.oPgFrm.Page1.oPag.oSCDATINI_1_4.value=this.w_SCDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTCPDF_1_5.value==this.w_SCQTCPDF)
      this.oPgFrm.Page1.oPag.oSCQTCPDF_1_5.value=this.w_SCQTCPDF
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC01_1_6.value==this.w_SCQTAC01)
      this.oPgFrm.Page1.oPag.oSCQTAC01_1_6.value=this.w_SCQTAC01
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC02_1_7.value==this.w_SCQTAC02)
      this.oPgFrm.Page1.oPag.oSCQTAC02_1_7.value=this.w_SCQTAC02
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC03_1_8.value==this.w_SCQTAC03)
      this.oPgFrm.Page1.oPag.oSCQTAC03_1_8.value=this.w_SCQTAC03
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC04_1_9.value==this.w_SCQTAC04)
      this.oPgFrm.Page1.oPag.oSCQTAC04_1_9.value=this.w_SCQTAC04
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC05_1_10.value==this.w_SCQTAC05)
      this.oPgFrm.Page1.oPag.oSCQTAC05_1_10.value=this.w_SCQTAC05
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC06_1_12.value==this.w_SCQTAC06)
      this.oPgFrm.Page1.oPag.oSCQTAC06_1_12.value=this.w_SCQTAC06
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC07_1_13.value==this.w_SCQTAC07)
      this.oPgFrm.Page1.oPag.oSCQTAC07_1_13.value=this.w_SCQTAC07
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC08_1_14.value==this.w_SCQTAC08)
      this.oPgFrm.Page1.oPag.oSCQTAC08_1_14.value=this.w_SCQTAC08
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC09_1_15.value==this.w_SCQTAC09)
      this.oPgFrm.Page1.oPag.oSCQTAC09_1_15.value=this.w_SCQTAC09
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC10_1_16.value==this.w_SCQTAC10)
      this.oPgFrm.Page1.oPag.oSCQTAC10_1_16.value=this.w_SCQTAC10
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC11_1_17.value==this.w_SCQTAC11)
      this.oPgFrm.Page1.oPag.oSCQTAC11_1_17.value=this.w_SCQTAC11
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC12_1_18.value==this.w_SCQTAC12)
      this.oPgFrm.Page1.oPag.oSCQTAC12_1_18.value=this.w_SCQTAC12
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC13_1_19.value==this.w_SCQTAC13)
      this.oPgFrm.Page1.oPag.oSCQTAC13_1_19.value=this.w_SCQTAC13
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAC14_1_20.value==this.w_SCQTAC14)
      this.oPgFrm.Page1.oPag.oSCQTAC14_1_20.value=this.w_SCQTAC14
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_22.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_22.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCALRIS_1_36.value==this.w_SCCALRIS)
      this.oPgFrm.Page1.oPag.oSCCALRIS_1_36.value=this.w_SCCALRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESCAL_1_37.value==this.w_SCDESCAL)
      this.oPgFrm.Page1.oPag.oSCDESCAL_1_37.value=this.w_SCDESCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCTIPPIA_1_40.RadioValue()==this.w_SCTIPPIA)
      this.oPgFrm.Page1.oPag.oSCTIPPIA_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCODLSUG_1_42.RadioValue()==this.w_SCODLSUG)
      this.oPgFrm.Page1.oPag.oSCODLSUG_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCODLPIA_1_43.RadioValue()==this.w_SCODLPIA)
      this.oPgFrm.Page1.oPag.oSCODLPIA_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCODLLAN_1_44.RadioValue()==this.w_SCODLLAN)
      this.oPgFrm.Page1.oPag.oSCODLLAN_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATFIN_1_48.value==this.w_SCDATFIN)
      this.oPgFrm.Page1.oPag.oSCDATFIN_1_48.value=this.w_SCDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTUNMIS_2_19.value==this.w_OLTUNMIS)
      this.oPgFrm.Page1.oPag.oOLTUNMIS_2_19.value=this.w_OLTUNMIS
      replace t_OLTUNMIS with this.oPgFrm.Page1.oPag.oOLTUNMIS_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODCIC_2_21.value==this.w_SCCODCIC)
      this.oPgFrm.Page1.oPag.oSCCODCIC_2_21.value=this.w_SCCODCIC
      replace t_SCCODCIC with this.oPgFrm.Page1.oPag.oSCCODCIC_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDINRIC_2_27.value==this.w_SCDINRIC)
      this.oPgFrm.Page1.oPag.oSCDINRIC_2_27.value=this.w_SCDINRIC
      replace t_SCDINRIC with this.oPgFrm.Page1.oPag.oSCDINRIC_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODICE_2_28.value==this.w_SCCODICE)
      this.oPgFrm.Page1.oPag.oSCCODICE_2_28.value=this.w_SCCODICE
      replace t_SCCODICE with this.oPgFrm.Page1.oPag.oSCCODICE_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODODL_2_29.value==this.w_SCCODODL)
      this.oPgFrm.Page1.oPag.oSCCODODL_2_29.value=this.w_SCCODODL
      replace t_SCCODODL with this.oPgFrm.Page1.oPag.oSCCODODL_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCIC_2_30.value==this.w_DESCIC)
      this.oPgFrm.Page1.oPag.oDESCIC_2_30.value=this.w_DESCIC
      replace t_DESCIC with this.oPgFrm.Page1.oPag.oDESCIC_2_30.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTINI_2_31.value==this.w_OLTDTINI)
      this.oPgFrm.Page1.oPag.oOLTDTINI_2_31.value=this.w_OLTDTINI
      replace t_OLTDTINI with this.oPgFrm.Page1.oPag.oOLTDTINI_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTODL_2_32.value==this.w_OLTQTODL)
      this.oPgFrm.Page1.oPag.oOLTQTODL_2_32.value=this.w_OLTQTODL
      replace t_OLTQTODL with this.oPgFrm.Page1.oPag.oOLTQTODL_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESART_2_34.value==this.w_SCDESART)
      this.oPgFrm.Page1.oPag.oSCDESART_2_34.value=this.w_SCDESART
      replace t_SCDESART with this.oPgFrm.Page1.oPag.oSCDESART_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTOEV_2_35.value==this.w_OLTQTOEV)
      this.oPgFrm.Page1.oPag.oOLTQTOEV_2_35.value=this.w_OLTQTOEV
      replace t_OLTQTOEV with this.oPgFrm.Page1.oPag.oOLTQTOEV_2_35.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTORE_2_36.value==this.w_OLTQTORE)
      this.oPgFrm.Page1.oPag.oOLTQTORE_2_36.value=this.w_OLTQTORE
      replace t_OLTQTORE with this.oPgFrm.Page1.oPag.oOLTQTORE_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC01_3_1.value==this.w_TOQTAC01)
      this.oPgFrm.Page1.oPag.oTOQTAC01_3_1.value=this.w_TOQTAC01
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC02_3_2.value==this.w_TOQTAC02)
      this.oPgFrm.Page1.oPag.oTOQTAC02_3_2.value=this.w_TOQTAC02
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC03_3_5.value==this.w_TOQTAC03)
      this.oPgFrm.Page1.oPag.oTOQTAC03_3_5.value=this.w_TOQTAC03
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC04_3_6.value==this.w_TOQTAC04)
      this.oPgFrm.Page1.oPag.oTOQTAC04_3_6.value=this.w_TOQTAC04
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC05_3_7.value==this.w_TOQTAC05)
      this.oPgFrm.Page1.oPag.oTOQTAC05_3_7.value=this.w_TOQTAC05
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC06_3_8.value==this.w_TOQTAC06)
      this.oPgFrm.Page1.oPag.oTOQTAC06_3_8.value=this.w_TOQTAC06
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC07_3_9.value==this.w_TOQTAC07)
      this.oPgFrm.Page1.oPag.oTOQTAC07_3_9.value=this.w_TOQTAC07
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC08_3_10.value==this.w_TOQTAC08)
      this.oPgFrm.Page1.oPag.oTOQTAC08_3_10.value=this.w_TOQTAC08
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC09_3_11.value==this.w_TOQTAC09)
      this.oPgFrm.Page1.oPag.oTOQTAC09_3_11.value=this.w_TOQTAC09
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC10_3_12.value==this.w_TOQTAC10)
      this.oPgFrm.Page1.oPag.oTOQTAC10_3_12.value=this.w_TOQTAC10
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC11_3_13.value==this.w_TOQTAC11)
      this.oPgFrm.Page1.oPag.oTOQTAC11_3_13.value=this.w_TOQTAC11
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC12_3_14.value==this.w_TOQTAC12)
      this.oPgFrm.Page1.oPag.oTOQTAC12_3_14.value=this.w_TOQTAC12
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC13_3_15.value==this.w_TOQTAC13)
      this.oPgFrm.Page1.oPag.oTOQTAC13_3_15.value=this.w_TOQTAC13
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTAC14_3_16.value==this.w_TOQTAC14)
      this.oPgFrm.Page1.oPag.oTOQTAC14_3_16.value=this.w_TOQTAC14
    endif
    if not(this.oPgFrm.Page1.oPag.oTOQTARES_3_17.value==this.w_TOQTARES)
      this.oPgFrm.Page1.oPag.oTOQTARES_3_17.value=this.w_TOQTARES
    endif
    if not(this.oPgFrm.Page1.oPag.oSCPIAINI_1_63.value==this.w_SCPIAINI)
      this.oPgFrm.Page1.oPag.oSCPIAINI_1_63.value=this.w_SCPIAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCPIAFIN_1_64.value==this.w_SCPIAFIN)
      this.oPgFrm.Page1.oPag.oSCPIAFIN_1_64.value=this.w_SCPIAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2.value==this.w_SCCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2.value=this.w_SCCODICE
      replace t_SCCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3.value==this.w_SCCODODL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3.value=this.w_SCCODODL
      replace t_SCCODODL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTARIC_2_4.value==this.w_SCQTARIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTARIC_2_4.value=this.w_SCQTARIC
      replace t_SCQTARIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTARIC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP01_2_5.value==this.w_SCQTAP01)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP01_2_5.value=this.w_SCQTAP01
      replace t_SCQTAP01 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP01_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP02_2_6.value==this.w_SCQTAP02)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP02_2_6.value=this.w_SCQTAP02
      replace t_SCQTAP02 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP02_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP03_2_7.value==this.w_SCQTAP03)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP03_2_7.value=this.w_SCQTAP03
      replace t_SCQTAP03 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP03_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP04_2_8.value==this.w_SCQTAP04)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP04_2_8.value=this.w_SCQTAP04
      replace t_SCQTAP04 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP04_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP05_2_9.value==this.w_SCQTAP05)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP05_2_9.value=this.w_SCQTAP05
      replace t_SCQTAP05 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP05_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP06_2_10.value==this.w_SCQTAP06)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP06_2_10.value=this.w_SCQTAP06
      replace t_SCQTAP06 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP06_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP07_2_11.value==this.w_SCQTAP07)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP07_2_11.value=this.w_SCQTAP07
      replace t_SCQTAP07 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP07_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP08_2_12.value==this.w_SCQTAP08)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP08_2_12.value=this.w_SCQTAP08
      replace t_SCQTAP08 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP08_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP09_2_13.value==this.w_SCQTAP09)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP09_2_13.value=this.w_SCQTAP09
      replace t_SCQTAP09 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP09_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP10_2_14.value==this.w_SCQTAP10)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP10_2_14.value=this.w_SCQTAP10
      replace t_SCQTAP10 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP10_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP11_2_15.value==this.w_SCQTAP11)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP11_2_15.value=this.w_SCQTAP11
      replace t_SCQTAP11 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP11_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP12_2_16.value==this.w_SCQTAP12)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP12_2_16.value=this.w_SCQTAP12
      replace t_SCQTAP12 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP12_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP13_2_17.value==this.w_SCQTAP13)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP13_2_17.value=this.w_SCQTAP13
      replace t_SCQTAP13 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP13_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP14_2_18.value==this.w_SCQTAP14)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP14_2_18.value=this.w_SCQTAP14
      replace t_SCQTAP14 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTAP14_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTARES_2_22.value==this.w_QTARES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTARES_2_22.value=this.w_QTARES
      replace t_QTARES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTARES_2_22.value
    endif
    cp_SetControlsValueExtFlds(this,'SCALETTM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SCSERIAL))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCSERIAL_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SCSERIAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SCTABRIS))  and (empty(.w_SCTABRIS) )
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCTABRIS_1_3.SetFocus()
            i_bnoObbl = !empty(.w_SCTABRIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
          case   (empty(.w_SCDATINI) or not(not empty(.w_SCTIPPIA)))  and (not empty(.w_SCTIPPIA))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCDATINI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SCDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il periodo deve iniziare di luned�.")
          case   not(.w_SCQTCPDF >=0)  and not(.w_SCTIPSCA<>'CSQ')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCQTCPDF_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SCCALRIS) or not(.w_SCTIPSCA='CSO'))  and not(.w_SCTIPSCA<>'CSO')  and (.cfunction <> 'Query' and not empty(.w_SCTABRIS) AND not empty(.w_SCDATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCCALRIS_1_36.SetFocus()
            i_bnoObbl = !empty(.w_SCCALRIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SCTIPPIA))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCTIPPIA_1_40.SetFocus()
            i_bnoObbl = !empty(.w_SCTIPPIA)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_SCCODODL)));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(not empty(nvl(.w_SCCODCIC,' ')) and (.w_OLTSTATO = .w_SCODLSUG or .w_OLTSTATO = .w_SCODLPIA or .w_OLTSTATO = .w_SCODLLAN or .w_OLTSTATO = 'F') or (.w_SCDINRIC >= .w_SCDATINI or .w_SCDINRIC <= .w_SCDATFIN)) and (!empty(.w_SCTABRIS)) and not(empty(.w_SCCODICE)) and (not(Empty(.w_SCCODODL)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(not empty(nvl(.w_SCCODCIC,' ')) and (.w_OLTSTATO = .w_SCODLSUG or .w_OLTSTATO = .w_SCODLPIA or .w_OLTSTATO = .w_SCODLLAN or .w_OLTSTATO = 'F') or (.w_SCDINRIC >= .w_SCDATINI or .w_SCDINRIC <= .w_SCDATFIN)) and (!empty(.w_SCTABRIS)) and not(empty(.w_SCCODODL)) and (not(Empty(.w_SCCODODL)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODODL_2_3
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_SCQTARIC > 0) and (NOT EMPTY(.w_SCCODODL)) and (not(Empty(.w_SCCODODL)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCQTARIC_2_4
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_SCCODODL))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCTABRIS = this.w_SCTABRIS
    this.o_CPROWORD = this.w_CPROWORD
    this.o_SCCODICE = this.w_SCCODICE
    this.o_SCCODODL = this.w_SCCODODL
    this.o_SCQTARIC = this.w_SCQTARIC
    this.o_SCQTAP01 = this.w_SCQTAP01
    this.o_SCQTAP02 = this.w_SCQTAP02
    this.o_SCQTAP03 = this.w_SCQTAP03
    this.o_SCQTAP04 = this.w_SCQTAP04
    this.o_SCQTAP05 = this.w_SCQTAP05
    this.o_SCQTAP06 = this.w_SCQTAP06
    this.o_SCQTAP07 = this.w_SCQTAP07
    this.o_SCQTAP08 = this.w_SCQTAP08
    this.o_SCQTAP09 = this.w_SCQTAP09
    this.o_SCQTAP10 = this.w_SCQTAP10
    this.o_SCQTAP11 = this.w_SCQTAP11
    this.o_SCQTAP12 = this.w_SCQTAP12
    this.o_SCQTAP13 = this.w_SCQTAP13
    this.o_SCQTAP14 = this.w_SCQTAP14
    this.o_SCCODCIC = this.w_SCCODCIC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_SCCODODL)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_SCCODICE=space(20)
      .w_SCCODODL=space(15)
      .w_SCQTARIC=0
      .w_SCQTAP01=0
      .w_SCQTAP02=0
      .w_SCQTAP03=0
      .w_SCQTAP04=0
      .w_SCQTAP05=0
      .w_SCQTAP06=0
      .w_SCQTAP07=0
      .w_SCQTAP08=0
      .w_SCQTAP09=0
      .w_SCQTAP10=0
      .w_SCQTAP11=0
      .w_SCQTAP12=0
      .w_SCQTAP13=0
      .w_SCQTAP14=0
      .w_OLTUNMIS=space(3)
      .w_OLTCOART=space(20)
      .w_SCCODCIC=space(15)
      .w_QTARES=0
      .w_OLTSTATO=space(1)
      .w_OLTQTOD1=0
      .w_SCSEQSCA=0
      .w_SCDINRIC=ctod("  /  /  ")
      .w_SCCODICE=space(20)
      .w_SCCODODL=space(15)
      .w_DESCIC=space(40)
      .w_OLTDTINI=ctod("  /  /  ")
      .w_OLTQTODL=0
      .w_SCQTAODL=0
      .w_SCDESART=space(40)
      .w_OLTQTOEV=0
      .w_OLTQTORE=0
      .w_SCRISODL=space(15)
      .w_INPUTODL=.f.
      .w_SCQTMODL=0
      .DoRTCalc(1,34,.f.)
      if not(empty(.w_SCCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(35,35,.f.)
      if not(empty(.w_SCCODODL))
        .link_2_3('Full')
      endif
        .w_SCQTARIC = .w_SCQTAODL
      .DoRTCalc(37,52,.f.)
      if not(empty(.w_OLTCOART))
        .link_2_20('Full')
      endif
        .w_SCCODCIC = .w_SCCODCIC
      .DoRTCalc(53,53,.f.)
      if not(empty(.w_SCCODCIC))
        .link_2_21('Full')
      endif
        .w_QTARES = .w_SCQTARIC-(nvl(.w_SCQTAP01,0)+nvl(.w_SCQTAP02,0)+nvl(.w_SCQTAP03,0)+nvl(.w_SCQTAP04,0)+nvl(.w_SCQTAP05,0)+nvl(.w_SCQTAP06,0)+nvl(.w_SCQTAP07,0)+nvl(.w_SCQTAP08,0)+nvl(.w_SCQTAP09,0)+nvl(.w_SCQTAP10,0)+nvl(.w_SCQTAP11,0)+nvl(.w_SCQTAP12,0)+nvl(.w_SCQTAP13,0)+nvl(.w_SCQTAP14,0))
      .DoRTCalc(55,55,.f.)
        .w_OLTQTOD1 = 0
        .w_SCSEQSCA = .w_CPROWORD
      .DoRTCalc(58,59,.f.)
      if not(empty(.w_SCCODICE))
        .link_2_28('Full')
      endif
      .DoRTCalc(60,63,.f.)
        .w_SCQTAODL = 0
      .DoRTCalc(65,66,.f.)
        .w_OLTQTORE = .w_OLTQTODL-.w_OLTQTOEV
      .DoRTCalc(68,68,.f.)
        .w_INPUTODL = .t.
        .w_SCQTMODL = 0
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_40.Calculate()
    endwith
    this.DoRTCalc(71,104,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SCCODICE = t_SCCODICE
    this.w_SCCODODL = t_SCCODODL
    this.w_SCQTARIC = t_SCQTARIC
    this.w_SCQTAP01 = t_SCQTAP01
    this.w_SCQTAP02 = t_SCQTAP02
    this.w_SCQTAP03 = t_SCQTAP03
    this.w_SCQTAP04 = t_SCQTAP04
    this.w_SCQTAP05 = t_SCQTAP05
    this.w_SCQTAP06 = t_SCQTAP06
    this.w_SCQTAP07 = t_SCQTAP07
    this.w_SCQTAP08 = t_SCQTAP08
    this.w_SCQTAP09 = t_SCQTAP09
    this.w_SCQTAP10 = t_SCQTAP10
    this.w_SCQTAP11 = t_SCQTAP11
    this.w_SCQTAP12 = t_SCQTAP12
    this.w_SCQTAP13 = t_SCQTAP13
    this.w_SCQTAP14 = t_SCQTAP14
    this.w_OLTUNMIS = t_OLTUNMIS
    this.w_OLTCOART = t_OLTCOART
    this.w_SCCODCIC = t_SCCODCIC
    this.w_QTARES = t_QTARES
    this.w_OLTSTATO = t_OLTSTATO
    this.w_OLTQTOD1 = t_OLTQTOD1
    this.w_SCSEQSCA = t_SCSEQSCA
    this.w_SCDINRIC = t_SCDINRIC
    this.w_SCCODICE = t_SCCODICE
    this.w_SCCODODL = t_SCCODODL
    this.w_DESCIC = t_DESCIC
    this.w_OLTDTINI = t_OLTDTINI
    this.w_OLTQTODL = t_OLTQTODL
    this.w_SCQTAODL = t_SCQTAODL
    this.w_SCDESART = t_SCDESART
    this.w_OLTQTOEV = t_OLTQTOEV
    this.w_OLTQTORE = t_OLTQTORE
    this.w_SCRISODL = t_SCRISODL
    this.w_INPUTODL = t_INPUTODL
    this.w_SCQTMODL = t_SCQTMODL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SCCODICE with this.w_SCCODICE
    replace t_SCCODODL with this.w_SCCODODL
    replace t_SCQTARIC with this.w_SCQTARIC
    replace t_SCQTAP01 with this.w_SCQTAP01
    replace t_SCQTAP02 with this.w_SCQTAP02
    replace t_SCQTAP03 with this.w_SCQTAP03
    replace t_SCQTAP04 with this.w_SCQTAP04
    replace t_SCQTAP05 with this.w_SCQTAP05
    replace t_SCQTAP06 with this.w_SCQTAP06
    replace t_SCQTAP07 with this.w_SCQTAP07
    replace t_SCQTAP08 with this.w_SCQTAP08
    replace t_SCQTAP09 with this.w_SCQTAP09
    replace t_SCQTAP10 with this.w_SCQTAP10
    replace t_SCQTAP11 with this.w_SCQTAP11
    replace t_SCQTAP12 with this.w_SCQTAP12
    replace t_SCQTAP13 with this.w_SCQTAP13
    replace t_SCQTAP14 with this.w_SCQTAP14
    replace t_OLTUNMIS with this.w_OLTUNMIS
    replace t_OLTCOART with this.w_OLTCOART
    replace t_SCCODCIC with this.w_SCCODCIC
    replace t_QTARES with this.w_QTARES
    replace t_OLTSTATO with this.w_OLTSTATO
    replace t_OLTQTOD1 with this.w_OLTQTOD1
    replace t_SCSEQSCA with this.w_SCSEQSCA
    replace t_SCDINRIC with this.w_SCDINRIC
    replace t_SCCODICE with this.w_SCCODICE
    replace t_SCCODODL with this.w_SCCODODL
    replace t_DESCIC with this.w_DESCIC
    replace t_OLTDTINI with this.w_OLTDTINI
    replace t_OLTQTODL with this.w_OLTQTODL
    replace t_SCQTAODL with this.w_SCQTAODL
    replace t_SCDESART with this.w_SCDESART
    replace t_OLTQTOEV with this.w_OLTQTOEV
    replace t_OLTQTORE with this.w_OLTQTORE
    replace t_SCRISODL with this.w_SCRISODL
    replace t_INPUTODL with this.w_INPUTODL
    replace t_SCQTMODL with this.w_SCQTMODL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOQTAC01 = .w_TOQTAC01-.w_scqtap01
        .w_TOQTAC02 = .w_TOQTAC02-.w_scqtap02
        .w_TOQTAC03 = .w_TOQTAC03-.w_scqtap03
        .w_TOQTAC04 = .w_TOQTAC04-.w_scqtap04
        .w_TOQTAC05 = .w_TOQTAC05-.w_scqtap05
        .w_TOQTAC06 = .w_TOQTAC06-.w_scqtap06
        .w_TOQTAC07 = .w_TOQTAC07-.w_scqtap07
        .w_TOQTAC08 = .w_TOQTAC08-.w_scqtap08
        .w_TOQTAC09 = .w_TOQTAC09-.w_scqtap09
        .w_TOQTAC10 = .w_TOQTAC10-.w_scqtap10
        .w_TOQTAC11 = .w_TOQTAC11-.w_scqtap11
        .w_TOQTAC12 = .w_TOQTAC12-.w_scqtap12
        .w_TOQTAC13 = .w_TOQTAC13-.w_scqtap13
        .w_TOQTAC14 = .w_TOQTAC14-.w_scqtap14
        .w_TOQTARES = .w_TOQTARES-.w_qtares
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mspPag1 as StdContainer
  Width  = 1065
  height = 580
  stdWidth  = 1065
  stdheight = 580
  resizeXpos=74
  resizeYpos=320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=97, width=1026,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=18,Field1="CPROWORD",Label1="Seq.",Field2="SCCODODL",Label2="IIF(w_TIPGES=='LQA' or w_TIPGES=='LOA', ah_msgformat('Codice articolo'), IIF(w_TIPGES=='LQO' or w_TIPGES=='LOO', ah_msgformat('Codice ODL'), ah_msgformat(' ')))",Field3="SCQTARIC",Label3="Qta",Field4="SCQTAP01",Label4="ah_msgformat(w_SCHPER01)",Field5="SCQTAP02",Label5="ah_msgformat(w_SCHPER02)",Field6="SCQTAP03",Label6="ah_msgformat(w_SCHPER03)",Field7="SCQTAP04",Label7="ah_msgformat(w_SCHPER04)",Field8="SCQTAP05",Label8="ah_msgformat(w_SCHPER05)",Field9="SCQTAP06",Label9="ah_msgformat(w_SCHPER06)",Field10="SCQTAP07",Label10="ah_msgformat(w_SCHPER07)",Field11="SCQTAP08",Label11="ah_msgformat(w_SCHPER08)",Field12="SCQTAP09",Label12="ah_msgformat(w_SCHPER09)",Field13="SCQTAP10",Label13="ah_msgformat(w_SCHPER10)",Field14="SCQTAP11",Label14="ah_msgformat(w_SCHPER11)",Field15="SCQTAP12",Label15="ah_msgformat(w_SCHPER12)",Field16="SCQTAP13",Label16="ah_msgformat(w_SCHPER13)",Field17="SCQTAP14",Label17="ah_msgformat(w_SCHPER14)",Field18="QTARES",Label18="Residuo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218989690

  add object oSCSERIAL_1_2 as StdField with uid="EBYXIRDUVC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SCSERIAL", cQueryName = "SCSERIAL",nZero=15,;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice progressivo",;
    HelpContextID = 123677838,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=84, Top=10, InputMask=replicate('X',15)

  add object oSCTABRIS_1_3 as StdField with uid="UEJXODIXJQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SCTABRIS", cQueryName = "SCTABRIS",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inestente.",;
    ToolTipText = "Codice risorsa",;
    HelpContextID = 10281849,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=328, Top=10, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="TAB_RISO", oKey_1_1="RICODICE", oKey_1_2="this.w_SCTABRIS"

  func oSCTABRIS_1_3.mCond()
    with this.Parent.oContained
      return (empty(.w_SCTABRIS) )
    endwith
  endfunc

  func oSCTABRIS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCTABRIS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCTABRIS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_RISO','*','RICODICE',cp_AbsName(this.parent,'oSCTABRIS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCO_ZLR.TAB_RISO_VZM',this.parent.oContained
  endproc

  add object oSCDATINI_1_4 as StdField with uid="TQSOMUGKKF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCDATINI", cQueryName = "SCDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Il periodo deve iniziare di luned�.",;
    ToolTipText = "Data inizio periodo",;
    HelpContextID = 121904273,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=938, Top=10

  func oSCDATINI_1_4.mCond()
    with this.Parent.oContained
      return (not empty(.w_SCTIPPIA))
    endwith
  endfunc

  func oSCDATINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_SCTIPPIA))
    endwith
    return bRes
  endfunc

  add object oSCQTCPDF_1_5 as StdField with uid="XZVSEWMLIX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCQTCPDF", cQueryName = "SCQTCPDF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� produttiva default",;
    HelpContextID = 247444332,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=328, Top=34, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oSCQTCPDF_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPSCA<>'CSQ')
    endwith
    endif
  endfunc

  func oSCQTCPDF_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SCQTCPDF >=0)
    endwith
    return bRes
  endfunc

  add object oSCQTAC01_1_6 as StdField with uid="QAROVPVKNW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCQTAC01", cQueryName = "SCQTAC01",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 01",;
    HelpContextID = 241192105,;
   bGlobalFont=.t.,;
    Height=21, Width=54, Left=217, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC02_1_7 as StdField with uid="SLRZOBWMMS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SCQTAC02", cQueryName = "SCQTAC02",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 02",;
    HelpContextID = 241192104,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=270, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC03_1_8 as StdField with uid="NSQXNMKKTZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SCQTAC03", cQueryName = "SCQTAC03",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 03",;
    HelpContextID = 241192103,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=322, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC04_1_9 as StdField with uid="TINMVRMUPY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SCQTAC04", cQueryName = "SCQTAC04",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 04",;
    HelpContextID = 241192102,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=375, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC05_1_10 as StdField with uid="GTOCTXAEDM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SCQTAC05", cQueryName = "SCQTAC05",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 05",;
    HelpContextID = 241192101,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=429, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC06_1_12 as StdField with uid="GSDUADENZT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SCQTAC06", cQueryName = "SCQTAC06",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 06",;
    HelpContextID = 241192100,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=482, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC07_1_13 as StdField with uid="LKAOAXWMRC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SCQTAC07", cQueryName = "SCQTAC07",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 07",;
    HelpContextID = 241192099,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=535, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC08_1_14 as StdField with uid="GSWYKGAQHK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SCQTAC08", cQueryName = "SCQTAC08",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 08",;
    HelpContextID = 241192098,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=588, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC09_1_15 as StdField with uid="AKXLIRDKHX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCQTAC09", cQueryName = "SCQTAC09",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 09",;
    HelpContextID = 241192097,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=641, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC10_1_16 as StdField with uid="TSHGFLRQLU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SCQTAC10", cQueryName = "SCQTAC10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 10",;
    HelpContextID = 241192106,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=694, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC11_1_17 as StdField with uid="ODJPENZFRS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SCQTAC11", cQueryName = "SCQTAC11",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 11",;
    HelpContextID = 241192105,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=747, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC12_1_18 as StdField with uid="MZFPSANRCD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCQTAC12", cQueryName = "SCQTAC12",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 12",;
    HelpContextID = 241192104,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=800, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC13_1_19 as StdField with uid="SMCKZXZENH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SCQTAC13", cQueryName = "SCQTAC13",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 13",;
    HelpContextID = 241192103,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=853, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oSCQTAC14_1_20 as StdField with uid="KYSBKIHRPH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SCQTAC14", cQueryName = "SCQTAC14",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Capacit� periodo 14",;
    HelpContextID = 241192102,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=906, Top=74, cSayPict='"99999999.99"', cGetPict='"99999999.99"'

  add object oRLDESCRI_1_22 as StdField with uid="OWNHJHPXXE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223351713,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=499, Top=10, InputMask=replicate('X',40)


  add object oObj_1_27 as cp_runprogram with uid="NYFUVFKKHL",left=1088, top=119, width=202,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("Intestazione")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 187538970


  add object SUGG as cp_calclbl with uid="IIADOZHOOG",left=785, top=438, width=76,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontsize=8,;
    nPag=1;
    , HelpContextID = 187538970


  add object CONFE as cp_calclbl with uid="REWIRXYEES",left=887, top=438, width=76,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontsize=8,;
    nPag=1;
    , HelpContextID = 187538970


  add object LAN as cp_calclbl with uid="UDWEVISGYO",left=985, top=437, width=76,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 187538970


  add object oObj_1_34 as cp_runprogram with uid="SXEPVEPGTX",left=1089, top=317, width=202,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("Calendario")',;
    cEvent = "w_SCTABRIS Changed",;
    nPag=1;
    , HelpContextID = 187538970

  add object oSCCALRIS_1_36 as StdField with uid="XRWSQAREMM",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SCCALRIS", cQueryName = "SCCALRIS",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 20697977,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=328, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", oKey_1_1="TCCODICE", oKey_1_2="this.w_SCCALRIS"

  func oSCCALRIS_1_36.mCond()
    with this.Parent.oContained
      return (.cfunction <> 'Query' and not empty(.w_SCTABRIS) AND not empty(.w_SCDATINI))
    endwith
  endfunc

  func oSCCALRIS_1_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPSCA<>'CSO')
    endwith
    endif
  endfunc

  func oSCCALRIS_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCALRIS_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCALRIS_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oSCCALRIS_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oSCDESCAL_1_37 as StdField with uid="FDFPDYNBGY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SCDESCAL", cQueryName = "SCDESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223353998,;
   bGlobalFont=.t.,;
    Height=21, Width=284, Left=398, Top=35, InputMask=replicate('X',40)

  func oSCDESCAL_1_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPSCA<>'CSO')
    endwith
    endif
  endfunc


  add object oSCTIPPIA_1_40 as StdCombo with uid="QRIMIMPUHG",rtseq=26,rtrep=.f.,left=686,top=35,width=115,height=21;
    , height=21, tabstop=.f.;
    , HelpContextID = 260367207;
    , cFormVar="w_SCTIPPIA",RowSource=""+"Giornaliera,"+"Settimanale,"+"Mensile,"+"Annuale", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSCTIPPIA_1_40.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCTIPPIA,&i_cF..t_SCTIPPIA),this.value)
    return(iif(xVal =1,"G",;
    iif(xVal =2,"S",;
    iif(xVal =3,"M",;
    iif(xVal =4,"A",;
    space(1))))))
  endfunc
  func oSCTIPPIA_1_40.GetRadio()
    this.Parent.oContained.w_SCTIPPIA = this.RadioValue()
    return .t.
  endfunc

  func oSCTIPPIA_1_40.ToRadio()
    this.Parent.oContained.w_SCTIPPIA=trim(this.Parent.oContained.w_SCTIPPIA)
    return(;
      iif(this.Parent.oContained.w_SCTIPPIA=="G",1,;
      iif(this.Parent.oContained.w_SCTIPPIA=="S",2,;
      iif(this.Parent.oContained.w_SCTIPPIA=="M",3,;
      iif(this.Parent.oContained.w_SCTIPPIA=="A",4,;
      0)))))
  endfunc

  func oSCTIPPIA_1_40.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_41 as cp_runprogram with uid="OLSSDNRZMS",left=1088, top=161, width=202,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("ControlloDati")',;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 187538970

  add object oSCODLSUG_1_42 as StdCheck with uid="ICCFUJWBUM",rtseq=27,rtrep=.f.,left=769, top=436, caption=" ",;
    ToolTipText = "Abilita l'inserimento di ODL 'suggeriti MRP'",;
    HelpContextID = 37720941,;
    cFormVar="w_SCODLSUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCODLSUG_1_42.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCODLSUG,&i_cF..t_SCODLSUG),this.value)
    return(iif(xVal =1,'M',;
    '.'))
  endfunc
  func oSCODLSUG_1_42.GetRadio()
    this.Parent.oContained.w_SCODLSUG = this.RadioValue()
    return .t.
  endfunc

  func oSCODLSUG_1_42.ToRadio()
    this.Parent.oContained.w_SCODLSUG=trim(this.Parent.oContained.w_SCODLSUG)
    return(;
      iif(this.Parent.oContained.w_SCODLSUG=='M',1,;
      0))
  endfunc

  func oSCODLSUG_1_42.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCODLSUG_1_42.mCond()
    with this.Parent.oContained
      return (g_MMRP='S' and (.cfunction='Load' or .cfunction='Edit'))
    endwith
  endfunc

  add object oSCODLPIA_1_43 as StdCheck with uid="TCVQDIYQVO",rtseq=28,rtrep=.f.,left=871, top=436, caption=" ",;
    ToolTipText = "Abilita l'inserimento di ODL 'pianificati'",;
    HelpContextID = 255824743,;
    cFormVar="w_SCODLPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCODLPIA_1_43.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCODLPIA,&i_cF..t_SCODLPIA),this.value)
    return(iif(xVal =1,'P',;
    '.'))
  endfunc
  func oSCODLPIA_1_43.GetRadio()
    this.Parent.oContained.w_SCODLPIA = this.RadioValue()
    return .t.
  endfunc

  func oSCODLPIA_1_43.ToRadio()
    this.Parent.oContained.w_SCODLPIA=trim(this.Parent.oContained.w_SCODLPIA)
    return(;
      iif(this.Parent.oContained.w_SCODLPIA=='P',1,;
      0))
  endfunc

  func oSCODLPIA_1_43.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCODLPIA_1_43.mCond()
    with this.Parent.oContained
      return (.cfunction='Load' or .cfunction='Edit')
    endwith
  endfunc

  add object oSCODLLAN_1_44 as StdCheck with uid="MFDCPUVDMG",rtseq=29,rtrep=.f.,left=969, top=436, caption=" ",;
    ToolTipText = "Abilita l'inserimento di ODL 'lanciati'",;
    HelpContextID = 79719564,;
    cFormVar="w_SCODLLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCODLLAN_1_44.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCODLLAN,&i_cF..t_SCODLLAN),this.value)
    return(iif(xVal =1,'L',;
    '.'))
  endfunc
  func oSCODLLAN_1_44.GetRadio()
    this.Parent.oContained.w_SCODLLAN = this.RadioValue()
    return .t.
  endfunc

  func oSCODLLAN_1_44.ToRadio()
    this.Parent.oContained.w_SCODLLAN=trim(this.Parent.oContained.w_SCODLLAN)
    return(;
      iif(this.Parent.oContained.w_SCODLLAN=='L',1,;
      0))
  endfunc

  func oSCODLLAN_1_44.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCODLLAN_1_44.mCond()
    with this.Parent.oContained
      return (.cfunction='Load' or .cfunction='Edit')
    endwith
  endfunc


  add object oObj_1_47 as cp_runprogram with uid="OUOGGZXMDM",left=1088, top=182, width=202,height=21,;
    caption='GSCO_BSP',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("Riordina")',;
    cEvent = "Riordina",;
    nPag=1;
    , HelpContextID = 226893130

  add object oSCDATFIN_1_48 as StdField with uid="MJLVYPGUVM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SCDATFIN", cQueryName = "SCDATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine periodo",;
    HelpContextID = 96199540,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=938, Top=35


  add object CPPER as cp_calclbl with uid="WJUGTDXSHZ",left=217, top=58, width=169,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontbold=.t.,fontsize=9,;
    cEvent = "FormLoad",;
    nPag=1;
    , HelpContextID = 187538970


  add object oObj_1_62 as cp_runprogram with uid="PLCBPZSQPD",left=1088, top=77, width=202,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("NEWROW")',;
    cEvent = "Edit Started",;
    nPag=1;
    , HelpContextID = 187538970

  add object oSCPIAINI_1_63 as StdField with uid="GFOTHIOXNC",rtseq=86,rtrep=.f.,;
    cFormVar = "w_SCPIAINI", cQueryName = "SCPIAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 141253777,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=968, Top=480

  add object oSCPIAFIN_1_64 as StdField with uid="WUIPXOSBJC",rtseq=87,rtrep=.f.,;
    cFormVar = "w_SCPIAFIN", cQueryName = "SCPIAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 76850036,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=968, Top=505

  add object oStr_1_11 as StdString with uid="IZZUGIBDIB",Visible=.t., Left=27, Top=555,;
    Alignment=1, Width=85, Height=18,;
    Caption="Codice articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="AGKURXCQFB",Visible=.t., Left=216, Top=12,;
    Alignment=1, Width=110, Height=18,;
    Caption="Codice risorsa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="SMOKGTQSGI",Visible=.t., Left=845, Top=12,;
    Alignment=1, Width=89, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SWPSKUUMYK",Visible=.t., Left=82, Top=532,;
    Alignment=1, Width=30, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TITJAPTHZC",Visible=.t., Left=5, Top=14,;
    Alignment=1, Width=78, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ZHHXKLMBDP",Visible=.t., Left=845, Top=37,;
    Alignment=1, Width=89, Height=18,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HRSWBUEQRG",Visible=.t., Left=163, Top=37,;
    Alignment=1, Width=163, Height=18,;
    Caption="Cap. produttiva risorsa:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_SCTIPSCA<>'CSQ')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="GSOSKCBYME",Visible=.t., Left=190, Top=37,;
    Alignment=1, Width=136, Height=18,;
    Caption="Calendario risorsa:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_SCTIPSCA<>'CSO')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="SFWYCKOUMO",Visible=.t., Left=41, Top=509,;
    Alignment=1, Width=71, Height=18,;
    Caption="Codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="JKZQAZZQMZ",Visible=.t., Left=13, Top=442,;
    Alignment=0, Width=157, Height=17,;
    Caption="Dati ciclo semplificato"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="JIESNRTVRD",Visible=.t., Left=5, Top=467,;
    Alignment=1, Width=107, Height=18,;
    Caption="Ciclo semplificato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="CNSIKAVVIT",Visible=.t., Left=405, Top=532,;
    Alignment=1, Width=76, Height=18,;
    Caption="Inizio effett.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="XIXWNUXLID",Visible=.t., Left=248, Top=509,;
    Alignment=1, Width=55, Height=18,;
    Caption="Evasa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="XPARPTBKUZ",Visible=.t., Left=233, Top=532,;
    Alignment=1, Width=70, Height=18,;
    Caption="Residua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="SOXSLHCIPR",Visible=.t., Left=413, Top=509,;
    Alignment=1, Width=68, Height=18,;
    Caption="Inizio prev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="WGDITBDOAT",Visible=.t., Left=0, Top=593,;
    Alignment=0, Width=875, Height=15,;
    Caption="I campi scqtah%% servono per l�elaborazione della scaletta oraria U.M. per ODL e per la visualizzazione del totale consumo di periodo in ore"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="JVMDYTZPPX",Visible=.t., Left=831, Top=484,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da data inizio ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="EOYQGVGYQP",Visible=.t., Left=819, Top=506,;
    Alignment=1, Width=147, Height=18,;
    Caption="A data inizio ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="IWKPBOSDED",Visible=.t., Left=673, Top=441,;
    Alignment=0, Width=94, Height=17,;
    Caption="Selezione ordini"  ;
  , bGlobalFont=.t.

  add object oBox_1_53 as StdBox with uid="JGKGNOHZHC",left=10, top=457, width=509,height=2

  add object oBox_1_55 as StdBox with uid="CCJTVFNAPX",left=8, top=499, width=760,height=2

  add object oBox_1_67 as StdBox with uid="PPYKMJFLIH",left=678, top=457, width=382,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=116,;
    width=1022+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=117,width=1021+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ODL_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oOLTUNMIS_2_19.Refresh()
      this.Parent.oSCCODCIC_2_21.Refresh()
      this.Parent.oSCDINRIC_2_27.Refresh()
      this.Parent.oSCCODICE_2_28.Refresh()
      this.Parent.oSCCODODL_2_29.Refresh()
      this.Parent.oDESCIC_2_30.Refresh()
      this.Parent.oOLTDTINI_2_31.Refresh()
      this.Parent.oOLTQTODL_2_32.Refresh()
      this.Parent.oSCDESART_2_34.Refresh()
      this.Parent.oOLTQTOEV_2_35.Refresh()
      this.Parent.oOLTQTORE_2_36.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ODL_MAST'
        oDropInto=this.oBodyCol.oRow.oSCCODICE_2_2
      case cFile='ODL_MAST'
        oDropInto=this.oBodyCol.oRow.oSCCODODL_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oOLTUNMIS_2_19 as StdTrsField with uid="DDTAEDJFDY",rtseq=51,rtrep=.t.,;
    cFormVar="w_OLTUNMIS",value=space(3),enabled=.f.,;
    HelpContextID = 208727097,;
    cTotal="", bFixedPos=.t., cQueryName = "OLTUNMIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=111, Top=529, InputMask=replicate('X',3)

  add object oSCCODCIC_2_21 as StdTrsField with uid="MUAHFVLNYA",rtseq=53,rtrep=.t.,;
    cFormVar="w_SCCODCIC",value=space(15),enabled=.f.,;
    HelpContextID = 30004073,;
    cTotal="", bFixedPos=.t., cQueryName = "SCCODCIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=111, Top=465, InputMask=replicate('X',15), cLinkFile="TABMCICL", oKey_1_1="CSCODICE", oKey_1_2="this.w_SCCODCIC"

  func oSCCODCIC_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oSCDINRIC_2_27 as StdTrsField with uid="YFTJOWENLV",rtseq=58,rtrep=.t.,;
    cFormVar="w_SCDINRIC",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 23323497,;
    cTotal="", bFixedPos=.t., cQueryName = "SCDINRIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=482, Top=505

  add object oSCCODICE_2_28 as StdTrsField with uid="PFGEZKTGUB",rtseq=59,rtrep=.t.,;
    cFormVar="w_SCCODICE",value=space(20),enabled=.f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 137768085,;
    cTotal="", bFixedPos=.t., cQueryName = "SCCODICE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=111, Top=553, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_SCCODICE"

  func oSCCODICE_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oSCCODODL_2_29 as StdTrsField with uid="PBCQSEYXLL",rtseq=60,rtrep=.t.,;
    cFormVar="w_SCCODODL",value=space(15),enabled=.f.,;
    ToolTipText = "Codice ODL",;
    HelpContextID = 231330674,;
    cTotal="", bFixedPos=.t., cQueryName = "SCCODODL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=111, Top=505, InputMask=replicate('X',15)

  add object oDESCIC_2_30 as StdTrsField with uid="XIPMNFVCSE",rtseq=61,rtrep=.t.,;
    cFormVar="w_DESCIC",value=space(40),enabled=.f.,;
    HelpContextID = 233909194,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=234, Top=465, InputMask=replicate('X',40)

  add object oOLTDTINI_2_31 as StdTrsField with uid="FODAOHMOOY",rtseq=62,rtrep=.t.,;
    cFormVar="w_OLTDTINI",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data inizio effettivo",;
    HelpContextID = 121639889,;
    cTotal="", bFixedPos=.t., cQueryName = "OLTDTINI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=482, Top=529

  add object oOLTQTODL_2_32 as StdTrsField with uid="VJJPZNXSUR",rtseq=63,rtrep=.t.,;
    cFormVar="w_OLTQTODL",value=0,enabled=.f.,;
    HelpContextID = 248310834,;
    cTotal="", bFixedPos=.t., cQueryName = "OLTQTODL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=148, Top=529, cSayPict=[v_GQ(12)], cGetPict=[v_GQ(12)]

  add object oSCDESART_2_34 as StdTrsField with uid="NVUIHEDWUQ",rtseq=65,rtrep=.t.,;
    cFormVar="w_SCDESART",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 256908422,;
    cTotal="", bFixedPos=.t., cQueryName = "SCDESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=318, Left=414, Top=553, InputMask=replicate('X',40)

  add object oOLTQTOEV_2_35 as StdTrsField with uid="RRFAYSKHBS",rtseq=66,rtrep=.t.,;
    cFormVar="w_OLTQTOEV",value=0,enabled=.f.,;
    ToolTipText = "Quantit� evasa dell'ordine",;
    HelpContextID = 248310844,;
    cTotal="", bFixedPos=.t., cQueryName = "OLTQTOEV",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=305, Top=503

  add object oOLTQTORE_2_36 as StdTrsField with uid="MWSMUMNRIR",rtseq=67,rtrep=.t.,;
    cFormVar="w_OLTQTORE",value=0,enabled=.f.,;
    ToolTipText = "Quantit� residua dell'ordine",;
    HelpContextID = 20124629,;
    cTotal="", bFixedPos=.t., cQueryName = "OLTQTORE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=305, Top=529

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOQTAC01_3_1 as StdField with uid="ABWCCVUBAY",rtseq=71,rtrep=.f.,;
    cFormVar="w_TOQTAC01",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 01",;
    HelpContextID = 241189017,;
    cQueryName = "TOQTAC01",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=218, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC02_3_2 as StdField with uid="VFOJQGUQLY",rtseq=72,rtrep=.f.,;
    cFormVar="w_TOQTAC02",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 02",;
    HelpContextID = 241189016,;
    cQueryName = "TOQTAC02",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=271, Top=406, cSayPict=["99999.99"]

  add object oBtn_3_3 as StdButton with uid="ASPJDGGNLU",width=48,height=45,;
   left=882, top=530,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=3;
    , ToolTipText = "Riempie il dettaglio con fasi che utilizzano la risorsa";
    , HelpContextID = 138278001;
    , TabStop=.f., Caption='\<Car.Rap.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"Dettaglio")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_3.mCond()
    with this.Parent.oContained
      return ((.cfunction='Load' Or .cfunction='Edit') AND not empty(.w_SCTABRIS) AND not empty(.w_SCDATINI))
    endwith
  endfunc

  add object oBtn_3_4 as StdButton with uid="GPSHUJNUGW",width=48,height=45,;
   left=938, top=530,;
    CpPicture="BMP\ELABORA.bmp", caption="", nPag=3;
    , ToolTipText = "Elabora la scaletta di produzione";
    , HelpContextID = 59693894;
    , TabStop=.f., Caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_4.Click()
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"Elabora")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTOQTAC03_3_5 as StdField with uid="AOVKHTJAHC",rtseq=73,rtrep=.f.,;
    cFormVar="w_TOQTAC03",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 03",;
    HelpContextID = 241189015,;
    cQueryName = "TOQTAC03",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=324, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC04_3_6 as StdField with uid="VLTBZFJRPP",rtseq=74,rtrep=.f.,;
    cFormVar="w_TOQTAC04",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 04",;
    HelpContextID = 241189014,;
    cQueryName = "TOQTAC04",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=377, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC05_3_7 as StdField with uid="HOSLOORPGC",rtseq=75,rtrep=.f.,;
    cFormVar="w_TOQTAC05",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 05",;
    HelpContextID = 241189013,;
    cQueryName = "TOQTAC05",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=430, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC06_3_8 as StdField with uid="WCUSSFWGBN",rtseq=76,rtrep=.f.,;
    cFormVar="w_TOQTAC06",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 06",;
    HelpContextID = 241189012,;
    cQueryName = "TOQTAC06",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=483, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC07_3_9 as StdField with uid="OGUTFAEOOT",rtseq=77,rtrep=.f.,;
    cFormVar="w_TOQTAC07",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 07",;
    HelpContextID = 241189011,;
    cQueryName = "TOQTAC07",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=536, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC08_3_10 as StdField with uid="ERUNZTECXU",rtseq=78,rtrep=.f.,;
    cFormVar="w_TOQTAC08",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 08",;
    HelpContextID = 241189010,;
    cQueryName = "TOQTAC08",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=589, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC09_3_11 as StdField with uid="WQRZUNQRFU",rtseq=79,rtrep=.f.,;
    cFormVar="w_TOQTAC09",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 09",;
    HelpContextID = 241189009,;
    cQueryName = "TOQTAC09",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=642, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC10_3_12 as StdField with uid="NCXMWPPLTR",rtseq=80,rtrep=.f.,;
    cFormVar="w_TOQTAC10",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 10",;
    HelpContextID = 241189018,;
    cQueryName = "TOQTAC10",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=695, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC11_3_13 as StdField with uid="MCTTOTCGDT",rtseq=81,rtrep=.f.,;
    cFormVar="w_TOQTAC11",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 11",;
    HelpContextID = 241189017,;
    cQueryName = "TOQTAC11",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=748, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC12_3_14 as StdField with uid="GQPFJHKGHU",rtseq=82,rtrep=.f.,;
    cFormVar="w_TOQTAC12",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 12",;
    HelpContextID = 241189016,;
    cQueryName = "TOQTAC12",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=801, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC13_3_15 as StdField with uid="XYTDSSOBKE",rtseq=83,rtrep=.f.,;
    cFormVar="w_TOQTAC13",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 13",;
    HelpContextID = 241189015,;
    cQueryName = "TOQTAC13",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=854, Top=406, cSayPict=["99999.99"]

  add object oTOQTAC14_3_16 as StdField with uid="NGIPIGJGWU",rtseq=84,rtrep=.f.,;
    cFormVar="w_TOQTAC14",value=0,enabled=.f.,;
    ToolTipText = "Totale consumo periodo 14",;
    HelpContextID = 241189014,;
    cQueryName = "TOQTAC14",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=908, Top=406, cSayPict=["99999.99"]

  add object oTOQTARES_3_17 as StdField with uid="ZHCTYADWRN",rtseq=85,rtrep=.f.,;
    cFormVar="w_TOQTARES",value=0,enabled=.f.,;
    ToolTipText = "Totale residuo",;
    HelpContextID = 10469257,;
    cQueryName = "TOQTARES",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=53, Left=962, Top=406, cSayPict=["99999.99"]

  add object oBtn_3_18 as StdButton with uid="NNBGTDHLFI",width=48,height=45,;
   left=826, top=530,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per eseguire l'importazione da altri piani";
    , HelpContextID = 171127674;
    , TabStop=.f., Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_18.Click()
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"Importa")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_18.mCond()
    with this.Parent.oContained
      return ((.cfunction='Load' or .cfunction='Edit') AND not empty(.w_SCTABRIS) AND not empty(.w_SCDATINI))
    endwith
  endfunc

  add object oBtn_3_19 as StdButton with uid="UONTLYFTVR",width=48,height=45,;
   left=994, top=530,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=3;
    , ToolTipText = "Esegue la stampa della scaletta";
    , HelpContextID = 223747034;
    , TabStop=.f., Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_19.Click()
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"Stampa")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_19.mCond()
    with this.Parent.oContained
      return (.cFunction='Query' and NOT EMPTY(.w_SCSERIAL))
    endwith
  endfunc

  add object oBtn_3_20 as StdButton with uid="RXYCQISMZY",width=48,height=45,;
   left=769, top=530,;
    CpPicture="BMP\fifocont.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per eseguire l'ordinamento della scaletta in base alla sequenza";
    , HelpContextID = 226094618;
    , TabStop=.f., Caption='\<Sequenza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_20.Click()
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"Riordina")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_20.mCond()
    with this.Parent.oContained
      return (.cfunction='Load' or .cfunction='Edit')
    endwith
  endfunc

  add object oObj_3_21 as cp_runprogram with uid="OIKNQAYIQX",width=202,height=21,;
   left=1087, top=231,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("CapPeriodo")',;
    cEvent = "w_SCTABRIS Changed, w_SCDATINI Changed, w_SCTIPPIA Changed,w_SCCALRIS Changed, w_SCQTCPDF Changed",;
    nPag=3;
    , HelpContextID = 187538970

  add object oObj_3_22 as cp_runprogram with uid="PKGIOIUYFX",width=202,height=21,;
   left=1087, top=252,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BSP('DatiPeriodo')",;
    cEvent = "DatiPeriodo,Load,Blank,Init",;
    nPag=3;
    , HelpContextID = 187538970

  add object oObj_3_23 as cp_runprogram with uid="TLZHWZZDIT",width=202,height=21,;
   left=1087, top=210,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("Elabora")',;
    cEvent = "w_SCQTCPDF Changed",;
    nPag=3;
    , HelpContextID = 187538970
enddefine

* --- Defining Body row
define class tgsco_mspBodyRow as CPBodyRowCnt
  Width=1012
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="PKSXNZHTNC",rtseq=33,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza",;
    HelpContextID = 17117334,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], BackStyle=0, bUseMenu=.t.

  add object oSCCODICE_2_2 as StdTrsField with uid="JVDTGCAEEP",rtseq=34,rtrep=.t.,;
    cFormVar="w_SCCODICE",value=space(20),;
    HelpContextID = 137768085,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=7, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=18, Width=109, Left=29, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , BackStyle=0, bUseMenu=.t.,bNoContxtBtn=.t., cLinkFile="ODL_MAST", oKey_1_1="OLTCODIC", oKey_1_2="this.w_SCCODICE"

  func oSCCODICE_2_2.mCond()
    with this.Parent.oContained
      return (!empty(.w_SCTABRIS))
    endwith
  endfunc

  func oSCCODICE_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES=='LQO' or .w_TIPGES=='LOO')
    endwith
    endif
  endfunc

  func oSCCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSCCODICE_2_2.mZoom
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"ZINPUTODL")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSCCODODL_2_3 as StdTrsField with uid="QGBSZOOZFK",rtseq=35,rtrep=.t.,;
    cFormVar="w_SCCODODL",value=space(15),nZero=15,;
    ToolTipText = "Codice ODL",;
    HelpContextID = 231330674,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=110, Left=29, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , BackStyle=0, bUseMenu=.t.,bNoContxtBtn=.t., cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_SCCODODL"

  func oSCCODODL_2_3.mCond()
    with this.Parent.oContained
      return (!empty(.w_SCTABRIS))
    endwith
  endfunc

  func oSCCODODL_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES=='LQA' or .w_TIPGES=='LOA')
    endwith
    endif
  endfunc

  func oSCCODODL_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODODL_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSCCODODL_2_3.mZoom
      with this.Parent.oContained
        GSCO_BSP(this.Parent.oContained,"ZINPUTODL")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSCQTARIC_2_4 as StdTrsField with uid="OWVUTZKKUG",rtseq=36,rtrep=.t.,;
    cFormVar="w_SCQTARIC",value=0,;
    ToolTipText = "Quantit� richiesta",;
    HelpContextID = 10466153,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=63, Left=142, Top=0, cSayPict=["999999.99999"], cGetPict=["999999.99999"], BackStyle=0, bUseMenu=.t.,bNoContxtBtn=.t.

  func oSCQTARIC_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_SCCODODL))
    endwith
  endfunc

  func oSCQTARIC_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SCQTARIC > 0)
    endwith
    return bRes
  endfunc

  add object oSCQTAP01_2_5 as StdTrsField with uid="XYYRMMSSTN",rtseq=37,rtrep=.t.,;
    cFormVar="w_SCQTAP01",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 01",;
    HelpContextID = 23088297,;
    cTotal = "this.Parent.oContained.w_toqtac01", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=208, Top=0, cSayPict=["@Z 999999.99999"], cGetPict=["999999.99999"]

  add object oSCQTAP02_2_6 as StdTrsField with uid="FUISDFHMXR",rtseq=38,rtrep=.t.,;
    cFormVar="w_SCQTAP02",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 02",;
    HelpContextID = 23088296,;
    cTotal = "this.Parent.oContained.w_toqtac02", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=18, Width=53, Left=262, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP03_2_7 as StdTrsField with uid="RWIHHUIDHM",rtseq=39,rtrep=.t.,;
    cFormVar="w_SCQTAP03",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 03",;
    HelpContextID = 23088295,;
    cTotal = "this.Parent.oContained.w_toqtac03", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=315, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP04_2_8 as StdTrsField with uid="NLYAAMAQYK",rtseq=40,rtrep=.t.,;
    cFormVar="w_SCQTAP04",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 04",;
    HelpContextID = 23088294,;
    cTotal = "this.Parent.oContained.w_toqtac04", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=368, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP05_2_9 as StdTrsField with uid="QQGZEGNMHJ",rtseq=41,rtrep=.t.,;
    cFormVar="w_SCQTAP05",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 05",;
    HelpContextID = 23088293,;
    cTotal = "this.Parent.oContained.w_toqtac05", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=422, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP06_2_10 as StdTrsField with uid="MSOAKEYKDM",rtseq=42,rtrep=.t.,;
    cFormVar="w_SCQTAP06",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 06",;
    HelpContextID = 23088292,;
    cTotal = "this.Parent.oContained.w_toqtac06", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=475, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP07_2_11 as StdTrsField with uid="AIVZMEIFEW",rtseq=43,rtrep=.t.,;
    cFormVar="w_SCQTAP07",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 07",;
    HelpContextID = 23088291,;
    cTotal = "this.Parent.oContained.w_toqtac07", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=528, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP08_2_12 as StdTrsField with uid="CGVLQUZFAH",rtseq=44,rtrep=.t.,;
    cFormVar="w_SCQTAP08",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 08",;
    HelpContextID = 23088290,;
    cTotal = "this.Parent.oContained.w_toqtac08", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=581, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP09_2_13 as StdTrsField with uid="IUYQQIKGXB",rtseq=45,rtrep=.t.,;
    cFormVar="w_SCQTAP09",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 09",;
    HelpContextID = 23088289,;
    cTotal = "this.Parent.oContained.w_toqtac09", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=634, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP10_2_14 as StdTrsField with uid="XYJOMPXVXL",rtseq=46,rtrep=.t.,;
    cFormVar="w_SCQTAP10",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 10",;
    HelpContextID = 23088298,;
    cTotal = "this.Parent.oContained.w_toqtac10", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=687, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP11_2_15 as StdTrsField with uid="JWPQVWZIEU",rtseq=47,rtrep=.t.,;
    cFormVar="w_SCQTAP11",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 11",;
    HelpContextID = 23088297,;
    cTotal = "this.Parent.oContained.w_toqtac11", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=740, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP12_2_16 as StdTrsField with uid="NSBRRWSQNN",rtseq=48,rtrep=.t.,;
    cFormVar="w_SCQTAP12",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 12",;
    HelpContextID = 23088296,;
    cTotal = "this.Parent.oContained.w_toqtac12", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=793, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP13_2_17 as StdTrsField with uid="ZVCJWBAWCF",rtseq=49,rtrep=.t.,;
    cFormVar="w_SCQTAP13",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 13",;
    HelpContextID = 23088295,;
    cTotal = "this.Parent.oContained.w_toqtac13", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=846, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oSCQTAP14_2_18 as StdTrsField with uid="SIOVWPREFO",rtseq=50,rtrep=.t.,;
    cFormVar="w_SCQTAP14",value=0,enabled=.f.,;
    ToolTipText = "Consumo periodo 14",;
    HelpContextID = 23088294,;
    cTotal = "this.Parent.oContained.w_toqtac14", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=899, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oQTARES_2_22 as StdTrsField with uid="KLGLWPFJKW",rtseq=54,rtrep=.t.,;
    cFormVar="w_QTARES",value=0,enabled=.f.,;
    ToolTipText = "Q.t� residua di riga",;
    HelpContextID = 237190138,;
    cTotal = "this.Parent.oContained.w_toqtares", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=954, Top=0, cSayPict=["@Z 9999.99999"], cGetPict=["9999.99999"]

  add object oObj_2_40 as cp_runprogram with uid="EIEWDBTSFV",width=198,height=27,;
   left=787, top=476,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCO_BSP("QTAVAR")',;
    cEvent = "w_SCQTARIC Changed",;
    nPag=2;
    , HelpContextID = 187538970
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_msp','SCALETTM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCSERIAL=SCALETTM.SCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsco_msp
* --- Funzione per il recupero dell'anno in base alla data passata
* --- utilizzato per il calcolo delle settimane
func CalcYear(DatProgr)
      do case
        case week(DatProgr + 6 ,2,2)=52 Or week(DatProgr + 6,2,2)=53
          L_Year = Alltrim(str(year(DatProgr)))
        otherwise
          L_Year = Alltrim(str(year(DatProgr+ 6)))
      endcase
      return(L_Year)
endfun
* --- Fine Area Manuale
