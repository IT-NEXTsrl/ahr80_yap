* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bpp                                                        *
*              Parametri produzione                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_173]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-20                                                      *
* Last revis.: 2016-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bpp",oParentObject,m.Azione)
return(i_retval)

define class tgsco_bpp as StdBatch
  * --- Local variables
  Azione = space(1)
  PunPAD = .NULL.
  w_COLORI = space(10)
  TmpN = 0
  w_MSG = space(10)
  w_PPCOLSUM = 0
  w_PPCOLSUG = 0
  w_PPCOLCON = 0
  w_PPCOLMDP = 0
  w_PPCOLPIA = 0
  w_PPCOLLAN = 0
  w_PPCOLMIS = 0
  w_PPCOLFES = 0
  w_PPCOLMCO = 0
  w_NMAXPE = 0
  w_TCAUCOL = space(5)
  w_TFLIMPE = space(1)
  w_TFLORDI = space(1)
  w_TFLRISE = space(1)
  w_TFLCASC = space(1)
  w_CDPERMRP = space(200)
  w_OLDCOM = space(1)
  w_OLDCODCOM = space(15)
  w_PREFAS = space(6)
  w_FASSEP = space(1)
  w_PREFASE = space(6)
  w_ARTEXIST = .f.
  w_STR = space(20)
  w_UNMIS1 = space(3)
  w_FLTEMP1 = space(1)
  * --- WorkFile variables
  PAR_PROD_idx=0
  CAM_AGAZ_idx=0
  TIP_DOCU_idx=0
  MAGAZZIN_idx=0
  CENCOST_idx=0
  TAB_CALE_idx=0
  ART_ICOL_idx=0
  CCF_MAST_idx=0
  UNIMIS_idx=0
  CAN_TIER_idx=0
  RIS_ORSE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Parametri Produzione (da GSCO_KPP)
    * --- Variabili maschera da valorizzare
    * --- --- Variabili Parametri ODA ---
    * --- --- Variabili Parametri MPS ---
    * --- --- Variabili Parametri export MS project ---
    * --- --- Variabili Cursore MRP  ---
    * --- --- Variabili Prouduzione funzioni avanzate
    * --- Causale rientro conto lavoro fasi da avanzare
    * --- Codice Fisso di Ricerca
    this.PunPAD = this.oParentObject
    this.w_COLORI = "COLSUM-SFOSUM-COLPIA-SFOPIA-COLLAN-SFOLAN-COLMIS-SFOMIS-COLFES-SFOMCO-SFOSUG-COLSUG-COLCON-SFOCON-COLDPI-SFODPI"
    do case
      case this.Azione = "LOAD"
        * --- Apertura della maschera - Legge Parametri e scrive variabili
        * --- Try
        local bErr_041BB0C0
        bErr_041BB0C0=bTrsErr
        this.Try_041BB0C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Parametri non ancora caricati
          ah_ErrorMsg("Tabella parametri produzione non caricata","!","")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_041BB0C0
        * --- End
        if not empty(this.oParentObject.w_PPCCSODA)
          * --- Read from CENCOST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CENCOST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCDESPIA,CCNUMLIV"+;
              " from "+i_cTable+" CENCOST where ";
                  +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_PPCCSODA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCDESPIA,CCNUMLIV;
              from (i_cTable) where;
                  CC_CONTO = this.oParentObject.w_PPCCSODA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCCODA = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
            this.oParentObject.w_NUMLVODA = NVL(cp_ToDate(_read_.CCNUMLIV),cp_NullValue(_read_.CCNUMLIV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUORD)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMDESCRI,CMFLORDI"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCAUORD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMDESCRI,CMFLORDI;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_PPCAUORD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESORD = NVL(cp_ToDate(_read_.CMDESCRI),cp_NullValue(_read_.CMDESCRI))
            this.oParentObject.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUIMP)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMDESCRI,CMFLIMPE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCAUIMP);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMDESCRI,CMFLIMPE;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_PPCAUIMP;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESIMP = NVL(cp_ToDate(_read_.CMDESCRI),cp_NullValue(_read_.CMDESCRI))
            this.oParentObject.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUTER)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC,TDFLVEAC,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCAUTER);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCAUTER;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAZ = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLVEAZ = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.oParentObject.w_CATDOZ = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUTRA)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCAUTRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCAUTRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAT = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLVEAT = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.oParentObject.w_CATDOT = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            this.oParentObject.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCODCAU)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC,TDFLINTE,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC,TDFLINTE,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAB = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLINTB = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.oParentObject.w_CATDOB = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUCAR)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC,TDFLINTE,TDCAUMAG"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCAUCAR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC,TDFLINTE,TDCAUMAG;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCAUCAR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAR = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLINTC = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.oParentObject.w_CAUCAR = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUSCA)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC,TDFLINTE,TDCAUMAG"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCAUSCA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC,TDFLINTE,TDCAUMAG;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCAUSCA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESSCA = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLINTS = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.oParentObject.w_CAUSCA = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPMAGPRO)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDESMAG,MGDISMAG,MGTIPMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_PPMAGPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDESMAG,MGDISMAG,MGTIPMAG;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_PPMAGPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
            this.oParentObject.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            this.oParentObject.w_PROWIP = NVL(cp_ToDate(_read_.MGTIPMAG),cp_NullValue(_read_.MGTIPMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPMAGSCA)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDESMAG,MGTIPMAG,MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_PPMAGSCA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDESMAG,MGTIPMAG,MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_PPMAGSCA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESMAS = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
            this.oParentObject.w_SCAWIP = NVL(cp_ToDate(_read_.MGTIPMAG),cp_NullValue(_read_.MGTIPMAG))
            this.oParentObject.w_SCADIS = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPMAGWIP)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDESMAG,MGTIPMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_PPMAGWIP);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDESMAG,MGTIPMAG;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_PPMAGWIP;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESMAW = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
            this.oParentObject.w_TIPWIP = NVL(cp_ToDate(_read_.MGTIPMAG),cp_NullValue(_read_.MGTIPMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCENCOS)
          * --- Read from CENCOST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CENCOST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCDESPIA,CCDTOBSO"+;
              " from "+i_cTable+" CENCOST where ";
                  +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_PPCENCOS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCDESPIA,CCDTOBSO;
              from (i_cTable) where;
                  CC_CONTO = this.oParentObject.w_PPCENCOS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCEN = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
            this.oParentObject.w_DATOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCALSTA)
          * --- Read from TAB_CALE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TAB_CALE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_CALE_idx,2],.t.,this.TAB_CALE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCDESCRI"+;
              " from "+i_cTable+" TAB_CALE where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCALSTA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCDESCRI;
              from (i_cTable) where;
                  TCCODICE = this.oParentObject.w_PPCALSTA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAL = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUCCM)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMDESCRI,CMAGGVAL,CMFLCASC"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCAUCCM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMDESCRI,CMAGGVAL,CMFLCASC;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_PPCAUCCM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCCM = NVL(cp_ToDate(_read_.CMDESCRI),cp_NullValue(_read_.CMDESCRI))
            this.oParentObject.w_CHKCC = NVL(cp_ToDate(_read_.CMAGGVAL),cp_NullValue(_read_.CMAGGVAL))
            this.oParentObject.w_CHKCSC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUSCM)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMDESCRI,CMAGGVAL,CMFLCASC"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCAUSCM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMDESCRI,CMAGGVAL,CMFLCASC;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_PPCAUSCM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESSCM = NVL(cp_ToDate(_read_.CMDESCRI),cp_NullValue(_read_.CMDESCRI))
            this.oParentObject.w_CHKCS = NVL(cp_ToDate(_read_.CMAGGVAL),cp_NullValue(_read_.CMAGGVAL))
            this.oParentObject.w_CHKCSS = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if not empty(this.oParentObject.w_PPSRVDFT)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART,ARTIPART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_PPSRVDFT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART,ARTIPART;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_PPSRVDFT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PPSRVDSC = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            this.oParentObject.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if not empty(this.oParentObject.w_PPCLAFAS)
          * --- Read from CCF_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCF_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCF_MAST_idx,2],.t.,this.CCF_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CFDESCRI,CFTIPCLA"+;
              " from "+i_cTable+" CCF_MAST where ";
                  +"CFCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCLAFAS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CFDESCRI,CFTIPCLA;
              from (i_cTable) where;
                  CFCODICE = this.oParentObject.w_PPCLAFAS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PPDESCFAS = NVL(cp_ToDate(_read_.CFDESCRI),cp_NullValue(_read_.CFDESCRI))
            this.oParentObject.w_CFTIPCLC = NVL(cp_ToDate(_read_.CFTIPCLA),cp_NullValue(_read_.CFTIPCLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if not empty(this.oParentObject.w_PPCLAART)
          * --- Read from CCF_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCF_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCF_MAST_idx,2],.t.,this.CCF_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CFDESCRI,CFTIPCLA"+;
              " from "+i_cTable+" CCF_MAST where ";
                  +"CFCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCLAART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CFDESCRI,CFTIPCLA;
              from (i_cTable) where;
                  CFCODICE = this.oParentObject.w_PPCLAART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PPDESCART = NVL(cp_ToDate(_read_.CFDESCRI),cp_NullValue(_read_.CFDESCRI))
            this.oParentObject.w_CFTIPCLA = NVL(cp_ToDate(_read_.CFTIPCLA),cp_NullValue(_read_.CFTIPCLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCODCOM)
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_PPCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.oParentObject.w_PPCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCOM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if not empty(this.oParentObject.w_PPUNIPRO)
          * --- Read from RIS_ORSE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RLDESCRI"+;
              " from "+i_cTable+" RIS_ORSE where ";
                  +"RL__TIPO = "+cp_ToStrODBC("UP");
                  +" and RLCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPUNIPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RLDESCRI;
              from (i_cTable) where;
                  RL__TIPO = "UP";
                  and RLCODICE = this.oParentObject.w_PPUNIPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DEUNIPRO = NVL(cp_ToDate(_read_.RLDESCRI),cp_NullValue(_read_.RLDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if g_COLA="S" And Not Empty(this.oParentObject.w_PPCAURCL)
          * --- Leggo dati causale rientro conto lavoro per fasi da avanzare
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCAURCL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCAURCL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PPCAURCL = NVL(cp_ToDate(_read_.TDTIPDOC),cp_NullValue(_read_.TDTIPDOC))
            this.oParentObject.w_DESDOC = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.oParentObject.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCAUMOU)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC,TDFLINTE,TDCAUMAG"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPCAUMOU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC,TDFLINTE,TDCAUMAG;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_PPCAUMOU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAM = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            this.oParentObject.w_FLINTM = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.oParentObject.w_CAUMOU = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case this.Azione = "SALVA"
        * --- Ricalcola colori
        this.w_PPCOLSUG = this.oParentObject.w_COLSUG*(rgb(255,255,255)+1) + this.oParentObject.w_SFOSUG
        this.w_PPCOLCON = this.oParentObject.w_COLCON*(rgb(255,255,255)+1) + this.oParentObject.w_SFOCON
        this.w_PPCOLMDP = this.oParentObject.w_COLDPI*(rgb(255,255,255)+1) + this.oParentObject.w_SFODPI
        this.w_PPCOLSUM = this.oParentObject.w_COLSUM*(rgb(255,255,255)+1) + this.oParentObject.w_SFOSUM
        this.w_PPCOLPIA = this.oParentObject.w_COLPIA*(rgb(255,255,255)+1) + this.oParentObject.w_SFOPIA
        this.w_PPCOLLAN = this.oParentObject.w_COLLAN*(rgb(255,255,255)+1) + this.oParentObject.w_SFOLAN
        this.w_PPCOLMIS = this.oParentObject.w_COLMIS*(rgb(255,255,255)+1) + this.oParentObject.w_SFOMIS
        this.w_PPCOLFES = this.oParentObject.w_COLFES
        this.w_PPCOLMCO = this.oParentObject.w_SFOMCO
        this.oParentObject.w_PPNMAXPE = iif(this.oParentObject.w_PPNMAXPE=0,54,this.oParentObject.w_PPNMAXPE)
        this.oParentObject.w_PPGIOSCA = iif(g_MMPS="S",this.oParentObject.w_PPGIOSCA,0)
        this.oParentObject.w_PP___DTF = iif(g_MMPS="S",this.oParentObject.w_PP___DTF,0)
        this.w_NMAXPE = this.oParentObject.w_PPNUMGIO+this.oParentObject.w_PPNUMSET+this.oParentObject.w_PPNUMMES+this.oParentObject.w_PPNUMTRI
        this.w_CDPERMRP = alltrim(this.oParentObject.w_PPATHMRP)
        if this.w_NMAXPE > this.oParentObject.w_PPNMAXPE
          ah_ErrorMsg("La somma del numero di periodi per la definizione dei bidoni temporali � %1%0Valore massimo consentito: %2","STOP","", str(this.w_NMAXPE,3,0), str(this.oParentObject.w_PPNMAXPE,3,0) )
          i_retcode = 'stop'
          return
        endif
        * --- Effettuo il controllo per vedere se � cambiato il flag PPSALCOM
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPSALCOM"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPSALCOM;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDCOM = NVL(cp_ToDate(_read_.PPSALCOM),cp_NullValue(_read_.PPSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_OLDCOM="P" AND this.oParentObject.w_PPSALCOM="S"
          this.w_MSG = "La modifica effettuata all'opzione di pianificazione per gli articoli gestiti a commessa (impostata su gestione saldi commessa) eliminer� in modo definitivo le associazioni contenute all'interno della tabella pegging di secondo livello."
          this.w_MSG = this.w_MSG+"%0Tali associazioni non potranno essere in alcun modo ripristinate"
          this.w_MSG = this.w_MSG+"%0Prima di proseguire con l'aggiornamento � consigliato effettuare il backup del database.%0%0Si desidera sospendere l'operazione ed eseguire il backup?"
          if ah_yesno(this.w_MSG)
            this.w_MSG = "Salvataggio interrotto dall'utente!%0Per eseguire il backup accedere al menu sistema->Amministrazione sistema->backup database."
            ah_ErrorMsg(this.w_MSG,64,"")
            i_retcode = 'stop'
            return
          else
            this.w_MSG = "L'utente ha scelto di proseguire senza effettuare il backup del database.%0Sar� comunque possibile effettuare il backup prima dell'elaborazione dell'MRP"
            this.w_MSG = this.w_MSG+"%0Per eseguire il backup accedere al menu sistema->Amministrazione sistema->backup database."
            ah_ErrorMsg(this.w_MSG,64,"")
          endif
        endif
        * --- Try
        local bErr_040FD7B8
        bErr_040FD7B8=bTrsErr
        this.Try_040FD7B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_040FD7B8
        * --- End
        * --- Try
        local bErr_040FFAC8
        bErr_040FFAC8=bTrsErr
        this.Try_040FFAC8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Impossibile aggiornare la tabella parametri PDA","STOP","")
        endif
        bTrsErr=bTrsErr or bErr_040FFAC8
        * --- End
      case this.Azione = "CONTROLLI"
        * --- Controlli alla CheckForm
        this.oParentObject.w_ERRORE = .F.
        if NOT EMPTY(this.oParentObject.w_PPCAUCAR)
          this.w_TCAUCOL = SPACE(5)
          this.w_TFLIMPE = " "
          this.w_TFLORDI = " "
          this.w_TFLRISE = " "
          this.w_TFLCASC = " "
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLIMPE,CMFLORDI,CMFLRISE,CMFLCASC"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUCAR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLIMPE,CMFLORDI,CMFLRISE,CMFLCASC;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_CAUCAR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_TFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_TFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_TFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_TFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case NOT EMPTY(this.w_TCAUCOL)
              ah_ErrorMsg("La causale magazzino associata al documento di carico non pu� avere una causale collegata","STOP","")
              this.oParentObject.w_ERRORE = .T.
            case NOT (this.w_TFLCASC="+" AND EMPTY(this.w_TFLORDI+this.w_TFLIMPE+this.w_TFLRISE))
              ah_ErrorMsg("Causale magazzino associata al documento di carico incongruente","STOP","")
              this.oParentObject.w_ERRORE = .T.
          endcase
        endif
        if this.oParentObject.w_ERRORE=.F. AND NOT EMPTY(this.oParentObject.w_PPCAUSCA)
          this.w_TCAUCOL = SPACE(5)
          this.w_TFLIMPE = " "
          this.w_TFLORDI = " "
          this.w_TFLRISE = " "
          this.w_TFLCASC = " "
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLIMPE,CMFLORDI,CMFLRISE,CMFLCASC"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUSCA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLIMPE,CMFLORDI,CMFLRISE,CMFLCASC;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_CAUSCA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_TFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_TFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_TFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_TFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case NOT EMPTY(this.w_TCAUCOL)
              ah_ErrorMsg("La causale magazzino associata al documento di scarico non pu� avere una causale collegata","STOP","")
              this.oParentObject.w_ERRORE = .T.
            case NOT (this.w_TFLCASC="-" AND EMPTY(this.w_TFLORDI+this.w_TFLIMPE+this.w_TFLRISE))
              ah_ErrorMsg("Causale magazzino associata al documento di scarico incongruente","STOP","")
              this.oParentObject.w_ERRORE = .T.
          endcase
        endif
        if this.oParentObject.w_ERRORE=.F. AND this.oParentObject.w_PPGESWIP<>this.oParentObject.w_OGESWIP
          * --- Verifica Esistenza ODP/ODL in Stato Lanciato o Successivi (se si il Flag Gestione Mag.Wip non puo' essere modificato)
          ah_Msg("Verifica esistenza ODP/ODL lanciati...",.T.)
          vq_exec("..\COLA\EXE\QUERY\GSCO1BKM.VQR",this,"VERIFICA")
          WAIT CLEAR
          if USED("VERIFICA")
            SELECT VERIFICA
            GO TOP
            this.oParentObject.w_ERRORE = IIF(NVL(TOTALE,0)=0, .F., .T.)
            USE
            if this.oParentObject.w_ERRORE=.T.
              ah_ErrorMsg("Esistono ODL/ODP lanciati; impossibile modificare il check gestione magazzino WIP","STOP","")
            endif
          endif
        endif
        if g_PRFA="S" and !this.oParentObject.w_ERRORE
          * --- Se � attivo il modulo verifico che il prefisso e il separatore per gli articoli di fase siano pieni e validi
          do case
            case empty(this.oParentObject.w_PPPREFAS)
              ah_ErrorMsg("Indicare il prefisso da utilizzare per la generazione dei codici di fase","STOP","" )
              this.oParentObject.w_ERRORE = .T.
            case empty(this.oParentObject.w_PPFASSEP)
              ah_ErrorMsg("Indicare il separatore da utilizzare per la generazione dei codici di fase","STOP","" )
              this.oParentObject.w_ERRORE = .T.
            otherwise
              * --- Read from PAR_PROD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_PROD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PPPREFAS,PPFASSEP"+;
                  " from "+i_cTable+" PAR_PROD where ";
                      +"PPCODICE = "+cp_ToStrODBC("PP");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PPPREFAS,PPFASSEP;
                  from (i_cTable) where;
                      PPCODICE = "PP";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PREFAS = NVL(cp_ToDate(_read_.PPPREFAS),cp_NullValue(_read_.PPPREFAS))
                this.w_FASSEP = NVL(cp_ToDate(_read_.PPFASSEP),cp_NullValue(_read_.PPFASSEP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Controllo da effettuare solamente se ho modificato codice o separatore
              if ALLTRIM(this.oParentObject.w_PPPREFAS)+ALLTRIM(this.oParentObject.w_PPFASSEP)<>ALLTRIM(this.w_PREFAS)+ALLTRIM(this.w_FASSEP)
                this.w_PREFAS = ALLTRIM(this.oParentObject.w_PPPREFAS)+ALLTRIM(this.oParentObject.w_PPFASSEP)
                this.w_PREFASE = this.w_PREFAS+"%"
                this.w_ARTEXIST = .F.
                this.w_STR = "[NOTEMPTYSTR(ART_ICOL->ARFSRIFE)]"
                * --- Select from ART_ICOL
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" ART_ICOL ";
                      +" where ARCODART like "+cp_ToStrODBC(this.w_PREFASE)+" AND '+ cp_SetSQLFunctions(w_STR, CP_DBTYPE, .null.) + '=0";
                       ,"_Curs_ART_ICOL")
                else
                  select * from (i_cTable);
                   where ARCODART like this.w_PREFASE AND "+ cp_SetSQLFunctions(w_STR, CP_DBTYPE, .null.) + "=0;
                    into cursor _Curs_ART_ICOL
                endif
                if used('_Curs_ART_ICOL')
                  select _Curs_ART_ICOL
                  locate for 1=1
                  do while not(eof())
                  this.w_ARTEXIST = .T.
                  exit
                    select _Curs_ART_ICOL
                    continue
                  enddo
                  use
                endif
              endif
          endcase
          if this.w_ARTEXIST
            this.w_MSG = ah_MsgFormat("Attenzione esistono articoli che iniziano per %1%0Indicare un prefisso oppure un separatore differente", alltrim(this.w_PREFAS))
            ah_ErrorMsg(this.w_MSG,"STOP","" )
            this.oParentObject.w_ERRORE = .T.
          endif
        endif
        if this.oParentObject.w_ERRORE=.F. AND NOT EMPTY(this.oParentObject.w_PPCAUMOU)
          this.w_TCAUCOL = SPACE(5)
          this.w_TFLIMPE = " "
          this.w_TFLORDI = " "
          this.w_TFLRISE = " "
          this.w_TFLCASC = " "
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLIMPE,CMFLORDI,CMFLRISE,CMFLCASC"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUMOU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLIMPE,CMFLORDI,CMFLRISE,CMFLCASC;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_CAUMOU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_TFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_TFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_TFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_TFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case NOT EMPTY(this.w_TCAUCOL)
              ah_ErrorMsg("La causale magazzino associata al documento di carico dei materiali di output non pu� avere una causale collegata","STOP","")
              this.oParentObject.w_ERRORE = .T.
            case NOT (this.w_TFLCASC="+" AND EMPTY(this.w_TFLORDI+this.w_TFLIMPE+this.w_TFLRISE))
              ah_ErrorMsg("Causale magazzino associata al documento di carico dei materiali di output incongruente","STOP","")
              this.oParentObject.w_ERRORE = .T.
          endcase
        endif
        if g_PRFA="S" and !this.oParentObject.w_ERRORE
          * --- Controllo la congruit� dell'U.M. del servizio scelto
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_PPSRVDFT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_PPSRVDFT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMFLTEMP"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMFLTEMP;
              from (i_cTable) where;
                  UMCODICE = this.w_UNMIS1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLTEMP1 = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLTEMP1<>"S"
            ah_ErrorMsg("Indicare un servizio con U.M. principale gestita a tempo!","STOP","" )
            this.oParentObject.w_ERRORE = .T.
          endif
        endif
      case this.Azione $ this.w_COLORI
        * --- Modifica il colore richiesto
        this.TmpN = GetColor()
        if this.TmpN<>-1
          pRit = "w_" + this.Azione
          this.oParentObject.&pRit = this.TmpN
        endif
    endcase
  endproc
  proc Try_041BB0C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_PP_CICLI = NVL(cp_ToDate(_read_.PP_CICLI),cp_NullValue(_read_.PP_CICLI))
      this.oParentObject.w_PPCAUCAR = NVL(cp_ToDate(_read_.PPCAUCAR),cp_NullValue(_read_.PPCAUCAR))
      this.oParentObject.w_PPCAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      this.oParentObject.w_PPCAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.oParentObject.w_PPCAUSCA = NVL(cp_ToDate(_read_.PPCAUSCA),cp_NullValue(_read_.PPCAUSCA))
      this.oParentObject.w_PPCAUTER = NVL(cp_ToDate(_read_.PPCAUTER),cp_NullValue(_read_.PPCAUTER))
      this.oParentObject.w_PPCAUTRA = NVL(cp_ToDate(_read_.PPCAUTRA),cp_NullValue(_read_.PPCAUTRA))
      this.oParentObject.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      this.oParentObject.w_PPCODCAU = NVL(cp_ToDate(_read_.PPCODCAU),cp_NullValue(_read_.PPCODCAU))
      this.w_PPCOLPIA = NVL(cp_ToDate(_read_.PPCOLPIA),cp_NullValue(_read_.PPCOLPIA))
      this.w_PPCOLFES = NVL(cp_ToDate(_read_.PPCOLFES),cp_NullValue(_read_.PPCOLFES))
      this.w_PPCOLMCO = NVL(cp_ToDate(_read_.PPCOLMCO),cp_NullValue(_read_.PPCOLMCO))
      this.w_PPCOLMIS = NVL(cp_ToDate(_read_.PPCOLMIS),cp_NullValue(_read_.PPCOLMIS))
      this.oParentObject.w_PPCOLMPS = NVL(cp_ToDate(_read_.PPCOLMPS),cp_NullValue(_read_.PPCOLMPS))
      this.w_PPCOLLAN = NVL(cp_ToDate(_read_.PPCOLLAN),cp_NullValue(_read_.PPCOLLAN))
      this.w_PPCOLSUM = NVL(cp_ToDate(_read_.PPCOLSUM),cp_NullValue(_read_.PPCOLSUM))
      this.oParentObject.w_PPFONMPS = NVL(cp_ToDate(_read_.PPFONMPS),cp_NullValue(_read_.PPFONMPS))
      this.oParentObject.w_PPGESWIP = NVL(cp_ToDate(_read_.PPGESWIP),cp_NullValue(_read_.PPGESWIP))
      this.oParentObject.w_PPMAGPRO = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.oParentObject.w_PPMAGSCA = NVL(cp_ToDate(_read_.PPMAGSCA),cp_NullValue(_read_.PPMAGSCA))
      this.oParentObject.w_PPMAGWIP = NVL(cp_ToDate(_read_.PPMAGWIP),cp_NullValue(_read_.PPMAGWIP))
      this.oParentObject.w_PPNMAXPE = NVL(cp_ToDate(_read_.PPNMAXPE),cp_NullValue(_read_.PPNMAXPE))
      this.oParentObject.w_PPNUMGIO = NVL(cp_ToDate(_read_.PPNUMGIO),cp_NullValue(_read_.PPNUMGIO))
      this.oParentObject.w_PPNUMMES = NVL(cp_ToDate(_read_.PPNUMMES),cp_NullValue(_read_.PPNUMMES))
      this.oParentObject.w_PPNUMSET = NVL(cp_ToDate(_read_.PPNUMSET),cp_NullValue(_read_.PPNUMSET))
      this.oParentObject.w_PPNUMTRI = NVL(cp_ToDate(_read_.PPNUMTRI),cp_NullValue(_read_.PPNUMTRI))
      this.oParentObject.w_PPBLOMRP = NVL(cp_ToDate(_read_.PPBLOMRP),cp_NullValue(_read_.PPBLOMRP))
      this.oParentObject.w_PPCALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
      this.oParentObject.w_PPIMPLOT = NVL(cp_ToDate(_read_.PPIMPLOT                    ),cp_NullValue(_read_.PPIMPLOT                    ))
      this.oParentObject.w_PP__MODA = NVL(cp_ToDate(_read_.PP__MODA),cp_NullValue(_read_.PP__MODA))
      this.oParentObject.w_CRIFOR = NVL(cp_ToDate(_read_.PPVERSUG),cp_NullValue(_read_.PPVERSUG))
      this.oParentObject.w_PPCCSODA = NVL(cp_ToDate(_read_.PPCCSODA),cp_NullValue(_read_.PPCCSODA))
      this.w_PPCOLSUG = NVL(cp_ToDate(_read_.PPCOLSUG),cp_NullValue(_read_.PPCOLSUG))
      this.w_PPCOLCON = NVL(cp_ToDate(_read_.PPCOLCON),cp_NullValue(_read_.PPCOLCON))
      this.w_PPCOLMDP = NVL(cp_ToDate(_read_.PPCOLMDP),cp_NullValue(_read_.PPCOLMDP))
      this.oParentObject.w_PPGIOSCA = NVL(cp_ToDate(_read_.PPGIOSCA),cp_NullValue(_read_.PPGIOSCA))
      this.oParentObject.w_PP___DTF = NVL(cp_ToDate(_read_.PP___DTF),cp_NullValue(_read_.PP___DTF))
      this.oParentObject.w_PPPRJMOD = NVL(cp_ToDate(_read_.PPPRJMOD),cp_NullValue(_read_.PPPRJMOD))
      this.oParentObject.w_PPORDICM = NVL(cp_ToDate(_read_.PPORDICM),cp_NullValue(_read_.PPORDICM))
      this.oParentObject.w_PPSCAMRP = NVL(cp_ToDate(_read_.PPSCAMRP),cp_NullValue(_read_.PPSCAMRP))
      this.oParentObject.w_PPCODCOM = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      this.oParentObject.w_PPMRPDIV = NVL(cp_ToDate(_read_.PPMRPDIV),cp_NullValue(_read_.PPMRPDIV))
      this.w_CDPERMRP = NVL(cp_ToDate(_read_.PPATHMRP),cp_NullValue(_read_.PPATHMRP))
      this.oParentObject.w_PPSALCOM = NVL(cp_ToDate(_read_.PPSALCOM),cp_NullValue(_read_.PPSALCOM))
      this.oParentObject.w_PPMRPLOG = NVL(cp_ToDate(_read_.PPMRPLOG),cp_NullValue(_read_.PPMRPLOG))
      this.oParentObject.w_PPDICCOM = NVL(cp_ToDate(_read_.PPDICCOM                   ),cp_NullValue(_read_.PPDICCOM                   ))
      this.oParentObject.w_PPCAUCCM = NVL(cp_ToDate(_read_.PPCAUCCM),cp_NullValue(_read_.PPCAUCCM))
      this.oParentObject.w_PPCAUSCM = NVL(cp_ToDate(_read_.PPCAUSCM),cp_NullValue(_read_.PPCAUSCM))
      this.oParentObject.w_PPCRIELA = NVL(cp_ToDate(_read_.PPCRIELA),cp_NullValue(_read_.PPCRIELA))
      this.oParentObject.w_PPPERPIA = NVL(cp_ToDate(_read_.PPPERPIA),cp_NullValue(_read_.PPPERPIA))
      this.oParentObject.w_PPPIAPUN = NVL(cp_ToDate(_read_.PPPIAPUN),cp_NullValue(_read_.PPPIAPUN))
      this.oParentObject.w_PPSRVDFT = NVL(cp_ToDate(_read_.PPSRVDFT),cp_NullValue(_read_.PPSRVDFT))
      this.oParentObject.w_PPMATINP = NVL(cp_ToDate(_read_.PPMATINP),cp_NullValue(_read_.PPMATINP))
      this.oParentObject.w_PPMATOUP = NVL(cp_ToDate(_read_.PPMATOUP),cp_NullValue(_read_.PPMATOUP))
      this.oParentObject.w_PPDSUPFA = NVL(cp_ToDate(_read_.PPDSUPFA),cp_NullValue(_read_.PPDSUPFA))
      this.oParentObject.w_PPDCODFAS = NVL(cp_ToDate(_read_.PPDCODFAS),cp_NullValue(_read_.PPDCODFAS))
      this.oParentObject.w_PPPREFAS = NVL(cp_ToDate(_read_.PPPREFAS),cp_NullValue(_read_.PPPREFAS))
      this.oParentObject.w_PPFASSEP = NVL(cp_ToDate(_read_.PPFASSEP),cp_NullValue(_read_.PPFASSEP))
      this.oParentObject.w_PPCLAFAS = NVL(cp_ToDate(_read_.PPCLAFAS),cp_NullValue(_read_.PPCLAFAS))
      this.oParentObject.w_PPUNIPRO = NVL(cp_ToDate(_read_.PPUNIPRO),cp_NullValue(_read_.PPUNIPRO))
      this.oParentObject.w_PPPSCLAV = NVL(cp_ToDate(_read_.PPPSCLAV),cp_NullValue(_read_.PPPSCLAV))
      this.oParentObject.w_PPCAURCL = NVL(cp_ToDate(_read_.PPCAURCL),cp_NullValue(_read_.PPCAURCL))
      this.oParentObject.w_PPFECLAV = NVL(cp_ToDate(_read_.PPFECLAV),cp_NullValue(_read_.PPFECLAV))
      this.oParentObject.w_PPMGARFS = NVL(cp_ToDate(_read_.PPMGARFS),cp_NullValue(_read_.PPMGARFS))
      this.oParentObject.w_PPCLAART = NVL(cp_ToDate(_read_.PPCLAART),cp_NullValue(_read_.PPCLAART))
      this.oParentObject.w_PPNOTFAS = NVL(cp_ToDate(_read_.PPNOTFAS),cp_NullValue(_read_.PPNOTFAS))
      this.oParentObject.w_PPCAUMOU = NVL(cp_ToDate(_read_.PPCAUMOU),cp_NullValue(_read_.PPCAUMOU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_PPATHMRP = ALLTRIM(this.w_CDPERMRP)
    * --- Colori di default
    this.w_PPCOLSUG = iif(this.w_PPCOLSUG=0,rgb(255,255,255),this.w_PPCOLSUG)
    this.w_PPCOLCON = iif(this.w_PPCOLCON=0,rgb(255,255,255),this.w_PPCOLCON)
    this.w_PPCOLMDP = iif(this.w_PPCOLMDP=0,rgb(255,255,255),this.w_PPCOLMDP)
    this.w_PPCOLSUM = iif(this.w_PPCOLSUM=0,rgb(255,255,255),this.w_PPCOLSUM)
    this.w_PPCOLPIA = iif(this.w_PPCOLPIA=0,rgb(255,255,255),this.w_PPCOLPIA)
    this.w_PPCOLLAN = iif(this.w_PPCOLLAN=0,rgb(255,255,255),this.w_PPCOLLAN)
    this.w_PPCOLMIS = iif(this.w_PPCOLMIS=0,rgb(255,255,255),this.w_PPCOLMIS)
    this.w_PPCOLFES = iif(this.w_PPCOLFES=0,rgb(255,255,255),this.w_PPCOLFES)
    this.w_PPCOLMCO = iif(this.w_PPCOLMCO=0,rgb(255,255,255),this.w_PPCOLMCO)
    this.oParentObject.w_SFOSUG = mod(this.w_PPCOLSUG,(rgb(255,255,255)+1))
    this.oParentObject.w_COLSUG = int(this.w_PPCOLSUG/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOCON = mod(this.w_PPCOLCON,(rgb(255,255,255)+1))
    this.oParentObject.w_COLCON = int(this.w_PPCOLCON/(rgb(255,255,255)+1))
    this.oParentObject.w_COLDPI = int(this.w_PPCOLMDP/(rgb(255,255,255)+1))
    this.oParentObject.w_SFODPI = mod(this.w_PPCOLMDP,(rgb(255,255,255)+1))
    this.oParentObject.w_COLSUM = int(this.w_PPCOLSUM/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOSUM = mod(this.w_PPCOLSUM,(rgb(255,255,255)+1))
    this.oParentObject.w_COLPIA = int(this.w_PPCOLPIA/(rgb(255,255,255)+1))
    this.oParentObject.w_COLLAN = int(this.w_PPCOLLAN/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOPIA = mod(this.w_PPCOLPIA,(rgb(255,255,255)+1))
    this.oParentObject.w_SFOLAN = mod(this.w_PPCOLLAN,(rgb(255,255,255)+1))
    this.oParentObject.w_COLMIS = int(this.w_PPCOLMIS/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOMIS = mod(this.w_PPCOLMIS,(rgb(255,255,255)+1))
    this.oParentObject.w_COLFES = this.w_PPCOLFES
    this.oParentObject.w_SFOMCO = this.w_PPCOLMCO
    this.oParentObject.w_OGESWIP = this.oParentObject.w_PPGESWIP
    this.oParentObject.w_PPGIOSCA = iif(g_MMPS="S",this.oParentObject.w_PPGIOSCA,0)
    this.oParentObject.w_PP___DTF = iif(g_MMPS="S",this.oParentObject.w_PP___DTF,0)
    return
  proc Try_040FD7B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_PROD
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PPCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("PP"),'PAR_PROD','PPCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PPCODICE',"PP")
      insert into (i_cTable) (PPCODICE &i_ccchkf. );
         values (;
           "PP";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_040FFAC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCODCOM"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCODCOM;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLDCODCOM = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not this.w_OLDCODCOM==this.oParentObject.w_PPCODCOM
      this.w_MSG = "Attenzione! A seguito della modifica della commessa di default sar� necessario eseguire la procedura di ricostruzione dei saldi di magazzino"
      ah_ErrorMsg(this.w_MSG,64,"")
    endif
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPNUMTRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMTRI),'PAR_PROD','PPNUMTRI');
      +",PPNUMSET ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMSET),'PAR_PROD','PPNUMSET');
      +",PPNUMMES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMMES),'PAR_PROD','PPNUMMES');
      +",PPNUMGIO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMGIO),'PAR_PROD','PPNUMGIO');
      +",PPNMAXPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNMAXPE),'PAR_PROD','PPNMAXPE');
      +",PPMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMAGWIP),'PAR_PROD','PPMAGWIP');
      +",PPMAGSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMAGSCA),'PAR_PROD','PPMAGSCA');
      +",PPMAGPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMAGPRO),'PAR_PROD','PPMAGPRO');
      +",PPGIOSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPGIOSCA),'PAR_PROD','PPGIOSCA');
      +",PPGESWIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPGESWIP),'PAR_PROD','PPGESWIP');
      +",PPFONMPS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPFONMPS),'PAR_PROD','PPFONMPS');
      +",PPCOLSUM ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLSUM),'PAR_PROD','PPCOLSUM');
      +",PPCOLLAN ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLLAN),'PAR_PROD','PPCOLLAN');
      +",PPCOLMPS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCOLMPS),'PAR_PROD','PPCOLMPS');
      +",PPCOLMIS ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLMIS),'PAR_PROD','PPCOLMIS');
      +",PPCOLMCO ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLMCO),'PAR_PROD','PPCOLMCO');
      +",PPCOLFES ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLFES),'PAR_PROD','PPCOLFES');
      +",PPCOLPIA ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLPIA),'PAR_PROD','PPCOLPIA');
      +",PPCODCAU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCODCAU),'PAR_PROD','PPCODCAU');
      +",PPCENCOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCENCOS),'PAR_PROD','PPCENCOS');
      +",PPCAUTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUTRA),'PAR_PROD','PPCAUTRA');
      +",PPCAUTER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUTER),'PAR_PROD','PPCAUTER');
      +",PPCAUSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUSCA),'PAR_PROD','PPCAUSCA');
      +",PPCAUORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUORD),'PAR_PROD','PPCAUORD');
      +",PPCAUIMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUIMP),'PAR_PROD','PPCAUIMP');
      +",PPCAUCAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUCAR),'PAR_PROD','PPCAUCAR');
      +",PPCARVAL ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_PROD','PPCARVAL');
      +",PP_CICLI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PP_CICLI),'PAR_PROD','PP_CICLI');
      +",PPBLOMRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPBLOMRP),'PAR_PROD','PPBLOMRP');
      +",PPCALSTA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCALSTA),'PAR_PROD','PPCALSTA');
      +",PPIMPLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPIMPLOT),'PAR_PROD','PPIMPLOT');
      +",PP__MODA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PP__MODA),'PAR_PROD','PP__MODA');
      +",PPVERSUG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRIFOR),'PAR_PROD','PPVERSUG');
      +",PPCCSODA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCCSODA),'PAR_PROD','PPCCSODA');
      +",PPCOLSUG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLSUG),'PAR_PROD','PPCOLSUG');
      +",PPCOLCON ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLCON),'PAR_PROD','PPCOLCON');
      +",PPCOLMDP ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLMDP),'PAR_PROD','PPCOLMDP');
      +",PP___DTF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PP___DTF),'PAR_PROD','PP___DTF');
      +",PPPRJMOD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPPRJMOD),'PAR_PROD','PPPRJMOD');
      +",PPSCAMRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPSCAMRP),'PAR_PROD','PPSCAMRP');
      +",PPCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCODCOM),'PAR_PROD','PPCODCOM');
      +",PPMRPDIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMRPDIV),'PAR_PROD','PPMRPDIV');
      +",PPATHMRP ="+cp_NullLink(cp_ToStrODBC(this.w_CDPERMRP),'PAR_PROD','PPATHMRP');
      +",PPSALCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPSALCOM),'PAR_PROD','PPSALCOM');
      +",PPMRPLOG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMRPLOG),'PAR_PROD','PPMRPLOG');
      +",PPDICCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPDICCOM),'PAR_PROD','PPDICCOM');
      +",PPORDICM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPORDICM),'PAR_PROD','PPORDICM');
      +",PPCAUCCM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUCCM),'PAR_PROD','PPCAUCCM');
      +",PPCAUSCM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUSCM),'PAR_PROD','PPCAUSCM');
      +",PPCRIELA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCRIELA),'PAR_PROD','PPCRIELA');
      +",PPPERPIA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPPERPIA),'PAR_PROD','PPPERPIA');
      +",PPPIAPUN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPPIAPUN),'PAR_PROD','PPPIAPUN');
      +",PPSRVDFT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPSRVDFT),'PAR_PROD','PPSRVDFT');
      +",PPMATINP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMATINP),'PAR_PROD','PPMATINP');
      +",PPMATOUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMATOUP),'PAR_PROD','PPMATOUP');
      +",PPDSUPFA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPDSUPFA),'PAR_PROD','PPDSUPFA');
      +",PPDCODFAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPDCODFAS),'PAR_PROD','PPDCODFAS');
      +",PPPREFAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPPREFAS),'PAR_PROD','PPPREFAS');
      +",PPFASSEP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPFASSEP),'PAR_PROD','PPFASSEP');
      +",PPCLAFAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCLAFAS),'PAR_PROD','PPCLAFAS');
      +",PPUNIPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPUNIPRO),'PAR_PROD','PPUNIPRO');
      +",PPPSCLAV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPPSCLAV),'PAR_PROD','PPPSCLAV');
      +",PPCAURCL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAURCL),'PAR_PROD','PPCAURCL');
      +",PPFECLAV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPFECLAV),'PAR_PROD','PPFECLAV');
      +",PPMGARFS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMGARFS),'PAR_PROD','PPMGARFS');
      +",PPCLAART ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCLAART),'PAR_PROD','PPCLAART');
      +",PPNOTFAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNOTFAS),'PAR_PROD','PPNOTFAS');
      +",PPCAUMOU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCAUMOU),'PAR_PROD','PPCAUMOU');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("PP");
             )
    else
      update (i_cTable) set;
          PPNUMTRI = this.oParentObject.w_PPNUMTRI;
          ,PPNUMSET = this.oParentObject.w_PPNUMSET;
          ,PPNUMMES = this.oParentObject.w_PPNUMMES;
          ,PPNUMGIO = this.oParentObject.w_PPNUMGIO;
          ,PPNMAXPE = this.oParentObject.w_PPNMAXPE;
          ,PPMAGWIP = this.oParentObject.w_PPMAGWIP;
          ,PPMAGSCA = this.oParentObject.w_PPMAGSCA;
          ,PPMAGPRO = this.oParentObject.w_PPMAGPRO;
          ,PPGIOSCA = this.oParentObject.w_PPGIOSCA;
          ,PPGESWIP = this.oParentObject.w_PPGESWIP;
          ,PPFONMPS = this.oParentObject.w_PPFONMPS;
          ,PPCOLSUM = this.w_PPCOLSUM;
          ,PPCOLLAN = this.w_PPCOLLAN;
          ,PPCOLMPS = this.oParentObject.w_PPCOLMPS;
          ,PPCOLMIS = this.w_PPCOLMIS;
          ,PPCOLMCO = this.w_PPCOLMCO;
          ,PPCOLFES = this.w_PPCOLFES;
          ,PPCOLPIA = this.w_PPCOLPIA;
          ,PPCODCAU = this.oParentObject.w_PPCODCAU;
          ,PPCENCOS = this.oParentObject.w_PPCENCOS;
          ,PPCAUTRA = this.oParentObject.w_PPCAUTRA;
          ,PPCAUTER = this.oParentObject.w_PPCAUTER;
          ,PPCAUSCA = this.oParentObject.w_PPCAUSCA;
          ,PPCAUORD = this.oParentObject.w_PPCAUORD;
          ,PPCAUIMP = this.oParentObject.w_PPCAUIMP;
          ,PPCAUCAR = this.oParentObject.w_PPCAUCAR;
          ,PPCARVAL = "  ";
          ,PP_CICLI = this.oParentObject.w_PP_CICLI;
          ,PPBLOMRP = this.oParentObject.w_PPBLOMRP;
          ,PPCALSTA = this.oParentObject.w_PPCALSTA;
          ,PPIMPLOT = this.oParentObject.w_PPIMPLOT;
          ,PP__MODA = this.oParentObject.w_PP__MODA;
          ,PPVERSUG = this.oParentObject.w_CRIFOR;
          ,PPCCSODA = this.oParentObject.w_PPCCSODA;
          ,PPCOLSUG = this.w_PPCOLSUG;
          ,PPCOLCON = this.w_PPCOLCON;
          ,PPCOLMDP = this.w_PPCOLMDP;
          ,PP___DTF = this.oParentObject.w_PP___DTF;
          ,PPPRJMOD = this.oParentObject.w_PPPRJMOD;
          ,PPSCAMRP = this.oParentObject.w_PPSCAMRP;
          ,PPCODCOM = this.oParentObject.w_PPCODCOM;
          ,PPMRPDIV = this.oParentObject.w_PPMRPDIV;
          ,PPATHMRP = this.w_CDPERMRP;
          ,PPSALCOM = this.oParentObject.w_PPSALCOM;
          ,PPMRPLOG = this.oParentObject.w_PPMRPLOG;
          ,PPDICCOM = this.oParentObject.w_PPDICCOM;
          ,PPORDICM = this.oParentObject.w_PPORDICM;
          ,PPCAUCCM = this.oParentObject.w_PPCAUCCM;
          ,PPCAUSCM = this.oParentObject.w_PPCAUSCM;
          ,PPCRIELA = this.oParentObject.w_PPCRIELA;
          ,PPPERPIA = this.oParentObject.w_PPPERPIA;
          ,PPPIAPUN = this.oParentObject.w_PPPIAPUN;
          ,PPSRVDFT = this.oParentObject.w_PPSRVDFT;
          ,PPMATINP = this.oParentObject.w_PPMATINP;
          ,PPMATOUP = this.oParentObject.w_PPMATOUP;
          ,PPDSUPFA = this.oParentObject.w_PPDSUPFA;
          ,PPDCODFAS = this.oParentObject.w_PPDCODFAS;
          ,PPPREFAS = this.oParentObject.w_PPPREFAS;
          ,PPFASSEP = this.oParentObject.w_PPFASSEP;
          ,PPCLAFAS = this.oParentObject.w_PPCLAFAS;
          ,PPUNIPRO = this.oParentObject.w_PPUNIPRO;
          ,PPPSCLAV = this.oParentObject.w_PPPSCLAV;
          ,PPCAURCL = this.oParentObject.w_PPCAURCL;
          ,PPFECLAV = this.oParentObject.w_PPFECLAV;
          ,PPMGARFS = this.oParentObject.w_PPMGARFS;
          ,PPCLAART = this.oParentObject.w_PPCLAART;
          ,PPNOTFAS = this.oParentObject.w_PPNOTFAS;
          ,PPCAUMOU = this.oParentObject.w_PPCAUMOU;
          &i_ccchkf. ;
       where;
          PPCODICE = "PP";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    g_MODA = this.oParentObject.w_PP__MODA
    g_ATTIVAMRPLOG = this.oParentObject.w_PPMRPLOG
    g_OLDCOM = this.oParentObject.w_PPSALCOM="P"
    g_CICLILAV = this.oParentObject.w_PP_CICLI
    if vartype(g_PPCODCOM)="U"
       public g_PPCODCOM
    endif
    g_PPCODCOM = this.oParentObject.w_PPCODCOM
    return


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CENCOST'
    this.cWorkTables[6]='TAB_CALE'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='CCF_MAST'
    this.cWorkTables[9]='UNIMIS'
    this.cWorkTables[10]='CAN_TIER'
    this.cWorkTables[11]='RIS_ORSE'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
