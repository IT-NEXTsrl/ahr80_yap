* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_btm                                                        *
*              Tracciabilita commessa                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-19                                                      *
* Last revis.: 2010-06-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ODPSEL,Scelta,w_PDANUM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_btm",oParentObject,m.w_ODPSEL,m.Scelta,m.w_PDANUM)
return(i_retval)

define class tgsco_btm as StdBatch
  * --- Local variables
  w_ODPSEL = space(15)
  Scelta = space(10)
  w_PDANUM = 0
  GestODP = .NULL.
  TMPc = space(10)
  w_OLTCOART = space(20)
  w_OLTPROVE = space(1)
  w_OLTSTATO = space(1)
  w_DPOGGMPS = space(1)
  w_OK = .f.
  w_ODPSEL = space(10)
  * --- WorkFile variables
  ODL_MAST_idx=0
  ART_PROD_idx=0
  MPS_TPER_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gestione PDA/ODP 
    *     Da GSCO_KTC
    do case
      case len(this.w_ODPSEL)=15
        * --- Legge ODP selezionato
        * --- Read from ODL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLTCOART,OLTSTATO,OLTPROVE"+;
            " from "+i_cTable+" ODL_MAST where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_ODPSEL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLTCOART,OLTSTATO,OLTPROVE;
            from (i_cTable) where;
                OLCODODL = this.w_ODPSEL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
          this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
          this.w_OLTPROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Definisce e chiama gestione
        do case
          case this.w_OLTPROVE="L"
            * --- OCL
            this.GestODP = GSCO_AOP(this.oParentObject, "Z")
          case this.w_OLTPROVE="I"
            * --- ODL
            this.GestODP = GSCO_AOP(this.oParentObject, "L")
          otherwise
            * --- ODA
            this.GestODP = GSCO_AOP(this.oParentObject, "E")
        endcase
        this.GestODP.w_OLCODODL = this.w_ODPSEL
        this.TMPc = "OLCODODL="+cp_ToStrODBC(this.w_ODPSEL)
        this.w_OK = TRUE
      case len(this.w_ODPSEL)=10 and type("this.w_PDANUM")="N" and this.w_PDANUM>0
        * --- Gestione PDA
        this.GestODP = GSAC_APD()
        this.GestODP.w_PDSERIAL = this.w_ODPSEL
        this.GestODP.w_PDROWNUM = this.w_PDANUM
        this.TMPc = "PDSERIAL="+cp_ToStrODBC(this.w_ODPSEL)+" and PDROWNUM="+cp_ToStrODBC(this.w_PDANUM)
        this.w_OK = TRUE
    endcase
    do case
      case this.w_OK AND (this.Scelta="VISUALIZZA_ODP" or this.Scelta="MODIFICA_ODP" or this.Scelta="ELIMINA_ODP")
        * --- Carica record e lascia in interrograzione ...
        this.GestODP.QueryKeySet(this.TmpC,"")     
        this.GestODP.LoadRecWarn()     
        do case
          case this.Scelta = "MODIFICA_ODP"
            * --- Modifica record
            this.GestODP.ECPEdit()     
          case this.Scelta = "ELIMINA_ODP"
            * --- Cancella record ...
            this.GestODP.ECPDelete()     
        endcase
    endcase
  endproc


  proc Init(oParentObject,w_ODPSEL,Scelta,w_PDANUM)
    this.w_ODPSEL=w_ODPSEL
    this.Scelta=Scelta
    this.w_PDANUM=w_PDANUM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='ART_PROD'
    this.cWorkTables[3]='MPS_TPER'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='PAR_RIOR'
    this.cWorkTables[7]='CONTI'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ODPSEL,Scelta,w_PDANUM"
endproc
