* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bss                                                        *
*              Stampa scaletta di produzione                                   *
*                                                                              *
*      Author: Marco Pasquali                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-01                                                      *
* Last revis.: 2016-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bss",oParentObject,m.pAzione)
return(i_retval)

define class tgsco_bss as StdBatch
  * --- Local variables
  pAzione = space(15)
  RS = space(10)
  w_SCTIPSCA = space(3)
  w_SCTPQTOR = space(1)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_APPO = 0
  w_DATFIL = ctod("  /  /  ")
  w_COPERRIC = 0
  ElabCursor = space(10)
  w_PERRIC = space(1)
  w_LenLvl = 0
  w_maxlvl = 0
  QTC = 0
  QTN = 0
  QTR = 0
  w_CODDIS = space(20)
  w_CODART = space(20)
  w_DISPAD = space(20)
  w_ARTPAD = space(20)
  w_CODRIS = space(15)
  w_ORDINE = space(1)
  w_NUMLEV = 0
  w_NUMERO = 0
  w_CODCOM = space(20)
  w_DESCOM = space(40)
  w_ARTCOM = space(20)
  w_DESPAD = space(15)
  w_UNIMIS = space(3)
  w_QTANET = 0
  w_QTANE1 = 0
  w_QTALOR = 0
  w_QTALO1 = 0
  w_PERSCA = 0
  w_RECSCA = 0
  w_PERSFR = 0
  w_RECSFR = 0
  w_FLVARI = space(1)
  w_FLESPL = space(1)
  w_FLCOMP = space(1)
  w_cosuni = 0
  w_coscic = 0
  w_PROPRE = space(1)
  w_QTAFIN = 0
  w_QTAFIN1 = 0
  w_PERREC = 0
  w_QTAREC = 0
  w_QTARE1 = 0
  w_QTAPARZ = 0
  w_QTAPAR1 = 0
  i_cNum = 0
  w_LVLKEY = space(200)
  w_OLDKEY = space(200)
  w_APPOLW = space(200)
  w_OLDLEVEL = 0
  w_NUMLEW = 0
  w_OLDQTAFIN = 0
  w_OLDQTAFIN1 = 0
  TmpD = ctod("  /  /  ")
  BmpCursor = space(10)
  cODLField = space(254)
  cOtherField = space(254)
  cBaseField = space(254)
  DataRiferimento = ctod("  /  /  ")
  TmpN = 0
  FlgPreferenziale = space(1)
  DistManutenzione = space(1)
  DistProvvisorie = space(1)
  OutCursor = space(15)
  InCursor = space(10)
  w_SCCOLPER = space(50)
  w_SCTIPPIA = space(1)
  w_SCDATFIN = ctod("  /  /  ")
  w_SCDATSCA = ctod("  /  /  ")
  w_OLDSERIAL = space(15)
  w_SCSERIAL = space(15)
  w_SCSERIAL1 = space(15)
  w_ARCLACRI = space(5)
  w_ARCLACRI1 = space(5)
  w_ARCLACRI2 = space(5)
  w_SCCODICE = space(20)
  w_SCCODODL = space(15)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_SCNUMSCA = 0
  w_SERSCA = space(15)
  w_ORD = 0
  w_PAG = 0
  w_RIEPI = space(1)
  w_EXPODL = space(15)
  w_MESS = space(200)
  i = 0
  y = 0
  l_tCol = 0
  l_nCol = 0
  w_OLDCLASS = space(5)
  w_CLASS = space(5)
  w_NREC1 = 0
  w_NREC2 = 0
  w_NREC3 = 0
  w_NTPGMAST = 0
  w_NRECLS = 0
  w_PGMAST = 0
  w_NREC = 0
  w_DBCODICE = space(20)
  w_DISPRO = space(1)
  w_DISMAN = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_STADIG = space(1)
  w_FLDIGE = space(1)
  w_FLGPRE = space(1)
  w_QUANTI = 0
  w_TIPART = space(2)
  CONTADB = 0
  NUMDB = 0
  w_TIPGES = space(1)
  w_CODGES = space(2)
  TipoGestione = space(1)
  OutCursor = space(10)
  * --- WorkFile variables
  PAR_PROD_idx=0
  KEY_ARTI_idx=0
  RIS_ORSE_idx=0
  TMPRIS_ORSE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Stampa scaletta di Produzione (da GSCI_MSP)
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Dichiara le varibili private usate da gsdb_btv per l'esplosione delle distinte e dalla stampa
    declare tit(1,1)
    this.w_SCNUMSCA = 0
    this.w_SCSERIAL = this.oParentObject.w_SERIAL
    this.w_SCSERIAL1 = this.oParentObject.w_SERIAL1
    this.w_ARCLACRI = this.oParentObject.w_CLACRI
    this.w_ARCLACRI1 = this.oParentObject.w_CLACRI1
    this.w_ARCLACRI2 = this.oParentObject.w_CLACRI2
    this.w_RIEPI = this.oParentObject.w_FLRIEPI
    L_CODRIS = this.oParentObject.w_SCCODRIS 
 L_CODRIS1 = this.oParentObject.w_SCCODRI1 
 L_SERIAL = this.w_SCSERIAL 
 L_SERIAL1 = this.w_SCSERIAL1 
 L_FLRIEPI = this.oParentObject.w_FLRIEPI 
 L_CLACRI1 = this.w_ARCLACRI 
 L_CLACRI2 = this.w_ARCLACRI1 
 L_CLACRI3 = this.w_ARCLACRI2
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiunta Count su Scadett a volte non fa il refresh il cursore pwerde l'ultima riga
    SELECT Scadett 
 COUNT TO nRec
    Select * from Scadett; 
 union all; 
 select PAG as PAG, MAX(ORDINE) AS ORDINE, "xxxRiepixxx" as DESAUT,; 
 MAX(SCNUMSCA) as SCNUMSCA,MAX(SCTIPPIA) AS SCTIPPIA,; 
 SCSERIAL AS SCSERIAL, MAX(CPROWORD) AS CPROWORD, MAX(CPROWNUM) AS CPROWNUM,; 
 SPACE(20) AS SCCODRIS, MAX(SCDATINI) AS SCDATINI,SPACE(15) AS SCCODODL,; 
 SPACE(20) AS SCCODICE, SPACE(20) AS ARDESART, space(3) as OLTUNMIS, ; 
 0 AS SCQTARIC, 0 AS SCQTAC01, 0 AS SCQTAC02, 0 AS SCQTAC03, 0 AS SCQTAC04, 0 AS SCQTAC05,; 
 0 AS SCQTAC06, 0 AS SCQTAC07, 0 AS SCQTAC08, 0 AS SCQTAC09, 0 AS SCQTAC10,; 
 0 AS SCQTAC11, 0 AS SCQTAC12, 0 AS SCQTAC13, 0 AS SCQTAC14, 0 AS SCQTAP01,; 
 0 AS SCQTAP02, 0 AS SCQTAP03, 0 AS SCQTAP04, 0 AS SCQTAP05, 0 AS SCQTAP06,; 
 0 AS SCQTAP07, 0 AS SCQTAP08, 0 AS SCQTAP09, 0 AS SCQTAP10, 0 AS SCQTAP11,; 
 0 AS SCQTAP12, 0 AS SCQTAP13, 0 AS SCQTAP14, 0 AS SCQTARES,; 
 SPACE(20) AS MATCRI_1, SPACE(5) AS CLACRI_1, SPACE(20) AS MATCRI_2,; 
 SPACE(5) AS CLACRI_2, SPACE(20) AS MATCRI_3, ; 
 SPACE(5) AS CLACRI_3,; 
 MTCODICE AS MTCODICE, MAX(CADESCRI) AS CADESCRI, MAX(MTUNMIS) as MTUNMIS, ; 
 CLCODICE AS CLCODICE, MAX(CLDESCRI) AS CLDESCRI,; 
 SUM(MTQTAP01) as MTQTAP01, SUM(MTQTAP02) as MTQTAP02,; 
 SUM(MTQTAP03) as MTQTAP03, SUM(MTQTAP04) as MTQTAP04,; 
 SUM(MTQTAP05) as MTQTAP05, SUM(MTQTAP06) as MTQTAP06,; 
 SUM(MTQTAP07) as MTQTAP07, SUM(MTQTAP08) as MTQTAP08,; 
 SUM(MTQTAP09) as MTQTAP09, SUM(MTQTAP10) as MTQTAP10,; 
 SUM(MTQTAP11) as MTQTAP11, SUM(MTQTAP12) as MTQTAP12,; 
 SUM(MTQTAP13) as MTQTAP13, SUM(MTQTAP14) as MTQTAP14, "R" as RIGA; 
 FROM ScaRiepi GROUP BY SCSERIAL, PAG, CLCODICE, MTCODICE; 
 INTO CURSOR _sssp_
    use in Select("ScaTmp")
    use in Select("ScaRiepi")
    use in Select("ScaDett")
    use in Select("MatRich")
    use in Select("Matcri")
    SELECT * FROM _sssp_ into cursor __tmp__ ORDER BY SCSERIAL, PAG, RIGA, CPROWORD, ORDINE
    do case
      case this.oParentObject.w_TIPOSTA=="SA3"
        CP_CHPRN("..\COLA\EXE\QUERY\GSCO2BSS")
      case this.oParentObject.w_TIPOSTA=="GA4"
        CP_CHPRN("..\COLA\EXE\QUERY\GSCO1BSS")
      case this.oParentObject.w_TIPOSTA=="SA4"
        CP_CHPRN("..\COLA\EXE\QUERY\GSCO_BSS")
    endcase
    * --- rilascio array
    release tit
    if used ("_sssp_")
      use in _sssp_
    endif
    if used ("__tmp__")
      use in __tmp__
    endif
    if used ("Appogg")
      use in Appogg
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio Video ...
    this.InCursor = sys(2015)
    cOC = this.InCursor
    * --- Cursore di input
    this.OutCursor = "TES_PLOS"
    this.ElabCursor = sys(2015)
    * --- Variabili locali
    * --- DEVE RESTARE VUOTA (esplode le distinte in base alla data di inizio lavorazione)
    * --- Crea cursore con i BitMap parametrici
    this.BmpCursor = sys(2015)
    if Used( this.BmpCursor )
      USE IN SELECT( this.BmpCursor )
    endif
    CREATE CURSOR ( this.BmpCursor ) (ARTIPART C(2), BMPNAME C(200))
    if used( this.BmpCursor )
      Insert into ( this.BmpCursor ) (ARTIPART, BMPNAME) VALUES ("PF" , "BMP\Tree1.ico")
      Insert into ( this.BmpCursor ) (ARTIPART, BMPNAME) VALUES ("SE" , "BMP\Tree4.ico")
      Insert into ( this.BmpCursor ) (ARTIPART, BMPNAME) VALUES ("MP" , "BMP\Tree2.ico")
      Insert into ( this.BmpCursor ) (ARTIPART, BMPNAME) VALUES ("PH" , "BMP\Tree5.ico")
    endif
    * --- Crea cursore con ODL da Esplodere
    vq_exec("..\COLA\exe\query\GSCOKBSS", this, "CodiciRicerca")
    if g_PRFA="S" AND g_CICLILAV="S" AND Upper(This.oParentObject.Class )="TGSCI_SSC"
      vq_exec("..\PRFA\exe\query\GSCIOBSS", this, "ListaODL")
    else
      vq_exec("..\COLA\exe\query\GSCOOBSS", this, "ListaODL")
    endif
    this.w_CODGES = "PP"
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCODICE,PPFLDIGE"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC(this.w_CODGES);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCODICE,PPFLDIGE;
        from (i_cTable) where;
            PPCODICE = this.w_CODGES;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODGES = NVL(cp_ToDate(_read_.PPCODICE),cp_NullValue(_read_.PPCODICE))
      this.w_FLDIGE = NVL(cp_ToDate(_read_.PPFLDIGE),cp_NullValue(_read_.PPFLDIGE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Preparo il Cursore con lle selezioni della scaletta
    if g_PRFA="S" AND g_CICLILAV="S" AND Upper(This.oParentObject.Class )="TGSCI_SSC"
      * --- Create temporary table TMPRIS_ORSE
      i_nIdx=cp_AddTableDef('TMPRIS_ORSE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_FGMSBBKSCN[1]
      indexes_FGMSBBKSCN[1]='RL__TIPO,RLCODICE'
      i_nConn=i_TableProp[this.RIS_ORSE_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"RL__TIPO, RLCODICE "," from "+i_cTable;
            +" where 1=2";
            ,.f.,@indexes_FGMSBBKSCN)
      this.TMPRIS_ORSE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if g_PRFA="S"
        if VarType(This.oParentObject.w_ZoomRiso)="O"
          * --- Nome cursore collegato allo zoom risorse
          this.RS = This.oParentObject.w_ZoomRiso.cCursor
          if !Empty(this.RS) and USED(this.RS)
            SELECT RL__TIPO, RLCODICE FROM (this.RS) into cursor __ResTmp__ where xchk=1
            SELECT __ResTmp__
            SCAN
            * --- Insert into TMPRIS_ORSE
            i_nConn=i_TableProp[this.TMPRIS_ORSE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPRIS_ORSE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPRIS_ORSE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"RL__TIPO"+",RLCODICE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(__ResTmp__.RL__TIPO),'TMPRIS_ORSE','RL__TIPO');
              +","+cp_NullLink(cp_ToStrODBC(__ResTmp__.RLCODICE),'TMPRIS_ORSE','RLCODICE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'RL__TIPO',__ResTmp__.RL__TIPO,'RLCODICE',__ResTmp__.RLCODICE)
              insert into (i_cTable) (RL__TIPO,RLCODICE &i_ccchkf. );
                 values (;
                   __ResTmp__.RL__TIPO;
                   ,__ResTmp__.RLCODICE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            SELECT __ResTmp__
            ENDSCAN
          endif
          USE IN SELECT("__ResTmp__")
        endif
      endif
      VQ_EXEC("..\PRFA\EXE\QUERY\GSCISBSS", this, "ScaTmp")
      * --- Drop temporary table TMPRIS_ORSE
      i_nIdx=cp_GetTableDefIdx('TMPRIS_ORSE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPRIS_ORSE')
      endif
    else
      VQ_EXEC("..\COLA\EXE\QUERY\GSCOSBSS", this, "ScaTmp")
    endif
    * --- Preparo il Cursore con la struttura della stampa
    VQ_EXEC("..\COLA\EXE\QUERY\GSCO3BSS", this, "ScaRiepi")
    =wrcursor("ScaRiepi")
    zap
    VQ_EXEC("..\COLA\EXE\QUERY\GSCO3BSS", this, "ScaDett")
    =wrcursor("ScaDett")
    zap
    * --- Preparo il Cursore con i soli materiali critici
    VQ_EXEC("..\COLA\EXE\QUERY\GSCO1BSS", this, "MatCri")
    * --- Preparo il Cursore con le sole Classi Criticit�
    VQ_EXEC("..\COLA\EXE\QUERY\GSCO2BSS", this, "CursClaCri")
    if reccount("CursClaCri")=0
      SELECT CursClaCri
      APPEND BLANK
    endif
    CREATE CURSOR ClaMast (PAG N(5), NREC N(5), ORDINE N(5), SCSERIAL C(15), CPROWNUM N(5),; 
 CLACRI_1 C(5), CLACRI_2 C(5), CLACRI_3 C(5))
    =wrcursor("ClaMast")
    this.w_NREC = 1
    select CursClaCri
    GO TOP
    this.w_NTPGMAST = reccount()
    this.w_NTPGMAST = iif(this.w_NTPGMAST = 0, 1, iif((this.w_NTPGMAST / 3)=0, 1, (this.w_NTPGMAST / 3))) + 1
    this.w_NTPGMAST = int(this.w_NTPGMAST)
    do while not eof("CursClaCri")
      for this.w_PGMAST = 1 TO this.w_NTPGMAST
      select ClaMast
      if not eof("CursClaCri")
        append blank
      else
        this.i = 3
        exit
      endif
      for this.i = 1 to 3
      l_sCol = alltrim(str(this.i))
      select CursClaCri
      Scatter memvar
      select ClaMast
      replace PAG WITH this.w_PGMAST
      replace NREC WITH this.w_NREC
      replace ORDINE WITH 1
      replace SCSERIAL WITH SPACE(15) && this.w_SCSERIAL
      replace CPROWNUM WITH 0 && this.w_CPROWNUM
      replace clacri_&l_sCol with nvl(m.mtcodice, space(5))
      select CursClaCri
      if not eof("CursClaCri")
        skip +1
      endif
      next
      this.w_NREC = this.w_NREC + 1
      next
    enddo
    if used("ScaTmp")
      if used ("tmpdbMatcri")
        use in tmpdbMatcri
      endif
      if used (this.Outcursor)
        use in (this.outcursor)
      endif
      if used ("ClaDett")
        use in ClaDett
      endif
      this.w_OLDCLASS = ""
      this.w_CLASS = ""
      select ScaTmp
      GO TOP
      SCAN FOR NOT EMPTY(SCSERIAL)
      this.w_SCDATSCA = SCDATINI
      this.w_SCTIPPIA = NVL(SCTIPPIA, SPACE(1))
      this.w_SCSERIAL = NVL(SCSERIAL, SPACE(15))
      this.w_SCCODODL = NVL(SCCODODL, SPACE(15))
      this.w_SCCODICE = NVL(SCCODICE, SPACE(20))
      this.w_CPROWNUM = NVL(CPROWNUM, 0)
      this.w_SCTIPSCA = NVL(SCTIPSCA, SPACE(3))
      this.w_SCTPQTOR = NVL(SCTPQTOR, SPACE(1))
      if NOT EMPTY(this.w_SCSERIAL) AND this.w_CPROWNUM >0 AND NOT EMPTY(this.w_SCCODICE)
        if this.w_SCSERIAL <> this.w_OLDSERIAL
          this.w_SCNUMSCA = this.w_SCNUMSCA + 1
          * --- Cursore contenente i Periodi da Stampare
          dimension tit(this.w_SCNUMSCA, 16)
          do case
            case this.w_SCTIPPIA = "G"
              this.w_SCDATFIN = this.w_SCDATSCA+13
            case this.w_SCTIPPIA = "S"
              this.w_SCDATFIN = this.w_SCDATSCA + (14*7)
            case this.w_SCTIPPIA = "M"
              this.w_SCDATFIN = GOMONTH(this.w_SCDATSCA , 13)
            case this.w_SCTIPPIA = "A"
              this.w_SCDATFIN = Date(YEAR(this.w_SCDATSCA)+13,12,31)
          endcase
          for this.y = 0 to 13
          do case
            case this.w_SCTIPPIA = "G"
              this.w_SCCOLPER = iif(empty(this.w_SCDATSCA) or UPPER(this.w_SCTIPPIA) <> "G", "", left(g_GIORNO(DOW((TTOD(this.w_SCDATSCA) +this.y))),1) + " " + left(dtoc(TTOD(this.w_SCDATSCA)+this.y),5))
            case this.w_SCTIPPIA = "S"
              this.w_SCCOLPER = iif(empty(this.w_SCDATSCA) or UPPER(this.w_SCTIPPIA) <> "S", "", "S" + padl(alltrim(str(WEEK(TTOD(this.w_SCDATSCA) + 7 * this.y,3,2)+1)),2,"0") + "-" + RIGHT(ALLTRIM(str(YEAR(TTOD(this.w_SCDATSCA) + 7 * 2))),2))
            case this.w_SCTIPPIA = "M"
              this.w_SCCOLPER = iif(empty(this.w_SCDATSCA) or UPPER(this.w_SCTIPPIA) <> "M", "", "M" + padl(alltrim(str(MONTH(GOMONTH(TTOD(this.w_SCDATSCA), this.y)))),2,"0") + "-" + right(alltrim(str(YEAR(GOMONTH(TTOD(this.w_SCDATSCA), this.y)))),2))
            case this.w_SCTIPPIA = "A"
              this.w_SCCOLPER = iif(empty(this.w_SCDATSCA) or UPPER(this.w_SCTIPPIA) <> "A", "", "A " + alltrim(str(YEAR(TTOD(this.w_SCDATSCA))+ this.y)))
          endcase
          STORE alltrim(this.w_SCCOLPER) TO tit(this.w_SCNUMSCA, this.y+1)
          next
          STORE this.w_SCDATSCA TO tit(this.w_SCNUMSCA, 15)
          STORE this.w_SCDATFIN TO tit(this.w_SCNUMSCA, 16)
        endif
        * --- Esplodo Le distinte
        if (this.oParentObject.w_TIPOSTA="SA3") or (this.oParentObject.w_TIPOSTA="SA4" and this.w_RIEPI="S")
          if Not(g_PRFA="S" AND g_CICLILAV="S" AND Upper(This.oParentObject.Class )="TGSCI_SSC")
            select ScaTmp
            this.w_EXPODL = SCCODODL
            Select distinct * from ListaODL where olcododl=this.w_EXPODL into cursor ELABREC
            SELECT("ELABREC")
            this.w_MESS = "Elaborazione Scaletta %1%0 Esplosione Distinta %2 in corso"
            ah_msg (this.w_MESS,.t.,.f.,.f.,alltrim(this.w_SCSERIAL),alltrim(this.w_SCCODICE))
            this.w_DBCODINI = space(20)
            this.w_DBCODFIN = space(20)
            this.w_VERIFICA = "S C"
            this.w_FILSTAT = " "
            this.w_DATFIL = i_DATSYS
            gsar_bde(this,"P")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            DIMENSION QTC[99]
            DIMENSION QTN[99]
            DIMENSION QTR[99]
            this.w_PERRIC = "N"
            this.w_LenLvl = 4
            if used("TES_PLOS")
              * --- Crea Cursore di Appoggio
              * --- Cursore di Elaborazione/Stampa
              CREATE CURSOR (this.ElabCursor) ;
              (CODART C(20), DISPAD C(20), ARTPAD C(20), ORDINE C(1), CODRES C(20), NUMERO N(5,0), CODCOM C(20), NUMLEV C(4), DESCOM C(40), UNIMIS C(3), ;
              QTANET N(15,6), QTANET1 N(15,6), PERSCA N(6,2), PERSFR N(6,2), QTALOR N(15,6), QTALO1 N(15,6), QTAFIN N(15,6), QTAFIN1 N(15,6), ;
              RECSCA N(15,6), RECSFR N(15,6), PERRIC N(15,6), ;
              FLVARI C(1), DESPAD C(40), FLESPL C(1), CODDIS C(20), ARTCOM C(20), PROPRE C(1), PERCOS N(6,2), CODRIS C(15), FLCOMP C(1),;
              FLSETU C(1), LVLKEY C(200), COSTO N(14,8), COSREC N(14,8), COSUNISE N(14,8), COSUNIST N(14,8), COSUNIME N(14,8), COSUNIUL N(14,8), ;
              COSCIC N(14,8), COSSETU N(14,8), LIVELLO N(4), CSTANDR N(14,8), CMEDIO N(14,8), CULTIM N(14,8), EVASOCL C(1), COSTOCL N(14,8), RAGGRUPP N(2))
              SELECT TES_PLOS
              GO TOP
              this.w_CODART = CODART
              this.w_NUMERO = 0
              * --- Inizializzo le variabili per il calcolo del LVLKY
              this.w_LVLKEY = "000"
              this.w_OLDKEY = "000"
              this.w_APPOLW = "000"
              this.w_OLDLEVEL = 0
              * --- Cicla sulle Distinte Base Esplose
              SCAN FOR NOT EMPTY(DISPAD)
              this.w_CODART = CODART
              this.w_DISPAD = DISPAD
              this.w_CODRIS = CODRIS
              * --- ORDINE: Se '0' = Riga Componente ; '1' = Riga Ciclo Semplificato
              this.w_ORDINE = "0"
              this.w_NUMLEV = NUMLEV
              this.w_DESCOM = DESCOM
              * --- Codice Articolo Associato al Componente
              this.w_ARTCOM = ARTCOM
              this.w_PROPRE = PROPRE
              this.w_UNIMIS = UNMIS0
              this.w_QTANET = QTANET
              this.w_QTANE1 = IIF(QTANE1=0, QTANET, QTANE1)
              this.w_QTALOR = QTAMOV
              this.w_QTALO1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
              this.w_QTAFIN = QTAREC
              this.w_QTAFIN1 = IIF(QTARE1=0, QTAREC, QTARE1)
              this.w_PERSCA = PERSCA
              this.w_RECSCA = RECSCA
              this.w_PERSFR = PERSFR
              this.w_RECSFR = RECSFR
              this.w_COPERRIC = IIF(this.w_PERRIC="S", PERRIC, 0)
              this.w_FLVARI = FLVARI
              this.w_FLESPL = FLESPL
              this.w_ARTPAD = NVL(ARTPAD, SPACE(20))
              * --- % Recupero Scarto e Sfrido (Attenzione al calcolo del Costo Totale: i valori sono normalmente Negativi)
              if this.w_RECSFR<>0 AND this.w_PERSFR<>0 or this.w_RECSCA<>0 AND this.w_PERSCA<>0
                this.w_PERREC = cp_ROUND((this.w_RECSCA+this.w_RECSFR)/100, 6)
                this.w_PERREC = (this.w_RECSCA+this.w_RECSFR)/100
              else
                this.w_PERREC = 0
              endif
              * --- Testa se l'Articolo Componente e' un Prodotto Finito (S) o un Semilavorato
              this.w_FLCOMP = IIF(VAL(this.w_NUMLEV)=0 OR this.w_FLESPL="S", " ", "S")
              if VAL(this.w_NUMLEV)=0
                * --- Inizia una nuova Distinta Base, inizia da qui a calcolare le qta dei Componenti 
                this.w_CODCOM = ALLTRIM(DISPAD)
                this.w_ARTCOM = ALLTRIM(CODART)
                this.w_CODDIS = ALLTRIM(DISPAD)
                this.w_NUMERO = 0
                if .f.
                  this.w_QTANET = this.w_QUANTI
                  this.w_QTANE1 = this.w_QUANTI
                  this.w_QTALOR = this.w_QUANTI
                  this.w_QTALO1 = this.w_QUANTI
                  this.w_QTAFIN = this.w_QUANTI
                  this.w_QTAFIN1 = this.w_QUANTI
                endif
                FOR L_i = 1 TO 99
                QTC[L_i] = 0
                QTN[L_i] = 0
                QTR[L_i] = 0
                ENDFOR
              else
                * --- Componente: Prodotto Finito o Semilavorato
                this.w_NUMERO = this.w_NUMERO + 1
                this.w_CODCOM = ALLTRIM(STR(VAL(this.w_NUMLEV)))+" " + ALLTRIM(CODCOM)
                this.w_CODCOM = REPL(". ", IIF(VAL(this.w_NUMLEV)<10, VAL(this.w_NUMLEV)-1, 9))+this.w_CODCOM
                this.w_CODCOM = ALLTRIM(CODCOM)
                this.w_CODDIS = NVL(CODDIS, SPACE(20))
                * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
                QTN[VAL(this.w_NUMLEV)] = this.w_QTANE1
                if .F.
                  this.w_QTANET = this.w_QUANTI
                  this.w_QTANE1 = this.w_QUANTI
                endif
                QTC[VAL(this.w_NUMLEV)] = this.w_QTALO1
                if .F.
                  this.w_QTALOR = this.w_QUANTI
                  this.w_QTALO1 = this.w_QUANTI
                endif
                QTR[VAL(this.w_NUMLEV)] = this.w_QTAFIN1
                if .F.
                  this.w_QTAFIN = this.w_QUANTI
                  this.w_QTAFIN1 = this.w_QUANTI
                endif
                FOR L_i = 1 TO VAL(this.w_NUMLEV)
                * --- Solo la riga del componente viene moltiplicato per l'effettivo valore, gli altri calcoli vanno riportati alla 1^UM
                this.w_QTANET = this.w_QTANET * IIF(L_i=VAL(this.w_NUMLEV), QTANET, QTN[L_i])
                this.w_QTANE1 = this.w_QTANE1 * QTN[L_i]
                this.w_QTALOR = this.w_QTALOR * IIF(L_i=VAL(this.w_NUMLEV), QTAMOV, QTC[L_i])
                this.w_QTALO1 = this.w_QTALO1 * QTC[L_i]
                this.w_QTAFIN = this.w_QTAFIN * IIF(L_i=VAL(this.w_NUMLEV), QTAREC, QTR[L_i])
                this.w_QTAFIN1 = this.w_QTAFIN1 * QTR[L_i]
                ENDFOR
              endif
              * --- Mentre Creo il cursore di elaborazione in base al livello che
              *     st� elaborando costruisco il lvlkey
              this.w_NUMLEW = NVL(VAL(NUMLEV), 0)
              if this.w_NUMLEW = 0
                this.w_LVLKEY = cp_bintoc(this.i_cNum)
                this.w_OLDLEVEL = this.w_NUMLEW
                this.i_cNum = this.i_cNum + 1
              else
                this.w_APPOLW = VAL(SUBSTR(this.w_LVLKEY, this.w_NUMLEW * 4 + 1, 3))+1
                if this.w_NUMLEW > this.w_OLDLEVEL
                  this.w_LVLKEY = rTrim(this.w_LVLKEY) + "." + str(this.w_APPOLW,3,0)
                else
                  this.w_LVLKEY = left(this.w_LVLKEY, this.w_NUMLEW * 4) + str(this.w_APPOLW,3,0)
                endif
              endif
              this.w_OLDLEVEL = this.w_NUMLEW
              * --- Scrivo il Temporaneo
              INSERT INTO (this.ElabCursor) ;
              (CODART, DISPAD, ARTPAD, ORDINE, NUMERO, CODCOM, NUMLEV, DESCOM, UNIMIS, QTANET, QTANET1, PERSCA, PERSFR, QTALOR, QTALO1, QTAFIN, QTAFIN1, ;
              RECSCA, RECSFR, PERRIC, FLVARI, FLESPL, CODDIS, ARTCOM, PROPRE, PERCOS, CODRIS, FLCOMP, FLSETU, ;
              LVLKEY, COSTO, COSREC, COSUNISE, COSUNIST, COSUNIME, COSUNIUL, COSCIC, COSSETU, ;
              LIVELLO, CSTANDR, CMEDIO, CULTIM, EVASOCL, COSTOCL, RAGGRUPP) ;
              VALUES (this.w_CODART, this.w_DISPAD, this.w_ARTPAD, this.w_ORDINE, this.w_NUMERO, this.w_CODCOM, this.w_NUMLEV, this.w_DESCOM, this.w_UNIMIS, ;
              this.w_QTANET,this.w_QTANE1, this.w_PERSCA, this.w_PERSFR, this.w_QTALOR, this.w_QTALO1, this.w_QTAFIN, this.w_QTAFIN1, this.w_RECSCA, this.w_RECSFR, this.w_COPERRIC,;
              this.w_FLVARI, this.w_FLESPL, this.w_CODDIS, this.w_ARTCOM, this.w_PROPRE, 0, this.w_CODRIS, this.w_FLCOMP, " ", this.w_LVLKEY, 0,0,0,0,0,0,0,0, this.w_NUMLEW, 0, 0, 0, "N", 0, 1)
              SELECT TES_PLOS
              ENDSCAN
              * --- Chiude il Cursore generato dalla Procedura di Esplosione
              select TES_PLOS
              use
              this.w_PROPRE = Space(1)
              select (this.ElabCursor)
              GO TOP
              =wrcursor(this.ElabCursor) 
 INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE" 
 UPDATE (this.ElabCursor) SET livello = (1+len(rtrim(lvlkey)))/ this.w_LenLvl -1
              SELECT max(livello) AS massimo FROM (this.ElabCursor) INTO CURSOR maxlevel
              this.w_maxlvl = maxlevel.massimo
              USE IN maxlevel
              select (this.ElabCursor)
            endif
            this.OutCursor = SYS(2015)
            use in select(this.OutCursor)
            if used (this.ElabCursor)
              select a.cacodice, a.cadesart, b.* from (this.ElabCursor) b inner join CodiciRicerca a on a.cacodart==b.artcom into cursor (this.OutCursor) readwrite
              cOC = this.OutCursor
              * --- Esclude il padre supremo e gli articoli PS, DC
              *     Tolto il PH perxh� lo vogliamo Vedere
              SELECT cacodice as COMPONENTI, cadesart as CADESART, Matcri.arclacri AS CLACRI,; 
 MatCri.cldescri as CLDESCRI, left(lvlkey,3) as raggr, max(unimis) as counimis,; 
 sum(QtaNet) as somma, sum(QtaLor) as sommaL FROM (this.OutCursor); 
 inner join Matcri on (&cOC..cacodice=matcri.mtcodice) where at(".",LVLKEY) > 0; 
 GROUP BY raggr, COMPONENTI; 
 INTO CURSOR tmpdbMatcri
              select &cOC..cacodice as cacodice, tmpdbMatcri.* FROM (this.OutCursor); 
 right join tmpdbMatcri on (left(&cOC..LVLKEY,3)=tmpdbMatcri.raggr) where at(".", &cOC..lvlkey)=0; 
 INTO CURSOR dbMatcri
              * --- Chiudo il Cursore
              use in tmpdbMatcri
              use in select(this.OutCursor)
              use in select(this.ElabCursor)
            endif
          else
            cOC = "ListaODL"
            SELECT scserial, scrownum, cacodice as COMPONENTI, cadesart as CADESART, Matcri.arclacri AS CLACRI,; 
 MatCri.cldescri as CLDESCRI, max(unimis) as counimis,; 
 sum(QtaNet) as somma, sum(QtaLor) as sommaL FROM ListaODL ; 
 inner join Matcri on (&cOC..cacodice=matcri.mtcodice) ; 
 where SCSERIAL==this.w_SCSERIAL and SCROWNUM=this.w_CPROWNUM ; 
 GROUP BY scserial, scrownum, COMPONENTI INTO CURSOR tmpdbMatcri
            select * FROM tmpdbMatcri INTO CURSOR dbMatcri
          endif
        endif
        select this.w_SCNUMSCA as SCNUMSCA, 1 as ORDINE, "xxxMatCrixxx" as DESAUT, SCSERIAL AS SCSERIAL, CPROWORD as CPROWORD,; 
 CPROWNUM as CPROWNUM, SCCODRIS, SCDATINI AS SCDATINI, SCCODODL, SCCODICE, ARDESART,; 
 OLTUNMIS, SCQTARIC,; 
 SCQTAC01, SCQTAC02, SCQTAC03, SCQTAC04, SCQTAC05, SCQTAC06, SCQTAC07, SCQTAC08, SCQTAC09, SCQTAC10,; 
 SCQTAC11, SCQTAC12, SCQTAC13, SCQTAC14, SCQTAP01, SCQTAP02, SCQTAP03, SCQTAP04, SCQTAP05, SCQTAP06,; 
 SCQTAP07, SCQTAP08, SCQTAP09, SCQTAP10, SCQTAP11, SCQTAP12, SCQTAP13, SCQTAP14, SCQTARES; 
 FROM ScaTmp WHERE SCSERIAL=this.w_SCSERIAL AND CPROWNUM=this.w_CPROWNUM INTO CURSOR Appogg NOFILTER
        if (this.oParentObject.w_TIPOSTA="SA3") or (this.oParentObject.w_TIPOSTA="SA4" and this.w_RIEPI="S")
          if Not(g_PRFA="S" AND g_CICLILAV="S" AND Upper(This.oParentObject.Class )="TGSCI_SSC")
            select componenti, cadesart, clacri, cldescri, counimis, Somma from dbMatcri; 
 where cacodice==this.w_SCCODICE INTO CURSOR TmpMatRich ORDER BY CLACRI
          else
            select componenti, cadesart, clacri, cldescri, counimis, Somma from dbMatcri; 
 where SCSERIAL==this.w_SCSERIAL and SCROWNUM=this.w_CPROWNUM INTO CURSOR TmpMatRich ORDER BY CLACRI
          endif
          * --- Chiudo il Cursore
          Use in dbMatcri
          select distinct tmpmatrich.componenti, tmpmatrich.cadesart,; 
 cursclacri.mtcodice as clacri, cursclacri.MTDESCRI as CLDESCRI,; 
 tmpmatrich.counimis as counimis, tmpmatrich.Somma as somma; 
 from tmpmatrich; 
 right join cursclacri on (cursclacri.mtcodice=tmpmatrich.clacri); 
 INTO CURSOR MatRich order by cursclacri.mtcodice
          * --- Chiudo il Cursore
          Use in tmpmatrich
          * --- Creo i Cursori relativi alle colonne da stampare
          CREATE CURSOR ClaDett1 (PAG N(5), CONTA N(5), ORDINE N(5), SCSERIAL C(15), CPROWNUM N(5),; 
 MATCRI_1 C(40), CADESCRI_1 C(40), CLACRI_1 C(5), CLDESCRI_1 C(40), UNIMIS C(3), QTADIS N(12,5))
          =wrcursor("ClaDett1")
          CREATE CURSOR ClaDett2 (PAG N(5), CONTA N(5), ORDINE N(5), SCSERIAL C(15), CPROWNUM N(5),; 
 MATCRI_2 C(40), CADESCRI_2 C(40), CLACRI_2 C(5), CLDESCRI_2 C(40), UNIMIS C(3), QTADIS N(12,5))
          =wrcursor("ClaDett2")
          CREATE CURSOR ClaDett3 (PAG N(5), CONTA N(5), ORDINE N(5), SCSERIAL C(15), CPROWNUM N(5),; 
 MATCRI_3 C(40), CADESCRI_3 C(40), CLACRI_3 C(5), CLDESCRI_3 C(40), UNIMIS C(3), QTADIS N(12,5))
          =wrcursor("ClaDett3")
          select MatRich
          this.l_tCol = reccount()
          this.l_nCol = iif(this.l_tCol = 0, 1, iif((this.l_tCol / 3)=0, 1, (this.l_tCol / 3))) + 1
          this.l_nCol = int(this.l_nCol)
          l_sCol =""
          this.i = 0
          if reccount() >0
            Conta1=1 
 Conta2=1 
 Conta3=1
            select MatRich
            GO TOP
            do while not eof("MatRich")
              for this.w_PAG = 1 TO this.l_nCol
              for this.i = 1 to 3
              select MatRich
              Scatter memvar
              l_sCol= alltrim(str(this.i))
              this.w_CLASS = NVL(CLACRI, "")
              this.w_OLDCLASS = this.w_CLASS
              do while not empty(componenti) and this.w_OLDCLASS=this.w_CLASS
                Scatter memvar
                select ("ClaDett"+l_sCol)
                append blank
                replace PAG WITH this.w_PAG
                replace CONTA WITH Conta&l_sCol
                replace ORDINE WITH 1
                replace SCSERIAL WITH this.w_SCSERIAL
                replace CPROWNUM WITH this.w_CPROWNUM
                replace matcri_&l_sCol with NVL(m.componenti, SPACE(20))
                replace cadescri_&l_sCol with NVL(m.cadesart, SPACE(40))
                replace clacri_&l_sCol with NVL(m.clacri, SPACE(5))
                replace cldescri_&l_sCol with NVL(m.cldescri, SPACE(40))
                replace UNIMIS with NVL(m.counimis, SPACE(3))
                replace QTADIS with NVL(m.somma, 0)
                select MatRich
                skip + 1
                this.w_OLDCLASS = this.w_CLASS
                this.w_CLASS = NVL(CLACRI, "")
              enddo
              Conta&l_sCol=Conta&l_sCol+1
              next
              next
            enddo
          endif
           select distinct clamast.pag as pag, 1 as ordine, this.w_SCSERIAL as SCSERIAL, this.w_CPROWNUM AS CPROWNUM,; 
 cladett1.matcri_1 AS MATCRI_1, clamast.clacri_1, cladett2.matcri_2 AS MATCRI_2,; 
 clamast.clacri_2, cladett3.matcri_3 AS MATCRI_3, clamast.clacri_3 from clamast; 
 left join cladett1 on (clamast.nrec=cladett1.conta); 
 left join cladett2 on (clamast.nrec=cladett2.conta); 
 left join cladett3 on (clamast.nrec=cladett3.conta); 
 into cursor TempClaDett
        endif
        if (this.oParentObject.w_TIPOSTA="SA3") or (this.oParentObject.w_TIPOSTA="SA4" and this.w_RIEPI="S")
          SELECT TempClaDett.PAG, Appogg.*,; 
 TempClaDett.MatCri_1, TempClaDett.Clacri_1, TempClaDett.MatCri_2, TempClaDett.Clacri_2,; 
 TempClaDett.MatCri_3, TempClaDett.Clacri_3, "D" as RIGA; 
 FROM Appogg inner join TempClaDett on (Appogg.scserial=TempClaDett.scserial ; 
 and Appogg.cprownum=TempClaDett.cprownum); 
 into Cursor TempScaDett
        else
          SELECT 1 as PAG, Appogg.*,; 
 SPACE(20) as MatCri_1, SPACE(5) as Clacri_1, SPACE(20) as MatCri_2, SPACE(5) as Clacri_2,; 
 SPACE(20) as MatCri_3, SPACE(5) as Clacri_3, "D" as RIGA; 
 FROM Appogg into Cursor TempScaDett
        endif
        * --- In Questo Caso Stampo il Rieplilogo dei Materiali Critici Utilizzati
        if this.w_RIEPI = "S"
          SELECT PAG AS PAG, MAX(ORDINE) AS ORDINE, SCSERIAL AS SCSERIAL, MAX(CPROWNUM) AS CPROWNUM,; 
 MATCRI_1 AS MATCRI, MAX(CADESCRI_1) AS CADESCRI, CLACRI_1 as CLACRI, MAX(CLDESCRI_1) AS CLDESCRI,; 
 MAX(UNIMIS) AS UNIMIS, SUM(QTADIS) AS QTADIS; 
 FROM CLADETT1 WHERE NOT EMPTY(MATCRI_1) GROUP BY PAG, SCSERIAL, CLACRI, MATCRI;
          UNION;
          SELECT PAG AS PAG, MAX(ORDINE) AS ORDINE, SCSERIAL AS SCSERIAL, MAX(CPROWNUM) AS CPROWNUM,; 
 MATCRI_2 AS MATCRI, MAX(CADESCRI_2) AS CADESCRI, CLACRI_2 as CLACRI, MAX(CLDESCRI_2) AS CLDESCRI,; 
 MAX(UNIMIS) AS UNIMIS, SUM(QTADIS) AS QTADIS; 
 FROM CLADETT2 WHERE NOT EMPTY(MATCRI_2) GROUP BY PAG, SCSERIAL, CLACRI, MATCRI; 
          UNION;
          SELECT PAG AS PAG, MAX(ORDINE) AS ORDINE, SCSERIAL AS SCSERIAL, MAX(CPROWNUM) AS CPROWNUM,; 
 MATCRI_3 AS MATCRI, MAX(CADESCRI_3) AS CADESCRI, CLACRI_3 as CLACRI, MAX(CLDESCRI_3) AS CLDESCRI,; 
 MAX(UNIMIS) AS UNIMIS, SUM(QTADIS) AS QTADIS; 
 FROM CLADETT3 WHERE NOT EMPTY(MATCRI_3) GROUP BY PAG, SCSERIAL, MATCRI INTO CURSOR CLADETTS
          if this.w_SCTIPSCA="CLQ" or (this.w_SCTIPSCA="CLO" and this.w_SCTPQTOR="Q")
            select distinct CLADETTS.PAG AS PAG, 2 as ORDINE, "xxxRiepixxx" as DESAUT,; 
 appogg.SCNUMSCA as SCNUMSCA, appogg.SCSERIAL AS SCSERIAL,; 
 appogg.CPROWORD as CPROWORD, appogg.CPROWNUM as CPROWNUM, appogg.SCCODRIS, appogg.SCCODODL,; 
 SPACE(20) AS SCCODICE, SPACE(20) AS ARDESART,; 
 CLADETTS.CLACRI AS CLCODICE,CLADETTS.CLDESCRI AS CLDESCRI,; 
 CLADETTS.MATCRI AS MTCODICE, CLADETTS.CADESCRI AS CADESCRI,; 
 CLADETTS.UNIMIS AS MTUNMIS,; 
 appogg.SCQTAP01*CLADETTS.QTADIS as MTQTAP01, appogg.SCQTAP02*CLADETTS.QTADIS as MTQTAP02,; 
 appogg.SCQTAP03*CLADETTS.QTADIS as MTQTAP03, appogg.SCQTAP04*CLADETTS.QTADIS as MTQTAP04,; 
 appogg.SCQTAP05*CLADETTS.QTADIS as MTQTAP05, appogg.SCQTAP06*CLADETTS.QTADIS as MTQTAP06,; 
 appogg.SCQTAP07*CLADETTS.QTADIS as MTQTAP07, appogg.SCQTAP08*CLADETTS.QTADIS as MTQTAP08,; 
 appogg.SCQTAP09*CLADETTS.QTADIS as MTQTAP09, appogg.SCQTAP10*CLADETTS.QTADIS as MTQTAP10,; 
 appogg.SCQTAP11*CLADETTS.QTADIS as MTQTAP11, appogg.SCQTAP12*CLADETTS.QTADIS as MTQTAP12,; 
 appogg.SCQTAP13*CLADETTS.QTADIS as MTQTAP13, appogg.SCQTAP14*CLADETTS.QTADIS as MTQTAP14,; 
 SPACE(20) AS MATCRI_1, SPACE(5) AS CLACRI_1, SPACE(20) AS MATCRI_2, SPACE(5) AS CLACRI_2,; 
 SPACE(20) AS MATCRI_3, SPACE(5) AS CLACRI_3, "R" as RIGA; 
 FROM Appogg INNER JOIN CLADETTS ON; 
 (Appogg.scserial=CLADETTS.scserial and appogg.cprownum=CLADETTS.cprownum); 
 INTO CURSOR TmpRiepi && WHERE appogg.SCCODODL not in (select distinct SCCODODL from scariepi)
          endif
          USE IN CLADETTS
        endif
        if used ("TempScaDett")
          select TempScaDett
          SCAN
          * --- Legge e scrive su cursore
          SCATTER MEMVAR
          Select ScaDett
          append BLANK
          m.Matcri_1=nvl(m.Matcri_1, SPACE(20))
          m.Clacri_1=nvl(m.Clacri_1, space(5))
          m.Matcri_2=nvl(m.Matcri_2, SPACE(20))
          m.Clacri_2=nvl(m.Clacri_2, space(5))
          m.Matcri_3=nvl(m.Matcri_3, SPACE(20))
          m.Clacri_3=nvl(m.Clacri_3, space(5))
          GATHER MEMVAR
          select TempScaDett
          ENDSCAN
          use in TempScaDett
        endif
        if used ("TmpRiepi")
          select TmpRiepi
          SCAN
          * --- Legge e scrive su cursore
          SCATTER MEMVAR
          Select ScaRiepi
          append BLANK
          m.Matcri_1=nvl(m.Matcri_1, SPACE(20))
          m.Clacri_1=nvl(m.Clacri_1, space(5))
          m.Matcri_2=nvl(m.Matcri_2, SPACE(20))
          m.Clacri_2=nvl(m.Clacri_2, space(5))
          m.Matcri_3=nvl(m.Matcri_3, SPACE(20))
          m.Clacri_3=nvl(m.Clacri_3, space(5))
          GATHER MEMVAR
          select TmpRiepi
          ENDSCAN
          use in TmpRiepi
        endif
      endif
      if used ("ClaDett1")
        use in ClaDett1
      endif
      if used ("ClaDett2")
        use in ClaDett2
      endif
      if used ("ClaDett3")
        use in ClaDett3
      endif
      if used ("ClaDetts")
        use in ClaDetts
      endif
      if used ("TempClaDett")
        use in TempClaDett
      endif
      if used("TempScaDett")
        use in TempScaDett
      endif
      if used ("Appogg")
        use in Appogg
      endif
      if used ("TmpRiepi")
        use in TmpRiepi
      endif
      this.w_OLDSERIAL = this.w_SCSERIAL
      select ScaTmp
      ENDSCAN
      * --- Aggiorno le righe del riepilogo con l'ultimo CPROWORD della pagina
      SELECT Scadett 
 COUNT TO nRec
      SELECT SCSERIAL, PAG, MAX(CPROWORD) AS CPROWORD FROM SCADETT; 
 GROUP BY SCSERIAL, PAG INTO CURSOR XROWS
      SELECT XROWS
      SCAN
      this.w_PAG = PAG
      this.w_SERSCA = SCSERIAL
      this.w_ORD = CPROWORD
      UPDATE ScaRiepi set CPROWORD = this.w_ORD where SCSERIAL = this.w_SERSCA and PAG = this.w_PAG
      SELECT XROWS
      ENDSCAN
      Use in XROWS
    else
    endif
    use in select(this.BmpCursor)
    use in select(this.InCursor)
    use in select(this.OutCursor)
    use in select("ListaODL")
    use in select("dbMatcri")
    use in select("ClaMast")
    Use in select("CursClaCri")
    Use in select("CodiciRicerca")
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='*TMPRIS_ORSE'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
