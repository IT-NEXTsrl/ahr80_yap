* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bst                                                        *
*              Stampa Tracciabilità ODL                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-25                                                      *
* Last revis.: 2015-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bst",oParentObject)
return(i_retval)

define class tgsco_bst as StdBatch
  * --- Local variables
  w_CI = 0
  w_CJ = 0
  w_CK = 0
  w_CX = 0
  w_CY = 0
  w_CZ = 0
  w_CW = 0
  w_LVLKEY = space(240)
  w_LVLKEY1 = space(240)
  w_LVLKEY2 = space(240)
  w_LVLKEY2_ = space(240)
  w_LVLKEY3 = space(240)
  w_LVLKEY4 = space(240)
  w_LVLKEY5 = space(240)
  w_LVLKEY6 = space(240)
  w_APPTIPO = 0
  * --- WorkFile variables
  TMPODL_MAST_idx=0
  TMPOCL_MAST_idx=0
  TMPDOCUM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Stampa tracciabilità ODL
    * --- --Filitro sul codice ODL per la stampa
    * --- Create temporary table TMPOCL_MAST
    i_nIdx=cp_AddTableDef('TMPOCL_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_GBOQUWBBYL[1]
    indexes_GBOQUWBBYL[1]='OLCODODL'
    vq_exec('gsco_pto',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_GBOQUWBBYL,.f.)
    this.TMPOCL_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- --ODL contenuti nell' albero
    * --- Create temporary table TMPDOCUM
    i_nIdx=cp_AddTableDef('TMPDOCUM') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_SDSNPKKRTK[1]
    indexes_SDSNPKKRTK[1]='OLCODODL'
    vq_exec('gsco_pto1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_SDSNPKKRTK,.f.)
    this.TMPDOCUM_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Drop temporary table TMPODL_MAST
    i_nIdx=cp_GetTableDefIdx('TMPODL_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPODL_MAST')
    endif
    * --- --odl da stampare filtrati
    * --- Create temporary table TMPODL_MAST
    i_nIdx=cp_AddTableDef('TMPODL_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_OXCZFMLXGR[1]
    indexes_OXCZFMLXGR[1]='OLCODODL'
    vq_exec('gsco_pto2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_OXCZFMLXGR,.f.)
    this.TMPODL_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    vq_exec("..\cola\exe\query\gsco_fto", this, "_curs_")
    * --- Solo trasferimento e dichiarazioni
    vq_exec("..\cola\exe\query\gsco1fto", this, "_dett_")
    select Olcododl,Padre,Oltstato,Oltcodic,Ardesart + space(10) as Ardesart,Oltdinric,; 
 Oltdtric,Oltunmis,Oltqtodl,Oltqtoev,Oltqtobf,Oltcofor,Oltcomag,Mvserial as Mvserial,Mvnumreg,CPROWNUM,Mvtipdoc,; 
 Mvcladoc,Mvcaumag,Mvflveac,Mvnumdoc,Mvalfdoc,Tipo,Livello,Posizione, space(240) as lvlkey,; 
 space(150) as CPBmpName, space(250) as Descri, oltprove, ordfas from _curs_ union ; 
 select Olcododl,Padre,Oltstato,Oltcodic,Ardesart,Oltdinric,; 
 Oltdtric,Oltunmis,Oltqtodl,Oltqtoev,Oltqtobf,Oltcofor,Oltcomag,Mvserial,Mvnumreg,CPROWNUM,Mvtipdoc,; 
 Mvcladoc,Mvcaumag,Mvflveac,Mvnumdoc,Mvalfdoc,Tipo,Livello,Posizione, space(240) as lvlkey, space(150) as CPBmpName,; 
 space(250) as Descri, oltprove, ordfas from _dett_ order by 1,30,2,25,14,24,16 into cursor __tmp__ 
 =wrcursor("__tmp__")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    CP_CHPRN("..\cola\EXE\QUERY\gsco_BTO")
    * --- Drop temporary table TMPODL_MAST
    i_nIdx=cp_GetTableDefIdx('TMPODL_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPODL_MAST')
    endif
    * --- Ripristina la tabella temporanea usata da GSCO_BTO
    * --- Create temporary table TMPODL_MAST
    i_nIdx=cp_AddTableDef('TMPODL_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_OYVHQYHSLV[1]
    indexes_OYVHQYHSLV[1]='OLCODODL'
    vq_exec('gsco_pto3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_OYVHQYHSLV,.f.)
    this.TMPODL_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Drop temporary table TMPOCL_MAST
    i_nIdx=cp_GetTableDefIdx('TMPOCL_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPOCL_MAST')
    endif
    * --- Drop temporary table TMPDOCUM
    i_nIdx=cp_GetTableDefIdx('TMPDOCUM')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDOCUM')
    endif
    if used("_curs_")
      use in _curs_
    endif
    if used("_dett_")
      use in _dett_
    endif
    this.OparentObject.ecpquit()
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    scan
    * --- Calcolo la chiave della tree-view (lvlkey)
    do case
      case livello=0
        * --- ODL 
        this.w_CI = this.w_CI + 1
        this.w_CJ = 1
        this.w_CK = 0
        this.w_LVLKEY = right("00000000"+alltrim(str(this.w_CI)),9)
        replace lvlkey with this.w_LVLKEY
      case livello=1
        if tipo$ "TRA-FOT-FCP-FSE-DPC-ODF "
          * --- TRA, FOT,FCP,FSE,DPC,ODF
          this.w_LVLKEY1 = this.w_LVLKEY+"."+right("00000000"+alltrim(str(this.w_CJ)),9)
          replace lvlkey with this.w_LVLKEY1
          this.w_CJ = this.w_CJ + 1
          this.w_CK = 1
        endif
        * --- TRASFERIMENTO, FASI OUTPUT, COUNT POINT, ESTERNE DICHIARAZIONI
      case livello=2
        * --- --MOV-DDO-DDP-DRD-FDO-FSC-DPP-TRF
        this.w_LVLKEY2 = alltrim(this.w_LVLKEY1)+ "." + right("00000000"+alltrim(str(this.w_CK)),9)
        replace lvlkey with this.w_LVLKEY2
        this.w_CK = this.w_CK + 1
        this.w_CW =  1
      case livello=3
        this.w_LVLKEY3 = ALLTRIM(this.w_LVLKEY2)+"."+right("00000000"+alltrim(str(this.w_CW)),9)
        this.w_CW = this.w_CW+ 1
        replace lvlkey with this.w_LVLKEY3
        this.w_CZ = 1
        * --- --RIG-DIM-MOS-DIR-DPT-DCF-MOV(solo se di fase)
      case livello=4
        * --- --DMT-MRS-DPT
        this.W_LVLKEY4 = ALLTRIM(this.w_LVLKEY3)+"."+right("00000000"+alltrim(str(this.w_CZ)),9)
        replace lvlkey with this.w_LVLKEY4
        this.w_CZ = this.w_CZ+ 1
        this.w_CX =  1
      case livello=5
        * --- --DMM-DPM
        this.w_LVLKEY5 = this.w_LVLKEY4+"."+right("00000000"+alltrim(str(this.w_CX)),9)
        replace lvlkey with this.w_LVLKEY5
        this.w_CX = this.w_CX + 1
    endcase
    ENDSCAN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*TMPODL_MAST'
    this.cWorkTables[2]='*TMPOCL_MAST'
    this.cWorkTables[3]='*TMPDOCUM'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
