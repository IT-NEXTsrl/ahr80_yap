* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_blo                                                        *
*              Lancio ODL da Pegging                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-09                                                      *
* Last revis.: 2018-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_blo",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_blo as StdBatch
  * --- Local variables
  pAzione = space(2)
  w_SELEZI = space(1)
  w_PPTIPDOC = space(5)
  w_PPNUMDOC = 0
  w_PPALFDOC = space(2)
  w_PPDATDOC = ctod("  /  /  ")
  w_PPDATDIV = ctod("  /  /  ")
  w_PPFLVEAC = space(1)
  w_PPPRD = space(2)
  w_PPCLADOC = space(2)
  w_PPFLINTE = space(1)
  w_PPTCAMAG = space(5)
  w_PPTFRAGG = space(1)
  w_PPCAUCON = space(5)
  w_PPNUMSCO = 0
  w_PPFLACCO = 0
  w_PPANNPRO = space(4)
  w_PPANNDOC = space(4)
  w_PPFLPROV = space(1)
  w_PPDATTRA = ctod("  /  /  ")
  w_PPORATRA = space(2)
  w_PPMINTRA = space(2)
  w_PPNOTAGG = space(40)
  w_PPCHKPRE = space(1)
  w_STADOC = space(1)
  w_STAMPODL = space(1)
  w_STAMPFAS = space(1)
  * --- WorkFile variables
  PAR_PROD_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Lancio ODL / Generazione Buoni di Prelievo (da GSCO_KGI, GSCO_KGB)
    * --- Batch di Appoggio per lanciare gli ODL dal pegging (GSMR_KPE)
    do case
      case this.pAzione="AG"
        this.w_SELEZI = "Z"
        this.w_STAMPODL = "N"
        this.w_STAMPFAS = "N"
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPCODICE,PPCODCAU,PPGESWIP"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPCODICE,PPCODCAU,PPGESWIP;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_READPAR = NVL(cp_ToDate(_read_.PPCODICE),cp_NullValue(_read_.PPCODICE))
          this.w_PPTIPDOC = NVL(cp_ToDate(_read_.PPCODCAU),cp_NullValue(_read_.PPCODCAU))
          w_GESWIP = NVL(cp_ToDate(_read_.PPGESWIP),cp_NullValue(_read_.PPGESWIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_PPTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO;
            from (i_cTable) where;
                TDTIPDOC = this.w_PPTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PPTIPDOC = NVL(cp_ToDate(_read_.TDTIPDOC),cp_NullValue(_read_.TDTIPDOC))
          w_DESRIF = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
          this.w_PPFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          this.w_PPALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
          this.w_PPPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
          this.w_PPCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          this.w_PPFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
          this.w_PPFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_PPTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_PPTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
          this.w_PPCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
          w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
          this.w_PPNUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        GSCO_BGI(this,"AG")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione="PI"
        this.w_SELEZI = "Z"
        GSCO_BCS(this,"AG")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
