* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bdo                                                        *
*              Eventi da gestione documenti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1096]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-29                                                      *
* Last revis.: 2018-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe,pSerDoc,pOFLPROV,pFLPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bdo",oParentObject,m.pTipOpe,m.pSerDoc,m.pOFLPROV,m.pFLPROV)
return(i_retval)

define class tgsco_bdo as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  pSerDoc = space(10)
  pOFLPROV = space(1)
  pFLPROV = space(1)
  w_PPCAUORD = space(5)
  w_PPCAUIMP = space(5)
  w_SERFIL = space(10)
  w_MESS = space(10)
  w_PDTIPGEN = space(2)
  w_PDSERIAL = space(10)
  w_ODLFIL = space(15)
  w_SERODL = space(15)
  w_ROWODL = 0
  w_CODODL = space(15)
  w_KEYSAL = space(20)
  w_CODMAG = space(5)
  w_CODMAT = space(5)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_DISMAG = space(1)
  w_QTAPRE = 0
  w_QTAPR1 = 0
  w_QTASAL = 0
  w_FLPREV = space(1)
  w_QTOEV1 = 0
  w_FLEVAS = space(1)
  w_TOTALE = .f.
  w_RECO = 0
  w_ROWDOC = 0
  w_APPO = 0
  w_APPO1 = 0
  w_TIPRIG = space(1)
  w_CPROWNUM = 0
  w_OLCOEIMP = 0
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_OLFLEVAS = space(1)
  w_OLEVAAUT = space(1)
  w_OLQTASAL = 0
  w_OLQTAPRE = 0
  w_OLQTAPR1 = 0
  w_QTODL1 = 0
  w_QTAODL = 0
  w_ODLQTASAL = 0
  w_OLDQTASA = 0
  w_APPO2 = 0
  w_OLKEYSAL = space(40)
  w_SEGNO = space(1)
  w_QTA1 = 0
  w_FLUBIC = space(1)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_OLTCOMME = space(15)
  w_OLTCOART = space(20)
  w_SALCOM = space(1)
  w_DATEVA = ctod("  /  /  ")
  w_OLTEMLAV = 0
  w_OLTDINRIC = ctod("  /  /  ")
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLDATRIC = ctod("  /  /  ")
  w_DATRIF = ctod("  /  /  ")
  w_OLTPERAS = space(3)
  w_OLTLEMPS = 0
  w_OLTDTMPS = ctod("  /  /  ")
  TmpC = space(100)
  w_ROWORD = 0
  w_OFLPROV = space(1)
  w_FLPROV = space(1)
  w_PADRE = .NULL.
  w_DATEVAL = ctod("  /  /  ")
  w_NUMREC = 0
  w_CODLOT = space(20)
  w_OLTPROVE = space(1)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  RIF_GODL_idx=0
  SALDIART_idx=0
  MAGAZZIN_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Modifica/Cancellazione documento generato da Piano Generazione (da GSVE_BMK)
    * --- Il programma e' derivato da GSCO_BMG
    * --- Parametri: 
    * --- Tipo Operazione -  E = Elimina ; V = Varia
    * --- Chiave Documento
    * --- Variabili per la gestione delle date dell'OCL
    this.w_OFLPROV = IIF(type("this.pOFLPROV")<>"C", "N", this.pOFLPROV)
    this.w_FLPROV = IIF(type("this.pFLPROV")<>"C", "N", this.pFLPROV)
    this.w_PADRE = this.oParentObject.oParentObject
    this.w_MESS = " "
    this.w_PPCAUORD = " "
    this.w_PPCAUIMP = " "
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUORD,PPCAUIMP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUORD,PPCAUIMP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPCAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.w_PPCAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    ah_Msg("Elimina riferiferimenti generazione ODL/OCL...",.T.)
    * --- Seleziona i riferimenti al Piano di Generazione che ha creato il Documento
    this.w_SERFIL = this.pSerDoc
    vq_exec("..\COLA\EXE\QUERY\GSCODBDO.VQR",this,"LISTAODL")
    if USED("LISTAODL")
      if this.pTipOpe="V"
        * --- Memorizzo la posizione sul Trs dei documenti
        this.w_PADRE.MarkPos()     
        * --- Documento Variato, setta le sole righe di RIF_GODL effettivamente da Eliminare/Variare 
        OLDSET = SET("DELETED")
        SET DELETED OFF
        * --- Cicla sul Cursore
        SELECT LISTAODL
        GO TOP
        SCAN FOR NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0
        this.w_ROWDOC = NVL(PDROWDOC, 0)
        this.w_QTAPRE = NVL(PDQTAPRE, 0)
        this.w_QTAPR1 = NVL(PDQTAPR1, 0)
        this.w_DATEVAL = NVL(MVDATEVA, cp_CharToDate("  -  -  "))
        * --- Mi metto sulla prima riga
        *     Verifica corrispondenza sul temporaneo del Documento 
        this.w_PADRE.FirstRow()     
        this.w_NUMREC = this.w_PADRE.Search( "CPROWNUM= "+cp_ToStrODBC(this.w_ROWDOC))
        if this.w_NUMREC<>-1
          this.w_PADRE.SetRow(this.w_NUMREC)     
          this.w_TIPRIG = IIF(this.w_PADRE.ROWSTATUS()="D", "E", "V")
          this.w_QTAMOV = NVL( this.w_PADRE.Get("t_MVQTAMOV") , 0)
          this.w_QTAUM1 = NVL( this.w_PADRE.Get("t_MVQTAUM1") , 0)
          this.w_DATEVA = NVL( this.w_PADRE.Get("t_MVDATEVA") , cp_CharToDate("  -  -  "))
          this.w_CODLOT = NVL( this.w_PADRE.Get("t_MVCODLOT") , "")
          if this.w_TIPRIG="V"
            if this.w_QTAPRE<>this.w_QTAMOV OR this.w_QTAPR1<>this.w_QTAUM1 OR DTOS(this.w_DATEVA)<>DTOS(this.w_DATEVAL)
              * --- Default Cancellato
               
 SELECT LISTAODL 
 REPLACE MVQTAMOV WITH this.w_QTAMOV, MVQTAUM1 WITH this.w_QTAUM1, TIPRIG WITH "V", MVDATEVA with this.w_DATEVA
            else
              this.w_TIPRIG = " "
              SELECT LISTAODL 
 REPLACE TIPRIG WITH " "
            endif
          endif
        else
          this.w_TIPRIG = " "
          SELECT LISTAODL 
 REPLACE TIPRIG WITH " "
        endif
        SELECT LISTAODL
        ENDSCAN 
        * --- set deleted ripristinata al valore originario al termine del Batch
        SET DELETED &OLDSET
        * --- Questa Parte derivata dal Metodo LoadRec
        this.w_PADRE.RePos()     
      endif
      * --- Legge Tipo del Piano di Generazione
      SELECT LISTAODL
      GO TOP
      this.w_PDTIPGEN = LISTAODL.PDTIPGEN
      this.w_PDSERIAL = LISTAODL.PDSERIAL
      this.w_SERFIL = this.w_PDSERIAL
      if this.w_PDTIPGEN="OR"
        * --- Se Ordine a Terzista, cicla sulle righe eliminate (TIPRIG='E')
        * --- ed elimina i DDT di Trasferimento associati ai Componenti dell' OCL da riaprire.
        * --- Rielaboro Curosore ListaOdl nel caso di pi� righe documento collegato ad unico ODL (Split)
        SELECT PDCODODL,PDROWODL,IIF(TIPRIG=" ","Z",TIPRIG) AS TIPRIG FROM LISTAODL INTO CURSOR ELAB
        SELECT PDCODODL,PDROWODL,MIN(TIPRIG) AS TIPRIG FROM ELAB GROUP BY PDCODODL,PDROWODL INTO CURSOR ELAB
        SELECT ELAB
        GO TOP
        SCAN FOR NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0 AND TIPRIG="E"
        this.w_ODLFIL = PDCODODL
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT ELAB
        ENDSCAN 
        SELECT ELAB
        USE
      endif
      SELECT LISTAODL
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0 AND TIPRIG $ "VE"
      this.w_CODODL = PDCODODL
      this.w_ROWODL = PDROWODL
      this.w_ROWDOC = NVL(PDROWDOC, 0)
      this.w_TIPRIG = TIPRIG
      this.w_QTAPRE = NVL(PDQTAPRE, 0)
      this.w_QTAPR1 = NVL(PDQTAPR1, 0)
      this.w_QTASAL = NVL(PDQTAPR1, 0)
      this.w_QTAMOV = NVL(MVQTAMOV, 0)
      this.w_QTAUM1 = NVL(MVQTAUM1, 0)
      this.w_FLPREV = NVL(PDFLPREV, " ")
      this.w_DATEVA = NVL(MVDATEVA, cp_CharToDate("  -  -  "))
      do case
        case this.w_PDTIPGEN="OR"
          this.w_CODMAG = " "
          this.w_KEYSAL = " "
          this.w_QTOEV1 = 0
          this.w_FLEVAS = " "
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLTCOMAG,OLTKEYSA,OLTQTOE1,OLTFLEVA,OLTCOMME,OLTCOART"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLTCOMAG,OLTKEYSA,OLTQTOE1,OLTFLEVA,OLTCOMME,OLTCOART;
              from (i_cTable) where;
                  OLCODODL = this.w_CODODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
            this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
            this.w_QTOEV1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
            this.w_FLEVAS = NVL(cp_ToDate(_read_.OLTFLEVA),cp_NullValue(_read_.OLTFLEVA))
            this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
            this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = 'Errore in Lettura ODL_MAST'
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_OLTCOMME,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_OLTCOMME
            endif
          endif
          if this.w_TIPRIG="E"
            * --- Se Elimino Riapre l' Ordine su ODL_MAST riportandolo allo stato Pianificato
            * --- In questo caso anche eliminando l'Ordine ad terzista, rimane invariato l'ordinato
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
              +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCAUORD),'ODL_MAST','OLTCAMAG');
              +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLORD');
              +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLIMP');
              +",OLTDTLAN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ODL_MAST','OLTDTLAN');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                     )
            else
              update (i_cTable) set;
                  OLTSTATO = "P";
                  ,OLTCAMAG = this.w_PPCAUORD;
                  ,OLTFLORD = "+";
                  ,OLTFLIMP = " ";
                  ,OLTDTLAN = cp_CharToDate("  -  -  ");
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_CODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura ODL_MAST (3)'
              return
            endif
            if this.w_QTASAL<>0 AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG)
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_QTASAL);
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = SLQTOPER + this.w_QTASAL;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART'
                return
              endif
              if this.w_SALCOM="S"
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_QTASAL);
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = SCQTOPER + this.w_QTASAL;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
            endif
          else
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTEMLAV,OLTDINRIC,OLTDTRIC,OLTLEMPS,OLTPROVE"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTEMLAV,OLTDINRIC,OLTDTRIC,OLTLEMPS,OLTPROVE;
                from (i_cTable) where;
                    OLCODODL = this.w_CODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLTEMLAV = NVL(cp_ToDate(_read_.OLTEMLAV),cp_NullValue(_read_.OLTEMLAV))
              this.w_OLTDINRIC = NVL(cp_ToDate(_read_.OLTDINRIC),cp_NullValue(_read_.OLTDINRIC))
              this.w_OLTDTRIC = NVL(cp_ToDate(_read_.OLTDTRIC),cp_NullValue(_read_.OLTDTRIC))
              this.w_OLTLEMPS = NVL(cp_ToDate(_read_.OLTLEMPS),cp_NullValue(_read_.OLTLEMPS))
              this.w_OLTPROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_DATEVA <> this.w_OLTDTRIC
              * --- Se � stata Modificata la Data
              * --- Ricalcola data di inizio (MPS)
              this.w_OLTDTMPS = COCALCLT(this.w_DATEVA, this.w_OLTLEMPS, "I", .F., "")
              * --- Ricalcola data di inizio (Produzione)
              this.w_OLTDINRIC = COCALCLT(this.w_DATEVA, this.w_OLTEMLAV, "I", .F., "")
              * --- Determina periodo di appartenenza dell' ODL/OCL
              this.w_DATRIF = this.w_DATEVA
              vq_exec ("..\COLA\EXE\QUERY\GSCO_BOL", this, "__Temp__")
              if used("__Temp__")
                * --- Preleva periodo assoluto
                select __Temp__
                go top
                this.w_OLTPERAS = __Temp__.TPPERASS
                * --- Chiude cursore
                USE IN __Temp__
              endif
            endif
            * --- Se Vario, Aggiorno Qta Ordine su ODL_MAST e Componenti (ODL_DETT)
            * --- Attenzione: L'ordinato sull' OCL rimane evaso e di tipo 'L' ; L'ordinato viene aggiornato direttamente dal Documento
            this.w_QTASAL = IIF(this.w_FLEVAS="S", 0, this.w_QTAUM1 - this.w_QTOEV1)
            this.w_QTASAL = IIF(this.w_QTASAL<0, 0, this.w_QTASAL)
            this.w_QTAODL = ( this.w_QTAMOV-this.w_QTAPRE)
            this.w_QTODL1 = ( this.w_QTAUM1-this.w_QTAPR1)
            if this.w_FLEVAS="S"
              * --- Write into ODL_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLTQTODL =OLTQTODL+ "+cp_ToStrODBC(this.w_QTAODL);
                +",OLTQTOD1 =OLTQTOD1+ "+cp_ToStrODBC(this.w_QTODL1);
                +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                       )
              else
                update (i_cTable) set;
                    OLTQTODL = OLTQTODL + this.w_QTAODL;
                    ,OLTQTOD1 = OLTQTOD1 + this.w_QTODL1;
                    ,OLTQTSAL = 0;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura ODL_MAST (4)'
                return
              endif
            else
              if this.w_DATEVA <> this.w_OLTDTRIC
                * --- Write into ODL_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTQTODL =OLTQTODL+ "+cp_ToStrODBC(this.w_QTAODL);
                  +",OLTQTOD1 =OLTQTOD1+ "+cp_ToStrODBC(this.w_QTODL1);
                  +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_QTASAL),'ODL_MAST','OLTQTSAL');
                  +",OLTDTRIC ="+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'ODL_MAST','OLTDTRIC');
                  +",OLTDINRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDINRIC),'ODL_MAST','OLTDINRIC');
                  +",OLTPERAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLTPERAS),'ODL_MAST','OLTPERAS');
                  +",OLTDTMPS ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTMPS),'ODL_MAST','OLTDTMPS');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                         )
                else
                  update (i_cTable) set;
                      OLTQTODL = OLTQTODL + this.w_QTAODL;
                      ,OLTQTOD1 = OLTQTOD1 + this.w_QTODL1;
                      ,OLTQTSAL = this.w_QTASAL;
                      ,OLTDTRIC = this.w_DATEVA;
                      ,OLTDINRIC = this.w_OLTDINRIC;
                      ,OLTPERAS = this.w_OLTPERAS;
                      ,OLTDTMPS = this.w_OLTDTMPS;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_MAST (4)'
                  return
                endif
              else
                * --- Write into ODL_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTQTODL =OLTQTODL+ "+cp_ToStrODBC(this.w_QTAODL);
                  +",OLTQTOD1 =OLTQTOD1+ "+cp_ToStrODBC(this.w_QTODL1);
                  +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_QTASAL),'ODL_MAST','OLTQTSAL');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                         )
                else
                  update (i_cTable) set;
                      OLTQTODL = OLTQTODL + this.w_QTAODL;
                      ,OLTQTOD1 = OLTQTOD1 + this.w_QTODL1;
                      ,OLTQTSAL = this.w_QTASAL;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_MAST (4)'
                  return
                endif
              endif
            endif
            * --- Aggiorna i Saldi Nel Caso in cui l'ordine a fornitore sia in stato provvisorio
            if this.w_FLPROV="S"
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_QTODL1);
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = SLQTOPER + this.w_QTODL1;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART'
                return
              endif
              if this.w_SALCOM="S"
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_QTODL1);
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = SCQTOPER + this.w_QTODL1;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
            else
              if this.w_FLPROV="N" and this.w_FLPROV<>this.w_OFLPROV
                * --- Nel Momento in cui viene confermato l'ordine a fornitore, storno
                *     l'rdinato dell'OCL e sbianco i Flag
                * --- Write into ODL_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(null),'ODL_MAST','OLTCAMAG');
                  +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLORD');
                  +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLIMP');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                         )
                else
                  update (i_cTable) set;
                      OLTCAMAG = null;
                      ,OLTFLORD = " ";
                      ,OLTFLIMP = " ";
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_MAST'
                  return
                endif
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER =SLQTOPER- "+cp_ToStrODBC(this.w_QTAPR1);
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = SLQTOPER - this.w_QTAPR1;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_KEYSAL;
                      and SLCODMAG = this.w_CODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDIART (3)'
                  return
                endif
                if this.w_SALCOM="S"
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTOPER =SCQTOPER- "+cp_ToStrODBC(this.w_QTAPR1);
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                        +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                        +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                           )
                  else
                    update (i_cTable) set;
                        SCQTOPER = SCQTOPER - this.w_QTAPR1;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = this.w_KEYSAL;
                        and SCCODMAG = this.w_CODMAG;
                        and SCCODCAN = this.w_COMMAPPO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa (3)'
                    return
                  endif
                endif
              endif
            endif
            if this.w_OLTPROVE<>"E"
              * --- Aggiorna i Materiali OCL in base alla nuova quantita'
              * --- Select from ODL_DETT
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
                    +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+"";
                     ,"_Curs_ODL_DETT")
              else
                select * from (i_cTable);
                 where OLCODODL=this.w_CODODL;
                  into cursor _Curs_ODL_DETT
              endif
              if used('_Curs_ODL_DETT')
                select _Curs_ODL_DETT
                locate for 1=1
                do while not(eof())
                this.w_CPROWNUM = NVL(_Curs_ODL_DETT.CPROWNUM,0)
                this.w_OLDATRIC = NVL(_Curs_ODL_DETT.OLDATRIC, cp_CharToDate("  -  -  "))
                if this.w_CPROWNUM<>0
                  * --- Recupero La Quantit� del Saldo prima dell'aggiornamento
                  this.w_OLDQTASA = NVL(_Curs_ODL_DETT.OLQTASAL,0)
                  this.w_CODMAG = NVL(_Curs_ODL_DETT.OLCODMAG, SPACE(5))
                  this.w_OLKEYSAL = NVL(_Curs_ODL_DETT.OLKEYSAL, SPACE(40))
                  this.w_OLTCOART = NVL(_Curs_ODL_DETT.OLCODART, SPACE(20))
                  * --- -----------------------------------------------------------
                  this.w_OLEVAAUT = NVL(_Curs_ODL_DETT.OLEVAAUT," ")
                  this.w_OLFLEVAS = NVL(_Curs_ODL_DETT.OLFLEVAS," ")
                  this.w_OLCOEIMP = NVL(_Curs_ODL_DETT.OLCOEIMP,0)
                  this.w_OLQTAMOV = this.w_QTAUM1 * this.w_OLCOEIMP
                  this.w_OLQTAUM1 = this.w_OLQTAMOV
                  this.w_APPO = NVL(_Curs_ODL_DETT.OLQTAMOV,0)
                  this.w_APPO1 = NVL(_Curs_ODL_DETT.OLQTAUM1,0)
                  if this.w_APPO<>this.w_APPO1 AND this.w_APPO<>0
                    this.w_OLQTAUM1 = cp_ROUND((this.w_APPO1 * this.w_OLQTAMOV) / this.w_APPO, 3)
                  endif
                  this.w_OLQTAEV1 = IIF (this.w_OLEVAAUT="S" , this.w_OLQTAUM1, NVL(_Curs_ODL_DETT.OLQTAEV1,0))
                  this.w_OLQTAEVA = IIF (this.w_OLEVAAUT="S" , this.w_OLQTAMOV, NVL(_Curs_ODL_DETT.OLQTAEVA,0))
                  this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S" OR this.w_OLQTAEV1>this.w_OLQTAUM1, 0, this.w_OLQTAUM1-this.w_OLQTAEV1)
                  this.w_OLDATRIC = IIF(this.w_DATEVA <> this.w_OLTDTRIC , this.w_OLTDINRIC, this.w_OLDATRIC)
                  * --- Write into ODL_DETT
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OLQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
                    +",OLQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
                    +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
                    +",OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
                    +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
                    +",OLDATRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLDATRIC),'ODL_DETT','OLDATRIC');
                        +i_ccchkf ;
                    +" where ";
                        +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        OLQTAMOV = this.w_OLQTAMOV;
                        ,OLQTAUM1 = this.w_OLQTAUM1;
                        ,OLQTASAL = this.w_OLQTASAL;
                        ,OLQTAEVA = this.w_OLQTAEVA;
                        ,OLQTAEV1 = this.w_OLQTAEV1;
                        ,OLDATRIC = this.w_OLDATRIC;
                        &i_ccchkf. ;
                     where;
                        OLCODODL = this.w_CODODL;
                        and CPROWNUM = this.w_CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore in scrittura ODL_DETT (4)'
                    return
                  endif
                  * --- Calcolo Il segno, la Quantit� da Aggiornare e Aggiorno i Saldi
                  this.w_APPO2 = ABS(this.w_OLDQTASA-this.w_OLQTASAL)
                  this.w_SEGNO = IIF(this.w_OLDQTASA<this.w_OLQTASAL, "+", "-")
                  * --- Write into SALDIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_SEGNO,'SLQTIPER','this.w_APPO2',this.w_APPO2,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                        +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                           )
                  else
                    update (i_cTable) set;
                        SLQTIPER = &i_cOp1.;
                        &i_ccchkf. ;
                     where;
                        SLCODICE = this.w_OLKEYSAL;
                        and SLCODMAG = this.w_CODMAG;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa'
                    return
                  endif
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_OLTCOART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_SALCOM="S"
                    if empty(nvl(this.w_OLTCOMME,""))
                      this.w_COMMAPPO = this.w_COMMDEFA
                    else
                      this.w_COMMAPPO = this.w_OLTCOMME
                    endif
                    * --- Write into SALDICOM
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDICOM_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                      i_cOp1=cp_SetTrsOp(this.w_SEGNO,'SCQTIPER','this.w_APPO2',this.w_APPO2,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                          +i_ccchkf ;
                      +" where ";
                          +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                          +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                             )
                    else
                      update (i_cTable) set;
                          SCQTIPER = &i_cOp1.;
                          &i_ccchkf. ;
                       where;
                          SCCODICE = this.w_OLKEYSAL;
                          and SCCODMAG = this.w_CODMAG;
                          and SCCODCAN = this.w_COMMAPPO;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore Aggiornamento Saldi Commessa'
                      return
                    endif
                  endif
                endif
                  select _Curs_ODL_DETT
                  continue
                enddo
                use
              endif
            endif
          endif
        case this.w_PDTIPGEN $ "DT-BP"
          * --- Generazione DDT di Trasferimento o Buono di Prelievo
          this.w_KEYSAL = SPACE(20)
          this.w_CODMAG = SPACE(5)
          this.w_CODMAT = SPACE(5)
          this.w_QTASAL = 0
          this.w_DISMAG = " "
          this.w_TOTALE = .F.
          if this.w_TIPRIG="E"
            * --- Se Elimina riga, Legge la riga dell' ODL_DETT associato con i riferimenti ai piani in cui esso e' inserito
            vq_exec("..\COLA\EXE\QUERY\GSCOIBMG.VQR",this,"APPO")
            if USED("APPO")
              SELECT APPO
              GO TOP
              LOCATE FOR NVL(PDSERIAL, SPACE(10))<>this.w_PDSERIAL AND NOT EMPTY(NVL(PDSERIAL,""))
              if NOT FOUND()
                * --- Non esistono altri piani associati all' ODL_DETT
                * --- Ripristina la Riga dell'ODL alla situazione Originaria
                GO TOP
                LOCATE FOR NVL(PDSERIAL, SPACE(10))=this.w_PDSERIAL
                * --- Legge i dati per il Ripristino della situazione Originaria
                this.w_KEYSAL = NVL(OLKEYSAL, SPACE(20))
                this.w_OLTCOMME = NVL(OLTCOMME, SPACE(15))
                this.w_OLTCOART = NVL(OLCODART, SPACE(20))
                this.w_CODMAG = NVL(OLMAGPRE, SPACE(5))
                this.w_CODMAT = NVL(OLMAGWIP, SPACE(5))
                this.w_QTASAL = NVL(OLQTASAL, 0)
                this.w_DISMAG = NVL(MGDISMAG, " ")
                * --- Read from MAGAZZIN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MGFLUBIC"+;
                    " from "+i_cTable+" MAGAZZIN where ";
                        +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MGFLUBIC;
                    from (i_cTable) where;
                        MGCODMAG = this.w_CODMAG;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if (nvl(this.w_FLUBIC,"N")<>"S" and empty(nvl(this.w_CODLOT,""))) or this.pTipOpe="E"
                  * --- Se ho l'ubicazione o il lotto potrei avere pi� righe riferite allo stesso componente ed a ubicazioni/lotti diversi
                  this.w_TOTALE = .T.
                endif
              endif
              USE
            endif
          endif
          if this.w_TOTALE=.T.
            * --- Ripristina l'ODL alla situazione Originaria (solo se cancellazione)
            if this.w_DISMAG="S"
              * --- Mag. Nettificabile
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
                +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
                +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
                +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                       )
              else
                update (i_cTable) set;
                    OLCODMAG = this.w_CODMAG;
                    ,OLQTAPRE = 0;
                    ,OLQTAPR1 = 0;
                    ,OLFLPREV = " ";
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;
                    and CPROWNUM = this.w_ROWODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura ODL_DETT (1)'
                return
              endif
            else
              * --- Se il Magazzino non e' nettificabile azzera la Causale
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
                +",OLCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCAUIMP),'ODL_DETT','OLCAUMAG');
                +",OLFLORDI ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLORDI');
                +",OLFLIMPE ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_DETT','OLFLIMPE');
                +",OLFLRISE ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLRISE');
                +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
                +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
                +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                       )
              else
                update (i_cTable) set;
                    OLCODMAG = this.w_CODMAG;
                    ,OLCAUMAG = this.w_PPCAUIMP;
                    ,OLFLORDI = " ";
                    ,OLFLIMPE = "+";
                    ,OLFLRISE = " ";
                    ,OLQTAPRE = 0;
                    ,OLQTAPR1 = 0;
                    ,OLFLPREV = " ";
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;
                    and CPROWNUM = this.w_ROWODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura ODL_DETT (2)'
                return
              endif
            endif
            if this.w_CODMAG<>this.w_CODMAT
              * --- Lo storno dell' Impegno avviene una volta sola
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OLTCOART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_QTASAL<>0 AND NOT EMPTY(this.w_CODMAG)
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_QTASAL);
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTIPER = SLQTIPER + this.w_QTASAL;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_KEYSAL;
                      and SLCODMAG = this.w_CODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDIART (4)'
                  return
                endif
                if this.w_SALCOM="S"
                  if empty(nvl(this.w_OLTCOMME,""))
                    this.w_COMMAPPO = this.w_COMMDEFA
                  else
                    this.w_COMMAPPO = this.w_OLTCOMME
                  endif
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_QTASAL);
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                        +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                        +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                           )
                  else
                    update (i_cTable) set;
                        SCQTIPER = SCQTIPER + this.w_QTASAL;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = this.w_KEYSAL;
                        and SCCODMAG = this.w_CODMAG;
                        and SCCODCAN = this.w_COMMAPPO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa (4)'
                    return
                  endif
                endif
              endif
              if this.w_DISMAG="S" AND this.w_QTASAL<>0 AND NOT EMPTY(this.w_CODMAT)
                * --- Il trasferimento dell'Impegno avviene una volta sola e solo se il Magazzino e' Nettificabile
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTIPER =SLQTIPER- "+cp_ToStrODBC(this.w_QTASAL);
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                         )
                else
                  update (i_cTable) set;
                      SLQTIPER = SLQTIPER - this.w_QTASAL;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_KEYSAL;
                      and SLCODMAG = this.w_CODMAT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDIART (5)'
                  return
                endif
                if this.w_SALCOM="S"
                  if empty(nvl(this.w_OLTCOMME,""))
                    this.w_COMMAPPO = this.w_COMMDEFA
                  else
                    this.w_COMMAPPO = this.w_OLTCOMME
                  endif
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTIPER =SCQTIPER- "+cp_ToStrODBC(this.w_QTASAL);
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                        +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                        +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                           )
                  else
                    update (i_cTable) set;
                        SCQTIPER = SCQTIPER - this.w_QTASAL;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = this.w_KEYSAL;
                        and SCCODMAG = this.w_CODMAT;
                        and SCCODCAN = this.w_COMMAPPO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa (5)'
                    return
                  endif
                endif
              endif
            endif
          else
            * --- ODL_DETT presente in altri piani o solo variazione, aggiorna il prelievo
            * --- Read from ODL_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLQTAUM1,CPROWORD"+;
                " from "+i_cTable+" ODL_DETT where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLQTAUM1,CPROWORD;
                from (i_cTable) where;
                    OLCODODL = this.w_CODODL;
                    and CPROWNUM = this.w_ROWODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_QTA1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
              this.w_ROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OLQTAPRE = IIF(this.w_TIPRIG="E", 0, this.w_QTAMOV) - this.w_QTAPRE
            this.w_OLQTAPR1 = IIF(this.w_TIPRIG="E", 0, this.w_QTAUM1) - this.w_QTAPR1
            if this.w_OLQTAPRE<>0 or this.w_TIPRIG="E"
              if this.w_FLPREV="S" and this.w_TIPRIG<>"E"
                * --- Se ho modificato la quantit� e tale quantit� non evade totalmente la riga chiedo cosa fare all'utente
                if this.w_QTAUM1<this.w_QTA1
                  this.TmpC = "Si � modificata la quantit� prelevata dell'ordine n. %1 riga %2%0Tale riga risulta attualmente evasa, la si vuole riaprire?"
                  if ah_YesNo(this.TmpC,"",this.w_CODODL,alltrim(str(this.w_ROWORD)))
                    this.w_FLPREV = " "
                  else
                    this.w_FLPREV = "S"
                  endif
                else
                  this.w_FLPREV = "S"
                endif
              else
                this.w_FLPREV = iif(this.w_QTAUM1<this.w_QTA1," ","S")
              endif
            else
              * --- Se sono in variazione e non ho toccato la quantit� il flag deve restare invariato
            endif
            if this.w_PDTIPGEN<>"BP"
              this.w_QTAMOV = this.w_OLQTAPRE
              this.w_QTAUM1 = this.w_OLQTAPR1
            endif
            if (nvl(this.w_FLUBIC,"N")<>"S" and this.pTipOpe="E" and this.w_OLQTAPRE=0) or this.w_PDTIPGEN<>"DT"
              if this.w_PDTIPGEN="BP"
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLQTAPRE =OLQTAPRE+ "+cp_ToStrODBC(this.w_OLQTAPRE);
                  +",OLQTAPR1 =OLQTAPR1+ "+cp_ToStrODBC(this.w_OLQTAPR1);
                  +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(this.w_FLPREV),'ODL_DETT','OLFLPREV');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                         )
                else
                  update (i_cTable) set;
                      OLQTAPRE = OLQTAPRE + this.w_OLQTAPRE;
                      ,OLQTAPR1 = OLQTAPR1 + this.w_OLQTAPR1;
                      ,OLFLPREV = this.w_FLPREV;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;
                      and CPROWNUM = this.w_ROWODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_DETT (5)'
                  return
                endif
              else
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
                  +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
                  +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(this.w_FLPREV),'ODL_DETT','OLFLPREV');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                         )
                else
                  update (i_cTable) set;
                      OLQTAPRE = 0;
                      ,OLQTAPR1 = 0;
                      ,OLFLPREV = this.w_FLPREV;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;
                      and CPROWNUM = this.w_ROWODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_DETT (5)'
                  return
                endif
              endif
            else
              * --- Write into ODL_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLQTAPRE =OLQTAPRE+ "+cp_ToStrODBC(this.w_OLQTAPRE);
                +",OLQTAPR1 =OLQTAPR1+ "+cp_ToStrODBC(this.w_OLQTAPR1);
                +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(this.w_FLPREV),'ODL_DETT','OLFLPREV');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                       )
              else
                update (i_cTable) set;
                    OLQTAPRE = OLQTAPRE + this.w_OLQTAPRE;
                    ,OLQTAPR1 = OLQTAPR1 + this.w_OLQTAPR1;
                    ,OLFLPREV = this.w_FLPREV;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;
                    and CPROWNUM = this.w_ROWODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura ODL_DETT (5)'
                return
              endif
            endif
          endif
      endcase
      if this.w_TIPRIG="E"
        * --- Elimina Riferimento Generazione ODL
        * --- Delete from RIF_GODL
        i_nConn=i_TableProp[this.RIF_GODL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PDSERIAL = "+cp_ToStrODBC(this.w_PDSERIAL);
                +" and PDTIPGEN = "+cp_ToStrODBC(this.w_PDTIPGEN);
                +" and PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
                +" and PDROWODL = "+cp_ToStrODBC(this.w_ROWODL);
                +" and PDROWDOC = "+cp_ToStrODBC(this.w_ROWDOC);
                 )
        else
          delete from (i_cTable) where;
                PDSERIAL = this.w_PDSERIAL;
                and PDTIPGEN = this.w_PDTIPGEN;
                and PDCODODL = this.w_CODODL;
                and PDROWODL = this.w_ROWODL;
                and PDROWDOC = this.w_ROWDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione dettaglio riferimenti'
          return
        endif
      else
        * --- Aggiorna QTA Riferimento Generazione ODL
        if this.w_QTAMOV<>0
          if this.w_PDTIPGEN<>"DT"
            * --- Write into RIF_GODL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RIF_GODL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RIF_GODL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PDQTAPRE ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'RIF_GODL','PDQTAPRE');
              +",PDQTAPR1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAUM1),'RIF_GODL','PDQTAPR1');
              +",PDFLPREV ="+cp_NullLink(cp_ToStrODBC(this.w_FLPREV),'RIF_GODL','PDFLPREV');
                  +i_ccchkf ;
              +" where ";
                  +"PDSERIAL = "+cp_ToStrODBC(this.w_PDSERIAL);
                  +" and PDTIPGEN = "+cp_ToStrODBC(this.w_PDTIPGEN);
                  +" and PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
                  +" and PDROWODL = "+cp_ToStrODBC(this.w_ROWODL);
                  +" and PDROWDOC = "+cp_ToStrODBC(this.w_ROWDOC);
                     )
            else
              update (i_cTable) set;
                  PDQTAPRE = this.w_QTAMOV;
                  ,PDQTAPR1 = this.w_QTAUM1;
                  ,PDFLPREV = this.w_FLPREV;
                  &i_ccchkf. ;
               where;
                  PDSERIAL = this.w_PDSERIAL;
                  and PDTIPGEN = this.w_PDTIPGEN;
                  and PDCODODL = this.w_CODODL;
                  and PDROWODL = this.w_ROWODL;
                  and PDROWDOC = this.w_ROWDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura RIF_GODL (4)'
              return
            endif
          else
            * --- Write into RIF_GODL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RIF_GODL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RIF_GODL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PDQTAPRE =PDQTAPRE+ "+cp_ToStrODBC(this.w_QTAMOV);
              +",PDQTAPR1 =PDQTAPR1+ "+cp_ToStrODBC(this.w_QTAUM1);
              +",PDFLPREV ="+cp_NullLink(cp_ToStrODBC(this.w_FLPREV),'RIF_GODL','PDFLPREV');
                  +i_ccchkf ;
              +" where ";
                  +"PDSERIAL = "+cp_ToStrODBC(this.w_PDSERIAL);
                  +" and PDTIPGEN = "+cp_ToStrODBC(this.w_PDTIPGEN);
                  +" and PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
                  +" and PDROWODL = "+cp_ToStrODBC(this.w_ROWODL);
                  +" and PDROWDOC = "+cp_ToStrODBC(this.w_ROWDOC);
                     )
            else
              update (i_cTable) set;
                  PDQTAPRE = PDQTAPRE + this.w_QTAMOV;
                  ,PDQTAPR1 = PDQTAPR1 + this.w_QTAUM1;
                  ,PDFLPREV = this.w_FLPREV;
                  &i_ccchkf. ;
               where;
                  PDSERIAL = this.w_PDSERIAL;
                  and PDTIPGEN = this.w_PDTIPGEN;
                  and PDCODODL = this.w_CODODL;
                  and PDROWODL = this.w_ROWODL;
                  and PDROWDOC = this.w_ROWDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura RIF_GODL (4)'
              return
            endif
          endif
        endif
      endif
      SELECT LISTAODL
      ENDSCAN
      if this.w_FLPROV="N" and this.w_FLPROV<>this.w_OFLPROV
        if USED("LISTAODL")
          SELECT LISTAODL
          GO TOP
          SCAN FOR NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0 AND empty(NVL(TIPRIG, ""))
          this.w_CODODL = PDCODODL
          this.w_ROWODL = PDROWODL
          this.w_ROWDOC = NVL(PDROWDOC, 0)
          this.w_TIPRIG = TIPRIG
          this.w_QTAPR1 = NVL(PDQTAPR1, 0)
          do case
            case this.w_PDTIPGEN="OR"
              this.w_CODMAG = " "
              this.w_KEYSAL = " "
              this.w_QTOEV1 = 0
              this.w_FLEVAS = " "
              * --- Read from ODL_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLTCOMAG,OLTKEYSA,OLTQTOE1,OLTFLEVA,OLTCOART,OLTCOMME"+;
                  " from "+i_cTable+" ODL_MAST where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLTCOMAG,OLTKEYSA,OLTQTOE1,OLTFLEVA,OLTCOART,OLTCOMME;
                  from (i_cTable) where;
                      OLCODODL = this.w_CODODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
                this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
                this.w_QTOEV1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
                this.w_FLEVAS = NVL(cp_ToDate(_read_.OLTFLEVA),cp_NullValue(_read_.OLTFLEVA))
                this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
                this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
                use
              else
                * --- Error: sql sentence error.
                i_Error = 'Errore in Lettura ODL_MAST'
                return
              endif
              select (i_nOldArea)
              * --- Nel Momento in cui viene confermato l'ordine a fornitore, storno
              *     l'rdinato dell'OCL e sbianco i Flag
              * --- Write into ODL_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(null),'ODL_MAST','OLTCAMAG');
                +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLORD');
                +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLIMP');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                       )
              else
                update (i_cTable) set;
                    OLTCAMAG = null;
                    ,OLTFLORD = " ";
                    ,OLTFLIMP = " ";
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura ODL_MAST'
                return
              endif
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER =SLQTOPER- "+cp_ToStrODBC(this.w_QTAPR1);
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = SLQTOPER - this.w_QTAPR1;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART (3)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OLTCOART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OLTCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OLTCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER =SCQTOPER- "+cp_ToStrODBC(this.w_QTAPR1);
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = SCQTOPER - this.w_QTAPR1;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa (3)'
                  return
                endif
              endif
          endcase
          SELECT LISTAODL
          ENDSCAN
        endif
      endif
      if USED("LISTAODL")
        * --- Elimina Cursore di Appoggio se esiste ancora
        use in LISTAODL
      endif
    endif
    WAIT CLEAR
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina tutti i Riferimenti ai DDT di Trasferimento
    * --- Lettura dell' ODL_DETT con i riferimenti ai piani in cui esso e' inserito
    vq_exec("..\COLA\EXE\QUERY\GSCOOBMG.VQR",this,"APPO")
    if USED("APPO")
      SELECT APPO
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PDSERIAL,"")) AND NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0
      * --- Legge i dati per il Ripristino della situazione Originaria
      this.w_SERODL = PDSERIAL
      this.w_CODODL = PDCODODL
      this.w_ROWODL = PDROWODL
      this.w_ROWDOC = PDROWDOC
      this.w_KEYSAL = NVL(OLKEYSAL, SPACE(20))
      this.w_OLTCOMME = NVL(OLTCOMME, SPACE(15))
      this.w_OLTCOART = NVL(OLCODART, SPACE(20))
      this.w_CODMAG = NVL(OLMAGPRE, SPACE(5))
      this.w_CODMAT = NVL(OLMAGWIP, SPACE(5))
      this.w_QTASAL = NVL(OLQTASAL, 0)
      this.w_DISMAG = NVL(MGDISMAG, " ")
      * --- Ripristina l'ODL_DETT alla situazione Pianificata
      if this.w_DISMAG="S"
        * --- Mag. Nettificabile
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
          +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
          +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
          +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                 )
        else
          update (i_cTable) set;
              OLCODMAG = this.w_CODMAG;
              ,OLQTAPRE = 0;
              ,OLQTAPR1 = 0;
              ,OLFLPREV = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;
              and CPROWNUM = this.w_ROWODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura ODL_DETT (1)'
          return
        endif
      else
        * --- Se il Magazzino non e' nettificabile azzera la Causale
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
          +",OLCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCAUIMP),'ODL_DETT','OLCAUMAG');
          +",OLFLORDI ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLORDI');
          +",OLFLIMPE ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_DETT','OLFLIMPE');
          +",OLFLRISE ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLRISE');
          +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
          +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
          +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                 )
        else
          update (i_cTable) set;
              OLCODMAG = this.w_CODMAG;
              ,OLCAUMAG = this.w_PPCAUIMP;
              ,OLFLORDI = " ";
              ,OLFLIMPE = "+";
              ,OLFLRISE = " ";
              ,OLQTAPRE = 0;
              ,OLQTAPR1 = 0;
              ,OLFLPREV = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;
              and CPROWNUM = this.w_ROWODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura ODL_DETT (2)'
          return
        endif
      endif
      if this.w_CODMAG<>this.w_CODMAT
        * --- Lo storno dell' Impegno avviene una volta sola
        if this.w_QTASAL<>0 AND NOT EMPTY(this.w_CODMAG)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_QTASAL);
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = SLQTIPER + this.w_QTASAL;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (4)'
            return
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_OLTCOMME,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_OLTCOMME
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_QTASAL);
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = SCQTIPER + this.w_QTASAL;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (4)'
              return
            endif
          endif
        endif
        if this.w_DISMAG="S" AND this.w_QTASAL<>0 AND NOT EMPTY(this.w_CODMAT)
          * --- Il trasferimento dell'Impegno avviene una volta sola e solo se il Magazzino e' Nettificabile
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER =SLQTIPER- "+cp_ToStrODBC(this.w_QTASAL);
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = SLQTIPER - this.w_QTASAL;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (5)'
            return
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_OLTCOMME,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_OLTCOMME
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER =SCQTIPER- "+cp_ToStrODBC(this.w_QTASAL);
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = SCQTIPER - this.w_QTASAL;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAT;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (5)'
              return
            endif
          endif
        endif
      endif
      * --- Elimina Riferimenti alle generazioni dei DDT di Trasferimento (i DDT gia' eseguiti non vengono modificati)
      * --- Delete from RIF_GODL
      i_nConn=i_TableProp[this.RIF_GODL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PDSERIAL = "+cp_ToStrODBC(this.w_SERODL);
              +" and PDTIPGEN = "+cp_ToStrODBC("DT");
              +" and PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
              +" and PDROWODL = "+cp_ToStrODBC(this.w_ROWODL);
              +" and PDROWDOC = "+cp_ToStrODBC(this.w_ROWDOC);
               )
      else
        delete from (i_cTable) where;
              PDSERIAL = this.w_SERODL;
              and PDTIPGEN = "DT";
              and PDCODODL = this.w_CODODL;
              and PDROWODL = this.w_ROWODL;
              and PDROWDOC = this.w_ROWDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione dettaglio riferimenti'
        return
      endif
      SELECT APPO
      ENDSCAN
      USE
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOpe,pSerDoc,pOFLPROV,pFLPROV)
    this.pTipOpe=pTipOpe
    this.pSerDoc=pSerDoc
    this.pOFLPROV=pOFLPROV
    this.pFLPROV=pFLPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='ODL_DETT'
    this.cWorkTables[3]='ODL_MAST'
    this.cWorkTables[4]='PAR_PROD'
    this.cWorkTables[5]='RIF_GODL'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='SALDICOM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe,pSerDoc,pOFLPROV,pFLPROV"
endproc
