* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bpe                                                        *
*              Seleziona periodo                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-17                                                      *
* Last revis.: 2002-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bpe",oParentObject)
return(i_retval)

define class tgsco_bpe as StdBatch
  * --- Local variables
  w_TPPERASS = space(3)
  * --- WorkFile variables
  MPS_TPER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona periodo OCL (da GSCL_KST)
    * --- Assegna periodo da Piano OCL
    vx_exec("..\COLA\EXE\QUERY\GSCOLPER.VZM",this)
    if NOT EMPTY(this.w_TPPERASS)
      this.oParentObject.w_PERINI = this.w_TPPERASS
      * --- Read from MPS_TPER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MPS_TPER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TPPERREL,TPDATINI,TPDATFIN"+;
          " from "+i_cTable+" MPS_TPER where ";
              +"TPPERASS = "+cp_ToStrODBC(this.oParentObject.w_PERINI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TPPERREL,TPDATINI,TPDATFIN;
          from (i_cTable) where;
              TPPERASS = this.oParentObject.w_PERINI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESPIN = NVL(cp_ToDate(_read_.TPPERREL),cp_NullValue(_read_.TPPERREL))
        this.oParentObject.w_DINIINI = NVL(cp_ToDate(_read_.TPDATINI),cp_NullValue(_read_.TPDATINI))
        this.oParentObject.w_DINIFIN = NVL(cp_ToDate(_read_.TPDATFIN),cp_NullValue(_read_.TPDATFIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MPS_TPER'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
