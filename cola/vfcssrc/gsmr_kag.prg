* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_kag                                                        *
*              Aggiorna commessa                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-06                                                      *
* Last revis.: 2014-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_kag",oParentObject))

* --- Class definition
define class tgsmr_kag as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 716
  Height = 357
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-18"
  HelpContextID=132737897
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  PAR_PROD_IDX = 0
  cPrg = "gsmr_kag"
  cComment = "Aggiorna commessa"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(2)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_CODRIC = space(20)
  w_DESRIC = space(40)
  w_UNIMIS = space(3)
  w_CODCOM = space(15)
  w_DESCOM = space(30)
  w_QTAAVA = 0
  w_NEWCOM = space(15)
  w_QTANUO = 0
  o_QTANUO = 0
  w_NEWMAG = space(5)
  w_CAUTRA = space(5)
  w_CAUSCA = space(5)
  w_DESNEW = space(30)
  w_ESI = 0
  o_ESI = 0
  w_OBTEST = ctod('  /  /  ')
  w_RIS = 0
  w_ORD = 0
  w_IMP = 0
  w_DIS = 0
  w_DIC = 0
  w_DESTRA = space(35)
  w_DESNMA = space(30)
  w_UNIMIS = space(3)
  w_DESCAR = space(35)
  w_CODSAL = space(40)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_AGGVAC = space(1)
  w_AGGVAS = space(1)
  w_CAUTRA1 = space(5)
  w_CAUSCA1 = space(5)
  w_UBICAZ = space(1)
  w_NEWUBI = space(1)
  w_LOTTI = space(1)
  w_MATRIC = space(1)
  w_OK = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_kagPag1","gsmr_kag",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNEWCOM_1_22
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='PAR_PROD'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(2)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_CODRIC=space(20)
      .w_DESRIC=space(40)
      .w_UNIMIS=space(3)
      .w_CODCOM=space(15)
      .w_DESCOM=space(30)
      .w_QTAAVA=0
      .w_NEWCOM=space(15)
      .w_QTANUO=0
      .w_NEWMAG=space(5)
      .w_CAUTRA=space(5)
      .w_CAUSCA=space(5)
      .w_DESNEW=space(30)
      .w_ESI=0
      .w_OBTEST=ctod("  /  /  ")
      .w_RIS=0
      .w_ORD=0
      .w_IMP=0
      .w_DIS=0
      .w_DIC=0
      .w_DESTRA=space(35)
      .w_DESNMA=space(30)
      .w_UNIMIS=space(3)
      .w_DESCAR=space(35)
      .w_CODSAL=space(40)
      .w_CODART=space(20)
      .w_CODVAR=space(20)
      .w_AGGVAC=space(1)
      .w_AGGVAS=space(1)
      .w_CAUTRA1=space(5)
      .w_CAUSCA1=space(5)
      .w_UBICAZ=space(1)
      .w_NEWUBI=space(1)
      .w_LOTTI=space(1)
      .w_MATRIC=space(1)
      .w_OK=.f.
      .w_CODMAG=oParentObject.w_CODMAG
      .w_DESMAG=oParentObject.w_DESMAG
      .w_CODRIC=oParentObject.w_CODRIC
      .w_DESRIC=oParentObject.w_DESRIC
      .w_UNIMIS=oParentObject.w_UNIMIS
      .w_CODCOM=oParentObject.w_CODCOM
      .w_DESCOM=oParentObject.w_DESCOM
      .w_NEWMAG=oParentObject.w_NEWMAG
      .w_ESI=oParentObject.w_ESI
      .w_RIS=oParentObject.w_RIS
      .w_ORD=oParentObject.w_ORD
      .w_IMP=oParentObject.w_IMP
      .w_DIS=oParentObject.w_DIS
      .w_DIC=oParentObject.w_DIC
      .w_DESNMA=oParentObject.w_DESNMA
      .w_UNIMIS=oParentObject.w_UNIMIS
      .w_CODSAL=oParentObject.w_CODSAL
      .w_CODART=oParentObject.w_CODART
      .w_CODVAR=oParentObject.w_CODVAR
      .w_UBICAZ=oParentObject.w_UBICAZ
      .w_NEWUBI=oParentObject.w_NEWUBI
      .w_LOTTI=oParentObject.w_LOTTI
      .w_MATRIC=oParentObject.w_MATRIC
        .w_READPAR = 'PP'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,8,.f.)
        .w_QTAAVA = .w_ESI-.w_QTANUO
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_NEWCOM))
          .link_1_22('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_NEWMAG))
          .link_1_24('Full')
        endif
        .w_CAUTRA = .w_CAUTRA1
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CAUTRA))
          .link_1_25('Full')
        endif
        .w_CAUSCA = .w_CAUSCA1
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CAUSCA))
          .link_1_26('Full')
        endif
          .DoRTCalc(15,16,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(18,32,.f.)
        if not(empty(.w_CAUTRA1))
          .link_1_56('Full')
        endif
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CAUSCA1))
          .link_1_57('Full')
        endif
    endwith
    this.DoRTCalc(34,38,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODMAG=.w_CODMAG
      .oParentObject.w_DESMAG=.w_DESMAG
      .oParentObject.w_CODRIC=.w_CODRIC
      .oParentObject.w_DESRIC=.w_DESRIC
      .oParentObject.w_UNIMIS=.w_UNIMIS
      .oParentObject.w_CODCOM=.w_CODCOM
      .oParentObject.w_DESCOM=.w_DESCOM
      .oParentObject.w_NEWMAG=.w_NEWMAG
      .oParentObject.w_ESI=.w_ESI
      .oParentObject.w_RIS=.w_RIS
      .oParentObject.w_ORD=.w_ORD
      .oParentObject.w_IMP=.w_IMP
      .oParentObject.w_DIS=.w_DIS
      .oParentObject.w_DIC=.w_DIC
      .oParentObject.w_DESNMA=.w_DESNMA
      .oParentObject.w_UNIMIS=.w_UNIMIS
      .oParentObject.w_CODSAL=.w_CODSAL
      .oParentObject.w_CODART=.w_CODART
      .oParentObject.w_CODVAR=.w_CODVAR
      .oParentObject.w_UBICAZ=.w_UBICAZ
      .oParentObject.w_NEWUBI=.w_NEWUBI
      .oParentObject.w_LOTTI=.w_LOTTI
      .oParentObject.w_MATRIC=.w_MATRIC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,8,.t.)
        if .o_ESI<>.w_ESI.or. .o_QTANUO<>.w_QTANUO
            .w_QTAAVA = .w_ESI-.w_QTANUO
        endif
        .DoRTCalc(10,31,.t.)
          .link_1_56('Full')
          .link_1_57('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,38,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCAUCCM,PPCAUSCM";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPCAUCCM,PPCAUSCM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(2))
      this.w_CAUTRA1 = NVL(_Link_.PPCAUCCM,space(5))
      this.w_CAUSCA1 = NVL(_Link_.PPCAUSCM,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(2)
      endif
      this.w_CAUTRA1 = space(5)
      this.w_CAUSCA1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWCOM
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_NEWCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_NEWCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oNEWCOM_1_22'),i_cWhere,'',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_NEWCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_NEWCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESNEW = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_NEWCOM = space(15)
      endif
      this.w_DESNEW = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWMAG
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_NEWMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_NEWMAG))
          select MGCODMAG,MGDESMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oNEWMAG_1_24'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_NEWMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_NEWMAG)
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESNMA = NVL(_Link_.MGDESMAG,space(30))
      this.w_NEWUBI = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWMAG = space(5)
      endif
      this.w_DESNMA = space(30)
      this.w_NEWUBI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUTRA
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUTRA))
          select CMCODICE,CMDESCRI,CMAGGVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUTRA)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CAUTRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CAUTRA)+"%");

            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUTRA) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUTRA_1_25'),i_cWhere,'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUTRA)
            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUTRA = NVL(_Link_.CMCODICE,space(5))
      this.w_DESTRA = NVL(_Link_.CMDESCRI,space(35))
      this.w_AGGVAC = NVL(_Link_.CMAGGVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUTRA = space(5)
      endif
      this.w_DESTRA = space(35)
      this.w_AGGVAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AGGVAC<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUTRA = space(5)
        this.w_DESTRA = space(35)
        this.w_AGGVAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSCA
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUSCA))
          select CMCODICE,CMDESCRI,CMAGGVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSCA)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CAUSCA)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CAUSCA)+"%");

            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUSCA) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUSCA_1_26'),i_cWhere,'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSCA)
            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSCA = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAR = NVL(_Link_.CMDESCRI,space(35))
      this.w_AGGVAS = NVL(_Link_.CMAGGVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSCA = space(5)
      endif
      this.w_DESCAR = space(35)
      this.w_AGGVAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AGGVAS<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUSCA = space(5)
        this.w_DESCAR = space(35)
        this.w_AGGVAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUTRA1
  func Link_1_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUTRA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUTRA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUTRA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUTRA1)
            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUTRA1 = NVL(_Link_.CMCODICE,space(5))
      this.w_DESTRA = NVL(_Link_.CMDESCRI,space(35))
      this.w_AGGVAC = NVL(_Link_.CMAGGVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUTRA1 = space(5)
      endif
      this.w_DESTRA = space(35)
      this.w_AGGVAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AGGVAC<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUTRA1 = space(5)
        this.w_DESTRA = space(35)
        this.w_AGGVAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUTRA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSCA1
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSCA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSCA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSCA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSCA1)
            select CMCODICE,CMDESCRI,CMAGGVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSCA1 = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAR = NVL(_Link_.CMDESCRI,space(35))
      this.w_AGGVAS = NVL(_Link_.CMAGGVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSCA1 = space(5)
      endif
      this.w_DESCAR = space(35)
      this.w_AGGVAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AGGVAS<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUSCA1 = space(5)
        this.w_DESCAR = space(35)
        this.w_AGGVAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSCA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_14.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_14.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_15.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_15.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRIC_1_16.value==this.w_CODRIC)
      this.oPgFrm.Page1.oPag.oCODRIC_1_16.value=this.w_CODRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIC_1_17.value==this.w_DESRIC)
      this.oPgFrm.Page1.oPag.oDESRIC_1_17.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_18.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_18.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_19.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_19.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_20.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_20.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAAVA_1_21.value==this.w_QTAAVA)
      this.oPgFrm.Page1.oPag.oQTAAVA_1_21.value=this.w_QTAAVA
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWCOM_1_22.value==this.w_NEWCOM)
      this.oPgFrm.Page1.oPag.oNEWCOM_1_22.value=this.w_NEWCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oQTANUO_1_23.value==this.w_QTANUO)
      this.oPgFrm.Page1.oPag.oQTANUO_1_23.value=this.w_QTANUO
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWMAG_1_24.value==this.w_NEWMAG)
      this.oPgFrm.Page1.oPag.oNEWMAG_1_24.value=this.w_NEWMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUTRA_1_25.value==this.w_CAUTRA)
      this.oPgFrm.Page1.oPag.oCAUTRA_1_25.value=this.w_CAUTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSCA_1_26.value==this.w_CAUSCA)
      this.oPgFrm.Page1.oPag.oCAUSCA_1_26.value=this.w_CAUSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEW_1_27.value==this.w_DESNEW)
      this.oPgFrm.Page1.oPag.oDESNEW_1_27.value=this.w_DESNEW
    endif
    if not(this.oPgFrm.Page1.oPag.oESI_1_30.value==this.w_ESI)
      this.oPgFrm.Page1.oPag.oESI_1_30.value=this.w_ESI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIS_1_32.value==this.w_RIS)
      this.oPgFrm.Page1.oPag.oRIS_1_32.value=this.w_RIS
    endif
    if not(this.oPgFrm.Page1.oPag.oORD_1_33.value==this.w_ORD)
      this.oPgFrm.Page1.oPag.oORD_1_33.value=this.w_ORD
    endif
    if not(this.oPgFrm.Page1.oPag.oIMP_1_34.value==this.w_IMP)
      this.oPgFrm.Page1.oPag.oIMP_1_34.value=this.w_IMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDIS_1_35.value==this.w_DIS)
      this.oPgFrm.Page1.oPag.oDIS_1_35.value=this.w_DIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDIC_1_36.value==this.w_DIC)
      this.oPgFrm.Page1.oPag.oDIC_1_36.value=this.w_DIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRA_1_42.value==this.w_DESTRA)
      this.oPgFrm.Page1.oPag.oDESTRA_1_42.value=this.w_DESTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNMA_1_45.value==this.w_DESNMA)
      this.oPgFrm.Page1.oPag.oDESNMA_1_45.value=this.w_DESNMA
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_47.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_47.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAR_1_48.value==this.w_DESCAR)
      this.oPgFrm.Page1.oPag.oDESCAR_1_48.value=this.w_DESCAR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_QTANUO<=.w_DIS)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oQTANUO_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUTRA)) or not(.w_AGGVAC<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUTRA_1_25.SetFocus()
            i_bnoObbl = !empty(.w_CAUTRA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUSCA)) or not(.w_AGGVAS<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSCA_1_26.SetFocus()
            i_bnoObbl = !empty(.w_CAUSCA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUTRA1)) or not(.w_AGGVAC<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUTRA1_1_56.SetFocus()
            i_bnoObbl = !empty(.w_CAUTRA1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUSCA1)) or not(.w_AGGVAS<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSCA1_1_57.SetFocus()
            i_bnoObbl = !empty(.w_CAUSCA1)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_QTANUO = this.w_QTANUO
    this.o_ESI = this.w_ESI
    return

enddefine

* --- Define pages as container
define class tgsmr_kagPag1 as StdContainer
  Width  = 712
  height = 357
  stdWidth  = 712
  stdheight = 357
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODMAG_1_14 as StdField with uid="FKTFUWTYFZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 58206246,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=109, Top=7, InputMask=replicate('X',5)

  add object oDESMAG_1_15 as StdField with uid="STGIQIZUST",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 58265142,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=194, Top=7, InputMask=replicate('X',30)

  add object oCODRIC_1_16 as StdField with uid="MQZWLATWFH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODRIC", cQueryName = "CODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 268249126,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=109, Top=31, InputMask=replicate('X',20)

  add object oDESRIC_1_17 as StdField with uid="WAYWKHKUDD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 268308022,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=272, Top=31, InputMask=replicate('X',40)

  add object oUNIMIS_1_18 as StdField with uid="XNFAALGVQF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 267941958,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=109, Top=55, InputMask=replicate('X',3)

  add object oCODCOM_1_19 as StdField with uid="GPABDDBGTG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 172894246,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=153, InputMask=replicate('X',15)

  add object oDESCOM_1_20 as StdField with uid="EEFMINQOTJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 172953142,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=229, Top=153, InputMask=replicate('X',30)

  add object oQTAAVA_1_21 as StdField with uid="MAOXJGCEFD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_QTAAVA", cQueryName = "QTAAVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 247201286,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=109, Top=177, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oNEWCOM_1_22 as StdField with uid="LUDKACNWWC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NEWCOM", cQueryName = "NEWCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa di destinazione",;
    HelpContextID = 172969686,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=144, Top=233, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_NEWCOM"

  func oNEWCOM_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWCOM_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWCOM_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oNEWCOM_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"COMMESSE",'',this.parent.oContained
  endproc

  add object oQTANUO_1_23 as StdField with uid="BZGDDWCDAU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_QTANUO", cQueryName = "QTANUO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą da trasferire",;
    HelpContextID = 213450246,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=264, Top=257, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func oQTANUO_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_QTANUO<=.w_DIS)
    endwith
    return bRes
  endfunc

  add object oNEWMAG_1_24 as StdField with uid="CVVOGKTUNL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NEWMAG", cQueryName = "NEWMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di destinazione",;
    HelpContextID = 58281686,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_NEWMAG"

  func oNEWMAG_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWMAG_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWMAG_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oNEWMAG_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oCAUTRA_1_25 as StdField with uid="YGQANEDFIO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CAUTRA", cQueryName = "CAUTRA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino da utilizzare per il carico",;
    HelpContextID = 244328998,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=144, Top=305, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUTRA"

  func oCAUTRA_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUTRA_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUTRA_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUTRA_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oCAUTRA_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CAUTRA
     i_obj.ecpSave()
  endproc

  add object oCAUSCA_1_26 as StdField with uid="PKLZVWYRRE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CAUSCA", cQueryName = "CAUSCA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino da utilizzare per lo scarico",;
    HelpContextID = 228534822,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=144, Top=329, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUSCA"

  func oCAUSCA_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSCA_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSCA_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUSCA_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oCAUSCA_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CAUSCA
     i_obj.ecpSave()
  endproc

  add object oDESNEW_1_27 as StdField with uid="VKILDQHATA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESNEW", cQueryName = "DESNEW",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 62524982,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=264, Top=233, InputMask=replicate('X',30)


  add object oBtn_1_28 as StdButton with uid="SJTIKSWOLY",left=603, top=307, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il movimento di trasferimento";
    , HelpContextID = 226643478;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSMR_BAG(this.Parent.oContained,"AGG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_QTANUO) and not empty(.w_CAUTRA) and not empty(.w_CAUSCA) and not empty(.w_NEWMAG))
      endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="DAGYNAJGDG",left=655, top=307, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 226643478;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oESI_1_30 as StdField with uid="VNNJFENLRR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ESI", cQueryName = "ESI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132416442,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=109, Top=98, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oRIS_1_32 as StdField with uid="TARTBHGAHQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_RIS", cQueryName = "RIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132377834,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=207, Top=98, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oORD_1_33 as StdField with uid="DSMPBMJNSR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ORD", cQueryName = "ORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132437018,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=305, Top=98, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oIMP_1_34 as StdField with uid="BCPZXTNYLK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_IMP", cQueryName = "IMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132389242,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=403, Top=98, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oDIS_1_35 as StdField with uid="IKOSBYOYYY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DIS", cQueryName = "DIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132378058,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=501, Top=98, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oDIC_1_36 as StdField with uid="QNFTNWLDXR",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DIC", cQueryName = "DIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132443594,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=599, Top=98, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oDESTRA_1_42 as StdField with uid="VIPSIOWLPD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESTRA", cQueryName = "DESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244321846,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=208, Top=305, InputMask=replicate('X',35)

  add object oDESNMA_1_45 as StdField with uid="PYHUMFXQKL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESNMA", cQueryName = "DESNMA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 238685750,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=229, Top=281, InputMask=replicate('X',30)

  add object oUNIMIS_1_47 as StdField with uid="RLHKFIIVOQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 267941958,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=144, Top=257, InputMask=replicate('X',3)

  add object oDESCAR_1_48 as StdField with uid="FBPOIWIHWK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242159158,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=208, Top=329, InputMask=replicate('X',35)

  add object oStr_1_2 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=20, Top=9,;
    Alignment=1, Width=83, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="SEMWLMFXCN",Visible=.t., Left=42, Top=33,;
    Alignment=1, Width=61, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="WRAEOEHQIV",Visible=.t., Left=109, Top=79,;
    Alignment=0, Width=97, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="CYVSTVWPFX",Visible=.t., Left=57, Top=57,;
    Alignment=1, Width=46, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FDKAMZOQXS",Visible=.t., Left=29, Top=126,;
    Alignment=0, Width=82, Height=15,;
    Caption="Origine"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="VXJGVVEGAN",Visible=.t., Left=29, Top=206,;
    Alignment=0, Width=82, Height=15,;
    Caption="Destinazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NMQQVISSLX",Visible=.t., Left=34, Top=155,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="YNTZFEUDEW",Visible=.t., Left=37, Top=179,;
    Alignment=1, Width=66, Height=15,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GHZHFWCBXH",Visible=.t., Left=57, Top=235,;
    Alignment=1, Width=81, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="HGHFUPXIIH",Visible=.t., Left=207, Top=259,;
    Alignment=1, Width=55, Height=15,;
    Caption="Quantitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="NYKYMMZMYS",Visible=.t., Left=207, Top=79,;
    Alignment=0, Width=97, Height=15,;
    Caption="Riservato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BEQTXNQDHM",Visible=.t., Left=403, Top=79,;
    Alignment=0, Width=97, Height=15,;
    Caption="Impegnato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="YEYCYJBFGS",Visible=.t., Left=501, Top=79,;
    Alignment=0, Width=97, Height=15,;
    Caption="Disponibilitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="EHVZXECOWB",Visible=.t., Left=305, Top=79,;
    Alignment=0, Width=97, Height=15,;
    Caption="Ordinato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="YTJCAXZKNV",Visible=.t., Left=599, Top=79,;
    Alignment=0, Width=97, Height=15,;
    Caption="Disp. contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="DOXIKFONET",Visible=.t., Left=9, Top=307,;
    Alignment=1, Width=129, Height=15,;
    Caption="Causale carico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="MEVQCTQXLU",Visible=.t., Left=58, Top=283,;
    Alignment=1, Width=80, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="TICQVEQGTN",Visible=.t., Left=60, Top=259,;
    Alignment=1, Width=78, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="UXOBSPWRET",Visible=.t., Left=9, Top=331,;
    Alignment=1, Width=129, Height=15,;
    Caption="Causale scarico:"  ;
  , bGlobalFont=.t.

  add object oBox_1_6 as StdBox with uid="MMTRPJCQXR",left=7, top=142, width=697,height=1

  add object oBox_1_8 as StdBox with uid="DKGLGGKHDX",left=7, top=222, width=697,height=1

  add object oBox_1_53 as StdBox with uid="RSJXAVBWSQ",left=106, top=94, width=598,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_kag','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
