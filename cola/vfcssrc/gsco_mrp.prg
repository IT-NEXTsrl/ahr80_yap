* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mrp                                                        *
*              Risorse di produzione                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_37]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-15                                                      *
* Last revis.: 2016-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mrp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mrp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mrp")
  return

* --- Class definition
define class tgsco_mrp as StdPCForm
  Width  = 672
  Height = 413
  Top    = 10
  Left   = 10
  cComment = "Risorse di produzione"
  cPrg = "gsco_mrp"
  HelpContextID=154557079
  add object cnt as tcgsco_mrp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mrp as PCContext
  w_RPSERIAL = space(10)
  w_QTAUM1 = 0
  w_CALCPICT = 0
  w_CPROWORD = 0
  w_RPCODRIS = space(15)
  w_DESRIS = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CODVAL = space(3)
  w_PREZZO = 0
  w_CAOVAL = 0
  w_PRENAZ = 0
  w_RPUNIMIS = space(3)
  w_RPQTAMOV = 0
  w_PREUM = 0
  w_TOTDAR = 0
  w_TOTALE = 0
  w_RPQTAUNI = 0
  w_RPSETUP = space(1)
  w_RPIMTORI = 0
  proc Save(i_oFrom)
    this.w_RPSERIAL = i_oFrom.w_RPSERIAL
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_RPCODRIS = i_oFrom.w_RPCODRIS
    this.w_DESRIS = i_oFrom.w_DESRIS
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_PREZZO = i_oFrom.w_PREZZO
    this.w_CAOVAL = i_oFrom.w_CAOVAL
    this.w_PRENAZ = i_oFrom.w_PRENAZ
    this.w_RPUNIMIS = i_oFrom.w_RPUNIMIS
    this.w_RPQTAMOV = i_oFrom.w_RPQTAMOV
    this.w_PREUM = i_oFrom.w_PREUM
    this.w_TOTDAR = i_oFrom.w_TOTDAR
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_RPQTAUNI = i_oFrom.w_RPQTAUNI
    this.w_RPSETUP = i_oFrom.w_RPSETUP
    this.w_RPIMTORI = i_oFrom.w_RPIMTORI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_RPSERIAL = this.w_RPSERIAL
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_RPCODRIS = this.w_RPCODRIS
    i_oTo.w_DESRIS = this.w_DESRIS
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_PREZZO = this.w_PREZZO
    i_oTo.w_CAOVAL = this.w_CAOVAL
    i_oTo.w_PRENAZ = this.w_PRENAZ
    i_oTo.w_RPUNIMIS = this.w_RPUNIMIS
    i_oTo.w_RPQTAMOV = this.w_RPQTAMOV
    i_oTo.w_PREUM = this.w_PREUM
    i_oTo.w_TOTDAR = this.w_TOTDAR
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_RPQTAUNI = this.w_RPQTAUNI
    i_oTo.w_RPSETUP = this.w_RPSETUP
    i_oTo.w_RPIMTORI = this.w_RPIMTORI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mrp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 672
  Height = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-26"
  HelpContextID=154557079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  RIS_PROD_IDX = 0
  TAB_RISO_IDX = 0
  UNIMIS_IDX = 0
  cFile = "RIS_PROD"
  cKeySelect = "RPSERIAL"
  cKeyWhere  = "RPSERIAL=this.w_RPSERIAL"
  cKeyDetail  = "RPSERIAL=this.w_RPSERIAL"
  cKeyWhereODBC = '"RPSERIAL="+cp_ToStrODBC(this.w_RPSERIAL)';

  cKeyDetailWhereODBC = '"RPSERIAL="+cp_ToStrODBC(this.w_RPSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"RIS_PROD.RPSERIAL="+cp_ToStrODBC(this.w_RPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RIS_PROD.CPROWORD '
  cPrg = "gsco_mrp"
  cComment = "Risorse di produzione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 18
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RPSERIAL = space(10)
  w_QTAUM1 = 0
  o_QTAUM1 = 0
  w_CALCPICT = 0
  w_CPROWORD = 0
  w_RPCODRIS = space(15)
  o_RPCODRIS = space(15)
  w_DESRIS = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CODVAL = space(3)
  w_PREZZO = 0
  w_CAOVAL = 0
  w_PRENAZ = 0
  w_RPUNIMIS = space(3)
  o_RPUNIMIS = space(3)
  w_RPQTAMOV = 0
  o_RPQTAMOV = 0
  w_PREUM = 0
  w_TOTDAR = 0
  w_TOTALE = 0
  w_RPQTAUNI = 0
  w_RPSETUP = space(1)
  o_RPSETUP = space(1)
  w_RPIMTORI = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mrpPag1","gsco_mrp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TAB_RISO'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='RIS_PROD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIS_PROD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIS_PROD_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mrp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from RIS_PROD where RPSERIAL=KeySet.RPSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_PROD_IDX,2],this.bLoadRecFilter,this.RIS_PROD_IDX,"gsco_mrp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIS_PROD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIS_PROD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIS_PROD '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RPSERIAL',this.w_RPSERIAL  )
      select * from (i_cTable) RIS_PROD where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_QTAUM1 = this.oParentObject.w_DPQTAPR1 + this.oParentObject.w_DPQTASC1
        .w_TOTALE = 0
        .w_RPSERIAL = NVL(RPSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('('+g_PERVAL+') :',RGB(0,0,0))
        .w_CALCPICT = DEFPIC(g_PERPVL)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate('('+g_PERVAL+')',RGB(0,0,0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'RIS_PROD')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_DESRIS = space(40)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_CODVAL = space(3)
          .w_PREZZO = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RPCODRIS = NVL(RPCODRIS,space(15))
          if link_2_2_joined
            this.w_RPCODRIS = NVL(RICODICE202,NVL(this.w_RPCODRIS,space(15)))
            this.w_DESRIS = NVL(RIDESCRI202,space(40))
            this.w_UNMIS1 = NVL(RIUNMIS1202,space(3))
            this.w_UNMIS2 = NVL(RIUNMIS2202,space(3))
            this.w_OPERAT = NVL(RIOPERAT202,space(1))
            this.w_MOLTIP = NVL(RIMOLTIP202,0)
            this.w_PREZZO = NVL(RIPREZZO202,0)
            this.w_CODVAL = NVL(RICODVAL202,space(3))
          else
          .link_2_2('Load')
          endif
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
          .w_RPUNIMIS = NVL(RPUNIMIS,space(3))
          * evitabile
          *.link_2_12('Load')
          .w_RPQTAMOV = NVL(RPQTAMOV,0)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_RPUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_TOTDAR = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
          .w_RPQTAUNI = NVL(RPQTAUNI,0)
          .w_RPSETUP = NVL(RPSETUP,space(1))
          .w_RPIMTORI = NVL(RPIMTORI,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_RPIMTORI
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('('+g_PERVAL+') :',RGB(0,0,0))
        .w_CALCPICT = DEFPIC(g_PERPVL)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate('('+g_PERVAL+')',RGB(0,0,0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RPSERIAL=space(10)
      .w_QTAUM1=0
      .w_CALCPICT=0
      .w_CPROWORD=10
      .w_RPCODRIS=space(15)
      .w_DESRIS=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_CODVAL=space(3)
      .w_PREZZO=0
      .w_CAOVAL=0
      .w_PRENAZ=0
      .w_RPUNIMIS=space(3)
      .w_RPQTAMOV=0
      .w_PREUM=0
      .w_TOTDAR=0
      .w_TOTALE=0
      .w_RPQTAUNI=0
      .w_RPSETUP=space(1)
      .w_RPIMTORI=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_QTAUM1 = this.oParentObject.w_DPQTAPR1 + this.oParentObject.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('('+g_PERVAL+') :',RGB(0,0,0))
        .w_CALCPICT = DEFPIC(g_PERPVL)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate('('+g_PERVAL+')',RGB(0,0,0))
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_RPCODRIS))
         .link_2_2('Full')
        endif
        .DoRTCalc(6,12,.f.)
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        .w_RPUNIMIS = .w_UNMIS1
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_RPUNIMIS))
         .link_2_12('Full')
        endif
        .w_RPQTAMOV = .w_RPQTAUNI * IIF(.w_RPSETUP<> "S", .w_QTAUM1, 1)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_RPUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_TOTDAR = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
        .DoRTCalc(19,19,.f.)
        .w_RPQTAUNI = IIF(.w_RPQTAUNI=0, .w_RPQTAMOV, .w_RPQTAUNI)
        .w_RPSETUP = 'N'
        .w_RPIMTORI = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIS_PROD')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'RIS_PROD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RPSERIAL,"RPSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_RPCODRIS C(15);
      ,t_DESRIS C(40);
      ,t_RPUNIMIS C(3);
      ,t_RPQTAMOV N(10,3);
      ,t_RPSETUP N(3);
      ,t_RPIMTORI N(18,4);
      ,CPROWNUM N(10);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_CODVAL C(3);
      ,t_PREZZO N(18,4);
      ,t_CAOVAL N(12,7);
      ,t_PRENAZ N(18,4);
      ,t_PREUM N(18,4);
      ,t_TOTDAR N(18,4);
      ,t_RPQTAUNI N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mrpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRPCODRIS_2_2.controlsource=this.cTrsName+'.t_RPCODRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_3.controlsource=this.cTrsName+'.t_DESRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRPUNIMIS_2_12.controlsource=this.cTrsName+'.t_RPUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRPQTAMOV_2_13.controlsource=this.cTrsName+'.t_RPQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRPSETUP_2_17.controlsource=this.cTrsName+'.t_RPSETUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRPIMTORI_2_18.controlsource=this.cTrsName+'.t_RPIMTORI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(54)
    this.AddVLine(169)
    this.AddVLine(390)
    this.AddVLine(412)
    this.AddVLine(451)
    this.AddVLine(531)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_PROD_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_PROD_IDX,2])
      *
      * insert into RIS_PROD
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIS_PROD')
        i_extval=cp_InsertValODBCExtFlds(this,'RIS_PROD')
        i_cFldBody=" "+;
                  "(RPSERIAL,CPROWORD,RPCODRIS,RPUNIMIS,RPQTAMOV"+;
                  ",RPQTAUNI,RPSETUP,RPIMTORI,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RPSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_RPCODRIS)+","+cp_ToStrODBCNull(this.w_RPUNIMIS)+","+cp_ToStrODBC(this.w_RPQTAMOV)+;
             ","+cp_ToStrODBC(this.w_RPQTAUNI)+","+cp_ToStrODBC(this.w_RPSETUP)+","+cp_ToStrODBC(this.w_RPIMTORI)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIS_PROD')
        i_extval=cp_InsertValVFPExtFlds(this,'RIS_PROD')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RPSERIAL',this.w_RPSERIAL)
        INSERT INTO (i_cTable) (;
                   RPSERIAL;
                  ,CPROWORD;
                  ,RPCODRIS;
                  ,RPUNIMIS;
                  ,RPQTAMOV;
                  ,RPQTAUNI;
                  ,RPSETUP;
                  ,RPIMTORI;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RPSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_RPCODRIS;
                  ,this.w_RPUNIMIS;
                  ,this.w_RPQTAMOV;
                  ,this.w_RPQTAUNI;
                  ,this.w_RPSETUP;
                  ,this.w_RPIMTORI;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_PROD_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_RPCODRIS) AND t_RPQTAMOV<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_PROD')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_PROD')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_RPCODRIS) AND t_RPQTAMOV<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RIS_PROD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_PROD')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",RPCODRIS="+cp_ToStrODBCNull(this.w_RPCODRIS)+;
                     ",RPUNIMIS="+cp_ToStrODBCNull(this.w_RPUNIMIS)+;
                     ",RPQTAMOV="+cp_ToStrODBC(this.w_RPQTAMOV)+;
                     ",RPQTAUNI="+cp_ToStrODBC(this.w_RPQTAUNI)+;
                     ",RPSETUP="+cp_ToStrODBC(this.w_RPSETUP)+;
                     ",RPIMTORI="+cp_ToStrODBC(this.w_RPIMTORI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_PROD')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,RPCODRIS=this.w_RPCODRIS;
                     ,RPUNIMIS=this.w_RPUNIMIS;
                     ,RPQTAMOV=this.w_RPQTAMOV;
                     ,RPQTAUNI=this.w_RPQTAUNI;
                     ,RPSETUP=this.w_RPSETUP;
                     ,RPIMTORI=this.w_RPIMTORI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_PROD_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_RPCODRIS) AND t_RPQTAMOV<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete RIS_PROD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_RPCODRIS) AND t_RPQTAMOV<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIS_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_PROD_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('('+g_PERVAL+') :',RGB(0,0,0))
        .DoRTCalc(1,2,.t.)
          .w_CALCPICT = DEFPIC(g_PERPVL)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate('('+g_PERVAL+')',RGB(0,0,0))
        .DoRTCalc(4,12,.t.)
        if .o_RPCODRIS<>.w_RPCODRIS
          .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        endif
        if .o_RPCODRIS<>.w_RPCODRIS
          .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        endif
        if .o_RPCODRIS<>.w_RPCODRIS
          .w_RPUNIMIS = .w_UNMIS1
          .link_2_12('Full')
        endif
        if .o_RPSETUP<>.w_RPSETUP.or. .o_QTAUM1<>.w_QTAUM1
          .w_RPQTAMOV = .w_RPQTAUNI * IIF(.w_RPSETUP<> "S", .w_QTAUM1, 1)
        endif
        if .o_RPCODRIS<>.w_RPCODRIS.or. .o_RPUNIMIS<>.w_RPUNIMIS
          .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_RPUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        endif
        if .o_RPQTAMOV<>.w_RPQTAMOV
          .w_TOTDAR = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
        endif
        .DoRTCalc(19,19,.t.)
          .w_RPQTAUNI = IIF(.w_RPQTAUNI=0, .w_RPQTAMOV, .w_RPQTAUNI)
        .DoRTCalc(21,21,.t.)
        if .o_RPQTAMOV<>.w_RPQTAMOV.or. .o_RPUNIMIS<>.w_RPUNIMIS
          .w_TOTALE = .w_TOTALE-.w_rpimtori
          .w_RPIMTORI = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
          .w_TOTALE = .w_TOTALE+.w_rpimtori
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_CODVAL with this.w_CODVAL
      replace t_PREZZO with this.w_PREZZO
      replace t_CAOVAL with this.w_CAOVAL
      replace t_PRENAZ with this.w_PRENAZ
      replace t_PREUM with this.w_PREUM
      replace t_TOTDAR with this.w_TOTDAR
      replace t_RPQTAUNI with this.w_RPQTAUNI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate('('+g_PERVAL+') :',RGB(0,0,0))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate('('+g_PERVAL+')',RGB(0,0,0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRPUNIMIS_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRPUNIMIS_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRPIMTORI_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRPIMTORI_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RPCODRIS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_lTable = "TAB_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2], .t., this.TAB_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RPCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ARI',True,'TAB_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_RPCODRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_RPCODRIS))
          select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RPCODRIS)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RPCODRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_RISO','*','RICODICE',cp_AbsName(oSource.parent,'oRPCODRIS_2_2'),i_cWhere,'GSDS_ARI',"Elenco risorse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RPCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_RPCODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_RPCODRIS)
            select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RPCODRIS = NVL(_Link_.RICODICE,space(15))
      this.w_DESRIS = NVL(_Link_.RIDESCRI,space(40))
      this.w_UNMIS1 = NVL(_Link_.RIUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.RIUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.RIOPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.RIMOLTIP,0)
      this.w_PREZZO = NVL(_Link_.RIPREZZO,0)
      this.w_CODVAL = NVL(_Link_.RICODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_RPCODRIS = space(15)
      endif
      this.w_DESRIS = space(40)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_PREZZO = 0
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RPCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_RISO_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.RICODICE as RICODICE202"+ ",link_2_2.RIDESCRI as RIDESCRI202"+ ",link_2_2.RIUNMIS1 as RIUNMIS1202"+ ",link_2_2.RIUNMIS2 as RIUNMIS2202"+ ",link_2_2.RIOPERAT as RIOPERAT202"+ ",link_2_2.RIMOLTIP as RIMOLTIP202"+ ",link_2_2.RIPREZZO as RIPREZZO202"+ ",link_2_2.RICODVAL as RICODVAL202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on RIS_PROD.RPCODRIS=link_2_2.RICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and RIS_PROD.RPCODRIS=link_2_2.RICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RPUNIMIS
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RPUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RPUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_RPUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RPUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RPUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oRPUNIMIS_2_12'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSDS_MCS.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RPUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RPUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_RPUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RPUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_RPUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_RPUNIMIS, .w_UNMIS1, .w_UNMIS2, '   ')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_RPUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RPUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPCODRIS_2_2.value==this.w_RPCODRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPCODRIS_2_2.value=this.w_RPCODRIS
      replace t_RPCODRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPCODRIS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_3.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_3.value=this.w_DESRIS
      replace t_DESRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPUNIMIS_2_12.value==this.w_RPUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPUNIMIS_2_12.value=this.w_RPUNIMIS
      replace t_RPUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPUNIMIS_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPQTAMOV_2_13.value==this.w_RPQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPQTAMOV_2_13.value=this.w_RPQTAMOV
      replace t_RPQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPQTAMOV_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPSETUP_2_17.RadioValue()==this.w_RPSETUP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPSETUP_2_17.SetRadio()
      replace t_RPSETUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPSETUP_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPIMTORI_2_18.value==this.w_RPIMTORI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPIMTORI_2_18.value=this.w_RPIMTORI
      replace t_RPIMTORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPIMTORI_2_18.value
    endif
    cp_SetControlsValueExtFlds(this,'RIS_PROD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_RPUNIMIS) or not(CHKUNIMI(.w_RPUNIMIS, .w_UNMIS1, .w_UNMIS2, '   '))) and (NOT EMPTY(.w_RPCODRIS)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_RPCODRIS) AND .w_RPQTAMOV<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPUNIMIS_2_12
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      if not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_RPCODRIS) AND .w_RPQTAMOV<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_QTAUM1 = this.w_QTAUM1
    this.o_RPCODRIS = this.w_RPCODRIS
    this.o_RPUNIMIS = this.w_RPUNIMIS
    this.o_RPQTAMOV = this.w_RPQTAMOV
    this.o_RPSETUP = this.w_RPSETUP
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND NOT EMPTY(t_RPCODRIS) AND t_RPQTAMOV<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_RPCODRIS=space(15)
      .w_DESRIS=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_CODVAL=space(3)
      .w_PREZZO=0
      .w_CAOVAL=0
      .w_PRENAZ=0
      .w_RPUNIMIS=space(3)
      .w_RPQTAMOV=0
      .w_PREUM=0
      .w_TOTDAR=0
      .w_RPQTAUNI=0
      .w_RPSETUP=space(1)
      .w_RPIMTORI=0
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_RPCODRIS))
        .link_2_2('Full')
      endif
      .DoRTCalc(6,12,.f.)
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        .w_RPUNIMIS = .w_UNMIS1
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_RPUNIMIS))
        .link_2_12('Full')
      endif
        .w_RPQTAMOV = .w_RPQTAUNI * IIF(.w_RPSETUP<> "S", .w_QTAUM1, 1)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_RPUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_TOTDAR = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
      .DoRTCalc(19,19,.f.)
        .w_RPQTAUNI = IIF(.w_RPQTAUNI=0, .w_RPQTAMOV, .w_RPQTAUNI)
        .w_RPSETUP = 'N'
        .w_RPIMTORI = cp_ROUND(.w_PREUM*.w_RPQTAMOV, g_PERPVL)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RPCODRIS = t_RPCODRIS
    this.w_DESRIS = t_DESRIS
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_CODVAL = t_CODVAL
    this.w_PREZZO = t_PREZZO
    this.w_CAOVAL = t_CAOVAL
    this.w_PRENAZ = t_PRENAZ
    this.w_RPUNIMIS = t_RPUNIMIS
    this.w_RPQTAMOV = t_RPQTAMOV
    this.w_PREUM = t_PREUM
    this.w_TOTDAR = t_TOTDAR
    this.w_RPQTAUNI = t_RPQTAUNI
    this.w_RPSETUP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPSETUP_2_17.RadioValue(.t.)
    this.w_RPIMTORI = t_RPIMTORI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RPCODRIS with this.w_RPCODRIS
    replace t_DESRIS with this.w_DESRIS
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_CODVAL with this.w_CODVAL
    replace t_PREZZO with this.w_PREZZO
    replace t_CAOVAL with this.w_CAOVAL
    replace t_PRENAZ with this.w_PRENAZ
    replace t_RPUNIMIS with this.w_RPUNIMIS
    replace t_RPQTAMOV with this.w_RPQTAMOV
    replace t_PREUM with this.w_PREUM
    replace t_TOTDAR with this.w_TOTDAR
    replace t_RPQTAUNI with this.w_RPQTAUNI
    replace t_RPSETUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRPSETUP_2_17.ToRadio()
    replace t_RPIMTORI with this.w_RPIMTORI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_rpimtori
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mrpPag1 as StdContainer
  Width  = 668
  height = 413
  stdWidth  = 668
  stdheight = 413
  resizeXpos=330
  resizeYpos=173
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_4 as cp_calclbl with uid="HUBIDDYKUA",left=467, top=386, width=41,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 204316186


  add object oObj_1_6 as cp_calclbl with uid="SDXMFEVZAY",left=619, top=11, width=37,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 204316186


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=11, width=651,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Posiz.",Field2="RPCODRIS",Label2="Risorsa",Field3="DESRIS",Label3="Descrizione operazione",Field4="RPSETUP",Label4="Set",Field5="RPUNIMIS",Label5="U.M.",Field6="RPQTAMOV",Label6="Quantit�",Field7="RPIMTORI",Label7="Importo totale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202212474

  add object oStr_1_3 as StdString with uid="HOOINDKSII",Visible=.t., Left=328, Top=386,;
    Alignment=1, Width=131, Height=18,;
    Caption="Costo totale"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=30,;
    width=647+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*18*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=31,width=646+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*18*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TAB_RISO|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TAB_RISO'
        oDropInto=this.oBodyCol.oRow.oRPCODRIS_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oRPUNIMIS_2_12
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="DOFEMETEME",rtseq=19,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Importo totale del ciclo semplificato",;
    HelpContextID = 214110410,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=512, Top=383, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]
enddefine

* --- Defining Body row
define class tgsco_mrpBodyRow as CPBodyRowCnt
  Width=637
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="YIKODDOLMS",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza temporale di elaborazione del ciclo semplificato",;
    HelpContextID = 33894550,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oRPCODRIS_2_2 as StdTrsField with uid="FMJVHAAQYN",rtseq=5,rtrep=.t.,;
    cFormVar="w_RPCODRIS",value=space(15),;
    ToolTipText = "Codice della risorsa impegnata",;
    HelpContextID = 264888425,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=43, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TAB_RISO", cZoomOnZoom="GSDS_ARI", oKey_1_1="RICODICE", oKey_1_2="this.w_RPCODRIS"

  func oRPCODRIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRPCODRIS_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRPCODRIS_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_RISO','*','RICODICE',cp_AbsName(this.parent,'oRPCODRIS_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ARI',"Elenco risorse",'',this.parent.oContained
  endproc
  proc oRPCODRIS_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ARI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RICODICE=this.parent.oContained.w_RPCODRIS
    i_obj.ecpSave()
  endproc

  add object oDESRIS_2_3 as StdTrsField with uid="ILSXFZEPEV",rtseq=6,rtrep=.t.,;
    cFormVar="w_DESRIS",value=space(40),enabled=.f.,;
    HelpContextID = 249703370,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=216, Left=159, Top=0, InputMask=replicate('X',40)

  add object oRPUNIMIS_2_12 as StdTrsField with uid="NNLDJJETXR",rtseq=15,rtrep=.t.,;
    cFormVar="w_RPUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura associata alla risorsa",;
    HelpContextID = 186253417,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=33, Left=403, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_RPUNIMIS"

  func oRPUNIMIS_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_RPCODRIS))
    endwith
  endfunc

  func oRPUNIMIS_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oRPUNIMIS_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRPUNIMIS_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oRPUNIMIS_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSDS_MCS.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oRPUNIMIS_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_RPUNIMIS
    i_obj.ecpSave()
  endproc

  add object oRPQTAMOV_2_13 as StdTrsField with uid="PTAACKZZQF",rtseq=16,rtrep=.t.,;
    cFormVar="w_RPQTAMOV",value=0,;
    HelpContextID = 90193812,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=74, Left=441, Top=0, cSayPict=[v_PQ(10)], cGetPict=[v_GQ(10)]

  add object oRPSETUP_2_17 as StdTrsCheck with uid="XRVBANMNFN",rtrep=.t.,;
    cFormVar="w_RPSETUP",  caption="",;
    ToolTipText = "Check fase di setup (costo fisso)",;
    HelpContextID = 205463530,;
    Left=380, Top=0, Width=16,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oRPSETUP_2_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RPSETUP,&i_cF..t_RPSETUP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oRPSETUP_2_17.GetRadio()
    this.Parent.oContained.w_RPSETUP = this.RadioValue()
    return .t.
  endfunc

  func oRPSETUP_2_17.ToRadio()
    this.Parent.oContained.w_RPSETUP=trim(this.Parent.oContained.w_RPSETUP)
    return(;
      iif(this.Parent.oContained.w_RPSETUP=='S',1,;
      0))
  endfunc

  func oRPSETUP_2_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oRPIMTORI_2_18 as StdTrsField with uid="QVGOUWMOPK",rtseq=22,rtrep=.t.,;
    cFormVar="w_RPIMTORI",value=0,;
    HelpContextID = 37207969,;
    cTotal = "this.Parent.oContained.w_totale", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=521, Top=0

  func oRPIMTORI_2_18.mCond()
    with this.Parent.oContained
      return (.w_RPQTAMOV>0)
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=17
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mrp','RIS_PROD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RPSERIAL=RIS_PROD.RPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
