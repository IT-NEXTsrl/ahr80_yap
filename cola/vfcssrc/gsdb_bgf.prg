* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bgf                                                        *
*              Generazione MPS                                                 *
*                                                                              *
*      Author: TAM Software Srl (SM)                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-08-02                                                      *
* Last revis.: 2012-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bgf",oParentObject,m.pOper)
return(i_retval)

define class tgsdb_bgf as StdBatch
  * --- Local variables
  pOper = space(1)
  w_DataElab = ctod("  /  /  ")
  w_PPNMAXPE = 0
  PunPAD = .NULL.
  TmpN = 0
  TmpC = space(10)
  TmpD = ctod("  /  /  ")
  cicla = .f.
  TmpN1 = 0
  TmpC1 = space(10)
  TmpD1 = ctod("  /  /  ")
  TmpN2 = 0
  TmpC2 = space(10)
  TmpD2 = ctod("  /  /  ")
  i = 0
  TmpC3 = space(10)
  TmpD3 = ctod("  /  /  ")
  TotPer = 0
  cCURKEY = space(40)
  DatProgr = ctod("  /  /  ")
  NumPer = 0
  w_MPSERIAL = space(15)
  DatEla = ctod("  /  /  ")
  cPREVIS = 0
  cCODART = space(20)
  cDatFinMPS = ctod("  /  /  ")
  cORDINI = 0
  cCODVAR = space(20)
  cDatIniMPS = ctod("  /  /  ")
  cIMPPRO = 0
  cPERASS = space(3)
  cODPCOR = 0
  cCODICE = space(41)
  cORDFOR = 0
  cKEYSAL = space(40)
  cONHAND = 0
  cUNMIS1 = space(3)
  cMPSSUG = 0
  w_CALSTA = space(5)
  cPABCUM = 0
  w_GIOSCA = 0
  cMPSSUG = 0
  cMPSTOT = 0
  cRESPRE = 0
  cDOMANDA = 0
  OldPeriodo = 0
  cLeadTime = 0
  w_SERIALE = space(10)
  w_CRITFORN = space(1)
  w_MICOM = space(1)
  w_MIODL = space(1)
  w_MIOCL = space(1)
  w_MIODA = space(1)
  w_MAGFOR = space(5)
  w_MAGFOC = space(5)
  w_MAGFOL = space(5)
  w_MAGFOA = space(5)
  w_PRVMPS = space(1)
  w_ODA = space(1)
  w_ODF = space(1)
  * --- WorkFile variables
  MPS_TPER_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametri per generazione scorta
    this.w_DataElab = i_datsys
    * --- Punta al padre
    this.PunPAD = this.oParentObject
    do case
      case this.pOper="G"
        * --- La variabili w_ELAODF e w_ELAMPS vengono utilizzate dalle queries come filter parameters per discrimanare o meno l'elaborazione
        *     degli articoli oggetto MPS con provenienza preferenziale esterna:
        *     
        *     w_ELAODF='S' vengono presi in considerazione articoli con provenienza esterna
        *     w_ELAMPS='S' vengono presi in considerazione articoli con provenienza <> esterna
        * --- Data di partenza elaborazione
        this.w_DataElab = i_datsys
        * --- Legge da tabella parametri il calendario std
        this.w_PPNMAXPE = 0
        this.w_CALSTA = " "
        this.w_GIOSCA = 0
        this.w_CRITFORN = this.oParentObject.w_CRIFOR
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPCALSTA,PPGIOSCA,PPNMAXPE"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPCALSTA,PPGIOSCA,PPNMAXPE;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
          this.w_GIOSCA = NVL(cp_ToDate(_read_.PPGIOSCA),cp_NullValue(_read_.PPGIOSCA))
          this.w_PPNMAXPE = NVL(cp_ToDate(_read_.PPNMAXPE),cp_NullValue(_read_.PPNMAXPE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PPNMAXPE = iif(this.w_PPNMAXPE=0,54,this.w_PPNMAXPE)
        if empty(this.w_CALSTA)
          ah_ErrorMSG("Calendario di stabilimento non specificato (tabella parametri)",16)
          i_retcode = 'stop'
          return
        endif
        * --- Ricalcola bidoni temporali ....
        if this.oParentObject.w_FLTEMPOR="S"
          do GSCO_BCB with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Inizia cancellando tutti gli ODP suggeriti
        * --- Try
        local bErr_04D86120
        bErr_04D86120=bTrsErr
        this.Try_04D86120()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_Errormsg("Errore nell'inizializzazione del database",16)
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_04D86120
        * --- End
        * --- Lancia programma di pianificazione articoli a scorta
        * --- Parametri per generazione scorta
        this.w_MICOM = "M"
        this.w_MIODL = "M"
        this.w_MIOCL = "M"
        this.w_MIODA = "M"
        this.w_MAGFOR = SPACE(5)
        this.w_MAGFOC = SPACE(5)
        this.w_MAGFOL = SPACE(5)
        this.w_MAGFOA = SPACE(5)
        if this.oParentObject.w_ELASCORTA = "S"
          * --- w_ODA:Parametro di GSCO_BGS.BTCDEF per generazione a scorta materie prime
          this.w_ODA = "N"
          this.w_PRVMPS = "S"
          if this.oParentObject.w_ELAODF="S"
            this.w_ODF = "S"
            if this.oParentObject.w_PARAM="F"
              this.w_ODA = "S"
            endif
          endif
          GSCO_BGS(this,"M")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- LANCIA PROGRAMMA DI PIANIFICAZIONE MATERIALI A FABBISOGNO (OGGETTI MPS o RICAMBI)
        do GSDB_BRI with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Riassegna time-buckets
        do GSDB_BTB with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Messaggio Finale
        ah_ErrorMsg("Elaborazione completata",64)
        * --- Chiude Padre
        this.PunPAD.ECPQuit()     
      case this.pOper="T"
        if this.PunPAD.w_PARAM="F"
          this.PunPAD.cComment = "Generazione proposte ordini fornitori"
        else
          this.PunPAD.cComment = "Pianificazione ordini di produzione (MPS)"
        endif
        this.PunPAD.Caption = this.PunPAD.cComment
    endcase
  endproc
  proc Try_04D86120()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Cancella gli ODL Suggeriti
    * --- Delete from ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
    
      do vq_exec with '..\cola\exe\query\gsdb1bgf',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Azzera Periodo Assoluto per tutti gli ODP rimasti
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OLCODODL"
      do vq_exec with '..\cola\exe\query\gsdb2bgf',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC(space(3)),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" from "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST, "+i_cQueryTable+" _t2 set ";
      +"ODL_MAST.OLTPERAS ="+cp_NullLink(cp_ToStrODBC(space(3)),'ODL_MAST','OLTPERAS');
          +Iif(Empty(i_ccchkf),"",",ODL_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ODL_MAST.OLCODODL = t2.OLCODODL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set (";
          +"OLTPERAS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(space(3)),'ODL_MAST','OLTPERAS')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ODL_MAST.OLCODODL = _t2.OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_MAST set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC(space(3)),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC(space(3)),'ODL_MAST','OLTPERAS');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MPS_TPER'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='PAR_PROD'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
