* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_adp                                                        *
*              Dichiarazioni di produzione                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_172]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-15                                                      *
* Last revis.: 2016-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_adp"))

* --- Class definition
define class tgsco_adp as StdForm
  Top    = 6
  Left   = 10

  * --- Standard Properties
  Width  = 726
  Height = 455+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-06"
  HelpContextID=175528599
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=98

  * --- Constant Properties
  DIC_PROD_IDX = 0
  ODL_MAST_IDX = 0
  PAR_PROD_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  KEY_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  cpusers_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  CAM_AGAZ_IDX = 0
  TIP_DOCU_IDX = 0
  UNIMIS_IDX = 0
  cFile = "DIC_PROD"
  cKeySelect = "DPSERIAL"
  cKeyWhere  = "DPSERIAL=this.w_DPSERIAL"
  cKeyWhereODBC = '"DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cKeyWhereODBCqualified = '"DIC_PROD.DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cPrg = "gsco_adp"
  cComment = "Dichiarazioni di produzione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DPSERIAL = space(10)
  w_DPDATREG = ctod('  /  /  ')
  w_DPNUMOPE = 0
  w_READPAR = space(10)
  w_CAUSCA = space(5)
  w_CAUCAR = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DPTIPATT = space(1)
  w_DPCODODL = space(15)
  o_DPCODODL = space(15)
  w_OLTSTATO = space(1)
  w_OLTPROVE = space(1)
  w_OLTQTODL = 0
  w_OLTQTOEV = 0
  w_QTARES = 0
  w_OLTFLEVA = space(1)
  w_DPCODART = space(20)
  o_DPCODART = space(20)
  w_FLLOTT = space(1)
  w_OLTUNMIS = space(3)
  w_DPCODMAG = space(5)
  o_DPCODMAG = space(5)
  w_FLUBIC = space(1)
  w_DPCODICE = space(20)
  w_DPUNIMIS = space(3)
  w_TIPVOC = space(1)
  w_DPQTAPRO = 0
  o_DPQTAPRO = 0
  w_DPQTASCA = 0
  o_DPQTASCA = 0
  w_DPFLEVAS = space(1)
  w_DPDATINI = ctod('  /  /  ')
  w_DPORAINI = space(5)
  w_DPDATFIN = ctod('  /  /  ')
  w_DPORAFIN = space(5)
  w_STADOC = space(10)
  w_DPCODCEN = space(15)
  w_DESCON = space(40)
  w_DPCODVOC = space(15)
  w_VOCDES = space(40)
  w_DPCODCOM = space(15)
  w_DPCODATT = space(15)
  w_DESCAN = space(40)
  w_DESATT = space(30)
  w_DPCODLOT = space(20)
  o_DPCODLOT = space(20)
  w_DPCODUBI = space(20)
  o_DPCODUBI = space(20)
  w_DTOBVO = ctod('  /  /  ')
  w_CODCOS = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_DESART = space(40)
  w_DESMAG = space(30)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_DPQTAPR1 = 0
  w_DPQTASC1 = 0
  w_QTAUM1 = 0
  w_DATOBSO = ctod('  /  /  ')
  w_DESUTE = space(20)
  w_OLTQTOD1 = 0
  w_OLTQTOE1 = 0
  w_TESTFORM = .F.
  w_DPRIFCAR = space(10)
  w_DPRIFSCA = space(10)
  w_OLTKEYSA = space(20)
  w_STATOGES = space(10)
  w_OLTQTSAL = 0
  w_OLTCICLO = space(15)
  w_MGSCAR = space(5)
  w_CRIVAL = space(2)
  w_ARTLOT = space(20)
  w_FLSTAT = space(1)
  w_MATENABL = .F.
  w_DPCOMMAT = space(1)
  w_GESMAT = space(1)
  w_EDCOMMAT = .F.
  w_CAUMGC = space(5)
  w_MTCARI = space(1)
  w_FLUBIC1 = space(1)
  w_MATCAR = 0
  w_MATSCA = 0
  w_RIGMOVLOT = .F.
  w_FLUSEP = space(1)
  w_MODUM2 = space(1)
  w_IMPLOT = space(1)
  w_ROWDOC = 0
  w_NUMRIF = 0
  w_FLPRG = space(1)
  w_SERFAS = space(15)
  w_SECIC = space(10)
  w_DPRIFMOU = space(10)
  w_CAUMOU = space(5)
  w_FASE = 0
  w_OLTSECPR = space(15)
  w_DPODLFAS = space(15)
  w_DPCPRFAS = 0
  w_DPFASOUT = space(1)
  w_DPULTFAS = space(1)
  w_ODLSOSPE = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DPSERIAL = this.W_DPSERIAL

  * --- Children pointers
  GSCO_MMP = .NULL.
  GSCO_MRP = .NULL.
  GSCO_MMT = .NULL.
  GSCO_MCO = .NULL.
  GSCO_MOU = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_adp
  * Varibile che se valorizzatat a .t. alla Pressione dell'F10 oppure del tasto ESC chiude la maschera
  w_AUTOCLOSE=.f.
  
    proc ecpsave()
  	  DoDefault()
  	  * Se attivo Flussi e Autorizzazioni chiudo la maschera a seguito della sua conferma
      if this.w_AUTOCLOSE
  		  * Se bUpdated=.f. significa che la registrazione e' stata salvata sul database con successo	
  		  * e quindi posso chiudere la maschera
  		  if Not this.bUpdated
  			  this.ecpquit()
  		  endif
  	  endif
    endproc
  
    proc ecpquit()
  	  DoDefault()
  	  * Se attivo Flussi e Autorizzazioni chiudo la maschera a seguito del primo Esc (Abbandoni le modifiche?)
      if this.w_AUTOCLOSE
  		  * Se lo stato non � di modifica allora chiudo la maschera
  		  * Il codice all'interno dell'IF e' preso dalla CP_FORMS
  		  if !inlist(this.cFunction,'Edit','Load')
  		  	this.NotifyEvent("Done")
  		  	this.Hide()
  		  	this.Release()
  		  EndiF
  	  Endif
    endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIC_PROD','gsco_adp')
    stdPageFrame::Init()
    *set procedure to GSCO_MMT additive
    *set procedure to GSCO_MCO additive
    with this
      .Pages(1).addobject("oPag","tgsco_adpPag1","gsco_adp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dichiarazione")
      .Pages(1).HelpContextID = 113616585
      .Pages(2).addobject("oPag","tgsco_adpPag2","gsco_adp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Materiali")
      .Pages(2).HelpContextID = 83687486
      .Pages(3).addobject("oPag","tgsco_adpPag3","gsco_adp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Risorse")
      .Pages(3).HelpContextID = 84736278
      .Pages(4).addobject("oPag","tgsco_adpPag4","gsco_adp",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Materiali di output")
      .Pages(4).HelpContextID = 59104202
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDPSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCO_MMT
    *release procedure GSCO_MCO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='VOC_COST'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='cpusers'
    this.cWorkTables[11]='LOTTIART'
    this.cWorkTables[12]='UBICAZIO'
    this.cWorkTables[13]='CAM_AGAZ'
    this.cWorkTables[14]='TIP_DOCU'
    this.cWorkTables[15]='UNIMIS'
    this.cWorkTables[16]='DIC_PROD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(16))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_PROD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_PROD_IDX,3]
  return

  function CreateChildren()
    this.GSCO_MMP = CREATEOBJECT('stdDynamicChild',this,'GSCO_MMP',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSCO_MMP.createrealchild()
    this.GSCO_MRP = CREATEOBJECT('stdDynamicChild',this,'GSCO_MRP',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSCO_MRP.createrealchild()
    this.GSCO_MMT = CREATEOBJECT('stdLazyChild',this,'GSCO_MMT')
    this.GSCO_MCO = CREATEOBJECT('stdLazyChild',this,'GSCO_MCO')
    this.GSCO_MOU = CREATEOBJECT('stdDynamicChild',this,'GSCO_MOU',this.oPgFrm.Page4.oPag.oLinkPC_4_1)
    this.GSCO_MOU.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCO_MMP)
      this.GSCO_MMP.DestroyChildrenChain()
      this.GSCO_MMP=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.GSCO_MRP)
      this.GSCO_MRP.DestroyChildrenChain()
      this.GSCO_MRP=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSCO_MMT)
      this.GSCO_MMT.DestroyChildrenChain()
      this.GSCO_MMT=.NULL.
    endif
    if !ISNULL(this.GSCO_MCO)
      this.GSCO_MCO.DestroyChildrenChain()
      this.GSCO_MCO=.NULL.
    endif
    if !ISNULL(this.GSCO_MOU)
      this.GSCO_MOU.DestroyChildrenChain()
      this.GSCO_MOU=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCO_MMP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MRP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MMT.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MCO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MOU.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCO_MMP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MRP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MMT.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MCO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MOU.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCO_MMP.NewDocument()
    this.GSCO_MRP.NewDocument()
    this.GSCO_MMT.NewDocument()
    this.GSCO_MCO.NewDocument()
    this.GSCO_MOU.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCO_MMP.SetKey(;
            .w_DPSERIAL,"MPSERIAL";
            ,.w_ROWDOC,"MPROWDOC";
            ,.w_NUMRIF,"MPNUMRIF";
            )
      this.GSCO_MRP.SetKey(;
            .w_DPSERIAL,"RPSERIAL";
            )
      this.GSCO_MMT.SetKey(;
            .w_DPSERIAL,"MTSERIAL";
            )
      this.GSCO_MCO.SetKey(;
            .w_DPSERIAL,"MTSERIAL";
            )
      this.GSCO_MOU.SetKey(;
            .w_DPSERIAL,"MDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCO_MMP.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MPSERIAL";
             ,.w_ROWDOC,"MPROWDOC";
             ,.w_NUMRIF,"MPNUMRIF";
             )
      .GSCO_MRP.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"RPSERIAL";
             )
      .GSCO_MMT.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MTSERIAL";
             )
      .GSCO_MCO.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MTSERIAL";
             )
      .WriteTo_GSCO_MCO()
      .GSCO_MOU.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCO_MMP)
        i_f=.GSCO_MMP.BuildFilter()
        if !(i_f==.GSCO_MMP.cQueryFilter)
          i_fnidx=.GSCO_MMP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MMP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MMP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MMP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MMP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MRP)
        i_f=.GSCO_MRP.BuildFilter()
        if !(i_f==.GSCO_MRP.cQueryFilter)
          i_fnidx=.GSCO_MRP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MRP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MRP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MRP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MRP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MOU)
        i_f=.GSCO_MOU.BuildFilter()
        if !(i_f==.GSCO_MOU.cQueryFilter)
          i_fnidx=.GSCO_MOU.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MOU.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MOU.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MOU.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MOU.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSCO_MCO()
  if at('gsco_mco',lower(this.GSCO_MCO.class))<>0
    if this.GSCO_MCO.cnt.w_CODLOTTES<>this.w_DPCODLOT or this.GSCO_MCO.cnt.w_CODUBITES<>this.w_DPCODUBI or this.GSCO_MCO.cnt.w_CODUBI<>this.w_DPCODUBI
      this.GSCO_MCO.cnt.w_CODLOTTES = this.w_DPCODLOT
      this.GSCO_MCO.cnt.w_CODUBITES = this.w_DPCODUBI
      this.GSCO_MCO.cnt.w_CODUBI = this.w_DPCODUBI
      this.GSCO_MCO.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DPSERIAL = NVL(DPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_39_joined
    link_1_39_joined=.f.
    local link_1_42_joined
    link_1_42_joined=.f.
    local link_1_45_joined
    link_1_45_joined=.f.
    local link_1_47_joined
    link_1_47_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIC_PROD where DPSERIAL=KeySet.DPSERIAL
    *
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_PROD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_PROD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_PROD '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_39_joined=this.AddJoinedLink_1_39(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_42_joined=this.AddJoinedLink_1_42(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_45_joined=this.AddJoinedLink_1_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_47_joined=this.AddJoinedLink_1_47(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CAUSCA = space(5)
        .w_CAUCAR = space(5)
        .w_OBTEST = i_datsys
        .w_OLTSTATO = space(1)
        .w_OLTPROVE = space(1)
        .w_OLTQTODL = 0
        .w_OLTQTOEV = 0
        .w_OLTFLEVA = space(1)
        .w_FLLOTT = space(1)
        .w_OLTUNMIS = space(3)
        .w_FLUBIC = space(1)
        .w_TIPVOC = space(1)
        .w_STADOC = space(10)
        .w_DESCON = space(40)
        .w_VOCDES = space(40)
        .w_DESCAN = space(40)
        .w_DESATT = space(30)
        .w_DTOBVO = ctod("  /  /  ")
        .w_CODCOS = space(5)
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESART = space(40)
        .w_DESMAG = space(30)
        .w_UNMIS3 = space(3)
        .w_OPERA3 = space(1)
        .w_MOLTI3 = 0
        .w_UNMIS1 = space(3)
        .w_OPERAT = space(1)
        .w_MOLTIP = 0
        .w_UNMIS2 = space(3)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESUTE = space(20)
        .w_OLTQTOD1 = 0
        .w_OLTQTOE1 = 0
        .w_TESTFORM = .T.
        .w_OLTKEYSA = space(20)
        .w_OLTQTSAL = 0
        .w_OLTCICLO = space(15)
        .w_MGSCAR = space(5)
        .w_ARTLOT = space(20)
        .w_FLSTAT = space(1)
        .w_EDCOMMAT = True
        .w_CAUMGC = space(5)
        .w_MTCARI = space(1)
        .w_FLUBIC1 = space(1)
        .w_MATCAR = -1
        .w_MATSCA = -1
        .w_RIGMOVLOT = .f.
        .w_FLUSEP = space(1)
        .w_MODUM2 = space(1)
        .w_IMPLOT = space(1)
        .w_ROWDOC = 0
        .w_NUMRIF = -50
        .w_SERFAS = space(15)
        .w_SECIC = space(10)
        .w_CAUMOU = space(5)
        .w_FASE = 0
        .w_OLTSECPR = space(15)
        .w_DPODLFAS = space(15)
        .w_ODLSOSPE = space(1)
        .w_DPSERIAL = NVL(DPSERIAL,space(10))
        .op_DPSERIAL = .w_DPSERIAL
        .w_DPDATREG = NVL(cp_ToDate(DPDATREG),ctod("  /  /  "))
        .w_DPNUMOPE = NVL(DPNUMOPE,0)
          .link_1_3('Load')
        .w_READPAR = 'PP'
          .link_1_4('Load')
          .link_1_6('Load')
        .w_DPTIPATT = NVL(DPTIPATT,space(1))
        .w_DPCODODL = NVL(DPCODODL,space(15))
          if link_1_10_joined
            this.w_DPCODODL = NVL(OLCODODL110,NVL(this.w_DPCODODL,space(15)))
            this.w_OLTSTATO = NVL(OLTSTATO110,space(1))
            this.w_OLTPROVE = NVL(OLTPROVE110,space(1))
            this.w_OLTUNMIS = NVL(OLTUNMIS110,space(3))
            this.w_DPCODICE = NVL(OLTCODIC110,space(20))
            this.w_DPCODMAG = NVL(OLTCOMAG110,space(5))
            this.w_OLTQTODL = NVL(OLTQTODL110,0)
            this.w_OLTQTOEV = NVL(OLTQTOEV110,0)
            this.w_OLTFLEVA = NVL(OLTFLEVA110,space(1))
            this.w_DPCODCEN = NVL(OLTCOCEN110,space(15))
            this.w_DPCODVOC = NVL(OLTVOCEN110,space(15))
            this.w_DPCODCOM = NVL(OLTCOMME110,space(15))
            this.w_DPCODATT = NVL(OLTCOATT110,space(15))
            this.w_DPCODART = NVL(OLTCOART110,space(20))
            this.w_OLTQTOD1 = NVL(OLTQTOD1110,0)
            this.w_OLTQTOE1 = NVL(OLTQTOE1110,0)
            this.w_OLTKEYSA = NVL(OLTKEYSA110,space(20))
            this.w_OLTQTSAL = NVL(OLTQTSAL110,0)
            this.w_OLTCICLO = NVL(OLTCICLO110,space(15))
            this.w_SERFAS = NVL(OLTSEODL110,space(15))
            this.w_SECIC = NVL(OLTSECIC110,space(10))
            this.w_ODLSOSPE = NVL(OLTSOSPE110,space(1))
          else
          .link_1_10('Load')
          endif
        .w_QTARES = MAX(0, .w_OLTQTODL-.w_OLTQTOEV)
        .w_DPCODART = NVL(DPCODART,space(20))
          if link_1_17_joined
            this.w_DPCODART = NVL(ARCODART117,NVL(this.w_DPCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1117,space(3))
            this.w_OPERAT = NVL(AROPERAT117,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP117,0)
            this.w_UNMIS2 = NVL(ARUNMIS2117,space(3))
            this.w_FLLOTT = NVL(ARFLLOTT117,space(1))
            this.w_GESMAT = NVL(ARGESMAT117,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_DPCODMAG = NVL(DPCODMAG,space(5))
          if link_1_20_joined
            this.w_DPCODMAG = NVL(MGCODMAG120,NVL(this.w_DPCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG120,space(30))
            this.w_FLUBIC = NVL(MGFLUBIC120,space(1))
          else
          .link_1_20('Load')
          endif
        .w_DPCODICE = NVL(DPCODICE,space(20))
          if link_1_22_joined
            this.w_DPCODICE = NVL(CACODICE122,NVL(this.w_DPCODICE,space(20)))
            this.w_DESART = NVL(CADESART122,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS122,space(3))
            this.w_OPERA3 = NVL(CAOPERAT122,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP122,0)
          else
          .link_1_22('Load')
          endif
        .w_DPUNIMIS = NVL(DPUNIMIS,space(3))
        .w_DPQTAPRO = NVL(DPQTAPRO,0)
        .w_DPQTASCA = NVL(DPQTASCA,0)
        .w_DPFLEVAS = NVL(DPFLEVAS,space(1))
        .w_DPDATINI = NVL(cp_ToDate(DPDATINI),ctod("  /  /  "))
        .w_DPORAINI = NVL(DPORAINI,space(5))
        .w_DPDATFIN = NVL(cp_ToDate(DPDATFIN),ctod("  /  /  "))
        .w_DPORAFIN = NVL(DPORAFIN,space(5))
        .w_DPCODCEN = NVL(DPCODCEN,space(15))
          if link_1_39_joined
            this.w_DPCODCEN = NVL(CC_CONTO139,NVL(this.w_DPCODCEN,space(15)))
            this.w_DESCON = NVL(CCDESPIA139,space(40))
          else
          .link_1_39('Load')
          endif
        .w_DPCODVOC = NVL(DPCODVOC,space(15))
          if link_1_42_joined
            this.w_DPCODVOC = NVL(VCCODICE142,NVL(this.w_DPCODVOC,space(15)))
            this.w_VOCDES = NVL(VCDESCRI142,space(40))
            this.w_TIPVOC = NVL(VCTIPVOC142,space(1))
            this.w_DTOBVO = NVL(cp_ToDate(VCDTOBSO142),ctod("  /  /  "))
            this.w_CODCOS = NVL(VCTIPCOS142,space(5))
          else
          .link_1_42('Load')
          endif
        .w_DPCODCOM = NVL(DPCODCOM,space(15))
          if link_1_45_joined
            this.w_DPCODCOM = NVL(CNCODCAN145,NVL(this.w_DPCODCOM,space(15)))
            this.w_DTOBSO = NVL(cp_ToDate(CNDTOBSO145),ctod("  /  /  "))
            this.w_DESCAN = NVL(CNDESCAN145,space(40))
          else
          .link_1_45('Load')
          endif
        .w_DPCODATT = NVL(DPCODATT,space(15))
          if link_1_47_joined
            this.w_DPCODATT = NVL(ATCODATT147,NVL(this.w_DPCODATT,space(15)))
            this.w_DESATT = NVL(ATDESCRI147,space(30))
          else
          .link_1_47('Load')
          endif
        .w_DPCODLOT = NVL(DPCODLOT,space(20))
          .link_1_50('Load')
        .w_DPCODUBI = NVL(DPCODUBI,space(20))
          * evitabile
          *.link_1_52('Load')
          .link_1_63('Load')
        .w_DPQTAPR1 = NVL(DPQTAPR1,0)
        .w_DPQTASC1 = NVL(DPQTASC1,0)
        .w_QTAUM1 = .w_DPQTAPR1+.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .w_DPRIFCAR = NVL(DPRIFCAR,space(10))
        .w_DPRIFSCA = NVL(DPRIFSCA,space(10))
        .w_STATOGES = upper(this.cFunction)
          .link_1_99('Load')
        .w_CRIVAL = 'US'
        .w_MATENABL = g_MATR="S" and g_DATMAT<=.w_DPDATREG
        .w_DPCOMMAT = NVL(DPCOMMAT,space(1))
        .w_GESMAT = iif(not .w_MATENABL, "N", .w_GESMAT)
          .link_1_109('Load')
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .w_FLPRG = 'K'
        .w_DPRIFMOU = NVL(DPRIFMOU,space(10))
        .w_DPCPRFAS = NVL(DPCPRFAS,0)
        .w_DPFASOUT = NVL(DPFASOUT,space(1))
        .w_DPULTFAS = NVL(DPULTFAS,space(1))
        .oPgFrm.Page1.oPag.oObj_1_136.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIC_PROD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_97.enabled = this.oPgFrm.Page1.oPag.oBtn_1_97.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_127.enabled = this.oPgFrm.Page1.oPag.oBtn_1_127.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DPSERIAL = space(10)
      .w_DPDATREG = ctod("  /  /  ")
      .w_DPNUMOPE = 0
      .w_READPAR = space(10)
      .w_CAUSCA = space(5)
      .w_CAUCAR = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DPTIPATT = space(1)
      .w_DPCODODL = space(15)
      .w_OLTSTATO = space(1)
      .w_OLTPROVE = space(1)
      .w_OLTQTODL = 0
      .w_OLTQTOEV = 0
      .w_QTARES = 0
      .w_OLTFLEVA = space(1)
      .w_DPCODART = space(20)
      .w_FLLOTT = space(1)
      .w_OLTUNMIS = space(3)
      .w_DPCODMAG = space(5)
      .w_FLUBIC = space(1)
      .w_DPCODICE = space(20)
      .w_DPUNIMIS = space(3)
      .w_TIPVOC = space(1)
      .w_DPQTAPRO = 0
      .w_DPQTASCA = 0
      .w_DPFLEVAS = space(1)
      .w_DPDATINI = ctod("  /  /  ")
      .w_DPORAINI = space(5)
      .w_DPDATFIN = ctod("  /  /  ")
      .w_DPORAFIN = space(5)
      .w_STADOC = space(10)
      .w_DPCODCEN = space(15)
      .w_DESCON = space(40)
      .w_DPCODVOC = space(15)
      .w_VOCDES = space(40)
      .w_DPCODCOM = space(15)
      .w_DPCODATT = space(15)
      .w_DESCAN = space(40)
      .w_DESATT = space(30)
      .w_DPCODLOT = space(20)
      .w_DPCODUBI = space(20)
      .w_DTOBVO = ctod("  /  /  ")
      .w_CODCOS = space(5)
      .w_DTOBSO = ctod("  /  /  ")
      .w_DESART = space(40)
      .w_DESMAG = space(30)
      .w_UNMIS3 = space(3)
      .w_OPERA3 = space(1)
      .w_MOLTI3 = 0
      .w_UNMIS1 = space(3)
      .w_OPERAT = space(1)
      .w_MOLTIP = 0
      .w_UNMIS2 = space(3)
      .w_DPQTAPR1 = 0
      .w_DPQTASC1 = 0
      .w_QTAUM1 = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESUTE = space(20)
      .w_OLTQTOD1 = 0
      .w_OLTQTOE1 = 0
      .w_TESTFORM = .f.
      .w_DPRIFCAR = space(10)
      .w_DPRIFSCA = space(10)
      .w_OLTKEYSA = space(20)
      .w_STATOGES = space(10)
      .w_OLTQTSAL = 0
      .w_OLTCICLO = space(15)
      .w_MGSCAR = space(5)
      .w_CRIVAL = space(2)
      .w_ARTLOT = space(20)
      .w_FLSTAT = space(1)
      .w_MATENABL = .f.
      .w_DPCOMMAT = space(1)
      .w_GESMAT = space(1)
      .w_EDCOMMAT = .f.
      .w_CAUMGC = space(5)
      .w_MTCARI = space(1)
      .w_FLUBIC1 = space(1)
      .w_MATCAR = 0
      .w_MATSCA = 0
      .w_RIGMOVLOT = .f.
      .w_FLUSEP = space(1)
      .w_MODUM2 = space(1)
      .w_IMPLOT = space(1)
      .w_ROWDOC = 0
      .w_NUMRIF = 0
      .w_FLPRG = space(1)
      .w_SERFAS = space(15)
      .w_SECIC = space(10)
      .w_DPRIFMOU = space(10)
      .w_CAUMOU = space(5)
      .w_FASE = 0
      .w_OLTSECPR = space(15)
      .w_DPODLFAS = space(15)
      .w_DPCPRFAS = 0
      .w_DPFASOUT = space(1)
      .w_DPULTFAS = space(1)
      .w_ODLSOSPE = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DPDATREG = i_datsys
        .w_DPNUMOPE = i_CODUTE
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_DPNUMOPE))
          .link_1_3('Full')
          endif
        .w_READPAR = 'PP'
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_READPAR))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,6,.f.)
          if not(empty(.w_CAUCAR))
          .link_1_6('Full')
          endif
        .w_OBTEST = i_datsys
        .w_DPTIPATT = 'A'
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_DPCODODL))
          .link_1_10('Full')
          endif
          .DoRTCalc(10,13,.f.)
        .w_QTARES = MAX(0, .w_OLTQTODL-.w_OLTQTOEV)
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_DPCODART))
          .link_1_17('Full')
          endif
        .DoRTCalc(17,19,.f.)
          if not(empty(.w_DPCODMAG))
          .link_1_20('Full')
          endif
        .DoRTCalc(20,21,.f.)
          if not(empty(.w_DPCODICE))
          .link_1_22('Full')
          endif
        .w_DPUNIMIS = .w_OLTUNMIS
          .DoRTCalc(23,23,.f.)
        .w_DPQTAPRO = .w_QTARES
        .w_DPQTASCA = 0
        .DoRTCalc(26,32,.f.)
          if not(empty(.w_DPCODCEN))
          .link_1_39('Full')
          endif
        .DoRTCalc(33,34,.f.)
          if not(empty(.w_DPCODVOC))
          .link_1_42('Full')
          endif
        .DoRTCalc(35,36,.f.)
          if not(empty(.w_DPCODCOM))
          .link_1_45('Full')
          endif
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_DPCODATT))
          .link_1_47('Full')
          endif
          .DoRTCalc(38,39,.f.)
        .w_DPCODLOT = SPACE(20)
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_DPCODLOT))
          .link_1_50('Full')
          endif
        .w_DPCODUBI = SPACE(20)
        .DoRTCalc(41,41,.f.)
          if not(empty(.w_DPCODUBI))
          .link_1_52('Full')
          endif
        .DoRTCalc(42,50,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_63('Full')
          endif
          .DoRTCalc(51,53,.f.)
        .w_DPQTAPR1 = CALQTAADV(.w_DPQTAPRO,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTAPRO")
        .w_DPQTASC1 = CALQTAADV(.w_DPQTASCA,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTASCA")
        .w_QTAUM1 = .w_DPQTAPR1+.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
          .DoRTCalc(57,60,.f.)
        .w_TESTFORM = .T.
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
          .DoRTCalc(62,64,.f.)
        .w_STATOGES = upper(this.cFunction)
        .DoRTCalc(66,68,.f.)
          if not(empty(.w_MGSCAR))
          .link_1_99('Full')
          endif
        .w_CRIVAL = 'US'
          .DoRTCalc(70,71,.f.)
        .w_MATENABL = g_MATR="S" and g_DATMAT<=.w_DPDATREG
          .DoRTCalc(73,73,.f.)
        .w_GESMAT = iif(not .w_MATENABL, "N", .w_GESMAT)
        .w_EDCOMMAT = True
        .DoRTCalc(76,76,.f.)
          if not(empty(.w_CAUMGC))
          .link_1_109('Full')
          endif
          .DoRTCalc(77,78,.f.)
        .w_MATCAR = -1
        .w_MATSCA = -1
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
          .DoRTCalc(81,84,.f.)
        .w_ROWDOC = 0
        .w_NUMRIF = -50
        .w_FLPRG = 'K'
        .w_SERFAS = space(15)
        .oPgFrm.Page1.oPag.oObj_1_136.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_PROD')
    this.DoRTCalc(89,98,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_97.enabled = this.oPgFrm.Page1.oPag.oBtn_1_97.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_127.enabled = this.oPgFrm.Page1.oPag.oBtn_1_127.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEDICPR","i_CODAZI,w_DPSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DPSERIAL = .w_DPSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDPSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oDPDATREG_1_2.enabled = i_bVal
      .Page1.oPag.oDPCODODL_1_10.enabled = i_bVal
      .Page1.oPag.oDPQTAPRO_1_25.enabled = i_bVal
      .Page1.oPag.oDPQTASCA_1_26.enabled = i_bVal
      .Page1.oPag.oDPFLEVAS_1_27.enabled = i_bVal
      .Page1.oPag.oSTADOC_1_32.enabled = i_bVal
      .Page1.oPag.oDPCODCEN_1_39.enabled = i_bVal
      .Page1.oPag.oDPCODVOC_1_42.enabled = i_bVal
      .Page1.oPag.oDPCODCOM_1_45.enabled = i_bVal
      .Page1.oPag.oDPCODATT_1_47.enabled = i_bVal
      .Page1.oPag.oDPCODLOT_1_50.enabled = i_bVal
      .Page1.oPag.oDPCODUBI_1_52.enabled = i_bVal
      .Page1.oPag.oBtn_1_51.enabled = i_bVal
      .Page1.oPag.oBtn_1_96.enabled = .Page1.oPag.oBtn_1_96.mCond()
      .Page1.oPag.oBtn_1_97.enabled = .Page1.oPag.oBtn_1_97.mCond()
      .Page1.oPag.oBtn_1_127.enabled = .Page1.oPag.oBtn_1_127.mCond()
      .Page1.oPag.oObj_1_81.enabled = i_bVal
      .Page1.oPag.oObj_1_84.enabled = i_bVal
      .Page1.oPag.oObj_1_85.enabled = i_bVal
      .Page1.oPag.oObj_1_87.enabled = i_bVal
      .Page1.oPag.oObj_1_88.enabled = i_bVal
      .Page1.oPag.oObj_1_114.enabled = i_bVal
      .Page1.oPag.oObj_1_115.enabled = i_bVal
      .Page1.oPag.oObj_1_116.enabled = i_bVal
      .Page1.oPag.oObj_1_118.enabled = i_bVal
      .Page1.oPag.oObj_1_121.enabled = i_bVal
      .Page1.oPag.oObj_1_136.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDPSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDPSERIAL_1_1.enabled = .t.
        .Page1.oPag.oDPDATREG_1_2.enabled = .t.
        .Page1.oPag.oDPCODODL_1_10.enabled = .t.
      endif
    endwith
    this.GSCO_MMP.SetStatus(i_cOp)
    this.GSCO_MRP.SetStatus(i_cOp)
    this.GSCO_MMT.SetStatus(i_cOp)
    this.GSCO_MCO.SetStatus(i_cOp)
    this.GSCO_MOU.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIC_PROD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsco_adp
    this.w_STATOGES=UPPER(this.cFunction)
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCO_MMP.SetChildrenStatus(i_cOp)
  *  this.GSCO_MRP.SetChildrenStatus(i_cOp)
  *  this.GSCO_MMT.SetChildrenStatus(i_cOp)
  *  this.GSCO_MCO.SetChildrenStatus(i_cOp)
  *  this.GSCO_MOU.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERIAL,"DPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATREG,"DPDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNUMOPE,"DPNUMOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTIPATT,"DPTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODODL,"DPCODODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODART,"DPCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODMAG,"DPCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODICE,"DPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPUNIMIS,"DPUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTAPRO,"DPQTAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTASCA,"DPQTASCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLEVAS,"DPFLEVAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATINI,"DPDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPORAINI,"DPORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATFIN,"DPDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPORAFIN,"DPORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCEN,"DPCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODVOC,"DPCODVOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCOM,"DPCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODATT,"DPCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODLOT,"DPCODLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODUBI,"DPCODUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTAPR1,"DPQTAPR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTASC1,"DPQTASC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFCAR,"DPRIFCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFSCA,"DPRIFSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCOMMAT,"DPCOMMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFMOU,"DPRIFMOU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCPRFAS,"DPCPRFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFASOUT,"DPFASOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPULTFAS,"DPULTFAS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    i_lTable = "DIC_PROD"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIC_PROD_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIC_PROD_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEDICPR","i_CODAZI,w_DPSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIC_PROD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_PROD')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_PROD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DPSERIAL,DPDATREG,DPNUMOPE,DPTIPATT,DPCODODL"+;
                  ",DPCODART,DPCODMAG,DPCODICE,DPUNIMIS,DPQTAPRO"+;
                  ",DPQTASCA,DPFLEVAS,DPDATINI,DPORAINI,DPDATFIN"+;
                  ",DPORAFIN,DPCODCEN,DPCODVOC,DPCODCOM,DPCODATT"+;
                  ",DPCODLOT,DPCODUBI,DPQTAPR1,DPQTASC1,DPRIFCAR"+;
                  ",DPRIFSCA,DPCOMMAT,DPRIFMOU,DPCPRFAS,DPFASOUT"+;
                  ",DPULTFAS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DPSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DPDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_DPNUMOPE)+;
                  ","+cp_ToStrODBC(this.w_DPTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODODL)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODART)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODICE)+;
                  ","+cp_ToStrODBC(this.w_DPUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_DPQTAPRO)+;
                  ","+cp_ToStrODBC(this.w_DPQTASCA)+;
                  ","+cp_ToStrODBC(this.w_DPFLEVAS)+;
                  ","+cp_ToStrODBC(this.w_DPDATINI)+;
                  ","+cp_ToStrODBC(this.w_DPORAINI)+;
                  ","+cp_ToStrODBC(this.w_DPDATFIN)+;
                  ","+cp_ToStrODBC(this.w_DPORAFIN)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODVOC)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODCOM)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODLOT)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODUBI)+;
                  ","+cp_ToStrODBC(this.w_DPQTAPR1)+;
                  ","+cp_ToStrODBC(this.w_DPQTASC1)+;
                  ","+cp_ToStrODBC(this.w_DPRIFCAR)+;
                  ","+cp_ToStrODBC(this.w_DPRIFSCA)+;
                  ","+cp_ToStrODBC(this.w_DPCOMMAT)+;
                  ","+cp_ToStrODBC(this.w_DPRIFMOU)+;
                  ","+cp_ToStrODBC(this.w_DPCPRFAS)+;
                  ","+cp_ToStrODBC(this.w_DPFASOUT)+;
                  ","+cp_ToStrODBC(this.w_DPULTFAS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_PROD')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_PROD')
        cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_DPSERIAL)
        INSERT INTO (i_cTable);
              (DPSERIAL,DPDATREG,DPNUMOPE,DPTIPATT,DPCODODL,DPCODART,DPCODMAG,DPCODICE,DPUNIMIS,DPQTAPRO,DPQTASCA,DPFLEVAS,DPDATINI,DPORAINI,DPDATFIN,DPORAFIN,DPCODCEN,DPCODVOC,DPCODCOM,DPCODATT,DPCODLOT,DPCODUBI,DPQTAPR1,DPQTASC1,DPRIFCAR,DPRIFSCA,DPCOMMAT,DPRIFMOU,DPCPRFAS,DPFASOUT,DPULTFAS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DPSERIAL;
                  ,this.w_DPDATREG;
                  ,this.w_DPNUMOPE;
                  ,this.w_DPTIPATT;
                  ,this.w_DPCODODL;
                  ,this.w_DPCODART;
                  ,this.w_DPCODMAG;
                  ,this.w_DPCODICE;
                  ,this.w_DPUNIMIS;
                  ,this.w_DPQTAPRO;
                  ,this.w_DPQTASCA;
                  ,this.w_DPFLEVAS;
                  ,this.w_DPDATINI;
                  ,this.w_DPORAINI;
                  ,this.w_DPDATFIN;
                  ,this.w_DPORAFIN;
                  ,this.w_DPCODCEN;
                  ,this.w_DPCODVOC;
                  ,this.w_DPCODCOM;
                  ,this.w_DPCODATT;
                  ,this.w_DPCODLOT;
                  ,this.w_DPCODUBI;
                  ,this.w_DPQTAPR1;
                  ,this.w_DPQTASC1;
                  ,this.w_DPRIFCAR;
                  ,this.w_DPRIFSCA;
                  ,this.w_DPCOMMAT;
                  ,this.w_DPRIFMOU;
                  ,this.w_DPCPRFAS;
                  ,this.w_DPFASOUT;
                  ,this.w_DPULTFAS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIC_PROD_IDX,i_nConn)
      *
      * update DIC_PROD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_PROD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DPDATREG="+cp_ToStrODBC(this.w_DPDATREG)+;
             ",DPNUMOPE="+cp_ToStrODBCNull(this.w_DPNUMOPE)+;
             ",DPTIPATT="+cp_ToStrODBC(this.w_DPTIPATT)+;
             ",DPCODODL="+cp_ToStrODBCNull(this.w_DPCODODL)+;
             ",DPCODART="+cp_ToStrODBCNull(this.w_DPCODART)+;
             ",DPCODMAG="+cp_ToStrODBCNull(this.w_DPCODMAG)+;
             ",DPCODICE="+cp_ToStrODBCNull(this.w_DPCODICE)+;
             ",DPUNIMIS="+cp_ToStrODBC(this.w_DPUNIMIS)+;
             ",DPQTAPRO="+cp_ToStrODBC(this.w_DPQTAPRO)+;
             ",DPQTASCA="+cp_ToStrODBC(this.w_DPQTASCA)+;
             ",DPFLEVAS="+cp_ToStrODBC(this.w_DPFLEVAS)+;
             ",DPDATINI="+cp_ToStrODBC(this.w_DPDATINI)+;
             ",DPORAINI="+cp_ToStrODBC(this.w_DPORAINI)+;
             ",DPDATFIN="+cp_ToStrODBC(this.w_DPDATFIN)+;
             ",DPORAFIN="+cp_ToStrODBC(this.w_DPORAFIN)+;
             ",DPCODCEN="+cp_ToStrODBCNull(this.w_DPCODCEN)+;
             ",DPCODVOC="+cp_ToStrODBCNull(this.w_DPCODVOC)+;
             ",DPCODCOM="+cp_ToStrODBCNull(this.w_DPCODCOM)+;
             ",DPCODATT="+cp_ToStrODBCNull(this.w_DPCODATT)+;
             ",DPCODLOT="+cp_ToStrODBCNull(this.w_DPCODLOT)+;
             ",DPCODUBI="+cp_ToStrODBCNull(this.w_DPCODUBI)+;
             ",DPQTAPR1="+cp_ToStrODBC(this.w_DPQTAPR1)+;
             ",DPQTASC1="+cp_ToStrODBC(this.w_DPQTASC1)+;
             ",DPRIFCAR="+cp_ToStrODBC(this.w_DPRIFCAR)+;
             ",DPRIFSCA="+cp_ToStrODBC(this.w_DPRIFSCA)+;
             ",DPCOMMAT="+cp_ToStrODBC(this.w_DPCOMMAT)+;
             ",DPRIFMOU="+cp_ToStrODBC(this.w_DPRIFMOU)+;
             ",DPCPRFAS="+cp_ToStrODBC(this.w_DPCPRFAS)+;
             ",DPFASOUT="+cp_ToStrODBC(this.w_DPFASOUT)+;
             ",DPULTFAS="+cp_ToStrODBC(this.w_DPULTFAS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_PROD')
        i_cWhere = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
        UPDATE (i_cTable) SET;
              DPDATREG=this.w_DPDATREG;
             ,DPNUMOPE=this.w_DPNUMOPE;
             ,DPTIPATT=this.w_DPTIPATT;
             ,DPCODODL=this.w_DPCODODL;
             ,DPCODART=this.w_DPCODART;
             ,DPCODMAG=this.w_DPCODMAG;
             ,DPCODICE=this.w_DPCODICE;
             ,DPUNIMIS=this.w_DPUNIMIS;
             ,DPQTAPRO=this.w_DPQTAPRO;
             ,DPQTASCA=this.w_DPQTASCA;
             ,DPFLEVAS=this.w_DPFLEVAS;
             ,DPDATINI=this.w_DPDATINI;
             ,DPORAINI=this.w_DPORAINI;
             ,DPDATFIN=this.w_DPDATFIN;
             ,DPORAFIN=this.w_DPORAFIN;
             ,DPCODCEN=this.w_DPCODCEN;
             ,DPCODVOC=this.w_DPCODVOC;
             ,DPCODCOM=this.w_DPCODCOM;
             ,DPCODATT=this.w_DPCODATT;
             ,DPCODLOT=this.w_DPCODLOT;
             ,DPCODUBI=this.w_DPCODUBI;
             ,DPQTAPR1=this.w_DPQTAPR1;
             ,DPQTASC1=this.w_DPQTASC1;
             ,DPRIFCAR=this.w_DPRIFCAR;
             ,DPRIFSCA=this.w_DPRIFSCA;
             ,DPCOMMAT=this.w_DPCOMMAT;
             ,DPRIFMOU=this.w_DPRIFMOU;
             ,DPCPRFAS=this.w_DPCPRFAS;
             ,DPFASOUT=this.w_DPFASOUT;
             ,DPULTFAS=this.w_DPULTFAS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCO_MMP : Saving
      this.GSCO_MMP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MPSERIAL";
             ,this.w_ROWDOC,"MPROWDOC";
             ,this.w_NUMRIF,"MPNUMRIF";
             )
      this.GSCO_MMP.mReplace()
      * --- GSCO_MRP : Saving
      this.GSCO_MRP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"RPSERIAL";
             )
      this.GSCO_MRP.mReplace()
      * --- GSCO_MMT : Saving
      this.GSCO_MMT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MTSERIAL";
             )
      this.GSCO_MMT.mReplace()
      * --- GSCO_MCO : Saving
      this.GSCO_MCO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MTSERIAL";
             )
      this.GSCO_MCO.mReplace()
      * --- GSCO_MOU : Saving
      this.GSCO_MOU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MDSERIAL";
             )
      this.GSCO_MOU.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsco_adp
    * --- Controlli Finali su DB
    if not(bTrsErr) and (this.w_GESMAT='S' or this.w_DPCOMMAT="S")
        * --- Controlli Righe Lotti/Ubicazioni
        this.NotifyEvent('Controlli Updated')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsco_adp
    this.NotifyEvent('CTRLMAT')
    * --- Fine Area Manuale
    * --- GSCO_MMP : Deleting
    this.GSCO_MMP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MPSERIAL";
           ,this.w_ROWDOC,"MPROWDOC";
           ,this.w_NUMRIF,"MPNUMRIF";
           )
    this.GSCO_MMP.mDelete()
    * --- GSCO_MRP : Deleting
    this.GSCO_MRP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"RPSERIAL";
           )
    this.GSCO_MRP.mDelete()
    * --- GSCO_MMT : Deleting
    this.GSCO_MMT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MTSERIAL";
           )
    this.GSCO_MMT.mDelete()
    * --- GSCO_MCO : Deleting
    this.GSCO_MCO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MTSERIAL";
           )
    this.GSCO_MCO.mDelete()
    * --- GSCO_MOU : Deleting
    this.GSCO_MOU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MDSERIAL";
           )
    this.GSCO_MOU.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIC_PROD_IDX,i_nConn)
      *
      * delete DIC_PROD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
            .w_READPAR = 'PP'
          .link_1_4('Full')
        .DoRTCalc(5,5,.t.)
          .link_1_6('Full')
        .DoRTCalc(7,13,.t.)
            .w_QTARES = MAX(0, .w_OLTQTODL-.w_OLTQTOEV)
        .DoRTCalc(15,15,.t.)
          .link_1_17('Full')
        .DoRTCalc(17,18,.t.)
          .link_1_20('Full')
        .DoRTCalc(20,20,.t.)
          .link_1_22('Full')
            .w_DPUNIMIS = .w_OLTUNMIS
        .DoRTCalc(23,23,.t.)
        if .o_DPCODODL<>.w_DPCODODL
            .w_DPQTAPRO = .w_QTARES
        endif
        if .o_DPCODODL<>.w_DPCODODL
            .w_DPQTASCA = 0
        endif
        .DoRTCalc(26,31,.t.)
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_39('Full')
        endif
        .DoRTCalc(33,33,.t.)
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_42('Full')
        endif
        .DoRTCalc(35,35,.t.)
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_45('Full')
        endif
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_47('Full')
        endif
        .DoRTCalc(38,39,.t.)
        if .o_DPCODART<>.w_DPCODART
            .w_DPCODLOT = SPACE(20)
          .link_1_50('Full')
        endif
        if .o_DPCODART<>.w_DPCODART.or. .o_DPCODMAG<>.w_DPCODMAG
            .w_DPCODUBI = SPACE(20)
          .link_1_52('Full')
        endif
        .DoRTCalc(42,49,.t.)
          .link_1_63('Full')
        .DoRTCalc(51,53,.t.)
        if .o_DPQTAPRO<>.w_DPQTAPRO
            .w_DPQTAPR1 = CALQTAADV(.w_DPQTAPRO,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTAPRO")
        endif
        if .o_DPQTASCA<>.w_DPQTASCA
            .w_DPQTASC1 = CALQTAADV(.w_DPQTASCA,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTASCA")
        endif
            .w_QTAUM1 = .w_DPQTAPR1+.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .DoRTCalc(57,64,.t.)
            .w_STATOGES = upper(this.cFunction)
        if  .o_DPCODLOT<>.w_DPCODLOT.or. .o_DPCODUBI<>.w_DPCODUBI.or. .o_DPCODUBI<>.w_DPCODUBI
          .WriteTo_GSCO_MCO()
        endif
        .DoRTCalc(66,67,.t.)
          .link_1_99('Full')
            .w_CRIVAL = 'US'
        .DoRTCalc(70,71,.t.)
            .w_MATENABL = g_MATR="S" and g_DATMAT<=.w_DPDATREG
        .DoRTCalc(73,73,.t.)
            .w_GESMAT = iif(not .w_MATENABL, "N", .w_GESMAT)
        .DoRTCalc(75,75,.t.)
          .link_1_109('Full')
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .DoRTCalc(77,86,.t.)
            .w_FLPRG = 'K'
        .oPgFrm.Page1.oPag.oObj_1_136.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEDICPR","i_CODAZI,w_DPSERIAL")
          .op_DPSERIAL = .w_DPSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(88,98,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_136.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDPCODODL_1_10.enabled = this.oPgFrm.Page1.oPag.oDPCODODL_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDPFLEVAS_1_27.enabled = this.oPgFrm.Page1.oPag.oDPFLEVAS_1_27.mCond()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_39.enabled = this.oPgFrm.Page1.oPag.oDPCODCEN_1_39.mCond()
    this.oPgFrm.Page1.oPag.oDPCODVOC_1_42.enabled = this.oPgFrm.Page1.oPag.oDPCODVOC_1_42.mCond()
    this.oPgFrm.Page1.oPag.oDPCODCOM_1_45.enabled = this.oPgFrm.Page1.oPag.oDPCODCOM_1_45.mCond()
    this.oPgFrm.Page1.oPag.oDPCODATT_1_47.enabled = this.oPgFrm.Page1.oPag.oDPCODATT_1_47.mCond()
    this.oPgFrm.Page1.oPag.oDPCODLOT_1_50.enabled = this.oPgFrm.Page1.oPag.oDPCODLOT_1_50.mCond()
    this.oPgFrm.Page1.oPag.oDPCODUBI_1_52.enabled = this.oPgFrm.Page1.oPag.oDPCODUBI_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_97.enabled = this.oPgFrm.Page1.oPag.oBtn_1_97.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_127.enabled = this.oPgFrm.Page1.oPag.oBtn_1_127.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_94.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_94.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_95.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_95.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_39.visible=!this.oPgFrm.Page1.oPag.oDPCODCEN_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_40.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oDPCODVOC_1_42.visible=!this.oPgFrm.Page1.oPag.oDPCODVOC_1_42.mHide()
    this.oPgFrm.Page1.oPag.oVOCDES_1_43.visible=!this.oPgFrm.Page1.oPag.oVOCDES_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCOM_1_45.visible=!this.oPgFrm.Page1.oPag.oDPCODCOM_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oDPCODATT_1_47.visible=!this.oPgFrm.Page1.oPag.oDPCODATT_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_48.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_1_49.visible=!this.oPgFrm.Page1.oPag.oDESATT_1_49.mHide()
    this.oPgFrm.Page1.oPag.oDPCODLOT_1_50.visible=!this.oPgFrm.Page1.oPag.oDPCODLOT_1_50.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_51.visible=!this.oPgFrm.Page1.oPag.oBtn_1_51.mHide()
    this.oPgFrm.Page1.oPag.oDPCODUBI_1_52.visible=!this.oPgFrm.Page1.oPag.oDPCODUBI_1_52.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_94.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_94.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_95.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_95.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_96.visible=!this.oPgFrm.Page1.oPag.oBtn_1_96.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_97.visible=!this.oPgFrm.Page1.oPag.oBtn_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_127.visible=!this.oPgFrm.Page1.oPag.oBtn_1_127.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsco_adp
     * istanzio immediatamente i figli della seconda e terza pagina
     * perch� vengono richiamati
     IF Upper(CEVENT)='INIT'
       if Upper(this.GSCO_MCO.class)='STDLAZYCHILD'
          this.GSCO_MCO.createrealchild()
          This.oPgFrm.ActivePage=1
       Endif
     Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_114.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_115.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_116.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_118.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_121.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_136.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DPNUMOPE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPNUMOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPNUMOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_DPNUMOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_DPNUMOPE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPNUMOPE = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPNUMOPE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPNUMOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCAUCAR,PPCAUSCA,PPMAGSCA,PPIMPLOT,PPCAUMOU";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPCAUCAR,PPCAUSCA,PPMAGSCA,PPIMPLOT,PPCAUMOU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(10))
      this.w_CAUCAR = NVL(_Link_.PPCAUCAR,space(5))
      this.w_CAUSCA = NVL(_Link_.PPCAUSCA,space(5))
      this.w_MGSCAR = NVL(_Link_.PPMAGSCA,space(5))
      this.w_IMPLOT = NVL(_Link_.PPIMPLOT,space(1))
      this.w_CAUMOU = NVL(_Link_.PPCAUMOU,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_CAUCAR = space(5)
      this.w_CAUSCA = space(5)
      this.w_MGSCAR = space(5)
      this.w_IMPLOT = space(1)
      this.w_CAUMOU = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCAR
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUCAR)
            select TDTIPDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCAR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_CAUMGC = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCAR = space(5)
      endif
      this.w_CAUMGC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODODL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_DPCODODL)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTSECIC,OLTSOSPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_DPCODODL))
          select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTSECIC,OLTSOSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODODL)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODODL) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oDPCODODL_1_10'),i_cWhere,'GSCO_BZA',"Elenco ODL",'GSCO_ADP.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTSECIC,OLTSOSPE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTSECIC,OLTSOSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTSECIC,OLTSOSPE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_DPCODODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_DPCODODL)
            select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTSECIC,OLTSOSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODODL = NVL(_Link_.OLCODODL,space(15))
      this.w_OLTSTATO = NVL(_Link_.OLTSTATO,space(1))
      this.w_OLTPROVE = NVL(_Link_.OLTPROVE,space(1))
      this.w_OLTUNMIS = NVL(_Link_.OLTUNMIS,space(3))
      this.w_DPCODICE = NVL(_Link_.OLTCODIC,space(20))
      this.w_DPCODMAG = NVL(_Link_.OLTCOMAG,space(5))
      this.w_OLTQTODL = NVL(_Link_.OLTQTODL,0)
      this.w_OLTQTOEV = NVL(_Link_.OLTQTOEV,0)
      this.w_OLTFLEVA = NVL(_Link_.OLTFLEVA,space(1))
      this.w_DPCODCEN = NVL(_Link_.OLTCOCEN,space(15))
      this.w_DPCODVOC = NVL(_Link_.OLTVOCEN,space(15))
      this.w_DPCODCOM = NVL(_Link_.OLTCOMME,space(15))
      this.w_DPCODATT = NVL(_Link_.OLTCOATT,space(15))
      this.w_DPCODART = NVL(_Link_.OLTCOART,space(20))
      this.w_OLTQTOD1 = NVL(_Link_.OLTQTOD1,0)
      this.w_OLTQTOE1 = NVL(_Link_.OLTQTOE1,0)
      this.w_OLTKEYSA = NVL(_Link_.OLTKEYSA,space(20))
      this.w_OLTQTSAL = NVL(_Link_.OLTQTSAL,0)
      this.w_OLTCICLO = NVL(_Link_.OLTCICLO,space(15))
      this.w_SERFAS = NVL(_Link_.OLTSEODL,space(15))
      this.w_SECIC = NVL(_Link_.OLTSECIC,space(10))
      this.w_ODLSOSPE = NVL(_Link_.OLTSOSPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODODL = space(15)
      endif
      this.w_OLTSTATO = space(1)
      this.w_OLTPROVE = space(1)
      this.w_OLTUNMIS = space(3)
      this.w_DPCODICE = space(20)
      this.w_DPCODMAG = space(5)
      this.w_OLTQTODL = 0
      this.w_OLTQTOEV = 0
      this.w_OLTFLEVA = space(1)
      this.w_DPCODCEN = space(15)
      this.w_DPCODVOC = space(15)
      this.w_DPCODCOM = space(15)
      this.w_DPCODATT = space(15)
      this.w_DPCODART = space(20)
      this.w_OLTQTOD1 = 0
      this.w_OLTQTOE1 = 0
      this.w_OLTKEYSA = space(20)
      this.w_OLTQTSAL = 0
      this.w_OLTCICLO = space(15)
      this.w_SERFAS = space(15)
      this.w_SECIC = space(10)
      this.w_ODLSOSPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLSOSPE<>'S' and (.w_STATOGES<>'LOAD' OR (.w_OLTSTATO='L' AND .w_OLTPROVE='I' AND .w_OLTFLEVA<>'S' AND .w_OLTQTODL>.w_OLTQTOEV) AND empty(NVL(.w_SERFAS,'')) AND empty(NVL(.w_SECIC,'')))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("ODL non valido, sospeso o dichiarato in contemporanea da pi� utenti")
        endif
        this.w_DPCODODL = space(15)
        this.w_OLTSTATO = space(1)
        this.w_OLTPROVE = space(1)
        this.w_OLTUNMIS = space(3)
        this.w_DPCODICE = space(20)
        this.w_DPCODMAG = space(5)
        this.w_OLTQTODL = 0
        this.w_OLTQTOEV = 0
        this.w_OLTFLEVA = space(1)
        this.w_DPCODCEN = space(15)
        this.w_DPCODVOC = space(15)
        this.w_DPCODCOM = space(15)
        this.w_DPCODATT = space(15)
        this.w_DPCODART = space(20)
        this.w_OLTQTOD1 = 0
        this.w_OLTQTOE1 = 0
        this.w_OLTKEYSA = space(20)
        this.w_OLTQTSAL = 0
        this.w_OLTCICLO = space(15)
        this.w_SERFAS = space(15)
        this.w_SECIC = space(10)
        this.w_ODLSOSPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 22 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ODL_MAST_IDX,3] and i_nFlds+22<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.OLCODODL as OLCODODL110"+ ",link_1_10.OLTSTATO as OLTSTATO110"+ ",link_1_10.OLTPROVE as OLTPROVE110"+ ",link_1_10.OLTUNMIS as OLTUNMIS110"+ ",link_1_10.OLTCODIC as OLTCODIC110"+ ",link_1_10.OLTCOMAG as OLTCOMAG110"+ ",link_1_10.OLTQTODL as OLTQTODL110"+ ",link_1_10.OLTQTOEV as OLTQTOEV110"+ ",link_1_10.OLTFLEVA as OLTFLEVA110"+ ",link_1_10.OLTCOCEN as OLTCOCEN110"+ ",link_1_10.OLTVOCEN as OLTVOCEN110"+ ",link_1_10.OLTCOMME as OLTCOMME110"+ ",link_1_10.OLTCOATT as OLTCOATT110"+ ",link_1_10.OLTCOART as OLTCOART110"+ ",link_1_10.OLTQTOD1 as OLTQTOD1110"+ ",link_1_10.OLTQTOE1 as OLTQTOE1110"+ ",link_1_10.OLTKEYSA as OLTKEYSA110"+ ",link_1_10.OLTQTSAL as OLTQTSAL110"+ ",link_1_10.OLTCICLO as OLTCICLO110"+ ",link_1_10.OLTSEODL as OLTSEODL110"+ ",link_1_10.OLTSECIC as OLTSECIC110"+ ",link_1_10.OLTSOSPE as OLTSOSPE110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on DIC_PROD.DPCODODL=link_1_10.OLCODODL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+22
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODODL=link_1_10.OLCODODL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+22
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODART
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARFLLOTT,ARGESMAT,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DPCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DPCODART)
            select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARFLLOTT,ARGESMAT,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_FLLOTT = space(1)
      this.w_GESMAT = space(1)
      this.w_FLUSEP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.ARCODART as ARCODART117"+ ",link_1_17.ARUNMIS1 as ARUNMIS1117"+ ",link_1_17.AROPERAT as AROPERAT117"+ ",link_1_17.ARMOLTIP as ARMOLTIP117"+ ",link_1_17.ARUNMIS2 as ARUNMIS2117"+ ",link_1_17.ARFLLOTT as ARFLLOTT117"+ ",link_1_17.ARGESMAT as ARGESMAT117"+ ",link_1_17.ARFLUSEP as ARFLUSEP117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on DIC_PROD.DPCODART=link_1_17.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODART=link_1_17.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODMAG
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DPCODMAG)
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.MGCODMAG as MGCODMAG120"+ ",link_1_20.MGDESMAG as MGDESMAG120"+ ",link_1_20.MGFLUBIC as MGFLUBIC120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on DIC_PROD.DPCODMAG=link_1_20.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODMAG=link_1_20.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODICE
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DPCODICE)
            select CACODICE,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DPCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CACODICE as CACODICE122"+ ",link_1_22.CADESART as CADESART122"+ ",link_1_22.CAUNIMIS as CAUNIMIS122"+ ",link_1_22.CAOPERAT as CAOPERAT122"+ ",link_1_22.CAMOLTIP as CAMOLTIP122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on DIC_PROD.DPCODICE=link_1_22.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODICE=link_1_22.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODCEN
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DPCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DPCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_DPCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_DPCODCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDPCODCEN_1_39'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DPCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DPCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCON = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCEN = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_39(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_39.CC_CONTO as CC_CONTO139"+ ",link_1_39.CCDESPIA as CCDESPIA139"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_39 on DIC_PROD.DPCODCEN=link_1_39.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_39"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODCEN=link_1_39.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODVOC
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DPCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DPCODVOC))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDPCODVOC_1_42'),i_cWhere,'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DPCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DPCODVOC)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_VOCDES = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_DTOBVO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODVOC = space(15)
      endif
      this.w_VOCDES = space(40)
      this.w_TIPVOC = space(1)
      this.w_DTOBVO = ctod("  /  /  ")
      this.w_CODCOS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_DPCODVOC = space(15)
        this.w_VOCDES = space(40)
        this.w_TIPVOC = space(1)
        this.w_DTOBVO = ctod("  /  /  ")
        this.w_CODCOS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_42(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_42.VCCODICE as VCCODICE142"+ ",link_1_42.VCDESCRI as VCDESCRI142"+ ",link_1_42.VCTIPVOC as VCTIPVOC142"+ ",link_1_42.VCDTOBSO as VCDTOBSO142"+ ",link_1_42.VCTIPCOS as VCTIPCOS142"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_42 on DIC_PROD.DPCODVOC=link_1_42.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_42"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODVOC=link_1_42.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODCOM
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DPCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DPCODCOM))
          select CNCODCAN,CNDTOBSO,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDPCODCOM_1_45'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DPCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DPCODCOM)
            select CNCODCAN,CNDTOBSO,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCOM = space(15)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DESCAN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_DPCODCOM = space(15)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DESCAN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_45.CNCODCAN as CNCODCAN145"+ ",link_1_45.CNDTOBSO as CNDTOBSO145"+ ",link_1_45.CNDESCAN as CNDESCAN145"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_45 on DIC_PROD.DPCODCOM=link_1_45.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_45"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODCOM=link_1_45.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODATT
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DPCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DPCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_DPTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_DPCODCOM;
                     ,'ATTIPATT',this.w_DPTIPATT;
                     ,'ATCODATT',trim(this.w_DPCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDPCODATT_1_47'),i_cWhere,'GSPC_BZZ',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODCOM<>oSource.xKey(1);
           .or. this.w_DPTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_DPCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_DPTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DPCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DPCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_DPTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_DPCODCOM;
                       ,'ATTIPATT',this.w_DPTIPATT;
                       ,'ATCODATT',this.w_DPCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_DPCODATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_DPCODCOM))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        endif
        this.w_DPCODATT = space(15)
        this.w_DESATT = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_47(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIVITA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_47.ATCODATT as ATCODATT147"+ ",link_1_47.ATDESCRI as ATDESCRI147"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_47 on DIC_PROD.DPCODATT=link_1_47.ATCODATT"+" and DIC_PROD.DPCODCOM=link_1_47.ATCODCOM"+" and DIC_PROD.DPTIPATT=link_1_47.ATTIPATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_47"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODATT=link_1_47.ATCODATT(+)"'+'+" and DIC_PROD.DPCODCOM=link_1_47.ATCODCOM(+)"'+'+" and DIC_PROD.DPTIPATT=link_1_47.ATTIPATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODLOT
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_DPCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_DPCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_DPCODART;
                     ,'LOCODICE',trim(this.w_DPCODLOT))
          select LOCODART,LOCODICE,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oDPCODLOT_1_50'),i_cWhere,'GSMD_ALO',"Anagrafica lotti",'GSMD_BZL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_DPCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_DPCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_DPCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_DPCODART;
                       ,'LOCODICE',this.w_DPCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DPCODLOT) OR (.w_ARTLOT=.w_DPCODART AND .w_FLSTAT<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        endif
        this.w_DPCODLOT = space(20)
        this.w_ARTLOT = space(20)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODUBI
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_DPCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_DPCODMAG;
                     ,'UBCODICE',trim(this.w_DPCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oDPCODUBI_1_52'),i_cWhere,'GSMD_MUB',"Ubicazioni",'GSCO_ADP.UBICAZIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_DPCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_DPCODMAG;
                       ,'UBCODICE',this.w_DPCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MGSCAR
  func Link_1_99(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGSCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGSCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MGSCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MGSCAR)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MGSCAR = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC1 = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MGSCAR = space(5)
      endif
      this.w_FLUBIC1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGSCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMGC
  func Link_1_109(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMGC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMGC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMMTCARI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUMGC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUMGC)
            select CMCODICE,CMMTCARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMGC = NVL(_Link_.CMCODICE,space(5))
      this.w_MTCARI = NVL(_Link_.CMMTCARI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMGC = space(5)
      endif
      this.w_MTCARI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMGC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDPSERIAL_1_1.value==this.w_DPSERIAL)
      this.oPgFrm.Page1.oPag.oDPSERIAL_1_1.value=this.w_DPSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDATREG_1_2.value==this.w_DPDATREG)
      this.oPgFrm.Page1.oPag.oDPDATREG_1_2.value=this.w_DPDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDPNUMOPE_1_3.value==this.w_DPNUMOPE)
      this.oPgFrm.Page1.oPag.oDPNUMOPE_1_3.value=this.w_DPNUMOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODODL_1_10.value==this.w_DPCODODL)
      this.oPgFrm.Page1.oPag.oDPCODODL_1_10.value=this.w_DPCODODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTODL_1_13.value==this.w_OLTQTODL)
      this.oPgFrm.Page1.oPag.oOLTQTODL_1_13.value=this.w_OLTQTODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTOEV_1_14.value==this.w_OLTQTOEV)
      this.oPgFrm.Page1.oPag.oOLTQTOEV_1_14.value=this.w_OLTQTOEV
    endif
    if not(this.oPgFrm.Page1.oPag.oQTARES_1_15.value==this.w_QTARES)
      this.oPgFrm.Page1.oPag.oQTARES_1_15.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTUNMIS_1_19.value==this.w_OLTUNMIS)
      this.oPgFrm.Page1.oPag.oOLTUNMIS_1_19.value=this.w_OLTUNMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODMAG_1_20.value==this.w_DPCODMAG)
      this.oPgFrm.Page1.oPag.oDPCODMAG_1_20.value=this.w_DPCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODICE_1_22.value==this.w_DPCODICE)
      this.oPgFrm.Page1.oPag.oDPCODICE_1_22.value=this.w_DPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPUNIMIS_1_23.value==this.w_DPUNIMIS)
      this.oPgFrm.Page1.oPag.oDPUNIMIS_1_23.value=this.w_DPUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDPQTAPRO_1_25.value==this.w_DPQTAPRO)
      this.oPgFrm.Page1.oPag.oDPQTAPRO_1_25.value=this.w_DPQTAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDPQTASCA_1_26.value==this.w_DPQTASCA)
      this.oPgFrm.Page1.oPag.oDPQTASCA_1_26.value=this.w_DPQTASCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPFLEVAS_1_27.RadioValue()==this.w_DPFLEVAS)
      this.oPgFrm.Page1.oPag.oDPFLEVAS_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADOC_1_32.RadioValue()==this.w_STADOC)
      this.oPgFrm.Page1.oPag.oSTADOC_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCEN_1_39.value==this.w_DPCODCEN)
      this.oPgFrm.Page1.oPag.oDPCODCEN_1_39.value=this.w_DPCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_40.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_40.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODVOC_1_42.value==this.w_DPCODVOC)
      this.oPgFrm.Page1.oPag.oDPCODVOC_1_42.value=this.w_DPCODVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oVOCDES_1_43.value==this.w_VOCDES)
      this.oPgFrm.Page1.oPag.oVOCDES_1_43.value=this.w_VOCDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCOM_1_45.value==this.w_DPCODCOM)
      this.oPgFrm.Page1.oPag.oDPCODCOM_1_45.value=this.w_DPCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODATT_1_47.value==this.w_DPCODATT)
      this.oPgFrm.Page1.oPag.oDPCODATT_1_47.value=this.w_DPCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_48.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_48.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_49.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_49.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODLOT_1_50.value==this.w_DPCODLOT)
      this.oPgFrm.Page1.oPag.oDPCODLOT_1_50.value=this.w_DPCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODUBI_1_52.value==this.w_DPCODUBI)
      this.oPgFrm.Page1.oPag.oDPCODUBI_1_52.value=this.w_DPCODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_56.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_56.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_59.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_59.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_80.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_80.value=this.w_DESUTE
    endif
    cp_SetControlsValueExtFlds(this,'DIC_PROD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DPSERIAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPSERIAL_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DPSERIAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DPDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPDATREG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DPDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLSOSPE<>'S' and (.w_STATOGES<>'LOAD' OR (.w_OLTSTATO='L' AND .w_OLTPROVE='I' AND .w_OLTFLEVA<>'S' AND .w_OLTQTODL>.w_OLTQTOEV) AND empty(NVL(.w_SERFAS,'')) AND empty(NVL(.w_SECIC,''))))  and (upper(.cFunction)<>'EDIT')  and not(empty(.w_DPCODODL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODODL_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("ODL non valido, sospeso o dichiarato in contemporanea da pi� utenti")
          case   not(.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST))  and not(g_PERCCR<>'S')  and (g_PERCCR='S')  and not(empty(.w_DPCODVOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODVOC_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(g_PERCAN<>'S' and g_COMM<>'S' and g_COAN<>'S')  and (g_PERCAN='S' or g_COMM='S')  and not(empty(.w_DPCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODCOM_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(NOT EMPTY(.w_DPCODATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_DPCODCOM)))  and not(g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))  and ((g_PERCAN='S' or g_COMM='S')  AND NOT EMPTY(.w_DPCODCOM))  and not(empty(.w_DPCODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODATT_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice attivit� inesistente o incongruente")
          case   not(EMPTY(.w_DPCODLOT) OR (.w_ARTLOT=.w_DPCODART AND .w_FLSTAT<>'S'))  and not(g_PERLOT<>'S')  and (g_PERLOT='S' AND .w_FLLOTT$ 'SC')  and not(empty(.w_DPCODLOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODLOT_1_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCO_MMP.CheckForm()
      if i_bres
        i_bres=  .GSCO_MMP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MRP.CheckForm()
      if i_bres
        i_bres=  .GSCO_MRP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MMT.CheckForm()
      if i_bres
        i_bres=  .GSCO_MMT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MCO.CheckForm()
      if i_bres
        i_bres=  .GSCO_MCO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MOU.CheckForm()
      if i_bres
        i_bres=  .GSCO_MOU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsco_adp
      * Prima esegue i controlli finali
      .w_TESTFORM=.T.
      if i_bRes
         .NotifyEvent('Controlli Finali')
         i_bRes = .w_TESTFORM
      endif
      if i_bRes and .w_FLLOTT$'SC' and EMPTY(.w_DPCODLOT)
        if .w_IMPLOT = 'S'
           i_bRes = .f.
           i_bnoChk = .f.	
           i_cErrorMsg = ah_MsgFormat("Non � stato inserito il dettaglio lotti/ubicazioni. %0Impossibile confermare")
        else
           ah_ErrorMsg("Non � stato inserito il dettaglio lotti/ubicazioni")
        endif
      endif
      
      if i_bres
        if alltrim(str(year(.w_DPDATREG)))<>g_CODESE
          i_bres = ah_yesno("Il documento verr� generato con data esterna all'esercizio di competenza. %0Si desidera proseguire?")
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DPCODODL = this.w_DPCODODL
    this.o_DPCODART = this.w_DPCODART
    this.o_DPCODMAG = this.w_DPCODMAG
    this.o_DPQTAPRO = this.w_DPQTAPRO
    this.o_DPQTASCA = this.w_DPQTASCA
    this.o_DPCODLOT = this.w_DPCODLOT
    this.o_DPCODUBI = this.w_DPCODUBI
    * --- GSCO_MMP : Depends On
    this.GSCO_MMP.SaveDependsOn()
    * --- GSCO_MRP : Depends On
    this.GSCO_MRP.SaveDependsOn()
    * --- GSCO_MMT : Depends On
    this.GSCO_MMT.SaveDependsOn()
    * --- GSCO_MCO : Depends On
    this.GSCO_MCO.SaveDependsOn()
    * --- GSCO_MOU : Depends On
    this.GSCO_MOU.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsco_adpPag1 as StdContainer
  Width  = 724
  height = 455
  stdWidth  = 724
  stdheight = 455
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPSERIAL_1_1 as StdField with uid="JYHDWBXNBQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DPSERIAL", cQueryName = "DPSERIAL",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo dichiarazione",;
    HelpContextID = 119480446,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=100, Top=11, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oDPDATREG_1_2 as StdField with uid="HEPMYBLWIJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DPDATREG", cQueryName = "DPDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 33288061,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=240, Top=11

  add object oDPNUMOPE_1_3 as StdField with uid="BPWYKEWEZG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DPNUMOPE", cQueryName = "DPNUMOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'operatore",;
    HelpContextID = 23031941,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=439, Top=11, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_DPNUMOPE"

  func oDPNUMOPE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDPCODODL_1_10 as StdField with uid="CLDBIJETVB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DPCODODL", cQueryName = "DPCODODL",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "ODL non valido, sospeso o dichiarato in contemporanea da pi� utenti",;
    HelpContextID = 32907390,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=100, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_DPCODODL"

  func oDPCODODL_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(.cFunction)<>'EDIT')
    endwith
   endif
  endfunc

  func oDPCODODL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODODL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODODL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oDPCODODL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"Elenco ODL",'GSCO_ADP.ODL_MAST_VZM',this.parent.oContained
  endproc
  proc oDPCODODL_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLCODODL=this.parent.oContained.w_DPCODODL
     i_obj.ecpSave()
  endproc

  add object oOLTQTODL_1_13 as StdField with uid="NWZEJLGNGL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_OLTQTODL", cQueryName = "OLTQTODL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� ordinata",;
    HelpContextID = 15930318,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=235, Top=178, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oOLTQTOEV_1_14 as StdField with uid="RYNLYEQOZP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OLTQTOEV", cQueryName = "OLTQTOEV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� evasa",;
    HelpContextID = 252505148,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=402, Top=178, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oQTARES_1_15 as StdField with uid="UVAHAVZKUM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� residua",;
    HelpContextID = 232995834,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=576, Top=178, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oOLTUNMIS_1_19 as StdField with uid="ZCWUZUZJOF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_OLTUNMIS", cQueryName = "OLTUNMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura ODL",;
    HelpContextID = 212921401,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=109, Top=178, InputMask=replicate('X',3)

  add object oDPCODMAG_1_20 as StdField with uid="QQUMLUETCH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DPCODMAG", cQueryName = "DPCODMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 66461827,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=152, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DPCODMAG"

  func oDPCODMAG_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DPCODUBI)
        bRes2=.link_1_52('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDPCODICE_1_22 as StdField with uid="ICPCACFDPB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DPCODICE", cQueryName = "DPCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo associato all'ODL",;
    HelpContextID = 133570693,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=126, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DPCODICE"

  func oDPCODICE_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDPUNIMIS_1_23 as StdField with uid="SNMPAXFHOA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DPUNIMIS", cQueryName = "DPUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura ODL",;
    HelpContextID = 207224713,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=100, Top=67, InputMask=replicate('X',3)

  add object oDPQTAPRO_1_25 as StdField with uid="BRPROJYBUN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DPQTAPRO", cQueryName = "DPQTAPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� da produrre",;
    HelpContextID = 18890875,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=240, Top=67, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oDPQTASCA_1_26 as StdField with uid="HHKIPKDNNC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DPQTASCA", cQueryName = "DPQTASCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale quantit� scartata",;
    HelpContextID = 236994697,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=456, Top=67, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oDPFLEVAS_1_27 as StdCheck with uid="AFRCDQEPBT",rtseq=26,rtrep=.f.,left=558, top=67, caption="ODL completato",;
    ToolTipText = "Se attivo: ODL completato",;
    HelpContextID = 183038071,;
    cFormVar="w_DPFLEVAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDPFLEVAS_1_27.RadioValue()
    return(iif(this.value =1,"S",;
    ' '))
  endfunc
  func oDPFLEVAS_1_27.GetRadio()
    this.Parent.oContained.w_DPFLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oDPFLEVAS_1_27.SetRadio()
    this.Parent.oContained.w_DPFLEVAS=trim(this.Parent.oContained.w_DPFLEVAS)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLEVAS=="S",1,;
      0)
  endfunc

  func oDPFLEVAS_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oSTADOC_1_32 as StdCheck with uid="JNBDANBLVK",rtseq=31,rtrep=.f.,left=558, top=90, caption="Stampa immediata",;
    ToolTipText = "Se attivo: stampa immediata dei documenti generati",;
    HelpContextID = 223427546,;
    cFormVar="w_STADOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADOC_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTADOC_1_32.GetRadio()
    this.Parent.oContained.w_STADOC = this.RadioValue()
    return .t.
  endfunc

  func oSTADOC_1_32.SetRadio()
    this.Parent.oContained.w_STADOC=trim(this.Parent.oContained.w_STADOC)
    this.value = ;
      iif(this.Parent.oContained.w_STADOC=='S',1,;
      0)
  endfunc

  add object oDPCODCEN_1_39 as StdField with uid="SMAGWUUUCR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DPCODCEN", cQueryName = "DPCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo associato",;
    HelpContextID = 34201476,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=237, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DPCODCEN"

  func oDPCODCEN_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oDPCODCEN_1_39.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oDPCODCEN_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODCEN_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCEN_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDPCODCEN_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oDPCODCEN_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DPCODCEN
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_40 as StdField with uid="ETZQFDWCRD",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38874058,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=231, Top=237, InputMask=replicate('X',40)

  func oDESCON_1_40.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDPCODVOC_1_42 as StdField with uid="ZAQEESVTFE",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DPCODVOC", cQueryName = "DPCODVOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
    ToolTipText = "Voce di costo analitica abbinata all'articolo",;
    HelpContextID = 183902343,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=264, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_DPCODVOC"

  func oDPCODVOC_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oDPCODVOC_1_42.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oDPCODVOC_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODVOC_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODVOC_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDPCODVOC_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oDPCODVOC_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_DPCODVOC
     i_obj.ecpSave()
  endproc

  add object oVOCDES_1_43 as StdField with uid="FNEYLPUUBZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_VOCDES", cQueryName = "VOCDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 233906346,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=231, Top=264, InputMask=replicate('X',40)

  func oVOCDES_1_43.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDPCODCOM_1_45 as StdField with uid="EOKQCWHWCJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DPCODCOM", cQueryName = "DPCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice della commessa associata al centro di costo",;
    HelpContextID = 234233981,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=291, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DPCODCOM"

  func oDPCODCOM_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S' or g_COMM='S')
    endwith
   endif
  endfunc

  func oDPCODCOM_1_45.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S' and g_COAN<>'S')
    endwith
  endfunc

  func oDPCODCOM_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
      if .not. empty(.w_DPCODATT)
        bRes2=.link_1_47('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDPCODCOM_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCOM_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDPCODCOM_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oDPCODCOM_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DPCODCOM
     i_obj.ecpSave()
  endproc

  add object oDPCODATT_1_47 as StdField with uid="VVYUUEIBCE",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DPCODATT", cQueryName = "DPCODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� inesistente o incongruente",;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 267788406,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=318, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_DPCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_DPTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DPCODATT"

  func oDPCODATT_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S')  AND NOT EMPTY(.w_DPCODCOM))
    endwith
   endif
  endfunc

  func oDPCODATT_1_47.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  func oDPCODATT_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODATT_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODATT_1_47.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_DPCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_DPCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_DPTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDPCODATT_1_47'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oDPCODATT_1_47.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_DPCODCOM
    i_obj.ATTIPATT=w_DPTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DPCODATT
     i_obj.ecpSave()
  endproc

  add object oDESCAN_1_48 as StdField with uid="YHHBBWWUZS",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53554122,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=231, Top=291, InputMask=replicate('X',40)

  func oDESCAN_1_48.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  add object oDESATT_1_49 as StdField with uid="BDHTVMWFVL",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 201534410,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=231, Top=318, InputMask=replicate('X',30)

  func oDESATT_1_49.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  add object oDPCODLOT_1_50 as StdField with uid="MJAKQGSQMM",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DPCODLOT", cQueryName = "DPCODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente o sospeso",;
    ToolTipText = "Codice lotto associato all'articolo da caricare",;
    HelpContextID = 83239030,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=345, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_DPCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_DPCODLOT"

  func oDPCODLOT_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT='S' AND .w_FLLOTT$ 'SC')
    endwith
   endif
  endfunc

  func oDPCODLOT_1_50.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
  endfunc

  func oDPCODLOT_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODLOT_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODLOT_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_DPCODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_DPCODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oDPCODLOT_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Anagrafica lotti",'GSMD_BZL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oDPCODLOT_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_DPCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_DPCODLOT
     i_obj.ecpSave()
  endproc


  add object oBtn_1_51 as StdButton with uid="GKUCVKDSGM",left=263, top=345, width=22,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per creare nuovo lotto";
    , HelpContextID = 175729622;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_PERLOT='S' AND .w_FLLOTT$ 'SC')
      endwith
    endif
  endfunc

  func oBtn_1_51.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_PERLOT<>'S')
     endwith
    endif
  endfunc

  add object oDPCODUBI_1_52 as StdField with uid="MPIVRIWUFC",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DPCODUBI", cQueryName = "DPCODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente o sospeso",;
    ToolTipText = "Codice ubicazione associata al magazzino da caricare",;
    HelpContextID = 200679553,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=375, Top=345, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_DPCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_DPCODUBI"

  func oDPCODUBI_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI='S' AND .w_FLUBIC='S')
    endwith
   endif
  endfunc

  func oDPCODUBI_1_52.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  func oDPCODUBI_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODUBI_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODUBI_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_DPCODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_DPCODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oDPCODUBI_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Ubicazioni",'GSCO_ADP.UBICAZIO_VZM',this.parent.oContained
  endproc
  proc oDPCODUBI_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_DPCODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_DPCODUBI
     i_obj.ecpSave()
  endproc

  add object oDESART_1_56 as StdField with uid="MIKMVMQFZH",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203631562,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=266, Top=126, InputMask=replicate('X',40)

  add object oDESMAG_1_59 as StdField with uid="CEOVSXFBFF",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 170339274,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=174, Top=152, InputMask=replicate('X',30)

  add object oDESUTE_1_80 as StdField with uid="TIOPLZQQPB",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 183446474,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=484, Top=11, InputMask=replicate('X',20)


  add object oObj_1_81 as cp_runprogram with uid="SIYYGYFHQY",left=17, top=473, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('MATERIALI')",;
    cEvent = "w_DPCODODL Changed",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_84 as cp_runprogram with uid="TGJXHGSNRT",left=17, top=541, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('QTAMAT')",;
    cEvent = "w_DPQTAPRO Changed,w_DPQTASCA Changed,w_DPFLEVAS Changed,w_DPCODCOM Changed",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_85 as cp_runprogram with uid="VMOFKKFTFG",left=17, top=490, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CARICA')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_87 as cp_runprogram with uid="YJMSCAUSBL",left=17, top=507, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CONTROLLI')",;
    cEvent = "Controlli Finali",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_88 as cp_runprogram with uid="PXTMGCWWPR",left=17, top=524, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('ELIMINA')",;
    cEvent = "Delete start,Update start",;
    nPag=1;
    , HelpContextID = 222698826


  add object oLinkPC_1_94 as StdButton with uid="EDNQHYPCNP",left=632, top=231, width=48,height=45,;
    CpPicture="bmp\MATRICOLE.bmp", caption="", nPag=1;
    , ToolTipText = "Dettaglio matricole articoli prodotti";
    , HelpContextID = 192936062;
    , Caption='\<Matricole';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_94.Click()
      this.Parent.oContained.GSCO_MMT.LinkPCClick()
    endproc

  func oLinkPC_1_94.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_GESMAT='S' And .w_DPQTAPRO+.w_DPQTASCA>0)
      endwith
    endif
  endfunc

  func oLinkPC_1_94.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GESMAT<>'S')
     endwith
    endif
  endfunc


  add object oLinkPC_1_95 as StdButton with uid="DMSDMXHHNV",left=632, top=275, width=48,height=45,;
    CpPicture="bmp\Matrcomp.bmp", caption="", nPag=1;
    , ToolTipText = "Dettaglio matricole componenti utilizzati";
    , HelpContextID = 266211028;
    , Caption='\<Componenti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_95.Click()
      this.Parent.oContained.GSCO_MCO.LinkPCClick()
      this.Parent.oContained.WriteTo_GSCO_MCO()
    endproc

  func oLinkPC_1_95.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MATENABL and .w_DPCOMMAT="S")
      endwith
    endif
  endfunc

  func oLinkPC_1_95.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not .w_MATENABL or .w_DPCOMMAT<>"S")
     endwith
    endif
  endfunc


  add object oBtn_1_96 as StdButton with uid="KKTWUWGJAF",left=632, top=319, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di carico prodotti per produzione";
    , HelpContextID = 266936282;
    , Caption='\<Carico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_96.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"CARI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_96.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFCAR) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_96.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFCAR))
     endwith
    endif
  endfunc


  add object oBtn_1_97 as StdButton with uid="FZPWBPRUQW",left=632, top=363, width=48,height=45,;
    CpPicture="BMP\NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di scarico materiali";
    , HelpContextID = 193015002;
    , Caption='\<Scarico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_97.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"SCAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_97.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFSCA) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_97.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFSCA))
     endwith
    endif
  endfunc


  add object oObj_1_114 as cp_runprogram with uid="PYXEELJVCC",left=17, top=558, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('SETMATR')",;
    cEvent = "w_DPCODODL Changed",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_115 as cp_runprogram with uid="UJFNRIMMLG",left=17, top=575, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('UPDMATR')",;
    cEvent = "w_DPQTAPRO Changed,w_DPQTASCA Changed",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_116 as cp_runprogram with uid="QFSYCKOZLJ",left=17, top=592, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('F3PRESSED')",;
    cEvent = "Edit Started",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_118 as cp_runprogram with uid="CNJMISAOPR",left=17, top=609, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CHKUPDATED')",;
    cEvent = "Controlli Updated",;
    nPag=1;
    , HelpContextID = 222698826


  add object oObj_1_121 as cp_runprogram with uid="XGPHTFWEDU",left=326, top=473, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CTRLMAT')",;
    cEvent = "CTRLMAT",;
    nPag=1;
    , HelpContextID = 222698826


  add object oBtn_1_127 as StdButton with uid="RBQMJUNGDP",left=632, top=407, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di carico materiali di output";
    , HelpContextID = 77364358;
    , Caption='Mat.\<Output';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_127.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"MOUT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_127.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFMOU) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_127.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFMOU))
     endwith
    endif
  endfunc


  add object oObj_1_136 as cp_runprogram with uid="PIVNIHZEGA",left=17, top=626, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('USCITA')",;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 222698826

  add object oStr_1_9 as StdString with uid="DIGFMBVGOE",Visible=.t., Left=352, Top=11,;
    Alignment=1, Width=82, Height=18,;
    Caption="Operatore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="WXPCDTXCKN",Visible=.t., Left=3, Top=39,;
    Alignment=1, Width=95, Height=18,;
    Caption="ODL:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="ZIQFMADMCZ",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=95, Height=18,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="NVUJQZDGDF",Visible=.t., Left=203, Top=11,;
    Alignment=1, Width=35, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="LOAHVGVQKE",Visible=.t., Left=5, Top=205,;
    Alignment=0, Width=184, Height=18,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="OHIYQZEKQI",Visible=.t., Left=12, Top=237,;
    Alignment=1, Width=95, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="PVEMQUYUNU",Visible=.t., Left=12, Top=264,;
    Alignment=1, Width=95, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="MNYUTFZFML",Visible=.t., Left=12, Top=291,;
    Alignment=1, Width=95, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="CTCXTYMFHC",Visible=.t., Left=12, Top=318,;
    Alignment=1, Width=95, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="GEWDGAGYKJ",Visible=.t., Left=5, Top=98,;
    Alignment=0, Width=184, Height=18,;
    Caption="Dati ODL"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="NUCDGRRCSM",Visible=.t., Left=3, Top=67,;
    Alignment=1, Width=95, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="CDSCIQDCHO",Visible=.t., Left=145, Top=67,;
    Alignment=1, Width=93, Height=18,;
    Caption="Qta prodotta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="HLCTRMYUDF",Visible=.t., Left=350, Top=67,;
    Alignment=1, Width=104, Height=18,;
    Caption="Qta scartata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="BMHBEQZKXC",Visible=.t., Left=12, Top=123,;
    Alignment=1, Width=95, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="CLUZGRALEB",Visible=.t., Left=12, Top=152,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="RMWUWNXGSO",Visible=.t., Left=12, Top=178,;
    Alignment=1, Width=95, Height=15,;
    Caption="UM:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="IZKADZIYEQ",Visible=.t., Left=342, Top=178,;
    Alignment=1, Width=55, Height=15,;
    Caption="Evasa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="RZVHFBWORF",Visible=.t., Left=155, Top=178,;
    Alignment=1, Width=77, Height=15,;
    Caption="Pianificata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="ZAAPRCQBLL",Visible=.t., Left=507, Top=178,;
    Alignment=1, Width=67, Height=18,;
    Caption="Residua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="AUOXIAUZIZ",Visible=.t., Left=12, Top=345,;
    Alignment=1, Width=95, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="PBFSNOWELL",Visible=.t., Left=294, Top=345,;
    Alignment=1, Width=78, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oBox_1_37 as StdBox with uid="OANOEOLUAB",left=2, top=226, width=676,height=2

  add object oBox_1_58 as StdBox with uid="IGSZBYYUKD",left=2, top=119, width=668,height=2
enddefine
define class tgsco_adpPag2 as StdContainer
  Width  = 724
  height = 455
  stdWidth  = 724
  stdheight = 455
  resizeXpos=291
  resizeYpos=322
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="AXVJSZOODA",left=2, top=7, width=677, height=439, bOnScreen=.t.;

enddefine
define class tgsco_adpPag3 as StdContainer
  Width  = 724
  height = 455
  stdWidth  = 724
  stdheight = 455
  resizeXpos=286
  resizeYpos=324
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="ZHDDJHLJPA",left=2, top=8, width=666, height=421, bOnScreen=.t.;

enddefine
define class tgsco_adpPag4 as StdContainer
  Width  = 724
  height = 455
  stdWidth  = 724
  stdheight = 455
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="EPGOSDQTQX",left=-2, top=5, width=726, height=448, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_adp','DIC_PROD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DPSERIAL=DIC_PROD.DPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
