* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kpp                                                        *
*              Parametri produzione                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_140]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-20                                                      *
* Last revis.: 2015-12-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kpp",oParentObject))

* --- Class definition
define class tgsco_kpp as StdForm
  Top    = 2
  Left   = 17

  * --- Standard Properties
  Width  = 556
  Height = 493+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-09"
  HelpContextID=118905495
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=136

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  CENCOST_IDX = 0
  TIP_DOCU_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  TAB_CALE_IDX = 0
  CAN_TIER_IDX = 0
  ART_ICOL_IDX = 0
  CCF_MAST_IDX = 0
  UNIMIS_IDX = 0
  RIS_ORSE_IDX = 0
  cPrg = "gsco_kpp"
  cComment = "Parametri produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_PPNMAXPE = 0
  w_PPNUMGIO = 0
  w_PPNUMSET = 0
  w_PPNUMMES = 0
  w_PPNUMTRI = 0
  w_PPCOLMPS = 0
  w_PPFONMPS = 0
  w_PPGIOSCA = 0
  w_PP___DTF = 0
  w_DATOBSO = ctod('  /  /  ')
  w_COLSUM = 0
  w_SFOSUM = 0
  w_COLPIA = 0
  w_SFOPIA = 0
  w_COLLAN = 0
  w_SFOLAN = 0
  w_COLMIS = 0
  w_SFOMIS = 0
  w_SFOMCO = 0
  w_COLFES = 0
  w_ERRORE = .F.
  w_OGESWIP = space(1)
  w_COLSUG = 0
  w_SFOSUG = 0
  w_COLCON = 0
  w_SFOCON = 0
  w_COLDPI = 0
  w_SFODPI = 0
  w_PPGESWIP = space(10)
  o_PPGESWIP = space(10)
  w_PPCAUORD = space(5)
  w_DESORD = space(35)
  w_PPCAUIMP = space(5)
  w_DESIMP = space(35)
  w_PPCAUTER = space(5)
  w_DESCAZ = space(35)
  w_PPCAURCL = space(5)
  w_DESDOC = space(40)
  w_PPCAUTRA = space(5)
  w_DESCAT = space(35)
  w_PPCODCAU = space(5)
  w_DESCAB = space(35)
  w_PPCAUCAR = space(5)
  w_DESCAR = space(35)
  w_PPCAUSCA = space(5)
  w_DESSCA = space(35)
  w_PPCAUMOU = space(5)
  w_PPMAGPRO = space(5)
  w_DESMAG = space(30)
  w_PPMAGSCA = space(5)
  w_DESMAS = space(30)
  w_PPMAGWIP = space(5)
  w_DESMAW = space(30)
  w_PPCENCOS = space(15)
  w_PPCAUCCM = space(5)
  w_PPCAUSCM = space(5)
  w_DESCEN = space(40)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_PROWIP = space(1)
  w_DISMAG = space(1)
  w_TIPWIP = space(1)
  w_FLVEAZ = space(1)
  w_CATDOZ = space(2)
  w_FLVEAT = space(1)
  w_CATDOT = space(2)
  w_FLINTB = space(1)
  w_CATDOB = space(2)
  w_FLINTC = space(1)
  w_CAUCAR = space(5)
  w_FLINTS = space(1)
  w_CAUSCA = space(5)
  w_FLANAL = space(1)
  w_SCADIS = space(1)
  w_SCAWIP = space(1)
  w_PPCALSTA = space(5)
  w_DESCAL = space(40)
  w_DESCCM = space(35)
  w_DESSCM = space(35)
  w_CHKCC = space(1)
  w_CHKCS = space(1)
  w_CHKCSC = space(1)
  w_CHKCSS = space(1)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_PPPSCLAV = space(1)
  w_DESCAM = space(35)
  w_CAUMOU = space(5)
  w_FLINTM = space(1)
  w_CRIFOR = space(1)
  w_PPCCSODA = space(15)
  w_DESCCODA = space(40)
  w_PPBORODA = space(1)
  w_PP__MODA = space(1)
  w_NUMLVODA = 0
  w_PPBLOMRP = space(1)
  w_PPMRPLOG = space(1)
  w_PPSALCOM = space(1)
  o_PPSALCOM = space(1)
  w_PPMRPDIV = space(1)
  w_PPORDICM = space(1)
  w_PPSCAMRP = space(1)
  w_PPCODCOM = space(15)
  w_DESCOM = space(40)
  w_PPDICCOM = space(1)
  w_PPATHMRP = space(200)
  o_PPATHMRP = space(200)
  w_PPIMPLOT = space(1)
  w_PPPRJMOD = space(200)
  w_PPPERPIA = space(1)
  w_PPCRIELA = space(1)
  w_PPPIAPUN = space(1)
  w_TIPART = space(2)
  w_PPSRVDFT = space(20)
  w_PPMATINP = space(1)
  w_PPMATOUP = space(1)
  w_PPMGARFS = space(1)
  w_PPPREFAS = space(5)
  o_PPPREFAS = space(5)
  w_PPFASSEP = space(1)
  w_PPCLAART = space(5)
  w_PPDCODFAS = space(1)
  w_PPDSUPFA = space(1)
  w_PPSRVDSC = space(40)
  w_ARTIPART = space(2)
  w_PPCLAFAS = space(5)
  w_PPDESCFAS = space(30)
  w_PP_CICLI = space(1)
  w_UNIPRO = space(10)
  w_PPUNIPRO = space(20)
  w_DEUNIPRO = space(40)
  w_PPFECLAV = space(1)
  w_PPDESCART = space(30)
  w_CFTIPCLA = space(10)
  w_CFTIPCLC = space(10)
  w_PPTIPCLA = space(10)
  w_PPTIPCLC = space(10)
  w_PPNOTFAS = space(1)
  w_SUGGE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_kppPag1","gsco_kpp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(2).addobject("oPag","tgsco_kppPag2","gsco_kpp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Causali movimenti")
      .Pages(3).addobject("oPag","tgsco_kppPag3","gsco_kpp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Ordini di acquisto")
      .Pages(4).addobject("oPag","tgsco_kppPag4","gsco_kpp",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Pianificazione della produzione")
      .Pages(5).addobject("oPag","tgsco_kppPag5","gsco_kpp",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Funzioni avanzate")
      .Pages(5).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPPNUMGIO_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_SUGGE = this.oPgFrm.Pages(1).oPag.SUGGE
    DoDefault()
    proc Destroy()
      this.w_SUGGE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='INVENTAR'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='CAM_AGAZ'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='TAB_CALE'
    this.cWorkTables[10]='CAN_TIER'
    this.cWorkTables[11]='ART_ICOL'
    this.cWorkTables[12]='CCF_MAST'
    this.cWorkTables[13]='UNIMIS'
    this.cWorkTables[14]='RIS_ORSE'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_PPNMAXPE=0
      .w_PPNUMGIO=0
      .w_PPNUMSET=0
      .w_PPNUMMES=0
      .w_PPNUMTRI=0
      .w_PPCOLMPS=0
      .w_PPFONMPS=0
      .w_PPGIOSCA=0
      .w_PP___DTF=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_COLSUM=0
      .w_SFOSUM=0
      .w_COLPIA=0
      .w_SFOPIA=0
      .w_COLLAN=0
      .w_SFOLAN=0
      .w_COLMIS=0
      .w_SFOMIS=0
      .w_SFOMCO=0
      .w_COLFES=0
      .w_ERRORE=.f.
      .w_OGESWIP=space(1)
      .w_COLSUG=0
      .w_SFOSUG=0
      .w_COLCON=0
      .w_SFOCON=0
      .w_COLDPI=0
      .w_SFODPI=0
      .w_PPGESWIP=space(10)
      .w_PPCAUORD=space(5)
      .w_DESORD=space(35)
      .w_PPCAUIMP=space(5)
      .w_DESIMP=space(35)
      .w_PPCAUTER=space(5)
      .w_DESCAZ=space(35)
      .w_PPCAURCL=space(5)
      .w_DESDOC=space(40)
      .w_PPCAUTRA=space(5)
      .w_DESCAT=space(35)
      .w_PPCODCAU=space(5)
      .w_DESCAB=space(35)
      .w_PPCAUCAR=space(5)
      .w_DESCAR=space(35)
      .w_PPCAUSCA=space(5)
      .w_DESSCA=space(35)
      .w_PPCAUMOU=space(5)
      .w_PPMAGPRO=space(5)
      .w_DESMAG=space(30)
      .w_PPMAGSCA=space(5)
      .w_DESMAS=space(30)
      .w_PPMAGWIP=space(5)
      .w_DESMAW=space(30)
      .w_PPCENCOS=space(15)
      .w_PPCAUCCM=space(5)
      .w_PPCAUSCM=space(5)
      .w_DESCEN=space(40)
      .w_FLORDI=space(1)
      .w_FLIMPE=space(1)
      .w_PROWIP=space(1)
      .w_DISMAG=space(1)
      .w_TIPWIP=space(1)
      .w_FLVEAZ=space(1)
      .w_CATDOZ=space(2)
      .w_FLVEAT=space(1)
      .w_CATDOT=space(2)
      .w_FLINTB=space(1)
      .w_CATDOB=space(2)
      .w_FLINTC=space(1)
      .w_CAUCAR=space(5)
      .w_FLINTS=space(1)
      .w_CAUSCA=space(5)
      .w_FLANAL=space(1)
      .w_SCADIS=space(1)
      .w_SCAWIP=space(1)
      .w_PPCALSTA=space(5)
      .w_DESCAL=space(40)
      .w_DESCCM=space(35)
      .w_DESSCM=space(35)
      .w_CHKCC=space(1)
      .w_CHKCS=space(1)
      .w_CHKCSC=space(1)
      .w_CHKCSS=space(1)
      .w_FLVEAC=space(1)
      .w_CATDOC=space(2)
      .w_PPPSCLAV=space(1)
      .w_DESCAM=space(35)
      .w_CAUMOU=space(5)
      .w_FLINTM=space(1)
      .w_CRIFOR=space(1)
      .w_PPCCSODA=space(15)
      .w_DESCCODA=space(40)
      .w_PPBORODA=space(1)
      .w_PP__MODA=space(1)
      .w_NUMLVODA=0
      .w_PPBLOMRP=space(1)
      .w_PPMRPLOG=space(1)
      .w_PPSALCOM=space(1)
      .w_PPMRPDIV=space(1)
      .w_PPORDICM=space(1)
      .w_PPSCAMRP=space(1)
      .w_PPCODCOM=space(15)
      .w_DESCOM=space(40)
      .w_PPDICCOM=space(1)
      .w_PPATHMRP=space(200)
      .w_PPIMPLOT=space(1)
      .w_PPPRJMOD=space(200)
      .w_PPPERPIA=space(1)
      .w_PPCRIELA=space(1)
      .w_PPPIAPUN=space(1)
      .w_TIPART=space(2)
      .w_PPSRVDFT=space(20)
      .w_PPMATINP=space(1)
      .w_PPMATOUP=space(1)
      .w_PPMGARFS=space(1)
      .w_PPPREFAS=space(5)
      .w_PPFASSEP=space(1)
      .w_PPCLAART=space(5)
      .w_PPDCODFAS=space(1)
      .w_PPDSUPFA=space(1)
      .w_PPSRVDSC=space(40)
      .w_ARTIPART=space(2)
      .w_PPCLAFAS=space(5)
      .w_PPDESCFAS=space(30)
      .w_PP_CICLI=space(1)
      .w_UNIPRO=space(10)
      .w_PPUNIPRO=space(20)
      .w_DEUNIPRO=space(40)
      .w_PPFECLAV=space(1)
      .w_PPDESCART=space(30)
      .w_CFTIPCLA=space(10)
      .w_CFTIPCLC=space(10)
      .w_PPTIPCLA=space(10)
      .w_PPTIPCLC=space(10)
      .w_PPNOTFAS=space(1)
        .w_CODAZI = i_CODAZI
        .w_OBTEST = i_DATSYS
          .DoRTCalc(3,7,.f.)
        .w_PPCOLMPS = 65
        .w_PPFONMPS = 9
        .w_PPGIOSCA = 30
      .oPgFrm.Page1.oPag.SUGGE.Calculate('      1234567890     ',.w_COLSUM,.w_SFOSUM)
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate('      1234567890     ',.w_COLPIA,.w_SFOPIA)
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate('      1234567890     ',.w_COLLAN,.w_SFOLAN)
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate('      1234567890     ',.w_COLMIS,.w_SFOMIS)
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate('      1234567890     ',.w_COLFES,RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate('      1234567890     ',RGB(0,0,0),.w_SFOMCO)
          .DoRTCalc(11,22,.f.)
        .w_ERRORE = .F.
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate('      1234567890     ',.w_COLSUG,.w_SFOSUG)
      .oPgFrm.Page1.oPag.oObj_1_81.Calculate('      1234567890     ',.w_COLCON,.w_SFOCON)
      .oPgFrm.Page1.oPag.oObj_1_84.Calculate('      1234567890     ',.w_COLDPI,.w_SFODPI)
      .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
          .DoRTCalc(24,30,.f.)
        .w_PPGESWIP = ' '
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_PPCAUORD))
          .link_2_2('Full')
        endif
        .DoRTCalc(33,34,.f.)
        if not(empty(.w_PPCAUIMP))
          .link_2_4('Full')
        endif
        .DoRTCalc(35,36,.f.)
        if not(empty(.w_PPCAUTER))
          .link_2_6('Full')
        endif
        .DoRTCalc(37,38,.f.)
        if not(empty(.w_PPCAURCL))
          .link_2_8('Full')
        endif
        .DoRTCalc(39,40,.f.)
        if not(empty(.w_PPCAUTRA))
          .link_2_10('Full')
        endif
          .DoRTCalc(41,41,.f.)
        .w_PPCODCAU = IIF(Empty(.w_PPGESWIP) , ' ', .w_PPCODCAU)
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_PPCODCAU))
          .link_2_12('Full')
        endif
        .DoRTCalc(43,44,.f.)
        if not(empty(.w_PPCAUCAR))
          .link_2_14('Full')
        endif
        .DoRTCalc(45,46,.f.)
        if not(empty(.w_PPCAUSCA))
          .link_2_16('Full')
        endif
        .DoRTCalc(47,48,.f.)
        if not(empty(.w_PPCAUMOU))
          .link_2_18('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_PPMAGPRO))
          .link_2_19('Full')
        endif
        .DoRTCalc(50,51,.f.)
        if not(empty(.w_PPMAGSCA))
          .link_2_21('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_PPMAGWIP = IIF(Empty(.w_PPGESWIP), ' ', .w_PPMAGWIP)
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_PPMAGWIP))
          .link_2_23('Full')
        endif
        .DoRTCalc(54,55,.f.)
        if not(empty(.w_PPCENCOS))
          .link_2_25('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_PPCAUCCM))
          .link_2_26('Full')
        endif
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_PPCAUSCM))
          .link_2_27('Full')
        endif
        .DoRTCalc(58,77,.f.)
        if not(empty(.w_PPCALSTA))
          .link_2_58('Full')
        endif
        .DoRTCalc(78,92,.f.)
        if not(empty(.w_PPCCSODA))
          .link_3_2('Full')
        endif
          .DoRTCalc(93,94,.f.)
        .w_PP__MODA = 'N'
          .DoRTCalc(96,97,.f.)
        .w_PPMRPLOG = 'N'
        .w_PPSALCOM = 'P'
        .w_PPMRPDIV = 'N'
        .w_PPORDICM = iif(.w_PPSALCOM='S',.w_PPORDICM,'N')
        .w_PPSCAMRP = iif(.w_PPSALCOM='S','N',.w_PPSCAMRP)
        .DoRTCalc(103,103,.f.)
        if not(empty(.w_PPCODCOM))
          .link_4_10('Full')
        endif
          .DoRTCalc(104,104,.f.)
        .w_PPDICCOM = 'C'
        .w_PPATHMRP = iif(!EMPTY(alltrim(.w_PPATHMRP)), FULLPATH(alltrim(.w_PPATHMRP)+iif(right(alltrim(.w_PPATHMRP),1)="\","","\")), '')
        .w_PPIMPLOT = 'N'
          .DoRTCalc(108,108,.f.)
        .w_PPPERPIA = 'D'
          .DoRTCalc(110,110,.f.)
        .w_PPPIAPUN = 'N'
        .w_TIPART = 'FM'
        .DoRTCalc(113,113,.f.)
        if not(empty(.w_PPSRVDFT))
          .link_5_2('Full')
        endif
        .w_PPMATINP = 'S'
        .w_PPMATOUP = 'N'
        .w_PPMGARFS = 'C'
        .DoRTCalc(117,119,.f.)
        if not(empty(.w_PPCLAART))
          .link_5_9('Full')
        endif
        .w_PPDCODFAS = 'S'
        .w_PPDSUPFA = 'S'
        .DoRTCalc(122,124,.f.)
        if not(empty(.w_PPCLAFAS))
          .link_5_21('Full')
        endif
          .DoRTCalc(125,125,.f.)
        .w_PP_CICLI = 'N'
        .w_UNIPRO = 'UP'
        .DoRTCalc(128,128,.f.)
        if not(empty(.w_PPUNIPRO))
          .link_5_29('Full')
        endif
          .DoRTCalc(129,133,.f.)
        .w_PPTIPCLA = 'A'
        .w_PPTIPCLC = 'C'
        .w_PPNOTFAS = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_16.enabled = this.oPgFrm.Page4.oPag.oBtn_4_16.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_23.enabled = this.oPgFrm.Page4.oPag.oBtn_4_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.SUGGE.Calculate('      1234567890     ',.w_COLSUM,.w_SFOSUM)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate('      1234567890     ',.w_COLPIA,.w_SFOPIA)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate('      1234567890     ',.w_COLLAN,.w_SFOLAN)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate('      1234567890     ',.w_COLMIS,.w_SFOMIS)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate('      1234567890     ',.w_COLFES,RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate('      1234567890     ',RGB(0,0,0),.w_SFOMCO)
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('      1234567890     ',.w_COLSUG,.w_SFOSUG)
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate('      1234567890     ',.w_COLCON,.w_SFOCON)
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate('      1234567890     ',.w_COLDPI,.w_SFODPI)
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .DoRTCalc(1,41,.t.)
        if .o_PPGESWIP<>.w_PPGESWIP
            .w_PPCODCAU = IIF(Empty(.w_PPGESWIP) , ' ', .w_PPCODCAU)
          .link_2_12('Full')
        endif
        .DoRTCalc(43,52,.t.)
        if .o_PPGESWIP<>.w_PPGESWIP
            .w_PPMAGWIP = IIF(Empty(.w_PPGESWIP), ' ', .w_PPMAGWIP)
          .link_2_23('Full')
        endif
        .DoRTCalc(54,100,.t.)
        if .o_PPSALCOM<>.w_PPSALCOM
            .w_PPORDICM = iif(.w_PPSALCOM='S',.w_PPORDICM,'N')
        endif
        if .o_PPSALCOM<>.w_PPSALCOM
            .w_PPSCAMRP = iif(.w_PPSALCOM='S','N',.w_PPSCAMRP)
        endif
        .DoRTCalc(103,105,.t.)
        if .o_PPATHMRP<>.w_PPATHMRP
            .w_PPATHMRP = iif(!EMPTY(alltrim(.w_PPATHMRP)), FULLPATH(alltrim(.w_PPATHMRP)+iif(right(alltrim(.w_PPATHMRP),1)="\","","\")), '')
        endif
        if .o_PPPREFAS<>.w_PPPREFAS
          .Calculate_OYKEJWPRFE()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(107,136,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.SUGGE.Calculate('      1234567890     ',.w_COLSUM,.w_SFOSUM)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate('      1234567890     ',.w_COLPIA,.w_SFOPIA)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate('      1234567890     ',.w_COLLAN,.w_SFOLAN)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate('      1234567890     ',.w_COLMIS,.w_SFOMIS)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate('      1234567890     ',.w_COLFES,RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate('      1234567890     ',RGB(0,0,0),.w_SFOMCO)
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('      1234567890     ',.w_COLSUG,.w_SFOSUG)
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate('      1234567890     ',.w_COLCON,.w_SFOCON)
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate('      1234567890     ',.w_COLDPI,.w_SFODPI)
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
    endwith
  return

  proc Calculate_OYKEJWPRFE()
    with this
          * --- 
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oPPCODCAU_2_12.enabled = this.oPgFrm.Page2.oPag.oPPCODCAU_2_12.mCond()
    this.oPgFrm.Page2.oPag.oPPMAGWIP_2_23.enabled = this.oPgFrm.Page2.oPag.oPPMAGWIP_2_23.mCond()
    this.oPgFrm.Page2.oPag.oPPCENCOS_2_25.enabled = this.oPgFrm.Page2.oPag.oPPCENCOS_2_25.mCond()
    this.oPgFrm.Page3.oPag.oPPCCSODA_3_2.enabled = this.oPgFrm.Page3.oPag.oPPCCSODA_3_2.mCond()
    this.oPgFrm.Page4.oPag.oPPORDICM_4_6.enabled = this.oPgFrm.Page4.oPag.oPPORDICM_4_6.mCond()
    this.oPgFrm.Page4.oPag.oPPSCAMRP_4_9.enabled = this.oPgFrm.Page4.oPag.oPPSCAMRP_4_9.mCond()
    this.oPgFrm.Page5.oPag.oPPPREFAS_5_7.enabled = this.oPgFrm.Page5.oPag.oPPPREFAS_5_7.mCond()
    this.oPgFrm.Page5.oPag.oPPFASSEP_5_8.enabled = this.oPgFrm.Page5.oPag.oPPFASSEP_5_8.mCond()
    this.oPgFrm.Page5.oPag.oPPCLAART_5_9.enabled = this.oPgFrm.Page5.oPag.oPPCLAART_5_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page5.oPag.oPPPREFAS_5_7
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page5.oPag.oPPFASSEP_5_8
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page5.oPag.oPPCLAART_5_9
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page5.oPag.oPPCLAFAS_5_21
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page5.oPag.oPPUNIPRO_5_29
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(5).enabled=not(g_PRFA<>'S')
    this.oPgFrm.Page1.oPag.oPPGIOSCA_1_10.visible=!this.oPgFrm.Page1.oPag.oPPGIOSCA_1_10.mHide()
    this.oPgFrm.Page1.oPag.oPP___DTF_1_11.visible=!this.oPgFrm.Page1.oPag.oPP___DTF_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page2.oPag.oPPCODCAU_2_12.visible=!this.oPgFrm.Page2.oPag.oPPCODCAU_2_12.mHide()
    this.oPgFrm.Page2.oPag.oDESCAB_2_13.visible=!this.oPgFrm.Page2.oPag.oDESCAB_2_13.mHide()
    this.oPgFrm.Page2.oPag.oPPMAGWIP_2_23.visible=!this.oPgFrm.Page2.oPag.oPPMAGWIP_2_23.mHide()
    this.oPgFrm.Page2.oPag.oDESMAW_2_24.visible=!this.oPgFrm.Page2.oPag.oDESMAW_2_24.mHide()
    this.oPgFrm.Page2.oPag.oPPCENCOS_2_25.visible=!this.oPgFrm.Page2.oPag.oPPCENCOS_2_25.mHide()
    this.oPgFrm.Page2.oPag.oPPCAUCCM_2_26.visible=!this.oPgFrm.Page2.oPag.oPPCAUCCM_2_26.mHide()
    this.oPgFrm.Page2.oPag.oPPCAUSCM_2_27.visible=!this.oPgFrm.Page2.oPag.oPPCAUSCM_2_27.mHide()
    this.oPgFrm.Page2.oPag.oDESCEN_2_28.visible=!this.oPgFrm.Page2.oPag.oDESCEN_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_29.visible=!this.oPgFrm.Page2.oPag.oStr_2_29.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_55.visible=!this.oPgFrm.Page2.oPag.oStr_2_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oDESCCM_2_63.visible=!this.oPgFrm.Page2.oPag.oDESCCM_2_63.mHide()
    this.oPgFrm.Page2.oPag.oDESSCM_2_64.visible=!this.oPgFrm.Page2.oPag.oDESSCM_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_73.visible=!this.oPgFrm.Page2.oPag.oStr_2_73.mHide()
    this.oPgFrm.Page4.oPag.oPPMRPLOG_4_2.visible=!this.oPgFrm.Page4.oPag.oPPMRPLOG_4_2.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.SUGGE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_97.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_98.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCAUORD
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PPCAUORD)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLORDI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PPCAUORD))
          select CMCODICE,CMDESCRI,CMFLORDI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUORD)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUORD) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPPCAUORD_2_2'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSCOOKPP.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLORDI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLORDI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLORDI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PPCAUORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PPCAUORD)
            select CMCODICE,CMDESCRI,CMFLORDI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUORD = NVL(_Link_.CMCODICE,space(5))
      this.w_DESORD = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLORDI = NVL(_Link_.CMFLORDI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUORD = space(5)
      endif
      this.w_DESORD = space(35)
      this.w_FLORDI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PPCAUORD) OR .w_FLORDI='+'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale ordini di produzione inesistente o incongruente")
        endif
        this.w_PPCAUORD = space(5)
        this.w_DESORD = space(35)
        this.w_FLORDI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUIMP
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PPCAUIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLIMPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PPCAUIMP))
          select CMCODICE,CMDESCRI,CMFLIMPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUIMP)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUIMP) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPPCAUIMP_2_4'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSCOIKPP.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLIMPE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLIMPE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PPCAUIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PPCAUIMP)
            select CMCODICE,CMDESCRI,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUIMP = NVL(_Link_.CMCODICE,space(5))
      this.w_DESIMP = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLIMPE = NVL(_Link_.CMFLIMPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUIMP = space(5)
      endif
      this.w_DESIMP = space(35)
      this.w_FLIMPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PPCAUIMP) OR .w_FLIMPE='+'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale impegni ODL inesistente o incongruente")
        endif
        this.w_PPCAUIMP = space(5)
        this.w_DESIMP = space(35)
        this.w_FLIMPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUTER
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCAUTER)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCAUTER))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUTER)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUTER) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCAUTER_2_6'),i_cWhere,'GSOR_ATD',"Causali documenti",'GSCO_KGL.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCAUTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCAUTER)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUTER = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAZ = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAZ = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CATDOZ = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUTER = space(5)
      endif
      this.w_DESCAZ = space(35)
      this.w_FLVEAZ = space(1)
      this.w_CATDOZ = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOZ='OR' AND .w_FLVEAZ='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale ordini a terzisti incongruente")
        endif
        this.w_PPCAUTER = space(5)
        this.w_DESCAZ = space(35)
        this.w_FLVEAZ = space(1)
        this.w_CATDOZ = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAURCL
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAURCL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCAURCL)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCAURCL))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAURCL)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAURCL) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCAURCL_2_8'),i_cWhere,'',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAURCL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCAURCL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCAURCL)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAURCL = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(40))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAURCL = space(5)
      endif
      this.w_DESDOC = space(40)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLVEAC='A' AND .w_CATDOC='DT'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCAURCL = space(5)
        this.w_DESDOC = space(40)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAURCL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUTRA
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCAUTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCAUTRA))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUTRA)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUTRA) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCAUTRA_2_10'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSCO_KGT.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCAUTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCAUTRA)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUTRA = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAT = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAT = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CATDOT = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUTRA = space(5)
      endif
      this.w_DESCAT = space(35)
      this.w_FLVEAT = space(1)
      this.w_CATDOT = space(2)
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLVEAT='A' AND .w_CATDOT='DT' AND .w_FLANAL<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale DDT di trasferimento incongruente")
        endif
        this.w_PPCAUTRA = space(5)
        this.w_DESCAT = space(35)
        this.w_FLVEAT = space(1)
        this.w_CATDOT = space(2)
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCODCAU
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCODCAU))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCODCAU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCODCAU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCODCAU_2_12'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSCO_KGI.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCODCAU)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODCAU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAB = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLINTB = NVL(_Link_.TDFLINTE,space(1))
      this.w_CATDOB = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODCAU = space(5)
      endif
      this.w_DESCAB = space(35)
      this.w_FLINTB = space(1)
      this.w_CATDOB = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PPGESWIP=' ' OR (.w_FLINTB='N' AND .w_CATDOB = 'DI')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale buono di prelievo incongruente")
        endif
        this.w_PPCODCAU = space(5)
        this.w_DESCAB = space(35)
        this.w_FLINTB = space(1)
        this.w_CATDOB = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUCAR
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCAUCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCAUCAR))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUCAR)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUCAR) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCAUCAR_2_14'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSCO_KGC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCAUCAR)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUCAR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAR = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLINTC = NVL(_Link_.TDFLINTE,space(1))
      this.w_CAUCAR = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUCAR = space(5)
      endif
      this.w_DESCAR = space(35)
      this.w_FLINTC = space(1)
      this.w_CAUCAR = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLINTC='N' AND NOT EMPTY(.w_CAUCAR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento carichi di magazzino inesistente o incongruente")
        endif
        this.w_PPCAUCAR = space(5)
        this.w_DESCAR = space(35)
        this.w_FLINTC = space(1)
        this.w_CAUCAR = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUSCA
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCAUSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCAUSCA))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUSCA)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUSCA) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCAUSCA_2_16'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSCO_KGS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCAUSCA)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUSCA = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESSCA = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLINTS = NVL(_Link_.TDFLINTE,space(1))
      this.w_CAUSCA = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUSCA = space(5)
      endif
      this.w_DESSCA = space(35)
      this.w_FLINTS = space(1)
      this.w_CAUSCA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLINTS='N' AND NOT EMPTY(.w_CAUSCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento scarichi di magazzino inesistente o incongruente")
        endif
        this.w_PPCAUSCA = space(5)
        this.w_DESSCA = space(35)
        this.w_FLINTS = space(1)
        this.w_CAUSCA = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUMOU
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUMOU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PPCAUMOU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PPCAUMOU))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUMOU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUMOU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPPCAUMOU_2_18'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSCO_KGC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUMOU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PPCAUMOU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PPCAUMOU)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUMOU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAM = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLINTM = NVL(_Link_.TDFLINTE,space(1))
      this.w_CAUMOU = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUMOU = space(5)
      endif
      this.w_DESCAM = space(35)
      this.w_FLINTM = space(1)
      this.w_CAUMOU = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLINTM='N' AND NOT EMPTY(.w_CAUMOU)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento carichi di magazzino inesistente o incongruente")
        endif
        this.w_PPCAUMOU = space(5)
        this.w_DESCAM = space(35)
        this.w_FLINTM = space(1)
        this.w_CAUMOU = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUMOU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPMAGPRO
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPMAGPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PPMAGPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PPMAGPRO))
          select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPMAGPRO)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPMAGPRO) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPPMAGPRO_2_19'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPMAGPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PPMAGPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PPMAGPRO)
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPMAGPRO = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG = NVL(_Link_.MGDISMAG,space(1))
      this.w_PROWIP = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPMAGPRO = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DISMAG = space(1)
      this.w_PROWIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DISMAG='S' AND .w_PROWIP<>'W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino produzione inesistente o non nettificabile o di tipo WIP")
        endif
        this.w_PPMAGPRO = space(5)
        this.w_DESMAG = space(30)
        this.w_DISMAG = space(1)
        this.w_PROWIP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPMAGPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPMAGSCA
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPMAGSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PPMAGSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG,MGDISMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PPMAGSCA))
          select MGCODMAG,MGDESMAG,MGTIPMAG,MGDISMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPMAGSCA)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPMAGSCA) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPPMAGSCA_2_21'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOSMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG,MGDISMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGTIPMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPMAGSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG,MGDISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PPMAGSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PPMAGSCA)
            select MGCODMAG,MGDESMAG,MGTIPMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPMAGSCA = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAS = NVL(_Link_.MGDESMAG,space(30))
      this.w_SCAWIP = NVL(_Link_.MGTIPMAG,space(1))
      this.w_SCADIS = NVL(_Link_.MGDISMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPMAGSCA = space(5)
      endif
      this.w_DESMAS = space(30)
      this.w_SCAWIP = space(1)
      this.w_SCADIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_SCAWIP<>'W' AND .w_SCADIS<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino scarti inesistente o di tipo WIP o nettificabile")
        endif
        this.w_PPMAGSCA = space(5)
        this.w_DESMAS = space(30)
        this.w_SCAWIP = space(1)
        this.w_SCADIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPMAGSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPMAGWIP
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPMAGWIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PPMAGWIP)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PPMAGWIP))
          select MGCODMAG,MGDESMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPMAGWIP)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPMAGWIP) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPPMAGWIP_2_23'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPMAGWIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PPMAGWIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PPMAGWIP)
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPMAGWIP = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAW = NVL(_Link_.MGDESMAG,space(30))
      this.w_TIPWIP = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPMAGWIP = space(5)
      endif
      this.w_DESMAW = space(30)
      this.w_TIPWIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PPGESWIP=' ' OR .w_TIPWIP='W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino WIP inesistente o non di tipo WIP")
        endif
        this.w_PPMAGWIP = space(5)
        this.w_DESMAW = space(30)
        this.w_TIPWIP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPMAGWIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCENCOS
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PPCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PPCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPPCENCOS_2_25'),i_cWhere,'',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PPCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PPCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PPCENCOS = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Costo obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCENCOS = space(15)
        this.w_DESCEN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUCCM
  func Link_2_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUCCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PPCAUCCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PPCAUCCM))
          select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUCCM)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_PPCAUCCM)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_PPCAUCCM)+"%");

            select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUCCM) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPPCAUCCM_2_26'),i_cWhere,'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUCCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PPCAUCCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PPCAUCCM)
            select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUCCM = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCCM = NVL(_Link_.CMDESCRI,space(35))
      this.w_CHKCC = NVL(_Link_.CMAGGVAL,space(1))
      this.w_CHKCSC = NVL(_Link_.CMFLCASC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUCCM = space(5)
      endif
      this.w_DESCCM = space(35)
      this.w_CHKCC = space(1)
      this.w_CHKCSC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CHKCC<>'S' and .w_CHKCSC $ '+-'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCAUCCM = space(5)
        this.w_DESCCM = space(35)
        this.w_CHKCC = space(1)
        this.w_CHKCSC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUCCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCAUSCM
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCAUSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PPCAUSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PPCAUSCM))
          select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCAUSCM)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_PPCAUSCM)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_PPCAUSCM)+"%");

            select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PPCAUSCM) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPPCAUSCM_2_27'),i_cWhere,'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCAUSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PPCAUSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PPCAUSCM)
            select CMCODICE,CMDESCRI,CMAGGVAL,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCAUSCM = NVL(_Link_.CMCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CMDESCRI,space(35))
      this.w_CHKCS = NVL(_Link_.CMAGGVAL,space(1))
      this.w_CHKCSS = NVL(_Link_.CMFLCASC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCAUSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_CHKCS = space(1)
      this.w_CHKCSS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CHKCS<>'S' and .w_CHKCSS $ '+-'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCAUSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_CHKCS = space(1)
        this.w_CHKCSS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCAUSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCALSTA
  func Link_2_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCALSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_PPCALSTA)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_PPCALSTA))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCALSTA)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCALSTA) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oPPCALSTA_2_58'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCALSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_PPCALSTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_PPCALSTA)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCALSTA = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PPCALSTA = space(5)
      endif
      this.w_DESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCALSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCCSODA
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCCSODA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PPCCSODA)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PPCCSODA))
          select CC_CONTO,CCDESPIA,CCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCCSODA)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCCSODA) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPPCCSODA_3_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCCSODA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PPCCSODA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PPCCSODA)
            select CC_CONTO,CCDESPIA,CCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCCSODA = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCCODA = NVL(_Link_.CCDESPIA,space(40))
      this.w_NUMLVODA = NVL(_Link_.CCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_PPCCSODA = space(15)
      endif
      this.w_DESCCODA = space(40)
      this.w_NUMLVODA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCCSODA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCODCOM
  func Link_4_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PPCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PPCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPPCODCOM_4_10'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PPCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PPCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODCOM = space(15)
      endif
      this.w_DESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPSRVDFT
  func Link_5_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPSRVDFT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PPSRVDFT)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PPSRVDFT))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPSRVDFT)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPSRVDFT) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPPSRVDFT_5_2'),i_cWhere,'',"Servizi",'GSCO_KPP.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPSRVDFT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PPSRVDFT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PPSRVDFT)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPSRVDFT = NVL(_Link_.ARCODART,space(20))
      this.w_PPSRVDSC = NVL(_Link_.ARDESART,space(40))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PPSRVDFT = space(20)
      endif
      this.w_PPSRVDSC = space(40)
      this.w_ARTIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTIPART='FM'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un servizio di tipo quantit� e valore")
        endif
        this.w_PPSRVDFT = space(20)
        this.w_PPSRVDSC = space(40)
        this.w_ARTIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPSRVDFT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCLAART
  func Link_5_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_lTable = "CCF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2], .t., this.CCF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCLAART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCI_ACC',True,'CCF_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_PPCLAART)+"%");
                   +" and CFTIPCLA="+cp_ToStrODBC(this.w_PPTIPCLA);

          i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCLA,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCLA',this.w_PPTIPCLA;
                     ,'CFCODICE',trim(this.w_PPCLAART))
          select CFTIPCLA,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCLA,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCLAART)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCLAART) and !this.bDontReportError
            deferred_cp_zoom('CCF_MAST','*','CFTIPCLA,CFCODICE',cp_AbsName(oSource.parent,'oPPCLAART_5_9'),i_cWhere,'GSCI_ACC',"Classi codice di fase",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PPTIPCLA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCLA,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCLA="+cp_ToStrODBC(this.w_PPTIPCLA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCLA',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCLA,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCLAART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_PPCLAART);
                   +" and CFTIPCLA="+cp_ToStrODBC(this.w_PPTIPCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCLA',this.w_PPTIPCLA;
                       ,'CFCODICE',this.w_PPCLAART)
            select CFTIPCLA,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCLAART = NVL(_Link_.CFCODICE,space(5))
      this.w_PPDESCART = NVL(_Link_.CFDESCRI,space(30))
      this.w_CFTIPCLA = NVL(_Link_.CFTIPCLA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PPCLAART = space(5)
      endif
      this.w_PPDESCART = space(30)
      this.w_CFTIPCLA = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=g_PRFA='S' and .w_CFTIPCLA='A' or g_PRFA<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCLAART = space(5)
        this.w_PPDESCART = space(30)
        this.w_CFTIPCLA = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCLA,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CCF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCLAART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCLAFAS
  func Link_5_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_lTable = "CCF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2], .t., this.CCF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCLAFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCI_ACC',True,'CCF_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_PPCLAFAS)+"%");
                   +" and CFTIPCLA="+cp_ToStrODBC(this.w_PPTIPCLC);

          i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCLA,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCLA',this.w_PPTIPCLC;
                     ,'CFCODICE',trim(this.w_PPCLAFAS))
          select CFTIPCLA,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCLA,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCLAFAS)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCLAFAS) and !this.bDontReportError
            deferred_cp_zoom('CCF_MAST','*','CFTIPCLA,CFCODICE',cp_AbsName(oSource.parent,'oPPCLAFAS_5_21'),i_cWhere,'GSCI_ACC',"Classi codice di fase",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PPTIPCLC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCLA,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCLA="+cp_ToStrODBC(this.w_PPTIPCLC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCLA',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCLA,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCLAFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCLA,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_PPCLAFAS);
                   +" and CFTIPCLA="+cp_ToStrODBC(this.w_PPTIPCLC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCLA',this.w_PPTIPCLC;
                       ,'CFCODICE',this.w_PPCLAFAS)
            select CFTIPCLA,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCLAFAS = NVL(_Link_.CFCODICE,space(5))
      this.w_PPDESCFAS = NVL(_Link_.CFDESCRI,space(30))
      this.w_CFTIPCLC = NVL(_Link_.CFTIPCLA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PPCLAFAS = space(5)
      endif
      this.w_PPDESCFAS = space(30)
      this.w_CFTIPCLC = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=g_PRFA='S' and .w_CFTIPCLC='C' or g_PRFA<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCLAFAS = space(5)
        this.w_PPDESCFAS = space(30)
        this.w_CFTIPCLC = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCLA,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CCF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCLAFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPUNIPRO
  func Link_5_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPUNIPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_PPUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_UNIPRO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_UNIPRO;
                     ,'RLCODICE',trim(this.w_PPUNIPRO))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPUNIPRO)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_PPUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_UNIPRO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_PPUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_UNIPRO);

            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PPUNIPRO) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oPPUNIPRO_5_29'),i_cWhere,'',"Servizi",'GSCI_ZLR.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UNIPRO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_UNIPRO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPUNIPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_PPUNIPRO);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_UNIPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_UNIPRO;
                       ,'RLCODICE',this.w_PPUNIPRO)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPUNIPRO = NVL(_Link_.RLCODICE,space(20))
      this.w_DEUNIPRO = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PPUNIPRO = space(20)
      endif
      this.w_DEUNIPRO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPUNIPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPPNUMGIO_1_4.value==this.w_PPNUMGIO)
      this.oPgFrm.Page1.oPag.oPPNUMGIO_1_4.value=this.w_PPNUMGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMSET_1_5.value==this.w_PPNUMSET)
      this.oPgFrm.Page1.oPag.oPPNUMSET_1_5.value=this.w_PPNUMSET
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMMES_1_6.value==this.w_PPNUMMES)
      this.oPgFrm.Page1.oPag.oPPNUMMES_1_6.value=this.w_PPNUMMES
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMTRI_1_7.value==this.w_PPNUMTRI)
      this.oPgFrm.Page1.oPag.oPPNUMTRI_1_7.value=this.w_PPNUMTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCOLMPS_1_8.value==this.w_PPCOLMPS)
      this.oPgFrm.Page1.oPag.oPPCOLMPS_1_8.value=this.w_PPCOLMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oPPFONMPS_1_9.value==this.w_PPFONMPS)
      this.oPgFrm.Page1.oPag.oPPFONMPS_1_9.value=this.w_PPFONMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oPPGIOSCA_1_10.value==this.w_PPGIOSCA)
      this.oPgFrm.Page1.oPag.oPPGIOSCA_1_10.value=this.w_PPGIOSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oPP___DTF_1_11.value==this.w_PP___DTF)
      this.oPgFrm.Page1.oPag.oPP___DTF_1_11.value=this.w_PP___DTF
    endif
    if not(this.oPgFrm.Page2.oPag.oPPGESWIP_2_1.RadioValue()==this.w_PPGESWIP)
      this.oPgFrm.Page2.oPag.oPPGESWIP_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUORD_2_2.value==this.w_PPCAUORD)
      this.oPgFrm.Page2.oPag.oPPCAUORD_2_2.value=this.w_PPCAUORD
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORD_2_3.value==this.w_DESORD)
      this.oPgFrm.Page2.oPag.oDESORD_2_3.value=this.w_DESORD
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUIMP_2_4.value==this.w_PPCAUIMP)
      this.oPgFrm.Page2.oPag.oPPCAUIMP_2_4.value=this.w_PPCAUIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESIMP_2_5.value==this.w_DESIMP)
      this.oPgFrm.Page2.oPag.oDESIMP_2_5.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUTER_2_6.value==this.w_PPCAUTER)
      this.oPgFrm.Page2.oPag.oPPCAUTER_2_6.value=this.w_PPCAUTER
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAZ_2_7.value==this.w_DESCAZ)
      this.oPgFrm.Page2.oPag.oDESCAZ_2_7.value=this.w_DESCAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAURCL_2_8.value==this.w_PPCAURCL)
      this.oPgFrm.Page2.oPag.oPPCAURCL_2_8.value=this.w_PPCAURCL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOC_2_9.value==this.w_DESDOC)
      this.oPgFrm.Page2.oPag.oDESDOC_2_9.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUTRA_2_10.value==this.w_PPCAUTRA)
      this.oPgFrm.Page2.oPag.oPPCAUTRA_2_10.value=this.w_PPCAUTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT_2_11.value==this.w_DESCAT)
      this.oPgFrm.Page2.oPag.oDESCAT_2_11.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCODCAU_2_12.value==this.w_PPCODCAU)
      this.oPgFrm.Page2.oPag.oPPCODCAU_2_12.value=this.w_PPCODCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAB_2_13.value==this.w_DESCAB)
      this.oPgFrm.Page2.oPag.oDESCAB_2_13.value=this.w_DESCAB
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUCAR_2_14.value==this.w_PPCAUCAR)
      this.oPgFrm.Page2.oPag.oPPCAUCAR_2_14.value=this.w_PPCAUCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAR_2_15.value==this.w_DESCAR)
      this.oPgFrm.Page2.oPag.oDESCAR_2_15.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUSCA_2_16.value==this.w_PPCAUSCA)
      this.oPgFrm.Page2.oPag.oPPCAUSCA_2_16.value=this.w_PPCAUSCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSCA_2_17.value==this.w_DESSCA)
      this.oPgFrm.Page2.oPag.oDESSCA_2_17.value=this.w_DESSCA
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUMOU_2_18.value==this.w_PPCAUMOU)
      this.oPgFrm.Page2.oPag.oPPCAUMOU_2_18.value=this.w_PPCAUMOU
    endif
    if not(this.oPgFrm.Page2.oPag.oPPMAGPRO_2_19.value==this.w_PPMAGPRO)
      this.oPgFrm.Page2.oPag.oPPMAGPRO_2_19.value=this.w_PPMAGPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_20.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_20.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oPPMAGSCA_2_21.value==this.w_PPMAGSCA)
      this.oPgFrm.Page2.oPag.oPPMAGSCA_2_21.value=this.w_PPMAGSCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAS_2_22.value==this.w_DESMAS)
      this.oPgFrm.Page2.oPag.oDESMAS_2_22.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page2.oPag.oPPMAGWIP_2_23.value==this.w_PPMAGWIP)
      this.oPgFrm.Page2.oPag.oPPMAGWIP_2_23.value=this.w_PPMAGWIP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAW_2_24.value==this.w_DESMAW)
      this.oPgFrm.Page2.oPag.oDESMAW_2_24.value=this.w_DESMAW
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCENCOS_2_25.value==this.w_PPCENCOS)
      this.oPgFrm.Page2.oPag.oPPCENCOS_2_25.value=this.w_PPCENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUCCM_2_26.value==this.w_PPCAUCCM)
      this.oPgFrm.Page2.oPag.oPPCAUCCM_2_26.value=this.w_PPCAUCCM
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCAUSCM_2_27.value==this.w_PPCAUSCM)
      this.oPgFrm.Page2.oPag.oPPCAUSCM_2_27.value=this.w_PPCAUSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_28.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_28.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oPPCALSTA_2_58.value==this.w_PPCALSTA)
      this.oPgFrm.Page2.oPag.oPPCALSTA_2_58.value=this.w_PPCALSTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAL_2_59.value==this.w_DESCAL)
      this.oPgFrm.Page2.oPag.oDESCAL_2_59.value=this.w_DESCAL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCCM_2_63.value==this.w_DESCCM)
      this.oPgFrm.Page2.oPag.oDESCCM_2_63.value=this.w_DESCCM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSCM_2_64.value==this.w_DESSCM)
      this.oPgFrm.Page2.oPag.oDESSCM_2_64.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oPPPSCLAV_2_72.RadioValue()==this.w_PPPSCLAV)
      this.oPgFrm.Page2.oPag.oPPPSCLAV_2_72.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAM_2_74.value==this.w_DESCAM)
      this.oPgFrm.Page2.oPag.oDESCAM_2_74.value=this.w_DESCAM
    endif
    if not(this.oPgFrm.Page3.oPag.oCRIFOR_3_1.RadioValue()==this.w_CRIFOR)
      this.oPgFrm.Page3.oPag.oCRIFOR_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPPCCSODA_3_2.value==this.w_PPCCSODA)
      this.oPgFrm.Page3.oPag.oPPCCSODA_3_2.value=this.w_PPCCSODA
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCCODA_3_6.value==this.w_DESCCODA)
      this.oPgFrm.Page3.oPag.oDESCCODA_3_6.value=this.w_DESCCODA
    endif
    if not(this.oPgFrm.Page3.oPag.oPPBORODA_3_9.RadioValue()==this.w_PPBORODA)
      this.oPgFrm.Page3.oPag.oPPBORODA_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPP__MODA_3_12.RadioValue()==this.w_PP__MODA)
      this.oPgFrm.Page3.oPag.oPP__MODA_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPBLOMRP_4_1.RadioValue()==this.w_PPBLOMRP)
      this.oPgFrm.Page4.oPag.oPPBLOMRP_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPMRPLOG_4_2.RadioValue()==this.w_PPMRPLOG)
      this.oPgFrm.Page4.oPag.oPPMRPLOG_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPSALCOM_4_3.RadioValue()==this.w_PPSALCOM)
      this.oPgFrm.Page4.oPag.oPPSALCOM_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPMRPDIV_4_5.RadioValue()==this.w_PPMRPDIV)
      this.oPgFrm.Page4.oPag.oPPMRPDIV_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPORDICM_4_6.RadioValue()==this.w_PPORDICM)
      this.oPgFrm.Page4.oPag.oPPORDICM_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPSCAMRP_4_9.RadioValue()==this.w_PPSCAMRP)
      this.oPgFrm.Page4.oPag.oPPSCAMRP_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPCODCOM_4_10.value==this.w_PPCODCOM)
      this.oPgFrm.Page4.oPag.oPPCODCOM_4_10.value=this.w_PPCODCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCOM_4_12.value==this.w_DESCOM)
      this.oPgFrm.Page4.oPag.oDESCOM_4_12.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oPPDICCOM_4_13.RadioValue()==this.w_PPDICCOM)
      this.oPgFrm.Page4.oPag.oPPDICCOM_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPATHMRP_4_15.value==this.w_PPATHMRP)
      this.oPgFrm.Page4.oPag.oPPATHMRP_4_15.value=this.w_PPATHMRP
    endif
    if not(this.oPgFrm.Page4.oPag.oPPIMPLOT_4_20.RadioValue()==this.w_PPIMPLOT)
      this.oPgFrm.Page4.oPag.oPPIMPLOT_4_20.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPPRJMOD_4_22.value==this.w_PPPRJMOD)
      this.oPgFrm.Page4.oPag.oPPPRJMOD_4_22.value=this.w_PPPRJMOD
    endif
    if not(this.oPgFrm.Page4.oPag.oPPPERPIA_4_25.RadioValue()==this.w_PPPERPIA)
      this.oPgFrm.Page4.oPag.oPPPERPIA_4_25.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPCRIELA_4_26.RadioValue()==this.w_PPCRIELA)
      this.oPgFrm.Page4.oPag.oPPCRIELA_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPPPIAPUN_4_28.RadioValue()==this.w_PPPIAPUN)
      this.oPgFrm.Page4.oPag.oPPPIAPUN_4_28.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPSRVDFT_5_2.value==this.w_PPSRVDFT)
      this.oPgFrm.Page5.oPag.oPPSRVDFT_5_2.value=this.w_PPSRVDFT
    endif
    if not(this.oPgFrm.Page5.oPag.oPPMATINP_5_4.RadioValue()==this.w_PPMATINP)
      this.oPgFrm.Page5.oPag.oPPMATINP_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPMATOUP_5_5.RadioValue()==this.w_PPMATOUP)
      this.oPgFrm.Page5.oPag.oPPMATOUP_5_5.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPMGARFS_5_6.RadioValue()==this.w_PPMGARFS)
      this.oPgFrm.Page5.oPag.oPPMGARFS_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPPREFAS_5_7.value==this.w_PPPREFAS)
      this.oPgFrm.Page5.oPag.oPPPREFAS_5_7.value=this.w_PPPREFAS
    endif
    if not(this.oPgFrm.Page5.oPag.oPPFASSEP_5_8.value==this.w_PPFASSEP)
      this.oPgFrm.Page5.oPag.oPPFASSEP_5_8.value=this.w_PPFASSEP
    endif
    if not(this.oPgFrm.Page5.oPag.oPPCLAART_5_9.value==this.w_PPCLAART)
      this.oPgFrm.Page5.oPag.oPPCLAART_5_9.value=this.w_PPCLAART
    endif
    if not(this.oPgFrm.Page5.oPag.oPPDCODFAS_5_10.RadioValue()==this.w_PPDCODFAS)
      this.oPgFrm.Page5.oPag.oPPDCODFAS_5_10.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPDSUPFA_5_11.RadioValue()==this.w_PPDSUPFA)
      this.oPgFrm.Page5.oPag.oPPDSUPFA_5_11.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPSRVDSC_5_15.value==this.w_PPSRVDSC)
      this.oPgFrm.Page5.oPag.oPPSRVDSC_5_15.value=this.w_PPSRVDSC
    endif
    if not(this.oPgFrm.Page5.oPag.oPPCLAFAS_5_21.value==this.w_PPCLAFAS)
      this.oPgFrm.Page5.oPag.oPPCLAFAS_5_21.value=this.w_PPCLAFAS
    endif
    if not(this.oPgFrm.Page5.oPag.oPPDESCFAS_5_22.value==this.w_PPDESCFAS)
      this.oPgFrm.Page5.oPag.oPPDESCFAS_5_22.value=this.w_PPDESCFAS
    endif
    if not(this.oPgFrm.Page5.oPag.oPP_CICLI_5_24.RadioValue()==this.w_PP_CICLI)
      this.oPgFrm.Page5.oPag.oPP_CICLI_5_24.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPUNIPRO_5_29.value==this.w_PPUNIPRO)
      this.oPgFrm.Page5.oPag.oPPUNIPRO_5_29.value=this.w_PPUNIPRO
    endif
    if not(this.oPgFrm.Page5.oPag.oDEUNIPRO_5_31.value==this.w_DEUNIPRO)
      this.oPgFrm.Page5.oPag.oDEUNIPRO_5_31.value=this.w_DEUNIPRO
    endif
    if not(this.oPgFrm.Page5.oPag.oPPFECLAV_5_32.RadioValue()==this.w_PPFECLAV)
      this.oPgFrm.Page5.oPag.oPPFECLAV_5_32.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPPDESCART_5_39.value==this.w_PPDESCART)
      this.oPgFrm.Page5.oPag.oPPDESCART_5_39.value=this.w_PPDESCART
    endif
    if not(this.oPgFrm.Page5.oPag.oPPNOTFAS_5_47.RadioValue()==this.w_PPNOTFAS)
      this.oPgFrm.Page5.oPag.oPPNOTFAS_5_47.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PPNUMGIO)) or not(.w_PPNUMGIO>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMGIO_1_4.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMGIO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   ((empty(.w_PPNUMSET)) or not(.w_PPNUMSET>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMSET_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMSET)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   ((empty(.w_PPNUMMES)) or not(.w_PPNUMMES>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMMES_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMMES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   ((empty(.w_PPNUMTRI)) or not(.w_PPNUMTRI>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMTRI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMTRI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   (empty(.w_PPCOLMPS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPCOLMPS_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PPCOLMPS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPFONMPS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPFONMPS_1_9.SetFocus()
            i_bnoObbl = !empty(.w_PPFONMPS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PPCAUORD)) or not(EMPTY(.w_PPCAUORD) OR .w_FLORDI='+'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUORD_2_2.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUORD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale ordini di produzione inesistente o incongruente")
          case   ((empty(.w_PPCAUIMP)) or not(EMPTY(.w_PPCAUIMP) OR .w_FLIMPE='+'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUIMP_2_4.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUIMP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale impegni ODL inesistente o incongruente")
          case   not(.w_CATDOZ='OR' AND .w_FLVEAZ='A')  and not(empty(.w_PPCAUTER))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUTER_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale ordini a terzisti incongruente")
          case   not(.w_FLVEAC='A' AND .w_CATDOC='DT')  and not(empty(.w_PPCAURCL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAURCL_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLVEAT='A' AND .w_CATDOT='DT' AND .w_FLANAL<>'S')  and not(empty(.w_PPCAUTRA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUTRA_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale DDT di trasferimento incongruente")
          case   not(.w_PPGESWIP=' ' OR (.w_FLINTB='N' AND .w_CATDOB = 'DI'))  and not(.w_PPGESWIP=' ')  and (.w_PPGESWIP<>' ')  and not(empty(.w_PPCODCAU))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCODCAU_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale buono di prelievo incongruente")
          case   ((empty(.w_PPCAUCAR)) or not(.w_FLINTC='N' AND NOT EMPTY(.w_CAUCAR)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUCAR_2_14.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUCAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento carichi di magazzino inesistente o incongruente")
          case   ((empty(.w_PPCAUSCA)) or not(.w_FLINTS='N' AND NOT EMPTY(.w_CAUSCA)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUSCA_2_16.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUSCA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento scarichi di magazzino inesistente o incongruente")
          case   ((empty(.w_PPCAUMOU)) or not(.w_FLINTM='N' AND NOT EMPTY(.w_CAUMOU)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUMOU_2_18.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUMOU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento carichi di magazzino inesistente o incongruente")
          case   ((empty(.w_PPMAGPRO)) or not(.w_DISMAG='S' AND .w_PROWIP<>'W'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPMAGPRO_2_19.SetFocus()
            i_bnoObbl = !empty(.w_PPMAGPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino produzione inesistente o non nettificabile o di tipo WIP")
          case   ((empty(.w_PPMAGSCA)) or not(.w_SCAWIP<>'W' AND .w_SCADIS<>'S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPMAGSCA_2_21.SetFocus()
            i_bnoObbl = !empty(.w_PPMAGSCA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino scarti inesistente o di tipo WIP o nettificabile")
          case   not(.w_PPGESWIP=' ' OR .w_TIPWIP='W')  and not(.w_PPGESWIP=' ')  and (.w_PPGESWIP<>' ')  and not(empty(.w_PPMAGWIP))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPMAGWIP_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino WIP inesistente o non di tipo WIP")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Costo obsoleto!",.F.))  and not(g_PERCCR<>'S')  and (g_PERCCR='S')  and not(empty(.w_PPCENCOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCENCOS_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PPCAUCCM)) or not(.w_CHKCC<>'S' and .w_CHKCSC $ '+-'))  and not(.w_PPSALCOM<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUCCM_2_26.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUCCM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PPCAUSCM)) or not(.w_CHKCS<>'S' and .w_CHKCSS $ '+-'))  and not(.w_PPSALCOM<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCAUSCM_2_27.SetFocus()
            i_bnoObbl = !empty(.w_PPCAUSCM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPCALSTA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPCALSTA_2_58.SetFocus()
            i_bnoObbl = !empty(.w_PPCALSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPCCSODA))  and (g_PERCCR='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPPCCSODA_3_2.SetFocus()
            i_bnoObbl = !empty(.w_PPCCSODA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(!EMPTY(alltrim(.w_PPATHMRP)), DIRECTORY(.w_PPATHMRP), true))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPPATHMRP_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARTIPART='FM')  and not(empty(.w_PPSRVDFT))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPPSRVDFT_5_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un servizio di tipo quantit� e valore")
          case   (empty(.w_PPPREFAS) and (g_PRFA='S'))  and (.w_PPMGARFS='P')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPPPREFAS_5_7.SetFocus()
            i_bnoObbl = !empty(.w_PPPREFAS) or !(g_PRFA='S')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPFASSEP) and (g_PRFA='S'))  and (.w_PPMGARFS='P')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPPFASSEP_5_8.SetFocus()
            i_bnoObbl = !empty(.w_PPFASSEP) or !(g_PRFA='S')
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PPCLAART) and (g_PRFA='S')) or not(g_PRFA='S' and .w_CFTIPCLA='A' or g_PRFA<>'S'))  and (.w_PPMGARFS='C')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPPCLAART_5_9.SetFocus()
            i_bnoObbl = !empty(.w_PPCLAART) or !(g_PRFA='S')
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PPCLAFAS) and (g_PRFA='S')) or not(g_PRFA='S' and .w_CFTIPCLC='C' or g_PRFA<>'S'))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPPCLAFAS_5_21.SetFocus()
            i_bnoObbl = !empty(.w_PPCLAFAS) or !(g_PRFA='S')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPUNIPRO) and (g_PRFA='S' and g_CICLILAV='S'))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPPUNIPRO_5_29.SetFocus()
            i_bnoObbl = !empty(.w_PPUNIPRO) or !(g_PRFA='S' and g_CICLILAV='S')
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsco_kpp
      if i_bRes=.t.
        this.w_ERRORE=.F.
          this.NotifyEvent("Controlli Finali")
          i_bRes=not this.w_ERRORE
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PPGESWIP = this.w_PPGESWIP
    this.o_PPSALCOM = this.w_PPSALCOM
    this.o_PPATHMRP = this.w_PPATHMRP
    this.o_PPPREFAS = this.w_PPPREFAS
    return

enddefine

* --- Define pages as container
define class tgsco_kppPag1 as StdContainer
  Width  = 552
  height = 493
  stdWidth  = 552
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPNUMGIO_1_4 as StdField with uid="LNOCRFNDCW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PPNUMGIO", cQueryName = "PPNUMGIO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero minimo di periodi giornalieri",;
    HelpContextID = 54562885,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=227, Top=35, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMGIO_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMGIO>0)
    endwith
    return bRes
  endfunc

  add object oPPNUMSET_1_5 as StdField with uid="NWIAEBOZDN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PPNUMSET", cQueryName = "PPNUMSET",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero minimo di periodi settimanali",;
    HelpContextID = 255889482,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=227, Top=62, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMSET_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMSET>0)
    endwith
    return bRes
  endfunc

  add object oPPNUMMES_1_6 as StdField with uid="XVWFKYKYWF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PPNUMMES", cQueryName = "PPNUMMES",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero minimo di periodi mensili",;
    HelpContextID = 155226185,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=227, Top=89, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMMES_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMMES>0)
    endwith
    return bRes
  endfunc

  add object oPPNUMTRI_1_7 as StdField with uid="EMJRAPSTKG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PPNUMTRI", cQueryName = "PPNUMTRI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero di periodi trimestrali",;
    HelpContextID = 264204225,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=227, Top=116, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMTRI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMTRI>0)
    endwith
    return bRes
  endfunc

  add object oPPCOLMPS_1_8 as StdField with uid="OZYMHICSEC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PPCOLMPS", cQueryName = "PPCOLMPS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Larghezza in pixel delle colonne nelle griglie di visualizzazione ODL/OCL",;
    HelpContextID = 114696119,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=385, Top=35, cSayPict='"@Z 999"', cGetPict='"999"'

  add object oPPFONMPS_1_9 as StdField with uid="RIUNXVRMXE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PPFONMPS", cQueryName = "PPFONMPS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione del font dei valori nelle griglie di visualizzazione ODL/OCL",;
    HelpContextID = 112586679,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=517, Top=35, cSayPict='"@Z 99"', cGetPict='"99"'

  add object oPPGIOSCA_1_10 as StdField with uid="KAOZHGMSLZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PPGIOSCA", cQueryName = "PPGIOSCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero giorni di scaduto valutati dalla procedura nell'analisi di magazzino",;
    HelpContextID = 11263945,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=517, Top=89

  func oPPGIOSCA_1_10.mHide()
    with this.Parent.oContained
      return (g_MMPS<>'S')
    endwith
  endfunc

  add object oPP___DTF_1_11 as StdField with uid="UMQULEECOL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PP___DTF", cQueryName = "PP___DTF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Demand time fence (non considera le previsioni nei prossimi N giorni)",;
    HelpContextID = 23830588,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=517, Top=116

  func oPP___DTF_1_11.mHide()
    with this.Parent.oContained
      return (g_MMPS<>'S')
    endwith
  endfunc


  add object oBtn_1_23 as StdButton with uid="XAOFRYAIID",left=441, top=446, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 118934246;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="KYQESPJULE",left=493, top=446, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 126222918;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="WPZBJMMAWS",left=227, top=177, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per MPS suggerito";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLSUG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MMPS='S')
      endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="SJZCYREAQT",left=271, top=177, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per MPS suggerito";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOSUG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MMPS='S')
      endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="SCHTWOHOAN",left=227, top=201, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per MPS confermato";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLCON")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MMPS='S')
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="NLBGZTMPUS",left=271, top=201, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per MPS confermato";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOCON")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MMPS='S')
      endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="ICXOSFFIRD",left=227, top=225, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per MPS da pianificare";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLDPI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MMPS='S')
      endwith
    endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="CZNVCUUJOG",left=271, top=225, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per MPS da pianificare";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFODPI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MMPS='S')
      endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="IBQYHSMVQN",left=227, top=249, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per ODL/OCL suggeriti";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_41.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLSUM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="UVFPOGMJRN",left=271, top=249, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per ODL/OCL suggeriti";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOSUM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object SUGGE as cp_calclbl with uid="NZSYKJYMIJ",left=433, top=251, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_44 as StdButton with uid="DHMXWKGUUP",left=227, top=273, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per ODL/OCL pianificati o lanciati";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLPIA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_45 as StdButton with uid="YEBBQWEXAO",left=271, top=273, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per ODL/OCL pianificati o lanciati";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOPIA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_46 as cp_calclbl with uid="XKUSUEFLCD",left=433, top=276, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_47 as StdButton with uid="FEXRJABPEL",left=227, top=297, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per ODL lanciato";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLLAN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_48 as StdButton with uid="XUDSBAJZIU",left=271, top=297, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per ODL lanciato";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOLAN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_49 as cp_calclbl with uid="EKIZPVRFRJ",left=433, top=301, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_50 as StdButton with uid="GPFXYLMZHW",left=227, top=321, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per ODL/OCL a composizione mista";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_50.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLMIS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_51 as StdButton with uid="ENYHKBKEKW",left=271, top=321, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per ODL/OCL a composizione mista";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_51.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOMIS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_52 as cp_calclbl with uid="VPHSIVEYAL",left=433, top=326, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_53 as StdButton with uid="VHRYPIRBZB",left=227, top=386, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo dei periodi festivi";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_53.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"COLFES")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_54 as cp_calclbl with uid="KNLRXANLDP",left=433, top=386, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oBtn_1_55 as StdButton with uid="VKEYZOZPYC",left=271, top=411, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per il sovraccarico della capacit�";
    , HelpContextID = 119106518;
  , bGlobalFont=.t.

    proc oBtn_1_55.Click()
      with this.Parent.oContained
        GSCO_BPP(this.Parent.oContained,"SFOMCO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_56 as cp_calclbl with uid="VBOCAAMEIP",left=433, top=411, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_78 as cp_calclbl with uid="PNSUPLBHKZ",left=433, top=176, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_81 as cp_calclbl with uid="QNQCKHHCOC",left=433, top=201, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_84 as cp_calclbl with uid="GFHHHMTXSC",left=433, top=226, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_96 as cp_runprogram with uid="WBJQCZKMBT",left=570, top=457, width=165,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BPP('CONTROLLI')",;
    cEvent = "Controlli Finali",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_97 as cp_runprogram with uid="WJCSVMRUVM",left=570, top=478, width=165,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BPP('SALVA')",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 239967770


  add object oObj_1_98 as cp_runprogram with uid="TXHSZXCKUT",left=570, top=436, width=165,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BPP('LOAD')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 239967770

  add object oStr_1_12 as StdString with uid="CWLWOJCEIV",Visible=.t., Left=18, Top=35,;
    Alignment=1, Width=202, Height=15,;
    Caption="Numero minimo periodi giornalieri:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VKIEIJTAWI",Visible=.t., Left=7, Top=62,;
    Alignment=1, Width=213, Height=15,;
    Caption="Numero minimo periodi settimanali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RHROCCKOIY",Visible=.t., Left=18, Top=89,;
    Alignment=1, Width=202, Height=15,;
    Caption="Numero minimo periodi mensili:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DZQIELETCS",Visible=.t., Left=18, Top=116,;
    Alignment=1, Width=202, Height=15,;
    Caption="Numero periodi trimestrali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WZANXJWXGR",Visible=.t., Left=8, Top=6,;
    Alignment=0, Width=252, Height=18,;
    Caption="Definizione time buckets piano di produzione"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XVGDWKRNYU",Visible=.t., Left=278, Top=32,;
    Alignment=1, Width=103, Height=18,;
    Caption="Dimens. colonna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WZMIRIBYUI",Visible=.t., Left=426, Top=35,;
    Alignment=1, Width=87, Height=18,;
    Caption="Dimens. font:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="APJYDEPHGL",Visible=.t., Left=284, Top=6,;
    Alignment=0, Width=234, Height=18,;
    Caption="Dimensioni griglia"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="LGJXHJHMRF",Visible=.t., Left=362, Top=252,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="NZXEUNMETO",Visible=.t., Left=217, Top=151,;
    Alignment=2, Width=38, Height=15,;
    Caption="Testo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="KKZZCKYXCJ",Visible=.t., Left=263, Top=151,;
    Alignment=2, Width=71, Height=15,;
    Caption="Sfondo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="WDVWWVDEAN",Visible=.t., Left=18, Top=252,;
    Alignment=1, Width=202, Height=15,;
    Caption="ODL/OCL suggerito MRP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="TGFWRBXNYS",Visible=.t., Left=362, Top=277,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="KGSADKHGAN",Visible=.t., Left=18, Top=277,;
    Alignment=1, Width=202, Height=15,;
    Caption="ODP/ODL/OCL pianificato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="OWYLODBZFI",Visible=.t., Left=362, Top=327,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="GNYFGDAKMG",Visible=.t., Left=18, Top=327,;
    Alignment=1, Width=202, Height=15,;
    Caption="Composizione mista:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="EGDZWEMMTK",Visible=.t., Left=8, Top=151,;
    Alignment=0, Width=205, Height=18,;
    Caption="Definizione colori griglia"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="SKVJZWGXEO",Visible=.t., Left=362, Top=411,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="YWMGQNOSTP",Visible=.t., Left=18, Top=411,;
    Alignment=1, Width=202, Height=15,;
    Caption="Sfondo per ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="OCMDCRAHQP",Visible=.t., Left=4, Top=358,;
    Alignment=0, Width=218, Height=15,;
    Caption="Parametri verifica temporale"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="FVCYQZYFYK",Visible=.t., Left=362, Top=386,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="VLMPZFUSLG",Visible=.t., Left=18, Top=386,;
    Alignment=1, Width=202, Height=18,;
    Caption="Testo periodi festivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="CIYNSAVJRR",Visible=.t., Left=30, Top=302,;
    Alignment=1, Width=190, Height=15,;
    Caption="ODP/ODL/OCL lanciato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="ZCPIQFYLZM",Visible=.t., Left=362, Top=302,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="PPXNPSRHJU",Visible=.t., Left=362, Top=177,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="SGZULSZJZH",Visible=.t., Left=69, Top=177,;
    Alignment=1, Width=151, Height=15,;
    Caption="ODP suggerito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="ARWQEANMGT",Visible=.t., Left=362, Top=202,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="AYZERBYRNA",Visible=.t., Left=69, Top=202,;
    Alignment=1, Width=151, Height=15,;
    Caption="ODP confermato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="DXMRWPHCCU",Visible=.t., Left=362, Top=227,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="FALFCQLPOR",Visible=.t., Left=68, Top=227,;
    Alignment=1, Width=152, Height=15,;
    Caption="ODP da pianificare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="UFAJSZEDQG",Visible=.t., Left=308, Top=89,;
    Alignment=1, Width=205, Height=15,;
    Caption="Giorni scaduto:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (g_MMPS<>'S')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="VXOPVPCUQD",Visible=.t., Left=308, Top=116,;
    Alignment=1, Width=205, Height=15,;
    Caption="Giorni DTF (demand time fence):"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (g_MMPS<>'S')
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="YWJZYRJZWB",Visible=.t., Left=284, Top=62,;
    Alignment=0, Width=186, Height=15,;
    Caption="Parametri generazione MPS"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (g_MMPS<>'S')
    endwith
  endfunc

  add object oBox_1_17 as StdBox with uid="OMCTQHDULU",left=7, top=26, width=255,height=2

  add object oBox_1_21 as StdBox with uid="OTZTVLEAGP",left=284, top=26, width=264,height=2

  add object oBox_1_66 as StdBox with uid="BTSHSJEOXI",left=3, top=171, width=541,height=2

  add object oBox_1_70 as StdBox with uid="ZGRXAAQAJO",left=3, top=377, width=541,height=2

  add object oBox_1_95 as StdBox with uid="MFLECKONGE",left=284, top=83, width=264,height=2
enddefine
define class tgsco_kppPag2 as StdContainer
  Width  = 552
  height = 493
  stdWidth  = 552
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPGESWIP_2_1 as StdCheck with uid="HEOZCCPRHF",rtseq=31,rtrep=.f.,left=53, top=5, caption="Gestione magazzino WIP",;
    ToolTipText = "Se attivo: la progedura gestisce il magazzino WIP",;
    HelpContextID = 59777094,;
    cFormVar="w_PPGESWIP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPPGESWIP_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPGESWIP_2_1.GetRadio()
    this.Parent.oContained.w_PPGESWIP = this.RadioValue()
    return .t.
  endfunc

  func oPPGESWIP_2_1.SetRadio()
    this.Parent.oContained.w_PPGESWIP=trim(this.Parent.oContained.w_PPGESWIP)
    this.value = ;
      iif(this.Parent.oContained.w_PPGESWIP=='S',1,;
      0)
  endfunc

  add object oPPCAUORD_2_2 as StdField with uid="ZWXCDDIIQF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PPCAUORD", cQueryName = "PPCAUORD",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale ordini di produzione inesistente o incongruente",;
    ToolTipText = "Codice causale associata agli ordini di produzione",;
    HelpContextID = 72622022,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PPCAUORD"

  func oPPCAUORD_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUORD_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUORD_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPPCAUORD_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSCOOKPP.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPPCAUORD_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PPCAUORD
     i_obj.ecpSave()
  endproc

  add object oDESORD_2_3 as StdField with uid="FXWOMDYSDV",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESORD", cQueryName = "DESORD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259337162,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=32, InputMask=replicate('X',35)

  add object oPPCAUIMP_2_4 as StdField with uid="JMKQEFDHNZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PPCAUIMP", cQueryName = "PPCAUIMP",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale impegni ODL inesistente o incongruente",;
    ToolTipText = "Codice causale associata agli impegni ODL",;
    HelpContextID = 173285306,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=57, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PPCAUIMP"

  func oPPCAUIMP_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUIMP_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUIMP_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPPCAUIMP_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSCOIKPP.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPPCAUIMP_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PPCAUIMP
     i_obj.ecpSave()
  endproc

  add object oDESIMP_2_5 as StdField with uid="MHWQEKWCKE",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 63646666,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=57, InputMask=replicate('X',35)

  add object oPPCAUTER_2_6 as StdField with uid="RHKXAMLBNJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PPCAUTER", cQueryName = "PPCAUTER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale ordini a terzisti incongruente",;
    ToolTipText = "Codice causale associata al documento di ordini a terzisti (per OCL)",;
    HelpContextID = 11264072,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=82, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCAUTER"

  func oPPCAUTER_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUTER_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUTER_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCAUTER_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali documenti",'GSCO_KGL.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPPCAUTER_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PPCAUTER
     i_obj.ecpSave()
  endproc

  add object oDESCAZ_2_7 as StdField with uid="EBUOFUENZA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESCAZ", cQueryName = "DESCAZ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177286090,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=247, Top=82, InputMask=replicate('X',35)

  add object oPPCAURCL_2_8 as StdField with uid="QUHSZKSUOG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_PPCAURCL", cQueryName = "PPCAURCL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di default (rientro conto lavoro) per fasi da avanzare",;
    HelpContextID = 22290366,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCAURCL"

  func oPPCAURCL_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAURCL_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAURCL_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCAURCL_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'',this.parent.oContained
  endproc

  add object oDESDOC_2_9 as StdField with uid="ECMEUHIUFH",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11545546,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=248, Top=107, InputMask=replicate('X',40)

  add object oPPCAUTRA_2_10 as StdField with uid="HZXKEZUFOQ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_PPCAUTRA", cQueryName = "PPCAUTRA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale DDT di trasferimento incongruente",;
    ToolTipText = "Codice causale associata al DDT di trasferimento (per OCL)",;
    HelpContextID = 257171401,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCAUTRA"

  func oPPCAUTRA_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUTRA_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUTRA_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCAUTRA_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSCO_KGT.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPPCAUTRA_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PPCAUTRA
     i_obj.ecpSave()
  endproc

  add object oDESCAT_2_11 as StdField with uid="OFNOYQAVEB",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 9513930,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=247, Top=132, InputMask=replicate('X',35)

  add object oPPCODCAU_2_12 as StdField with uid="BJQLYJLFDC",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PPCODCAU", cQueryName = "PPCODCAU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale buono di prelievo incongruente",;
    ToolTipText = "Codice causale associata al buono di prelievo (per ODL)",;
    HelpContextID = 22421429,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCODCAU"

  func oPPCODCAU_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPGESWIP<>' ')
    endwith
   endif
  endfunc

  func oPPCODCAU_2_12.mHide()
    with this.Parent.oContained
      return (.w_PPGESWIP=' ')
    endwith
  endfunc

  func oPPCODCAU_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCODCAU_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCODCAU_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCODCAU_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSCO_KGI.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPPCODCAU_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PPCODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAB_2_13 as StdField with uid="IDDUSLSQKX",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESCAB", cQueryName = "DESCAB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 43068362,;
   bGlobalFont=.t.,;
    Height=21, Width=294, Left=247, Top=157, InputMask=replicate('X',35)

  func oDESCAB_2_13.mHide()
    with this.Parent.oContained
      return (.w_PPGESWIP=' ')
    endwith
  endfunc

  add object oPPCAUCAR_2_14 as StdField with uid="OTFIVGROIF",rtseq=44,rtrep=.f.,;
    cFormVar = "w_PPCAUCAR", cQueryName = "PPCAUCAR",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento carichi di magazzino inesistente o incongruente",;
    ToolTipText = "Codice causale associata ai documenti di carico p.F. (dichiarazioni di produzione)",;
    HelpContextID = 5513144,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCAUCAR"

  func oPPCAUCAR_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUCAR_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUCAR_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCAUCAR_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSCO_KGC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPPCAUCAR_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PPCAUCAR
     i_obj.ecpSave()
  endproc

  add object oDESCAR_2_15 as StdField with uid="RTDRJMFOJO",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 43068362,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=182, InputMask=replicate('X',35)

  add object oPPCAUSCA_2_16 as StdField with uid="OTIYLMNXVE",rtseq=46,rtrep=.f.,;
    cFormVar = "w_PPCAUSCA", cQueryName = "PPCAUSCA",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento scarichi di magazzino inesistente o incongruente",;
    ToolTipText = "Codice causale associata ai documenti di scarico componenti (dichiarazioni di produzione)",;
    HelpContextID = 5513161,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=207, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCAUSCA"

  func oPPCAUSCA_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUSCA_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUSCA_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCAUSCA_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSCO_KGS.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPPCAUSCA_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PPCAUSCA
     i_obj.ecpSave()
  endproc

  add object oDESSCA_2_17 as StdField with uid="MNZEWYYWHT",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESSCA", cQueryName = "DESSCA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 56699850,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=207, InputMask=replicate('X',35)

  add object oPPCAUMOU_2_18 as StdField with uid="GXWJGJJTVA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_PPCAUMOU", cQueryName = "PPCAUMOU",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento carichi di magazzino inesistente o incongruente",;
    ToolTipText = "Codice causale associata ai documenti di carico dei materiali di output (dichiarazioni di produzione)",;
    HelpContextID = 106176437,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=232, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PPCAUMOU"

  func oPPCAUMOU_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUMOU_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUMOU_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPPCAUMOU_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSCO_KGC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPPCAUMOU_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PPCAUMOU
     i_obj.ecpSave()
  endproc

  add object oPPMAGPRO_2_19 as StdField with uid="WDUTVMIAUT",rtseq=49,rtrep=.f.,;
    cFormVar = "w_PPMAGPRO", cQueryName = "PPMAGPRO",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino produzione inesistente o non nettificabile o di tipo WIP",;
    ToolTipText = "Codice magazzino produzione di default",;
    HelpContextID = 70483899,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=262, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PPMAGPRO"

  func oPPMAGPRO_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPMAGPRO_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPMAGPRO_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPPMAGPRO_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oPPMAGPRO_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PPMAGPRO
     i_obj.ecpSave()
  endproc

  add object oDESMAG_2_20 as StdField with uid="TIKCYBUTYM",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 226962378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=247, Top=264, InputMask=replicate('X',30)

  add object oPPMAGSCA_2_21 as StdField with uid="JFUNYHDHRJ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_PPMAGSCA", cQueryName = "PPMAGSCA",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino scarti inesistente o di tipo WIP o nettificabile",;
    ToolTipText = "Codice magazzino scarti di utilizzato nelle dichiarazioni di produzione",;
    HelpContextID = 20152265,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=292, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PPMAGSCA"

  func oPPMAGSCA_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPMAGSCA_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPMAGSCA_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPPMAGSCA_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOSMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oPPMAGSCA_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PPMAGSCA
     i_obj.ecpSave()
  endproc

  add object oDESMAS_2_22 as StdField with uid="ANWAUZFMOO",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 25635786,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=247, Top=292, InputMask=replicate('X',30)

  add object oPPMAGWIP_2_23 as StdField with uid="JGNCLWFUVX",rtseq=53,rtrep=.f.,;
    cFormVar = "w_PPMAGWIP", cQueryName = "PPMAGWIP",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino WIP inesistente o non di tipo WIP",;
    ToolTipText = "Codice magazzino WIP di default utilizzato per la gestione ODL/OCL",;
    HelpContextID = 46956614,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=322, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PPMAGWIP"

  func oPPMAGWIP_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPGESWIP<>' ')
    endwith
   endif
  endfunc

  func oPPMAGWIP_2_23.mHide()
    with this.Parent.oContained
      return (.w_PPGESWIP=' ')
    endwith
  endfunc

  func oPPMAGWIP_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPMAGWIP_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPMAGWIP_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPPMAGWIP_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oPPMAGWIP_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PPMAGWIP
     i_obj.ecpSave()
  endproc

  add object oDESMAW_2_24 as StdField with uid="GODFASNSZX",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DESMAW", cQueryName = "DESMAW",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 226962378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=247, Top=322, InputMask=replicate('X',30)

  func oDESMAW_2_24.mHide()
    with this.Parent.oContained
      return (.w_PPGESWIP=' ')
    endwith
  endfunc

  add object oPPCENCOS_2_25 as StdField with uid="AEEZGZBQCB",rtseq=55,rtrep=.f.,;
    cFormVar = "w_PPCENCOS", cQueryName = "PPCENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo impiegato nei documenti di produzione",;
    HelpContextID = 12591031,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=179, Top=377, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PPCENCOS"

  func oPPCENCOS_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oPPCENCOS_2_25.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oPPCENCOS_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCENCOS_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCENCOS_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPPCENCOS_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo",'',this.parent.oContained
  endproc

  add object oPPCAUCCM_2_26 as StdField with uid="TRIATECHEW",rtseq=56,rtrep=.f.,;
    cFormVar = "w_PPCAUCCM", cQueryName = "PPCAUCCM",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale da utilizzare per i carichi da commessa",;
    HelpContextID = 5513149,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=415, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PPCAUCCM"

  func oPPCAUCCM_2_26.mHide()
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
  endfunc

  func oPPCAUCCM_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUCCM_2_26.ecpDrop(oSource)
    this.Parent.oContained.link_2_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUCCM_2_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPPCAUCCM_2_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPPCAUCCM_2_26.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PPCAUCCM
     i_obj.ecpSave()
  endproc

  add object oPPCAUSCM_2_27 as StdField with uid="PDWDQSHGLX",rtseq=57,rtrep=.f.,;
    cFormVar = "w_PPCAUSCM", cQueryName = "PPCAUSCM",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale da utilizzare per gli scarichi da commessa",;
    HelpContextID = 5513149,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=439, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PPCAUSCM"

  func oPPCAUSCM_2_27.mHide()
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
  endfunc

  func oPPCAUSCM_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCAUSCM_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCAUSCM_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPPCAUSCM_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"CAUSALI DI MAGAZZINO",'gsdb_zmc.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPPCAUSCM_2_27.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PPCAUSCM
     i_obj.ecpSave()
  endproc

  add object oDESCEN_2_28 as StdField with uid="LIVHFQLEYG",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 105982922,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=300, Top=377, InputMask=replicate('X',40)

  func oDESCEN_2_28.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oPPCALSTA_2_58 as StdField with uid="NHMLPATPJR",rtseq=77,rtrep=.f.,;
    cFormVar = "w_PPCALSTA", cQueryName = "PPCALSTA",nZero=5,;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario di stabilimento",;
    HelpContextID = 253485111,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=351, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_PPCALSTA"

  func oPPCALSTA_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCALSTA_2_58.ecpDrop(oSource)
    this.Parent.oContained.link_2_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCALSTA_2_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oPPCALSTA_2_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oPPCALSTA_2_58.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_PPCALSTA
     i_obj.ecpSave()
  endproc

  add object oDESCAL_2_59 as StdField with uid="VQWOQOUELG",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 143731658,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=247, Top=351, InputMask=replicate('X',40)

  add object oDESCCM_2_63 as StdField with uid="VKNBBQUBBN",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESCCM", cQueryName = "DESCCM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 124857290,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=415, InputMask=replicate('X',35)

  func oDESCCM_2_63.mHide()
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
  endfunc

  add object oDESSCM_2_64 as StdField with uid="XEZFBDCSQO",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 123808714,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=439, InputMask=replicate('X',35)

  func oDESSCM_2_64.mHide()
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
  endfunc


  add object oPPPSCLAV_2_72 as StdCombo with uid="GWLDPGSRYO",rtseq=87,rtrep=.f.,left=179,top=469,width=208,height=21;
    , ToolTipText = "Trasferimento Impegni lista materiali OCL";
    , HelpContextID = 140595124;
    , cFormVar="w_PPPSCLAV",RowSource=""+"Standard,"+"Totale all'ordine a fornitore", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPPPSCLAV_2_72.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oPPPSCLAV_2_72.GetRadio()
    this.Parent.oContained.w_PPPSCLAV = this.RadioValue()
    return .t.
  endfunc

  func oPPPSCLAV_2_72.SetRadio()
    this.Parent.oContained.w_PPPSCLAV=trim(this.Parent.oContained.w_PPPSCLAV)
    this.value = ;
      iif(this.Parent.oContained.w_PPPSCLAV=='S',1,;
      iif(this.Parent.oContained.w_PPPSCLAV=='O',2,;
      0))
  endfunc

  add object oDESCAM_2_74 as StdField with uid="KOLMVWPTKX",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESCAM", cQueryName = "DESCAM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 126954442,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=232, InputMask=replicate('X',35)

  add object oStr_2_29 as StdString with uid="XSFFBMTZKX",Visible=.t., Left=6, Top=159,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale buono di prelievo:"  ;
  , bGlobalFont=.t.

  func oStr_2_29.mHide()
    with this.Parent.oContained
      return (.w_PPGESWIP=' ')
    endwith
  endfunc

  add object oStr_2_30 as StdString with uid="RDNIYCWOQA",Visible=.t., Left=6, Top=134,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale DDT trasferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="ZDUMPECZNB",Visible=.t., Left=6, Top=380,;
    Alignment=1, Width=170, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="NGCEZRTSCI",Visible=.t., Left=6, Top=34,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale magazzino ordini:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="FCNPGIABBM",Visible=.t., Left=6, Top=59,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale magazzino impegni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="HPGLQIXOJE",Visible=.t., Left=6, Top=184,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale doc. di carico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="RWFIVVGYUS",Visible=.t., Left=6, Top=209,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale doc. di scarico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="WBGXGHGSWZ",Visible=.t., Left=6, Top=264,;
    Alignment=1, Width=170, Height=18,;
    Caption="Magazzino produzione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="BRGTODZGZU",Visible=.t., Left=6, Top=292,;
    Alignment=1, Width=170, Height=18,;
    Caption="Magazzino scarti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="RQIHYRQPQJ",Visible=.t., Left=6, Top=84,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale ordine a terzista:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="CVXQUTMGKR",Visible=.t., Left=6, Top=322,;
    Alignment=1, Width=170, Height=18,;
    Caption="Magazzino WIP:"  ;
  , bGlobalFont=.t.

  func oStr_2_55.mHide()
    with this.Parent.oContained
      return (.w_PPGESWIP=' ')
    endwith
  endfunc

  add object oStr_2_60 as StdString with uid="YMTGNEIBBP",Visible=.t., Left=80, Top=353,;
    Alignment=1, Width=96, Height=18,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="BOVKPFIMMV",Visible=.t., Left=6, Top=417,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale carichi comm.:"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="FVTTCSLTOG",Visible=.t., Left=6, Top=442,;
    Alignment=1, Width=170, Height=18,;
    Caption="Causale scarichi comm.:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
  endfunc

  add object oStr_2_69 as StdString with uid="QQNXSGNAKY",Visible=.t., Left=-9, Top=109,;
    Alignment=1, Width=185, Height=18,;
    Caption="Causale DDT rientro C/Lavoro:"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="UJODKECMHS",Visible=.t., Left=12, Top=472,;
    Alignment=1, Width=164, Height=18,;
    Caption="Impegni lista materiali OCL"  ;
  , bGlobalFont=.t.

  func oStr_2_73.mHide()
    with this.Parent.oContained
      return (g_COLA<>'S')
    endwith
  endfunc

  add object oStr_2_75 as StdString with uid="LYAAPPCVSY",Visible=.t., Left=-31, Top=234,;
    Alignment=1, Width=207, Height=18,;
    Caption="Causale doc. mat.output:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsco_kppPag3 as StdContainer
  Width  = 552
  height = 493
  stdWidth  = 552
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCRIFOR_3_1 as StdCombo with uid="BKBXNLQDDK",rtseq=91,rtrep=.f.,left=365,top=79,width=162,height=21;
    , ToolTipText = "Criterio di scelta di default del miglior fornitore in base ai contratti in esse";
    , HelpContextID = 28229338;
    , cFormVar="w_CRIFOR",RowSource=""+"Tempo,"+"Prezzo,"+"Affidabilit�,"+"Priorit�", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oCRIFOR_3_1.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'A',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oCRIFOR_3_1.GetRadio()
    this.Parent.oContained.w_CRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCRIFOR_3_1.SetRadio()
    this.Parent.oContained.w_CRIFOR=trim(this.Parent.oContained.w_CRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CRIFOR=='T',1,;
      iif(this.Parent.oContained.w_CRIFOR=='Z',2,;
      iif(this.Parent.oContained.w_CRIFOR=='A',3,;
      iif(this.Parent.oContained.w_CRIFOR=='I',4,;
      0))))
  endfunc

  add object oPPCCSODA_3_2 as StdField with uid="YBHQNZCOSM",rtseq=92,rtrep=.f.,;
    cFormVar = "w_PPCCSODA", cQueryName = "PPCCSODA",;
    bObbl = .t. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo di default",;
    HelpContextID = 193847351,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=39, Top=126, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PPCCSODA"

  func oPPCCSODA_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oPPCCSODA_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCCSODA_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCCSODA_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPPCCSODA_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESCCODA_3_6 as StdField with uid="XVMGSINGFW",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DESCCODA", cQueryName = "DESCCODA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 177132663,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=176, Top=126, InputMask=replicate('X',40)

  add object oPPBORODA_3_9 as StdCheck with uid="MIRLZFBAFR",rtseq=94,rtrep=.f.,left=11, top=187, caption="Blocca generazione ordini da ODA",;
    ToolTipText = " Se attivo: blocca temporaneamente la generazione ordini a fornitore da ODA",;
    HelpContextID = 193581111,;
    cFormVar="w_PPBORODA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPPBORODA_3_9.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPPBORODA_3_9.GetRadio()
    this.Parent.oContained.w_PPBORODA = this.RadioValue()
    return .t.
  endfunc

  func oPPBORODA_3_9.SetRadio()
    this.Parent.oContained.w_PPBORODA=trim(this.Parent.oContained.w_PPBORODA)
    this.value = ;
      iif(this.Parent.oContained.w_PPBORODA=="S",1,;
      0)
  endfunc

  add object oPP__MODA_3_12 as StdRadio with uid="GKOQCMBESV",rtseq=95,rtrep=.f.,left=39, top=34, width=109,height=21;
    , ToolTipText = "Consente di scegliere la tipologia di pianificazione per gli articoli di provenienza esterna";
    , cFormVar="w_PP__MODA", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oPP__MODA_3_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Oda"
      this.Buttons(1).HelpContextID = 189505591
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Oda","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Pda"
      this.Buttons(2).HelpContextID = 189505591
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Pda","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",21)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Consente di scegliere la tipologia di pianificazione per gli articoli di provenienza esterna")
      StdRadio::init()
    endproc

  func oPP__MODA_3_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPP__MODA_3_12.GetRadio()
    this.Parent.oContained.w_PP__MODA = this.RadioValue()
    return .t.
  endfunc

  func oPP__MODA_3_12.SetRadio()
    this.Parent.oContained.w_PP__MODA=trim(this.Parent.oContained.w_PP__MODA)
    this.value = ;
      iif(this.Parent.oContained.w_PP__MODA=='S',1,;
      iif(this.Parent.oContained.w_PP__MODA=='N',2,;
      0))
  endfunc

  add object oStr_3_3 as StdString with uid="PMBXTXAVVN",Visible=.t., Left=268, Top=81,;
    Alignment=1, Width=94, Height=15,;
    Caption="Default:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="NBEAWDCSUP",Visible=.t., Left=8, Top=60,;
    Alignment=0, Width=255, Height=15,;
    Caption="Criterio di scelta del fornitore"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="MJEYCVWDGE",Visible=.t., Left=8, Top=106,;
    Alignment=0, Width=130, Height=15,;
    Caption="Centro di costo"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="YTWRTMULQB",Visible=.t., Left=8, Top=169,;
    Alignment=0, Width=199, Height=15,;
    Caption="Blocco elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="DHEORNVXUG",Visible=.t., Left=3, Top=10,;
    Alignment=0, Width=162, Height=17,;
    Caption="Articoli prov. esterna:"  ;
  , bGlobalFont=.t.

  add object oBox_3_5 as StdBox with uid="HPZZEVMUYY",left=4, top=74, width=541,height=1

  add object oBox_3_8 as StdBox with uid="SKGQABGWZI",left=4, top=121, width=541,height=1

  add object oBox_3_10 as StdBox with uid="LVKOHYQADO",left=4, top=183, width=541,height=1

  add object oBox_3_14 as StdBox with uid="WTOGUBLAXQ",left=4, top=26, width=541,height=1
enddefine
define class tgsco_kppPag4 as StdContainer
  Width  = 552
  height = 493
  stdWidth  = 552
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPBLOMRP_4_1 as StdCheck with uid="FCRQSDKZEM",rtseq=97,rtrep=.f.,left=19, top=18, caption="Blocca generazione fabbisogni",;
    ToolTipText = " Se attivo: blocca temporaneamente la generazione fabbisogni",;
    HelpContextID = 111751098,;
    cFormVar="w_PPBLOMRP", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPPBLOMRP_4_1.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPPBLOMRP_4_1.GetRadio()
    this.Parent.oContained.w_PPBLOMRP = this.RadioValue()
    return .t.
  endfunc

  func oPPBLOMRP_4_1.SetRadio()
    this.Parent.oContained.w_PPBLOMRP=trim(this.Parent.oContained.w_PPBLOMRP)
    this.value = ;
      iif(this.Parent.oContained.w_PPBLOMRP=="S",1,;
      0)
  endfunc

  add object oPPMRPLOG_4_2 as StdCheck with uid="BQDHQQVVVK",rtseq=98,rtrep=.f.,left=332, top=18, caption="Log elaborazione MRP",;
    ToolTipText = " Se attivo: abilita la scrittura file di log dell'elaborazione MRP",;
    HelpContextID = 127041475,;
    cFormVar="w_PPMRPLOG", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPPMRPLOG_4_2.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPPMRPLOG_4_2.GetRadio()
    this.Parent.oContained.w_PPMRPLOG = this.RadioValue()
    return .t.
  endfunc

  func oPPMRPLOG_4_2.SetRadio()
    this.Parent.oContained.w_PPMRPLOG=trim(this.Parent.oContained.w_PPMRPLOG)
    this.value = ;
      iif(this.Parent.oContained.w_PPMRPLOG=="S",1,;
      0)
  endfunc

  func oPPMRPLOG_4_2.mHide()
    with this.Parent.oContained
      return (g_MMRP<>'S')
    endwith
  endfunc


  add object oPPSALCOM_4_3 as StdCombo with uid="KZNOUGWCMS",rtseq=99,rtrep=.f.,left=176,top=230,width=167,height=21;
    , ToolTipText = "Consente di scegliere la modalit� di pianificazione per gli articoli gestiti a commessa";
    , HelpContextID = 14884797;
    , cFormVar="w_PPSALCOM",RowSource=""+"Pegging secondo livello,"+"Saldi commessa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPPSALCOM_4_3.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPPSALCOM_4_3.GetRadio()
    this.Parent.oContained.w_PPSALCOM = this.RadioValue()
    return .t.
  endfunc

  func oPPSALCOM_4_3.SetRadio()
    this.Parent.oContained.w_PPSALCOM=trim(this.Parent.oContained.w_PPSALCOM)
    this.value = ;
      iif(this.Parent.oContained.w_PPSALCOM=='P',1,;
      iif(this.Parent.oContained.w_PPSALCOM=='S',2,;
      0))
  endfunc

  add object oPPMRPDIV_4_5 as StdCheck with uid="DGSRRHPKGQ",rtseq=100,rtrep=.f.,left=332, top=44, caption="Abilita divisione cursore MRP",;
    ToolTipText = "Se attivo: abilita la divisione del cursore di elaborazione MRP in 2 parti",;
    HelpContextID = 7176268,;
    cFormVar="w_PPMRPDIV", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPPMRPDIV_4_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPPMRPDIV_4_5.GetRadio()
    this.Parent.oContained.w_PPMRPDIV = this.RadioValue()
    return .t.
  endfunc

  func oPPMRPDIV_4_5.SetRadio()
    this.Parent.oContained.w_PPMRPDIV=trim(this.Parent.oContained.w_PPMRPDIV)
    this.value = ;
      iif(this.Parent.oContained.w_PPMRPDIV=='S',1,;
      0)
  endfunc

  add object oPPORDICM_4_6 as StdCheck with uid="FONZRBBSNS",rtseq=101,rtrep=.f.,left=19, top=287, caption="Considera l'ordinato non a commessa nel MRP",;
    ToolTipText = "Se attivo: nel MRP considera l'ordinato non a commessa per gli articoli che gestiscono commessa con nettificazione",;
    HelpContextID = 189947837,;
    cFormVar="w_PPORDICM", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPPORDICM_4_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPPORDICM_4_6.GetRadio()
    this.Parent.oContained.w_PPORDICM = this.RadioValue()
    return .t.
  endfunc

  func oPPORDICM_4_6.SetRadio()
    this.Parent.oContained.w_PPORDICM=trim(this.Parent.oContained.w_PPORDICM)
    this.value = ;
      iif(this.Parent.oContained.w_PPORDICM=='S',1,;
      0)
  endfunc

  func oPPORDICM_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPSALCOM='S')
    endwith
   endif
  endfunc

  add object oPPSCAMRP_4_9 as StdCheck with uid="TKGWUXCCQI",rtseq=102,rtrep=.f.,left=19, top=314, caption="Non considerare gli abbinamenti al magazzino scarti nel pegging di commessa",;
    ToolTipText = " Se attivo: Non considera gli abbinamenti al magazzino scarti nel pegging di commessa nella generazione dei fabbisogni",;
    HelpContextID = 126951354,;
    cFormVar="w_PPSCAMRP", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPPSCAMRP_4_9.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPPSCAMRP_4_9.GetRadio()
    this.Parent.oContained.w_PPSCAMRP = this.RadioValue()
    return .t.
  endfunc

  func oPPSCAMRP_4_9.SetRadio()
    this.Parent.oContained.w_PPSCAMRP=trim(this.Parent.oContained.w_PPSCAMRP)
    this.value = ;
      iif(this.Parent.oContained.w_PPSCAMRP=="S",1,;
      0)
  endfunc

  func oPPSCAMRP_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPSALCOM<>'S')
    endwith
   endif
  endfunc

  add object oPPCODCOM_4_10 as StdField with uid="IFRCPPJABI",rtseq=103,rtrep=.f.,;
    cFormVar = "w_PPCODCOM", cQueryName = "PPCODCOM",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di default per saldi liberi",;
    HelpContextID = 22421437,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=176, Top=258, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PPCODCOM"

  func oPPCODCOM_4_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCODCOM_4_10.ecpDrop(oSource)
    this.Parent.oContained.link_4_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCODCOM_4_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPPCODCOM_4_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oDESCOM_4_12 as StdField with uid="XISLSXZGHT",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 112274378,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=297, Top=258, InputMask=replicate('X',40)


  add object oPPDICCOM_4_13 as StdCombo with uid="GKMKSZRIVY",rtseq=105,rtrep=.f.,left=228,top=409,width=210,height=21;
    , ToolTipText = "Consente di gestire la commessa e l'eventuale attivit� nei movimenti di scarico della produzione sempre o per i soli articoli personalizzati";
    , HelpContextID = 23859133;
    , cFormVar="w_PPDICCOM",RowSource=""+"Gestisci sempre commessa e attivit�,"+"Gestisci commessa e attivit� solo articoli gestiti a commessa pers.", bObbl = .f. , nPag = 4;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPPDICCOM_4_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oPPDICCOM_4_13.GetRadio()
    this.Parent.oContained.w_PPDICCOM = this.RadioValue()
    return .t.
  endfunc

  func oPPDICCOM_4_13.SetRadio()
    this.Parent.oContained.w_PPDICCOM=trim(this.Parent.oContained.w_PPDICCOM)
    this.value = ;
      iif(this.Parent.oContained.w_PPDICCOM=='S',1,;
      iif(this.Parent.oContained.w_PPDICCOM=='C',2,;
      0))
  endfunc

  add object oPPATHMRP_4_15 as StdField with uid="QUARTVKHNU",rtseq=106,rtrep=.f.,;
    cFormVar = "w_PPATHMRP", cQueryName = "PPATHMRP",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di destinazione del file di elaborazione MRP (elab.DBF)",;
    HelpContextID = 118570938,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=195, Top=74, InputMask=replicate('X',200)

  func oPPATHMRP_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(!EMPTY(alltrim(.w_PPATHMRP)), DIRECTORY(.w_PPATHMRP), true))
    endwith
    return bRes
  endfunc


  add object oBtn_4_16 as StdButton with uid="XUMXKKPAKR",left=522, top=74, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Premere per selezionare il percorso di destinazione";
    , HelpContextID = 119106518;
    , Tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_4_16.Click()
      with this.Parent.oContained
        .w_PPATHMRP=left(getdir(IIF(EMPTY(.w_PPATHMRP),sys(5)+sys(2003),.w_PPATHMRP),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPPIMPLOT_4_20 as StdCombo with uid="LSXEIXMIWM",rtseq=107,rtrep=.f.,left=228,top=379,width=183,height=21;
    , ToolTipText = "Tipologia imputazione lotto";
    , HelpContextID = 127385526;
    , cFormVar="w_PPIMPLOT",RowSource=""+"Non obbligatoria,"+"Obbligatoria", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPPIMPLOT_4_20.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPPIMPLOT_4_20.GetRadio()
    this.Parent.oContained.w_PPIMPLOT = this.RadioValue()
    return .t.
  endfunc

  func oPPIMPLOT_4_20.SetRadio()
    this.Parent.oContained.w_PPIMPLOT=trim(this.Parent.oContained.w_PPIMPLOT)
    this.value = ;
      iif(this.Parent.oContained.w_PPIMPLOT=='N',1,;
      iif(this.Parent.oContained.w_PPIMPLOT=='S',2,;
      0))
  endfunc

  add object oPPPRJMOD_4_22 as StdField with uid="WOOEWHUAOF",rtseq=108,rtrep=.f.,;
    cFormVar = "w_PPPRJMOD", cQueryName = "PPPRJMOD",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso del file utilizzato per l'esportazione/importazione su MSProject",;
    HelpContextID = 116543430,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=195, Top=99, InputMask=replicate('X',200)


  add object oBtn_4_23 as StdButton with uid="OWSCPWLAVP",left=522, top=99, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Premere per selezionare il file di MSProject";
    , HelpContextID = 119106518;
    , Tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_4_23.Click()
      with this.Parent.oContained
        .w_PPPRJMOD=getfile('mp?')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPPPERPIA_4_25 as StdCombo with uid="DRHNEVIUIN",rtseq=109,rtrep=.f.,left=195,top=124,width=162,height=21;
    , ToolTipText = "Pianifica per periodo";
    , HelpContextID = 209760311;
    , cFormVar="w_PPPERPIA",RowSource=""+"Giornaliero,"+"Settimanale,"+"Mensile,"+"Semestrale,"+"Da anagrafica articoli", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPPPERPIA_4_25.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'S',;
    iif(this.value =5,'A',;
    space(1)))))))
  endfunc
  func oPPPERPIA_4_25.GetRadio()
    this.Parent.oContained.w_PPPERPIA = this.RadioValue()
    return .t.
  endfunc

  func oPPPERPIA_4_25.SetRadio()
    this.Parent.oContained.w_PPPERPIA=trim(this.Parent.oContained.w_PPPERPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PPPERPIA=='D',1,;
      iif(this.Parent.oContained.w_PPPERPIA=='W',2,;
      iif(this.Parent.oContained.w_PPPERPIA=='M',3,;
      iif(this.Parent.oContained.w_PPPERPIA=='S',4,;
      iif(this.Parent.oContained.w_PPPERPIA=='A',5,;
      0)))))
  endfunc


  add object oPPCRIELA_4_26 as StdCombo with uid="KQYUEYOITH",rtseq=110,rtrep=.f.,left=195,top=148,width=162,height=21;
    , ToolTipText = "Criterio di pianificazione MRP di default";
    , HelpContextID = 251862985;
    , cFormVar="w_PPCRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPPCRIELA_4_26.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oPPCRIELA_4_26.GetRadio()
    this.Parent.oContained.w_PPCRIELA = this.RadioValue()
    return .t.
  endfunc

  func oPPCRIELA_4_26.SetRadio()
    this.Parent.oContained.w_PPCRIELA=trim(this.Parent.oContained.w_PPCRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_PPCRIELA=='A',1,;
      iif(this.Parent.oContained.w_PPCRIELA=='M',2,;
      iif(this.Parent.oContained.w_PPCRIELA=='G',3,;
      0)))
  endfunc


  add object oPPPIAPUN_4_28 as StdCombo with uid="QHVQIVCNUN",rtseq=111,rtrep=.f.,left=195,top=172,width=162,height=21;
    , ToolTipText = "Pianificazione puntuale";
    , HelpContextID = 192196676;
    , cFormVar="w_PPPIAPUN",RowSource=""+"Si,"+"No,"+"Da anagrafica articoli", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPPPIAPUN_4_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPPPIAPUN_4_28.GetRadio()
    this.Parent.oContained.w_PPPIAPUN = this.RadioValue()
    return .t.
  endfunc

  func oPPPIAPUN_4_28.SetRadio()
    this.Parent.oContained.w_PPPIAPUN=trim(this.Parent.oContained.w_PPPIAPUN)
    this.value = ;
      iif(this.Parent.oContained.w_PPPIAPUN=='S',1,;
      iif(this.Parent.oContained.w_PPPIAPUN=='N',2,;
      iif(this.Parent.oContained.w_PPPIAPUN=='A',3,;
      0)))
  endfunc

  add object oStr_4_4 as StdString with uid="RISQQCETLV",Visible=.t., Left=34, Top=232,;
    Alignment=1, Width=137, Height=18,;
    Caption="Gestisci commessa con:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="JABUBYLAJQ",Visible=.t., Left=6, Top=198,;
    Alignment=0, Width=218, Height=18,;
    Caption="Parametri gestione commessa"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="MGCOXOFDWE",Visible=.t., Left=1, Top=260,;
    Alignment=1, Width=170, Height=18,;
    Caption="Commessa saldi liberi:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="EXYAOJAOBT",Visible=.t., Left=24, Top=410,;
    Alignment=1, Width=197, Height=18,;
    Caption="In fase di scarico della produzione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="BQPXSTBRUO",Visible=.t., Left=16, Top=77,;
    Alignment=1, Width=175, Height=18,;
    Caption="Path elab MRP (elab.DBF):"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="GASQLEPJGW",Visible=.t., Left=5, Top=348,;
    Alignment=0, Width=218, Height=18,;
    Caption="Parametri dichiarazioni di produzione"  ;
  , bGlobalFont=.t.

  add object oStr_4_21 as StdString with uid="CNDWQAUWLJ",Visible=.t., Left=44, Top=382,;
    Alignment=1, Width=177, Height=18,;
    Caption="Imputazione lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_24 as StdString with uid="JWGDEXABFX",Visible=.t., Left=37, Top=102,;
    Alignment=1, Width=154, Height=18,;
    Caption="Modello MSProject:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="VFGVCCTVJM",Visible=.t., Left=39, Top=151,;
    Alignment=1, Width=152, Height=18,;
    Caption="Criterio di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_29 as StdString with uid="VSKSWONFNJ",Visible=.t., Left=9, Top=128,;
    Alignment=1, Width=182, Height=18,;
    Caption="Periodo di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_30 as StdString with uid="UYQPTYMPSQ",Visible=.t., Left=9, Top=175,;
    Alignment=1, Width=182, Height=17,;
    Caption="Pianificazione puntuale:"  ;
  , bGlobalFont=.t.

  add object oBox_4_8 as StdBox with uid="VLGZZVZSZD",left=5, top=217, width=541,height=2

  add object oBox_4_19 as StdBox with uid="JBJBTAGXDR",left=4, top=366, width=541,height=2
enddefine
define class tgsco_kppPag5 as StdContainer
  Width  = 552
  height = 493
  stdWidth  = 552
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPSRVDFT_5_2 as StdField with uid="TGWFLOKZQJ",rtseq=113,rtrep=.f.,;
    cFormVar = "w_PPSRVDFT", cQueryName = "PPSRVDFT",;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un servizio di tipo quantit� e valore",;
    ToolTipText = "Servizio di default (usato dalle risorse)",;
    HelpContextID = 13492298,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=20, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PPSRVDFT"

  func oPPSRVDFT_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPSRVDFT_5_2.ecpDrop(oSource)
    this.Parent.oContained.link_5_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPSRVDFT_5_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPPSRVDFT_5_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Servizi",'GSCO_KPP.ART_ICOL_VZM',this.parent.oContained
  endproc


  add object oPPMATINP_5_4 as StdCombo with uid="PRPDXWDFON",rtseq=114,rtrep=.f.,left=160,top=47,width=147,height=21;
    , ToolTipText = "Indica la modalit� di gestione dei materiali di input all'interno dell'ordine di lavorazione";
    , HelpContextID = 174292922;
    , cFormVar="w_PPMATINP",RowSource=""+"Associa all'ultima fase,"+"Associa alla prima fase", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPPMATINP_5_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oPPMATINP_5_4.GetRadio()
    this.Parent.oContained.w_PPMATINP = this.RadioValue()
    return .t.
  endfunc

  func oPPMATINP_5_4.SetRadio()
    this.Parent.oContained.w_PPMATINP=trim(this.Parent.oContained.w_PPMATINP)
    this.value = ;
      iif(this.Parent.oContained.w_PPMATINP=='S',1,;
      iif(this.Parent.oContained.w_PPMATINP=='P',2,;
      0))
  endfunc


  add object oPPMATOUP_5_5 as StdCombo with uid="WWNOCLMILD",rtseq=115,rtrep=.f.,left=160,top=74,width=147,height=21;
    , ToolTipText = "Indica la modalit� di gestione dei materiali di output all'interno dell'ordine di lavorazione";
    , HelpContextID = 194805830;
    , cFormVar="w_PPMATOUP",RowSource=""+"Associa all'ultima fase,"+"Associa alla prima fase,"+"Non associare", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPPMATOUP_5_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPPMATOUP_5_5.GetRadio()
    this.Parent.oContained.w_PPMATOUP = this.RadioValue()
    return .t.
  endfunc

  func oPPMATOUP_5_5.SetRadio()
    this.Parent.oContained.w_PPMATOUP=trim(this.Parent.oContained.w_PPMATOUP)
    this.value = ;
      iif(this.Parent.oContained.w_PPMATOUP=='S',1,;
      iif(this.Parent.oContained.w_PPMATOUP=='P',2,;
      iif(this.Parent.oContained.w_PPMATOUP=='N',3,;
      0)))
  endfunc


  add object oPPMGARFS_5_6 as StdCombo with uid="YLPWBKIBIB",rtseq=116,rtrep=.f.,left=160,top=130,width=147,height=22;
    , ToolTipText = "Modalit� di generazione dell'articolo di fase";
    , HelpContextID = 225607753;
    , cFormVar="w_PPMGARFS",RowSource=""+"Prefisso Articolo,"+"Classe codifica articolo", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPPMGARFS_5_6.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oPPMGARFS_5_6.GetRadio()
    this.Parent.oContained.w_PPMGARFS = this.RadioValue()
    return .t.
  endfunc

  func oPPMGARFS_5_6.SetRadio()
    this.Parent.oContained.w_PPMGARFS=trim(this.Parent.oContained.w_PPMGARFS)
    this.value = ;
      iif(this.Parent.oContained.w_PPMGARFS=='P',1,;
      iif(this.Parent.oContained.w_PPMGARFS=='C',2,;
      0))
  endfunc

  add object oPPPREFAS_5_7 as StdField with uid="DMSABJNDTJ",rtseq=117,rtrep=.f.,;
    cFormVar = "w_PPPREFAS", cQueryName = "PPPREFAS",;
    bObbl = .t. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso articolo fase",;
    HelpContextID = 239226807,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=157, InputMask=replicate('X',5)

  func oPPPREFAS_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPMGARFS='P')
    endwith
   endif
  endfunc

  func oPPPREFAS_5_7.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_PRFA='S'
    endwith
    return i_bres
  endfunc

  add object oPPFASSEP_5_8 as StdField with uid="ERDWWQDYDF",rtseq=118,rtrep=.f.,;
    cFormVar = "w_PPFASSEP", cQueryName = "PPFASSEP",;
    bObbl = .t. , nPag = 5, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Separatore articolo di fase",;
    HelpContextID = 260837446,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=375, Top=157, InputMask=replicate('X',1)

  func oPPFASSEP_5_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPMGARFS='P')
    endwith
   endif
  endfunc

  func oPPFASSEP_5_8.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_PRFA='S'
    endwith
    return i_bres
  endfunc

  add object oPPCLAART_5_9 as StdField with uid="YYXWWWAEQY",rtseq=119,rtrep=.f.,;
    cFormVar = "w_PPCLAART", cQueryName = "PPCLAART",;
    bObbl = .t. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe di codifica dell'articolo di fase",;
    HelpContextID = 59318198,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=184, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCF_MAST", cZoomOnZoom="GSCI_ACC", oKey_1_1="CFTIPCLA", oKey_1_2="this.w_PPTIPCLA", oKey_2_1="CFCODICE", oKey_2_2="this.w_PPCLAART"

  func oPPCLAART_5_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPMGARFS='C')
    endwith
   endif
  endfunc

  func oPPCLAART_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_9('Part',this)
    endwith
    return bRes
  endfunc

  func oPPCLAART_5_9.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_PRFA='S'
    endwith
    return i_bres
  endfunc

  proc oPPCLAART_5_9.ecpDrop(oSource)
    this.Parent.oContained.link_5_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCLAART_5_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CCF_MAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCLA="+cp_ToStrODBC(this.Parent.oContained.w_PPTIPCLA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCLA="+cp_ToStr(this.Parent.oContained.w_PPTIPCLA)
    endif
    do cp_zoom with 'CCF_MAST','*','CFTIPCLA,CFCODICE',cp_AbsName(this.parent,'oPPCLAART_5_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCI_ACC',"Classi codice di fase",'',this.parent.oContained
  endproc
  proc oPPCLAART_5_9.mZoomOnZoom
    local i_obj
    i_obj=GSCI_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCLA=w_PPTIPCLA
     i_obj.w_CFCODICE=this.parent.oContained.w_PPCLAART
     i_obj.ecpSave()
  endproc


  add object oPPDCODFAS_5_10 as StdCombo with uid="TOIUGYTWXI",rtseq=120,rtrep=.f.,left=160,top=211,width=181,height=22;
    , ToolTipText = "Indica la modalit� di composizione della descrizione dell'articolo di fase";
    , HelpContextID = 5109095;
    , cFormVar="w_PPDCODFAS",RowSource=""+"Desc articolo+fase,"+"Descrizione articolo,"+"Descrizione della fase,"+"Codice articolo+codice fase", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPPDCODFAS_5_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    iif(this.value =3,'D',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPPDCODFAS_5_10.GetRadio()
    this.Parent.oContained.w_PPDCODFAS = this.RadioValue()
    return .t.
  endfunc

  func oPPDCODFAS_5_10.SetRadio()
    this.Parent.oContained.w_PPDCODFAS=trim(this.Parent.oContained.w_PPDCODFAS)
    this.value = ;
      iif(this.Parent.oContained.w_PPDCODFAS=='C',1,;
      iif(this.Parent.oContained.w_PPDCODFAS=='S',2,;
      iif(this.Parent.oContained.w_PPDCODFAS=='D',3,;
      iif(this.Parent.oContained.w_PPDCODFAS=='A',4,;
      0))))
  endfunc


  add object oPPDSUPFA_5_11 as StdCombo with uid="YOQMAATJIZ",rtseq=121,rtrep=.f.,left=160,top=239,width=181,height=21;
    , ToolTipText = "Indica la modalit� di composizione della descrizione supplementare dell'articolo di fase";
    , HelpContextID = 213774391;
    , cFormVar="w_PPDSUPFA",RowSource=""+"Descrizione supp. dell'articolo,"+"Descrizione della fase,"+"Dettaglio,"+"Nessuna descrizione", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPPDSUPFA_5_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oPPDSUPFA_5_11.GetRadio()
    this.Parent.oContained.w_PPDSUPFA = this.RadioValue()
    return .t.
  endfunc

  func oPPDSUPFA_5_11.SetRadio()
    this.Parent.oContained.w_PPDSUPFA=trim(this.Parent.oContained.w_PPDSUPFA)
    this.value = ;
      iif(this.Parent.oContained.w_PPDSUPFA=='S',1,;
      iif(this.Parent.oContained.w_PPDSUPFA=='D',2,;
      iif(this.Parent.oContained.w_PPDSUPFA=='G',3,;
      iif(this.Parent.oContained.w_PPDSUPFA=='N',4,;
      0))))
  endfunc

  add object oPPSRVDSC_5_15 as StdField with uid="JZMFBJJSGQ",rtseq=122,rtrep=.f.,;
    cFormVar = "w_PPSRVDSC", cQueryName = "PPSRVDSC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 254943175,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=314, Top=20, InputMask=replicate('X',40)

  add object oPPCLAFAS_5_21 as StdField with uid="XTMFFOISEV",rtseq=124,rtrep=.f.,;
    cFormVar = "w_PPCLAFAS", cQueryName = "PPCLAFAS",;
    bObbl = .t. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe codice fase di default",;
    HelpContextID = 243867575,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=293, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCF_MAST", cZoomOnZoom="GSCI_ACC", oKey_1_1="CFTIPCLA", oKey_1_2="this.w_PPTIPCLC", oKey_2_1="CFCODICE", oKey_2_2="this.w_PPCLAFAS"

  func oPPCLAFAS_5_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_21('Part',this)
    endwith
    return bRes
  endfunc

  func oPPCLAFAS_5_21.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_PRFA='S'
    endwith
    return i_bres
  endfunc

  proc oPPCLAFAS_5_21.ecpDrop(oSource)
    this.Parent.oContained.link_5_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCLAFAS_5_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CCF_MAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCLA="+cp_ToStrODBC(this.Parent.oContained.w_PPTIPCLC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCLA="+cp_ToStr(this.Parent.oContained.w_PPTIPCLC)
    endif
    do cp_zoom with 'CCF_MAST','*','CFTIPCLA,CFCODICE',cp_AbsName(this.parent,'oPPCLAFAS_5_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCI_ACC',"Classi codice di fase",'',this.parent.oContained
  endproc
  proc oPPCLAFAS_5_21.mZoomOnZoom
    local i_obj
    i_obj=GSCI_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCLA=w_PPTIPCLC
     i_obj.w_CFCODICE=this.parent.oContained.w_PPCLAFAS
     i_obj.ecpSave()
  endproc

  add object oPPDESCFAS_5_22 as StdField with uid="IOREEZOQDO",rtseq=125,rtrep=.f.,;
    cFormVar = "w_PPDESCFAS", cQueryName = "PPDESCFAS",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 261092711,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=225, Top=293, InputMask=replicate('X',30)


  add object oPP_CICLI_5_24 as StdCombo with uid="QOBMLVGUMC",rtseq=126,rtrep=.f.,left=160,top=352,width=147,height=22;
    , ToolTipText = "Se attivo: abilita utilizzo in linea dei cicli di lavorazione";
    , HelpContextID = 250585151;
    , cFormVar="w_PP_CICLI",RowSource=""+"Cicli di lavorazione,"+"Cicli semplificati", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPP_CICLI_5_24.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPP_CICLI_5_24.GetRadio()
    this.Parent.oContained.w_PP_CICLI = this.RadioValue()
    return .t.
  endfunc

  func oPP_CICLI_5_24.SetRadio()
    this.Parent.oContained.w_PP_CICLI=trim(this.Parent.oContained.w_PP_CICLI)
    this.value = ;
      iif(this.Parent.oContained.w_PP_CICLI=='S',1,;
      iif(this.Parent.oContained.w_PP_CICLI=='N',2,;
      0))
  endfunc

  add object oPPUNIPRO_5_29 as StdField with uid="KNQYCMBMDK",rtseq=128,rtrep=.f.,;
    cFormVar = "w_PPUNIPRO", cQueryName = "PPUNIPRO",;
    bObbl = .t. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inestente.",;
    ToolTipText = "Unit� produttiva",;
    HelpContextID = 67502011,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=381, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_UNIPRO", oKey_2_1="RLCODICE", oKey_2_2="this.w_PPUNIPRO"

  func oPPUNIPRO_5_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_29('Part',this)
    endwith
    return bRes
  endfunc

  func oPPUNIPRO_5_29.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_PRFA='S' and g_CICLILAV='S'
    endwith
    return i_bres
  endfunc

  proc oPPUNIPRO_5_29.ecpDrop(oSource)
    this.Parent.oContained.link_5_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPUNIPRO_5_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_UNIPRO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_UNIPRO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oPPUNIPRO_5_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Servizi",'GSCI_ZLR.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oDEUNIPRO_5_31 as StdField with uid="NUHLTCNYOD",rtseq=129,rtrep=.f.,;
    cFormVar = "w_DEUNIPRO", cQueryName = "DEUNIPRO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 67505019,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=314, Top=381, InputMask=replicate('X',40)


  add object oPPFECLAV_5_32 as StdCombo with uid="BFGRGKSRPL",rtseq=130,rtrep=.f.,left=262,top=437,width=142,height=21;
    , ToolTipText = "Controlla, in fase di salvataggio di documenti di rientro di conto lavoro, le righe relative a fasi esterne per le quali non � stata avanzata in quantit� sufficiente la fase precedente.";
    , HelpContextID = 141553588;
    , cFormVar="w_PPFECLAV",RowSource=""+"Si con conferma,"+"Si", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oPPFECLAV_5_32.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPPFECLAV_5_32.GetRadio()
    this.Parent.oContained.w_PPFECLAV = this.RadioValue()
    return .t.
  endfunc

  func oPPFECLAV_5_32.SetRadio()
    this.Parent.oContained.w_PPFECLAV=trim(this.Parent.oContained.w_PPFECLAV)
    this.value = ;
      iif(this.Parent.oContained.w_PPFECLAV=='N',1,;
      iif(this.Parent.oContained.w_PPFECLAV=='S',2,;
      0))
  endfunc

  add object oPPDESCART_5_39 as StdField with uid="IBNATSBACA",rtseq=131,rtrep=.f.,;
    cFormVar = "w_PPDESCART", cQueryName = "PPDESCART",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 7342712,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=223, Top=184, InputMask=replicate('X',30)

  add object oPPNOTFAS_5_47 as StdCheck with uid="KLJAODNIGF",rtseq=136,rtrep=.f.,left=375, top=237, caption="Note fase",;
    ToolTipText = "Se attivo inserisce le note della fase nella descrizione supplementare dell'articolo di fase",;
    HelpContextID = 223702967,;
    cFormVar="w_PPNOTFAS", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPPNOTFAS_5_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPPNOTFAS_5_47.GetRadio()
    this.Parent.oContained.w_PPNOTFAS = this.RadioValue()
    return .t.
  endfunc

  func oPPNOTFAS_5_47.SetRadio()
    this.Parent.oContained.w_PPNOTFAS=trim(this.Parent.oContained.w_PPNOTFAS)
    this.value = ;
      iif(this.Parent.oContained.w_PPNOTFAS=='S',1,;
      0)
  endfunc

  add object oStr_5_3 as StdString with uid="RXZXCHEGNG",Visible=.t., Left=49, Top=21,;
    Alignment=1, Width=107, Height=18,;
    Caption="Servizio di default:"  ;
  , bGlobalFont=.t.

  add object oStr_5_12 as StdString with uid="RDPEOSIYIG",Visible=.t., Left=0, Top=242,;
    Alignment=1, Width=156, Height=18,;
    Caption="Desc. supp. articolo di fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_13 as StdString with uid="NHWDHRGYQY",Visible=.t., Left=49, Top=48,;
    Alignment=1, Width=107, Height=18,;
    Caption="Materiali di input:"  ;
  , bGlobalFont=.t.

  add object oStr_5_14 as StdString with uid="MWSTKRPEKV",Visible=.t., Left=0, Top=214,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione articolo di fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_17 as StdString with uid="QZEXKZZQMQ",Visible=.t., Left=48, Top=75,;
    Alignment=1, Width=108, Height=18,;
    Caption="Materiali di output:"  ;
  , bGlobalFont=.t.

  add object oStr_5_18 as StdString with uid="AYZYDUJUMV",Visible=.t., Left=26, Top=159,;
    Alignment=1, Width=130, Height=18,;
    Caption="Prefisso articolo fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_19 as StdString with uid="MZDVWBORPV",Visible=.t., Left=224, Top=159,;
    Alignment=1, Width=147, Height=18,;
    Caption="Separatore articolo fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_23 as StdString with uid="CWDVKVJXPB",Visible=.t., Left=26, Top=294,;
    Alignment=1, Width=130, Height=18,;
    Caption="Classe codice fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_25 as StdString with uid="ZAMBGOSRNB",Visible=.t., Left=61, Top=355,;
    Alignment=1, Width=95, Height=14,;
    Caption="Tipologia cicli:"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="CATCEQRDYC",Visible=.t., Left=3, Top=324,;
    Alignment=0, Width=218, Height=17,;
    Caption="Parametri stabilimento"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="GWPLJTMKOP",Visible=.t., Left=49, Top=383,;
    Alignment=1, Width=107, Height=18,;
    Caption="Unit� produttiva:"  ;
  , bGlobalFont=.t.

  add object oStr_5_34 as StdString with uid="CFKBHZCVZD",Visible=.t., Left=3, Top=413,;
    Alignment=0, Width=199, Height=18,;
    Caption="Avanzamento fasi"  ;
  , bGlobalFont=.t.

  add object oStr_5_35 as StdString with uid="ZGFJWDGJRU",Visible=.t., Left=11, Top=440,;
    Alignment=1, Width=248, Height=18,;
    Caption="Controllo avanzamento fasi esterne:"  ;
  , bGlobalFont=.t.

  add object oStr_5_36 as StdString with uid="CEMGAXNLFX",Visible=.t., Left=3, Top=103,;
    Alignment=0, Width=228, Height=18,;
    Caption="Parametri generazione articolo di fase"  ;
  , bGlobalFont=.t.

  add object oStr_5_38 as StdString with uid="OAWIMIIFBD",Visible=.t., Left=-15, Top=132,;
    Alignment=1, Width=171, Height=18,;
    Caption="Generazione articolo di fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_40 as StdString with uid="YRKVJSTYIB",Visible=.t., Left=18, Top=186,;
    Alignment=1, Width=138, Height=18,;
    Caption="Classe articolo di fase:"  ;
  , bGlobalFont=.t.

  add object oStr_5_41 as StdString with uid="CCUHQUZPJF",Visible=.t., Left=3, Top=267,;
    Alignment=0, Width=228, Height=18,;
    Caption="Parametri generazione codice di fase"  ;
  , bGlobalFont=.t.

  add object oBox_5_27 as StdBox with uid="XMFYNHSJQY",left=0, top=340, width=541,height=2

  add object oBox_5_33 as StdBox with uid="PUOUUFOQPT",left=2, top=430, width=541,height=1

  add object oBox_5_37 as StdBox with uid="UCKSYEHZYJ",left=0, top=120, width=541,height=2

  add object oBox_5_42 as StdBox with uid="IBGOGCAPBB",left=0, top=284, width=541,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kpp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
