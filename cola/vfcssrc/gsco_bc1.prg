* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bc1                                                        *
*              Carica materiali terzista                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_68]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-17                                                      *
* Last revis.: 2002-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bc1",oParentObject)
return(i_retval)

define class tgsco_bc1 as StdBatch
  * --- Local variables
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_DATFIL = ctod("  /  /  ")
  w_ROWORD = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- batch caricamento materiali contratto acquisto - (da GSAC_MCL)
    * --- Legge i materiali dalla distinta base
    this.w_DBCODINI = SPACE(20)
    this.w_DBCODFIN = SPACE(20)
    if NOT EMPTY(this.oParentObject.w_CODART)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODDIS"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODDIS;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DBCODINI = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(this.w_DBCODINI)
        ah_ErrorMsg("Distinta base non definita",,"")
      else
        * --- Eplode Distinta Selezionata
        ah_Msg("Fase 1: esplosione distinta base...",.T.)
        this.w_DBCODINI = this.oParentObject.w_CODART
        this.w_DBCODFIN = this.oParentObject.w_CODART
        this.w_MAXLEVEL = 1
        this.w_VERIFICA = "S"
        this.w_FILSTAT = " "
        this.w_DATFIL = i_DATSYS
        if  used("TES_PLOS")
          select TES_PLOS
          use
        endif
        gsar_bde(this,"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if  used("TES_PLOS")
          * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
          SELECT (this.oParentObject.cTrsName)
          delete ALL
          L_del=SET("DELETED")
          SET DELETED OFF
          GO TOP
          do while DELETED() AND NOT EOF()
            SKIP
          enddo
          this.oParentObject.nFirstRow=recno()
          GO BOTTOM
          do while DELETED() AND NOT BOF()
            SKIP -1
          enddo
          this.oParentObject.nLastRow=recno()
          SET DELETED &L_del
          * --- Cursore di Elaborazione
          this.w_ROWORD = 0
          select TES_PLOS
          GO TOP
          * --- Cicla sulla Distinta Base Esplosa
          SCAN FOR NOT EMPTY(DISPAD) AND NOT EMPTY(NVL(CODCOM,"")) AND VAL(NVL(NUMLEV,0))=1
          this.oParentObject.InitRow()
          * --- Valorizza Variabili di Riga ...
          select TES_PLOS
          this.w_ROWORD = this.w_ROWORD + 10
          this.oParentObject.w_CPROWORD = this.w_ROWORD
          this.oParentObject.w_MCCODICE = CODCOM
          this.oParentObject.w_DESART = DESCOM
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.oParentObject.TrsFromWork()
          SELECT TES_PLOS
          ENDSCAN
          * --- Rinfrescamenti vari
          SELECT (this.oParentObject.cTrsName)
          GO TOP
          With this.oParentObject
          .WorkFromTrs()
          .SetControlsValue()
          .ChildrenChangeRow()
          .oPgFrm.Page1.oPag.oBody.Refresh()
          .oPgFrm.Page1.oPag.oBody.nAbsRow=1
          .oPgFrm.Page1.oPag.oBody.nRelRow=1
          EndWith
          * --- Chiude Cursore Elaborazione
          select TES_PLOS
          use
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
