* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bsm                                                        *
*              Stampa ODL/OCL/ODA                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_65]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-06                                                      *
* Last revis.: 2012-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bsm",oParentObject,m.Azione)
return(i_retval)

define class tgsco_bsm as StdBatch
  * --- Local variables
  Azione = space(10)
  w_PrimoPer = 0
  w_UltimoPer = 0
  i = 0
  w_MPSPER = 0
  w_Periodo = 0
  w_PerDesc = 0
  w_TITOLO = space(30)
  * --- WorkFile variables
  MPS_TPER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa ODL/OCL (da GSCO_SMP)
    do case
      case this.Azione="INIT"
        * --- Imposta il titolo della finestra
        this.w_TITOLO = IIF(this.oParentObject.w_MPS_ODL="O", "ODL", IIF(this.oParentObject.w_MPS_ODL="L", "OCL" , iif(this.oParentObject.w_MPS_ODL="C", "ODP",iif(this.oParentObject.w_MPS_ODL="F","ODR",iif(this.oParentObject.w_MPS_ODL $ "M-T","ODF","ODA")))))
        this.oparentObject.cComment = "Stampa piano " + this.w_TITOLO
        this.oparentObject.Caption = "Stampa piano " + this.w_TITOLO
      case this.Azione="STAMPA"
        if EMPTY(this.oParentObject.w_PERINI)
          this.oParentObject.w_PERINI = "000"
          this.oParentObject.w_PERFIN = right("000"+alltrim(str(this.oParentObject.w_INTERPER,3,0)),3)
        endif
        this.w_PrimoPer = val(this.oParentObject.w_PERINI)
        this.w_UltimoPer = val(this.oParentObject.w_PERFIN)
        * --- Tracciato record del cursore di stampa
        do vq_exec with "..\COLA\EXE\QUERY\GSCO1BSM",this,"__tmp__"
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Dati da stampare (pivottati)
        do case
          case this.oParentObject.w_MPS_ODL="O"
            * --- Stampa ODL (O)
            do vq_exec with "..\COLA\EXE\QUERY\GSCOOBSM",this,"_smps_"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_MPS_ODL="L"
            * --- Stampa OCL (L)
            do vq_exec with "..\COLA\EXE\QUERY\GSCOLBSM",this,"_smps_"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_MPS_ODL="C"
            * --- Stampa ODP (C)
            do vq_exec with "..\COLA\EXE\QUERY\GSCOCBSM",this,"_smps_"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_MPS_ODL="F"
            * --- Stampa ODR (F)
            do vq_exec with "..\COLA\EXE\QUERY\GSCOFBSM",this,"_smps_"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_MPS_ODL $ "T-M"
            * --- Stampa ODF (T)
            do vq_exec with "..\COLA\EXE\QUERY\GSCOTBSM",this,"_smps_"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          otherwise
            * --- Stampa ODA (E)
            do vq_exec with "..\COLA\EXE\QUERY\GSCOEBSM",this,"_smps_"
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
        select "_smps_"
        scan
        * --- resetta i valori
        this.i = this.w_PrimoPer
        do while this.i <= this.w_UltimoPer
          l_mps = right("000"+alltrim(str(this.i,2,0)),3)
          m.mps_&l_mps = -1
          this.i = this.i + 1
        enddo
        scatter memvar
        * --- Prepara i valori da inserire su __tmp__
        this.i = 0
        do while this.i<=this.oParentObject.w_INTERPER
          l_per = right("000"+alltrim(str(this.i,2,0)),3)
          l_mps = right("000"+alltrim(str( this.i+this.w_PrimoPer, 2,0)),3)
          this.w_MPSPER = m.mps_&l_mps
          if this.w_MPSPER>0
            m.per_&l_per = this.w_MPSPER
          else
            m.per_&l_per = 0
          endif
          this.i = this.i + 1
        enddo
        select __tmp__
        append blank
        gather memvar
        endscan
        * --- Prepara il titolo delle colonne per il report di stampa
        dimension tit(this.oParentObject.w_INTERPER+1)
        * --- Select from MPS_TPER
        i_nConn=i_TableProp[this.MPS_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2],.t.,this.MPS_TPER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MPS_TPER ";
              +" where TPPERASS>="+cp_ToStrODBC(this.oParentObject.w_PERINI)+" and TPPERASS<="+cp_ToStrODBC(this.oParentObject.w_PERFIN)+"";
               ,"_Curs_MPS_TPER")
        else
          select * from (i_cTable);
           where TPPERASS>=this.oParentObject.w_PERINI and TPPERASS<=this.oParentObject.w_PERFIN;
            into cursor _Curs_MPS_TPER
        endif
        if used('_Curs_MPS_TPER')
          select _Curs_MPS_TPER
          locate for 1=1
          do while not(eof())
          if not empty(_Curs_MPS_TPER.TPPERASS)
            tit(val(_Curs_MPS_TPER.TPPERASS)+1-this.w_PrimoPer) = iif(_Curs_MPS_TPER.TPPERREL="XXXX", "", _Curs_MPS_TPER.TPPERREL)
          endif
            select _Curs_MPS_TPER
            continue
          enddo
          use
        endif
        tit(val(this.oParentObject.w_PERFIN) + 1-this.w_PrimoPer) = "Oltre"
        codini = this.oParentObject.w_CODINI
        codfin = this.oParentObject.w_CODFIN
        mps_odl = this.oParentObject.w_MPS_ODL
        if this.oParentObject.w_FLGDES
          * --- Stampa anche la Descrizione
          select __tmp__
          go top
          SELECT a.fmcodice, b.dbdescri,a.fmunimis,a.per_000,a.per_001,a.per_002,a.per_003,a.per_004,a.per_005,a.per_006,;
          a.per_007,a.per_008,a.per_009,a.per_010,a.per_011,a.per_012,a.per_013,a.per_014,a.per_015,a.per_016,a.per_017;
           FROM __tmp__ a, _smps_ b WHERE a.fmcodice = b.fmcodice into cursor app
          if used("__tmp__")
            use in __tmp__
          endif
          select * from app into cursor __tmp__
          CP_CHPRN("..\COLA\EXE\QUERY\GSCODBSM", " ", this)
        else
          * --- Non stampa la Descrizione
          CP_CHPRN("..\COLA\EXE\QUERY\GSCO_BSM", " ", this)
        endif
        * --- Rilascio cursori
        if used("app")
          use in app
        endif
        if used("_smps_")
          use in _smps_
        endif
        if used("__tmp__")
          use in __tmp__
        endif
    endcase
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MPS_TPER'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
