* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscl_bgt                                                        *
*              Generazione DDT di trasferimento OCL                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_777]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-07                                                      *
* Last revis.: 2011-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscl_bgt",oParentObject,m.pAzione)
return(i_retval)

define class tgscl_bgt as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_nRecEla = 0
  w_nRecDoc = 0
  w_OPERAZ = space(2)
  w_ERRORE = .f.
  TmpC = space(100)
  TmpM = space(100)
  w_PPCENCOS = space(15)
  w_PPCAUORD = space(5)
  w_PPMAGPRO = space(5)
  w_PPCAUIMP = space(5)
  w_FLORDI = space(1)
  w_FLCASC = space(1)
  w_F2CASC = space(1)
  w_APPO = 0
  w_APPO1 = 0
  w_CAUMA2 = space(5)
  w_LNumErr = 0
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_CODODL = space(15)
  w_ROWNUM = 0
  w_QTAODL = 0
  w_QTAOD1 = 0
  w_QTAEVA = 0
  w_QTAEV1 = 0
  w_QTAPRE = 0
  w_QTAPR1 = 0
  w_QTASAL = 0
  w_OLDPR1 = 0
  w_UNMIS = space(3)
  w_FLEVAS = space(1)
  w_KEYSAL = space(20)
  w_CODART = space(20)
  w_CODICE = space(20)
  w_PROVEN = space(1)
  w_COFOR = space(15)
  w_CODDIS = space(20)
  w_CODCIC = space(15)
  w_CODMAG = space(5)
  w_MAGPRE = space(5)
  w_MAGWIP = space(5)
  w_CODCOM = space(15)
  w_TIPATT = space(1)
  w_CODATT = space(15)
  w_CODCOS = space(5)
  w_FLORCO = space(1)
  w_FLCOCO = space(1)
  w_IMPCOM = 0
  w_COCEN = space(15)
  w_VOCEN = space(15)
  w_FLIMCO = space(1)
  w_DINRIC = ctod("  /  /  ")
  w_DTRIC = ctod("  /  /  ")
  w_OMAG = space(5)
  w_PDORDINE = space(15)
  w_PDSERDOC = space(10)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_OBBLANAL = .f.
  w_QTASAL = 0
  w_MVTIPDOC = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLVEAC = space(1)
  w_MVEMERIC = space(1)
  w_MVCLADOC = space(2)
  w_MVFLPROV = space(1)
  w_NUMSCO = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_DPCODODL = space(15)
  w_PECODRIC = space(20)
  w_PECODODL = space(15)
  w_PGCODODL = space(15)
  w_PEQTAUM1 = 0
  w_PECODART = space(20)
  w_PETIPRIF = space(1)
  w_PESERIAL = space(15)
  w_PEFLCOMM = space(1)
  w_PROGR = 0
  w_PESERIAL = space(10)
  w_PEQTAABB = 0
  w_PECODCOM = space(15)
  w_PERIFTRS = space(20)
  w_PESERORD = space(10)
  w_PERIGORD = 0
  w_PEODLORI = space(15)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  DISMBASE_idx=0
  MA_COSTI_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  PAR_RIOR_idx=0
  SALDIART_idx=0
  MAGAZZIN_idx=0
  TIP_DOCU_idx=0
  TMPMODL_idx=0
  CONTI_idx=0
  PEG_SELI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Generazione DDT di Trasferimento da OCL (da GSCL_KGT)
    * --- Log Errori
    * --- Variabili Cursore
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_OPERAZ = "DT"
    * --- Legge da Parametri Causali
    this.w_PPCAUORD = " "
    this.w_PPMAGPRO = " "
    this.w_PPCAUIMP = " "
    this.w_OBBLANAL = .F.
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUORD,PPMAGPRO,PPCENCOS,PPCAUIMP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUORD,PPMAGPRO,PPCENCOS,PPCAUIMP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPCAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.w_PPMAGPRO = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      this.w_PPCAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controlla
    do case
      case EMPTY(this.w_PPMAGPRO)
        ah_ErrorMsg("Magazzino produzione non definito in tabella parametri","STOP","")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_PPCAUORD)
        ah_ErrorMsg("Causale di default (ordinato) non definita in tabella parametri","STOP","")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_PPCAUIMP)
        ah_ErrorMsg("Causale di default (impegnato) non definita in tabella parametri","STOP","")
        i_retcode = 'stop'
        return
    endcase
    * --- Legge FLAGS Causali
    this.w_TIPATT = "A"
    this.w_nRecSel = 0
    this.w_nRecEla = 0
    this.w_nRecDoc = 0
    this.w_LNumErr = 0
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET XCHK=1, OLQTAPRE=OLQTAMOV
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET XCHK=0, OLQTAPRE=0
          endif
        endif
      case this.pAzione $ "CHK-UNC"
        if used(this.NC)
          SELECT (this.NC)
          REPLACE OLQTAPRE WITH IIF(this.pAzione="CHK", OLQTAMOV, 0)
        endif
      case this.pAzione="INTERROGA"
        * --- Visualizza Zoom Elendo OCL periodo
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
        this.oParentObject.w_SELEZI = "D"
      case this.pAzione="AG"
        * --- Generazione OCL
        * --- Controlli Preliminari
        if EMPTY(this.oParentObject.w_PPTIPDOC)
          ah_ErrorMsg("Causale DDT di trasferimento non definita","STOP","")
          i_retcode = 'stop'
          return
        endif
        this.w_APPO = " "
        this.w_FLANAL = " "
        this.w_FLGCOM = " "
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDCAUMAG,TDFLANAL,TDFLCOMM"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PPTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDCAUMAG,TDFLANAL,TDFLCOMM;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_PPTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          this.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Test se Documento Gestisce Dati Analitica
        this.w_OBBLANAL = IIF((g_PERCCR="S" AND this.w_FLANAL="S") OR (g_COMM="S" AND this.w_FLGCOM="S"), .T., .F.)
        if EMPTY(this.w_APPO)
          ah_ErrorMsg("Causale magazzino associata a documento di DDT di trasferimento non definita","STOP","")
          i_retcode = 'stop'
          return
        else
          this.w_FLCASC = " "
          this.w_F2CASC = " "
          this.w_CAUMA2 = " "
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_APPO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = this.w_APPO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_CAUMA2 = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLCASC<>"-"
            ah_ErrorMsg("Causale magazzino principale incongruente (deve scaricare il mag. di prelievo)","STOP","")
            i_retcode = 'stop'
            return
          else
            if EMPTY(this.w_CAUMA2)
              ah_ErrorMsg("Causale magazzino collegata inesistente","STOP","")
              i_retcode = 'stop'
              return
            else
              * --- Read from CAM_AGAZ
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CMFLCASC"+;
                  " from "+i_cTable+" CAM_AGAZ where ";
                      +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMA2);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CMFLCASC;
                  from (i_cTable) where;
                      CMCODICE = this.w_CAUMA2;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_F2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_F2CASC<>"+"
                ah_ErrorMsg("Causale magazzino collegata incongruente (deve caricare il mag. terzista)","STOP","")
                i_retcode = 'stop'
                return
              endif
            endif
          endif
        endif
        * --- Crea tabella Temporanea dati 
        * --- Create temporary table TMPMODL
        i_nIdx=cp_AddTableDef('TMPMODL') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\COLA\EXE\QUERY\GSCOTBGL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPMODL_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Controlla selezioni
        SELECT (this.NC)
        GO TOP
        COUNT FOR (XCHK=1 OR NVL(OLQTAPRE,0)>0) AND NOT EMPTY(NVL(OLCODODL,"")) AND NVL(OLQTAMOV,0)>0 TO this.w_nRecSel
        if this.w_nRecSel>0
          if this.oParentObject.w_PPCHKPRE="S" OR this.oParentObject.w_PPCHKWIP="S"
            * --- Calcola Disponibilita' Effettiva Componenti
            ah_Msg("Lettura saldi esistenza/disponibilit�...",.T.)
            * --- Calcola La disponibilita' degli Articoli di Tipo C/Lavori eo Interni per i Mag.Nettificabili
            vq_exec("..\COLA\EXE\QUERY\GSCOSBGT.VQR", this,"SaldiArt")
            if this.oParentObject.w_PPCHKWIP="S"
              * --- Elenco Magazzini Nettificabili
              vq_exec("..\COLA\EXE\QUERY\GSCO_QMN.VQR", this,"MagaNett")
            endif
          endif
          this.w_nRecSel = 0
          this.w_nRecEla = 0
          this.w_LNumErr = 0
          * --- Crea il File delle Messaggistiche di Errore
          CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
          * --- Legge cursore di selezione ...
          * --- Cicla sui codici selezionati ...
          SELECT (this.NC)
          GO TOP
          SCAN FOR (XCHK=1 OR NVL(OLQTAPRE,0)>0) AND NOT EMPTY(NVL(OLCODODL,"")) AND NVL(OLQTAMOV,0)>0
          * --- Legge dati di interesse ....
          this.w_CODODL = OLCODODL
          this.w_ROWNUM = CPROWNUM
          this.w_CODICE = OLCODICE
          this.w_CODART = NVL(OLCODART, SPACE(15))
          this.w_DINRIC = CP_TODATE(OLDATRIC)
          this.w_DTRIC = CP_TODATE(OLDATRIC)
          this.w_CODMAG = NVL(OLCODMAG, SPACE(5))
          this.w_MAGPRE = NVL(OLMAGPRE, SPACE(5))
          this.w_MAGWIP = NVL(OLMAGWIP, SPACE(5))
          this.w_COFOR = NVL(OLTCOFOR, SPACE(15))
          this.w_PDSERDOC = NVL(PDSERDOC, SPACE(10))
          if NOT EMPTY(this.w_COFOR)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANMAGTER"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC("F");
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_COFOR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANMAGTER;
                from (i_cTable) where;
                    ANTIPCON = "F";
                    and ANCODICE = this.w_COFOR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MAGWIP = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Aggiorna magazzino WIP
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_MAGWIP),'ODL_DETT','OLMAGWIP');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                     )
            else
              update (i_cTable) set;
                  OLMAGWIP = this.w_MAGWIP;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_CODODL;
                  and CPROWNUM = this.w_ROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_UNMIS = NVL(OLUNIMIS, "   ")
          this.w_CODATT = NVL(OLTCOATT, SPACE(15))
          this.w_COCEN = NVL(OLTCOCEN, SPACE(15))
          this.w_VOCEN = NVL(OLTVOCEN, SPACE(15))
          this.w_CODCOM = NVL(OLTCOMME, SPACE(15))
          this.w_QTAODL = NVL(OLQTAMOV, 0)
          this.w_QTAOD1 = NVL(OLQTAUM1, 0)
          this.w_QTAPRE = NVL(OLQTAPRE, 0)
          this.w_QTAPR1 = NVL(OLQTAPRE, 0)
          if this.w_QTAODL<>this.w_QTAOD1
            this.w_QTAPR1 = IIF(this.w_QTAPRE=this.w_QTAODL, this.w_QTAOD1, cp_ROUND((this.w_QTAPRE * this.w_QTAOD1) / this.w_QTAODL,3))
          endif
          this.w_FLEVAS = IIF(XCHK=1, "S", " ")
          this.w_PDORDINE = this.w_COFOR
          * --- Azione ...
          this.w_nRecSel = this.w_nRecSel + 1
          do case
            case EMPTY(this.w_COFOR)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice fornitore C/Lavoro non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_CODART)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice articolo non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_MAGPRE)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = Ah_Msgformat("Codice magazzino prelievo non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case EMPTY(this.w_MAGWIP)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Codice magazzino terzista non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_OBBLANAL=.T. AND EMPTY(this.w_VOCEN)
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Voce di costo/ricavo non definita")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_OBBLANAL=.T. AND EMPTY(this.w_CODCOM) AND g_COMM="S" AND this.w_FLGCOM="S"
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("commessa non definita")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_OBBLANAL=.T. AND EMPTY(this.w_COCEN) AND g_PERCCR="S" AND this.w_FLANAL="S"
              this.w_LOggErr = this.w_CODODL
              this.w_LErrore = ah_Msgformat("Elaborazione codice OCL non eseguita (%1)", ALLTRIM(this.w_CODICE) )
              this.w_LTesMes = ah_Msgformat("Centro di costo non definito")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            otherwise
              * --- Generazione OCL
              if this.oParentObject.w_PPCHKWIP="S" AND this.w_QTAPR1<>0
                * --- Se Attivo il Check Disponibilita' sul Magazzino Terzista Trasferisce solo per la Qta Effettivamente necessaria (Disponibilita)
                if USED("SaldiArt") AND USED("MagaNett")
                  * --- Verifica se il Magazzino e' Nettificabile, altrimenti ignora controlli su Disponibilita'
                  SELECT MagaNett
                  GO TOP
                  LOCATE FOR MGCODMAG=this.w_MAGWIP
                  if FOUND()
                    SELECT SaldiArt
                    GO TOP
                    LOCATE FOR CODART=this.w_CODART AND CODMAG=this.w_MAGWIP
                    if ! FOUND()
                      * --- Crea il Magazzino
                      INSERT INTO SaldiArt (CODART, CODMAG, ESISTE, DISPON, SCOMIN) VALUES (this.w_CODART, this.w_MAGWIP, 0, 0, 0)
                    endif
                    if NVL(MGDISMAG,"")="S"
                      * --- Magazzino terzista NETTIFICABILE
                      * --- La query GSCOSBGT legge i saldi prima che l'impegno venga spostato dal magazzino di prelievo a quello del terzista 
                      *     (lo fa il batch GSCO_BGD), perch� il risultato ottenuto dalla verifica disponibilit� sia corretto simulo qui lo spostamento.
                      *     Sarebbe pi� corretto usare qta movimentata (QTAMOV) invece di qta prelevata (QTAPRE), uso QTAPRE perch� in realt�
                      *     qui � indifferente (lo spostamento � fittizio sul cursore, la insert su SALDIART non � qui).
                      *     Attenzione, se il trasferimento � gi� stato effettuato (Magazzino ODL=Magazzino Terzista) 
                      *     questa parte non deve essere tenuta presente.
                      if this.w_CODMAG<>this.w_MAGWIP
                        REPLACE DISPON WITH NVL(DISPON, 0) - this.w_QTAOD1
                        riga=recno()
                        SELECT SaldiArt
                        GO TOP
                        LOCATE FOR CODART=this.w_CODART AND CODMAG=this.w_CODMAG
                        if FOUND()
                          REPLACE DISPON WITH NVL(DISPON, 0) + this.w_QTAPR1
                        endif
                        GO riga
                      endif
                      this.w_APPO1 = NVL(DISPON, 0) && - NVL(SCOMIN,0)
                      if this.w_APPO1 < 0
                        if ABS(this.w_APPO1) <= this.w_QTAPR1
                          this.w_QTAPR1 = ABS(this.w_APPO1)
                          this.w_QTAPRE = this.w_QTAPR1
                          if this.w_QTAODL<>this.w_QTAOD1 AND this.w_QTAOD1<>0
                            this.w_QTAPRE = cp_ROUND((this.w_QTAPR1 * this.w_QTAODL) / this.w_QTAOD1,3)
                          endif
                        else
                          this.w_QTAPRE = this.w_QTAPR1
                          if this.w_QTAODL<>this.w_QTAOD1 AND this.w_QTAOD1<>0
                            this.w_QTAPRE = cp_ROUND((this.w_QTAPR1 * this.w_QTAODL) / this.w_QTAOD1,3)
                          endif
                        endif
                      else
                        this.w_QTAPR1 = 0
                        this.w_QTAPRE = 0
                      endif
                      REPLACE ESISTE WITH NVL(ESISTE, 0) + this.w_QTAPR1, DISPON WITH NVL(DISPON, 0) + this.w_QTAPR1
                    else
                      * --- Magazzino terzista NON NETTIFICABILE
                      * --- Select from gsco1qmn
                      do vq_exec with 'gsco1qmn',this,'_Curs_gsco1qmn','',.f.,.t.
                      if used('_Curs_gsco1qmn')
                        select _Curs_gsco1qmn
                        locate for 1=1
                        do while not(eof())
                        this.w_QTASAL = NVL(OLQTASAL,0)
                          select _Curs_gsco1qmn
                          continue
                        enddo
                        use
                      endif
                      SELECT SaldiArt
                      GO TOP
                      LOCATE FOR CODART=this.w_CODART AND CODMAG=this.w_MAGWIP
                      * --- Se la qta richiesta � maggiore dell'esistenza allora trasferisco...
                      if NVL(ESISTE,0) - this.w_QTASAL>=0
                        this.w_QTAPR1 = 0
                        this.w_QTAPRE = 0
                      endif
                      REPLACE ESISTE WITH NVL(ESISTE, 0)+this.w_QTAPR1, DISPON WITH NVL(DISPON, 0)+this.w_QTAPR1
                    endif
                  endif
                endif
              endif
              if this.oParentObject.w_PPCHKPRE="S" AND this.w_QTAPR1<>0
                * --- Se Attivo il Check Disponibilita' sul Magazzino Prelievo verifica Qta Max Prelevabile (Esistenza)
                this.w_OLDPR1 = this.w_QTAPR1
                if USED("SaldiArt")
                  SELECT SaldiArt
                  GO TOP
                  LOCATE FOR CODART=this.w_CODART AND CODMAG=this.w_MAGPRE
                  if FOUND()
                    if NVL(ESISTE, 0)>0
                      * --- Massimo Prelevabile
                      this.w_APPO1 = MIN(this.w_QTAPR1, NVL(ESISTE, 0))
                      if this.w_APPO1<>this.w_QTAPR1
                        this.w_QTAPR1 = this.w_APPO1
                        this.w_QTAPRE = this.w_QTAPR1
                        if this.w_QTAODL<>this.w_QTAOD1 AND this.w_QTAOD1<>0
                          this.w_QTAPRE = cp_ROUND((this.w_QTAPR1 * this.w_QTAODL) / this.w_QTAOD1,3)
                        endif
                      endif
                      * --- Diminuisce esistenza e disponibilita'
                      REPLACE ESISTE WITH NVL(ESISTE, 0) - this.w_QTAPR1, DISPON WITH NVL(DISPON, 0) - this.w_QTAPR1
                    else
                      this.w_QTAPRE = 0
                      this.w_QTAPR1 = 0
                    endif
                  else
                    this.w_QTAPRE = 0
                    this.w_QTAPR1 = 0
                  endif
                endif
                * --- Se la Qta effettivamente prelevabile e' minore di quella programmata, toglie il Flag evaso
                this.w_FLEVAS = IIF(this.w_QTAPR1 < this.w_OLDPR1, " ", this.w_FLEVAS)
              endif
              * --- Insert into TMPMODL
              i_nConn=i_TableProp[this.TMPMODL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMODL_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPMODL_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PDSERIAL"+",PDROWNUM"+",PDCODICE"+",PDCODART"+",PDDATEVA"+",PDCODMAG"+",PDTIPCON"+",PDCODCON"+",PDUMORDI"+",PDQTAORD"+",PDQTAUM1"+",PDCODCOM"+",PDTIPATT"+",PDCODATT"+",PDCODCEN"+",PDVOCCOS"+",PDDATORD"+",PDMAGPRE"+",PDMAGWIP"+",PDQTAMOV"+",PDQTAMO1"+",PDFLEVAS"+",PDORDINE"+",PDSERDOC"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODODL),'TMPMODL','PDSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'TMPMODL','PDROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'TMPMODL','PDCODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'TMPMODL','PDCODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DTRIC),'TMPMODL','PDDATEVA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'TMPMODL','PDCODMAG');
                +","+cp_NullLink(cp_ToStrODBC("F"),'TMPMODL','PDTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_COFOR),'TMPMODL','PDCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_UNMIS),'TMPMODL','PDUMORDI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAPRE),'TMPMODL','PDQTAORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAPR1),'TMPMODL','PDQTAUM1');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'TMPMODL','PDCODCOM');
                +","+cp_NullLink(cp_ToStrODBC("A"),'TMPMODL','PDTIPATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'TMPMODL','PDCODATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_COCEN),'TMPMODL','PDCODCEN');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VOCEN),'TMPMODL','PDVOCCOS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DINRIC),'TMPMODL','PDDATORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MAGPRE),'TMPMODL','PDMAGPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MAGWIP),'TMPMODL','PDMAGWIP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAODL),'TMPMODL','PDQTAMOV');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTAOD1),'TMPMODL','PDQTAMO1');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FLEVAS),'TMPMODL','PDFLEVAS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PDORDINE),'TMPMODL','PDORDINE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PDSERDOC),'TMPMODL','PDSERDOC');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_CODODL,'PDROWNUM',this.w_ROWNUM,'PDCODICE',this.w_CODICE,'PDCODART',this.w_CODART,'PDDATEVA',this.w_DTRIC,'PDCODMAG',this.w_CODMAG,'PDTIPCON',"F",'PDCODCON',this.w_COFOR,'PDUMORDI',this.w_UNMIS,'PDQTAORD',this.w_QTAPRE,'PDQTAUM1',this.w_QTAPR1,'PDCODCOM',this.w_CODCOM)
                insert into (i_cTable) (PDSERIAL,PDROWNUM,PDCODICE,PDCODART,PDDATEVA,PDCODMAG,PDTIPCON,PDCODCON,PDUMORDI,PDQTAORD,PDQTAUM1,PDCODCOM,PDTIPATT,PDCODATT,PDCODCEN,PDVOCCOS,PDDATORD,PDMAGPRE,PDMAGWIP,PDQTAMOV,PDQTAMO1,PDFLEVAS,PDORDINE,PDSERDOC &i_ccchkf. );
                   values (;
                     this.w_CODODL;
                     ,this.w_ROWNUM;
                     ,this.w_CODICE;
                     ,this.w_CODART;
                     ,this.w_DTRIC;
                     ,this.w_CODMAG;
                     ,"F";
                     ,this.w_COFOR;
                     ,this.w_UNMIS;
                     ,this.w_QTAPRE;
                     ,this.w_QTAPR1;
                     ,this.w_CODCOM;
                     ,"A";
                     ,this.w_CODATT;
                     ,this.w_COCEN;
                     ,this.w_VOCEN;
                     ,this.w_DINRIC;
                     ,this.w_MAGPRE;
                     ,this.w_MAGWIP;
                     ,this.w_QTAODL;
                     ,this.w_QTAOD1;
                     ,this.w_FLEVAS;
                     ,this.w_PDORDINE;
                     ,this.w_PDSERDOC;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
          endcase
          * --- Aggiornamento pegging
          this.w_PECODRIC = this.w_CODART
          this.w_PECODODL = " Magazz.: "+this.w_MAGWIP
          this.w_PGCODODL = " Magazz.: "+this.w_MAGPRE
          this.w_DPCODODL = this.w_CODODL
          this.w_PEQTAUM1 = this.w_QTAOD1
          this.w_PECODART = this.w_CODART
          this.w_PETIPRIF = "M"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT (this.NC)
          ENDSCAN
          vq_exec("..\COLA\EXE\QUERY\GSCO_BGT.VQR",this,"GENEORDI")
          * --- Elimina Tabella temporanea
          * --- Drop temporary table TMPMODL
          i_nIdx=cp_GetTableDefIdx('TMPMODL')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPMODL')
          endif
          * --- Generazione OCL
          ah_Msg("Generazione ordini a terzisti...",.T.)
          this.w_MVTIPDOC = this.oParentObject.w_PPTIPDOC
          this.w_MVDATDOC = this.oParentObject.w_PPDATDOC
          this.w_MVDATDIV = this.oParentObject.w_PPDATDIV
          this.w_MVNUMDOC = this.oParentObject.w_PPNUMDOC
          this.w_MVALFDOC = this.oParentObject.w_PPALFDOC
          this.w_MVPRD = iif(this.oParentObject.w_FLPDOC<>"S",this.oParentObject.w_PPPRD,"DV")
          this.w_MVPRP = "NN"
          this.w_MVFLVEAC = this.oParentObject.w_PPFLVEAC
          this.w_MVEMERIC = this.oParentObject.w_PPEMERIC
          this.w_MVCLADOC = this.oParentObject.w_PPCLADOC
          this.w_MVFLINTE = this.oParentObject.w_PPFLINTE
          this.w_MVTCAMAG = this.oParentObject.w_PPTCAMAG
          this.w_MVTFRAGG = this.oParentObject.w_PPTFRAGG
          this.w_MVCAUCON = this.oParentObject.w_PPCAUCON
          this.w_NUMSCO = this.oParentObject.w_PPNUMSCO
          this.w_MVFLACCO = this.oParentObject.w_PPFLACCO
          this.w_MVFLPROV = this.oParentObject.w_PPFLPROV
          this.w_MVSERIAL = SPACE(10)
          this.w_MVTIPCON = "F"
          this.w_MVCODESE = g_CODESE
          this.w_MVNUMEST = 0
          this.w_MVALFEST = Space(10)
          this.w_MVANNPRO = this.oParentObject.w_PPANNPRO
          this.w_MVDATREG = this.w_MVDATDOC
          this.w_MVDATCIV = this.w_MVDATDOC
          this.w_MVNUMREG = 0
          this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
          this.w_MVANNDOC = this.oParentObject.w_PPANNDOC
          * --- Lancia Batch di Generazione Ordini a Terzisti
          do GSCO_BGD with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_oPart = this.w_oMess.AddMsgPartNL("Elaborazione terminata%0N.%1 documenti generati%0N.%2 records elaborati%0su %3 records selezionati")
          this.w_oPart.AddParam(alltrim(str(this.w_nRecDoc,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecEla,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecSel,5,0)))     
          if USED("MessErr") AND this.w_LNumErr>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?")
            this.w_oPart.AddParam(alltrim(str(this.w_LNumErr,5,0)))     
            if this.w_oMess.ah_YesNo()
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
            endif
          else
            this.w_oMess.ah_ErrorMsg()
          endif
          * --- -- Controllo matricole/lotti/ubicazioni
          if USED("MATRERRO")
            if reccount("MATRERRO")>0
              if ah_YesNo("Alcuni documenti/movimenti di magazzino necessitano di un aggiornamento differito%0Desideri la stampa dell'elenco dei movimenti?")
                SELECT * FROM MATRERRO INTO CURSOR __TMP__
                CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SML.FRX", " ", this)
              endif
            endif
          endif
          * --- Chiusura Cursori
          if USED("MATRERRO")
            use in MATRERRO
          endif
          if USED("SaldiArt")
            use in SaldiArt
          endif
          if USED("MessErr")
            SELECT MessErr
            USE
          endif
          if USED("MagaNett")
            SELECT MagaNett
            USE
          endif
          if USED("__TMP__")
            SELECT __TMP__
            USE
          endif
          if USED("GENEORDI")
            SELECT GENEORDI
            USE
          endif
          * --- Refresh nuovo Progressivo e Zoom
          this.Padre.NotifyEvent("NewProg")     
          this.Padre.NotifyEvent("Interroga")     
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare","!","")
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    * --- Scrive LOG
    if EMPTY(this.w_LTesMes)
      this.w_LTesMes = "Message()= "+message()
    endif
    INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
    (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LTesMes)
    this.w_LTesMes = ""
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Pegging
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLCOMM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_PECODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLCOMM;
        from (i_cTable) where;
            ARCODART = this.w_PECODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PEFLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
    * --- Select from GSDB_BAG
    do vq_exec with 'GSDB_BAG',this,'_Curs_GSDB_BAG','',.f.,.t.
    if used('_Curs_GSDB_BAG')
      select _Curs_GSDB_BAG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDB_BAG.PESERIAL
      this.w_PEQTAABB = _Curs_GSDB_BAG.PEQTAABB
      this.w_PECODCOM = _Curs_GSDB_BAG.PECODCOM
      this.w_PERIFTRS = _Curs_GSDB_BAG.PERIFTRS
      this.w_PESERORD = _Curs_GSDB_BAG.PERIFODL
      this.w_PERIGORD = _Curs_GSDB_BAG.PERIGORD
      this.w_PEODLORI = _Curs_GSDB_BAG.PEODLORI
      if this.w_PEQTAUM1>0
        if this.w_PEQTAUM1 >= this.w_PEQTAABB
          if this.w_PGCODODL=" Magazz"
            * --- Write into PEG_SELI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                  +i_ccchkf ;
              +" where ";
                  +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                     )
            else
              update (i_cTable) set;
                  PESERODL = this.w_PECODODL;
                  &i_ccchkf. ;
               where;
                  PESERIAL = this.w_PESERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_PEQTAUM1 = this.w_PEQTAUM1 - this.w_PEQTAABB
          endif
        else
          if this.w_PGCODODL=" Magazz"
            * --- Write into PEG_SELI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PEQTAABB =PEQTAABB- "+cp_ToStrODBC(this.w_PEQTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                     )
            else
              update (i_cTable) set;
                  PEQTAABB = PEQTAABB - this.w_PEQTAUM1;
                  &i_ccchkf. ;
               where;
                  PESERIAL = this.w_PESERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_PROGR = max(this.w_PROGR + 1,1)
            this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
            * --- Insert into PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+",PERIFTRS"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
              +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PEODLORI),'PEG_SELI','PEODLORI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PECODCOM),'PEG_SELI','PECODCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PERIFODL',this.w_PESERORD,'PERIGORD',this.w_PERIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_PEODLORI,'PECODCOM',this.w_PECODCOM,'PECODRIC',this.w_PECODRIC,'PERIFTRS',this.w_PERIFTRS)
              insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC,PERIFTRS &i_ccchkf. );
                 values (;
                   this.w_PESERIAL;
                   ,this.w_PECODODL;
                   ,"M";
                   ,this.w_PESERORD;
                   ,this.w_PERIGORD;
                   ,this.w_PEQTAUM1;
                   ,this.w_PEODLORI;
                   ,this.w_PECODCOM;
                   ,this.w_PECODRIC;
                   ,this.w_PERIFTRS;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            this.w_PEQTAUM1 = 0
          endif
        endif
      endif
        select _Curs_GSDB_BAG
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='MA_COSTI'
    this.cWorkTables[4]='ODL_DETT'
    this.cWorkTables[5]='ODL_MAST'
    this.cWorkTables[6]='PAR_PROD'
    this.cWorkTables[7]='PAR_RIOR'
    this.cWorkTables[8]='SALDIART'
    this.cWorkTables[9]='MAGAZZIN'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='*TMPMODL'
    this.cWorkTables[12]='CONTI'
    this.cWorkTables[13]='PEG_SELI'
    this.cWorkTables[14]='ART_ICOL'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_gsco1qmn')
      use in _Curs_gsco1qmn
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_GSDB_BAG')
      use in _Curs_GSDB_BAG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
