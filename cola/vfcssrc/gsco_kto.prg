* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_kto                                                        *
*              Tracciabilit� ODL                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-17                                                      *
* Last revis.: 2015-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsco_kto",oParentObject))

* --- Class definition
define class tgsco_kto as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 793
  Height = 534+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-01"
  HelpContextID=186014359
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=86

  * --- Constant Properties
  _IDX = 0
  CAM_AGAZ_IDX = 0
  CONTI_IDX = 0
  ODL_MAST_IDX = 0
  MAGAZZIN_IDX = 0
  BUSIUNIT_IDX = 0
  KEY_ARTI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  ART_ICOL_IDX = 0
  DOC_MAST_IDX = 0
  cPrg = "gsco_kto"
  cComment = "Tracciabilit� ODL"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPMAG = space(5)
  w_DESDOC = space(35)
  w_CODMAG = space(5)
  w_NUMINI = 0
  w_SERIE1 = space(10)
  w_NUMFIN = 0
  w_SERIE2 = space(10)
  w_DOCINI = ctod('  /  /  ')
  o_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_NUMREI = 0
  w_NUMREF = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODCOM = space(15)
  w_DESCAN = space(30)
  w_CODATT = space(15)
  w_DESATT = space(30)
  w_DBCODINI = space(20)
  w_DESDISI = space(40)
  w_DBCODFIN = space(20)
  w_DESDISF = space(40)
  w_FAMAINI = space(5)
  w_DESFAMAI = space(35)
  w_FAMAFIN = space(5)
  w_DESFAMAF = space(35)
  w_GRUINI = space(5)
  w_DESGRUI = space(35)
  w_GRUFIN = space(5)
  w_DESGRUF = space(35)
  w_CATINI = space(5)
  w_DESCATI = space(35)
  w_CATFIN = space(5)
  w_DESCATF = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_TIPOARTI = space(2)
  w_OLCODINI = space(15)
  w_OLCODFIN = space(15)
  w_DATIODL = ctod('  /  /  ')
  w_DATFODL = ctod('  /  /  ')
  w_DATFINE = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_SELODL = space(1)
  w_TIPATT = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_AZIENDA = space(5)
  w_gestbu = space(1)
  w_LEVELTMP = 0
  w_TMPTYPE = space(3)
  w_ST = space(1)
  w_PV = space(1)
  w_LEVEL = 0
  w_CLADOC = space(2)
  w_FLVEAC = space(1)
  w_TYPE = space(3)
  w_STATO = space(10)
  w_PROVE = space(10)
  w_ROWNUM = 0
  w_CODODL2 = space(10)
  w_ST1 = space(1)
  w_STATOR = space(15)
  w_FirstTime = .F.
  w_COD3 = space(41)
  w_RIF2 = 0
  w_CODRIC = space(20)
  w_CADESAR = space(40)
  w_QTAORD = 0
  w_MAGAZ = space(5)
  w_DATEMS = ctod('  /  /  ')
  w_DATEVF = ctod('  /  /  ')
  w_CAUMAG = space(5)
  w_CODODL1 = space(10)
  w_MAGAZ = space(5)
  w_FORNIT = space(15)
  w_QTASCA = 0
  w_QTARES = 0
  w_NUMREG = 0
  w_DATEVA = ctod('  /  /  ')
  w_FORNIT = space(15)
  w_RIF = space(15)
  w_RIF3 = space(15)
  w_QTAORD = 0
  w_QTAEVA = 0
  w_UNIMIS1 = space(3)
  w_CODODL_ = space(15)
  w_CPROWNUM = 0
  w_TREEV = .NULL.
  w_StatoODL = .NULL.
  w_ProvODLF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_ktoPag1","gsco_kto",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsco_ktoPag2","gsco_kto",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tree-View")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPMAG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEV = this.oPgFrm.Pages(2).oPag.TREEV
    this.w_StatoODL = this.oPgFrm.Pages(2).oPag.StatoODL
    this.w_ProvODLF = this.oPgFrm.Pages(2).oPag.ProvODLF
    DoDefault()
    proc Destroy()
      this.w_TREEV = .NULL.
      this.w_StatoODL = .NULL.
      this.w_ProvODLF = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ODL_MAST'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='BUSIUNIT'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='FAM_ARTI'
    this.cWorkTables[10]='GRUMERC'
    this.cWorkTables[11]='CATEGOMO'
    this.cWorkTables[12]='ART_ICOL'
    this.cWorkTables[13]='DOC_MAST'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      GSCO_BTO("Tree-View")
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPMAG=space(5)
      .w_DESDOC=space(35)
      .w_CODMAG=space(5)
      .w_NUMINI=0
      .w_SERIE1=space(10)
      .w_NUMFIN=0
      .w_SERIE2=space(10)
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_NUMREI=0
      .w_NUMREF=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODCOM=space(15)
      .w_DESCAN=space(30)
      .w_CODATT=space(15)
      .w_DESATT=space(30)
      .w_DBCODINI=space(20)
      .w_DESDISI=space(40)
      .w_DBCODFIN=space(20)
      .w_DESDISF=space(40)
      .w_FAMAINI=space(5)
      .w_DESFAMAI=space(35)
      .w_FAMAFIN=space(5)
      .w_DESFAMAF=space(35)
      .w_GRUINI=space(5)
      .w_DESGRUI=space(35)
      .w_GRUFIN=space(5)
      .w_DESGRUF=space(35)
      .w_CATINI=space(5)
      .w_DESCATI=space(35)
      .w_CATFIN=space(5)
      .w_DESCATF=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOARTI=space(2)
      .w_OLCODINI=space(15)
      .w_OLCODFIN=space(15)
      .w_DATIODL=ctod("  /  /  ")
      .w_DATFODL=ctod("  /  /  ")
      .w_DATFINE=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_SELODL=space(1)
      .w_TIPATT=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_AZIENDA=space(5)
      .w_gestbu=space(1)
      .w_LEVELTMP=0
      .w_TMPTYPE=space(3)
      .w_ST=space(1)
      .w_PV=space(1)
      .w_LEVEL=0
      .w_CLADOC=space(2)
      .w_FLVEAC=space(1)
      .w_TYPE=space(3)
      .w_STATO=space(10)
      .w_PROVE=space(10)
      .w_ROWNUM=0
      .w_CODODL2=space(10)
      .w_ST1=space(1)
      .w_STATOR=space(15)
      .w_FirstTime=.f.
      .w_COD3=space(41)
      .w_RIF2=0
      .w_CODRIC=space(20)
      .w_CADESAR=space(40)
      .w_QTAORD=0
      .w_MAGAZ=space(5)
      .w_DATEMS=ctod("  /  /  ")
      .w_DATEVF=ctod("  /  /  ")
      .w_CAUMAG=space(5)
      .w_CODODL1=space(10)
      .w_MAGAZ=space(5)
      .w_FORNIT=space(15)
      .w_QTASCA=0
      .w_QTARES=0
      .w_NUMREG=0
      .w_DATEVA=ctod("  /  /  ")
      .w_FORNIT=space(15)
      .w_RIF=space(15)
      .w_RIF3=space(15)
      .w_QTAORD=0
      .w_QTAEVA=0
      .w_UNIMIS1=space(3)
      .w_CODODL_=space(15)
      .w_CPROWNUM=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_TIPMAG))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODMAG))
          .link_1_3('Full')
        endif
        .w_NUMINI = 0
        .w_SERIE1 = ''
        .w_NUMFIN = 999999999999999
        .w_SERIE2 = ''
          .DoRTCalc(8,8,.f.)
        .w_DOCFIN = .w_DOCINI
        .w_NUMREI = 1
        .w_NUMREF = 999999999999999
          .DoRTCalc(12,12,.f.)
        .w_DATFIN = .w_DATINI
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODCOM))
          .link_1_15('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_CODATT))
          .link_1_17('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_DBCODINI))
          .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_DBCODFIN))
          .link_1_21('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_23('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_25('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_GRUINI))
          .link_1_27('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_29('Full')
        endif
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_CATINI))
          .link_1_31('Full')
        endif
        .DoRTCalc(31,32,.f.)
        if not(empty(.w_CATFIN))
          .link_1_33('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(35,36,.f.)
        if not(empty(.w_OLCODINI))
          .link_1_37('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_OLCODFIN))
          .link_1_38('Full')
        endif
          .DoRTCalc(38,38,.f.)
        .w_DATFODL = .w_DATIODL
          .DoRTCalc(40,41,.f.)
        .w_SELODL = "T"
        .w_TIPATT = "A"
          .DoRTCalc(44,44,.f.)
        .w_CODAZI = i_codazi
        .w_AZIENDA = i_CODAZI
          .DoRTCalc(47,47,.f.)
        .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
        .w_TMPTYPE = .w_TREEV.getvar("TIPO")
        .w_ST = .w_TREEV.getvar("OLTSTATO")
        .w_PV = .w_TREEV.getvar("OLTPROVE")
        .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
        .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
        .w_FLVEAC = .w_TREEV.getvar("MVFLVEAC")
      .oPgFrm.Page2.oPag.oObj_2_8.Calculate()
      .oPgFrm.Page2.oPag.TREEV.Calculate()
        .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
        .w_STATO = iif(.w_ST='M','Suggerito',iif(.w_ST='P','Pianificato',iif(.w_ST='L',iif(.w_Type$'ODL-ODF','Lanciato','Ordinato'),iif(.w_ST='C','Confermato',iif(.w_ST='D','Da Pianificare',iif(.w_ST='S','Suggerito',iif(.w_ST='F','Finito',' ')))))))
        .w_PROVE = iif(.w_TYPE='ODF',iif(.w_PV='I','Fase interna',iif(.w_PV='L','Fase esterna','')),'')
        .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
      .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_CODODL2))
          .link_2_16('Full')
        endif
          .DoRTCalc(60,60,.f.)
        .w_STATOR = iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' ,'Confermato',''))
      .oPgFrm.Page2.oPag.oObj_2_19.Calculate()
        .w_FirstTime = .T.
        .w_COD3 = .w_TREEV.getvar("OLTCODIC ")
        .w_RIF2 = .w_TREEV.getvar("MVNUMDOC ")
        .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
        .DoRTCalc(65,65,.f.)
        if not(empty(.w_CODRIC))
          .link_2_23('Full')
        endif
          .DoRTCalc(66,66,.f.)
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_DATEMS = nvl(.w_TREEV.getvar("OLTDINRIC"),ctod('  -  -  '))
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC "),ctod('  -  -  '))
        .w_CAUMAG = .w_TREEV.getvar("MVCAUMAG")
        .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_QTASCA = nvl(.w_TREEV.getvar("OLTQTOBF"),0) 
        .w_QTARES = iif((nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0))>0,nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0)
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),ctod('  -  -  '))
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_RIF = .w_TREEV.getvar("PADRE")
        .w_RIF3 = iif(.w_type$'DIM-DIR',.w_TREEV.getvar("ardesart "),' ')
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
      .oPgFrm.Page2.oPag.StatoODL.Calculate(.w_STATO)
        .w_UNIMIS1 = nvl(.w_TREEV.getvar("OLTUNMIS"),' ')
        .w_CODODL_ = IIF(.w_type='ODL',.w_TREEV.getvar("OLCODODL"),.w_TREEV.getvar("PADRE"))
        .w_CPROWNUM = nvl(.w_TREEV.getvar("CPROWNUM"),0)
      .oPgFrm.Page2.oPag.ProvODLF.Calculate(.w_PROVE)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_80.enabled = this.oPgFrm.Page1.oPag.oBtn_1_80.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_40.enabled = this.oPgFrm.Page2.oPag.oBtn_2_40.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_DOCINI<>.w_DOCINI
            .w_DOCFIN = .w_DOCINI
        endif
        .DoRTCalc(10,12,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = .w_DATINI
        endif
        .DoRTCalc(14,38,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFODL = .w_DATIODL
        endif
        .DoRTCalc(40,47,.t.)
            .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
            .w_TMPTYPE = .w_TREEV.getvar("TIPO")
            .w_ST = .w_TREEV.getvar("OLTSTATO")
            .w_PV = .w_TREEV.getvar("OLTPROVE")
            .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
            .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
            .w_FLVEAC = .w_TREEV.getvar("MVFLVEAC")
        .oPgFrm.Page2.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page2.oPag.TREEV.Calculate()
            .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
            .w_STATO = iif(.w_ST='M','Suggerito',iif(.w_ST='P','Pianificato',iif(.w_ST='L',iif(.w_Type$'ODL-ODF','Lanciato','Ordinato'),iif(.w_ST='C','Confermato',iif(.w_ST='D','Da Pianificare',iif(.w_ST='S','Suggerito',iif(.w_ST='F','Finito',' ')))))))
            .w_PROVE = iif(.w_TYPE='ODF',iif(.w_PV='I','Fase interna',iif(.w_PV='L','Fase esterna','')),'')
            .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
            .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
          .link_2_16('Full')
        .DoRTCalc(60,60,.t.)
            .w_STATOR = iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' ,'Confermato',''))
        .oPgFrm.Page2.oPag.oObj_2_19.Calculate()
        .DoRTCalc(62,62,.t.)
            .w_COD3 = .w_TREEV.getvar("OLTCODIC ")
            .w_RIF2 = .w_TREEV.getvar("MVNUMDOC ")
            .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
          .link_2_23('Full')
        .DoRTCalc(66,66,.t.)
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_DATEMS = nvl(.w_TREEV.getvar("OLTDINRIC"),ctod('  -  -  '))
            .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC "),ctod('  -  -  '))
            .w_CAUMAG = .w_TREEV.getvar("MVCAUMAG")
            .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
            .w_QTASCA = nvl(.w_TREEV.getvar("OLTQTOBF"),0) 
            .w_QTARES = iif((nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0))>0,nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0)
            .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
            .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),ctod('  -  -  '))
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
            .w_RIF = .w_TREEV.getvar("PADRE")
            .w_RIF3 = iif(.w_type$'DIM-DIR',.w_TREEV.getvar("ardesart "),' ')
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .oPgFrm.Page2.oPag.StatoODL.Calculate(.w_STATO)
            .w_UNIMIS1 = nvl(.w_TREEV.getvar("OLTUNMIS"),' ')
            .w_CODODL_ = IIF(.w_type='ODL',.w_TREEV.getvar("OLCODODL"),.w_TREEV.getvar("PADRE"))
            .w_CPROWNUM = nvl(.w_TREEV.getvar("CPROWNUM"),0)
        .oPgFrm.Page2.oPag.ProvODLF.Calculate(.w_PROVE)
        * --- Area Manuale = Calculate
        * --- gsco_kto
        local l_obj
        l_obj= this.getctrl('StatoODL')
        if type('l_obj.tooltiptext')='C'
        l_obj.tooltiptext=iif(empty(this.w_STATO),'','StatoODL')
        endif
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page2.oPag.TREEV.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_19.Calculate()
        .oPgFrm.Page2.oPag.StatoODL.Calculate(.w_STATO)
        .oPgFrm.Page2.oPag.ProvODLF.Calculate(.w_PROVE)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_15.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_17.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oCODRIC_2_23.visible=!this.oPgFrm.Page2.oPag.oCODRIC_2_23.mHide()
    this.oPgFrm.Page2.oPag.oCADESAR_2_24.visible=!this.oPgFrm.Page2.oPag.oCADESAR_2_24.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oQTAORD_2_27.visible=!this.oPgFrm.Page2.oPag.oQTAORD_2_27.mHide()
    this.oPgFrm.Page2.oPag.oMAGAZ_2_28.visible=!this.oPgFrm.Page2.oPag.oMAGAZ_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_29.visible=!this.oPgFrm.Page2.oPag.oStr_2_29.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oDATEMS_2_31.visible=!this.oPgFrm.Page2.oPag.oDATEMS_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_33.visible=!this.oPgFrm.Page2.oPag.oStr_2_33.mHide()
    this.oPgFrm.Page2.oPag.oDATEVF_2_34.visible=!this.oPgFrm.Page2.oPag.oDATEVF_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_35.visible=!this.oPgFrm.Page2.oPag.oStr_2_35.mHide()
    this.oPgFrm.Page2.oPag.oCAUMAG_2_36.visible=!this.oPgFrm.Page2.oPag.oCAUMAG_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_38.visible=!this.oPgFrm.Page2.oPag.oStr_2_38.mHide()
    this.oPgFrm.Page2.oPag.oMAGAZ_2_39.visible=!this.oPgFrm.Page2.oPag.oMAGAZ_2_39.mHide()
    this.oPgFrm.Page2.oPag.oFORNIT_2_41.visible=!this.oPgFrm.Page2.oPag.oFORNIT_2_41.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oQTASCA_2_44.visible=!this.oPgFrm.Page2.oPag.oQTASCA_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page2.oPag.oQTARES_2_46.visible=!this.oPgFrm.Page2.oPag.oQTARES_2_46.mHide()
    this.oPgFrm.Page2.oPag.oNUMREG_2_47.visible=!this.oPgFrm.Page2.oPag.oNUMREG_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oDATEVA_2_49.visible=!this.oPgFrm.Page2.oPag.oDATEVA_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oFORNIT_2_53.visible=!this.oPgFrm.Page2.oPag.oFORNIT_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_55.visible=!this.oPgFrm.Page2.oPag.oStr_2_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_56.visible=!this.oPgFrm.Page2.oPag.oStr_2_56.mHide()
    this.oPgFrm.Page2.oPag.oRIF_2_57.visible=!this.oPgFrm.Page2.oPag.oRIF_2_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oQTAORD_2_60.visible=!this.oPgFrm.Page2.oPag.oQTAORD_2_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oQTAEVA_2_62.visible=!this.oPgFrm.Page2.oPag.oQTAEVA_2_62.mHide()
    this.oPgFrm.Page2.oPag.oUNIMIS1_2_64.visible=!this.oPgFrm.Page2.oPag.oUNIMIS1_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page2.oPag.oCODODL__2_66.visible=!this.oPgFrm.Page2.oPag.oCODODL__2_66.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_67.visible=!this.oPgFrm.Page2.oPag.oStr_2_67.mHide()
    this.oPgFrm.Page2.oPag.oCPROWNUM_2_68.visible=!this.oPgFrm.Page2.oPag.oCPROWNUM_2_68.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_8.Event(cEvent)
      .oPgFrm.Page2.oPag.TREEV.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_19.Event(cEvent)
      .oPgFrm.Page2.oPag.StatoODL.Event(cEvent)
      .oPgFrm.Page2.oPag.ProvODLF.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPMAG
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_TIPMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_TIPMAG))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oTIPMAG_1_1'),i_cWhere,'',"CAUSALI DI MAGAZZINO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_TIPMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_TIPMAG)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESDOC = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPMAG = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_3'),i_cWhere,'',"MAGAZZINI",'GSCO_SCG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_15'),i_cWhere,'',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_17'),i_cWhere,'',"ATTIVITA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODINI
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODINI_1_19'),i_cWhere,'',"CODICI DI RICERCA",'GSCO_ZAR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODFIN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODFIN_1_21'),i_cWhere,'',"CODICI DI RICERCA",'GSCO_ZAR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_23'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_25'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_27'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_29'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_31'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(_Link_.OMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_33'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(_Link_.OMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODINI
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODINI_1_37'),i_cWhere,'',"ODL",'GSCO_zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODFIN
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODFIN_1_38'),i_cWhere,'',"ODL",'GSCO_zod.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODODL2
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODODL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODODL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_CODODL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_CODODL2)
            select MVSERIAL,MVFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODODL2 = NVL(_Link_.MVSERIAL,space(10))
      this.w_ST1 = NVL(_Link_.MVFLPROV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODODL2 = space(10)
      endif
      this.w_ST1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODODL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIC
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODRIC)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIC = NVL(_Link_.CACODICE,space(20))
      this.w_CADESAR = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIC = space(20)
      endif
      this.w_CADESAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPMAG_1_1.value==this.w_TIPMAG)
      this.oPgFrm.Page1.oPag.oTIPMAG_1_1.value=this.w_TIPMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_2.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_2.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_3.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_3.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_4.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_4.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_5.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_5.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_6.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_6.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_7.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_7.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_9.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_9.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_10.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_10.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_13.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_13.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_14.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_14.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_15.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_15.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_16.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_16.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_17.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_17.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_18.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_18.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODINI_1_19.value==this.w_DBCODINI)
      this.oPgFrm.Page1.oPag.oDBCODINI_1_19.value=this.w_DBCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISI_1_20.value==this.w_DESDISI)
      this.oPgFrm.Page1.oPag.oDESDISI_1_20.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODFIN_1_21.value==this.w_DBCODFIN)
      this.oPgFrm.Page1.oPag.oDBCODFIN_1_21.value=this.w_DBCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISF_1_22.value==this.w_DESDISF)
      this.oPgFrm.Page1.oPag.oDESDISF_1_22.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_23.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_23.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_24.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_24.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_25.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_25.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_26.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_26.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_27.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_27.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_28.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_28.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_29.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_29.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_30.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_30.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_31.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_31.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_32.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_32.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_33.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_33.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_34.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_34.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODINI_1_37.value==this.w_OLCODINI)
      this.oPgFrm.Page1.oPag.oOLCODINI_1_37.value=this.w_OLCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOLCODFIN_1_38.value==this.w_OLCODFIN)
      this.oPgFrm.Page1.oPag.oOLCODFIN_1_38.value=this.w_OLCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATIODL_1_39.value==this.w_DATIODL)
      this.oPgFrm.Page1.oPag.oDATIODL_1_39.value=this.w_DATIODL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFODL_1_40.value==this.w_DATFODL)
      this.oPgFrm.Page1.oPag.oDATFODL_1_40.value=this.w_DATFODL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINE_1_41.value==this.w_DATFINE)
      this.oPgFrm.Page1.oPag.oDATFINE_1_41.value=this.w_DATFINE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_42.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_42.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oSELODL_1_43.RadioValue()==this.w_SELODL)
      this.oPgFrm.Page1.oPag.oSELODL_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODRIC_2_23.value==this.w_CODRIC)
      this.oPgFrm.Page2.oPag.oCODRIC_2_23.value=this.w_CODRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oCADESAR_2_24.value==this.w_CADESAR)
      this.oPgFrm.Page2.oPag.oCADESAR_2_24.value=this.w_CADESAR
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAORD_2_27.value==this.w_QTAORD)
      this.oPgFrm.Page2.oPag.oQTAORD_2_27.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGAZ_2_28.value==this.w_MAGAZ)
      this.oPgFrm.Page2.oPag.oMAGAZ_2_28.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oDATEMS_2_31.value==this.w_DATEMS)
      this.oPgFrm.Page2.oPag.oDATEMS_2_31.value=this.w_DATEMS
    endif
    if not(this.oPgFrm.Page2.oPag.oDATEVF_2_34.value==this.w_DATEVF)
      this.oPgFrm.Page2.oPag.oDATEVF_2_34.value=this.w_DATEVF
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUMAG_2_36.value==this.w_CAUMAG)
      this.oPgFrm.Page2.oPag.oCAUMAG_2_36.value=this.w_CAUMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGAZ_2_39.value==this.w_MAGAZ)
      this.oPgFrm.Page2.oPag.oMAGAZ_2_39.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oFORNIT_2_41.value==this.w_FORNIT)
      this.oPgFrm.Page2.oPag.oFORNIT_2_41.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page2.oPag.oQTASCA_2_44.value==this.w_QTASCA)
      this.oPgFrm.Page2.oPag.oQTASCA_2_44.value=this.w_QTASCA
    endif
    if not(this.oPgFrm.Page2.oPag.oQTARES_2_46.value==this.w_QTARES)
      this.oPgFrm.Page2.oPag.oQTARES_2_46.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMREG_2_47.value==this.w_NUMREG)
      this.oPgFrm.Page2.oPag.oNUMREG_2_47.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page2.oPag.oDATEVA_2_49.value==this.w_DATEVA)
      this.oPgFrm.Page2.oPag.oDATEVA_2_49.value=this.w_DATEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oFORNIT_2_53.value==this.w_FORNIT)
      this.oPgFrm.Page2.oPag.oFORNIT_2_53.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page2.oPag.oRIF_2_57.value==this.w_RIF)
      this.oPgFrm.Page2.oPag.oRIF_2_57.value=this.w_RIF
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAORD_2_60.value==this.w_QTAORD)
      this.oPgFrm.Page2.oPag.oQTAORD_2_60.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page2.oPag.oQTAEVA_2_62.value==this.w_QTAEVA)
      this.oPgFrm.Page2.oPag.oQTAEVA_2_62.value=this.w_QTAEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oUNIMIS1_2_64.value==this.w_UNIMIS1)
      this.oPgFrm.Page2.oPag.oUNIMIS1_2_64.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODODL__2_66.value==this.w_CODODL_)
      this.oPgFrm.Page2.oPag.oCODODL__2_66.value=this.w_CODODL_
    endif
    if not(this.oPgFrm.Page2.oPag.oCPROWNUM_2_68.value==this.w_CPROWNUM)
      this.oPgFrm.Page2.oPag.oCPROWNUM_2_68.value=this.w_CPROWNUM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN))  and not(empty(.w_DBCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODINI_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI))  and not(empty(.w_DBCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN))  and not(empty(.w_OLCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODINI_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI))  and not(empty(.w_OLCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLCODFIN_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATIODL<=.w_DATFODL or empty(.w_DATFODL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATIODL_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATIODL<=.w_DATFODL)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFODL_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DOCINI = this.w_DOCINI
    this.o_DATINI = this.w_DATINI
    return

enddefine

* --- Define pages as container
define class tgsco_ktoPag1 as StdContainer
  Width  = 789
  height = 534
  stdWidth  = 789
  stdheight = 534
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPMAG_1_1 as StdField with uid="EQVCIIEFMI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TIPMAG", cQueryName = "TIPMAG",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di Magazzino",;
    HelpContextID = 159864522,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=109, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", oKey_1_1="CMCODICE", oKey_1_2="this.w_TIPMAG"

  func oTIPMAG_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPMAG_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPMAG_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oTIPMAG_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CAUSALI DI MAGAZZINO",'',this.parent.oContained
  endproc

  add object oDESDOC_1_2 as StdField with uid="CGRVBJTKDZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 212872138,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=157, Top=32, InputMask=replicate('X',35)

  add object oCODMAG_1_3 as StdField with uid="JURKTACLPV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Magazzino",;
    HelpContextID = 159912410,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=515, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'GSCO_SCG.MAGAZZIN_VZM',this.parent.oContained
  endproc

  add object oNUMINI_1_4 as StdField with uid="IKHMXRTVNM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 112950058,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=57, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_5 as StdField with uid="CPLEKOYGGO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 256588506,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=249, Top=57, InputMask=replicate('X',10)

  func oSERIE1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_6 as StdField with uid="ZQAMWANEZG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 34503466,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=80, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_7 as StdField with uid="DHTTXQQJBV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 239811290,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=249, Top=80, InputMask=replicate('X',10)

  func oSERIE2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oDOCINI_1_9 as StdField with uid="NLSHJRFBFX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Documento di Inizio Selezione (vuota=nessuna selezione)",;
    HelpContextID = 112992714,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=455, Top=57

  func oDOCINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_10 as StdField with uid="WKYDEMVJUZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Documento di Fine Selezione (vuota=nessuna selezione)",;
    HelpContextID = 34546122,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=455, Top=80

  func oDOCFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oDATINI_1_13 as StdField with uid="QMGDPWFMND",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Registrazione Documento di Inizio Selezione (vuota=nessuna selezione)",;
    HelpContextID = 112926666,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=658, Top=57

  func oDATINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_14 as StdField with uid="VRSTSJQFDH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Registrazione Documento di Fine Selezione (vuota=nessuna selezione)",;
    HelpContextID = 34480074,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=658, Top=80

  func oDATFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oCODCOM_1_15 as StdField with uid="YLQVWTCUFO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice Commessa",;
    HelpContextID = 45224410,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=105, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"COMMESSE",'',this.parent.oContained
  endproc

  add object oDESCAN_1_16 as StdField with uid="PJHERXXTYJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43068362,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=245, Top=105, InputMask=replicate('X',30)

  add object oCODATT_1_17 as StdField with uid="CMRHBCMAKU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice Attivit�",;
    HelpContextID = 191107546,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=130, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" and not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ATTIVITA",'',this.parent.oContained
  endproc

  add object oDESATT_1_18 as StdField with uid="JRUPUPZCWV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 191048650,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=245, Top=130, InputMask=replicate('X',30)

  add object oDBCODINI_1_19 as StdField with uid="EGVKVVQLEU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DBCODINI", cQueryName = "DBCODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Articolo di inizio selezione",;
    HelpContextID = 123088513,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=108, Top=182, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODINI"

  func oDBCODINI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODINI_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODINI_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODINI_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CODICI DI RICERCA",'GSCO_ZAR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESDISI_1_20 as StdField with uid="QSEMNVVIFP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49271862,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=182, InputMask=replicate('X',40)

  add object oDBCODFIN_1_21 as StdField with uid="VCEEWLYQYH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DBCODFIN", cQueryName = "DBCODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Articolo di fine selezione",;
    HelpContextID = 95015300,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=108, Top=207, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODFIN"

  proc oDBCODFIN_1_21.mDefault
    with this.Parent.oContained
      if empty(.w_DBCODFIN)
        .w_DBCODFIN = .w_DBCODINI
      endif
    endwith
  endproc

  func oDBCODFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODFIN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODFIN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODFIN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CODICI DI RICERCA",'GSCO_ZAR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESDISF_1_22 as StdField with uid="BLVZKOKGVG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49271862,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=207, InputMask=replicate('X',40)

  add object oFAMAINI_1_23 as StdField with uid="BAUZFWPTWC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Famiglia Articolo di Inizio Selezione",;
    HelpContextID = 233599062,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=108, Top=232, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oDESFAMAI_1_24 as StdField with uid="AOYBYAMKXI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59648897,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=164, Top=232, InputMask=replicate('X',35)

  add object oFAMAFIN_1_25 as StdField with uid="GHDDKOKVIG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Famiglia Articolo di Fine Selezione",;
    HelpContextID = 121868202,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=108, Top=256, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_25.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oDESFAMAF_1_26 as StdField with uid="WOCDSQXKSA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59648900,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=164, Top=256, InputMask=replicate('X',35)

  add object oGRUINI_1_27 as StdField with uid="VYQYSTJFCH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Gruppo Merceologico di Inizio Selezione",;
    HelpContextID = 112918170,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=108, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oDESGRUI_1_28 as StdField with uid="ZABORREMDH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92460086,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=164, Top=281, InputMask=replicate('X',35)

  add object oGRUFIN_1_29 as StdField with uid="BKTBROMCMD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Gruppo Merceologico di Fine Selezione",;
    HelpContextID = 34471578,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=108, Top=304, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_29.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oDESGRUF_1_30 as StdField with uid="PYGQUHDJMJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92460086,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=164, Top=304, InputMask=replicate('X',35)

  add object oCATINI_1_31 as StdField with uid="WQNLFQEXDD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Categoria Omogenea di Inizio Selezione",;
    HelpContextID = 112926682,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=108, Top=330, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oDESCATI_1_32 as StdField with uid="ZFWYARIWSP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 57594934,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=164, Top=330, InputMask=replicate('X',35)

  add object oCATFIN_1_33 as StdField with uid="WHQQEZYTLE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Categoria Omogenea di Fine Selezione",;
    HelpContextID = 34480090,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=108, Top=353, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_33.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oDESCATF_1_34 as StdField with uid="SXSGGSHCII",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 57594934,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=164, Top=353, InputMask=replicate('X',35)

  add object oOLCODINI_1_37 as StdField with uid="DXTOMLLUGU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_OLCODINI", cQueryName = "OLCODINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ODL Inizio Selezione",;
    HelpContextID = 123085777,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=410, cSayPict='"999999999999999"', cGetPict='"999999999999999"', InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODINI"

  func oOLCODINI_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODINI_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODINI_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODINI_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ODL",'GSCO_zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oOLCODFIN_1_38 as StdField with uid="RYBOKFOTRV",rtseq=37,rtrep=.f.,;
    cFormVar = "w_OLCODFIN", cQueryName = "OLCODFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ODL di Fine Selezione",;
    HelpContextID = 95018036,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=109, Top=435, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODFIN"

  proc oOLCODFIN_1_38.mDefault
    with this.Parent.oContained
      if empty(.w_OLCODFIN)
        .w_OLCODFIN = .w_OLCODINI
      endif
    endwith
  endproc

  func oOLCODFIN_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODFIN_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODFIN_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODFIN_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ODL",'GSCO_zod.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oDATIODL_1_39 as StdField with uid="JTIGDSWUON",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DATIODL", cQueryName = "DATIODL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data  di Inizio Selezione (vuota=nessuna selezione)",;
    HelpContextID = 72671286,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=375, Top=410

  func oDATIODL_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATIODL<=.w_DATFODL or empty(.w_DATFODL))
    endwith
    return bRes
  endfunc

  add object oDATFODL_1_40 as StdField with uid="HBIGVLINRF",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DATFODL", cQueryName = "DATFODL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di Fine Selezione (vuota=nessuna selezione)",;
    HelpContextID = 72474678,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=375, Top=435

  func oDATFODL_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATIODL<=.w_DATFODL)
    endwith
    return bRes
  endfunc

  add object oDATFINE_1_41 as StdField with uid="FAXQJUJEXU",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DATFINE", cQueryName = "DATFINE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 233955382,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=582, Top=410

  add object oDATFIN1_1_42 as StdField with uid="QWARCSPHHS",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 34480074,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=582, Top=435


  add object oSELODL_1_43 as StdCombo with uid="HACJPEBNXC",rtseq=42,rtrep=.f.,left=109,top=461,width=84,height=21;
    , ToolTipText = "Selezione Stato ODL";
    , HelpContextID = 72719066;
    , cFormVar="w_SELODL",RowSource=""+"Suggeriti,"+"Pianificati,"+"Lanciati,"+"Finiti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELODL_1_43.RadioValue()
    return(iif(this.value =1,"M",;
    iif(this.value =2,"P",;
    iif(this.value =3,"L",;
    iif(this.value =4,"F",;
    iif(this.value =5,"T",;
    space(1)))))))
  endfunc
  func oSELODL_1_43.GetRadio()
    this.Parent.oContained.w_SELODL = this.RadioValue()
    return .t.
  endfunc

  func oSELODL_1_43.SetRadio()
    this.Parent.oContained.w_SELODL=trim(this.Parent.oContained.w_SELODL)
    this.value = ;
      iif(this.Parent.oContained.w_SELODL=="M",1,;
      iif(this.Parent.oContained.w_SELODL=="P",2,;
      iif(this.Parent.oContained.w_SELODL=="L",3,;
      iif(this.Parent.oContained.w_SELODL=="F",4,;
      iif(this.Parent.oContained.w_SELODL=="T",5,;
      0)))))
  endfunc


  add object oBtn_1_79 as StdButton with uid="WBDQHNKONY",left=681, top=472, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 259910634;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_79.Click()
      with this.Parent.oContained
        GSCO_BTO(this.Parent.oContained,"Tree-View")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_80 as StdButton with uid="MWXKLVEWEK",left=732, top=472, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 259910634;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_80.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="CGEEJWMTRL",Visible=.t., Left=242, Top=80,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="OLHXGJTWFV",Visible=.t., Left=41, Top=185,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="IEHZXMOABQ",Visible=.t., Left=50, Top=209,;
    Alignment=1, Width=55, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=36, Top=108,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=57, Top=132,;
    Alignment=1, Width=48, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=35, Top=234,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=22, Top=283,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da Gr. Merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=15, Top=331,;
    Alignment=1, Width=90, Height=18,;
    Caption="Da Cat. Omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="BETNLUNOYI",Visible=.t., Left=40, Top=258,;
    Alignment=1, Width=65, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=40, Top=306,;
    Alignment=1, Width=65, Height=18,;
    Caption="A Gr. Merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=27, Top=354,;
    Alignment=1, Width=78, Height=18,;
    Caption="A Cat. Omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="PEISJRCHUW",Visible=.t., Left=4, Top=160,;
    Alignment=0, Width=98, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="HRQFQNITXV",Visible=.t., Left=568, Top=60,;
    Alignment=1, Width=88, Height=18,;
    Caption="Da data Reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="DNNAWKFMZY",Visible=.t., Left=582, Top=80,;
    Alignment=1, Width=74, Height=18,;
    Caption="A data Reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="PYKVJJBJUN",Visible=.t., Left=365, Top=60,;
    Alignment=1, Width=88, Height=18,;
    Caption="Da data Doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="BGHQBDDCZG",Visible=.t., Left=380, Top=80,;
    Alignment=1, Width=73, Height=18,;
    Caption="A data Doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="VQZWRBIBXU",Visible=.t., Left=10, Top=60,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da numero Doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="PMVZWORABY",Visible=.t., Left=19, Top=82,;
    Alignment=1, Width=86, Height=18,;
    Caption="A numero Doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="UKWJKYAMUK",Visible=.t., Left=242, Top=57,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="MKNJKQCTUA",Visible=.t., Left=273, Top=412,;
    Alignment=1, Width=96, Height=18,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="SLVPJSANLN",Visible=.t., Left=285, Top=437,;
    Alignment=1, Width=84, Height=18,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="MUGJTUPSPN",Visible=.t., Left=4, Top=384,;
    Alignment=0, Width=86, Height=18,;
    Caption="Selezioni ODL"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="KOQRSMEMEH",Visible=.t., Left=13, Top=412,;
    Alignment=1, Width=92, Height=18,;
    Caption="Da codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="TUJOWKPNBZ",Visible=.t., Left=15, Top=437,;
    Alignment=1, Width=90, Height=18,;
    Caption="A codice ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="NWROBYOJOC",Visible=.t., Left=55, Top=35,;
    Alignment=1, Width=50, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="ZEENFEJXJZ",Visible=.t., Left=440, Top=35,;
    Alignment=1, Width=71, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="OKKRCHZNDZ",Visible=.t., Left=46, Top=463,;
    Alignment=1, Width=59, Height=18,;
    Caption="Stato ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="XCOUXXLAEH",Visible=.t., Left=5, Top=9,;
    Alignment=0, Width=185, Height=18,;
    Caption="Selezioni documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="DTJYHAGEOT",Visible=.t., Left=461, Top=412,;
    Alignment=1, Width=116, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="LKUZJNOGIO",Visible=.t., Left=461, Top=437,;
    Alignment=1, Width=116, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oBox_1_55 as StdBox with uid="OTTRKDEJWA",left=1, top=26, width=774,height=1

  add object oBox_1_57 as StdBox with uid="BQDUBJBORW",left=1, top=175, width=777,height=1

  add object oBox_1_69 as StdBox with uid="UPYCFJNTIK",left=1, top=403, width=779,height=1
enddefine
define class tgsco_ktoPag2 as StdContainer
  Width  = 789
  height = 534
  stdWidth  = 789
  stdheight = 534
  resizeXpos=625
  resizeYpos=249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_8 as cp_runprogram with uid="EBVGCFPGTX",left=-3, top=569, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BTO('Page2')",;
    cEvent = "ActivatePage 2,",;
    nPag=2;
    , HelpContextID = 172858906


  add object TREEV as cp_Treeview with uid="QTFEDDFLDB",left=1, top=7, width=788,height=398,;
    caption='TREEV',;
   bGlobalFont=.t.,;
    cCursor="_tree_",cShowFields="DESCRI",cNodeShowField="",cLeafShowField="",cNodeBmp="odll.bmp",cLeafBmp="",nIndent=0,cMenuFile="GSCO_KTO",bNoContextMenu=.f.,;
    cEvent = "updtreev",;
    nPag=2;
    , HelpContextID = 12583478


  add object oBtn_2_13 as StdButton with uid="WZCLYAIBJF",left=734, top=478, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , HelpContextID = 259910634;
    , Caption='\<Esci' ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_15 as cp_runprogram with uid="PFRWIUKVRB",left=-3, top=602, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BTO('EXPNODE')",;
    cEvent = "w_treev Expanded",;
    nPag=2;
    , HelpContextID = 172858906


  add object oObj_2_19 as cp_runprogram with uid="GHTXBKLGSI",left=830, top=47, width=72,height=22,;
    caption='GSCO_BTO',;
   bGlobalFont=.t.,;
    prg="GSCO_BTO('Done')",;
    cEvent = "Done",;
    nPag=2;
    , HelpContextID = 212213067

  add object oCODRIC_2_23 as StdField with uid="VXOAYLFGWV",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CODRIC", cQueryName = "CODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo",;
    HelpContextID = 218304986,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=53, Top=485, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODRIC"

  func oCODRIC_2_23.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DDP-MRS-DDO-DIM-DCC-DCF-DRD-DIR-FDO-DPM-DMM')
    endwith
  endfunc

  func oCODRIC_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCADESAR_2_24 as StdField with uid="RXUHOYJINC",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CADESAR", cQueryName = "CADESAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 242229210,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=224, Top=485, InputMask=replicate('X',40)

  func oCADESAR_2_24.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DDP-MRS-DDO-DIM-DRD-DIR-FDO-DCC-DCF-DPM-DMM')
    endwith
  endfunc

  add object oQTAORD_2_27 as StdField with uid="ZJAXUJZHTF",rtseq=67,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 192297978,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=155, Top=452, cSayPict="V_PQ(14)"

  func oQTAORD_2_27.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DMM-MRS-DPM-MRS')
    endwith
  endfunc

  add object oMAGAZ_2_28 as StdField with uid="ULHQATTNMM",rtseq=68,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino",;
    HelpContextID = 16519366,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=635, Top=419, InputMask=replicate('X',5)

  func oMAGAZ_2_28.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'MOV-RIG-MOS-DMT-DMM-DPT-DPM-MRS')
    endwith
  endfunc

  add object oDATEMS_2_31 as StdField with uid="RXYJBPTCQG",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DATEMS", cQueryName = "DATEMS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Inizio Prevista",;
    HelpContextID = 214900682,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=416, Top=419

  func oDATEMS_2_31.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC')
    endwith
  endfunc

  add object oDATEVF_2_34 as StdField with uid="CVKORJQHVQ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DATEVF", cQueryName = "DATEVF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Fine Prevista",;
    HelpContextID = 155131850,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=554, Top=419

  func oDATEVF_2_34.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC')
    endwith
  endfunc

  add object oCAUMAG_2_36 as StdField with uid="VAASQHBFOG",rtseq=71,rtrep=.f.,;
    cFormVar = "w_CAUMAG", cQueryName = "CAUMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale Magazzino",;
    HelpContextID = 159846362,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=503, Top=419, InputMask=replicate('X',5)

  func oCAUMAG_2_36.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-MOS-MRS-DMT-DMM-DPT-DPM')
    endwith
  endfunc

  add object oMAGAZ_2_39 as StdField with uid="ROHJTWHOPL",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino Collegato",;
    HelpContextID = 16519366,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=733, Top=453, InputMask=replicate('X',5)

  func oMAGAZ_2_39.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF')
    endwith
  endfunc


  add object oBtn_2_40 as StdButton with uid="TCINDJTGWB",left=683, top=478, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , HelpContextID = 259910634;
    , Caption='S\<tampa' ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_40.Click()
      do GSCO_sto with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFORNIT_2_41 as StdField with uid="DLFOPXBGEX",rtseq=74,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Fornitore",;
    HelpContextID = 201732522,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=733, Top=452, InputMask=replicate('X',15)

  func oFORNIT_2_41.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG')
    endwith
  endfunc

  add object oQTASCA_2_44 as StdField with uid="ZTZVADQUYJ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_QTASCA", cQueryName = "QTASCA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 258096122,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=329, Top=452, cSayPict="V_PQ(14)"

  func oQTASCA_2_44.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DIM-DCC-DCF-DDO-DRD-DIR-FDO')
    endwith
  endfunc

  add object oQTARES_2_46 as StdField with uid="TRUDNJFLPI",rtseq=76,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� Residua",;
    HelpContextID = 222510074,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=524, Top=452, cSayPict="V_PQ(14)", cGetPict='"999999.99999"'

  func oQTARES_2_46.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF')
    endwith
  endfunc

  add object oNUMREG_2_47 as StdField with uid="HMGELCHHAU",rtseq=77,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero Registrazione",;
    HelpContextID = 155351850,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=53, Top=419, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMREG_2_47.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'MOV-RIG-DIM-DDP-DMT-MOS-DPT-MRS-DIR-FDO-DPT-DPM-DMM')
    endwith
  endfunc

  add object oDATEVA_2_49 as StdField with uid="TSBNOAIARF",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DATEVA", cQueryName = "DATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data Evasione",;
    HelpContextID = 239017930,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=293, Top=419

  func oDATEVA_2_49.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-DDP-MOS-DPT-DIM-MRS-DDO-DRD-DIR-FDO-DCC-DCF-DMT-DMM-DPM')
    endwith
  endfunc

  add object oFORNIT_2_53 as StdField with uid="ENPHLDRIEX",rtseq=79,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 201732522,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=344, Top=452, InputMask=replicate('X',15)

  func oFORNIT_2_53.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'FSC')
    endwith
  endfunc

  add object oRIF_2_57 as StdField with uid="FNGKZIMNPQ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_RIF", cQueryName = "RIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento",;
    HelpContextID = 186321174,;
   bGlobalFont=.t.,;
    Height=20, Width=116, Left=53, Top=419, InputMask=replicate('X',15)

  func oRIF_2_57.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'DDO-DRD-DCC-DCF')
    endwith
  endfunc

  add object oQTAORD_2_60 as StdField with uid="LGQGFGWBBB",rtseq=82,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 192297978,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=158, Top=452, cSayPict="V_PQ(14)"

  func oQTAORD_2_60.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DIM-FDO-DCC-DCF-DDO-DRD-DIR')
    endwith
  endfunc

  add object oQTAEVA_2_62 as StdField with uid="HDXHCNMVBO",rtseq=83,rtrep=.f.,;
    cFormVar = "w_QTAEVA", cQueryName = "QTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 239090682,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=330, Top=452, cSayPict="V_PQ(14)"

  func oQTAEVA_2_62.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF')
    endwith
  endfunc


  add object StatoODL as cp_calclbl with uid="EGPCZBJOSD",left=212, top=419, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , ToolTipText = "Stato ODL";
    , HelpContextID = 172858906

  add object oUNIMIS1_2_64 as StdField with uid="PIEVLFELWU",rtseq=84,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 218612154,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=53, Top=452, InputMask=replicate('X',3)

  func oUNIMIS1_2_64.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF-RIG-FSC-DDP-DMM-MRS-DPM-DDO-DIM-DRD-DIR-FDO-DCC-DCF-DPM')
    endwith
  endfunc

  add object oCODODL__2_66 as StdField with uid="QMETSTGGSI",rtseq=85,rtrep=.f.,;
    cFormVar = "w_CODODL_", cQueryName = "CODODL_",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL",;
    HelpContextID = 72749530,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=53, Top=419, InputMask=replicate('X',15)

  func oCODODL__2_66.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'FSC-ODL-ODF'  or .w_LEVEL>2)
    endwith
  endfunc

  add object oCPROWNUM_2_68 as StdField with uid="ZZMVCTSBRG",rtseq=86,rtrep=.f.,;
    cFormVar = "w_CPROWNUM", cQueryName = "CPROWNUM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero Registrazione",;
    HelpContextID = 249220979,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=195, Top=419, cSayPict='"9999"', cGetPict='"9999"'

  func oCPROWNUM_2_68.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'MOV-RIG-DIM-DDP-DMT-MOS-DPT-MRS-DIR-FDO-DPM-DMM')
    endwith
  endfunc


  add object ProvODLF as cp_calclbl with uid="GLBLCRWGSV",left=53, top=514, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",;
    nPag=2;
    , ToolTipText = "Provenienza fase";
    , HelpContextID = 172858906

  add object oStr_2_25 as StdString with uid="OHCFHJNCKJ",Visible=.t., Left=4, Top=485,;
    Alignment=0, Width=44, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DDP-MRS-DDO-DIM-DRD-DIR-FDO-DCC-DCF-DPM-DMM')
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="TYMQXOFOZF",Visible=.t., Left=19, Top=419,;
    Alignment=0, Width=29, Height=19,;
    Caption="ODL:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>1 or !.w_type $ 'ODL-ODF-FSC')
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="FEOVYMXXOC",Visible=.t., Left=5, Top=419,;
    Alignment=0, Width=43, Height=18,;
    Caption="N.Reg.:"  ;
  , bGlobalFont=.t.

  func oStr_2_29.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or !.w_type $ 'MOV-RIG-MOS-MRS-DMT-DMM-DPT-DPM')
    endwith
  endfunc

  add object oStr_2_30 as StdString with uid="ZREGTQDKVC",Visible=.t., Left=491, Top=419,;
    Alignment=1, Width=60, Height=18,;
    Caption="Data Fine:"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="MROTAHJPCA",Visible=.t., Left=345, Top=419,;
    Alignment=1, Width=65, Height=18,;
    Caption="Data Inizio:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC')
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="YRUYCTEQFN",Visible=.t., Left=273, Top=452,;
    Alignment=1, Width=48, Height=18,;
    Caption="Q.t� Eva:"  ;
  , bGlobalFont=.t.

  func oStr_2_33.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF')
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="XLZDIQZVOB",Visible=.t., Left=390, Top=419,;
    Alignment=0, Width=107, Height=18,;
    Caption="Causale Magazzino"  ;
  , bGlobalFont=.t.

  func oStr_2_35.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'MOV-RIG-MOS-MRS-DMT-DMM-DPT-DPM')
    endwith
  endfunc

  add object oStr_2_38 as StdString with uid="TXYPTWVYPT",Visible=.t., Left=568, Top=421,;
    Alignment=1, Width=64, Height=18,;
    Caption="Magazzino"  ;
  , bGlobalFont=.t.

  func oStr_2_38.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-MOS--MRS-DMT-DMM-DPT-DPM')
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="ZSHYFFEHKF",Visible=.t., Left=670, Top=452,;
    Alignment=1, Width=60, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF')
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="VVTEDZSMKJ",Visible=.t., Left=21, Top=452,;
    Alignment=0, Width=27, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DDP-DMM-MRS-DPM-DDO-DIM-DRD-DIR-FDO-DCC-DCF-DPM')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="PEAGIPMTQK",Visible=.t., Left=104, Top=452,;
    Alignment=1, Width=48, Height=18,;
    Caption="Q.t� Ord:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="UTOQEVGYND",Visible=.t., Left=260, Top=421,;
    Alignment=0, Width=30, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-DDP-MOS-DPT-DMT-DPT-DDO-DIM-DRD-DIR-FDO-MRS-DMM-DCC-DCF-DPM')
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="LVRPZORVLS",Visible=.t., Left=652, Top=452,;
    Alignment=1, Width=79, Height=18,;
    Caption="Mag collegato:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'MOV-RIG')
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="RKBLGZYVPL",Visible=.t., Left=99, Top=452,;
    Alignment=0, Width=53, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'RIG-FSC-DMM-MRS-DPM-MRS')
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="RNLXLDMWCD",Visible=.t., Left=271, Top=452,;
    Alignment=1, Width=57, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'FSC')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="BRGBABXCFQ",Visible=.t., Left=6, Top=420,;
    Alignment=0, Width=42, Height=18,;
    Caption="N. doc:"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2  or !.w_type $ 'DDP-DDO-DIM-DCC-DCF-DRD-DIR-FDO')
    endwith
  endfunc

  add object oStr_2_55 as StdString with uid="NMYMELHNBC",Visible=.t., Left=269, Top=452,;
    Alignment=0, Width=57, Height=18,;
    Caption="Q.t� Sca.:"  ;
  , bGlobalFont=.t.

  func oStr_2_55.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DDO-DIM-DCC-DCF-DIR-DRD-FDO')
    endwith
  endfunc

  add object oStr_2_56 as StdString with uid="QOOJGWSKSX",Visible=.t., Left=470, Top=452,;
    Alignment=1, Width=51, Height=18,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_2_56.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF')
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="LDVDZCRTET",Visible=.t., Left=98, Top=452,;
    Alignment=1, Width=59, Height=18,;
    Caption="Q.t� Prod.:"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DIM-DCC-DCF-FDO')
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="VPQPDVXLQB",Visible=.t., Left=97, Top=452,;
    Alignment=1, Width=59, Height=18,;
    Caption="Q.t� Vers.:"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDO-DRD-DIR')
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="TKOPVAQFSL",Visible=.t., Left=23, Top=419,;
    Alignment=0, Width=25, Height=18,;
    Caption="OCL"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'FSC')
    endwith
  endfunc

  add object oStr_2_67 as StdString with uid="TDNGMPKGYX",Visible=.t., Left=188, Top=419,;
    Alignment=0, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_67.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'MOV-RIG-DIM-DDP-DMT-MOS-DPT-MRS-DIR-FDO-DPT-DPM-DMM')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_kto','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
