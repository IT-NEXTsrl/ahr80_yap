* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_brl                                                        *
*              Gestione rientro C/Lavoro                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_428]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-10                                                      *
* Last revis.: 2018-05-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_brl",oParentObject)
return(i_retval)

define class tgsco_brl as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_CHKDEL = .f.
  w_CODCON = space(15)
  w_CODMAG = space(5)
  w_WIPNETT = space(1)
  w_CAUSCA = space(5)
  w_CAUCAR = space(5)
  w_DPDATREG = ctod("  /  /  ")
  w_OK = .f.
  w_MESS = space(10)
  w_CFUNC = space(10)
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_TRIG = 0
  w_SERIAL = space(10)
  w_NUMRIF = 0
  w_CLARIF = space(2)
  w_ODLRIF = space(10)
  w_TRIG = 0
  w_ROWORD = 0
  w_QTASAL = 0
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_FLEVAS = space(1)
  w_SRV = space(1)
  w_MAGDOC = space(5)
  w_CODRIC = space(20)
  w_COART = space(20)
  w_CECOS = space(15)
  w_VOCOS = space(15)
  w_COMME = space(15)
  w_COATT = space(15)
  w_NR = space(4)
  w_UMRIF = space(3)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_SEROCL = space(15)
  w_QTAPRE = 0
  w_MAGPRE = space(5)
  w_OLDFLERIF = space(1)
  w_FLDEL = .f.
  w_SEDOC = space(10)
  w_FLGAGGOCL = space(1)
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_FLCAR = space(1)
  w_FLORD = space(1)
  w_FLIMP = space(1)
  w_FLRIS = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_TmpN = 0
  w_VALUNI = 0
  w_DECTOT = 0
  TmpC = space(100)
  w_FIELDS = space(10)
  w_FILTERS = space(10)
  w_DELTESTATA = space(1)
  w_OQTAMOV = 0
  w_OQTAUM1 = 0
  w_COECONV = 0
  w_OCODCOM = space(15)
  w_OCODMAG = space(5)
  w_AVMQTA = 0
  w_AVMQT1 = 0
  w_AVFLEV = space(15)
  w_SERDT = space(10)
  w_UPDATED = .f.
  w_QTADAB = 0
  w_QTAPR1 = 0
  w_MAGTRA = space(5)
  w_MAGRIF = space(5)
  w_PEQTAOLD = 0
  w_PEQTAUM1 = 0
  w_ROWNUM_MAT = 0
  w_ROWORD_MAT = 0
  w_MVEMERIC = space(1)
  w_COMMDEFA = space(0)
  w_OLDCOM = space(1)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_OLDCOM = .f.
  w_switch = .f.
  w_ELIMINA = .f.
  w_OLTQTSAL = 0
  w_OLTSTATO = space(1)
  w_OLTCODIC = space(20)
  w_OLTCOART = space(20)
  w_OLTQTODL = 0
  w_OLTQTOD1 = 0
  w_OLTQTOEV = 0
  w_OLTQTOE1 = 0
  w_OLTFLORD = space(1)
  w_OLTFLIMP = space(1)
  w_OLTKEYSA = space(20)
  w_OLTCOMAG = space(5)
  w_OLTUNMIS = space(3)
  w_OLTFLEVA = space(1)
  w_OLTDTINI = ctod("  /  /  ")
  w_OLTDTFIN = ctod("  /  /  ")
  w_OLCODMAG = space(5)
  w_OLUNIMIS = space(3)
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_OLQTASAL = 0
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OLCOEIMP = 0
  w_OLFLPREV = space(1)
  w_OLFLEVAS = space(1)
  w_OLFLORDI = space(1)
  w_OLFLIMPE = space(1)
  w_OLFLRISE = space(1)
  w_CPROWNUM = 0
  w_MVKEYSAL = space(20)
  w_MVCODMAG = space(5)
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVCODCON = space(15)
  w_MVCODODL = space(15)
  w_MVRIGMAT = 0
  w_RIGMAT = 0
  w_MVPREZZO = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVVALRIG = 0
  w_MVVALMAG = 0
  w_MVIMPNAZ = 0
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVROWDDT = 0
  w_RIGMOVLOT = .f.
  w_PERASS = space(3)
  w_QTDIF = 0
  w_OLTQTPRO = 0
  w_OLTQTPR1 = 0
  w_OPERAZ = space(2)
  w_CRIVAL = space(2)
  w_MVTIPDOC = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_MVFLPROV = space(1)
  w_NUMSCO = 0
  w_FLPPRO = space(1)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_nRecSel = 0
  w_nRecEla = 0
  w_nRecDoc = 0
  w_DATMAT = ctod("  /  /  ")
  w_DOCSCA = space(10)
  w_HASMAT = .f.
  w_GENROWDOC = 0
  w_GENROWMAT = 0
  w_MATQTAUM1 = 0
  w_DOCSER = space(10)
  w_LNumErr = 0
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_TIPART = space(2)
  radice = space(240)
  w_QTATOT = 0
  riga = 0
  w_PDSERIAL = space(15)
  w_PDROWNUM = 0
  w_QTARES = 0
  w_COECONV = 0
  w_STORNAMAT = .f.
  w_MTCODMAT = space(40)
  w_MSGMATRI = space(10)
  w_MT__FLAG = space(1)
  w_AM_PRENO = 0
  w_CODODL = space(15)
  w_DPSERIAL = space(15)
  w_TROV = .f.
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  w_MVCODART = space(20)
  w_MVDESART = space(40)
  w_MVUNIMIS = space(3)
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODLOT = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVF2CASC = space(1)
  w_MVF2RISE = space(1)
  w_MVFLUBIC = space(1)
  w_MVFLUBI2 = space(1)
  w_MVARTLOT = space(1)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVRIFRIG = 0
  w_MVTIPRIG = space(1)
  w_MVCAUMAG = space(5)
  w_MVCATCON = space(5)
  w_MVCONTRA = space(15)
  w_MVDESSUP = space(0)
  w_MVINICOM = ctod("  /  /  ")
  w_MVFINCOM = ctod("  /  /  ")
  w_MVCODLIS = space(5)
  w_MVCODCLA = space(3)
  w_MVPREZZO = 0
  w_VALUNI = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVFLOMAG = space(1)
  w_MVIMPACC = 0
  w_MVIMPSCO = 0
  w_MVIMPNAZ = 0
  w_MVVALMAG = 0
  w_MVVALRIG = 0
  w_MVPESNET = 0
  w_MVFLRAGG = space(1)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVCODIVA = space(5)
  w_MVCONIND = space(15)
  w_MVNOMENC = space(8)
  w_MVFLTRAS = space(1)
  w_MVMOLSUP = 0
  w_MVCAUCOL = space(5)
  w_MVCODATT = space(15)
  w_MVCODCEN = space(15)
  w_MVCODCOM = space(15)
  w_MVCODODL = space(15)
  w_MVF2ORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLELGM = space(1)
  w_MVFLIMPE = space(1)
  w_MVKEYSAL = space(20)
  w_MVTIPATT = space(1)
  w_MVRIGMAT = 0
  w_MVVOCCEN = space(15)
  w_CONFERMA = .f.
  w_DATDOC = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CAUDOC = space(5)
  w_DESDOC = space(40)
  w_OLDROW = 0
  w_OLDART = space(20)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_DATREG = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_ROWMAT = 0
  CONTA = 0
  SERDOC = space(10)
  RIGDOC = 0
  NUMROW = 0
  LOTTO = space(20)
  UBICA = space(20)
  UBICA2 = space(20)
  QTA1 = 0
  QTA2 = 0
  RIFRIGA = 0
  w_MVFLELAN = space(1)
  w_MV_SEGNO = space(1)
  w_ROWDOC = 0
  w_QTADIF = 0
  w_QTADIF2 = 0
  QTA1MP = 0
  QTA2MP = 0
  w_POS = 0
  w_RIGADUP = .f.
  w_OLDSERROW = space(30)
  w_SPLITRIG = space(1)
  w_FINERIG = .f.
  w_ULTRIG = .f.
  OLDUBICA = space(20)
  OLDUBICA2 = space(20)
  OLDSERDOC = space(10)
  OLDRIFRIGA = 0
  OLDRIGDOC = 0
  OLDNUMROW = 0
  w_OLDQTADIF = 0
  w_OLDQTADIF2 = 0
  w_SPLITLOT = space(1)
  w_MODUM2D = space(1)
  w_FLUSEPD = space(1)
  w_MOLT3 = 0
  w_OP3 = space(1)
  w_MVLOTMAG = space(5)
  w_MVLOTMAT = space(5)
  w_PEQTAUM1 = 0
  w_PECODODL = space(15)
  w_PGCODODL = space(15)
  w_PESERIAL = space(15)
  w_PEQTAABB = 0
  w_PENUMRIF = 0
  w_PESERORD = space(10)
  w_PERIGORD = 0
  w_PROGR = 0
  w_PETIPRIF = space(1)
  w_PECODART = space(20)
  w_DPOGGMPS = space(1)
  w_PECODCOM = space(15)
  w_PECODRIC = space(20)
  w_PEFLCOMM = space(1)
  w_PERIFTRS = space(20)
  w_TRSRIFODL = space(15)
  w_TRSSERORD = space(10)
  w_TRSRIGORD = 0
  w_OLTCOMME = space(15)
  w_RIFMAG = space(15)
  w_DPCODCOM = space(15)
  w_DPCODODL = space(15)
  w_CLTQTOEV = 0
  w_CLTQTOE1 = 0
  w_CLTQTODL = 0
  w_CLTQTOD1 = 0
  w_CLTFLEVA = space(1)
  w_CLTQTSAL = 0
  w_CLTSTATO = space(1)
  w_CLTDTFIN = ctod("  /  /  ")
  w_CLTDTINI = ctod("  /  /  ")
  w_QTADASTO = 0
  w_QTADAST1 = 0
  w_CLDICUM1 = 0
  w_CLQTADIC = 0
  w_MYFAS = 0
  w_CLAVAUM1 = 0
  w_CLQTAAVA = 0
  w_CLQTASCA = 0
  w_CLSCAUM1 = 0
  w_FASE = 0
  w_TmpC1 = space(1)
  w_TmpC2 = space(1)
  w_TmpC4 = space(5)
  w_TmpC3 = space(20)
  w_TmpC5 = space(1)
  w_TmpART = space(20)
  w_TmpCOMM = space(15)
  w_TmpFLC = space(1)
  w_CLULTFAS = space(1)
  w_OQTSAL = 0
  w_OPERAZCL = space(2)
  w_OLDQTAMOV = 0
  w_OLDQTAPRE = 0
  w_PROVE = space(1)
  w_FASPD = 0
  w_ODLPD = space(15)
  w_OLTSECPR = space(15)
  w_DOCDAGEN = space(3)
  w_SEDOCSM = space(10)
  w_DELMASSM = space(1)
  w_SEDOCSF = space(10)
  w_DELMASSF = space(1)
  w_SEDOCCF = space(10)
  w_DELMASCF = space(1)
  w_FLAPP = .f.
  w_MATRUPD = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  MAGAZZIN_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  RIF_GODL_idx=0
  MOVIMATR_idx=0
  MVM_DETT_idx=0
  TMPMOVIMAST_idx=0
  DELTA_idx=0
  MAT_PROD_idx=0
  AGG_LOTT_idx=0
  SPL_LOTT_idx=0
  DIC_MATR_idx=0
  DIC_MCOM_idx=0
  MATRICOL_idx=0
  PEG_SELI_idx=0
  MPS_TFOR_idx=0
  SALDICOM_idx=0
  ODL_CICL_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Evasione dati OCL per gestione C/Lavoro (da GSAC_MDV)
    * --- Verifica se ordine generante � da c/lavoro e genera Documento di Scarico per i materiali utilizzati
    * --- Viene eseguito in A.M. Replace e dall Evento Delete End (relativamento allo Storno Doc.Import)
    this.w_OK = .T.
    this.w_MESS = ah_Msgformat("Transazione abbandonata")
    this.w_PADRE = this.oParentObject
    this.w_CFUNC = this.oParentObject.cFunction
    this.w_FLDEL = .F.
    this.w_TRIG = 0
    this.w_SERIAL = this.oParentObject.w_MVSERIAL
    this.w_SERDT = this.w_SERIAL
    this.w_CODCON = this.oParentObject.w_MVCODCON
    this.w_DECTOT = this.oParentObject.w_DECTOT
    this.w_NUMRIF = -20
    this.w_MVNUMRIF = -20
    this.w_switch = .f.
    if this.oParentObject.GSAC_MMP.class="Stdlazychild"
      * --- Apro
      this.oParentObject.GSAC_MMP.LinkPCClick()
      * --- Chiudo
      this.oParentObject.GSAC_MMP.LinkPCClick()
    endif
    this.w_RIGMOVLOT = this.oParentObject.GSAC_MMP.cnt.w_RIGMOVLOT
    * --- Legge codice magazzino terzista (w_CODMAG)
    this.w_CODMAG = SPACE(5)
    this.w_WIPNETT = " "
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANMAGTER"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC("F");
            +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANMAGTER;
        from (i_cTable) where;
            ANTIPCON = "F";
            and ANCODICE = this.w_CODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODMAG = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Verifica se WIP terzista � nettificabile (w_WIPNETT)
    if NOT EMPTY(this.w_CODMAG)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGDISMAG"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGDISMAG;
          from (i_cTable) where;
              MGCODMAG = this.w_CODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_WIPNETT = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Legge causali produzione (carico/scarico)
    this.w_CAUSCA = SPACE(5)
    this.w_CAUCAR = SPACE(5)
    this.w_DPDATREG = this.oParentObject.w_MVDATREG
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUSCA,PPCAUCAR"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUSCA,PPCAUCAR;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUSCA = NVL(cp_ToDate(_read_.PPCAUSCA),cp_NullValue(_read_.PPCAUSCA))
      this.w_CAUCAR = NVL(cp_ToDate(_read_.PPCAUCAR),cp_NullValue(_read_.PPCAUCAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Tipologia gestione commessa
    if VARTYPE(g_OLDCOM)="L" AND g_OLDCOM
      this.w_OLDCOM = .t.
    else
      this.w_OLDCOM = .f.
    endif
    * --- Crea il File delle Messaggistiche di Errore
    CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
    * --- Calcola i Costi Unitari e Globali su Ultimo Costo Standard Articolo
    vq_exec("..\COLA\EXE\QUERY\GSCOXQIC.VQR", this,"APPOVALO")
    * --- set deleted ripristinata al valore originario al termine del Batch
    ah_Msg("Controlli modulo produzione...",.T.)
    OLDSET = SET("DELETED")
    * --- Inizializzo variabili per eventuale inserimento in MAT_PROD
    this.w_ROWNUM_MAT = 0
    this.w_ROWORD_MAT = 0
    if upper(this.w_cFunc)="LOAD"
      * --- Imposta la Deleted ON per non elaborare i record Cancellati con F6 nel caricamento di un nuovo DDT
      this.w_CHKDEL = .F.
    else
      * --- Imposta la Deleted OFF per riattivare i record Cancellati con F6 (serve nei Controlli su Doc.Importati)
      this.w_CHKDEL = .T.
      * --- Ricerca Documenti di Scarico Materiali associati al DDT per eventuale Storno (MVSERDDT = w_SERIAL)
      VQ_EXEC("..\COLA\EXE\QUERY\GSCO_QDO.VQR",this,"DOCSCA")
    endif
    this.w_PADRE.MarkPos()     
    this.w_FIELDS = "this.w_MVSERIAL as MVSERIAL, CPROWNUM, t_MVSERRIF, t_MVROWRIF, t_CLARIF, t_OFLDIF, t_MVFLERIF, t_ODLRIF, MVCODMAG, MVFLERIF"
    this.w_FIELDS = this.w_FIELDS + ", t_MVQTASAL, t_MVQTAMOV, t_MVQTAUM1, t_MVFLERIF, t_MVCODMAG, t_MVCODICE, t_MVCODART"
    this.w_FIELDS = this.w_FIELDS + ", t_MVCODCEN, t_MVVOCCEN, t_MVCODCOM, t_MVCODATT, t_MVUNIMIS"
    this.w_FILTERS = "(t_CPROWORD<>0 AND t_MVTIPRIG<>' ' AND NOT EMPTY(t_MVCODICE)) AND (I_SRV $ 'U-A' Or this.cFunction='Query')"
    this.w_FILTERS = this.w_FILTERS + "AND NOT EMPTY(t_MVSERRIF) AND t_MVROWRIF<>0 AND t_CLARIF='OR' AND NOT EMPTY(t_ODLRIF)"
    this.w_PADRE.Exec_Select("RowsDoc", this.w_FIELDS , this.w_FILTERS , "t_MVSERRIF, MVSERRIF, CPROWNUM" , "CPROWNUM, t_MVSERRIF, t_MVROWRIF")
    this.w_UPDATED = .f.
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      this.w_MVSERRIF = NVL(this.w_PADRE.GET("t_MVSERRIF"), space(10))
      this.w_MVROWRIF = NVL(this.w_PADRE.GET("t_MVROWRIF"), 0)
      this.w_CLARIF = NVL(this.w_PADRE.GET("t_CLARIF"), space(2))
      this.w_ODLRIF = NVL(this.w_PADRE.GET("t_ODLRIF"), space(10))
      * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
      *     se non cancellate
      if this.w_PADRE.FullRow() And NOT EMPTY(this.w_MVSERRIF) AND this.w_MVROWRIF<>0 AND this.w_CLARIF="OR" AND NOT EMPTY(this.w_ODLRIF)
        this.w_SEDOCCF = SPACE(10)
        this.w_SEDOCSF = SPACE(10)
        this.w_DELTESTATA = "S"
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Se tutto ok passo alla prossima riga altrimenti esco...
      if this.w_OK
        this.w_PADRE.NextRow()     
      else
        Exit
      endif
    enddo
    if this.w_OK
      if this.w_CHKDEL
        this.w_PADRE.FirstRowDel()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          this.w_MVSERRIF = NVL(this.w_PADRE.GET("t_MVSERRIF"), space(10))
          this.w_MVROWRIF = NVL(this.w_PADRE.GET("t_MVROWRIF"), 0)
          this.w_CLARIF = NVL(this.w_PADRE.GET("t_CLARIF"), space(2))
          this.w_ODLRIF = NVL(this.w_PADRE.GET("t_ODLRIF"), space(10))
          if this.w_PADRE.FullRow() and NOT EMPTY(this.w_MVSERRIF) AND this.w_MVROWRIF<>0 AND this.w_CLARIF="OR" AND NOT EMPTY(this.w_ODLRIF)
            this.w_DELTESTATA = "S"
            this.Page_12()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_PADRE.NextRowDel()     
          else
            exit
          endif
        enddo
      endif
    endif
    if Used("RowsDoc")
      Use in RowsDoc
    endif
    if USED("DOCSCA")
      SELECT DOCSCA
      USE
    endif
    if USED("MessErr")
      SELECT MessErr
      USE
    endif
    if USED("APPOVALO")
      SELECT APPOVALO
      USE
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    this.w_PADRE.RePos(True)     
    if ! this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Evasione OCL Collegato
    * --- Leggo i dati sulla quantit� a saldo dell' OCL subito prima dell'aggiornamento
    * --- Non avviene nessuno Storno dell' Ordinato in quanto l'OCL viene gia' evaso dalla Generazione dell' Ordine
    this.w_OLTDTINI = cp_CharToDate("  -  -  ")
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTQTSAL,OLTSTATO,OLTCODIC,OLTCOART,OLTQTODL,OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTFLORD,OLTFLIMP,OLTKEYSA,OLTCOMAG,OLTUNMIS,OLTDTINI,OLTCOMME,OLTPERAS,OLTQTPRO,OLTQTPR1"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_SEROCL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTQTSAL,OLTSTATO,OLTCODIC,OLTCOART,OLTQTODL,OLTQTOD1,OLTQTOEV,OLTQTOE1,OLTFLORD,OLTFLIMP,OLTKEYSA,OLTCOMAG,OLTUNMIS,OLTDTINI,OLTCOMME,OLTPERAS,OLTQTPRO,OLTQTPR1;
        from (i_cTable) where;
            OLCODODL = this.w_SEROCL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLTQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
      this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
      this.w_OLTCODIC = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
      this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
      this.w_OLTQTODL = NVL(cp_ToDate(_read_.OLTQTODL),cp_NullValue(_read_.OLTQTODL))
      this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
      this.w_OLTQTOEV = NVL(cp_ToDate(_read_.OLTQTOEV),cp_NullValue(_read_.OLTQTOEV))
      this.w_OLTQTOE1 = NVL(cp_ToDate(_read_.OLTQTOE1),cp_NullValue(_read_.OLTQTOE1))
      this.w_OLTFLORD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
      this.w_OLTFLIMP = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
      this.w_OLTKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
      this.w_OLTCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
      this.w_OLTUNMIS = NVL(cp_ToDate(_read_.OLTUNMIS),cp_NullValue(_read_.OLTUNMIS))
      this.w_OLTDTINI = NVL(cp_ToDate(_read_.OLTDTINI),cp_NullValue(_read_.OLTDTINI))
      this.w_DPCODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
      this.w_PERASS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
      this.w_OLTQTPRO = NVL(cp_ToDate(_read_.OLTQTPRO),cp_NullValue(_read_.OLTQTPRO))
      this.w_OLTQTPR1 = NVL(cp_ToDate(_read_.OLTQTPR1),cp_NullValue(_read_.OLTQTPR1))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Rilegge dati UM
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
        from (i_cTable) where;
            ARCODART = this.w_OLTCOART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_OLTCODIC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAUNIMIS,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = this.w_OLTCODIC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_UM2 = PADR(this.w_UM2,3)
    this.w_UM3 = PADR(this.w_UM3,3)
    this.w_PECODART = this.w_OLTCOART
    this.w_PECODODL = this.w_SEROCL
    do case
      case this.w_FLGAGGOCL="C"
        * --- Caso caricamento nuova riga
        * --- Calcola nuove quantit� e flags
        this.w_OLTQTOEV = this.w_OLTQTOEV + this.w_QTAMOV
        this.w_OLTQTOE1 = this.w_OLTQTOE1 + this.w_QTAUM1
        this.w_OLTFLEVA = IIF( this.w_OLTQTOE1 >= this.w_OLTQTOD1 OR this.w_FLEVAS="S", "S", " ")
        this.w_OLTQTSAL = IIF(this.w_OLTFLEVA="S", 0, this.w_OLTQTOD1-this.w_OLTQTOE1)
        this.w_OLTSTATO = IIF(this.w_OLTFLEVA="S", "F", this.w_OLTSTATO)
        this.w_OLTDTINI = IIF(EMPTY(this.w_OLTDTINI), i_DATSYS, this.w_OLTDTINI)
        this.w_OLTDTFIN = IIF(this.w_OLTSTATO="F", i_DATSYS, cp_CharToDate("  -  -  "))
        this.w_OLTQTPRO = this.w_OLTQTPRO + this.w_QTAMOV
        this.w_OLTQTPR1 = this.w_OLTQTPRO + this.w_QTAUM1
        * --- Aggiorna saldi MPS
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_QTAUM1);
          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAUM1);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_OLTCOART);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSLAN = FMMPSLAN - this.w_QTAUM1;
              ,FMMPSTOT = FMMPSTOT - this.w_QTAUM1;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_OLTCOART;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_OLDCOM
          * --- Aggiorna Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          if not empty(this.w_PECODART)
            this.w_PGCODODL = this.w_SEROCL
            this.w_PECODODL = " Magazz.: "+this.w_MAGDOC
            this.w_DPCODODL = this.w_SEROCL
            this.w_PEQTAUM1 = this.w_QTAUM1
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.w_FLGAGGOCL="D"
        * --- Caso cancellazione riga
        * --- Calcola nuove quantit� e flags
        this.w_OLTQTOEV = this.w_OLTQTOEV - this.w_OQTAMOV
        this.w_OLTQTOE1 = this.w_OLTQTOE1 - this.w_OQTAUM1
        this.w_OLTFLEVA = IIF(this.w_OLTQTOE1 >= this.w_OLTQTOD1, "S", " ")
        this.w_OLTQTSAL = IIF(this.w_OLTFLEVA="S", 0, this.w_OLTQTOD1-this.w_OLTQTOE1)
        this.w_OLTSTATO = IIF(this.w_OLTFLEVA="S", "F", "L")
        this.w_OLTDTFIN = IIF(this.w_OLTSTATO="F", i_DATSYS, cp_CharToDate("  -  -  "))
        this.w_OLTQTPRO = this.w_OLTQTPRO - this.w_OQTAMOV
        this.w_OLTQTPR1 = this.w_OLTQTPRO - this.w_OQTAUM1
        * --- Aggiorna saldi MPS
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_QTAUM1);
          +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.w_QTAUM1);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_OLTCOART);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSLAN = FMMPSLAN + this.w_QTAUM1;
              ,FMMPSTOT = FMMPSTOT + this.w_QTAUM1;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_OLTCOART;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_OLDCOM
          * --- Aggiorna Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          if not empty(this.w_PECODART)
            this.w_PGCODODL = " Magazz.: "+this.w_MAGDOC
            this.w_PECODODL = this.w_SEROCL
            this.w_DPCODODL = this.w_SEROCL
            this.w_PEQTAUM1 = this.w_QTAUM1
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.w_FLGAGGOCL="M"
        * --- Caso modifica riga
        * --- Calcola nuove quantit� e flags
        this.w_OLTQTOEV = this.w_OLTQTOEV - this.w_QTAPRE + this.w_QTAMOV
        this.w_OLTQTOE1 = this.w_OLTQTOEV
        this.w_OLTQTPRO = this.w_OLTQTPRO - this.w_QTAPRE + this.w_QTAMOV
        this.w_OLTQTPR1 = this.w_OLTQTPRO - this.w_QTAPR1 + this.w_QTAUM1
        if this.w_OLTUNMIS<>this.w_UM1
          this.w_OLTQTOE1 = CALQTA(this.w_OLTQTOEV, this.w_OLTUNMIS,this.w_UM2, this.w_OPERAT, this.w_MOLTIP,"", "N", "",, this.w_UM3, this.w_OPERA3, this.w_MOLTI3)
        endif
        this.w_OLTQTOE1 = this.w_OLTQTOE1
        this.w_OLTFLEVA = IIF( this.w_OLTQTOE1 >= this.w_OLTQTOD1 OR this.w_FLEVAS="S", "S", " ")
        this.w_OLTQTSAL = IIF(this.w_OLTFLEVA="S", 0, this.w_OLTQTOD1-this.w_OLTQTOE1)
        this.w_OLTSTATO = IIF(this.w_OLTFLEVA="S", "F", "L")
        this.w_OLTDTFIN = IIF(this.w_OLTSTATO="F", i_DATSYS, cp_CharToDate("  -  -  "))
        this.w_QTDIF = this.w_QTAPRE-this.w_QTAUM1
        * --- Aggiorna saldi MPS
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSLAN =FMMPSLAN+ "+cp_ToStrODBC(this.w_QTDIF);
          +",FMMPSTOT =FMMPSTOT+ "+cp_ToStrODBC(this.w_QTDIF);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_OLTCOART);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSLAN = FMMPSLAN + this.w_QTDIF;
              ,FMMPSTOT = FMMPSTOT + this.w_QTDIF;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_OLTCOART;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_OLDCOM
          * --- Aggiorna Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          if not empty(this.w_PECODART)
            this.w_PGCODODL = " Magazz.: "+this.w_MAGDOC
            this.w_PECODODL = this.w_SEROCL
            this.w_DPCODODL = this.w_SEROCL
            this.w_PEQTAUM1 = this.w_QTAPRE
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PGCODODL = this.w_SEROCL
            this.w_PECODODL = " Magazz.: "+this.w_MAGDOC
            this.w_DPCODODL = this.w_SEROCL
            this.w_PEQTAUM1 = this.w_QTAUM1
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
    endcase
    * --- Aggiorna saldi MPS
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTQTOEV ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOEV),'ODL_MAST','OLTQTOEV');
      +",OLTQTOE1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOE1),'ODL_MAST','OLTQTOE1');
      +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTSAL),'ODL_MAST','OLTQTSAL');
      +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_MAST','OLTFLEVA');
      +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
      +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTFIN),'ODL_MAST','OLTDTFIN');
      +",OLTDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTINI),'ODL_MAST','OLTDTINI');
      +",OLTQTPRO ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTPRO),'ODL_MAST','OLTQTPRO');
      +",OLTQTPR1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTPR1),'ODL_MAST','OLTQTPR1');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.w_SEROCL);
             )
    else
      update (i_cTable) set;
          OLTQTOEV = this.w_OLTQTOEV;
          ,OLTQTOE1 = this.w_OLTQTOE1;
          ,OLTQTSAL = this.w_OLTQTSAL;
          ,OLTFLEVA = this.w_OLTFLEVA;
          ,OLTSTATO = this.w_OLTSTATO;
          ,OLTDTFIN = this.w_OLTDTFIN;
          ,OLTDTINI = this.w_OLTDTINI;
          ,OLTQTPRO = this.w_OLTQTPRO;
          ,OLTQTPR1 = this.w_OLTQTPR1;
          &i_ccchkf. ;
       where;
          OLCODODL = this.w_SEROCL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documento di Scarico Materiali Collegato
    * --- Log Errori
    this.w_LNumErr = 0
    this.w_nRecSel = 0
    this.w_nRecEla = 0
    this.w_nRecDoc = 0
    this.w_DOCSCA = SPACE(10)
    this.w_OPERAZ = "SM"
    this.w_CRIVAL = "US"
    this.w_nRecSel = this.w_nRecSel + 1
    this.w_HASMAT = .f.
    * --- Generazione Documento di Scarico
    ah_Msg("Generazione scarico materiali per produzione...",.T.)
    if this.w_switch
      * --- GENEORDI creato a pagina 12
      this.w_OPERAZ = "CS"
      this.w_HASMAT = .t.
      this.w_MVQTAMOV = this.w_QTAMOV
      this.w_MVQTAUM1 = this.w_QTAUM1
      do case
        case this.w_OPERAZCL = "CF"
          * --- Carico prodotto finito
          vq_exec("..\COLA\EXE\QUERY\GSCO8BRL.VQR",this,"GENEORDI")
        case this.w_OPERAZCL = "SF"
          * --- Scarico fase
          vq_exec("..\COLA\EXE\QUERY\GSCO9BRL.VQR",this,"GENEORDI")
      endcase
    else
      * --- Select from MAT_PROD
      i_nConn=i_TableProp[this.MAT_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(MPSERIAL) as SERODL  from "+i_cTable+" MAT_PROD ";
            +" where MPSERIAL="+cp_ToStrODBC(this.w_SERDT)+" and MPNUMRIF=-30 and MPSERODL="+cp_ToStrODBC(this.w_SEROCL)+" and MPROWDOC="+cp_ToStrODBC(this.w_ROWORD)+"";
             ,"_Curs_MAT_PROD")
      else
        select MAX(MPSERIAL) as SERODL from (i_cTable);
         where MPSERIAL=this.w_SERDT and MPNUMRIF=-30 and MPSERODL=this.w_SEROCL and MPROWDOC=this.w_ROWORD;
          into cursor _Curs_MAT_PROD
      endif
      if used('_Curs_MAT_PROD')
        select _Curs_MAT_PROD
        locate for 1=1
        do while not(eof())
        this.w_HASMAT = !EMPTY(NVL(SERODL,""))
          select _Curs_MAT_PROD
          continue
        enddo
        use
      endif
      if this.w_HASMAT
        vq_exec("..\COLA\EXE\QUERY\GSCO3BSM.VQR",this,"GENEORDI")
      else
        vq_exec("..\COLA\EXE\QUERY\GSCO_BSM.VQR",this,"GENEORDI")
      endif
    endif
    * --- Switch codice di fase - prodotto finito
    * --- L'articolo di fase lo scarico, il prodotto finito lo devo caricare, setto la causale corretta da utilizzare
    if this.w_OPERAZ = "CS"
      do case
        case this.w_OPERAZCL = "CF"
          this.w_DELTESTATA = this.w_DELMASCF
          this.w_SEDOC = this.w_SEDOCCF
          * --- Carico prodotto finito
          this.w_MVTIPDOC = this.w_CAUCAR
        case this.w_OPERAZCL = "SF"
          this.w_DELTESTATA = this.w_DELMASSF
          this.w_SEDOC = this.w_SEDOCSF
          * --- Scarico fase
          this.w_MVTIPDOC = this.w_CAUSCA
      endcase
    else
      this.w_DELTESTATA = this.w_DELMASSM
      this.w_SEDOC = this.w_SEDOCSM
      * --- Scartico materiali
      this.w_MVTIPDOC = this.w_CAUSCA
    endif
    this.w_MVDATDOC = this.oParentObject.w_MVDATDOC
    this.w_MVDATDIV = this.oParentObject.w_MVDATREG
    this.w_MVDATREG = this.oParentObject.w_MVDATREG
    this.w_MVDATCIV = this.oParentObject.w_MVDATREG
    this.w_MVPRP = "NN"
    this.w_MVNUMDOC = 0
    this.w_MVFLPROV = "N"
    this.w_MVNUMREG = 0
    this.w_MVNUMEST = 0
    this.w_MVALFEST = Space(10)
    this.w_MVSERIAL = SPACE(10)
    this.w_MVCODESE = this.oParentObject.w_MVCODESE
    this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO;
        from (i_cTable) where;
            TDTIPDOC = this.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
      this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
      this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
      this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
      this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
      this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
      this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
      this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
      this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
      this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
      this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVTIPCON = IIF(this.w_MVFLINTE $ "CF", this.w_MVFLINTE, " ")
    this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
    this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
    if USED("GENEORDI")
      * --- Aggiorna nel Cursore i dati da Riportare nei Documenti
      SELECT GENEORDI
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PDSERIAL,""))
      this.w_OLCOEIMP = NVL(OLCOEIMP, 0)
      this.w_OLUNIMIS = NVL(PDUMORDI, "   ")
      this.w_OLQTAEVA = NVL(PDQTAMOV, 0)
      this.w_OLQTAEV1 = NVL(PDQTAMO1, 0)
      this.w_OLQTAUM1 = NVL(PDQTAUM1, 0)
      this.w_UM1 = NVL(ARUNMIS1, "   ")
      this.w_UM2 = NVL(ARUNMIS2, "   ")
      this.w_UM3 = NVL(CAUNIMIS, "   ")
      this.w_OPERAT = NVL(AROPERAT, " ")
      this.w_OPERA3 = NVL(CAOPERAT, " ")
      this.w_MOLTIP = NVL(ARMOLTIP, 0)
      this.w_MOLTI3 = NVL(CAMOLTIP, 0)
      if this.w_HASMAT and this.w_OPERAZ <> "CS"
        this.w_MATQTAUM1 = GENEORDI.QTAUM1
      else
        this.w_MATQTAUM1 = this.w_OLQTAUM1
      endif
      if OLFLELAB = "F" or this.w_HASMAT
        this.w_MVQTAMOV = cp_ROUND(NVL(GENEORDI.PDQTAORD, 0), 3)
        this.w_MVQTAUM1 = this.w_MVQTAMOV
      else
        this.w_MVQTAMOV = cp_ROUND(this.w_OLCOEIMP * this.w_QTASAL, 3)
        this.w_MVQTAUM1 = this.w_MVQTAMOV
      endif
      if this.w_OLUNIMIS<>this.w_UM1
        this.w_MVQTAUM1 = CALQTA(this.w_MVQTAMOV, this.w_OLUNIMIS,this.w_UM2, this.w_OPERAT, this.w_MOLTIP,"", "N", "",, this.w_UM3, this.w_OPERA3, this.w_MOLTI3)
        SELECT GENEORDI
      endif
      this.w_OLQTAEVA = this.w_MVQTAMOV
      this.w_OLQTAEV1 = this.w_MVQTAUM1
      SELECT GENEORDI
      this.w_PDSERIAL = GENEORDI.PDORDINE
      this.w_PDROWNUM = GENEORDI.PDROWNUM
      this.w_TIPART = GENEORDI.ARTIPART
      if not empty(nvl(GENEORDI.OLPROFAN," ")) and this.w_TIPART="PH"
        * --- Mi memorizzo la radice della catena del fantasma
        this.radice = GENEORDI.OLPROFAN
        * --- Select from GSCL_BRL
        do vq_exec with 'GSCL_BRL',this,'_Curs_GSCL_BRL','',.f.,.t.
        if used('_Curs_GSCL_BRL')
          select _Curs_GSCL_BRL
          locate for 1=1
          do while not(eof())
          this.w_QTATOT = nvl(_Curs_GSCL_BRL.QTAMOV,0)
            select _Curs_GSCL_BRL
            continue
          enddo
          use
        endif
        SELECT GENEORDI
        this.riga = recno()
        * --- Se l'articolo in esame � un fantasma (o figlio di un fantasma) deve essere consumato finch� ce n'� disponibile a magazzino, poi si passa 
        *     l'impegno ai suoi figli.
        * --- Verifico se la quantit� disponibile del fantasma � sufficiente a coprire l'impegno residuo
        if (NVL(GENEORDI.OLCOEIMP, 0)* iif(qtares<>0,qtares,this.w_QTASAL))>(GENEORDI.PDQTAORD-this.w_QTATOT) and GENEORDI.OLFLELAB $ "S-F"
          * --- La quantit� disponibile non � sufficiente, devo considerare anche i/il figlio
          * --- Quantit� che resta da evadere
          this.w_QTARES = max((GENEORDI.OLCOEIMP * iif(qtares<>0,qtares,this.w_QTASAL))-(GENEORDI.PDQTAORD-this.w_QTATOT),0)/GENEORDI.OLCOEIMP
          replace qtares with this.w_QTARES
          * --- Setto la corretta quantit� anche per tutti i figli
          update GENEORDI set OLFLELAB="F" ,PDQTAORD= iif(artipart="PH",min(this.w_qtares*GENEORDI.OLCOEIMP,PDQTAORD),this.w_qtares*GENEORDI.OLCOEIMP),QTARES=this.w_QTARES where OLPROFAN=alltrim(this.radice)+"."
          * --- Mi riposiziono sulla riga corretta
          SELECT GENEORDI 
 go this.riga
          if min(GENEORDI.OLCOEIMP * this.w_QTASAL,(GENEORDI.PDQTAORD-this.w_QTATOT))>0
            update GENEORDI set OLFLELAB="F" ,PDQTAORD=min(GENEORDI.OLCOEIMP * this.w_QTASAL,(PDQTAORD-this.w_QTATOT)) where OLPROFAN==alltrim(this.radice)
          else
            update GENEORDI set OLFLELAB="N" ,PDQTAORD=min(GENEORDI.OLCOEIMP * this.w_QTASAL,(PDQTAORD-this.w_QTATOT)) where OLPROFAN==alltrim(this.radice)
          endif
        else
          * --- La quantit� disponibile � sufficiente, non devo considerare anche i/il figlio
          update GENEORDI set OLFLELAB="N" where OLPROFAN=alltrim(this.radice)+"."
        endif
        SELECT GENEORDI 
 go this.riga
        this.w_COECONV = this.w_MVQTAUM1/this.w_MVQTAMOV
        if OLFLELAB = "F"
          * --- Fantasmi
          this.w_MVQTAMOV = cp_Round(GENEORDI.PDQTAORD,5)
          this.w_MVQTAUM1 = cp_Round(GENEORDI.PDQTAORD * this.w_COECONV,5)
        else
          this.w_MVQTAMOV = cp_Round(GENEORDI.OLCOEIMP*this.w_QTASAL,5)
          this.w_MVQTAUM1 = cp_Round(GENEORDI.OLCOEIMP*this.w_QTASAL*this.w_COECONV,5)
        endif
        this.w_OLQTAEVA = this.w_MVQTAMOV
        this.w_OLQTAEV1 = this.w_MVQTAUM1
      endif
      * --- Verifico Flag Evasione Lo accendo solo se si verifica la condizione e sono l'ultima riga che evade
      *     che evade il documento di origine (caso evasione ripetuta)
      SELECT "ROWSDOC" 
 GO TOP
      LOCATE FOR t_MVSERRIF=this.w_SERRIF AND t_MVROWRIF=this.w_ROWRIF and CPROWNUM>this.w_ROWORD
      if FOUND()
        this.w_OLFLEVAS = SPACE(1)
      else
        if this.w_HASMAT
          this.w_OLFLEVAS = IIF( this.w_OLQTAEV1>=this.w_MATQTAUM1 OR this.w_FLEVAS="S", "S", " ")
        else
          this.w_OLFLEVAS = IIF( this.w_OLQTAEV1>=this.w_OLQTAUM1 OR this.w_FLEVAS="S", "S", " ")
        endif
      endif
      SELECT GENEORDI
      REPLACE PDDATEVA WITH this.w_MVDATDOC, PDQTAORD WITH this.w_MVQTAMOV, PDQTAUM1 WITH this.w_MVQTAUM1
      if !this.w_HASMAT
        REPLACE PDQTAMOV WITH this.w_OLQTAEVA, PDQTAMO1 WITH this.w_OLQTAEV1, PDFLEVAS WITH this.w_OLFLEVAS
        REPLACE PDMAGWIP WITH this.w_CODMAG, DISMAG WITH this.w_WIPNETT
        * --- Inserisco il record in MAT_PROD
        this.w_ROWNUM_MAT = this.w_ROWNUM_MAT + 1
        this.w_ROWORD_MAT = this.w_ROWORD_MAT + 10
        * --- Insert into MAT_PROD
        i_nConn=i_TableProp[this.MAT_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_PROD_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MPSERIAL"+",MPNUMRIF"+",MPROWDOC"+",CPROWNUM"+",CPROWORD"+",MPCODICE"+",MPCODART"+",MPUNIMIS"+",MPCOEIMP"+",MPQTAMOV"+",MPQTAUM1"+",MPFLEVAS"+",MPCODMAG"+",MPSERODL"+",MPROWODL"+",MPQTAEVA"+",MPQTAEV1"+",MPKEYSAL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'MAT_PROD','MPSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(-30),'MAT_PROD','MPNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'MAT_PROD','MPROWDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM_MAT),'MAT_PROD','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD_MAT),'MAT_PROD','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDCODICE),'MAT_PROD','MPCODICE');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDCODART),'MAT_PROD','MPCODART');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDUMORDI),'MAT_PROD','MPUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.OLCOEIMP),'MAT_PROD','MPCOEIMP');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDQTAMOV),'MAT_PROD','MPQTAMOV');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDQTAUM1),'MAT_PROD','MPQTAUM1');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDFLEVAS),'MAT_PROD','MPFLEVAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'MAT_PROD','MPCODMAG');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDORDINE),'MAT_PROD','MPSERODL');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDROWNUM),'MAT_PROD','MPROWODL');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDQTAMOV),'MAT_PROD','MPQTAEVA');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDQTAUM1),'MAT_PROD','MPQTAEV1');
          +","+cp_NullLink(cp_ToStrODBC(GENEORDI.PDCODICE),'MAT_PROD','MPKEYSAL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MPSERIAL',this.w_SERIAL,'MPNUMRIF',-30,'MPROWDOC',this.w_ROWORD,'CPROWNUM',this.w_ROWNUM_MAT,'CPROWORD',this.w_ROWORD_MAT,'MPCODICE',GENEORDI.PDCODICE,'MPCODART',GENEORDI.PDCODART,'MPUNIMIS',GENEORDI.PDUMORDI,'MPCOEIMP',GENEORDI.OLCOEIMP,'MPQTAMOV',GENEORDI.PDQTAMOV,'MPQTAUM1',GENEORDI.PDQTAUM1,'MPFLEVAS',GENEORDI.PDFLEVAS)
          insert into (i_cTable) (MPSERIAL,MPNUMRIF,MPROWDOC,CPROWNUM,CPROWORD,MPCODICE,MPCODART,MPUNIMIS,MPCOEIMP,MPQTAMOV,MPQTAUM1,MPFLEVAS,MPCODMAG,MPSERODL,MPROWODL,MPQTAEVA,MPQTAEV1,MPKEYSAL &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,-30;
               ,this.w_ROWORD;
               ,this.w_ROWNUM_MAT;
               ,this.w_ROWORD_MAT;
               ,GENEORDI.PDCODICE;
               ,GENEORDI.PDCODART;
               ,GENEORDI.PDUMORDI;
               ,GENEORDI.OLCOEIMP;
               ,GENEORDI.PDQTAMOV;
               ,GENEORDI.PDQTAUM1;
               ,GENEORDI.PDFLEVAS;
               ,this.w_CODMAG;
               ,GENEORDI.PDORDINE;
               ,GENEORDI.PDROWNUM;
               ,GENEORDI.PDQTAMOV;
               ,GENEORDI.PDQTAUM1;
               ,GENEORDI.PDCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      ENDSCAN
      * --- Gestione dei fantasmi: elimino tutti i materiali figli di fantasma che non devo scaricare
      DELETE FROM GENEORDI WHERE OLFLELAB="N"
      * --- Lancia Batch di Generazione Documenti di Scarico
      do GSCO_BGD with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if USED("DOCGENE") and RECCOUNT("DOCGENE")>0
        this.w_CLADOC = "DI"
        this.w_FLVEAC = "V"
        this.w_DATMAT = g_DATMAT
        this.w_DOCSER = this.w_SERIAL
        Select DOCGENE 
 GO TOP 
 SCAN
        this.w_SERIAL = nvl(DOCGENE.SERDOC,"")
        this.w_GENROWDOC = nvl(DOCGENE.ROWDOC,0)
        this.w_GENROWMAT = nvl(DOCGENE.ROWMAT,0)
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ENDSCAN
        * --- La variabile w_SERIAL viene ripristinata al valore precedente in modo che non ci siano anomalie se ci sono pi� righe importate
        *     Azzero anche il contatore della riga di MAT_PROD
        this.w_SERIAL = this.w_DOCSER
        this.w_ROWNUM_MAT = 0
        this.w_ROWORD_MAT = 0
      endif
      USE IN SELECT("DOCGENE")
      if USED("MessErr") AND this.w_LNumErr>0
        if ah_YesNo("Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?","", alltrim(str(this.w_LNumErr,5,0)), )
          SELECT * FROM MessErr INTO CURSOR __TMP__
          CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
        endif
        do GSMD_SZM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Documento di Scarico Materiali Collegato
    * --- Aggiorna prima i saldi - Poi cancella dettaglio e testata
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
          +" where MVSERIAL="+cp_ToStrODBC(this.w_SEDOC)+"";
           ,"_Curs_DOC_DETT")
    else
      select * from (i_cTable);
       where MVSERIAL=this.w_SEDOC;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      * --- Storna Matricole: CONTROLLA CHE SIA L'ULTIMO MOVIMENTO
      this.w_STORNAMAT = False
      this.w_MTCODMAT = ""
      this.w_MSGMATRI = ""
      this.w_MVSERIAL = this.w_SEDOC
      this.w_CPROWNUM = _Curs_DOC_DETT.CPROWNUM
      this.w_NUMRIF = -20
      * --- Select from MOVIMATR
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MTKEYSAL,MTCODMAT,MT_SALDO  from "+i_cTable+" MOVIMATR ";
            +" where MTSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+" AND MTROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)+" AND MTNUMRIF="+cp_ToStrODBC(this.w_NUMRIF)+"";
             ,"_Curs_MOVIMATR")
      else
        select MTKEYSAL,MTCODMAT,MT_SALDO from (i_cTable);
         where MTSERIAL=this.w_MVSERIAL AND MTROWNUM=this.w_CPROWNUM AND MTNUMRIF=this.w_NUMRIF;
          into cursor _Curs_MOVIMATR
      endif
      if used('_Curs_MOVIMATR')
        select _Curs_MOVIMATR
        locate for 1=1
        do while not(eof())
        this.w_STORNAMAT = True
        this.w_MTCODMAT = iif(_Curs_MOVIMATR.MT_SALDO>0, _Curs_MOVIMATR.MTKEYSAL+_Curs_MOVIMATR.MTCODMAT, this.w_MTCODMAT)
          select _Curs_MOVIMATR
          continue
        enddo
        use
      endif
      if not empty(this.w_MTCODMAT)
        * --- Verifica se qualche matricola � stata usata
        this.w_MSGMATRI = ah_Msgformat("Impossibile stornare documento di scarico di conto lavoro collegato,%0Movimentato articolo: %1%0Matricola: %2",left(this.w_MTCODMAT,40), alltrim(right(this.w_MTCODMAT,40)) )
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MSGMATRI
        i_retcode = 'stop'
        return
      endif
      if this.w_STORNAMAT
        * --- Storna i saldi su MOVIMATR
        * --- Verifica se la matricola � stata nuovamente dichiarata in produzione
        this.w_MTCODMAT = ""
        this.w_MT__FLAG = " "
        this.w_AM_PRENO = 1
        * --- Select from GSCO_BAD
        do vq_exec with 'GSCO_BAD',this,'_Curs_GSCO_BAD','',.f.,.t.
        if used('_Curs_GSCO_BAD')
          select _Curs_GSCO_BAD
          locate for 1=1
          do while not(eof())
          this.w_MTCODMAT = _Curs_GSCO_BAD.AMKEYSAL+_Curs_GSCO_BAD.AMCODICE
            select _Curs_GSCO_BAD
            continue
          enddo
          use
        endif
        if not empty(this.w_MTCODMAT)
          * --- Impossibile cancellare Doc. di Scarico
          this.w_MSGMATRI = ah_Msgformat("Impossibile stornare movimento di magazzino di conto lavoro collegato,%0Dichiarato in produzione articolo: %1%0Matricola: %2", left(this.w_MTCODMAT,40), alltrim(right(this.w_MTCODMAT,40)) )
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MSGMATRI
          i_retcode = 'stop'
          return
        endif
        * --- Storna i saldi su MOVIMATR del movimento collegato
        this.w_AM_PRENO = 0
        this.w_MT__FLAG = "+"
        * --- Create temporary table DELTA
        i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCO_BAD.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.DELTA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Write into MOVIMATR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MOVIMATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"
          do vq_exec with 'GSCO_BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                  +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                  +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                  +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                  +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
              +i_ccchkf;
              +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                  +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                  +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                  +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                  +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
          +"MOVIMATR.MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
              +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                  +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                  +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
                  +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                  +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
              +"MT_SALDO";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"MOVIMATR.MT_SALDO-"+cp_ToStrODBC(1)+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                  +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                  +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                  +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                  +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
          +"MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                  +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                  +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
                  +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                  +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MT_SALDO =MT_SALDO- "+cp_ToStrODBC(1);
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Impossibile aggiornare dettaglio matricole'
          return
        endif
        * --- Write into MATRICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MATRICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="AMKEYSAL,AMCODICE"
          do vq_exec with 'GSCO1BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MATRICOL_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                  +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
              +i_ccchkf;
              +" from "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                  +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 set ";
          +"MATRICOL.AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
              +Iif(Empty(i_ccchkf),"",",MATRICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="MATRICOL.AMKEYSAL = t2.AMKEYSAL";
                  +" and "+"MATRICOL.AMCODICE = t2.AMCODICE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set (";
              +"AM_PRENO";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
                  +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set ";
          +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".AMKEYSAL = "+i_cQueryTable+".AMKEYSAL";
                  +" and "+i_cTable+".AMCODICE = "+i_cQueryTable+".AMCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Drop temporary table DELTA
        i_nIdx=cp_GetTableDefIdx('DELTA')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('DELTA')
        endif
      endif
      this.w_MVKEYSAL = NVL(_Curs_DOC_DETT.MVKEYSAL, "")
      this.w_MVCODART = NVL(_Curs_DOC_DETT.MVCODART, "")
      this.w_MVCODMAG = NVL(_Curs_DOC_DETT.MVCODMAG,"")
      this.w_MVQTAMOV = NVL(_Curs_DOC_DETT.MVQTAMOV, 0)
      this.w_MVQTAUM1 = NVL(_Curs_DOC_DETT.MVQTAUM1, 0)
      this.w_MVCODODL = NVL(_Curs_DOC_DETT.MVCODODL,"")
      this.w_MVCODCOM = _Curs_DOC_DETT.MVCODCOM
      this.w_MVRIGMAT = NVL(_Curs_DOC_DETT.MVRIGMAT, 0)
      this.w_FLCAR = IIF(_Curs_DOC_DETT.MVFLCASC="-","+",IIF(_Curs_DOC_DETT.MVFLCASC="+","-"," "))
      this.w_FLORD = IIF(_Curs_DOC_DETT.MVFLORDI="-","+",IIF(_Curs_DOC_DETT.MVFLORDI="+","-"," "))
      this.w_FLIMP = IIF(_Curs_DOC_DETT.MVFLIMPE="-","+",IIF(_Curs_DOC_DETT.MVFLIMPE="+","-"," "))
      this.w_FLRIS = IIF(_Curs_DOC_DETT.MVFLRISE="-","+",IIF(_Curs_DOC_DETT.MVFLRISE="+","-"," "))
      if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG)
        * --- Attenzione: La Causale di Scarico non prevede Movimenti Collegati
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCAR,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Saldi commessa
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_MVCODCOM
          endif
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCAR,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDICOM'
            return
          endif
        endif
      endif
      this.w_MVCODLOT = NVL(_Curs_DOC_DETT.MVCODLOT,"")
      this.w_MVCODUBI = NVL(_Curs_DOC_DETT.MVCODUBI,"")
      if g_PERUBI="S" OR g_PERLOT="S" and !empty(this.w_MVCODLOT)
        GSMD_BRL (this, this.w_SEDOC , "V" , "-" , this.w_CPROWNUM ,.T. )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Eventuale aggiornamento lista materiali stimati per ripristino impegno
      if NOT EMPTY(this.w_MVCODODL) AND this.w_MVRIGMAT>0
        * --- Legge dati ODL
        this.w_OLQTAEVA = 0
        this.w_OLQTAEV1 = 0
        this.w_FLORDI = " "
        this.w_FLIMPE = " "
        this.w_FLRISE = " "
        * --- Read from ODL_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLCODMAG,OLQTAEVA,OLQTAEV1,OLQTASAL,OLQTAMOV,OLQTAUM1,OLFLORDI,OLFLIMPE,OLFLRISE,OLFLPREV"+;
            " from "+i_cTable+" ODL_DETT where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLCODMAG,OLQTAEVA,OLQTAEV1,OLQTASAL,OLQTAMOV,OLQTAUM1,OLFLORDI,OLFLIMPE,OLFLRISE,OLFLPREV;
            from (i_cTable) where;
                OLCODODL = this.w_MVCODODL;
                and CPROWNUM = this.w_MVRIGMAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLCODMAG = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
          this.w_OLQTAEVA = NVL(cp_ToDate(_read_.OLQTAEVA),cp_NullValue(_read_.OLQTAEVA))
          this.w_OLQTAEV1 = NVL(cp_ToDate(_read_.OLQTAEV1),cp_NullValue(_read_.OLQTAEV1))
          this.w_OLQTASAL = NVL(cp_ToDate(_read_.OLQTASAL),cp_NullValue(_read_.OLQTASAL))
          this.w_OLQTAMOV = NVL(cp_ToDate(_read_.OLQTAMOV),cp_NullValue(_read_.OLQTAMOV))
          this.w_OLQTAUM1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
          this.w_FLORDI = NVL(cp_ToDate(_read_.OLFLORDI),cp_NullValue(_read_.OLFLORDI))
          this.w_FLIMPE = NVL(cp_ToDate(_read_.OLFLIMPE),cp_NullValue(_read_.OLFLIMPE))
          this.w_FLRISE = NVL(cp_ToDate(_read_.OLFLRISE),cp_NullValue(_read_.OLFLRISE))
          this.w_OLFLPREV = NVL(cp_ToDate(_read_.OLFLPREV),cp_NullValue(_read_.OLFLPREV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Ho cancellato un rientro. E' come se dovessi restituire la merce al terzista ...
        this.w_AVMQTA = 0
        this.w_AVMQT1 = 0
        this.w_AVFLEV = "N"
        * --- Aggiorno ODL_DETT
        * --- Select from MAT_PROD
        i_nConn=i_TableProp[this.MAT_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select sum(MPQTAEVA) as SQTAEVA,sum(MPQTAEV1) as SQTAEV1,max(MPFLEVAS) as SFLEVAS  from "+i_cTable+" MAT_PROD ";
              +" where MPNUMRIF=-30 AND MPSERODL="+cp_ToStrODBC(this.w_MVCODODL)+" AND MPROWODL="+cp_ToStrODBC(this.w_MVRIGMAT)+" AND (MPSERIAL<>"+cp_ToStrODBC(this.w_SERIAL)+" OR MPSERIAL="+cp_ToStrODBC(this.w_SERIAL)+" AND MPROWDOC<>"+cp_ToStrODBC(this.w_ROWORD)+")";
               ,"_Curs_MAT_PROD")
        else
          select sum(MPQTAEVA) as SQTAEVA,sum(MPQTAEV1) as SQTAEV1,max(MPFLEVAS) as SFLEVAS from (i_cTable);
           where MPNUMRIF=-30 AND MPSERODL=this.w_MVCODODL AND MPROWODL=this.w_MVRIGMAT AND (MPSERIAL<>this.w_SERIAL OR MPSERIAL=this.w_SERIAL AND MPROWDOC<>this.w_ROWORD);
            into cursor _Curs_MAT_PROD
        endif
        if used('_Curs_MAT_PROD')
          select _Curs_MAT_PROD
          locate for 1=1
          do while not(eof())
          this.w_AVMQTA = NVL(SQTAEVA, 0)
          this.w_AVMQT1 = NVL(SQTAEV1, 0)
          this.w_AVFLEV = NVL(SFLEVAS, "N")
            select _Curs_MAT_PROD
            continue
          enddo
          use
        endif
        if this.w_WIPNETT="S"
          * --- Se il magazzino del terzista � nettificabile, quando la restituisco la devo anche re-impegnare
          this.w_OLQTAEVA = this.w_AVMQTA
          this.w_OLQTAEV1 = this.w_AVMQT1
          this.w_OLFLEVAS = iif( this.w_OLQTAEV1>=this.w_OLQTAUM1, "S", " ")
          this.w_TmpN = this.w_OLQTASAL
          this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S", 0, this.w_OLQTAUM1-this.w_OLQTAEV1)
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
            +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
            +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
            +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                   )
          else
            update (i_cTable) set;
                OLQTAEVA = this.w_OLQTAEVA;
                ,OLQTAEV1 = this.w_OLQTAEV1;
                ,OLFLEVAS = this.w_OLFLEVAS;
                ,OLQTASAL = this.w_OLQTASAL;
                &i_ccchkf. ;
             where;
                OLCODODL = this.w_MVCODODL;
                and CPROWNUM = this.w_MVRIGMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorna Saldo
          this.w_OLQTASAL = this.w_OLQTASAL - this.w_TmpN
          if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTRPER = &i_cOp1.;
                  ,SLQTOPER = &i_cOp2.;
                  ,SLQTIPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_MVKEYSAL;
                  and SLCODMAG = this.w_OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Saldi commessa
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_MVCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_MVCODCOM,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_MVCODCOM
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTRPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_MVKEYSAL;
                    and SCCODMAG = this.w_OLCODMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDICOM (1)'
                return
              endif
            endif
          endif
        else
          * --- Se il magazzino del terzista non � nettificabile, occorre verificare se il movimento in cancellazione � nato
          * --- da un DDT che evadeva completamente il documento ORDINE (e OCL) di origine.
          * --- In tal caso ripristina l'impegnato su ODL_DETT
          if this.w_FLEVAS="S" AND this.w_OLFLPREV<>"S"
            * --- Aggiornamento SALDI - Storna vecchie quantit�
            this.w_OLQTASAL = this.w_OLQTAUM1-this.w_OLQTAEV1
            if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
                +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTRPER = &i_cOp1.;
                    ,SLQTOPER = &i_cOp2.;
                    ,SLQTIPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_MVKEYSAL;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Saldi commessa
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_MVCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_MVCODCOM,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_MVCODCOM
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
                  +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTRPER = &i_cOp1.;
                      ,SCQTOPER = &i_cOp2.;
                      ,SCQTIPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_MVKEYSAL;
                      and SCCODMAG = this.w_OLCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura SALDICOM (1)'
                  return
                endif
              endif
            endif
            * --- Aggiorna dettaglio ODL
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLEVAS');
              +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                     )
            else
              update (i_cTable) set;
                  OLFLEVAS = " ";
                  ,OLQTASAL = this.w_OLQTASAL;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_MVCODODL;
                  and CPROWNUM = this.w_MVRIGMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if this.w_OLDCOM
          * --- Storna il Pegging <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          this.w_QTADAB = 0
          * --- Read from ODL_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLQTAPR1,OLCODMAG,OLMAGPRE"+;
              " from "+i_cTable+" ODL_DETT where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLQTAPR1,OLCODMAG,OLMAGPRE;
              from (i_cTable) where;
                  OLCODODL = this.w_MVCODODL;
                  and CPROWNUM = this.w_MVRIGMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_QTAPR1 = NVL(cp_ToDate(_read_.OLQTAPR1),cp_NullValue(_read_.OLQTAPR1))
            this.w_MAGTRA = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
            this.w_MAGRIF = NVL(cp_ToDate(_read_.OLMAGPRE),cp_NullValue(_read_.OLMAGPRE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_QTAPR1>0
            * --- Sottraggo alla quantit� trasferita per l'ODL quella gi� utilizzata
            this.w_QTAPR1 = max(this.w_QTAPR1,0)
            this.w_QTADAB = this.w_QTADAB+this.w_QTAPR1
          endif
          this.w_PEQTAUM1 = min(this.w_MVQTAUM1,this.w_QTADAB)
          this.w_PEQTAOLD = this.w_PEQTAUM1
          this.w_RIFMAG = " Magazz.: " + this.w_MAGTRA
          * --- Primo giro quantit� abbinata al WIP terzista
          * --- Select from PEG_SELI
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB  from "+i_cTable+" PEG_SELI ";
                +" where PERIFODL="+cp_ToStrODBC(this.w_MVCODODL)+" AND PERIGORD="+cp_ToStrODBC(this.w_MVRIGMAT)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_RIFMAG)+"";
                 ,"_Curs_PEG_SELI")
          else
            select PESERIAL,PEQTAABB from (i_cTable);
             where PERIFODL=this.w_MVCODODL AND PERIGORD=this.w_MVRIGMAT AND PETIPRIF="M" AND PESERODL=this.w_RIFMAG;
              into cursor _Curs_PEG_SELI
          endif
          if used('_Curs_PEG_SELI')
            select _Curs_PEG_SELI
            locate for 1=1
            do while not(eof())
            * --- Se la select non restituisce nulla fare insert?
            * --- Write into PEG_SELI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(_Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                  +i_ccchkf ;
              +" where ";
                  +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                     )
            else
              update (i_cTable) set;
                  PEQTAABB = _Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1;
                  &i_ccchkf. ;
               where;
                  PESERIAL = _Curs_PEG_SELI.PESERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_PEQTAUM1 = 0
              select _Curs_PEG_SELI
              continue
            enddo
            use
          endif
          this.w_PEQTAUM1 = this.w_MVQTAUM1-this.w_PEQTAOLD
          this.w_RIFMAG = " Magazz.: " + this.w_MAGRIF
          * --- Secondo giro, quantit� abbinata a magazzino originale di riferimento (solo materiale non trasferito)
          if this.w_PEQTAUM1>0
            * --- Select from PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2],.t.,this.PEG_SELI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PESERIAL,PEQTAABB  from "+i_cTable+" PEG_SELI ";
                  +" where PERIFODL="+cp_ToStrODBC(this.w_MVCODODL)+" AND PERIGORD="+cp_ToStrODBC(this.w_MVRIGMAT)+" AND PETIPRIF='M' AND PESERODL="+cp_ToStrODBC(this.w_RIFMAG)+"";
                   ,"_Curs_PEG_SELI")
            else
              select PESERIAL,PEQTAABB from (i_cTable);
               where PERIFODL=this.w_MVCODODL AND PERIGORD=this.w_MVRIGMAT AND PETIPRIF="M" AND PESERODL=this.w_RIFMAG;
                into cursor _Curs_PEG_SELI
            endif
            if used('_Curs_PEG_SELI')
              select _Curs_PEG_SELI
              locate for 1=1
              do while not(eof())
              * --- Se la select non restituisce nulla fare insert?
              * --- Write into PEG_SELI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PEQTAABB ="+cp_NullLink(cp_ToStrODBC(_Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +i_ccchkf ;
                +" where ";
                    +"PESERIAL = "+cp_ToStrODBC(_Curs_PEG_SELI.PESERIAL);
                       )
              else
                update (i_cTable) set;
                    PEQTAABB = _Curs_PEG_SELI.PEQTAABB+this.w_PEQTAUM1;
                    &i_ccchkf. ;
                 where;
                    PESERIAL = _Curs_PEG_SELI.PESERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_PEQTAUM1 = 0
                select _Curs_PEG_SELI
                continue
              enddo
              use
            endif
          endif
        endif
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    * --- Elimina Documento
    if this.w_DELTESTATA="N"
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.DOC_MAST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            +" where MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC)+"";
            )
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    GSAR_BED(this,this.w_SEDOC, this.w_NUMRIF)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_DELTESTATA="N"
      * --- Insert into DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC)+"",this.DOC_MAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se il riferimento alla riga del DDT (MVROWDDT) � zero allora la aggiorno
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVROWDDT"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVROWDDT;
        from (i_cTable) where;
            MVSERIAL = this.w_SEDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVROWDDT = NVL(cp_ToDate(_read_.MVROWDDT),cp_NullValue(_read_.MVROWDDT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_MVROWDDT= 0
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVROWDDT ="+cp_NullLink(cp_ToStrODBC(this.w_MVROWDDT),'DOC_MAST','MVROWDDT');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
               )
      else
        update (i_cTable) set;
            MVROWDDT = this.w_MVROWDDT;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SEDOC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Variazione Documento di Scarico Materiali Collegato
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
          +" where MVSERIAL="+cp_ToStrODBC(this.w_SEDOC)+"";
           ,"_Curs_DOC_DETT")
    else
      select * from (i_cTable);
       where MVSERIAL=this.w_SEDOC;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = NVL(_Curs_DOC_DETT.CPROWNUM, 0)
      this.w_MVKEYSAL = NVL(_Curs_DOC_DETT.MVKEYSAL, "")
      this.w_MVCODART = NVL(_Curs_DOC_DETT.MVCODART, "")
      this.w_MVCODMAG = NVL(_Curs_DOC_DETT.MVCODMAG,"")
      this.w_MVQTAMOV = NVL(_Curs_DOC_DETT.MVQTAMOV, 0)
      this.w_MVQTAUM1 = NVL(_Curs_DOC_DETT.MVQTAUM1, 0)
      this.w_MVCODODL = NVL(_Curs_DOC_DETT.MVCODODL,"")
      this.w_MVRIGMAT = NVL(_Curs_DOC_DETT.MVRIGMAT, 0)
      this.w_MVPREZZO = NVL(_Curs_DOC_DETT.MVPREZZO, 0)
      this.w_MVSCONT1 = NVL(_Curs_DOC_DETT.MVSCONT1, 0)
      this.w_MVSCONT2 = NVL(_Curs_DOC_DETT.MVSCONT2, 0)
      this.w_MVSCONT3 = NVL(_Curs_DOC_DETT.MVSCONT3, 0)
      this.w_MVSCONT4 = NVL(_Curs_DOC_DETT.MVSCONT4, 0)
      this.w_MVVALRIG = NVL(_Curs_DOC_DETT.MVVALRIG, 0)
      this.w_MVVALMAG = NVL(_Curs_DOC_DETT.MVVALMAG, 0)
      this.w_MVIMPNAZ = NVL(_Curs_DOC_DETT.MVIMPNAZ, 0)
      this.w_MVCODCOM = _Curs_DOC_DETT.MVCODCOM
      * --- Storna, da saldi, vecchia quantita' movimentata <<<<<<<<<<<<<<<<<<<<<<<<<<<
      this.w_FLCAR = IIF(_Curs_DOC_DETT.MVFLCASC="-","+",IIF(_Curs_DOC_DETT.MVFLCASC="+","-"," "))
      this.w_FLORD = IIF(_Curs_DOC_DETT.MVFLORDI="-","+",IIF(_Curs_DOC_DETT.MVFLORDI="+","-"," "))
      this.w_FLIMP = IIF(_Curs_DOC_DETT.MVFLIMPE="-","+",IIF(_Curs_DOC_DETT.MVFLIMPE="+","-"," "))
      this.w_FLRIS = IIF(_Curs_DOC_DETT.MVFLRISE="-","+",IIF(_Curs_DOC_DETT.MVFLRISE="+","-"," "))
      if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG)
        * --- Attenzione: La Causale di Scarico non prevede Movimenti Collegati
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCAR,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Saldi commessa
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_MVCODCOM
          endif
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCAR,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDICOM (1)'
            return
          endif
        endif
        * --- Calcola e aggiorna saldi per nuova quantita' movimentata
        this.w_MVQTAMOV = cp_ROUND((this.w_MVQTAMOV * this.w_QTAMOV) / this.w_QTAPRE, 3)
        this.w_MVQTAUM1 = cp_ROUND((this.w_MVQTAUM1 * this.w_QTAMOV) / this.w_QTAPRE, 3)
        * --- Aggiorna saldi
        this.w_FLCAR = NVL(_Curs_DOC_DETT.MVFLCASC, " ")
        this.w_FLORD = NVL(_Curs_DOC_DETT.MVFLORDI, " ")
        this.w_FLIMP = NVL(_Curs_DOC_DETT.MVFLIMPE, " ")
        this.w_FLRIS = NVL(_Curs_DOC_DETT.MVFLRISE, " ")
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCAR,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Saldi commessa
        if this.w_SALCOM="S"
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCAR,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDICOM (1)'
            return
          endif
        endif
      endif
      if this.w_MVQTAMOV<>0
        this.w_VALUNI = this.w_MVPREZZO * (1+this.w_MVSCONT1/100) * (1+this.w_MVSCONT2/100) * (1+this.w_MVSCONT3/100) * (1+this.w_MVSCONT4/100)
        this.w_TmpN = this.w_MVVALRIG
        this.w_MVVALRIG = cp_ROUND(this.w_MVQTAMOV * this.w_VALUNI, this.w_DECTOT)
        if this.w_TmpN<>0
          this.w_MVVALMAG = cp_ROUND((this.w_MVVALMAG * this.w_MVVALRIG) / this.w_TmpN, this.w_DECTOT)
          this.w_MVIMPNAZ = (this.w_MVIMPNAZ * this.w_MVVALRIG) / this.w_TmpN
        endif
      endif
      * --- Eventuale aggiornamento lista materiali stimati per ripristino impegno
      if .f.
        if NOT EMPTY(this.w_MVCODODL) AND this.w_MVRIGMAT>0
          * --- Legge dati ODL
          this.w_OLQTAEVA = 0
          this.w_OLQTAEV1 = 0
          this.w_FLORDI = " "
          this.w_FLIMPE = " "
          this.w_FLRISE = " "
          * --- Read from ODL_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLCODMAG,OLQTAEVA,OLQTAEV1,OLQTASAL,OLQTAMOV,OLQTAUM1,OLFLORDI,OLFLIMPE,OLFLRISE,OLFLPREV,OLFLEVAS"+;
              " from "+i_cTable+" ODL_DETT where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLCODMAG,OLQTAEVA,OLQTAEV1,OLQTASAL,OLQTAMOV,OLQTAUM1,OLFLORDI,OLFLIMPE,OLFLRISE,OLFLPREV,OLFLEVAS;
              from (i_cTable) where;
                  OLCODODL = this.w_MVCODODL;
                  and CPROWNUM = this.w_MVRIGMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OLCODMAG = NVL(cp_ToDate(_read_.OLCODMAG),cp_NullValue(_read_.OLCODMAG))
            this.w_OLQTAEVA = NVL(cp_ToDate(_read_.OLQTAEVA),cp_NullValue(_read_.OLQTAEVA))
            this.w_OLQTAEV1 = NVL(cp_ToDate(_read_.OLQTAEV1),cp_NullValue(_read_.OLQTAEV1))
            this.w_OLQTASAL = NVL(cp_ToDate(_read_.OLQTASAL),cp_NullValue(_read_.OLQTASAL))
            this.w_OLQTAMOV = NVL(cp_ToDate(_read_.OLQTAMOV),cp_NullValue(_read_.OLQTAMOV))
            this.w_OLQTAUM1 = NVL(cp_ToDate(_read_.OLQTAUM1),cp_NullValue(_read_.OLQTAUM1))
            this.w_FLORDI = NVL(cp_ToDate(_read_.OLFLORDI),cp_NullValue(_read_.OLFLORDI))
            this.w_FLIMPE = NVL(cp_ToDate(_read_.OLFLIMPE),cp_NullValue(_read_.OLFLIMPE))
            this.w_FLRISE = NVL(cp_ToDate(_read_.OLFLRISE),cp_NullValue(_read_.OLFLRISE))
            this.w_OLFLPREV = NVL(cp_ToDate(_read_.OLFLPREV),cp_NullValue(_read_.OLFLPREV))
            this.w_OLFLEVAS = NVL(cp_ToDate(_read_.OLFLEVAS),cp_NullValue(_read_.OLFLEVAS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Ho cancellato un rientro. E' come se dovessi restituire la merce al terzista ...
          this.w_AVMQTA = 0
          this.w_AVMQT1 = 0
          this.w_AVFLEV = "N"
          * --- Aggiorno ODL_DETT
          * --- Select from MAT_PROD
          i_nConn=i_TableProp[this.MAT_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select sum(MPQTAEVA) as SQTAEVA,sum(MPQTAEV1) as SQTAEV1,max(MPFLEVAS) as SFLEVAS  from "+i_cTable+" MAT_PROD ";
                +" where MPNUMRIF=-30 AND MPSERODL="+cp_ToStrODBC(this.w_MVCODODL)+" AND MPROWODL="+cp_ToStrODBC(this.w_MVRIGMAT)+" AND MPSERIAL<>"+cp_ToStrODBC(this.w_DPSERIAL)+"";
                 ,"_Curs_MAT_PROD")
          else
            select sum(MPQTAEVA) as SQTAEVA,sum(MPQTAEV1) as SQTAEV1,max(MPFLEVAS) as SFLEVAS from (i_cTable);
             where MPNUMRIF=-30 AND MPSERODL=this.w_MVCODODL AND MPROWODL=this.w_MVRIGMAT AND MPSERIAL<>this.w_DPSERIAL;
              into cursor _Curs_MAT_PROD
          endif
          if used('_Curs_MAT_PROD')
            select _Curs_MAT_PROD
            locate for 1=1
            do while not(eof())
            this.w_AVMQTA = NVL(SQTAEVA, 0)
            this.w_AVMQT1 = NVL(SQTAEV1, 0)
            this.w_AVFLEV = NVL(SFLEVAS, "N")
              select _Curs_MAT_PROD
              continue
            enddo
            use
          endif
          if this.w_WIPNETT = "S"
            * --- Se il magazzino del terzista � nettificabile, quando la restituisco la devo anche re-impegnare
            this.w_OLQTAEVA = this.w_AVMQTA
            this.w_OLQTAEV1 = this.w_AVMQT1
            this.w_OLFLEVAS = iif( this.w_OLQTAEV1>=this.w_OLQTAUM1, "S", " ")
            this.w_TmpN = this.w_OLQTASAL
            this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S", 0, this.w_OLQTAUM1-this.w_OLQTAEV1)
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
              +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
              +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
              +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                     )
            else
              update (i_cTable) set;
                  OLQTAEVA = this.w_OLQTAEVA;
                  ,OLQTAEV1 = this.w_OLQTAEV1;
                  ,OLFLEVAS = this.w_OLFLEVAS;
                  ,OLQTASAL = this.w_OLQTASAL;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_MVCODODL;
                  and CPROWNUM = this.w_MVRIGMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Aggiorna Saldo
            this.w_OLQTASAL = this.w_OLQTASAL - this.w_TmpN
            if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
                +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTRPER = &i_cOp1.;
                    ,SLQTOPER = &i_cOp2.;
                    ,SLQTIPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_MVKEYSAL;
                    and SLCODMAG = this.w_OLCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            * --- Se il magazzino del terzista non � nettificabile, occorre verificare se il movimento in cancellazione � nato
            * --- da un DDT che evadeva completamente il documento ORDINE (e OCL) di origine.
            * --- In tal caso ripristina l'impegnato su ODL_DETT
            if this.w_FLEVAS="S"
              if this.w_OLFLEVAS<>"S"
                * --- Aggiornamento SALDI - Storna vecchie quantit�
                this.w_OLFLORDI = IIF(this.w_FLORDI="+", "-", IIF(this.w_FLORDI="-", "+", " "))
                this.w_OLFLIMPE = IIF(this.w_FLIMPE="+", "-", IIF(this.w_FLIMPE="-", "+", " "))
                this.w_OLFLRISE = IIF(this.w_FLRISE="+", "-", IIF(this.w_FLRISE="-", "+", " "))
                if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0
                  * --- Write into SALDIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_OLFLRISE,'SLQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_OLFLORDI,'SLQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                    i_cOp3=cp_SetTrsOp(this.w_OLFLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
                    +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
                    +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                        +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                           )
                  else
                    update (i_cTable) set;
                        SLQTRPER = &i_cOp1.;
                        ,SLQTOPER = &i_cOp2.;
                        ,SLQTIPER = &i_cOp3.;
                        &i_ccchkf. ;
                     where;
                        SLCODICE = this.w_MVKEYSAL;
                        and SLCODMAG = this.w_OLCODMAG;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                * --- Aggiorna dettaglio ODL
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
                  +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                         )
                else
                  update (i_cTable) set;
                      OLFLEVAS = "S";
                      ,OLQTASAL = 0;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_MVCODODL;
                      and CPROWNUM = this.w_MVRIGMAT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              * --- La nuova riga non evade, mentra la vecchia evadeva.
              if this.w_OLDFLERIF="S" AND this.w_OLFLPREV<>"S"
                * --- Aggiornamento SALDI - Storna vecchie quantit�
                this.w_OLQTASAL = this.w_OLQTAUM1-this.w_OLQTAEV1
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
                  +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTRPER = &i_cOp1.;
                      ,SLQTOPER = &i_cOp2.;
                      ,SLQTIPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_MVKEYSAL;
                      and SLCODMAG = this.w_OLCODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Aggiorna dettaglio ODL
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLEVAS');
                  +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_MVCODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIGMAT);
                         )
                else
                  update (i_cTable) set;
                      OLFLEVAS = " ";
                      ,OLQTASAL = this.w_OLQTASAL;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_MVCODODL;
                      and CPROWNUM = this.w_MVRIGMAT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
        endif
      endif
      * --- Aggiorna Riga Documento
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
        +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
        +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
        +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
        +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               )
      else
        update (i_cTable) set;
            MVVALRIG = this.w_MVVALRIG;
            ,MVVALMAG = this.w_MVVALMAG;
            ,MVIMPNAZ = this.w_MVIMPNAZ;
            ,MVQTAMOV = this.w_MVQTAMOV;
            ,MVQTAUM1 = this.w_MVQTAUM1;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SEDOC;
            and CPROWNUM = this.w_CPROWNUM;
            and MVNUMRIF = this.w_NUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Lotti/Ubicazioni/Matricole, ripresa da GSMD_BVM
    this.w_DATDOC = cp_CharToDate("  -  -  ")
    this.w_DATREG = cp_CharToDate("  -  -  ")
    this.w_NUMDOC = 0
    this.w_ALFDOC = Space(10)
    this.w_CAUDOC = SPACE(5)
    this.w_DESDOC = SPACE(40)
    this.w_TIPCON = " "
    this.w_CODCON = SPACE(15)
    vq_exec("..\COLA\EXE\QUERY\GSCO_QDP.VQR", this,"AGGIORNA")
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON;
        from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_CAUDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDDESDOC"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDDESDOC;
        from (i_cTable) where;
            TDTIPDOC = this.w_CAUDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESDOC = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROV = .F.
    if USED("AGGIORNA")
      * --- Azzerro Archivi di Appoggio
      * --- Delete from AGG_LOTT
      i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione AGG_LOTT'
        return
      endif
      * --- Delete from SPL_LOTT
      i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              SPSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione SPL_LOTT'
        return
      endif
      * --- Inserisco Dati in Archivi di Appoggio
      ah_Msg("Fase 1: inserimento dati...",.T.)
      SELECT AGGIORNA
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(CPROWNUM, 0)>0
        this.w_DPSERIAL = this.oParentObject.w_AVASERIA
        this.w_CPROWNUM = CPROWNUM
        this.w_MVNUMRIF = MVNUMRIF
        this.w_CPROWORD = CPROWORD
        this.w_MVCODICE = NVL(MVCODICE," ")
        this.w_MVCODART = NVL(MVCODART, " ")
        this.w_MVKEYSAL = NVL(MVKEYSAL, " ")
        this.w_MVDESART = NVL(MVDESART," ")
        this.w_MVUNIMIS = NVL(MVUNIMIS," ")
        this.w_MVQTAMOV = NVL(MVQTAMOV, 0)
        this.w_MVQTAUM1 = NVL(MVQTAUM1, 0)
        this.w_MVCODMAG = NVL(MVCODMAG," ")
        this.w_MVCODMAT = NVL(MVCODMAT," ")
        this.w_MVCODLOT = NVL(MVCODLOT," ")
        this.w_MVCODUBI = NVL(MVCODUBI," ")
        this.w_MVCODUB2 = NVL(MVCODUB2," ")
        this.w_MVFLCASC = NVL(MVFLCASC," ")
        this.w_MVFLRISE = NVL(MVFLRISE," ")
        this.w_MVF2CASC = NVL(MVF2CASC," ")
        this.w_MVF2RISE = NVL(MVF2RISE," ")
        this.w_MVFLUBIC = NVL(FLUBIC, " ")
        this.w_MVFLUBI2 = NVL(FLUBI2, " ")
        this.w_MVARTLOT = NVL(FLLOTT, " ")
        this.w_MVSERRIF = NVL(MVSERRIF, " ")
        this.w_MVROWRIF = NVL(MVROWRIF, 0)
        this.w_MVFLLOTT = IIF((g_PERLOT="S" AND this.w_MVARTLOT$ "SC") OR (g_PERUBI="S" AND this.w_MVFLUBIC="S"), LEFT(ALLTRIM(this.w_MVFLCASC)+IIF(this.w_MVFLRISE="+", "-", IIF(this.w_MVFLRISE="-", "+", " ")), 1), " ")
        this.w_MVF2LOTT = IIF((g_PERLOT="S" AND this.w_MVARTLOT$ "SC") OR (g_PERUBI="S" AND this.w_MVFLUBI2="S"), LEFT(ALLTRIM(this.w_MVF2CASC)+IIF(this.w_MVF2RISE="+", "-", IIF(this.w_MVF2RISE="-", "+", " ")), 1), " ")
        this.w_MVFLELAN = NVL(MVFLELAN," ")
        this.w_MV_SEGNO = NVL(MV_SEGNO," ")
        this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
        if NOT EMPTY(this.w_MVCODICE) AND this.w_MVQTAUM1>0
          * --- Try
          local bErr_049FC3E8
          bErr_049FC3E8=bTrsErr
          this.Try_049FC3E8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_049FC3E8
          * --- End
        endif
        * --- Temporaneo per split lotti per movimenti di scarico da produzione
        vq_exec("..\COLA\EXE\QUERY\GSCL2BRL.VQR", this,"tmpsplot")
        if used("spllott") and reccount("spllott")>0
           Select tmpsplot 
 scan 
 scatter memvar 
 Select spllott 
 append blank 
 gather memvar 
 endscan
        else
          * --- Il cursore potrebbe esistere ed essere vuoto, per sicurezza verifico se lo devo droppare
          if used("spllott")
            select spllott 
 use
          endif
          select * from tmpsplot into cursor spllott order by 1,2
          cur=wrcursor("spllott")
        endif
        * --- Cancello le righe che andr� ad aggiornare dal cursore MATRERRO
        if USED("MATRERRO")
          Delete from matrerro where alltrim(matrerro.serdoc)+alltrim(str(matrerro.rownum)) in (select alltrim(spserial)+alltrim(str(sproword)) from spllott)
          =wrcursor("MATRERRO") 
 Select "MATRERRO" 
 PACK
          if RECCOUNT("MATRERRO")=0
            USE IN SELECT("MATRERRO")
          endif
        endif
        * --- Rilascio il temporaneo di appoggio
        if used("tmpsplot")
          select tmpsplot 
 use
        endif
        SELECT AGGIORNA
        ENDSCAN
      endif
      if this.w_TROV=.T.
        if (g_PERLOT="S" OR g_PERUBI="S")
          * --- Inizializzo la variabile di riferimento alla riga ogni volta che cambio documento
          this.w_OLDROW = 0
          * --- Try
          local bErr_049D0248
          bErr_049D0248=bTrsErr
          this.Try_049D0248()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg(i_ErrMsg,,"")
          endif
          bTrsErr=bTrsErr or bErr_049D0248
          * --- End
          * --- Chiude i Cursori che non servono piu'
          if USED("AGGIORNA")
            SELECT AGGIORNA
            USE
          endif
        else
          ah_Msg("Fase 2: aggiornamento matricole...",.T.)
          * --- Aggiungo righe matricole alle righe documento aggiornate
          * --- Try
          local bErr_049C3BB8
          bErr_049C3BB8=bTrsErr
          this.Try_049C3BB8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg(i_ErrMsg,,"")
          endif
          bTrsErr=bTrsErr or bErr_049C3BB8
          * --- End
        endif
        * --- Ripulisce le tabelle temporanee
        * --- Delete from SPL_LOTT
        i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                SPSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione SPL_LOTT'
          return
        endif
        * --- Delete from AGG_LOTT
        i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione AGG_LOTT'
          return
        endif
      endif
      if USED("AGGIORNA")
        SELECT AGGIORNA
        USE
      endif
    endif
    if this.w_CONFERMA
      This.OparentObject.NotifyEvent("Esegui")
    endif
  endproc
  proc Try_049FC3E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into AGG_LOTT
    i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AGG_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVCODICE"+",MVDESART"+",MVUNIMIS"+",MVQTAMOV"+",MVQTAUM1"+",MVCODMAG"+",MVCODMAT"+",MVCODLOT"+",MVCODUBI"+",MVCODUB2"+",MVFLLOTT"+",MVF2LOTT"+",MVFLUBIC"+",MVFLUBI2"+",MVARTLOT"+",MVCODART"+",MVSERRIF"+",MVROWRIF"+",MVFLCASC"+",MVFLRISE"+",MVF2CASC"+",MVF2RISE"+",MVCODCOM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'AGG_LOTT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'AGG_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'AGG_LOTT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'AGG_LOTT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'AGG_LOTT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'AGG_LOTT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'AGG_LOTT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'AGG_LOTT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'AGG_LOTT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'AGG_LOTT','MVCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'AGG_LOTT','MVCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AGG_LOTT','MVCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'AGG_LOTT','MVCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'AGG_LOTT','MVCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'AGG_LOTT','MVFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'AGG_LOTT','MVF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLUBIC),'AGG_LOTT','MVFLUBIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLUBI2),'AGG_LOTT','MVFLUBI2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVARTLOT),'AGG_LOTT','MVARTLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'AGG_LOTT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'AGG_LOTT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'AGG_LOTT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'AGG_LOTT','MVFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'AGG_LOTT','MVFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'AGG_LOTT','MVF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'AGG_LOTT','MVF2RISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'AGG_LOTT','MVCODCOM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_CPROWORD,'MVCODICE',this.w_MVCODICE,'MVDESART',this.w_MVDESART,'MVUNIMIS',this.w_MVUNIMIS,'MVQTAMOV',this.w_MVQTAMOV,'MVQTAUM1',this.w_MVQTAUM1,'MVCODMAG',this.w_MVCODMAG,'MVCODMAT',this.w_MVCODMAT,'MVCODLOT',this.w_MVCODLOT)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVCODICE,MVDESART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODMAG,MVCODMAT,MVCODLOT,MVCODUBI,MVCODUB2,MVFLLOTT,MVF2LOTT,MVFLUBIC,MVFLUBI2,MVARTLOT,MVCODART,MVSERRIF,MVROWRIF,MVFLCASC,MVFLRISE,MVF2CASC,MVF2RISE,MVCODCOM &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MVCODICE;
           ,this.w_MVDESART;
           ,this.w_MVUNIMIS;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVCODMAG;
           ,this.w_MVCODMAT;
           ,this.w_MVCODLOT;
           ,this.w_MVCODUBI;
           ,this.w_MVCODUB2;
           ,this.w_MVFLLOTT;
           ,this.w_MVF2LOTT;
           ,this.w_MVFLUBIC;
           ,this.w_MVFLUBI2;
           ,this.w_MVARTLOT;
           ,this.w_MVCODART;
           ,this.w_MVSERRIF;
           ,this.w_MVROWRIF;
           ,this.w_MVFLCASC;
           ,this.w_MVFLRISE;
           ,this.w_MVF2CASC;
           ,this.w_MVF2RISE;
           ,this.w_MVCODCOM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento AGG_LOTT'
      return
    endif
    this.w_TROV = .T.
    return
  proc Try_049D0248()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("Fase 2: aggiornamento righe documento...",.T.)
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_049C3BB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SELECT AGGIORNA 
 go top 
 scan
    this.w_CPROWNUM = CPROWNUM
    this.w_MVCODMAG = NVL(MVCODMAG, " ")
    this.w_MVNUMRIF = -20
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Cancello le righe che andr� ad aggiornare dal cursore MATRERRO
    if USED("MATRERRO")
      Delete from matrerro where matrerro.serdoc=aggiorna.mvserial and matrerro.rownum=this.w_CPROWNUM
      =wrcursor("MATRERRO") 
 Select "MATRERRO" 
 PACK
      if RECCOUNT("MATRERRO")=0
        USE IN SELECT("MATRERRO")
      endif
    endif
    endscan
    if USED("AGGIORNA")
      SELECT AGGIORNA
      USE
    endif
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Documenti di Evasione
    this.CONTA = 0
    this.w_OLDSERROW = SPACE(30)
    this.w_QTADIF = 0
    this.w_RIGADUP = .F.
    select spllott 
 scan
    this.w_SPLITRIG = NVL(SPLITRIG,"N")
    this.w_ULTRIG = RECNO()=RECCOUNT()
    * --- Testa se cambio riga del documento o se sono all'ultima riga del cursore:
    *     Nel primo caso fa un insert in tabella con i valori della riga precedente: 
    *     nel secondo caso quando sono alla fine del cursore duplico la riga precedente e modifico quantit� di riga con la quantit� mancante per evadere tutta la quantit�
    if (this.w_OLDSERROW<>ALLTRIM(spllott.SPSERIAL) + ALLTRIM(STR(spllott.SPROWORD)) AND NOT EMPTY(this.w_OLDSERROW)) OR this.w_ULTRIG
      this.w_FINERIG = .T.
    endif
    if this.w_FINERIG AND NOT this.w_ULTRIG
      * --- al cambio della riga documento memorizzo le old della riga precedente
      this.OLDUBICA = this.UBICA
      this.OLDUBICA2 = this.UBICA2
      this.OLDSERDOC = this.SERDOC
      this.OLDRIFRIGA = this.RIFRIGA
      this.OLDRIGDOC = this.RIGDOC
      this.w_OLDQTADIF = this.w_QTADIF
      this.w_OLDQTADIF2 = this.w_QTADIF2
      this.w_RIGADUP = .F.
    endif
    this.CONTA = this.CONTA+1
    this.UBICA = spllott.SPCODUBI
    this.UBICA2 = spllott.SPCODUB2
    this.SERDOC = spllott.SPSERIAL
    this.RIFRIGA = spllott.SPNUMRIF
    this.RIGDOC = spllott.SPROWORD
    this.NUMROW = spllott.CPROWNUM+this.conta
    this.LOTTO = spllott.SPCODLOT
    if this.w_MVUNIMIS<>this.w_UM1
      this.QTA1 = CALQTA(spllott.SPQTAMOV, this.w_MVUNIMIS,iif(this.w_MVUNIMIS=this.w_UM2,this.w_UM2,space(3)),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, this.w_FLUSEPD, "N", this.w_MODUM2D, "", this.w_UM3, IIF(this.w_OP3="/","*","/"), this.w_MOLT3)
      this.QTA1MP = CALQTA(spllott.MPQTAMOV, this.w_MVUNIMIS,iif(this.w_MVUNIMIS=this.w_UM2,this.w_UM2,space(3)),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, this.w_FLUSEPD, "N", this.w_MODUM2D, "", this.w_UM3, IIF(this.w_OP3="/","*","/"), this.w_MOLT3)
    else
      this.QTA1 = spllott.SPQTAMOV
      this.QTA1MP = spllott.MPQTAMOV
    endif
    this.QTA2 = spllott.SPQTAMOV1
    this.QTA2MP = spllott.MPQTAMOV1
    if !this.w_RIGADUP
      this.w_QTADIF = spllott.QTADIF
    else
      * --- la riga � duplicata quindi non controllo la differenza
      this.w_QTADIF = 0
    endif
    if this.w_MVUNIMIS<>this.w_UM1
      this.w_QTADIF2 = CALQTA(this.w_QTADIF, this.w_MVUNIMIS,iif(this.w_MVUNIMIS=this.w_UM2,this.w_UM2,space(3)),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, this.w_FLUSEPD, "N", this.w_MODUM2D, "", this.w_UM3, IIF(this.w_OP3="/","*","/"), this.w_MOLT3)
    else
      this.w_QTADIF2 = this.w_QTADIF
    endif
    this.w_OLDSERROW = ALLTRIM(spllott.SPSERIAL) + ALLTRIM(STR(spllott.SPROWORD))
    do case
      case this.w_FINERIG AND this.w_OLDQTADIF<>0
        this.OLDRIGDOC=this.OLDRIGDOC+1
        this.OLDNUMROW = this.OLDNUMROW +1
        * --- Try
        local bErr_04AE0C48
        bErr_04AE0C48=bTrsErr
        this.Try_04AE0C48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04AE0C48
        * --- End
      case this.w_ULTRIG and this.w_QTADIF<>0
        * --- Ultima riga del cursore, duplico la precedente e cambio i campi quantit� e lotto
        this.w_POS = RECNO()
        SCATTER MEMVAR
        APPEND BLANK
        spqtamov=this.w_QTADIF
        spqtamov1=this.w_QTADIF2
        spcodlot=space(20)
        GATHER MEMVAR
        SELECT SPLLOTT 
 GOTO this.w_POS
        this.w_RIGADUP = .t.
    endcase
    * --- Try
    local bErr_04A77768
    bErr_04A77768=bTrsErr
    this.Try_04A77768()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04A77768
    * --- End
    endscan
    * --- Carica Temporaneo Evasioni
    vq_exec("..\COLA\EXE\QUERY\GSCL1QDO.VQR", this,"AGGIORNA")
    * --- Rilascio il temporaneo per lo split dei lotti
    if used("spllott")
      select spllott 
 use
    endif
    if USED("AGGIORNA")
      SELECT AGGIORNA
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(CPROWNUM, 0)>0 AND NVL(MVQTAMOV, 0)>0
        * --- Esegue Update su riga gi� presente.
        this.w_CPROWNUM = CPROWNUM
        this.w_SPLITLOT = SPLITLOT
        if this.w_OLDROW<>this.w_CPROWNUM
          * --- Informazioni Mantenute dalla riga originaria
          this.w_MVDESART = NVL(MVDESART, " ")
          this.w_MVCODART = NVL(MVCODART, " ")
          this.w_MVCODICE = NVL(MVCODICE, " ")
          this.w_MVCODMAG = NVL(MVCODMAG, " ")
          this.w_MVCODMAT = NVL(MVCODMAT, " ")
          this.w_MVFLLOTT = NVL(MVFLLOTT, " ")
          this.w_MVF2LOTT = NVL(MVF2LOTT, " ")
          this.w_MVARTLOT = NVL(MVARTLOT, " ")
          this.w_MVFLUBIC = NVL(MVFLUBIC, " ")
          this.w_MVFLUBI2 = NVL(MVFLUBI2, " ")
          this.w_MVCODLOT = SPACE(20)
          this.w_MVCODUBI = SPACE(20)
          this.w_MVCODUB2 = SPACE(20)
          this.w_MVCODMAG = NVL(MVCODMAG,SPACE(5))
          this.w_MVCODMAT = NVL(MVCODMAT,SPACE(5))
          this.w_MVFLCASC = NVL(MVFLCASC, " ")
          this.w_MVFLRISE = NVL(MVFLRISE, " ")
          this.w_MVF2CASC = NVL(MVF2CASC, " ")
          this.w_MVF2RISE = NVL(MVF2RISE, " ")
          this.w_MVSERRIF = NVL(MVSERRIF,SPACE(10))
          this.w_MVROWRIF = NVL(MVROWRIF,0)
          this.w_MVTIPRIG = NVL(MVTIPRIG," ")
          this.w_MVCAUMAG = NVL(MVCAUMAG,SPACE(5))
          this.w_MVCATCON = NVL(MVCATCON,SPACE(5))
          this.w_MVCONTRA = NVL(MVCONTRA,SPACE(15))
          this.w_MVDESSUP = NVL(MVDESSUP,SPACE(10))
          this.w_MVCODLIS = NVL(MVCODLIS,SPACE(5))
          this.w_MVCODCLA = NVL(MVCODCLA,SPACE(5))
          this.w_MVPREZZO = NVL(MVPREZZO,0)
          this.w_MVSCONT1 = NVL(MVSCONT1,0)
          this.w_MVSCONT2 = NVL(MVSCONT2,0)
          this.w_MVSCONT3 = NVL(MVSCONT3,0)
          this.w_MVSCONT4 = NVL(MVSCONT4,0)
          this.w_MVFLOMAG = NVL(MVFLOMAG," ")
          this.w_MVIMPACC = 0
          this.w_MVIMPSCO = 0
          this.w_MVPESNET = NVL(MVPESNET,0)
          this.w_MVFLRAGG = NVL(MVFLRAGG," ")
          this.w_MVDATEVA = CP_TODATE(MVDATEVA)
          this.w_MVCODIVA = NVL(MVCODIVA,SPACE(5))
          this.w_MVCONIND = NVL(MVCONIND,SPACE(15))
          this.w_MVNOMENC = NVL(MVNOMENC,SPACE(8))
          this.w_MVFLTRAS = NVL(MVFLTRAS,SPACE(1))
          this.w_MVMOLSUP = NVL(MVMOLSUP,0)
          this.w_MVCAUCOL = NVL(MVCAUCOL,SPACE(5))
          this.w_MVCODATT = NVL(MVCODATT,SPACE(15))
          this.w_MVCODCEN = NVL(MVCODCEN,SPACE(15))
          this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
          this.w_MVCODODL = NVL(MVCODODL,SPACE(15))
          this.w_MVF2ORDI = NVL(MVF2ORDI," ")
          this.w_MVF2IMPE = NVL(MVF2IMPE," ")
          this.w_MVFLORDI = NVL(MVFLORDI," ")
          this.w_MVFLELGM = NVL(MVFLELGM," ")
          this.w_MVFLIMPE = NVL(MVFLIMPE," ")
          this.w_MVKEYSAL = NVL(MVKEYSAL,SPACE(20))
          this.w_MVTIPATT = NVL(MVTIPATT," ")
          this.w_MVRIGMAT = NVL(MVRIGMAT,0)
          this.w_MVVOCCEN = NVL(MVVOCCEN,SPACE(15))
          this.w_MVIMPNAZ = 0
          this.w_MVVALMAG = 0
          this.w_MVVALRIG = 0
          this.w_MVINICOM = CP_TODATE(MVINICOM)
          this.w_MVFINCOM = CP_TODATE(MVFINCOM)
        endif
        this.w_MVQTAMOV = NVL(MVQTAMOV,0)
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAUNIMIS,CAMOLTIP,CAOPERAT"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(AGGIORNA.MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAUNIMIS,CAMOLTIP,CAOPERAT;
            from (i_cTable) where;
                CACODICE = AGGIORNA.MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UM3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.w_MOLT3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          this.w_OP3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARFLUSEP"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(AGGIORNA.MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARFLUSEP;
            from (i_cTable) where;
                ARCODART = AGGIORNA.MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UM1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_UM2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          w_MOLT = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          w_OP = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_FLUSEPD = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT AGGIORNA
        if MVUNIMIS<>this.w_UM1
          msg=" "
          this.w_MVQTAUM1 = CALQTA(this.w_MVQTAMOV, this.w_MVUNIMIS,this.w_UM2,w_OP, w_MOLT, this.w_FLUSEPD, "N", this.w_MODUM2D, @msg, this.w_UM3, this.w_OP3, this.w_MOLT3)
          if not empty(msg)
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=msg
          endif
        else
          this.w_MVQTAUM1 = NVL(MVQTAMOV, 0)
        endif
        if g_PERLOT="S" AND this.w_MVARTLOT$ "SC" AND (this.w_MVFLLOTT $ "+-" OR this.w_MVF2LOTT $ "+-")
          this.w_MVCODLOT = NVL(MVCODLOT, SPACE(20))
        endif
        if g_PERUBI="S" AND NOT EMPTY(this.w_MVCODMAG) AND this.w_MVFLUBIC="S" AND this.w_MVFLLOTT $ "+-"
          this.w_MVCODUBI = NVL(MVCODUBI, SPACE(20))
        endif
        if NOT EMPTY(this.w_MVCODMAT) AND g_PERUBI="S" AND this.w_MVFLUBI2="S" AND this.w_MVF2LOTT $ "+-"
          this.w_MVCODUB2 = NVL(MVCODUB2, SPACE(20))
        endif
        this.w_VALUNI = cp_ROUND(this.w_MVPREZZO * (1+this.w_MVSCONT1/100)*(1+this.w_MVSCONT2/100)*(1+this.w_MVSCONT3/100)*(1+this.w_MVSCONT4/100),5)
        this.w_MVVALRIG = cp_ROUND(this.w_MVQTAMOV*this.w_VALUNI, 5)
        this.w_ROWMAT = SPROWNUM
        this.w_MVLOTMAG = Iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUBI ) , Space(5) , this.w_MVCODMAG )
        this.w_MVLOTMAT = Iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUB2 ) , Space(5) , this.w_MVCODMAT )
        * --- Riferimento di riga dello split del lotto passato come parametro alla 
        *     query per la determinazione delle matricole da inserire.
        *     Nel caso di righe aggiuntive la variabile w_ROWNUM viene passato come 
        *     parametro alla query senza filtro poich� sar� effettivamente l'espressione del 
        *     campo MTROWNUM da inserire in MOVIMATR
        this.w_MVNUMRIF = -20
        if this.w_OLDROW<>this.w_CPROWNUM AND this.w_SPLITLOT<>"S"
          * --- Modifico riga gi� esistente
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
            +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
            +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
            +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
            +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
            +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
            +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
            +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
            +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVCODLOT = this.w_MVCODLOT;
                ,MVCODUBI = this.w_MVCODUBI;
                ,MVCODUB2 = this.w_MVCODUB2;
                ,MVQTAUM1 = this.w_MVQTAUM1;
                ,MVQTASAL = this.w_MVQTAUM1;
                ,MVQTAMOV = this.w_MVQTAMOV;
                ,MVVALRIG = this.w_MVVALRIG;
                ,MVLOTMAG = this.w_MVLOTMAG;
                ,MVLOTMAT = this.w_MVLOTMAT;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_CPROWNUM;
                and MVNUMRIF = this.w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorno Righe Piano Produzione collegato
          if g_PERUBI="S" OR g_PERLOT="S"
            GSMD_BRL (this, this.w_SERIAL , "V" , "+" , this.w_CPROWNUM ,.T. )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Determino il Max Numero di riga del documento
          this.w_ROWNUM = this.w_CPROWNUM
          if empty(this.w_MVLOTMAT)
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Cancello le righe che andr� ad aggiornare dal cursore MATRERRO
          if USED("MATRERRO")
            Delete from matrerro where matrerro.serdoc=aggiorna.mvserial and matrerro.rownum=this.w_CPROWNUM
            =wrcursor("MATRERRO") 
 Select "MATRERRO" 
 PACK
            if RECCOUNT("MATRERRO")=0
              USE IN SELECT("MATRERRO")
            endif
          endif
          vq_exec("..\MADV\EXE\QUERY\GSMD2QDO.VQR", this,"MAXRIG")
          SELECT MAXRIG
          this.w_ROWNUM = NVL(MAXRIG.NUMRIG,0)
          this.w_ROWDOC = NVL(MAXRIG.RIFRIG,0)
        else
          * --- Inserisco Righe nuove
          this.w_MVCODODL = IIF(EMPTY(this.w_MVCODODL),SPACE(15),this.w_MVCODODL)
          this.w_ROWNUM = this.w_ROWNUM + 1
          this.w_ROWDOC = this.w_ROWDOC + 10
          * --- Insert into DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCONTRO"+",MVCAUMAG"+",MVCODCLA"+",MVCONTRA"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVFLOMAG"+",MVCODIVA"+",MVCONIND"+",MVVALRIG"+",MVIMPACC"+",MVIMPSCO"+",MVVALMAG"+",MVIMPNAZ"+",MVPERPRO"+",MVIMPPRO"+",MVPESNET"+",MVNOMENC"+",MVMOLSUP"+",MVNUMCOL"+",MVFLTRAS"+",MVFLRAGG"+",MVSERRIF"+",MVROWRIF"+",MVFLARIF"+",MVQTASAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_DETT','MVSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DOC_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWDOC),'DOC_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLRAGG),'DOC_DETT','MVFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'CPROWNUM',this.w_ROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_ROWDOC,'MVTIPRIG',this.w_MVTIPRIG,'MVCODICE',this.w_MVCODICE,'MVCODART',this.w_MVCODART,'MVDESART',this.w_MVDESART,'MVDESSUP',this.w_MVDESSUP,'MVUNIMIS',this.w_MVUNIMIS,'MVCATCON',this.w_MVCATCON,'MVCONTRO',SPACE(15))
            insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCONTRO,MVCAUMAG,MVCODCLA,MVCONTRA,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVFLOMAG,MVCODIVA,MVCONIND,MVVALRIG,MVIMPACC,MVIMPSCO,MVVALMAG,MVIMPNAZ,MVPERPRO,MVIMPPRO,MVPESNET,MVNOMENC,MVMOLSUP,MVNUMCOL,MVFLTRAS,MVFLRAGG,MVSERRIF,MVROWRIF,MVFLARIF,MVQTASAL &i_ccchkf. );
               values (;
                 this.w_SERIAL;
                 ,this.w_ROWNUM;
                 ,this.w_MVNUMRIF;
                 ,this.w_ROWDOC;
                 ,this.w_MVTIPRIG;
                 ,this.w_MVCODICE;
                 ,this.w_MVCODART;
                 ,this.w_MVDESART;
                 ,this.w_MVDESSUP;
                 ,this.w_MVUNIMIS;
                 ,this.w_MVCATCON;
                 ,SPACE(15);
                 ,this.w_MVCAUMAG;
                 ,this.w_MVCODCLA;
                 ,this.w_MVCONTRA;
                 ,this.w_MVCODLIS;
                 ,this.w_MVQTAMOV;
                 ,this.w_MVQTAUM1;
                 ,this.w_MVPREZZO;
                 ,this.w_MVSCONT1;
                 ,this.w_MVSCONT2;
                 ,this.w_MVSCONT3;
                 ,this.w_MVSCONT4;
                 ,this.w_MVFLOMAG;
                 ,this.w_MVCODIVA;
                 ,this.w_MVCONIND;
                 ,this.w_MVVALRIG;
                 ,this.w_MVIMPACC;
                 ,this.w_MVIMPSCO;
                 ,this.w_MVVALMAG;
                 ,this.w_MVIMPNAZ;
                 ,0;
                 ,0;
                 ,this.w_MVPESNET;
                 ,this.w_MVNOMENC;
                 ,this.w_MVMOLSUP;
                 ,0;
                 ,this.w_MVFLTRAS;
                 ,this.w_MVFLRAGG;
                 ,SPACE(10);
                 ,0;
                 ," ";
                 ,this.w_MVQTAUM1;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Errore in inserimento DOC_DETT'
            return
          endif
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
            +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
            +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
            +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
            +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
            +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
            +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
            +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCOL),'DOC_DETT','MVCAUCOL');
            +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
            +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
            +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
            +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
            +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
            +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
            +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'DOC_DETT','MVCODMAT');
            +",MVCODODL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODODL),'DOC_DETT','MVCODODL');
            +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
            +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
            +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
            +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'DOC_DETT','MVF2CASC');
            +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2IMPE),'DOC_DETT','MVF2IMPE');
            +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
            +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2ORDI),'DOC_DETT','MVF2ORDI');
            +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'DOC_DETT','MVF2RISE');
            +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFINCOM),'DOC_DETT','MVFINCOM');
            +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'DOC_DETT','MVFLCASC');
            +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELGM),'DOC_DETT','MVFLELGM');
            +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLIMPE),'DOC_DETT','MVFLIMPE');
            +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
            +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORDI),'DOC_DETT','MVFLORDI');
            +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'DOC_DETT','MVFLRISE');
            +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVINICOM),'DOC_DETT','MVINICOM');
            +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
            +",MVRIGMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIGMAT),'DOC_DETT','MVRIGMAT');
            +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
            +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
            +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
            +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
            +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
            +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVFLARIF = " ";
                ,MVFLRIPA = " ";
                ,MVFLEVAS = " ";
                ,MVFLERIF = " ";
                ,MVQTAEVA = 0;
                ,MVQTAIMP = 0;
                ,MVQTAEV1 = 0;
                ,MVIMPEVA = 0;
                ,MVQTAIM1 = 0;
                ,MVCAUCOL = this.w_MVCAUCOL;
                ,MVCAUMAG = this.w_MVCAUMAG;
                ,MVCODATT = this.w_MVCODATT;
                ,MVCODCEN = this.w_MVCODCEN;
                ,MVCODCOM = this.w_MVCODCOM;
                ,MVCODLOT = this.w_MVCODLOT;
                ,MVCODMAG = this.w_MVCODMAG;
                ,MVCODMAT = this.w_MVCODMAT;
                ,MVCODODL = this.w_MVCODODL;
                ,MVCODUB2 = this.w_MVCODUB2;
                ,MVCODUBI = this.w_MVCODUBI;
                ,MVDATEVA = this.w_MVDATEVA;
                ,MVF2CASC = this.w_MVF2CASC;
                ,MVF2IMPE = this.w_MVF2IMPE;
                ,MVF2LOTT = this.w_MVF2LOTT;
                ,MVF2ORDI = this.w_MVF2ORDI;
                ,MVF2RISE = this.w_MVF2RISE;
                ,MVFINCOM = this.w_MVFINCOM;
                ,MVFLCASC = this.w_MVFLCASC;
                ,MVFLELGM = this.w_MVFLELGM;
                ,MVFLIMPE = this.w_MVFLIMPE;
                ,MVFLLOTT = this.w_MVFLLOTT;
                ,MVFLORDI = this.w_MVFLORDI;
                ,MVFLRISE = this.w_MVFLRISE;
                ,MVINICOM = this.w_MVINICOM;
                ,MVKEYSAL = this.w_MVKEYSAL;
                ,MVRIGMAT = this.w_MVRIGMAT;
                ,MVTIPATT = this.w_MVTIPATT;
                ,MVVOCCEN = this.w_MVVOCCEN;
                ,MV_SEGNO = this.w_MV_SEGNO;
                ,MVFLELAN = this.w_MVFLELAN;
                ,MVLOTMAG = this.w_MVLOTMAG;
                ,MVLOTMAT = this.w_MVLOTMAT;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;
                and MVNUMRIF = this.w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura DOC_DETT'
            return
          endif
          * --- Aggiorno il riferimento sulle Dichiarazioni matricole
          if g_PERUBI="S" OR g_PERLOT="S"
            GSMD_BRL (this, this.w_SERIAL , "V" , "+" , this.w_ROWNUM ,.T. )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if empty(nvl(this.w_MVCODUBI,space(10))) or empty(nvl(this.w_MVCODLOT,space(10)))
            if empty(nvl(this.w_MVCODUBI,space(10)))
              if this.w_OPERAZ="CA"
                * --- Write into DIC_MATR
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DIC_MATR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MATR','MTROWDIS');
                      +i_ccchkf ;
                  +" where ";
                      +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                      +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                         )
                else
                  update (i_cTable) set;
                      MTROWDIS = this.w_ROWNUM;
                      &i_ccchkf. ;
                   where;
                      MTRIFDIS = this.w_SERIAL;
                      and MTROWDIS = this.w_CPROWNUM;
                      and MTCODLOT = this.w_MVCODLOT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Write into DIC_MCOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MCOM','MTROWDIS');
                      +i_ccchkf ;
                  +" where ";
                      +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                      +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                         )
                else
                  update (i_cTable) set;
                      MTROWDIS = this.w_ROWNUM;
                      &i_ccchkf. ;
                   where;
                      MTRIFDIS = this.w_SERIAL;
                      and MTROWDIS = this.w_CPROWNUM;
                      and MTCODLOT = this.w_MVCODLOT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              if empty(nvl(this.w_MVCODLOT,space(10)))
                if this.w_OPERAZ="CA"
                  * --- Write into DIC_MATR
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DIC_MATR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MATR','MTROWDIS');
                        +i_ccchkf ;
                    +" where ";
                        +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                        +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                        +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                           )
                  else
                    update (i_cTable) set;
                        MTROWDIS = this.w_ROWNUM;
                        &i_ccchkf. ;
                     where;
                        MTRIFDIS = this.w_SERIAL;
                        and MTROWDIS = this.w_CPROWNUM;
                        and MTCODUBI = this.w_MVCODUBI;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  * --- Write into DIC_MCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MCOM','MTROWDIS');
                        +i_ccchkf ;
                    +" where ";
                        +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                        +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                        +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                           )
                  else
                    update (i_cTable) set;
                        MTROWDIS = this.w_ROWNUM;
                        &i_ccchkf. ;
                     where;
                        MTRIFDIS = this.w_SERIAL;
                        and MTROWDIS = this.w_CPROWNUM;
                        and MTCODUBI = this.w_MVCODUBI;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                * --- Non devo aggiornare niente
              endif
            endif
          else
            if this.w_OPERAZ="CA"
              * --- Write into DIC_MATR
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIC_MATR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_MATR_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MATR_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MATR','MTROWDIS');
                    +i_ccchkf ;
                +" where ";
                    +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                    +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                    +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                       )
              else
                update (i_cTable) set;
                    MTROWDIS = this.w_ROWNUM;
                    &i_ccchkf. ;
                 where;
                    MTRIFDIS = this.w_SERIAL;
                    and MTROWDIS = this.w_CPROWNUM;
                    and MTCODLOT = this.w_MVCODLOT;
                    and MTCODUBI = this.w_MVCODUBI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Write into DIC_MCOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIC_MCOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_MCOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_MCOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MTROWDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DIC_MCOM','MTROWDIS');
                    +i_ccchkf ;
                +" where ";
                    +"MTRIFDIS = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and MTROWDIS = "+cp_ToStrODBC(this.w_CPROWNUM);
                    +" and MTCODLOT = "+cp_ToStrODBC(this.w_MVCODLOT);
                    +" and MTCODUBI = "+cp_ToStrODBC(this.w_MVCODUBI);
                       )
              else
                update (i_cTable) set;
                    MTROWDIS = this.w_ROWNUM;
                    &i_ccchkf. ;
                 where;
                    MTRIFDIS = this.w_SERIAL;
                    and MTROWDIS = this.w_CPROWNUM;
                    and MTCODLOT = this.w_MVCODLOT;
                    and MTCODUBI = this.w_MVCODUBI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          * --- Inserisce nuova riga nel piano produzione
          Gsmd_bap(this,"B")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_OLDROW = this.w_CPROWNUM
        SELECT AGGIORNA
        ENDSCAN 
        * --- Rieseguo ricalcoli Totali documenti ed eventuali Ripartizioni (Sconti,Spese Accessorie)
        gsar_brd(this,this.w_SERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc
  proc Try_04AE0C48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- inserisco la riga con la differenza e il lotto vuoto
    * --- Insert into SPL_LOTT
    i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SPL_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SPSERIAL"+",SPROWORD"+",CPROWNUM"+",SPCODLOT"+",SPCODUBI"+",SPCODUB2"+",SPQTAMOV"+",SPQTAUM1"+",SPNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oldserdoc),'SPL_LOTT','SPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oldrigdoc),'SPL_LOTT','SPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oldnumrow),'SPL_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(space(20)),'SPL_LOTT','SPCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.oldubica),'SPL_LOTT','SPCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oldubica2),'SPL_LOTT','SPCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_oldqtadif),'SPL_LOTT','SPQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_oldqtadif2),'SPL_LOTT','SPQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.oldrifriga),'SPL_LOTT','SPNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SPSERIAL',this.oldserdoc,'SPROWORD',this.oldrigdoc,'CPROWNUM',this.oldnumrow,'SPCODLOT',space(20),'SPCODUBI',this.oldubica,'SPCODUB2',this.oldubica2,'SPQTAMOV',this.w_oldqtadif,'SPQTAUM1',this.w_oldqtadif2,'SPNUMRIF',this.oldrifriga)
      insert into (i_cTable) (SPSERIAL,SPROWORD,CPROWNUM,SPCODLOT,SPCODUBI,SPCODUB2,SPQTAMOV,SPQTAUM1,SPNUMRIF &i_ccchkf. );
         values (;
           this.oldserdoc;
           ,this.oldrigdoc;
           ,this.oldnumrow;
           ,space(20);
           ,this.oldubica;
           ,this.oldubica2;
           ,this.w_oldqtadif;
           ,this.w_oldqtadif2;
           ,this.oldrifriga;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nell inserimento di SPL_LOTT'
      return
    endif
    return
  proc Try_04A77768()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SPL_LOTT
    i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SPL_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SPSERIAL"+",SPROWORD"+",CPROWNUM"+",SPCODLOT"+",SPCODUBI"+",SPCODUB2"+",SPQTAMOV"+",SPQTAUM1"+",SPNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.serdoc),'SPL_LOTT','SPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.rigdoc),'SPL_LOTT','SPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.numrow),'SPL_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.lotto),'SPL_LOTT','SPCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.ubica),'SPL_LOTT','SPCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.ubica2),'SPL_LOTT','SPCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.qta1),'SPL_LOTT','SPQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.qta2),'SPL_LOTT','SPQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.rifriga),'SPL_LOTT','SPNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SPSERIAL',this.serdoc,'SPROWORD',this.rigdoc,'CPROWNUM',this.numrow,'SPCODLOT',this.lotto,'SPCODUBI',this.ubica,'SPCODUB2',this.ubica2,'SPQTAMOV',this.qta1,'SPQTAUM1',this.qta2,'SPNUMRIF',this.rifriga)
      insert into (i_cTable) (SPSERIAL,SPROWORD,CPROWNUM,SPCODLOT,SPCODUBI,SPCODUB2,SPQTAMOV,SPQTAUM1,SPNUMRIF &i_ccchkf. );
         values (;
           this.serdoc;
           ,this.rigdoc;
           ,this.numrow;
           ,this.lotto;
           ,this.ubica;
           ,this.ubica2;
           ,this.qta1;
           ,this.qta2;
           ,this.rifriga;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nell inserimento di SPL_LOTT'
      return
    endif
    return


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo Matricole nel documento selezionato
    * --- Create temporary table DELTA
    i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCL_QAM.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.DELTA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Insert into MOVIMATR
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.DELTA_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOVIMATR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento matricole'
      return
    endif
    if this.w_OPERAZ<>"CA"
      * --- Aggiorna il saldo Movimenti Matricole Matricole (blocco sul semaforo)
      * --- Write into MOVIMATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"
        do vq_exec with 'GSCL5QAM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(1),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
        +"MOVIMATR.MT_SALDO ="+cp_NullLink(cp_ToStrODBC(1),'MOVIMATR','MT_SALDO');
            +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
            +"MT_SALDO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(1),'MOVIMATR','MT_SALDO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(1),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
                +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(1),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Write into MATRICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MATRICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="AMKEYSAL,AMCODICE"
      do vq_exec with 'GSCO4QAM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MATRICOL_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
              +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
          +i_ccchkf;
          +" from "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
              +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL, "+i_cQueryTable+" _t2 set ";
      +"MATRICOL.AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
          +Iif(Empty(i_ccchkf),"",",MATRICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MATRICOL.AMKEYSAL = t2.AMKEYSAL";
              +" and "+"MATRICOL.AMCODICE = t2.AMCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set (";
          +"AM_PRENO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MATRICOL.AMKEYSAL = _t2.AMKEYSAL";
              +" and "+"MATRICOL.AMCODICE = _t2.AMCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATRICOL set ";
      +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".AMKEYSAL = "+i_cQueryTable+".AMKEYSAL";
              +" and "+i_cTable+".AMCODICE = "+i_cQueryTable+".AMCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table DELTA
    i_nIdx=cp_GetTableDefIdx('DELTA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('DELTA')
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Pegging
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLCOMM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_PECODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLCOMM;
        from (i_cTable) where;
            ARCODART = this.w_PECODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PEFLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTCOMME"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_DPCODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTCOMME;
        from (i_cTable) where;
            OLCODODL = this.w_DPCODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Sposta gli abbinamenti dagli ODL al Magazzino (MRP)
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
    pegging=.F.
    * --- Select from GSDB2BAM
    do vq_exec with 'GSDB2BAM',this,'_Curs_GSDB2BAM','',.f.,.t.
    if used('_Curs_GSDB2BAM')
      select _Curs_GSDB2BAM
      locate for 1=1
      do while not(eof())
      pegging=.T.
      if this.w_PEQTAUM1>0
        this.w_PESERIAL = _Curs_GSDB2BAM.PESERIAL
        this.w_PEQTAABB = _Curs_GSDB2BAM.PEQTAABB
        this.w_PECODCOM = _Curs_GSDB2BAM.PECODCOM
        this.w_PECODRIC = _Curs_GSDB2BAM.PECODRIC
        if this.w_PEQTAUM1 >= this.w_PEQTAABB
          * --- Porta tutto l'abbinamento a Magazzino
          if this.w_PGCODODL=" Magazz"
            * --- Storna Dichiarazione
            this.w_PETIPRIF = iif(empty(nvl(_Curs_GSDB2BAM.PERIFODL,"")), "D", "O")
          else
            this.w_PETIPRIF = "M"
          endif
          if empty(nvl(_Curs_GSDB2BAM.PERIFODL," ")) and empty(nvl(_Curs_GSDB2BAM.PESERORD," ")) and empty(nvl(_Curs_GSDB2BAM.PERIFTRS," "))
            * --- Elimino la riga
            * --- Delete from PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                     )
            else
              delete from (i_cTable) where;
                    PESERIAL = this.w_PESERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            this.w_PEQTAUM1 = this.w_PEQTAUM1 - this.w_PEQTAABB
          else
            if this.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S"
              do case
                case this.w_PETIPRIF="M"
                  do case
                    case _Curs_GSDB2BAM.PETIPRIF="M"
                      this.w_PERIFTRS = space(20)
                    case _Curs_GSDB2BAM.PETIPRIF="D"
                      this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PESERORD)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
                    case _Curs_GSDB2BAM.PETIPRIF="O"
                      this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PERIFODL)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
                  endcase
                  * --- Write into PEG_SELI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                    +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_DPCODCOM),'PEG_SELI','PECODCOM');
                    +",PERIFODL ="+cp_NullLink(cp_ToStrODBC(space(15)),'PEG_SELI','PERIFODL');
                    +",PESERORD ="+cp_NullLink(cp_ToStrODBC(space(10)),'PEG_SELI','PESERORD');
                    +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PERIGORD');
                    +",PERIFTRS ="+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                        +i_ccchkf ;
                    +" where ";
                        +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                           )
                  else
                    update (i_cTable) set;
                        PESERODL = this.w_PECODODL;
                        ,PETIPRIF = this.w_PETIPRIF;
                        ,PECODCOM = this.w_DPCODCOM;
                        ,PERIFODL = space(15);
                        ,PESERORD = space(10);
                        ,PERIGORD = 0;
                        ,PERIFTRS = this.w_PERIFTRS;
                        &i_ccchkf. ;
                     where;
                        PESERIAL = this.w_PESERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.w_PETIPRIF="D"
                  this.w_TRSSERORD = left(_Curs_GSDB2BAM.PERIFTRS,10)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Write into PEG_SELI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                    +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +",PESERORD ="+cp_NullLink(cp_ToStrODBC(this.w_TRSSERORD),'PEG_SELI','PESERORD');
                    +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +",PERIFTRS ="+cp_NullLink(cp_ToStrODBC(space(20)),'PEG_SELI','PERIFTRS');
                        +i_ccchkf ;
                    +" where ";
                        +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                           )
                  else
                    update (i_cTable) set;
                        PESERODL = this.w_PECODODL;
                        ,PETIPRIF = this.w_PETIPRIF;
                        ,PECODCOM = this.w_OLTCOMME;
                        ,PESERORD = this.w_TRSSERORD;
                        ,PERIGORD = this.w_TRSRIGORD;
                        ,PERIFTRS = space(20);
                        &i_ccchkf. ;
                     where;
                        PESERIAL = this.w_PESERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.w_PETIPRIF="O"
                  this.w_TRSRIFODL = left(_Curs_GSDB2BAM.PERIFTRS,15)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Write into PEG_SELI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                    +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +",PERIFODL ="+cp_NullLink(cp_ToStrODBC(this.w_TRSRIFODL),'PEG_SELI','PERIFODL');
                    +",PERIGORD ="+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +",PERIFTRS ="+cp_NullLink(cp_ToStrODBC(space(20)),'PEG_SELI','PERIFTRS');
                        +i_ccchkf ;
                    +" where ";
                        +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                           )
                  else
                    update (i_cTable) set;
                        PESERODL = this.w_PECODODL;
                        ,PETIPRIF = this.w_PETIPRIF;
                        ,PECODCOM = this.w_OLTCOMME;
                        ,PERIFODL = this.w_TRSRIFODL;
                        ,PERIGORD = this.w_TRSRIGORD;
                        ,PERIFTRS = space(20);
                        &i_ccchkf. ;
                     where;
                        PESERIAL = this.w_PESERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
              endcase
            else
              * --- Write into PEG_SELI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PESERODL ="+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                +",PETIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PETIPRIF),'PEG_SELI','PETIPRIF');
                +",PECODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_DPCODCOM),'PEG_SELI','PECODCOM');
                    +i_ccchkf ;
                +" where ";
                    +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                       )
              else
                update (i_cTable) set;
                    PESERODL = this.w_PECODODL;
                    ,PETIPRIF = this.w_PETIPRIF;
                    ,PECODCOM = this.w_DPCODCOM;
                    &i_ccchkf. ;
                 where;
                    PESERIAL = this.w_PESERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            this.w_PEQTAUM1 = this.w_PEQTAUM1 - this.w_PEQTAABB
            if this.w_PETIPRIF="M" and this.w_PEQTAUM1>0
              * --- Se dichiaro pi� del dovuto inserisco a magazzino la parte non abbinata
              pegging=.F.
            endif
          endif
        else
          * --- Divide l'abbinamento in due parti: una rimane all'ODL e l'altra va a Magazzino
          * --- Write into PEG_SELI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PEG_SELI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PEG_SELI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PEQTAABB =PEQTAABB- "+cp_ToStrODBC(this.w_PEQTAUM1);
                +i_ccchkf ;
            +" where ";
                +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                   )
          else
            update (i_cTable) set;
                PEQTAABB = PEQTAABB - this.w_PEQTAUM1;
                &i_ccchkf. ;
             where;
                PESERIAL = this.w_PESERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_PROGR = max(this.w_PROGR + 1,1)
          this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
          do case
            case _Curs_GSDB2BAM.PETIPRIF="O"
              if this.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S"
                this.w_PESERORD = space(10)
                this.w_PERIGORD = 0
                this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PERIFODL)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
              else
                this.w_PESERORD = _Curs_GSDB2BAM.PERIFODL
                this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                this.w_PERIFTRS = space(20)
              endif
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+",PERIFTRS"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODCOM),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PERIFODL',this.w_PESERORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PERIGORD',this.w_PERIGORD,'PECODCOM',this.w_DPCODCOM,'PECODRIC',this.w_PECODRIC,'PERIFTRS',this.w_PERIFTRS)
                insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC,PERIFTRS &i_ccchkf. );
                   values (;
                     this.w_PESERIAL;
                     ,this.w_PECODODL;
                     ,"M";
                     ,this.w_PESERORD;
                     ,this.w_PEQTAUM1;
                     ,this.w_DPCODODL;
                     ,this.w_PERIGORD;
                     ,this.w_DPCODCOM;
                     ,this.w_PECODRIC;
                     ,this.w_PERIFTRS;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            case _Curs_GSDB2BAM.PETIPRIF="D"
              if this.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S"
                this.w_PESERORD = space(10)
                this.w_PERIGORD = 0
                this.w_PERIFTRS = left(alltrim(_Curs_GSDB2BAM.PESERORD)+space(5),15)+alltrim(str(_Curs_GSDB2BAM.PERIGORD))
              else
                this.w_PESERORD = _Curs_GSDB2BAM.PESERORD
                this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                this.w_PERIFTRS = space(20)
              endif
              * --- Insert into PEG_SELI
              i_nConn=i_TableProp[this.PEG_SELI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+",PERIFTRS"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PESERORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODCOM),'PEG_SELI','PECODCOM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERIFTRS),'PEG_SELI','PERIFTRS');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PESERORD',this.w_PESERORD,'PERIGORD',this.w_PERIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PECODCOM',this.w_DPCODCOM,'PECODRIC',this.w_PECODRIC,'PERIFTRS',this.w_PERIFTRS)
                insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC,PERIFTRS &i_ccchkf. );
                   values (;
                     this.w_PESERIAL;
                     ,this.w_PECODODL;
                     ,"M";
                     ,this.w_PESERORD;
                     ,this.w_PERIGORD;
                     ,this.w_PEQTAUM1;
                     ,this.w_DPCODODL;
                     ,this.w_DPCODCOM;
                     ,this.w_PECODRIC;
                     ,this.w_PERIFTRS;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            case _Curs_GSDB2BAM.PETIPRIF="M"
              if empty(nvl(_Curs_GSDB2BAM.PERIFODL,""))
                * --- Storna pegging di documento
                if this.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S" and ! empty(nvl(_Curs_GSDB2BAM.PERIFTRS," "))
                  this.w_TRSSERORD = left(_Curs_GSDB2BAM.PERIFTRS,10)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSSERORD),'PEG_SELI','PESERORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"D",'PESERORD',this.w_TRSSERORD,'PERIGORD',this.w_TRSRIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"D";
                         ,this.w_TRSSERORD;
                         ,this.w_TRSRIGORD;
                         ,this.w_PEQTAUM1;
                         ,this.w_DPCODODL;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                else
                  this.w_PESERORD = _Curs_GSDB2BAM.PESERORD
                  this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PESERORD"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("D"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PESERORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"D",'PESERORD',this.w_PESERORD,'PERIGORD',this.w_PERIGORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PESERORD,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"D";
                         ,this.w_PESERORD;
                         ,this.w_PERIGORD;
                         ,this.w_PEQTAUM1;
                         ,this.w_DPCODODL;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              else
                * --- Storna pegging di impegno ODL
                if this.w_DPCODCOM<>this.w_OLTCOMME and this.w_PEFLCOMM="S" and ! empty(nvl(_Curs_GSDB2BAM.PERIFTRS," "))
                  this.w_TRSRIFODL = left(_Curs_GSDB2BAM.PERIFTRS,15)
                  this.w_TRSRIGORD = val(right(_Curs_GSDB2BAM.PERIFTRS,5))
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSRIFODL),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_TRSRIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"O",'PERIFODL',this.w_TRSRIFODL,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PERIGORD',this.w_TRSRIGORD,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"O";
                         ,this.w_TRSRIFODL;
                         ,this.w_PEQTAUM1;
                         ,this.w_DPCODODL;
                         ,this.w_TRSRIGORD;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                else
                  this.w_PESERORD = _Curs_GSDB2BAM.PERIFODL
                  this.w_PERIGORD = _Curs_GSDB2BAM.PERIGORD
                  * --- Insert into PEG_SELI
                  i_nConn=i_TableProp[this.PEG_SELI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
                    +","+cp_NullLink(cp_ToStrODBC("O"),'PEG_SELI','PETIPRIF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'PEG_SELI','PECODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PECODRIC),'PEG_SELI','PECODRIC');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"O",'PERIFODL',this.w_PESERORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PERIGORD',this.w_PERIGORD,'PECODCOM',this.w_OLTCOMME,'PECODRIC',this.w_PECODRIC)
                    insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
                       values (;
                         this.w_PESERIAL;
                         ,this.w_PECODODL;
                         ,"O";
                         ,this.w_PESERORD;
                         ,this.w_PEQTAUM1;
                         ,this.w_DPCODODL;
                         ,this.w_PERIGORD;
                         ,this.w_OLTCOMME;
                         ,this.w_PECODRIC;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              endif
          endcase
          this.w_PEQTAUM1 = 0
        endif
      endif
        select _Curs_GSDB2BAM
        continue
      enddo
      use
    endif
    if ! pegging and not empty (this.w_DPCODCOM) and (this.w_PEFLCOMM="S")
      * --- Inserisco sul pegging la quantit� non abbinata inserita sul magazzino per la commessa
      this.w_PETIPRIF = "M"
      this.w_PROGR = max(this.w_PROGR + 1,1)
      this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
      * --- Insert into PEG_SELI
      i_nConn=i_TableProp[this.PEG_SELI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIFODL"+",PEQTAABB"+",PEODLORI"+",PERIGORD"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PECODODL),'PEG_SELI','PESERODL');
        +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PESERORD),'PEG_SELI','PERIFODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PEQTAUM1),'PEG_SELI','PEQTAABB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODODL),'PEG_SELI','PEODLORI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PERIGORD),'PEG_SELI','PERIGORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DPCODCOM),'PEG_SELI','PECODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PECODART),'PEG_SELI','PECODRIC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_PECODODL,'PETIPRIF',"M",'PERIFODL',this.w_PESERORD,'PEQTAABB',this.w_PEQTAUM1,'PEODLORI',this.w_DPCODODL,'PERIGORD',this.w_PERIGORD,'PECODCOM',this.w_DPCODCOM,'PECODRIC',this.w_PECODART)
        insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIFODL,PEQTAABB,PEODLORI,PERIGORD,PECODCOM,PECODRIC &i_ccchkf. );
           values (;
             this.w_PESERIAL;
             ,this.w_PECODODL;
             ,"M";
             ,this.w_PESERORD;
             ,this.w_PEQTAUM1;
             ,this.w_DPCODODL;
             ,this.w_PERIGORD;
             ,this.w_DPCODCOM;
             ,this.w_PECODART;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conto lavoro di fase
    if this.w_FASPD>0 and NOT EMPTY(this.w_ODLPD) and g_PRFA="S" and g_CICLILAV="S"
      * --- Read from ODL_CICL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLULTFAS,CLROWORD,CLQTADIC,CLDICUM1"+;
          " from "+i_cTable+" ODL_CICL where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_FASPD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLULTFAS,CLROWORD,CLQTADIC,CLDICUM1;
          from (i_cTable) where;
              CLCODODL = this.w_ODLPD;
              and CPROWNUM = this.w_FASPD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLULTFAS = NVL(cp_ToDate(_read_.CLULTFAS),cp_NullValue(_read_.CLULTFAS))
        this.w_MYFAS = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
        this.w_CLQTADIC = NVL(cp_ToDate(_read_.CLQTADIC),cp_NullValue(_read_.CLQTADIC))
        this.w_CLDICUM1 = NVL(cp_ToDate(_read_.CLDICUM1),cp_NullValue(_read_.CLDICUM1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Aggiorna dettaglio CICLO dell'ODL Padre
      do case
        case this.w_FLGAGGOCL="C"
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLQTAAVA =CLQTAAVA+ "+cp_ToStrODBC(this.w_QTAMOV);
            +",CLAVAUM1 =CLAVAUM1+ "+cp_ToStrODBC(this.w_QTAUM1);
            +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_CICL','CLFASEVA');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_FASPD);
                   )
          else
            update (i_cTable) set;
                CLQTAAVA = CLQTAAVA + this.w_QTAMOV;
                ,CLAVAUM1 = CLAVAUM1 + this.w_QTAUM1;
                ,CLFASEVA = this.w_OLTFLEVA;
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_ODLPD;
                and CPROWNUM = this.w_FASPD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorna la quantit� dichiarabile
          * --- Read from ODL_CICL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLROWORD,CLAVAUM1,CLQTAAVA,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1"+;
              " from "+i_cTable+" ODL_CICL where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_FASPD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLROWORD,CLAVAUM1,CLQTAAVA,CLQTASCA,CLSCAUM1,CLQTADIC,CLDICUM1;
              from (i_cTable) where;
                  CLCODODL = this.w_ODLPD;
                  and CPROWNUM = this.w_FASPD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FASE = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
            this.w_CLAVAUM1 = NVL(cp_ToDate(_read_.CLAVAUM1),cp_NullValue(_read_.CLAVAUM1))
            this.w_CLQTAAVA = NVL(cp_ToDate(_read_.CLQTAAVA),cp_NullValue(_read_.CLQTAAVA))
            this.w_CLQTASCA = NVL(cp_ToDate(_read_.CLQTASCA),cp_NullValue(_read_.CLQTASCA))
            this.w_CLSCAUM1 = NVL(cp_ToDate(_read_.CLSCAUM1),cp_NullValue(_read_.CLSCAUM1))
            this.w_CLQTADIC = NVL(cp_ToDate(_read_.CLQTADIC),cp_NullValue(_read_.CLQTADIC))
            this.w_CLDICUM1 = NVL(cp_ToDate(_read_.CLDICUM1),cp_NullValue(_read_.CLDICUM1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLQTADIC =CLQTADIC+ "+cp_ToStrODBC(this.w_CLQTADIC+this.w_CLQTAAVA);
            +",CLDICUM1 =CLDICUM1+ "+cp_ToStrODBC(this.w_CLDICUM1+this.w_CLAVAUM1);
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
                +" and CLROWORD > "+cp_ToStrODBC(this.w_FASE);
                   )
          else
            update (i_cTable) set;
                CLQTADIC = CLQTADIC + this.w_CLQTADIC+this.w_CLQTAAVA;
                ,CLDICUM1 = CLDICUM1 + this.w_CLDICUM1+this.w_CLAVAUM1;
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_ODLPD;
                and CLROWORD > this.w_FASE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_FLGAGGOCL="D"
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLQTAAVA =CLQTAAVA- "+cp_ToStrODBC(this.w_QTAMOV);
            +",CLAVAUM1 =CLAVAUM1- "+cp_ToStrODBC(this.w_QTAUM1);
            +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_CICL','CLFASEVA');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_FASPD);
                   )
          else
            update (i_cTable) set;
                CLQTAAVA = CLQTAAVA - this.w_QTAMOV;
                ,CLAVAUM1 = CLAVAUM1 - this.w_QTAUM1;
                ,CLFASEVA = this.w_OLTFLEVA;
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_ODLPD;
                and CPROWNUM = this.w_FASPD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_FLGAGGOCL="M"
          this.w_QTADASTO = - this.w_QTAPRE + this.w_QTAMOV
          this.w_QTADAST1 = - this.w_QTAPR1 + this.w_QTAUM1
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLQTAAVA =CLQTAAVA+ "+cp_ToStrODBC(this.w_QTADASTO);
            +",CLAVAUM1 =CLAVAUM1+ "+cp_ToStrODBC(this.w_QTADAST1);
            +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_CICL','CLFASEVA');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_MYFAS);
                   )
          else
            update (i_cTable) set;
                CLQTAAVA = CLQTAAVA + this.w_QTADASTO;
                ,CLAVAUM1 = CLAVAUM1 + this.w_QTADAST1;
                ,CLFASEVA = this.w_OLTFLEVA;
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_ODLPD;
                and CLROWORD = this.w_MYFAS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
      if this.w_OLTFLEVA="S"
        this.w_CLQTADIC = this.w_OLTQTOEV
        this.w_CLDICUM1 = this.w_OLTQTOE1
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLQTADIC ="+cp_NullLink(cp_ToStrODBC(this.w_CLQTADIC),'ODL_CICL','CLQTADIC');
          +",CLDICUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_CLDICUM1),'ODL_CICL','CLDICUM1');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
              +" and CLROWNUM > "+cp_ToStrODBC(this.w_FASPD);
                 )
        else
          update (i_cTable) set;
              CLQTADIC = this.w_CLQTADIC;
              ,CLDICUM1 = this.w_CLDICUM1;
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_ODLPD;
              and CLROWNUM > this.w_FASPD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Legge dati di interesse da ODL Padre
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTFLORD,OLTFLIMP,OLTCOMAG,OLTKEYSA,OLTSTATO,OLTCOART,OLTCOMME,OLTQTSAL"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTFLORD,OLTFLIMP,OLTCOMAG,OLTKEYSA,OLTSTATO,OLTCOART,OLTCOMME,OLTQTSAL;
          from (i_cTable) where;
              OLCODODL = this.w_ODLPD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TmpC1 = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
        this.w_TmpC2 = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
        this.w_TmpC4 = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
        this.w_TmpC3 = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
        this.w_TmpC5 = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
        this.w_TmpART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
        this.w_TmpCOMM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
        this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_TmpART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_TmpART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TmpFLC = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ODL_CICL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLULTFAS"+;
          " from "+i_cTable+" ODL_CICL where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
              +" and CLROWNUM = "+cp_ToStrODBC(this.w_FASPD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLULTFAS;
          from (i_cTable) where;
              CLCODODL = this.w_ODLPD;
              and CLROWNUM = this.w_FASPD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLULTFAS = NVL(cp_ToDate(_read_.CLULTFAS),cp_NullValue(_read_.CLULTFAS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controlla se ultima fase
      if this.w_OLTFLEVA = "S" and this.w_OK
        * --- Select from ODL_MAST
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select OLTFLEVA AS CLFASEVA  from "+i_cTable+" ODL_MAST ";
              +" where OLCODODL = "+cp_ToStrODBC(this.w_OLTSECPR)+" AND (OLTFLEVA<>'S' OR OLTSTATO<>'F')";
               ,"_Curs_ODL_MAST")
        else
          select OLTFLEVA AS CLFASEVA from (i_cTable);
           where OLCODODL = this.w_OLTSECPR AND (OLTFLEVA<>"S" OR OLTSTATO<>"F");
            into cursor _Curs_ODL_MAST
        endif
        if used('_Curs_ODL_MAST')
          select _Curs_ODL_MAST
          locate for 1=1
          do while not(eof())
          this.w_OK = False
          this.w_MESS = "Impossibile completare una fase senza aver completato la precedente count point"
          this.w_MESS = ah_msgformat(this.w_MESS, this.w_ODLPD)
            select _Curs_ODL_MAST
            continue
          enddo
          use
        endif
      endif
      if this.w_CLULTFAS="S" and this.w_OK
        if ! this.w_TmpC5 $ "L-F" and this.w_FLGAGGOCL<>"D"
          this.w_MESS = "Aggiornamento conto/lavoro non possibile: ODL padre <%1> in stato pianificato%0Eseguire il lancio dell'ordine"
          this.w_MESS = ah_msgformat(this.w_MESS, this.w_ODLPD)
          this.w_OK = False
        else
          * --- Aggiorna anche ODL padre (nello stesso modo con cui ha aggiornato l'OCL di fase)
          * --- Write into ODL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLTQTOEV ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOEV),'ODL_MAST','OLTQTOEV');
            +",OLTQTOE1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOE1),'ODL_MAST','OLTQTOE1');
            +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_MAST','OLTFLEVA');
            +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTSAL),'ODL_MAST','OLTQTSAL');
            +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
            +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTFIN),'ODL_MAST','OLTDTFIN');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_ODLPD);
                   )
          else
            update (i_cTable) set;
                OLTQTOEV = this.w_OLTQTOEV;
                ,OLTQTOE1 = this.w_OLTQTOE1;
                ,OLTFLEVA = this.w_OLTFLEVA;
                ,OLTQTSAL = this.w_OLTQTSAL;
                ,OLTSTATO = this.w_OLTSTATO;
                ,OLTDTFIN = this.w_OLTDTFIN;
                &i_ccchkf. ;
             where;
                OLCODODL = this.w_ODLPD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorna saldi (stornati prima di fare tutto ...)
          this.w_TmpC1 = IIF(this.w_TmpC1="+", "-", IIF(this.w_TmpC1="-", "+", " "))
          this.w_TmpC2 = IIF(this.w_TmpC2="+", "-", IIF(this.w_TmpC2="-", "+", " "))
          if .F.
            this.w_TmpC1 = nvl(this.w_TmpC1, " ")
            this.w_TmpC2 = nvl(this.w_TmpC2, " ")
          endif
          this.w_QTADAST1 = (this.w_OQTSAL - this.w_OLTQTSAL)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_TmpC1,'SLQTOPER','this.w_QTADAST1',this.w_QTADAST1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_TmpC2,'SLQTIPER','this.w_QTADAST1',this.w_QTADAST1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_TmpC3);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_TmpC4);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_TmpC3;
                and SLCODMAG = this.w_TmpC4;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NVL(this.w_TmpFLC,"N")="S" and ! empty(nvl(this.w_TmpCOMM," "))
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_TmpC1,'SCQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_TmpC2,'SCQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_TmpC3);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_TmpC4);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_TmpCOMM);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_TmpC3;
                  and SCCODMAG = this.w_TmpC4;
                  and SCCODCAN = this.w_TmpCOMM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
      * --- Poich� siamo sull'ultima fase, abbiamo avanzato il codice <lungo>.
      * --- Ora lo storniamo e carichiamo il codice presente sull'ODL_MAST
      if this.w_FLGAGGOCL $ "C-M" and this.w_CLULTFAS="S" and this.w_OK
        this.w_QTAMOV = this.w_OLDQTAMOV
        this.w_QTAPRE = this.w_OLDQTAPRE
        this.w_switch = .t.
        * --- Scarico
        this.w_OPERAZCL = "SF"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Carico
        this.w_OPERAZCL = "CF"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_switch = .f.
      endif
    else
      * --- Verifica il caso particolare del salto di codice su ciclo monofase
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTSECIC"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_SEROCL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTSECIC;
          from (i_cTable) where;
              OLCODODL = this.w_SEROCL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TmpC1 = NVL(cp_ToDate(_read_.OLTSECIC),cp_NullValue(_read_.OLTSECIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if not empty(this.w_TmpC1)
        * --- Aggiorna dettaglio CICLO dell'ODL Padre
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLQTAAVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOEV),'ODL_CICL','CLQTAAVA');
          +",CLAVAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOE1),'ODL_CICL','CLAVAUM1');
          +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTFLEVA),'ODL_CICL','CLFASEVA');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_SEROCL);
                 )
        else
          update (i_cTable) set;
              CLQTAAVA = this.w_OLTQTOEV;
              ,CLAVAUM1 = this.w_OLTQTOE1;
              ,CLFASEVA = this.w_OLTFLEVA;
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_SEROCL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scan transitorio
    this.w_TRIG = this.w_TRIG + 1
    this.w_NR = ALLTRIM(STR(this.w_PADRE.GET("t_CPROWORD")))
    this.w_ROWORD = this.w_PADRE.GET("CPROWNUM")
    this.w_QTASAL = NVL(this.w_PADRE.GET("t_MVQTASAL"), 0)
    this.w_QTAMOV = NVL(this.w_PADRE.GET("t_MVQTAMOV"), 0)
    this.w_QTAUM1 = NVL(this.w_PADRE.GET("t_MVQTAUM1"), 0)
    this.w_COECONV = this.w_QTAUM1 / this.w_QTAMOV
    this.w_OQTAMOV = VAL(SUBSTR(NVL(this.w_PADRE.GET("t_OFLDIF"), SPACE(35) ) , 24, 12))
    this.w_OQTAUM1 = this.w_OQTAMOV * this.w_COECONV
    this.w_OCODCOM = NVL(this.w_PADRE.GET("MVCODCOM"), SPACE(15))
    this.w_OCODCOM = NVL(this.w_PADRE.GET("MVCODCOM"), SPACE(15))
    this.w_OCODMAG = NVL(this.w_PADRE.GET("MVCODMAG"), SPACE(5))
    * --- Testa se Evade Documento di Origine
    this.w_FLEVAS = NVL(this.w_PADRE.GET("t_MVFLERIF"), " ")
    this.w_MAGDOC = NVL(this.w_PADRE.GET("t_MVCODMAG"), SPACE(5))
    this.w_CODRIC = NVL(this.w_PADRE.GET("t_MVCODICE"), SPACE(20))
    this.w_COART = NVL(this.w_PADRE.GET("t_MVCODART"), SPACE(20))
    this.w_CECOS = NVL(this.w_PADRE.GET("t_MVCODCEN"),SPACE(15))
    this.w_VOCOS = NVL(this.w_PADRE.GET("t_MVVOCCEN"),SPACE(15))
    this.w_COMME = NVL(this.w_PADRE.GET("t_MVCODCOM"),SPACE(15))
    this.w_COATT = NVL(this.w_PADRE.GET("t_MVCODATT"),SPACE(15))
    this.w_UMRIF = NVL(this.w_PADRE.GET("t_MVUNIMIS"),SPACE(3))
    * --- Riga Documento di origine (ordine a fornitore) generato da C/L
    this.w_SERRIF = NVL(this.w_PADRE.GET("t_MVSERRIF"), SPACE(10))
    this.w_ROWRIF = NVL(this.w_PADRE.GET("t_MVROWRIF"), 0)
    this.w_CLARIF = NVL(this.w_PADRE.GET("t_CLARIF"), SPACE(2))
    this.w_ODLRIF = NVL(this.w_PADRE.GET("t_ODLRIF"), SPACE(10))
    this.w_SRV = this.w_PADRE.RowStatus()
    this.w_OLDQTAMOV = this.w_QTAMOV
    this.w_OLDQTAPRE = this.w_QTAPRE
    * --- ------------------------------------------------------------------------------------------------------------------------------
    * --- Riferimento ai documenti generati dal rientro del conto lavoro
    this.w_DELMASCF = this.w_DELTESTATA
    this.w_SEDOCCF = SPACE(10)
    this.w_DELMASSF = this.w_DELTESTATA
    this.w_SEDOCSF = SPACE(10)
    this.w_DELMASSM = this.w_DELTESTATA
    this.w_SEDOCSM = SPACE(10)
    * --- ------------------------------------------------------------------------------------------------------------------------------
    * --- Seriale OCL di origine
    this.w_SEROCL = SPACE(15)
    * --- Read from RIF_GODL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.RIF_GODL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2],.t.,this.RIF_GODL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDCODODL"+;
        " from "+i_cTable+" RIF_GODL where ";
            +"PDSERIAL = "+cp_ToStrODBC(this.w_ODLRIF);
            +" and PDTIPGEN = "+cp_ToStrODBC(this.w_CLARIF);
            +" and PDSERDOC = "+cp_ToStrODBC(this.w_SERRIF);
            +" and PDROWDOC = "+cp_ToStrODBC(this.w_ROWRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDCODODL;
        from (i_cTable) where;
            PDSERIAL = this.w_ODLRIF;
            and PDTIPGEN = this.w_CLARIF;
            and PDSERDOC = this.w_SERRIF;
            and PDROWDOC = this.w_ROWRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SEROCL = NVL(cp_ToDate(_read_.PDCODODL),cp_NullValue(_read_.PDCODODL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTPROVE,OLTFAODL,OLTSEODL,OLTULFAS,OLTSECPR"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_SEROCL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTPROVE,OLTFAODL,OLTSEODL,OLTULFAS,OLTSECPR;
        from (i_cTable) where;
            OLCODODL = this.w_SEROCL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
      this.w_FASPD = NVL(cp_ToDate(_read_.OLTFAODL),cp_NullValue(_read_.OLTFAODL))
      this.w_ODLPD = NVL(cp_ToDate(_read_.OLTSEODL),cp_NullValue(_read_.OLTSEODL))
      this.w_CLULTFAS = NVL(cp_ToDate(_read_.OLTULFAS),cp_NullValue(_read_.OLTULFAS))
      this.w_OLTSECPR = NVL(cp_ToDate(_read_.OLTSECPR),cp_NullValue(_read_.OLTSECPR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Fa qualcosa solo se c'� un OCL collegato
    if NOT EMPTY(this.w_SEROCL)
      if EMPTY(this.w_CODMAG) AND this.w_PROVE="L"
        this.w_OK = .F.
        this.w_MESS = ah_Msgformat("Codice magazzino fornitore C/Lavoro non definito; impossibile confermare")
        EXIT
      endif
      * --- Riferimenti a vecchi valori
      this.w_QTAPRE = VAL(SUBSTR(NVL(this.w_PADRE.GET("t_OFLDIF"), 0), 24, 12))
      this.w_MAGPRE = NVL(this.w_PADRE.GET("MVCODMAG"),SPACE(5))
      this.w_OLDFLERIF = NVL(this.w_PADRE.GET("MVFLERIF"), "N")
      * --- Test Se riga Eliminata - Cancella eventuale DOC generato e aggiorna OCL collegato
      this.w_FLDEL = this.w_CFUNC="Query" OR this.w_SRV="D"
      if this.w_FLDEL and (not EMPTY(this.w_SEROCL)) and this.w_ROWRIF<>0
        * --- Riga Eliminata gia' Inserita
        this.w_FLGAGGOCL = "D"
        if this.w_PROVE="L"
          if USED("DOCSCA")
            SELECT DOCSCA
            GO TOP
            * --- Per mantenere compatibilit� con il vecchio prendo in considerazione anche le righe
            *     con riferimento alla riga del DDT a  zero
            SCAN FOR MVSERDDT=this.w_SERIAL AND (MVROWDDT=this.w_ROWORD OR MVROWDDT=0)
            this.w_SEDOC = MVSERIAL
            this.w_RIGMAT = NVL(MVRIGMAT, 0)
            this.w_DELTESTATA = "S"
            this.w_FLGAGGOCL = "D"
            * --- Nel caso il tipo del documento sia vuoto ()variabile w_DOCDAGEN lo elimino in modo da ricrearlo corretto
            *     In MVRIFORDL dovrebbe esserci scritto il tipo SCARICOMCL-SCARICOFCL-CARICOFCL
            this.w_FLGAGGOCL = "D"
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT DOCSCA
            ENDSCAN
            SELECT (this.oParentObject.cTrsName)
            if this.w_OK
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_FASPD>0
                this.Page_11()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
        else
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Casi: riga Inserita o riga variata Variata
        this.w_FLAPP = this.w_SRV="A"
        if this.w_CFUNC="Load" OR this.w_SRV="A"
          * --- Nuova Riga non Eliminata
          this.w_FLGAGGOCL = "C"
          if this.w_PROVE="L"
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT (this.oParentObject.cTrsName)
            if this.w_OK
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if this.w_OK and this.w_FASPD>0
              this.w_FLGAGGOCL = "C"
              this.Page_11()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_MATRUPD = this.w_PADRE.GSAC_MMP.IsAChildUpdated()
          if this.w_SRV="U" or this.w_MATRUPD
            if this.w_PROVE="L"
              * --- Riga Variata
              this.w_FLGAGGOCL = "D"
              this.w_ELIMINA = .T.
              * --- Nel caso in cui sono all'ultima fase allora effettuo il controllo se devo oppure no eliminare il movimento di magazzino
              this.w_ELIMINA = this.w_MVQTAMOV<>this.w_OQTAMOV Or this.w_MVQTAUM1<>this.w_OQTAUM1 Or this.w_MVCODCOM<>this.w_OCODCOM Or this.w_MVCODMAG<>this.w_OCODMAG
              this.w_ELIMINA = this.w_QTAPRE<>this.w_QTASAL OR this.w_RIGMOVLOT
              this.w_ELIMINA = this.w_ELIMINA Or this.w_PADRE.GSAC_MMP.IsAChildUpdated()
              this.w_ELIMINA = this.w_ELIMINA Or this.w_PADRE.GSVE_MMT.IsAChildUpdated()
              if this.w_ELIMINA
                * --- Riga Eliminata gia' Inserita
                if this.w_PROVE="L"
                  if USED("DOCSCA")
                    SELECT DOCSCA
                    GO TOP
                    * --- Per mantenere compatibilit� con il vecchio prendo in considerazione anche le righe
                    *     con riferimento alla riga del DDT a  zero
                    SCAN FOR MVSERDDT=this.w_SERIAL AND MVROWDDT=this.w_ROWORD
                    this.w_SEDOC = MVSERIAL
                    this.w_RIGMAT = NVL(MVRIGMAT, 0)
                    this.w_DOCDAGEN = ALLTRIM(NVL(MVRIFODL, SPACE(10)))
                    this.w_MVCODODL = NVL(MVCODODL,SPACE(15))
                    * --- Nel caso il tipo del documento sia vuoto ()variabile w_DOCDAGEN lo elimino in modo da ricrearlo corretto
                    *     In MVRIFORDL dovrebbe esserci scritto il tipo SCARICOMCL-SCARICOFCL-CARICOFCL
                    if Empty(this.w_DOCDAGEN) or Empty(this.w_MVCODODL)
                      * --- Verifico se � un movimento di scarico da conto lavoro
                      *     Lo identifico perch� ha MVRIGMAT maggiore di zero
                      this.w_DOCDAGEN = SPACE(10)
                      this.w_DELTESTATA = "S"
                    else
                      * --- Se � un ordine di conto lavoro di fase e la fase � l'ultima 
                      *     non elimino la testata dovr� ricreare il documento
                      if this.w_FASPD>0 and this.w_CLULTFAS="S" and this.w_DOCDAGEN<>"SCARICOMCL"
                        this.w_DELTESTATA = "N"
                      else
                        this.w_DELTESTATA = "S"
                      endif
                    endif
                    this.w_FLGAGGOCL = "D"
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    * --- In base al tipo di documento mi memorizzo se ho eliminato l
                    *     a testata e l'eventuale seriale documento da rigenerare
                    this.w_SEDOC = IIF( this.w_DELTESTATA="S" , SPACE(10), this.w_SEDOC)
                    do case
                      case this.w_DOCDAGEN == "CARICOFCL"
                        this.w_DELMASCF = this.w_DELTESTATA
                        this.w_SEDOCCF = this.w_SEDOC
                      case this.w_DOCDAGEN == "SCARICOFCL"
                        this.w_DELMASSF = this.w_DELTESTATA
                        this.w_SEDOCSF = this.w_SEDOC
                      case this.w_DOCDAGEN == "SCARICOMCL"
                        this.w_DELMASSM = this.w_DELTESTATA
                        this.w_SEDOCSM = this.w_SEDOC
                    endcase
                    SELECT DOCSCA
                    ENDSCAN
                    * --- Ora li ricreo
                    this.w_switch = .f.
                    this.w_FLGAGGOCL = "M"
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    SELECT (this.oParentObject.cTrsName)
                    if this.w_OK
                      this.Page_3()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                    if this.w_OK and this.w_FASPD>0
                      this.Page_11()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  endif
                else
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            else
              * --- Riga Variata
              this.w_FLGAGGOCL = "D"
              if this.w_QTAPRE<>this.w_QTASAL
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_FLGAGGOCL = "C"
                SELECT (this.oParentObject.cTrsName)
                if this.w_OK
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,28)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='DOC_RATE'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='ODL_DETT'
    this.cWorkTables[9]='ODL_MAST'
    this.cWorkTables[10]='PAR_PROD'
    this.cWorkTables[11]='SALDIART'
    this.cWorkTables[12]='TIP_DOCU'
    this.cWorkTables[13]='RIF_GODL'
    this.cWorkTables[14]='MOVIMATR'
    this.cWorkTables[15]='MVM_DETT'
    this.cWorkTables[16]='*TMPMOVIMAST'
    this.cWorkTables[17]='*DELTA'
    this.cWorkTables[18]='MAT_PROD'
    this.cWorkTables[19]='AGG_LOTT'
    this.cWorkTables[20]='SPL_LOTT'
    this.cWorkTables[21]='DIC_MATR'
    this.cWorkTables[22]='DIC_MCOM'
    this.cWorkTables[23]='MATRICOL'
    this.cWorkTables[24]='PEG_SELI'
    this.cWorkTables[25]='MPS_TFOR'
    this.cWorkTables[26]='SALDICOM'
    this.cWorkTables[27]='ODL_CICL'
    this.cWorkTables[28]='CAM_AGAZ'
    return(this.OpenAllTables(28))

  proc CloseCursors()
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_GSCL_BRL')
      use in _Curs_GSCL_BRL
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_MOVIMATR')
      use in _Curs_MOVIMATR
    endif
    if used('_Curs_GSCO_BAD')
      use in _Curs_GSCO_BAD
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_PEG_SELI')
      use in _Curs_PEG_SELI
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    if used('_Curs_GSDB2BAM')
      use in _Curs_GSDB2BAM
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
