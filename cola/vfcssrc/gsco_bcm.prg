* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bcm                                                        *
*              Gestione componenti matricole                                   *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_166]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-26                                                      *
* Last revis.: 2015-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bcm",oParentObject,m.pTipo)
return(i_retval)

define class tgsco_bcm as StdBatch
  * --- Local variables
  pTipo = space(10)
  GSCO_MCO = .NULL.
  w_GENCOMP = .f.
  w_DPCODODL = space(15)
  w_STATOANAL = space(10)
  w_DPQTAPR1 = 0
  w_DPQTASC1 = 0
  w_FLLOTTI = space(1)
  w_DESARTI = space(40)
  w_MTKEYSAL = space(40)
  w_MESS = space(100)
  w_SALDO = 0
  w_PRODU = 0
  * --- WorkFile variables
  MOVIMATR_idx=0
  MATRICOL_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione matricole
    do case
      case this.pTipo="CREATEROW"
        * --- Crea Righe dei componenti gestiti a matricole
        this.GSCO_MCO = this.oParentObject
        * --- Inizializza le variabili dal nonno
        this.w_DPCODODL = this.GSCO_MCO.oParentObject.w_DPCODODL
        this.w_STATOANAL = this.GSCO_MCO.oParentObject.w_STATOGES
        this.w_DPQTAPR1 = this.GSCO_MCO.oParentObject.w_DPQTAPR1
        this.w_DPQTASC1 = this.GSCO_MCO.oParentObject.w_DPQTASC1
        * --- Verifica se la fase � obbligatoria
        this.w_GENCOMP = not empty(this.w_DPCODODL) and this.w_STATOANAL="LOAD"
        if this.w_GENCOMP and this.w_DPQTAPR1+this.w_DPQTASC1>0
          * --- Componenti Gestiti a matricole da scaricare
          cCurMCO = this.GSCO_MCO.cTrsname
          if not empty(cCurMCO) and used(cCurMCO)
            Select (cCurMCO) 
 zap
            cCurMMP = this.GSCO_MCO.oParentObject.GSCO_MMP.cTrsname
            Select * from (cCurMMP) where t_MATCOM="S" into cursor temp
            Select Temp
            go top 
 scan
            * --- Aggiunge le righe nel figlio
            ah_Msg("Ricerca articoli gestiti a matricole",.T.)
            this.GSCO_MCO.InitRow()     
            this.GSCO_MCO.w_MTMAGSCA = temp.t_MPCODMAG
            this.GSCO_MCO.w_MTFLCARI = " "
            this.GSCO_MCO.w_MTFLSCAR = "E"
            * --- Riempie le righe del cursore
            this.GSCO_MCO.w_MTROWODL = Temp.t_MPROWODL
            this.GSCO_MCO.w_CPROWORD = Temp.t_CPROWORD
            this.GSCO_MCO.w_MTCODRIC = Temp.t_MPCODICE
            this.GSCO_MCO.w_CODART = Temp.t_MPCODART
            this.GSCO_MCO.w_MTUNIMIS = Temp.t_MPUNIMIS
            this.GSCO_MCO.w_MTCOEIMP = Temp.t_MPCOEIMP
            this.GSCO_MCO.w_MTQTAMOV = Temp.t_MPCOEIMP * (this.w_DPQTAPR1+this.w_DPQTASC1)
            this.GSCO_MCO.w_MTKEYSAL = left(Temp.t_MPCODART+space(40),40)
            this.GSCO_MCO.w_MTCODCOM = NVL(Temp.t_MPCODCOM, SPACE(15))
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLLOTT,ARDESART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(Temp.t_MPCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLLOTT,ARDESART;
                from (i_cTable) where;
                    ARCODART = Temp.t_MPCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLLOTTI = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
              this.w_DESARTI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.GSCO_MCO.w_FLLOTT = this.w_FLLOTTI
            this.GSCO_MCO.w_DESART = this.w_DESARTI
            * --- Disabilita il campo Codice ODL
            this.GSCO_MCO.oParentObject.w_EDCOMMAT = False
            * --- Carica il Temporaneo dei Dati e skippa al record successivo
            this.GSCO_MCO.TrsFromWork()     
            endscan
            if USED("Temp")
              SELECT Temp
              USE
            endif
            * --- Rinfrescamenti vari
            SELECT (cCurMCO) 
 GO TOP 
 With this.GSCO_MCO 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
            * --- Verifico se lanciare il caricamento (Area Manuale GSDB_MCM Blank Record End)
            With this.GSCO_MCO.GSCO_MCM 
 .InitSon() 
 .TrsFromWork() 
 If Not Empty(.w_MTMAGCAR) or Not Empty(.w_MTMAGSCA) 
 * sono in un caricamento verifico se riga aggiunta 
 Local L_area,L_Srv 
 L_Area=Select() 
 Select(.oParentObject.cTrsName) 
 L_Srv=I_Srv 
 Select (L_Area) 
 * se riga in append e attivo flag caricmaento rapido 
 * sulla causale 
 If L_Srv="A" And This.oParentObject.w_MTCARI="S" 
 .NotifyEvent("CarRapido") 
 Endif 
 Endif 
 EndWith
          endif
        endif
      case this.pTipo="DELETEROW" Or this.pTipo="UPDATEROW" Or this.pTipo="INSERTROW"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.pTipo="DELETEROW"
          this.oParentObject.w_MT__FLAG = " "
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check cancellazione/ modifica Riga
    * --- Notifico a GSxx_BMK di creare i lotti / ubicazioni (ho modificato qualcosa)
    This.oParentObject.oParentObject.w_RIGMOVLOT=.T.
    this.w_MTKEYSAL = this.oParentObject.w_MTKEYSAL
    if this.oParentObject.w_MT_SALDO<>0 And ( this.pTipo="DELETEROW" Or this.pTipo="UPDATEROW" )
      Local L_Area 
 L_area=Select() 
 SELECT (this.oParentObject.cTrsName)
      this.w_MESS = ah_Msgformat("Impossibile eliminare / modificare matricola (%1) dell'articolo %2",ALLTRIM(MTCODMAT), Left(MTKEYSAL,20))
      Select ( L_Area )
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
    * --- Verifico all'inserimento che la matricola non venga utilizzata in contemporanea
    *     da un altro utente.
    *     Due casi
    *     a) La matricola � stata movimentata in precedenza (MTRIFNUM � pieno)
    *     b) Primo carico
    if this.pTipo="INSERTROW"
      this.w_SALDO = -1
      * --- Read from MATRICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MATRICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AM_PRENO,AM_PRODU"+;
          " from "+i_cTable+" MATRICOL where ";
              +"AMKEYSAL = "+cp_ToStrODBC(this.w_MTKEYSAL);
              +" and AMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AM_PRENO,AM_PRODU;
          from (i_cTable) where;
              AMKEYSAL = this.w_MTKEYSAL;
              and AMCODICE = this.oParentObject.w_MTCODMAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALDO = NVL(cp_ToDate(_read_.AM_PRENO),cp_NullValue(_read_.AM_PRENO))
        this.w_PRODU = NVL(cp_ToDate(_read_.AM_PRODU),cp_NullValue(_read_.AM_PRODU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if (this.w_SALDO<>1 AND this.oParentObject.w_FLPRD="+") OR (this.w_SALDO<>0 AND this.oParentObject.w_FLPRD=" ") OR (this.oParentObject.w_MTFLCARI="E" AND this.w_PRODU<>1)
        this.w_MESS = ah_Msgformat("Impossibile utilizzare matricola (%1) dell'articolo %2. Gi� utilizzata da altro utente", ALLTRIM(this.oParentObject.w_MTCODMAT), Left(this.w_MTKEYSAL,20) )
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      else
        this.w_SALDO = -1
        if Not Empty( this.oParentObject.w_MTRIFNUM )
          * --- Leggo in MOVIMATR il valore del saldo se a uno tutto OK - Evento scatenato dopo l'Update da saldo
          *     automatico
          * --- Read from MOVIMATR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MT_SALDO"+;
              " from "+i_cTable+" MOVIMATR where ";
                  +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MTSERRIF);
                  +" and MTROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MTROWRIF);
                  +" and MTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MTRIFNUM);
                  +" and MTKEYSAL = "+cp_ToStrODBC(this.w_MTKEYSAL);
                  +" and MTCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MT_SALDO;
              from (i_cTable) where;
                  MTSERIAL = this.oParentObject.w_MTSERRIF;
                  and MTROWNUM = this.oParentObject.w_MTROWRIF;
                  and MTNUMRIF = this.oParentObject.w_MTRIFNUM;
                  and MTKEYSAL = this.w_MTKEYSAL;
                  and MTCODMAT = this.oParentObject.w_MTCODMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALDO = NVL(cp_ToDate(_read_.MT_SALDO),cp_NullValue(_read_.MT_SALDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Inverto il valore se MTRIFNUM � >0
          this.w_SALDO = iif(this.w_SALDO>1,1,0)
        else
          * --- Nel caso di primo carico verifico la presenza della matricola in MOVIMATR
          * --- Select from MOVIMATR
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MOVIMATR ";
                +" where MTCODMAT= "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT)+" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)+"";
                 ,"_Curs_MOVIMATR")
          else
            select Count(*) As Conta from (i_cTable);
             where MTCODMAT= this.oParentObject.w_MTCODMAT and MTKEYSAL=this.w_MTKEYSAL;
              into cursor _Curs_MOVIMATR
          endif
          if used('_Curs_MOVIMATR')
            select _Curs_MOVIMATR
            locate for 1=1
            do while not(eof())
            this.w_SALDO = Nvl ( _Curs_MOVIMATR.CONTA , 0 )
              select _Curs_MOVIMATR
              continue
            enddo
            use
          endif
        endif
        if this.w_SALDO<>0
          this.w_MESS = ah_Msgformat("Impossibile utilizzare matricola (%1) dell'articolo %2. Gi� utilizzata da altro utente", ALLTRIM(this.oParentObject.w_MTCODMAT), Left(this.w_MTKEYSAL,20) )
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOVIMATR'
    this.cWorkTables[2]='MATRICOL'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_MOVIMATR')
      use in _Curs_MOVIMATR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
