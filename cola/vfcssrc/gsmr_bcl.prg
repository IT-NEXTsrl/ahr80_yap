* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bcl                                                        *
*              Caricamento rapido lotti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-12                                                      *
* Last revis.: 2016-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bcl",oParentObject,m.pOPER)
return(i_retval)

define class tgsmr_bcl as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_PUNPAD = .NULL.
  w_TOTSEL = .f.
  w_TOTDIS = .f.
  w_CHKCODUBI = .f.
  w_CHKQTA = .f.
  w_RECPOS = 0
  w_TMPCODLOT = space(20)
  w_TMPCODUBI = space(20)
  w_TMPQTA = 0
  w_MAGAZ = space(5)
  w_CRSERIAL = space(10)
  w_CRROWNUM = 0
  w_CRNUMRIF = 0
  w_CRSERRIF = space(10)
  w_CRROWRIF = 0
  w_CRRIFNUM = 0
  w_CRMAGCAR = space(5)
  w_CRFLCARI = space(1)
  w_CRKEYSAL = space(40)
  w_CRCODUBI = space(20)
  w_CRCODLOT = space(20)
  w_CRQTASEL = 0
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_CAUMAG = space(5)
  w_CODCOM = space(15)
  w_RIGORI = 0
  w_SERIAL = space(10)
  w_NUMRIF = 0
  w_TMPQTA2 = 0
  w_NONSEL = .f.
  w_SPOSTA = .f.
  w_CRUNIMIS = space(3)
  RigaArch = 0
  w_rigamvm = 0
  w_COMMAPPO = space(15)
  w_CODART = space(20)
  w_MMCODLOT = space(20)
  w_MMCODUBI = space(20)
  w_MMLOTMAG = space(5)
  w_oMESS = .NULL.
  * --- WorkFile variables
  MOVIMATR_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  CAM_AGAZ_idx=0
  SALDIART_idx=0
  SALDICOM_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  DOC_DETT_idx=0
  DIC_MATR_idx=0
  DIC_MCOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Caricamento Rapido lotti da GSMR_KCL
    this.w_PUNPAD = this.oParentObject
    NS = this.w_PUNPAD.w_ZoomSel.cCursor
    NC = this.w_PUNPAD.w_ZoomCar.cCursor
    do case
      case this.pOPER="R"
        * --- Eseguo la Ricerca
        this.w_PUNPAD.NotifyEvent("Esegui")     
        Select (NC)
        if RecCount(NC)>0
          zap
        endif
        this.oParentObject.w_CONTA = 0
        this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1
      case this.pOPER="C"
        * --- Seleziono nel Primo Zoom lotto/ubicazioni
        Select (NS)
        this.w_RECPOS = RECNO()
        * --- Controlli dopo aver selezionato la riga nel primo Zoom
        this.oParentObject.w_CONTA = this.oParentObject.w_CONTA+nvl(SUQTACAR,0)
        this.w_TOTDIS = nvl(SUQTACAR,0)>nvl(SUDISPON,0)
        this.w_CHKQTA = !(nvl(SUQTACAR,0)>0)
        this.w_TOTSEL = nvl(SUQTACAR,0)>this.oParentObject.w_NPROG
        do case
          case this.w_CHKQTA
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
            this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
            GOTO this.w_RECPOS
            this.w_PUNPAD.w_ZoomSel.grd.refresh
            this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
          case this.w_TOTDIS
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
            this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
            GOTO this.w_RECPOS
            this.w_PUNPAD.w_ZoomSel.grd.refresh
            this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
          case this.w_TOTSEL
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
            this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
            GOTO this.w_RECPOS
            this.w_PUNPAD.w_ZoomSel.grd.refresh
            this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
          otherwise
            Select (NS)
            this.w_TMPCODLOT = NVL(SUCODLOT,Space(20))
            this.w_TMPQTA = nvl(SUQTACAR,0)
            * --- Prendo l'ubicazione dal primo Zoom se � vuota la selezione
            this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICAR="S" and this.oParentObject.w_MGUBISCA="S" and this.oParentObject.w_MAGSCA<>this.oParentObject.w_MAGCAR
            if this.oParentObject.w_MAGCAR = this.oParentObject.w_MAGSCA and empty(nvl(this.oParentObject.w_CODUBI," "))
              this.w_TMPCODUBI = NVL(SUCODUBI,Space(20))
            else
              this.w_TMPCODUBI = this.oParentObject.w_CODUBI
            endif
            if this.w_CHKCODUBI and empty(nvl(this.w_TMPCODUBI,""))
              this.w_RECPOS = RECNO()
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
              this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
              GOTO this.w_RECPOS
              this.w_PUNPAD.w_ZoomSel.grd.refresh
              this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-1
            else
              this.w_MAGAZ = this.oParentObject.w_MAGCAR
              insert into (NC) values (this.w_MAGAZ,this.w_TMPCODUBI,this.w_TMPCODLOT,this.w_TMPQTA)
              this.w_PUNPAD.w_ZoomCar.grd.refresh
            endif
        endcase
        this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1- this.oParentObject.w_CONTA
      case this.pOPER="U"
        * --- Deseleziona riga primo Zoom:decremento contatore
        this.w_TMPQTA = nvl(SUQTACAR,0)
        this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-this.w_TMPQTA
        this.w_TMPCODLOT = NVL(SUCODLOT,Space(20))
        this.w_TMPCODUBI = NVL(SUCODUBI,Space(20))
        * --- Aggiorno direttamente il campo XCHK del cursore (NS), primo zoom, 
        *      in quanto in alcuni casi  con Uncheck non si verifica l'aggiornamento a 0 di tale campo 
        SELECT (NS)
        REPLACE XCHK WITH 0
        * --- Memorizzo Codice Matricola
        * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
        SELECT (NC)
        if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
          LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
          DELETE
        endif
        GO TOP
        * --- Refresh della griglia
        this.w_PUNPAD.w_ZoomCar.grd.refresh
        this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
      case this.pOPER="L"
        * --- Se premo F10 evito il controllo sul pulsante, quindi devo rieffettuare la verifica delle quantit� corrette
        if this.oParentObject.w_CONTA>0 and this.oParentObject.w_NPROG=0
          * --- Controllo se l'utente ha modificato la quantit� sulla riga selezionata
          Select (NS)
          this.w_RECPOS = RECNO()
          if XCHK=1
            * --- Memorizzo quantit� e chiave lotto/ubicazione
            this.w_TMPQTA = nvl(SUQTACAR,0)
            this.w_TMPCODLOT = NVL(SUCODLOT,Space(20))
            this.w_TMPCODUBI = NVL(SUCODUBI,Space(20))
            * --- Prima di fare qualsiasi considerazione relativa al secondo zoom verifico che la quantit� inserita sia corretta
            SELECT (NC)
            if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
              LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
              if QTASEL<>this.w_TMPQTA
                this.w_TMPQTA2 = nvl(QTASEL,0)
                Select (NS)
                this.oParentObject.w_CONTA = this.oParentObject.w_CONTA+nvl(SUQTACAR,0)-this.w_TMPQTA2
                this.w_TOTDIS = nvl(SUQTACAR,0)>nvl(SUDISPON,0)
                this.w_CHKQTA = !(nvl(SUQTACAR,0)>0)
                this.w_TOTSEL = nvl(SUQTACAR,0)>(this.oParentObject.w_NPROG+this.w_TMPQTA2)
                do case
                  case this.w_CHKQTA
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                    this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                    GOTO this.w_RECPOS
                    this.w_PUNPAD.w_ZoomSel.grd.refresh
                    this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
                    * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                    SELECT (NC)
                    if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
                      LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                      DELETE
                    endif
                    GO TOP
                    * --- Refresh della griglia
                    this.w_PUNPAD.w_ZoomCar.grd.refresh
                    this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                    i_retcode = 'stop'
                    return
                  case this.w_TOTDIS
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                    this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                    GOTO this.w_RECPOS
                    this.w_PUNPAD.w_ZoomSel.grd.refresh
                    this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
                    * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                    SELECT (NC)
                    if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
                      LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                      DELETE
                    endif
                    GO TOP
                    * --- Refresh della griglia
                    this.w_PUNPAD.w_ZoomCar.grd.refresh
                    this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                    i_retcode = 'stop'
                    return
                  case this.w_TOTSEL
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                    this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                    GOTO this.w_RECPOS
                    this.w_PUNPAD.w_ZoomSel.grd.refresh
                    this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
                    * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                    SELECT (NC)
                    if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
                      LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                      DELETE
                    endif
                    GO TOP
                    * --- Refresh della griglia
                    this.w_PUNPAD.w_ZoomCar.grd.refresh
                    this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                    i_retcode = 'stop'
                    return
                  otherwise
                    this.w_NONSEL = (this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA)>0
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                    SELECT (NC)
                    if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
                      LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                      REPLACE QTASEL with this.w_TMPQTA
                    endif
                    GO TOP
                    this.w_PUNPAD.w_ZoomCar.grd.refresh
                    i_retcode = 'stop'
                    return
                endcase
              endif
            endif
          endif
          * --- Controllo se l'utente ha modificato la quantit� sulle altre righe
          Select (NS)
          this.w_RECPOS = RECNO()
          this.w_TMPQTA2 = 0
          go top 
 scan
          if XCHK=1
            * --- Memorizzo quantit� e chiave lotto/ubicazione
            this.w_TMPQTA = nvl(SUQTACAR,0)
            this.w_TMPQTA2 = nvl(SUQTACAR,0)+this.w_TMPQTA2
            this.w_TMPCODLOT = NVL(SUCODLOT,Space(20))
            this.w_TMPCODUBI = NVL(SUCODUBI,Space(20))
            * --- Prima di fare qualsiasi considerazione relativa al secondo zoom verifico che la quantit� inserita sia corretta
            SELECT (NC)
            if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
              LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
              if QTASEL<>this.w_TMPQTA
                * --- La riga � sicuramente sbagliata perch� � stata modificata senza aggiornare nulla
                Select (NS)
                this.w_SPOSTA = .t.
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                SELECT (NC)
                LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-NVL(QTASEL,0)
                this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                DELETE
                GO TOP
                * --- Refresh della griglia
                this.w_PUNPAD.w_ZoomCar.grd.refresh
                this.w_TMPQTA2 = this.w_TMPQTA2-this.w_TMPQTA
                i_retcode = 'stop'
                return
              endif
            endif
          endif
          endscan
          * --- ricalcolare conta e tutto il resto
          GOTO this.w_RECPOS
          this.w_PUNPAD.w_ZoomSel.grd.refresh
          Select (NC)
          GO TOP
          Select * from (NC) into Cursor Carica Order by codlot,codubi NoFilter
          Select * from (NS) into Cursor Scarica where xchk=1 Order by sucodlot, sucodubi NoFilter
          * --- Riga massima cprownum (per eventuale aggiunta righe a lotti/ubicazioni)
          this.w_CRSERIAL = this.w_PUNPAD.oParentObject.w_MMSERIAL
          * --- Select from MVM_DETT
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select max(cprownum) as maxrow  from "+i_cTable+" MVM_DETT ";
                +" where MMSERIAL="+cp_ToStrODBC(this.w_CRSERIAL)+"";
                 ,"_Curs_MVM_DETT")
          else
            select max(cprownum) as maxrow from (i_cTable);
             where MMSERIAL=this.w_CRSERIAL;
              into cursor _Curs_MVM_DETT
          endif
          if used('_Curs_MVM_DETT')
            select _Curs_MVM_DETT
            locate for 1=1
            do while not(eof())
            this.w_rigamvm = maxrow
              select _Curs_MVM_DETT
              continue
            enddo
            use
          endif
          * --- Scrivo direttamente sul database
          * --- Mov.lotti di scarico
          this.w_CRROWNUM = this.w_PUNPAD.oParentObject.w_CPSCA
          this.w_CRNUMRIF = this.w_PUNPAD.oParentObject.w_MMNUMRIF
          this.w_CRMAGCAR = this.w_PUNPAD.oParentObject.w_MAGSCA
          this.w_CRFLCARI = "S"
          this.w_CRKEYSAL = this.w_PUNPAD.oParentObject.w_MMKEYSAL
          this.w_CRUNIMIS = this.w_PUNPAD.oParentObject.w_UM1
          * --- Try
          local bErr_042A2EB0
          bErr_042A2EB0=bTrsErr
          this.Try_042A2EB0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            if Empty( i_ErrMsg )
              ah_ErrorMsg("Errore aggiornamento movimento lotti/ubicazioni riga di scarico: %1", 48, "", message())
            else
              ah_ErrorMsg("Errore aggiornamento movimento lotti/ubicazioni riga di scarico: %1", 48, "",i_ErrMsg)
            endif
          endif
          bTrsErr=bTrsErr or bErr_042A2EB0
          * --- End
          * --- Mov.lotti di carico
          this.w_CRROWNUM = this.w_PUNPAD.oParentObject.w_CPCAR
          this.w_CRNUMRIF = this.w_PUNPAD.oParentObject.w_MMNUMRIF
          this.w_CRMAGCAR = this.w_PUNPAD.oParentObject.w_MAGCAR
          this.w_CRFLCARI = "C"
          this.w_CRUNIMIS = this.w_PUNPAD.oParentObject.w_UM1
          this.w_CRKEYSAL = this.w_PUNPAD.oParentObject.w_MMKEYSAL
          * --- Try
          local bErr_04126050
          bErr_04126050=bTrsErr
          this.Try_04126050()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            if Empty( i_ErrMsg )
              ah_ErrorMsg("Errore aggiornamento movimento lotti/ubicazioni di carico: %1", 48, "", message())
            else
              ah_ErrorMsg("Errore aggiornamento dmovimento lotti/ubicazioni di carico: %1", 48, "",i_ErrMsg)
            endif
          endif
          bTrsErr=bTrsErr or bErr_04126050
          * --- End
          * --- Chiudo la maschera
          this.oParentObject.w_USCITA = .T.
          this.w_PUNPAD.ecpquit()     
        else
          this.w_NONSEL = .T.
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOPER="X"
        * --- All'uscita cancella il movimento di magazzino e storna i saldi
        if ! this.oParentObject.w_USCITA
          this.w_CRSERIAL = this.w_PUNPAD.oParentObject.w_MMSERIAL
          this.w_CRKEYSAL = this.w_PUNPAD.oParentObject.w_MMKEYSAL
          * --- Elimino movimento di magazzino
          * --- Delete from MVM_DETT
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.w_CRSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  MMSERIAL = this.w_CRSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from MVM_MAST
          i_nConn=i_TableProp[this.MVM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.w_CRSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  MMSERIAL = this.w_CRSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Storno i saldi
          this.w_CRMAGCAR = this.w_PUNPAD.oParentObject.w_MAGSCA
          if UPPER(this.w_PUNPAD.oParentObject.oParentObject.class)<>"TGSVE_BGE"
            this.w_CODCOM = this.w_PUNPAD.oParentObject.oParentObject.w_CODCOM
            this.w_CAUMAG = this.w_PUNPAD.oParentObject.oParentObject.w_CAUSCA
          else
            this.w_CODCOM = this.w_PUNPAD.oParentObject.w_CODCOM
            this.w_CAUMAG = this.w_PUNPAD.oParentObject.w_CAUSCA
          endif
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Inverto i flags per lo storno
          this.w_FLCASC = iif(this.w_FLCASC="+","-",iif(this.w_FLCASC="-","+"," "))
          this.w_FLRISE = iif(this.w_FLRISE="+","-",iif(this.w_FLRISE="-","+"," "))
          this.w_FLORDI = iif(this.w_FLORDI="+","-",iif(this.w_FLORDI="-","+"," "))
          this.w_FLIMPE = iif(this.w_FLIMPE="+","-",iif(this.w_FLIMPE="-","+"," "))
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SLQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_CRKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CRMAGCAR);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_CRKEYSAL;
                and SLCODMAG = this.w_CRMAGCAR;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if empty(nvl(this.w_CODCOM,""))
            this.w_COMMAPPO = this.w_PUNPAD.oParentObject.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_CODCOM
          endif
          if NVL(this.w_PUNPAD.oParentObject.w_ARFLCOMM,"N")="S"
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SCQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_CRKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CRMAGCAR);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_CRKEYSAL;
                  and SCCODMAG = this.w_CRMAGCAR;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_CRMAGCAR = this.w_PUNPAD.oParentObject.w_MAGCAR
          if UPPER(this.w_PUNPAD.oParentObject.oParentObject.class)<>"TGSVE_BGE"
            this.w_CODCOM = this.w_PUNPAD.oParentObject.oParentObject.w_NEWCOM
            this.w_CAUMAG = this.w_PUNPAD.oParentObject.oParentObject.w_CAUTRA
          else
            this.w_CODCOM = this.w_PUNPAD.oParentObject.w_NEWCOM
            this.w_CAUMAG = this.w_PUNPAD.oParentObject.w_CAUTRA
          endif
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Inverto i flags per lo storno
          this.w_FLCASC = iif(this.w_FLCASC="+","-",iif(this.w_FLCASC="-","+"," "))
          this.w_FLRISE = iif(this.w_FLRISE="+","-",iif(this.w_FLRISE="-","+"," "))
          this.w_FLORDI = iif(this.w_FLORDI="+","-",iif(this.w_FLORDI="-","+"," "))
          this.w_FLIMPE = iif(this.w_FLIMPE="+","-",iif(this.w_FLIMPE="-","+"," "))
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SLQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_CRKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CRMAGCAR);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_CRKEYSAL;
                and SLCODMAG = this.w_CRMAGCAR;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if empty(nvl(this.w_CODCOM,""))
            this.w_COMMAPPO = this.w_PUNPAD.oParentObject.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_CODCOM
          endif
          if NVL(this.w_PUNPAD.oParentObject.w_ARFLCOMM,"N")="S"
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SCQTAPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.oParentObject.w_MMQTAUM1',this.oParentObject.w_MMQTAUM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_CRKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CRMAGCAR);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_CRKEYSAL;
                  and SCCODMAG = this.w_CRMAGCAR;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_PUNPAD.oParentObject.oParentObject.w_OK = .F.
        endif
      case this.pOPER="Q"
        this.oParentObject.w_CALCOLA = .t.
        * --- Eliminata l'esecuzione della mCalc dalla Maschera perch� potrebbe richiamare questo batch entrando in loop
        this.bUpdateParentObject = .F.
      case this.pOPER="V"
        * --- Controllo se l'utente ha modificato la quantit� su una riga gi� selezionata
        Select (NS)
        this.w_RECPOS = RECNO()
        if XCHK=1
          * --- Memorizzo quantit� e chiave lotto/ubicazione
          this.w_TMPQTA = nvl(SUQTACAR,0)
          this.w_TMPCODLOT = NVL(SUCODLOT,Space(20))
          this.w_TMPCODUBI = NVL(SUCODUBI,Space(20))
          * --- Prima di fare qualsiasi considerazione relativa al secondo zoom verifico che la quantit� inserita sia corretta
          SELECT (NC)
          if not empty(this.w_TMPCODLOT) or not empty(this.w_TMPCODUBI)
            LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
            if QTASEL<>this.w_TMPQTA
              this.w_TMPQTA2 = nvl(QTASEL,0)
              Select (NS)
              this.oParentObject.w_CONTA = this.oParentObject.w_CONTA+nvl(SUQTACAR,0)-this.w_TMPQTA2
              this.w_TOTDIS = nvl(SUQTACAR,0)>nvl(SUDISPON,0)
              this.w_CHKQTA = !(nvl(SUQTACAR,0)>0)
              this.w_TOTSEL = nvl(SUQTACAR,0)>(this.oParentObject.w_NPROG+this.w_TMPQTA2)
              do case
                case this.w_CHKQTA
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                  this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                  GOTO this.w_RECPOS
                  this.w_PUNPAD.w_ZoomSel.grd.refresh
                  this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
                  * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                  SELECT (NC)
                  LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                  DELETE
                  GO TOP
                  * --- Refresh della griglia
                  this.w_PUNPAD.w_ZoomCar.grd.refresh
                  this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                  i_retcode = 'stop'
                  return
                case this.w_TOTDIS
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                  this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                  GOTO this.w_RECPOS
                  this.w_PUNPAD.w_ZoomSel.grd.refresh
                  this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
                  * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                  SELECT (NC)
                  LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                  DELETE
                  GO TOP
                  * --- Refresh della griglia
                  this.w_PUNPAD.w_ZoomCar.grd.refresh
                  this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                  i_retcode = 'stop'
                  return
                case this.w_TOTSEL
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
                  this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
                  GOTO this.w_RECPOS
                  this.w_PUNPAD.w_ZoomSel.grd.refresh
                  this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-nvl(SUQTACAR,0)
                  * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
                  SELECT (NC)
                  LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                  DELETE
                  GO TOP
                  * --- Refresh della griglia
                  this.w_PUNPAD.w_ZoomCar.grd.refresh
                  this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                  i_retcode = 'stop'
                  return
                otherwise
                  SELECT (NC)
                  LOCATE FOR (CODUBI=this.w_TMPCODUBI or empty(this.w_TMPCODUBI)) and (CODLOT=this.w_TMPCODLOT or empty(this.w_TMPCODLOT))
                  if QTASEL<>this.w_TMPQTA
                    * --- Controllo conguit� quantit� inserita, se non � congrua elimino la riga ed avviso altrimenti la aggiorno
                    REPLACE QTASEL with this.w_TMPQTA
                  endif
                  this.oParentObject.w_NPROG = this.oParentObject.w_MMQTAUM1 - this.oParentObject.w_CONTA
                  GO TOP
                  this.w_PUNPAD.w_ZoomCar.grd.refresh
              endcase
            endif
          endif
        endif
    endcase
    if USED("Carica")
      SELECT Carica
      USE
    endif
    if USED("Scarica")
      SELECT Scarica
      USE
    endif
  endproc
  proc Try_042A2EB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.RigaArch = 0
    if reccount("Scarica")=1
      * --- Se ho una sola riga nel cursore allora anche nel movimento ne avr� solo una, non � necessario splittare
      Select Scarica 
 go top
      this.w_CRCODLOT = SUCODLOT
      this.w_CRCODUBI = SUCODUBI
      this.w_CRQTASEL = SUQTACAR
      * --- Write into MVM_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODLOT),'MVM_DETT','MMCODLOT');
        +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODUBI),'MVM_DETT','MMCODUBI');
        +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MVM_DETT','MMLOTMAG');
            +i_ccchkf ;
        +" where ";
            +"MMSERIAL = "+cp_ToStrODBC(this.w_CRSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CRROWNUM);
               )
      else
        update (i_cTable) set;
            MMCODLOT = this.w_CRCODLOT;
            ,MMCODUBI = this.w_CRCODUBI;
            ,MMLOTMAG = this.w_CRMAGCAR;
            &i_ccchkf. ;
         where;
            MMSERIAL = this.w_CRSERIAL;
            and CPROWNUM = this.w_CRROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      Select Scarica 
 go top
      do while not eof ("Scarica")
        Select Scarica
        this.w_CRCODLOT = SUCODLOT
        this.w_CRCODUBI = SUCODUBI
        this.w_CRQTASEL = SUQTACAR
        * --- Splitto lotti/ubicazioni
        this.RigaArch = this.RigaArch+1
        if this.RigaArch=1
          * --- Prima riga, update
          * --- Write into MVM_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODLOT),'MVM_DETT','MMCODLOT');
            +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODUBI),'MVM_DETT','MMCODUBI');
            +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MVM_DETT','MMLOTMAG');
            +",MMQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_CRQTASEL),'MVM_DETT','MMQTAMOV');
            +",MMQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_CRQTASEL),'MVM_DETT','MMQTAUM1');
                +i_ccchkf ;
            +" where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.w_CRSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CRROWNUM);
                   )
          else
            update (i_cTable) set;
                MMCODLOT = this.w_CRCODLOT;
                ,MMCODUBI = this.w_CRCODUBI;
                ,MMLOTMAG = this.w_CRMAGCAR;
                ,MMQTAMOV = this.w_CRQTASEL;
                ,MMQTAUM1 = this.w_CRQTASEL;
                &i_ccchkf. ;
             where;
                MMSERIAL = this.w_CRSERIAL;
                and CPROWNUM = this.w_CRROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Righe successive, insert
          this.w_rigamvm = this.w_rigamvm+1
          this.w_MMCODLOT = iif(Empty(this.w_CRCODLOT),NULL,this.w_CRCODLOT)
          this.w_MMCODUBI = iif(Empty(this.w_CRCODUBI),NULL,this.w_CRCODUBI)
          this.w_MMLOTMAG = iif(Empty(nvl(this.w_CRCODLOT,"")) and Empty(nvl(this.w_CRCODUBI,"")),NULL,this.w_CRMAGCAR)
          * --- Insert into MVM_DETT
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMR_BCL",this.MVM_DETT_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        Select Scarica
        if Not Eof("Scarica")
          Skip
        endif
      enddo
    endif
    return
  proc Try_04126050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.RigaArch = 0
    if reccount("Carica")=1
      * --- Se ho una sola riga nel cursore allora anche nel movimento ne avr� solo una, non � necessario splittare
      Select Carica 
 go top
      this.w_CRCODLOT = CODLOT
      this.w_CRCODUBI = CODUBI
      this.w_CRQTASEL = QTASEL 
      * --- Write into MVM_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODLOT),'MVM_DETT','MMCODLOT');
        +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODUBI),'MVM_DETT','MMCODUBI');
        +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MVM_DETT','MMLOTMAG');
            +i_ccchkf ;
        +" where ";
            +"MMSERIAL = "+cp_ToStrODBC(this.w_CRSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CRROWNUM);
               )
      else
        update (i_cTable) set;
            MMCODLOT = this.w_CRCODLOT;
            ,MMCODUBI = this.w_CRCODUBI;
            ,MMLOTMAG = this.w_CRMAGCAR;
            &i_ccchkf. ;
         where;
            MMSERIAL = this.w_CRSERIAL;
            and CPROWNUM = this.w_CRROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      Select Carica 
 go top
      do while not eof ("Carica")
        Select Carica
        this.w_CRCODLOT = CODLOT
        this.w_CRCODUBI = CODUBI
        this.w_CRQTASEL = QTASEL 
        * --- Splitto lotti/ubicazioni
        this.RigaArch = this.RigaArch+1
        if this.RigaArch=1
          * --- Prima riga, update
          * --- Write into MVM_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODLOT),'MVM_DETT','MMCODLOT');
            +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_CRCODUBI),'MVM_DETT','MMCODUBI');
            +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MVM_DETT','MMLOTMAG');
            +",MMQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_CRQTASEL),'MVM_DETT','MMQTAMOV');
            +",MMQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_CRQTASEL),'MVM_DETT','MMQTAUM1');
                +i_ccchkf ;
            +" where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.w_CRSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CRROWNUM);
                   )
          else
            update (i_cTable) set;
                MMCODLOT = this.w_CRCODLOT;
                ,MMCODUBI = this.w_CRCODUBI;
                ,MMLOTMAG = this.w_CRMAGCAR;
                ,MMQTAMOV = this.w_CRQTASEL;
                ,MMQTAUM1 = this.w_CRQTASEL;
                &i_ccchkf. ;
             where;
                MMSERIAL = this.w_CRSERIAL;
                and CPROWNUM = this.w_CRROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Righe successive, insert
          this.w_rigamvm = this.w_rigamvm+1
          this.w_MMCODLOT = iif(Empty(this.w_CRCODLOT),NULL,this.w_CRCODLOT)
          this.w_MMCODUBI = iif(Empty(this.w_CRCODUBI),NULL,this.w_CRCODUBI)
          this.w_MMLOTMAG = iif(Empty(nvl(this.w_CRCODLOT,"")) and Empty(nvl(this.w_CRCODUBI,"")),NULL,this.w_CRMAGCAR)
          * --- Insert into MVM_DETT
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMR_BCL",this.MVM_DETT_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        Select Carica
        if Not Eof("Carica")
          Skip
        endif
      enddo
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggi di errore in caso di selezione errata
    this.w_oMESS=createobject("ah_message")
    if this.w_TOTSEL
      this.w_oMESS.AddMsgPartNL("La quantit� selezionata � maggiore di quella richiesta")     
    endif
    if this.w_CHKCODUBI
      this.w_oMESS.AddMsgPartNL("Codice ubicazione non impostato")     
    endif
    if this.w_CHKQTA
      this.w_oMESS.AddMsgPartNL("La quantit� selezionata deve essere maggiore di 0")     
    endif
    if this.w_TOTDIS
      this.w_oMESS.AddMsgPartNL("La quantit� selezionata non deve essere maggiore della disponibilit�")     
    endif
    if this.w_NONSEL
      this.w_oMESS.AddMsgPartNL("La quantit� selezionata � inferiore a quella richiesta")     
    endif
    if this.w_SPOSTA
      this.w_oMESS.AddMsgPartNL("La quantit� selezionata � incongrua")     
    endif
    this.w_oMESS.AddMsgPart("Impossibile confermare la selezione")     
    this.w_oMESS.ah_ErrorMsg(48)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='MOVIMATR'
    this.cWorkTables[2]='MVM_DETT'
    this.cWorkTables[3]='MVM_MAST'
    this.cWorkTables[4]='CAM_AGAZ'
    this.cWorkTables[5]='SALDIART'
    this.cWorkTables[6]='SALDICOM'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='DOC_DETT'
    this.cWorkTables[12]='DIC_MATR'
    this.cWorkTables[13]='DIC_MCOM'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
