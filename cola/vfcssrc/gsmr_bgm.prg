* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bgm                                                        *
*              Gestione archivio generazione MRP                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-23                                                      *
* Last revis.: 2014-08-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bgm",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_bgm as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_PATHEL = space(200)
  w_SERIAL = space(10)
  w_DBFDACANC = space(200)
  w_DBFBISDACANC = space(200)
  w_LNumErr = 0
  w_LSerial = space(10)
  w_LSelOpe = space(1)
  w_LEERR = space(250)
  w_LDettTec = space(1)
  w_LDatini = ctod("  /  /  ")
  w_LDatfin = ctod("  /  /  ")
  TmpC = space(100)
  w_LOggErr = space(41)
  w_LOperaz = space(20)
  w_LErrore = space(80)
  w_LOra = space(8)
  w_LMessage = space(0)
  * --- WorkFile variables
  SEL__MRP_idx=0
  PRD_ERRO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di eliminazione record Archivio Generazione MRP e relativo file DBF collegato (da GSMR_KGP)
    * --- Variabili per la gestione del Log Errori
    * --- Riferimento al padre
    this.Padre = this.oParentObject
    * --- Riferimento al cursore dello zoom del padre
    this.NC = this.Padre.w_ZoomSel.cCursor
    * --- Variabili per la gestione del Log Errori
    this.w_LOperaz = "CS"
    this.w_LNumErr = 0
    this.w_LSerial = space(10)
    this.w_LSelOpe = "T"
    this.w_LDettTec = this.Padre.w_LDettTec
    this.w_LDatini = i_DATSYS
    this.w_LDatfin = i_DATSYS
    do case
      case this.pAzione = "AG"
        * --- Controlla selezioni
        select count(*) as Numero from (this.NC) where xChk=1 into cursor __temp__
        go top
        this.w_nRecSel = NVL(__temp__.Numero,0)
        use in __Temp__
        if this.w_nRecSel>0
          * --- Legge cursore di selezione ...
          select * from (this.NC) where xChk=1 into cursor _AGG_
          select _AGG_ 
 scan
          * --- Fase 1 - Eliminazione file DBF associati
          *     --------------------------------------------------------------
          this.w_PATHEL = _AGG_.GMPATHEL
          this.w_SERIAL = _AGG_.GMSERIAL
          * --- Path+Nome file DBF da eliminare
          this.w_DBFDACANC = (alltrim(this.w_PATHEL)+"elab"+alltrim(i_CODAZI)+alltrim(this.w_SERIAL)+".dbf")
          * --- Variabili per la gestione dell'errore
          l_ErrorDel=.f. 
 l_OldOnError=ON("Error")
          if file(this.w_DBFDACANC)
             ah_msg("Eliminazione file %1 in corso",.t.,.f.,.f.,this.w_DBFdacanc) 
 ON Error l_ErrorDel=.t. 
 delete file (this.w_DBFdacanc) 
 * Ripristina ON ERROR
            if not(empty(l_OldOnError))
               ON Error &l_OldOnError
            else
              ON Error
            endif
            if l_ErrorDel
              this.w_LOggErr = this.w_SERIAL
              * --- Il file potrebbe essere in uso...
              this.w_LErrore = ah_msgformat("Impossibile eliminare il file elab%1%2.dbf",alltrim(i_CODAZI),alltrim(this.w_SERIAL))
              this.w_LMessage = ah_msgformat("Impossibile accedere al file. Verificare che non sia in uso.Il seriale non � stato eliminato")
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_DBFBISDACANC = (alltrim(this.w_PATHEL)+"elab"+alltrim(i_CODAZI)+alltrim(this.w_SERIAL)+"_bis.dbf")
            if file(this.w_DBFBISDACANC)
               ah_msg("Eliminazione file %1 in corso",.t.,.f.,.f.,this.w_DBFbisdacanc) 
 ON Error l_ErrorDel=.t. 
 delete file (this.w_DBFbisdacanc) 
 * Ripristina ON ERROR
              if l_ErrorDel
                this.w_LOggErr = this.w_SERIAL
                * --- Il file potrebbe essere in uso...
                this.w_LErrore = ah_msgformat("Impossibile eliminare il file elab%1%2_bis.dbf",alltrim(i_CODAZI),alltrim(this.w_SERIAL))
                this.w_LMessage = ah_msgformat("Impossibile accedere al file. Verificare che non sia in uso.Il seriale non � stato eliminato")
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          else
            * --- Il file DBF non esiste, elimino comunque il seriale di elaborazione
            this.w_LOggErr = this.w_SERIAL
            this.w_LErrore = ah_msgformat("Il file elab%1%2.dbf non esiste",alltrim(i_CODAZI), alltrim(this.w_SERIAL))
            this.w_LMessage = ah_msgformat("Impossibile trovare il file nel percorso specificato. Il file potrebbe essere stato spostato od eliminato in precedenza. Il seriale � stato comunque eliminato")
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Fase 2 - Eliminazione records da SEL__MRP
          *     ----------------------------------------------------------------------
          if ! l_ErrorDel
            * --- Try
            local bErr_036BCD50
            bErr_036BCD50=bTrsErr
            this.Try_036BCD50()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              this.w_LOggErr = this.w_SERIAL
              this.w_LErrore = ah_msgformat("Eliminazione seriale %1 non riuscita",alltrim(this.w_SERIAL))
              this.w_LMessage = "Message()= "+message()
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            bTrsErr=bTrsErr or bErr_036BCD50
            * --- End
          endif
          endscan
          * --- Riesegue Interrogazione
          if this.w_LNumErr=0
            ah_ErrorMsg("Aggiornamento completato",64)
          else
            this.TmpC = "Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?"
            if ah_YesNo(this.TmpC,"",alltrim(str(this.w_LNumErr,5,0)))
              * --- Lancia Stampa
              do GSDB_BLE with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          use in _AGG_
          this.Padre.NotifyEvent("Interroga")     
          this.oParentObject.w_SELEZI = "D"
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare",48)
        endif
      case this.pAzione = "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
    endcase
  endproc
  proc Try_036BCD50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     ah_msg("Eliminazione seriale %1 in corso",.t.,.f.,.f.,this.w_SERIAL)
    * --- Delete from SEL__MRP
    i_nConn=i_TableProp[this.SEL__MRP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEL__MRP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GMSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      delete from (i_cTable) where;
            GMSERIAL = this.w_SERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    this.w_LEERR = " "
    if empty(this.w_LSerial)
      * --- Determina progressivo log
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_0385DF08
      bErr_0385DF08=bTrsErr
      this.Try_0385DF08()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Impossibile aggiornare log-errori",16)
      endif
      bTrsErr=bTrsErr or bErr_0385DF08
      * --- End
    endif
    * --- Scrive LOG
    if not empty(this.w_LSerial)
      * --- Scrive LOG
      this.w_LOra = time()
      * --- Try
      local bErr_038C9400
      bErr_038C9400=bTrsErr
      this.Try_038C9400()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_038C9400
      * --- End
    endif
  endproc
  proc Try_0385DF08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_LSerial = space(10)
    this.w_LSerial = cp_GetProg("PRD_ERRO","PRERR",this.w_LSerial,i_CODAZI)
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_038C9400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRD_ERRO
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRD_ERRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LESERIAL"+",LEROWNUM"+",LEOPERAZ"+",LEDATOPE"+",LEORAOPE"+",LECODUTE"+",LECODICE"+",LEERRORE"+",LEMESSAG"+",LESTORIC"+",LESTAMPA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSerial),'PRD_ERRO','LESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LNumErr),'PRD_ERRO','LEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOperaz),'PRD_ERRO','LEOPERAZ');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PRD_ERRO','LEDATOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOra),'PRD_ERRO','LEORAOPE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PRD_ERRO','LECODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOggErr),'PRD_ERRO','LECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LErrore),'PRD_ERRO','LEERRORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMessage),'PRD_ERRO','LEMESSAG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PRD_ERRO','LESTORIC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTAMPA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LESERIAL',this.w_LSerial,'LEROWNUM',this.w_LNumErr,'LEOPERAZ',this.w_LOperaz,'LEDATOPE',i_DATSYS,'LEORAOPE',this.w_LOra,'LECODUTE',i_CODUTE,'LECODICE',this.w_LOggErr,'LEERRORE',this.w_LErrore,'LEMESSAG',this.w_LMessage,'LESTORIC',"S",'LESTAMPA',"N")
      insert into (i_cTable) (LESERIAL,LEROWNUM,LEOPERAZ,LEDATOPE,LEORAOPE,LECODUTE,LECODICE,LEERRORE,LEMESSAG,LESTORIC,LESTAMPA &i_ccchkf. );
         values (;
           this.w_LSerial;
           ,this.w_LNumErr;
           ,this.w_LOperaz;
           ,i_DATSYS;
           ,this.w_LOra;
           ,i_CODUTE;
           ,this.w_LOggErr;
           ,this.w_LErrore;
           ,this.w_LMessage;
           ,"S";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SEL__MRP'
    this.cWorkTables[2]='PRD_ERRO'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
