* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bvc                                                        *
*              Controlli su attributi SOS in classe documentale                *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-30                                                      *
* Last revis.: 2010-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bvc",oParentObject,m.pOPER)
return(i_retval)

define class tgsdm_bvc as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_CallerObj = .NULL.
  w_TRANSCUR = space(10)
  w_NUMATT = 0
  w_NUMPRA = 0
  w_MESS = space(254)
  w_OK = .f.
  * --- WorkFile variables
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per il controllo sulla completezza ed univocit� degli attributi SOStitutiva inseriti nella classe documentale
    if this.pOPER <> "K"
      this.w_CallerObj = this.oParentObject
      * --- Verifica univocit� attributi inseriti
      this.w_CallerObj.Markpos()     
      this.w_TRANSCUR = this.w_CallerObj.cTrsname
      do case
        case this.pOPER = "C"
          if ! empty(this.oParentObject.w_CDCLASOS)
            this.w_CallerObj.Exec_Select("RESULT", "t_CDATTSOS,count(*) as contatore","t_CDFLGSOS=2","","t_CDATTSOS","contatore > 1")     
            if RESULT.contatore > 0
              this.oParentObject.w_RESCHK = 1
            endif
            use in select("RESULT")
            * --- Verifico completezza attributi  inseriti
            vq_exec("QUERY\ATTSOS",this,"LISTAATT")
            Select COUNT(*) as CONTATORE, A.SZCODKEY as ATTSOS from LISTAATT A left join (this.w_TRANSCUR) B on A.SZCODKEY=B.t_CDATTSOS WHERE t_cdattsos is null into cursor "TMP_COUNTATT"
            if TMP_COUNTATT.CONTATORE > 0
              this.oParentObject.w_RESCHK = this.oParentObject.w_RESCHK + 2
              this.oParentObject.w_COKEYSOS = TMP_COUNTATT.ATTSOS
            endif
            use in select("TMP_COUNTATT")
            use in select("LISTAATT")
          endif
          * --- Verifico che sia stato attivato al max un solo check per il riferimento temporale della firma digitale
          this.w_CallerObj.Exec_Select("RESULT", "count(*) as contatore","t_CDRIFTEM=1")     
          if RESULT.contatore > 1
            this.oParentObject.w_RESCHK = this.oParentObject.w_RESCHK + 3
          endif
          use in select("RESULT")
        case this.pOPER = "I"
          this.w_CallerObj.FirstRow()     
          do while not this.w_CallerObj.eof_trs()
            this.w_CallerObj.Set("w_CDATTSOS"," ")     
            this.w_CallerObj.Set("w_CDFLGSOS",1)     
            this.w_CallerObj.NextRow()     
          enddo
        case this.pOPER = "A"
          * --- Controllo su numeri check 'attributo primario' attivi
          this.w_CallerObj.Exec_Select("RESULT", "count(*) as contatore","t_CDATTPRI=1","","","")     
          do case
            case RESULT.contatore > 2
              this.oParentObject.w_RESCHK = 1
            case RESULT.contatore <= 1
              this.oParentObject.w_RESCHK = 2
          endcase
          use in select("RESULT")
          * --- Verifico che in corrispondenza dell'attributo primario sia sta indicata una tabella fisica su cui operare 
          this.w_CallerObj.Exec_Select("RESULT", "count(*) as contatore","t_CDATTPRI=1 and empty(nvl(t_CDTABKEY,' '))","","","")     
          if RESULT.contatore > 0
            this.oParentObject.w_RESCHK = 4
          endif
          use in select("RESULT")
          if this.oParentObject.w_CDCLAPRA = "S"
            if this.oParentObject.w_RESCHK=0
              * --- Controllo che il check 'attributo primario' sia attivo in corrispondenza del campo CNCODCAN
              *     Il primo campo � stato inserito per ovviare all'errore sulla clausola order by, poich� il campo CDCAMCUR � memo
              this.w_CallerObj.Exec_Select("RESULT", "t_CDTABKEY as tabella,t_CDCAMCUR as nomecampo","t_CDATTPRI=1","","","")     
              if used("RESULT")
                 
 Select Result 
 Go top 
 scan
                if upper(RESULT.nomecampo) = "CNCODCAN"
                  this.oParentObject.w_RESCHK = 0
                  exit
                else
                  this.oParentObject.w_RESCHK = 3
                endif
                Endscan
              endif
              use in select("RESULT")
            endif
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
      this.w_CallerObj.Repos()     
    else
      * --- Controlli flag predefinita
      if this.oParentObject.w_CDCLAPRA = "S"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_OK = this.oParentObject.w_RESCHK=0
        if this.w_OK
          this.w_OK = ChkAttObb(this.oParentObject.w_CDCODCLA,.t.)
          this.w_MESS = Ah_Msgformat("Transazione abbandonata")
        else
          this.w_MESS = Ah_Msgformat("Sono presenti altre classi con attivo il check classe predefinita, selezione non ammessa")
        endif
        if ! this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico che non vi siano altre classi con il flag 'classe archivio pratiche' attivo
    this.w_NUMPRA = 0
    * --- Select from PROMCLAS
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) as NUMCLAS  from "+i_cTable+" PROMCLAS ";
          +" where CDCLAPRA='S' and   CDCODCLA<>"+cp_ToStrODBC(this.oParentObject.w_CDCODCLA)+"";
           ,"_Curs_PROMCLAS")
    else
      select Count(*) as NUMCLAS from (i_cTable);
       where CDCLAPRA="S" and   CDCODCLA<>this.oParentObject.w_CDCODCLA;
        into cursor _Curs_PROMCLAS
    endif
    if used('_Curs_PROMCLAS')
      select _Curs_PROMCLAS
      locate for 1=1
      do while not(eof())
      this.w_NUMPRA = _Curs_PROMCLAS.NUMCLAS
        select _Curs_PROMCLAS
        continue
      enddo
      use
    endif
    if this.w_NUMPRA > 0
      this.oParentObject.w_RESCHK = this.oParentObject.w_RESCHK + 5
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PROMCLAS')
      use in _Curs_PROMCLAS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
