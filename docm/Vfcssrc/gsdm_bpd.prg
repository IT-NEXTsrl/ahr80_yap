* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bpd                                                        *
*              Gestione processo documentale                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-09                                                      *
* Last revis.: 2017-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOOPE,pKEYISTANZA,pDMReportSplit
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bpd",oParentObject,m.pTIPOOPE,m.pKEYISTANZA,m.pDMReportSplit)
return(i_retval)

define class tgsdm_bpd as StdBatch
  * --- Local variables
  pTIPOOPE = space(1)
  pKEYISTANZA = space(10)
  pDMReportSplit = .NULL.
  w_AWEB = .NULL.
  w_NUMMAXALL = 0
  w_TOTALLEGA = 0
  w_NUMALLEGA = 0
  w_DAELEM = 0
  w_SAELEM = 0
  w_KAELEM = 0
  w_LPCHKPTL = 0
  w_LICLADOC = space(15)
  w_oESEPTL = space(1)
  w_FLGSTAMPA = space(1)
  w_IDESEPTL = space(1)
  w_REPORT = space(100)
  w_POSTELSN = space(1)
  w_CDVLPRED = space(100)
  w_CDCAMRAG = space(100)
  w_VALDATRAG = ctod("  /  /  ")
  w_LIPATIMG = space(254)
  w_nIMGPAT = space(100)
  w_PERIODO = ctod("  /  /  ")
  w_CRITPERI = space(1)
  w_ImgPat = space(10)
  w_TmpPat = space(100)
  w_TMPD = ctod("  /  /  ")
  w_INDPREDVAL = space(100)
  w_CDCAMCUR = space(100)
  w_OKALLEGA = .f.
  w_NALLEGA = space(100)
  w_CONTADOC = 0
  w_CONTAPTL = 0
  w_TMPT = space(1)
  w_TMPC = space(100)
  w_TMPN = 0
  w_LEGENDA = space(100)
  w_MASKLOG = .NULL.
  w_MASKLEG = .NULL.
  w_PDSYSTEM = .NULL.
  w_OPBAR = .NULL.
  w_OPADRE = .NULL.
  w_OPLABEL = .NULL.
  w_NRECTOT = 0
  w_NREC = 0
  w_VALKEYROTT = space(100)
  w_VALCODCON = space(15)
  w_VALOGGETTO = space(100)
  w_VALTESTO = space(100)
  w_PDKEYPRO = space(100)
  w_PDCODCON = space(100)
  w_PDCODORN = space(100)
  w_PDOGGETT = space(100)
  w_PDCLADOC = space(10)
  w_PD_TESTO = space(100)
  w_COUNTER = 0
  w_COUNTERXCO = 0
  w_IDVALATT = space(100)
  w_LPCHKMAI = 0
  w_LPCHKPEC = 0
  w_CHKMAIAGG = 0
  w_CHKPECAGG = 0
  w_CHKMAIXCO = 0
  w_CHKPECXCO = 0
  w_LPCHKFAX = 0
  w_CHKFAXAGG = 0
  w_CHKFAXXCO = 0
  w_LPCHKWWP = 0
  w_LPCHKSTA = 0
  w_LPCHKARC = 0
  w_LPCHKFIR = 0
  w_LPANEMAIL = space(254)
  w_LPANEMPEC = space(254)
  w_EMAILAGG = space(254)
  w_EMPECAGG = space(254)
  w_EMAILXCO = space(254)
  w_EMPECXCO = space(254)
  w_MAILSN = space(1)
  w_PECSN = space(1)
  w_FAXSN = space(1)
  w_WESN = space(1)
  w_REPSN = space(1)
  w_ARCHSN = space(1)
  w_DESCRITTORE = space(50)
  w_NOMEFILE = space(10)
  w_CALLEGA = 0
  w_TIPOPUBBL = space(10)
  w_OPERAZIONE = space(10)
  w_RET = .f.
  w_INDVALATTR = space(100)
  w_LPDOMIWE = space(10)
  w_LPWELOGI = space(30)
  w_LPWEMAIL = space(1)
  w_LPWETXMA = space(100)
  w_LPWE_SMS = space(1)
  w_LPWETSMS = space(100)
  w_LPWE_FAX = space(1)
  w_LPNOMDSC = space(35)
  w_LPINDSWE = space(75)
  w_LPFROFAX = space(1)
  w_LPTELFAX = space(20)
  w_TELFAXAGG = space(20)
  w_TELFAXXCO = space(20)
  w_LPRAGSOC = space(40)
  w_RAGSOCXCO = space(40)
  w_RAGSOCAGG = space(40)
  w_IDSERIAL = space(15)
  w_IDVALDAT = ctod("  /  /  ")
  w_IDVALNUM = 0
  w_IDESEMAI = space(1)
  w_IDESEPEC = space(1)
  w_IDESEFAX = space(1)
  w_IDESEWWP = space(1)
  w_IDESESTA = space(1)
  w_IDESEARC = space(1)
  w_IDESECPZ = space(1)
  w_OLDONERROR = space(100)
  w_oESEMAI = space(1)
  w_oESEPEC = space(1)
  w_oESEFAX = space(1)
  w_oESEWWP = space(1)
  w_oESESTA = space(1)
  w_oESEARC = space(1)
  w_PDREPORT = space(75)
  w_PDDESREP = space(50)
  w_CODAZI = space(5)
  w_CODUTE = 0
  w_DBSERVER = space(20)
  w_DBNAME = space(30)
  w_USRNAME = space(30)
  w_USRPWD = space(30)
  w_TMPL = .f.
  w_CPZSN = space(1)
  w_LPCHKCPZ = 0
  w_oESECPZ = space(1)
  w_COPATHDO = space(100)
  w_DVPATHFI = space(100)
  w_DVDESELA = space(50)
  w_DV__NOTE = space(0)
  w_DVDATCRE = ctod("  /  /  ")
  w_DV_PARAM = space(0)
  w_DVCODELA = 0
  w_DESTFILE = space(100)
  w_TFOLDER = space(1)
  w_PFOLDER = space(254)
  w_PFOLDER_A = space(254)
  w_CFOLDER = 0
  w_RISERVATO = space(1)
  w_DEKEYINF = space(100)
  w_TIPDES1 = space(1)
  w_TIPDES1A = space(1)
  w_CODAGE1 = space(5)
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_DENOMFIL = space(200)
  w_FLOK = space(1)
  w_FLOKA = space(1)
  w_TMPDES1 = space(1)
  w_TIPO = space(1)
  w_CODGRU = 0
  w_DEFILKEY = space(50)
  w_DEFILTIP = space(5)
  w_CHIAVEDMS = space(50)
  w_CODCON = space(15)
  w_PNAME = space(254)
  w_oMESS = .NULL.
  w_PDISOLAN = space(50)
  w_VALLANGUAGE = space(3)
  w_REPSESCODE = space(10)
  w_CREC = 0
  w_CLAN = 0
  w_PREC = 0
  w_INDREP = 0
  w_REPORTLOC = space(10)
  w_OLDSUBJECT = space(254)
  w_MultiRepFas = .NULL.
  w_PFOLDER_A = space(254)
  w_QUERY = space(1)
  w_PROCOD = space(17)
  w_NomRep = space(100)
  w_PATDOCFIR = space(220)
  w_FIRMRICH = space(1)
  w_DATAFIRM = ctot("")
  w_INARCHIVIO = space(1)
  w_CODPROC = space(10)
  w_STRCONTROL = space(7)
  w_IRDR_MR = .NULL.
  w_SCHEDULER = .f.
  w_OFALLENAME = .f.
  w_OFFE_CLASS = space(10)
  w_StmpOrderListStr = space(254)
  w_NumStmpOrder = 0
  L_ii = 0
  w_IDFIRMDIG = space(150)
  w_NOMEFILFIR = space(254)
  w_nCount = 0
  w_StmpOrder = space(254)
  w_nDelta = 0
  w_CounterRep = 0
  w_PDPROCES = space(3)
  w_CATEGORIA = space(5)
  w_NOMEFILEIN = space(250)
  w_CDERASEF = space(1)
  w_CDMODALL = space(1)
  w_COPATHDS = space(200)
  w_MultiRep = .NULL.
  w_NUMDESTAG = 0
  w_DESTAGG = .f.
  w_LCHKMAIAGG = 0
  w_LCHKPECAGG = 0
  w_LCHKFAXAGG = 0
  w_LEMAILAGG = space(254)
  w_LEMPECAGG = space(254)
  w_IDXADD = 0
  w_GOPROC = .f.
  w_APPWEBARC = .f.
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_CODCONAG = space(15)
  w_NOMEFILE_CPZ = space(254)
  w_ESTENSIONE_CPZ = space(10)
  w_EXTGRF = space(6)
  w_FILESEARCH = space(254)
  w_BPDSERIAL = space(20)
  w_TMPN_1 = 0
  w_MultiReportSplit = .NULL.
  w_NomeRep = space(10)
  w_Formato = 0
  w_EXT = space(10)
  w_TMPPOS = 0
  w_MESS = space(10)
  w_PCNAME = space(20)
  w_SFEXTFOR = space(10)
  w_NomeFileWeb = space(200)
  w_Report2 = space(10)
  w_PagAtt = 0
  w_NFileCom = space(254)
  w_FRONTE = space(1)
  w_MITTEN = space(40)
  w_MAIMIT = space(18)
  w_NOMEFRS = space(10)
  w_UTEAZI = space(40)
  w_OGGFAX = space(0)
  w_RIFERI = space(40)
  w_FAXMIT = space(18)
  w_SOGFAX = space(0)
  w_TELMIT = space(18)
  w_FAXA = space(18)
  w_INVIAA = space(40)
  w_CHKFAX = 0
  w_DIALOG = space(1)
  w_FILEXML = space(254)
  w_NHF = 0
  w_TMPN1 = 0
  w_OBJXML = .NULL.
  w_UploadDocumentsXML = space(254)
  w_ErrorDMSInfinity = .f.
  w_ErrorDMSInfinityMsg = space(10)
  w_oObjInfinity = .NULL.
  w_CDCODATT = space(20)
  w_ERRINF = 0
  w_OBJXMLRES = .NULL.
  w_FILETOSTR = space(254)
  w_NUMNODI = 0
  w_oNODE = .NULL.
  w_iChild = 0
  w_nDocumentUploaded = 0
  w_NameError = space(254)
  w_WORK_DIR = space(254)
  w_WORK_FILE = space(254)
  w_EXT = space(5)
  w_TMP_DIR = space(254)
  w_RetMailList = space(0)
  w_POS1 = 0
  w_POS2 = 0
  w_LOOP = 0
  w_IND_MAIL = space(100)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  CPUSERS_idx=0
  OUT_PUTS_idx=0
  PRO_LOGP_idx=0
  PRODCLAS_idx=0
  PRODDOCU_idx=0
  PRODINDI_idx=0
  PROMCLAS_idx=0
  PROMDOCU_idx=0
  PROMINDI_idx=0
  PROSLOGP_idx=0
  UTE_NTI_idx=0
  WECONTI_idx=0
  CONTROPA_idx=0
  PROPPERM_idx=0
  PRO_PCPZ_idx=0
  ZFOLDERS_idx=0
  ZDESTIN_idx=0
  AGENTI_idx=0
  PRO_FCPZ_idx=0
  STMPFILE_idx=0
  MOD_OFFE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Processo Documentale
    * --- Non deve chiamare la mcalc() dell' oggetto chiamante (darebbe errore)
    this.bUpdateParentObject=.f.
    * --- Check tipo operazione
    do case
      case this.pTIPOOPE = "CL"
        * --- Cancella LOG esecuzione processo
        * --- Delete from PRO_LOGP
        i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                 )
        else
          delete from (i_cTable) where;
                LPKEYIST = this.pKEYISTANZA;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pTIPOOPE = "LL"
        * --- Legenda 
        this.w_oMESS=createobject("AH_MESSAGE")
        this.w_oMESS.AddMsgPartNL("Errori formulazione processo:")     
        this.w_oMESS.AddMsgPartNL("1 - [Processo] <Campo chiave> di processo non specificato o non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("2 - [Processo] Campo <Codice intestatario> vuoto o non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("3 - [Processo] Campo <Oggetto> non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("4 - [Processo] Campo <Testo> non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("5 - [Classe Documentale] Campo <Data raggruppamento> non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("6 - [Classe Documentale] Attributo obbligatorio senza valore di riferimento e valore predefinito")     
        this.w_oMESS.AddMsgPartNL("7 - [Processo] Campo <Filtro avanzato> non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("8 - [Processo] Processo con classe documentale con tipo archiviazione manuale")     
        this.w_oMESS.AddMsgPartNL("9 - [Classe Documentale] Campo di riga <Campo/i cursore> non valutabile - verificare espressione")     
        this.w_oMESS.AddMsgPartNL("10 - [Generico] Documento gi� processato (attivit� non rilanciabile)")     
        this.w_oMESS.AddMsgPartNL("11 - [Generico] Verifica del filtro avanzato di processo fallita")     
        this.w_oMESS.AddMsgPartNL("12 - [Generico] Servizi MAPI non disponibili")     
        this.w_oMESS.AddMsgPartNL("13 - [Generico] Errore generico in invio di eMail")     
        this.w_oMESS.AddMsgPartNL("14 - [Generico] Impossibile creare allegato per supporto MAPI")     
        this.w_oMESS.AddMsgPartNL("15 - [Processo] Campo <Lingua> non valutabile - Verificare espressione")     
        this.w_oMESS.AddMsgPartNL("16 - [Processo] Processo con classe documentale con modalit� archiviazione di tipo <collegamento> (deve essere <copia file>)")     
        this.w_oMESS.AddMsgPartNL("17 - [Processo] Campo <Sede> non valutabile - Verificare espressione")     
        this.w_oMESS.AddMsgPartNL("18 - [Processo]  Codice destinatario specificato nel campo 'per conto di' non valutabile",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi We:")     
        this.w_oMESS.AddMsgPartNL("19 - [Servizi WE] Errore generico in creazione file descrittore")     
        this.w_oMESS.AddMsgPartNL("20 - [Servizi WE] Errore generico in invio eMail a Net-Folder")     
        this.w_oMESS.AddMsgPartNL("21 - [Servizi We] Servizi We non attivati per azienda o per utente")     
        this.w_oMESS.AddMsgPartNL("22 - [Servizi We] Account We non definito o non attivato per intestatario")     
        this.w_oMESS.AddMsgPartNL("23 - [Servizi We] Modalit� di invio documenti di tipo Simple-Mail (necessario descrittore)")     
        this.w_oMESS.AddMsgPartNL("24 - [Servizi WE] Nome descrittore o Indirizzo eMail per descrittore non definiti in parametri WE",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi eMail:")     
        this.w_oMESS.AddMsgPartNL("27 - [Servizi eMail] Indirizzo email non presente su anagrafica intestatario")     
        this.w_oMESS.AddMsgPartNL("28 - [Servizi eMail] Indirizzo email non presente su sede intestatario")     
        this.w_oMESS.AddMsgPartNL("29 - [Servizi eMail] Indirizzo email non individuato per il destinatario specificato nel campo per conto di")     
        this.w_oMESS.AddMsgPartNL("30 - [Servizi eMail] Indirizzo email non valutabile per l'intestatario del processo",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi Fax:")     
        this.w_oMESS.AddMsgPartNL("32 - [Servizi Fax] Numero TeleFax non presente su anagrafica intestatario")     
        this.w_oMESS.AddMsgPartNL("33 - [Servizi Fax] Numero TeleFax non presente su sede intestatario")     
        this.w_oMESS.AddMsgPartNL("34 - [Servizi Fax] Servizio Fax non configurato")     
        this.w_oMESS.AddMsgPartNL("35 - [Servizi Fax] Errore in creazione allegato per frontespizio")     
        this.w_oMESS.AddMsgPartNL("36 - [Servizi Fax] Errore in invio FAX (modalit� MAPI)")     
        this.w_oMESS.AddMsgPartNL("37 - [Servizi Fax] Errore in invio FAX (modalit� stampante virtuale)")     
        this.w_oMESS.AddMsgPartNL("38 - [Servizi Fax] Errore in invio FAX (modalit� Rendering Subsystem)",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi archiviazione:")     
        this.w_oMESS.AddMsgPartNL("40 - [Archiviazione] Impossibile copiare il file nella cartella di destinazione")     
        this.w_oMESS.AddMsgPartNL("41 - [Archiviazione] Impossibile creare la cartella di destinazione")     
        if g_DMIP="S"
          this.w_oMESS.AddMsgPartNL("43 - [Archiviazione] Errore di archiviazione D.M.S. Infinity")     
        endif
        this.w_oMESS.AddMsgPartNL("45 - [Archiviazione ArchiWEB] Nome DBServer non indicato in tabella parametri")     
        this.w_oMESS.AddMsgPartNL("46 - [Archiviazione ArchiWEB] Nome DataBase non indicato in tabella parametri")     
        this.w_oMESS.AddMsgPartNL("47 - [Archiviazione ArchiWEB] Utente ArchiWEB non indicato in tabella parametri")     
        this.w_oMESS.AddMsgPartNL("48 - [Archiviazione ArchiWEB] Errore generico archiviazione silente",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi stampa:")     
        this.w_oMESS.AddMsgPartNL("52 - [Servizi Stampa] Stampa non eseguita",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi PEC:")     
        this.w_oMESS.AddMsgPartNL("54 - [Servizi PEC] Configurazione account PEC non definita per l'utente")     
        this.w_oMESS.AddMsgPartNL("55 - [Servizi PEC] Indirizzo email PEC non presente su anagrafica intestatario")     
        this.w_oMESS.AddMsgPartNL("56 - [Servizi PEC] Indirizzo email PEC non presente su sede intestatario")     
        this.w_oMESS.AddMsgPartNL("57 - [Servizi PEC] Indirizzo email PEC non individuato per il destinatario specificato nel campo per conto di")     
        this.w_oMESS.AddMsgPartNL("58 - [Servizi PEC] Indirizzo email PEC non valutabile per l'intestatario del processo",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori servizi PostaLite:")     
        this.w_oMESS.AddMsgPartNL("61 - [Servizi PostaLite] WorkStation non abilitata (parametri Postalite)")     
        this.w_oMESS.AddMsgPartNL("62 - [Servizi PostaLite] Utente non abilitato")     
        this.w_oMESS.AddMsgPartNL("63 - [Servizi PostaLite] Report non abilitato (parametri Postalite)")     
        this.w_oMESS.AddMsgPartNL("64 - [Servizi PostaLite] Device di stampa non definito (parametri Postalite)")     
        this.w_oMESS.AddMsgPartNL("65 - [Servizi PostaLite] Percorso files in uscita Incoming non definito (parametri Postalite)")     
        this.w_oMESS.AddMsgPartNL("66 - [Servizi PostaLite] Nome file .AFP Standard non definito (parametri Postalite)")     
        this.w_oMESS.AddMsgPartNL("67 - [Servizi PostaLite] Impossibile rinominare il file Postalite.AFP")     
        this.w_oMESS.AddMsgPartNL("68 - [Servizi PostaLite] Impossibile creare il file Postalite.AFP")     
        this.w_oMESS.AddMsgPartNL("69 - [Servizi PostaLite] Report non trovato",2)     
        * --- --
        if g_CPIN<>"S" and g_DMIP<>"S"
          if g_ICPZSTANDALONE or g_IZCP$"SA"
            this.w_oMESS.AddMsgPartNL("Errori servizi Corporate Portal:")     
            this.w_oMESS.AddMsgPartNL("71 - [Servizi Corporate Portal] Utente non abilitato")     
            this.w_oMESS.AddMsgPartNL("72 - [Servizi Corporate Portal] Espressione destinatario tipo cliente/fornitore non valutabile")     
            this.w_oMESS.AddMsgPartNL("73 - [Servizi Corporate Portal] Espressione destinatario tipo agente non valutabile")     
            this.w_oMESS.AddMsgPartNL("74 - [Servizi Corporate Portal] Path documenti non specificato in <Archivi>/<Corporate Portal</<Gestione path>")     
            this.w_oMESS.AddMsgPartNL("75 - [Servizi Corporate Portal] Impossibile creare cartella per <Documenti CPZ>")     
            this.w_oMESS.AddMsgPartNL("76 - [Servizi Corporate Portal] Permessi di base o nome file non definiti in permessi CPZ processo documentale")     
            this.w_oMESS.AddMsgPartNL("77 - [Servizi Corporate Portal] Permessi riferiti a Company Folder senza indicazione destinatario principale")     
            this.w_oMESS.AddMsgPartNL("78 - [Servizi Corporate Portal] Espressione <Nome file> non valutabile in permessi CPZ processo documentale")     
            this.w_oMESS.AddMsgPartNL("79 - [Servizi Corporate Portal] Impossibile cancellare/copiare il file nella cartella di destinazione <PATHPAR>")     
            this.w_oMESS.AddMsgPartNL("80 - [Servizi Corporate Portal] Impossibile valutare espressione <Chiave CPZ-DMS>")     
            this.w_oMESS.AddMsgPartNL("81 - [Servizi Corporate Portal] Mancanza di uno o pi� parametri riferiti a Company Folder",2)     
          endif
        else
          this.w_oMESS.AddMsgPartNL("Errori servizi Web Application:")     
          this.w_oMESS.AddMsgPartNL("71 - [Servizi Web Application] Utente non abilitato")     
          this.w_oMESS.AddMsgPartNL("72 - [Servizi Web Application] Nome file mancante nella classe documentale")     
          this.w_oMESS.AddMsgPartNL("73 - [Servizi Web Application] Nome file non valutabile")     
          this.w_oMESS.AddMsgPartNL("74 - [Servizi Web Application] Path documenti non specificato in  Parametri pubblicazione su web")     
          this.w_oMESS.AddMsgPartNL("75 - [Servizi Web Application] Impossibile creare cartella per <Documenti web>")     
          this.w_oMESS.AddMsgPartNL("82 - [Servizi Web Application] Classe documentale di tipo E.D.S. e processo con invio web privo di archiviazione (deve essere <copia file>)",2)     
        endif
        this.w_oMESS.AddMsgPartNL("Errori servizi Web Application stand alone:")     
        if g_CPIN<>"S" and g_DMIP<>"S" AND NOT(g_ICPZSTANDALONE or g_IZCP$"SA")
          this.w_oMESS.AddMsgPartNL("71 - [Servizi Web Application stand alone] Utente non abilitato")     
        endif
        this.w_oMESS.AddMsgPartNL("85 - [Servizi Web Application stand alone] Nome file mancante nella classe documentale")     
        this.w_oMESS.AddMsgPartNL("86 - [Servizi Web Application stand alone] Nome file non valutabile")     
        this.w_oMESS.AddMsgPartNL("87 - [Servizi Web Application stand alone] Percorso destinazione file XML non specificato")     
        this.w_oMESS.AddMsgPartNL("88 - [Servizi Web Application stand alone] Percorso destinazione file XML non valido",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Abilitazioni clienti/fornitori:")     
        this.w_oMESS.AddMsgPartNL("91 - [Servizi eMail] Intestatario non abilitato per invio email da processo documentale")     
        this.w_oMESS.AddMsgPartNL("92 - [Servizi WE] Intestatario non abilitato per invio a Net-Folder da processo documentale")     
        this.w_oMESS.AddMsgPartNL("93 - [Servizi Fax] Intestatario non abilitato per invio Fax da processo documentale")     
        this.w_oMESS.AddMsgPartNL("94 - [Servizi Stampa] Intestatario non abilitato per stampa da processo documentale")     
        this.w_oMESS.AddMsgPartNL("95 - [Servizi Postalite] Intestatario non abilitato per invio a Postel da processo documentale")     
        if g_CPIN<>"S"
          this.w_oMESS.AddMsgPartNL("96 - [Servizi Corporate Portal] Intestatario (principale) non abilitato per invio a Corporate Portal da processo documentale")     
          this.w_oMESS.AddMsgPartNL("97 - [Servizi Corporate Portal] Intestatario (per conto di) non abilitato per invio a Corporate Portal da processo documentale")     
        else
          this.w_oMESS.AddMsgPartNL("96 - [Servizi Web Application] Intestatario (principale) non abilitato per invio a Infinity da processo documentale")     
          this.w_oMESS.AddMsgPartNL("97 - [Servizi Web Application] Intestatario (per conto di) non abilitato per invio a Infinity da processo documentale")     
        endif
        this.w_oMESS.AddMsgPartNL("98 - [Servizi PEC] Intestatario non abilitato per invio PEC da processo documentale",2)     
        * --- --
        this.w_oMESS.AddMsgPartNL("Errori firma digitale:")     
        this.w_oMESS.AddMsgPartNL("101 - [Firma digitale] Errore nella creazione del documento firmato")     
        this.w_oMESS.AddMsgPartNL("102 - [Firma digitale] Errore in aggiornamento indice, nome file firmato")     
        this.w_oMESS.AddMsgPartNL("103 - [Firma digitale] Errore procedura non abilitata alla firma digitale ma intestatario con attivo check firma digitale")     
        this.w_oMESS.AddMsgPartNL("104 - [Firma digitale] Non � stata selezionata la modalit� di firma")     
        this.w_oMESS.AddMsgPartNL("200 - [Firma digitale] La firma digitale � demandata al Client SOStitutiva")     
        * --- Compone messaggio
        this.w_LEGENDA = this.w_oMESS.ComposeMessage()
        this.w_MASKLEG = GSDM_KLL(this)
        this.w_MASKLEG.mCalc(.T.)     
        this.w_MASKLEG = .null.
      case this.pTIPOOPE = "VL"
        this.w_MASKLOG = GSDM_KVL()
        if UPPER(this.oParentObject.class)="TGSIR_KGD"
          this.w_IRDR_MR=createobject("MultiReport")
          this.w_IRDR_MR.AddNewReport(this.oParentObject.w_AQCODICE, "","",.null.,.null.,,.t.)     
          w_IRDR_RET = .null.
          pd_StrProcesso = CHKPROCD(this.w_IRDR_MR,@w_IRDR_RET,this.pKEYISTANZA,this, .T.)
          this.w_MASKLOG.w_LPCODPRO = w_IRDR_RET.cPDCodProc
        else
          this.w_MASKLOG.w_LPCODPRO = this.pDMReportSplit.cPDCodProc
        endif
        if Empty(this.w_MASKLOG.w_LPCODPRO)
          this.w_MASKLOG.w_ALLPROC = "S"
        endif
        * --- Visualizza log status processo
        this.w_MASKLOG.w_LPKEYIST = this.pKEYISTANZA
        this.w_MASKLOG.w_LPESEDAT = DATE()
        this.w_MASKLOG.w_LPESEUTE = i_CODUTE
        * --- Read from CPUSERS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPUSERS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NAME"+;
            " from "+i_cTable+" CPUSERS where ";
                +"CODE = "+cp_ToStrODBC(i_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NAME;
            from (i_cTable) where;
                CODE = i_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TMPC = NVL(cp_ToDate(_read_.NAME),cp_NullValue(_read_.NAME))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MASKLOG.w_DESUTE = this.w_TMPC
        * --- Read from PROMDOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMDOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMDOCU_idx,2],.t.,this.PROMDOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PDDESPRO"+;
            " from "+i_cTable+" PROMDOCU where ";
                +"PDCODPRO = "+cp_ToStrODBC(this.w_MASKLOG.w_LPCODPRO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PDDESPRO;
            from (i_cTable) where;
                PDCODPRO = this.w_MASKLOG.w_LPCODPRO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TMPC = NVL(cp_ToDate(_read_.PDDESPRO),cp_NullValue(_read_.PDDESPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MASKLOG.w_DESPRO = this.w_TMPC
        this.w_MASKLOG.w_LPTIPCON = " "
        this.w_MASKLOG.w_SOLOSHOW = .T.
        this.w_MASKLOG.NotifyEvent("Interroga")     
      case this.pTIPOOPE = "ZP"
        this.w_PDREPORT = this.oParentObject.w_PDREPORT
        this.w_PDDESREP = this.oParentObject.w_PDDESREP
        if this.oParentObject.w_TipProcesso <> "IRP"
          vx_exec("..\docm\exe\query\GSDM_AOU.VZM",this)
        else
          * --- Infopublisher
          vx_exec("..\docm\exe\query\GSDM1AOU.VZM",this)
        endif
        * --- Essendo state dichiarate, in origine, due variabili "w_PDREPORT" una locale ed una caller,
        *     l'istruzione 'IF/ELSE' del Painter traduce entrambe le occorrenze della variabile suddetta con this.oparentobject.w_PDREPORT.
        *     Questo rende inutile il comando stesso, perci� � stata sostituito, onde evitare di riportare una modifica pi� onerosa, direttamente con una
        *     istruzione VFP.
        if not empty(this.w_PDREPORT) and this.oParentObject.w_PDREPORT <> this.w_PDREPORT 
 this.oParentObject.w_PDREPORT = upper(this.w_PDREPORT) 
 this.oParentObject.w_PDDESREP = this.w_PDDESREP 
 this.oParentObject.NotifyEvent("w_PDREPORT Changed") 
 endif
      case this.pTIPOOPE = "PR"
        this.w_TMPT = rtrim(this.pKEYISTANZA)
        this.w_TMPC = " "
        * --- Select from PRODDOCU
        i_nConn=i_TableProp[this.PRODDOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODDOCU_idx,2],.t.,this.PRODDOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRODDOCU ";
              +" where PDREPORT="+cp_ToStrODBC(this.w_TMPT)+" and PDCODPRO<>"+cp_ToStrODBC(this.w_CODPROC)+"";
               ,"_Curs_PRODDOCU")
        else
          select * from (i_cTable);
           where PDREPORT=this.w_TMPT and PDCODPRO<>this.w_CODPROC;
            into cursor _Curs_PRODDOCU
        endif
        if used('_Curs_PRODDOCU')
          select _Curs_PRODDOCU
          locate for 1=1
          do while not(eof())
          this.w_TMPC = _Curs_PRODDOCU.PDCODPRO
          if not empty(this.w_TMPC)
            if this.oParentObject.w_TipProcesso="IRP"
              ah_ErrorMSG("Esistono altri processi (es. %1) definiti per la query di InfoPublisher selezionata.%0Per ogni query deve essere definito un solo processo documentale",48,"",alltrim(this.w_TMPC))
            else
              ah_ErrorMSG("Esistono altri processi (es. %1) definiti per il report selezionato.%0Per ogni report deve essere definito un solo processo documentale",48,"",alltrim(this.w_TMPC))
            endif
            this.oParentObject.w_PDREPORT = space(100)
            this.oParentObject.w_PDDESREP = SPACE(50)
            exit
          endif
            select _Curs_PRODDOCU
            continue
          enddo
          use
        endif
      case this.pTIPOOPE = "EP"
        * --- Esecuzione processo documentale
        this.w_SCHEDULER = TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
        this.w_OFALLENAME = VARTYPE(i_OFALLENAME)="C" AND !EMPTY(i_OFALLENAME)
        this.w_OFFE_CLASS = IIF(this.w_OFALLENAME AND G_OFFE="S", UPPER(THIS.OPARENTOBJECT.NOPARENTOBJECT.CLASS), "XXXXXXX")
        * --- Puntatori a progress bar
        this.w_OPADRE = this.oParentObject
        if UPPER(this.w_OPADRE.Class)="TGSIR_BGD" and TYPE("this.oParentObject.oParentObject.oParentObject")="O" and UPPER(this.oParentObject.oParentObject.oParentObject.class)="ZTAM_PRNBTN"
          this.w_OPADRE = this.oParentObject.oParentObject.oParentObject.parent
        endif
        if TYPE("this.w_OPADRE.oPDPrgBar")="O"
          this.w_OPBAR = this.w_OPADRE.oPDPrgBar
          this.w_OPLABEL = this.w_OPADRE.oPDLbl4
          * --- Progress Bar
          this.w_OPLABEL.Caption = AH_MsgFormat("Esecuzione processo documentale in corso...")
          if this.w_PDPROCES="IRP"
            select (w_CURSORE)
            w_NUMERO=recno()
          endif
          if NOT UPPER(this.oParentObject.Class)="TGSIR_BGD" or w_NUMERO=1
            this.w_OPBAR.Value = 0
          endif
        endif
        this.w_StmpOrderListStr = this.pDMReportSplit.GetStmpOrderList()
        this.w_NumStmpOrder = ALINES(aStmpOrderList, this.w_StmpOrderListStr, ",")
        * --- Controllo quanti giri dovr� fare nel ciclo, mi serve per la progress bar
        this.w_nDelta = 100/this.pDMReportSplit.GetNumMainReport()
        FOR this.L_ii = 1 TO this.w_NumStmpOrder
        * --- ----------  Multireport -----------
        this.w_StmpOrder = aStmpOrderList[this.L_ii]
        this.w_CounterRep = -1
        PRIVATE w_CURSORE
        do while .T.
          this.w_CounterRep = this.w_CounterRep + 1
          this.w_nCount = this.pDMReportSplit.GetMainReport(this.w_StmpOrder, this.w_nCount)
          if this.w_nCount <> -1
            * --- Composizione:
            *     . 1>10 = Codice Processo
            *     . 11>11 = Check Stampa
            *     . 12>12 = Check Mail
            *     . 13>13 = Check Fax
            *     . 14>14 = Check We
            *     . 15>15 = Check POSTEL
            *     . 16>16 = Check Archiviazione
            *     - 17>17 = Check Corporate Portal
            *     . 18>18 = Check PEC
            *     . 19>19 = Check Obbligatoriet� Attivit� Processo
            *     . 20>20 = Check Log Processo permanente
            *     . 21>21 = Check Anteprima
            *     . 22>27 = Numero documenti esaminati
            *     . 28>33 = Numero documenti da processare
            *     . 34>39 = Numero documenti non processabili
            *     . 40>89 = Campo identificativo codice ISO lingua processo
            this.w_CODPROC = substr(this.pDMReportSplit.GetStrProc(this.w_nCount),1,10)
            this.w_STRCONTROL = substr(this.pDMReportSplit.GetStrProc(this.w_nCount),11,8)
            * --- Legge dati da processo documentale 
            this.w_CATEGORIA = " "
            * --- Read from PROMDOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMDOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMDOCU_idx,2],.t.,this.PROMDOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PDCLADOC,PDNMAXAL,PDPROCES"+;
                " from "+i_cTable+" PROMDOCU where ";
                    +"PDCODPRO = "+cp_ToStrODBC(this.w_CODPROC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PDCLADOC,PDNMAXAL,PDPROCES;
                from (i_cTable) where;
                    PDCODPRO = this.w_CODPROC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PDCLADOC = NVL(cp_ToDate(_read_.PDCLADOC),cp_NullValue(_read_.PDCLADOC))
              this.w_NUMMAXALL = NVL(cp_ToDate(_read_.PDNMAXAL),cp_NullValue(_read_.PDNMAXAL))
              this.w_PDPROCES = NVL(cp_ToDate(_read_.PDPROCES),cp_NullValue(_read_.PDPROCES))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_REPORT = Forceext(alltrim(this.pDMReportSplit.GetReportOrig(this.w_nCount)), iif(this.w_PDPROCES="IRP","",".FRX"))
            * --- Bisognerebbe fare una READ sul database ma siccome non si pu� inserire la condizione  UPPER(PRODDOCU.PDREPORT) = UPPER( 'w_REPORT' ))   
            *     bisogna utilizzare una query dopodich� si effettua l assegnazione dei campi letti dalla query alle variabili locali
            VQ_EXEC("..\DOCM\EXE\QUERY\GSDM_BPD.VQR", this, "_Curs_PRODDOCU")
            if USED("_Curs_PRODDOCU")
              Select _Curs_PRODDOCU
              this.w_PDKEYPRO = NVL(PDKEYPRO,"")
              this.w_PDCODCON = NVL(PDCONCON,"")
              this.w_PDOGGETT = NVL(PDOGGETT,"")
              this.w_PD_TESTO = NVL(PD_TESTO,"")
              this.w_PDISOLAN = NVL(PDISOLAN,"")
              Select _Curs_PRODDOCU 
 Use
            endif
            * --- Legge dalla classe documentale il campo data da usare per il raggruppamento (archiviazione)
            if this.w_PDPROCES="IRP"
              * --- Read from PROMCLAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PROMCLAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CDCAMRAG,CDPATSTD,CDTIPRAG"+;
                  " from "+i_cTable+" PROMCLAS where ";
                      +"CDCODCLA = "+cp_ToStrODBC(this.w_PDCLADOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CDCAMRAG,CDPATSTD,CDTIPRAG;
                  from (i_cTable) where;
                      CDCODCLA = this.w_PDCLADOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CDCAMRAG = NVL(cp_ToDate(_read_.CDCAMRAG),cp_NullValue(_read_.CDCAMRAG))
                this.w_LIPATIMG = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
                this.w_CRITPERI = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Stringa che individua la sessione di stampa (per evitare, quando necessario, di tradurre pi� volte lo stesso report)
            * --- Esecuzione Processo
            this.w_REPSN = substr(this.w_STRCONTROL,1,1)
            this.w_MAILSN = substr(this.w_STRCONTROL,2,1)
            this.w_PECSN = substr(this.w_STRCONTROL,8,1)
            this.w_FAXSN = substr(this.w_STRCONTROL,3,1)
            this.w_WESN = substr(this.w_STRCONTROL,4,1)
            this.w_POSTELSN = substr(this.w_STRCONTROL,5,1)
            this.w_ARCHSN = substr(this.w_STRCONTROL,6,1)
            this.w_CPZSN = substr(this.w_STRCONTROL,7,1)
            * --- Legge percorso per Corporate Portal
            this.w_COPATHDO = " "
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDMODALL"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_PDCLADOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDMODALL;
                from (i_cTable) where;
                    CDCODCLA = this.w_PDCLADOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CDMODALL = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if (g_IZCP$"SA" and this.w_CPZSN="S") or g_CPIN = "S" or g_DMIP="S" or this.w_CDMODALL="S"
              * --- Legge percorso per documenti
              this.w_CODAZI = i_CODAZI
              * --- Read from CONTROPA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTROPA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "COPATHDO,COPATHDS"+;
                  " from "+i_cTable+" CONTROPA where ";
                      +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  COPATHDO,COPATHDS;
                  from (i_cTable) where;
                      COCODAZI = this.w_CODAZI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
                this.w_COPATHDS = NVL(cp_ToDate(_read_.COPATHDS),cp_NullValue(_read_.COPATHDS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Aggiunge barra finale
              this.w_COPATHDO = ADDBS(this.w_COPATHDO)
              if g_CPIN<>"S" and this.w_CDMODALL<>"S"
                * --- Determina tipo folder e diritti di base -----------------------
                * --- Read from PRO_PCPZ
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRO_PCPZ_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRO_PCPZ_idx,2],.t.,this.PRO_PCPZ_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DERISERV,DEFOLDER,DENOMFIL,DEFILTIP,DEFILKEY"+;
                    " from "+i_cTable+" PRO_PCPZ where ";
                        +"DECODICE = "+cp_ToStrODBC(this.w_CODPROC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DERISERV,DEFOLDER,DENOMFIL,DEFILTIP,DEFILKEY;
                    from (i_cTable) where;
                        DECODICE = this.w_CODPROC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_RISERVATO = NVL(cp_ToDate(_read_.DERISERV),cp_NullValue(_read_.DERISERV))
                  this.w_CFOLDER = NVL(cp_ToDate(_read_.DEFOLDER),cp_NullValue(_read_.DEFOLDER))
                  this.w_DENOMFIL = NVL(cp_ToDate(_read_.DENOMFIL),cp_NullValue(_read_.DENOMFIL))
                  this.w_DEFILTIP = NVL(cp_ToDate(_read_.DEFILTIP),cp_NullValue(_read_.DEFILTIP))
                  this.w_DEFILKEY = NVL(cp_ToDate(_read_.DEFILKEY),cp_NullValue(_read_.DEFILKEY))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from ZFOLDERS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ZFOLDERS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ZFOLDERS_idx,2],.t.,this.ZFOLDERS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "FO__TIPO,FO__PATH"+;
                    " from "+i_cTable+" ZFOLDERS where ";
                        +"FOSERIAL = "+cp_ToStrODBC(this.w_CFOLDER);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    FO__TIPO,FO__PATH;
                    from (i_cTable) where;
                        FOSERIAL = this.w_CFOLDER;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TFOLDER = NVL(cp_ToDate(_read_.FO__TIPO),cp_NullValue(_read_.FO__TIPO))
                  this.w_PFOLDER = NVL(cp_ToDate(_read_.FO__PATH),cp_NullValue(_read_.FO__PATH))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Assegno il valore letto alla variabile di appoggio 'w_PFOLDER_A', per averlo sempre disponibile all'interno del ciclo sui dati da stampare (V. ristampa documenti)
                this.w_PFOLDER_A = this.w_PFOLDER
                * --- Se folder di tipo company, determina destinatario principale
                if this.w_TFOLDER="C"
                  * --- Read from PRO_PCPZ
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PRO_PCPZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRO_PCPZ_idx,2],.t.,this.PRO_PCPZ_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "DETIPDES,DEKEYINF"+;
                      " from "+i_cTable+" PRO_PCPZ where ";
                          +"DECODICE = "+cp_ToStrODBC(this.w_CODPROC);
                          +" and DERIFCFO = "+cp_ToStrODBC("S");
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      DETIPDES,DEKEYINF;
                      from (i_cTable) where;
                          DECODICE = this.w_CODPROC;
                          and DERIFCFO = "S";
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_TIPDES1A = NVL(cp_ToDate(_read_.DETIPDES),cp_NullValue(_read_.DETIPDES))
                    this.w_DEKEYINF = NVL(cp_ToDate(_read_.DEKEYINF),cp_NullValue(_read_.DEKEYINF))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
              else
                * --- Infinity
                *     Leggo nome file e flag erase
                * --- Read from PROMCLAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CDNOMFIL,CDERASEF"+;
                    " from "+i_cTable+" PROMCLAS where ";
                        +"CDCODCLA = "+cp_ToStrODBC(this.w_PDCLADOC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CDNOMFIL,CDERASEF;
                    from (i_cTable) where;
                        CDCODCLA = this.w_PDCLADOC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_NOMEFILEIN = NVL(cp_ToDate(_read_.CDNOMFIL),cp_NullValue(_read_.CDNOMFIL))
                  this.w_CDERASEF = NVL(cp_ToDate(_read_.CDERASEF),cp_NullValue(_read_.CDERASEF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_CDMODALL="S"
                  this.w_COPATHDO = ADDBS(this.w_COPATHDS)
                endif
              endif
            endif
            w_CURSORE = this.pDMReportSplit.GetCursorName(this.w_nCount)
            if USED(w_CURSORE)
              * --- Report Principale per categoria, elimino l'eventuale estensione
              this.w_REPORT = Forceext(alltrim(this.pDMReportSplit.GetReportOrig(this.w_nCount)), iif(this.w_PDPROCES="IRP","",".FRX"))
              this.w_NomRep = this.w_REPORT
              this.w_QUERY = "2"
              this.w_PROCOD = this.w_CODPROC
              * --- Select from ..\DOCM\EXE\QUERY\chkprocd
              do vq_exec with '..\DOCM\EXE\QUERY\chkprocd',this,'_Curs__d__d__DOCM_EXE_QUERY_chkprocd','',.f.,.t.
              if used('_Curs__d__d__DOCM_EXE_QUERY_chkprocd')
                select _Curs__d__d__DOCM_EXE_QUERY_chkprocd
                locate for 1=1
                do while not(eof())
                this.w_PDKEYPRO = NVL(PDKEYPRO,"")
                this.w_PDCODCON = UPPER (ALLTRIM(NVL(PDCONCON,"")))
                this.w_PDCODORN = UPPER (ALLTRIM(NVL(PDCODORD,"")))
                this.w_PDOGGETT = NVL(PDOGGETT,"")
                this.w_PD_TESTO = NVL(PD_TESTO,"")
                this.w_PDISOLAN = NVL(PDISOLAN,"")
                  select _Curs__d__d__DOCM_EXE_QUERY_chkprocd
                  continue
                enddo
                use
              endif
              * --- Legge dalla classe documentale il campo data da usare per il raggruppamento (archiviazione)
              if this.w_PDPROCES="IRP"
                * --- Read from PROMCLAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CDCAMRAG,CDPATSTD,CDTIPRAG"+;
                    " from "+i_cTable+" PROMCLAS where ";
                        +"CDCODCLA = "+cp_ToStrODBC(this.w_PDCLADOC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CDCAMRAG,CDPATSTD,CDTIPRAG;
                    from (i_cTable) where;
                        CDCODCLA = this.w_PDCLADOC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CDCAMRAG = NVL(cp_ToDate(_read_.CDCAMRAG),cp_NullValue(_read_.CDCAMRAG))
                  this.w_LIPATIMG = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
                  this.w_CRITPERI = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              * --- Prepara chiave di rottura
              this.w_CALLEGA = 0
              this.w_CONTADOC = 0
              this.w_CONTAPTL = 0
              this.w_VALKEYROTT = "@#!?"
              * --- =================================== SCANSIONE TEMPORANEO DI STAMPA ================================
              * --- Conta il numero di record
              if this.w_PDPROCES<>"IRP"
                this.w_NRECTOT = this.w_OPADRE.pdnTotDocOk *2 +2
              else
                this.w_NRECTOT = reccount(w_CURSORE)
              endif
              this.w_NREC = 0
              if this.w_PDPROCES="IRP"
                select (w_CURSORE)
                w_NUMERO=recno()
              endif
              * --- Stringa che identifica la sessione di stampa (evita che la stampa venga tradotta pi� volte all'interno dello stesso processo)
              this.w_REPSESCODE = IIF(Vartype(pREPSESCODE)="C" and Not(Empty(pREPSESCODE)), pREPSESCODE, sys(2015))
              * --- Creo il Multireport
              this.w_MultiRep=createobject("Multireport")
              * --- Ordino il cursore di stampa in base alla chiave impostata nel processo documentale
              if this.w_PDPROCES#"IRP"
                ORD = alltrim(this.w_PDKEYPRO)
                * --- Ordino per i campi o il campo specificato come chiave del processo
                Select &ORD as _TMPORDER,* from (w_CURSORE) order by 1 into cursor TEMP_CURS
                USE IN SELECT(w_CURSORE)
                * --- Riverso nel cursore di stampa i record ordinati per il campo chiave specificato nel processo
                Select * FROM TEMP_CURS into cursor (w_CURSORE)
                * --- Elimino il cursore di appoggio...
                USE IN SELECT("TEMP_CURS")
                * --- Elimino il campo inserito per ordinare il cursore  '_TMPORDER'
                 =WRCURSOR(w_CURSORE) 
                ALTER TABLE (w_CURSORE) DROP COLUMN _TMPORDER
              endif
              * --- Cicla sui dati da stampare ...
              select (w_CURSORE)
              SCAN FOR this.w_PDPROCES<>"IRP" or RECNO()=w_NUMERO
              if (this.w_PDPROCES<>"IRP" and Eval(this.w_PDKEYPRO) <> this.w_VALKEYROTT) or (this.w_PDPROCES="IRP" and this.w_PDKEYPRO <> this.w_VALKEYROTT)
                this.w_TIPDES1 = this.w_TIPDES1A
                this.w_VALKEYROTT = Alltrim(iif(this.w_PDPROCES<>"IRP" ,Eval(this.w_PDKEYPRO),this.w_PDKEYPRO))
                this.w_NUMDESTAG = ALINES(w_DESTINATARI, this.w_PDCODCON, ";")
                if this.w_PDPROCES="IRP" 
                  * --- non ci possono essere dest. aggiuntivi 
                  this.w_NUMDESTAG = 1
                endif
                this.w_DESTAGG = this.w_NUMDESTAG > 1
                this.w_VALCODCON = IIF(empty(w_DESTINATARI)," ",Eval(w_DESTINATARI))
                this.w_VALOGGETTO = IIF(empty(this.w_PDOGGETT)," ",Eval(this.w_PDOGGETT))
                this.w_VALOGGETTO = IIF(vartype(this.w_VALOGGETTO)="C", left(this.w_VALOGGETTO,75), " ")
                this.w_VALTESTO = IIF(empty(this.w_PD_TESTO)," ",Eval(this.w_PD_TESTO))
                this.w_VALDATRAG = IIF(empty(this.w_CDCAMRAG), i_DATSYS, eval(this.w_CDCAMRAG))
                * --- Legge dati anagrafici da LOG processo
                this.w_COUNTER = nvl(&w_CURSORE..__PD_KPR__ , 0)
                this.w_COUNTERXCO = this.w_COUNTER
                * --- Read from PRO_LOGP
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2],.t.,this.PRO_LOGP_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "LPCHKMAI,LPCHKPEC,LPCHKFAX,LPCHKWWP,LPCHKSTA,LPCHKARC,LPANEMAIL,LPANEMPEC,LPWELOGI,LPWEMAIL,LPWETXMA,LPWE_SMS,LPWETSMS,LPWE_FAX,LPNOMDSC,LPINDSWE,LPDOMIWE,LPFROFAX,LPTELFAX,LPRAGSOC,LPCHKPTL,LPCHKCPZ,LPCHKFIR,LPINTFIR,LPDATFIR"+;
                    " from "+i_cTable+" PRO_LOGP where ";
                        +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                        +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTER);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    LPCHKMAI,LPCHKPEC,LPCHKFAX,LPCHKWWP,LPCHKSTA,LPCHKARC,LPANEMAIL,LPANEMPEC,LPWELOGI,LPWEMAIL,LPWETXMA,LPWE_SMS,LPWETSMS,LPWE_FAX,LPNOMDSC,LPINDSWE,LPDOMIWE,LPFROFAX,LPTELFAX,LPRAGSOC,LPCHKPTL,LPCHKCPZ,LPCHKFIR,LPINTFIR,LPDATFIR;
                    from (i_cTable) where;
                        LPKEYIST = this.pKEYISTANZA;
                        and LPROWNUM = this.w_COUNTER;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_LPCHKMAI = NVL(cp_ToDate(_read_.LPCHKMAI),cp_NullValue(_read_.LPCHKMAI))
                  this.w_LPCHKPEC = NVL(cp_ToDate(_read_.LPCHKPEC),cp_NullValue(_read_.LPCHKPEC))
                  this.w_LPCHKFAX = NVL(cp_ToDate(_read_.LPCHKFAX),cp_NullValue(_read_.LPCHKFAX))
                  this.w_LPCHKWWP = NVL(cp_ToDate(_read_.LPCHKWWP),cp_NullValue(_read_.LPCHKWWP))
                  this.w_LPCHKSTA = NVL(cp_ToDate(_read_.LPCHKSTA),cp_NullValue(_read_.LPCHKSTA))
                  this.w_LPCHKARC = NVL(cp_ToDate(_read_.LPCHKARC),cp_NullValue(_read_.LPCHKARC))
                  this.w_LPANEMAIL = NVL(cp_ToDate(_read_.LPANEMAIL),cp_NullValue(_read_.LPANEMAIL))
                  this.w_LPANEMPEC = NVL(cp_ToDate(_read_.LPANEMPEC),cp_NullValue(_read_.LPANEMPEC))
                  this.w_LPWELOGI = NVL(cp_ToDate(_read_.LPWELOGI),cp_NullValue(_read_.LPWELOGI))
                  this.w_LPWEMAIL = NVL(cp_ToDate(_read_.LPWEMAIL),cp_NullValue(_read_.LPWEMAIL))
                  this.w_LPWETXMA = NVL(cp_ToDate(_read_.LPWETXMA),cp_NullValue(_read_.LPWETXMA))
                  this.w_LPWE_SMS = NVL(cp_ToDate(_read_.LPWE_SMS),cp_NullValue(_read_.LPWE_SMS))
                  this.w_LPWETSMS = NVL(cp_ToDate(_read_.LPWETSMS),cp_NullValue(_read_.LPWETSMS))
                  this.w_LPWE_FAX = NVL(cp_ToDate(_read_.LPWE_FAX),cp_NullValue(_read_.LPWE_FAX))
                  this.w_LPNOMDSC = NVL(cp_ToDate(_read_.LPNOMDSC),cp_NullValue(_read_.LPNOMDSC))
                  this.w_LPINDSWE = NVL(cp_ToDate(_read_.LPINDSWE),cp_NullValue(_read_.LPINDSWE))
                  this.w_LPDOMIWE = NVL(cp_ToDate(_read_.LPDOMIWE),cp_NullValue(_read_.LPDOMIWE))
                  this.w_LPFROFAX = NVL(cp_ToDate(_read_.LPFROFAX),cp_NullValue(_read_.LPFROFAX))
                  this.w_LPTELFAX = NVL(cp_ToDate(_read_.LPTELFAX),cp_NullValue(_read_.LPTELFAX))
                  this.w_LPRAGSOC = NVL(cp_ToDate(_read_.LPRAGSOC),cp_NullValue(_read_.LPRAGSOC))
                  this.w_LPCHKPTL = NVL(cp_ToDate(_read_.LPCHKPTL),cp_NullValue(_read_.LPCHKPTL))
                  this.w_LPCHKCPZ = NVL(cp_ToDate(_read_.LPCHKCPZ),cp_NullValue(_read_.LPCHKCPZ))
                  this.w_LPCHKFIR = NVL(cp_ToDate(_read_.LPCHKFIR),cp_NullValue(_read_.LPCHKFIR))
                  this.w_FIRMRICH = NVL(cp_ToDate(_read_.LPINTFIR),cp_NullValue(_read_.LPINTFIR))
                  this.w_DATAFIRM = NVL((_read_.LPDATFIR),cp_NullValue(_read_.LPDATFIR))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_LCHKMAIAGG = -1
                this.w_LCHKPECAGG = -1
                this.w_LCHKFAXAGG = -1
                this.w_LEMAILAGG = ""
                this.w_LEMPECAGG = ""
                if this.w_DESTAGG
                  * --- il fax va gestitio come invio singoolo,. devo usare un array
                  declare fax_res(this.w_NUMDESTAG,3)
                  this.w_IDXADD = 2
                  do while this.w_IDXADD<=this.w_NUMDESTAG
                    this.w_COUNTERXCO = this.w_COUNTERXCO+1
                    * --- Read from PRO_LOGP
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2],.t.,this.PRO_LOGP_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "LPANEMAIL,LPANEMPEC,LPTELFAX,LPCHKMAI,LPCHKPEC,LPCHKFAX,LPRAGSOC"+;
                        " from "+i_cTable+" PRO_LOGP where ";
                            +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                            +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTERXCO);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        LPANEMAIL,LPANEMPEC,LPTELFAX,LPCHKMAI,LPCHKPEC,LPCHKFAX,LPRAGSOC;
                        from (i_cTable) where;
                            LPKEYIST = this.pKEYISTANZA;
                            and LPROWNUM = this.w_COUNTERXCO;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_EMAILAGG = NVL(cp_ToDate(_read_.LPANEMAIL),cp_NullValue(_read_.LPANEMAIL))
                      this.w_EMPECAGG = NVL(cp_ToDate(_read_.LPANEMPEC),cp_NullValue(_read_.LPANEMPEC))
                      this.w_TELFAXAGG = NVL(cp_ToDate(_read_.LPTELFAX),cp_NullValue(_read_.LPTELFAX))
                      this.w_CHKMAIAGG = NVL(cp_ToDate(_read_.LPCHKMAI),cp_NullValue(_read_.LPCHKMAI))
                      this.w_CHKPECAGG = NVL(cp_ToDate(_read_.LPCHKPEC),cp_NullValue(_read_.LPCHKPEC))
                      this.w_CHKFAXAGG = NVL(cp_ToDate(_read_.LPCHKFAX),cp_NullValue(_read_.LPCHKFAX))
                      this.w_RAGSOCAGG = NVL(cp_ToDate(_read_.LPRAGSOC),cp_NullValue(_read_.LPRAGSOC))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.w_LCHKMAIAGG = IIF(this.w_LCHKMAIAGG=-1, this.w_CHKMAIAGG, MIN(this.w_LCHKMAIAGG,this.w_CHKMAIAGG))
                    this.w_LCHKPECAGG = IIF(this.w_LCHKPECAGG=-1, this.w_CHKPECAGG, MIN(this.w_LCHKPECAGG,this.w_CHKPECAGG))
                    this.w_LCHKFAXAGG = IIF(this.w_LCHKFAXAGG=-1, this.w_CHKFAXAGG, MIN(this.w_LCHKFAXAGG,this.w_CHKFAXAGG))
                    this.w_LEMAILAGG = this.w_LEMAILAGG + iif(this.w_CHKMAIAGG=0, alltrim(this.w_EMAILAGG) + ";", "")
                    this.w_LEMPECAGG = this.w_LEMPECAGG + iif(this.w_CHKPECAGG=0, alltrim(this.w_EMPECAGG) + ";", "")
                    fax_res[this.w_IDXADD,1] = this.w_CHKFAXAGG
                    fax_res[this.w_IDXADD,2] = this.w_TELFAXAGG
                    fax_res[this.w_IDXADD,3] = this.w_RAGSOCAGG
                    this.w_IDXADD = this.w_IDXADD+1
                  enddo
                endif
                this.w_CHKMAIAGG = this.w_LCHKMAIAGG
                this.w_CHKPECAGG = this.w_LCHKPECAGG
                this.w_CHKFAXAGG = this.w_LCHKFAXAGG
                this.w_EMAILAGG = left(this.w_LEMAILAGG, len(this.w_LEMAILAGG) - iif(right(this.w_LEMAILAGG,1)=";", 1,0))
                this.w_EMPECAGG = left(this.w_LEMPECAGG, len(this.w_LEMPECAGG) - iif(right(this.w_LEMPECAGG,1)=";", 1,0))
                this.w_CHKMAIXCO = -1
                this.w_CHKPECXCO = -1
                this.w_CHKFAXXCO = -1
                this.w_EMAILXCO = ""
                this.w_EMPECXCO = ""
                this.w_TELFAXXCO = ""
                this.w_RAGSOCXCO = ""
                if Not Empty(this.w_PDCODORN)
                  this.w_COUNTERXCO = this.w_COUNTERXCO+1
                  * --- Read from PRO_LOGP
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2],.t.,this.PRO_LOGP_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LPANEMAIL,LPANEMPEC,LPTELFAX,LPCHKMAI,LPCHKPEC,LPCHKFAX,LPRAGSOC"+;
                      " from "+i_cTable+" PRO_LOGP where ";
                          +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                          +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTERXCO);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LPANEMAIL,LPANEMPEC,LPTELFAX,LPCHKMAI,LPCHKPEC,LPCHKFAX,LPRAGSOC;
                      from (i_cTable) where;
                          LPKEYIST = this.pKEYISTANZA;
                          and LPROWNUM = this.w_COUNTERXCO;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_EMAILXCO = NVL(cp_ToDate(_read_.LPANEMAIL),cp_NullValue(_read_.LPANEMAIL))
                    this.w_EMPECXCO = NVL(cp_ToDate(_read_.LPANEMPEC),cp_NullValue(_read_.LPANEMPEC))
                    this.w_TELFAXXCO = NVL(cp_ToDate(_read_.LPTELFAX),cp_NullValue(_read_.LPTELFAX))
                    this.w_CHKMAIXCO = NVL(cp_ToDate(_read_.LPCHKMAI),cp_NullValue(_read_.LPCHKMAI))
                    this.w_CHKPECXCO = NVL(cp_ToDate(_read_.LPCHKPEC),cp_NullValue(_read_.LPCHKPEC))
                    this.w_CHKFAXXCO = NVL(cp_ToDate(_read_.LPCHKFAX),cp_NullValue(_read_.LPCHKFAX))
                    this.w_RAGSOCXCO = NVL(cp_ToDate(_read_.LPRAGSOC),cp_NullValue(_read_.LPRAGSOC))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                * --- Crea Record in tabella indici documenti
                * --- Read from PROSLOGP
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PROSLOGP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROSLOGP_idx,2],.t.,this.PROSLOGP_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "LSCHKSTA,LSCHKFAX,LSCHKMAI,LSCHKPEC,LSCHKWWP,LSCHKPTL,LSCHKCPZ"+;
                    " from "+i_cTable+" PROSLOGP where ";
                        +"LSCODPRO = "+cp_ToStrODBC(this.w_CODPROC);
                        +" and LSKEYVAL = "+cp_ToStrODBC(this.w_VALKEYROTT);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    LSCHKSTA,LSCHKFAX,LSCHKMAI,LSCHKPEC,LSCHKWWP,LSCHKPTL,LSCHKCPZ;
                    from (i_cTable) where;
                        LSCODPRO = this.w_CODPROC;
                        and LSKEYVAL = this.w_VALKEYROTT;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_oESEARC = NVL(cp_ToDate(_read_.LSCHKSTA),cp_NullValue(_read_.LSCHKSTA))
                  this.w_oESEFAX = NVL(cp_ToDate(_read_.LSCHKFAX),cp_NullValue(_read_.LSCHKFAX))
                  this.w_oESEMAI = NVL(cp_ToDate(_read_.LSCHKMAI),cp_NullValue(_read_.LSCHKMAI))
                  this.w_oESEPEC = NVL(cp_ToDate(_read_.LSCHKPEC),cp_NullValue(_read_.LSCHKPEC))
                  this.w_oESESTA = NVL(cp_ToDate(_read_.LSCHKSTA),cp_NullValue(_read_.LSCHKSTA))
                  this.w_oESEWWP = NVL(cp_ToDate(_read_.LSCHKWWP),cp_NullValue(_read_.LSCHKWWP))
                  this.w_oESEPTL = NVL(cp_ToDate(_read_.LSCHKPTL),cp_NullValue(_read_.LSCHKPTL))
                  this.w_oESECPZ = NVL(cp_ToDate(_read_.LSCHKCPZ),cp_NullValue(_read_.LSCHKCPZ))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if i_Rows=0
                  * --- Crea record di log sintetico
                  * --- Insert into PROSLOGP
                  i_nConn=i_TableProp[this.PROSLOGP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROSLOGP_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROSLOGP_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"LSCODPRO"+",LSKEYVAL"+",LSUPRESE"+",LSDPRESE"+",LSOGGETT"+",LS_TESTO"+",LSCHKMAI"+",LSCHKPEC"+",LSCHKSTA"+",LSCHKFAX"+",LSCHKWWP"+",LSCHKARC"+",LSCHKPTL"+",LSCHKCPZ"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_CODPROC),'PROSLOGP','LSCODPRO');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_VALKEYROTT),'PROSLOGP','LSKEYVAL');
                    +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PROSLOGP','LSUPRESE');
                    +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROSLOGP','LSDPRESE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_VALOGGETTO),'PROSLOGP','LSOGGETT');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_VALTESTO),'PROSLOGP','LS_TESTO');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKMAI');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKPEC');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKSTA');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKFAX');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKWWP');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKARC');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKPTL');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PROSLOGP','LSCHKCPZ');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'LSCODPRO',this.w_CODPROC,'LSKEYVAL',this.w_VALKEYROTT,'LSUPRESE',i_CODUTE,'LSDPRESE',i_DATSYS,'LSOGGETT',this.w_VALOGGETTO,'LS_TESTO',this.w_VALTESTO,'LSCHKMAI',"N",'LSCHKPEC',"N",'LSCHKSTA',"N",'LSCHKFAX',"N",'LSCHKWWP',"N",'LSCHKARC',"N")
                    insert into (i_cTable) (LSCODPRO,LSKEYVAL,LSUPRESE,LSDPRESE,LSOGGETT,LS_TESTO,LSCHKMAI,LSCHKPEC,LSCHKSTA,LSCHKFAX,LSCHKWWP,LSCHKARC,LSCHKPTL,LSCHKCPZ &i_ccchkf. );
                       values (;
                         this.w_CODPROC;
                         ,this.w_VALKEYROTT;
                         ,i_CODUTE;
                         ,i_DATSYS;
                         ,this.w_VALOGGETTO;
                         ,this.w_VALTESTO;
                         ,"N";
                         ,"N";
                         ,"N";
                         ,"N";
                         ,"N";
                         ,"N";
                         ,"N";
                         ,"N";
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
                * --- Se deve inviare la mail o inviare il documento su NET-FOLDER prepara allegato
                this.w_NOMEFILE = " "
                this.w_GOPROC = (this.w_MAILSN="S" and (this.w_LPCHKMAI=0 OR this.w_CHKMAIAGG=0)) or (this.w_PECSN="S" and (this.w_LPCHKPEC=0 OR this.w_CHKPECAGG=0)) or (this.w_WESN="S" and this.w_LPCHKWWP=0) 
                if this.w_GOPROC or (this.w_FAXSN="S" and (g_TIPFAX="M" Or g_TIPFAX="E") and (this.w_LPCHKFAX=0 OR this.w_CHKFAXAGG=0)) or (this.w_ARCHSN="S" and this.w_LPCHKARC=0) or (this.w_CPZSN="S" and this.w_LPCHKCPZ=0)
                  this.w_OKALLEGA = .F.
                  this.w_NALLEGA = " "
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if not this.w_OKALLEGA
                    this.w_LPCHKMAI = IIF( this.w_MAILSN="S" and empty(this.w_LPCHKMAI), 14, this.w_LPCHKMAI)
                    this.w_LPCHKPEC = IIF( this.w_PECSN="S" and empty(this.w_LPCHKPEC), 14, this.w_LPCHKPEC)
                    this.w_CHKMAIAGG = IIF( this.w_MAILSN="S" and empty(this.w_CHKMAIAGG), 14, this.w_CHKMAIAGG)
                    this.w_CHKPECAGG = IIF( this.w_PECSN="S" and empty(this.w_CHKPECAGG), 14, this.w_CHKPECAGG)
                    this.w_CHKMAIXCO = IIF( this.w_MAILSN="S" and empty(this.w_CHKMAIXCO), 14, this.w_CHKMAIXCO)
                    this.w_CHKPECXCO = IIF( this.w_PECSN="S" and empty(this.w_CHKPECXCO), 14, this.w_CHKPECXCO)
                    this.w_LPCHKWWP = IIF( this.w_WESN="S" and empty(this.w_LPCHKWWP), 14, this.w_LPCHKWWP)
                    this.w_LPCHKFAX = IIF(empty(this.w_LPCHKFAX) and this.w_FAXSN="S" and g_TIPFAX="M", 14, this.w_LPCHKFAX)
                    this.w_CHKFAXAGG = IIF(empty(this.w_CHKFAXAGG) and this.w_FAXSN="S" and g_TIPFAX="M", 14, this.w_CHKFAXAGG)
                    this.w_CHKFAXXCO = IIF(empty(this.w_CHKFAXXCO) and this.w_FAXSN="S" and g_TIPFAX="M", 14, this.w_CHKFAXXCO)
                    this.w_LPCHKCPZ = IIF( this.w_CPZSN="S" and empty(this.w_LPCHKCPZ), 14, this.w_LPCHKCPZ)
                  endif
                endif
                * --- CASO Archiviazione <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                if this.w_ARCHSN="S" and empty(this.w_LPCHKARC) and this.w_OKALLEGA
                  * --- Gestione ARCHIVIAZIONE DOCUMENTALE ===============================================================================================
                  * --- La gestione di archiviazione tramite ArchiWEB non � attualmente gestibile
                  *     ne viene quindi inibita l'esecuzione
                  w_SERIALEID=space(15)
                  this.w_INARCHIVIO = "S"
                  if ";"$this.w_NOMEFILE
                    this.w_NOMEFILE = this.create_zip(this.w_NOMEFILE)
                  endif
                  Private L_ArrParam
                  dimension L_ArrParam(30)
                  L_ArrParam(1)="PR"
                  L_ArrParam(2)=this.w_NOMEFILE
                  L_ArrParam(3)=this.w_PDCLADOC
                  L_ArrParam(4)=i_CODUTE
                  L_ArrParam(5)=w_CURSORE
                  L_ArrParam(6)=this.w_CODPROC
                  L_ArrParam(7)=this.w_VALKEYROTT
                  L_ArrParam(8)=this.w_VALOGGETTO
                  L_ArrParam(9)=this.w_VALTESTO
                  L_ArrParam(10)=.T.
                  L_ArrParam(11)=.F.
                  L_ArrParam(12)=" "
                  L_ArrParam(13)=" "
                  L_ArrParam(14)=this.oParentObject
                  L_ArrParam(15)=" "
                  L_ArrParam(16)=" "
                  L_ArrParam(17)=this.w_PDPROCES
                  L_ArrParam(18)=" "
                  L_ArrParam(19)=this.w_OPERAZIONE
                  L_ArrParam(21)=.F.
                  L_ArrParam(22)=" "
                  L_ArrParam(23)=( (g_IZCP$"SA" And this.w_CPZSN="S" OR g_CPIN="S") and empty(this.w_LPCHKCPZ) )
                  this.w_LPCHKARC = GSUT_BBA(this, @L_ArrParam, @w_SERIALEID)
                  this.w_APPWEBARC = .F.
                  if (g_IZCP$"SA" And this.w_CPZSN="S" OR g_CPIN="S") and empty(this.w_LPCHKCPZ) and this.w_OKALLEGA
                    this.w_APPWEBARC = .T.
                  endif
                endif
                * --- FIRMA DIGITALE DELL'ALLEGATO SE RICHIESTO
                if empty(this.w_LPCHKFIR) and vartype(w_SERIALEID) = "C"
                  this.w_PATDOCFIR = addbs(substr(w_SERIALEID,at("*",w_SERIALEID)+1))
                  w_SERIALEID=left(w_SERIALEID,at("*",w_SERIALEID)-1)
                  * --- Avvio la funzione per creare il documento firmato
                  this.w_NOMEFILFIR = FirDigDoc(Justpath(this.w_NOMEFILE),this.w_PATDOCFIR,JUSTFNAME(this.w_NOMEFILE),this.w_DATAFIRM)
                  if this.w_NOMEFILFIR # "Error"
                    if ! empty(w_SERIALEID)
                      * --- Write into PROMINDI
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.PROMINDI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"IDDOCFIR ="+cp_NullLink(cp_ToStrODBC(JustFname(this.w_NOMEFILFIR)),'PROMINDI','IDDOCFIR');
                            +i_ccchkf ;
                        +" where ";
                            +"IDSERIAL = "+cp_ToStrODBC(w_SERIALEID);
                               )
                      else
                        update (i_cTable) set;
                            IDDOCFIR = JustFname(this.w_NOMEFILFIR);
                            &i_ccchkf. ;
                         where;
                            IDSERIAL = w_SERIALEID;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    else
                      * --- Errore: non restitutita la chiave dell'indice creato/modificato
                      this.w_LPCHKFIR = 102
                    endif
                  else
                    * --- Si � verificato un errore in fase di creazione del documento firmato
                    this.w_LPCHKFIR = 101
                  endif
                endif
                if this.w_OKALLEGA
                  * --- CASO MAIL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                  if this.w_MAILSN="S" and (Empty(this.w_LPCHKMAI) OR Empty(this.w_CHKMAIAGG))
                    public g_DOCM_EMAILSN 
 g_DOCM_EMAILSN= .t. 
 w_OLDANEMAIL = this.w_LPANEMAIL
                    do case
                      case Empty(this.w_LPCHKMAI) AND Empty(this.w_CHKMAIAGG)
                        this.w_LPANEMAIL = ALLTRIM(this.w_LPANEMAIL)+";"+ALLTRIM(this.w_EMAILAGG)
                      case Empty(this.w_LPCHKMAI) AND Not Empty(this.w_CHKMAIAGG)
                        this.w_LPANEMAIL = ALLTRIM(this.w_LPANEMAIL)
                      case Not Empty(this.w_LPCHKMAI) AND Empty(this.w_CHKMAIAGG)
                        this.w_LPANEMAIL = ALLTRIM(this.w_EMAILAGG)
                    endcase
                    this.w_LPANEMAIL = this.NormMailList(this.w_LPANEMAIL)
                    * --- Inoltro E-Mail 
                    * Variabili pubbliche per mail 
 i_EMAIL = this.w_LPANEMAIL 
 this.w_OLDSUBJECT = i_EMAILSUBJECT 
 i_EMAILSUBJECT = this.w_VALOGGETTO 
 i_EMAILTEXT = " "+this.w_VALTESTO
                    if this.w_SCHEDULER OR this.w_PDPROCES="IRP"
                      if this.w_PDPROCES="IRP" AND UPPER(this.oParentObject.w_EXT)=".HTM"
                        if !EMPTY(i_WEALLENAME)
                          this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_WEALLENAME)
                        endif
                        this.w_RET = EMAIL(alltrim(this.w_NomeFile)+";"+ALLTRIM(this.oParentObject.w_DTPATH)+"\"+ALLTRIM(this.oParentObject.w_DTFILE)+"_Data"+ALLTRIM(this.oParentObject.w_EXT)+"L", "N", ; 
 .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                      else
                        if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                          this.w_RET = EMAIL(this.w_NOMEFILFIR, "N", .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                        else
                          if this.w_OFALLENAME
                            if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                              if this.w_OFFE_CLASS="TGSOF_BGN"
                                this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                              else
                                this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                              endif
                              * --- Read from MOD_OFFE
                              i_nOldArea=select()
                              if used('_read_')
                                select _read_
                                use
                              endif
                              i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                              if i_nConn<>0
                                cp_sqlexec(i_nConn,"select "+;
                                  "MOALMAIL"+;
                                  " from "+i_cTable+" MOD_OFFE where ";
                                      +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                       ,"_read_")
                                i_Rows=iif(used('_read_'),reccount(),0)
                              else
                                select;
                                  MOALMAIL;
                                  from (i_cTable) where;
                                      MOCODICE = this.w_OFCODMOD;
                                   into cursor _read_
                                i_Rows=_tally
                              endif
                              if used('_read_')
                                locate for 1=1
                                this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                                use
                              else
                                * --- Error: sql sentence error.
                                i_Error = MSG_READ_ERROR
                                return
                              endif
                              select (i_nOldArea)
                              do case
                                case this.w_MOALMAIL="M"
                                  this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                                case this.w_MOALMAIL $ "E-S"
                                  if Not Empty(ALLTRIM(i_OFALLENAME))
                                    this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                  else
                                    this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                  endif
                              endcase
                            else
                              this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                            endif
                          endif
                          this.w_RET = EMAIL(this.w_NomeFile, "N", .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                        endif
                      endif
                    else
                      if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                        this.w_RET = EMAIL(this.w_NOMEFILFIR, g_UEDIAL, .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                      else
                        if this.w_OFALLENAME
                          if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                            if this.w_OFFE_CLASS="TGSOF_BGN"
                              this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                            else
                              this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                            endif
                            * --- Read from MOD_OFFE
                            i_nOldArea=select()
                            if used('_read_')
                              select _read_
                              use
                            endif
                            i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                            if i_nConn<>0
                              cp_sqlexec(i_nConn,"select "+;
                                "MOALMAIL"+;
                                " from "+i_cTable+" MOD_OFFE where ";
                                    +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                     ,"_read_")
                              i_Rows=iif(used('_read_'),reccount(),0)
                            else
                              select;
                                MOALMAIL;
                                from (i_cTable) where;
                                    MOCODICE = this.w_OFCODMOD;
                                 into cursor _read_
                              i_Rows=_tally
                            endif
                            if used('_read_')
                              locate for 1=1
                              this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                              use
                            else
                              * --- Error: sql sentence error.
                              i_Error = MSG_READ_ERROR
                              return
                            endif
                            select (i_nOldArea)
                            do case
                              case this.w_MOALMAIL="M"
                                this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                              case this.w_MOALMAIL $ "E-S"
                                if Not Empty(ALLTRIM(i_OFALLENAME))
                                  this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                else
                                  this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                endif
                            endcase
                          else
                            this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                          endif
                        endif
                        this.w_RET = EMAIL(this.w_NomeFile, g_UEDIAL, .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                      endif
                    endif
                    release g_DOCM_EMAILSN
                    * Variabili pubbliche per mail 
 i_EMAIL = " " 
 i_EMAILSUBJECT = this.w_OLDSUBJECT 
 i_EMAILTEXT = " " 
 
 this.w_LPANEMAIL = w_OLDANEMAIL
                    if at("; ",this.w_NomeFile)>0
                      this.w_NomeFile = ALLTRIM(left(this.w_NomeFile,at("; ",this.w_NomeFile)-1))
                    endif
                    this.w_LPCHKMAI = IIF(Empty(this.w_LPCHKMAI), IIF(this.w_RET, this.w_LPCHKMAI, 13), this.w_LPCHKMAI)
                    this.w_CHKMAIAGG = IIF(Empty(this.w_CHKMAIAGG), IIF(this.w_RET, this.w_CHKMAIAGG, 13), this.w_CHKMAIAGG)
                  endif
                  if Not Empty(this.w_PDCODORN) and this.w_MAILSN="S" and Empty(this.w_CHKMAIXCO)
                    public g_DOCM_EMAILSN 
 g_DOCM_EMAILSN= .t. 
 w_OLDANEMAIL = this.w_LPANEMAIL 
 this.w_LPANEMAIL = this.NormMailList(this.w_EMAILXCO)
                    * --- Inoltro E-Mail 
                    * Variabili pubbliche per mail 
 i_EMAIL = this.w_LPANEMAIL 
 this.w_OLDSUBJECT = i_EMAILSUBJECT 
 i_EMAILSUBJECT = this.w_VALOGGETTO 
 i_EMAILTEXT = " "+this.w_VALTESTO
                    if this.w_SCHEDULER OR this.w_PDPROCES="IRP"
                      if this.w_PDPROCES="IRP" AND UPPER(this.oParentObject.w_EXT)=".HTM"
                        if !EMPTY(i_WEALLENAME)
                          this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_WEALLENAME)
                        endif
                        this.w_RET = EMAIL(alltrim(this.w_NomeFile)+";"+ALLTRIM(this.oParentObject.w_DTPATH)+"\"+ALLTRIM(this.oParentObject.w_DTFILE)+"_Data"+ALLTRIM(this.oParentObject.w_EXT)+"L", "N", ; 
 .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                      else
                        if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                          this.w_RET = EMAIL(this.w_NOMEFILFIR, "N", .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                        else
                          if this.w_OFALLENAME
                            if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                              if this.w_OFFE_CLASS="TGSOF_BGN"
                                this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                              else
                                this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                              endif
                              * --- Read from MOD_OFFE
                              i_nOldArea=select()
                              if used('_read_')
                                select _read_
                                use
                              endif
                              i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                              if i_nConn<>0
                                cp_sqlexec(i_nConn,"select "+;
                                  "MOALMAIL"+;
                                  " from "+i_cTable+" MOD_OFFE where ";
                                      +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                       ,"_read_")
                                i_Rows=iif(used('_read_'),reccount(),0)
                              else
                                select;
                                  MOALMAIL;
                                  from (i_cTable) where;
                                      MOCODICE = this.w_OFCODMOD;
                                   into cursor _read_
                                i_Rows=_tally
                              endif
                              if used('_read_')
                                locate for 1=1
                                this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                                use
                              else
                                * --- Error: sql sentence error.
                                i_Error = MSG_READ_ERROR
                                return
                              endif
                              select (i_nOldArea)
                              do case
                                case this.w_MOALMAIL="M"
                                  this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                                case this.w_MOALMAIL $ "E-S"
                                  if Not Empty(ALLTRIM(i_OFALLENAME))
                                    this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                  else
                                    this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                  endif
                              endcase
                            else
                              this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                            endif
                          endif
                          this.w_RET = EMAIL(this.w_NomeFile, "N", .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                        endif
                      endif
                    else
                      if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                        this.w_RET = EMAIL(this.w_NOMEFILFIR, g_UEDIAL, .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                      else
                        if this.w_OFALLENAME
                          if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                            if this.w_OFFE_CLASS="TGSOF_BGN"
                              this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                            else
                              this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                            endif
                            * --- Read from MOD_OFFE
                            i_nOldArea=select()
                            if used('_read_')
                              select _read_
                              use
                            endif
                            i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                            if i_nConn<>0
                              cp_sqlexec(i_nConn,"select "+;
                                "MOALMAIL"+;
                                " from "+i_cTable+" MOD_OFFE where ";
                                    +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                     ,"_read_")
                              i_Rows=iif(used('_read_'),reccount(),0)
                            else
                              select;
                                MOALMAIL;
                                from (i_cTable) where;
                                    MOCODICE = this.w_OFCODMOD;
                                 into cursor _read_
                              i_Rows=_tally
                            endif
                            if used('_read_')
                              locate for 1=1
                              this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                              use
                            else
                              * --- Error: sql sentence error.
                              i_Error = MSG_READ_ERROR
                              return
                            endif
                            select (i_nOldArea)
                            do case
                              case this.w_MOALMAIL="M"
                                this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                              case this.w_MOALMAIL $ "E-S"
                                if Not Empty(ALLTRIM(i_OFALLENAME))
                                  this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                else
                                  this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                endif
                            endcase
                          else
                            this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                          endif
                        endif
                        this.w_RET = EMAIL(this.w_NomeFile, g_UEDIAL, .F., this.w_LPANEMAIL, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                      endif
                    endif
                    release g_DOCM_EMAILSN
                    * Variabili pubbliche per mail 
 i_EMAIL = " " 
 i_EMAILSUBJECT = this.w_OLDSUBJECT 
 i_EMAILTEXT = " " 
 this.w_LPANEMAIL = w_OLDANEMAIL
                    if at("; ",this.w_NomeFile)>0
                      this.w_NomeFile = ALLTRIM(left(this.w_NomeFile,at("; ",this.w_NomeFile)-1))
                    endif
                    this.w_CHKMAIXCO = IIF(Empty(this.w_CHKMAIXCO), IIF(this.w_RET, this.w_CHKMAIXCO, 13), this.w_CHKMAIXCO)
                  endif
                  * --- CASO PEC <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                  if this.w_PECSN="S"
                    * --- Memorizzo le var. globali della configurazione mail
                    CFGMAIL(i_CODUTE,"PUSH_CFG")
                    if Empty(CFGMAIL(i_CODUTE, "P", "", .T.))
                      * --- Configurazione account PEC non definita per l'utente
                      this.w_LPCHKPEC = IIF(empty(this.w_LPCHKPEC), 54, this.w_LPCHKPEC)
                      this.w_CHKPECAGG = IIF(empty(this.w_CHKPECAGG), 54, this.w_CHKPECAGG)
                      this.w_CHKPECXCO = IIF(empty(this.w_CHKPECXCO), 54, this.w_CHKPECXCO)
                    else
                      cp_SetGlobalVar("i_bPEC",True)
                      if Empty(this.w_LPCHKPEC) OR Empty(this.w_CHKPECAGG)
                        public g_DOCM_EMAILSN 
 g_DOCM_EMAILSN= .t. 
 w_OLDANEMPEC = this.w_LPANEMPEC
                        do case
                          case Empty(this.w_LPCHKPEC) AND Empty(this.w_CHKPECAGG)
                            this.w_LPANEMPEC = ALLTRIM(this.w_LPANEMPEC)+";"+ALLTRIM(this.w_EMPECAGG)
                          case Empty(this.w_LPCHKPEC) AND Not Empty(this.w_CHKPECAGG)
                            this.w_LPANEMPEC = ALLTRIM(this.w_LPANEMPEC)
                          case Not Empty(this.w_LPCHKPEC) AND Empty(this.w_CHKPECAGG)
                            this.w_LPANEMPEC = ALLTRIM(this.w_EMPECAGG)
                        endcase
                        this.w_LPANEMPEC = this.NormMailList(this.w_LPANEMPEC)
                        * --- Inoltro E-Mail 
                        * Variabili pubbliche per mail 
 i_EMAIL_PEC = this.w_LPANEMPEC 
 this.w_OLDSUBJECT = i_EMAILSUBJECT 
 i_EMAILSUBJECT = this.w_VALOGGETTO 
 i_EMAILTEXT = " "+this.w_VALTESTO
                        if this.w_SCHEDULER OR this.w_PDPROCES="IRP"
                          if this.w_PDPROCES="IRP" AND UPPER(this.oParentObject.w_EXT)=".HTM"
                            if !EMPTY(i_WEALLENAME)
                              this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_WEALLENAME)
                            endif
                            this.w_RET = EMAIL(alltrim(this.w_NomeFile)+";"+ALLTRIM(this.oParentObject.w_DTPATH)+"\"+ALLTRIM(this.oParentObject.w_DTFILE)+"_Data"+ALLTRIM(this.oParentObject.w_EXT)+"L", "N", ; 
 .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                          else
                            if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                              this.w_RET = EMAIL(this.w_NOMEFILFIR, "N", .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                            else
                              if this.w_OFALLENAME
                                if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                                  if this.w_OFFE_CLASS="TGSOF_BGN"
                                    this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                                  else
                                    this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                                  endif
                                  * --- Read from MOD_OFFE
                                  i_nOldArea=select()
                                  if used('_read_')
                                    select _read_
                                    use
                                  endif
                                  i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                                  i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                                  if i_nConn<>0
                                    cp_sqlexec(i_nConn,"select "+;
                                      "MOALMAIL"+;
                                      " from "+i_cTable+" MOD_OFFE where ";
                                          +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                           ,"_read_")
                                    i_Rows=iif(used('_read_'),reccount(),0)
                                  else
                                    select;
                                      MOALMAIL;
                                      from (i_cTable) where;
                                          MOCODICE = this.w_OFCODMOD;
                                       into cursor _read_
                                    i_Rows=_tally
                                  endif
                                  if used('_read_')
                                    locate for 1=1
                                    this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                                    use
                                  else
                                    * --- Error: sql sentence error.
                                    i_Error = MSG_READ_ERROR
                                    return
                                  endif
                                  select (i_nOldArea)
                                  do case
                                    case this.w_MOALMAIL="M"
                                      this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                                    case this.w_MOALMAIL $ "E-S"
                                      if Not Empty(ALLTRIM(i_OFALLENAME))
                                        this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                      else
                                        this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                      endif
                                  endcase
                                else
                                  this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                endif
                              endif
                              this.w_RET = EMAIL(this.w_NomeFile, "N", .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                            endif
                          endif
                        else
                          if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                            this.w_RET = EMAIL(this.w_NOMEFILFIR, g_UEDIAL, .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                          else
                            if this.w_OFALLENAME
                              if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                                if this.w_OFFE_CLASS="TGSOF_BGN"
                                  this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                                else
                                  this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                                endif
                                * --- Read from MOD_OFFE
                                i_nOldArea=select()
                                if used('_read_')
                                  select _read_
                                  use
                                endif
                                i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                                if i_nConn<>0
                                  cp_sqlexec(i_nConn,"select "+;
                                    "MOALMAIL"+;
                                    " from "+i_cTable+" MOD_OFFE where ";
                                        +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                         ,"_read_")
                                  i_Rows=iif(used('_read_'),reccount(),0)
                                else
                                  select;
                                    MOALMAIL;
                                    from (i_cTable) where;
                                        MOCODICE = this.w_OFCODMOD;
                                     into cursor _read_
                                  i_Rows=_tally
                                endif
                                if used('_read_')
                                  locate for 1=1
                                  this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                                  use
                                else
                                  * --- Error: sql sentence error.
                                  i_Error = MSG_READ_ERROR
                                  return
                                endif
                                select (i_nOldArea)
                                do case
                                  case this.w_MOALMAIL="M"
                                    this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                                  case this.w_MOALMAIL $ "E-S"
                                    if Not Empty(ALLTRIM(i_OFALLENAME))
                                      this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                    else
                                      this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                    endif
                                endcase
                              else
                                this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                              endif
                            endif
                            this.w_RET = EMAIL(this.w_NomeFile, g_UEDIAL, .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                          endif
                        endif
                        release g_DOCM_EMAILSN
                        * Variabili pubbliche per mail 
 i_EMAIL_PEC = " " 
 i_EMAILSUBJECT = this.w_OLDSUBJECT 
 i_EMAILTEXT = " " 
 
 this.w_LPANEMPEC = w_OLDANEMPEC
                        if at("; ",this.w_NomeFile)>0
                          this.w_NomeFile = ALLTRIM(left(this.w_NomeFile,at("; ",this.w_NomeFile)-1))
                        endif
                        this.w_LPCHKPEC = IIF(Empty(this.w_LPCHKPEC), IIF(this.w_RET, this.w_LPCHKPEC, 13), this.w_LPCHKPEC)
                        this.w_CHKPECAGG = IIF(Empty(this.w_CHKPECAGG), IIF(this.w_RET, this.w_CHKPECAGG, 13), this.w_CHKPECAGG)
                      endif
                      if Not Empty(this.w_PDCODORN) and Empty(this.w_CHKPECXCO)
                        public g_DOCM_EMAILSN 
 g_DOCM_EMAILSN= .t. 
 w_OLDANEMPEC = this.w_LPANEMPEC 
 this.w_LPANEMPEC = this.NormMailList(this.w_EMPECXCO)
                        * --- Inoltro E-Mail 
                        * Variabili pubbliche per mail 
 i_EMAIL_PEC = this.w_LPANEMPEC 
 this.w_OLDSUBJECT = i_EMAILSUBJECT 
 i_EMAILSUBJECT = this.w_VALOGGETTO 
 i_EMAILTEXT = " "+this.w_VALTESTO
                        if this.w_SCHEDULER OR this.w_PDPROCES="IRP"
                          if this.w_PDPROCES="IRP" AND UPPER(this.oParentObject.w_EXT)=".HTM"
                            if !EMPTY(i_WEALLENAME)
                              this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_WEALLENAME)
                            endif
                            this.w_RET = EMAIL(alltrim(this.w_NomeFile)+";"+ALLTRIM(this.oParentObject.w_DTPATH)+"\"+ALLTRIM(this.oParentObject.w_DTFILE)+"_Data"+ALLTRIM(this.oParentObject.w_EXT)+"L", "N", ; 
 .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                          else
                            if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                              this.w_RET = EMAIL(this.w_NOMEFILFIR, "N", .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                            else
                              if this.w_OFALLENAME
                                if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                                  if this.w_OFFE_CLASS="TGSOF_BGN"
                                    this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                                  else
                                    this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                                  endif
                                  * --- Read from MOD_OFFE
                                  i_nOldArea=select()
                                  if used('_read_')
                                    select _read_
                                    use
                                  endif
                                  i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                                  i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                                  if i_nConn<>0
                                    cp_sqlexec(i_nConn,"select "+;
                                      "MOALMAIL"+;
                                      " from "+i_cTable+" MOD_OFFE where ";
                                          +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                           ,"_read_")
                                    i_Rows=iif(used('_read_'),reccount(),0)
                                  else
                                    select;
                                      MOALMAIL;
                                      from (i_cTable) where;
                                          MOCODICE = this.w_OFCODMOD;
                                       into cursor _read_
                                    i_Rows=_tally
                                  endif
                                  if used('_read_')
                                    locate for 1=1
                                    this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                                    use
                                  else
                                    * --- Error: sql sentence error.
                                    i_Error = MSG_READ_ERROR
                                    return
                                  endif
                                  select (i_nOldArea)
                                  do case
                                    case this.w_MOALMAIL="M"
                                      this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                                    case this.w_MOALMAIL $ "E-S"
                                      if Not Empty(ALLTRIM(i_OFALLENAME))
                                        this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                      else
                                        this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                      endif
                                  endcase
                                else
                                  this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                endif
                              endif
                              this.w_RET = EMAIL(this.w_NomeFile, "N", .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                            endif
                          endif
                        else
                          if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR)
                            this.w_RET = EMAIL(this.w_NOMEFILFIR, g_UEDIAL, .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                          else
                            if this.w_OFALLENAME
                              if G_OFFE="S" AND INLIST(this.w_OFFE_CLASS, "TGSOF_AOF", "TGSOF_KRO", "TGSOF_BGN")
                                if this.w_OFFE_CLASS="TGSOF_BGN"
                                  this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                                else
                                  this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                                endif
                                * --- Read from MOD_OFFE
                                i_nOldArea=select()
                                if used('_read_')
                                  select _read_
                                  use
                                endif
                                i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                                i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                                if i_nConn<>0
                                  cp_sqlexec(i_nConn,"select "+;
                                    "MOALMAIL"+;
                                    " from "+i_cTable+" MOD_OFFE where ";
                                        +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                                         ,"_read_")
                                  i_Rows=iif(used('_read_'),reccount(),0)
                                else
                                  select;
                                    MOALMAIL;
                                    from (i_cTable) where;
                                        MOCODICE = this.w_OFCODMOD;
                                     into cursor _read_
                                  i_Rows=_tally
                                endif
                                if used('_read_')
                                  locate for 1=1
                                  this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                                  use
                                else
                                  * --- Error: sql sentence error.
                                  i_Error = MSG_READ_ERROR
                                  return
                                endif
                                select (i_nOldArea)
                                do case
                                  case this.w_MOALMAIL="M"
                                    this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                                  case this.w_MOALMAIL $ "E-S"
                                    if Not Empty(ALLTRIM(i_OFALLENAME))
                                      this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                                    else
                                      this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                                    endif
                                endcase
                              else
                                this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                              endif
                            endif
                            this.w_RET = EMAIL(this.w_NomeFile, g_UEDIAL, .F., this.w_LPANEMPEC, evl(this.w_VALOGGETTO,.F.), EVL(" "+this.w_VALTESTO,.F.))
                          endif
                        endif
                        release g_DOCM_EMAILSN
                        * Variabili pubbliche per mail 
 i_EMAIL_PEC = " " 
 i_EMAILSUBJECT = this.w_OLDSUBJECT 
 i_EMAILTEXT = " " 
 this.w_LPANEMPEC = w_OLDANEMPEC
                        if at("; ",this.w_NomeFile)>0
                          this.w_NomeFile = ALLTRIM(left(this.w_NomeFile,at("; ",this.w_NomeFile)-1))
                        endif
                        this.w_CHKPECXCO = IIF(Empty(this.w_CHKPECXCO), IIF(this.w_RET, this.w_CHKPECXCO, 13), this.w_CHKPECXCO)
                      endif
                    endif
                    * --- ripristino le var. globali
                    CFGMAIL(i_CODUTE,"POP_CFG")
                  endif
                endif
                * --- Progress Bar
                this.w_NREC = this.w_NREC+1
                if TYPE("this.w_OPADRE.oPDPrgBar")="O"
                  if NOT UPPER(this.oParentObject.Class)="TGSIR_BGD" 
                    this.w_OPBAR.Value = IIF(this.w_NRECTOT>0, int (this.w_NREC/this.w_NRECTOT*this.w_nDelta), this.w_nDelta) + (this.w_nDelta*this.w_CounterRep) 
                  else
                    this.w_OPBAR.Value = IIF(w_NUMERO>0, int (w_NUMERO/this.w_NRECTOT*this.w_nDelta), this.w_nDelta) + (this.w_nDelta*this.w_CounterRep) 
                  endif
                endif
                * --- CASO Fax (Prepara i dati per creazione descrittore) <<<<<<<<<<<<<<
                if this.w_FAXSN="S" and ((g_TIPFAX<>"M" And g_TIPFAX<>"E") or this.w_OKALLEGA)
                  if Empty(this.w_LPCHKFAX)
                    * Variabili pubbliche per Fax 
 i_FAXNO = ALLTRIM(this.w_LPTELFAX) 
 i_DEST = ALLTRIM(this.w_LPRAGSOC) 
 this.w_OLDSUBJECT = i_FAXSUBJECT 
 i_FAXSUBJECT = " Fax "+ALLTRIM(this.w_VALOGGETTO)
                    this.w_LPCHKFAX = this.Inoltro_FAX()
                    * Variabili pubbliche per Fax 
 i_FAXNO = " " 
 i_DEST = " " 
 i_FAXSUBJECT = this.w_OLDSUBJECT
                  endif
                  if this.w_DESTAGG and Empty(this.w_CHKFAXAGG)
                    this.w_IDXADD = 2
                    do while this.w_IDXADD<=this.w_NUMDESTAG
                      this.w_CHKFAXAGG = fax_res[this.w_IDXADD,1]
                      this.w_TELFAXAGG = fax_res[this.w_IDXADD,2]
                      this.w_RAGSOCAGG = fax_res[this.w_IDXADD,3]
                      if Empty(this.w_CHKFAXAGG)
                        * Variabili pubbliche per Fax 
 i_FAXNO = ALLTRIM(this.w_TELFAXAGG) 
 i_DEST = ALLTRIM(this.w_RAGSOCAGG) 
 this.w_OLDSUBJECT = i_FAXSUBJECT 
 i_FAXSUBJECT = " Fax "+ALLTRIM(this.w_VALOGGETTO)
                        this.w_CHKFAXAGG = this.Inoltro_FAX()
                        * Variabili pubbliche per Fax 
 i_FAXNO = " " 
 i_DEST = " " 
 i_FAXSUBJECT = this.w_OLDSUBJECT
                        * --- aggiorno il vettore con le risposte
                        fax_res[this.w_IDXADD,1] = this.w_CHKFAXAGG
                      endif
                      this.w_IDXADD = this.w_IDXADD+1
                    enddo
                  endif
                  if Not Empty(this.w_PDCODORN) And Empty(this.w_CHKFAXXCO)
                    * Variabili pubbliche per Fax 
 i_FAXNO = ALLTRIM(this.w_TELFAXXCO) 
 i_DEST = ALLTRIM(this.w_RAGSOCXCO) 
 this.w_OLDSUBJECT = i_FAXSUBJECT 
 i_FAXSUBJECT = " Fax "+ALLTRIM(this.w_VALOGGETTO)
                    this.w_CHKFAXXCO = this.Inoltro_FAX()
                    * Variabili pubbliche per Fax 
 i_FAXNO = " " 
 i_DEST = " " 
 i_FAXSUBJECT = this.w_OLDSUBJECT
                  endif
                endif
                * --- CASO WE (Prepara i dati per creazione descrittore) <<<<<<<<<<<<<<
                if this.w_WESN="S" and empty(this.w_LPCHKWWP) and this.w_OKALLEGA
                  this.w_CALLEGA = this.w_CALLEGA + 1
                  Dimension L_ARRAYDOC(this.w_CALLEGA,10)
                  L_ARRAYDOC(this.w_CALLEGA,1) = alltrim(this.w_LPWELOGI)
                  L_ARRAYDOC(this.w_CALLEGA,2) = alltrim(substr(this.w_NOMEFILE, rat("\",this.w_NOMEFILE)+1 ) )
                  L_ARRAYDOC(this.w_CALLEGA,3) = alltrim(this.w_VALOGGETTO)
                  L_ARRAYDOC(this.w_CALLEGA,4) = "4"
                  L_ARRAYDOC(this.w_CALLEGA,5) = alltrim(this.w_VALTESTO)
                  L_ARRAYDOC(this.w_CALLEGA,6) = IIF(this.w_LPWEMAIL="S","SI","NO")
                  L_ARRAYDOC(this.w_CALLEGA,7) = rtrim(this.w_LPWETXMA)
                  L_ARRAYDOC(this.w_CALLEGA,8) = IIF(this.w_LPWE_SMS="S","SI","NO")
                  L_ARRAYDOC(this.w_CALLEGA,9) = rtrim(this.w_LPWETSMS)
                  L_ARRAYDOC(this.w_CALLEGA,10) = IIF(this.w_LPWE_FAX="S","SI","NO")
                  * --- Array con elenco nome allegati
                  dimension L_AllegatiWE(this.w_CALLEGA+1)
                  L_AllegatiWE(this.w_CALLEGA+1) = this.w_NOMEFILE
                  * --- Array con coordinate documento x aggioirnamento log e indici
                  Dimension L_ARRAYKEYWWP(this.w_CALLEGA,3)
                  L_ARRAYKEYWWP(this.w_CALLEGA,1) = this.w_VALKEYROTT
                  L_ARRAYKEYWWP(this.w_CALLEGA,2) = this.w_COUNTER
                  L_ARRAYKEYWWP(this.w_CALLEGA,3) = this.w_oESEWWP
                endif
                * --- CASO invio a CORPORATE PORTAL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                *     se integrazione infinity easy lancio pubblicazione solo se proviene da IRP
                if g_DMIP<>"S" AND this.w_CDMODALL<>"S" AND (g_IZCP$"SA" and this.w_CPZSN="S" or g_CPIN="S") and empty(this.w_LPCHKCPZ) and this.w_OKALLEGA
                  if g_CPIN<>"S" 
                    * --- Invio documenti a CORPORATE PORTAL ZUCCHETTI
                    * --- Leggo i parametri impostati e valorizzo le variabili associate
                    * --- Select from PRO_FCPZ
                    i_nConn=i_TableProp[this.PRO_FCPZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRO_FCPZ_idx,2],.t.,this.PRO_FCPZ_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select PFNOMVAR,PFVARVAL  from "+i_cTable+" PRO_FCPZ ";
                          +" where PFCODICE="+cp_ToStrODBC(this.w_CODPROC)+"";
                           ,"_Curs_PRO_FCPZ")
                    else
                      select PFNOMVAR,PFVARVAL from (i_cTable);
                       where PFCODICE=this.w_CODPROC;
                        into cursor _Curs_PRO_FCPZ
                    endif
                    if used('_Curs_PRO_FCPZ')
                      select _Curs_PRO_FCPZ
                      locate for 1=1
                      do while not(eof())
                      L_MAC = ALLTRIM(_Curs_PRO_FCPZ.PFNOMVAR)+" = "+ALLTRIM(_Curs_PRO_FCPZ.PFVARVAL)
                      select (w_CURSORE)
                      &L_MAC
                        select _Curs_PRO_FCPZ
                        continue
                      enddo
                      use
                    endif
                    select (w_CURSORE)
                    * --- Ripristino 'w_PFOLDER' al valore originario, per poter rieseguire la corretta trascodifica
                    this.w_PFOLDER = this.w_PFOLDER_A
                    * --- Trascodifico i parametri
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%LOCATION%",g_ZCPLOCATION)
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%YEAR%",g_ZCPYEAR)
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%MONTH%",g_ZCPMONTH)
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%DOC_TYPE%",g_ZCPDOCTYPE)
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%CUSTOMER%",g_ZCPCUSTOMER)
                    * --- Verifico se l'agente ha associato un fornitore e se quest'ultimo  ha attivo il check CPZ
                    if ! empty(NVL(g_ZCPAGENT,""))
                      * --- Read from AGENTI
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.AGENTI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "AGCHKCPZ,AGCODFOR"+;
                          " from "+i_cTable+" AGENTI where ";
                              +"AGCODAGE = "+cp_ToStrODBC(g_ZCPAGENT);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          AGCHKCPZ,AGCODFOR;
                          from (i_cTable) where;
                              AGCODAGE = g_ZCPAGENT;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_FLOK = NVL(cp_ToDate(_read_.AGCHKCPZ),cp_NullValue(_read_.AGCHKCPZ))
                        this.w_CODCONAG = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_FLOK = "S"
                        if ! empty(this.w_CODCONAG)
                          * --- Read from CONTI
                          i_nOldArea=select()
                          if used('_read_')
                            select _read_
                            use
                          endif
                          i_nConn=i_TableProp[this.CONTI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select "+;
                              "ANCHKCPZ"+;
                              " from "+i_cTable+" CONTI where ";
                                  +"ANTIPCON = "+cp_ToStrODBC("F");
                                  +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCONAG);
                                   ,"_read_")
                            i_Rows=iif(used('_read_'),reccount(),0)
                          else
                            select;
                              ANCHKCPZ;
                              from (i_cTable) where;
                                  ANTIPCON = "F";
                                  and ANCODICE = this.w_CODCONAG;
                               into cursor _read_
                            i_Rows=_tally
                          endif
                          if used('_read_')
                            locate for 1=1
                            this.w_FLOK = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
                            use
                          else
                            * --- Error: sql sentence error.
                            i_Error = MSG_READ_ERROR
                            return
                          endif
                          select (i_nOldArea)
                          if this.w_FLOK = "S"
                            * --- Sostituisco il codice dell'agente con quello del fornitore associato
                            g_ZCPAGENT = alltrim(this.w_CODCONAG)
                          endif
                        endif
                      else
                        g_ZCPAGENT = ""
                      endif
                    endif
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%AGENT%",g_ZCPAGENT)
                    * --- Elimino eventuali parametri non valorizzati
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%LOCATION%","")
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%YEAR%","")
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%MONTH%","")
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%DOC_TYPE%","")
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%CUSTOMER%","")
                    this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"%AGENT%","")
                    * --- Elimino eventuali doppie barre derivanti da eliminazione parametri non valorizzati
                    do while "//" $ this.w_PFOLDER
                      this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"//","/")
                    enddo
                    do while "\\" $ this.w_PFOLDER
                      this.w_PFOLDER = STRTRAN(this.w_PFOLDER,"\\","\")
                    enddo
                  endif
                  * --- Deve copiare il documento w_NOMEFILE nella cartella di transito per i documenti diretti al portale
                  if this.w_PDPROCES="IRP" AND UPPER(this.oParentObject.w_EXT)=".HTM"
                    this.w_NOMEFILE_CPZ = this.w_NOMEFILE
                    this.w_NOMEFILE = LEFT(ALLTRIM(this.w_NOMEFILE),LEN(ALLTRIM(this.w_NOMEFILE))-5)+"_Data.html"
                  endif
                  if file(this.w_NOMEFILE)
                    * --- Isola il nome del file ... cos� lo sposta ...
                    this.w_ESTENSIONE_CPZ = JUSTEXT(this.w_NOMEFILE)
                    this.w_TMPC = JUSTFNAME(this.w_NOMEFILE)
                    if g_CPIN<>"S"
                      this.w_DESTFILE = FORCEEXT(this.w_COPATHDO+alltrim(i_CODAZI)+"_"+alltrim(str(i_CODUTE,5,0))+"_"+SYS(2015),this.w_ESTENSIONE_CPZ)
                    else
                      * --- Memorizza stato ONERROR
                      this.w_OLDONERROR = ON("ERROR")
                      i_pdError = 0
                      ON ERROR i_pdError = 72
                      select (w_CURSORE)
                      this.w_TMPC = Eval(this.w_NOMEFILEIN)
                      this.w_DESTFILE = FORCEEXT(this.w_COPATHDO + this.w_TMPC+".", this.w_ESTENSIONE_CPZ)
                      * --- Controllo se sto archiviando un archivio di file in formato grafico e in caso affermativo inserisco l'estensione nel nome
                      this.w_EXTGRF = this.check_grafico(this.w_NOMEFILE)
                      if LOWER(JUSTEXT(this.w_NOMEFILE))="zip" AND not EMPTY(this.w_EXTGRF)
                        * --- Controllo che il nome non contenga gi� l'indicazione dell'estensione dei file grafici
                        if EMPTY(this.check_grafico(this.w_DESTFILE))
                          this.w_DESTFILE = FORCEEXT(ADDBS(JUSTPATH(this.w_DESTFILE))+JUSTSTEM(this.w_DESTFILE)+this.w_EXTGRF, JUSTEXT(this.w_DESTFILE))
                        endif
                      endif
                      * --- Ripristina ONERROR
                      if empty(this.w_OLDONERROR)
                        ON ERROR 
                      else
                        lOldOnError = this.w_OLDONERROR
                        ON ERROR &lOldOnError
                      endif
                      * --- Assegna eventuale errore
                      this.w_LPCHKCPZ = IIF(empty(this.w_LPCHKCPZ), i_pdError ,this.w_LPCHKCPZ)
                    endif
                    * --- Se esiste la cartella e non c'� ancora il file ... esegue
                    * --- Se non c'� crea la cartella ...
                    if Not(this.AH_ExistFolder(this.w_COPATHDO))
                      if Not(this.AH_CreateFolder(this.w_COPATHDO))
                        this.w_LPCHKCPZ = 75
                      endif
                    endif
                    * --- Trattamento errori
                    i_pdError=0
                    this.w_OLDONERROR = ON("ERROR")
                    ON ERROR i_pdError=79
                    * --- Eventuale cancellazione
                    if FILE(this.w_DESTFILE)
                      ERASE (this.w_DESTFILE)
                    endif
                    * --- Copia
                    if this.w_FIRMRICH = "S" and empty(this.w_LPCHKFIR) and ! this.w_PDPROCES="IRP" 
                      COPY FILE (this.w_NOMEFILFIR) TO (this.w_DESTFILE)
                    else
                      COPY FILE (this.w_NOMEFILE) TO (this.w_DESTFILE)
                    endif
                    if empty(this.w_OLDONERROR)
                      ON ERROR 
                    else
                      lOldOnError = this.w_OLDONERROR
                      ON ERROR &lOldOnError
                    endif
                    * --- Assegna eventuale errore
                    this.w_LPCHKCPZ = IIF(empty(this.w_LPCHKCPZ), i_pdError ,this.w_LPCHKCPZ)
                  endif
                  * --- Deve inserire il record per segnalare la presenza del file da inviare ...
                  if this.w_LPCHKCPZ=0 and FILE(this.w_DESTFILE)
                    if g_CPIN<>"S"
                      * --- Invia a ZCP
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    else
                      * --- Infinity - Se non archivio creo qui l'indice altrimenti ho gi� fatto
                      SERIALE= "   "
                      if !this.w_APPWEBARC
                        this.w_INARCHIVIO = " "
                        if ";"$this.w_DESTFILE
                          this.w_DESTFILE = this.create_zip(this.w_DESTFILE)
                        endif
                        Private L_ArrParam
                        dimension L_ArrParam(30)
                        L_ArrParam(1)="AR"
                        L_ArrParam(2)=this.w_DESTFILE
                        L_ArrParam(3)=this.w_PDCLADOC
                        L_ArrParam(4)=i_CODUTE
                        L_ArrParam(5)=w_CURSORE
                        L_ArrParam(6)=""
                        L_ArrParam(7)=""
                        L_ArrParam(8)=this.w_VALOGGETTO
                        L_ArrParam(9)=this.w_VALTESTO
                        L_ArrParam(10)=.T.
                        L_ArrParam(11)=.T.
                        L_ArrParam(12)=" "
                        L_ArrParam(13)=" "
                        L_ArrParam(14)=.null.
                        L_ArrParam(15)=" "
                        L_ArrParam(16)=" "
                        L_ArrParam(17)=""
                        L_ArrParam(18)=" "
                        L_ArrParam(19)=""
                        L_ArrParam(21)=.F.
                        L_ArrParam(22)=" "
                        L_ArrParam(23)=.T.
                        this.w_LPCHKCPZ = GSUT_BBA(this, @L_ArrParam, @SERIALE)
                        if ! empty(SERIALE)
                          * --- Procedo alla cancellazione degli indici gi� presenti facenti riferimento alla stesso file allegato. Questa situazione si verifica solo se ho modificato le attivit� pianificate nel processo
                          *     documentale, in particolare 'Web application' e 'Archiviazione' (Standard).
                          this.w_FILESEARCH = JUSTFNAME(this.w_DESTFILE)
                          this.w_BPDSERIAL = SERIALE
                          * --- Select from PROMINDI
                          i_nConn=i_TableProp[this.PROMINDI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select IDSERIAL,IDWEBFIL  from "+i_cTable+" PROMINDI ";
                                +" where IDSERIAL <> "+cp_ToStrODBC(this.w_BPDSERIAL)+" AND   IDORIGIN = "+cp_ToStrODBC(this.w_FILESEARCH)+"";
                                 ,"_Curs_PROMINDI")
                          else
                            select IDSERIAL,IDWEBFIL from (i_cTable);
                             where IDSERIAL <> this.w_BPDSERIAL AND   IDORIGIN = this.w_FILESEARCH;
                              into cursor _Curs_PROMINDI
                          endif
                          if used('_Curs_PROMINDI')
                            select _Curs_PROMINDI
                            locate for 1=1
                            do while not(eof())
                            this.w_FILESEARCH = alltrim(_Curs_PROMINDI.IDWEBFIL)
                            w_ERRORE = .F.
                            w_ErrorHandler = on("ERROR")
                            on error w_ERRORE = .T.
                            delete file (alltrim(this.w_COPATHDO)+this.w_FILESEARCH)
                            on error &w_ErrorHandler
                            * --- Delete from PRODINDI
                            i_nConn=i_TableProp[this.PRODINDI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                    +"IDSERIAL = "+cp_ToStrODBC(_Curs_PROMINDI.IDSERIAL);
                                     )
                            else
                              delete from (i_cTable) where;
                                    IDSERIAL = _Curs_PROMINDI.IDSERIAL;

                              i_Rows=_tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              * --- Error: delete not accepted
                              i_Error=MSG_DELETE_ERROR
                              return
                            endif
                            * --- Delete from PROMINDI
                            i_nConn=i_TableProp[this.PROMINDI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                    +"IDSERIAL = "+cp_ToStrODBC(_Curs_PROMINDI.IDSERIAL);
                                     )
                            else
                              delete from (i_cTable) where;
                                    IDSERIAL = _Curs_PROMINDI.IDSERIAL;

                              i_Rows=_tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              * --- Error: delete not accepted
                              i_Error=MSG_DELETE_ERROR
                              return
                            endif
                              select _Curs_PROMINDI
                              continue
                            enddo
                            use
                          endif
                        endif
                      else
                        * --- Aggiorno l'indice
                        * --- w_BPDSERIAL valorizzato da GSUT_BBA
                        this.w_FILESEARCH = JUSTFNAME(this.w_DESTFILE)
                        if Not Empty(this.w_BPDSERIAL)
                          w_ERRORE = .F.
                          w_ErrorHandler = on("ERROR")
                          on error w_ERRORE = .T.
                          if cp_fileexist(alltrim(this.w_COPATHDO)+this.w_FILESEARCH)
                            if cp_fileexist(alltrim(this.w_COPATHDO)+alltrim(this.w_BPDSERIAL)+"_"+this.w_FILESEARCH)
                              delete file (alltrim(this.w_COPATHDO)+alltrim(this.w_BPDSERIAL)+"_"+this.w_FILESEARCH)
                            endif
                            rename (alltrim(this.w_COPATHDO)+this.w_FILESEARCH) TO (alltrim(this.w_COPATHDO)+alltrim(this.w_BPDSERIAL)+"_"+this.w_FILESEARCH)
                          endif
                          on error &w_ErrorHandler
                          if NOT w_ERRORE
                            this.w_FILESEARCH = alltrim(this.w_BPDSERIAL)+"_"+JUSTFNAME(this.w_DESTFILE)
                          endif
                          * --- Write into PROMINDI
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.PROMINDI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.w_FILESEARCH),'PROMINDI','IDWEBFIL');
                                +i_ccchkf ;
                            +" where ";
                                +"IDSERIAL = "+cp_ToStrODBC(this.w_BPDSERIAL);
                                   )
                          else
                            update (i_cTable) set;
                                IDWEBFIL = this.w_FILESEARCH;
                                &i_ccchkf. ;
                             where;
                                IDSERIAL = this.w_BPDSERIAL;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                          * --- Procedo alla cancellazione degli indici gi� presenti facenti riferimento alla stesso file allegato. Questa situazione si verifica solo se ho modificato le attivit� pianificate nel processo
                          *     documentale, in particolare 'Web application' e 'Archiviazione' (Standard).
                          this.w_FILESEARCH = JUSTFNAME(this.w_NOMEFILE)
                          * --- Select from PROMINDI
                          i_nConn=i_TableProp[this.PROMINDI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select IDSERIAL,IDWEBFIL  from "+i_cTable+" PROMINDI ";
                                +" where IDSERIAL <> "+cp_ToStrODBC(this.w_BPDSERIAL)+" AND   IDORIGIN = "+cp_ToStrODBC(this.w_FILESEARCH)+"";
                                 ,"_Curs_PROMINDI")
                          else
                            select IDSERIAL,IDWEBFIL from (i_cTable);
                             where IDSERIAL <> this.w_BPDSERIAL AND   IDORIGIN = this.w_FILESEARCH;
                              into cursor _Curs_PROMINDI
                          endif
                          if used('_Curs_PROMINDI')
                            select _Curs_PROMINDI
                            locate for 1=1
                            do while not(eof())
                            this.w_FILESEARCH = alltrim(_Curs_PROMINDI.IDWEBFIL)
                            w_ERRORE = .F.
                            w_ErrorHandler = on("ERROR")
                            on error w_ERRORE = .T.
                            delete file (alltrim(this.w_COPATHDO)+this.w_FILESEARCH)
                            on error &w_ErrorHandler
                            * --- Delete from PRODINDI
                            i_nConn=i_TableProp[this.PRODINDI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                    +"IDSERIAL = "+cp_ToStrODBC(_Curs_PROMINDI.IDSERIAL);
                                     )
                            else
                              delete from (i_cTable) where;
                                    IDSERIAL = _Curs_PROMINDI.IDSERIAL;

                              i_Rows=_tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              * --- Error: delete not accepted
                              i_Error=MSG_DELETE_ERROR
                              return
                            endif
                            * --- Delete from PROMINDI
                            i_nConn=i_TableProp[this.PROMINDI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            if i_nConn<>0
                              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                    +"IDSERIAL = "+cp_ToStrODBC(_Curs_PROMINDI.IDSERIAL);
                                     )
                            else
                              delete from (i_cTable) where;
                                    IDSERIAL = _Curs_PROMINDI.IDSERIAL;

                              i_Rows=_tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              * --- Error: delete not accepted
                              i_Error=MSG_DELETE_ERROR
                              return
                            endif
                              select _Curs_PROMINDI
                              continue
                            enddo
                            use
                          endif
                        endif
                      endif
                    endif
                  endif
                  if this.w_PDPROCES="IRP" AND UPPER(this.oParentObject.w_EXT)=".HTM"
                    this.w_NOMEFILE = this.w_NOMEFILE_CPZ
                  endif
                endif
                * --- Verifica per PostaLite <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                if this.w_POSTELSN="S" and empty(this.w_LPCHKPTL)
                  * --- Registra il flag di postalite
                  this.w_TMPC = nvl(&w_CURSORE..__PD_FPL__, "  ")
                  this.w_LPCHKPTL = int(val(this.w_TMPC))
                endif
                if this.w_LPCHKPTL=0
                  this.w_CONTAPTL = this.w_CONTAPTL+1
                  Dimension L_ARRAYKEYPLITE(this.w_CONTAPTL,3)
                  L_ARRAYKEYPLITE(this.w_CONTAPTL,1) = this.w_VALKEYROTT
                  L_ARRAYKEYPLITE(this.w_CONTAPTL,2) = this.w_COUNTER
                  L_ARRAYKEYPLITE(this.w_CONTAPTL,3) = this.w_oESEPTL
                endif
                * --- Memorizza Chiave di rottura x stampa
                * --- Veririfca x stampa
                if empty(this.w_LPCHKSTA)
                  * --- Registra, su cursore di stampa, il flag di stampa
                  this.w_TMPC = nvl(&w_CURSORE..__PD_FST__, " ")
                  this.w_LPCHKSTA = iif(this.w_TMPC="R",10,IIF(this.w_TMPC="P",51,0))
                endif
                * --- Memorizza Chiave di rottura x stampa
                if this.w_LPCHKSTA=0
                  this.w_CONTADOC = this.w_CONTADOC+1
                  Dimension L_ARRAYKEYROTT(this.w_CONTADOC,3)
                  L_ARRAYKEYROTT(this.w_CONTADOC,1) = this.w_VALKEYROTT
                  L_ARRAYKEYROTT(this.w_CONTADOC,2) = this.w_COUNTER
                  L_ARRAYKEYROTT(this.w_CONTADOC,3) = this.w_oESESTA
                endif
                * --- Aggiorna Tabella Indici Documenti <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                this.w_IDESEMAI = IIF(this.w_oESEMAI="S", "S", IIF(EMPTY(this.w_LPCHKMAI) or this.w_LPCHKMAI=10,"S","N"))
                this.w_IDESEMAI = IIF(this.w_IDESEMAI="S", "S", IIF(this.w_CHKMAIAGG=0 or this.w_CHKMAIAGG=10,"S","N"))
                this.w_IDESEMAI = IIF(this.w_IDESEMAI="S", "S", IIF(this.w_CHKMAIXCO=0 or this.w_CHKMAIXCO=10,"S","N"))
                this.w_IDESEPEC = IIF(this.w_oESEPEC="S", "S", IIF(EMPTY(this.w_LPCHKPEC) or this.w_LPCHKPEC=10,"S","N"))
                this.w_IDESEPEC = IIF(this.w_IDESEPEC="S", "S", IIF(this.w_CHKPECAGG=0 or this.w_CHKPECAGG=10,"S","N"))
                this.w_IDESEPEC = IIF(this.w_IDESEPEC="S", "S", IIF(this.w_CHKPECXCO=0 or this.w_CHKPECXCO=10,"S","N"))
                this.w_IDESEFAX = IIF(this.w_oESEFAX="S", "S", IIF(EMPTY(this.w_LPCHKFAX) or this.w_LPCHKFAX=10,"S","N"))
                this.w_IDESEFAX = IIF(this.w_IDESEFAX="S", "S", IIF(this.w_CHKFAXAGG=0 or this.w_CHKFAXAGG=10,"S","N"))
                this.w_IDESEFAX = IIF(this.w_IDESEFAX="S", "S", IIF(this.w_CHKFAXXCO=0 or this.w_CHKFAXXCO=10,"S","N"))
                this.w_IDESEWWP = IIF(this.w_oESEWWP="S", "S", IIF(EMPTY(this.w_LPCHKWWP) or this.w_LPCHKWWP=10,"S","N"))
                this.w_IDESEARC = IIF(this.w_oESEARC="S", "S", IIF(EMPTY(this.w_LPCHKARC) or this.w_LPCHKARC=10,"S","N"))
                this.w_IDESESTA = IIF(this.w_oESESTA="S", "S", IIF(EMPTY(this.w_LPCHKSTA) or this.w_LPCHKSTA=10,"S","N"))
                this.w_IDESEPTL = IIF(this.w_oESEPTL="S", "S", IIF(EMPTY(this.w_LPCHKPTL) or this.w_LPCHKPTL=10,"S","N"))
                this.w_IDESECPZ = IIF(this.w_oESECPZ="S", "S", IIF(EMPTY(this.w_LPCHKCPZ) or this.w_LPCHKCPZ=10,"S","N"))
                * --- Write into PROSLOGP
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROSLOGP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROSLOGP_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROSLOGP_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LSCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_IDESEMAI),'PROSLOGP','LSCHKMAI');
                  +",LSCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_IDESEPEC),'PROSLOGP','LSCHKPEC');
                  +",LSCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_IDESEFAX),'PROSLOGP','LSCHKFAX');
                  +",LSCHKWWP ="+cp_NullLink(cp_ToStrODBC(this.w_IDESEWWP),'PROSLOGP','LSCHKWWP');
                  +",LSCHKARC ="+cp_NullLink(cp_ToStrODBC(this.w_IDESEARC),'PROSLOGP','LSCHKARC');
                  +",LSUULESE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PROSLOGP','LSUULESE');
                  +",LSDULESE ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROSLOGP','LSDULESE');
                  +",LSKEYIST ="+cp_NullLink(cp_ToStrODBC(this.pKEYISTANZA),'PROSLOGP','LSKEYIST');
                  +",LSROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_COUNTER),'PROSLOGP','LSROWNUM');
                  +",LSCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_IDESESTA),'PROSLOGP','LSCHKSTA');
                  +",LSCHKPTL ="+cp_NullLink(cp_ToStrODBC(this.w_IDESEPTL),'PROSLOGP','LSCHKPTL');
                  +",LSCHKCPZ ="+cp_NullLink(cp_ToStrODBC(this.w_IDESECPZ),'PROSLOGP','LSCHKCPZ');
                      +i_ccchkf ;
                  +" where ";
                      +"LSCODPRO = "+cp_ToStrODBC(this.w_CODPROC);
                      +" and LSKEYVAL = "+cp_ToStrODBC(this.w_VALKEYROTT);
                         )
                else
                  update (i_cTable) set;
                      LSCHKMAI = this.w_IDESEMAI;
                      ,LSCHKPEC = this.w_IDESEPEC;
                      ,LSCHKFAX = this.w_IDESEFAX;
                      ,LSCHKWWP = this.w_IDESEWWP;
                      ,LSCHKARC = this.w_IDESEARC;
                      ,LSUULESE = i_CODUTE;
                      ,LSDULESE = i_DATSYS;
                      ,LSKEYIST = this.pKEYISTANZA;
                      ,LSROWNUM = this.w_COUNTER;
                      ,LSCHKSTA = this.w_IDESESTA;
                      ,LSCHKPTL = this.w_IDESEPTL;
                      ,LSCHKCPZ = this.w_IDESECPZ;
                      &i_ccchkf. ;
                   where;
                      LSCODPRO = this.w_CODPROC;
                      and LSKEYVAL = this.w_VALKEYROTT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Aggiorna Log Esecuzione processo <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                * --- Intestatario
                * --- Write into PRO_LOGP
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LPCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKMAI),'PRO_LOGP','LPCHKMAI');
                  +",LPCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKPEC),'PRO_LOGP','LPCHKPEC');
                  +",LPCHKWWP ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKWWP),'PRO_LOGP','LPCHKWWP');
                  +",LPCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKFAX),'PRO_LOGP','LPCHKFAX');
                  +",LPCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKSTA),'PRO_LOGP','LPCHKSTA');
                  +",LPCHKPTL ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKPTL),'PRO_LOGP','LPCHKPTL');
                  +",LPCHKARC ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKARC),'PRO_LOGP','LPCHKARC');
                  +",LPCHKCPZ ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKCPZ),'PRO_LOGP','LPCHKCPZ');
                  +",LPCHKFIR ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKFIR),'PRO_LOGP','LPCHKFIR');
                      +i_ccchkf ;
                  +" where ";
                      +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                      +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTER);
                         )
                else
                  update (i_cTable) set;
                      LPCHKMAI = this.w_LPCHKMAI;
                      ,LPCHKPEC = this.w_LPCHKPEC;
                      ,LPCHKWWP = this.w_LPCHKWWP;
                      ,LPCHKFAX = this.w_LPCHKFAX;
                      ,LPCHKSTA = this.w_LPCHKSTA;
                      ,LPCHKPTL = this.w_LPCHKPTL;
                      ,LPCHKARC = this.w_LPCHKARC;
                      ,LPCHKCPZ = this.w_LPCHKCPZ;
                      ,LPCHKFIR = this.w_LPCHKFIR;
                      &i_ccchkf. ;
                   where;
                      LPKEYIST = this.pKEYISTANZA;
                      and LPROWNUM = this.w_COUNTER;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.w_COUNTERXCO = this.w_COUNTER+1
                * --- Destinatari aggiunti
                if this.w_DESTAGG
                  this.w_IDXADD = 2
                  do while this.w_IDXADD<=this.w_NUMDESTAG
                    * --- Read from PRO_LOGP
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2],.t.,this.PRO_LOGP_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "LPCHKMAI,LPCHKPEC,LPCHKFAX"+;
                        " from "+i_cTable+" PRO_LOGP where ";
                            +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                            +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTERXCO);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        LPCHKMAI,LPCHKPEC,LPCHKFAX;
                        from (i_cTable) where;
                            LPKEYIST = this.pKEYISTANZA;
                            and LPROWNUM = this.w_COUNTERXCO;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_LCHKMAIAGG = NVL(cp_ToDate(_read_.LPCHKMAI),cp_NullValue(_read_.LPCHKMAI))
                      this.w_LCHKPECAGG = NVL(cp_ToDate(_read_.LPCHKPEC),cp_NullValue(_read_.LPCHKPEC))
                      this.w_LCHKFAXAGG = NVL(cp_ToDate(_read_.LPCHKFAX),cp_NullValue(_read_.LPCHKFAX))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- il FAX fa l'invio singolo, ra risposta � nell'array
                    this.w_CHKFAXAGG = fax_res[this.w_IDXADD,1]
                    this.w_LCHKMAIAGG = EVL(this.w_LCHKMAIAGG,this.w_CHKMAIAGG)
                    this.w_LCHKPECAGG = EVL(this.w_LCHKPECAGG,this.w_CHKPECAGG)
                    this.w_LCHKFAXAGG = EVL(this.w_LCHKFAXAGG,this.w_CHKFAXAGG)
                    * --- Write into PRO_LOGP
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"LPCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_LCHKMAIAGG),'PRO_LOGP','LPCHKMAI');
                      +",LPCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_LCHKPECAGG),'PRO_LOGP','LPCHKPEC');
                      +",LPCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_LCHKFAXAGG),'PRO_LOGP','LPCHKFAX');
                          +i_ccchkf ;
                      +" where ";
                          +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                          +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTERXCO);
                             )
                    else
                      update (i_cTable) set;
                          LPCHKMAI = this.w_LCHKMAIAGG;
                          ,LPCHKPEC = this.w_LCHKPECAGG;
                          ,LPCHKFAX = this.w_LCHKFAXAGG;
                          &i_ccchkf. ;
                       where;
                          LPKEYIST = this.pKEYISTANZA;
                          and LPROWNUM = this.w_COUNTERXCO;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                    this.w_COUNTERXCO = this.w_COUNTERXCO+1
                    this.w_IDXADD = this.w_IDXADD+1
                  enddo
                  release fax_res
                endif
                * --- Destinatario per conto di
                if Not Empty(this.w_PDCODORN)
                  * --- Write into PRO_LOGP
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"LPCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_CHKMAIXCO),'PRO_LOGP','LPCHKMAI');
                    +",LPCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_CHKPECXCO),'PRO_LOGP','LPCHKPEC');
                    +",LPCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_CHKFAXXCO),'PRO_LOGP','LPCHKFAX');
                        +i_ccchkf ;
                    +" where ";
                        +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                        +" and LPROWNUM = "+cp_ToStrODBC(this.w_COUNTERXCO);
                           )
                  else
                    update (i_cTable) set;
                        LPCHKMAI = this.w_CHKMAIXCO;
                        ,LPCHKPEC = this.w_CHKPECXCO;
                        ,LPCHKFAX = this.w_CHKFAXXCO;
                        &i_ccchkf. ;
                     where;
                        LPKEYIST = this.pKEYISTANZA;
                        and LPROWNUM = this.w_COUNTERXCO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                if g_DMIP="S" AND this.w_LPCHKARC=0
                  if vartype(aDocumentArch)="U"
                    dimension aDocumentArch(1)
                  else
                    dimension aDocumentArch(Alen(aDocumentArch)+1)
                  endif
                  aDocumentArch[Alen(aDocumentArch)]=this.w_COUNTER
                endif
              endif
              ENDSCAN
              * --- Se doveva inviare dati su NET-FOLDER crea descrittore
              this.w_NUMMAXALL = IIF(this.w_NUMMAXALL=0,9999,this.w_NUMMAXALL)
              if this.w_WESN="S" and this.w_CALLEGA>0 
                this.w_DESCRITTORE = tempadhoc()+"\"+alltrim(this.w_LPNOMDSC)
                this.w_TIPOPUBBL = "NETFOLDER"
                this.w_OPERAZIONE = "INSERT"
                this.w_TOTALLEGA = this.w_CALLEGA
                this.w_DAELEM = 1
                this.w_SAELEM = 1
                this.w_KAELEM = 1
                do while this.w_TOTALLEGA>0
                  * --- Numero degli allegati da inserire nella mail ...
                  this.w_NUMALLEGA = IIF(this.w_TOTALLEGA>=this.w_NUMMAXALL,this.w_NUMMAXALL,this.w_TOTALLEGA)
                  * --- Copia, da array L_ARRAYDOC, gli elementi di interesse ...
                  Dimension L_ARRAYWWPD(this.w_NUMALLEGA,10), L_ARRAYWWPA(this.w_NUMALLEGA+1), L_ARRAYWWPK(this.w_NUMALLEGA,3)
                  ACOPY(L_ARRAYDOC,L_ARRAYWWPD,this.w_DAELEM,this.w_NUMALLEGA*10)
                  ACOPY(L_ARRAYKEYWWP, L_ARRAYWWPK,this.w_KAELEM,this.w_NUMALLEGA*3)
                  ACOPY(L_AllegatiWE,L_ARRAYWWPA,this.w_SAELEM+1,this.w_NUMALLEGA, 2)
                  * --- Creazione Descrittore ...
                  this.w_RET = WEDESXML(this.w_DESCRITTORE,this.w_LPDOMIWE,this.w_TIPOPUBBL,this.w_OPERAZIONE, @L_ARRAYWWPD)
                  if this.w_RET
                    * --- Invia e-mail
                    * Variabili pubbliche per mail 
 i_EMAIL = this.w_LPINDSWE 
 this.w_OLDSUBJECT = i_EMAILSUBJECT 
 i_EMAILSUBJECT = alltrim(i_msgtitle)+": utilizzo servizi NET-FOLDER" 
 i_EMAILTEXT = " Vedere allegati "
                     L_ARRAYWWPA(1) = this.w_DESCRITTORE
                    this.w_RET = EMAIL( @L_ARRAYWWPA , g_UEDIAL)
                    * Variabili pubbliche per mail 
 i_EMAIL = " " 
 i_EMAILSUBJECT = this.w_OLDSUBJECT 
 i_EMAILTEXT = " "
                    if this.w_RET
                      * --- Aggiorna log sintetico (deve memorizzare se almeno una volta il processo � stato eseguito)
                      this.w_TMPN = 1
                      do while this.w_TMPN <= this.w_NUMALLEGA
                        this.w_TMPC = L_ARRAYWWPK(this.w_TMPN,1)
                        * --- Se nel log comulativo (sintetico) non c'era scritto ancora 'S', lo mette
                        if L_ARRAYWWPK(this.w_TMPN,3) <> "S"
                          * --- Write into PROSLOGP
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.PROSLOGP_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PROSLOGP_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROSLOGP_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"LSCHKWWP ="+cp_NullLink(cp_ToStrODBC("S"),'PROSLOGP','LSCHKWWP');
                                +i_ccchkf ;
                            +" where ";
                                +"LSCODPRO = "+cp_ToStrODBC(this.w_CODPROC);
                                +" and LSKEYVAL = "+cp_ToStrODBC(this.w_TMPC);
                                   )
                          else
                            update (i_cTable) set;
                                LSCHKWWP = "S";
                                &i_ccchkf. ;
                             where;
                                LSCODPRO = this.w_CODPROC;
                                and LSKEYVAL = this.w_TMPC;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                        endif
                        this.w_TMPN = this.w_TMPN+1
                      enddo
                    else
                      * --- Se non ha funzionato l'invio della eMail, scrive sul log di TUTTI i documenti l'evento
                      this.w_LPCHKWWP = 20
                      this.w_TMPN = 1
                      do while this.w_TMPN <= this.w_NUMALLEGA
                        * --- Nel log cumulativo sintetico (PROSLOGP) non deve mettere nulla (se c'era 'S' deve rimanere 'S', altrimenti va bene 'N')
                        * --- Aggiorna invece log di dettaglio
                        this.w_TMPC = L_ARRAYWWPK(this.w_TMPN,2)
                        * --- Write into PRO_LOGP
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"LPCHKWWP ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKWWP),'PRO_LOGP','LPCHKWWP');
                              +i_ccchkf ;
                          +" where ";
                              +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                              +" and LPROWNUM = "+cp_ToStrODBC(this.w_TMPC);
                                 )
                        else
                          update (i_cTable) set;
                              LPCHKWWP = this.w_LPCHKWWP;
                              &i_ccchkf. ;
                           where;
                              LPKEYIST = this.pKEYISTANZA;
                              and LPROWNUM = this.w_TMPC;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error=MSG_WRITE_ERROR
                          return
                        endif
                        this.w_TMPN = this.w_TMPN+1
                      enddo
                    endif
                  else
                    this.w_LPCHKWWP = 19
                    this.w_TMPN = 1
                    do while this.w_TMPN <= this.w_NUMALLEGA
                      * --- Nel log cumulativo sintetico (PROSLOGP) non deve mettere nulla (se c'era 'S' deve rimanere 'S', altrimenti va bene 'N')
                      * --- Aggiorna invece log di dettaglio
                      this.w_TMPC = L_ARRAYWWPK(this.w_TMPN,2)
                      * --- Write into PRO_LOGP
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"LPCHKWWP ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKWWP),'PRO_LOGP','LPCHKWWP');
                            +i_ccchkf ;
                        +" where ";
                            +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                            +" and LPROWNUM = "+cp_ToStrODBC(this.w_TMPC);
                               )
                      else
                        update (i_cTable) set;
                            LPCHKWWP = this.w_LPCHKWWP;
                            &i_ccchkf. ;
                         where;
                            LPKEYIST = this.pKEYISTANZA;
                            and LPROWNUM = this.w_TMPC;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                      this.w_TMPN = this.w_TMPN+1
                    enddo
                  endif
                  * --- Aggiorna residuo numero allegati ...
                  release L_ARRAYWWPD, L_ARRAYWWPA, L_ARRAYWWPk
                  this.w_TOTALLEGA = this.w_TOTALLEGA - this.w_NUMALLEGA
                  this.w_DAELEM = this.w_DAELEM+this.w_NUMALLEGA*10
                  this.w_KAELEM = this.w_KAELEM+this.w_NUMALLEGA*3
                  this.w_SAELEM = this.w_SAELEM+this.w_NUMALLEGA
                enddo
              endif
              * --- Se deve postalizzare alcuni documenti ...
              if this.w_POSTELSN="S" 
                * --- Estrae da cursore di stampa il cursore con documenti da postalizzare
                select * from (w_CURSORE) where __PD_FPL__="PL" into cursor __TMP_PLT__
                if used("__TMP_PLT__") and reccount()>0
                  public g_DOCM_POSTELSN 
 g_DOCM_POSTELSN = .t.
                  this.pDMReportSplit.ExportReport(this.w_MultiRep,this.w_nCount,"__TMP_PLT__")
                  * --- Creo un nuovo multireport
                  this.w_MultiRepFas=createobject("Multireport")
                  * --- Eseguo la fascicolazione sul MultiReport
                  SplitReport(null,this.w_MultiRep,this.w_MultiRepFas)
                  this.w_LPCHKPTL = PLITEEXE( this.w_multirepFas, True)
                  release g_DOCM_POSTELSN
                  * --- Rimuovo il report aggiunto in precedenza
                  this.w_MultiRep.ResetReport()     
                  this.w_MultiRepFas.ResetReport()     
                  * --- Chiude cursore ...
                  if used("__TMP_PLT__")
                    USE IN __TMP_PLT__
                  endif
                  * --- Devo aggiornare il log
                  if this.w_LPCHKPTL=0
                    * --- Aggiorna indici documenti
                    this.w_TMPN = 1
                    do while this.w_TMPN <= this.w_CONTAPTL
                      this.w_TMPC = L_ARRAYKEYPLITE(this.w_TMPN,1)
                      * --- Se nel log comulativo (sintetico) non c'era scritto ancora 'S', lo mette
                      if L_ARRAYKEYPLITE(this.w_TMPN,3) <> "S"
                        * --- Write into PROSLOGP
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.PROSLOGP_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PROSLOGP_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.PROSLOGP_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"LSCHKPTL ="+cp_NullLink(cp_ToStrODBC("S"),'PROSLOGP','LSCHKPTL');
                              +i_ccchkf ;
                          +" where ";
                              +"LSCODPRO = "+cp_ToStrODBC(this.w_CODPROC);
                              +" and LSKEYVAL = "+cp_ToStrODBC(this.w_TMPC);
                                 )
                        else
                          update (i_cTable) set;
                              LSCHKPTL = "S";
                              &i_ccchkf. ;
                           where;
                              LSCODPRO = this.w_CODPROC;
                              and LSKEYVAL = this.w_TMPC;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error=MSG_WRITE_ERROR
                          return
                        endif
                      endif
                      this.w_TMPN = this.w_TMPN+1
                    enddo
                  else
                    * --- Se non ha funzionato Postalite, scrive sul log di TUTTI i documenti l'evento
                    * --- Write into PRO_LOGP
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"LPCHKPTL ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKPTL),'PRO_LOGP','LPCHKPTL');
                          +i_ccchkf ;
                      +" where ";
                          +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                          +" and LPCHKPTL = "+cp_ToStrODBC(0);
                             )
                    else
                      update (i_cTable) set;
                          LPCHKPTL = this.w_LPCHKPTL;
                          &i_ccchkf. ;
                       where;
                          LPKEYIST = this.pKEYISTANZA;
                          and LPCHKPTL = 0;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                endif
              endif
            endif
          else
            exit
          endif
        enddo
        NEXT
        * --- Se deve stampare qualcosa ....
        if this.w_REPSN = "S"
          * --- Se stampa residuale, cancella tutti i documenti che non devono essere stampati
          this.w_nCount = 0
          this.w_TMPN = 0
          this.w_MultiReportSplit=createobject("MultiReport")
          do while .T.
            this.w_nCount = this.pDMReportSplit.GetMainReport( , this.w_nCount)
            if this.w_nCount <> -1
              w_CURSORE = this.pDMReportSplit.GetCursorName(this.w_nCount)
              select (w_CURSORE)
              WRCURSOR(w_CURSORE)
              * --- Cancella, dal cursore, eventuali righe da cancellare ...
              DELETE from (w_CURSORE) where __PD_FST__$ "RPN"
              COUNT for not DELETED() to this.w_TMPN_1
              this.w_TMPN = this.w_TMPN + this.w_TMPN_1
            else
              exit
            endif
          enddo
          * --- Fascicolo
          SplitReport(null,this.pDMReportSplit, this.w_MultiReportSplit)
          * --- Esegue metodo di stampa
          if this.w_TMPN > 0
            * --- Variabile globale necessaria alla funzione ambdoc
            public g_DOCM_PROCESSO 
 g_DOCM_PROCESSO = .t.
            * --- Lancia report ...
            if vartype (i_bNewPrintSystem)="L" AND i_bNewPrintSystem
              this.w_RET = CP_CHPFUN (this.w_OPADRE, "ExecuteReport" ,this.w_MultiReportSplit)
            else
              this.w_RET = this.w_OPADRE.ExecuteReport(this.w_MultiReportSplit)
            endif
            * --- Ripristino le condizioni iniziali
            release g_DOCM_PROCESSO 
            if this.w_RET
              * --- Aggiorna indici documenti
              this.w_TMPN = 1
              do while this.w_TMPN <= this.w_CONTADOC
                this.w_TMPC = L_ARRAYKEYROTT(this.w_TMPN,1)
                * --- Se nel log comulativo (sintetico) non c'era scritto ancora 'S', lo mette
                if L_ARRAYKEYROTT(this.w_TMPN,3) <> "S"
                  * --- Write into PROSLOGP
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PROSLOGP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROSLOGP_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PROSLOGP_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"LSCHKSTA ="+cp_NullLink(cp_ToStrODBC("S"),'PROSLOGP','LSCHKSTA');
                        +i_ccchkf ;
                    +" where ";
                        +"LSCODPRO = "+cp_ToStrODBC(this.w_CODPROC);
                        +" and LSKEYVAL = "+cp_ToStrODBC(this.w_TMPC);
                           )
                  else
                    update (i_cTable) set;
                        LSCHKSTA = "S";
                        &i_ccchkf. ;
                     where;
                        LSCODPRO = this.w_CODPROC;
                        and LSKEYVAL = this.w_TMPC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                this.w_TMPN = this.w_TMPN+1
              enddo
            else
              * --- Se non ha funzionato la stampa, scrive sul log di TUTTI i documenti l'evento
              this.w_LPCHKSTA = 52
              this.w_TMPN = 1
              do while this.w_TMPN <= this.w_CONTADOC
                * --- Nel log cumulativo sintetico (PROSLOGP) non deve mettere nulla (se c'era 'S' deve rimanere 'S', altrimenti va bene 'N')
                * --- Aggiorna invece log di dettaglio
                this.w_TMPC = L_ARRAYKEYROTT(this.w_TMPN,2)
                * --- Write into PRO_LOGP
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LPCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_LPCHKSTA),'PRO_LOGP','LPCHKSTA');
                      +i_ccchkf ;
                  +" where ";
                      +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                      +" and LPROWNUM = "+cp_ToStrODBC(this.w_TMPC);
                         )
                else
                  update (i_cTable) set;
                      LPCHKSTA = this.w_LPCHKSTA;
                      &i_ccchkf. ;
                   where;
                      LPKEYIST = this.pKEYISTANZA;
                      and LPROWNUM = this.w_TMPC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.w_TMPN = this.w_TMPN+1
              enddo
            endif
          endif
        endif
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Progress Bar
        if TYPE("this.w_OPADRE.oPDPrgBar")="O"
          this.w_OPLABEL.Caption = AH_MsgFormat("Esecuzione processo completata")
          if NOT UPPER(this.oParentObject.Class)="TGSIR_BGD" 
            this.w_OPBAR.Value = 100
          endif
          if vartype (i_bNewPrintSystem)="L" AND i_bNewPrintSystem
            this.w_OPADRE.pdEXEPROCESSO = .T.
            this.w_OPADRE.w_PDBTN = .F.
            this.w_OPADRE.w_BTNANTE = .F.
            this.w_OPADRE.mEnableControls()     
          else
            this.w_OPADRE.pdEXEPROCESSO = .T.
            this.w_OPADRE.oBtn_ESC.ToolTipText = AH_MsgFormat("Premere per uscire")
            this.w_OPADRE.oPDBtn_Proc.Enabled = .F.
            this.w_OPADRE.oPDBtn_PROC.ToolTipText = AH_MsgFormat("Processo documentale gi� eseguito")
            this.w_OPADRE.oBtn_Ante.Enabled = .F.
            this.w_OPADRE.oBtn_ANTE.ToolTipText = AH_MsgFormat("Processo documentale gi� eseguito")
          endif
          if NOT UPPER(this.oParentObject.Class)="TGSIR_BGD" 
            this.w_TMPC = "Esecuzione processo documentale completata"
            if this.w_ErrorDMSInfinity and g_DMIP="S"
              if empty(this.w_ErrorDMSInfinityMsg)
                this.w_TMPC = this.w_TMPC+ ": controllare il log perch� si sono verificati degli errori nell'invio ad Infinity DMS"
              else
                this.w_TMPC = ah_msgformat("%1%0%2", this.w_TMPC, this.w_ErrorDMSInfinityMsg)
              endif
            endif
            ah_ErrorMsg(this.w_TMPC,64)
          endif
        endif
        this.w_MultiRep = .null.
        this.w_MultiRepFas = .null.
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparazione Allegato per Mail e WE
    if NOT this.w_PDPROCES="IRP"
      * --- Nel caso di invio tramite MailToFax il formato predefinito � 'PDF'
      if g_TIPFAX="E"
        this.w_Formato = "PDF"
      else
        * --- Verifico se � stata richiesta la possibilit� di selezionare l'allegato
        this.w_Formato = IIF(g_UETIAL="A", 1, 0)
        if this.w_Formato=0
          * --- Pulisce da titolo e descrizione eventuali caratteri sporchi
          do GSUT_KXP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Se non sepcificato formato eMAIL, forza il formato PDF
          this.w_Formato = IIF(g_UETIAL="A", g_UEFORM, "PDF")
        endif
      endif
      * --- Imposta estensione allegato in base al formato
      this.w_EXT = IIF(TYPE("this.w_Formato")="N", g_UEFORM, this.w_Formato)
      * --- Se � attivo, sulla classe documentale legata al processo, il check firma digitale, viene impostato
      *     obbligatoriamente il formato a 'PDF/A'
      if empty(this.w_LPCHKFIR) or this.w_LPCHKFIR = 200 
        this.w_EXT = "PDF/A"
      endif
      this.w_PCNAME = Left( Sys(0), Rat("#",Sys( 0 ))-1)
      * --- Leggo TypeClass
      * --- Read from STMPFILE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STMPFILE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SFEXTFOR"+;
          " from "+i_cTable+" STMPFILE where ";
              +"SFPCNAME = "+cp_ToStrODBC(this.w_PCNAME);
              +" and SFFORMAT = "+cp_ToStrODBC(this.w_EXT);
              +" and SFFLGSEL = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SFEXTFOR;
          from (i_cTable) where;
              SFPCNAME = this.w_PCNAME;
              and SFFORMAT = this.w_EXT;
              and SFFLGSEL = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SFEXTFOR = NVL(cp_ToDate(_read_.SFEXTFOR),cp_NullValue(_read_.SFEXTFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows = 0
        * --- Configurazione stampa su file non configurata per la workstation
        * --- Read from STMPFILE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STMPFILE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SFEXTFOR"+;
            " from "+i_cTable+" STMPFILE where ";
                +"SFPCNAME = "+cp_ToStrODBC("INSTALLAZIONE");
                +" and SFFORMAT = "+cp_ToStrODBC(this.w_EXT);
                +" and SFFLGSEL = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SFEXTFOR;
            from (i_cTable) where;
                SFPCNAME = "INSTALLAZIONE";
                and SFFORMAT = this.w_EXT;
                and SFFLGSEL = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SFEXTFOR = NVL(cp_ToDate(_read_.SFEXTFOR),cp_NullValue(_read_.SFEXTFOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Determina nome del report <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      this.w_NomeRep = cp_GetStdFile(this.w_REPORTLOC, "FRX")
      * --- Determina nome allegato <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      this.w_NomeFileWeb = ""
      if !Empty(this.w_PDCLADOC)
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDNOMFIL"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDCODCLA = "+cp_ToStrODBC(this.w_PDCLADOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDNOMFIL;
            from (i_cTable) where;
                CDCODCLA = this.w_PDCLADOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NomeFileWeb = NVL(cp_ToDate(_read_.CDNOMFIL),cp_NullValue(_read_.CDNOMFIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Se sulla classe documentale � specificato un nome file web utilizzo quello
      if !Empty(this.w_NomeFileWeb)
        l_EvalErr = .F.
        l_oldError = On("Error")
        On Error l_EvalErr = .T.
        select (w_CURSORE)
        this.w_NOMEFILE = Eval(this.w_NomeFileWeb)
        On Error &l_oldError
        if l_EvalErr
          this.w_NomeFileWeb = ""
        endif
      endif
      * --- Se non sono riuscito a valorizzare il nome file utilizzo:
      *     CodiceProcesso_ChiaveRottura
      if Empty(this.w_NomeFile)
        this.w_NOMEFILE = WEVERTXT( alltrim(this.w_CODPROC) + "_" + alltrim(this.w_VALKEYROTT) , .F., "A")
      endif
      this.w_NOMEFILE = ForceExt( this.w_NOMEFILE, this.w_SFEXTFOR)
      this.w_NALLEGA = this.w_NOMEFILE
      * --- Aggiunge a w_NOMEFILE la directory di lavoro w_DIRLAV
      if AT( ":",this.w_NomeFile)=0
        this.w_NomeFile = addbs(tempadhoc())+this.w_NomeFile
      endif
      * --- Memorizza posizione su (w_CURSORE)
      select (w_CURSORE)
      this.w_TMPPOS = RECNO()
      * --- Crea cursore __EMAILPD__
      * --- Variabile globale necessaria alla funzione ambdoc
      public g_DOCM_PROCESSO 
 g_DOCM_PROCESSO = .t.
      * >>>>>>>>>>>>>>>>>>> Esecuzione frase di estrazione dati da cursore di stampa <<<<<<<<<<<<<<<<<<<<<<<<<< 
 select * from (w_CURSORE) ; 
 where Alltrim(Eval(this.w_PDKEYPRO)) = this.w_VALKEYROTT ; 
 into cursor __EMAILPD__
      if used("__EMAILPD__")
        * --- Chiama la Funzione di Generazione dei File secondo il Formato 
        L_PagGen = -1
        if rat(".",this.w_report) > 0
          * --- Questa operazione � resa necessaria poich� in print_to_file � usata la funzione 'cp_getstdfile()'
          this.w_Report2 = substr(this.w_report,1,rat(".",this.w_report)-1)
        else
          this.w_Report2 = this.w_report
        endif
        if TYPE("__EMAILPD__.LUCODISO") = "C"
          this.pDMReportSplit.ExportReport(this.w_MultiRep,this.w_nCount,"__EMAILPD__", __EMAILPD__.LUCODISO)
        else
          this.pDMReportSplit.ExportReport(this.w_MultiRep,this.w_nCount,"__EMAILPD__", "")
        endif
        * --- Creo un nuovo multireport
        this.w_MultiRepFas=createobject("Multireport")
        * --- Eseguo la fascicolazione sul MultiReport
        SplitReport(null,this.w_MultiRep,this.w_MultiRepFas)
        * --- Eliminati i seguenti parametri nella chiamata alla print_to_file ", w_VALLANGUAGE, w_REPSESCODE"
        this.w_OKALLEGA = Print_To_File(this.w_NomeFile, this.w_MultiRepFas, alltrim(this.w_EXT), " " , @L_PagGen)
        if L_PagGen>0
          * --- La generazione di immagini ha creato file multipli
          this.w_NFileCom = ALLTRIM(LEFT(ALLTRIM(this.w_NomeFile),LEN(ALLTRIM(this.w_NomeFile))-LEN(ALLTRIM(this.w_SFEXTFOR))-1))
          this.w_NOMEFILE = ""
          this.w_PagAtt = 1
          do while this.w_PagAtt<=L_PagGen
            this.w_NOMEFILE = this.w_NomeFile + this.w_NFileCom + "_Pag_" + ALLTRIM(STR(this.w_PagAtt)) + "." + this.w_SFEXTFOR +"; "
            this.w_PagAtt = this.w_PagAtt + 1
          enddo
          this.w_NOMEFILE = LEFT(this.w_NomeFile, LEN(this.w_NomeFile)-2)
        endif
        select __EMAILPD__ 
 use 
        * --- Rimuovo il report aggiunto in precedenza
        this.w_MultiRep.ResetReport()     
        this.w_MultiRepFas.ResetReport()     
      endif
      * --- Ripristino le condizioni iniziali
      release g_DOCM_PROCESSO 
      * --- Ripristina posizione su (w_CURSORE)
      select (w_CURSORE)
      if this.w_TMPPOS>0
        goto (this.w_TMPPOS)
      endif
    else
      if UPPER(this.oParentObject.w_EXT)=".HTM"
        this.w_NomeFile = FORCEEXT(ADDBS( this.oParentObject.w_DTPATH ) + ALLTRIM( this.oParentObject.w_DTFILE ), "HTML")
      else
        this.w_NomeFile = FORCEEXT(ADDBS( this.oParentObject.w_DTPATH ) + ALLTRIM( this.oParentObject.w_DTFILE ), this.oParentObject.w_EXT)
      endif
      this.w_OKALLEGA = FILE(this.w_NomeFile)
    endif
  endproc


  procedure Inoltro_FAX
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inoltro FAX
    this.w_CHKFAX = 0
    * --- Memorizza posizione su (w_CURSORE)
    select (w_CURSORE)
    this.w_TMPPOS = RECNO()
    do case
      case g_TIPFAX = "M"
        * --- Servizio MAPI <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        this.w_FAXA = i_FAXNO
        this.w_INVIAA = i_DEST
        this.w_MITTEN = g_RAGAZI
        this.w_MAIMIT = g_MITTEN
        this.w_UTEAZI = g_DESUTE
        this.w_FAXMIT = g_TELFAX
        this.w_TELMIT = g_TELEFO
        this.w_NOMEFRS = ""
        this.w_SOGFAX = this.w_VALTESTO
        this.w_OGGFAX = this.w_VALOGGETTO
        this.w_RIFERI = " "
        * --- Se necessario prepara FRONTESPIZIO (sempre formato PDF)
        this.w_RET = .T.
        this.w_FRONTE = this.w_LPFROFAX
        if this.w_FRONTE="S"
          this.w_Formato = 5
          * --- Prepara Frontespizio
          CREATE CURSOR TMP_FRS (FSINVIAA C(40), FSRIFERI C(40), FS__NUMA C(18), ; 
 FSSOGGET M(10), FSOGGETT M(10), FS__DATA D(8), FSRAGSDA C(40), FSUTENTE C(40), ; 
 FS_NUMD C(18), FS_EMAIL C(50), FSTELEFO C(18), FS__TIME C(10))
          INSERT INTO TMP_FRS (FSINVIAA, FSRIFERI, FS__NUMA, ; 
 FSSOGGET, FSOGGETT, FS__DATA, FSRAGSDA, FSUTENTE, ; 
 FS_NUMD, FS_EMAIL, FSTELEFO, FS__TIME) ; 
 VALUES (this.w_INVIAA, this.w_RIFERI, this.w_FAXA, ; 
 this.w_SOGFAX, this.w_OGGFAX, DATE(), this.w_MITTEN, this.w_UTEAZI, ; 
 this.w_FAXMIT, this.w_MAIMIT, this.w_TELMIT, TIME())
          this.w_NomeFrs = tempadhoc() + "\"+cp_NewCCChk()+"_FRS." + this.w_SFEXTFOR
          * --- Chiama la Funzione di Generazione dei File secondo il Formato (A.M. Function & Procedures)
          this.w_RET = .F.
          do case
            case this.w_Formato=5
              this.w_RET = PDF_Format(this.w_NomeFrs, "QUERY\GSUT3FRS.FRX")
            case this.w_Formato=6
              this.w_RET = HTM_Format(this.w_NomeFrs, "QUERY\GSUT3FRS.FRX")
            case this.w_Formato=7
              this.w_RET = RTF_Format(this.w_NomeFrs, "QUERY\GSUT3FRS.FRX")
          endcase
          if used("TMP_FRS")
            SELECT TMP_FRS
            USE
          endif
          * --- Se non ha funzionato la creazione del frontespizio, annota sul log
          if not this.w_RET
            this.w_CHKFAX = IIF(Empty(this.w_CHKFAX), 35, this.w_CHKFAX)
          endif
        endif
        * --- Invio FAX 
        if this.w_RET
          this.w_RET = FAX_MAPI(this.w_NomeFile, this.w_NomeFrs)
          if not this.w_RET
            this.w_CHKFAX = IIF(Empty(this.w_CHKFAX), 36, this.w_CHKFAX)
          endif
        endif
      case g_TIPFAX = "S"
        * --- Stampante Virtuale <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Esecuzione frase di estrazione dati da cursore di stampa
         
 select * from (w_CURSORE) ; 
 where Alltrim(Eval(this.w_PDKEYPRO)) = this.w_VALKEYROTT ; 
 into cursor __FAXPD__
        if used("__FAXPD__")
          this.pDMReportSplit.ExportReport(this.w_MultiRep,this.w_nCount,"__FAXPD__",__FAXPD__.LUCODISO)
          * --- Creo un nuovo multireport
          this.w_MultiRepFas=createobject("Multireport")
          * --- Eseguo la fascicolazione sul MultiReport
          SplitReport(null,this.w_MultiRep,this.w_MultiRepFas)
          * --- Eliminati i seguenti parametri nella chiamata alla print_to_file ", w_VALLANGUAGE, w_REPSESCODE"
          this.w_RET = FAX_STA(this.w_MultirepFas)
          if not this.w_RET
            this.w_CHKFAX = IIF(Empty(this.w_CHKFAX), 37, this.w_CHKFAX)
          endif
          * --- Rimuovo il report aggiunto in precedenza
          this.w_MultiRep.ResetReport()     
          this.w_MultiRepFas.ResetReport()     
          if used("__FAXPD__")
            select __FAXPD__
            Use
          endif
        endif
      case g_TIPFAX="R"
        * --- Rendering Subsystem <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Esecuzione frase di estrazione dati da cursore di stampa
        i_FAXNO = " "
        i_DEST = " "
         
 select * from (w_CURSORE) ; 
 where Alltrim(Eval(this.w_PDKEYPRO)) = this.w_VALKEYROTT ; 
 into cursor __FAXPD__
        if used("__FAXPD__")
          SELECT "__FAXPD__"
          this.w_RET = FAX_RSS(this.w_NomeRep, "FRX")
          if not this.w_RET
            this.w_CHKFAX = IIF(Empty(this.w_CHKFAX), 38, this.w_CHKFAX)
          endif
          if used("__FAXPD__")
            select __FAXPD__
            Use
          endif
        endif
      case g_TIPFAX="E"
        * --- leggo il valore della variabile e lo metto in w_VEDIFAX
        * --- Read from UTE_NTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UTE_NTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UTDIAFAX,UTFROFAX"+;
            " from "+i_cTable+" UTE_NTI where ";
                +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UTDIAFAX,UTFROFAX;
            from (i_cTable) where;
                UTCODICE = i_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_VEDIFAX = NVL(cp_ToDate(_read_.UTDIAFAX),cp_NullValue(_read_.UTDIAFAX))
          this.w_FRONTE = NVL(cp_ToDate(_read_.UTFROFAX),cp_NullValue(_read_.UTFROFAX))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Nel caso di schedulatore non apro mai la maschera di Invio Fax
        if this.w_SCHEDULER
          this.w_DIALOG = "N"
        else
          if (alltrim(w_VEDIFAX) = "S") or (alltrim(w_VEDIFAX) = "A" and empty(i_FAXNO) )
            this.w_DIALOG = "S"
          else
            this.w_DIALOG = "N"
          endif
        endif
        * --- Se l'utente ha selezionato il servizio MailToFax apro la stessa maschera
        *     dell'invio E-Mail
        this.w_RET = EMAIL(this.w_NomeFile, this.w_DIALOG, .F., i_FAXNO, i_FAXSUBJECT, "", .T., IIF(empty(this.w_FRONTE),"N", this.w_FRONTE)="S")
    endcase
    * --- Ripristina posizione su (w_CURSORE)
    select (w_CURSORE)
    if this.w_TMPPOS>0
      goto (this.w_TMPPOS)
    endif
    return(this.w_CHKFAX)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- = =   I N V I O   A   C O R P O R A T E   P O R T A L   Z U C C H E T T I   = = 
    this.w_CODAGE1 = " "
    this.w_TIPCON1 = " "
    this.w_CODCON1 = " "
    * --- Valorizza intestatario principale ...
    if this.w_TFOLDER="C"
      select (w_CURSORE)
      do case
        case this.w_TIPDES1 $ "CF"
          this.w_TMPC = Eval(this.w_DEKEYINF)
          * --- Riassegna
          this.w_TIPCON1 = IIF(empty(this.w_TMPC), " ", left(this.w_TMPC,1) )
          this.w_CODCON1 = IIF(empty(this.w_TMPC) or LEN(ALLTRIM(this.w_TMPC))<2, " ", substr(this.w_TMPC,2) )
          * --- Riassegna a w_TIPDES1 il valore di w_TIPCON1 (a livello di processo documentale viene indicato un generico 'C'
          *     che identifica un Cliente/Fornitore => ora viene assegnato il giusto valore C o F)
          this.w_TIPDES1 = this.w_TIPCON1
        case this.w_TIPDES1 ="A"
          this.w_CODAGE1 = Eval(this.w_DEKEYINF)
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGCODFOR"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGCODFOR;
              from (i_cTable) where;
                  AGCODAGE = this.w_CODAGE1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se l'agente � legato ad un fornitore, allora deve mandare (come destinatario) il fornitore (ci� che � stato spedito al portale in fase di pubblicazione anagrafiche)
          if Not(Empty(this.w_CODAGE1)) and Not(Empty(this.w_CODCON))
            * --- Sostituisce l'agente con il fornitore ....
            this.w_TIPDES1 = "F"
            this.w_TIPCON1 = "F"
            this.w_CODCON1 = this.w_CODCON
            this.w_CODAGE1 = Space(5)
          endif
      endcase
    endif
    * --- Determina la chiave per DMS-CPZ
    this.w_CHIAVEDMS = ""
    if Not(Empty(this.w_DEFILKEY))
      select (w_CURSORE)
      this.w_CHIAVEDMS = Eval(this.w_DEFILKEY)
    endif
    * --- Estrae nome file
    if Not(Empty(this.w_DENOMFIL))
      * --- Ottiene Nome File ...
      select (w_CURSORE)
      this.w_TMPC = Eval(this.w_DENOMFIL)
      * --- Aggiunge estensione ...
      this.w_PNAME = iif(rightc(AllTrim(this.w_TMPC),1)#".",AllTrim(this.w_TMPC)+".",AllTrim(this.w_TMPC))+this.w_ESTENSIONE_CPZ
    else
      this.w_PNAME = SUBSTR(this.w_DESTFILE, RAT("\",this.w_DESTFILE)+1)
    endif
    * --- Inserisce record testata ...
    this.w_DVPATHFI = this.w_DESTFILE
    this.w_DVDESELA = LEFT(nvl(EVAL(this.w_DENOMFIL), this.w_VALOGGETTO),100)
    this.w_DV__NOTE = this.w_VALTESTO
    this.w_DVDATCRE = i_DATSYS
    if g_ICPZSTANDALONE or g_IZCP="A"
      this.w_FILEXML = LRTrim(this.w_DVPATHFI)+".xml"
      this.w_NHF = FCreate(this.w_FILEXML)
      if this.w_NHF>=0
        this.w_TmPC = '<?xml version="1.0" encoding="iso-8859-1"?>'
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = '<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = "  <Name>" + convertXML(LRTrim(this.w_PNAME)) + "</Name>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        if this.w_TFOLDER="C"
          this.w_TMPC = "  <FolderCompany>%" + IIF(this.w_TIPDES1$"CFA",this.w_TIPDES1,"")+IIF(this.w_TIPDES1="A",alltrim(this.w_CODAGE1),alltrim(this.w_CODCON1))+"%"+alltrim(this.w_PFOLDER) + "</FolderCompany>"
          this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        else
          this.w_TmPC = "  <Folder>" + alltrim(this.w_PFOLDER) + "</Folder>"
          this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        endif
        this.w_TmPC = "  <Description>" + convertXML(LRTrim(this.w_DVDESELA)) + "</Description>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = "  <Notes>" + convertXML(LRTrim(this.w_DV__NOTE)) + "</Notes>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = "  <AttachmentType>" + this.w_DEFILTIP + "</AttachmentType>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = "  <AttachmentOwnerCode>" + this.w_CHIAVEDMS + "</AttachmentOwnerCode>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = "  <RemoveFileAfterUpload>S</RemoveFileAfterUpload>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
        this.w_TmPC = "  <Reserved>" + this.w_RISERVATO + "</Reserved>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      endif
    else
      this.w_DV_PARAM = "NAME="+alltrim(this.w_PNAME)+"; "
      if this.w_TFOLDER="C"
        this.w_DV_PARAM = this.w_DV_PARAM+"FOLDERCOMPANY=%"+IIF(this.w_TIPDES1$"CFA",this.w_TIPDES1,"")+IIF(this.w_TIPDES1="A",alltrim(this.w_CODAGE1),alltrim(this.w_CODCON1))+"%"+alltrim(this.w_PFOLDER)+"; "
      else
        this.w_DV_PARAM = this.w_DV_PARAM+"FOLDER="+alltrim(this.w_PFOLDER)+"; "
      endif
      this.w_DVCODELA = ZCPINDIV("TRA",this.w_DVDESELA,this.w_DVDATCRE,"S",this.w_DVPATHFI,this.w_DV_PARAM,this.w_DV__NOTE,this.w_RISERVATO,this.w_DEFILTIP,this.w_CHIAVEDMS)
    endif
    * --- Dettaglio permessi ...
    if this.w_RISERVATO="S"
      * --- Se stand-alone apre il tag Securities
      if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
        this.w_TmPC = "  <Securities>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      endif
      * --- Inserisce Destinatari ...
      * --- Select from PRO_PCPZ
      i_nConn=i_TableProp[this.PRO_PCPZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_PCPZ_idx,2],.t.,this.PRO_PCPZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRO_PCPZ ";
            +" where DECODICE="+cp_ToStrODBC(this.w_CODPROC)+"";
             ,"_Curs_PRO_PCPZ")
      else
        select * from (i_cTable);
         where DECODICE=this.w_CODPROC;
          into cursor _Curs_PRO_PCPZ
      endif
      if used('_Curs_PRO_PCPZ')
        select _Curs_PRO_PCPZ
        locate for 1=1
        do while not(eof())
        * --- Reset
        this.w_CODAGE1 = " "
        this.w_TIPCON1 = " "
        this.w_CODCON1 = " "
        this.w_TMPC = " "
        this.w_FLOK = "N"
        this.w_TMPDES1 = NVL(_Curs_PRO_PCPZ.DETIPDES," ")
        * --- Valutazione campo cursore ...
        if Not(Empty(Nvl(_Curs_PRO_PCPZ.DEKEYINF," ")))
          select (w_CURSORE)
          this.w_TMPC = Eval(_Curs_PRO_PCPZ.DEKEYINF)
        endif
        if Not(Empty(Nvl(this.w_TMPC," ")))
          do case
            case this.w_TMPDES1 $ "CF" 
              * --- Riassegna
              this.w_TMPC = Nvl(this.w_TMPC, " ")
              this.w_TIPCON1 = IIF(empty(this.w_TMPC), " ", left(this.w_TMPC,1) )
              this.w_CODCON1 = IIF(empty(this.w_TMPC) or LEN(ALLTRIM(this.w_TMPC))<2, " ", substr(this.w_TMPC,2) )
              * --- Legge flag di pubblicazione
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCHKCPZ"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON1);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON1);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCHKCPZ;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON1;
                      and ANCODICE = this.w_CODCON1;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLOK = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Riassegna a w_TMPDES1 il valore di w_TIPCON1 (a livello di processo documentale viene indicato un generico 'C'
              *     che identifica un Cliente/Fornitore => ora viene assegnato il giusto valore C o F)
              this.w_TMPDES1 = this.w_TIPCON1
            case this.w_TMPDES1 ="A"
              this.w_CODAGE1 = Nvl(this.w_TMPC, " ")
              * --- Read from AGENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.AGENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "AGCHKCPZ,AGCODFOR"+;
                  " from "+i_cTable+" AGENTI where ";
                      +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE1);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  AGCHKCPZ,AGCODFOR;
                  from (i_cTable) where;
                      AGCODAGE = this.w_CODAGE1;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLOK = NVL(cp_ToDate(_read_.AGCHKCPZ),cp_NullValue(_read_.AGCHKCPZ))
                this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Se l'agente � legato ad un fornitore, allora deve mandare (come destinatario) il fornitore (ci� che � stato spedito al portale in fase di pubblicazione anagrafiche)
              if Not(Empty(this.w_CODAGE1)) and Not(Empty(this.w_CODCON))
                * --- Sostituisce l'agente con il fornitore ....
                this.w_TMPDES1 = "F"
                this.w_TIPDES1 = "F"
                this.w_TIPCON1 = "F"
                this.w_CODCON1 = this.w_CODCON
                this.w_CODAGE1 = Space(5)
                * --- RILegge flag di pubblicazione
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANCHKCPZ"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON1);
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON1);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANCHKCPZ;
                    from (i_cTable) where;
                        ANTIPCON = this.w_TIPCON1;
                        and ANCODICE = this.w_CODCON1;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_FLOK = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
          endcase
        endif
        if this.w_TMPDES1 = "G"
          this.w_FLOK = "S"
        endif
        this.w_CODGRU = IIF(this.w_TMPDES1="C",20, IIF(this.w_TMPDES1="A",30, IIF(this.w_TMPDES1="F",40, _Curs_PRO_PCPZ.DECODGRU)))
        * --- Inserimento in dettaglio ...
        if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
          if this.w_NHF>=0
            this.w_TmPC = IIF(this.w_TMPDES1$"CF", this.w_CODCON1, IIF(this.w_TMPDES1="A",this.w_CODAGE1, IIF(this.w_TMPDES1="G", str(_Curs_PRO_PCPZ.DECODGRU,6,0), " ")))
            if Not(Empty(this.w_TMPC)) AND this.w_FLOK="S"
              this.w_TmPC = "    <Security>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
              this.w_TmPC = IIF(this.w_TMPDES1="C","CUSTOMER", IIF(this.w_TMPDES1="F", "SUPPLIER", IIF(this.w_TMPDES1="A","SALESAGENT",IIF(this.w_TMPDES1="G","GROUP"," "))))
              this.w_TmPC = space(6)+"<EntityType>" + this.w_Tmpc + "</EntityType>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
              this.w_TmPC = IIF(this.w_TMPDES1$"CF", this.w_TMPDES1+this.w_CODCON1, IIF(this.w_TMPDES1="A", "A"+this.w_CODAGE1, IIF(this.w_TMPDES1="G", str(_Curs_PRO_PCPZ.DECODGRU,6,0), " ")))
              this.w_TmPC = space(6)+"<EntityCode>" + LRTrim(this.w_Tmpc) + "</EntityCode>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
              * --- Utente abilitato, lo inserisco ...
              this.w_TmPC = space(6)+"<Read>" + _Curs_PRO_PCPZ.DE__READ + "</Read>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
              this.w_TmPC = space(6)+"<Write>" + _Curs_PRO_PCPZ.DE_WRITE + "</Write>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
              this.w_TmPC = space(6)+"<Delete>" + _Curs_PRO_PCPZ.DEDELETE + "</Delete>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
              this.w_TmPC = "    </Security>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            endif
          endif
        else
          if this.w_FLOK="S"
            this.w_TMPC = IIF(this.w_TMPDES1$"CF", this.w_CODCON1, IIF(this.w_TMPDES1="A", this.w_CODAGE1, IIF(this.w_TMPDES1="G", str(_Curs_PRO_PCPZ.DECODGRU,6,0), " ")))
            if Not(Empty(Nvl(this.w_TMPC," ")))
              * --- Insert into ZDESTIN
              i_nConn=i_TableProp[this.ZDESTIN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ZDESTIN_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZDESTIN_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"DECODELA"+",CPROWNUM"+",DETIPDES"+",DECODGRU"+",DETIPCON"+",DECODCON"+",DECODAGE"+",DECODROL"+",DEVALDES"+",DE__READ"+",DE_WRITE"+",DEDELETE"+",DEFLGESE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_DVCODELA),'ZDESTIN','DECODELA');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_PRO_PCPZ.CPROWNUM),'ZDESTIN','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TMPDES1),'ZDESTIN','DETIPDES');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODGRU),'ZDESTIN','DECODGRU');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON1),'ZDESTIN','DETIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON1),'ZDESTIN','DECODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE1),'ZDESTIN','DECODAGE');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_PRO_PCPZ.DECODROL),'ZDESTIN','DECODROL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TMPC),'ZDESTIN','DEVALDES');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_PRO_PCPZ.DE__READ),'ZDESTIN','DE__READ');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_PRO_PCPZ.DE_WRITE),'ZDESTIN','DE_WRITE');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_PRO_PCPZ.DEDELETE),'ZDESTIN','DEDELETE');
                +","+cp_NullLink(cp_ToStrODBC("N"),'ZDESTIN','DEFLGESE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'DECODELA',this.w_DVCODELA,'CPROWNUM',_Curs_PRO_PCPZ.CPROWNUM,'DETIPDES',this.w_TMPDES1,'DECODGRU',this.w_CODGRU,'DETIPCON',this.w_TIPCON1,'DECODCON',this.w_CODCON1,'DECODAGE',this.w_CODAGE1,'DECODROL',_Curs_PRO_PCPZ.DECODROL,'DEVALDES',this.w_TMPC,'DE__READ',_Curs_PRO_PCPZ.DE__READ,'DE_WRITE',_Curs_PRO_PCPZ.DE_WRITE,'DEDELETE',_Curs_PRO_PCPZ.DEDELETE)
                insert into (i_cTable) (DECODELA,CPROWNUM,DETIPDES,DECODGRU,DETIPCON,DECODCON,DECODAGE,DECODROL,DEVALDES,DE__READ,DE_WRITE,DEDELETE,DEFLGESE &i_ccchkf. );
                   values (;
                     this.w_DVCODELA;
                     ,_Curs_PRO_PCPZ.CPROWNUM;
                     ,this.w_TMPDES1;
                     ,this.w_CODGRU;
                     ,this.w_TIPCON1;
                     ,this.w_CODCON1;
                     ,this.w_CODAGE1;
                     ,_Curs_PRO_PCPZ.DECODROL;
                     ,this.w_TMPC;
                     ,_Curs_PRO_PCPZ.DE__READ;
                     ,_Curs_PRO_PCPZ.DE_WRITE;
                     ,_Curs_PRO_PCPZ.DEDELETE;
                     ,"N";
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
        endif
          select _Curs_PRO_PCPZ
          continue
        enddo
        use
      endif
      * --- Se stand-alone chiude il tag Securities
      if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
        this.w_TmPC = "  </Securities>"
        this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Se stand-alone chiude il tag Securities
    if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
      this.w_TmPC = "</Document>"
      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
      this.w_TMPN = FClose(this.w_NHF)
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    *     Chiudo XML descrittore per invio a Infinity
    if g_DMIP="S" and not empty(this.w_UploadDocumentsXML) AND this.w_CDMODALL<>"S"
      * --- Se non esiste ancora xml descrittore lo creo
      this.w_OBJXML.Save(this.w_UploadDocumentsXML)     
      * --- Faccio lo zip della cartella con il descrittore e i file da inviare
      local L_ERROREZIP
      if ZIPUNZIP("Z", FORCEEXT(this.w_UploadDocumentsXML,"ZIP"), ADDBS(JUSTPATH(this.w_UploadDocumentsXML)), "", @L_ERROREZIP, "7zzip")
        * --- Chiamo procedura per invio del file zippato
        * --- variabili Infinity D.M.S.
        * --- Memorizza ... e imposta
         w_CreateObjErr = .F. 
 w_OldErr=on("ERROR")
        on error w_CreateObjErr=.t.
        this.w_SCHEDULER = TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
        this.w_oObjInfinity = InfinityConnect(.F., g_REVI="S", g_REVI="S" And Not this.w_SCHEDULER)
        if vartype(this.w_oObjInfinity)="O"
          this.w_ERRINF = this.w_oObjInfinity.UploadDocuments(FORCEEXT(this.w_UploadDocumentsXML,"ZIP"))
          if this.w_ERRINF = 0
            this.w_ERRINF = this.w_oObjInfinity.GetUploadResponse(FORCEEXT(this.w_UploadDocumentsXML,"")+"_response.xml","generic")
            if this.w_ERRINF = 0
              * --- Gestione xml di risposta
              this.w_FILETOSTR = FILETOSTR(FORCEEXT(this.w_UploadDocumentsXML,"")+"_response.xml")
              this.w_OBJXMLRES = createobject("Msxml2.DOMDocument")
              this.w_OBJXMLRES.LoadXML(this.w_FILETOSTR)     
              this.w_NUMNODI = this.w_OBJXMLRES.childNodes.item(1).childNodes.length
              this.w_iChild = 0
              * --- * recupera tutti i nodi figli
              do while this.w_iChild < this.w_NUMNODI
                * --- recupero nodo Process
                if this.w_OBJXMLRES.childNodes.item(1).childNodes.item(this.w_iChild).nodeName="Add_UploadDocuments"
                  this.w_oNODE = this.w_OBJXMLRES.childNodes.item(1).childNodes.item(this.w_iChild)
                  this.w_nDocumentUploaded = this.w_nDocumentUploaded + 1
                  this.w_NameError = NVL(this.w_oNODE.GetAttribute("Error"), "")
                  if not empty(this.w_NameError)
                    this.w_ErrorDMSInfinity = .t.
                    * --- Write into PRO_LOGP
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRO_LOGP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LOGP_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LOGP_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"LPCHKARC ="+cp_NullLink(cp_ToStrODBC(43),'PRO_LOGP','LPCHKARC');
                      +",LPMESSAG ="+cp_NullLink(cp_ToStrODBC(this.w_NameError),'PRO_LOGP','LPMESSAG');
                          +i_ccchkf ;
                      +" where ";
                          +"LPKEYIST = "+cp_ToStrODBC(this.pKEYISTANZA);
                          +" and LPROWNUM = "+cp_ToStrODBC(aDocumentArch[this.w_nDocumentUploaded]);
                             )
                    else
                      update (i_cTable) set;
                          LPCHKARC = 43;
                          ,LPMESSAG = this.w_NameError;
                          &i_ccchkf. ;
                       where;
                          LPKEYIST = this.pKEYISTANZA;
                          and LPROWNUM = aDocumentArch[this.w_nDocumentUploaded];

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                endif
                this.w_iChild = this.w_iChild + 1
              enddo
            else
              this.w_ErrorDMSInfinityMsg = ah_msgformat("Errore nell'invio a Infinity dei documenti%0Codice errore:%1", str(this.w_ERRINF, 6,0))
            endif
          else
            this.w_ErrorDMSInfinityMsg = ah_msgformat("Errore nell'invio a Infinity dei documenti%0Codice errore:%1", str(this.w_ERRINF, 6,0))
          endif
          this.w_oObjInfinity.DisConnect()     
        else
          this.w_ErrorDMSInfinityMsg = ah_msgformat("Errore nell'invio a Infinity dei documenti%0Impossibile creare connessione")
        endif
        this.w_ErrorDMSInfinity = this.w_ErrorDMSInfinity or not empty(this.w_ErrorDMSInfinityMsg)
        * --- Ripristina OnError
        on error &w_OldErr
      else
        ah_errormsg("Errore nella creazione dello zip per l'invio a Infinity dei documenti%0%1","!","",L_ERROREZIP)
      endif
    endif
  endproc


  procedure create_zip
    param pNOMEFILE
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ALINES(ARRAY_FILE,m.pNOMEFILE,";",CHR(13)+CHR(10))
    this.w_WORK_FILE = ALLTRIM(ARRAY_FILE)
    this.w_WORK_DIR = ADDBS(JUSTPATH(this.w_WORK_FILE))
    this.w_TMP_DIR = ADDBS(this.w_WORK_DIR+SYS(2015))
    this.w_EXT = ALLTRIM(JUSTEXT(this.w_WORK_FILE))
    makedir(this.w_TMP_DIR)
    l_m = 'RENAME "' + SUBSTR(this.w_WORK_FILE,1,RAT("_pag_",LOWER(this.w_WORK_FILE))+4)+"*."+this.w_EXT+'" TO "' + this.w_TMP_DIR + '*"' 
 &l_m
    this.w_WORK_FILE = JUSTSTEM(this.w_WORK_FILE)
    m.pNOMEFILE = this.w_WORK_DIR+SUBSTR(this.w_WORK_FILE,1,RAT("_pag_",LOWER(this.w_WORK_FILE))-1) + "_"+this.w_EXT+"_.zip"
    DELETE FILE (m.pNOMEFILE)
    zipunzip("Z",m.pNOMEFILE,this.w_TMP_DIR,"","","7zzip")
    deletefolder(this.w_TMP_DIR)
    return(m.pNOMEFILE)
  endproc


  procedure NormMailList
    param w_MailList
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- normalizza lista indirizzi e-mail
    this.w_RetMailList = alltrim(m.w_MailList)
    this.w_LOOP = Occurs(";", this.w_RetMailList)
    this.w_POS1 = Rat(";", this.w_RetMailList)
    do while this.w_LOOP>1
      this.w_POS2 = this.w_POS1
      this.w_POS1 = At(";", this.w_RetMailList, this.w_LOOP-1)
      if this.w_POS2>0
        this.w_IND_MAIL = SubStr(this.w_RetMailList, this.w_POS1+1, this.w_POS2-this.w_POS1-1)
        if empty(this.w_IND_MAIL)
          * --- Nessuna mail tra i due ";"
          this.w_RetMailList = Left(this.w_RetMailList, this.w_POS1) + SubStr(this.w_RetMailList, this.w_POS2+1)
        endif
      endif
      this.w_LOOP = this.w_LOOP-1
    enddo
    if Right(this.w_RetMailList,1)=";"
      this.w_RetMailList = Left(this.w_RetMailList, Len(this.w_RetMailList)-1)
    endif
    return(this.w_RetMailList)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTIPOOPE,pKEYISTANZA,pDMReportSplit)
    this.pTIPOOPE=pTIPOOPE
    this.pKEYISTANZA=pKEYISTANZA
    this.pDMReportSplit=pDMReportSplit
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,23)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='OUT_PUTS'
    this.cWorkTables[5]='PRO_LOGP'
    this.cWorkTables[6]='PRODCLAS'
    this.cWorkTables[7]='PRODDOCU'
    this.cWorkTables[8]='PRODINDI'
    this.cWorkTables[9]='PROMCLAS'
    this.cWorkTables[10]='PROMDOCU'
    this.cWorkTables[11]='PROMINDI'
    this.cWorkTables[12]='PROSLOGP'
    this.cWorkTables[13]='UTE_NTI'
    this.cWorkTables[14]='WECONTI'
    this.cWorkTables[15]='CONTROPA'
    this.cWorkTables[16]='PROPPERM'
    this.cWorkTables[17]='PRO_PCPZ'
    this.cWorkTables[18]='ZFOLDERS'
    this.cWorkTables[19]='ZDESTIN'
    this.cWorkTables[20]='AGENTI'
    this.cWorkTables[21]='PRO_FCPZ'
    this.cWorkTables[22]='STMPFILE'
    this.cWorkTables[23]='MOD_OFFE'
    return(this.OpenAllTables(23))

  proc CloseCursors()
    if used('_Curs_PRODDOCU')
      use in _Curs_PRODDOCU
    endif
    if used('_Curs__d__d__DOCM_EXE_QUERY_chkprocd')
      use in _Curs__d__d__DOCM_EXE_QUERY_chkprocd
    endif
    if used('_Curs_PRO_FCPZ')
      use in _Curs_PRO_FCPZ
    endif
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_PRO_PCPZ')
      use in _Curs_PRO_PCPZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsdm_bpd
  * ===============================================================================
    * AH_ExistFolder() - Funzione che verifica l'esistenza di una cartella
    *
    Function AH_ExistFolder(pDirectory,pDirName)
    Private n_File, sTemp,a_Dir,n_Dir,sDirName,lDirectory,lDirName,i
    * check parametri
    pDirectory=alltrim(pDirectory)
    if vartype(pDirName)<>'C'
       pDirectory=iif(right(pDirectory,1)="\", left(pDirectory,len(pDirectory)-1), pDirectory)
       lDirectory=substr(pDirectory,1,rat('\',pDirectory))
       lDirName=substr(pDirectory,rat('\',pDirectory)+1)
    else
       lDirectory=pDirectory
       lDirName=pDirName
    endif
    *
    n_File=0
    n_Dir = adir(a_Dir,lDirectory+iif(right(lDirectory,1)='\','','\')+"*","D")
    for i=1 to n_Dir
       sDirName = upper(alltrim(a_Dir(i,1)))      && ADIR ritorna senza \ finale
       if !((sDirName == ".") .or. (sDirName == "..")) and sDirName==alltrim(upper(LDirName))
         n_File=n_File+1
       endif
    next
    Return (n_File>0)
    
    * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=alltrim(pDirectory)
    pDirectory=iif(right(pDirectory,1)="\", left(pDirectory,len(pDirectory)-1), pDirectory)
    *
    MakeDir(pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = ah_errormsg("Impossibile creare la cartella %1",16,'',pDirectory)
    endif
    * Risultato
    Return (lRet)
    
    *====================================================================================
    * controlla se il file zip passato contiene file in uno dei formati grafici elencati
    * l'indicazione del formato dei file grafici contenuti � presente in fondo al nome e,
    * se trovata, viene restituita.
    *
    * NOTA: questa funzione � replicata anche nella gsut_bba
    *
    * BMP - JPG - JPEG - PNG - GIF - TIF - TIFF 
    *____________________________________________________________________________________
    FUNCTION check_grafico(pDoc)
     IF ("_BMP_"$pDoc)
      RETURN "_BMP_"
     ENDIF
     IF ("_JPG_"$pDoc)
      RETURN "_JPG_"
     ENDIF
     IF ("_JPEG_"$pDoc)
      RETURN "_JPEG_"
     ENDIF
     IF ("_PNG_"$pDoc)
      RETURN "_PNG_"
     ENDIF
     IF ("_GIF_"$pDoc)
      RETURN "_GIF_"
     ENDIF
     IF ("_TIF_"$pDoc)
      RETURN "_TIF_"
     ENDIF
     IF ("_TIFF_"$pDoc)
      RETURN "_TIFF_"
     ENDIF
     RETURN ""
    ENDFUN
    *____________________________________________________________________________________
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOOPE,pKEYISTANZA,pDMReportSplit"
endproc
