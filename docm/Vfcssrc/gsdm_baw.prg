* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_baw                                                        *
*              Archiviazione con ArchiWEB                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-27                                                      *
* Last revis.: 2007-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPERAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_baw",oParentObject,m.pOPERAZIONE)
return(i_retval)

define class tgsdm_baw as StdBatch
  * --- Local variables
  pOPERAZIONE = space(1)
  w_AWEB = .NULL.
  w_PUNNON = .NULL.
  w_LICLADOC = space(10)
  w_CDCAMCUR = space(100)
  w_INDVALATTR = space(0)
  w_IDVALATT = space(100)
  w_TMPT = space(1)
  w_TMPN = 0
  w_DBSERVER = space(20)
  w_DBNAME = space(30)
  w_USRNAME = space(30)
  w_USRPWD = space(30)
  w_CONSTR = space(100)
  w_NOMEFILE = space(100)
  w_ERRORE = .f.
  w_LENATT = 0
  * --- WorkFile variables
  PRODCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Puntatore a routine chiamante 
    if this.pOPERAZIONE = "D"
      * --- GSDM_BPD
      this.w_PUNNON = this.oParentObject
    else
      * --- GSUT_BCV
      this.w_PUNNON = this.oParentObject.oParentObject
    endif
    * --- Classe Documentale selezionata ...
    this.w_LICLADOC = rtrim(this.w_PUNNON.w_LICLADOC)
    * --- Recupera parametri di collegamento al DB di archiWEB
    this.w_DBSERVER = rtrim(this.w_PUNNON.w_DBSERVER)
    this.w_DBNAME = rtrim(this.w_PUNNON.w_DBNAME)
    this.w_USRNAME = rtrim(this.w_PUNNON.w_USRNAME)
    this.w_USRPWD = rtrim(this.w_PUNNON.w_USRPWD)
    * --- C=Archivia, V=Ricerca, D=Archivia da Processo Doc.
    this.w_TMPN = 0
    if not empty(this.w_LICLADOC)
      * --- Prepara attributi e relativo valore
      this.w_TMPN = 1
      * --- Select from PRODCLAS
      i_nConn=i_TableProp[this.PRODCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRODCLAS ";
            +" where CDCODCLA="+cp_ToStrODBC(this.w_LICLADOC)+"";
            +" order by CPROWORD";
             ,"_Curs_PRODCLAS")
      else
        select * from (i_cTable);
         where CDCODCLA=this.w_LICLADOC;
         order by CPROWORD;
          into cursor _Curs_PRODCLAS
      endif
      if used('_Curs_PRODCLAS')
        select _Curs_PRODCLAS
        locate for 1=1
        do while not(eof())
        * --- Prepara valore campo chiave
        this.w_TMPT = nvl(_Curs_PRODCLAS.CDTIPATT, " ")
        this.w_CDCAMCUR = nvl(_Curs_PRODCLAS.CDCAMCUR," ")
        if not empty(this.w_CDCAMCUR)
          if this.pOPERAZIONE="D"
            select __PDTMP__
          else
            select (this.oParentObject.oParentObject.oParentObject.cCursor)
          endif
          w_ErrorHandler = on("ERROR")
          this.w_ERRORE = .F.
          on error this.w_ERRORE = .T.
          this.w_INDVALATTR = eval(this.w_CDCAMCUR)
          on error &w_ErrorHandler
        else
          this.w_INDVALATTR = " "
        endif
        * --- Traduce in stringa
        this.w_LENATT = nvl(_Curs_PRODCLAS.CDLUNATT,0)
        if this.w_ERRORE
          this.w_IDVALATT = " "
        else
          do case
            case this.w_TMPT="N"
              this.w_IDVALATT = alltrim(str(this.w_INDVALATTR))
            case this.w_TMPT="D"
              this.w_IDVALATT = dtoc(this.w_INDVALATTR)
            otherwise
              this.w_IDVALATT = this.w_INDVALATTR
          endcase
        endif
        this.w_IDVALATT = padr(this.w_IDVALATT,this.w_LENATT)
        dimension arrvalchiavi(this.w_TMPN) 
 arrvalchiavi(this.w_TMPN) = nvl(alltrim(this.w_IDVALATT),"")
        this.w_TMPN = this.w_TMPN+1
          select _Curs_PRODCLAS
          continue
        enddo
        use
      endif
      this.w_CONSTR = "Server="+this.w_DBSERVER+";Provider=SQLOLEDB;database="+this.w_DBNAME+";User Id="+this.w_USRNAME+";password="+this.w_USRPWD
      do case
        case this.pOPERAZIONE="C"
          this.w_AWEB = this.oParentObject.w_AWEB
          this.w_TMPN = this.w_AWEB.archiviadoc("", this.w_LICLADOC, @arrvalchiavi, this.w_CONSTR, this.w_USRNAME, 1,1)
        case this.pOPERAZIONE="V"
          this.w_TMPN = 0
          this.w_AWEB = this.oParentObject.w_AWEBR
          this.w_AWEB.StartEX(this.w_USRNAME,this.w_USRPWD,this.w_CONSTR)     
          Dimension l_ArrayResult(1)
          l_ArrayResult = this.w_AWEB.cerca(this.w_LICLADOC, @arrvalchiavi, .T.,1)
        case this.pOPERAZIONE="D"
          this.w_NOMEFILE = rtrim(this.w_PUNNON.w_NOMEFILE)
          this.w_AWEB = this.oParentObject.w_AWEB
          this.w_TMPN = this.w_AWEB.w_ARCHIWEB.archiviadoc(this.w_NOMEFILE, this.w_LICLADOC, @arrvalchiavi, this.w_CONSTR, this.w_USRNAME, 2,1)
      endcase
    endif
    i_retcode = 'stop'
    i_retval = this.w_TMPN
    return
  endproc


  proc Init(oParentObject,pOPERAZIONE)
    this.pOPERAZIONE=pOPERAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRODCLAS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PRODCLAS')
      use in _Curs_PRODCLAS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPERAZIONE"
endproc
