* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bat                                                        *
*              Verifica attributi classe                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-04                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bat",oParentObject,m.pTIPOP)
return(i_retval)

define class tgsdm_bat as StdBatch
  * --- Local variables
  pTIPOP = space(1)
  w_APPO = space(40)
  w_PADRE = .NULL.
  * --- WorkFile variables
  CLASDSOS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica attributi
    if Vartype(this.pTIPOP) = "C" 
      this.w_PADRE = This.oParentObject
      do case
        case this.pTIPOP="X"
          * --- Pulisco i dati relativi a SOStitutiva nel dettaglio
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while ! this.w_PADRE.Eof_Trs()
            if nvl(this.w_PADRE.Get("w_CDFLGSOS"),"  ") > 2 
              this.w_PADRE.DeleteRow()     
            else
              this.w_PADRE.Set("w_CDATTSOS"," ")     
              this.w_PADRE.Set("w_CDFLGSOS",1)     
              this.w_PADRE.Set("w_CODCLASOS",this.oParentObject.w_CDCLASOS)     
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos()     
          this.oParentObject.w_CDKEYUPD = "N"
        case this.pTIPOP="S"
          * --- Inserisco gli attributi relativi a chiavi SOS non di tipo 'Libero' 
          this.w_PADRE.MarkPos()     
          * --- Select from CLASDSOS
          i_nConn=i_TableProp[this.CLASDSOS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2],.t.,this.CLASDSOS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SZCODKEY,SZDESKEY,SZTIPKEY,SZTIPINT  from "+i_cTable+" CLASDSOS ";
                +" where SZCODCLA="+cp_ToStrODBC(this.oParentObject.w_CDCLASOS)+" and SZTIPINT not in ('LI', 'HS', 'NM') and   SZKEYOBS<>'S'";
                +" order by CPROWORD";
                 ,"_Curs_CLASDSOS")
          else
            select SZCODKEY,SZDESKEY,SZTIPKEY,SZTIPINT from (i_cTable);
             where SZCODCLA=this.oParentObject.w_CDCLASOS and SZTIPINT not in ("LI", "HS", "NM") and   SZKEYOBS<>"S";
             order by CPROWORD;
              into cursor _Curs_CLASDSOS
          endif
          if used('_Curs_CLASDSOS')
            select _Curs_CLASDSOS
            locate for 1=1
            do while not(eof())
            * --- Aggiungo le chiavi  mancanti al transitorio
            this.w_PADRE.AddRow()     
            this.oParentObject.w_CDCODATT = _Curs_CLASDSOS.SZCODKEY
            this.oParentObject.w_CDDESATT = _Curs_CLASDSOS.SZDESKEY
            this.oParentObject.w_CDTIPATT = ICASE(_Curs_CLASDSOS.SZTIPKEY="varchar","C",_Curs_CLASDSOS.SZTIPKEYE="int","N",_Curs_CLASDSOS.SZTIPKEY="datetime","D","X")
            this.oParentObject.w_KEYTYPE = _Curs_CLASDSOS.SZTIPKEY
            this.oParentObject.w_CDFLGSOS = _Curs_CLASDSOS.SZTIPINT
            this.oParentObject.w_CDATTSOS = _Curs_CLASDSOS.SZCODKEY
            this.w_PADRE.SaveRow()     
              select _Curs_CLASDSOS
              continue
            enddo
            use
          endif
          this.w_PADRE.RePos()     
          this.oParentObject.w_CDKEYUPD = "S"
          this.w_PADRE.oPgFrm.Page1.oPag.oBody.Refresh()     
      endcase
    else
      this.w_APPO = alltrim(this.oParentObject.w_CDCODATT)
      * --- Eseguo la sostituzione dei caratteri per i quali non � possibile salvare il nome di un file. /,:, *,?, ", <, >, |
      this.w_APPO = chkstrch(this.w_APPO)
      this.oParentObject.w_CDCODATT = this.w_APPO
    endif
  endproc


  proc Init(oParentObject,pTIPOP)
    this.pTIPOP=pTIPOP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CLASDSOS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CLASDSOS')
      use in _Curs_CLASDSOS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOP"
endproc
