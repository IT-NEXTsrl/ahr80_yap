* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_sau                                                        *
*              Stampa autorizzazioni processi                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2009-08-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_sau",oParentObject))

* --- Class definition
define class tgsdm_sau as StdForm
  Top    = 12
  Left   = 10

  * --- Standard Properties
  Width  = 586
  Height = 222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-08-05"
  HelpContextID=124372073
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  PROMDOCU_IDX = 0
  PRODCLAS_IDX = 0
  PROMCLAS_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cPrg = "gsdm_sau"
  cComment = "Stampa autorizzazioni processi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODUTEIN = 0
  w_DESCUTIN = space(20)
  w_CODUTEFI = 0
  w_DESCUTFI = space(20)
  w_CODGRUIN = 0
  w_DESCGRUI = space(20)
  w_CODGRUFI = 0
  w_DESCGRUF = space(20)
  w_PROCESS = space(10)
  w_DESPROCI = space(40)
  w_CLASDOCU = space(15)
  w_DESCLASS = space(40)
  w_TIPOARCH = space(1)
  w_TIPOSOG = space(1)
  o_TIPOSOG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_sauPag1","gsdm_sau",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTEIN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PROMDOCU'
    this.cWorkTables[2]='PRODCLAS'
    this.cWorkTables[3]='PROMCLAS'
    this.cWorkTables[4]='CPUSERS'
    this.cWorkTables[5]='CPGROUPS'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODUTEIN=0
      .w_DESCUTIN=space(20)
      .w_CODUTEFI=0
      .w_DESCUTFI=space(20)
      .w_CODGRUIN=0
      .w_DESCGRUI=space(20)
      .w_CODGRUFI=0
      .w_DESCGRUF=space(20)
      .w_PROCESS=space(10)
      .w_DESPROCI=space(40)
      .w_CLASDOCU=space(15)
      .w_DESCLASS=space(40)
      .w_TIPOARCH=space(1)
      .w_TIPOSOG=space(1)
        .w_CODUTEIN = 0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODUTEIN))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODUTEFI = 0
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODUTEFI))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_CODGRUIN = 0
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODGRUIN))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CODGRUFI = 0
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODGRUFI))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_PROCESS))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CLASDOCU))
          .link_1_11('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .DoRTCalc(12,13,.f.)
        .w_TIPOSOG = 'U'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODUTEIN = 0
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODUTEFI = 0
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODGRUIN = 0
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODGRUFI = 0
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODUTEIN_1_1.visible=!this.oPgFrm.Page1.oPag.oCODUTEIN_1_1.mHide()
    this.oPgFrm.Page1.oPag.oDESCUTIN_1_2.visible=!this.oPgFrm.Page1.oPag.oDESCUTIN_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCODUTEFI_1_3.visible=!this.oPgFrm.Page1.oPag.oCODUTEFI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDESCUTFI_1_4.visible=!this.oPgFrm.Page1.oPag.oDESCUTFI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCODGRUIN_1_5.visible=!this.oPgFrm.Page1.oPag.oCODGRUIN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESCGRUI_1_6.visible=!this.oPgFrm.Page1.oPag.oDESCGRUI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCODGRUFI_1_7.visible=!this.oPgFrm.Page1.oPag.oCODGRUFI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESCGRUF_1_8.visible=!this.oPgFrm.Page1.oPag.oDESCGRUF_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTEIN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTEIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTEIN);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTEIN)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTEIN) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTEIN_1_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTEIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTEIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTEIN)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTEIN = NVL(_Link_.CODE,0)
      this.w_DESCUTIN = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTEIN = 0
      endif
      this.w_DESCUTIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale o inesistente")
        endif
        this.w_CODUTEIN = 0
        this.w_DESCUTIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTEIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTEFI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTEFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTEFI);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTEFI)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTEFI) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTEFI_1_3'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTEFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTEFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTEFI)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTEFI = NVL(_Link_.CODE,0)
      this.w_DESCUTFI = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTEFI = 0
      endif
      this.w_DESCUTFI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore del codice iniziale o inesistente")
        endif
        this.w_CODUTEFI = 0
        this.w_DESCUTFI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTEFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRUIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRUIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRUIN);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRUIN)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRUIN) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRUIN_1_5'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRUIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRUIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRUIN)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRUIN = NVL(_Link_.CODE,0)
      this.w_DESCGRUI = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRUIN = 0
      endif
      this.w_DESCGRUI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice del gruppo iniziale � maggiore di quello finale o inesistente")
        endif
        this.w_CODGRUIN = 0
        this.w_DESCGRUI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRUIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRUFI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRUFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRUFI);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRUFI)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRUFI) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRUFI_1_7'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRUFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRUFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRUFI)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRUFI = NVL(_Link_.CODE,0)
      this.w_DESCGRUF = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRUFI = 0
      endif
      this.w_DESCGRUF = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice gruppo finale � minore di quello iniziale o inesistente")
        endif
        this.w_CODGRUFI = 0
        this.w_DESCGRUF = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRUFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROCESS
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_lTable = "PROMDOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2], .t., this.PROMDOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROCESS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMDOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDCODPRO like "+cp_ToStrODBC(trim(this.w_PROCESS)+"%");

          i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDCODPRO',trim(this.w_PROCESS))
          select PDCODPRO,PDDESPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROCESS)==trim(_Link_.PDCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROCESS) and !this.bDontReportError
            deferred_cp_zoom('PROMDOCU','*','PDCODPRO',cp_AbsName(oSource.parent,'oPROCESS_1_9'),i_cWhere,'',"Processi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',oSource.xKey(1))
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROCESS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(this.w_PROCESS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',this.w_PROCESS)
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROCESS = NVL(_Link_.PDCODPRO,space(10))
      this.w_DESPROCI = NVL(_Link_.PDDESPRO,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PROCESS = space(10)
      endif
      this.w_DESPROCI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])+'\'+cp_ToStr(_Link_.PDCODPRO,1)
      cp_ShowWarn(i_cKey,this.PROMDOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROCESS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASDOCU
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASDOCU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDM_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASDOCU)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLASDOCU))
          select CDCODCLA,CDDESCLA,CDTIPARC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASDOCU)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASDOCU) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLASDOCU_1_11'),i_cWhere,'GSDM_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASDOCU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASDOCU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLASDOCU)
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASDOCU = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLASS = NVL(_Link_.CDDESCLA,space(40))
      this.w_TIPOARCH = NVL(_Link_.CDTIPARC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASDOCU = space(15)
      endif
      this.w_DESCLASS = space(40)
      this.w_TIPOARCH = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOARCH='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida oppure con tipo-archiviazione manuale (deve essere automatica)")
        endif
        this.w_CLASDOCU = space(15)
        this.w_DESCLASS = space(40)
        this.w_TIPOARCH = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASDOCU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTEIN_1_1.value==this.w_CODUTEIN)
      this.oPgFrm.Page1.oPag.oCODUTEIN_1_1.value=this.w_CODUTEIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTIN_1_2.value==this.w_DESCUTIN)
      this.oPgFrm.Page1.oPag.oDESCUTIN_1_2.value=this.w_DESCUTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTEFI_1_3.value==this.w_CODUTEFI)
      this.oPgFrm.Page1.oPag.oCODUTEFI_1_3.value=this.w_CODUTEFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTFI_1_4.value==this.w_DESCUTFI)
      this.oPgFrm.Page1.oPag.oDESCUTFI_1_4.value=this.w_DESCUTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRUIN_1_5.value==this.w_CODGRUIN)
      this.oPgFrm.Page1.oPag.oCODGRUIN_1_5.value=this.w_CODGRUIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGRUI_1_6.value==this.w_DESCGRUI)
      this.oPgFrm.Page1.oPag.oDESCGRUI_1_6.value=this.w_DESCGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRUFI_1_7.value==this.w_CODGRUFI)
      this.oPgFrm.Page1.oPag.oCODGRUFI_1_7.value=this.w_CODGRUFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGRUF_1_8.value==this.w_DESCGRUF)
      this.oPgFrm.Page1.oPag.oDESCGRUF_1_8.value=this.w_DESCGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oPROCESS_1_9.value==this.w_PROCESS)
      this.oPgFrm.Page1.oPag.oPROCESS_1_9.value=this.w_PROCESS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPROCI_1_10.value==this.w_DESPROCI)
      this.oPgFrm.Page1.oPag.oDESPROCI_1_10.value=this.w_DESPROCI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASDOCU_1_11.value==this.w_CLASDOCU)
      this.oPgFrm.Page1.oPag.oCLASDOCU_1_11.value=this.w_CLASDOCU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLASS_1_12.value==this.w_DESCLASS)
      this.oPgFrm.Page1.oPag.oDESCLASS_1_12.value=this.w_DESCLASS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSOG_1_21.RadioValue()==this.w_TIPOSOG)
      this.oPgFrm.Page1.oPag.oTIPOSOG_1_21.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEFI))  and not(.w_TIPOSOG<>'U')  and not(empty(.w_CODUTEIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODUTEIN_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale o inesistente")
          case   not((.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEIN))  and not(.w_TIPOSOG<>'U')  and not(empty(.w_CODUTEFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODUTEFI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore del codice iniziale o inesistente")
          case   not((.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUFI))  and not(.w_TIPOSOG<>'G')  and not(empty(.w_CODGRUIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODGRUIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice del gruppo iniziale � maggiore di quello finale o inesistente")
          case   not((.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUIN))  and not(.w_TIPOSOG<>'G')  and not(empty(.w_CODGRUFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODGRUFI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice gruppo finale � minore di quello iniziale o inesistente")
          case   not(.w_TIPOARCH='A')  and not(empty(.w_CLASDOCU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASDOCU_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida oppure con tipo-archiviazione manuale (deve essere automatica)")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOSOG = this.w_TIPOSOG
    return

enddefine

* --- Define pages as container
define class tgsdm_sauPag1 as StdContainer
  Width  = 582
  height = 222
  stdWidth  = 582
  stdheight = 222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODUTEIN_1_1 as StdField with uid="ABGCYOUKIO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODUTEIN", cQueryName = "CODUTEIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale o inesistente",;
    ToolTipText = "Codice utente iniziale",;
    HelpContextID = 214970508,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=153, Top=29, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTEIN"

  func oCODUTEIN_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  func oCODUTEIN_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTEIN_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTEIN_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTEIN_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESCUTIN_1_2 as StdField with uid="ROHBQQPVTV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCUTIN", cQueryName = "DESCUTIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente iniziale",;
    HelpContextID = 231819900,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=212, Top=29, InputMask=replicate('X',20)

  func oDESCUTIN_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  add object oCODUTEFI_1_3 as StdField with uid="RHVPTHQVBP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODUTEFI", cQueryName = "CODUTEFI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore del codice iniziale o inesistente",;
    ToolTipText = "Codice utente finale",;
    HelpContextID = 53464943,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=153, Top=55, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTEFI"

  func oCODUTEFI_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  func oCODUTEFI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTEFI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTEFI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTEFI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESCUTFI_1_4 as StdField with uid="RMJAFAKDHC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCUTFI", cQueryName = "DESCUTFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente finale",;
    HelpContextID = 36615551,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=212, Top=56, InputMask=replicate('X',20)

  func oDESCUTFI_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  add object oCODGRUIN_1_5 as StdField with uid="RGUATXEEUW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODGRUIN", cQueryName = "CODGRUIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice del gruppo iniziale � maggiore di quello finale o inesistente",;
    ToolTipText = "Codice gruppo iniziale",;
    HelpContextID = 217985164,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=153, Top=29, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRUIN"

  func oCODGRUIN_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  func oCODGRUIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRUIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRUIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRUIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESCGRUI_1_6 as StdField with uid="DPVMJNUWZC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCGRUI", cQueryName = "DESCGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo iniziale",;
    HelpContextID = 256816511,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=212, Top=29, InputMask=replicate('X',20)

  func oDESCGRUI_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  add object oCODGRUFI_1_7 as StdField with uid="JXHNESXSEP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODGRUFI", cQueryName = "CODGRUFI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice gruppo finale � minore di quello iniziale o inesistente",;
    ToolTipText = "Codice gruppo finale",;
    HelpContextID = 50450287,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=153, Top=55, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRUFI"

  func oCODGRUFI_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  func oCODGRUFI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRUFI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRUFI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRUFI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESCGRUF_1_8 as StdField with uid="IOZTSZEUAB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCGRUF", cQueryName = "DESCGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo finale",;
    HelpContextID = 256816508,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=212, Top=56, InputMask=replicate('X',20)

  func oDESCGRUF_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  add object oPROCESS_1_9 as StdField with uid="YDJNMPSZIQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PROCESS", cQueryName = "PROCESS",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice del processo documentale",;
    HelpContextID = 3048182,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=153, Top=83, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PROMDOCU", oKey_1_1="PDCODPRO", oKey_1_2="this.w_PROCESS"

  func oPROCESS_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROCESS_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROCESS_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMDOCU','*','PDCODPRO',cp_AbsName(this.parent,'oPROCESS_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Processi documentali",'',this.parent.oContained
  endproc

  add object oDESPROCI_1_10 as StdField with uid="SZTLYKXEHS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESPROCI", cQueryName = "DESPROCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del processo iniziale",;
    HelpContextID = 218871167,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=288, Top=83, InputMask=replicate('X',40)

  add object oCLASDOCU_1_11 as StdField with uid="HTWLNLKNKC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLASDOCU", cQueryName = "CLASDOCU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida oppure con tipo-archiviazione manuale (deve essere automatica)",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 204315771,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=153, Top=110, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSDM_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLASDOCU"

  func oCLASDOCU_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASDOCU_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASDOCU_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oCLASDOCU_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDM_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oCLASDOCU_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSDM_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_CLASDOCU
     i_obj.ecpSave()
  endproc

  add object oDESCLASS_1_12 as StdField with uid="PXXOXRHLPO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCLASS", cQueryName = "DESCLASS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del processo iniziale",;
    HelpContextID = 245282185,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=288, Top=110, InputMask=replicate('X',40)


  add object oObj_1_13 as cp_outputCombo with uid="ZLERXIVPWT",left=153, top=145, width=422,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53625574


  add object oBtn_1_14 as StdButton with uid="WDHBUTUXIR",left=472, top=174, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 17417510;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="FGOJEBDDCO",left=526, top=174, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 117054650;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPOSOG_1_21 as StdCombo with uid="JAZVMLTCMO",value=3,rtseq=14,rtrep=.f.,left=153,top=3,width=78,height=21;
    , ToolTipText = "Tipo filtro (utente/gruppo/entrambi)";
    , HelpContextID = 219843126;
    , cFormVar="w_TIPOSOG",RowSource=""+"Utente,"+"Gruppo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSOG_1_21.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPOSOG_1_21.GetRadio()
    this.Parent.oContained.w_TIPOSOG = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSOG_1_21.SetRadio()
    this.Parent.oContained.w_TIPOSOG=trim(this.Parent.oContained.w_TIPOSOG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSOG=='U',1,;
      iif(this.Parent.oContained.w_TIPOSOG=='G',2,;
      iif(this.Parent.oContained.w_TIPOSOG=='',3,;
      0)))
  endfunc

  add object oStr_1_16 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=39, Top=148,;
    Alignment=1, Width=112, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="QEJMRMWTBF",Visible=.t., Left=34, Top=86,;
    Alignment=1, Width=117, Height=18,;
    Caption="Codice processo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PJFDIZMZHH",Visible=.t., Left=6, Top=112,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="XKNETBNRGP",Visible=.t., Left=33, Top=32,;
    Alignment=1, Width=117, Height=18,;
    Caption="Da codice utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="RCEYAMNBEB",Visible=.t., Left=48, Top=6,;
    Alignment=1, Width=102, Height=18,;
    Caption="Soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="NOLMRMDXPU",Visible=.t., Left=32, Top=58,;
    Alignment=1, Width=118, Height=18,;
    Caption="A codice utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="OEZOJTEAFS",Visible=.t., Left=33, Top=32,;
    Alignment=1, Width=117, Height=18,;
    Caption="Da codice gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="DKSSVBKEOQ",Visible=.t., Left=32, Top=58,;
    Alignment=1, Width=118, Height=18,;
    Caption="A codice gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_sau','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
