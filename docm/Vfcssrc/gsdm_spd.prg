* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_spd                                                        *
*              Stampa processi documentali                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2012-05-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_spd",oParentObject))

* --- Class definition
define class tgsdm_spd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 589
  Height = 173
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-08"
  HelpContextID=141149289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  PROMDOCU_IDX = 0
  PRODCLAS_IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsdm_spd"
  cComment = "Stampa processi documentali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PROCEINI = space(10)
  w_DESPROCI = space(40)
  w_PROCEFIN = space(10)
  w_TIPARCH = space(1)
  w_DESPROCF = space(40)
  w_CLASDOCU = space(15)
  w_DESCLASS = space(40)
  w_TIPOARCH = space(1)
  w_TABARCRA = space(20)
  w_TABRIFER = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_spdPag1","gsdm_spd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPROCEINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PROMDOCU'
    this.cWorkTables[2]='PRODCLAS'
    this.cWorkTables[3]='PROMCLAS'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROCEINI=space(10)
      .w_DESPROCI=space(40)
      .w_PROCEFIN=space(10)
      .w_TIPARCH=space(1)
      .w_DESPROCF=space(40)
      .w_CLASDOCU=space(15)
      .w_DESCLASS=space(40)
      .w_TIPOARCH=space(1)
      .w_TABARCRA=space(20)
      .w_TABRIFER=space(15)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PROCEINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_PROCEFIN))
          .link_1_3('Full')
        endif
        .w_TIPARCH = 'A'
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CLASDOCU))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(7,7,.f.)
        .w_TIPOARCH = 'A'
    endwith
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PROCEINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_lTable = "PROMDOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2], .t., this.PROMDOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROCEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMDOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDCODPRO like "+cp_ToStrODBC(trim(this.w_PROCEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDCODPRO',trim(this.w_PROCEINI))
          select PDCODPRO,PDDESPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROCEINI)==trim(_Link_.PDCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROCEINI) and !this.bDontReportError
            deferred_cp_zoom('PROMDOCU','*','PDCODPRO',cp_AbsName(oSource.parent,'oPROCEINI_1_1'),i_cWhere,'',"Processi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',oSource.xKey(1))
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROCEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(this.w_PROCEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',this.w_PROCEINI)
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROCEINI = NVL(_Link_.PDCODPRO,space(10))
      this.w_DESPROCI = NVL(_Link_.PDDESPRO,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PROCEINI = space(10)
      endif
      this.w_DESPROCI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PROCEINI<=.w_PROCEFIN) OR EMPTY(.w_PROCEFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale o inesistente")
        endif
        this.w_PROCEINI = space(10)
        this.w_DESPROCI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])+'\'+cp_ToStr(_Link_.PDCODPRO,1)
      cp_ShowWarn(i_cKey,this.PROMDOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROCEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROCEFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_lTable = "PROMDOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2], .t., this.PROMDOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROCEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMDOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDCODPRO like "+cp_ToStrODBC(trim(this.w_PROCEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDCODPRO',trim(this.w_PROCEFIN))
          select PDCODPRO,PDDESPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROCEFIN)==trim(_Link_.PDCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROCEFIN) and !this.bDontReportError
            deferred_cp_zoom('PROMDOCU','*','PDCODPRO',cp_AbsName(oSource.parent,'oPROCEFIN_1_3'),i_cWhere,'',"Processi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',oSource.xKey(1))
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROCEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(this.w_PROCEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',this.w_PROCEFIN)
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROCEFIN = NVL(_Link_.PDCODPRO,space(10))
      this.w_DESPROCF = NVL(_Link_.PDDESPRO,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PROCEFIN = space(10)
      endif
      this.w_DESPROCF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PROCEFIN>=.w_PROCEINI) OR EMPTY(.w_PROCEINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore di quello iniziale o inesistente")
        endif
        this.w_PROCEFIN = space(10)
        this.w_DESPROCF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])+'\'+cp_ToStr(_Link_.PDCODPRO,1)
      cp_ShowWarn(i_cKey,this.PROMDOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROCEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASDOCU
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASDOCU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDM_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASDOCU)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLASDOCU))
          select CDCODCLA,CDDESCLA,CDTIPARC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASDOCU)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASDOCU) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLASDOCU_1_6'),i_cWhere,'GSDM_MCD',"Classi documentali",'GSDM1TAR.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASDOCU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASDOCU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLASDOCU)
            select CDCODCLA,CDDESCLA,CDTIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASDOCU = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLASS = NVL(_Link_.CDDESCLA,space(40))
      this.w_TIPOARCH = NVL(_Link_.CDTIPARC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASDOCU = space(15)
      endif
      this.w_DESCLASS = space(40)
      this.w_TIPOARCH = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOARCH='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida oppure con tipo-archiviazione manuale (deve essere automatica)")
        endif
        this.w_CLASDOCU = space(15)
        this.w_DESCLASS = space(40)
        this.w_TIPOARCH = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASDOCU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPROCEINI_1_1.value==this.w_PROCEINI)
      this.oPgFrm.Page1.oPag.oPROCEINI_1_1.value=this.w_PROCEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPROCI_1_2.value==this.w_DESPROCI)
      this.oPgFrm.Page1.oPag.oDESPROCI_1_2.value=this.w_DESPROCI
    endif
    if not(this.oPgFrm.Page1.oPag.oPROCEFIN_1_3.value==this.w_PROCEFIN)
      this.oPgFrm.Page1.oPag.oPROCEFIN_1_3.value=this.w_PROCEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPROCF_1_5.value==this.w_DESPROCF)
      this.oPgFrm.Page1.oPag.oDESPROCF_1_5.value=this.w_DESPROCF
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASDOCU_1_6.value==this.w_CLASDOCU)
      this.oPgFrm.Page1.oPag.oCLASDOCU_1_6.value=this.w_CLASDOCU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLASS_1_7.value==this.w_DESCLASS)
      this.oPgFrm.Page1.oPag.oDESCLASS_1_7.value=this.w_DESCLASS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_PROCEINI<=.w_PROCEFIN) OR EMPTY(.w_PROCEFIN))  and not(empty(.w_PROCEINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPROCEINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale o inesistente")
          case   not((.w_PROCEFIN>=.w_PROCEINI) OR EMPTY(.w_PROCEINI))  and not(empty(.w_PROCEFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPROCEFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore di quello iniziale o inesistente")
          case   not(.w_TIPOARCH='A')  and not(empty(.w_CLASDOCU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASDOCU_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida oppure con tipo-archiviazione manuale (deve essere automatica)")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_spdPag1 as StdContainer
  Width  = 585
  height = 173
  stdWidth  = 585
  stdheight = 173
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPROCEINI_1_1 as StdField with uid="YDJNMPSZIQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PROCEINI", cQueryName = "PROCEINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale o inesistente",;
    ToolTipText = "Codice del processo iniziale",;
    HelpContextID = 181501121,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=150, Top=9, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PROMDOCU", oKey_1_1="PDCODPRO", oKey_1_2="this.w_PROCEINI"

  func oPROCEINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROCEINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROCEINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMDOCU','*','PDCODPRO',cp_AbsName(this.parent,'oPROCEINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Processi documentali",'',this.parent.oContained
  endproc

  add object oDESPROCI_1_2 as StdField with uid="SZTLYKXEHS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESPROCI", cQueryName = "DESPROCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del processo iniziale",;
    HelpContextID = 202093951,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=9, InputMask=replicate('X',40)

  add object oPROCEFIN_1_3 as StdField with uid="WGTUYZJFWW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PROCEFIN", cQueryName = "PROCEFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore di quello iniziale o inesistente",;
    ToolTipText = "Codice del processo finale",;
    HelpContextID = 231832764,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=150, Top=36, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PROMDOCU", oKey_1_1="PDCODPRO", oKey_1_2="this.w_PROCEFIN"

  func oPROCEFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROCEFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROCEFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMDOCU','*','PDCODPRO',cp_AbsName(this.parent,'oPROCEFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Processi documentali",'',this.parent.oContained
  endproc

  add object oDESPROCF_1_5 as StdField with uid="BKJABBXIGS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESPROCF", cQueryName = "DESPROCF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del processo iniziale",;
    HelpContextID = 202093948,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=36, InputMask=replicate('X',40)

  add object oCLASDOCU_1_6 as StdField with uid="HTWLNLKNKC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CLASDOCU", cQueryName = "CLASDOCU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida oppure con tipo-archiviazione manuale (deve essere automatica)",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 187538555,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=150, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSDM_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLASDOCU"

  func oCLASDOCU_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASDOCU_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASDOCU_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oCLASDOCU_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDM_MCD',"Classi documentali",'GSDM1TAR.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oCLASDOCU_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSDM_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_CLASDOCU
     i_obj.ecpSave()
  endproc

  add object oDESCLASS_1_7 as StdField with uid="PXXOXRHLPO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCLASS", cQueryName = "DESCLASS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del processo iniziale",;
    HelpContextID = 228504969,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=63, InputMask=replicate('X',40)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=150, top=98, width=418,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 36848358


  add object oBtn_1_9 as StdButton with uid="WDHBUTUXIR",left=469, top=124, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 640294;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="FGOJEBDDCO",left=524, top=124, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 133831866;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=15, Top=101,;
    Alignment=1, Width=133, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QEJMRMWTBF",Visible=.t., Left=14, Top=13,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da codice processo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OJGWZGVSZY",Visible=.t., Left=14, Top=39,;
    Alignment=1, Width=135, Height=18,;
    Caption="A codice processo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="PJFDIZMZHH",Visible=.t., Left=4, Top=65,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_spd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
