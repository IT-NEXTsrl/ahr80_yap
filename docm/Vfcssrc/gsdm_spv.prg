* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_spv                                                        *
*              Stampa preferenze di visualizzazione                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_spv",oParentObject))

* --- Class definition
define class tgsdm_spv as StdForm
  Top    = 10
  Left   = 34

  * --- Standard Properties
  Width  = 518
  Height = 171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=141149289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cPrg = "gsdm_spv"
  cComment = "Stampa preferenze di visualizzazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOSOG = space(1)
  o_TIPOSOG = space(1)
  w_CODGRUIN = 0
  w_DESCGRUI = space(20)
  w_CODGRUFI = 0
  w_DESCGRUF = space(20)
  w_CODUTEIN = 0
  w_DESCUTEI = space(20)
  w_CODUTEFI = 0
  w_DESCUTEF = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_spvPag1","gsdm_spv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOSOG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOSOG=space(1)
      .w_CODGRUIN=0
      .w_DESCGRUI=space(20)
      .w_CODGRUFI=0
      .w_DESCGRUF=space(20)
      .w_CODUTEIN=0
      .w_DESCUTEI=space(20)
      .w_CODUTEFI=0
      .w_DESCUTEF=space(20)
        .w_TIPOSOG = 'U'
        .w_CODGRUIN = 0
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODGRUIN))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_CODGRUFI = 0
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODGRUFI))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_CODUTEIN = 0
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODUTEIN))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_CODUTEFI = 0
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODUTEFI))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODGRUIN = 0
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODGRUFI = 0
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODUTEIN = 0
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_TIPOSOG<>.w_TIPOSOG
            .w_CODUTEFI = 0
          .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODGRUIN_1_2.visible=!this.oPgFrm.Page1.oPag.oCODGRUIN_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDESCGRUI_1_3.visible=!this.oPgFrm.Page1.oPag.oDESCGRUI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCODGRUFI_1_4.visible=!this.oPgFrm.Page1.oPag.oCODGRUFI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oDESCGRUF_1_5.visible=!this.oPgFrm.Page1.oPag.oDESCGRUF_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCODUTEIN_1_6.visible=!this.oPgFrm.Page1.oPag.oCODUTEIN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDESCUTEI_1_7.visible=!this.oPgFrm.Page1.oPag.oDESCUTEI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODUTEFI_1_8.visible=!this.oPgFrm.Page1.oPag.oCODUTEFI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDESCUTEF_1_9.visible=!this.oPgFrm.Page1.oPag.oDESCUTEF_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGRUIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRUIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRUIN);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRUIN)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRUIN) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRUIN_1_2'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRUIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRUIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRUIN)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRUIN = NVL(_Link_.CODE,0)
      this.w_DESCGRUI = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRUIN = 0
      endif
      this.w_DESCGRUI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice del gruppo iniziale � maggiore di quello finale o inesistente")
        endif
        this.w_CODGRUIN = 0
        this.w_DESCGRUI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRUIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRUFI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRUFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRUFI);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRUFI)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRUFI) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRUFI_1_4'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRUFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRUFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRUFI)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRUFI = NVL(_Link_.CODE,0)
      this.w_DESCGRUF = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRUFI = 0
      endif
      this.w_DESCGRUF = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice gruppo finale � minore di quello iniziale o inesistente")
        endif
        this.w_CODGRUFI = 0
        this.w_DESCGRUF = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRUFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTEIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTEIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTEIN);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTEIN)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTEIN) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTEIN_1_6'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTEIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTEIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTEIN)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTEIN = NVL(_Link_.CODE,0)
      this.w_DESCUTEI = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTEIN = 0
      endif
      this.w_DESCUTEI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice dell'utente iniziale � maggiore di quello finale o inesistente")
        endif
        this.w_CODUTEIN = 0
        this.w_DESCUTEI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTEIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTEFI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTEFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTEFI);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTEFI)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTEFI) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTEFI_1_8'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTEFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTEFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTEFI)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTEFI = NVL(_Link_.CODE,0)
      this.w_DESCUTEF = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTEFI = 0
      endif
      this.w_DESCUTEF = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice utente finale � minore del codice iniziale o inesistente")
        endif
        this.w_CODUTEFI = 0
        this.w_DESCUTEF = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTEFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOSOG_1_1.RadioValue()==this.w_TIPOSOG)
      this.oPgFrm.Page1.oPag.oTIPOSOG_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRUIN_1_2.value==this.w_CODGRUIN)
      this.oPgFrm.Page1.oPag.oCODGRUIN_1_2.value=this.w_CODGRUIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGRUI_1_3.value==this.w_DESCGRUI)
      this.oPgFrm.Page1.oPag.oDESCGRUI_1_3.value=this.w_DESCGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRUFI_1_4.value==this.w_CODGRUFI)
      this.oPgFrm.Page1.oPag.oCODGRUFI_1_4.value=this.w_CODGRUFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGRUF_1_5.value==this.w_DESCGRUF)
      this.oPgFrm.Page1.oPag.oDESCGRUF_1_5.value=this.w_DESCGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTEIN_1_6.value==this.w_CODUTEIN)
      this.oPgFrm.Page1.oPag.oCODUTEIN_1_6.value=this.w_CODUTEIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTEI_1_7.value==this.w_DESCUTEI)
      this.oPgFrm.Page1.oPag.oDESCUTEI_1_7.value=this.w_DESCUTEI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTEFI_1_8.value==this.w_CODUTEFI)
      this.oPgFrm.Page1.oPag.oCODUTEFI_1_8.value=this.w_CODUTEFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTEF_1_9.value==this.w_DESCUTEF)
      this.oPgFrm.Page1.oPag.oDESCUTEF_1_9.value=this.w_DESCUTEF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUFI))  and not(.w_TIPOSOG<>'G')  and not(empty(.w_CODGRUIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODGRUIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice del gruppo iniziale � maggiore di quello finale o inesistente")
          case   not((.w_CODGRUIN<=.w_CODGRUFI) OR EMPTY(.w_CODGRUIN))  and not(.w_TIPOSOG<>'G')  and not(empty(.w_CODGRUFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODGRUFI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice gruppo finale � minore di quello iniziale o inesistente")
          case   not((.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEFI))  and not(.w_TIPOSOG<>'U')  and not(empty(.w_CODUTEIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODUTEIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice dell'utente iniziale � maggiore di quello finale o inesistente")
          case   not((.w_CODUTEIN<=.w_CODUTEFI) OR EMPTY(.w_CODUTEIN))  and not(.w_TIPOSOG<>'U')  and not(empty(.w_CODUTEFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODUTEFI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice utente finale � minore del codice iniziale o inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOSOG = this.w_TIPOSOG
    return

enddefine

* --- Define pages as container
define class tgsdm_spvPag1 as StdContainer
  Width  = 514
  height = 171
  stdWidth  = 514
  stdheight = 171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOSOG_1_1 as StdCombo with uid="JAZVMLTCMO",value=4,rtseq=1,rtrep=.f.,left=128,top=11,width=103,height=21;
    , ToolTipText = "Tipo filtro (utente/gruppo/entrambi)";
    , HelpContextID = 65369546;
    , cFormVar="w_TIPOSOG",RowSource=""+"Utente,"+"Gruppo,"+"Default,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSOG_1_1.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    iif(this.value =3,'D',;
    iif(this.value =4,' ',;
    space(1))))))
  endfunc
  func oTIPOSOG_1_1.GetRadio()
    this.Parent.oContained.w_TIPOSOG = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSOG_1_1.SetRadio()
    this.Parent.oContained.w_TIPOSOG=trim(this.Parent.oContained.w_TIPOSOG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSOG=='U',1,;
      iif(this.Parent.oContained.w_TIPOSOG=='G',2,;
      iif(this.Parent.oContained.w_TIPOSOG=='D',3,;
      iif(this.Parent.oContained.w_TIPOSOG=='',4,;
      0))))
  endfunc

  add object oCODGRUIN_1_2 as StdField with uid="RGUATXEEUW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODGRUIN", cQueryName = "CODGRUIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice del gruppo iniziale � maggiore di quello finale o inesistente",;
    ToolTipText = "Codice gruppo iniziale",;
    HelpContextID = 234762380,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=128, Top=38, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRUIN"

  func oCODGRUIN_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  func oCODGRUIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRUIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRUIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRUIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESCGRUI_1_3 as StdField with uid="DPVMJNUWZC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCGRUI", cQueryName = "DESCGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente iniziale",;
    HelpContextID = 240039295,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=186, Top=38, InputMask=replicate('X',20)

  func oDESCGRUI_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  add object oCODGRUFI_1_4 as StdField with uid="JXHNESXSEP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODGRUFI", cQueryName = "CODGRUFI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice gruppo finale � minore di quello iniziale o inesistente",;
    ToolTipText = "Codice gruppo finale",;
    HelpContextID = 234762385,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=128, Top=66, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRUFI"

  func oCODGRUFI_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  func oCODGRUFI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRUFI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRUFI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRUFI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESCGRUF_1_5 as StdField with uid="IOZTSZEUAB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCGRUF", cQueryName = "DESCGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente iniziale",;
    HelpContextID = 240039292,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=186, Top=66, InputMask=replicate('X',20)

  func oDESCGRUF_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'G')
    endwith
  endfunc

  add object oCODUTEIN_1_6 as StdField with uid="ABGCYOUKIO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODUTEIN", cQueryName = "CODUTEIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice dell'utente iniziale � maggiore di quello finale o inesistente",;
    ToolTipText = "Codice utente iniziale",;
    HelpContextID = 231747724,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=128, Top=38, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTEIN"

  func oCODUTEIN_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  func oCODUTEIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTEIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTEIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTEIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESCUTEI_1_7 as StdField with uid="ROHBQQPVTV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCUTEI", cQueryName = "DESCUTEI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente iniziale",;
    HelpContextID = 248597121,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=186, Top=38, InputMask=replicate('X',20)

  func oDESCUTEI_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  add object oCODUTEFI_1_8 as StdField with uid="RUNUEDQATC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODUTEFI", cQueryName = "CODUTEFI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice utente finale � minore del codice iniziale o inesistente",;
    ToolTipText = "Codice utente finale",;
    HelpContextID = 231747729,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=128, Top=66, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTEFI"

  func oCODUTEFI_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc

  func oCODUTEFI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTEFI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTEFI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTEFI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESCUTEF_1_9 as StdField with uid="BIVTHBWVLD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCUTEF", cQueryName = "DESCUTEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente iniziale",;
    HelpContextID = 248597124,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=186, Top=66, InputMask=replicate('X',20)

  func oDESCUTEF_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG<>'U')
    endwith
  endfunc


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=128, top=97, width=378,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 36848358


  add object oBtn_1_11 as StdButton with uid="WDHBUTUXIR",left=401, top=122, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 640294;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="FGOJEBDDCO",left=456, top=122, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 133831866;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_13 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=4, Top=100,;
    Alignment=1, Width=119, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RCEYAMNBEB",Visible=.t., Left=37, Top=14,;
    Alignment=1, Width=86, Height=18,;
    Caption="Soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="HTQXSMVPJA",Visible=.t., Left=32, Top=41,;
    Alignment=1, Width=91, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG $('D '))
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="CNRTWANVBI",Visible=.t., Left=31, Top=67,;
    Alignment=1, Width=92, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPOSOG $('D '))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_spv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
