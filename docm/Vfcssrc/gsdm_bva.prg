* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bva                                                        *
*              Gestione abilitazioni intestatari                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-22                                                      *
* Last revis.: 2014-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPERAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bva",oParentObject,m.w_OPERAZIONE)
return(i_retval)

define class tgsdm_bva as StdBatch
  * --- Local variables
  w_OPERAZIONE = space(10)
  w_PUNPAD = .NULL.
  w_NC = space(10)
  w_COUNTER = 0
  w_ANTIPCON = space(1)
  w_ANCODCON = space(15)
  w_TMPN = 0
  w_TIPOMSG = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  AGENTI_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamato da GSDM_KVA
    * --- Dichiarazione Variabili
    this.w_PUNPAD = this.oParentObject
    do case
      case this.w_OPERAZIONE="UPDATE"
        * --- Gestione anagrafiche MULTIPLE ---------------------------------------------------------------------------
        this.w_NC = this.w_PUNPAD.w_ZoomAnag.cCursor
        * --- Messaggio
        do case
          case this.oParentObject.w_TIPCON="A"
            this.w_TIPOMSG = "agenti"
          case this.oParentObject.w_TIPCON="N"
            this.w_TIPOMSG = "nominativi"
          case this.oParentObject.w_TIPCON="B"
            this.w_TIPOMSG = "debitori creditori diversi"
        endcase
        if  "AAAAAA" = this.oParentObject.w_STAMPA+this.oParentObject.w_EMAIL+this.oParentObject.w_EMPEC+this.oParentObject.w_FAX+this.oParentObject.w_CPZ+this.oParentObject.w_POSTEL
          ah_ERRORMSG("Non sono stati selezionati valori da aggiornare",64)
          i_retcode = 'stop'
          return
        endif
        if used(this.w_NC)
          * --- Conta ....
          this.w_COUNTER = 0
          select (this.w_NC)
          COUNT to this.w_COUNTER for xCHK=1
          go top
          if this.w_COUNTER=0
            * --- Messaggio
            ah_ERRORMSG("Non sono stati selezionati %1 da aggiornare",64,,this.w_TIPOMSG)
            i_retcode = 'stop'
            return
          else
            * --- Chiede conferma ....
            if not ah_YESNO("Confermi richiesta di aggiornamento dell'anagrafica %1?%0Eventuali impostazioni preesistenti saranno sovrascritte",, this.w_TIPOMSG)
              i_retcode = 'stop'
              return
            endif
            * --- Seleziona tutte le righe dello zoom
            this.w_COUNTER = 0
            select (this.w_NC)
            go top
            scan for xChk=1
            * --- Incrementa contatore
            this.w_COUNTER = this.w_COUNTER + 1
            * --- Prepara variabili
            this.w_ANTIPCON = NVL(ANTIPCON," ")
            this.w_ANCODCON = NVL(ANCODICE," ")
            if this.w_ANTIPCON="A"
              * --- Write into AGENTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.AGENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
                i_cOp1=cp_SetTrsOp(iif(this.oParentObject.w_STAMPA="A"," ","="),'AGCHKSTA','this.oParentObject.w_STAMPA',this.oParentObject.w_STAMPA,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(iif(this.oParentObject.w_EMAIL="A"," ","="),'AGCHKMAI','this.oParentObject.w_EMAIL',this.oParentObject.w_EMAIL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(iif(this.oParentObject.w_EMPEC="A"," ","="),'AGCHKPEC','this.oParentObject.w_EMPEC',this.oParentObject.w_EMPEC,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(iif(this.oParentObject.w_FAX="A"," ","="),'AGCHKFAX','this.oParentObject.w_FAX',this.oParentObject.w_FAX,'update',i_nConn)
                i_cOp5=cp_SetTrsOp(iif(this.oParentObject.w_POSTEL="A"," ","="),'AGCHKPTL','this.oParentObject.w_POSTEL',this.oParentObject.w_POSTEL,'update',i_nConn)
                i_cOp6=cp_SetTrsOp(iif(this.oParentObject.w_CPZ="A"," ","="),'AGCHKCPZ','this.oParentObject.w_CPZ',this.oParentObject.w_CPZ,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.AGENTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"AGCHKSTA ="+cp_NullLink(i_cOp1,'AGENTI','AGCHKSTA');
                +",AGCHKMAI ="+cp_NullLink(i_cOp2,'AGENTI','AGCHKMAI');
                +",AGCHKPEC ="+cp_NullLink(i_cOp3,'AGENTI','AGCHKPEC');
                +",AGCHKFAX ="+cp_NullLink(i_cOp4,'AGENTI','AGCHKFAX');
                +",AGCHKPTL ="+cp_NullLink(i_cOp5,'AGENTI','AGCHKPTL');
                +",AGCHKCPZ ="+cp_NullLink(i_cOp6,'AGENTI','AGCHKCPZ');
                    +i_ccchkf ;
                +" where ";
                    +"AGCODAGE = "+cp_ToStrODBC(this.w_ANCODCON);
                       )
              else
                update (i_cTable) set;
                    AGCHKSTA = &i_cOp1.;
                    ,AGCHKMAI = &i_cOp2.;
                    ,AGCHKPEC = &i_cOp3.;
                    ,AGCHKFAX = &i_cOp4.;
                    ,AGCHKPTL = &i_cOp5.;
                    ,AGCHKCPZ = &i_cOp6.;
                    &i_ccchkf. ;
                 where;
                    AGCODAGE = this.w_ANCODCON;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Write into OFF_NOMI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
                i_cOp1=cp_SetTrsOp(iif(this.oParentObject.w_STAMPA="A"," ","="),'NOCHKSTA','this.oParentObject.w_STAMPA',this.oParentObject.w_STAMPA,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(iif(this.oParentObject.w_EMAIL="A"," ","="),'NOCHKMAI','this.oParentObject.w_EMAIL',this.oParentObject.w_EMAIL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(iif(this.oParentObject.w_EMPEC="A"," ","="),'NOCHKPEC','this.oParentObject.w_EMPEC',this.oParentObject.w_EMPEC,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(iif(this.oParentObject.w_FAX="A"," ","="),'NOCHKFAX','this.oParentObject.w_FAX',this.oParentObject.w_FAX,'update',i_nConn)
                i_cOp5=cp_SetTrsOp(iif(this.oParentObject.w_POSTEL="A"," ","="),'NOCHKPTL','this.oParentObject.w_POSTEL',this.oParentObject.w_POSTEL,'update',i_nConn)
                i_cOp6=cp_SetTrsOp(iif(this.oParentObject.w_CPZ="A"," ","="),'NOCHKCPZ','this.oParentObject.w_CPZ',this.oParentObject.w_CPZ,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NOCHKSTA ="+cp_NullLink(i_cOp1,'OFF_NOMI','NOCHKSTA');
                +",NOCHKMAI ="+cp_NullLink(i_cOp2,'OFF_NOMI','NOCHKMAI');
                +",NOCHKPEC ="+cp_NullLink(i_cOp3,'OFF_NOMI','NOCHKPEC');
                +",NOCHKFAX ="+cp_NullLink(i_cOp4,'OFF_NOMI','NOCHKFAX');
                +",NOCHKPTL ="+cp_NullLink(i_cOp5,'OFF_NOMI','NOCHKPTL');
                +",NOCHKCPZ ="+cp_NullLink(i_cOp6,'OFF_NOMI','NOCHKCPZ');
                    +i_ccchkf ;
                +" where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_ANCODCON);
                       )
              else
                update (i_cTable) set;
                    NOCHKSTA = &i_cOp1.;
                    ,NOCHKMAI = &i_cOp2.;
                    ,NOCHKPEC = &i_cOp3.;
                    ,NOCHKFAX = &i_cOp4.;
                    ,NOCHKPTL = &i_cOp5.;
                    ,NOCHKCPZ = &i_cOp6.;
                    &i_ccchkf. ;
                 where;
                    NOCODICE = this.w_ANCODCON;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            endscan
          endif
          * --- Lancia programma di creazione descrittore XML
          if this.w_COUNTER>0
            * --- Seleziona tutte le righe dello zoom
            this.oParentObject.w_SELEZI = "D"
            * --- Punta al padre
            this.w_PUNPAD.NotifyEvent("Interroga")     
            * --- Messaggio
            ah_ERRORMSG("Aggiornamento abilitazioni %1 completata con successo [%2]",64,"", this.w_TIPOMSG, alltrim(str(this.w_COUNTER,10,0)))
          else
            * --- Messaggio
            ah_ERRORMSG("Non sono stati selezionati %1 da aggiornare",64,, this.w_TIPOMSG)
          endif
        endif
      case this.w_OPERAZIONE="SELDESEL"
        * --- Punta al padre
        this.w_NC = this.w_PUNPAD.w_ZoomAnag.cCursor
        if used(this.w_NC)
          * --- Seleziona tutte le righe dello zoom
          UPDATE (this.w_NC) SET xChk=iif(this.oParentObject.w_SELEZI="S",1,0)
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_OPERAZIONE)
    this.w_OPERAZIONE=w_OPERAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='OFF_NOMI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPERAZIONE"
endproc
