* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_mpv                                                        *
*              Preferenze di ricerca                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-26                                                      *
* Last revis.: 2011-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_mpv"))

* --- Class definition
define class tgsdm_mpv as StdTrsForm
  Top    = 3
  Left   = 9

  * --- Standard Properties
  Width  = 755
  Height = 285+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-03"
  HelpContextID=147440745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRO_PVIS_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  PROMCLAS_IDX = 0
  cFile = "PRO_PVIS"
  cKeySelect = "PVNOMTAB"
  cKeyWhere  = "PVNOMTAB=this.w_PVNOMTAB"
  cKeyDetail  = "PVNOMTAB=this.w_PVNOMTAB"
  cKeyWhereODBC = '"PVNOMTAB="+cp_ToStrODBC(this.w_PVNOMTAB)';

  cKeyDetailWhereODBC = '"PVNOMTAB="+cp_ToStrODBC(this.w_PVNOMTAB)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PRO_PVIS.PVNOMTAB="+cp_ToStrODBC(this.w_PVNOMTAB)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRO_PVIS.CPROWNUM '
  cPrg = "gsdm_mpv"
  cComment = "Preferenze di ricerca"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PVNOMTAB = space(20)
  w_PVTIPFIL = space(1)
  o_PVTIPFIL = space(1)
  w_PVCODUTE = 0
  w_PVCODGRU = 0
  w_PVCODCLA = space(15)
  w_PVPERVIS = 0
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_DCLASSE = space(40)
  w_PVSELUTE = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRO_PVIS','gsdm_mpv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_mpvPag1","gsdm_mpv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Preferenze")
      .Pages(1).HelpContextID = 823292
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPVNOMTAB_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='PROMCLAS'
    this.cWorkTables[4]='PRO_PVIS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_PVIS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_PVIS_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PVNOMTAB = NVL(PVNOMTAB,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRO_PVIS where PVNOMTAB=KeySet.PVNOMTAB
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2],this.bLoadRecFilter,this.PRO_PVIS_IDX,"gsdm_mpv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_PVIS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_PVIS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_PVIS '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PVNOMTAB',this.w_PVNOMTAB  )
      select * from (i_cTable) PRO_PVIS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PVNOMTAB = NVL(PVNOMTAB,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRO_PVIS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(20)
          .w_DESGRU = space(20)
          .w_DCLASSE = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PVTIPFIL = NVL(PVTIPFIL,space(1))
          .w_PVCODUTE = NVL(PVCODUTE,0)
          if link_2_2_joined
            this.w_PVCODUTE = NVL(CODE202,NVL(this.w_PVCODUTE,0))
            this.w_DESUTE = NVL(NAME202,space(20))
          else
          .link_2_2('Load')
          endif
          .w_PVCODGRU = NVL(PVCODGRU,0)
          if link_2_3_joined
            this.w_PVCODGRU = NVL(CODE203,NVL(this.w_PVCODGRU,0))
            this.w_DESGRU = NVL(NAME203,space(20))
          else
          .link_2_3('Load')
          endif
          .w_PVCODCLA = NVL(PVCODCLA,space(15))
          if link_2_4_joined
            this.w_PVCODCLA = NVL(CDCODCLA204,NVL(this.w_PVCODCLA,space(15)))
            this.w_DCLASSE = NVL(CDDESCLA204,space(40))
          else
          .link_2_4('Load')
          endif
          .w_PVPERVIS = NVL(PVPERVIS,0)
          .w_PVSELUTE = NVL(PVSELUTE,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PVNOMTAB=space(20)
      .w_PVTIPFIL=space(1)
      .w_PVCODUTE=0
      .w_PVCODGRU=0
      .w_PVCODCLA=space(15)
      .w_PVPERVIS=0
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_DCLASSE=space(40)
      .w_PVSELUTE=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_PVTIPFIL = 'U'
        .w_PVCODUTE = IIF(.w_PVTIPFIL='U',.w_PVCODUTE, 0)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PVCODUTE))
         .link_2_2('Full')
        endif
        .w_PVCODGRU = IIF(.w_PVTIPFIL='G',.w_PVCODGRU, 0)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PVCODGRU))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PVCODCLA))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,9,.f.)
        .w_PVSELUTE = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_PVIS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPVNOMTAB_1_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPVNOMTAB_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPVNOMTAB_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PRO_PVIS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVNOMTAB,"PVNOMTAB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2])
    i_lTable = "PRO_PVIS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRO_PVIS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSDM_SPV with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PVTIPFIL N(3);
      ,t_PVCODUTE N(4);
      ,t_PVCODGRU N(4);
      ,t_PVCODCLA C(15);
      ,t_PVPERVIS N(3);
      ,t_DCLASSE C(40);
      ,t_PVSELUTE N(3);
      ,CPROWNUM N(10);
      ,t_DESUTE C(20);
      ,t_DESGRU C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsdm_mpvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1.controlsource=this.cTrsName+'.t_PVTIPFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODUTE_2_2.controlsource=this.cTrsName+'.t_PVCODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODGRU_2_3.controlsource=this.cTrsName+'.t_PVCODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODCLA_2_4.controlsource=this.cTrsName+'.t_PVCODCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVPERVIS_2_5.controlsource=this.cTrsName+'.t_PVPERVIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCLASSE_2_8.controlsource=this.cTrsName+'.t_DCLASSE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVSELUTE_2_9.controlsource=this.cTrsName+'.t_PVSELUTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(92)
    this.AddVLine(152)
    this.AddVLine(214)
    this.AddVLine(352)
    this.AddVLine(541)
    this.AddVLine(699)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2])
      *
      * insert into PRO_PVIS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_PVIS')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_PVIS')
        i_cFldBody=" "+;
                  "(PVNOMTAB,PVTIPFIL,PVCODUTE,PVCODGRU,PVCODCLA"+;
                  ",PVPERVIS,PVSELUTE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PVNOMTAB)+","+cp_ToStrODBC(this.w_PVTIPFIL)+","+cp_ToStrODBCNull(this.w_PVCODUTE)+","+cp_ToStrODBCNull(this.w_PVCODGRU)+","+cp_ToStrODBCNull(this.w_PVCODCLA)+;
             ","+cp_ToStrODBC(this.w_PVPERVIS)+","+cp_ToStrODBC(this.w_PVSELUTE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_PVIS')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_PVIS')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PVNOMTAB',this.w_PVNOMTAB)
        INSERT INTO (i_cTable) (;
                   PVNOMTAB;
                  ,PVTIPFIL;
                  ,PVCODUTE;
                  ,PVCODGRU;
                  ,PVCODCLA;
                  ,PVPERVIS;
                  ,PVSELUTE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PVNOMTAB;
                  ,this.w_PVTIPFIL;
                  ,this.w_PVCODUTE;
                  ,this.w_PVCODGRU;
                  ,this.w_PVCODCLA;
                  ,this.w_PVPERVIS;
                  ,this.w_PVSELUTE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PVTIPFIL)) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_PVIS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_PVIS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PVTIPFIL)) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRO_PVIS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_PVIS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PVTIPFIL="+cp_ToStrODBC(this.w_PVTIPFIL)+;
                     ",PVCODUTE="+cp_ToStrODBCNull(this.w_PVCODUTE)+;
                     ",PVCODGRU="+cp_ToStrODBCNull(this.w_PVCODGRU)+;
                     ",PVCODCLA="+cp_ToStrODBCNull(this.w_PVCODCLA)+;
                     ",PVPERVIS="+cp_ToStrODBC(this.w_PVPERVIS)+;
                     ",PVSELUTE="+cp_ToStrODBC(this.w_PVSELUTE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_PVIS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PVTIPFIL=this.w_PVTIPFIL;
                     ,PVCODUTE=this.w_PVCODUTE;
                     ,PVCODGRU=this.w_PVCODGRU;
                     ,PVCODCLA=this.w_PVCODCLA;
                     ,PVPERVIS=this.w_PVPERVIS;
                     ,PVSELUTE=this.w_PVSELUTE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PVTIPFIL)) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRO_PVIS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PVTIPFIL)) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_PVIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PVIS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_PVTIPFIL<>.w_PVTIPFIL
          .w_PVCODUTE = IIF(.w_PVTIPFIL='U',.w_PVCODUTE, 0)
          .link_2_2('Full')
        endif
        if .o_PVTIPFIL<>.w_PVTIPFIL
          .w_PVCODGRU = IIF(.w_PVTIPFIL='G',.w_PVCODGRU, 0)
          .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESUTE with this.w_DESUTE
      replace t_DESGRU with this.w_DESGRU
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVCODUTE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVCODUTE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVCODGRU_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVCODGRU_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PVCODUTE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PVCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PVCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PVCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oPVCODUTE_2_2'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PVCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PVCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PVCODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PVCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CODE as CODE202"+ ",link_2_2.NAME as NAME202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on PRO_PVIS.PVCODUTE=link_2_2.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and PRO_PVIS.PVCODUTE=link_2_2.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PVCODGRU
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PVCODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PVCODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PVCODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oPVCODGRU_2_3'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PVCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PVCODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PVCODGRU = NVL(_Link_.CODE,0)
      this.w_DESGRU = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PVCODGRU = 0
      endif
      this.w_DESGRU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPGROUPS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CODE as CODE203"+ ",link_2_3.NAME as NAME203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on PRO_PVIS.PVCODGRU=link_2_3.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and PRO_PVIS.PVCODGRU=link_2_3.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PVCODCLA
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_PVCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_PVCODCLA))
          select CDCODCLA,CDDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PVCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PVCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oPVCODCLA_2_4'),i_cWhere,'GSUT_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_PVCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_PVCODCLA)
            select CDCODCLA,CDDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PVCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_DCLASSE = NVL(_Link_.CDDESCLA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PVCODCLA = space(15)
      endif
      this.w_DCLASSE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CDCODCLA as CDCODCLA204"+ ",link_2_4.CDDESCLA as CDDESCLA204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on PRO_PVIS.PVCODCLA=link_2_4.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and PRO_PVIS.PVCODCLA=link_2_4.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPVNOMTAB_1_1.value==this.w_PVNOMTAB)
      this.oPgFrm.Page1.oPag.oPVNOMTAB_1_1.value=this.w_PVNOMTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1.RadioValue()==this.w_PVTIPFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1.SetRadio()
      replace t_PVTIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODUTE_2_2.value==this.w_PVCODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODUTE_2_2.value=this.w_PVCODUTE
      replace t_PVCODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODUTE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODGRU_2_3.value==this.w_PVCODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODGRU_2_3.value=this.w_PVCODGRU
      replace t_PVCODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODGRU_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODCLA_2_4.value==this.w_PVCODCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODCLA_2_4.value=this.w_PVCODCLA
      replace t_PVCODCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVCODCLA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVPERVIS_2_5.RadioValue()==this.w_PVPERVIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVPERVIS_2_5.SetRadio()
      replace t_PVPERVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVPERVIS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCLASSE_2_8.value==this.w_DCLASSE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCLASSE_2_8.value=this.w_DCLASSE
      replace t_DCLASSE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCLASSE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSELUTE_2_9.RadioValue()==this.w_PVSELUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSELUTE_2_9.SetRadio()
      replace t_PVSELUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSELUTE_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'PRO_PVIS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PVNOMTAB))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPVNOMTAB_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PVNOMTAB)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_PVTIPFIL)) );
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_PVTIPFIL)) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PVTIPFIL = this.w_PVTIPFIL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PVTIPFIL)) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PVTIPFIL=space(1)
      .w_PVCODUTE=0
      .w_PVCODGRU=0
      .w_PVCODCLA=space(15)
      .w_PVPERVIS=0
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_DCLASSE=space(40)
      .w_PVSELUTE=space(1)
      .DoRTCalc(1,1,.f.)
        .w_PVTIPFIL = 'U'
        .w_PVCODUTE = IIF(.w_PVTIPFIL='U',.w_PVCODUTE, 0)
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_PVCODUTE))
        .link_2_2('Full')
      endif
        .w_PVCODGRU = IIF(.w_PVTIPFIL='G',.w_PVCODGRU, 0)
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_PVCODGRU))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_PVCODCLA))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,9,.f.)
        .w_PVSELUTE = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PVTIPFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1.RadioValue(.t.)
    this.w_PVCODUTE = t_PVCODUTE
    this.w_PVCODGRU = t_PVCODGRU
    this.w_PVCODCLA = t_PVCODCLA
    this.w_PVPERVIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVPERVIS_2_5.RadioValue(.t.)
    this.w_DESUTE = t_DESUTE
    this.w_DESGRU = t_DESGRU
    this.w_DCLASSE = t_DCLASSE
    this.w_PVSELUTE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSELUTE_2_9.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PVTIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVTIPFIL_2_1.ToRadio()
    replace t_PVCODUTE with this.w_PVCODUTE
    replace t_PVCODGRU with this.w_PVCODGRU
    replace t_PVCODCLA with this.w_PVCODCLA
    replace t_PVPERVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVPERVIS_2_5.ToRadio()
    replace t_DESUTE with this.w_DESUTE
    replace t_DESGRU with this.w_DESGRU
    replace t_DCLASSE with this.w_DCLASSE
    replace t_PVSELUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSELUTE_2_9.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsdm_mpvPag1 as StdContainer
  Width  = 751
  height = 285
  stdWidth  = 751
  stdheight = 285
  resizeXpos=380
  resizeYpos=181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPVNOMTAB_1_1 as StdField with uid="GUMDWFRSSH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PVNOMTAB", cQueryName = "PVNOMTAB",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome tabella di riferimento",;
    HelpContextID = 5928760,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=132, Left=73, Top=8, InputMask=replicate('X',20)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=34, width=744,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="PVTIPFIL",Label1="Soggetto",Field2="PVCODUTE",Label2="Utente",Field3="PVCODGRU",Label3="Gruppo",Field4="PVCODCLA",Label4="Classe documentale",Field5="DCLASSE",Label5="Descrizione",Field6="PVPERVIS",Label6="Filtro date",Field7="PVSELUTE",Label7="Utente",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168650106

  add object oStr_1_2 as StdString with uid="YDNUJWMULQ",Visible=.t., Left=18, Top=11,;
    Alignment=1, Width=50, Height=18,;
    Caption="Tabella:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=56,;
    width=740+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=57,width=739+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPUSERS|CPGROUPS|PROMCLAS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oPVCODUTE_2_2
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oPVCODGRU_2_3
      case cFile='PROMCLAS'
        oDropInto=this.oBodyCol.oRow.oPVCODCLA_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsdm_mpvBodyRow as CPBodyRowCnt
  Width=730
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPVTIPFIL_2_1 as StdTrsCombo with uid="FXEZSUYKVH",rtrep=.t.,;
    cFormVar="w_PVTIPFIL", RowSource=""+"Utente,"+"Gruppo,"+"Default" , ;
    HelpContextID = 226175166,;
    Height=21, Width=83, Left=-2, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPVTIPFIL_2_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PVTIPFIL,&i_cF..t_PVTIPFIL),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    iif(xVal =3,'D',;
    space(1)))))
  endfunc
  func oPVTIPFIL_2_1.GetRadio()
    this.Parent.oContained.w_PVTIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oPVTIPFIL_2_1.ToRadio()
    this.Parent.oContained.w_PVTIPFIL=trim(this.Parent.oContained.w_PVTIPFIL)
    return(;
      iif(this.Parent.oContained.w_PVTIPFIL=='U',1,;
      iif(this.Parent.oContained.w_PVTIPFIL=='G',2,;
      iif(this.Parent.oContained.w_PVTIPFIL=='D',3,;
      0))))
  endfunc

  func oPVTIPFIL_2_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPVCODUTE_2_2 as StdTrsField with uid="HZGGWJFOBA",rtseq=3,rtrep=.t.,;
    cFormVar="w_PVCODUTE",value=0,;
    HelpContextID = 13223739,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=87, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_PVCODUTE"

  func oPVCODUTE_2_2.mCond()
    with this.Parent.oContained
      return (.w_PVTIPFIL='U')
    endwith
  endfunc

  func oPVCODUTE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVCODUTE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPVCODUTE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oPVCODUTE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPVCODGRU_2_3 as StdTrsField with uid="FXJSDZUIVQ",rtseq=4,rtrep=.t.,;
    cFormVar="w_PVCODGRU",value=0,;
    HelpContextID = 46778187,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=147, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_PVCODGRU"

  func oPVCODGRU_2_3.mCond()
    with this.Parent.oContained
      return (.w_PVTIPFIL='G')
    endwith
  endfunc

  func oPVCODGRU_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVCODGRU_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPVCODGRU_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oPVCODGRU_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oPVCODCLA_2_4 as StdTrsField with uid="XSJOIKFFFP",rtseq=5,rtrep=.t.,;
    cFormVar="w_PVCODCLA",value=space(15),;
    ToolTipText = "Classe documentale",;
    HelpContextID = 20330697,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=210, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_PVCODCLA"

  func oPVCODCLA_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVCODCLA_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPVCODCLA_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oPVCODCLA_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oPVCODCLA_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_PVCODCLA
    i_obj.ecpSave()
  endproc

  add object oPVPERVIS_2_5 as StdTrsCombo with uid="JWFOIBLNEP",rtrep=.t.,;
    cFormVar="w_PVPERVIS", RowSource=""+"Ultimi 7 giorni,"+"Ultimi 15 giorni,"+"Ultimo mese,"+"Ultimo trimestre,"+"Ultimo semestre,"+"Ultimo anno,"+"Nessun filtro" , ;
    HelpContextID = 224356535,;
    Height=21, Width=148, Left=537, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPVPERVIS_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PVPERVIS,&i_cF..t_PVPERVIS),this.value)
    return(iif(xVal =1,7,;
    iif(xVal =2,15,;
    iif(xVal =3,30,;
    iif(xVal =4,90,;
    iif(xVal =5,180,;
    iif(xVal =6,360,;
    iif(xVal =7,1,;
    0))))))))
  endfunc
  func oPVPERVIS_2_5.GetRadio()
    this.Parent.oContained.w_PVPERVIS = this.RadioValue()
    return .t.
  endfunc

  func oPVPERVIS_2_5.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_PVPERVIS==7,1,;
      iif(this.Parent.oContained.w_PVPERVIS==15,2,;
      iif(this.Parent.oContained.w_PVPERVIS==30,3,;
      iif(this.Parent.oContained.w_PVPERVIS==90,4,;
      iif(this.Parent.oContained.w_PVPERVIS==180,5,;
      iif(this.Parent.oContained.w_PVPERVIS==360,6,;
      iif(this.Parent.oContained.w_PVPERVIS==1,7,;
      0))))))))
  endfunc

  func oPVPERVIS_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDCLASSE_2_8 as StdTrsField with uid="WMKKVOIVDO",rtseq=9,rtrep=.t.,;
    cFormVar="w_DCLASSE",value=space(40),enabled=.f.,;
    HelpContextID = 262947638,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=182, Left=347, Top=0, InputMask=replicate('X',40)

  add object oPVSELUTE_2_9 as StdTrsCheck with uid="FBWGNWXXWY",rtrep=.t.,;
    cFormVar="w_PVSELUTE",  caption="",;
    HelpContextID = 21022523,;
    Left=697, Top=0, Width=28,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , TABSTOP=.F. , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPVSELUTE_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PVSELUTE,&i_cF..t_PVSELUTE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPVSELUTE_2_9.GetRadio()
    this.Parent.oContained.w_PVSELUTE = this.RadioValue()
    return .t.
  endfunc

  func oPVSELUTE_2_9.ToRadio()
    this.Parent.oContained.w_PVSELUTE=trim(this.Parent.oContained.w_PVSELUTE)
    return(;
      iif(this.Parent.oContained.w_PVSELUTE=='S',1,;
      0))
  endfunc

  func oPVSELUTE_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oPVTIPFIL_2_1.When()
    return(.t.)
  proc oPVTIPFIL_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPVTIPFIL_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_mpv','PRO_PVIS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PVNOMTAB=PRO_PVIS.PVNOMTAB";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
