* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kii                                                        *
*              Creazione file evidenze                                         *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-03                                                      *
* Last revis.: 2016-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kii",oParentObject))

* --- Class definition
define class tgsdm_kii as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 616
  Height = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-01-27"
  HelpContextID=1457047
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  CLASMSOS_IDX = 0
  cPrg = "gsdm_kii"
  cComment = "Creazione file evidenze"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COCLASOS = space(10)
  w_RINVIO = space(1)
  w_INVDOCFIR = space(1)
  w_DESCLA = space(50)
  w_OBSOLETA = space(1)
  w_SELEZIONE = space(1)
  w_INDSOS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kiiPag1","gsdm_kii",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOCLASOS_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_INDSOS = this.oPgFrm.Pages(1).oPag.INDSOS
    DoDefault()
    proc Destroy()
      this.w_INDSOS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CLASMSOS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COCLASOS=space(10)
      .w_RINVIO=space(1)
      .w_INVDOCFIR=space(1)
      .w_DESCLA=space(50)
      .w_OBSOLETA=space(1)
      .w_SELEZIONE=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_COCLASOS))
          .link_1_2('Full')
        endif
        .w_RINVIO = 'N'
        .w_INVDOCFIR = 'N'
      .oPgFrm.Page1.oPag.INDSOS.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(4,5,.f.)
        .w_SELEZIONE = 'D'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.INDSOS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.INDSOS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.INDSOS.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCLASOS
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLASMSOS_IDX,3]
    i_lTable = "CLASMSOS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2], .t., this.CLASMSOS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCLASOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLASMSOS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SZCODCLA like "+cp_ToStrODBC(trim(this.w_COCLASOS)+"%");

          i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA,SZCLAOBS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SZCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SZCODCLA',trim(this.w_COCLASOS))
          select SZCODCLA,SZDESCLA,SZCLAOBS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SZCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCLASOS)==trim(_Link_.SZCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCLASOS) and !this.bDontReportError
            deferred_cp_zoom('CLASMSOS','*','SZCODCLA',cp_AbsName(oSource.parent,'oCOCLASOS_1_2'),i_cWhere,'',"Elenco Classi SOS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA,SZCLAOBS";
                     +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',oSource.xKey(1))
            select SZCODCLA,SZDESCLA,SZCLAOBS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCLASOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA,SZCLAOBS";
                   +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(this.w_COCLASOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',this.w_COCLASOS)
            select SZCODCLA,SZDESCLA,SZCLAOBS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCLASOS = NVL(_Link_.SZCODCLA,space(10))
      this.w_DESCLA = NVL(_Link_.SZDESCLA,space(50))
      this.w_OBSOLETA = NVL(_Link_.SZCLAOBS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCLASOS = space(10)
      endif
      this.w_DESCLA = space(50)
      this.w_OBSOLETA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])+'\'+cp_ToStr(_Link_.SZCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLASMSOS_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !(.w_OBSOLTA = 'S')
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione: la classe selezionata � obsoleta!")))
          if not(i_bRes)
            this.w_COCLASOS = space(10)
            this.w_DESCLA = space(50)
            this.w_OBSOLETA = space(1)
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCLASOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOCLASOS_1_2.value==this.w_COCLASOS)
      this.oPgFrm.Page1.oPag.oCOCLASOS_1_2.value=this.w_COCLASOS
    endif
    if not(this.oPgFrm.Page1.oPag.oRINVIO_1_3.RadioValue()==this.w_RINVIO)
      this.oPgFrm.Page1.oPag.oRINVIO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINVDOCFIR_1_4.RadioValue()==this.w_INVDOCFIR)
      this.oPgFrm.Page1.oPag.oINVDOCFIR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_5.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_5.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZIONE_1_10.RadioValue()==this.w_SELEZIONE)
      this.oPgFrm.Page1.oPag.oSELEZIONE_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_kiiPag1 as StdContainer
  Width  = 612
  height = 398
  stdWidth  = 612
  stdheight = 398
  resizeXpos=317
  resizeYpos=218
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCLASOS_1_2 as StdField with uid="VNFDVSZXHM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COCLASOS", cQueryName = "COCLASOS",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe documentale SOS, per estrazione indici da inviare ",;
    HelpContextID = 125222777,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=100, Top=14, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLASMSOS", oKey_1_1="SZCODCLA", oKey_1_2="this.w_COCLASOS"

  func oCOCLASOS_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if bRes and !(.w_OBSOLTA = 'S')
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione: la classe selezionata � obsoleta!")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oCOCLASOS_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCLASOS_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLASMSOS','*','SZCODCLA',cp_AbsName(this.parent,'oCOCLASOS_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco Classi SOS",'',this.parent.oContained
  endproc

  add object oRINVIO_1_3 as StdCheck with uid="YTWHFQNSZX",rtseq=2,rtrep=.f.,left=100, top=39, caption="Rinvio indici",;
    ToolTipText = "Se selezionato permette di rinviare gli indici gi� 'Inviati'",;
    HelpContextID = 67201558,;
    cFormVar="w_RINVIO", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oRINVIO_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRINVIO_1_3.GetRadio()
    this.Parent.oContained.w_RINVIO = this.RadioValue()
    return .t.
  endfunc

  func oRINVIO_1_3.SetRadio()
    this.Parent.oContained.w_RINVIO=trim(this.Parent.oContained.w_RINVIO)
    this.value = ;
      iif(this.Parent.oContained.w_RINVIO=='S',1,;
      0)
  endfunc

  add object oINVDOCFIR_1_4 as StdCheck with uid="JNQKDSSCRC",rtseq=3,rtrep=.f.,left=472, top=39, caption="Invio doc. firm.",;
    ToolTipText = "Se attivo, verranno inviati a SOStitutiva i documenti firmati ove presenti",;
    HelpContextID = 128977937,;
    cFormVar="w_INVDOCFIR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINVDOCFIR_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oINVDOCFIR_1_4.GetRadio()
    this.Parent.oContained.w_INVDOCFIR = this.RadioValue()
    return .t.
  endfunc

  func oINVDOCFIR_1_4.SetRadio()
    this.Parent.oContained.w_INVDOCFIR=trim(this.Parent.oContained.w_INVDOCFIR)
    this.value = ;
      iif(this.Parent.oContained.w_INVDOCFIR=='S',1,;
      0)
  endfunc

  add object oDESCLA_1_5 as StdField with uid="USUSTYYKIQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "descrizone della classe documetnale SOStitutiva",;
    HelpContextID = 102675766,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=199, Top=14, InputMask=replicate('X',50)


  add object INDSOS as cp_szoombox with uid="TUUYLEYQZM",left=5, top=68, width=601,height=276,;
    caption='Indici ',;
   bGlobalFont=.t.,;
    cZoomFile="GSDM_KII",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.t.,cTable="PROMINDI",cMenuFile="",bQueryOnLoad=.f.,cZoomOnZoom="",;
    cEvent = "w_COCLASOS Changed,Aggiorna,w_RINVIO Changed",;
    nPag=1;
    , HelpContextID = 263581318


  add object oObj_1_9 as cp_runprogram with uid="ZBMIVFNYVL",left=1, top=407, width=189,height=35,;
    caption='Verifica attributi classe SOS',;
   bGlobalFont=.t.,;
    prg="GSDM_BII('C')",;
    cEvent = "w_COCLASOS Changed",;
    nPag=1;
    , HelpContextID = 30658257

  add object oSELEZIONE_1_10 as StdRadio with uid="GAZFYPZPND",rtseq=6,rtrep=.f.,left=5, top=350, width=131,height=34;
    , cFormVar="w_SELEZIONE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZIONE_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 251677380
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 251677380
      this.Buttons(2).Top=16
      this.SetAll("Width",129)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZIONE_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZIONE_1_10.GetRadio()
    this.Parent.oContained.w_SELEZIONE = this.RadioValue()
    return .t.
  endfunc

  func oSELEZIONE_1_10.SetRadio()
    this.Parent.oContained.w_SELEZIONE=trim(this.Parent.oContained.w_SELEZIONE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZIONE=='S',1,;
      iif(this.Parent.oContained.w_SELEZIONE=='D',2,;
      0))
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="JJPVZCCDYV",left=193, top=407, width=189,height=35,;
    caption='Gestione selezione/deselezione indici',;
   bGlobalFont=.t.,;
    prg="GSDM_BII('S')",;
    cEvent = "w_SELEZIONE Changed",;
    nPag=1;
    , HelpContextID = 202636513


  add object oBtn_1_12 as StdButton with uid="XPELXZOFEE",left=507, top=348, width=48,height=45,;
    CpPicture="bmp\carlotti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per creare il file evidenze.seq";
    , HelpContextID = 8259995;
    , caption='\<Crea .seq';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        do GSDM_BCZ with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="CKNHTSNWUY",left=558, top=348, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8774470;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="RGEDTYZOJE",Visible=.t., Left=9, Top=15,;
    Alignment=1, Width=87, Height=18,;
    Caption="Cod. classe:"  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="XXJIABDUJA",left=3, top=64, width=603,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kii','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
