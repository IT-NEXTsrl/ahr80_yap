* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_scs                                                        *
*              Stampa classi SOStitutiva                                       *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-20                                                      *
* Last revis.: 2009-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_scs",oParentObject))

* --- Class definition
define class tgsdm_scs as StdForm
  Top    = 10
  Left   = 8

  * --- Standard Properties
  Width  = 601
  Height = 178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-12"
  HelpContextID=90817641
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  CLASMSOS_IDX = 0
  cPrg = "gsdm_scs"
  cComment = "Stampa classi SOStitutiva"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCLASINI = space(10)
  w_CODCLASFIN = space(10)
  w_DATAGGINI = ctod('  /  /  ')
  w_DATAGGFIN = ctod('  /  /  ')
  w_OBSOLETE = space(1)
  w_DESCLAINI = space(50)
  w_DESCLAFIN = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_scsPag1","gsdm_scs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCLASINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CLASMSOS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCLASINI=space(10)
      .w_CODCLASFIN=space(10)
      .w_DATAGGINI=ctod("  /  /  ")
      .w_DATAGGFIN=ctod("  /  /  ")
      .w_OBSOLETE=space(1)
      .w_DESCLAINI=space(50)
      .w_DESCLAFIN=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCLASINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCLASFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_OBSOLETE = 'N'
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
    this.DoRTCalc(6,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLASINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLASMSOS_IDX,3]
    i_lTable = "CLASMSOS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2], .t., this.CLASMSOS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLASINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLASMSOS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SZCODCLA like "+cp_ToStrODBC(trim(this.w_CODCLASINI)+"%");

          i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SZCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SZCODCLA',trim(this.w_CODCLASINI))
          select SZCODCLA,SZDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SZCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLASINI)==trim(_Link_.SZCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLASINI) and !this.bDontReportError
            deferred_cp_zoom('CLASMSOS','*','SZCODCLA',cp_AbsName(oSource.parent,'oCODCLASINI_1_2'),i_cWhere,'',"Elenco Classi SOS ",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',oSource.xKey(1))
            select SZCODCLA,SZDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLASINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(this.w_CODCLASINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',this.w_CODCLASINI)
            select SZCODCLA,SZDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLASINI = NVL(_Link_.SZCODCLA,space(10))
      this.w_DESCLAINI = NVL(_Link_.SZDESCLA,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLASINI = space(10)
      endif
      this.w_DESCLAINI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCLASINI<=.w_CODCLASFIN OR EMPTY(.w_CODCLASFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice della classe documentale iniziale � maggiore di quello finale")
        endif
        this.w_CODCLASINI = space(10)
        this.w_DESCLAINI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])+'\'+cp_ToStr(_Link_.SZCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLASMSOS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLASINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLASFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLASMSOS_IDX,3]
    i_lTable = "CLASMSOS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2], .t., this.CLASMSOS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLASFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLASMSOS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SZCODCLA like "+cp_ToStrODBC(trim(this.w_CODCLASFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SZCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SZCODCLA',trim(this.w_CODCLASFIN))
          select SZCODCLA,SZDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SZCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLASFIN)==trim(_Link_.SZCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLASFIN) and !this.bDontReportError
            deferred_cp_zoom('CLASMSOS','*','SZCODCLA',cp_AbsName(oSource.parent,'oCODCLASFIN_1_3'),i_cWhere,'',"Elenco Classi SOS ",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',oSource.xKey(1))
            select SZCODCLA,SZDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLASFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(this.w_CODCLASFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',this.w_CODCLASFIN)
            select SZCODCLA,SZDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLASFIN = NVL(_Link_.SZCODCLA,space(10))
      this.w_DESCLAFIN = NVL(_Link_.SZDESCLA,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLASFIN = space(10)
      endif
      this.w_DESCLAFIN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCLASINI<=.w_CODCLASFIN OR EMPTY(.w_CODCLASINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice della classe documentale finale � minore di quello iniziale")
        endif
        this.w_CODCLASFIN = space(10)
        this.w_DESCLAFIN = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])+'\'+cp_ToStr(_Link_.SZCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLASMSOS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLASFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCLASINI_1_2.value==this.w_CODCLASINI)
      this.oPgFrm.Page1.oPag.oCODCLASINI_1_2.value=this.w_CODCLASINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLASFIN_1_3.value==this.w_CODCLASFIN)
      this.oPgFrm.Page1.oPag.oCODCLASFIN_1_3.value=this.w_CODCLASFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAGGINI_1_4.value==this.w_DATAGGINI)
      this.oPgFrm.Page1.oPag.oDATAGGINI_1_4.value=this.w_DATAGGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAGGFIN_1_5.value==this.w_DATAGGFIN)
      this.oPgFrm.Page1.oPag.oDATAGGFIN_1_5.value=this.w_DATAGGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSOLETE_1_6.RadioValue()==this.w_OBSOLETE)
      this.oPgFrm.Page1.oPag.oOBSOLETE_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAINI_1_8.value==this.w_DESCLAINI)
      this.oPgFrm.Page1.oPag.oDESCLAINI_1_8.value=this.w_DESCLAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAFIN_1_15.value==this.w_DESCLAFIN)
      this.oPgFrm.Page1.oPag.oDESCLAFIN_1_15.value=this.w_DESCLAFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODCLASINI<=.w_CODCLASFIN OR EMPTY(.w_CODCLASFIN))  and not(empty(.w_CODCLASINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLASINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice della classe documentale iniziale � maggiore di quello finale")
          case   not(.w_CODCLASINI<=.w_CODCLASFIN OR EMPTY(.w_CODCLASINI))  and not(empty(.w_CODCLASFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLASFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice della classe documentale finale � minore di quello iniziale")
          case   not(.w_DATAGGINI<=.w_DATAGGFIN or empty(.w_DATAGGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAGGINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data inizio aggiornamento maggiore della data di fine aggiornamento")
          case   not(.w_DATAGGINI<=.w_DATAGGFIN or empty(.w_DATAGGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAGGFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine aggiornamento minore della data di inizio aggiornamento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_scsPag1 as StdContainer
  Width  = 597
  height = 178
  stdWidth  = 597
  stdheight = 178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCLASINI_1_2 as StdField with uid="OSNUHWPTCN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCLASINI", cQueryName = "CODCLASINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice della classe documentale iniziale � maggiore di quello finale",;
    ToolTipText = "Codice classe documentale SOStitutiva",;
    HelpContextID = 10362191,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=131, Top=14, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLASMSOS", oKey_1_1="SZCODCLA", oKey_1_2="this.w_CODCLASINI"

  func oCODCLASINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLASINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLASINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLASMSOS','*','SZCODCLA',cp_AbsName(this.parent,'oCODCLASINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco Classi SOS ",'',this.parent.oContained
  endproc

  add object oCODCLASFIN_1_3 as StdField with uid="IBWYQTCKLU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCLASFIN", cQueryName = "CODCLASFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice della classe documentale finale � minore di quello iniziale",;
    ToolTipText = "Codice classe documentale SOStitutiva",;
    HelpContextID = 10363388,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=131, Top=39, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLASMSOS", oKey_1_1="SZCODCLA", oKey_1_2="this.w_CODCLASFIN"

  func oCODCLASFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLASFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLASFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLASMSOS','*','SZCODCLA',cp_AbsName(this.parent,'oCODCLASFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco Classi SOS ",'',this.parent.oContained
  endproc

  add object oDATAGGINI_1_4 as StdField with uid="DLDDMRCONY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATAGGINI", cQueryName = "DATAGGINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio aggiornamento maggiore della data di fine aggiornamento",;
    ToolTipText = "Data inizio selezione intervallo aggiornamenti",;
    HelpContextID = 105694740,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=131, Top=65

  func oDATAGGINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAGGINI<=.w_DATAGGFIN or empty(.w_DATAGGFIN))
    endwith
    return bRes
  endfunc

  add object oDATAGGFIN_1_5 as StdField with uid="QBPWJJKTGB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAGGFIN", cQueryName = "DATAGGFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine aggiornamento minore della data di inizio aggiornamento",;
    ToolTipText = "Data fine selezione intervallo aggiornamenti",;
    HelpContextID = 105694815,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=345, Top=65

  func oDATAGGFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAGGINI<=.w_DATAGGFIN or empty(.w_DATAGGINI))
    endwith
    return bRes
  endfunc

  add object oOBSOLETE_1_6 as StdCheck with uid="UBXBEVORIF",rtseq=5,rtrep=.f.,left=482, top=65, caption="Obsolete",;
    ToolTipText = "Se attivo: vengono estratte solo le classi obsolete",;
    HelpContextID = 78295851,;
    cFormVar="w_OBSOLETE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSOLETE_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oOBSOLETE_1_6.GetRadio()
    this.Parent.oContained.w_OBSOLETE = this.RadioValue()
    return .t.
  endfunc

  func oOBSOLETE_1_6.SetRadio()
    this.Parent.oContained.w_OBSOLETE=trim(this.Parent.oContained.w_OBSOLETE)
    this.value = ;
      iif(this.Parent.oContained.w_OBSOLETE=='S',1,;
      0)
  endfunc


  add object oObj_1_7 as cp_outputCombo with uid="ZLERXIVPWT",left=131, top=98, width=461,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87180006

  add object oDESCLAINI_1_8 as StdField with uid="ELSGPKQVCM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCLAINI", cQueryName = "DESCLAINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Decrizione",;
    HelpContextID = 10402324,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=229, Top=14, InputMask=replicate('X',50)


  add object oBtn_1_10 as StdButton with uid="WDHBUTUXIR",left=493, top=127, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 50971942;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="FGOJEBDDCO",left=544, top=127, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 83500218;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCLAFIN_1_15 as StdField with uid="MQDQKLOACG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCLAFIN", cQueryName = "DESCLAFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Decrizione",;
    HelpContextID = 10402399,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=229, Top=39, InputMask=replicate('X',50)

  add object oStr_1_1 as StdString with uid="QXZXAETGTK",Visible=.t., Left=26, Top=14,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da cod. classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=5, Top=101,;
    Alignment=1, Width=120, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="VOFIWPLHYS",Visible=.t., Left=42, Top=66,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="PLCCDPECAN",Visible=.t., Left=274, Top=66,;
    Alignment=1, Width=69, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DQAVAENAVB",Visible=.t., Left=29, Top=39,;
    Alignment=1, Width=99, Height=18,;
    Caption="A cod. classe:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_scs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
