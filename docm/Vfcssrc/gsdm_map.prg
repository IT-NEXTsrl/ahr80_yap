* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_map                                                        *
*              Autorizzazioni processo                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-26                                                      *
* Last revis.: 2016-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsdm_map")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsdm_map")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsdm_map")
  return

* --- Class definition
define class tgsdm_map as StdPCForm
  Width  = 804
  Height = 475
  Top    = 4
  Left   = 1
  cComment = "Autorizzazioni processo"
  cPrg = "gsdm_map"
  HelpContextID=137771927
  add object cnt as tcgsdm_map
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsdm_map as PCContext
  w_PECODPRO = space(10)
  w_CPROWORD = 0
  w_PETIPFIL = space(1)
  w_PECODUTE = 0
  w_PECODGRU = 0
  w_PECHKSTA = space(1)
  w_PECHKMAI = space(1)
  w_PECHKFAX = space(1)
  w_PECHKPTL = space(1)
  w_PECHKZCP = space(1)
  w_PECHKWWP = space(1)
  w_PECHKARC = space(1)
  w_PECHKANT = space(1)
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_PETIPOPE = space(1)
  w_PEFILTRO = space(10)
  w_DESUTEGRU = space(20)
  w_PECHKPEC = space(1)
  proc Save(i_oFrom)
    this.w_PECODPRO = i_oFrom.w_PECODPRO
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_PETIPFIL = i_oFrom.w_PETIPFIL
    this.w_PECODUTE = i_oFrom.w_PECODUTE
    this.w_PECODGRU = i_oFrom.w_PECODGRU
    this.w_PECHKSTA = i_oFrom.w_PECHKSTA
    this.w_PECHKMAI = i_oFrom.w_PECHKMAI
    this.w_PECHKFAX = i_oFrom.w_PECHKFAX
    this.w_PECHKPTL = i_oFrom.w_PECHKPTL
    this.w_PECHKZCP = i_oFrom.w_PECHKZCP
    this.w_PECHKWWP = i_oFrom.w_PECHKWWP
    this.w_PECHKARC = i_oFrom.w_PECHKARC
    this.w_PECHKANT = i_oFrom.w_PECHKANT
    this.w_DESUTE = i_oFrom.w_DESUTE
    this.w_DESGRU = i_oFrom.w_DESGRU
    this.w_PETIPOPE = i_oFrom.w_PETIPOPE
    this.w_PEFILTRO = i_oFrom.w_PEFILTRO
    this.w_DESUTEGRU = i_oFrom.w_DESUTEGRU
    this.w_PECHKPEC = i_oFrom.w_PECHKPEC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PECODPRO = this.w_PECODPRO
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_PETIPFIL = this.w_PETIPFIL
    i_oTo.w_PECODUTE = this.w_PECODUTE
    i_oTo.w_PECODGRU = this.w_PECODGRU
    i_oTo.w_PECHKSTA = this.w_PECHKSTA
    i_oTo.w_PECHKMAI = this.w_PECHKMAI
    i_oTo.w_PECHKFAX = this.w_PECHKFAX
    i_oTo.w_PECHKPTL = this.w_PECHKPTL
    i_oTo.w_PECHKZCP = this.w_PECHKZCP
    i_oTo.w_PECHKWWP = this.w_PECHKWWP
    i_oTo.w_PECHKARC = this.w_PECHKARC
    i_oTo.w_PECHKANT = this.w_PECHKANT
    i_oTo.w_DESUTE = this.w_DESUTE
    i_oTo.w_DESGRU = this.w_DESGRU
    i_oTo.w_PETIPOPE = this.w_PETIPOPE
    i_oTo.w_PEFILTRO = this.w_PEFILTRO
    i_oTo.w_DESUTEGRU = this.w_DESUTEGRU
    i_oTo.w_PECHKPEC = this.w_PECHKPEC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsdm_map as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 804
  Height = 475
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-01"
  HelpContextID=137771927
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PROPPERM_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cFile = "PROPPERM"
  cKeySelect = "PECODPRO"
  cKeyWhere  = "PECODPRO=this.w_PECODPRO"
  cKeyDetail  = "PECODPRO=this.w_PECODPRO"
  cKeyWhereODBC = '"PECODPRO="+cp_ToStrODBC(this.w_PECODPRO)';

  cKeyDetailWhereODBC = '"PECODPRO="+cp_ToStrODBC(this.w_PECODPRO)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PROPPERM.PECODPRO="+cp_ToStrODBC(this.w_PECODPRO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PROPPERM.CPROWORD '
  cPrg = "gsdm_map"
  cComment = "Autorizzazioni processo"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PECODPRO = space(10)
  w_CPROWORD = 0
  w_PETIPFIL = space(1)
  o_PETIPFIL = space(1)
  w_PECODUTE = 0
  w_PECODGRU = 0
  w_PECHKSTA = space(1)
  o_PECHKSTA = space(1)
  w_PECHKMAI = space(1)
  w_PECHKFAX = space(1)
  w_PECHKPTL = space(1)
  w_PECHKZCP = space(1)
  w_PECHKWWP = space(1)
  w_PECHKARC = space(1)
  w_PECHKANT = space(1)
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_PETIPOPE = space(1)
  w_PEFILTRO = space(0)
  w_DESUTEGRU = space(20)
  w_PECHKPEC = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_mapPag1","gsdm_map",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='PROPPERM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROPPERM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROPPERM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsdm_map'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PROPPERM where PECODPRO=KeySet.PECODPRO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PROPPERM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROPPERM_IDX,2],this.bLoadRecFilter,this.PROPPERM_IDX,"gsdm_map")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROPPERM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROPPERM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PROPPERM '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PECODPRO',this.w_PECODPRO  )
      select * from (i_cTable) PROPPERM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PECODPRO = NVL(PECODPRO,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PROPPERM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(20)
          .w_DESGRU = space(20)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PETIPFIL = NVL(PETIPFIL,space(1))
          .w_PECODUTE = NVL(PECODUTE,0)
          if link_2_3_joined
            this.w_PECODUTE = NVL(CODE203,NVL(this.w_PECODUTE,0))
            this.w_DESUTE = NVL(NAME203,space(20))
          else
          .link_2_3('Load')
          endif
          .w_PECODGRU = NVL(PECODGRU,0)
          if link_2_4_joined
            this.w_PECODGRU = NVL(CODE204,NVL(this.w_PECODGRU,0))
            this.w_DESGRU = NVL(NAME204,space(20))
          else
          .link_2_4('Load')
          endif
          .w_PECHKSTA = NVL(PECHKSTA,space(1))
          .w_PECHKMAI = NVL(PECHKMAI,space(1))
          .w_PECHKFAX = NVL(PECHKFAX,space(1))
          .w_PECHKPTL = NVL(PECHKPTL,space(1))
          .w_PECHKZCP = NVL(PECHKZCP,space(1))
          .w_PECHKWWP = NVL(PECHKWWP,space(1))
          .w_PECHKARC = NVL(PECHKARC,space(1))
          .w_PECHKANT = NVL(PECHKANT,space(1))
          .w_PETIPOPE = NVL(PETIPOPE,space(1))
          .w_PEFILTRO = NVL(PEFILTRO,space(0))
        .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
          .w_PECHKPEC = NVL(PECHKPEC,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PECODPRO=space(10)
      .w_CPROWORD=10
      .w_PETIPFIL=space(1)
      .w_PECODUTE=0
      .w_PECODGRU=0
      .w_PECHKSTA=space(1)
      .w_PECHKMAI=space(1)
      .w_PECHKFAX=space(1)
      .w_PECHKPTL=space(1)
      .w_PECHKZCP=space(1)
      .w_PECHKWWP=space(1)
      .w_PECHKARC=space(1)
      .w_PECHKANT=space(1)
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_PETIPOPE=space(1)
      .w_PEFILTRO=space(0)
      .w_DESUTEGRU=space(20)
      .w_PECHKPEC=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_PECODUTE = IIF(.w_PETIPFIL='U',.w_PECODUTE, 0)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PECODUTE))
         .link_2_3('Full')
        endif
        .w_PECODGRU = IIF(.w_PETIPFIL='G',.w_PECODGRU, 0)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PECODGRU))
         .link_2_4('Full')
        endif
        .w_PECHKSTA = 'N'
        .w_PECHKMAI = 'N'
        .w_PECHKFAX = 'N'
        .w_PECHKPTL = 'N'
        .w_PECHKZCP = 'N'
        .w_PECHKWWP = 'N'
        .w_PECHKARC = 'N'
        .w_PECHKANT = IIF(.w_PECHKSTA='S', .w_PECHKANT, 'N')
        .DoRTCalc(14,15,.f.)
        .w_PETIPOPE = 'A'
        .DoRTCalc(17,17,.f.)
        .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_PECHKPEC = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROPPERM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPETIPOPE_2_15.enabled = i_bVal
      .Page1.oPag.oPEFILTRO_2_16.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PROPPERM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROPPERM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PECODPRO,"PECODPRO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_PETIPFIL N(3);
      ,t_PECODUTE N(4);
      ,t_PECODGRU N(4);
      ,t_PECHKSTA N(3);
      ,t_PECHKMAI N(3);
      ,t_PECHKFAX N(3);
      ,t_PECHKPTL N(3);
      ,t_PECHKZCP N(3);
      ,t_PECHKWWP N(3);
      ,t_PECHKARC N(3);
      ,t_PECHKANT N(3);
      ,t_PETIPOPE N(3);
      ,t_PEFILTRO M(10);
      ,t_DESUTEGRU C(20);
      ,t_PECHKPEC N(3);
      ,CPROWNUM N(10);
      ,t_DESUTE C(20);
      ,t_DESGRU C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsdm_mapbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.controlsource=this.cTrsName+'.t_PETIPFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.controlsource=this.cTrsName+'.t_PECODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.controlsource=this.cTrsName+'.t_PECODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKSTA_2_5.controlsource=this.cTrsName+'.t_PECHKSTA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKMAI_2_6.controlsource=this.cTrsName+'.t_PECHKMAI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKFAX_2_7.controlsource=this.cTrsName+'.t_PECHKFAX'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPTL_2_8.controlsource=this.cTrsName+'.t_PECHKPTL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.controlsource=this.cTrsName+'.t_PECHKZCP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKWWP_2_10.controlsource=this.cTrsName+'.t_PECHKWWP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_11.controlsource=this.cTrsName+'.t_PECHKARC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKANT_2_12.controlsource=this.cTrsName+'.t_PECHKANT'
    this.oPgFRm.Page1.oPag.oPETIPOPE_2_15.controlsource=this.cTrsName+'.t_PETIPOPE'
    this.oPgFRm.Page1.oPag.oPEFILTRO_2_16.controlsource=this.cTrsName+'.t_PEFILTRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_18.controlsource=this.cTrsName+'.t_DESUTEGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPEC_2_19.controlsource=this.cTrsName+'.t_PECHKPEC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(48)
    this.AddVLine(126)
    this.AddVLine(176)
    this.AddVLine(226)
    this.AddVLine(400)
    this.AddVLine(445)
    this.AddVLine(486)
    this.AddVLine(527)
    this.AddVLine(567)
    this.AddVLine(607)
    this.AddVLine(647)
    this.AddVLine(687)
    this.AddVLine(732)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROPPERM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROPPERM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROPPERM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROPPERM_IDX,2])
      *
      * insert into PROPPERM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROPPERM')
        i_extval=cp_InsertValODBCExtFlds(this,'PROPPERM')
        i_cFldBody=" "+;
                  "(PECODPRO,CPROWORD,PETIPFIL,PECODUTE,PECODGRU"+;
                  ",PECHKSTA,PECHKMAI,PECHKFAX,PECHKPTL,PECHKZCP"+;
                  ",PECHKWWP,PECHKARC,PECHKANT,PETIPOPE,PEFILTRO"+;
                  ",PECHKPEC,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PECODPRO)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_PETIPFIL)+","+cp_ToStrODBCNull(this.w_PECODUTE)+","+cp_ToStrODBCNull(this.w_PECODGRU)+;
             ","+cp_ToStrODBC(this.w_PECHKSTA)+","+cp_ToStrODBC(this.w_PECHKMAI)+","+cp_ToStrODBC(this.w_PECHKFAX)+","+cp_ToStrODBC(this.w_PECHKPTL)+","+cp_ToStrODBC(this.w_PECHKZCP)+;
             ","+cp_ToStrODBC(this.w_PECHKWWP)+","+cp_ToStrODBC(this.w_PECHKARC)+","+cp_ToStrODBC(this.w_PECHKANT)+","+cp_ToStrODBC(this.w_PETIPOPE)+","+cp_ToStrODBC(this.w_PEFILTRO)+;
             ","+cp_ToStrODBC(this.w_PECHKPEC)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROPPERM')
        i_extval=cp_InsertValVFPExtFlds(this,'PROPPERM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PECODPRO',this.w_PECODPRO)
        INSERT INTO (i_cTable) (;
                   PECODPRO;
                  ,CPROWORD;
                  ,PETIPFIL;
                  ,PECODUTE;
                  ,PECODGRU;
                  ,PECHKSTA;
                  ,PECHKMAI;
                  ,PECHKFAX;
                  ,PECHKPTL;
                  ,PECHKZCP;
                  ,PECHKWWP;
                  ,PECHKARC;
                  ,PECHKANT;
                  ,PETIPOPE;
                  ,PEFILTRO;
                  ,PECHKPEC;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PECODPRO;
                  ,this.w_CPROWORD;
                  ,this.w_PETIPFIL;
                  ,this.w_PECODUTE;
                  ,this.w_PECODGRU;
                  ,this.w_PECHKSTA;
                  ,this.w_PECHKMAI;
                  ,this.w_PECHKFAX;
                  ,this.w_PECHKPTL;
                  ,this.w_PECHKZCP;
                  ,this.w_PECHKWWP;
                  ,this.w_PECHKARC;
                  ,this.w_PECHKANT;
                  ,this.w_PETIPOPE;
                  ,this.w_PEFILTRO;
                  ,this.w_PECHKPEC;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PROPPERM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROPPERM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PROPPERM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PROPPERM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PROPPERM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PROPPERM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PETIPFIL="+cp_ToStrODBC(this.w_PETIPFIL)+;
                     ",PECODUTE="+cp_ToStrODBCNull(this.w_PECODUTE)+;
                     ",PECODGRU="+cp_ToStrODBCNull(this.w_PECODGRU)+;
                     ",PECHKSTA="+cp_ToStrODBC(this.w_PECHKSTA)+;
                     ",PECHKMAI="+cp_ToStrODBC(this.w_PECHKMAI)+;
                     ",PECHKFAX="+cp_ToStrODBC(this.w_PECHKFAX)+;
                     ",PECHKPTL="+cp_ToStrODBC(this.w_PECHKPTL)+;
                     ",PECHKZCP="+cp_ToStrODBC(this.w_PECHKZCP)+;
                     ",PECHKWWP="+cp_ToStrODBC(this.w_PECHKWWP)+;
                     ",PECHKARC="+cp_ToStrODBC(this.w_PECHKARC)+;
                     ",PECHKANT="+cp_ToStrODBC(this.w_PECHKANT)+;
                     ",PETIPOPE="+cp_ToStrODBC(this.w_PETIPOPE)+;
                     ",PEFILTRO="+cp_ToStrODBC(this.w_PEFILTRO)+;
                     ",PECHKPEC="+cp_ToStrODBC(this.w_PECHKPEC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PROPPERM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PETIPFIL=this.w_PETIPFIL;
                     ,PECODUTE=this.w_PECODUTE;
                     ,PECODGRU=this.w_PECODGRU;
                     ,PECHKSTA=this.w_PECHKSTA;
                     ,PECHKMAI=this.w_PECHKMAI;
                     ,PECHKFAX=this.w_PECHKFAX;
                     ,PECHKPTL=this.w_PECHKPTL;
                     ,PECHKZCP=this.w_PECHKZCP;
                     ,PECHKWWP=this.w_PECHKWWP;
                     ,PECHKARC=this.w_PECHKARC;
                     ,PECHKANT=this.w_PECHKANT;
                     ,PETIPOPE=this.w_PETIPOPE;
                     ,PEFILTRO=this.w_PEFILTRO;
                     ,PECHKPEC=this.w_PECHKPEC;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PROPPERM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROPPERM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PROPPERM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROPPERM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROPPERM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_PETIPFIL<>.w_PETIPFIL
          .w_PECODUTE = IIF(.w_PETIPFIL='U',.w_PECODUTE, 0)
          .link_2_3('Full')
        endif
        if .o_PETIPFIL<>.w_PETIPFIL
          .w_PECODGRU = IIF(.w_PETIPFIL='G',.w_PECODGRU, 0)
          .link_2_4('Full')
        endif
        .DoRTCalc(6,12,.t.)
        if .o_PECHKSTA<>.w_PECHKSTA
          .w_PECHKANT = IIF(.w_PECHKSTA='S', .w_PECHKANT, 'N')
        endif
        .DoRTCalc(14,17,.t.)
          .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESUTE with this.w_DESUTE
      replace t_DESGRU with this.w_DESGRU
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODUTE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODUTE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODGRU_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODGRU_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKSTA_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKSTA_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKMAI_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKMAI_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKFAX_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKFAX_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKPTL_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKPTL_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKZCP_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKZCP_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKWWP_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKWWP_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKARC_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKARC_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKANT_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKANT_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKPEC_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECHKPEC_2_19.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PECODUTE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PECODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PECODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PECODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oPECODUTE_2_3'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PECODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PECODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PECODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CODE as CODE203"+ ",link_2_3.NAME as NAME203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on PROPPERM.PECODUTE=link_2_3.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and PROPPERM.PECODUTE=link_2_3.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PECODGRU
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PECODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PECODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PECODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oPECODGRU_2_4'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PECODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PECODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECODGRU = NVL(_Link_.CODE,0)
      this.w_DESGRU = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PECODGRU = 0
      endif
      this.w_DESGRU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPGROUPS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CODE as CODE204"+ ",link_2_4.NAME as NAME204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on PROPPERM.PECODGRU=link_2_4.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and PROPPERM.PECODGRU=link_2_4.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPETIPOPE_2_15.RadioValue()==this.w_PETIPOPE)
      this.oPgFrm.Page1.oPag.oPETIPOPE_2_15.SetRadio()
      replace t_PETIPOPE with this.oPgFrm.Page1.oPag.oPETIPOPE_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPEFILTRO_2_16.value==this.w_PEFILTRO)
      this.oPgFrm.Page1.oPag.oPEFILTRO_2_16.value=this.w_PEFILTRO
      replace t_PEFILTRO with this.oPgFrm.Page1.oPag.oPEFILTRO_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.RadioValue()==this.w_PETIPFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.SetRadio()
      replace t_PETIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.value==this.w_PECODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.value=this.w_PECODUTE
      replace t_PECODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODUTE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.value==this.w_PECODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.value=this.w_PECODGRU
      replace t_PECODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODGRU_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKSTA_2_5.RadioValue()==this.w_PECHKSTA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKSTA_2_5.SetRadio()
      replace t_PECHKSTA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKSTA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKMAI_2_6.RadioValue()==this.w_PECHKMAI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKMAI_2_6.SetRadio()
      replace t_PECHKMAI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKMAI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKFAX_2_7.RadioValue()==this.w_PECHKFAX)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKFAX_2_7.SetRadio()
      replace t_PECHKFAX with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKFAX_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPTL_2_8.RadioValue()==this.w_PECHKPTL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPTL_2_8.SetRadio()
      replace t_PECHKPTL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPTL_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.RadioValue()==this.w_PECHKZCP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.SetRadio()
      replace t_PECHKZCP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKWWP_2_10.RadioValue()==this.w_PECHKWWP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKWWP_2_10.SetRadio()
      replace t_PECHKWWP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKWWP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_11.RadioValue()==this.w_PECHKARC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_11.SetRadio()
      replace t_PECHKARC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKANT_2_12.RadioValue()==this.w_PECHKANT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKANT_2_12.SetRadio()
      replace t_PECHKANT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKANT_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_18.value==this.w_DESUTEGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_18.value=this.w_DESUTEGRU
      replace t_DESUTEGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPEC_2_19.RadioValue()==this.w_PECHKPEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPEC_2_19.SetRadio()
      replace t_PECHKPEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPEC_2_19.value
    endif
    cp_SetControlsValueExtFlds(this,'PROPPERM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and not empty(.w_PETIPFIL)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PETIPFIL = this.w_PETIPFIL
    this.o_PECHKSTA = this.w_PECHKSTA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not empty(t_PETIPFIL))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_PETIPFIL=space(1)
      .w_PECODUTE=0
      .w_PECODGRU=0
      .w_PECHKSTA=space(1)
      .w_PECHKMAI=space(1)
      .w_PECHKFAX=space(1)
      .w_PECHKPTL=space(1)
      .w_PECHKZCP=space(1)
      .w_PECHKWWP=space(1)
      .w_PECHKARC=space(1)
      .w_PECHKANT=space(1)
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_PETIPOPE=space(1)
      .w_PEFILTRO=space(0)
      .w_DESUTEGRU=space(20)
      .w_PECHKPEC=space(1)
      .DoRTCalc(1,3,.f.)
        .w_PECODUTE = IIF(.w_PETIPFIL='U',.w_PECODUTE, 0)
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_PECODUTE))
        .link_2_3('Full')
      endif
        .w_PECODGRU = IIF(.w_PETIPFIL='G',.w_PECODGRU, 0)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_PECODGRU))
        .link_2_4('Full')
      endif
        .w_PECHKSTA = 'N'
        .w_PECHKMAI = 'N'
        .w_PECHKFAX = 'N'
        .w_PECHKPTL = 'N'
        .w_PECHKZCP = 'N'
        .w_PECHKWWP = 'N'
        .w_PECHKARC = 'N'
        .w_PECHKANT = IIF(.w_PECHKSTA='S', .w_PECHKANT, 'N')
      .DoRTCalc(14,15,.f.)
        .w_PETIPOPE = 'A'
      .DoRTCalc(17,17,.f.)
        .w_DESUTEGRU = IIF(.w_PETIPFIL='U',.w_DESUTE,.w_DESGRU)
        .w_PECHKPEC = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PETIPFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.RadioValue(.t.)
    this.w_PECODUTE = t_PECODUTE
    this.w_PECODGRU = t_PECODGRU
    this.w_PECHKSTA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKSTA_2_5.RadioValue(.t.)
    this.w_PECHKMAI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKMAI_2_6.RadioValue(.t.)
    this.w_PECHKFAX = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKFAX_2_7.RadioValue(.t.)
    this.w_PECHKPTL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPTL_2_8.RadioValue(.t.)
    this.w_PECHKZCP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.RadioValue(.t.)
    this.w_PECHKWWP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKWWP_2_10.RadioValue(.t.)
    this.w_PECHKARC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_11.RadioValue(.t.)
    this.w_PECHKANT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKANT_2_12.RadioValue(.t.)
    this.w_DESUTE = t_DESUTE
    this.w_DESGRU = t_DESGRU
    this.w_PETIPOPE = this.oPgFrm.Page1.oPag.oPETIPOPE_2_15.RadioValue(.t.)
    this.w_PEFILTRO = t_PEFILTRO
    this.w_DESUTEGRU = t_DESUTEGRU
    this.w_PECHKPEC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPEC_2_19.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PETIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPFIL_2_2.ToRadio()
    replace t_PECODUTE with this.w_PECODUTE
    replace t_PECODGRU with this.w_PECODGRU
    replace t_PECHKSTA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKSTA_2_5.ToRadio()
    replace t_PECHKMAI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKMAI_2_6.ToRadio()
    replace t_PECHKFAX with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKFAX_2_7.ToRadio()
    replace t_PECHKPTL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPTL_2_8.ToRadio()
    replace t_PECHKZCP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKZCP_2_9.ToRadio()
    replace t_PECHKWWP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKWWP_2_10.ToRadio()
    replace t_PECHKARC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKARC_2_11.ToRadio()
    replace t_PECHKANT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKANT_2_12.ToRadio()
    replace t_DESUTE with this.w_DESUTE
    replace t_DESGRU with this.w_DESGRU
    replace t_PETIPOPE with this.oPgFrm.Page1.oPag.oPETIPOPE_2_15.ToRadio()
    replace t_PEFILTRO with this.w_PEFILTRO
    replace t_DESUTEGRU with this.w_DESUTEGRU
    replace t_PECHKPEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECHKPEC_2_19.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsdm_mapPag1 as StdContainer
  Width  = 800
  height = 475
  stdWidth  = 800
  stdheight = 475
  resizeXpos=299
  resizeYpos=228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=2, width=780,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=14,Field1="CPROWORD",Label1="Seq.",Field2="PETIPFIL",Label2="Soggetto",Field3="PECODUTE",Label3="Utente",Field4="PECODGRU",Label4="Gruppo",Field5="DESUTEGRU",Label5="Descrizione",Field6="PECHKSTA",Label6="Stampa",Field7="PECHKMAI",Label7="Email",Field8="PECHKPEC",Label8="PEC",Field9="PECHKFAX",Label9="FAX",Field10="PECHKPTL",Label10="Plite",Field11="PECHKZCP",Label11="IIF(g_DMIP<>'S',Ah_MsgFormat('WEB'),'')",Field12="PECHKWWP",Label12="WE",Field13="PECHKARC",Label13="Archivia",Field14="PECHKANT",Label14="Anteprima",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 83008134

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=21,;
    width=783+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=22,width=782+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPUSERS|CPGROUPS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPETIPOPE_2_15.Refresh()
      this.Parent.oPEFILTRO_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oPECODUTE_2_3
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oPECODGRU_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPETIPOPE_2_15 as StdTrsCombo with uid="ADCBRPMVZD",rtrep=.t.,;
    cFormVar="w_PETIPOPE", RowSource=""+"And,"+"Or,"+"Sovrascrive" , ;
    ToolTipText = "Operatore logico per trattamento filtro di default",;
    HelpContextID = 58407365,;
    Height=25, Width=93, Left=19, Top=442,;
    cTotal="", cQueryName = "PETIPOPE",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPOPE_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPOPE,&i_cF..t_PETIPOPE),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'O',;
    iif(xVal =3,'S',;
    space(1)))))
  endfunc
  func oPETIPOPE_2_15.GetRadio()
    this.Parent.oContained.w_PETIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oPETIPOPE_2_15.ToRadio()
    this.Parent.oContained.w_PETIPOPE=trim(this.Parent.oContained.w_PETIPOPE)
    return(;
      iif(this.Parent.oContained.w_PETIPOPE=='A',1,;
      iif(this.Parent.oContained.w_PETIPOPE=='O',2,;
      iif(this.Parent.oContained.w_PETIPOPE=='S',3,;
      0))))
  endfunc

  func oPETIPOPE_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPEFILTRO_2_16 as StdTrsMemo with uid="WCVLEBXHXT",rtseq=17,rtrep=.t.,;
    cFormVar="w_PEFILTRO",value=space(0),;
    ToolTipText = "Condizione di filtro su cursore di stampa",;
    HelpContextID = 247208379,;
    cTotal="", bFixedPos=.t., cQueryName = "PEFILTRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=48, Width=592, Left=119, Top=423

  add object oStr_2_17 as StdString with uid="LEMETBCVYT",Visible=.t., Left=15, Top=423,;
    Alignment=1, Width=94, Height=18,;
    Caption="Filtro avanzato:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsdm_mapBodyRow as CPBodyRowCnt
  Width=773
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="VMCUIGLUFG",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza di verifica condizioni",;
    HelpContextID = 50679702,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oPETIPFIL_2_2 as StdTrsCombo with uid="FXEZSUYKVH",rtrep=.t.,;
    cFormVar="w_PETIPFIL", RowSource=""+"Utente,"+"Gruppo" , ;
    HelpContextID = 59033154,;
    Height=22, Width=75, Left=42, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPFIL_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPFIL,&i_cF..t_PETIPFIL),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    space(1))))
  endfunc
  func oPETIPFIL_2_2.GetRadio()
    this.Parent.oContained.w_PETIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oPETIPFIL_2_2.ToRadio()
    this.Parent.oContained.w_PETIPFIL=trim(this.Parent.oContained.w_PETIPFIL)
    return(;
      iif(this.Parent.oContained.w_PETIPFIL=='U',1,;
      iif(this.Parent.oContained.w_PETIPFIL=='G',2,;
      0)))
  endfunc

  func oPETIPFIL_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPECODUTE_2_3 as StdTrsField with uid="HZGGWJFOBA",rtseq=4,rtrep=.t.,;
    cFormVar="w_PECODUTE",value=0,;
    ToolTipText = "Utente",;
    HelpContextID = 29996603,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=47, Left=120, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_PECODUTE"

  func oPECODUTE_2_3.mCond()
    with this.Parent.oContained
      return (.w_PETIPFIL='U')
    endwith
  endfunc

  func oPECODUTE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECODUTE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECODUTE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oPECODUTE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPECODGRU_2_4 as StdTrsField with uid="FXJSDZUIVQ",rtseq=5,rtrep=.t.,;
    cFormVar="w_PECODGRU",value=0,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 204884405,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=47, Left=170, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_PECODGRU"

  func oPECODGRU_2_4.mCond()
    with this.Parent.oContained
      return (.w_PETIPFIL='G')
    endwith
  endfunc

  func oPECODGRU_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECODGRU_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECODGRU_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oPECODGRU_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oPECHKSTA_2_5 as StdTrsCheck with uid="BZMUJYTTFW",rtrep=.t.,;
    cFormVar="w_PECHKSTA",  caption="",;
    ToolTipText = "Check stampante",;
    HelpContextID = 3323447,;
    Left=394, Top=-1, Width=42,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKSTA_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKSTA,&i_cF..t_PECHKSTA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKSTA_2_5.GetRadio()
    this.Parent.oContained.w_PECHKSTA = this.RadioValue()
    return .t.
  endfunc

  func oPECHKSTA_2_5.ToRadio()
    this.Parent.oContained.w_PECHKSTA=trim(this.Parent.oContained.w_PECHKSTA)
    return(;
      iif(this.Parent.oContained.w_PECHKSTA=='S',1,;
      0))
  endfunc

  func oPECHKSTA_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKSTA_2_5.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKSTA='S')
    endwith
  endfunc

  add object oPECHKMAI_2_6 as StdTrsCheck with uid="WXTDYCHCSL",rtrep=.t.,;
    cFormVar="w_PECHKMAI",  caption="",;
    ToolTipText = "Check email",;
    HelpContextID = 97339841,;
    Left=439, Top=-1, Width=38,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKMAI_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKMAI,&i_cF..t_PECHKMAI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKMAI_2_6.GetRadio()
    this.Parent.oContained.w_PECHKMAI = this.RadioValue()
    return .t.
  endfunc

  func oPECHKMAI_2_6.ToRadio()
    this.Parent.oContained.w_PECHKMAI=trim(this.Parent.oContained.w_PECHKMAI)
    return(;
      iif(this.Parent.oContained.w_PECHKMAI=='S',1,;
      0))
  endfunc

  func oPECHKMAI_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKMAI_2_6.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKMAI='S')
    endwith
  endfunc

  add object oPECHKFAX_2_7 as StdTrsCheck with uid="JRUUIKZUBI",rtrep=.t.,;
    cFormVar="w_PECHKFAX",  caption="",;
    ToolTipText = "Check FAX",;
    HelpContextID = 214780338,;
    Left=521, Top=-1, Width=37,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKFAX_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKFAX,&i_cF..t_PECHKFAX),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKFAX_2_7.GetRadio()
    this.Parent.oContained.w_PECHKFAX = this.RadioValue()
    return .t.
  endfunc

  func oPECHKFAX_2_7.ToRadio()
    this.Parent.oContained.w_PECHKFAX=trim(this.Parent.oContained.w_PECHKFAX)
    return(;
      iif(this.Parent.oContained.w_PECHKFAX=='S',1,;
      0))
  endfunc

  func oPECHKFAX_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKFAX_2_7.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKFAX='S')
    endwith
  endfunc

  add object oPECHKPTL_2_8 as StdTrsCheck with uid="XBUEZQEXHL",rtrep=.t.,;
    cFormVar="w_PECHKPTL",  caption="",;
    ToolTipText = "Check invio Postalite",;
    HelpContextID = 47008190,;
    Left=561, Top=-1, Width=37,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKPTL_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKPTL,&i_cF..t_PECHKPTL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKPTL_2_8.GetRadio()
    this.Parent.oContained.w_PECHKPTL = this.RadioValue()
    return .t.
  endfunc

  func oPECHKPTL_2_8.ToRadio()
    this.Parent.oContained.w_PECHKPTL=trim(this.Parent.oContained.w_PECHKPTL)
    return(;
      iif(this.Parent.oContained.w_PECHKPTL=='S',1,;
      0))
  endfunc

  func oPECHKPTL_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKPTL_2_8.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKPTL='S')
    endwith
  endfunc

  add object oPECHKZCP_2_9 as StdTrsCheck with uid="EIVNZFRCGM",rtrep=.t.,;
    cFormVar="w_PECHKZCP",  caption="",;
    ToolTipText = "Check invio Web Application",;
    HelpContextID = 147671482,;
    Left=601, Top=-1, Width=37,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKZCP_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKZCP,&i_cF..t_PECHKZCP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKZCP_2_9.GetRadio()
    this.Parent.oContained.w_PECHKZCP = this.RadioValue()
    return .t.
  endfunc

  func oPECHKZCP_2_9.ToRadio()
    this.Parent.oContained.w_PECHKZCP=trim(this.Parent.oContained.w_PECHKZCP)
    return(;
      iif(this.Parent.oContained.w_PECHKZCP=='S',1,;
      0))
  endfunc

  func oPECHKZCP_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKZCP_2_9.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKZCP='S')
    endwith
  endfunc

  func oPECHKZCP_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DMIP='S')
    endwith
    endif
  endfunc

  add object oPECHKWWP_2_10 as StdTrsCheck with uid="BFUZCVOGNY",rtrep=.t.,;
    cFormVar="w_PECHKWWP",  caption="",;
    ToolTipText = "Check WE",;
    HelpContextID = 70432326,;
    Left=641, Top=-1, Width=37,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKWWP_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKWWP,&i_cF..t_PECHKWWP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKWWP_2_10.GetRadio()
    this.Parent.oContained.w_PECHKWWP = this.RadioValue()
    return .t.
  endfunc

  func oPECHKWWP_2_10.ToRadio()
    this.Parent.oContained.w_PECHKWWP=trim(this.Parent.oContained.w_PECHKWWP)
    return(;
      iif(this.Parent.oContained.w_PECHKWWP=='S',1,;
      0))
  endfunc

  func oPECHKWWP_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKWWP_2_10.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKWWP='S')
    endwith
  endfunc

  add object oPECHKARC_2_11 as StdTrsCheck with uid="GQNLYPFOTZ",rtrep=.t.,;
    cFormVar="w_PECHKARC",  caption="",;
    ToolTipText = "Check archiviazione",;
    HelpContextID = 30230983,;
    Left=681, Top=-1, Width=42,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKARC_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKARC,&i_cF..t_PECHKARC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKARC_2_11.GetRadio()
    this.Parent.oContained.w_PECHKARC = this.RadioValue()
    return .t.
  endfunc

  func oPECHKARC_2_11.ToRadio()
    this.Parent.oContained.w_PECHKARC=trim(this.Parent.oContained.w_PECHKARC)
    return(;
      iif(this.Parent.oContained.w_PECHKARC=='S',1,;
      0))
  endfunc

  func oPECHKARC_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKARC_2_11.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKARC='S')
    endwith
  endfunc

  add object oPECHKANT_2_12 as StdTrsCheck with uid="XXEEVXCWQF",rtrep=.t.,;
    cFormVar="w_PECHKANT",  caption="",;
    ToolTipText = "Check anteprima",;
    HelpContextID = 30230966,;
    Left=726, Top=-1, Width=42,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKANT_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKANT,&i_cF..t_PECHKANT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKANT_2_12.GetRadio()
    this.Parent.oContained.w_PECHKANT = this.RadioValue()
    return .t.
  endfunc

  func oPECHKANT_2_12.ToRadio()
    this.Parent.oContained.w_PECHKANT=trim(this.Parent.oContained.w_PECHKANT)
    return(;
      iif(this.Parent.oContained.w_PECHKANT=='S',1,;
      0))
  endfunc

  func oPECHKANT_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKANT_2_12.mCond()
    with this.Parent.oContained
      return (.w_PECHKSTA='S')
    endwith
  endfunc

  add object oDESUTEGRU_2_18 as StdTrsField with uid="OORAQWARDS",rtseq=18,rtrep=.t.,;
    cFormVar="w_DESUTEGRU",value=space(20),enabled=.f.,;
    HelpContextID = 47233752,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=171, Left=220, Top=0, InputMask=replicate('X',20)

  add object oPECHKPEC_2_19 as StdTrsCheck with uid="TNYENTTDIN",rtrep=.t.,;
    cFormVar="w_PECHKPEC",  caption="",;
    ToolTipText = "Check email PEC",;
    HelpContextID = 221427257,;
    Left=480, Top=-1, Width=38,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPECHKPEC_2_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECHKPEC,&i_cF..t_PECHKPEC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPECHKPEC_2_19.GetRadio()
    this.Parent.oContained.w_PECHKPEC = this.RadioValue()
    return .t.
  endfunc

  func oPECHKPEC_2_19.ToRadio()
    this.Parent.oContained.w_PECHKPEC=trim(this.Parent.oContained.w_PECHKPEC)
    return(;
      iif(this.Parent.oContained.w_PECHKPEC=='S',1,;
      0))
  endfunc

  func oPECHKPEC_2_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECHKPEC_2_19.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_PDCHKPEC='S')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_map','PROPPERM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PECODPRO=PROPPERM.PECODPRO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
