* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_mpa                                                        *
*              Parametri document management                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-24                                                      *
* Last revis.: 2011-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsdm_mpa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsdm_mpa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsdm_mpa")
  return

* --- Class definition
define class tgsdm_mpa as StdPCForm
  Width  = 445
  Height = 302
  Top    = 2
  Left   = 15
  cComment = "Parametri document management"
  cPrg = "gsdm_mpa"
  HelpContextID=147440745
  add object cnt as tcgsdm_mpa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsdm_mpa as PCContext
  w_PACODAZI = space(5)
  w_PASERVER = space(20)
  w_PADATABA = space(30)
  w_PATIPFIL = space(1)
  w_PACODUTE = 0
  w_PACODGRU = 0
  w_PAPERVIS = 0
  w_PAUTEAWE = space(20)
  w_PAPASSWD = space(20)
  w_DESAZI = space(40)
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_PASOLUTE = space(1)
  proc Save(i_oFrom)
    this.w_PACODAZI = i_oFrom.w_PACODAZI
    this.w_PASERVER = i_oFrom.w_PASERVER
    this.w_PADATABA = i_oFrom.w_PADATABA
    this.w_PATIPFIL = i_oFrom.w_PATIPFIL
    this.w_PACODUTE = i_oFrom.w_PACODUTE
    this.w_PACODGRU = i_oFrom.w_PACODGRU
    this.w_PAPERVIS = i_oFrom.w_PAPERVIS
    this.w_PAUTEAWE = i_oFrom.w_PAUTEAWE
    this.w_PAPASSWD = i_oFrom.w_PAPASSWD
    this.w_DESAZI = i_oFrom.w_DESAZI
    this.w_DESUTE = i_oFrom.w_DESUTE
    this.w_DESGRU = i_oFrom.w_DESGRU
    this.w_PASOLUTE = i_oFrom.w_PASOLUTE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PACODAZI = this.w_PACODAZI
    i_oTo.w_PASERVER = this.w_PASERVER
    i_oTo.w_PADATABA = this.w_PADATABA
    i_oTo.w_PATIPFIL = this.w_PATIPFIL
    i_oTo.w_PACODUTE = this.w_PACODUTE
    i_oTo.w_PACODGRU = this.w_PACODGRU
    i_oTo.w_PAPERVIS = this.w_PAPERVIS
    i_oTo.w_PAUTEAWE = this.w_PAUTEAWE
    i_oTo.w_PAPASSWD = this.w_PAPASSWD
    i_oTo.w_DESAZI = this.w_DESAZI
    i_oTo.w_DESUTE = this.w_DESUTE
    i_oTo.w_DESGRU = this.w_DESGRU
    i_oTo.w_PASOLUTE = this.w_PASOLUTE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsdm_mpa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 445
  Height = 302
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-10"
  HelpContextID=147440745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DMPARAM_IDX = 0
  AZIENDA_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  PROMCLAS_IDX = 0
  cFile = "DMPARAM"
  cKeySelect = "PACODAZI"
  cKeyWhere  = "PACODAZI=this.w_PACODAZI"
  cKeyDetail  = "PACODAZI=this.w_PACODAZI"
  cKeyWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cKeyDetailWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DMPARAM.PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DMPARAM.CPROWNUM '
  cPrg = "gsdm_mpa"
  cComment = "Parametri document management"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODAZI = space(5)
  w_PASERVER = space(20)
  w_PADATABA = space(30)
  w_PATIPFIL = space(1)
  o_PATIPFIL = space(1)
  w_PACODUTE = 0
  w_PACODGRU = 0
  w_PAPERVIS = 0
  w_PAUTEAWE = space(20)
  w_PAPASSWD = space(20)
  w_DESAZI = space(40)
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_PASOLUTE = space(1)
  w_LblAzi = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_mpaPag1","gsdm_mpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LblAzi = this.oPgFrm.Pages(1).oPag.LblAzi
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_LblAzi = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='CPGROUPS'
    this.cWorkTables[4]='PROMCLAS'
    this.cWorkTables[5]='DMPARAM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DMPARAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DMPARAM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsdm_mpa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DMPARAM where PACODAZI=KeySet.PACODAZI
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DMPARAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DMPARAM_IDX,2],this.bLoadRecFilter,this.DMPARAM_IDX,"gsdm_mpa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DMPARAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DMPARAM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DMPARAM '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
      select * from (i_cTable) DMPARAM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESAZI = space(40)
        .w_PACODAZI = NVL(PACODAZI,space(5))
          if link_1_1_joined
            this.w_PACODAZI = NVL(AZCODAZI101,NVL(this.w_PACODAZI,space(5)))
            this.w_DESAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_PASERVER = NVL(PASERVER,space(20))
        .w_PADATABA = NVL(PADATABA,space(30))
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DMPARAM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(20)
          .w_DESGRU = space(20)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PATIPFIL = NVL(PATIPFIL,space(1))
          .w_PACODUTE = NVL(PACODUTE,0)
          if link_2_2_joined
            this.w_PACODUTE = NVL(CODE202,NVL(this.w_PACODUTE,0))
            this.w_DESUTE = NVL(NAME202,space(20))
          else
          .link_2_2('Load')
          endif
          .w_PACODGRU = NVL(PACODGRU,0)
          if link_2_3_joined
            this.w_PACODGRU = NVL(CODE203,NVL(this.w_PACODGRU,0))
            this.w_DESGRU = NVL(NAME203,space(20))
          else
          .link_2_3('Load')
          endif
          .w_PAPERVIS = NVL(PAPERVIS,0)
          .w_PAUTEAWE = NVL(PAUTEAWE,space(20))
          .w_PAPASSWD = NVL(PAPASSWD,space(20))
          .w_PASOLUTE = NVL(PASOLUTE,space(1))
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate('*')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PACODAZI=space(5)
      .w_PASERVER=space(20)
      .w_PADATABA=space(30)
      .w_PATIPFIL=space(1)
      .w_PACODUTE=0
      .w_PACODGRU=0
      .w_PAPERVIS=0
      .w_PAUTEAWE=space(20)
      .w_PAPASSWD=space(20)
      .w_DESAZI=space(40)
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_PASOLUTE=space(1)
      if .cFunction<>"Filter"
        .w_PACODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PACODAZI))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        .w_PACODUTE = IIF(.w_PATIPFIL='U',.w_PACODUTE, 0)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PACODUTE))
         .link_2_2('Full')
        endif
        .w_PACODGRU = IIF(.w_PATIPFIL='G',.w_PACODGRU, 0)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PACODGRU))
         .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(7,12,.f.)
        .w_PASOLUTE = 'N'
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate('*')
      endif
    endwith
    cp_BlankRecExtFlds(this,'DMPARAM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPACODAZI_1_1.enabled = !i_bVal
      .Page1.oPag.oObj_2_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DMPARAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DMPARAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODAZI,"PACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASERVER,"PASERVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATABA,"PADATABA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PATIPFIL N(3);
      ,t_PACODUTE N(4);
      ,t_PACODGRU N(4);
      ,t_PAPERVIS N(3);
      ,t_PASOLUTE N(3);
      ,CPROWNUM N(10);
      ,t_PAUTEAWE C(20);
      ,t_PAPASSWD C(20);
      ,t_DESUTE C(20);
      ,t_DESGRU C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsdm_mpabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1.controlsource=this.cTrsName+'.t_PATIPFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPACODUTE_2_2.controlsource=this.cTrsName+'.t_PACODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPACODGRU_2_3.controlsource=this.cTrsName+'.t_PACODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPAPERVIS_2_4.controlsource=this.cTrsName+'.t_PAPERVIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPASOLUTE_2_9.controlsource=this.cTrsName+'.t_PASOLUTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(91)
    this.AddVLine(150)
    this.AddVLine(209)
    this.AddVLine(367)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DMPARAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DMPARAM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DMPARAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DMPARAM_IDX,2])
      *
      * insert into DMPARAM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DMPARAM')
        i_extval=cp_InsertValODBCExtFlds(this,'DMPARAM')
        i_cFldBody=" "+;
                  "(PACODAZI,PASERVER,PADATABA,PATIPFIL,PACODUTE"+;
                  ",PACODGRU,PAPERVIS,PAUTEAWE,PAPASSWD,PASOLUTE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_PACODAZI)+","+cp_ToStrODBC(this.w_PASERVER)+","+cp_ToStrODBC(this.w_PADATABA)+","+cp_ToStrODBC(this.w_PATIPFIL)+","+cp_ToStrODBCNull(this.w_PACODUTE)+;
             ","+cp_ToStrODBCNull(this.w_PACODGRU)+","+cp_ToStrODBC(this.w_PAPERVIS)+","+cp_ToStrODBC(this.w_PAUTEAWE)+","+cp_ToStrODBC(this.w_PAPASSWD)+","+cp_ToStrODBC(this.w_PASOLUTE)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DMPARAM')
        i_extval=cp_InsertValVFPExtFlds(this,'DMPARAM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PACODAZI',this.w_PACODAZI)
        INSERT INTO (i_cTable) (;
                   PACODAZI;
                  ,PASERVER;
                  ,PADATABA;
                  ,PATIPFIL;
                  ,PACODUTE;
                  ,PACODGRU;
                  ,PAPERVIS;
                  ,PAUTEAWE;
                  ,PAPASSWD;
                  ,PASOLUTE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PACODAZI;
                  ,this.w_PASERVER;
                  ,this.w_PADATABA;
                  ,this.w_PATIPFIL;
                  ,this.w_PACODUTE;
                  ,this.w_PACODGRU;
                  ,this.w_PAPERVIS;
                  ,this.w_PAUTEAWE;
                  ,this.w_PAPASSWD;
                  ,this.w_PASOLUTE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DMPARAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DMPARAM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PATIPFIL))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DMPARAM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " PASERVER="+cp_ToStrODBC(this.w_PASERVER)+;
                 ",PADATABA="+cp_ToStrODBC(this.w_PADATABA)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DMPARAM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  PASERVER=this.w_PASERVER;
                 ,PADATABA=this.w_PADATABA;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PATIPFIL))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DMPARAM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DMPARAM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PASERVER="+cp_ToStrODBC(this.w_PASERVER)+;
                     ",PADATABA="+cp_ToStrODBC(this.w_PADATABA)+;
                     ",PATIPFIL="+cp_ToStrODBC(this.w_PATIPFIL)+;
                     ",PACODUTE="+cp_ToStrODBCNull(this.w_PACODUTE)+;
                     ",PACODGRU="+cp_ToStrODBCNull(this.w_PACODGRU)+;
                     ",PAPERVIS="+cp_ToStrODBC(this.w_PAPERVIS)+;
                     ",PAUTEAWE="+cp_ToStrODBC(this.w_PAUTEAWE)+;
                     ",PAPASSWD="+cp_ToStrODBC(this.w_PAPASSWD)+;
                     ",PASOLUTE="+cp_ToStrODBC(this.w_PASOLUTE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DMPARAM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PASERVER=this.w_PASERVER;
                     ,PADATABA=this.w_PADATABA;
                     ,PATIPFIL=this.w_PATIPFIL;
                     ,PACODUTE=this.w_PACODUTE;
                     ,PACODGRU=this.w_PACODGRU;
                     ,PAPERVIS=this.w_PAPERVIS;
                     ,PAUTEAWE=this.w_PAUTEAWE;
                     ,PAPASSWD=this.w_PAPASSWD;
                     ,PASOLUTE=this.w_PASOLUTE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DMPARAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DMPARAM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PATIPFIL))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DMPARAM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PATIPFIL))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DMPARAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DMPARAM_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_PATIPFIL<>.w_PATIPFIL
          .w_PACODUTE = IIF(.w_PATIPFIL='U',.w_PACODUTE, 0)
          .link_2_2('Full')
        endif
        if .o_PATIPFIL<>.w_PATIPFIL
          .w_PACODGRU = IIF(.w_PATIPFIL='G',.w_PACODGRU, 0)
          .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate('*')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PAUTEAWE with this.w_PAUTEAWE
      replace t_PAPASSWD with this.w_PAPASSWD
      replace t_DESUTE with this.w_DESUTE
      replace t_DESGRU with this.w_DESGRU
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.LblAzi.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate('*')
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate('*')
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPACODUTE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPACODUTE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPACODGRU_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPACODGRU_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPAPERVIS_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPAPERVIS_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPASOLUTE_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPASOLUTE_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.LblAzi.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PACODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_PACODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_PACODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_DESAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACODAZI = space(5)
      endif
      this.w_DESAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PACODAZI=i_codazi
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Funzione non consentita")
        endif
        this.w_PACODAZI = space(5)
        this.w_DESAZI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on DMPARAM.PACODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and DMPARAM.PACODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODUTE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PACODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PACODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PACODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oPACODUTE_2_2'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PACODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PACODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PACODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CODE as CODE202"+ ",link_2_2.NAME as NAME202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DMPARAM.PACODUTE=link_2_2.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DMPARAM.PACODUTE=link_2_2.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODGRU
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PACODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PACODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PACODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oPACODGRU_2_3'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PACODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PACODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODGRU = NVL(_Link_.CODE,0)
      this.w_DESGRU = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PACODGRU = 0
      endif
      this.w_DESGRU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPGROUPS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CODE as CODE203"+ ",link_2_3.NAME as NAME203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on DMPARAM.PACODGRU=link_2_3.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and DMPARAM.PACODGRU=link_2_3.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPACODAZI_1_1.value==this.w_PACODAZI)
      this.oPgFrm.Page1.oPag.oPACODAZI_1_1.value=this.w_PACODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAZI_1_6.value==this.w_DESAZI)
      this.oPgFrm.Page1.oPag.oDESAZI_1_6.value=this.w_DESAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1.RadioValue()==this.w_PATIPFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1.SetRadio()
      replace t_PATIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODUTE_2_2.value==this.w_PACODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODUTE_2_2.value=this.w_PACODUTE
      replace t_PACODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODUTE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODGRU_2_3.value==this.w_PACODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODGRU_2_3.value=this.w_PACODGRU
      replace t_PACODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODGRU_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPAPERVIS_2_4.RadioValue()==this.w_PAPERVIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPAPERVIS_2_4.SetRadio()
      replace t_PAPERVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPAPERVIS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPASOLUTE_2_9.RadioValue()==this.w_PASOLUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPASOLUTE_2_9.SetRadio()
      replace t_PASOLUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPASOLUTE_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'DMPARAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_PACODAZI=i_codazi)  and not(empty(.w_PACODAZI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPACODAZI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Funzione non consentita")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_PATIPFIL))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PATIPFIL = this.w_PATIPFIL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PATIPFIL)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PATIPFIL=space(1)
      .w_PACODUTE=0
      .w_PACODGRU=0
      .w_PAPERVIS=0
      .w_PAUTEAWE=space(20)
      .w_PAPASSWD=space(20)
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_PASOLUTE=space(1)
      .DoRTCalc(1,4,.f.)
        .w_PACODUTE = IIF(.w_PATIPFIL='U',.w_PACODUTE, 0)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_PACODUTE))
        .link_2_2('Full')
      endif
        .w_PACODGRU = IIF(.w_PATIPFIL='G',.w_PACODGRU, 0)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_PACODGRU))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,12,.f.)
        .w_PASOLUTE = 'N'
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate('*')
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PATIPFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1.RadioValue(.t.)
    this.w_PACODUTE = t_PACODUTE
    this.w_PACODGRU = t_PACODGRU
    this.w_PAPERVIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPAPERVIS_2_4.RadioValue(.t.)
    this.w_PAUTEAWE = t_PAUTEAWE
    this.w_PAPASSWD = t_PAPASSWD
    this.w_DESUTE = t_DESUTE
    this.w_DESGRU = t_DESGRU
    this.w_PASOLUTE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPASOLUTE_2_9.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PATIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPFIL_2_1.ToRadio()
    replace t_PACODUTE with this.w_PACODUTE
    replace t_PACODGRU with this.w_PACODGRU
    replace t_PAPERVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPAPERVIS_2_4.ToRadio()
    replace t_PAUTEAWE with this.w_PAUTEAWE
    replace t_PAPASSWD with this.w_PAPASSWD
    replace t_DESUTE with this.w_DESUTE
    replace t_DESGRU with this.w_DESGRU
    replace t_PASOLUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPASOLUTE_2_9.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsdm_mpaPag1 as StdContainer
  Width  = 441
  height = 302
  stdWidth  = 441
  stdheight = 302
  resizeXpos=339
  resizeYpos=213
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODAZI_1_1 as StdField with uid="RMLWRPOMXK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PACODAZI", cQueryName = "PACODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Funzione non consentita",;
    HelpContextID = 214544959,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=48, Left=73, Top=7, InputMask=replicate('X',5), cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_PACODAZI"

  func oPACODAZI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESAZI_1_6 as StdField with uid="OUIJNBPGUZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESAZI", cQueryName = "DESAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 165890762,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=124, Top=7, InputMask=replicate('X',40)


  add object LblAzi as cp_calclbl with uid="DILGRCBQKL",left=3, top=8, width=66,height=25,;
    caption='LblAzi',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.t.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 132226378


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=0, top=41, width=435,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="PATIPFIL",Label1="Soggetto",Field2="PACODUTE",Label2="Utente",Field3="PACODGRU",Label3="Gruppo",Field4="PAPERVIS",Label4="Filtro date",Field5="PASOLUTE",Label5="Ut.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168650106

  add object oStr_1_3 as StdString with uid="TKEPKTREMP",Visible=.t., Left=11, Top=352,;
    Alignment=1, Width=99, Height=18,;
    Caption="Nome server DB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="PHNDXKEOTR",Visible=.t., Left=-14, Top=383,;
    Alignment=1, Width=123, Height=18,;
    Caption="Nome database:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LAFSDRBBDX",Visible=.t., Left=18, Top=413,;
    Alignment=0, Width=58, Height=18,;
    Caption="Utente"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="WYDQOWWURJ",Visible=.t., Left=179, Top=413,;
    Alignment=0, Width=110, Height=18,;
    Caption="Password"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="UKIYGHKWBZ",Visible=.t., Left=268, Top=352,;
    Alignment=0, Width=60, Height=17,;
    Caption="[Archiweb]"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="TOSYSATJPQ",Visible=.t., Left=329, Top=383,;
    Alignment=0, Width=60, Height=17,;
    Caption="[Archiweb]"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="VQQBQZRLCT",Visible=.t., Left=110, Top=413,;
    Alignment=1, Width=60, Height=17,;
    Caption="[Archiweb]"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="RDVPQNVWSI",Visible=.t., Left=8, Top=320,;
    Alignment=0, Width=564, Height=22,;
    Caption="Campi momentaneamente nascosti perch� la funzionait� non � gestita"  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=65,;
    width=427+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=66,width=426+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPUSERS|CPGROUPS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oPACODUTE_2_2
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oPACODGRU_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_10 as cp_setobjprop with uid="WFXUNXJCQK",width=138,height=21,;
   left=654, top=36,;
    caption='PAPASSWD',;
   bGlobalFont=.t.,;
    cProp="passwordchar",cObj="w_PAPASSWD",;
    nPag=2;
    , HelpContextID = 262963770

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsdm_mpaBodyRow as CPBodyRowCnt
  Width=417
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPATIPFIL_2_1 as StdTrsCombo with uid="FXEZSUYKVH",rtrep=.t.,;
    cFormVar="w_PATIPFIL", RowSource=""+"Utente,"+"Gruppo,"+"Default" , ;
    HelpContextID = 42254914,;
    Height=21, Width=83, Left=-2, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPATIPFIL_2_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PATIPFIL,&i_cF..t_PATIPFIL),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    iif(xVal =3,'D',;
    space(1)))))
  endfunc
  func oPATIPFIL_2_1.GetRadio()
    this.Parent.oContained.w_PATIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oPATIPFIL_2_1.ToRadio()
    this.Parent.oContained.w_PATIPFIL=trim(this.Parent.oContained.w_PATIPFIL)
    return(;
      iif(this.Parent.oContained.w_PATIPFIL=='U',1,;
      iif(this.Parent.oContained.w_PATIPFIL=='G',2,;
      iif(this.Parent.oContained.w_PATIPFIL=='D',3,;
      0))))
  endfunc

  func oPATIPFIL_2_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPACODUTE_2_2 as StdTrsField with uid="HZGGWJFOBA",rtseq=5,rtrep=.t.,;
    cFormVar="w_PACODUTE",value=0,;
    HelpContextID = 13218363,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=86, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_PACODUTE"

  func oPACODUTE_2_2.mCond()
    with this.Parent.oContained
      return (.w_PATIPFIL='U')
    endwith
  endfunc

  func oPACODUTE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODUTE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPACODUTE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oPACODUTE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPACODGRU_2_3 as StdTrsField with uid="FXJSDZUIVQ",rtseq=6,rtrep=.t.,;
    cFormVar="w_PACODGRU",value=0,;
    HelpContextID = 221662645,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=144, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"], bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_PACODGRU"

  func oPACODGRU_2_3.mCond()
    with this.Parent.oContained
      return (.w_PATIPFIL='G')
    endwith
  endfunc

  func oPACODGRU_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODGRU_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPACODGRU_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oPACODGRU_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oPAPERVIS_2_4 as StdTrsCombo with uid="JWFOIBLNEP",rtrep=.t.,;
    cFormVar="w_PAPERVIS", RowSource=""+"Ultimi 7 giorni,"+"Ultimi 15 giorni,"+"Ultimo mese,"+"Ultimo trimestre,"+"Ultimo semestre,"+"Ultimo anno,"+"Nessun filtro" , ;
    HelpContextID = 44073545,;
    Height=21, Width=153, Left=204, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPAPERVIS_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAPERVIS,&i_cF..t_PAPERVIS),this.value)
    return(iif(xVal =1,7,;
    iif(xVal =2,15,;
    iif(xVal =3,30,;
    iif(xVal =4,90,;
    iif(xVal =5,180,;
    iif(xVal =6,360,;
    iif(xVal =7,1,;
    0))))))))
  endfunc
  func oPAPERVIS_2_4.GetRadio()
    this.Parent.oContained.w_PAPERVIS = this.RadioValue()
    return .t.
  endfunc

  func oPAPERVIS_2_4.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_PAPERVIS==7,1,;
      iif(this.Parent.oContained.w_PAPERVIS==15,2,;
      iif(this.Parent.oContained.w_PAPERVIS==30,3,;
      iif(this.Parent.oContained.w_PAPERVIS==90,4,;
      iif(this.Parent.oContained.w_PAPERVIS==180,5,;
      iif(this.Parent.oContained.w_PAPERVIS==360,6,;
      iif(this.Parent.oContained.w_PAPERVIS==1,7,;
      0))))))))
  endfunc

  func oPAPERVIS_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPAPERVIS_2_4.mCond()
    with this.Parent.oContained
      return (g_ARCW<>'S')
    endwith
  endfunc

  add object oPASOLUTE_2_9 as StdTrsCheck with uid="FBWGNWXXWY",rtrep=.t.,;
    cFormVar="w_PASOLUTE",  caption="",;
    ToolTipText = "Solo documenti archiviati dall'utente",;
    HelpContextID = 21672507,;
    Left=362, Top=0, Width=50,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oPASOLUTE_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PASOLUTE,&i_cF..t_PASOLUTE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPASOLUTE_2_9.GetRadio()
    this.Parent.oContained.w_PASOLUTE = this.RadioValue()
    return .t.
  endfunc

  func oPASOLUTE_2_9.ToRadio()
    this.Parent.oContained.w_PASOLUTE=trim(this.Parent.oContained.w_PASOLUTE)
    return(;
      iif(this.Parent.oContained.w_PASOLUTE=='S',1,;
      0))
  endfunc

  func oPASOLUTE_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPASOLUTE_2_9.mCond()
    with this.Parent.oContained
      return (g_ARCW<>'S')
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oPATIPFIL_2_1.When()
    return(.t.)
  proc oPATIPFIL_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPATIPFIL_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_mpa','DMPARAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODAZI=DMPARAM.PACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
