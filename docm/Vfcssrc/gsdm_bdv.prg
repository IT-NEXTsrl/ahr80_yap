* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bdv                                                        *
*              Check form gsir_mdv - riservato a                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-06                                                      *
* Last revis.: 2010-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pMOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bdv",oParentObject,m.pMOD)
return(i_retval)

define class tgsdm_bdv as StdBatch
  * --- Local variables
  pMOD = space(1)
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_PUNPAD = .NULL.
  w_oAREA = space(10)
  w_oPOS = 0
  w_CURSOR = space(10)
  w_TMPN = 0
  w_AQFLGZCP = space(1)
  w_MESS = space(254)
  w_PUNPAD = .NULL.
  w_CURSOR = space(10)
  w_TMPN = space(10)
  w_TMPN2 = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegno posizione per ritorno sul fondo dei controlli
    this.w_AbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
    this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
    SELECT (this.oParentObject.cTrsName)
    this.w_RECPOS = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
    if this.oParentObject.oParentObject.w_PDCHKZCP = "S" and g_IZCP$"SA" And g_CPIN="N"
      do case
        case this.pMOD = "M"
          * --- CheckForm
          * --- Inizializzazioni ...
          this.w_PUNPAD = this.oParentObject
          this.w_CURSOR = this.w_PUNPAD.cTrsname
          this.w_oAREA = ALIAS()
          * --- Memorizza posizione ...
          Select (this.w_CURSOR)
          this.w_oPOS = RECNO()
          * --- Verifica 1 - E' stato indicato un folder 
          if this.oParentObject.w_DEFOLDER=0
            this.w_MESS = AH_MsgFormat("Corporate Portal: occorre indicare un codice CPZ folder")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          * --- Il controllo seguente deve essere fatto solo se il permesso base � Riservato
          if this.oParentObject.w_DERISERV="S"
            * --- Verifica 2 - Se � stato indicato un folder di tipo company, occorre verificare che sia presente una sola riga con il flag principale attivato
            this.w_TMPN = 0
            if this.oParentObject.w_FO__TIPO="C"
              Select (this.w_CURSOR)
              SUM t_DERIFCFO TO this.w_TMPN 
              if this.w_TMPN=0
                this.w_MESS = AH_MsgFormat("Corporate Portal: occorre indicare il riferimento <Principale> necessario per identificare il Company Folder"+chr(13)+"Tale riferimento pu� essere specificato solo su righe destinatari tipo Cliente/Fornitore o Agente")
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=this.w_MESS
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                i_retcode = 'stop'
                return
              else
                if this.w_TMPN>1
                  this.w_MESS = AH_MsgFormat("Corporate Portal: esistono due o pi� righe con il flag <Principale> attivato")
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MESS
                  this.Pag2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  i_retcode = 'stop'
                  return
                endif
              endif
            endif
          endif
          * --- Ripristina ...
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.pMOD = "S"
          * --- Controllo che siano stati inseriti i permessi per i vari destinatari
          this.w_PUNPAD = this.oParentObject
          this.w_CURSOR = this.w_PUNPAD.cTrsname
          Select (this.w_CURSOR)
          calculate MAX(t_DECODGRU) TO this.w_TMPN2
          CLIFOR=.f.
          Go top 
 scan 
          if alltrim(str(t_DECODGRU)) $ "20-30" and empty(t_DEKEYINF)
            CLIFOR=.t.
            exit
          endif
          endscan
          if (empty(this.w_TMPN2) or CLIFOR) and this.oParentObject.w_DERISERV = "S" 
            this.oParentObject.w_PERMESSO = .F.
          else
            this.oParentObject.w_PERMESSO = .T.
          endif
          * --- Ripristina ...
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    else
      this.oParentObject.w_PERMESSO = .T.
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristina ...
     
 SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOS 
 With this.oParentObject 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRow 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRow 
 EndWith
    i_retcode = 'stop'
    return
  endproc


  proc Init(oParentObject,pMOD)
    this.pMOD=pMOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pMOD"
endproc
