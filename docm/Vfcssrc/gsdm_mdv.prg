* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_mdv                                                        *
*              DM - autorizzazione documenti zcp-DMS                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-22                                                      *
* Last revis.: 2011-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsdm_mdv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsdm_mdv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsdm_mdv")
  return

* --- Class definition
define class tgsdm_mdv as StdPCForm
  Width  = 750
  Height = 423
  Top    = 37
  Left   = 29
  cComment = "DM - autorizzazione documenti zcp-DMS"
  cPrg = "gsdm_mdv"
  HelpContextID=80331881
  add object cnt as tcgsdm_mdv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsdm_mdv as PCContext
  w_ATTCORP = space(1)
  w_DECODICE = space(10)
  w_DERISERV = space(1)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_DETIPDES = space(1)
  w_OTIPGRU = space(1)
  w_DECODGRU = 0
  w_DEKEYINF = space(100)
  w_DERIFCFO = space(1)
  w_DECODROL = 0
  w_DE__READ = space(1)
  w_DE_WRITE = space(1)
  w_DEDELETE = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_DEFOLDER = 0
  w_DESFOLD = space(50)
  w_PATHFOLD = space(255)
  w_DEDESFIL = space(100)
  w_DE__NOTE = space(10)
  w_TIPORIGA = space(1)
  w_PERMESSO = space(1)
  w_FO__TIPO = space(1)
  w_DESGRUPPO = space(50)
  w_DENOMFIL = space(200)
  w_RISPUB = space(1)
  w_DEFILTIP = space(5)
  w_DEFILKEY = space(50)
  proc Save(i_oFrom)
    this.w_ATTCORP = i_oFrom.w_ATTCORP
    this.w_DECODICE = i_oFrom.w_DECODICE
    this.w_DERISERV = i_oFrom.w_DERISERV
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_DETIPDES = i_oFrom.w_DETIPDES
    this.w_OTIPGRU = i_oFrom.w_OTIPGRU
    this.w_DECODGRU = i_oFrom.w_DECODGRU
    this.w_DEKEYINF = i_oFrom.w_DEKEYINF
    this.w_DERIFCFO = i_oFrom.w_DERIFCFO
    this.w_DECODROL = i_oFrom.w_DECODROL
    this.w_DE__READ = i_oFrom.w_DE__READ
    this.w_DE_WRITE = i_oFrom.w_DE_WRITE
    this.w_DEDELETE = i_oFrom.w_DEDELETE
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_DEFOLDER = i_oFrom.w_DEFOLDER
    this.w_DESFOLD = i_oFrom.w_DESFOLD
    this.w_PATHFOLD = i_oFrom.w_PATHFOLD
    this.w_DEDESFIL = i_oFrom.w_DEDESFIL
    this.w_DE__NOTE = i_oFrom.w_DE__NOTE
    this.w_TIPORIGA = i_oFrom.w_TIPORIGA
    this.w_PERMESSO = i_oFrom.w_PERMESSO
    this.w_FO__TIPO = i_oFrom.w_FO__TIPO
    this.w_DESGRUPPO = i_oFrom.w_DESGRUPPO
    this.w_DENOMFIL = i_oFrom.w_DENOMFIL
    this.w_RISPUB = i_oFrom.w_RISPUB
    this.w_DEFILTIP = i_oFrom.w_DEFILTIP
    this.w_DEFILKEY = i_oFrom.w_DEFILKEY
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ATTCORP = this.w_ATTCORP
    i_oTo.w_DECODICE = this.w_DECODICE
    i_oTo.w_DERISERV = this.w_DERISERV
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_DETIPDES = this.w_DETIPDES
    i_oTo.w_OTIPGRU = this.w_OTIPGRU
    i_oTo.w_DECODGRU = this.w_DECODGRU
    i_oTo.w_DEKEYINF = this.w_DEKEYINF
    i_oTo.w_DERIFCFO = this.w_DERIFCFO
    i_oTo.w_DECODROL = this.w_DECODROL
    i_oTo.w_DE__READ = this.w_DE__READ
    i_oTo.w_DE_WRITE = this.w_DE_WRITE
    i_oTo.w_DEDELETE = this.w_DEDELETE
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_DEFOLDER = this.w_DEFOLDER
    i_oTo.w_DESFOLD = this.w_DESFOLD
    i_oTo.w_PATHFOLD = this.w_PATHFOLD
    i_oTo.w_DEDESFIL = this.w_DEDESFIL
    i_oTo.w_DE__NOTE = this.w_DE__NOTE
    i_oTo.w_TIPORIGA = this.w_TIPORIGA
    i_oTo.w_PERMESSO = this.w_PERMESSO
    i_oTo.w_FO__TIPO = this.w_FO__TIPO
    i_oTo.w_DESGRUPPO = this.w_DESGRUPPO
    i_oTo.w_DENOMFIL = this.w_DENOMFIL
    i_oTo.w_RISPUB = this.w_RISPUB
    i_oTo.w_DEFILTIP = this.w_DEFILTIP
    i_oTo.w_DEFILKEY = this.w_DEFILKEY
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsdm_mdv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 750
  Height = 423
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-03"
  HelpContextID=80331881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRO_PCPZ_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  ZRUOLI_IDX = 0
  ZFOLDERS_IDX = 0
  ZGRUPPI_IDX = 0
  cFile = "PRO_PCPZ"
  cKeySelect = "DECODICE"
  cKeyWhere  = "DECODICE=this.w_DECODICE"
  cKeyDetail  = "DECODICE=this.w_DECODICE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"DECODICE="+cp_ToStrODBC(this.w_DECODICE)';

  cKeyDetailWhereODBC = '"DECODICE="+cp_ToStrODBC(this.w_DECODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PRO_PCPZ.DECODICE="+cp_ToStrODBC(this.w_DECODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRO_PCPZ.CPROWORD '
  cPrg = "gsdm_mdv"
  cComment = "DM - autorizzazione documenti zcp-DMS"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ATTCORP = space(1)
  o_ATTCORP = space(1)
  w_DECODICE = space(10)
  w_DERISERV = space(1)
  o_DERISERV = space(1)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_DETIPDES = space(1)
  o_DETIPDES = space(1)
  w_OTIPGRU = space(1)
  w_DECODGRU = 0
  w_DEKEYINF = space(100)
  w_DERIFCFO = space(1)
  w_DECODROL = 0
  w_DE__READ = space(1)
  w_DE_WRITE = space(1)
  w_DEDELETE = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DEFOLDER = 0
  o_DEFOLDER = 0
  w_DESFOLD = space(50)
  w_PATHFOLD = space(255)
  w_DEDESFIL = space(100)
  w_DE__NOTE = space(0)
  w_TIPORIGA = space(1)
  w_PERMESSO = .F.
  w_FO__TIPO = space(1)
  w_DESGRUPPO = space(50)
  w_DENOMFIL = space(200)
  w_RISPUB = space(1)
  w_DEFILTIP = space(5)
  w_DEFILKEY = space(50)

  * --- Children pointers
  GSDM_MPF = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSDM_MPF additive
    with this
      .Pages(1).addobject("oPag","tgsdm_mdvPag1","gsdm_mdv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDERISERV_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSDM_MPF
    * --- Area Manuale = Init Page Frame
    * --- gsdm_mdv
    
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='ZRUOLI'
    this.cWorkTables[4]='ZFOLDERS'
    this.cWorkTables[5]='ZGRUPPI'
    this.cWorkTables[6]='PRO_PCPZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_PCPZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_PCPZ_IDX,3]
  return

  function CreateChildren()
    this.GSDM_MPF = CREATEOBJECT('stdLazyChild',this,'GSDM_MPF')
    return

  procedure NewContext()
    return(createobject('tsgsdm_mdv'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSDM_MPF)
      this.GSDM_MPF.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSDM_MPF.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSDM_MPF)
      this.GSDM_MPF.DestroyChildrenChain()
      this.GSDM_MPF=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSDM_MPF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSDM_MPF.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSDM_MPF.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSDM_MPF.ChangeRow(this.cRowID+'      1',1;
             ,.w_DECODICE,"PFCODICE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRO_PCPZ where DECODICE=KeySet.DECODICE
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PCPZ_IDX,2],this.bLoadRecFilter,this.PRO_PCPZ_IDX,"gsdm_mdv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_PCPZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_PCPZ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_PCPZ '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DECODICE',this.w_DECODICE  )
      select * from (i_cTable) PRO_PCPZ where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESFOLD = space(50)
        .w_PATHFOLD = space(255)
        .w_PERMESSO = .F.
        .w_FO__TIPO = space(1)
        .w_ATTCORP = this.oParentObject .w_PDCHKZCP
        .w_DECODICE = NVL(DECODICE,space(10))
        .w_DERISERV = NVL(DERISERV,space(1))
        .w_DEFOLDER = NVL(DEFOLDER,0)
          if link_1_5_joined
            this.w_DEFOLDER = NVL(FOSERIAL105,NVL(this.w_DEFOLDER,0))
            this.w_DESFOLD = NVL(FODESCRI105,space(50))
            this.w_PATHFOLD = NVL(FO__PATH105,space(255))
            this.w_FO__TIPO = NVL(FO__TIPO105,space(1))
          else
          .link_1_5('Load')
          endif
        .w_DEDESFIL = NVL(DEDESFIL,space(100))
        .w_DE__NOTE = NVL(DE__NOTE,space(0))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_DENOMFIL = NVL(DENOMFIL,space(200))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .w_DEFILTIP = NVL(DEFILTIP,space(5))
        .w_DEFILKEY = NVL(DEFILKEY,space(50))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRO_PCPZ')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESGRUPPO = space(50)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_DETIPDES = NVL(DETIPDES,space(1))
        .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
          .w_DECODGRU = NVL(DECODGRU,0)
          .link_2_5('Load')
          .w_DEKEYINF = NVL(DEKEYINF,space(100))
          .w_DERIFCFO = NVL(DERIFCFO,space(1))
          .w_DECODROL = NVL(DECODROL,0)
          * evitabile
          *.link_2_8('Load')
          .w_DE__READ = NVL(DE__READ,space(1))
          .w_DE_WRITE = NVL(DE_WRITE,space(1))
          .w_DEDELETE = NVL(DEDELETE,space(1))
        .w_TIPORIGA = .w_DETIPDES
        .w_RISPUB = .w_DERISERV
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ATTCORP = this.oParentObject .w_PDCHKZCP
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_ATTCORP=space(1)
      .w_DECODICE=space(10)
      .w_DERISERV=space(1)
      .w_CPROWORD=10
      .w_CPROWNUM=0
      .w_DETIPDES=space(1)
      .w_OTIPGRU=space(1)
      .w_DECODGRU=0
      .w_DEKEYINF=space(100)
      .w_DERIFCFO=space(1)
      .w_DECODROL=0
      .w_DE__READ=space(1)
      .w_DE_WRITE=space(1)
      .w_DEDELETE=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DEFOLDER=0
      .w_DESFOLD=space(50)
      .w_PATHFOLD=space(255)
      .w_DEDESFIL=space(100)
      .w_DE__NOTE=space(0)
      .w_TIPORIGA=space(1)
      .w_PERMESSO=.f.
      .w_FO__TIPO=space(1)
      .w_DESGRUPPO=space(50)
      .w_DENOMFIL=space(200)
      .w_RISPUB=space(1)
      .w_DEFILTIP=space(5)
      .w_DEFILKEY=space(50)
      if .cFunction<>"Filter"
        .w_ATTCORP = this.oParentObject .w_PDCHKZCP
        .DoRTCalc(2,2,.f.)
        .w_DERISERV = 'S'
        .DoRTCalc(4,5,.f.)
        .w_DETIPDES = 'C'
        .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
        .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, .w_DECODGRU)))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DECODGRU))
         .link_2_5('Full')
        endif
        .w_DEKEYINF = space(100)
        .w_DERIFCFO = iif(.w_FO__TIPO='C',.w_DERIFCFO,'N')
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_DECODROL))
         .link_2_8('Full')
        endif
        .w_DE__READ = "S"
        .w_DE_WRITE = 'N'
        .w_DEDELETE = 'N'
        .w_OBTEST = i_DATSYS
        .DoRTCalc(16,16,.f.)
        .w_DEFOLDER = iif(.w_ATTCORP='S',.w_DEFOLDER,0)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DEFOLDER))
         .link_1_5('Full')
        endif
        .DoRTCalc(18,21,.f.)
        .w_TIPORIGA = .w_DETIPDES
        .w_PERMESSO = .F.
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(24,25,.f.)
        .w_DENOMFIL = iif(.w_ATTCORP='S',.w_DENOMFIL,'')
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .w_RISPUB = .w_DERISERV
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_PCPZ')
    this.DoRTCalc(28,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDERISERV_1_3.enabled_(i_bVal)
      .Page1.oPag.oDEFOLDER_1_5.enabled = i_bVal
      .Page1.oPag.oDENOMFIL_1_16.enabled = i_bVal
      .Page1.oPag.oDEFILTIP_1_18.enabled = i_bVal
      .Page1.oPag.oDEFILKEY_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSDM_MPF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PRO_PCPZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSDM_MPF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODICE,"DECODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DERISERV,"DERISERV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEFOLDER,"DEFOLDER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDESFIL,"DEDESFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__NOTE,"DE__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DENOMFIL,"DENOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEFILTIP,"DEFILTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEFILKEY,"DEFILKEY",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DETIPDES N(3);
      ,t_DECODGRU N(6);
      ,t_DEKEYINF C(100);
      ,t_DERIFCFO N(3);
      ,t_DE__READ N(3);
      ,t_DE_WRITE N(3);
      ,t_DEDELETE N(3);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(6);
      ,t_OTIPGRU C(1);
      ,t_DECODROL N(6);
      ,t_TIPORIGA C(1);
      ,t_DESGRUPPO C(50);
      ,t_RISPUB C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsdm_mdvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_3.controlsource=this.cTrsName+'.t_DETIPDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_5.controlsource=this.cTrsName+'.t_DECODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEKEYINF_2_6.controlsource=this.cTrsName+'.t_DEKEYINF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_7.controlsource=this.cTrsName+'.t_DERIFCFO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_9.controlsource=this.cTrsName+'.t_DE__READ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_10.controlsource=this.cTrsName+'.t_DE_WRITE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_11.controlsource=this.cTrsName+'.t_DEDELETE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(48)
    this.AddVLine(157)
    this.AddVLine(221)
    this.AddVLine(629)
    this.AddVLine(651)
    this.AddVLine(676)
    this.AddVLine(698)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PCPZ_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PCPZ_IDX,2])
      *
      * insert into PRO_PCPZ
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_PCPZ')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_PCPZ')
        i_cFldBody=" "+;
                  "(DECODICE,DERISERV,CPROWORD,DETIPDES,DECODGRU"+;
                  ",DEKEYINF,DERIFCFO,DECODROL,DE__READ,DE_WRITE"+;
                  ",DEDELETE,DEFOLDER,DEDESFIL,DE__NOTE,DENOMFIL"+;
                  ",DEFILTIP,DEFILKEY,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DECODICE)+","+cp_ToStrODBC(this.w_DERISERV)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DETIPDES)+","+cp_ToStrODBCNull(this.w_DECODGRU)+;
             ","+cp_ToStrODBC(this.w_DEKEYINF)+","+cp_ToStrODBC(this.w_DERIFCFO)+","+cp_ToStrODBCNull(this.w_DECODROL)+","+cp_ToStrODBC(this.w_DE__READ)+","+cp_ToStrODBC(this.w_DE_WRITE)+;
             ","+cp_ToStrODBC(this.w_DEDELETE)+","+cp_ToStrODBCNull(this.w_DEFOLDER)+","+cp_ToStrODBC(this.w_DEDESFIL)+","+cp_ToStrODBC(this.w_DE__NOTE)+","+cp_ToStrODBC(this.w_DENOMFIL)+;
             ","+cp_ToStrODBC(this.w_DEFILTIP)+","+cp_ToStrODBC(this.w_DEFILKEY)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_PCPZ')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_PCPZ')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DECODICE',this.w_DECODICE,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable) (;
                   DECODICE;
                  ,DERISERV;
                  ,CPROWORD;
                  ,DETIPDES;
                  ,DECODGRU;
                  ,DEKEYINF;
                  ,DERIFCFO;
                  ,DECODROL;
                  ,DE__READ;
                  ,DE_WRITE;
                  ,DEDELETE;
                  ,DEFOLDER;
                  ,DEDESFIL;
                  ,DE__NOTE;
                  ,DENOMFIL;
                  ,DEFILTIP;
                  ,DEFILKEY;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DECODICE;
                  ,this.w_DERISERV;
                  ,this.w_CPROWORD;
                  ,this.w_DETIPDES;
                  ,this.w_DECODGRU;
                  ,this.w_DEKEYINF;
                  ,this.w_DERIFCFO;
                  ,this.w_DECODROL;
                  ,this.w_DE__READ;
                  ,this.w_DE_WRITE;
                  ,this.w_DEDELETE;
                  ,this.w_DEFOLDER;
                  ,this.w_DEDESFIL;
                  ,this.w_DE__NOTE;
                  ,this.w_DENOMFIL;
                  ,this.w_DEFILTIP;
                  ,this.w_DEFILKEY;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PCPZ_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not(Empty(t_CPROWORD)) OR t_RISPUB = 'N') and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_PCPZ')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DERISERV="+cp_ToStrODBC(this.w_DERISERV)+;
                 ",DEFOLDER="+cp_ToStrODBCNull(this.w_DEFOLDER)+;
                 ",DEDESFIL="+cp_ToStrODBC(this.w_DEDESFIL)+;
                 ",DE__NOTE="+cp_ToStrODBC(this.w_DE__NOTE)+;
                 ",DENOMFIL="+cp_ToStrODBC(this.w_DENOMFIL)+;
                 ",DEFILTIP="+cp_ToStrODBC(this.w_DEFILTIP)+;
                 ",DEFILKEY="+cp_ToStrODBC(this.w_DEFILKEY)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_PCPZ')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DERISERV=this.w_DERISERV;
                 ,DEFOLDER=this.w_DEFOLDER;
                 ,DEDESFIL=this.w_DEDESFIL;
                 ,DE__NOTE=this.w_DE__NOTE;
                 ,DENOMFIL=this.w_DENOMFIL;
                 ,DEFILTIP=this.w_DEFILTIP;
                 ,DEFILKEY=this.w_DEFILKEY;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not(Empty(t_CPROWORD)) OR t_RISPUB = 'N') and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRO_PCPZ
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_PCPZ')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DERISERV="+cp_ToStrODBC(this.w_DERISERV)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DETIPDES="+cp_ToStrODBC(this.w_DETIPDES)+;
                     ",DECODGRU="+cp_ToStrODBCNull(this.w_DECODGRU)+;
                     ",DEKEYINF="+cp_ToStrODBC(this.w_DEKEYINF)+;
                     ",DERIFCFO="+cp_ToStrODBC(this.w_DERIFCFO)+;
                     ",DECODROL="+cp_ToStrODBCNull(this.w_DECODROL)+;
                     ",DE__READ="+cp_ToStrODBC(this.w_DE__READ)+;
                     ",DE_WRITE="+cp_ToStrODBC(this.w_DE_WRITE)+;
                     ",DEDELETE="+cp_ToStrODBC(this.w_DEDELETE)+;
                     ",DEFOLDER="+cp_ToStrODBCNull(this.w_DEFOLDER)+;
                     ",DEDESFIL="+cp_ToStrODBC(this.w_DEDESFIL)+;
                     ",DE__NOTE="+cp_ToStrODBC(this.w_DE__NOTE)+;
                     ",DENOMFIL="+cp_ToStrODBC(this.w_DENOMFIL)+;
                     ",DEFILTIP="+cp_ToStrODBC(this.w_DEFILTIP)+;
                     ",DEFILKEY="+cp_ToStrODBC(this.w_DEFILKEY)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_PCPZ')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DERISERV=this.w_DERISERV;
                     ,CPROWORD=this.w_CPROWORD;
                     ,DETIPDES=this.w_DETIPDES;
                     ,DECODGRU=this.w_DECODGRU;
                     ,DEKEYINF=this.w_DEKEYINF;
                     ,DERIFCFO=this.w_DERIFCFO;
                     ,DECODROL=this.w_DECODROL;
                     ,DE__READ=this.w_DE__READ;
                     ,DE_WRITE=this.w_DE_WRITE;
                     ,DEDELETE=this.w_DEDELETE;
                     ,DEFOLDER=this.w_DEFOLDER;
                     ,DEDESFIL=this.w_DEDESFIL;
                     ,DE__NOTE=this.w_DE__NOTE;
                     ,DENOMFIL=this.w_DENOMFIL;
                     ,DEFILTIP=this.w_DEFILTIP;
                     ,DEFILKEY=this.w_DEFILKEY;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask footer and header children to save themselves
      * --- GSDM_MPF : Saving
      this.GSDM_MPF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DECODICE,"PFCODICE";
             )
      this.GSDM_MPF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSDM_MPF : Deleting
    this.GSDM_MPF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DECODICE,"PFCODICE";
           )
    this.GSDM_MPF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_PCPZ_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not(Empty(t_CPROWORD)) OR t_RISPUB = 'N') and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRO_PCPZ
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not(Empty(t_CPROWORD)) OR t_RISPUB = 'N') and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_PCPZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_PCPZ_IDX,2])
    if i_bUpd
      with this
          .w_ATTCORP = this.oParentObject .w_PDCHKZCP
        .DoRTCalc(2,6,.t.)
          .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
        if .o_DETIPDES<>.w_DETIPDES
          .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, .w_DECODGRU)))
          .link_2_5('Full')
        endif
        if .o_DETIPDES<>.w_DETIPDES
          .w_DEKEYINF = space(100)
        endif
        if .o_DETIPDES<>.w_DETIPDES.or. .o_DEFOLDER<>.w_DEFOLDER
          .w_DERIFCFO = iif(.w_FO__TIPO='C',.w_DERIFCFO,'N')
        endif
          .link_2_8('Full')
        .DoRTCalc(12,12,.t.)
        if .o_DETIPDES<>.w_DETIPDES
          .w_DE_WRITE = 'N'
        endif
        if .o_DETIPDES<>.w_DETIPDES
          .w_DEDELETE = 'N'
        endif
        .DoRTCalc(15,16,.t.)
        if .o_ATTCORP<>.w_ATTCORP.or. .o_DERISERV<>.w_DERISERV
          .w_DEFOLDER = iif(.w_ATTCORP='S',.w_DEFOLDER,0)
          .link_1_5('Full')
        endif
        .DoRTCalc(18,21,.t.)
          .w_TIPORIGA = .w_DETIPDES
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(23,25,.t.)
        if .o_ATTCORP<>.w_ATTCORP
          .w_DENOMFIL = iif(.w_ATTCORP='S',.w_DENOMFIL,'')
        endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
          .w_RISPUB = .w_DERISERV
        if .o_DETIPDES<>.w_DETIPDES
          .Calculate_WSKSXBJBOZ()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_OTIPGRU with this.w_OTIPGRU
      replace t_DECODROL with this.w_DECODROL
      replace t_TIPORIGA with this.w_TIPORIGA
      replace t_DESGRUPPO with this.w_DESGRUPPO
      replace t_RISPUB with this.w_RISPUB
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_WSKSXBJBOZ()
    with this
          * --- Valorizzazione codice gruppo
          .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, 0)))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODGRU_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODGRU_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEKEYINF_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEKEYINF_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDERIFCFO_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDERIFCFO_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("Init Row") or lower(cEvent)==lower("New record")
          .Calculate_WSKSXBJBOZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DECODGRU
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_DECODGRU);
                   +" and grptype="+cp_ToStrODBC(this.w_OTIPGRU);

          i_ret=cp_SQL(i_nConn,"select grptype,code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'grptype',this.w_OTIPGRU;
                     ,'code',this.w_DECODGRU)
          select grptype,code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DECODGRU) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','grptype,code',cp_AbsName(oSource.parent,'oDECODGRU_2_5'),i_cWhere,'',"Gruppi organizzativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OTIPGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select grptype,code,name";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select grptype,code,name;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select grptype,code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(2));
                     +" and grptype="+cp_ToStrODBC(this.w_OTIPGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'grptype',oSource.xKey(1);
                       ,'code',oSource.xKey(2))
            select grptype,code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select grptype,code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_DECODGRU);
                   +" and grptype="+cp_ToStrODBC(this.w_OTIPGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'grptype',this.w_OTIPGRU;
                       ,'code',this.w_DECODGRU)
            select grptype,code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODGRU = NVL(_Link_.code,0)
      this.w_DESGRUPPO = NVL(_Link_.name,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DECODGRU = 0
      endif
      this.w_DESGRUPPO = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.grptype,1)+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DECODROL
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODROL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODROL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_DECODROL);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_DECODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_DECODGRU;
                       ,'GRROLE',this.w_DECODROL)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODROL = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_DECODROL = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODROL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DEFOLDER
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_lTable = "ZFOLDERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2], .t., this.ZFOLDERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DEFOLDER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZFOLDERS')
        if i_nConn<>0
          i_cWhere = " FOSERIAL="+cp_ToStrODBC(this.w_DEFOLDER);

          i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOSERIAL',this.w_DEFOLDER)
          select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DEFOLDER) and !this.bDontReportError
            deferred_cp_zoom('ZFOLDERS','*','FOSERIAL',cp_AbsName(oSource.parent,'oDEFOLDER_1_5'),i_cWhere,'',"Elenco folders",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOSERIAL',oSource.xKey(1))
            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DEFOLDER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(this.w_DEFOLDER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOSERIAL',this.w_DEFOLDER)
            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DEFOLDER = NVL(_Link_.FOSERIAL,0)
      this.w_DESFOLD = NVL(_Link_.FODESCRI,space(50))
      this.w_PATHFOLD = NVL(_Link_.FO__PATH,space(255))
      this.w_FO__TIPO = NVL(_Link_.FO__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DEFOLDER = 0
      endif
      this.w_DESFOLD = space(50)
      this.w_PATHFOLD = space(255)
      this.w_FO__TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DERISERV='N' and .w_FO__TIPO<>'C') or .w_DERISERV='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipologia folder non consentita per il permesso base selezionato")
        endif
        this.w_DEFOLDER = 0
        this.w_DESFOLD = space(50)
        this.w_PATHFOLD = space(255)
        this.w_FO__TIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])+'\'+cp_ToStr(_Link_.FOSERIAL,1)
      cp_ShowWarn(i_cKey,this.ZFOLDERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DEFOLDER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZFOLDERS_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.FOSERIAL as FOSERIAL105"+ ",link_1_5.FODESCRI as FODESCRI105"+ ",link_1_5.FO__PATH as FO__PATH105"+ ",link_1_5.FO__TIPO as FO__TIPO105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on PRO_PCPZ.DEFOLDER=link_1_5.FOSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and PRO_PCPZ.DEFOLDER=link_1_5.FOSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDERISERV_1_3.RadioValue()==this.w_DERISERV)
      this.oPgFrm.Page1.oPag.oDERISERV_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFOLDER_1_5.value==this.w_DEFOLDER)
      this.oPgFrm.Page1.oPag.oDEFOLDER_1_5.value=this.w_DEFOLDER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOLD_1_6.value==this.w_DESFOLD)
      this.oPgFrm.Page1.oPag.oDESFOLD_1_6.value=this.w_DESFOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHFOLD_1_7.value==this.w_PATHFOLD)
      this.oPgFrm.Page1.oPag.oPATHFOLD_1_7.value=this.w_PATHFOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOMFIL_1_16.value==this.w_DENOMFIL)
      this.oPgFrm.Page1.oPag.oDENOMFIL_1_16.value=this.w_DENOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFILTIP_1_18.value==this.w_DEFILTIP)
      this.oPgFrm.Page1.oPag.oDEFILTIP_1_18.value=this.w_DEFILTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFILKEY_1_21.value==this.w_DEFILKEY)
      this.oPgFrm.Page1.oPag.oDEFILKEY_1_21.value=this.w_DEFILKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_3.RadioValue()==this.w_DETIPDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_3.SetRadio()
      replace t_DETIPDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_5.value==this.w_DECODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_5.value=this.w_DECODGRU
      replace t_DECODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEKEYINF_2_6.value==this.w_DEKEYINF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEKEYINF_2_6.value=this.w_DEKEYINF
      replace t_DEKEYINF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEKEYINF_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_7.RadioValue()==this.w_DERIFCFO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_7.SetRadio()
      replace t_DERIFCFO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_9.RadioValue()==this.w_DE__READ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_9.SetRadio()
      replace t_DE__READ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_10.RadioValue()==this.w_DE_WRITE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_10.SetRadio()
      replace t_DE_WRITE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_11.RadioValue()==this.w_DEDELETE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_11.SetRadio()
      replace t_DEDELETE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'PRO_PCPZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DERISERV='N' and .w_FO__TIPO<>'C') or .w_DERISERV='S')  and not(empty(.w_DEFOLDER))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDEFOLDER_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipologia folder non consentita per il permesso base selezionato")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSDM_MPF.CheckForm()
      if i_bres
        i_bres=  .GSDM_MPF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsdm_mdv
      if g_IZCP$'SA' and empty(this.w_DEFOLDER) and this.oParentoBject.w_PDCHKZCP='S' and g_CPIN='N'
        i_bres=.f.
       ah_errormsg("Errore: Corporate Portal: occorre indicare un codice CPZ Folder","!")
      endif
      
      if g_IZCP$'SA' and empty(this.W_DENOMFIL) and this.oParentoBject.w_PDCHKZCP='S' and g_CPIN='N'
        i_bres=.f.
       ah_errormsg("Errore: Corporate Portal: occorre specificare un valore nel campo <Nome file>","!")
      endif
      
      if g_IZCP$'SA' and g_CPIN='N'
        DO GSDM_BDV WITH THIS,'S'
        if ! this.w_PERMESSO
         ah_errormsg("Errore: Corporate Portal: occorre specificare nome espressione per cliente/fornitore/agente o codice gruppo","!")
           i_bres=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if Not(Empty(.w_CPROWORD)) OR .w_RISPUB = 'N'
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATTCORP = this.w_ATTCORP
    this.o_DERISERV = this.w_DERISERV
    this.o_DETIPDES = this.w_DETIPDES
    this.o_DEFOLDER = this.w_DEFOLDER
    * --- GSDM_MPF : Depends On
    this.GSDM_MPF.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not(Empty(t_CPROWORD)) OR t_RISPUB = 'N')
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DETIPDES=space(1)
      .w_OTIPGRU=space(1)
      .w_DECODGRU=0
      .w_DEKEYINF=space(100)
      .w_DERIFCFO=space(1)
      .w_DECODROL=0
      .w_DE__READ=space(1)
      .w_DE_WRITE=space(1)
      .w_DEDELETE=space(1)
      .w_TIPORIGA=space(1)
      .w_DESGRUPPO=space(50)
      .w_RISPUB=space(1)
      .DoRTCalc(1,5,.f.)
        .w_DETIPDES = 'C'
        .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
        .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, .w_DECODGRU)))
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_DECODGRU))
        .link_2_5('Full')
      endif
        .w_DEKEYINF = space(100)
        .w_DERIFCFO = iif(.w_FO__TIPO='C',.w_DERIFCFO,'N')
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_DECODROL))
        .link_2_8('Full')
      endif
        .w_DE__READ = "S"
        .w_DE_WRITE = 'N'
        .w_DEDELETE = 'N'
      .DoRTCalc(15,21,.f.)
        .w_TIPORIGA = .w_DETIPDES
      .DoRTCalc(23,26,.f.)
        .w_RISPUB = .w_DERISERV
    endwith
    this.DoRTCalc(28,29,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CPROWNUM = t_CPROWNUM
    this.w_DETIPDES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_3.RadioValue(.t.)
    this.w_OTIPGRU = t_OTIPGRU
    this.w_DECODGRU = t_DECODGRU
    this.w_DEKEYINF = t_DEKEYINF
    this.w_DERIFCFO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_7.RadioValue(.t.)
    this.w_DECODROL = t_DECODROL
    this.w_DE__READ = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_9.RadioValue(.t.)
    this.w_DE_WRITE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_10.RadioValue(.t.)
    this.w_DEDELETE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_11.RadioValue(.t.)
    this.w_TIPORIGA = t_TIPORIGA
    this.w_DESGRUPPO = t_DESGRUPPO
    this.w_RISPUB = t_RISPUB
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_DETIPDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_3.ToRadio()
    replace t_OTIPGRU with this.w_OTIPGRU
    replace t_DECODGRU with this.w_DECODGRU
    replace t_DEKEYINF with this.w_DEKEYINF
    replace t_DERIFCFO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_7.ToRadio()
    replace t_DECODROL with this.w_DECODROL
    replace t_DE__READ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_9.ToRadio()
    replace t_DE_WRITE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_10.ToRadio()
    replace t_DEDELETE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_11.ToRadio()
    replace t_TIPORIGA with this.w_TIPORIGA
    replace t_DESGRUPPO with this.w_DESGRUPPO
    replace t_RISPUB with this.w_RISPUB
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsdm_mdvPag1 as StdContainer
  Width  = 746
  height = 423
  stdWidth  = 746
  stdheight = 423
  resizeXpos=378
  resizeYpos=250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDERISERV_1_3 as StdRadio with uid="RYBOWKVNMY",rtseq=3,rtrep=.f.,left=119, top=5, width=91,height=39;
    , ToolTipText = "Flag riservato/pubblico";
    , cFormVar="w_DERISERV", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDERISERV_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Riservato"
      this.Buttons(1).HelpContextID = 95724940
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Pubblico"
      this.Buttons(2).HelpContextID = 95724940
      this.Buttons(2).Top=18
      this.SetAll("Width",89)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Flag riservato/pubblico")
      StdRadio::init()
    endproc

  func oDERISERV_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DERISERV,&i_cF..t_DERISERV),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    'N')))
  endfunc
  func oDERISERV_1_3.GetRadio()
    this.Parent.oContained.w_DERISERV = this.RadioValue()
    return .t.
  endfunc

  func oDERISERV_1_3.ToRadio()
    this.Parent.oContained.w_DERISERV=trim(this.Parent.oContained.w_DERISERV)
    return(;
      iif(this.Parent.oContained.w_DERISERV=='S',1,;
      iif(this.Parent.oContained.w_DERISERV=='N',2,;
      0)))
  endfunc

  func oDERISERV_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEFOLDER_1_5 as StdField with uid="BTMXUBVQEY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DEFOLDER", cQueryName = "DEFOLDER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Tipologia folder non consentita per il permesso base selezionato",;
    ToolTipText = "Codice CPZ folder",;
    HelpContextID = 71951752,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=287, Top=5, cSayPict='"@Z 9999999999"', bHasZoom = .t. , cLinkFile="ZFOLDERS", oKey_1_1="FOSERIAL", oKey_1_2="this.w_DEFOLDER"

  func oDEFOLDER_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDEFOLDER_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDEFOLDER_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZFOLDERS','*','FOSERIAL',cp_AbsName(this.parent,'oDEFOLDER_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco folders",'',this.parent.oContained
  endproc

  add object oDESFOLD_1_6 as StdField with uid="IRSQHACCCX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESFOLD", cQueryName = "DESFOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione CPZ folder",;
    HelpContextID = 208778550,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=386, Top=5, InputMask=replicate('X',50)

  add object oPATHFOLD_1_7 as StdField with uid="CJBIDMRUBP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PATHFOLD", cQueryName = "PATHFOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    ToolTipText = "Path folder su DMS",;
    HelpContextID = 18628038,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=371, Left=287, Top=29, InputMask=replicate('X',255)


  add object oObj_1_14 as cp_runprogram with uid="NAPUDWHAAT",left=381, top=429, width=390,height=19,;
    caption='GSDM_BDV(M)',;
   bGlobalFont=.t.,;
    prg="GSDM_BDV('M')",;
    cEvent = "Insert start,Insert row start,Update start,Update row start",;
    nPag=1;
    , HelpContextID = 58372924

  add object oDENOMFIL_1_16 as StdField with uid="QEKRIXFZJQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DENOMFIL", cQueryName = "DENOMFIL",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Espressione per determinazione del nome file (es.: dtos (mvdatdoc) +mvalfdoc+...)",;
    HelpContextID = 161847934,;
   bGlobalFont=.t.,;
    Height=21, Width=626, Left=92, Top=57, InputMask=replicate('X',200)


  add object oObj_1_17 as cp_runprogram with uid="WSRRUWECSI",left=105, top=429, width=259,height=19,;
    caption='GSDM_BRP(P)',;
   bGlobalFont=.t.,;
    prg="GSDM_BRP('P')",;
    cEvent = "w_DERISERV Changed",;
    nPag=1;
    , HelpContextID = 58373686

  add object oDEFILTIP_1_18 as StdField with uid="LALWJCZHPW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DEFILTIP", cQueryName = "DEFILTIP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia documento per DMS (esempio: docum)",;
    HelpContextID = 196876922,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=92, Top=82, InputMask=replicate('X',5)

  add object oDEFILKEY_1_21 as StdField with uid="PRLQJUSYFJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DEFILKEY", cQueryName = "DEFILKEY",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Chiave univoca per identificazione documento",;
    HelpContextID = 188999055,;
   bGlobalFont=.t.,;
    Height=21, Width=442, Left=276, Top=82, InputMask=replicate('X',50)


  add object oLinkPC_1_23 as StdButton with uid="ZNVIIBEFST",left=667, top=6, width=48,height=45,;
    CpPicture="bmp\parametri.bmp", caption="", nPag=1;
    , ToolTipText = "Accede alla maschera di definizione parametri";
    , HelpContextID = 124700920;
    , caption='\<Parametri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_23.Click()
      this.Parent.oContained.GSDM_MPF.LinkPCClick()
    endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=12, top=109, width=725,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="DETIPDES",Label2="Tipo",Field3="DECODGRU",Label3="Gruppo",Field4="DEKEYINF",Label4="Espressione per cliente/fornitore/agente",Field5="DERIFCFO",Label5="PR",Field6="DE__READ",Label6="R",Field7="DE_WRITE",Label7="W",Field8="DEDELETE",Label8="D",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235758970

  add object oStr_1_4 as StdString with uid="NIHHFABBVG",Visible=.t., Left=771, Top=183,;
    Alignment=1, Width=42, Height=18,;
    Caption="Ruolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="ZMUPBXOVHC",Visible=.t., Left=8, Top=5,;
    Alignment=1, Width=107, Height=18,;
    Caption="Permessi base:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ATNZURRIRU",Visible=.t., Left=216, Top=5,;
    Alignment=1, Width=67, Height=18,;
    Caption="CPZ folder:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="AMDMCQTJSD",Visible=.t., Left=18, Top=57,;
    Alignment=1, Width=69, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GDJSVNGWVA",Visible=.t., Left=27, Top=85,;
    Alignment=1, Width=60, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YLUTUUTFUW",Visible=.t., Left=150, Top=85,;
    Alignment=1, Width=123, Height=18,;
    Caption="Chiave documento:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=129,;
    width=721+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=130,width=720+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ZGRUPPI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ZGRUPPI'
        oDropInto=this.oBodyCol.oRow.oDECODGRU_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsdm_mdvBodyRow as CPBodyRowCnt
  Width=711
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="KUICTKTBBH",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268087402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=36, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oDETIPDES_2_3 as StdTrsCombo with uid="HXGOLSXAGQ",rtrep=.t.,;
    cFormVar="w_DETIPDES", RowSource=""+"Agente,"+"Cliente/Fornitore,"+"Gruppo" , ;
    HelpContextID = 75810185,;
    Height=21, Width=105, Left=37, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDETIPDES_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DETIPDES,&i_cF..t_DETIPDES),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'C',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oDETIPDES_2_3.GetRadio()
    this.Parent.oContained.w_DETIPDES = this.RadioValue()
    return .t.
  endfunc

  func oDETIPDES_2_3.ToRadio()
    this.Parent.oContained.w_DETIPDES=trim(this.Parent.oContained.w_DETIPDES)
    return(;
      iif(this.Parent.oContained.w_DETIPDES=='A',1,;
      iif(this.Parent.oContained.w_DETIPDES=='C',2,;
      iif(this.Parent.oContained.w_DETIPDES=='G',3,;
      0))))
  endfunc

  func oDETIPDES_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDECODGRU_2_5 as StdTrsField with uid="WBLZRBUJID",rtseq=8,rtrep=.t.,;
    cFormVar="w_DECODGRU",value=0,;
    HelpContextID = 113882507,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=60, Left=146, Top=-1, cSayPict=["@Z 999999"], cGetPict=["@Z 999999"], bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="grptype", oKey_1_2="this.w_OTIPGRU", oKey_2_1="code", oKey_2_2="this.w_DECODGRU"

  func oDECODGRU_2_5.mCond()
    with this.Parent.oContained
      return (Not(.w_DETIPDES$'ACF'))
    endwith
  endfunc

  func oDECODGRU_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
      if .not. empty(.w_DECODROL)
        bRes2=.link_2_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDECODGRU_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDECODGRU_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ZGRUPPI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"grptype="+cp_ToStrODBC(this.Parent.oContained.w_OTIPGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"grptype="+cp_ToStr(this.Parent.oContained.w_OTIPGRU)
    endif
    do cp_zoom with 'ZGRUPPI','*','grptype,code',cp_AbsName(this.parent,'oDECODGRU_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi organizzativi",'',this.parent.oContained
  endproc

  add object oDEKEYINF_2_6 as StdTrsField with uid="QMQBFNPPNP",rtseq=9,rtrep=.t.,;
    cFormVar="w_DEKEYINF",value=space(100),;
    HelpContextID = 99601028,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=404, Left=210, Top=0, InputMask=replicate('X',100)

  func oDEKEYINF_2_6.mCond()
    with this.Parent.oContained
      return (.w_DETIPDES $ 'CA')
    endwith
  endfunc

  add object oDERIFCFO_2_7 as StdTrsCheck with uid="MLOYHGWVOW",rtrep=.t.,;
    cFormVar="w_DERIFCFO",  caption="",;
    ToolTipText = "Check riferimento company folder",;
    HelpContextID = 48539013,;
    Left=619, Top=-1, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , TABSTOP=.F. , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDERIFCFO_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DERIFCFO,&i_cF..t_DERIFCFO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDERIFCFO_2_7.GetRadio()
    this.Parent.oContained.w_DERIFCFO = this.RadioValue()
    return .t.
  endfunc

  func oDERIFCFO_2_7.ToRadio()
    this.Parent.oContained.w_DERIFCFO=trim(this.Parent.oContained.w_DERIFCFO)
    return(;
      iif(this.Parent.oContained.w_DERIFCFO=='S',1,;
      0))
  endfunc

  func oDERIFCFO_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDERIFCFO_2_7.mCond()
    with this.Parent.oContained
      return (.w_FO__TIPO='C' and .w_DETIPDES<>'G')
    endwith
  endfunc

  add object oDE__READ_2_9 as StdTrsCheck with uid="SNXSRXPZEP",rtrep=.t.,;
    cFormVar="w_DE__READ",  caption="",;
    HelpContextID = 96171386,;
    Left=639, Top=-1, Width=23,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDE__READ_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DE__READ,&i_cF..t_DE__READ),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDE__READ_2_9.GetRadio()
    this.Parent.oContained.w_DE__READ = this.RadioValue()
    return .t.
  endfunc

  func oDE__READ_2_9.ToRadio()
    this.Parent.oContained.w_DE__READ=trim(this.Parent.oContained.w_DE__READ)
    return(;
      iif(this.Parent.oContained.w_DE__READ=='S',1,;
      0))
  endfunc

  func oDE__READ_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDE_WRITE_2_10 as StdTrsCheck with uid="KRVMHUEBHD",rtrep=.t.,;
    cFormVar="w_DE_WRITE",  caption="",;
    HelpContextID = 162755963,;
    Left=664, Top=-1, Width=18,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDE_WRITE_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DE_WRITE,&i_cF..t_DE_WRITE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDE_WRITE_2_10.GetRadio()
    this.Parent.oContained.w_DE_WRITE = this.RadioValue()
    return .t.
  endfunc

  func oDE_WRITE_2_10.ToRadio()
    this.Parent.oContained.w_DE_WRITE=trim(this.Parent.oContained.w_DE_WRITE)
    return(;
      iif(this.Parent.oContained.w_DE_WRITE=='S',1,;
      0))
  endfunc

  func oDE_WRITE_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEDELETE_2_11 as StdTrsCheck with uid="VUQLWTWUMH",rtrep=.t.,;
    cFormVar="w_DEDELETE",  caption="",;
    HelpContextID = 88065403,;
    Left=688, Top=-1, Width=18,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDEDELETE_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEDELETE,&i_cF..t_DEDELETE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDEDELETE_2_11.GetRadio()
    this.Parent.oContained.w_DEDELETE = this.RadioValue()
    return .t.
  endfunc

  func oDEDELETE_2_11.ToRadio()
    this.Parent.oContained.w_DEDELETE=trim(this.Parent.oContained.w_DEDELETE)
    return(;
      iif(this.Parent.oContained.w_DEDELETE=='S',1,;
      0))
  endfunc

  func oDEDELETE_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_mdv','PRO_PCPZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DECODICE=PRO_PCPZ.DECODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
