* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kma                                                        *
*              Aggiornamento massivo tipologia e classe allegato               *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-04                                                      *
* Last revis.: 2009-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kma",oParentObject))

* --- Class definition
define class tgsdm_kma as StdForm
  Top    = 32
  Left   = 25

  * --- Standard Properties
  Width  = 749
  Height = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-11"
  HelpContextID=199869545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  cPrg = "gsdm_kma"
  cComment = "Aggiornamento massivo tipologia e classe allegato  "
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCLAS = space(15)
  w_SELEZI = space(1)
  w_TIPALL = space(5)
  o_TIPALL = space(5)
  w_DESCTIPAL = space(40)
  w_CLAALL = space(5)
  w_DESCLAAL = space(40)
  w_CLASSI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kmaPag1","gsdm_kma",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CLASSI = this.oPgFrm.Pages(1).oPag.CLASSI
    DoDefault()
    proc Destroy()
      this.w_CLASSI = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_ALLE'
    this.cWorkTables[2]='CLA_ALLE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCLAS=space(15)
      .w_SELEZI=space(1)
      .w_TIPALL=space(5)
      .w_DESCTIPAL=space(40)
      .w_CLAALL=space(5)
      .w_DESCLAAL=space(40)
      .oPgFrm.Page1.oPag.CLASSI.Calculate()
        .w_CODCLAS = .w_CLASSI.getvar('CDCODCLA')
        .w_SELEZI = "D"
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPALL))
          .link_1_6('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_CLAALL = iif(not empty(.w_TIPALL),.w_CLAALL,'')
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CLAALL))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CLASSI.Calculate()
            .w_CODCLAS = .w_CLASSI.getvar('CDCODCLA')
        .DoRTCalc(2,4,.t.)
        if .o_TIPALL<>.w_TIPALL
            .w_CLAALL = iif(not empty(.w_TIPALL),.w_CLAALL,'')
          .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CLASSI.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLAALL_1_8.enabled = this.oPgFrm.Page1.oPag.oCLAALL_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CLASSI.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPALL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_TIPALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_TIPALL))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oTIPALL_1_6'),i_cWhere,'',"Tipologia allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_TIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPALL)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESCTIPAL = NVL(_Link_.TADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TIPALL = space(5)
      endif
      this.w_DESCTIPAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAALL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CLAALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_TIPALL;
                     ,'TACODCLA',trim(this.w_CLAALL))
          select TACODICE,TACODCLA,TACLADES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCLAALL_1_8'),i_cWhere,'GSUT_MTA',"Classe allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_TIPALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CLAALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPALL;
                       ,'TACODCLA',this.w_CLAALL)
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLAAL = NVL(_Link_.TACLADES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLAALL = space(5)
      endif
      this.w_DESCLAAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_5.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPALL_1_6.value==this.w_TIPALL)
      this.oPgFrm.Page1.oPag.oTIPALL_1_6.value=this.w_TIPALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTIPAL_1_7.value==this.w_DESCTIPAL)
      this.oPgFrm.Page1.oPag.oDESCTIPAL_1_7.value=this.w_DESCTIPAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAALL_1_8.value==this.w_CLAALL)
      this.oPgFrm.Page1.oPag.oCLAALL_1_8.value=this.w_CLAALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAAL_1_9.value==this.w_DESCLAAL)
      this.oPgFrm.Page1.oPag.oDESCLAAL_1_9.value=this.w_DESCLAAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPALL = this.w_TIPALL
    return

enddefine

* --- Define pages as container
define class tgsdm_kmaPag1 as StdContainer
  Width  = 745
  height = 343
  stdWidth  = 745
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CLASSI as cp_szoombox with uid="OMAYYTGWPU",left=7, top=5, width=731,height=270,;
    caption='',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",cTable="PROMCLAS",cZoomFile="GSDM_KMA",bOptions=.f.,;
    cEvent = "Init,Calcola",;
    nPag=1;
    , HelpContextID = 199869450

  add object oSELEZI_1_5 as StdRadio with uid="BUGXSLLSWC",rtseq=2,rtrep=.f.,left=11, top=291, width=134,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_5.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 218085850
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 218085850
      this.Buttons(2).Top=15
      this.SetAll("Width",132)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_5.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_5.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_5.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oTIPALL_1_6 as StdField with uid="MAMBJXUMBN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPALL", cQueryName = "TIPALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 182678986,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=274, Top=287, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPALL"

  func oTIPALL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
      if .not. empty(.w_CLAALL)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTIPALL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPALL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oTIPALL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologia allegato",'',this.parent.oContained
  endproc

  add object oDESCTIPAL_1_7 as StdField with uid="KFXRRPRHPA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCTIPAL", cQueryName = "DESCTIPAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 224478665,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=340, Top=287, InputMask=replicate('X',40)

  add object oCLAALL_1_8 as StdField with uid="EHGPZLKUHK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CLAALL", cQueryName = "CLAALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 182739930,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=274, Top=318, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_CLAALL"

  func oCLAALL_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_TIPALL))
    endwith
   endif
  endfunc

  func oCLAALL_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAALL_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAALL_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_TIPALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_TIPALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCLAALL_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Classe allegato",'',this.parent.oContained
  endproc
  proc oCLAALL_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_TIPALL
     i_obj.w_TACODCLA=this.parent.oContained.w_CLAALL
     i_obj.ecpSave()
  endproc

  add object oDESCLAAL_1_9 as StdField with uid="OQOQZSICJH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCLAAL", cQueryName = "DESCLAAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 98650750,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=340, Top=318, InputMask=replicate('X',40)


  add object oBtn_1_11 as StdButton with uid="XQKGCSTQHP",left=640, top=287, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'aggiornamento sulle classi selezionate";
    , HelpContextID = 199840794;
    , caption='E\<segui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSDM_BMA(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! empty(nvl(.w_CODCLAS,' ')) )
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="YPTRHGBNFI",left=691, top=287, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192552122;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_13 as cp_runprogram with uid="PWWZFJEMIH",left=2, top=347, width=199,height=23,;
    caption='Gestione selezione',;
   bGlobalFont=.t.,;
    prg="GSDM_BMA('S')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 85653734

  add object oStr_1_3 as StdString with uid="KERGRFYCXE",Visible=.t., Left=147, Top=285,;
    Alignment=1, Width=121, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="EBQCSLVJCO",Visible=.t., Left=168, Top=319,;
    Alignment=1, Width=100, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="HBSTFBENWX",left=3, top=279, width=737,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kma','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
