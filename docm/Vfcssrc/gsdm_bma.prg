* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bma                                                        *
*              Assegnamento massivo tipologia e classe allegato                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-04                                                      *
* Last revis.: 2009-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bma",oParentObject,m.pTIPOP)
return(i_retval)

define class tgsdm_bma as StdBatch
  * --- Local variables
  pTIPOP = space(1)
  w_CurName = space(10)
  w_CODCLA = space(15)
  w_CALLER = .NULL.
  w_CONTROL = .NULL.
  * --- WorkFile variables
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegno alle classi selezionate nella maschera GSDM_KMA, la tipologia e la classe richiesta
    this.w_CALLER = this.OparentObject
    this.w_CurName = this.w_CALLER.w_CLASSI.cCursor
    do case
      case this.pTIPOP="E"
        Select (this.w_CurName) 
 Go Top
        locate for XCHK = 1
        if ! found()
          Ah_ErrorMsg("Selezionare almeno una classe da aggiornare",48,"")
          i_retcode = 'stop'
          return
        endif
        * --- Verifico la presenza dei dati da inserire nelle classi documentali
        if empty(this.oParentObject.w_TIPALL) or empty(this.oParentObject.w_CLAALL)
          Ah_ErrorMsg("Mancano informazioni sulla tipologia e/o sulla classe allegati da associare alle classi selezionate",48,"")
          i_retcode = 'stop'
          return
        endif
        Select (this.w_CurName) 
 Go Top
        SCAN FOR XCHK = 1
        this.w_CODCLA = eval(this.w_Curname+".CDCODCLA")
        * --- Write into PROMCLAS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMCLAS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CDTIPALL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPALL),'PROMCLAS','CDTIPALL');
          +",CDCLAALL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLAALL),'PROMCLAS','CDCLAALL');
              +i_ccchkf ;
          +" where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.w_CODCLA);
                 )
        else
          update (i_cTable) set;
              CDTIPALL = this.oParentObject.w_TIPALL;
              ,CDCLAALL = this.oParentObject.w_CLAALL;
              &i_ccchkf. ;
           where;
              CDCODCLA = this.w_CODCLA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ENDSCAN
        * --- Eseguo refresh sullo zoom della  maschera
        this.w_CALLER.NotifyEvent("Calcola")     
        this.w_CONTROL = this.w_CALLER.GetCTRL("w_TIPALL")
        this.w_CONTROL.value = ""
        this.w_CONTROL.Valid()     
      case this.pTIPOP="S"
        if used(this.w_CurName)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.w_CurName) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.w_CurName) SET xChk=0
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPOP)
    this.pTIPOP=pTIPOP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOP"
endproc
