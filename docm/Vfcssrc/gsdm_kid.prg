* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kid                                                        *
*              Indici documenti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-24                                                      *
* Last revis.: 2006-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kid",oParentObject))

* --- Class definition
define class tgsdm_kid as StdForm
  Top    = 12
  Left   = 8

  * --- Standard Properties
  Width  = 739
  Height = 326
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2006-11-28"
  HelpContextID=266978409
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsdm_kid"
  cComment = "Indici documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SELCODCLA = space(15)
  w_DESCLA = space(50)
  w_OGGETTO = space(75)
  w_TESTO = space(0)
  w_ZOOMGF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kidPag1","gsdm_kid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELCODCLA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMGF = this.oPgFrm.Pages(1).oPag.ZOOMGF
    DoDefault()
    proc Destroy()
      this.w_ZOOMGF = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SELCODCLA=space(15)
      .w_DESCLA=space(50)
      .w_OGGETTO=space(75)
      .w_TESTO=space(0)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SELCODCLA))
          .link_1_1('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMGF.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
    this.DoRTCalc(2,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMGF.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMGF.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMGF.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SELCODCLA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_SELCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_SELCODCLA))
          select CDCODCLA,CDDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SELCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oSELCODCLA_1_1'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_SELCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_SELCODCLA)
            select CDCODCLA,CDDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SELCODCLA = space(15)
      endif
      this.w_DESCLA = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endif
        this.w_SELCODCLA = space(15)
        this.w_DESCLA = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELCODCLA_1_1.value==this.w_SELCODCLA)
      this.oPgFrm.Page1.oPag.oSELCODCLA_1_1.value=this.w_SELCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_2.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_2.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGETTO_1_8.value==this.w_OGGETTO)
      this.oPgFrm.Page1.oPag.oOGGETTO_1_8.value=this.w_OGGETTO
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTO_1_9.value==this.w_TESTO)
      this.oPgFrm.Page1.oPag.oTESTO_1_9.value=this.w_TESTO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' ))  and not(empty(.w_SELCODCLA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELCODCLA_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_kidPag1 as StdContainer
  Width  = 735
  height = 326
  stdWidth  = 735
  stdheight = 326
  resizeXpos=580
  resizeYpos=229
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSELCODCLA_1_1 as StdField with uid="PKOJJUZXPM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SELCODCLA", cQueryName = "SELCODCLA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata",;
    HelpContextID = 156125826,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=79, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", oKey_1_1="CDCODCLA", oKey_1_2="this.w_SELCODCLA"

  func oSELCODCLA_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELCODCLA_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELCODCLA_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oSELCODCLA_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESCLA_1_2 as StdField with uid="XELGXDNVTX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 165759690,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=249, Top=8, InputMask=replicate('X',50)


  add object ZOOMGF as cp_zoombox with uid="EBBFLHQPPE",left=4, top=101, width=726,height=185,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSDM0KID",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="PRODCLAS",;
    nPag=1;
    , HelpContextID = 179454694


  add object oObj_1_4 as cp_runprogram with uid="EPHPFPTGOL",left=6, top=333, width=303,height=19,;
    caption='GSDM_BID(LOAD)',;
   bGlobalFont=.t.,;
    prg="GSDM_BID('LOAD')",;
    cEvent = "Blank,w_SELCODCLA Changed",;
    nPag=1;
    , HelpContextID = 169995734

  add object oOGGETTO_1_8 as StdField with uid="DRNFSNYGSN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_OGGETTO", cQueryName = "OGGETTO",;
    bObbl = .f. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del documento da archiviare",;
    HelpContextID = 106956826,;
   bGlobalFont=.t.,;
    Height=21, Width=538, Left=79, Top=34, InputMask=replicate('X',75)

  add object oTESTO_1_9 as StdMemo with uid="BLDMUERTYA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TESTO", cQueryName = "TESTO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 178276810,;
   bGlobalFont=.t.,;
    Height=37, Width=538, Left=79, Top=58


  add object oBtn_1_10 as StdButton with uid="HKXVNURELH",left=217, top=10, width=22,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la classe alla quale si vuole accociare il documento";
    , HelpContextID = 266777386;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      vx_exec("QUERY\GSDM_KSW.VZM",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="CBTFVRQQDM",Visible=.t., Left=13, Top=8,;
    Alignment=1, Width=59, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="OHLNDYEJHH",Visible=.t., Left=38, Top=65,;
    Alignment=1, Width=34, Height=18,;
    Caption="Testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KGAQQVMUFQ",Visible=.t., Left=26, Top=34,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kid','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
