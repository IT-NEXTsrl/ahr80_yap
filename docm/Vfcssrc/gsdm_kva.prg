* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kva                                                        *
*              Abilitazione altri intestatari                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-15                                                      *
* Last revis.: 2016-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kva",oParentObject))

* --- Class definition
define class tgsdm_kva as StdForm
  Top    = -2
  Left   = 4

  * --- Standard Properties
  Width  = 801
  Height = 553
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-01"
  HelpContextID=48874601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  CATECOMM_IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  AGENTI_IDX = 0
  ZONE_IDX = 0
  NAZIONI_IDX = 0
  CACOCLFO_IDX = 0
  AZIENDA_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsdm_kva"
  cComment = "Abilitazione altri intestatari"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_TIPCON = space(1)
  w_PBDEBCODI = space(15)
  w_PEDEBCODI = space(15)
  w_PBAGCODI = space(15)
  w_PEAGCODI = space(15)
  w_PBNOCODI = space(15)
  w_PENOCODI = space(15)
  w_PANCONSU = space(15)
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_SELEZI = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_GRUPPO = space(35)
  w_DESCR3 = space(40)
  w_DESCR4 = space(40)
  w_STAMPA = space(1)
  w_EMAIL = space(1)
  w_EMPEC = space(1)
  w_FAX = space(1)
  w_CPZ = space(1)
  w_POSTEL = space(1)
  w_DESCR5 = space(40)
  w_DESCR6 = space(40)
  w_NOFLGBEN = space(1)
  w_NOFLGBEN1 = space(1)
  w_ZOOMANAG = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kvaPag1","gsdm_kva",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Aggiornamento")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCON_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsdm_kva
    WITH THIS.PARENT
     DO CASE
             CASE  Isahr()
                   .cComment = ah_msgformat("Agenti/nominativi")
             CASE  Isahe()
                   .cComment = ah_msgformat("Abilitazione altri intestatari")
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMANAG = this.oPgFrm.Pages(1).oPag.ZOOMANAG
    DoDefault()
    proc Destroy()
      this.w_ZOOMANAG = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CATECOMM'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='MASTRI'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='ZONE'
    this.cWorkTables[6]='NAZIONI'
    this.cWorkTables[7]='CACOCLFO'
    this.cWorkTables[8]='AZIENDA'
    this.cWorkTables[9]='OFF_NOMI'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSDM_BVA(this,"UPDATE")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_TIPCON=space(1)
      .w_PBDEBCODI=space(15)
      .w_PEDEBCODI=space(15)
      .w_PBAGCODI=space(15)
      .w_PEAGCODI=space(15)
      .w_PBNOCODI=space(15)
      .w_PENOCODI=space(15)
      .w_PANCONSU=space(15)
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_GRUPPO=space(35)
      .w_DESCR3=space(40)
      .w_DESCR4=space(40)
      .w_STAMPA=space(1)
      .w_EMAIL=space(1)
      .w_EMPEC=space(1)
      .w_FAX=space(1)
      .w_CPZ=space(1)
      .w_POSTEL=space(1)
      .w_DESCR5=space(40)
      .w_DESCR6=space(40)
      .w_NOFLGBEN=space(1)
      .w_NOFLGBEN1=space(1)
        .w_TIPCON = 'A'
        .w_TIPCON = 'A'
        .w_PBDEBCODI = Space(15)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PBDEBCODI))
          .link_1_3('Full')
        endif
        .w_PEDEBCODI = Space(15)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PEDEBCODI))
          .link_1_4('Full')
        endif
        .w_PBAGCODI = Space(15)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PBAGCODI))
          .link_1_5('Full')
        endif
        .w_PEAGCODI = Space(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PEAGCODI))
          .link_1_6('Full')
        endif
        .w_PBNOCODI = Space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PBNOCODI))
          .link_1_7('Full')
        endif
        .w_PENOCODI = Space(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PENOCODI))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_PANCONSU))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_OBTEST = i_DATSYS
        .w_SELEZI = "D"
          .DoRTCalc(14,17,.f.)
        .w_STAMPA = 'A'
        .w_EMAIL = 'A'
        .w_EMPEC = 'A'
        .w_FAX = 'A'
        .w_CPZ = 'A'
        .w_POSTEL = 'A'
      .oPgFrm.Page1.oPag.ZOOMANAG.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
    this.DoRTCalc(24,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_PBDEBCODI = Space(15)
          .link_1_3('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_PEDEBCODI = Space(15)
          .link_1_4('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_PBAGCODI = Space(15)
          .link_1_5('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_PEAGCODI = Space(15)
          .link_1_6('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_PBNOCODI = Space(15)
          .link_1_7('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_PENOCODI = Space(15)
          .link_1_8('Full')
        endif
          .link_1_9('Full')
        .DoRTCalc(10,11,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(13,17,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_STAMPA = 'A'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_EMAIL = 'A'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_EMPEC = 'A'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_FAX = 'A'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CPZ = 'A'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_POSTEL = 'A'
        endif
        .oPgFrm.Page1.oPag.ZOOMANAG.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMANAG.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCPZ_1_25.enabled = this.oPgFrm.Page1.oPag.oCPZ_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPCON_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPCON_1_1.mHide()
    this.oPgFrm.Page1.oPag.oTIPCON_1_2.visible=!this.oPgFrm.Page1.oPag.oTIPCON_1_2.mHide()
    this.oPgFrm.Page1.oPag.oPBDEBCODI_1_3.visible=!this.oPgFrm.Page1.oPag.oPBDEBCODI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oPEDEBCODI_1_4.visible=!this.oPgFrm.Page1.oPag.oPEDEBCODI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oPBAGCODI_1_5.visible=!this.oPgFrm.Page1.oPag.oPBAGCODI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPEAGCODI_1_6.visible=!this.oPgFrm.Page1.oPag.oPEAGCODI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPBNOCODI_1_7.visible=!this.oPgFrm.Page1.oPag.oPBNOCODI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oPENOCODI_1_8.visible=!this.oPgFrm.Page1.oPag.oPENOCODI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDESCR1_1_12.visible=!this.oPgFrm.Page1.oPag.oDESCR1_1_12.mHide()
    this.oPgFrm.Page1.oPag.oDESCR2_1_13.visible=!this.oPgFrm.Page1.oPag.oDESCR2_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDESCR3_1_19.visible=!this.oPgFrm.Page1.oPag.oDESCR3_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESCR4_1_20.visible=!this.oPgFrm.Page1.oPag.oDESCR4_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCPZ_1_25.visible=!this.oPgFrm.Page1.oPag.oCPZ_1_25.mHide()
    this.oPgFrm.Page1.oPag.oPOSTEL_1_26.visible=!this.oPgFrm.Page1.oPag.oPOSTEL_1_26.mHide()
    this.oPgFrm.Page1.oPag.oDESCR5_1_30.visible=!this.oPgFrm.Page1.oPag.oDESCR5_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDESCR6_1_31.visible=!this.oPgFrm.Page1.oPag.oDESCR6_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMANAG.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PBDEBCODI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBDEBCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsar_bdc',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PBDEBCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PBDEBCODI))
          select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBDEBCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PBDEBCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PBDEBCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBDEBCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPBDEBCODI_1_3'),i_cWhere,'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBDEBCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PBDEBCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PBDEBCODI)
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBDEBCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR5 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOFLGBEN = NVL(_Link_.NOFLGBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PBDEBCODI = space(15)
      endif
      this.w_DESCR5 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NOFLGBEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEDEBCODI)) OR  (.w_PBDEBCODI<=.w_PEDEBCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBDEBCODI = space(15)
        this.w_DESCR5 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NOFLGBEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBDEBCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEDEBCODI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEDEBCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsar_bdc',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PEDEBCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PEDEBCODI))
          select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEDEBCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PEDEBCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PEDEBCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEDEBCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPEDEBCODI_1_4'),i_cWhere,'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEDEBCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PEDEBCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PEDEBCODI)
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEDEBCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR6 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOFLGBEN1 = NVL(_Link_.NOFLGBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PEDEBCODI = space(15)
      endif
      this.w_DESCR6 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NOFLGBEN1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEDEBCODI>=.w_PBDEBCODI) or (empty(.w_PBDEBCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN1='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEDEBCODI = space(15)
        this.w_DESCR6 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NOFLGBEN1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEDEBCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBAGCODI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBAGCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PBAGCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PBAGCODI))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBAGCODI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_PBAGCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_PBAGCODI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBAGCODI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPBAGCODI_1_5'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBAGCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PBAGCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PBAGCODI)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBAGCODI = NVL(_Link_.AGCODAGE,space(15))
      this.w_DESCR3 = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBAGCODI = space(15)
      endif
      this.w_DESCR3 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEAGCODI)) OR  (.w_PBAGCODI<=.w_PEAGCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBAGCODI = space(15)
        this.w_DESCR3 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBAGCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEAGCODI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEAGCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PEAGCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PEAGCODI))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEAGCODI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_PEAGCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_PEAGCODI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEAGCODI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPEAGCODI_1_6'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEAGCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PEAGCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PEAGCODI)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEAGCODI = NVL(_Link_.AGCODAGE,space(15))
      this.w_DESCR4 = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PEAGCODI = space(15)
      endif
      this.w_DESCR4 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEAGCODI>=.w_PBAGCODI) or (empty(.w_PBAGCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEAGCODI = space(15)
        this.w_DESCR4 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEAGCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBNOCODI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBNOCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PBNOCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PBNOCODI))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBNOCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PBNOCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PBNOCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBNOCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPBNOCODI_1_7'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBNOCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PBNOCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PBNOCODI)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBNOCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR5 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBNOCODI = space(15)
      endif
      this.w_DESCR5 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PENOCODI)) OR  (.w_PBNOCODI<=.w_PENOCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBNOCODI = space(15)
        this.w_DESCR5 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBNOCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PENOCODI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PENOCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PENOCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PENOCODI))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PENOCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PENOCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PENOCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PENOCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPENOCODI_1_8'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PENOCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PENOCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PENOCODI)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PENOCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR6 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PENOCODI = space(15)
      endif
      this.w_DESCR6 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PENOCODI>=.w_PBNOCODI) or (empty(.w_PBNOCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PENOCODI = space(15)
        this.w_DESCR6 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PENOCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_1.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_2.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPBDEBCODI_1_3.value==this.w_PBDEBCODI)
      this.oPgFrm.Page1.oPag.oPBDEBCODI_1_3.value=this.w_PBDEBCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDEBCODI_1_4.value==this.w_PEDEBCODI)
      this.oPgFrm.Page1.oPag.oPEDEBCODI_1_4.value=this.w_PEDEBCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPBAGCODI_1_5.value==this.w_PBAGCODI)
      this.oPgFrm.Page1.oPag.oPBAGCODI_1_5.value=this.w_PBAGCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEAGCODI_1_6.value==this.w_PEAGCODI)
      this.oPgFrm.Page1.oPag.oPEAGCODI_1_6.value=this.w_PEAGCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPBNOCODI_1_7.value==this.w_PBNOCODI)
      this.oPgFrm.Page1.oPag.oPBNOCODI_1_7.value=this.w_PBNOCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPENOCODI_1_8.value==this.w_PENOCODI)
      this.oPgFrm.Page1.oPag.oPENOCODI_1_8.value=this.w_PENOCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_12.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_12.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_13.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_13.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_16.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR3_1_19.value==this.w_DESCR3)
      this.oPgFrm.Page1.oPag.oDESCR3_1_19.value=this.w_DESCR3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR4_1_20.value==this.w_DESCR4)
      this.oPgFrm.Page1.oPag.oDESCR4_1_20.value=this.w_DESCR4
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPA_1_21.RadioValue()==this.w_STAMPA)
      this.oPgFrm.Page1.oPag.oSTAMPA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEMAIL_1_22.RadioValue()==this.w_EMAIL)
      this.oPgFrm.Page1.oPag.oEMAIL_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEMPEC_1_23.RadioValue()==this.w_EMPEC)
      this.oPgFrm.Page1.oPag.oEMPEC_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAX_1_24.RadioValue()==this.w_FAX)
      this.oPgFrm.Page1.oPag.oFAX_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCPZ_1_25.RadioValue()==this.w_CPZ)
      this.oPgFrm.Page1.oPag.oCPZ_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSTEL_1_26.RadioValue()==this.w_POSTEL)
      this.oPgFrm.Page1.oPag.oPOSTEL_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR5_1_30.value==this.w_DESCR5)
      this.oPgFrm.Page1.oPag.oDESCR5_1_30.value=this.w_DESCR5
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR6_1_31.value==this.w_DESCR6)
      this.oPgFrm.Page1.oPag.oDESCR6_1_31.value=this.w_DESCR6
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PEDEBCODI)) OR  (.w_PBDEBCODI<=.w_PEDEBCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B')  and not(.w_TIPCON<>'B')  and not(empty(.w_PBDEBCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBDEBCODI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PEDEBCODI>=.w_PBDEBCODI) or (empty(.w_PBDEBCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN1='B')  and not(.w_TIPCON<>'B')  and not(empty(.w_PEDEBCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEDEBCODI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_PEAGCODI)) OR  (.w_PBAGCODI<=.w_PEAGCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPCON<>'A')  and not(empty(.w_PBAGCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBAGCODI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PEAGCODI>=.w_PBAGCODI) or (empty(.w_PBAGCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPCON<>'A')  and not(empty(.w_PEAGCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEAGCODI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_PENOCODI)) OR  (.w_PBNOCODI<=.w_PENOCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPCON<>'N')  and not(empty(.w_PBNOCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBNOCODI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PENOCODI>=.w_PBNOCODI) or (empty(.w_PBNOCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPCON<>'N')  and not(empty(.w_PENOCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPENOCODI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    return

enddefine

* --- Define pages as container
define class tgsdm_kvaPag1 as StdContainer
  Width  = 797
  height = 553
  stdWidth  = 797
  stdheight = 553
  resizeXpos=683
  resizeYpos=244
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_1 as StdCombo with uid="UUDBDOJQXS",rtseq=1,rtrep=.f.,left=319,top=9,width=181,height=21;
    , ToolTipText = "Tipo: agente/nominativo/debitore creditore";
    , HelpContextID = 263288266;
    , cFormVar="w_TIPCON",RowSource=""+"Agente,"+"Nominativo,"+"Debitori creditori diversi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_1.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oTIPCON_1_1.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_1.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='A',1,;
      iif(this.Parent.oContained.w_TIPCON=='N',2,;
      iif(this.Parent.oContained.w_TIPCON=='B',3,;
      0)))
  endfunc

  func oTIPCON_1_1.mHide()
    with this.Parent.oContained
      return (Isahr())
    endwith
  endfunc


  add object oTIPCON_1_2 as StdCombo with uid="MRPYWSCVOY",rtseq=2,rtrep=.f.,left=319,top=9,width=126,height=21;
    , ToolTipText = "Tipo: agente/nominativo";
    , HelpContextID = 263288266;
    , cFormVar="w_TIPCON",RowSource=""+"Agente,"+"Nominativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTIPCON_1_2.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_2.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='A',1,;
      iif(this.Parent.oContained.w_TIPCON=='N',2,;
      0))
  endfunc

  func oTIPCON_1_2.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oPBDEBCODI_1_3 as StdField with uid="BFVZXIODCX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PBDEBCODI", cQueryName = "PBDEBCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 192952374,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=169, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="Gsar_bdc", oKey_1_1="NOCODICE", oKey_1_2="this.w_PBDEBCODI"

  func oPBDEBCODI_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'B')
    endwith
  endfunc

  func oPBDEBCODI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBDEBCODI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBDEBCODI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPBDEBCODI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oPBDEBCODI_1_3.mZoomOnZoom
    local i_obj
    i_obj=Gsar_bdc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PBDEBCODI
     i_obj.ecpSave()
  endproc

  add object oPEDEBCODI_1_4 as StdField with uid="QKJXCKWZSB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PEDEBCODI", cQueryName = "PEDEBCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 192951606,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=169, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="Gsar_bdc", oKey_1_1="NOCODICE", oKey_1_2="this.w_PEDEBCODI"

  func oPEDEBCODI_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'B')
    endwith
  endfunc

  func oPEDEBCODI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEDEBCODI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEDEBCODI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPEDEBCODI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oPEDEBCODI_1_4.mZoomOnZoom
    local i_obj
    i_obj=Gsar_bdc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PEDEBCODI
     i_obj.ecpSave()
  endproc

  add object oPBAGCODI_1_5 as StdField with uid="OVUQSKYCXY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PBAGCODI", cQueryName = "PBAGCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 9540415,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PBAGCODI"

  func oPBAGCODI_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'A')
    endwith
  endfunc

  func oPBAGCODI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBAGCODI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBAGCODI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPBAGCODI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oPEAGCODI_1_6 as StdField with uid="JXMAYGDIKD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PEAGCODI", cQueryName = "PEAGCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 9541183,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PEAGCODI"

  func oPEAGCODI_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'A')
    endwith
  endfunc

  func oPEAGCODI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEAGCODI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEAGCODI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPEAGCODI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oPBNOCODI_1_7 as StdField with uid="KWQUNRPVQU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PBNOCODI", cQueryName = "PBNOCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 10117951,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=169, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_PBNOCODI"

  func oPBNOCODI_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'N')
    endwith
  endfunc

  func oPBNOCODI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBNOCODI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBNOCODI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPBNOCODI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oPBNOCODI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PBNOCODI
     i_obj.ecpSave()
  endproc

  add object oPENOCODI_1_8 as StdField with uid="SKJCIEGOVJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PENOCODI", cQueryName = "PENOCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 10118719,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=169, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_PENOCODI"

  func oPENOCODI_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'N')
    endwith
  endfunc

  func oPENOCODI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPENOCODI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPENOCODI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPENOCODI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oPENOCODI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PENOCODI
     i_obj.ecpSave()
  endproc

  add object oDESCR1_1_12 as StdField with uid="CHLOPGXLBM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209799882,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=319, Top=39, InputMask=replicate('X',40)

  func oDESCR1_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESCR2_1_13 as StdField with uid="MINMVUHPXL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193022666,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=319, Top=66, InputMask=replicate('X',40)

  func oDESCR2_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc


  add object oBtn_1_15 as StdButton with uid="WKKNMMITWL",left=743, top=43, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la abilitazione/disabilitazione richiesta";
    , HelpContextID = 128047638;
    , caption='R\<icerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      this.parent.oContained.NotifyEvent("Interroga")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_16 as StdRadio with uid="PVBVSGJXHP",rtseq=13,rtrep=.f.,left=7, top=437, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 67090906
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 67090906
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_16.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_16.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_16.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oDESCR3_1_19 as StdField with uid="SYAVFVIJQN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCR3", cQueryName = "DESCR3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 176245450,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=319, Top=39, InputMask=replicate('X',40)

  func oDESCR3_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'A')
    endwith
  endfunc

  add object oDESCR4_1_20 as StdField with uid="YCNZVHFXMB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCR4", cQueryName = "DESCR4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 159468234,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=319, Top=66, InputMask=replicate('X',40)

  func oDESCR4_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'A')
    endwith
  endfunc


  add object oSTAMPA_1_21 as StdCombo with uid="HLKFLFIYUS",rtseq=18,rtrep=.f.,left=63,top=499,width=144,height=22;
    , ToolTipText = "Attiva opzione di stampa da processo documentale per intestatari selezionati";
    , HelpContextID = 211311322;
    , cFormVar="w_STAMPA",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAMPA_1_21.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oSTAMPA_1_21.GetRadio()
    this.Parent.oContained.w_STAMPA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPA_1_21.SetRadio()
    this.Parent.oContained.w_STAMPA=trim(this.Parent.oContained.w_STAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPA=='A',1,;
      iif(this.Parent.oContained.w_STAMPA=='S',2,;
      iif(this.Parent.oContained.w_STAMPA=='N',3,;
      0)))
  endfunc


  add object oEMAIL_1_22 as StdCombo with uid="SDWFEXJQPP",rtseq=19,rtrep=.f.,left=309,top=499,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio email da processo documentale per intestatari selezionati";
    , HelpContextID = 232547002;
    , cFormVar="w_EMAIL",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEMAIL_1_22.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oEMAIL_1_22.GetRadio()
    this.Parent.oContained.w_EMAIL = this.RadioValue()
    return .t.
  endfunc

  func oEMAIL_1_22.SetRadio()
    this.Parent.oContained.w_EMAIL=trim(this.Parent.oContained.w_EMAIL)
    this.value = ;
      iif(this.Parent.oContained.w_EMAIL=='A',1,;
      iif(this.Parent.oContained.w_EMAIL=='S',2,;
      iif(this.Parent.oContained.w_EMAIL=='N',3,;
      0)))
  endfunc


  add object oEMPEC_1_23 as StdCombo with uid="BDOEUSPJAL",rtseq=20,rtrep=.f.,left=519,top=499,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio PEC da processo documentale per intestatari selezionati";
    , HelpContextID = 242184890;
    , cFormVar="w_EMPEC",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEMPEC_1_23.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oEMPEC_1_23.GetRadio()
    this.Parent.oContained.w_EMPEC = this.RadioValue()
    return .t.
  endfunc

  func oEMPEC_1_23.SetRadio()
    this.Parent.oContained.w_EMPEC=trim(this.Parent.oContained.w_EMPEC)
    this.value = ;
      iif(this.Parent.oContained.w_EMPEC=='A',1,;
      iif(this.Parent.oContained.w_EMPEC=='S',2,;
      iif(this.Parent.oContained.w_EMPEC=='N',3,;
      0)))
  endfunc


  add object oFAX_1_24 as StdCombo with uid="TFIZBXCYTF",rtseq=21,rtrep=.f.,left=63,top=527,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio FAX da processo documentale per intestatari selezionati";
    , HelpContextID = 48496298;
    , cFormVar="w_FAX",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFAX_1_24.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFAX_1_24.GetRadio()
    this.Parent.oContained.w_FAX = this.RadioValue()
    return .t.
  endfunc

  func oFAX_1_24.SetRadio()
    this.Parent.oContained.w_FAX=trim(this.Parent.oContained.w_FAX)
    this.value = ;
      iif(this.Parent.oContained.w_FAX=='A',1,;
      iif(this.Parent.oContained.w_FAX=='S',2,;
      iif(this.Parent.oContained.w_FAX=='N',3,;
      0)))
  endfunc


  add object oCPZ_1_25 as StdCombo with uid="VBBMVDPHZB",rtseq=22,rtrep=.f.,left=309,top=527,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio a Web Application da processo documentale per intestatari selezionati";
    , HelpContextID = 48484314;
    , cFormVar="w_CPZ",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCPZ_1_25.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCPZ_1_25.GetRadio()
    this.Parent.oContained.w_CPZ = this.RadioValue()
    return .t.
  endfunc

  func oCPZ_1_25.SetRadio()
    this.Parent.oContained.w_CPZ=trim(this.Parent.oContained.w_CPZ)
    this.value = ;
      iif(this.Parent.oContained.w_CPZ=='A',1,;
      iif(this.Parent.oContained.w_CPZ=='S',2,;
      iif(this.Parent.oContained.w_CPZ=='N',3,;
      0)))
  endfunc

  func oCPZ_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DMIP<>'S' And (g_IZCP$'SA' or g_CPIN='S'))
    endwith
   endif
  endfunc

  func oCPZ_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPCON $ 'NB')
    endwith
  endfunc


  add object oPOSTEL_1_26 as StdCombo with uid="SBZFCROJGU",rtseq=23,rtrep=.f.,left=519,top=527,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio tramite PostaLite da processo documentale per intestatari selezionati";
    , HelpContextID = 37765130;
    , cFormVar="w_POSTEL",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOSTEL_1_26.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPOSTEL_1_26.GetRadio()
    this.Parent.oContained.w_POSTEL = this.RadioValue()
    return .t.
  endfunc

  func oPOSTEL_1_26.SetRadio()
    this.Parent.oContained.w_POSTEL=trim(this.Parent.oContained.w_POSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_POSTEL=='A',1,;
      iif(this.Parent.oContained.w_POSTEL=='S',2,;
      iif(this.Parent.oContained.w_POSTEL=='N',3,;
      0)))
  endfunc

  func oPOSTEL_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIPCON $ 'NB')
    endwith
  endfunc


  add object ZOOMANAG as cp_szoombox with uid="SFIVKVVAUJ",left=2, top=92, width=792,height=341,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSDM_KVA",bOptions=.f.,cTable="CONTI",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga,w_TIPCON Changed,w_PBAGCODI Changed,w_PBNOCODI Changed,w_PBDEBCODI Changed,w_PEAGCODI Changed,w_PENOCODI Changed,w_PEDEBCODI Changed",;
    nPag=1;
    , HelpContextID = 129123046


  add object oObj_1_28 as cp_runprogram with uid="QHWAXBZBKC",left=7, top=563, width=191,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSDM_BVA('SELDESEL')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 129123046

  add object oDESCR5_1_30 as StdField with uid="SMPNBMLESO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCR5", cQueryName = "DESCR5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142691018,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=319, Top=39, InputMask=replicate('X',40)

  func oDESCR5_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESCR6_1_31 as StdField with uid="HABNTYWKVU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCR6", cQueryName = "DESCR6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 125913802,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=319, Top=66, InputMask=replicate('X',40)

  func oDESCR6_1_31.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc


  add object oBtn_1_43 as StdButton with uid="XQKGCSTQHP",left=692, top=504, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la abilitazione/disabilitazione richiesta";
    , HelpContextID = 48845850;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSDM_BVA(this.Parent.oContained,"UPDATE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_44 as StdButton with uid="YPTRHGBNFI",left=743, top=504, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per sospendere operazione";
    , HelpContextID = 41557178;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="MAJRIEMKEU",Visible=.t., Left=99, Top=40,;
    Alignment=1, Width=65, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="XJEFRRYNMW",Visible=.t., Left=106, Top=66,;
    Alignment=1, Width=58, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="OOFMLKMVFR",Visible=.t., Left=21, Top=473,;
    Alignment=0, Width=295, Height=18,;
    Caption="Abilitazione processi documentali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="OLKLIXAJDO",Visible=.t., Left=184, Top=9,;
    Alignment=1, Width=131, Height=18,;
    Caption="Tipo agente/nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="KPXFTHGTEA",Visible=.t., Left=15, Top=9,;
    Alignment=1, Width=300, Height=18,;
    Caption="Tipo agente/nominativo/debitore creditore diverso:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (Isahr())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="TUTBAUJPCL",Visible=.t., Left=8, Top=503,;
    Alignment=1, Width=52, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="IMDKJHVVHW",Visible=.t., Left=210, Top=503,;
    Alignment=1, Width=96, Height=18,;
    Caption="Email:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="XHLJPMORTK",Visible=.t., Left=456, Top=503,;
    Alignment=1, Width=60, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="JNGAMUWXUF",Visible=.t., Left=8, Top=531,;
    Alignment=1, Width=52, Height=18,;
    Caption="FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="QVFNTIBPZV",Visible=.t., Left=210, Top=531,;
    Alignment=1, Width=96, Height=18,;
    Caption="Web Application:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_TIPCON $ 'NB')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="OCDZMMMXFG",Visible=.t., Left=456, Top=531,;
    Alignment=1, Width=60, Height=18,;
    Caption="PostaLite:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPCON $ 'NB')
    endwith
  endfunc

  add object oBox_1_36 as StdBox with uid="NJKHSREEVT",left=10, top=492, width=659,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kva','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
