* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_aid                                                        *
*              Parametri Infinity D.M.S.                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-10                                                      *
* Last revis.: 2010-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsdm_aid")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsdm_aid")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsdm_aid")
  return

* --- Class definition
define class tgsdm_aid as StdPCForm
  Width  = 577
  Height = 156
  Top    = 10
  Left   = 10
  cComment = "Parametri Infinity D.M.S."
  cPrg = "gsdm_aid"
  HelpContextID=9028713
  add object cnt as tcgsdm_aid
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsdm_aid as PCContext
  w_CDINFURL = space(254)
  w_CDCODAZI = space(5)
  w_CDAPPCOD = space(5)
  w_CDIDUSER = space(128)
  w_XXPASSWD = space(128)
  w_CDIDPSSW = space(128)
  proc Save(oFrom)
    this.w_CDINFURL = oFrom.w_CDINFURL
    this.w_CDCODAZI = oFrom.w_CDCODAZI
    this.w_CDAPPCOD = oFrom.w_CDAPPCOD
    this.w_CDIDUSER = oFrom.w_CDIDUSER
    this.w_XXPASSWD = oFrom.w_XXPASSWD
    this.w_CDIDPSSW = oFrom.w_CDIDPSSW
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CDINFURL = this.w_CDINFURL
    oTo.w_CDCODAZI = this.w_CDCODAZI
    oTo.w_CDAPPCOD = this.w_CDAPPCOD
    oTo.w_CDIDUSER = this.w_CDIDUSER
    oTo.w_XXPASSWD = this.w_XXPASSWD
    oTo.w_CDIDPSSW = this.w_CDIDPSSW
    PCContext::Load(oTo)
enddefine

define class tcgsdm_aid as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 577
  Height = 156
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-09-17"
  HelpContextID=9028713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  PARA_EDS_IDX = 0
  cFile = "PARA_EDS"
  cKeySelect = "CDCODAZI"
  cKeyWhere  = "CDCODAZI=this.w_CDCODAZI"
  cKeyWhereODBC = '"CDCODAZI="+cp_ToStrODBC(this.w_CDCODAZI)';

  cKeyWhereODBCqualified = '"PARA_EDS.CDCODAZI="+cp_ToStrODBC(this.w_CDCODAZI)';

  cPrg = "gsdm_aid"
  cComment = "Parametri Infinity D.M.S."
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CDINFURL = space(254)
  w_CDCODAZI = space(5)
  w_CDAPPCOD = space(5)
  w_CDIDUSER = space(128)
  w_XXPASSWD = space(128)
  o_XXPASSWD = space(128)
  w_CDIDPSSW = space(128)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_aidPag1","gsdm_aid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 219016714
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDINFURL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PARA_EDS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PARA_EDS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PARA_EDS_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsdm_aid'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PARA_EDS where CDCODAZI=KeySet.CDCODAZI
    *
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PARA_EDS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PARA_EDS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PARA_EDS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CDCODAZI',this.w_CDCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_XXPASSWD = space(128)
        .w_CDINFURL = NVL(CDINFURL,space(254))
        .w_CDCODAZI = NVL(CDCODAZI,space(5))
        .w_CDAPPCOD = NVL(CDAPPCOD,space(5))
        .w_CDIDUSER = NVL(CDIDUSER,space(128))
        .w_CDIDPSSW = NVL(CDIDPSSW,space(128))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate('*')
        cp_LoadRecExtFlds(this,'PARA_EDS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CDINFURL = space(254)
      .w_CDCODAZI = space(5)
      .w_CDAPPCOD = space(5)
      .w_CDIDUSER = space(128)
      .w_XXPASSWD = space(128)
      .w_CDIDPSSW = space(128)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate('*')
      endif
    endwith
    cp_BlankRecExtFlds(this,'PARA_EDS')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCDINFURL_1_1.enabled = i_bVal
      .Page1.oPag.oCDAPPCOD_1_3.enabled = i_bVal
      .Page1.oPag.oCDIDUSER_1_5.enabled = i_bVal
      .Page1.oPag.oXXPASSWD_1_6.enabled = i_bVal
      .Page1.oPag.oBtn_1_14.enabled = .Page1.oPag.oBtn_1_14.mCond()
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
      .Page1.oPag.oObj_1_10.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PARA_EDS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDINFURL,"CDINFURL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCODAZI,"CDCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDAPPCOD,"CDAPPCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDIDUSER,"CDIDUSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDIDPSSW,"CDIDPSSW",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PARA_EDS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PARA_EDS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PARA_EDS')
        i_extval=cp_InsertValODBCExtFlds(this,'PARA_EDS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CDINFURL,CDCODAZI,CDAPPCOD,CDIDUSER,CDIDPSSW "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CDINFURL)+;
                  ","+cp_ToStrODBC(this.w_CDCODAZI)+;
                  ","+cp_ToStrODBC(this.w_CDAPPCOD)+;
                  ","+cp_ToStrODBC(this.w_CDIDUSER)+;
                  ","+cp_ToStrODBC(this.w_CDIDPSSW)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PARA_EDS')
        i_extval=cp_InsertValVFPExtFlds(this,'PARA_EDS')
        cp_CheckDeletedKey(i_cTable,0,'CDCODAZI',this.w_CDCODAZI)
        INSERT INTO (i_cTable);
              (CDINFURL,CDCODAZI,CDAPPCOD,CDIDUSER,CDIDPSSW  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CDINFURL;
                  ,this.w_CDCODAZI;
                  ,this.w_CDAPPCOD;
                  ,this.w_CDIDUSER;
                  ,this.w_CDIDPSSW;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PARA_EDS_IDX,i_nConn)
      *
      * update PARA_EDS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PARA_EDS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CDINFURL="+cp_ToStrODBC(this.w_CDINFURL)+;
             ",CDAPPCOD="+cp_ToStrODBC(this.w_CDAPPCOD)+;
             ",CDIDUSER="+cp_ToStrODBC(this.w_CDIDUSER)+;
             ",CDIDPSSW="+cp_ToStrODBC(this.w_CDIDPSSW)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PARA_EDS')
        i_cWhere = cp_PKFox(i_cTable  ,'CDCODAZI',this.w_CDCODAZI  )
        UPDATE (i_cTable) SET;
              CDINFURL=this.w_CDINFURL;
             ,CDAPPCOD=this.w_CDAPPCOD;
             ,CDIDUSER=this.w_CDIDUSER;
             ,CDIDPSSW=this.w_CDIDPSSW;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PARA_EDS_IDX,i_nConn)
      *
      * delete PARA_EDS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CDCODAZI',this.w_CDCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate('*')
        if .o_XXPASSWD<>.w_XXPASSWD
          .Calculate_YSIDYAOZHI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate('*')
    endwith
  return

  proc Calculate_YSIDYAOZHI()
    with this
          * --- Cifra password
          .w_CDIDPSSW = CifraCnf( ALLTRIM(.w_XXPASSWD) , 'C' )
    endwith
  endproc
  proc Calculate_EYFXYUGQKX()
    with this
          * --- Decifra password
          .w_XXPASSWD = CifraCnf( ALLTRIM(.w_CDIDPSSW) , 'D' )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YSIDYAOZHI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_EYFXYUGQKX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCDINFURL_1_1.value==this.w_CDINFURL)
      this.oPgFrm.Page1.oPag.oCDINFURL_1_1.value=this.w_CDINFURL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDAPPCOD_1_3.value==this.w_CDAPPCOD)
      this.oPgFrm.Page1.oPag.oCDAPPCOD_1_3.value=this.w_CDAPPCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCDIDUSER_1_5.value==this.w_CDIDUSER)
      this.oPgFrm.Page1.oPag.oCDIDUSER_1_5.value=this.w_CDIDUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oXXPASSWD_1_6.value==this.w_XXPASSWD)
      this.oPgFrm.Page1.oPag.oXXPASSWD_1_6.value=this.w_XXPASSWD
    endif
    cp_SetControlsValueExtFlds(this,'PARA_EDS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CDINFURL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDINFURL_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CDINFURL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_XXPASSWD = this.w_XXPASSWD
    return

enddefine

* --- Define pages as container
define class tgsdm_aidPag1 as StdContainer
  Width  = 573
  height = 156
  stdWidth  = 573
  stdheight = 156
  resizeXpos=447
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCDINFURL_1_1 as StdField with uid="BMSXGUBRTZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CDINFURL", cQueryName = "CDINFURL",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo internet del server di Infinity",;
    HelpContextID = 153687154,;
   bGlobalFont=.t.,;
    Height=21, Width=428, Left=139, Top=4, InputMask=replicate('X',254)

  add object oCDAPPCOD_1_3 as StdField with uid="PMFPBZPKOA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CDAPPCOD", cQueryName = "CDAPPCOD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice applicazione registrata su Infinity",;
    HelpContextID = 137718678,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=30, InputMask=replicate('X',5)

  add object oCDIDUSER_1_5 as StdField with uid="GPKLNZGWVZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CDIDUSER", cQueryName = "CDIDUSER",;
    bObbl = .f. , nPag = 1, value=space(128), bMultilanguage =  .f.,;
    ToolTipText = "Utente infinity",;
    HelpContextID = 135206008,;
   bGlobalFont=.t.,;
    Height=21, Width=428, Left=139, Top=56, InputMask=replicate('X',128)

  add object oXXPASSWD_1_6 as StdField with uid="CWSADSZUKK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_XXPASSWD", cQueryName = "XXPASSWD",;
    bObbl = .f. , nPag = 1, value=space(128), bMultilanguage =  .f.,;
    ToolTipText = "Password utente E.D.S.",;
    HelpContextID = 132946362,;
   bGlobalFont=.t.,;
    Height=21, Width=428, Left=139, Top=82, InputMask=replicate('X',128)


  add object oObj_1_10 as cp_setobjprop with uid="AUWPJKIPCT",left=224, top=216, width=204,height=19,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj="w_XXPASSWD",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 8587274


  add object oBtn_1_14 as StdButton with uid="BSWARSVNYD",left=410, top=105, width=48,height=45,;
    CpPicture="BMP\PARAMETRI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per verficare la connessione ...";
    , HelpContextID = 9028618;
    , Caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        do GSDM_BIV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CDINFURL))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="PWCUCFWQQP",left=464, top=105, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte sulla macchina client";
    , HelpContextID = 9008154;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="LNMWHHBLEZ",left=518, top=105, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 45256198;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="WWGTAFYPGB",Visible=.t., Left=2, Top=32,;
    Alignment=1, Width=133, Height=18,;
    Caption="Applicazione esterna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="POMDVLONBR",Visible=.t., Left=59, Top=58,;
    Alignment=1, Width=76, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NNFWENYSKS",Visible=.t., Left=2, Top=84,;
    Alignment=1, Width=133, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OSHEIFVCQE",Visible=.t., Left=2, Top=6,;
    Alignment=1, Width=133, Height=18,;
    Caption="Server Infinity URL:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_aid','PARA_EDS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CDCODAZI=PARA_EDS.CDCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
