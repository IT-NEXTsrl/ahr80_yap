* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bve                                                        *
*              Verifica esito spedizione                                       *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-12                                                      *
* Last revis.: 2018-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipoOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bve",oParentObject,m.pTipoOp)
return(i_retval)

define class tgsdm_bve as StdBatch
  * --- Local variables
  pTipoOp = space(1)
  w_URL = space(254)
  w_WORKSTAT = space(15)
  w_CODCONS = space(10)
  w_CODCLI = space(10)
  w_GUID = space(36)
  w_RISPGUID = space(36)
  w_WS = .NULL.
  w_ESITOSPED = space(0)
  w_ESITOCONF = space(0)
  oXML = .NULL.
  w_RootNode = .NULL.
  w_CHILDREN = 0
  w_CODSOGG = space(5)
  w_NUMRIGA = 0
  w_ORDRIGA = 0
  w_IDSERIALE = space(20)
  w_OLDSERIALE = space(20)
  w_NOMZIP = space(200)
  w_CLASSE = space(10)
  w_STATO = space(10)
  w_LOOP = 0
  w_NUMDOC = 0
  w_OBJDOC = .NULL.
  w_NOMEFILEORIG = space(10)
  w_NUMCICLI = 0
  * --- WorkFile variables
  PAR_SOST_idx=0
  TMPINTSOS_idx=0
  PROMINDI_idx=0
  PRODINDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per la gestione delle operazioni inerenti il controllo di una spedizione specifica a SOS
    this.w_CHILDREN = 0
    * --- Priorit� di ricerca:
    *      1. Per codice azienda e codice utente
    *      2. Per codice azienda e codice workstation
    *      3. Per codice azienda
    *     Ovviamente appena � trovato un Web services le combinazioni rimanenti vengono scartate.
    this.w_WORKSTAT = substr(sys(0),1,at("#",sys(0))-1)
    * --- Read from PAR_SOST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_SOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG"+;
        " from "+i_cTable+" PAR_SOST where ";
            +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and PSCODUTE = "+cp_ToStrODBC(i_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG;
        from (i_cTable) where;
            PSCODAZI = i_CODAZI;
            and PSCODUTE = i_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
      this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
      this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
      this.w_CODSOGG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_URL) or (empty(this.w_CODCONS) and vartype(this.pTipoOp)="C")
      * --- Read from PAR_SOST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_SOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG"+;
          " from "+i_cTable+" PAR_SOST where ";
              +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and PSWORKST = "+cp_ToStrODBC(this.w_WORKSTAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG;
          from (i_cTable) where;
              PSCODAZI = i_CODAZI;
              and PSWORKST = this.w_WORKSTAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
        this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
        this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
        this.w_CODSOGG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(this.w_URL) or (empty(this.w_CODCONS) and vartype(this.pTipoOp)="C")
        * --- Read from PAR_SOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_SOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG"+;
            " from "+i_cTable+" PAR_SOST where ";
                +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and PSCODUTE = "+cp_ToStrODBC(0);
                +" and PSWORKST = "+cp_ToStrODBC(space(15));
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG;
            from (i_cTable) where;
                PSCODAZI = i_CODAZI;
                and PSCODUTE = 0;
                and PSWORKST = space(15);
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
          this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
          this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
          this.w_CODSOGG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0
          AH_ERRORMSG("Non sono stati definiti i parametri SOStitutiva, elaborazione interrotta.",48)
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    this.w_WS = Conwebser(this.w_URL)
    * --- Recupero l'identificativo GUID
    if isnull(this.w_WS)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_FLGSELE="N"
      this.w_GUID = RICGUID(this.w_WS)
      * --- Try
      local bErr_03565758
      bErr_03565758=bTrsErr
      this.Try_03565758()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03565758
      * --- End
    else
      * --- ws_as_servizio.esitospedizione
      *     (String  conservatore, String  soggetto, String TicketGUID,  String NomeFileZip,  String 
      *     CodiceClasseDocumentale)
      * --- Create temporary table TMPINTSOS
      i_nIdx=cp_AddTableDef('TMPINTSOS') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsdmibve',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPINTSOS_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Select from TMPINTSOS
      i_nConn=i_TableProp[this.TMPINTSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPINTSOS_idx,2],.t.,this.TMPINTSOS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPINTSOS ";
             ,"_Curs_TMPINTSOS")
      else
        select * from (i_cTable);
          into cursor _Curs_TMPINTSOS
      endif
      if used('_Curs_TMPINTSOS')
        select _Curs_TMPINTSOS
        locate for 1=1
        do while not(eof())
        * --- Recupero l'identificativo GUID
        this.w_NOMZIP = _Curs_TMPINTSOS.NOMZIP
        this.w_CLASSE = _Curs_TMPINTSOS.CDCLASOS
        * --- CDCLASOS
        this.w_GUID = RICGUID(this.w_WS)
        if Not empty(this.w_GUID)
          * --- Try
          local bErr_04E8A6B0
          bErr_04E8A6B0=bTrsErr
          this.Try_04E8A6B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_errormsg("%1", "!", ,MESSAGE())
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_04E8A6B0
          * --- End
        endif
          select _Curs_TMPINTSOS
          continue
        enddo
        use
      endif
      if Ah_yesno("Si desidera visualizzare l'elenco degli indici aggiornati?")
        vq_exec("..\docm\exe\query\gsdmsbve",this,"__TMP__")
        CP_CHPRN("..\DOCM\EXE\QUERY\GSDMSBVE.FRX", " ", this)
      endif
      * --- Drop temporary table TMPINTSOS
      i_nIdx=cp_GetTableDefIdx('TMPINTSOS')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPINTSOS')
      endif
    endif
    Ah_ErrorMsg("Elaborazione terminata",64,"")
  endproc
  proc Try_03565758()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if empty(this.w_GUID)
      * --- Raise
      i_Error="GUID non valido"
      return
    endif
    public g_ESITOCONF 
 g_ESITOCONF = " "
    * --- begin transaction
    cp_BeginTrs()
    * --- Creo la tabella temporanea in cui sar� riversato l'esito dell'interrogazione al WS SOS
    * --- Create temporary table TMPINTSOS
    i_nIdx=cp_AddTableDef('TMPINTSOS') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPINTSOS_proto';
          )
    this.TMPINTSOS_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_ESITOSPED = this.w_WS.variazionidaultimainterrogazione(this.w_CODCONS,this.w_CODSOGG,this.w_GUID)
    STRTOFILE(this.w_ESITOSPED,"C:\ESITOSOS.xml")
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    this.oXML = createobject("Msxml2.DOMDocument")
    if L_Err
      AH_ERRORMSG("Impossibile trovare componente MSXML2",48)
      i_retcode = 'stop'
      return
    endif
    ON ERROR &L_OLDERR
    * --- Load XML
    this.oXML.LoadXML(this.w_ESITOSPED)     
    this.w_RootNode = this.oXML.DocumentElement
    if type("this.w_RootNode") = "O"
      Ah_Msg("Inserimento classi documentali SOS")
      do while this.w_CHILDREN < this.w_RootNode.ChildNodes.Length
        * --- Ciclo sui figli del nodo principale -->  'risposta'
        a=ReadEsitoSped(this.w_RootNode)
        if ! a
          Ah_ErrorMsg("Errore nell'esecuzione della funzione ricorsiva.",48,"")
          * --- Drop temporary table TMPINTSOS
          i_nIdx=cp_GetTableDefIdx('TMPINTSOS')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPINTSOS')
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_CHILDREN = this.w_CHILDREN + 1
      enddo
    endif
    this.w_ESITOSPED = ""
    * --- Aggiorno gli attributi di sistema per gli indici variati
    Ah_Msg("Aggiornamento attributi di sistema per gli indici docuemnto variati in SOStitutiva")
    * --- Select from GSDM_BVE
    do vq_exec with 'GSDM_BVE',this,'_Curs_GSDM_BVE','',.f.,.t.
    if used('_Curs_GSDM_BVE')
      select _Curs_GSDM_BVE
      locate for 1=1
      do while not(eof())
      this.w_IDSERIALE = _Curs_GSDM_BVE.IDSERIAL
      this.w_RISPGUID = _Curs_GSDM_BVE.CODRISP
      if this.w_OLDSERIALE # this.w_IDSERIALE
        * --- Leggo il numero di riga
        * --- Select from gsdm1bve
        do vq_exec with 'gsdm1bve',this,'_Curs_gsdm1bve','',.f.,.t.
        if used('_Curs_gsdm1bve')
          select _Curs_gsdm1bve
          locate for 1=1
          do while not(eof())
          this.w_NUMRIGA = _Curs_GSDM1BVE.MAXROWN
          this.w_ORDRIGA = this.w_NUMRIGA * 10
            select _Curs_gsdm1bve
            continue
          enddo
          use
        endif
      endif
      this.w_OLDSERIALE = this.w_IDSERIALE
      * --- Write into PROMINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.STATO_DOC),'PROMINDI','IDSTASOS');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
               )
      else
        update (i_cTable) set;
            IDSTASOS = _Curs_GSDM_BVE.STATO_DOC;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno attributo 'Data conservazione sostitutiva'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.DATACONSOS),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("DataConSos");
               )
      else
        update (i_cTable) set;
            IDVALATT = _Curs_GSDM_BVE.DATACONSOS;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "DataConSos";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("DataConSos"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.DATACONSOS),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"DataConSos",'IDVALATT',_Curs_GSDM_BVE.DATACONSOS)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"DataConSos";
               ,_Curs_GSDM_BVE.DATACONSOS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Id lotto di conservazione'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDLOTCONS),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("IdLotCons");
               )
      else
        update (i_cTable) set;
            IDVALATT = _Curs_GSDM_BVE.IDLOTCONS;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "IdLotCons";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("IdLotCons"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDLOTCONS),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"IdLotCons",'IDVALATT',_Curs_GSDM_BVE.IDLOTCONS)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"IdLotCons";
               ,_Curs_GSDM_BVE.IDLOTCONS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Codice assoluto documento'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODASSDOC),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("CodAssDoc");
               )
      else
        update (i_cTable) set;
            IDVALATT = _Curs_GSDM_BVE.CODASSDOC;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "CodAssDoc";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("CodAssDoc"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODASSDOC),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"CodAssDoc",'IDVALATT',_Curs_GSDM_BVE.CODASSDOC)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"CodAssDoc";
               ,_Curs_GSDM_BVE.CODASSDOC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Data archiviazione documentale'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.DATAARCDOC),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("DataArcDoc");
               )
      else
        update (i_cTable) set;
            IDVALATT = _Curs_GSDM_BVE.DATAARCDOC;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "DataArcDoc";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("DataArcDoc"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.DATAARCDOC),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"DataArcDoc",'IDVALATT',_Curs_GSDM_BVE.DATAARCDOC)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"DataArcDoc";
               ,_Curs_GSDM_BVE.DATAARCDOC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Numero pagine'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALNUM ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.NUMPAG),'PRODINDI','IDVALNUM');
        +",IDVALATT ="+cp_NullLink(cp_ToStrODBC(ALLTRIM(STR(_Curs_GSDM_BVE.NUMPAG,60,0))),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("NumPag");
               )
      else
        update (i_cTable) set;
            IDVALNUM = _Curs_GSDM_BVE.NUMPAG;
            ,IDVALATT = ALLTRIM(STR(_Curs_GSDM_BVE.NUMPAG,60,0));
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "NumPag";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALNUM"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("NumPag"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.NUMPAG),'PRODINDI','IDVALNUM');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(STR(_Curs_GSDM_BVE.NUMPAG,60,0))),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"NumPag",'IDVALNUM',_Curs_GSDM_BVE.NUMPAG,'IDVALATT',ALLTRIM(STR(_Curs_GSDM_BVE.NUMPAG,60,0)))
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALNUM,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"NumPag";
               ,_Curs_GSDM_BVE.NUMPAG;
               ,ALLTRIM(STR(_Curs_GSDM_BVE.NUMPAG,60,0));
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Codice sige'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALNUM ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODSIGE),'PRODINDI','IDVALNUM');
        +",IDVALATT ="+cp_NullLink(cp_ToStrODBC(ALLTRIM(STR(_Curs_GSDM_BVE.CODSIGE,60,0))),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("CodSige");
               )
      else
        update (i_cTable) set;
            IDVALNUM = _Curs_GSDM_BVE.CODSIGE;
            ,IDVALATT = ALLTRIM(STR(_Curs_GSDM_BVE.CODSIGE,60,0));
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "CodSige";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALNUM"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("CodSige"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODSIGE),'PRODINDI','IDVALNUM');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(STR(_Curs_GSDM_BVE.CODSIGE,60,0))),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"CodSige",'IDVALNUM',_Curs_GSDM_BVE.CODSIGE,'IDVALATT',ALLTRIM(STR(_Curs_GSDM_BVE.CODSIGE,60,0)))
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALNUM,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"CodSige";
               ,_Curs_GSDM_BVE.CODSIGE;
               ,ALLTRIM(STR(_Curs_GSDM_BVE.CODSIGE,60,0));
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Codice conc'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODCONC),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("CodConc");
               )
      else
        update (i_cTable) set;
            IDVALATT = _Curs_GSDM_BVE.CODCONC;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "CodConc";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("CodConc"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODCONC),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"CodConc",'IDVALATT',_Curs_GSDM_BVE.CODCONC)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"CodConc";
               ,_Curs_GSDM_BVE.CODCONC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno attributo 'Codice sicon'
      * --- Write into PRODINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODSICON),'PRODINDI','IDVALATT');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL);
            +" and IDCODATT = "+cp_ToStrODBC("CodSicon");
               )
      else
        update (i_cTable) set;
            IDVALATT = _Curs_GSDM_BVE.CODSICON;
            &i_ccchkf. ;
         where;
            IDSERIAL = _Curs_GSDM_BVE.IDSERIAL;
            and IDCODATT = "CodSicon";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_ORDRIGA = this.w_NUMRIGA * 10
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIGA),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("CodSicon"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_GSDM_BVE.CODSICON),'PRODINDI','IDVALATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',_Curs_GSDM_BVE.IDSERIAL,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_ORDRIGA,'IDCODATT',"CodSicon",'IDVALATT',_Curs_GSDM_BVE.CODSICON)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT &i_ccchkf. );
             values (;
               _Curs_GSDM_BVE.IDSERIAL;
               ,this.w_NUMRIGA;
               ,this.w_ORDRIGA;
               ,"CodSicon";
               ,_Curs_GSDM_BVE.CODSICON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
        select _Curs_GSDM_BVE
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if ! empty(nvl(this.w_RISPGUID," "))
      * --- Conferma ricezione variazioni
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Drop temporary table TMPINTSOS
    i_nIdx=cp_GetTableDefIdx('TMPINTSOS')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPINTSOS')
    endif
    return
  proc Try_04E8A6B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.oXML = createobject("Msxml2.DOMDocument")
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    this.w_ESITOSPED = this.w_WS.esitospedizione(this.w_CODCONS,this.w_CODSOGG,this.w_GUID,this.w_NOMZIP,this.w_CLASSE)
    STRTOFILE(this.w_ESITOSPED,Addbs(tempadhoc())+"ESITOSOS.xml")
    * --- Load XML
    this.oXML.LoadXML(this.w_ESITOSPED)     
    this.w_RootNode = this.oXML.DocumentElement
    this.w_STATO = this.w_RootNode.childNodes.item(0).attributes.item(3).text
    do case
      case this.w_STATO="SPDALLOK" OR this.w_STATO="SPDALLKO" OR this.w_STATO="SPDRIF"
        this.w_STATO = IIF(this.w_STATO="SPDALLOK","DOCSTD","DOCKO") 
        * --- Write into PROMINDI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="IDSERIAL"
          do vq_exec with 'gsdmibve_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'PROMINDI','IDSTASOS');
              +i_ccchkf;
              +" from "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 set ";
          +"PROMINDI.IDSTASOS ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'PROMINDI','IDSTASOS');
              +Iif(Empty(i_ccchkf),"",",PROMINDI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PROMINDI.IDSERIAL = t2.IDSERIAL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set (";
              +"IDSTASOS";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(this.w_STATO),'PROMINDI','IDSTASOS')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set ";
          +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'PROMINDI','IDSTASOS');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'PROMINDI','IDSTASOS');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_STATO="SPDOKWERR"
        * --- Metto tutti da inviare e archivio solo quelli inviati
        * --- Write into PROMINDI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="IDSERIAL"
          do vq_exec with 'gsdmibve_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC("DOCKO"),'PROMINDI','IDSTASOS');
              +i_ccchkf;
              +" from "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 set ";
          +"PROMINDI.IDSTASOS ="+cp_NullLink(cp_ToStrODBC("DOCKO"),'PROMINDI','IDSTASOS');
              +Iif(Empty(i_ccchkf),"",",PROMINDI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PROMINDI.IDSERIAL = t2.IDSERIAL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set (";
              +"IDSTASOS";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("DOCKO"),'PROMINDI','IDSTASOS')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set ";
          +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC("DOCKO"),'PROMINDI','IDSTASOS');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC("DOCKO"),'PROMINDI','IDSTASOS');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Ciclo sui documenti e aggiorno i rispettivi stati in base a nomefileoriginale
        this.w_LOOP = 0
        this.w_NUMDOC = this.w_RootNode.childNodes.item(0).childNodes.length
        do while this.w_LOOP < this.w_NUMDOC
          this.w_OBJDOC = this.w_RootNode.childNodes.item(0).childNodes.item(this.w_LOOP)
          this.w_NOMEFILEORIG = this.w_OBJDOC.childNodes.item(0).text
          this.w_NOMEFILEORIG = STRTRAN(SUBSTR(this.w_NOMEFILEORIG,1,at("(",this.w_NOMEFILEORIG)-1)+ SUBSTR(this.w_NOMEFILEORIG,at(")",this.w_NOMEFILEORIG)+1),".p7m","")
          this.w_STATO = this.w_OBJDOC.childNodes.item(3).text
          * --- Write into PROMINDI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC("DOCSTD"),'PROMINDI','IDSTASOS');
                +i_ccchkf ;
            +" where ";
                +"IDNOMFIL = "+cp_ToStrODBC(this.w_NOMEFILEORIG);
                   )
          else
            update (i_cTable) set;
                IDSTASOS = "DOCSTD";
                &i_ccchkf. ;
             where;
                IDNOMFIL = this.w_NOMEFILEORIG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
    endcase
    * --- w_RootNode.childNodes.item(0).attributes.item(3).text
    ON ERROR &L_OLDERR
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_04EA8E60
    bErr_04EA8E60=bTrsErr
    this.Try_04EA8E60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      Ah_ErrorMsg("Si � verificato un errore durante l'operazione di conferma ricezione aggiornamenti",48,"")
    endif
    bTrsErr=bTrsErr or bErr_04EA8E60
    * --- End
  endproc
  proc Try_04EA8E60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_NUMCICLI = 1
    do while alltrim(g_ESITOCONF) # "CONFIRMOK" and this.w_NUMCICLI < 10
      this.w_GUID = RICGUID(this.w_WS)
      if empty(this.w_GUID)
        * --- Raise
        i_Error="GUID non valido"
        return
      endif
      this.w_ESITOCONF = this.w_WS.confermavariazioni(this.w_CODCONS,this.w_CODSOGG,this.w_GUID,this.w_RISPGUID)
      this.oXML.LoadXML(this.w_ESITOCONF)     
      this.w_RootNode = this.oXML.DocumentElement
      if type("this.w_RootNode") = "O"
        Ah_Msg("Verifica esito conferma ricezione aggiornamento")
        a=ReadEsitoSped(this.w_RootNode)
        if ! a
          Ah_ErrorMsg("Errore nell'esecuzione della funzione per conferma ricezione aggiornamento",48,"")
          i_retcode = 'stop'
          return
        endif
      endif
      this.w_NUMCICLI = this.w_NUMCICLI + 1
      InKey(2,"H")
    enddo
    if alltrim(g_ESITOCONF ) == "CONFIRMOK"
      Ah_ErrorMsg("Conferma ricezione aggiornamenti avvenuta correttamente",64,"")
    else
      Ah_ErrorMsg("Non � stato possibile confermare l'avvenuta ricezione degli aggiornamenti.",64,"")
    endif
    release g_ESITOCONF
    return


  proc Init(oParentObject,pTipoOp)
    this.pTipoOp=pTipoOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR_SOST'
    this.cWorkTables[2]='*TMPINTSOS'
    this.cWorkTables[3]='PROMINDI'
    this.cWorkTables[4]='PRODINDI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSDM_BVE')
      use in _Curs_GSDM_BVE
    endif
    if used('_Curs_gsdm1bve')
      use in _Curs_gsdm1bve
    endif
    if used('_Curs_TMPINTSOS')
      use in _Curs_TMPINTSOS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipoOp"
endproc
