* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bcp                                                        *
*              Inserimento processi documentali                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-28                                                      *
* Last revis.: 2016-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPINSERT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bcp",oParentObject,m.w_TIPINSERT)
return(i_retval)

define class tgsdm_bcp as StdBatch
  * --- Local variables
  w_TIPINSERT = space(1)
  w_MESS = space(50)
  * --- WorkFile variables
  PRODDOCU_idx=0
  PROMDOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'inserimento delle Classi documentali, con Carica/Salva dati esterni, invocato da 'GSUT_BCO('Carica')'
    * --- Prima Riga Dettaglio
    * --- Try
    local bErr_02A5BBA0
    bErr_02A5BBA0=bTrsErr
    this.Try_02A5BBA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERROREPROC = .T.
      if this.oParentObject.w_NOMSG
        * --- accept error
        bTrsErr=.f.
      endif
    endif
    bTrsErr=bTrsErr or bErr_02A5BBA0
    * --- End
  endproc
  proc Try_02A5BBA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_TIPINSERT="D"
        * --- Try
        local bErr_02A5C230
        bErr_02A5C230=bTrsErr
        this.Try_02A5C230()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_02A5C500
          bErr_02A5C500=bTrsErr
          this.Try_02A5C500()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_MESS = "Errore di scrittura in codice: %1%2Continuo?"
            if this.oParentObject.w_NOMSG or not AH_YESNO(this.w_MESS,"",this.oParentObject.w_PDCODPRO,CHR(13))
              * --- Raise
              i_Error=AH_MsgFormat("Import fallito")
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_02A5C500
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_02A5C230
        * --- End
      case this.w_TIPINSERT="M"
        * --- Try
        local bErr_02A59860
        bErr_02A59860=bTrsErr
        this.Try_02A59860()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = "Errore di inserimento in codice: %1%2Continuo?"
          if this.oParentObject.w_NOMSG or not AH_YESNO(this.w_MESS,"",this.oParentObject.w_PDCODPRO,CHR(13))
            * --- Raise
            i_Error=AH_MsgFormat("Import fallito")
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_02A59860
        * --- End
    endcase
    return
  proc Try_02A5C230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRODDOCU
    i_nConn=i_TableProp[this.PRODDOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODDOCU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODDOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PDCODPRO"+",PDREPORT"+",PDDESREP"+",PDKEYPRO"+",PDCONCON"+",PDOGGETT"+",PD_TESTO"+",PDFILTRO"+",PDTIPRIF"+",PDISOLAN"+",PDCODSED"+",PDCODORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCODPRO),'PRODDOCU','PDCODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDREPORT),'PRODDOCU','PDREPORT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDDESREP),'PRODDOCU','PDDESREP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDKEYPRO),'PRODDOCU','PDKEYPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCONCON),'PRODDOCU','PDCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDOGGETT),'PRODDOCU','PDOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PD_TESTO),'PRODDOCU','PD_TESTO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDFILTRO),'PRODDOCU','PDFILTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDTIPRIF),'PRODDOCU','PDTIPRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDISOLAN),'PRODDOCU','PDISOLAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCODSED),'PRODDOCU','PDCODSED');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCODORD),'PRODDOCU','PDCODORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PDCODPRO',this.oParentObject.w_PDCODPRO,'PDREPORT',this.oParentObject.w_PDREPORT,'PDDESREP',this.oParentObject.w_PDDESREP,'PDKEYPRO',this.oParentObject.w_PDKEYPRO,'PDCONCON',this.oParentObject.w_PDCONCON,'PDOGGETT',this.oParentObject.w_PDOGGETT,'PD_TESTO',this.oParentObject.w_PD_TESTO,'PDFILTRO',this.oParentObject.w_PDFILTRO,'PDTIPRIF',this.oParentObject.w_PDTIPRIF,'PDISOLAN',this.oParentObject.w_PDISOLAN,'PDCODSED',this.oParentObject.w_PDCODSED,'PDCODORD',this.oParentObject.w_PDCODORD)
      insert into (i_cTable) (PDCODPRO,PDREPORT,PDDESREP,PDKEYPRO,PDCONCON,PDOGGETT,PD_TESTO,PDFILTRO,PDTIPRIF,PDISOLAN,PDCODSED,PDCODORD &i_ccchkf. );
         values (;
           this.oParentObject.w_PDCODPRO;
           ,this.oParentObject.w_PDREPORT;
           ,this.oParentObject.w_PDDESREP;
           ,this.oParentObject.w_PDKEYPRO;
           ,this.oParentObject.w_PDCONCON;
           ,this.oParentObject.w_PDOGGETT;
           ,this.oParentObject.w_PD_TESTO;
           ,this.oParentObject.w_PDFILTRO;
           ,this.oParentObject.w_PDTIPRIF;
           ,this.oParentObject.w_PDISOLAN;
           ,this.oParentObject.w_PDCODSED;
           ,this.oParentObject.w_PDCODORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02A5C500()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRODDOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRODDOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODDOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODDOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PDREPORT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDREPORT),'PRODDOCU','PDREPORT');
      +",PDDESREP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDDESREP),'PRODDOCU','PDDESREP');
      +",PDKEYPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDKEYPRO),'PRODDOCU','PDKEYPRO');
      +",PDCONCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCONCON),'PRODDOCU','PDCONCON');
      +",PDOGGETT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDOGGETT),'PRODDOCU','PDOGGETT');
      +",PD_TESTO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PD_TESTO),'PRODDOCU','PD_TESTO');
      +",PDFILTRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDFILTRO),'PRODDOCU','PDFILTRO');
      +",PDISOLAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDISOLAN),'PRODDOCU','PDISOLAN');
      +",PDTIPRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDTIPRIF),'PRODDOCU','PDTIPRIF');
      +",PDCODSED ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCODSED),'PRODDOCU','PDCODSED');
      +",PDCODORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCODORD),'PRODDOCU','PDCODORD');
          +i_ccchkf ;
      +" where ";
          +"PDCODPRO = "+cp_ToStrODBC(this.oParentObject.w_PDCODPRO);
             )
    else
      update (i_cTable) set;
          PDREPORT = this.oParentObject.w_PDREPORT;
          ,PDDESREP = this.oParentObject.w_PDDESREP;
          ,PDKEYPRO = this.oParentObject.w_PDKEYPRO;
          ,PDCONCON = this.oParentObject.w_PDCONCON;
          ,PDOGGETT = this.oParentObject.w_PDOGGETT;
          ,PD_TESTO = this.oParentObject.w_PD_TESTO;
          ,PDFILTRO = this.oParentObject.w_PDFILTRO;
          ,PDISOLAN = this.oParentObject.w_PDISOLAN;
          ,PDTIPRIF = this.oParentObject.w_PDTIPRIF;
          ,PDCODSED = this.oParentObject.w_PDCODSED;
          ,PDCODORD = this.oParentObject.w_PDCODORD;
          &i_ccchkf. ;
       where;
          PDCODPRO = this.oParentObject.w_PDCODPRO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_02A59860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_msg("Inserisco il processo: %1",.t.,.f.,.f.,this.oParentObject.w_PDCODPRO )
    * --- Inserisco la testata
    * --- Insert into PROMDOCU
    i_nConn=i_TableProp[this.PROMDOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMDOCU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMDOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PDCODPRO"+",PDDESPRO"+",PDCLADOC"+",PDCHKOBB"+",PDCHKLOG"+",PDCHKANT"+",PDCHKSTA"+",PDCHKMAI"+",PDCHKFAX"+",PDCHKWWP"+",PDCHKARC"+",PDCHKPTL"+",PDRILSTA"+",PDRILMAI"+",PDRILFAX"+",PDRILWWP"+",PDRILARC"+",PDRILPTL"+",PDNMAXAL"+",PDCHKZCP"+",PDRILZCP"+",PDCHKPEC"+",PDRILPEC"+",PDPROCES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCODPRO),'PROMDOCU','PDCODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDDESPRO),'PROMDOCU','PDDESPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCLADOC),'PROMDOCU','PDCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKOBB),'PROMDOCU','PDCHKOBB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKLOG),'PROMDOCU','PDCHKLOG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKANT),'PROMDOCU','PDCHKANT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKSTA),'PROMDOCU','PDCHKSTA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKMAI),'PROMDOCU','PDCHKMAI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKFAX),'PROMDOCU','PDCHKFAX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKWWP),'PROMDOCU','PDCHKWWP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKARC),'PROMDOCU','PDCHKARC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKPTL),'PROMDOCU','PDCHKPTL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILSTA),'PROMDOCU','PDRILSTA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILMAI),'PROMDOCU','PDRILMAI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILFAX),'PROMDOCU','PDRILFAX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILWWP),'PROMDOCU','PDRILWWP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILARC),'PROMDOCU','PDRILARC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILPTL),'PROMDOCU','PDRILPTL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDNMAXAL),'PROMDOCU','PDNMAXAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKZCP),'PROMDOCU','PDCHKZCP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILZCP),'PROMDOCU','PDRILZCP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDCHKPEC),'PROMDOCU','PDCHKPEC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDRILPEC),'PROMDOCU','PDRILPEC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PDPROCES),'PROMDOCU','PDPROCES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PDCODPRO',this.oParentObject.w_PDCODPRO,'PDDESPRO',this.oParentObject.w_PDDESPRO,'PDCLADOC',this.oParentObject.w_PDCLADOC,'PDCHKOBB',this.oParentObject.w_PDCHKOBB,'PDCHKLOG',this.oParentObject.w_PDCHKLOG,'PDCHKANT',this.oParentObject.w_PDCHKANT,'PDCHKSTA',this.oParentObject.w_PDCHKSTA,'PDCHKMAI',this.oParentObject.w_PDCHKMAI,'PDCHKFAX',this.oParentObject.w_PDCHKFAX,'PDCHKWWP',this.oParentObject.w_PDCHKWWP,'PDCHKARC',this.oParentObject.w_PDCHKARC,'PDCHKPTL',this.oParentObject.w_PDCHKPTL)
      insert into (i_cTable) (PDCODPRO,PDDESPRO,PDCLADOC,PDCHKOBB,PDCHKLOG,PDCHKANT,PDCHKSTA,PDCHKMAI,PDCHKFAX,PDCHKWWP,PDCHKARC,PDCHKPTL,PDRILSTA,PDRILMAI,PDRILFAX,PDRILWWP,PDRILARC,PDRILPTL,PDNMAXAL,PDCHKZCP,PDRILZCP,PDCHKPEC,PDRILPEC,PDPROCES &i_ccchkf. );
         values (;
           this.oParentObject.w_PDCODPRO;
           ,this.oParentObject.w_PDDESPRO;
           ,this.oParentObject.w_PDCLADOC;
           ,this.oParentObject.w_PDCHKOBB;
           ,this.oParentObject.w_PDCHKLOG;
           ,this.oParentObject.w_PDCHKANT;
           ,this.oParentObject.w_PDCHKSTA;
           ,this.oParentObject.w_PDCHKMAI;
           ,this.oParentObject.w_PDCHKFAX;
           ,this.oParentObject.w_PDCHKWWP;
           ,this.oParentObject.w_PDCHKARC;
           ,this.oParentObject.w_PDCHKPTL;
           ,this.oParentObject.w_PDRILSTA;
           ,this.oParentObject.w_PDRILMAI;
           ,this.oParentObject.w_PDRILFAX;
           ,this.oParentObject.w_PDRILWWP;
           ,this.oParentObject.w_PDRILARC;
           ,this.oParentObject.w_PDRILPTL;
           ,this.oParentObject.w_PDNMAXAL;
           ,this.oParentObject.w_PDCHKZCP;
           ,this.oParentObject.w_PDRILZCP;
           ,this.oParentObject.w_PDCHKPEC;
           ,this.oParentObject.w_PDRILPEC;
           ,this.oParentObject.w_PDPROCES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_TIPINSERT)
    this.w_TIPINSERT=w_TIPINSERT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRODDOCU'
    this.cWorkTables[2]='PROMDOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPINSERT"
endproc
