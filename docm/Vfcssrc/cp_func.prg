* --- Container for functions
* --- START LOADCLASOS
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: loadclasos                                                      *
*              Carica/Aggiorna le classi documentali                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-26                                                      *
* Last revis.: 2009-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func loadclasos
param pOBJ,PCodClass,pNumRow,pCodChiave

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_NUM_ATTR
  m.w_NUM_ATTR=0
  private w_NUM_CHILD
  m.w_NUM_CHILD=0
  private w_VALORE
  m.w_VALORE=space(100)
  private w_NODO
  m.w_NODO=space(100)
  private w_CODCLASSE
  m.w_CODCLASSE=space(10)
  private w_CODCHIAVE
  m.w_CODCHIAVE=space(10)
  private w_DESCRCLASSE
  m.w_DESCRCLASSE=space(50)
  private w_ISDETT
  m.w_ISDETT=.f.
  private w_OBJCHILD
  m.w_OBJCHILD = .null.
* --- WorkFile variables
  private CLASMSOS_idx
  CLASMSOS_idx=0
  private CLASDSOS_idx
  CLASDSOS_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "loadclasos"
if vartype(__loadclasos_hook__)='O'
  __loadclasos_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'loadclasos('+Transform(pOBJ)+','+Transform(PCodClass)+','+Transform(pNumRow)+','+Transform(pCodChiave)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'loadclasos')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if loadclasos_OpenTables()
  loadclasos_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'loadclasos('+Transform(pOBJ)+','+Transform(PCodClass)+','+Transform(pNumRow)+','+Transform(pCodChiave)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'loadclasos')
Endif
*--- Activity log
if vartype(__loadclasos_hook__)='O'
  __loadclasos_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure loadclasos_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Routine per il caricamento/aggiornamento delle classi
  * --- Controllo dove devo inserire i dati presenti nell'oggetto (XML)
  m.w_ISDETT = iif(vartype(m.pNumRow)="N" and m.pNumRow > 0, .T.,.F.)
  m.w_CODCLASSE = iif(vartype(m.pCodClass)="C" and not empty(m.pCodClass),m.pCodClass,space(10))
  m.w_CODCHIAVE = iif(vartype(m.pCodChiave)="C" and not empty(m.pCodChiave),m.pCodChiave,space(10))
  if vartype(m.pOBJ) = "O"
    if vartype(m.pOBJ.Attributes.length) ="N" and m.pOBJ.Attributes.length>0
      m.w_NUM_ATTR = 0
      do while m.w_NUM_ATTR < m.pOBJ.Attributes.Length
        m.w_NODO = m.pOBJ.Attributes.item(m.w_NUM_ATTR).basename
        m.w_VALORE = m.pOBJ.Attributes.item(m.w_NUM_ATTR).text
        if ! m.w_ISDETT
          loadclasos_Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          loadclasos_Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        m.w_NUM_ATTR = m.w_NUM_ATTR + 1
      enddo
    endif
    if m.pOBJ.HasChildNodes
      m.w_NUM_CHILD = 0
      do while m.w_NUM_CHILD < m.pOBJ.childnodes.Length
        * --- Ciclo sui filgli (Sotto nodi)
        m.w_OBJCHILD = m.pOBJ.childnodes.item(m.w_NUM_CHILD)
        m.w_NODO = m.w_OBJCHILD.basename
        if ! empty(m.w_NODO)
          if (m.w_OBJCHILD.HasChildNodes or vartype(m.w_OBJCHILD.Attributes.Length)="N")
            a= LoadClaSos(m.w_OBJCHILD,m.w_CODCLASSE,@m.pNumRow,m.w_CodChiave)
            if ! a
              Ah_ErrorMsg("Errore nell'esecuzione della funzione ricorsiva. %1",48,"",m.w_NODO)
              i_retcode = 'stop'
              i_retval = .F.
              return
            endif
          else
            m.w_VALORE = w_OBJ_CHILD.Text
            m.w_NODO = w_OBJ_CHILD.BaseName
            if ! m.w_ISDETT
              loadclasos_Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              loadclasos_Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        else
          m.w_VALORE = m.pOBJ.Text
          m.w_NODO = m.pOBJ.BaseName
          if ! m.w_ISDETT
            loadclasos_Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            loadclasos_Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        m.w_NUM_CHILD = m.w_NUM_CHILD + 1
      enddo
    else
      m.w_NODO = m.pOBJ.BaseName
      m.w_VALORE = m.pOBJ.Text
      if ! m.w_ISDETT
        loadclasos_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        loadclasos_Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endif
  * --- Ritorna sempre true
  i_retcode = 'stop'
  i_retval = .T.
  return
endproc


procedure loadclasos_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Inserimento classi in tabella AHR CLASMSOS (TESTATA)
  do case
    case lower(alltrim(m.w_NODO)) = "codiceclassedocumentale"
      * --- Try
      loadclasos_Try_039E7D70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into CLASMSOS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[CLASMSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[CLASMSOS_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,CLASMSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SZDATAGG = "+cp_NullLink(cp_ToStrODBC(i_DATSYS),'CLASMSOS','SZDATAGG');
          +",SZCLAOBS = "+cp_NullLink(cp_ToStrODBC("N"),'CLASMSOS','SZCLAOBS');
              +i_ccchkf ;
          +" where ";
              +"SZCODCLA = "+cp_ToStrODBC(m.w_VALORE);
                 )
        else
          update (i_cTable) set;
              SZDATAGG = i_DATSYS;
              ,SZCLAOBS = "N";
              &i_ccchkf. ;
           where;
              SZCODCLA = m.w_VALORE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if i_Rows=0
        i_retcode = 'stop'
        i_retval = .F.
        return
        endif
      endif
      * --- End
    case lower(alltrim(m.w_NODO)) = "descrizione"
      * --- Write into CLASMSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[CLASMSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[CLASMSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,CLASMSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZDESCLA = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'CLASMSOS','SZDESCLA');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(m.w_CODCLASSE);
               )
      else
        update (i_cTable) set;
            SZDESCLA = m.w_VALORE;
            &i_ccchkf. ;
         where;
            SZCODCLA = m.w_CODCLASSE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
  endcase
endproc
  proc loadclasos_Try_039E7D70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  m.w_CODCLASSE = m.w_VALORE
  * --- Insert into CLASMSOS
  i_nConn=i_TableProp[CLASMSOS_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CLASMSOS_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_ccchkf=''
  i_ccchkv=''
  func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,CLASMSOS_idx,i_nConn)
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                " ("+"SZCODCLA"+",SZDATAGG"+",SZCLAOBS"+i_ccchkf+") values ("+;
    cp_NullLink(cp_ToStrODBC(m.w_VALORE),'CLASMSOS','SZCODCLA');
    +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'CLASMSOS','SZDATAGG');
    +","+cp_NullLink(cp_ToStrODBC("N"),'CLASMSOS','SZCLAOBS');
         +i_ccchkv+")")
  else
    cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',m.w_VALORE,'SZDATAGG',i_DATSYS,'SZCLAOBS',"N")
    insert into (i_cTable) (SZCODCLA,SZDATAGG,SZCLAOBS &i_ccchkf. );
       values (;
         m.w_VALORE;
         ,i_DATSYS;
         ,"N";
         &i_ccchkv. )
    i_Rows=iif(bTrsErr,0,1)
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if i_Rows<0 or bTrsErr
    * --- Error: insert not accepted
    i_Error=MSG_INSERT_ERROR
    return
  endif
    return


procedure loadclasos_Pag3
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Inserimento classi in tabella AHR CLASDSOS (DETTAGLIO)
  do case
    case lower(alltrim(m.w_NODO)) = "codiceclassedocumentale"
      m.w_CODCLASSE = m.w_VALORE
    case lower(alltrim(m.w_NODO)) = "codicechiave"
      * --- Try
      loadclasos_Try_03AD5FD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      * --- End
    case lower(alltrim(m.w_NODO)) = "descrizione"
      * --- Write into CLASDSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[CLASDSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[CLASDSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,CLASDSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZDESKEY = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'CLASDSOS','SZDESKEY');
        +",SZKEYOBS = "+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(m.w_CODCLASSE);
            +" and SZCODKEY = "+cp_ToStrODBC(m.w_CODCHIAVE);
               )
      else
        update (i_cTable) set;
            SZDESKEY = m.w_VALORE;
            ,SZKEYOBS = "N";
            &i_ccchkf. ;
         where;
            SZCODCLA = m.w_CODCLASSE;
            and SZCODKEY = m.w_CODCHIAVE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    case lower(alltrim(m.w_NODO)) = "tipo"
      * --- Write into CLASDSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[CLASDSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[CLASDSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,CLASDSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZTIPKEY = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'CLASDSOS','SZTIPKEY');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(m.w_CODCLASSE);
            +" and SZCODKEY = "+cp_ToStrODBC(m.w_CODCHIAVE);
               )
      else
        update (i_cTable) set;
            SZTIPKEY = m.w_VALORE;
            &i_ccchkf. ;
         where;
            SZCODCLA = m.w_CODCLASSE;
            and SZCODKEY = m.w_CODCHIAVE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
  endcase
endproc
  proc loadclasos_Try_03AD5FD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  m.w_CODCHIAVE = m.w_VALORE
  m.pNumRow = m.pNumRow + 10
  * --- Insert into CLASDSOS
  i_nConn=i_TableProp[CLASDSOS_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CLASDSOS_idx,2])
  i_commit = .f.
  local bErr_03ACBE30
  bErr_03ACBE30=bTrsErr
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_ccchkf=''
  i_ccchkv=''
  func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,CLASDSOS_idx,i_nConn)
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                " ("+"SZCODCLA"+",SZCODKEY"+",CPROWORD"+",SZTIPINT"+",SZKEYOBS"+i_ccchkf+") values ("+;
    cp_NullLink(cp_ToStrODBC(m.w_CODCLASSE),'CLASDSOS','SZCODCLA');
    +","+cp_NullLink(cp_ToStrODBC(m.w_CODCHIAVE),'CLASDSOS','SZCODKEY');
    +","+cp_NullLink(cp_ToStrODBC(m.pNumRow),'CLASDSOS','CPROWORD');
    +","+cp_NullLink(cp_ToStrODBC("LI"),'CLASDSOS','SZTIPINT');
    +","+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
         +i_ccchkv+")")
  else
    cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',m.w_CODCLASSE,'SZCODKEY',m.w_CODCHIAVE,'CPROWORD',m.pNumRow,'SZTIPINT',"LI",'SZKEYOBS',"N")
    insert into (i_cTable) (SZCODCLA,SZCODKEY,CPROWORD,SZTIPINT,SZKEYOBS &i_ccchkf. );
       values (;
         m.w_CODCLASSE;
         ,m.w_CODCHIAVE;
         ,m.pNumRow;
         ,"LI";
         ,"N";
         &i_ccchkv. )
    i_Rows=iif(bTrsErr,0,1)
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if i_Rows<0 or bTrsErr
    bTrsErr=bErr_03ACBE30
  * --- Write into CLASDSOS
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_nConn=i_TableProp[CLASDSOS_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CLASDSOS_idx,2])
  i_ccchkf=''
  func_SetCCCHKVarsWrite(@i_ccchkf,CLASDSOS_idx,i_nConn)
  if i_nConn<>0
    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
    +"SZCODKEY = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'CLASDSOS','SZCODKEY');
        +i_ccchkf ;
    +" where ";
        +"SZCODCLA = "+cp_ToStrODBC(m.w_CODCLASSE);
        +" and CPROWORD = "+cp_ToStrODBC(m.pNumRow);
           )
  else
    update (i_cTable) set;
        SZCODKEY = m.w_VALORE;
        &i_ccchkf. ;
     where;
        SZCODCLA = m.w_CODCLASSE;
        and CPROWORD = m.pNumRow;

    i_Rows = _tally
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if bTrsErr
    i_Error=MSG_WRITE_ERROR
    return
  endif
  endif
    return


  function loadclasos_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='CLASMSOS'
    i_cWorkTables[2]='CLASDSOS'
    return(cp_OpenFuncTables(2))
* --- END LOADCLASOS
* --- START READESITOSPED
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: readesitosped                                                   *
*              Carica/Aggiorna le classi documentali                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-26                                                      *
* Last revis.: 2009-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func readesitosped
param pOBJ,PCodRisp,pCodAbs,PCodKey

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_NUM_ATTR
  m.w_NUM_ATTR=0
  private w_NUM_CHILD
  m.w_NUM_CHILD=0
  private w_VALORE
  m.w_VALORE=space(100)
  private w_NODO
  m.w_NODO=space(100)
  private w_CODASSOLUT
  m.w_CODASSOLUT=space(20)
  private w_CODICERISP
  m.w_CODICERISP=space(50)
  private w_DATACONSOS
  m.w_DATACONSOS=space(10)
  private w_DATAARCDOC
  m.w_DATAARCDOC=space(10)
  private w_STATOSPED
  m.w_STATOSPED=space(15)
  private w_LOTTOID
  m.w_LOTTOID=space(50)
  private w_STATODOC
  m.w_STATODOC=space(15)
  private w_READKEYVALUE
  m.w_READKEYVALUE=.f.
  private w_CODKEY
  m.w_CODKEY=space(10)
  private w_OBJCHILD
  m.w_OBJCHILD = .null.
* --- WorkFile variables
  private CLASMSOS_idx
  CLASMSOS_idx=0
  private CLASDSOS_idx
  CLASDSOS_idx=0
  private TMPINTSOS_idx
  TMPINTSOS_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "readesitosped"
if vartype(__readesitosped_hook__)='O'
  __readesitosped_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'readesitosped('+Transform(pOBJ)+','+Transform(PCodRisp)+','+Transform(pCodAbs)+','+Transform(PCodKey)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'readesitosped')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if readesitosped_OpenTables()
  readesitosped_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'readesitosped('+Transform(pOBJ)+','+Transform(PCodRisp)+','+Transform(pCodAbs)+','+Transform(PCodKey)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'readesitosped')
Endif
*--- Activity log
if vartype(__readesitosped_hook__)='O'
  __readesitosped_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure readesitosped_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Routine per il caricamento/aggiornamento delle classi
  m.w_CODICERISP = iif(vartype(m.pCodRisp)="C" and not empty(m.pCodRisp),m.pCodRisp,space(50))
  m.w_CODASSOLUT = iif(vartype(m.pCodAbs)="C" and not empty(m.pCodAbs),m.pCodAbs,space(20))
  m.w_CODKEY = iif(vartype(m.pCodKey)="C" and not empty(m.pCodKey),m.pCodKey,space(10))
  m.w_READKEYVALUE = ! empty(m.w_CODKEY)
  if ! empty(m.w_CODICERISP)
    Ah_Msg("Codice  risposta: %1 cod. assoluto: %2 cod. chiave %3",.t.,.t.,.f.,m.w_CODICERISP,m.w_CODASSOLUT,m.w_CODKEY)
  endif
  if vartype(m.pOBJ) = "O"
    if vartype(m.pOBJ.Attributes.length) ="N" and m.pOBJ.Attributes.length>0
      m.w_NUM_ATTR = 0
      do while m.w_NUM_ATTR < m.pOBJ.Attributes.Length
        m.w_NODO = m.pOBJ.Attributes.item(m.w_NUM_ATTR).basename
        m.w_VALORE = m.pOBJ.Attributes.item(m.w_NUM_ATTR).text
        readesitosped_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        m.w_NUM_ATTR = m.w_NUM_ATTR + 1
      enddo
    endif
    if m.pOBJ.HasChildNodes
      m.w_NUM_CHILD = 0
      do while m.w_NUM_CHILD < m.pOBJ.childnodes.Length
        * --- Ciclo sui filgli (Sotto nodi)
        m.w_OBJCHILD = m.pOBJ.childnodes.item(m.w_NUM_CHILD)
        m.w_NODO = m.w_OBJCHILD.basename
        if ! empty(m.w_NODO)
          if (m.w_OBJCHILD.HasChildNodes or vartype(m.w_OBJCHILD.Attributes.Length)="N")
            a= ReadEsitoSped(m.w_OBJCHILD,m.w_CODICERISP,m.w_CODASSOLUT,m.w_CODKEY)
            if ! a
              Ah_ErrorMsg("Errore nell'esecuzione della funzione ricorsiva. %1",48,"",m.w_NODO)
              i_retcode = 'stop'
              i_retval = .F.
              return
            endif
          else
            m.w_VALORE = w_OBJ_CHILD.Text
            m.w_NODO = w_OBJ_CHILD.BaseName
            readesitosped_Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          m.w_VALORE = m.pOBJ.Text
          m.w_NODO = m.pOBJ.BaseName
          readesitosped_Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        m.w_NUM_CHILD = m.w_NUM_CHILD + 1
      enddo
    else
      m.w_NODO = m.pOBJ.BaseName
      m.w_VALORE = m.pOBJ.Text
      readesitosped_Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endif
  * --- Ritorna sempre true
  i_retcode = 'stop'
  i_retval = .T.
  return
endproc


procedure readesitosped_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Inserimento classi in tabella AHR CLASMSOS (TESTATA)
  * --- Controllo attributi della chiave
  do case
    case lower(alltrim(m.w_NODO)) = "codiceassoluto"
      m.w_CODASSOLUT = m.w_VALORE
      * --- Try
      readesitosped_Try_03A1AB18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        i_retcode = 'stop'
        i_retval = .F.
        return
      endif
      * --- End
    case lower(alltrim(m.w_NODO)) = "codicerisposta"
      m.w_CODICERISP = m.w_VALORE
    case lower(alltrim(m.w_NODO)) = "statospedizione"
      m.w_STATOSPED = m.w_VALORE
    case lower(alltrim(m.w_NODO)) = "dataconservazionesostitutiva"
      m.w_DATACONSOS = m.w_VALORE
      * --- Write into TMPINTSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMPINTSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DATACONSOS = "+cp_NullLink(cp_ToStrODBC(m.w_DATACONSOS),'TMPINTSOS','DATACONSOS');
            +i_ccchkf ;
        +" where ";
            +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
            +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
               )
      else
        update (i_cTable) set;
            DATACONSOS = m.w_DATACONSOS;
            &i_ccchkf. ;
         where;
            CODRISP = m.w_CODICERISP;
            and CODASSDOC = m.w_CODASSOLUT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    case lower(alltrim(m.w_NODO)) = "dataarchiviazionedocumentale"
      m.w_DATAARCDOC = m.w_VALORE
      * --- Write into TMPINTSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMPINTSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DATAARCDOC = "+cp_NullLink(cp_ToStrODBC(m.w_DATAARCDOC),'TMPINTSOS','DATAARCDOC');
            +i_ccchkf ;
        +" where ";
            +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
            +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
               )
      else
        update (i_cTable) set;
            DATAARCDOC = m.w_DATAARCDOC;
            &i_ccchkf. ;
         where;
            CODRISP = m.w_CODICERISP;
            and CODASSDOC = m.w_CODASSOLUT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    case lower(alltrim(m.w_NODO)) = "lottoid"
      m.w_LOTTOID = m.w_VALORE
      * --- Write into TMPINTSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMPINTSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDLOTCONS = "+cp_NullLink(cp_ToStrODBC(m.w_LOTTOID),'TMPINTSOS','IDLOTCONS');
            +i_ccchkf ;
        +" where ";
            +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
            +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
               )
      else
        update (i_cTable) set;
            IDLOTCONS = m.w_LOTTOID;
            &i_ccchkf. ;
         where;
            CODRISP = m.w_CODICERISP;
            and CODASSDOC = m.w_CODASSOLUT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    case lower(alltrim(m.w_NODO)) = "stato"
      m.w_STATODOC = m.w_VALORE
      * --- Write into TMPINTSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMPINTSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"STATO_DOC = "+cp_NullLink(cp_ToStrODBC(m.w_STATODOC),'TMPINTSOS','STATO_DOC');
            +i_ccchkf ;
        +" where ";
            +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
            +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
               )
      else
        update (i_cTable) set;
            STATO_DOC = m.w_STATODOC;
            &i_ccchkf. ;
         where;
            CODRISP = m.w_CODICERISP;
            and CODASSDOC = m.w_CODASSOLUT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    case lower(alltrim(m.w_NODO)) = "codicechiave"
      if inlist(lower(m.w_VALORE),"hashfilearc","numpag","codconc","codsige","codsicon")
        m.w_CODKEY = lower(m.w_VALORE)
        m.w_READKEYVALUE = .T.
      endif
    case lower(alltrim(m.w_NODO)) = "esito"
      * --- Utilizzo una variabile pubblica per mantenere indipendente la funzione dal chiamante
      g_ESITOCONF = m.w_VALORE
  endcase
  * --- Controllo valori della chiave
  if m.w_READKEYVALUE and lower(m.w_NODO)="valore"
    do case
      case m.w_CODKEY="hashfilearc"
        * --- Write into TMPINTSOS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[TMPINTSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODHASH = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'TMPINTSOS','CODHASH');
              +i_ccchkf ;
          +" where ";
              +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
              +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
                 )
        else
          update (i_cTable) set;
              CODHASH = m.w_VALORE;
              &i_ccchkf. ;
           where;
              CODRISP = m.w_CODICERISP;
              and CODASSDOC = m.w_CODASSOLUT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        m.w_CODKEY = space(10)
      case m.w_CODKEY="numpag"
        m.w_VALORE = int(val(m.w_VALORE))
        * --- Write into TMPINTSOS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[TMPINTSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NUMPAG = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'TMPINTSOS','NUMPAG');
              +i_ccchkf ;
          +" where ";
              +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
              +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
                 )
        else
          update (i_cTable) set;
              NUMPAG = m.w_VALORE;
              &i_ccchkf. ;
           where;
              CODRISP = m.w_CODICERISP;
              and CODASSDOC = m.w_CODASSOLUT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        m.w_CODKEY = space(10)
      case m.w_CODKEY="codconc"
        * --- Write into TMPINTSOS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[TMPINTSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODCONC = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'TMPINTSOS','CODCONC');
              +i_ccchkf ;
          +" where ";
              +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
              +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
                 )
        else
          update (i_cTable) set;
              CODCONC = m.w_VALORE;
              &i_ccchkf. ;
           where;
              CODRISP = m.w_CODICERISP;
              and CODASSDOC = m.w_CODASSOLUT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        m.w_CODKEY = space(10)
      case m.w_CODKEY="codsige"
        m.w_VALORE = int(val(m.w_VALORE))
        * --- Write into TMPINTSOS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[TMPINTSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODSIGE = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'TMPINTSOS','CODSIGE');
              +i_ccchkf ;
          +" where ";
              +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
              +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
                 )
        else
          update (i_cTable) set;
              CODSIGE = m.w_VALORE;
              &i_ccchkf. ;
           where;
              CODRISP = m.w_CODICERISP;
              and CODASSDOC = m.w_CODASSOLUT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        m.w_CODKEY = space(10)
      case m.w_CODKEY="codsicon"
        * --- Write into TMPINTSOS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[TMPINTSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMPINTSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODSICON = "+cp_NullLink(cp_ToStrODBC(m.w_VALORE),'TMPINTSOS','CODSICON');
              +i_ccchkf ;
          +" where ";
              +"CODRISP = "+cp_ToStrODBC(m.w_CODICERISP);
              +" and CODASSDOC = "+cp_ToStrODBC(m.w_CODASSOLUT);
                 )
        else
          update (i_cTable) set;
              CODSICON = m.w_VALORE;
              &i_ccchkf. ;
           where;
              CODRISP = m.w_CODICERISP;
              and CODASSDOC = m.w_CODASSOLUT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        m.w_CODKEY = space(10)
    endcase
    m.w_READKEYVALUE = .F.
  endif
endproc
  proc readesitosped_Try_03A1AB18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- Insert into TMPINTSOS
  i_nConn=i_TableProp[TMPINTSOS_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[TMPINTSOS_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_ccchkf=''
  i_ccchkv=''
  func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,TMPINTSOS_idx,i_nConn)
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                " ("+"CODRISP"+",CODASSDOC"+",DATACONSOS"+",IDLOTCONS"+",STATO_DOC"+",STATOSPED"+",DATAARCDOC"+i_ccchkf+") values ("+;
    cp_NullLink(cp_ToStrODBC(m.w_CODICERISP),'TMPINTSOS','CODRISP');
    +","+cp_NullLink(cp_ToStrODBC(m.w_CODASSOLUT),'TMPINTSOS','CODASSDOC');
    +","+cp_NullLink(cp_ToStrODBC(m.w_DATACONSOS),'TMPINTSOS','DATACONSOS');
    +","+cp_NullLink(cp_ToStrODBC(m.w_LOTTOID),'TMPINTSOS','IDLOTCONS');
    +","+cp_NullLink(cp_ToStrODBC(m.w_STATODOC),'TMPINTSOS','STATO_DOC');
    +","+cp_NullLink(cp_ToStrODBC(m.w_STATOSPED),'TMPINTSOS','STATOSPED');
    +","+cp_NullLink(cp_ToStrODBC(m.w_DATAARCDOC),'TMPINTSOS','DATAARCDOC');
         +i_ccchkv+")")
  else
    cp_CheckDeletedKey(i_cTable,0,'CODRISP',m.w_CODICERISP,'CODASSDOC',m.w_CODASSOLUT,'DATACONSOS',m.w_DATACONSOS,'IDLOTCONS',m.w_LOTTOID,'STATO_DOC',m.w_STATODOC,'STATOSPED',m.w_STATOSPED,'DATAARCDOC',m.w_DATAARCDOC)
    insert into (i_cTable) (CODRISP,CODASSDOC,DATACONSOS,IDLOTCONS,STATO_DOC,STATOSPED,DATAARCDOC &i_ccchkf. );
       values (;
         m.w_CODICERISP;
         ,m.w_CODASSOLUT;
         ,m.w_DATACONSOS;
         ,m.w_LOTTOID;
         ,m.w_STATODOC;
         ,m.w_STATOSPED;
         ,m.w_DATAARCDOC;
         &i_ccchkv. )
    i_Rows=iif(bTrsErr,0,1)
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if i_Rows<0 or bTrsErr
    * --- Error: insert not accepted
    i_Error=MSG_INSERT_ERROR
    return
  endif
    return


  function readesitosped_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='CLASMSOS'
    i_cWorkTables[2]='CLASDSOS'
    i_cWorkTables[3]='TMPINTSOS'
    return(cp_OpenFuncTables(3))
* --- END READESITOSPED
* --- START RICGUID
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ricguid                                                         *
*              Richiesta del GUID SOS                                          *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-28                                                      *
* Last revis.: 2009-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ricguid
param pWebSer

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODCON
  m.w_CODCON=space(10)
  private w_CODCLI
  m.w_CODCLI=space(10)
  private w_PASSWORD
  m.w_PASSWORD=space(20)
  private w_TICKET
  m.w_TICKET=space(254)
  private w_GUID
  m.w_GUID=space(36)
  private w_WORKSTA
  m.w_WORKSTA=space(15)
  private w_ERRORE
  m.w_ERRORE=space(1)
* --- WorkFile variables
  private PAR_SOST_idx
  PAR_SOST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ricguid"
if vartype(__ricguid_hook__)='O'
  __ricguid_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ricguid('+Transform(pWebSer)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ricguid')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ricguid_OpenTables()
  ricguid_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ricguid('+Transform(pWebSer)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ricguid')
Endif
*--- Activity log
if vartype(__ricguid_hook__)='O'
  __ricguid_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ricguid_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Questa funzione ritorna un identificativo GUID per il Web Services SOStitutiva
  m.w_ERRORE = "N"
  m.w_WORKSTA = substr(sys(0),1,at("#",sys(0))-1)
  * --- Leggo le informazioni necessarie alla richiesta del GUID dai parametri SOStitutiva
  * --- Read from PAR_SOST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PAR_SOST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PAR_SOST_idx,2],.t.,PAR_SOST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PSCODCON,PSCODCLI,PSPASWOR"+;
      " from "+i_cTable+" PAR_SOST where ";
          +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
          +" and PSCODUTE = "+cp_ToStrODBC(i_CODUTE);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PSCODCON,PSCODCLI,PSPASWOR;
      from (i_cTable) where;
          PSCODAZI = i_CODAZI;
          and PSCODUTE = i_CODUTE;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CODCON = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
    m.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
    m.w_PASSWORD = NVL(cp_ToDate(_read_.PSPASWOR),cp_NullValue(_read_.PSPASWOR))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if empty(m.w_CODCON) or empty(m.w_PASSWORD)
    * --- Read from PAR_SOST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[PAR_SOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[PAR_SOST_idx,2],.t.,PAR_SOST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PSCODCON,PSCODCLI,PSPASWOR"+;
        " from "+i_cTable+" PAR_SOST where ";
            +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and PSWORKST = "+cp_ToStrODBC(m.w_WORKSTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PSCODCON,PSCODCLI,PSPASWOR;
        from (i_cTable) where;
            PSCODAZI = i_CODAZI;
            and PSWORKST = m.w_WORKSTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_CODCON = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
      m.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
      m.w_PASSWORD = NVL(cp_ToDate(_read_.PSPASWOR),cp_NullValue(_read_.PSPASWOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(m.w_CODCON) or empty(m.w_PASSWORD)
      * --- Read from PAR_SOST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[PAR_SOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[PAR_SOST_idx,2],.t.,PAR_SOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PSCODCON,PSCODCLI,PSPASWOR"+;
          " from "+i_cTable+" PAR_SOST where ";
              +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PSCODCON,PSCODCLI,PSPASWOR;
          from (i_cTable) where;
              PSCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_CODCON = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
        m.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
        m.w_PASSWORD = NVL(cp_ToDate(_read_.PSPASWOR),cp_NullValue(_read_.PSPASWOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows = 0
        AH_ERRORMSG("Non sono stati definiti i parametri SOStitutiva, elaborazione interrotta.",48)
        i_retcode = 'stop'
        i_retval = " "
        return
      endif
    endif
  endif
  OldError = ON("ERROR")
  on error m.w_ERRORE="S"
  * --- Richiesta GUID
  m.w_TICKET = m.pWebSer.Singolo(m.w_CODCON,m.w_CODCLI,m.w_PASSWORD)
  XMLTOCURSOR(m.w_Ticket,"GUID")
  if ! used("GUID")
    m.w_ERRORE = "S"
  else
    Select GUID 
 Go Top
    m.w_GUID = guid
    USE IN SELECT("GUID")
  endif
  * --- Rirpistino il gestore degli errori a default
  On error &OldError
  if m.w_ERRORE = "S"
    Ah_ErrorMsg("Errore durante richiesta GUID al web services SOS",48," ")
    i_retcode = 'stop'
    i_retval = " "
    return
  else
    wait window m.w_GUID nowait
    i_retcode = 'stop'
    i_retval = m.w_GUID
    return
  endif
endproc


  function ricguid_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PAR_SOST'
    return(cp_OpenFuncTables(1))
* --- END RICGUID
* --- START CONWEBSER
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: conwebser                                                       *
*              Connessione a Web Services                                      *
*                                                                              *
*      Author: G. Bellani                                                      *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-23                                                      *
* Last revis.: 2009-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func conwebser
param pURL

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_URLWEBSER
  m.w_URLWEBSER=space(254)
  private w_ERRORE
  m.w_ERRORE=space(1)
  private w_WEBSERV
  m.w_WEBSERV = .null.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "conwebser"
if vartype(__conwebser_hook__)='O'
  __conwebser_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'conwebser('+Transform(pURL)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'conwebser')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
conwebser_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'conwebser('+Transform(pURL)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'conwebser')
Endif
*--- Activity log
if vartype(__conwebser_hook__)='O'
  __conwebser_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure conwebser_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione per collegarsi ad un Web Services, se specificato nel parametro, altrimenti si connette di defautl al Web Services di SOStitutiva
  m.w_ERRORE = "N"
  * --- Creo l'oggetto client SOAP
  m.w_WEBSERV = createobject("MSSOAP.SoapClient30")
  if vartype(m.pURL) = "C"
    m.w_URLWEBSER = alltrim(m.pURL)
  else
    m.w_URLWEBSER = "http://sostitutiva.zucchetticom.test.zucchetti.com/WSSOStitutiva/ws_as_servizio.asmx?WSDL"
  endif
  OldError = ON("ERROR")
  on error m.w_ERRORE="S"
  * --- Mi collego al Web Services
  w_WEBSERV.MSSoapInit(m.w_URLWEBSER)
  * --- Rirpistino il gestore degli errori a default
  On error &OldError
  if m.w_ERRORE = "S"
    Ah_ErrorMsg("Errore durante connessione al web services: %1",48," ",m.w_URLWEBSER)
    i_retcode = 'stop'
    i_retval = .null.
    return
  else
    * --- Ritorna l'oggetto client SOAP
    i_retcode = 'stop'
    i_retval = m.w_WEBSERV
    return
  endif
endproc


* --- END CONWEBSER
* --- START DEL_PRDO
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: del_prdo                                                        *
*              Elimina processo documentale                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-03-17                                                      *
* Last revis.: 2016-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func del_prdo
param pPDCODPRO,pPDCLADOC

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PDCODPRO
  m.w_PDCODPRO=space(15)
  private w_RES
  m.w_RES=space(0)
* --- WorkFile variables
  private PROMDOCU_idx
  PROMDOCU_idx=0
  private PRODDOCU_idx
  PRODDOCU_idx=0
  private PRO_FCPZ_idx
  PRO_FCPZ_idx=0
  private PRO_PCPZ_idx
  PRO_PCPZ_idx=0
  private PROPPERM_idx
  PROPPERM_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 2
  private i_func_name
  i_func_name = "del_prdo"
if vartype(__del_prdo_hook__)='O'
  __del_prdo_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'del_prdo('+Transform(pPDCODPRO)+','+Transform(pPDCLADOC)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'del_prdo')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if del_prdo_OpenTables()
  del_prdo_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_PROMDOCU')
  use in _Curs_PROMDOCU
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'del_prdo('+Transform(pPDCODPRO)+','+Transform(pPDCLADOC)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'del_prdo')
Endif
*--- Activity log
if vartype(__del_prdo_hook__)='O'
  __del_prdo_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure del_prdo_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Elimina processo documentale
  if Empty(m.pPDCODPRO)
    if not Empty(m.pPDCLADOC)
      * --- Select from PROMDOCU
      i_nConn=i_TableProp[PROMDOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[PROMDOCU_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PDCODPRO from "+i_cTable+" PROMDOCU ";
            +" where PDCLADOC="+cp_ToStrODBC(m.pPDCLADOC)+"";
             ,"_Curs_PROMDOCU")
      else
        select PDCODPRO from (i_cTable);
         where PDCLADOC=m.pPDCLADOC;
          into cursor _Curs_PROMDOCU
      endif
      if used('_Curs_PROMDOCU')
        select _Curs_PROMDOCU
        locate for 1=1
        do while not(eof())
        m.w_PDCODPRO = _Curs_PROMDOCU.PDCODPRO
        del_prdo_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if not Empty(m.w_RES)
          exit
        endif
          select _Curs_PROMDOCU
          continue
        enddo
        use
      endif
    endif
  else
    m.w_PDCODPRO = m.pPDCODPRO
    del_prdo_Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RES
  return
endproc


procedure del_prdo_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Try
  del_prdo_Try_029EA5B0()
  * --- Catch
  if !empty(i_Error)
    i_ErrMsg=i_Error
    i_Error=''
    m.w_RES = message()
  endif
  * --- End
endproc
  proc del_prdo_Try_029EA5B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- Delete from PRODDOCU
  i_nConn=i_TableProp[PRODDOCU_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PRODDOCU_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
          +"PDCODPRO = "+cp_ToStrODBC(m.w_PDCODPRO);
           )
  else
    delete from (i_cTable) where;
          PDCODPRO = m.w_PDCODPRO;

    i_Rows=_tally
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if bTrsErr
    * --- Error: delete not accepted
    i_Error=MSG_DELETE_ERROR
    return
  endif
  * --- Delete from PRO_FCPZ
  i_nConn=i_TableProp[PRO_FCPZ_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PRO_FCPZ_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
          +"PFCODICE = "+cp_ToStrODBC(m.w_PDCODPRO);
           )
  else
    delete from (i_cTable) where;
          PFCODICE = m.w_PDCODPRO;

    i_Rows=_tally
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if bTrsErr
    * --- Error: delete not accepted
    i_Error=MSG_DELETE_ERROR
    return
  endif
  * --- Delete from PRO_PCPZ
  i_nConn=i_TableProp[PRO_PCPZ_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PRO_PCPZ_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
          +"DECODICE = "+cp_ToStrODBC(m.w_PDCODPRO);
           )
  else
    delete from (i_cTable) where;
          DECODICE = m.w_PDCODPRO;

    i_Rows=_tally
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if bTrsErr
    * --- Error: delete not accepted
    i_Error=MSG_DELETE_ERROR
    return
  endif
  * --- Delete from PROPPERM
  i_nConn=i_TableProp[PROPPERM_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PROPPERM_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
          +"PECODPRO = "+cp_ToStrODBC(m.w_PDCODPRO);
           )
  else
    delete from (i_cTable) where;
          PECODPRO = m.w_PDCODPRO;

    i_Rows=_tally
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if bTrsErr
    * --- Error: delete not accepted
    i_Error=MSG_DELETE_ERROR
    return
  endif
  * --- Delete from PROMDOCU
  i_nConn=i_TableProp[PROMDOCU_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PROMDOCU_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
          +"PDCODPRO = "+cp_ToStrODBC(m.w_PDCODPRO);
           )
  else
    delete from (i_cTable) where;
          PDCODPRO = m.w_PDCODPRO;

    i_Rows=_tally
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if bTrsErr
    * --- Error: delete not accepted
    i_Error=MSG_DELETE_ERROR
    return
  endif
    return


  function del_prdo_OpenTables()
    dimension i_cWorkTables[max(1,5)]
    i_cWorkTables[1]='PROMDOCU'
    i_cWorkTables[2]='PRODDOCU'
    i_cWorkTables[3]='PRO_FCPZ'
    i_cWorkTables[4]='PRO_PCPZ'
    i_cWorkTables[5]='PROPPERM'
    return(cp_OpenFuncTables(5))
* --- END DEL_PRDO
