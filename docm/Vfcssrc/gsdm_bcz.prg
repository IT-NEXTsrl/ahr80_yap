* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bcz                                                        *
*              Crea archivio zip                                               *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-03                                                      *
* Last revis.: 2016-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pGEST
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bcz",oParentObject,m.pGEST)
return(i_retval)

define class tgsdm_bcz as StdBatch
  * --- Local variables
  pGEST = space(3)
  w_MASK = .NULL.
  w_IDCLADOC = space(15)
  w_CURSORE = space(10)
  w_PATHINVIO = space(254)
  w_IDSERIAL = space(20)
  w_RIGASEQUENZE = space(254)
  w_TIPOATT = space(8)
  w_INDICEOK = .f.
  w_VALORE = space(50)
  w_FINERIGA = space(15)
  w_LOGERRORI = .NULL.
  w_ERRORI = .f.
  w_SEQFILE = space(254)
  w_NOMEALLE = space(150)
  w_PATHALLE = space(150)
  w_ORIGALLE = space(254)
  w_DESTALLE = space(254)
  w_IDCODATT = space(15)
  w_HASH = space(160)
  w_ZIPCOMM = space(254)
  w_CODCONS = space(10)
  w_CODCLI = space(10)
  w_WORKSTAT = space(15)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_DATFORM = space(8)
  w_DOCUMENTO = space(250)
  w_CODSPED = space(100)
  w_ZIPORIGINE = space(254)
  w_ZIPDESTINAZ = space(254)
  w_NODOCUM = space(1)
  w_OPERAZIONE = space(30)
  w_NOMEMETO = space(30)
  w_LENAARR = 0
  w_MASK1 = .NULL.
  w_CODABS = space(10)
  w_WS = .NULL.
  w_GUID = space(36)
  w_RECUDOC = space(0)
  w_FILEREC = space(254)
  * --- WorkFile variables
  CONTROPA_idx=0
  PRODINDI_idx=0
  PAR_SOST_idx=0
  PROMINDI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione archivio Zip e file Evidenze.seq a partire dagli indici selezionati in GSDM_KII
    this.w_LOGERRORI=createobject("AH_ErrorLog")
    this.w_ERRORI = .F.
    this.w_MASK = this.oParentObject
    this.w_CURSORE = this.w_MASK.w_ZoomGF.cCursor
    this.w_RIGASEQUENZE = ""
    this.w_IDCLADOC = this.oParentObject.w_SELCODCLA
    this.w_FINERIGA = CHR(13)+CHR(10)
    curdir=Sys(5)+Sys(2003)
    * --- Leggo il codice conservatore ed il codice cliente in base alla seguente priorit� di ricerca
    *     ed  il path in cui devo copiare gli allegati, creare lo zip e il file Evidenze.seq
    this.w_WORKSTAT = substr(sys(0),1,at("#",sys(0))-1)
    * --- Priorit� di ricerca:
    *      1. Per codice azienda e codice utente
    *      2. Per codice azienda e codice workstation
    *      3. Per codice azienda
    *     Ovviamente appena � trovato un Web services le combinazioni rimanenti vengono scartate.
    * --- Read from PAR_SOST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_SOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG,PSPATSEQ"+;
        " from "+i_cTable+" PAR_SOST where ";
            +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and PSCODUTE = "+cp_ToStrODBC(i_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG,PSPATSEQ;
        from (i_cTable) where;
            PSCODAZI = i_CODAZI;
            and PSCODUTE = i_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
      this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
      this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
      w_CODSOG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
      this.w_PATHINVIO = NVL(cp_ToDate(_read_.PSPATSEQ),cp_NullValue(_read_.PSPATSEQ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(w_URL) or (empty(this.w_CODCONS) and vartype(pTipoOp)="C")
      * --- Read from PAR_SOST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_SOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG,PSPATSEQ"+;
          " from "+i_cTable+" PAR_SOST where ";
              +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and PSWORKST = "+cp_ToStrODBC(this.w_WORKSTAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG,PSPATSEQ;
          from (i_cTable) where;
              PSCODAZI = i_CODAZI;
              and PSWORKST = this.w_WORKSTAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
        this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
        this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
        w_CODSOG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
        this.w_PATHINVIO = NVL(cp_ToDate(_read_.PSPATSEQ),cp_NullValue(_read_.PSPATSEQ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(w_URL) or (empty(this.w_CODCONS) and vartype(pTipoOp)="C")
        * --- Read from PAR_SOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_SOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG,PSPATSEQ"+;
            " from "+i_cTable+" PAR_SOST where ";
                +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and PSCODUTE = "+cp_ToStrODBC(0);
                +" and PSWORKST = "+cp_ToStrODBC(space(15));
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG,PSPATSEQ;
            from (i_cTable) where;
                PSCODAZI = i_CODAZI;
                and PSCODUTE = 0;
                and PSWORKST = space(15);
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
          this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
          this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
          w_CODSOG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
          this.w_PATHINVIO = NVL(cp_ToDate(_read_.PSPATSEQ),cp_NullValue(_read_.PSPATSEQ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0
          AH_ERRORMSG("Non sono stati definiti i parametri SOStitutiva, elaborazione interrotta.",48)
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Richiesta selzione operazione da eseguire
    if this.pGEST = "SEQ"
      Select(this.w_CURSORE)
      Locate for XCHK=1
      if ! found() and this.pGEST = "SEQ"
         ah_errormsg("Selezionare almeno un indice da inviare",48)
        i_retcode = 'stop'
        return
      endif
      this.w_OPERAZIONE =  "Spedizione documento"
    else
      this.w_NOMEMETO = space(30)
      vx_exec("..\DOCM\EXE\QUERY\GSDM_BCZ.vzm",this)
      if ! empty(nvl(this.w_NOMEMETO," "))
        Select(this.w_CURSORE)
        Locate for XCHK=1
        if !found() and this.w_OPERAZIONE="Recupero documento"
           ah_errormsg("Selezionare almeno un indice di cui recuperare il documento",48)
          i_retcode = 'stop'
          return
        endif
        this.w_OPERAZIONE = alltrim(this.w_NOMEMETO)
      else
        i_retcode = 'stop'
        return
      endif
    endif
    do case
      case this.w_OPERAZIONE = "Spedizione documento"
        if empty(this.w_PATHINVIO)
           ah_errormsg("Inserire il path da utilizzare per l'invio in 'Gestione percorsi'",48)
          i_retcode = 'stop'
          return
        endif
        this.w_PATHINVIO = addbs(this.w_PATHINVIO)+alltrim(this.oParentObject.w_CODCLASOS)+"\"
        if ! Directory(this.w_PATHINVIO)
          * --- Creo la cartella di creazione dello zip
           
 cOlderr=ON("error") 
 cMsgErr="" 
 On error cMsgErr=Message(1)+": "+MESSAGE()
          MD (this.w_PATHINVIO)
          ON ERROR &cOldErr
          if ! empty(cMsgErr)
            ah_ErrorMsg(cMsgErr,"stop","")
            i_retcode = 'stop'
            return
          endif
        else
          * --- Cancellazione di tutti i files contenuti nella cartella di invio
          Ah_ErrorMsg("Attenzione, il contenuto della cartella %1, sar� cancellato.%0Provvedere a spostare i files da inviare a SOS",64,"",this.w_PATHINVIO)
          Cd (this.w_PATHINVIO)
          Delete file *.*
          Cd (curdir)
        endif
        this.w_SEQFILE = alltrim(this.w_PATHINVIO)+"evidenze.seq"
        * --- Calcolo il codice della spedizione attuale
        this.w_DATFORM = CHRTRAN(alltrim(DTOC(DATE())),"/:-*.","")
        this.w_DATFORM = RIGHT(this.w_DATFORM,4)+SUBSTR(this.w_DATFORM,3,2)+LEFT(this.w_DATFORM,2)
        this.w_CODSPED = JustStem(alltrim(this.w_PATHINVIO)+alltrim(this.oParentObject.w_CODCLASOS)+"@"+this.w_DATFORM+"@"+CHRTRAN(time(),"/:","")+"@"+alltrim(this.w_CODCONS)+"@"+alltrim(this.w_CODCLI)+".zip")
        * --- Mi creo il cursore contenente la lista degli attributi liberi definiti nella classe documentale SOS
        Select(this.w_CURSORE)
        Go Top
        Scan for XCHK = 1 and CHKATT="OK"
        this.w_RIGASEQUENZE = ""
        * --- Valorizzazione degli attributi per il file Evidenze.seq e copia allegato
        this.w_IDSERIAL = GFKEYINDIC
        this.w_INDICEOK = .T.
        if not empty(nvl(IDDOCFIR," "))
          * --- Prendo il documento firmato
          this.w_NOMEALLE = alltrim(nvl(IDDOCFIR," "))
        else
          this.w_NOMEALLE = alltrim(nvl(GFNOMEFILE," "))
        endif
        do case
          case IDMODALL = "F"
            this.w_PATHALLE = DCMPATAR(CDPATSTD, CDTIPRAG, IDDATRAG,this.oParentObject.w_SELCODCLA)
          case IDMODALL = "L"
            this.w_PATHALLE = JUSTPATH(IDORIFIL)
        endcase
        this.w_DOCUMENTO = addbs(this.w_PATHALLE)+this.w_NOMEALLE
        if ! cp_fileexist(this.w_DOCUMENTO)
          * --- File allegato non presente escludo il documento dal file .seq
          this.w_INDICEOK = .F.
          this.w_NODOCUM = "S"
        else
          this.w_NODOCUM = "N"
          * --- Select from gsdm_bcz
          do vq_exec with 'gsdm_bcz',this,'_Curs_gsdm_bcz','',.f.,.t.
          if used('_Curs_gsdm_bcz')
            select _Curs_gsdm_bcz
            locate for 1=1
            do while not(eof())
            do case
              case alltrim(_Curs_gsdm_bcz.SZCODKEY) == "NomeFile"
                this.w_VALORE = this.w_NOMEALLE
                * --- Read from PRODINDI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRODINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CPROWNUM"+;
                    " from "+i_cTable+" PRODINDI where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                        +" and IDCODATT = "+cp_ToStrODBC("NomeFile");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CPROWNUM;
                    from (i_cTable) where;
                        IDSERIAL = this.w_IDSERIAL;
                        and IDCODATT = "NomeFile";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if i_Rows = 0
                  * --- Attributo non presente, lo devo inserire per farlo devo prendere il primo valore utile per la chiave di riga
                  * --- Select from PRODINDI
                  i_nConn=i_TableProp[this.PRODINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select Max(CPROWNUM) as MAXNUMROW  from "+i_cTable+" PRODINDI ";
                        +" where IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)+"";
                         ,"_Curs_PRODINDI")
                  else
                    select Max(CPROWNUM) as MAXNUMROW from (i_cTable);
                     where IDSERIAL=this.w_IDSERIAL;
                      into cursor _Curs_PRODINDI
                  endif
                  if used('_Curs_PRODINDI')
                    select _Curs_PRODINDI
                    locate for 1=1
                    do while not(eof())
                    this.w_ROWNUM = _Curs_PRODINDI.MAXNUMROW
                      select _Curs_PRODINDI
                      continue
                    enddo
                    use
                  endif
                  this.w_ROWNUM = this.w_ROWNUM + 1
                  this.w_ROWORD = this.w_ROWNUM * 10
                  * --- Inserisco l'attributo nell'indice
                  * --- Insert into PRODINDI
                  i_nConn=i_TableProp[this.PRODINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDTIPATT"+",IDVALATT"+",IDDESATT"+",IDCHKOBB"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PRODINDI','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PRODINDI','CPROWORD');
                    +","+cp_NullLink(cp_ToStrODBC("NomeFile"),'PRODINDI','IDCODATT');
                    +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'PRODINDI','IDVALATT');
                    +","+cp_NullLink(cp_ToStrODBC("Nome file allegato"),'PRODINDI','IDDESATT');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'IDCODATT',"NomeFile",'IDTIPATT',"C",'IDVALATT',this.w_VALORE,'IDDESATT',"Nome file allegato",'IDCHKOBB',"N")
                    insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDTIPATT,IDVALATT,IDDESATT,IDCHKOBB &i_ccchkf. );
                       values (;
                         this.w_IDSERIAL;
                         ,this.w_ROWNUM;
                         ,this.w_ROWORD;
                         ,"NomeFile";
                         ,"C";
                         ,this.w_VALORE;
                         ,"Nome file allegato";
                         ,"N";
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              case alltrim(_Curs_gsdm_bcz.SZCODKEY) == "Hash"
                SET LIBRARY TO LOCFILE("SHA1.fll") ADDITIVE
                this.w_HASH = upper(HASH_FILE(this.w_DOCUMENTO))
                RELEASE LIBRARY SHA1.fll
                this.w_VALORE = this.w_HASH
                * --- Read from PRODINDI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRODINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CPROWNUM,CPROWORD"+;
                    " from "+i_cTable+" PRODINDI where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                        +" and IDCODATT = "+cp_ToStrODBC("Hash");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CPROWNUM,CPROWORD;
                    from (i_cTable) where;
                        IDSERIAL = this.w_IDSERIAL;
                        and IDCODATT = "Hash";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  this.w_ROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if i_Rows = 0
                  * --- Attributo non presente, lo devo inserire per farlo devo prendere il primo valore utile per la chiave di riga
                  * --- Select from PRODINDI
                  i_nConn=i_TableProp[this.PRODINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select Max(CPROWNUM) as MAXNUMROW  from "+i_cTable+" PRODINDI ";
                        +" where IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)+"";
                         ,"_Curs_PRODINDI")
                  else
                    select Max(CPROWNUM) as MAXNUMROW from (i_cTable);
                     where IDSERIAL=this.w_IDSERIAL;
                      into cursor _Curs_PRODINDI
                  endif
                  if used('_Curs_PRODINDI')
                    select _Curs_PRODINDI
                    locate for 1=1
                    do while not(eof())
                    this.w_ROWNUM = _Curs_PRODINDI.MAXNUMROW
                      select _Curs_PRODINDI
                      continue
                    enddo
                    use
                  endif
                  this.w_ROWNUM = this.w_ROWNUM + 1
                  this.w_ROWORD = this.w_ROWNUM * 10
                  * --- Inserisco l'attributo nell'indice
                  * --- Insert into PRODINDI
                  i_nConn=i_TableProp[this.PRODINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDTIPATT"+",IDVALATT"+",IDDESATT"+",IDCHKOBB"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PRODINDI','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PRODINDI','CPROWORD');
                    +","+cp_NullLink(cp_ToStrODBC("Hash"),'PRODINDI','IDCODATT');
                    +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'PRODINDI','IDVALATT');
                    +","+cp_NullLink(cp_ToStrODBC("Codifica SHA1 del documento allegato"),'PRODINDI','IDDESATT');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'IDCODATT',"Hash",'IDTIPATT',"C",'IDVALATT',this.w_VALORE,'IDDESATT',"Codifica SHA1 del documento allegato",'IDCHKOBB',"N")
                    insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDTIPATT,IDVALATT,IDDESATT,IDCHKOBB &i_ccchkf. );
                       values (;
                         this.w_IDSERIAL;
                         ,this.w_ROWNUM;
                         ,this.w_ROWORD;
                         ,"Hash";
                         ,"C";
                         ,this.w_VALORE;
                         ,"Codifica SHA1 del documento allegato";
                         ,"N";
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                else
                  * --- Write into PRODINDI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRODINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'PRODINDI','IDVALATT');
                        +i_ccchkf ;
                    +" where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                        +" and CPROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                           )
                  else
                    update (i_cTable) set;
                        IDVALATT = this.w_VALORE;
                        &i_ccchkf. ;
                     where;
                        IDSERIAL = this.w_IDSERIAL;
                        and CPROWNUM = this.w_ROWNUM;
                        and CPROWORD = this.w_ROWORD;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              otherwise
                if isnull(_Curs_gsdm_bcz.IDVALATT)
                  this.w_INDICEOK = .F.
                  exit
                endif
                this.w_TIPOATT = alltrim(_Curs_gsdm_bcz.SZTIPKEY)
                this.w_VALORE = _Curs_gsdm_bcz.IDVALATT
            endcase
            this.w_RIGASEQUENZE = this.w_RIGASEQUENZE + alltrim(this.w_VALORE)+";"
              select _Curs_gsdm_bcz
              continue
            enddo
            use
          endif
        endif
        Select(this.w_CURSORE)
        if this.w_INDICEOK
          this.w_RIGASEQUENZE = left(alltrim(this.w_RIGASEQUENZE),len(alltrim(this.w_RIGASEQUENZE))-1)+this.w_FINERIGA
          StrToFile(this.w_RIGASEQUENZE,this.w_SEQFILE,.T.)
          * --- Copia allegato nella cartella di invio a SOStitutiva
          this.w_ORIGALLE = addbs(this.w_PATHALLE)+alltrim(this.w_NOMEALLE)
          this.w_DESTALLE = this.w_PATHINVIO+alltrim(this.w_NOMEALLE)
          if cp_fileexist(this.w_ORIGALLE,.t.)
            copy file (this.w_ORIGALLE) to (this.w_DESTALLE)
          endif
          * --- Inserisco e valorizzo l'attributo 'Codice spedizione' per l'indice attuale
          * --- Read from PRODINDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM,CPROWORD"+;
              " from "+i_cTable+" PRODINDI where ";
                  +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                  +" and IDCODATT = "+cp_ToStrODBC("Cod_Sped");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM,CPROWORD;
              from (i_cTable) where;
                  IDSERIAL = this.w_IDSERIAL;
                  and IDCODATT = "Cod_Sped";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            this.w_ROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows = 0
            * --- Attributo non presente, lo devo inserire per farlo devo prendere il primo valore utile per la chiave di riga
            * --- Select from PRODINDI
            i_nConn=i_TableProp[this.PRODINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select Max(CPROWNUM) as MAXNUMROW  from "+i_cTable+" PRODINDI ";
                  +" where IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)+"";
                   ,"_Curs_PRODINDI")
            else
              select Max(CPROWNUM) as MAXNUMROW from (i_cTable);
               where IDSERIAL=this.w_IDSERIAL;
                into cursor _Curs_PRODINDI
            endif
            if used('_Curs_PRODINDI')
              select _Curs_PRODINDI
              locate for 1=1
              do while not(eof())
              this.w_ROWNUM = _Curs_PRODINDI.MAXNUMROW
                select _Curs_PRODINDI
                continue
              enddo
              use
            endif
            Select(this.w_CURSORE)
            this.w_ROWNUM = this.w_ROWNUM + 1
            this.w_ROWORD = this.w_ROWNUM * 10
            * --- Inserisco l'attributo nell'indice
            * --- Insert into PRODINDI
            i_nConn=i_TableProp[this.PRODINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDTIPATT"+",IDVALATT"+",IDDESATT"+",IDCHKOBB"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PRODINDI','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PRODINDI','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC("Cod_Sped"),'PRODINDI','IDCODATT');
              +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODSPED),'PRODINDI','IDVALATT');
              +","+cp_NullLink(cp_ToStrODBC("Codice della spedizione SOS"),'PRODINDI','IDDESATT');
              +","+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'IDCODATT',"Cod_Sped",'IDTIPATT',"C",'IDVALATT',this.w_CODSPED,'IDDESATT',"Codice della spedizione SOS",'IDCHKOBB',"N")
              insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDTIPATT,IDVALATT,IDDESATT,IDCHKOBB &i_ccchkf. );
                 values (;
                   this.w_IDSERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_ROWORD;
                   ,"Cod_Sped";
                   ,"C";
                   ,this.w_CODSPED;
                   ,"Codice della spedizione SOS";
                   ,"N";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Write into PRODINDI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PRODINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(this.w_CODSPED),'PRODINDI','IDVALATT');
                  +i_ccchkf ;
              +" where ";
                  +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and CPROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                     )
            else
              update (i_cTable) set;
                  IDVALATT = this.w_CODSPED;
                  &i_ccchkf. ;
               where;
                  IDSERIAL = this.w_IDSERIAL;
                  and CPROWNUM = this.w_ROWNUM;
                  and CPROWORD = this.w_ROWORD;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Aggiorno lo stato interno dell'indice:
          *       Da inviare -> Inviato
          *     Anche se l'effettivo invio avviene in modalit� asincrona tramite il client SOS, la marcatura garantisce di non inserire gli stessi indici in pi� zip diversi.
          *     Sar� consentito l'inserimento di un indice con stato 'Inviato' solo dietro diretta richiesta dell'utente.
          * --- Write into PROMINDI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDSTASOS ="+cp_NullLink(cp_ToStrODBC("INVIATO"),'PROMINDI','IDSTASOS');
                +i_ccchkf ;
            +" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                   )
          else
            update (i_cTable) set;
                IDSTASOS = "INVIATO";
                &i_ccchkf. ;
             where;
                IDSERIAL = this.w_IDSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Gestione log errori
          if this.w_NODOCUM = "N"
            this.w_LOGERRORI.AddMsgLog("Indice %1 scartato perch� mancano i dati relative ad alcuni attributi obbligatori. Controllare la classe documentale di appartenenza",this.w_IDSERIAL)     
          else
            this.w_LOGERRORI.AddMsgLog("Indice %1 scartato perch� non presente il documento allegato %2.",this.w_IDSERIAL,this.w_DOCUMENTO)     
          endif
          this.w_ERRORI = .T.
        endif
        Select(this.w_CURSORE)
        Endscan
        if this.w_ERRORI 
          this.w_LOGERRORI.PrintLog(this.oParentObject,"Elenco indici scartati")     
          Cd (this.w_PATHINVIO)
          this.w_LENAARR = ADIR(Evidenza,"*.seq")
          Cd (curdir)
          if this.w_LENAARR>0 and Alen(Evidenza,1) < 1 
            this.oParentObject.w_SELEZIONE = "D"
            this.w_MASK.SetControlsValue()     
            this.w_MASK.NotifyEvent("w_SELEZIONE Changed")     
            i_retcode = 'stop'
            return
          endif
        endif
        if this.oParentObject.w_CREAZIP="S"
          ZIPUNZIP("Z",Addbs(this.w_PATHINVIO)+Alltrim(this.w_CODSPED)+".zip",Addbs(this.w_PATHINVIO),"","","7zzip")
        endif
        Ah_ErrorMsg("Creazione files ultimata",64,"")
      case this.w_OPERAZIONE = "Variazioni da ultima interrogazione"
        this.w_MASK1 = GSDM_KES()
        this.w_MASK1.opgfrm.page1.oPag.oBtn_1_1.Click()     
        this.w_MASK1.EcpQuit()     
        this.w_MASK1 = .null.
      case this.w_OPERAZIONE = "Recupero documento"
        Select(this.w_CURSORE)
        Go Top
        Scan for XCHK = 1 and CHKATT="OK"
        this.w_IDSERIAL = GFKEYINDIC
        * --- Read from PRODINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IDVALATT"+;
            " from "+i_cTable+" PRODINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                +" and IDCODATT = "+cp_ToStrODBC("CodAssDoc");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IDVALATT;
            from (i_cTable) where;
                IDSERIAL = this.w_IDSERIAL;
                and IDCODATT = "CodAssDoc";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODABS = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Recupero il path di archiviazione ed  il nome del file archiviato
        if ! empty(this.w_CODABS)
          if not empty(nvl(IDDOCFIR," "))
            * --- Prendo il documento firmato
            this.w_NOMEALLE = alltrim(nvl(IDDOCFIR," "))
          else
            this.w_NOMEALLE = alltrim(nvl(GFNOMEFILE," "))
          endif
          do case
            case IDMODALL = "F"
              this.w_PATHALLE = DCMPATAR(CDPATSTD, CDTIPRAG, IDDATRAG)
            case IDMODALL = "L"
              this.w_PATHALLE = JUSTPATH(IDORIFIL)
          endcase
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        Endscan
        Ah_ErrorMsg("Recupero files ultimato",64,"")
      otherwise
        Ah_ErrorMsg("Operazione non gestita",64,"")
    endcase
    this.oParentObject.w_SELEZIONE = "D"
    this.w_MASK.SetControlsValue()     
    this.w_MASK.NotifyEvent("w_SELEZIONE Changed")     
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Recupero documento da codice assoluto
    this.w_WS = Conwebser(w_URL)
    if isnull(this.w_WS)
      i_retcode = 'stop'
      return
    endif
    * --- Recupero l'identificativo GUID
    this.w_GUID = RICGUID(this.w_WS)
    * --- Try
    local bErr_0403A238
    bErr_0403A238=bTrsErr
    this.Try_0403A238()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
    endif
    bTrsErr=bTrsErr or bErr_0403A238
    * --- End
  endproc
  proc Try_0403A238()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if empty(this.w_GUID)
      * --- Raise
      i_Error="GUID non valido"
      return
    endif
    * --- Recupero il singolo documento dal suo codice assoluto
    this.w_RECUDOC = this.w_WS.recuperodocumentocodiceassoluto(this.w_CODCONS,w_CODSOG,this.w_GUID,this.w_CODABS,this.oParentObject.w_CODCLASOS)
    this.w_FILEREC = addbs(this.w_PATHALLE)+JUSTSTEM(this.w_NOMEALLE)+"_RecSOS"+"."+JUSTEXT(this.w_NOMEALLE)
    * --- Converto la stringa da Base64Binary in ASCII e la salva in un file con estensione .pdf
    STRTOFILE(STRCONV(this.w_RECUDOC,14),this.w_FILEREC)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pGEST)
    this.pGEST=pGEST
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='PRODINDI'
    this.cWorkTables[3]='PAR_SOST'
    this.cWorkTables[4]='PROMINDI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_gsdm_bcz')
      use in _Curs_gsdm_bcz
    endif
    if used('_Curs_PRODINDI')
      use in _Curs_PRODINDI
    endif
    if used('_Curs_PRODINDI')
      use in _Curs_PRODINDI
    endif
    if used('_Curs_PRODINDI')
      use in _Curs_PRODINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pGEST"
endproc
