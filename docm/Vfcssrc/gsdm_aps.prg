* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_aps                                                        *
*              Parametri SOStitutiva                                           *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-21                                                      *
* Last revis.: 2011-04-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_aps"))

* --- Class definition
define class tgsdm_aps as StdForm
  Top    = 10
  Left   = 11

  * --- Standard Properties
  Width  = 664
  Height = 333+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-04-22"
  HelpContextID=160023657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  PAR_SOST_IDX = 0
  contropa_IDX = 0
  CONTROPA_IDX = 0
  cFile = "PAR_SOST"
  cKeySelect = "PSCODAZI,PSSERIAL"
  cKeyWhere  = "PSCODAZI=this.w_PSCODAZI and PSSERIAL=this.w_PSSERIAL"
  cKeyWhereODBC = '"PSCODAZI="+cp_ToStrODBC(this.w_PSCODAZI)';
      +'+" and PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cKeyWhereODBCqualified = '"PAR_SOST.PSCODAZI="+cp_ToStrODBC(this.w_PSCODAZI)';
      +'+" and PAR_SOST.PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cPrg = "gsdm_aps"
  cComment = "Parametri SOStitutiva"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PSVALIDT = space(1)
  o_PSVALIDT = space(1)
  w_PSCODAZI = space(5)
  w_PSWORKST = space(15)
  w_PSSERIAL = space(8)
  w_PSCODCON = space(10)
  w_PSCODCLI = space(10)
  w_PSCODSOG = space(5)
  w_PSPASWOR = space(20)
  w_PATSOS = space(254)
  w_PSPATSEQ = space(254)
  o_PSPATSEQ = space(254)
  w_PSINFWEB = space(0)
  w_PSATTSTA = space(15)
  w_PSMODFIR = space(1)
  w_PSCODUTE = 0
  w_LPATH = .F.

  * --- Autonumbered Variables
  op_PSSERIAL = this.W_PSSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR_SOST','gsdm_aps')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_apsPag1","gsdm_aps",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("SOStitutiva")
      .Pages(1).HelpContextID = 25578794
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPSCODAZI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='contropa'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='PAR_SOST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_SOST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_SOST_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PSCODAZI = NVL(PSCODAZI,space(5))
      .w_PSSERIAL = NVL(PSSERIAL,space(8))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_SOST where PSCODAZI=KeySet.PSCODAZI
    *                            and PSSERIAL=KeySet.PSSERIAL
    *
    i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_SOST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_SOST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_SOST '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PSCODAZI',this.w_PSCODAZI  ,'PSSERIAL',this.w_PSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PATSOS = space(254)
        .w_LPATH = .f.
        .w_PSVALIDT = NVL(PSVALIDT,space(1))
        .w_PSCODAZI = NVL(PSCODAZI,space(5))
          if link_1_2_joined
            this.w_PSCODAZI = NVL(COCODAZI102,NVL(this.w_PSCODAZI,space(5)))
            this.w_PATSOS = NVL(COPATSOS102,space(254))
          else
          .link_1_2('Load')
          endif
        .w_PSWORKST = NVL(PSWORKST,space(15))
        .w_PSSERIAL = NVL(PSSERIAL,space(8))
        .op_PSSERIAL = .w_PSSERIAL
        .w_PSCODCON = NVL(PSCODCON,space(10))
        .w_PSCODCLI = NVL(PSCODCLI,space(10))
        .w_PSCODSOG = NVL(PSCODSOG,space(5))
        .w_PSPASWOR = NVL(PSPASWOR,space(20))
        .w_PSPATSEQ = NVL(PSPATSEQ,space(254))
        .w_PSINFWEB = NVL(PSINFWEB,space(0))
        .w_PSATTSTA = NVL(PSATTSTA,space(15))
        .w_PSMODFIR = NVL(PSMODFIR,space(1))
        .w_PSCODUTE = NVL(PSCODUTE,0)
        cp_LoadRecExtFlds(this,'PAR_SOST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PSVALIDT = space(1)
      .w_PSCODAZI = space(5)
      .w_PSWORKST = space(15)
      .w_PSSERIAL = space(8)
      .w_PSCODCON = space(10)
      .w_PSCODCLI = space(10)
      .w_PSCODSOG = space(5)
      .w_PSPASWOR = space(20)
      .w_PATSOS = space(254)
      .w_PSPATSEQ = space(254)
      .w_PSINFWEB = space(0)
      .w_PSATTSTA = space(15)
      .w_PSMODFIR = space(1)
      .w_PSCODUTE = 0
      .w_LPATH = .f.
      if .cFunction<>"Filter"
        .w_PSVALIDT = 'A'
        .w_PSCODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_PSCODAZI))
          .link_1_2('Full')
          endif
        .w_PSWORKST = iif(.w_PSVALIDT='W',SUBSTR(SYS(0),1,AT('#',SYS(0))-1),space(15))
          .DoRTCalc(4,9,.f.)
        .w_PSPATSEQ = .w_PATSOS
          .DoRTCalc(11,11,.f.)
        .w_PSATTSTA = 'STATO_DOC'
        .w_PSMODFIR = 'N'
        .w_PSCODUTE = iif(.w_PSVALIDT='U',i_CODUTE,0)
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_SOST')
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SOSAZI","w_PSSERIAL")
      .op_PSSERIAL = .w_PSSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPSCODAZI_1_2.enabled = !i_bVal
      .Page1.oPag.oPSVALIDT_1_1.enabled = i_bVal
      .Page1.oPag.oPSCODCON_1_5.enabled = i_bVal
      .Page1.oPag.oPSCODCLI_1_6.enabled = i_bVal
      .Page1.oPag.oPSCODSOG_1_7.enabled = i_bVal
      .Page1.oPag.oPSPASWOR_1_9.enabled = i_bVal
      .Page1.oPag.oPSPATSEQ_1_12.enabled = i_bVal
      .Page1.oPag.oPSINFWEB_1_14.enabled = i_bVal
      .Page1.oPag.oPSATTSTA_1_16.enabled = i_bVal
      .Page1.oPag.oPSMODFIR_1_17.enabled = i_bVal
      .Page1.oPag.oBtn_1_13.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_SOST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSVALIDT,"PSVALIDT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODAZI,"PSCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSWORKST,"PSWORKST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSERIAL,"PSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODCON,"PSCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODCLI,"PSCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODSOG,"PSCODSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSPASWOR,"PSPASWOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSPATSEQ,"PSPATSEQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSINFWEB,"PSINFWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSATTSTA,"PSATTSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSMODFIR,"PSMODFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODUTE,"PSCODUTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
    i_lTable = "PAR_SOST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR_SOST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_SOST_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SOSAZI","w_PSSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_SOST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_SOST')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_SOST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PSVALIDT,PSCODAZI,PSWORKST,PSSERIAL,PSCODCON"+;
                  ",PSCODCLI,PSCODSOG,PSPASWOR,PSPATSEQ,PSINFWEB"+;
                  ",PSATTSTA,PSMODFIR,PSCODUTE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PSVALIDT)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODAZI)+;
                  ","+cp_ToStrODBC(this.w_PSWORKST)+;
                  ","+cp_ToStrODBC(this.w_PSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PSCODCON)+;
                  ","+cp_ToStrODBC(this.w_PSCODCLI)+;
                  ","+cp_ToStrODBC(this.w_PSCODSOG)+;
                  ","+cp_ToStrODBC(this.w_PSPASWOR)+;
                  ","+cp_ToStrODBC(this.w_PSPATSEQ)+;
                  ","+cp_ToStrODBC(this.w_PSINFWEB)+;
                  ","+cp_ToStrODBC(this.w_PSATTSTA)+;
                  ","+cp_ToStrODBC(this.w_PSMODFIR)+;
                  ","+cp_ToStrODBC(this.w_PSCODUTE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_SOST')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_SOST')
        cp_CheckDeletedKey(i_cTable,0,'PSCODAZI',this.w_PSCODAZI,'PSSERIAL',this.w_PSSERIAL)
        INSERT INTO (i_cTable);
              (PSVALIDT,PSCODAZI,PSWORKST,PSSERIAL,PSCODCON,PSCODCLI,PSCODSOG,PSPASWOR,PSPATSEQ,PSINFWEB,PSATTSTA,PSMODFIR,PSCODUTE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PSVALIDT;
                  ,this.w_PSCODAZI;
                  ,this.w_PSWORKST;
                  ,this.w_PSSERIAL;
                  ,this.w_PSCODCON;
                  ,this.w_PSCODCLI;
                  ,this.w_PSCODSOG;
                  ,this.w_PSPASWOR;
                  ,this.w_PSPATSEQ;
                  ,this.w_PSINFWEB;
                  ,this.w_PSATTSTA;
                  ,this.w_PSMODFIR;
                  ,this.w_PSCODUTE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_SOST_IDX,i_nConn)
      *
      * update PAR_SOST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_SOST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PSVALIDT="+cp_ToStrODBC(this.w_PSVALIDT)+;
             ",PSWORKST="+cp_ToStrODBC(this.w_PSWORKST)+;
             ",PSCODCON="+cp_ToStrODBC(this.w_PSCODCON)+;
             ",PSCODCLI="+cp_ToStrODBC(this.w_PSCODCLI)+;
             ",PSCODSOG="+cp_ToStrODBC(this.w_PSCODSOG)+;
             ",PSPASWOR="+cp_ToStrODBC(this.w_PSPASWOR)+;
             ",PSPATSEQ="+cp_ToStrODBC(this.w_PSPATSEQ)+;
             ",PSINFWEB="+cp_ToStrODBC(this.w_PSINFWEB)+;
             ",PSATTSTA="+cp_ToStrODBC(this.w_PSATTSTA)+;
             ",PSMODFIR="+cp_ToStrODBC(this.w_PSMODFIR)+;
             ",PSCODUTE="+cp_ToStrODBC(this.w_PSCODUTE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_SOST')
        i_cWhere = cp_PKFox(i_cTable  ,'PSCODAZI',this.w_PSCODAZI  ,'PSSERIAL',this.w_PSSERIAL  )
        UPDATE (i_cTable) SET;
              PSVALIDT=this.w_PSVALIDT;
             ,PSWORKST=this.w_PSWORKST;
             ,PSCODCON=this.w_PSCODCON;
             ,PSCODCLI=this.w_PSCODCLI;
             ,PSCODSOG=this.w_PSCODSOG;
             ,PSPASWOR=this.w_PSPASWOR;
             ,PSPATSEQ=this.w_PSPATSEQ;
             ,PSINFWEB=this.w_PSINFWEB;
             ,PSATTSTA=this.w_PSATTSTA;
             ,PSMODFIR=this.w_PSMODFIR;
             ,PSCODUTE=this.w_PSCODUTE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_SOST_IDX,i_nConn)
      *
      * delete PAR_SOST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PSCODAZI',this.w_PSCODAZI  ,'PSSERIAL',this.w_PSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_SOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_SOST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        if .o_PSVALIDT<>.w_PSVALIDT
            .w_PSWORKST = iif(.w_PSVALIDT='W',SUBSTR(SYS(0),1,AT('#',SYS(0))-1),space(15))
        endif
        .DoRTCalc(4,13,.t.)
        if .o_PSVALIDT<>.w_PSVALIDT
            .w_PSCODUTE = iif(.w_PSVALIDT='U',i_CODUTE,0)
        endif
        if .o_PSPATSEQ<>.w_PSPATSEQ
          .Calculate_LMZUISUYVI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_LMZUISUYVI()
    with this
          * --- Controllo path
          .w_PSPATSEQ = IIF(right(alltrim(.w_PSPATSEQ),1)='\' or empty(.w_PSPATSEQ),.w_PSPATSEQ,alltrim(.w_PSPATSEQ)+iif(len(alltrim(.w_PSPATSEQ))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PSPATSEQ,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PSCODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATSOS";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_PSCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_PSCODAZI)
            select COCODAZI,COPATSOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_PATSOS = NVL(_Link_.COPATSOS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODAZI = space(5)
      endif
      this.w_PATSOS = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTROPA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.COCODAZI as COCODAZI102"+ ",link_1_2.COPATSOS as COPATSOS102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on PAR_SOST.PSCODAZI=link_1_2.COCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and PAR_SOST.PSCODAZI=link_1_2.COCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSVALIDT_1_1.RadioValue()==this.w_PSVALIDT)
      this.oPgFrm.Page1.oPag.oPSVALIDT_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODAZI_1_2.value==this.w_PSCODAZI)
      this.oPgFrm.Page1.oPag.oPSCODAZI_1_2.value=this.w_PSCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSWORKST_1_3.value==this.w_PSWORKST)
      this.oPgFrm.Page1.oPag.oPSWORKST_1_3.value=this.w_PSWORKST
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODCON_1_5.value==this.w_PSCODCON)
      this.oPgFrm.Page1.oPag.oPSCODCON_1_5.value=this.w_PSCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODCLI_1_6.value==this.w_PSCODCLI)
      this.oPgFrm.Page1.oPag.oPSCODCLI_1_6.value=this.w_PSCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODSOG_1_7.value==this.w_PSCODSOG)
      this.oPgFrm.Page1.oPag.oPSCODSOG_1_7.value=this.w_PSCODSOG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSPASWOR_1_9.value==this.w_PSPASWOR)
      this.oPgFrm.Page1.oPag.oPSPASWOR_1_9.value=this.w_PSPASWOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPSPATSEQ_1_12.value==this.w_PSPATSEQ)
      this.oPgFrm.Page1.oPag.oPSPATSEQ_1_12.value=this.w_PSPATSEQ
    endif
    if not(this.oPgFrm.Page1.oPag.oPSINFWEB_1_14.value==this.w_PSINFWEB)
      this.oPgFrm.Page1.oPag.oPSINFWEB_1_14.value=this.w_PSINFWEB
    endif
    if not(this.oPgFrm.Page1.oPag.oPSATTSTA_1_16.value==this.w_PSATTSTA)
      this.oPgFrm.Page1.oPag.oPSATTSTA_1_16.value=this.w_PSATTSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSMODFIR_1_17.RadioValue()==this.w_PSMODFIR)
      this.oPgFrm.Page1.oPag.oPSMODFIR_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODUTE_1_30.value==this.w_PSCODUTE)
      this.oPgFrm.Page1.oPag.oPSCODUTE_1_30.value=this.w_PSCODUTE
    endif
    cp_SetControlsValueExtFlds(this,'PAR_SOST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsdm_aps
      if this.cfunction = 'Load'
        vq_exec('..\docm\exe\query\gsdm_aps',this,'Repliche')
        if Reccount('Repliche') > 0
           i_bRes=.f.
           ah_errormsg('Esiste gi� un parametro con le stesse impostazioni di validit�, impossibile confermare.',48,'')
        endif
        use in select('Repliche')
      endif  
      if i_bRes
         IF !chknfile(alltrim(.w_PSPATSEQ),'F')
            i_bRes = .f.
          	i_bnoChk = .t.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PSVALIDT = this.w_PSVALIDT
    this.o_PSPATSEQ = this.w_PSPATSEQ
    return

enddefine

* --- Define pages as container
define class tgsdm_apsPag1 as StdContainer
  Width  = 660
  height = 333
  stdWidth  = 660
  stdheight = 333
  resizeXpos=401
  resizeYpos=231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPSVALIDT_1_1 as StdCombo with uid="RNKICRGFWC",rtseq=1,rtrep=.f.,left=149,top=3,width=144,height=21;
    , ToolTipText = "Imposta il criterio di validit� per i parametri inseriti";
    , HelpContextID = 75297866;
    , cFormVar="w_PSVALIDT",RowSource=""+"Utente in uso,"+"Workstation in uso,"+"Azienda in uso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSVALIDT_1_1.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'W',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPSVALIDT_1_1.GetRadio()
    this.Parent.oContained.w_PSVALIDT = this.RadioValue()
    return .t.
  endfunc

  func oPSVALIDT_1_1.SetRadio()
    this.Parent.oContained.w_PSVALIDT=trim(this.Parent.oContained.w_PSVALIDT)
    this.value = ;
      iif(this.Parent.oContained.w_PSVALIDT=='U',1,;
      iif(this.Parent.oContained.w_PSVALIDT=='W',2,;
      iif(this.Parent.oContained.w_PSVALIDT=='A',3,;
      0)))
  endfunc

  add object oPSCODAZI_1_2 as StdField with uid="FTXBDUOOGV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PSCODAZI", cQueryName = "PSCODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 66468801,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=31, InputMask=replicate('X',5), cLinkFile="CONTROPA", oKey_1_1="COCODAZI", oKey_1_2="this.w_PSCODAZI"

  func oPSCODAZI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPSWORKST_1_3 as StdField with uid="SFYJJVULGS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PSWORKST", cQueryName = "PSWORKST",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nome computer in uso",;
    HelpContextID = 116065354,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=350, Top=31, InputMask=replicate('X',15)

  add object oPSCODCON_1_5 as StdField with uid="RVNIITIGIO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PSCODCON", cQueryName = "PSCODCON",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice di identificazione univoca del conservatore assegnato dal SOS",;
    HelpContextID = 32914364,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=148, Top=81, InputMask=replicate('X',10)

  add object oPSCODCLI_1_6 as StdField with uid="ZLQZEJBWCC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PSCODCLI", cQueryName = "PSCODCLI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice di identificazione univoca del cliente assegnato dal SOS",;
    HelpContextID = 32914369,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=148, Top=110, InputMask=replicate('X',10)

  add object oPSCODSOG_1_7 as StdField with uid="AUJXZTKWVC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PSCODSOG", cQueryName = "PSCODSOG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del soggetto associato al cliente",;
    HelpContextID = 32914371,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=350, Top=110, InputMask=replicate('X',5)

  add object oPSPASWOR_1_9 as StdField with uid="VFXJEQMFHC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PSPASWOR", cQueryName = "PSPASWOR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Password di accesso al Web Services",;
    HelpContextID = 219376568,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=148, Top=139, InputMask=replicate('X',20), PasswordChar='�'

  add object oPSPATSEQ_1_12 as StdField with uid="VDGZQTKDMD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PSPATSEQ", cQueryName = "PSPATSEQ",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso archiviazione file da inviare a SOStitutiva - Inserito in  'Gestione path'",;
    HelpContextID = 251434055,;
   bGlobalFont=.t.,;
    Height=21, Width=480, Left=148, Top=167, InputMask=replicate('X',254)


  add object oBtn_1_13 as StdButton with uid="WCMZURYTFI",left=637, top=168, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 159822634;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_PSPATSEQ=left(cp_getdir(IIF(EMPTY(.w_PSPATSEQ),sys(5)+sys(2003),.w_PSPATSEQ),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPSINFWEB_1_14 as StdMemo with uid="EVVFSBWRUB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PSINFWEB", cQueryName = "PSINFWEB",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Informazioni inerenti collegamento al web server SOStitutiva",;
    HelpContextID = 36250680,;
   bGlobalFont=.t.,;
    Height=55, Width=503, Left=148, Top=197, bdisablefrasimodello=.t.

  add object oPSATTSTA_1_16 as StdField with uid="ALLUKPRFMW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PSATTSTA", cQueryName = "PSATTSTA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice della chiave attributo stato nelle classi documentali SOStitutiva",;
    HelpContextID = 252617783,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=148, Top=260, InputMask=replicate('X',15)


  add object oPSMODFIR_1_17 as StdCombo with uid="INZGECFRXC",rtseq=13,rtrep=.f.,left=148,top=308,width=226,height=21;
    , ToolTipText = "Consente di selezionare la modalit� di firma digitale da utilizzare";
    , HelpContextID = 250977208;
    , cFormVar="w_PSMODFIR",RowSource=""+"Non gestita,"+"Tramite AdHoc (dll interna),"+"Tramite client SOStitutiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSMODFIR_1_17.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oPSMODFIR_1_17.GetRadio()
    this.Parent.oContained.w_PSMODFIR = this.RadioValue()
    return .t.
  endfunc

  func oPSMODFIR_1_17.SetRadio()
    this.Parent.oContained.w_PSMODFIR=trim(this.Parent.oContained.w_PSMODFIR)
    this.value = ;
      iif(this.Parent.oContained.w_PSMODFIR=='N',1,;
      iif(this.Parent.oContained.w_PSMODFIR=='A',2,;
      iif(this.Parent.oContained.w_PSMODFIR=='C',3,;
      0)))
  endfunc

  add object oPSCODUTE_1_30 as StdField with uid="VNEQDGZBHF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PSCODUTE", cQueryName = "PSCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 640059,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=596, Top=32

  add object oStr_1_8 as StdString with uid="KYEEQKECUQ",Visible=.t., Left=57, Top=113,;
    Alignment=1, Width=88, Height=18,;
    Caption="Cod. cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="OXTWLDUGZZ",Visible=.t., Left=75, Top=168,;
    Alignment=1, Width=70, Height=18,;
    Caption="Path SOS:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NLJRDALJEC",Visible=.t., Left=57, Top=197,;
    Alignment=1, Width=88, Height=18,;
    Caption="Web server:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CFMSZNFMXP",Visible=.t., Left=57, Top=260,;
    Alignment=1, Width=88, Height=18,;
    Caption="Cod. stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="CMFAAQASYO",Visible=.t., Left=25, Top=82,;
    Alignment=1, Width=120, Height=18,;
    Caption="Cod. conservatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="KKURRATPRI",Visible=.t., Left=57, Top=141,;
    Alignment=1, Width=88, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="PXEZIIMFAF",Visible=.t., Left=60, Top=5,;
    Alignment=1, Width=85, Height=18,;
    Caption="Validi per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MHNIXIMHUI",Visible=.t., Left=39, Top=35,;
    Alignment=1, Width=106, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GUXXMPUAEI",Visible=.t., Left=259, Top=35,;
    Alignment=1, Width=87, Height=18,;
    Caption="Workstation:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="FHSKOHHGOZ",Visible=.t., Left=28, Top=60,;
    Alignment=0, Width=72, Height=17,;
    Caption="Parametri"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="VULZEEEZRC",Visible=.t., Left=266, Top=113,;
    Alignment=1, Width=80, Height=18,;
    Caption="Soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="VRYAMGVFEL",Visible=.t., Left=28, Top=285,;
    Alignment=0, Width=122, Height=17,;
    Caption="Firma digitale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="VQKHXMNCDN",Visible=.t., Left=1, Top=309,;
    Alignment=1, Width=144, Height=18,;
    Caption="Strumento di firma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="TEDEHMJEMF",Visible=.t., Left=512, Top=35,;
    Alignment=1, Width=82, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oBox_1_22 as StdBox with uid="DAVHDNBFME",left=4, top=76, width=652,height=2

  add object oBox_1_27 as StdBox with uid="MLYOIVBOPC",left=4, top=301, width=652,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_aps','PAR_SOST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PSCODAZI=PAR_SOST.PSCODAZI";
  +" and "+i_cAliasName2+".PSSERIAL=PAR_SOST.PSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
