* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_svl                                                        *
*              Stampa log processi documentali                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2009-03-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_svl",oParentObject))

* --- Class definition
define class tgsdm_svl as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 671
  Height = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-03-05"
  HelpContextID=227949463
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  PROMDOCU_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsdm_svl"
  cComment = "Stampa log processi documentali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_LPESEUTE = 0
  w_DESUTE = space(20)
  w_LPESEDAT = ctod('  /  /  ')
  w_LPCODPRO = space(10)
  o_LPCODPRO = space(10)
  w_DESPRO = space(40)
  w_LPTIPCON = space(1)
  o_LPTIPCON = space(1)
  w_LPCODCON = space(15)
  w_DESCON = space(40)
  w_LPCODAGE = space(15)
  w_DESAGE = space(40)
  w_LPNOCODI = space(20)
  w_DESNOM = space(40)
  w_LPCODDEBI = space(15)
  w_SOLOSHOW = .F.
  w_LPKEYIST = space(10)
  w_LPNUMATT = 0
  w_LPULTIST = 0
  w_ALLPROC = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_NOFLGBEN = space(1)
  w_DESCDEB = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_svlPag1","gsdm_svl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLPESEUTE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='PROMDOCU'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='OFF_NOMI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LPESEUTE=0
      .w_DESUTE=space(20)
      .w_LPESEDAT=ctod("  /  /  ")
      .w_LPCODPRO=space(10)
      .w_DESPRO=space(40)
      .w_LPTIPCON=space(1)
      .w_LPCODCON=space(15)
      .w_DESCON=space(40)
      .w_LPCODAGE=space(15)
      .w_DESAGE=space(40)
      .w_LPNOCODI=space(20)
      .w_DESNOM=space(40)
      .w_LPCODDEBI=space(15)
      .w_SOLOSHOW=.f.
      .w_LPKEYIST=space(10)
      .w_LPNUMATT=0
      .w_LPULTIST=0
      .w_ALLPROC=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_NOFLGBEN=space(1)
      .w_DESCDEB=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LPESEUTE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_LPCODPRO))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_LPCODCON = space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_LPCODCON))
          .link_1_7('Full')
        endif
          .DoRTCalc(8,8,.f.)
        .w_LPCODAGE = space(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_LPCODAGE))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_LPNOCODI = Space(20)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_LPNOCODI))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_LPCODDEBI = SPACE(15)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_LPCODDEBI))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_SOLOSHOW = .F.
          .DoRTCalc(15,17,.f.)
        .w_ALLPROC = iif(empty(.w_LPCODPRO),'S','N')
        .w_OBTEST = i_DATSYS
    endwith
    this.DoRTCalc(20,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_LPTIPCON<>.w_LPTIPCON
            .w_LPCODCON = space(15)
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_LPTIPCON<>.w_LPTIPCON
            .w_LPCODAGE = space(15)
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_LPTIPCON<>.w_LPTIPCON
            .w_LPNOCODI = Space(20)
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.t.)
        if .o_LPTIPCON<>.w_LPTIPCON
            .w_LPCODDEBI = SPACE(15)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(14,17,.t.)
        if .o_LPCODPRO<>.w_LPCODPRO
            .w_ALLPROC = iif(empty(.w_LPCODPRO),'S','N')
        endif
            .w_OBTEST = i_DATSYS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLPESEUTE_1_1.enabled = this.oPgFrm.Page1.oPag.oLPESEUTE_1_1.mCond()
    this.oPgFrm.Page1.oPag.oLPESEDAT_1_3.enabled = this.oPgFrm.Page1.oPag.oLPESEDAT_1_3.mCond()
    this.oPgFrm.Page1.oPag.oLPCODPRO_1_4.enabled = this.oPgFrm.Page1.oPag.oLPCODPRO_1_4.mCond()
    this.oPgFrm.Page1.oPag.oLPTIPCON_1_6.enabled = this.oPgFrm.Page1.oPag.oLPTIPCON_1_6.mCond()
    this.oPgFrm.Page1.oPag.oLPCODCON_1_7.enabled = this.oPgFrm.Page1.oPag.oLPCODCON_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLPCODAGE_1_9.enabled = this.oPgFrm.Page1.oPag.oLPCODAGE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLPCODCON_1_7.visible=!this.oPgFrm.Page1.oPag.oLPCODCON_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_8.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_8.mHide()
    this.oPgFrm.Page1.oPag.oLPCODAGE_1_9.visible=!this.oPgFrm.Page1.oPag.oLPCODAGE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_10.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_10.mHide()
    this.oPgFrm.Page1.oPag.oLPNOCODI_1_11.visible=!this.oPgFrm.Page1.oPag.oLPNOCODI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDESNOM_1_12.visible=!this.oPgFrm.Page1.oPag.oDESNOM_1_12.mHide()
    this.oPgFrm.Page1.oPag.oLPCODDEBI_1_13.visible=!this.oPgFrm.Page1.oPag.oLPCODDEBI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDESCDEB_1_30.visible=!this.oPgFrm.Page1.oPag.oDESCDEB_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LPESEUTE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPESEUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_LPESEUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_LPESEUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LPESEUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oLPESEUTE_1_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPESEUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_LPESEUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_LPESEUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPESEUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LPESEUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPESEUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODPRO
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_lTable = "PROMDOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2], .t., this.PROMDOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PROMDOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDCODPRO like "+cp_ToStrODBC(trim(this.w_LPCODPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDCODPRO',trim(this.w_LPCODPRO))
          select PDCODPRO,PDDESPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODPRO)==trim(_Link_.PDCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LPCODPRO) and !this.bDontReportError
            deferred_cp_zoom('PROMDOCU','*','PDCODPRO',cp_AbsName(oSource.parent,'oLPCODPRO_1_4'),i_cWhere,'',"Processi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',oSource.xKey(1))
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(this.w_LPCODPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',this.w_LPCODPRO)
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODPRO = NVL(_Link_.PDCODPRO,space(10))
      this.w_DESPRO = NVL(_Link_.PDDESPRO,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODPRO = space(10)
      endif
      this.w_DESPRO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])+'\'+cp_ToStr(_Link_.PDCODPRO,1)
      cp_ShowWarn(i_cKey,this.PROMDOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODCON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_LPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_LPTIPCON;
                     ,'ANCODICE',trim(this.w_LPCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_LPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_LPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_LPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LPCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oLPCODCON_1_7'),i_cWhere,'',"Clienti / fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_LPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_LPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LPTIPCON;
                       ,'ANCODICE',this.w_LPCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODAGE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_LPCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_LPCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LPCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oLPCODAGE_1_9'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_LPCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_LPCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODAGE = NVL(_Link_.AGCODAGE,space(15))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODAGE = space(15)
      endif
      this.w_DESAGE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPNOCODI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPNOCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_LPNOCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_LPNOCODI))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPNOCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LPNOCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oLPNOCODI_1_11'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPNOCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_LPNOCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_LPNOCODI)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPNOCODI = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPNOCODI = space(20)
      endif
      this.w_DESNOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPNOCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODDEBI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODDEBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDC',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_LPCODDEBI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_LPCODDEBI))
          select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODDEBI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_LPCODDEBI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_LPCODDEBI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LPCODDEBI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oLPCODDEBI_1_13'),i_cWhere,'GSAR_BDC',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODDEBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_LPCODDEBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_LPCODDEBI)
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODDEBI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCDEB = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOFLGBEN = NVL(_Link_.NOFLGBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODDEBI = space(15)
      endif
      this.w_DESCDEB = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NOFLGBEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice inserito � obsoleto")
        endif
        this.w_LPCODDEBI = space(15)
        this.w_DESCDEB = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NOFLGBEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODDEBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLPESEUTE_1_1.value==this.w_LPESEUTE)
      this.oPgFrm.Page1.oPag.oLPESEUTE_1_1.value=this.w_LPESEUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_2.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_2.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oLPESEDAT_1_3.value==this.w_LPESEDAT)
      this.oPgFrm.Page1.oPag.oLPESEDAT_1_3.value=this.w_LPESEDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODPRO_1_4.value==this.w_LPCODPRO)
      this.oPgFrm.Page1.oPag.oLPCODPRO_1_4.value=this.w_LPCODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRO_1_5.value==this.w_DESPRO)
      this.oPgFrm.Page1.oPag.oDESPRO_1_5.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oLPTIPCON_1_6.RadioValue()==this.w_LPTIPCON)
      this.oPgFrm.Page1.oPag.oLPTIPCON_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODCON_1_7.value==this.w_LPCODCON)
      this.oPgFrm.Page1.oPag.oLPCODCON_1_7.value=this.w_LPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_8.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_8.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODAGE_1_9.value==this.w_LPCODAGE)
      this.oPgFrm.Page1.oPag.oLPCODAGE_1_9.value=this.w_LPCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_10.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_10.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oLPNOCODI_1_11.value==this.w_LPNOCODI)
      this.oPgFrm.Page1.oPag.oLPNOCODI_1_11.value=this.w_LPNOCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_12.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_12.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODDEBI_1_13.value==this.w_LPCODDEBI)
      this.oPgFrm.Page1.oPag.oLPCODDEBI_1_13.value=this.w_LPCODDEBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCDEB_1_30.value==this.w_DESCDEB)
      this.oPgFrm.Page1.oPag.oDESCDEB_1_30.value=this.w_DESCDEB
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B')  and not(.w_LPTIPCON<>'B')  and not(empty(.w_LPCODDEBI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLPCODDEBI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice inserito � obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LPCODPRO = this.w_LPCODPRO
    this.o_LPTIPCON = this.w_LPTIPCON
    return

enddefine

* --- Define pages as container
define class tgsdm_svlPag1 as StdContainer
  Width  = 667
  height = 163
  stdWidth  = 667
  stdheight = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLPESEUTE_1_1 as StdField with uid="UHOZWCLAIU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LPESEUTE", cQueryName = "LPESEUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente",;
    HelpContextID = 146939653,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=107, Top=3, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_LPESEUTE"

  func oLPESEUTE_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  func oLPESEUTE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPESEUTE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPESEUTE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oLPESEUTE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESUTE_1_2 as StdField with uid="STMPKGVBOK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 131025610,;
   bGlobalFont=.t.,;
    Height=21, Width=162, Left=165, Top=3, InputMask=replicate('X',20)

  add object oLPESEDAT_1_3 as StdField with uid="PISUMSZRTK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LPESEDAT", cQueryName = "LPESEDAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data esecuzione processo documentale",;
    HelpContextID = 163716854,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=426, Top=3

  func oLPESEDAT_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  add object oLPCODPRO_1_4 as StdField with uid="SJAGKDMOJR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LPCODPRO", cQueryName = "LPCODPRO",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice processo documentale",;
    HelpContextID = 232144635,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=107, Top=28, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PROMDOCU", oKey_1_1="PDCODPRO", oKey_1_2="this.w_LPCODPRO"

  func oLPCODPRO_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  func oLPCODPRO_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODPRO_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODPRO_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMDOCU','*','PDCODPRO',cp_AbsName(this.parent,'oLPCODPRO_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Processi documentali",'',this.parent.oContained
  endproc

  add object oDESPRO_1_5 as StdField with uid="BHEJCRDJDS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 234113738,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=208, Top=28, InputMask=replicate('X',40)


  add object oLPTIPCON_1_6 as StdCombo with uid="FZYEPZHXYM",value=1,rtseq=6,rtrep=.f.,left=107,top=54,width=149,height=21;
    , ToolTipText = "Tipo intestatario (cliente/fornitore/nessuno)";
    , HelpContextID = 98881796;
    , cFormVar="w_LPTIPCON",RowSource=""+"No filtro,"+"Clienti,"+"Fornitori,"+"Agenti,"+"Nominativi,"+"Debitori creditori diversi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLPTIPCON_1_6.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    iif(this.value =5,'N',;
    iif(this.value =6,'B',;
    space(1))))))))
  endfunc
  func oLPTIPCON_1_6.GetRadio()
    this.Parent.oContained.w_LPTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oLPTIPCON_1_6.SetRadio()
    this.Parent.oContained.w_LPTIPCON=trim(this.Parent.oContained.w_LPTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_LPTIPCON=='',1,;
      iif(this.Parent.oContained.w_LPTIPCON=='C',2,;
      iif(this.Parent.oContained.w_LPTIPCON=='F',3,;
      iif(this.Parent.oContained.w_LPTIPCON=='A',4,;
      iif(this.Parent.oContained.w_LPTIPCON=='N',5,;
      iif(this.Parent.oContained.w_LPTIPCON=='B',6,;
      0))))))
  endfunc

  func oLPTIPCON_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  func oLPTIPCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_LPCODCON)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oLPCODCON_1_7 as StdField with uid="HEPHXKOQUN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LPCODCON", cQueryName = "LPCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestario",;
    HelpContextID = 86622468,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=259, Top=54, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_LPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_LPCODCON"

  func oLPCODCON_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW and not empty(.w_LPTIPCON))
    endwith
   endif
  endfunc

  func oLPCODCON_1_7.mHide()
    with this.Parent.oContained
      return (! inlist(.w_LPTIPCON,'C','F'))
    endwith
  endfunc

  func oLPCODCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODCON_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODCON_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_LPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_LPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oLPCODCON_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti / fornitori",'',this.parent.oContained
  endproc

  add object oDESCON_1_8 as StdField with uid="HBYEYNFBWS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 254888650,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=394, Top=54, InputMask=replicate('X',40)

  func oDESCON_1_8.mHide()
    with this.Parent.oContained
      return (! inlist(.w_LPTIPCON,'C','F'))
    endwith
  endfunc

  add object oLPCODAGE_1_9 as StdField with uid="AHSBMOJZBT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LPCODAGE", cQueryName = "LPCODAGE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 215367429,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=259, Top=54, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_LPCODAGE"

  func oLPCODAGE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW and not empty(.w_LPTIPCON))
    endwith
   endif
  endfunc

  func oLPCODAGE_1_9.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'A')
    endwith
  endfunc

  func oLPCODAGE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODAGE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODAGE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oLPCODAGE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oDESAGE_1_10 as StdField with uid="FXIDPBVVUW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 145967818,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=394, Top=54, InputMask=replicate('X',40)

  func oDESAGE_1_10.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'A')
    endwith
  endfunc

  add object oLPNOCODI_1_11 as StdField with uid="BFVZXIODCX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_LPNOCODI", cQueryName = "LPNOCODI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 249925377,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=259, Top=54, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_LPNOCODI"

  func oLPNOCODI_1_11.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'N')
    endwith
  endfunc

  func oLPNOCODI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPNOCODI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPNOCODI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oLPNOCODI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oLPNOCODI_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_LPNOCODI
     i_obj.ecpSave()
  endproc

  add object oDESNOM_1_12 as StdField with uid="AONBOZUPPK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 2509514,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=431, Top=54, InputMask=replicate('X',40)

  func oDESNOM_1_12.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'N')
    endwith
  endfunc

  add object oLPCODDEBI_1_13 as StdField with uid="HIXWZHYNYU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_LPCODDEBI", cQueryName = "LPCODDEBI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice inserito � obsoleto",;
    ToolTipText = "Codice debitore creditore",;
    HelpContextID = 165034616,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=260, Top=54, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_BDC", oKey_1_1="NOCODICE", oKey_1_2="this.w_LPCODDEBI"

  func oLPCODDEBI_1_13.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'B')
    endwith
  endfunc

  func oLPCODDEBI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODDEBI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODDEBI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oLPCODDEBI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDC',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oLPCODDEBI_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_LPCODDEBI
     i_obj.ecpSave()
  endproc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=107, top=86, width=554,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 130923802


  add object oBtn_1_15 as StdButton with uid="WDHBUTUXIR",left=562, top=113, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 167131866;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)) and not empty(.w_LPCODPRO))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="FGOJEBDDCO",left=613, top=113, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 235266886;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCDEB_1_30 as StdField with uid="WUNNTZVNHX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCDEB", cQueryName = "DESCDEB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 148982474,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=394, Top=54, InputMask=replicate('X',40)

  func oDESCDEB_1_30.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON # 'B')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=5, Top=87,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IDFURKQPIT",Visible=.t., Left=27, Top=31,;
    Alignment=1, Width=79, Height=18,;
    Caption="Processo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="EXZPSDLQWD",Visible=.t., Left=379, Top=6,;
    Alignment=1, Width=47, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BHDQGUJCAS",Visible=.t., Left=42, Top=6,;
    Alignment=1, Width=64, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="LPOQAJPVKV",Visible=.t., Left=12, Top=55,;
    Alignment=1, Width=94, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_svl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
