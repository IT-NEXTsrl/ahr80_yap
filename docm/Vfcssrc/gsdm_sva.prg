* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_sva                                                        *
*              Stampa abilitazione intestatari                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2009-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_sva",oParentObject))

* --- Class definition
define class tgsdm_sva as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 664
  Height = 197
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-02-07"
  HelpContextID=40485993
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsdm_sva"
  cComment = "Stampa abilitazione intestatari"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOCON = space(1)
  o_TIPOCON = space(1)
  w_PBANCODI = space(15)
  w_DESCR1 = space(40)
  w_PEANCODI = space(15)
  w_DESCR2 = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_PBAGCODI = space(15)
  w_PEAGCODI = space(15)
  w_DESCR3 = space(40)
  w_DESCR4 = space(40)
  w_PBNOCODI = space(15)
  w_DESCR5 = space(40)
  w_TIPOCON = space(1)
  w_PBDEBCODI = space(15)
  w_PEDEBCODI = space(15)
  w_NOFLGBEN = space(1)
  w_NOFLGBEN1 = space(1)
  w_DESCR7 = space(40)
  w_DESCR8 = space(40)
  w_DESCR6 = space(40)
  w_PENOCODI = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_svaPag1","gsdm_sva",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOCON_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='OFF_NOMI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOCON=space(1)
      .w_PBANCODI=space(15)
      .w_DESCR1=space(40)
      .w_PEANCODI=space(15)
      .w_DESCR2=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_PBAGCODI=space(15)
      .w_PEAGCODI=space(15)
      .w_DESCR3=space(40)
      .w_DESCR4=space(40)
      .w_PBNOCODI=space(15)
      .w_DESCR5=space(40)
      .w_TIPOCON=space(1)
      .w_PBDEBCODI=space(15)
      .w_PEDEBCODI=space(15)
      .w_NOFLGBEN=space(1)
      .w_NOFLGBEN1=space(1)
      .w_DESCR7=space(40)
      .w_DESCR8=space(40)
      .w_DESCR6=space(40)
      .w_PENOCODI=space(15)
        .w_TIPOCON = 'C'
        .w_PBANCODI = space(15)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_PEANCODI = space(15)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(7,7,.f.)
        .w_PBAGCODI = space(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PBAGCODI))
          .link_1_12('Full')
        endif
        .w_PEAGCODI = space(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_PEAGCODI))
          .link_1_13('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_PBNOCODI = space(15)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PBNOCODI))
          .link_1_16('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
          .DoRTCalc(13,13,.f.)
        .w_TIPOCON = 'A'
        .w_PBDEBCODI = space(15)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_PBDEBCODI))
          .link_1_22('Full')
        endif
        .w_PEDEBCODI = space(15)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_PEDEBCODI))
          .link_1_23('Full')
        endif
          .DoRTCalc(17,21,.f.)
        .w_PENOCODI = Space(15)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_PENOCODI))
          .link_1_29('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPOCON<>.w_TIPOCON
            .w_PBANCODI = space(15)
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_TIPOCON<>.w_TIPOCON
            .w_PEANCODI = space(15)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(7,7,.t.)
        if .o_TIPOCON<>.w_TIPOCON
            .w_PBAGCODI = space(15)
          .link_1_12('Full')
        endif
        if .o_TIPOCON<>.w_TIPOCON
            .w_PEAGCODI = space(15)
          .link_1_13('Full')
        endif
        .DoRTCalc(10,11,.t.)
        if .o_TIPOCON<>.w_TIPOCON
            .w_PBNOCODI = space(15)
          .link_1_16('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .DoRTCalc(13,14,.t.)
        if .o_TIPOCON<>.w_TIPOCON
            .w_PBDEBCODI = space(15)
          .link_1_22('Full')
        endif
        if .o_TIPOCON<>.w_TIPOCON
            .w_PEDEBCODI = space(15)
          .link_1_23('Full')
        endif
        .DoRTCalc(17,21,.t.)
        if .o_TIPOCON<>.w_TIPOCON
            .w_PENOCODI = Space(15)
          .link_1_29('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPOCON_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPOCON_1_1.mHide()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_2.visible=!this.oPgFrm.Page1.oPag.oPBANCODI_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDESCR1_1_3.visible=!this.oPgFrm.Page1.oPag.oDESCR1_1_3.mHide()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_4.visible=!this.oPgFrm.Page1.oPag.oPEANCODI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oDESCR2_1_5.visible=!this.oPgFrm.Page1.oPag.oDESCR2_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPBAGCODI_1_12.visible=!this.oPgFrm.Page1.oPag.oPBAGCODI_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPEAGCODI_1_13.visible=!this.oPgFrm.Page1.oPag.oPEAGCODI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDESCR3_1_14.visible=!this.oPgFrm.Page1.oPag.oDESCR3_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDESCR4_1_15.visible=!this.oPgFrm.Page1.oPag.oDESCR4_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPBNOCODI_1_16.visible=!this.oPgFrm.Page1.oPag.oPBNOCODI_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDESCR5_1_20.visible=!this.oPgFrm.Page1.oPag.oDESCR5_1_20.mHide()
    this.oPgFrm.Page1.oPag.oTIPOCON_1_21.visible=!this.oPgFrm.Page1.oPag.oTIPOCON_1_21.mHide()
    this.oPgFrm.Page1.oPag.oPBDEBCODI_1_22.visible=!this.oPgFrm.Page1.oPag.oPBDEBCODI_1_22.mHide()
    this.oPgFrm.Page1.oPag.oPEDEBCODI_1_23.visible=!this.oPgFrm.Page1.oPag.oPEDEBCODI_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESCR7_1_26.visible=!this.oPgFrm.Page1.oPag.oDESCR7_1_26.mHide()
    this.oPgFrm.Page1.oPag.oDESCR8_1_27.visible=!this.oPgFrm.Page1.oPag.oDESCR8_1_27.mHide()
    this.oPgFrm.Page1.oPag.oDESCR6_1_28.visible=!this.oPgFrm.Page1.oPag.oDESCR6_1_28.mHide()
    this.oPgFrm.Page1.oPag.oPENOCODI_1_29.visible=!this.oPgFrm.Page1.oPag.oPENOCODI_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PBANCODI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCON;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_2'),i_cWhere,'',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCON;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCON;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_4'),i_cWhere,'',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCON;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBAGCODI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBAGCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PBAGCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PBAGCODI))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBAGCODI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_PBAGCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_PBAGCODI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBAGCODI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPBAGCODI_1_12'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBAGCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PBAGCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PBAGCODI)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBAGCODI = NVL(_Link_.AGCODAGE,space(15))
      this.w_DESCR3 = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBAGCODI = space(15)
      endif
      this.w_DESCR3 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEAGCODI)) OR  (.w_PBAGCODI<=.w_PEAGCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBAGCODI = space(15)
        this.w_DESCR3 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBAGCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEAGCODI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEAGCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PEAGCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PEAGCODI))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEAGCODI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_PEAGCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_PEAGCODI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEAGCODI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPEAGCODI_1_13'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEAGCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PEAGCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PEAGCODI)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEAGCODI = NVL(_Link_.AGCODAGE,space(15))
      this.w_DESCR4 = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PEAGCODI = space(15)
      endif
      this.w_DESCR4 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEAGCODI>=.w_PBAGCODI) or (empty(.w_PBAGCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEAGCODI = space(15)
        this.w_DESCR4 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEAGCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBNOCODI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBNOCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PBNOCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PBNOCODI))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBNOCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PBNOCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPBNOCODI_1_16'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBNOCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PBNOCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PBNOCODI)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBNOCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR5 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBNOCODI = space(15)
      endif
      this.w_DESCR5 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PENOCODI)) OR  (.w_PBNOCODI<=.w_PENOCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBNOCODI = space(15)
        this.w_DESCR5 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBNOCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBDEBCODI
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBDEBCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsar_bdc',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PBDEBCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PBDEBCODI))
          select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBDEBCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PBDEBCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PBDEBCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBDEBCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPBDEBCODI_1_22'),i_cWhere,'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBDEBCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PBDEBCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PBDEBCODI)
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBDEBCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR7 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOFLGBEN = NVL(_Link_.NOFLGBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PBDEBCODI = space(15)
      endif
      this.w_DESCR7 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NOFLGBEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEDEBCODI)) OR  (.w_PBDEBCODI<=.w_PEDEBCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBDEBCODI = space(15)
        this.w_DESCR7 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NOFLGBEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBDEBCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEDEBCODI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEDEBCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsar_bdc',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PEDEBCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PEDEBCODI))
          select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEDEBCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PEDEBCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PEDEBCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEDEBCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPEDEBCODI_1_23'),i_cWhere,'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEDEBCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PEDEBCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PEDEBCODI)
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEDEBCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR8 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOFLGBEN1 = NVL(_Link_.NOFLGBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PEDEBCODI = space(15)
      endif
      this.w_DESCR8 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NOFLGBEN1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEDEBCODI>=.w_PBDEBCODI) or (empty(.w_PBDEBCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN1='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEDEBCODI = space(15)
        this.w_DESCR8 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NOFLGBEN1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEDEBCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PENOCODI
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PENOCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PENOCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PENOCODI))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PENOCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PENOCODI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PENOCODI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PENOCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPENOCODI_1_29'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PENOCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PENOCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PENOCODI)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PENOCODI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCR6 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PENOCODI = space(15)
      endif
      this.w_DESCR6 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PENOCODI>=.w_PBNOCODI) or (empty(.w_PBNOCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PENOCODI = space(15)
        this.w_DESCR6 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PENOCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOCON_1_1.RadioValue()==this.w_TIPOCON)
      this.oPgFrm.Page1.oPag.oTIPOCON_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_2.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_2.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_3.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_3.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_4.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_4.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_5.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_5.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oPBAGCODI_1_12.value==this.w_PBAGCODI)
      this.oPgFrm.Page1.oPag.oPBAGCODI_1_12.value=this.w_PBAGCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEAGCODI_1_13.value==this.w_PEAGCODI)
      this.oPgFrm.Page1.oPag.oPEAGCODI_1_13.value=this.w_PEAGCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR3_1_14.value==this.w_DESCR3)
      this.oPgFrm.Page1.oPag.oDESCR3_1_14.value=this.w_DESCR3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR4_1_15.value==this.w_DESCR4)
      this.oPgFrm.Page1.oPag.oDESCR4_1_15.value=this.w_DESCR4
    endif
    if not(this.oPgFrm.Page1.oPag.oPBNOCODI_1_16.value==this.w_PBNOCODI)
      this.oPgFrm.Page1.oPag.oPBNOCODI_1_16.value=this.w_PBNOCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR5_1_20.value==this.w_DESCR5)
      this.oPgFrm.Page1.oPag.oDESCR5_1_20.value=this.w_DESCR5
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCON_1_21.RadioValue()==this.w_TIPOCON)
      this.oPgFrm.Page1.oPag.oTIPOCON_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPBDEBCODI_1_22.value==this.w_PBDEBCODI)
      this.oPgFrm.Page1.oPag.oPBDEBCODI_1_22.value=this.w_PBDEBCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDEBCODI_1_23.value==this.w_PEDEBCODI)
      this.oPgFrm.Page1.oPag.oPEDEBCODI_1_23.value=this.w_PEDEBCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR7_1_26.value==this.w_DESCR7)
      this.oPgFrm.Page1.oPag.oDESCR7_1_26.value=this.w_DESCR7
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR8_1_27.value==this.w_DESCR8)
      this.oPgFrm.Page1.oPag.oDESCR8_1_27.value=this.w_DESCR8
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR6_1_28.value==this.w_DESCR6)
      this.oPgFrm.Page1.oPag.oDESCR6_1_28.value=this.w_DESCR6
    endif
    if not(this.oPgFrm.Page1.oPag.oPENOCODI_1_29.value==this.w_PENOCODI)
      this.oPgFrm.Page1.oPag.oPENOCODI_1_29.value=this.w_PENOCODI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOCON$'ANBT')  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOCON$'ANBT')  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_PEAGCODI)) OR  (.w_PBAGCODI<=.w_PEAGCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOCON$'CFNBT')  and not(empty(.w_PBAGCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBAGCODI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PEAGCODI>=.w_PBAGCODI) or (empty(.w_PBAGCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOCON$'CFNBT')  and not(empty(.w_PEAGCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEAGCODI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_PENOCODI)) OR  (.w_PBNOCODI<=.w_PENOCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOCON$'CFABT')  and not(empty(.w_PBNOCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBNOCODI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_PEDEBCODI)) OR  (.w_PBDEBCODI<=.w_PEDEBCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B')  and not(.w_TIPOCON$'CFANT')  and not(empty(.w_PBDEBCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBDEBCODI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PEDEBCODI>=.w_PBDEBCODI) or (empty(.w_PBDEBCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN1='B')  and not(.w_TIPOCON$'CFANT')  and not(empty(.w_PEDEBCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEDEBCODI_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PENOCODI>=.w_PBNOCODI) or (empty(.w_PBNOCODI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOCON$'CFABT')  and not(empty(.w_PENOCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPENOCODI_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOCON = this.w_TIPOCON
    return

enddefine

* --- Define pages as container
define class tgsdm_svaPag1 as StdContainer
  Width  = 660
  height = 197
  stdWidth  = 660
  stdheight = 197
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOCON_1_1 as StdCombo with uid="JAZVMLTCMO",rtseq=1,rtrep=.f.,left=125,top=6,width=93,height=21;
    , ToolTipText = "Tipo filtro (cliente/fornitore/agente/nominativo)";
    , HelpContextID = 249918922;
    , cFormVar="w_TIPOCON",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Nominativo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCON_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oTIPOCON_1_1.GetRadio()
    this.Parent.oContained.w_TIPOCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCON_1_1.SetRadio()
    this.Parent.oContained.w_TIPOCON=trim(this.Parent.oContained.w_TIPOCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCON=='C',1,;
      iif(this.Parent.oContained.w_TIPOCON=='F',2,;
      iif(this.Parent.oContained.w_TIPOCON=='A',3,;
      iif(this.Parent.oContained.w_TIPOCON=='N',4,;
      iif(this.Parent.oContained.w_TIPOCON=='T',5,;
      0)))))
  endfunc

  func oTIPOCON_1_1.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oTIPOCON_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PBANCODI)
        bRes2=.link_1_2('Full')
      endif
      if .not. empty(.w_PEANCODI)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oPBANCODI_1_2 as StdField with uid="MRSFOXHTFW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 18387775,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=125, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'ANBT')
    endwith
  endfunc

  func oPBANCODI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCR1_1_3 as StdField with uid="CHLOPGXLBM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 201411274,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=40, InputMask=replicate('X',40)

  func oDESCR1_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'ANBT')
    endwith
  endfunc

  add object oPEANCODI_1_4 as StdField with uid="IIYGZMLHDJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 18388543,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'ANBT')
    endwith
  endfunc

  func oPEANCODI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCR2_1_5 as StdField with uid="MINMVUHPXL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184634058,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=75, InputMask=replicate('X',40)

  func oDESCR2_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'ANBT')
    endwith
  endfunc

  add object oPBAGCODI_1_12 as StdField with uid="OVUQSKYCXY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PBAGCODI", cQueryName = "PBAGCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 17929023,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=125, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PBAGCODI"

  func oPBAGCODI_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFNBT')
    endwith
  endfunc

  func oPBAGCODI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBAGCODI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBAGCODI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPBAGCODI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oPEAGCODI_1_13 as StdField with uid="JXMAYGDIKD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PEAGCODI", cQueryName = "PEAGCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 17929791,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PEAGCODI"

  func oPEAGCODI_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFNBT')
    endwith
  endfunc

  func oPEAGCODI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEAGCODI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEAGCODI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPEAGCODI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oDESCR3_1_14 as StdField with uid="SYAVFVIJQN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCR3", cQueryName = "DESCR3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 167856842,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=40, InputMask=replicate('X',40)

  func oDESCR3_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFNBT')
    endwith
  endfunc

  add object oDESCR4_1_15 as StdField with uid="YCNZVHFXMB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCR4", cQueryName = "DESCR4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151079626,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=75, InputMask=replicate('X',40)

  func oDESCR4_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFNBT')
    endwith
  endfunc

  add object oPBNOCODI_1_16 as StdField with uid="BFVZXIODCX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PBNOCODI", cQueryName = "PBNOCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 18506559,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_PBNOCODI"

  func oPBNOCODI_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFABT')
    endwith
  endfunc

  func oPBNOCODI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBNOCODI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBNOCODI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPBNOCODI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oPBNOCODI_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PBNOCODI
     i_obj.ecpSave()
  endproc


  add object oObj_1_17 as cp_outputCombo with uid="ZLERXIVPWT",left=125, top=114, width=525,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137511654


  add object oBtn_1_18 as StdButton with uid="WDHBUTUXIR",left=550, top=144, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 167131866;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)) and (not empty(.w_OREP)))
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="FGOJEBDDCO",left=603, top=144, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 33168570;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCR5_1_20 as StdField with uid="SMPNBMLESO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCR5", cQueryName = "DESCR5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134302410,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=40, InputMask=replicate('X',40)

  func oDESCR5_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFABT')
    endwith
  endfunc


  add object oTIPOCON_1_21 as StdCombo with uid="UUDBDOJQXS",rtseq=14,rtrep=.f.,left=125,top=6,width=163,height=21;
    , ToolTipText = "Tipo filtro (cliente/fornitore/agente/nominativo/debitore creditore)";
    , HelpContextID = 249918922;
    , cFormVar="w_TIPOCON",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Nominativo,"+"Debitore creditore diverso,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCON_1_21.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    iif(this.value =5,'B',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oTIPOCON_1_21.GetRadio()
    this.Parent.oContained.w_TIPOCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCON_1_21.SetRadio()
    this.Parent.oContained.w_TIPOCON=trim(this.Parent.oContained.w_TIPOCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCON=='C',1,;
      iif(this.Parent.oContained.w_TIPOCON=='F',2,;
      iif(this.Parent.oContained.w_TIPOCON=='A',3,;
      iif(this.Parent.oContained.w_TIPOCON=='N',4,;
      iif(this.Parent.oContained.w_TIPOCON=='B',5,;
      iif(this.Parent.oContained.w_TIPOCON=='T',6,;
      0))))))
  endfunc

  func oTIPOCON_1_21.mHide()
    with this.Parent.oContained
      return (Isahr())
    endwith
  endfunc

  func oTIPOCON_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PBANCODI)
        bRes2=.link_1_2('Full')
      endif
      if .not. empty(.w_PEANCODI)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oPBDEBCODI_1_22 as StdField with uid="NPAEUOPGJL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PBDEBCODI", cQueryName = "PBDEBCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 184563766,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="Gsar_bdc", oKey_1_1="NOCODICE", oKey_1_2="this.w_PBDEBCODI"

  func oPBDEBCODI_1_22.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFANT')
    endwith
  endfunc

  func oPBDEBCODI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBDEBCODI_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBDEBCODI_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPBDEBCODI_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oPBDEBCODI_1_22.mZoomOnZoom
    local i_obj
    i_obj=Gsar_bdc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PBDEBCODI
     i_obj.ecpSave()
  endproc

  add object oPEDEBCODI_1_23 as StdField with uid="WFANQGFXIC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PEDEBCODI", cQueryName = "PEDEBCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 184562998,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="Gsar_bdc", oKey_1_1="NOCODICE", oKey_1_2="this.w_PEDEBCODI"

  func oPEDEBCODI_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFANT')
    endwith
  endfunc

  func oPEDEBCODI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEDEBCODI_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEDEBCODI_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPEDEBCODI_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsar_bdc',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oPEDEBCODI_1_23.mZoomOnZoom
    local i_obj
    i_obj=Gsar_bdc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PEDEBCODI
     i_obj.ecpSave()
  endproc

  add object oDESCR7_1_26 as StdField with uid="BYHPSZFEEW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCR7", cQueryName = "DESCR7",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 100747978,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=40, InputMask=replicate('X',40)

  func oDESCR7_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFANT')
    endwith
  endfunc

  add object oDESCR8_1_27 as StdField with uid="YZIFRRUOAV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCR8", cQueryName = "DESCR8",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83970762,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=75, InputMask=replicate('X',40)

  func oDESCR8_1_27.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFANT')
    endwith
  endfunc

  add object oDESCR6_1_28 as StdField with uid="EQHQIMCIZR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCR6", cQueryName = "DESCR6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117525194,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=272, Top=75, InputMask=replicate('X',40)

  func oDESCR6_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON=' ' OR .w_TIPOCON$'CFABT')
    endwith
  endfunc

  add object oPENOCODI_1_29 as StdField with uid="SKJCIEGOVJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PENOCODI", cQueryName = "PENOCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 18507327,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_PENOCODI"

  func oPENOCODI_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON$'CFABT')
    endwith
  endfunc

  func oPENOCODI_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oPENOCODI_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPENOCODI_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPENOCODI_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oPENOCODI_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_PENOCODI
     i_obj.ecpSave()
  endproc

  add object oStr_1_6 as StdString with uid="ZTAJHPVLMP",Visible=.t., Left=6, Top=117,;
    Alignment=1, Width=115, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RCEYAMNBEB",Visible=.t., Left=17, Top=11,;
    Alignment=1, Width=104, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MAJRIEMKEU",Visible=.t., Left=17, Top=43,;
    Alignment=1, Width=104, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON='T')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="XJEFRRYNMW",Visible=.t., Left=19, Top=79,;
    Alignment=1, Width=102, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOCON='T')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_sva','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
