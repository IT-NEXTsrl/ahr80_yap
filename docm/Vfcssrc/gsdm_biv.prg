* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_biv                                                        *
*              Test connessione infinity                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-09-13                                                      *
* Last revis.: 2010-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_biv",oParentObject)
return(i_retval)

define class tgsdm_biv as StdBatch
  * --- Local variables
  w_IDPSSW = space(128)
  w_RETURNMSG = space(254)
  w_ERRORMSG = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Istanzia DLL
    this.w_IDPSSW = CifraCnf( ALLTRIM(this.oParentObject.w_CDIDPSSW) , "D" )
    this.w_RETURNMSG = ExecuteWSMethods("CheckUserPwd",, this.oParentObject.w_CDINFURL, this.oParentObject.w_CDIDUSER, this.w_IDPSSW)
    if Left(Lower(this.w_RETURNMSG),3)="ko:"
      * --- Restituisce messaggio di errore
      this.w_ERRORMSG = "[Infinity] "+IIF(Left(Lower(this.w_RETURNMSG),3)="ko:",SUBSTR(this.w_RETURNMSG,4),"Impossibile effettuare la connessione")
      ah_ErrorMsg(this.w_ERRORMSG, "!")
    else
      ah_ErrorMsg("Test di connessione eseguito con successo", "i")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
