* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bac                                                        *
*              Recupero e aggiornamento classi SOS                             *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-26                                                      *
* Last revis.: 2011-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipoOp,pVARCONS,pVARCLI,pVARURLWS
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bac",oParentObject,m.pTipoOp,m.pVARCONS,m.pVARCLI,m.pVARURLWS)
return(i_retval)

define class tgsdm_bac as StdBatch
  * --- Local variables
  pTipoOp = space(1)
  pVARCONS = space(10)
  pVARCLI = space(10)
  pVARURLWS = space(10)
  w_GUID = space(36)
  w_WS = .NULL.
  w_ELCLASDOC = space(0)
  w_ELKEYSCLA = space(0)
  oXML = .NULL.
  w_RootNode = .NULL.
  w_CHILDREN = 0
  w_CODCLASSE = space(10)
  w_NUMRIGA = 0
  w_URL = space(254)
  w_TOTCLAS = 0
  w_TOTCLASUPD = 0
  w_TOTKEYS = 0
  w_TOTKEYSUPD = 0
  w_WORKSTAT = space(15)
  w_CODCONS = space(10)
  w_CODCLI = space(10)
  w_CODSOG = space(5)
  w_Mess = .NULL.
  w_Par = .NULL.
  * --- WorkFile variables
  CLASMSOS_idx=0
  CLASDSOS_idx=0
  CONTROPA_idx=0
  PAR_SOST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento/Inserimento delle classi documentali di SOStitutiva
    this.w_WORKSTAT = substr(sys(0),1,at("#",sys(0))-1)
    this.w_URL = " "
    * --- Ricerco l'indirizzo del Web Service, in base alla seguente priorit�:
    *     
    *      1. Per codice azienda e codice utente;
    *      2. Per codice azienda e codice workstation
    *      3. Per codice azienda
    *     
    *     Ovviamente appena � trovato un Web services le combinazioni rimanenti vengono scartate.
    * --- Read from PAR_SOST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_SOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG"+;
        " from "+i_cTable+" PAR_SOST where ";
            +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and PSCODUTE = "+cp_ToStrODBC(i_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG;
        from (i_cTable) where;
            PSCODAZI = i_CODAZI;
            and PSCODUTE = i_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
      this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
      this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
      this.w_CODSOG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_URL) or (empty(this.w_CODCONS) and vartype(this.pTipoOp)="C")
      * --- Read from PAR_SOST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_SOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG"+;
          " from "+i_cTable+" PAR_SOST where ";
              +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and PSWORKST = "+cp_ToStrODBC(this.w_WORKSTAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG;
          from (i_cTable) where;
              PSCODAZI = i_CODAZI;
              and PSWORKST = this.w_WORKSTAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
        this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
        this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
        this.w_CODSOG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(this.w_URL) or (empty(this.w_CODCONS) and vartype(this.pTipoOp)="C")
        * --- Read from PAR_SOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_SOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_SOST_idx,2],.t.,this.PAR_SOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG"+;
            " from "+i_cTable+" PAR_SOST where ";
                +"PSCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and PSCODUTE = "+cp_ToStrODBC(0);
                +" and PSWORKST = "+cp_ToStrODBC(space(15));
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PSINFWEB,PSCODCON,PSCODCLI,PSCODSOG;
            from (i_cTable) where;
                PSCODAZI = i_CODAZI;
                and PSCODUTE = 0;
                and PSWORKST = space(15);
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_URL = NVL(cp_ToDate(_read_.PSINFWEB),cp_NullValue(_read_.PSINFWEB))
          this.w_CODCONS = NVL(cp_ToDate(_read_.PSCODCON),cp_NullValue(_read_.PSCODCON))
          this.w_CODCLI = NVL(cp_ToDate(_read_.PSCODCLI),cp_NullValue(_read_.PSCODCLI))
          this.w_CODSOG = NVL(cp_ToDate(_read_.PSCODSOG),cp_NullValue(_read_.PSCODSOG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0
          AH_ERRORMSG("Non sono stati definiti i parametri SOStitutiva, elaborazione interrotta.",48)
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if vartype(this.pTipoOp) # "C"
      this.w_WS = Conwebser(this.w_URL)
      * --- Recupero l'identificativo GUID
      if isnull(this.w_WS)
        i_retcode = 'stop'
        return
      endif
      this.w_GUID = RICGUID(this.w_WS)
      * --- Try
      local bErr_039E8760
      bErr_039E8760=bTrsErr
      this.Try_039E8760()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        Ah_ErrorMsg("Caricamento/aggiornamento delle classi documentali SOS non avvenuto.%0A causa di errori",64,"")
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_039E8760
      * --- End
    else
      ParentProp = "this.oParentObject"+alltrim(this.pVARCONS)
      &ParentProp = this.w_CODCONS
      ParentProp = "this.oParentObject"+alltrim(this.pVARCLI)
      &ParentProp = this.w_CODCLI
      ParentProp = "this.oParentObject"+alltrim(this.pVARURLWS)
      &ParentProp = this.w_URL
    endif
  endproc
  proc Try_039E8760()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if empty(this.w_GUID)
      * --- Raise
      i_Error="GUID non valido"
      return
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Marco tutte le classi presenti come obsolete
    * --- Write into CLASMSOS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CLASMSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASMSOS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CLASMSOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SZCLAOBS ="+cp_NullLink(cp_ToStrODBC("S"),'CLASMSOS','SZCLAOBS');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          SZCLAOBS = "S";
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TOTCLAS = i_rows
    * --- Recupero i dati di testata delle Classi Documentali SOS
    this.w_ELCLASDOC = this.w_WS.elencoclassidocumentali(this.w_CODCONS,this.w_CODSOG,this.w_GUID)
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    this.oXML = createobject("Msxml2.DOMDocument")
    if L_Err
      AH_ERRORMSG("Impossibile trovare componente MSXML2",48)
      i_retcode = 'stop'
      return
    endif
    ON ERROR &L_OLDERR
    * --- Load XML
    this.oXML.LoadXML(this.w_ELCLASDOC)     
    this.w_RootNode = this.oXML.DocumentElement
    if type("this.w_RootNode") = "O"
      Ah_Msg("Inserimento classi documentali SOS")
      do while this.w_CHILDREN < this.w_RootNode.ChildNodes.Length
        * --- Ciclo sui figli del nodo principale -->  'risposta'
        a=LoadCLaSos(this.w_RootNode)
        if ! a
          Ah_ErrorMsg("Errore nell'esecuzione della funzione ricorsiva.",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_CHILDREN = this.w_CHILDREN + 1
      enddo
    endif
    * --- W_CHILDREN parte da zero, questo per gestire correttamente il rootnode
    this.w_TOTCLASUPD = this.w_CHILDREN + 1 
    this.w_ELCLASDOC = ""
    * --- Recupero le chiavi relative alle classi Documentali SOS
    * --- Marco tutte le chiavi come obsolete
    * --- Write into CLASDSOS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CLASDSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CLASDSOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SZKEYOBS ="+cp_NullLink(cp_ToStrODBC("S"),'CLASDSOS','SZKEYOBS');
          +i_ccchkf ;
      +" where ";
          +"SZTIPINT = "+cp_ToStrODBC("LI");
             )
    else
      update (i_cTable) set;
          SZKEYOBS = "S";
          &i_ccchkf. ;
       where;
          SZTIPINT = "LI";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Nel calcolo del totale delle chiavi presenti, devo tenere conto anche di quelle di sistema e di tipologia diversa da 'Libera'
    this.w_TOTKEYS = i_rows + 10*this.w_TOTCLAS
     
 Local NUMRIGA
    * --- Select from CLASMSOS
    i_nConn=i_TableProp[this.CLASMSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASMSOS_idx,2],.t.,this.CLASMSOS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SZCODCLA,SZCLAOBS  from "+i_cTable+" CLASMSOS ";
          +" where SZCLAOBS<>'S'";
           ,"_Curs_CLASMSOS")
    else
      select SZCODCLA,SZCLAOBS from (i_cTable);
       where SZCLAOBS<>"S";
        into cursor _Curs_CLASMSOS
    endif
    if used('_Curs_CLASMSOS')
      select _Curs_CLASMSOS
      locate for 1=1
      do while not(eof())
      * --- Recuopero l'identificativo GUID
      this.w_GUID = RICGUID(this.w_WS)
      this.w_CHILDREN = 0
      this.w_CODCLASSE = alltrim(_Curs_CLASMSOS.SZCODCLA)
      this.w_ELKEYSCLA = this.w_WS.elencochiaviclassedocumentale(this.w_CODCONS,this.w_CODSOG,this.w_GUID,this.w_CODCLASSE)
      * --- Carico il nuovo XML delle chiavi
      this.oXML.LoadXML(this.w_ELKEYSCLA)     
      this.w_RootNode = this.oXML.DocumentElement
      NUMRIGA = 10
      if type("this.w_RootNode") = "O"
        Ah_Msg("Inserimento chiavi per la classe documentale %1",.t.,.f.,.f.,this.w_CODCLASSE)
        * --- Inserisco la prima chiave 'fissa' che il 'nomefile' per la classe in oggetto
        * --- Insert into CLASDSOS
        i_nConn=i_TableProp[this.CLASDSOS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
        i_commit = .f.
        local bErr_029AE6D8
        bErr_029AE6D8=bTrsErr
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLASDSOS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SZCODCLA"+",SZCODKEY"+",CPROWORD"+",SZDESKEY"+",SZTIPKEY"+",SZTIPINT"+",SZKEYOBS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODCLASSE),'CLASDSOS','SZCODCLA');
          +","+cp_NullLink(cp_ToStrODBC("NomeFile"),'CLASDSOS','SZCODKEY');
          +","+cp_NullLink(cp_ToStrODBC(NUMRIGA),'CLASDSOS','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("Nome file"),'CLASDSOS','SZDESKEY');
          +","+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
          +","+cp_NullLink(cp_ToStrODBC("NM"),'CLASDSOS','SZTIPINT');
          +","+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',this.w_CODCLASSE,'SZCODKEY',"NomeFile",'CPROWORD',NUMRIGA,'SZDESKEY',"Nome file",'SZTIPKEY',"varchar",'SZTIPINT',"NM",'SZKEYOBS',"N")
          insert into (i_cTable) (SZCODCLA,SZCODKEY,CPROWORD,SZDESKEY,SZTIPKEY,SZTIPINT,SZKEYOBS &i_ccchkf. );
             values (;
               this.w_CODCLASSE;
               ,"NomeFile";
               ,NUMRIGA;
               ,"Nome file";
               ,"varchar";
               ,"NM";
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          bTrsErr=bErr_029AE6D8
          * --- accept error
          bTrsErr=.f.
        endif
        do while this.w_CHILDREN < this.w_RootNode.ChildNodes.Length
          * --- Ciclo sui figli del nodo principale -->  'risposta'
          a=LoadCLaSos(this.w_RootNode," ",@NUMRIGA)
          if ! a
            Ah_ErrorMsg("Errore nell'esecuzione della funzione ricorsiva.",48,"")
            i_retcode = 'stop'
            return
          endif
          this.w_CHILDREN = this.w_CHILDREN + 1
        enddo
        this.w_NUMRIGA = NUMRIGA + 10
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
        select _Curs_CLASMSOS
        continue
      enddo
      use
    endif
    * --- Elimino i riferimenti agli oggetti usati
    this.w_WS = .null.
    this.oXML = .null.
    this.w_RootNode = .null.
    * --- commit
    cp_EndTrs(.t.)
    this.w_Mess=createobject("Ah_Message")
    this.w_Mess.AddMsgPartNL("Caricamento/aggiornamento delle classi documentali SOS terminato")     
    if this.w_TOTCLAS = 0
      this.w_Par = this.w_Mess.AddMsgPartNL("Classi  documentali SOStitutiva caricate %1")
      this.w_Par.AddParam(alltrim(str(this.w_TOTCLASUPD,6,0)))     
      this.w_Par = this.w_Mess.AddMsgPartNL("Chiavi caricate %1")
      this.w_Par.AddParam(alltrim(str(this.w_TOTKEYSUPD,6,0)))     
    else
      this.w_Par = this.w_Mess.AddMsgPartNL("Classi  documentali SOStitutiva caricate/aggiornate %1 su %2")
      this.w_Par.AddParam(alltrim(str(this.w_TOTCLASUPD,6,0)))     
      this.w_Par.AddParam(alltrim(str(this.w_TOTCLAS,6,0)))     
      this.w_Par = this.w_Mess.AddMsgPartNL("Chiavi caricate/aggiornate %1 su %2")
      this.w_Par.AddParam(alltrim(str(this.w_TOTKEYSUPD,6,0)))     
      this.w_Par.AddParam(alltrim(str(this.w_TOTKEYS,6,0)))     
    endif
    this.w_Mess.AH_ERRORMSG()     
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento delle chiavi fisse di coda (Hash, codice spedizione) e di quelle di sistema (Data archiviazione,lotto id, codice assoluto, ecc.))
    * --- Inserisco la chiave fissa  'CodSogPadre' per la classe in oggetto
    * --- Insert into CLASDSOS
    i_nConn=i_TableProp[this.CLASDSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
    i_commit = .f.
    local bErr_03AC3298
    bErr_03AC3298=bTrsErr
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLASDSOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SZCODCLA"+",SZCODKEY"+",CPROWORD"+",SZDESKEY"+",SZTIPKEY"+",SZTIPINT"+",SZKEYOBS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLASSE),'CLASDSOS','SZCODCLA');
      +","+cp_NullLink(cp_ToStrODBC("CodSogPad"),'CLASDSOS','SZCODKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'CLASDSOS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC("Codice del soggetto padre"),'CLASDSOS','SZDESKEY');
      +","+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
      +","+cp_NullLink(cp_ToStrODBC("LI"),'CLASDSOS','SZTIPINT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',this.w_CODCLASSE,'SZCODKEY',"CodSogPad",'CPROWORD',this.w_NUMRIGA,'SZDESKEY',"Codice del soggetto padre",'SZTIPKEY',"varchar",'SZTIPINT',"LI",'SZKEYOBS',"N")
      insert into (i_cTable) (SZCODCLA,SZCODKEY,CPROWORD,SZDESKEY,SZTIPKEY,SZTIPINT,SZKEYOBS &i_ccchkf. );
         values (;
           this.w_CODCLASSE;
           ,"CodSogPad";
           ,this.w_NUMRIGA;
           ,"Codice del soggetto padre";
           ,"varchar";
           ,"LI";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      bTrsErr=bErr_03AC3298
      * --- Write into CLASDSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLASDSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLASDSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZCODKEY ="+cp_NullLink(cp_ToStrODBC("CodSogPad"),'CLASDSOS','SZCODKEY');
        +",SZDESKEY ="+cp_NullLink(cp_ToStrODBC("Codice del soggetto padre"),'CLASDSOS','SZDESKEY');
        +",SZTIPKEY ="+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
        +",SZTIPINT ="+cp_NullLink(cp_ToStrODBC("LI"),'CLASDSOS','SZTIPINT');
        +",SZKEYOBS ="+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(this.w_CODCLASSE);
            +" and CPROWORD = "+cp_ToStrODBC(this.w_NUMRIGA);
               )
      else
        update (i_cTable) set;
            SZCODKEY = "CodSogPad";
            ,SZDESKEY = "Codice del soggetto padre";
            ,SZTIPKEY = "varchar";
            ,SZTIPINT = "LI";
            ,SZKEYOBS = "N";
            &i_ccchkf. ;
         where;
            SZCODCLA = this.w_CODCLASSE;
            and CPROWORD = this.w_NUMRIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_NUMRIGA = this.w_NUMRIGA + 10
    * --- Inserisco la chiave fissa  'CodSog' per la classe in oggetto
    * --- Insert into CLASDSOS
    i_nConn=i_TableProp[this.CLASDSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
    i_commit = .f.
    local bErr_03AC7E10
    bErr_03AC7E10=bTrsErr
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLASDSOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SZCODCLA"+",SZCODKEY"+",CPROWORD"+",SZDESKEY"+",SZTIPKEY"+",SZTIPINT"+",SZKEYOBS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLASSE),'CLASDSOS','SZCODCLA');
      +","+cp_NullLink(cp_ToStrODBC("CodSog"),'CLASDSOS','SZCODKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'CLASDSOS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC("Codice del soggetto"),'CLASDSOS','SZDESKEY');
      +","+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
      +","+cp_NullLink(cp_ToStrODBC("LI"),'CLASDSOS','SZTIPINT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',this.w_CODCLASSE,'SZCODKEY',"CodSog",'CPROWORD',this.w_NUMRIGA,'SZDESKEY',"Codice del soggetto",'SZTIPKEY',"varchar",'SZTIPINT',"LI",'SZKEYOBS',"N")
      insert into (i_cTable) (SZCODCLA,SZCODKEY,CPROWORD,SZDESKEY,SZTIPKEY,SZTIPINT,SZKEYOBS &i_ccchkf. );
         values (;
           this.w_CODCLASSE;
           ,"CodSog";
           ,this.w_NUMRIGA;
           ,"Codice del soggetto";
           ,"varchar";
           ,"LI";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      bTrsErr=bErr_03AC7E10
      * --- Write into CLASDSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLASDSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLASDSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZCODKEY ="+cp_NullLink(cp_ToStrODBC("CodSog"),'CLASDSOS','SZCODKEY');
        +",SZDESKEY ="+cp_NullLink(cp_ToStrODBC("Codice del soggetto"),'CLASDSOS','SZDESKEY');
        +",SZTIPKEY ="+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
        +",SZTIPINT ="+cp_NullLink(cp_ToStrODBC("LI"),'CLASDSOS','SZTIPINT');
        +",SZKEYOBS ="+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(this.w_CODCLASSE);
            +" and CPROWORD = "+cp_ToStrODBC(this.w_NUMRIGA);
               )
      else
        update (i_cTable) set;
            SZCODKEY = "CodSog";
            ,SZDESKEY = "Codice del soggetto";
            ,SZTIPKEY = "varchar";
            ,SZTIPINT = "LI";
            ,SZKEYOBS = "N";
            &i_ccchkf. ;
         where;
            SZCODCLA = this.w_CODCLASSE;
            and CPROWORD = this.w_NUMRIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_NUMRIGA = this.w_NUMRIGA + 10
    * --- Posizione per inserimento chiave 'Hash'
    * --- Inserisco la chiave fissa  'Codice di spedizione' per la classe in oggetto
    * --- Insert into CLASDSOS
    i_nConn=i_TableProp[this.CLASDSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
    i_commit = .f.
    local bErr_03AC99A0
    bErr_03AC99A0=bTrsErr
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLASDSOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SZCODCLA"+",SZCODKEY"+",CPROWORD"+",SZDESKEY"+",SZTIPKEY"+",SZTIPINT"+",SZKEYOBS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLASSE),'CLASDSOS','SZCODCLA');
      +","+cp_NullLink(cp_ToStrODBC("Cod_Sped"),'CLASDSOS','SZCODKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'CLASDSOS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC("Codifica di spedizione"),'CLASDSOS','SZDESKEY');
      +","+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
      +","+cp_NullLink(cp_ToStrODBC("CS"),'CLASDSOS','SZTIPINT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',this.w_CODCLASSE,'SZCODKEY',"Cod_Sped",'CPROWORD',this.w_NUMRIGA,'SZDESKEY',"Codifica di spedizione",'SZTIPKEY',"varchar",'SZTIPINT',"CS",'SZKEYOBS',"N")
      insert into (i_cTable) (SZCODCLA,SZCODKEY,CPROWORD,SZDESKEY,SZTIPKEY,SZTIPINT,SZKEYOBS &i_ccchkf. );
         values (;
           this.w_CODCLASSE;
           ,"Cod_Sped";
           ,this.w_NUMRIGA;
           ,"Codifica di spedizione";
           ,"varchar";
           ,"CS";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      bTrsErr=bErr_03AC99A0
      * --- Write into CLASDSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLASDSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLASDSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZCODKEY ="+cp_NullLink(cp_ToStrODBC("Cod_Sped"),'CLASDSOS','SZCODKEY');
        +",SZDESKEY ="+cp_NullLink(cp_ToStrODBC("Codifica di spedizione"),'CLASDSOS','SZDESKEY');
        +",SZTIPKEY ="+cp_NullLink(cp_ToStrODBC("varchar"),'CLASDSOS','SZTIPKEY');
        +",SZTIPINT ="+cp_NullLink(cp_ToStrODBC("CS"),'CLASDSOS','SZTIPINT');
        +",SZKEYOBS ="+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(this.w_CODCLASSE);
            +" and CPROWORD = "+cp_ToStrODBC(this.w_NUMRIGA);
               )
      else
        update (i_cTable) set;
            SZCODKEY = "Cod_Sped";
            ,SZDESKEY = "Codifica di spedizione";
            ,SZTIPKEY = "varchar";
            ,SZTIPINT = "CS";
            ,SZKEYOBS = "N";
            &i_ccchkf. ;
         where;
            SZCODCLA = this.w_CODCLASSE;
            and CPROWORD = this.w_NUMRIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Array contenente le chiavi di Sistema
    Dimension SISKEY (7,3)
    * --- Inserisco le chiavi di sistema nell'array
     
 store "DataArcDoc" to SISKEY(1,1) 
 store "Data archiviazione documentale" to SISKEY(1,2) 
 store "varchar" to SISKEY(1,3) 
 * 
 store "DataConSos" to SISKEY(2,1) 
 store "Data conservazione sostitutiva" to SISKEY(2,2) 
 store "varchar" to SISKEY(2,3) 
 * 
 store "CodAssDoc" to SISKEY(3,1) 
 store "Codice assoluto documento" to SISKEY(3,2) 
 store "varchar" to SISKEY(3,3) 
 * 
 store "IdLotCons" to SISKEY(4,1) 
 store "Lotto di conservazione" to SISKEY(4,2) 
 store "varchar" to SISKEY(4,3) 
 * 
 store "Valore" to SISKEY(5,1) 
 store "Valore" to SISKEY(5,2) 
 store "varchar" to SISKEY(5,3) 
 * 
 store "Stato_Doc" to SISKEY(6,1) 
 store "Stato documento" to SISKEY(6,2) 
 store "varchar" to SISKEY(6,3) 
 * 
 store "CodErrore" to SISKEY(7,1) 
 store "Errore in spedizione" to SISKEY(7,2) 
 store "varchar" to SISKEY(7,3)
    For i=1 to alen(SISKEY,1)
    this.w_NUMRIGA = this.w_NUMRIGA + 10
    * --- Insert into CLASDSOS
    i_nConn=i_TableProp[this.CLASDSOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
    i_commit = .f.
    local bErr_03ACD990
    bErr_03ACD990=bTrsErr
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLASDSOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SZCODCLA"+",SZCODKEY"+",CPROWORD"+",SZDESKEY"+",SZTIPKEY"+",SZTIPINT"+",SZKEYOBS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLASSE),'CLASDSOS','SZCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(SISKEY(i,1)),'CLASDSOS','SZCODKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'CLASDSOS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(SISKEY(i,2)),'CLASDSOS','SZDESKEY');
      +","+cp_NullLink(cp_ToStrODBC(SISKEY(i,3)),'CLASDSOS','SZTIPKEY');
      +","+cp_NullLink(cp_ToStrODBC("SI"),'CLASDSOS','SZTIPINT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SZCODCLA',this.w_CODCLASSE,'SZCODKEY',SISKEY(i,1),'CPROWORD',this.w_NUMRIGA,'SZDESKEY',SISKEY(i,2),'SZTIPKEY',SISKEY(i,3),'SZTIPINT',"SI",'SZKEYOBS',"N")
      insert into (i_cTable) (SZCODCLA,SZCODKEY,CPROWORD,SZDESKEY,SZTIPKEY,SZTIPINT,SZKEYOBS &i_ccchkf. );
         values (;
           this.w_CODCLASSE;
           ,SISKEY(i,1);
           ,this.w_NUMRIGA;
           ,SISKEY(i,2);
           ,SISKEY(i,3);
           ,"SI";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      bTrsErr=bErr_03ACD990
      * --- Write into CLASDSOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLASDSOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLASDSOS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLASDSOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SZCODKEY ="+cp_NullLink(cp_ToStrODBC(SISKEY(i,1)),'CLASDSOS','SZCODKEY');
        +",SZDESKEY ="+cp_NullLink(cp_ToStrODBC(SISKEY(i,2)),'CLASDSOS','SZDESKEY');
        +",SZTIPKEY ="+cp_NullLink(cp_ToStrODBC(SISKEY(i,3)),'CLASDSOS','SZTIPKEY');
        +",SZTIPINT ="+cp_NullLink(cp_ToStrODBC("SI"),'CLASDSOS','SZTIPINT');
        +",SZKEYOBS ="+cp_NullLink(cp_ToStrODBC("N"),'CLASDSOS','SZKEYOBS');
            +i_ccchkf ;
        +" where ";
            +"SZCODCLA = "+cp_ToStrODBC(this.w_CODCLASSE);
            +" and CPROWORD = "+cp_ToStrODBC(this.w_NUMRIGA);
               )
      else
        update (i_cTable) set;
            SZCODKEY = SISKEY(i,1);
            ,SZDESKEY = SISKEY(i,2);
            ,SZTIPKEY = SISKEY(i,3);
            ,SZTIPINT = "SI";
            ,SZKEYOBS = "N";
            &i_ccchkf. ;
         where;
            SZCODCLA = this.w_CODCLASSE;
            and CPROWORD = this.w_NUMRIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    EndFor
    this.w_TOTKEYSUPD = this.w_TOTKEYSUPD + this.w_NUMRIGA/10
  endproc


  proc Init(oParentObject,pTipoOp,pVARCONS,pVARCLI,pVARURLWS)
    this.pTipoOp=pTipoOp
    this.pVARCONS=pVARCONS
    this.pVARCLI=pVARCLI
    this.pVARURLWS=pVARURLWS
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CLASMSOS'
    this.cWorkTables[2]='CLASDSOS'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='PAR_SOST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CLASMSOS')
      use in _Curs_CLASMSOS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipoOp,pVARCONS,pVARCLI,pVARURLWS"
endproc
