* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_bss                                                        *
*              Selezione scanner                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-23                                                      *
* Last revis.: 2005-11-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdm_bss",oParentObject,m.pExec)
return(i_retval)

define class tgsdm_bss as StdBatch
  * --- Local variables
  pExec = space(1)
  w_GEST = .NULL.
  w_PADRE = .NULL.
  w_NAME = space(250)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione Scanner di Default
    this.w_PADRE = This.oParentObject
    this.w_NAME = Space(250)
    l_Name = Space(255)
    do case
      case this.pExec = "I"
         
 DECLARE LONG TWAIN_GetSourceList IN Eztwain3.DLL 
 DECLARE INTEGER TWAIN_IsAvailable IN Eztwain3.dll 
 DECLARE LONG TWAIN_GetNextSourceName IN Eztwain3.DLL STRING @sName
        * --- Controllo se � disponibile la libreria Twain
        if TWAIN_IsAvailable() <> 0
          * --- Creo un elenco dei dispositivi disponibili
          if TWAIN_GetSourceList()<>0
            * --- Eseguo una scansione interrogando uno alla volta i dispositivi
            do while TWAIN_GetNextSourceName(@l_Name)<>0
              * --- Inserisco ogni dispositivo nello zoom con selezione
              this.w_NAME = LEFT(l_Name,AT(CHR(0),l_Name)-1)
              Select(this.oParentObject.w_ZOOMSCA.cCursor) 
 Insert Into (this.oParentObject.w_ZOOMSCA.cCursor) (SCANNER) Values (this.w_Name)
            enddo
          else
            Ah_ErrorMsg("Nessun dispositivo TWAIN installato")
          endif
        else
          Ah_ErrorMsg("Libreria TWAIN non installata")
        endif
      case this.pExec = "S"
        * --- Ho selezionato un dispositivo Twain e salvo la selezione
        this.w_GEST = this.oParentObject.oParentObject
        this.oParentObject.w_RES = 1
        Select SCANNER From (this.oParentObject.w_ZOOMSCA.cCursor) Where XCHK = 1 into cursor SCANNER
        if Reccount("SCANNER") > 1
          this.oParentObject.w_RES = -1
          i_retcode = 'stop'
          return
        else
          if Reccount("SCANNER") = 1
            Store SCANNER.SCANNER To this.w_GEST.w_SCANNER
          endif
        endif
        if Used("Scanner")
           
 Select Scanner 
 Use
        endif
    endcase
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
