* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kvs                                                        *
*              Classi SOStitutiva                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-22                                                      *
* Last revis.: 2009-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kvs",oParentObject))

* --- Class definition
define class tgsdm_kvs as StdForm
  Top    = 10
  Left   = 9

  * --- Standard Properties
  Width  = 654
  Height = 323
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-30"
  HelpContextID=48874601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  CLASMSOS_IDX = 0
  cPrg = "gsdm_kvs"
  cComment = "Classi SOStitutiva"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCLAS = space(10)
  w_DESCLA = space(40)
  w_CLASOBS = space(1)
  w_DATAGG = ctod('  /  /  ')
  w_KEYSOS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kvsPag1","gsdm_kvs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCLAS_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_KEYSOS = this.oPgFrm.Pages(1).oPag.KEYSOS
    DoDefault()
    proc Destroy()
      this.w_KEYSOS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CLASMSOS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCLAS=space(10)
      .w_DESCLA=space(40)
      .w_CLASOBS=space(1)
      .w_DATAGG=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCLAS))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.KEYSOS.Calculate()
    endwith
    this.DoRTCalc(2,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.KEYSOS.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.KEYSOS.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.KEYSOS.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLAS
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLASMSOS_IDX,3]
    i_lTable = "CLASMSOS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2], .t., this.CLASMSOS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLASMSOS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SZCODCLA like "+cp_ToStrODBC(trim(this.w_CODCLAS)+"%");

          i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA,SZCLAOBS,SZDATAGG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SZCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SZCODCLA',trim(this.w_CODCLAS))
          select SZCODCLA,SZDESCLA,SZCLAOBS,SZDATAGG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SZCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLAS)==trim(_Link_.SZCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLAS) and !this.bDontReportError
            deferred_cp_zoom('CLASMSOS','*','SZCODCLA',cp_AbsName(oSource.parent,'oCODCLAS_1_2'),i_cWhere,'',"Elenco classi SOS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA,SZCLAOBS,SZDATAGG";
                     +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',oSource.xKey(1))
            select SZCODCLA,SZDESCLA,SZCLAOBS,SZDATAGG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZDESCLA,SZCLAOBS,SZDATAGG";
                   +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(this.w_CODCLAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',this.w_CODCLAS)
            select SZCODCLA,SZDESCLA,SZCLAOBS,SZDATAGG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLAS = NVL(_Link_.SZCODCLA,space(10))
      this.w_DESCLA = NVL(_Link_.SZDESCLA,space(40))
      this.w_CLASOBS = NVL(_Link_.SZCLAOBS,space(1))
      this.w_DATAGG = NVL(cp_ToDate(_Link_.SZDATAGG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLAS = space(10)
      endif
      this.w_DESCLA = space(40)
      this.w_CLASOBS = space(1)
      this.w_DATAGG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])+'\'+cp_ToStr(_Link_.SZCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLASMSOS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCLAS_1_2.value==this.w_CODCLAS)
      this.oPgFrm.Page1.oPag.oCODCLAS_1_2.value=this.w_CODCLAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_4.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_4.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASOBS_1_5.RadioValue()==this.w_CLASOBS)
      this.oPgFrm.Page1.oPag.oCLASOBS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAGG_1_6.value==this.w_DATAGG)
      this.oPgFrm.Page1.oPag.oDATAGG_1_6.value=this.w_DATAGG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_kvsPag1 as StdContainer
  Width  = 650
  height = 323
  stdWidth  = 650
  stdheight = 323
  resizeXpos=224
  resizeYpos=226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCLAS_1_2 as StdField with uid="YEDKNWDCCV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCLAS", cQueryName = "CODCLAS",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice della classe SOS",;
    HelpContextID = 52285222,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=101, Top=14, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLASMSOS", oKey_1_1="SZCODCLA", oKey_1_2="this.w_CODCLAS"

  func oCODCLAS_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLAS_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLAS_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLASMSOS','*','SZCODCLA',cp_AbsName(this.parent,'oCODCLAS_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco classi SOS",'',this.parent.oContained
  endproc

  add object oDESCLA_1_4 as StdField with uid="SNYJCBBMFM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 216091338,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=185, Top=14, InputMask=replicate('X',40)

  add object oCLASOBS_1_5 as StdCheck with uid="HDUTCQJMLN",rtseq=3,rtrep=.f.,left=103, top=46, caption="Classe obsoleta",;
    ToolTipText = "Classe obsoleta",;
    HelpContextID = 73243686,;
    cFormVar="w_CLASOBS", bObbl = .f. , nPag = 1;
    , enabled=.f.;
   , bGlobalFont=.t.


  func oCLASOBS_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCLASOBS_1_5.GetRadio()
    this.Parent.oContained.w_CLASOBS = this.RadioValue()
    return .t.
  endfunc

  func oCLASOBS_1_5.SetRadio()
    this.Parent.oContained.w_CLASOBS=trim(this.Parent.oContained.w_CLASOBS)
    this.value = ;
      iif(this.Parent.oContained.w_CLASOBS=='S',1,;
      0)
  endfunc

  add object oDATAGG_1_6 as StdField with uid="XKZKOCIJUW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAGG", cQueryName = "DATAGG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo aggiornamento della classe SOS",;
    HelpContextID = 120798922,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=402, Top=46


  add object KEYSOS as cp_zoombox with uid="FQJWBWTZRJ",left=2, top=84, width=638,height=233,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSDM_KVS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,cTable="CLASDSOS",cMenuFile="",cZoomOnZoom="",bQueryOnLoad=.f.,bRetriveAllRows=.f.,;
    cEvent = "w_CODCLAS Changed",;
    nPag=1;
    , HelpContextID = 129123046

  add object oStr_1_1 as StdString with uid="EWFRUJUHDX",Visible=.t., Left=6, Top=14,;
    Alignment=1, Width=91, Height=18,;
    Caption="Classe SOS:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="IIOXJATDIL",Visible=.t., Left=255, Top=46,;
    Alignment=1, Width=144, Height=18,;
    Caption="Ultimo aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="WPJFQOPDMD",left=2, top=76, width=643,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kvs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
