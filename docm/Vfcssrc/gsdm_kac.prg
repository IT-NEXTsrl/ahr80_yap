* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kac                                                        *
*              Aggiorna classi documentali SOStitutiva                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-26                                                      *
* Last revis.: 2009-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kac",oParentObject))

* --- Class definition
define class tgsdm_kac as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 555
  Height = 160
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-16"
  HelpContextID=132760681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  PAR_SOST_IDX = 0
  cPrg = "gsdm_kac"
  cComment = "Aggiorna classi documentali SOStitutiva"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_WEBSERV = space(0)
  w_CODCON = space(15)
  w_CODCLI = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kacPag1","gsdm_kac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oWEBSERV_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAR_SOST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_WEBSERV=space(0)
      .w_CODCON=space(15)
      .w_CODCLI=space(15)
    endwith
    this.DoRTCalc(1,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_THUYAVQKNM()
    with this
          * --- Valorizzazione del codice conservatore e cliente
          GSDM_BAC(this;
              ,'RicercaParametri';
              ,'.w_CODCON';
              ,'.w_CODCLI';
              ,'.w_WEBSERV';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_THUYAVQKNM()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oWEBSERV_1_2.value==this.w_WEBSERV)
      this.oPgFrm.Page1.oPag.oWEBSERV_1_2.value=this.w_WEBSERV
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_6.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_6.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLI_1_7.value==this.w_CODCLI)
      this.oPgFrm.Page1.oPag.oCODCLI_1_7.value=this.w_CODCLI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_kacPag1 as StdContainer
  Width  = 551
  height = 160
  stdWidth  = 551
  stdheight = 160
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oWEBSERV_1_2 as StdMemo with uid="MXNPHBNJGE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_WEBSERV", cQueryName = "WEBSERV",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "URL del Web Server SOStitutiva",;
    HelpContextID = 247309926,;
   bGlobalFont=.t.,;
    Height=78, Width=348, Left=131, Top=60, readonly=.T.,tabstop=.f.,bdisablefrasimodello=.t.


  add object oBtn_1_3 as StdButton with uid="ACPPRTTOEK",left=495, top=107, width=48,height=45,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , HelpContextID = 226620694;
    , caption='A\<ggiorna';
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        do GSDM_BAC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODCON_1_6 as StdField with uid="MZTOYXFFHO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del conservatore",;
    HelpContextID = 78786778,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=131, Top=9, InputMask=replicate('X',15)

  add object oCODCLI_1_7 as StdField with uid="UCEPFMWZMT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del cliente",;
    HelpContextID = 165818586,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=131, Top=34, InputMask=replicate('X',15)

  add object oStr_1_1 as StdString with uid="BKNBGWXCFP",Visible=.t., Left=42, Top=61,;
    Alignment=1, Width=87, Height=18,;
    Caption="Web server:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="YBWERRWRAY",Visible=.t., Left=39, Top=36,;
    Alignment=1, Width=90, Height=18,;
    Caption="Cod. cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="OTBCLQUZJS",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=123, Height=18,;
    Caption="Cod. conservatore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kac','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
