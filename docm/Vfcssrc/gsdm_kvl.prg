* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_kvl                                                        *
*              Log esecuzione processo documentale                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-15                                                      *
* Last revis.: 2009-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdm_kvl",oParentObject))

* --- Class definition
define class tgsdm_kvl as StdForm
  Top    = 3
  Left   = 3

  * --- Standard Properties
  Width  = 877
  Height = 434
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-08"
  HelpContextID=219560855
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  PROMDOCU_IDX = 0
  CPUSERS_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsdm_kvl"
  cComment = "Log esecuzione processo documentale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SOLOSHOW = .F.
  w_ALLPROC = space(1)
  w_LPCODPRO = space(10)
  w_DESPRO = space(40)
  w_LPKEYIST = space(10)
  w_LPESEDAT = ctod('  /  /  ')
  w_LPNUMATT = 0
  w_LPESEUTE = 0
  w_INDICE = space(25)
  o_INDICE = space(25)
  w_DESUTE = space(20)
  w_LPTIPCON = space(1)
  o_LPTIPCON = space(1)
  w_LPCODCON = space(15)
  w_DESCON = space(40)
  w_LPCODAGE = space(15)
  w_LPNOCODI = space(20)
  w_DESAGE = space(40)
  w_LPCODDEBI = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_LPULTIST = 0
  w_MSGERR = space(0)
  w_OQRY = space(30)
  w_OREP = space(30)
  w_DESNO = space(40)
  w_NOFLGBEN = space(1)
  w_DESCDEB = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_ZoomLog = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsdm_kvl
  *--- Stampa log
  Procedure PrintLog
     Local oMRep
     oMRep = CreateObject("MultiReport")
     oMRep.AddReport(this, alltrim(THIS.w_orep), "", alltrim(THIS.w_OQRY),,,.t.)
     cp_Chprn(oMRep,"",this)
     oMRep = .null.
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_kvlPag1","gsdm_kvl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLPCODPRO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomLog = this.oPgFrm.Pages(1).oPag.ZoomLog
    DoDefault()
    proc Destroy()
      this.w_ZoomLog = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PROMDOCU'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='OFF_NOMI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SOLOSHOW=.f.
      .w_ALLPROC=space(1)
      .w_LPCODPRO=space(10)
      .w_DESPRO=space(40)
      .w_LPKEYIST=space(10)
      .w_LPESEDAT=ctod("  /  /  ")
      .w_LPNUMATT=0
      .w_LPESEUTE=0
      .w_INDICE=space(25)
      .w_DESUTE=space(20)
      .w_LPTIPCON=space(1)
      .w_LPCODCON=space(15)
      .w_DESCON=space(40)
      .w_LPCODAGE=space(15)
      .w_LPNOCODI=space(20)
      .w_DESAGE=space(40)
      .w_LPCODDEBI=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_LPULTIST=0
      .w_MSGERR=space(0)
      .w_OQRY=space(30)
      .w_OREP=space(30)
      .w_DESNO=space(40)
      .w_NOFLGBEN=space(1)
      .w_DESCDEB=space(40)
      .w_DATOBSO=ctod("  /  /  ")
        .w_SOLOSHOW = .F.
        .w_ALLPROC = ' '
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LPCODPRO))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,8,.f.)
        if not(empty(.w_LPESEUTE))
          .link_1_9('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomLog.Calculate()
        .w_INDICE = alltrim(.w_ZoomLog.GetVar('LPKEYIST'))+alltrim(str(.w_ZoomLog.GetVar('LPROWNUM'),10,0))
        .DoRTCalc(10,12,.f.)
        if not(empty(.w_LPCODCON))
          .link_1_18('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_LPCODAGE))
          .link_1_20('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_LPNOCODI))
          .link_1_21('Full')
        endif
          .DoRTCalc(16,16,.f.)
        .w_LPCODDEBI = SPACE(15)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_LPCODDEBI))
          .link_1_23('Full')
        endif
          .DoRTCalc(18,19,.f.)
        .w_MSGERR = .w_ZoomLOG.GetVar("LPMESSAG")
        .w_OQRY = '..\DOCM\EXE\QUERY\GSDM_KVL.VQR'
        .w_OREP = '..\DOCM\EXE\QUERY\GSDM_SVL.FRX'
    endwith
    this.DoRTCalc(23,26,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsdm_kvl
     *--- Forzo visible a True altrimenti la maschera non � visibile in certe occasioni
     this.visible=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomLog.Calculate()
        .DoRTCalc(1,8,.t.)
            .w_INDICE = alltrim(.w_ZoomLog.GetVar('LPKEYIST'))+alltrim(str(.w_ZoomLog.GetVar('LPROWNUM'),10,0))
        .DoRTCalc(10,16,.t.)
        if .o_LPTIPCON<>.w_LPTIPCON
            .w_LPCODDEBI = SPACE(15)
          .link_1_23('Full')
        endif
        .DoRTCalc(18,19,.t.)
            .w_MSGERR = .w_ZoomLOG.GetVar("LPMESSAG")
        if .o_INDICE<>.w_INDICE
          .Calculate_CDZNELYIQR()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomLog.Calculate()
    endwith
  return

  proc Calculate_CDZNELYIQR()
    with this
          * --- Cambio colore zoom_select
          Zoom_Select_color(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLPCODPRO_1_3.enabled = this.oPgFrm.Page1.oPag.oLPCODPRO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oLPESEDAT_1_7.enabled = this.oPgFrm.Page1.oPag.oLPESEDAT_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLPESEUTE_1_9.enabled = this.oPgFrm.Page1.oPag.oLPESEUTE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oLPTIPCON_1_17.enabled = this.oPgFrm.Page1.oPag.oLPTIPCON_1_17.mCond()
    this.oPgFrm.Page1.oPag.oLPCODCON_1_18.enabled = this.oPgFrm.Page1.oPag.oLPCODCON_1_18.mCond()
    this.oPgFrm.Page1.oPag.oLPCODAGE_1_20.enabled = this.oPgFrm.Page1.oPag.oLPCODAGE_1_20.mCond()
    this.oPgFrm.Page1.oPag.oLPNOCODI_1_21.enabled = this.oPgFrm.Page1.oPag.oLPNOCODI_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLPCODCON_1_18.visible=!this.oPgFrm.Page1.oPag.oLPCODCON_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_19.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_19.mHide()
    this.oPgFrm.Page1.oPag.oLPCODAGE_1_20.visible=!this.oPgFrm.Page1.oPag.oLPCODAGE_1_20.mHide()
    this.oPgFrm.Page1.oPag.oLPNOCODI_1_21.visible=!this.oPgFrm.Page1.oPag.oLPNOCODI_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_22.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_22.mHide()
    this.oPgFrm.Page1.oPag.oLPCODDEBI_1_23.visible=!this.oPgFrm.Page1.oPag.oLPCODDEBI_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESNO_1_33.visible=!this.oPgFrm.Page1.oPag.oDESNO_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESCDEB_1_35.visible=!this.oPgFrm.Page1.oPag.oDESCDEB_1_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomLog.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_CDZNELYIQR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsdm_kvl
    *Ripristino cursore __tmp__ di stampa, potrebbe essere stato
    *asfaltato dalla stampa del Dettaglio Processo, il cursore
    *DocTmp � stato creato nella funzione chkprocd
    If used('DocTmp') and cEvent='Done'
      If used('__tmp__')
        Select  __tmp__
        Use
      Endif
    *Trsaferisco i dati della stampa nel cursore __tmp__
      Select * from DocTmp into cursor __tmp__ nofilter
      Select  DocTmp
      Use
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LPCODPRO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_lTable = "PROMDOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2], .t., this.PROMDOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsdm_mpd',True,'PROMDOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDCODPRO like "+cp_ToStrODBC(trim(this.w_LPCODPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDCODPRO',trim(this.w_LPCODPRO))
          select PDCODPRO,PDDESPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODPRO)==trim(_Link_.PDCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LPCODPRO) and !this.bDontReportError
            deferred_cp_zoom('PROMDOCU','*','PDCODPRO',cp_AbsName(oSource.parent,'oLPCODPRO_1_3'),i_cWhere,'gsdm_mpd',"Processi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',oSource.xKey(1))
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODPRO,PDDESPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODPRO="+cp_ToStrODBC(this.w_LPCODPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODPRO',this.w_LPCODPRO)
            select PDCODPRO,PDDESPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODPRO = NVL(_Link_.PDCODPRO,space(10))
      this.w_DESPRO = NVL(_Link_.PDDESPRO,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODPRO = space(10)
      endif
      this.w_DESPRO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])+'\'+cp_ToStr(_Link_.PDCODPRO,1)
      cp_ShowWarn(i_cKey,this.PROMDOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPESEUTE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPESEUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_LPESEUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_LPESEUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LPESEUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oLPESEUTE_1_9'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPESEUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_LPESEUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_LPESEUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPESEUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LPESEUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPESEUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODCON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_LPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_LPTIPCON;
                     ,'ANCODICE',trim(this.w_LPCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_LPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_LPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_LPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LPCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oLPCODCON_1_18'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_LPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_LPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LPTIPCON;
                       ,'ANCODICE',this.w_LPCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODAGE
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_LPCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_LPCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LPCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oLPCODAGE_1_20'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_LPCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_LPCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODAGE = NVL(_Link_.AGCODAGE,space(15))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODAGE = space(15)
      endif
      this.w_DESAGE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPNOCODI
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPNOCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_LPNOCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_LPNOCODI))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPNOCODI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LPNOCODI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oLPNOCODI_1_21'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPNOCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_LPNOCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_LPNOCODI)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPNOCODI = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNO = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LPNOCODI = space(20)
      endif
      this.w_DESNO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPNOCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LPCODDEBI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LPCODDEBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDC',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_LPCODDEBI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_LPCODDEBI))
          select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LPCODDEBI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_LPCODDEBI)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_LPCODDEBI)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LPCODDEBI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oLPCODDEBI_1_23'),i_cWhere,'GSAR_BDC',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LPCODDEBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_LPCODDEBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_LPCODDEBI)
            select NOCODICE,NODESCRI,NODTOBSO,NOFLGBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LPCODDEBI = NVL(_Link_.NOCODICE,space(15))
      this.w_DESCDEB = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOFLGBEN = NVL(_Link_.NOFLGBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LPCODDEBI = space(15)
      endif
      this.w_DESCDEB = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NOFLGBEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice inserito � obsoleto")
        endif
        this.w_LPCODDEBI = space(15)
        this.w_DESCDEB = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NOFLGBEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LPCODDEBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLPCODPRO_1_3.value==this.w_LPCODPRO)
      this.oPgFrm.Page1.oPag.oLPCODPRO_1_3.value=this.w_LPCODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRO_1_5.value==this.w_DESPRO)
      this.oPgFrm.Page1.oPag.oDESPRO_1_5.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oLPESEDAT_1_7.value==this.w_LPESEDAT)
      this.oPgFrm.Page1.oPag.oLPESEDAT_1_7.value=this.w_LPESEDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oLPESEUTE_1_9.value==this.w_LPESEUTE)
      this.oPgFrm.Page1.oPag.oLPESEUTE_1_9.value=this.w_LPESEUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_15.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_15.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oLPTIPCON_1_17.RadioValue()==this.w_LPTIPCON)
      this.oPgFrm.Page1.oPag.oLPTIPCON_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODCON_1_18.value==this.w_LPCODCON)
      this.oPgFrm.Page1.oPag.oLPCODCON_1_18.value=this.w_LPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_19.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_19.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODAGE_1_20.value==this.w_LPCODAGE)
      this.oPgFrm.Page1.oPag.oLPCODAGE_1_20.value=this.w_LPCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oLPNOCODI_1_21.value==this.w_LPNOCODI)
      this.oPgFrm.Page1.oPag.oLPNOCODI_1_21.value=this.w_LPNOCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_22.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_22.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oLPCODDEBI_1_23.value==this.w_LPCODDEBI)
      this.oPgFrm.Page1.oPag.oLPCODDEBI_1_23.value=this.w_LPCODDEBI
    endif
    if not(this.oPgFrm.Page1.oPag.oMSGERR_1_29.value==this.w_MSGERR)
      this.oPgFrm.Page1.oPag.oMSGERR_1_29.value=this.w_MSGERR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNO_1_33.value==this.w_DESNO)
      this.oPgFrm.Page1.oPag.oDESNO_1_33.value=this.w_DESNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCDEB_1_35.value==this.w_DESCDEB)
      this.oPgFrm.Page1.oPag.oDESCDEB_1_35.value=this.w_DESCDEB
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_NOFLGBEN='B')  and not(.w_LPTIPCON<>'B')  and not(empty(.w_LPCODDEBI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLPCODDEBI_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice inserito � obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INDICE = this.w_INDICE
    this.o_LPTIPCON = this.w_LPTIPCON
    return

enddefine

* --- Define pages as container
define class tgsdm_kvlPag1 as StdContainer
  Width  = 873
  height = 434
  stdWidth  = 873
  stdheight = 434
  resizeXpos=571
  resizeYpos=219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLPCODPRO_1_3 as StdField with uid="SJAGKDMOJR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LPCODPRO", cQueryName = "LPCODPRO",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice processo documentale",;
    HelpContextID = 240533243,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=80, Top=7, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PROMDOCU", cZoomOnZoom="gsdm_mpd", oKey_1_1="PDCODPRO", oKey_1_2="this.w_LPCODPRO"

  func oLPCODPRO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  func oLPCODPRO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODPRO_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODPRO_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMDOCU','*','PDCODPRO',cp_AbsName(this.parent,'oLPCODPRO_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gsdm_mpd',"Processi documentali",'',this.parent.oContained
  endproc
  proc oLPCODPRO_1_3.mZoomOnZoom
    local i_obj
    i_obj=gsdm_mpd()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PDCODPRO=this.parent.oContained.w_LPCODPRO
     i_obj.ecpSave()
  endproc

  add object oDESPRO_1_5 as StdField with uid="BHEJCRDJDS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242502346,;
   bGlobalFont=.t.,;
    Height=21, Width=235, Left=177, Top=7, InputMask=replicate('X',40)

  add object oLPESEDAT_1_7 as StdField with uid="PISUMSZRTK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LPESEDAT", cQueryName = "LPESEDAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data esecuzione processo documentale",;
    HelpContextID = 172105462,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=79, Top=33

  func oLPESEDAT_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  add object oLPESEUTE_1_9 as StdField with uid="UHOZWCLAIU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LPESEUTE", cQueryName = "LPESEUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente",;
    HelpContextID = 155328261,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=583, Top=9, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_LPESEUTE"

  func oLPESEUTE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  func oLPESEUTE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPESEUTE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPESEUTE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oLPESEUTE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc


  add object ZoomLog as cp_zoombox with uid="HIIVRWIQIF",left=2, top=59, width=865,height=320,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,bQueryOnLoad=.t.,cTable="PRO_LOGP",cZoomFile="GSDM_KVL",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cMenuFile="",bRetriveAllRows=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 139312410


  add object oBtn_1_12 as StdButton with uid="DHYEPGAVVT",left=819, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 140387818;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_LPCODPRO))
      endwith
    endif
  endfunc

  add object oDESUTE_1_15 as StdField with uid="STMPKGVBOK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 139414218,;
   bGlobalFont=.t.,;
    Height=21, Width=172, Left=641, Top=9, InputMask=replicate('X',20)


  add object oLPTIPCON_1_17 as StdCombo with uid="FZYEPZHXYM",value=1,rtseq=11,rtrep=.f.,left=238,top=32,width=151,height=21;
    , ToolTipText = "Tipo intestatario (cliente/fornitore/nessuno)";
    , HelpContextID = 90493188;
    , cFormVar="w_LPTIPCON",RowSource=""+"No filtro,"+"Clienti,"+"Fornitori,"+"Agenti,"+"Nominativi,"+"Debitori creditori diversi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLPTIPCON_1_17.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    iif(this.value =5,'N',;
    iif(this.value =6,'B',;
    space(1))))))))
  endfunc
  func oLPTIPCON_1_17.GetRadio()
    this.Parent.oContained.w_LPTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oLPTIPCON_1_17.SetRadio()
    this.Parent.oContained.w_LPTIPCON=trim(this.Parent.oContained.w_LPTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_LPTIPCON=='',1,;
      iif(this.Parent.oContained.w_LPTIPCON=='C',2,;
      iif(this.Parent.oContained.w_LPTIPCON=='F',3,;
      iif(this.Parent.oContained.w_LPTIPCON=='A',4,;
      iif(this.Parent.oContained.w_LPTIPCON=='N',5,;
      iif(this.Parent.oContained.w_LPTIPCON=='B',6,;
      0))))))
  endfunc

  func oLPTIPCON_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW)
    endwith
   endif
  endfunc

  func oLPTIPCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_LPCODCON)
        bRes2=.link_1_18('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oLPCODCON_1_18 as StdField with uid="HEPHXKOQUN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_LPCODCON", cQueryName = "LPCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestario",;
    HelpContextID = 78233860,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=380, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_LPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_LPCODCON"

  func oLPCODCON_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW and not empty(.w_LPTIPCON))
    endwith
   endif
  endfunc

  func oLPCODCON_1_18.mHide()
    with this.Parent.oContained
      return (! .w_LPTIPCON $ 'C-F')
    endwith
  endfunc

  func oLPCODCON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODCON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODCON_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_LPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_LPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oLPCODCON_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON_1_19 as StdField with uid="HBYEYNFBWS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 263277258,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=514, Top=33, InputMask=replicate('X',40)

  func oDESCON_1_19.mHide()
    with this.Parent.oContained
      return (! .w_LPTIPCON $ 'C-F')
    endwith
  endfunc

  add object oLPCODAGE_1_20 as StdField with uid="AHSBMOJZBT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_LPCODAGE", cQueryName = "LPCODAGE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 223756037,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=380, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_LPCODAGE"

  func oLPCODAGE_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW and not empty(.w_LPTIPCON))
    endwith
   endif
  endfunc

  func oLPCODAGE_1_20.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'A')
    endwith
  endfunc

  func oLPCODAGE_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODAGE_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODAGE_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oLPCODAGE_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oLPNOCODI_1_21 as StdField with uid="UEOUBXLYNB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_LPNOCODI", cQueryName = "LPNOCODI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice del nominatico",;
    HelpContextID = 258313985,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=380, Top=32, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_LPNOCODI"

  func oLPNOCODI_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_SOLOSHOW and not empty(.w_LPTIPCON))
    endwith
   endif
  endfunc

  func oLPNOCODI_1_21.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'N')
    endwith
  endfunc

  func oLPNOCODI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPNOCODI_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPNOCODI_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oLPNOCODI_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oLPNOCODI_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_LPNOCODI
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_22 as StdField with uid="FXIDPBVVUW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 154356426,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=514, Top=33, InputMask=replicate('X',40)

  func oDESAGE_1_22.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'A')
    endwith
  endfunc

  add object oLPCODDEBI_1_23 as StdField with uid="HIXWZHYNYU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_LPCODDEBI", cQueryName = "LPCODDEBI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice inserito � obsoleto",;
    ToolTipText = "Codice debitore creditore",;
    HelpContextID = 173423224,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=380, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_BDC", oKey_1_1="NOCODICE", oKey_1_2="this.w_LPCODDEBI"

  func oLPCODDEBI_1_23.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'B')
    endwith
  endfunc

  func oLPCODDEBI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oLPCODDEBI_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLPCODDEBI_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oLPCODDEBI_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDC',"Debitori creditori diversi",'GSAR5ACN.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oLPCODDEBI_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_LPCODDEBI
     i_obj.ecpSave()
  endproc


  add object oBtn_1_25 as StdButton with uid="YPTRHGBNFI",left=819, top=381, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226878278;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="PNJPSUXVQJ",left=762, top=381, width=48,height=45,;
    CpPicture="BMP\VISUALFLU.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la legenda per la lettura log processo documentale";
    , HelpContextID = 262357872;
    , caption='\<Legenda';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSDM_BPD(this.Parent.oContained,"LL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="DHWYCSERFT",left=705, top=381, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere stampare il log di esecuzione del processo documentale";
    , HelpContextID = 175520474;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        This.Parent.oContained.PrintLog()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMSGERR_1_29 as StdMemo with uid="GIPOWMLZVO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MSGERR", cQueryName = "MSGERR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Messaggio tecnico di errore",;
    HelpContextID = 192937018,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=39, Width=683, Left=12, Top=383

  add object oDESNO_1_33 as StdField with uid="DCFRKNFQHJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESNO", cQueryName = "DESNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39433526,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=540, Top=33, InputMask=replicate('X',40)

  func oDESNO_1_33.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON<>'N')
    endwith
  endfunc

  add object oDESCDEB_1_35 as StdField with uid="WUNNTZVNHX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCDEB", cQueryName = "DESCDEB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 157371082,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=515, Top=32, InputMask=replicate('X',40)

  func oDESCDEB_1_35.mHide()
    with this.Parent.oContained
      return (.w_LPTIPCON # 'B')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="IDFURKQPIT",Visible=.t., Left=8, Top=7,;
    Alignment=1, Width=67, Height=18,;
    Caption="Processo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="EXZPSDLQWD",Visible=.t., Left=27, Top=33,;
    Alignment=1, Width=47, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BHDQGUJCAS",Visible=.t., Left=511, Top=9,;
    Alignment=1, Width=69, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LPOQAJPVKV",Visible=.t., Left=164, Top=33,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_kvl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsdm_kvl
Procedure Zoom_Select_color(pParent)
   local l_macro,l_i,l_count,olderror,mess
   dimension Colonne[8]
   l_count=0
   For l_i=1 to pParent.w_ZoomLog.nFields
   do case
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKSTA'
       Colonne[1]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKMAI'
       Colonne[2]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKFAX'
       Colonne[3]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKCPZ'
       Colonne[4]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKPTL'
       Colonne[5]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKWWP'
       Colonne[6]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKARC'
       Colonne[7]=l_i
     case pParent.w_ZoomLog.cFields[l_i]=='LPCHKFIR'
       Colonne[8]=l_i
   endcase
   Next
   
   olderror=on ('Error')
   on error mess='Errore'
   For l_i = 1 to 8    
     If Colonne(l_i)>0
       l_macro=pParent.w_ZoomLog.grd.columns[Colonne(l_i)].dynamicforecolor
       if  pParent.w_ZoomLog.grd.columns[Colonne(l_i)].text1.value <> -1
         pParent.w_ZoomLog.grd.columns[Colonne(l_i)].text1.DisabledForeColor=rgb(0,0,0)
       else
         pParent.w_ZoomLog.grd.columns[Colonne(l_i)].text1.DisabledForeColor=&l_macro
       endif

       pParent.w_ZoomLog.grd.columns[Colonne(l_i)].text1.enabled=.F.

       l_macro=pParent.w_ZoomLog.grd.columns[Colonne(l_i)].dynamicbackcolor
       pParent.w_ZoomLog.grd.columns[Colonne(l_i)].text1.DisabledBackColor=&l_macro
     Endif
   Next
   on error &olderror
Endproc
* --- Fine Area Manuale
