* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_aed                                                        *
*              Parametri E.D.S.                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-10                                                      *
* Last revis.: 2011-01-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsdm_aed")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsdm_aed")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsdm_aed")
  return

* --- Class definition
define class tgsdm_aed as StdPCForm
  Width  = 308
  Height = 176
  Top    = 10
  Left   = 10
  cComment = "Parametri E.D.S."
  cPrg = "gsdm_aed"
  HelpContextID=76137577
  add object cnt as tcgsdm_aed
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsdm_aed as PCContext
  w_CDCODAZI = space(5)
  w_CDSERVER = space(20)
  w_CD__PORT = 0
  w_CD__USER = space(20)
  w_XXPASSWD = space(20)
  w_CDPASSWD = space(20)
  w_CDARCHIVE = space(20)
  proc Save(oFrom)
    this.w_CDCODAZI = oFrom.w_CDCODAZI
    this.w_CDSERVER = oFrom.w_CDSERVER
    this.w_CD__PORT = oFrom.w_CD__PORT
    this.w_CD__USER = oFrom.w_CD__USER
    this.w_XXPASSWD = oFrom.w_XXPASSWD
    this.w_CDPASSWD = oFrom.w_CDPASSWD
    this.w_CDARCHIVE = oFrom.w_CDARCHIVE
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CDCODAZI = this.w_CDCODAZI
    oTo.w_CDSERVER = this.w_CDSERVER
    oTo.w_CD__PORT = this.w_CD__PORT
    oTo.w_CD__USER = this.w_CD__USER
    oTo.w_XXPASSWD = this.w_XXPASSWD
    oTo.w_CDPASSWD = this.w_CDPASSWD
    oTo.w_CDARCHIVE = this.w_CDARCHIVE
    PCContext::Load(oTo)
enddefine

define class tcgsdm_aed as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 308
  Height = 176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-01-03"
  HelpContextID=76137577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  PARA_EDS_IDX = 0
  cFile = "PARA_EDS"
  cKeySelect = "CDCODAZI"
  cKeyWhere  = "CDCODAZI=this.w_CDCODAZI"
  cKeyWhereODBC = '"CDCODAZI="+cp_ToStrODBC(this.w_CDCODAZI)';

  cKeyWhereODBCqualified = '"PARA_EDS.CDCODAZI="+cp_ToStrODBC(this.w_CDCODAZI)';

  cPrg = "gsdm_aed"
  cComment = "Parametri E.D.S."
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CDCODAZI = space(5)
  w_CDSERVER = space(20)
  w_CD__PORT = 0
  w_CD__USER = space(20)
  w_XXPASSWD = space(20)
  w_CDPASSWD = space(20)
  w_CDARCHIVE = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_aedPag1","gsdm_aed",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 17690122
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDSERVER_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PARA_EDS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PARA_EDS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PARA_EDS_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsdm_aed'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PARA_EDS where CDCODAZI=KeySet.CDCODAZI
    *
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PARA_EDS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PARA_EDS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PARA_EDS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CDCODAZI',this.w_CDCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_XXPASSWD = space(20)
        .w_CDCODAZI = NVL(CDCODAZI,space(5))
        .w_CDSERVER = NVL(CDSERVER,space(20))
        .w_CD__PORT = NVL(CD__PORT,0)
        .w_CD__USER = NVL(CD__USER,space(20))
        .w_CDPASSWD = NVL(CDPASSWD,space(20))
        .w_CDARCHIVE = NVL(CDARCHIVE,space(20))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
        cp_LoadRecExtFlds(this,'PARA_EDS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CDCODAZI = space(5)
      .w_CDSERVER = space(20)
      .w_CD__PORT = 0
      .w_CD__USER = space(20)
      .w_XXPASSWD = space(20)
      .w_CDPASSWD = space(20)
      .w_CDARCHIVE = space(20)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
      endif
    endwith
    cp_BlankRecExtFlds(this,'PARA_EDS')
    this.DoRTCalc(1,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCDSERVER_1_2.enabled = i_bVal
      .Page1.oPag.oCD__PORT_1_4.enabled = i_bVal
      .Page1.oPag.oCD__USER_1_6.enabled = i_bVal
      .Page1.oPag.oXXPASSWD_1_7.enabled = i_bVal
      .Page1.oPag.oCDARCHIVE_1_11.enabled = i_bVal
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
      .Page1.oPag.oBtn_1_17.enabled = .Page1.oPag.oBtn_1_17.mCond()
      .Page1.oPag.oObj_1_13.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PARA_EDS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCODAZI,"CDCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDSERVER,"CDSERVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CD__PORT,"CD__PORT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CD__USER,"CD__USER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDPASSWD,"CDPASSWD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDARCHIVE,"CDARCHIVE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PARA_EDS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PARA_EDS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PARA_EDS')
        i_extval=cp_InsertValODBCExtFlds(this,'PARA_EDS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CDCODAZI,CDSERVER,CD__PORT,CD__USER,CDPASSWD"+;
                  ",CDARCHIVE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CDCODAZI)+;
                  ","+cp_ToStrODBC(this.w_CDSERVER)+;
                  ","+cp_ToStrODBC(this.w_CD__PORT)+;
                  ","+cp_ToStrODBC(this.w_CD__USER)+;
                  ","+cp_ToStrODBC(this.w_CDPASSWD)+;
                  ","+cp_ToStrODBC(this.w_CDARCHIVE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PARA_EDS')
        i_extval=cp_InsertValVFPExtFlds(this,'PARA_EDS')
        cp_CheckDeletedKey(i_cTable,0,'CDCODAZI',this.w_CDCODAZI)
        INSERT INTO (i_cTable);
              (CDCODAZI,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CDCODAZI;
                  ,this.w_CDSERVER;
                  ,this.w_CD__PORT;
                  ,this.w_CD__USER;
                  ,this.w_CDPASSWD;
                  ,this.w_CDARCHIVE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PARA_EDS_IDX,i_nConn)
      *
      * update PARA_EDS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PARA_EDS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CDSERVER="+cp_ToStrODBC(this.w_CDSERVER)+;
             ",CD__PORT="+cp_ToStrODBC(this.w_CD__PORT)+;
             ",CD__USER="+cp_ToStrODBC(this.w_CD__USER)+;
             ",CDPASSWD="+cp_ToStrODBC(this.w_CDPASSWD)+;
             ",CDARCHIVE="+cp_ToStrODBC(this.w_CDARCHIVE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PARA_EDS')
        i_cWhere = cp_PKFox(i_cTable  ,'CDCODAZI',this.w_CDCODAZI  )
        UPDATE (i_cTable) SET;
              CDSERVER=this.w_CDSERVER;
             ,CD__PORT=this.w_CD__PORT;
             ,CD__USER=this.w_CD__USER;
             ,CDPASSWD=this.w_CDPASSWD;
             ,CDARCHIVE=this.w_CDARCHIVE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PARA_EDS_IDX,i_nConn)
      *
      * delete PARA_EDS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CDCODAZI',this.w_CDCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
    endwith
  return

  proc Calculate_YSIDYAOZHI()
    with this
          * --- Cifra password
          .w_CDPASSWD = CifraCnf( ALLTRIM(.w_XXPASSWD) , 'C' )
    endwith
  endproc
  proc Calculate_EYFXYUGQKX()
    with this
          * --- Decifra password
          .w_XXPASSWD = CifraCnf( ALLTRIM(.w_CDPASSWD) , 'D' )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
        if lower(cEvent)==lower("Update start") or lower(cEvent)==lower("Insert start")
          .Calculate_YSIDYAOZHI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_EYFXYUGQKX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCDSERVER_1_2.value==this.w_CDSERVER)
      this.oPgFrm.Page1.oPag.oCDSERVER_1_2.value=this.w_CDSERVER
    endif
    if not(this.oPgFrm.Page1.oPag.oCD__PORT_1_4.value==this.w_CD__PORT)
      this.oPgFrm.Page1.oPag.oCD__PORT_1_4.value=this.w_CD__PORT
    endif
    if not(this.oPgFrm.Page1.oPag.oCD__USER_1_6.value==this.w_CD__USER)
      this.oPgFrm.Page1.oPag.oCD__USER_1_6.value=this.w_CD__USER
    endif
    if not(this.oPgFrm.Page1.oPag.oXXPASSWD_1_7.value==this.w_XXPASSWD)
      this.oPgFrm.Page1.oPag.oXXPASSWD_1_7.value=this.w_XXPASSWD
    endif
    if not(this.oPgFrm.Page1.oPag.oCDARCHIVE_1_11.value==this.w_CDARCHIVE)
      this.oPgFrm.Page1.oPag.oCDARCHIVE_1_11.value=this.w_CDARCHIVE
    endif
    cp_SetControlsValueExtFlds(this,'PARA_EDS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdm_aedPag1 as StdContainer
  Width  = 304
  height = 176
  stdWidth  = 304
  stdheight = 176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCDSERVER_1_2 as StdField with uid="PMFPBZPKOA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CDSERVER", cQueryName = "CDSERVER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Server E.D.S.",;
    HelpContextID = 115389560,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=142, Top=6, InputMask=replicate('X',20)

  add object oCD__PORT_1_4 as StdField with uid="FNBWPOTNAU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CD__PORT", cQueryName = "CD__PORT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Porta server E.D.S.",;
    HelpContextID = 266040442,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=142, Top=29, cSayPict='"999999"', cGetPict='"999999"'

  add object oCD__USER_1_6 as StdField with uid="GPKLNZGWVZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CD__USER", cQueryName = "CD__USER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Utente E.D.S.",;
    HelpContextID = 69956728,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=142, Top=52, InputMask=replicate('X',20)

  add object oXXPASSWD_1_7 as StdField with uid="CWSADSZUKK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_XXPASSWD", cQueryName = "XXPASSWD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Password utente E.D.S.",;
    HelpContextID = 65837498,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=142, Top=75, InputMask=replicate('X',20)

  add object oCDARCHIVE_1_11 as StdField with uid="FWHJRQLXFT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CDARCHIVE", cQueryName = "CDARCHIVE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Archivio E.D.S.",;
    HelpContextID = 134440756,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=142, Top=98, InputMask=replicate('X',20)


  add object oObj_1_13 as cp_setobjprop with uid="AUWPJKIPCT",left=1, top=183, width=204,height=19,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj="w_XXPASSWD",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 75696138


  add object oBtn_1_16 as StdButton with uid="PWCUCFWQQP",left=193, top=125, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte sulla macchina client";
    , HelpContextID = 76117018;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="LNMWHHBLEZ",left=247, top=125, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 246582790;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="WWGTAFYPGB",Visible=.t., Left=66, Top=10,;
    Alignment=1, Width=75, Height=18,;
    Caption="Server:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ERSDTWPJGO",Visible=.t., Left=35, Top=33,;
    Alignment=1, Width=106, Height=18,;
    Caption="Porta server:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="POMDVLONBR",Visible=.t., Left=65, Top=56,;
    Alignment=1, Width=76, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NNFWENYSKS",Visible=.t., Left=8, Top=79,;
    Alignment=1, Width=133, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="WJXBEJTPLP",Visible=.t., Left=59, Top=102,;
    Alignment=1, Width=82, Height=18,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_aed','PARA_EDS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CDCODAZI=PARA_EDS.CDCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
