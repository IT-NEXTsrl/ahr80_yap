* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdm_mpd                                                        *
*              Processi documentali                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-19                                                      *
* Last revis.: 2016-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsdm_mpd
* --- Se lancio con parametro 'IRDR' � un processo documentale
* --- lancaibile da InfoPublisher
PARAMETERS pTipProcesso
* --- Fine Area Manuale
return(createobject("tgsdm_mpd"))

* --- Class definition
define class tgsdm_mpd as StdTrsForm
  Top    = 3
  Left   = 8

  * --- Standard Properties
  Width  = 799
  Height = 478+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-01"
  HelpContextID=147440745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PROMDOCU_IDX = 0
  PRODDOCU_IDX = 0
  PROMCLAS_IDX = 0
  OUT_PUTS_IDX = 0
  INF_AQM_IDX = 0
  cFile = "PROMDOCU"
  cFileDetail = "PRODDOCU"
  cKeySelect = "PDCODPRO"
  cKeyWhere  = "PDCODPRO=this.w_PDCODPRO"
  cKeyDetail  = "PDCODPRO=this.w_PDCODPRO and PDREPORT=this.w_PDREPORT"
  cKeyWhereODBC = '"PDCODPRO="+cp_ToStrODBC(this.w_PDCODPRO)';

  cKeyDetailWhereODBC = '"PDCODPRO="+cp_ToStrODBC(this.w_PDCODPRO)';
      +'+" and PDREPORT="+cp_ToStrODBC(this.w_PDREPORT)';

  cKeyWhereODBCqualified = '"PRODDOCU.PDCODPRO="+cp_ToStrODBC(this.w_PDCODPRO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsdm_mpd"
  cComment = "Processi documentali"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  cAutoZoom = 'GSDM_MPD'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PDCODPRO = space(10)
  w_PDDESPRO = space(40)
  w_PDCLADOC = space(15)
  o_PDCLADOC = space(15)
  w_CDMODALL = space(1)
  o_CDMODALL = space(1)
  w_DCLADOC = space(40)
  w_PDCHKOBB = space(1)
  w_PDCHKLOG = space(1)
  w_PDCHKSTA = space(1)
  o_PDCHKSTA = space(1)
  w_PDCHKMAI = space(1)
  o_PDCHKMAI = space(1)
  w_PDCHKPEC = space(1)
  o_PDCHKPEC = space(1)
  w_PDCHKFAX = space(1)
  o_PDCHKFAX = space(1)
  w_PDCHKPTL = space(1)
  o_PDCHKPTL = space(1)
  w_PDCHKWWP = space(1)
  o_PDCHKWWP = space(1)
  w_PDCHKZCP = space(1)
  o_PDCHKZCP = space(1)
  w_PDCHKARC = space(1)
  o_PDCHKARC = space(1)
  w_PDCHKANT = space(1)
  w_PDRILSTA = space(1)
  w_PDRILMAI = space(1)
  w_PDRILPEC = space(1)
  w_PDRILFAX = space(1)
  w_PDRILPTL = space(1)
  w_PDRILWWP = space(1)
  w_PDRILZCP = space(1)
  w_PDRILARC = space(1)
  w_PDNMAXAL = 0
  w_PDREPORT = space(100)
  o_PDREPORT = space(100)
  w_PDDESREP = space(50)
  w_PDKEYPRO = space(100)
  w_PDKEYPRO = space(100)
  w_PDCONCON = space(60)
  o_PDCONCON = space(60)
  w_PDISOLAN = space(60)
  w_PDTIPRIF = space(2)
  w_PDCODSED = space(60)
  o_PDCODSED = space(60)
  w_PDCODORD = space(60)
  w_PDOGGETT = space(0)
  w_PD_TESTO = space(0)
  w_PDFILTRO = space(0)
  w_TIPOARCH = space(1)
  w_PDPROCES = space(3)
  w_FIRDIG = space(1)
  w_TIPRIFR = space(10)
  w_TIPRIFE = space(10)
  o_TIPRIFE = space(10)
  w_PDTIPRIA = space(1)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Children pointers
  GSDM_MAP = .NULL.
  GSDM_MDV = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsdm_mpd
  * --- variabile per determinare che tipo di processo deve essere attivato
  * --- se generico o infopublisher
  w_TipProcesso='   '
  
    * --- Per gestione sicurezza procedure con parametri
    func getSecuritycode()
      return(lower('GSDM_MPD'+iif(not empty(this.w_TipProcesso),','+this.w_TipProcesso,'')))
    EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROMDOCU','gsdm_mpd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdm_mpdPag1","gsdm_mpd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Processo")
      .Pages(1).HelpContextID = 15770469
      .Pages(2).addobject("oPag","tgsdm_mpdPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Autorizzazioni")
      .Pages(2).HelpContextID = 136512912
      .Pages(3).addobject("oPag","tgsdm_mpdPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Permessi Corporate Portal")
      .Pages(3).HelpContextID = 256479455
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPDCODPRO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsdm_mpd
    WITH THIS.PARENT
        .w_TipProcesso=iif(type("pTipProcesso")<>'C','   ',pTipProcesso)
        DO CASE
          CASE .w_TipProcesso = 'IRP'
            .cComment = cp_Translate('Processi documentali infopublisher')
            .cAutoZoom = 'GSDMIMPD'
        ENDCASE
    ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='OUT_PUTS'
    this.cWorkTables[3]='INF_AQM'
    this.cWorkTables[4]='PROMDOCU'
    this.cWorkTables[5]='PRODDOCU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROMDOCU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROMDOCU_IDX,3]
  return

  function CreateChildren()
    this.GSDM_MAP = CREATEOBJECT('stdDynamicChild',this,'GSDM_MAP',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    this.GSDM_MDV = CREATEOBJECT('stdDynamicChild',this,'GSDM_MDV',this.oPgFrm.Page3.oPag.oLinkPC_5_1)
    this.GSDM_MDV.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSDM_MAP)
      this.GSDM_MAP.DestroyChildrenChain()
      this.GSDM_MAP=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSDM_MDV)
      this.GSDM_MDV.DestroyChildrenChain()
      this.GSDM_MDV=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_5_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSDM_MAP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSDM_MDV.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSDM_MAP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSDM_MDV.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSDM_MAP.NewDocument()
    this.GSDM_MDV.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSDM_MAP.ChangeRow(this.cRowID+'      1',1;
             ,.w_PDCODPRO,"PECODPRO";
             )
      .GSDM_MDV.ChangeRow(this.cRowID+'      1',1;
             ,.w_PDCODPRO,"DECODICE";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PDCODPRO = NVL(PDCODPRO,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PROMDOCU where PDCODPRO=KeySet.PDCODPRO
    *
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2],this.bLoadRecFilter,this.PROMDOCU_IDX,"gsdm_mpd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROMDOCU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROMDOCU.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PRODDOCU.","PROMDOCU.")
      i_cTable = i_cTable+' PROMDOCU '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PDCODPRO',this.w_PDCODPRO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CDMODALL = space(1)
        .w_DCLADOC = space(40)
        .w_TIPOARCH = space(1)
        .w_FIRDIG = space(1)
        .w_PDCODPRO = NVL(PDCODPRO,space(10))
        .w_PDDESPRO = NVL(PDDESPRO,space(40))
        .w_PDCLADOC = NVL(PDCLADOC,space(15))
          if link_1_4_joined
            this.w_PDCLADOC = NVL(CDCODCLA104,NVL(this.w_PDCLADOC,space(15)))
            this.w_DCLADOC = NVL(CDDESCLA104,space(40))
            this.w_TIPOARCH = NVL(CDTIPARC104,space(1))
            this.w_CDMODALL = NVL(CDMODALL104,space(1))
            this.w_FIRDIG = NVL(CDFIRDIG104,space(1))
          else
          .link_1_4('Load')
          endif
        .w_PDCHKOBB = NVL(PDCHKOBB,space(1))
        .w_PDCHKLOG = NVL(PDCHKLOG,space(1))
        .w_PDCHKSTA = NVL(PDCHKSTA,space(1))
        .w_PDCHKMAI = NVL(PDCHKMAI,space(1))
        .w_PDCHKPEC = NVL(PDCHKPEC,space(1))
        .w_PDCHKFAX = NVL(PDCHKFAX,space(1))
        .w_PDCHKPTL = NVL(PDCHKPTL,space(1))
        .w_PDCHKWWP = NVL(PDCHKWWP,space(1))
        .w_PDCHKZCP = NVL(PDCHKZCP,space(1))
        .w_PDCHKARC = NVL(PDCHKARC,space(1))
        .w_PDCHKANT = NVL(PDCHKANT,space(1))
        .w_PDRILSTA = NVL(PDRILSTA,space(1))
        .w_PDRILMAI = NVL(PDRILMAI,space(1))
        .w_PDRILPEC = NVL(PDRILPEC,space(1))
        .w_PDRILFAX = NVL(PDRILFAX,space(1))
        .w_PDRILPTL = NVL(PDRILPTL,space(1))
        .w_PDRILWWP = NVL(PDRILWWP,space(1))
        .w_PDRILZCP = NVL(PDRILZCP,space(1))
        .w_PDRILARC = NVL(PDRILARC,space(1))
        .w_PDNMAXAL = NVL(PDNMAXAL,0)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_PDPROCES = NVL(PDPROCES,space(3))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'PROMDOCU')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PRODDOCU where PDCODPRO=KeySet.PDCODPRO
      *                            and PDREPORT=KeySet.PDREPORT
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PRODDOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODDOCU_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PRODDOCU')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PRODDOCU.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PRODDOCU"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'PDCODPRO',this.w_PDCODPRO  )
        select * from (i_cTable) PRODDOCU where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_PDREPORT = NVL(PDREPORT,space(100))
          * evitabile
          *.link_2_1('Load')
          .w_PDDESREP = NVL(PDDESREP,space(50))
          .w_PDKEYPRO = NVL(PDKEYPRO,space(100))
          .w_PDKEYPRO = NVL(PDKEYPRO,space(100))
          .w_PDCONCON = NVL(PDCONCON,space(60))
          .w_PDISOLAN = NVL(PDISOLAN,space(60))
          .w_PDTIPRIF = NVL(PDTIPRIF,space(2))
          .w_PDCODSED = NVL(PDCODSED,space(60))
          .w_PDCODORD = NVL(PDCODORD,space(60))
          .w_PDOGGETT = NVL(PDOGGETT,space(0))
          .w_PD_TESTO = NVL(PD_TESTO,space(0))
          .w_PDFILTRO = NVL(PDFILTRO,space(0))
        .w_TIPRIFR = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        .w_TIPRIFE = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
          .w_PDTIPRIA = NVL(PDTIPRIA,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PDREPORT with .w_PDREPORT
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsdm_mpd
    * --- Verifico se abilitare o meno il form "Permessi Corporate Portal"
    do GSDM_BRP with this,'A'
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PDCODPRO=space(10)
      .w_PDDESPRO=space(40)
      .w_PDCLADOC=space(15)
      .w_CDMODALL=space(1)
      .w_DCLADOC=space(40)
      .w_PDCHKOBB=space(1)
      .w_PDCHKLOG=space(1)
      .w_PDCHKSTA=space(1)
      .w_PDCHKMAI=space(1)
      .w_PDCHKPEC=space(1)
      .w_PDCHKFAX=space(1)
      .w_PDCHKPTL=space(1)
      .w_PDCHKWWP=space(1)
      .w_PDCHKZCP=space(1)
      .w_PDCHKARC=space(1)
      .w_PDCHKANT=space(1)
      .w_PDRILSTA=space(1)
      .w_PDRILMAI=space(1)
      .w_PDRILPEC=space(1)
      .w_PDRILFAX=space(1)
      .w_PDRILPTL=space(1)
      .w_PDRILWWP=space(1)
      .w_PDRILZCP=space(1)
      .w_PDRILARC=space(1)
      .w_PDNMAXAL=0
      .w_PDREPORT=space(100)
      .w_PDDESREP=space(50)
      .w_PDKEYPRO=space(100)
      .w_PDKEYPRO=space(100)
      .w_PDCONCON=space(60)
      .w_PDISOLAN=space(60)
      .w_PDTIPRIF=space(2)
      .w_PDCODSED=space(60)
      .w_PDCODORD=space(60)
      .w_PDOGGETT=space(0)
      .w_PD_TESTO=space(0)
      .w_PDFILTRO=space(0)
      .w_TIPOARCH=space(1)
      .w_PDPROCES=space(3)
      .w_FIRDIG=space(1)
      .w_TIPRIFR=space(10)
      .w_TIPRIFE=space(10)
      .w_PDTIPRIA=space(1)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_PDCLADOC))
         .link_1_4('Full')
        endif
        .DoRTCalc(4,5,.f.)
        .w_PDCHKOBB = 'N'
        .w_PDCHKLOG = 'N'
        .w_PDCHKSTA = 'N'
        .w_PDCHKMAI = 'N'
        .DoRTCalc(10,10,.f.)
        .w_PDCHKFAX = 'N'
        .w_PDCHKPTL = 'N'
        .w_PDCHKWWP = 'N'
        .w_PDCHKZCP = iif((g_IZCP$'SA' or g_CPIN='S') and .w_CDMODALL<>'S' And g_DMIP<>'S', .w_PDCHKZCP,'N')
        .w_PDCHKARC = IIF(empty(.w_PDCLADOC),'N',.w_PDCHKARC)
        .w_PDCHKANT = IIF(.w_PDCHKSTA='S', .w_PDCHKANT, 'N')
        .w_PDRILSTA = IIF(.w_PDCHKSTA='S',.w_PDRILSTA,'N')
        .w_PDRILMAI = iif(.w_TipProcesso='IRP' and .w_PDCHKMAI='S','S','N')
        .w_PDRILPEC = iif(.w_TipProcesso='IRP' and .w_PDCHKPEC='S','S','N')
        .w_PDRILFAX = IIF(.w_PDCHKFAX='S',.w_PDRILFAX,'N')
        .w_PDRILPTL = IIF(.w_PDCHKPTL='S',.w_PDRILPTL,'N')
        .w_PDRILWWP = IIF(.w_PDCHKWWP='S',.w_PDRILWWP,'N')
        .w_PDRILZCP = iif(.w_TipProcesso='IRP' and g_IZCP$'SA' and .w_PDCHKZCP='S','S','N')
        .w_PDRILARC = iif(.w_TipProcesso='IRP' and .w_PDCHKARC='S','S','N')
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_PDREPORT))
         .link_2_1('Full')
        endif
        .DoRTCalc(27,31,.f.)
        .w_PDTIPRIF = IIF (isahe() , .w_TIPRIFE , .w_TIPRIFR)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(33,38,.f.)
        .w_PDPROCES = .w_TipProcesso
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(40,40,.f.)
        .w_TIPRIFR = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        .w_TIPRIFE = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        .w_PDTIPRIA = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROMDOCU')
    this.DoRTCalc(44,47,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPDCODPRO_1_1.enabled = i_bVal
      .Page1.oPag.oPDDESPRO_1_3.enabled = i_bVal
      .Page1.oPag.oPDCLADOC_1_4.enabled = i_bVal
      .Page1.oPag.oPDCHKOBB_1_7.enabled = i_bVal
      .Page1.oPag.oPDCHKLOG_1_8.enabled = i_bVal
      .Page1.oPag.oPDCHKSTA_1_9.enabled = i_bVal
      .Page1.oPag.oPDCHKMAI_1_10.enabled = i_bVal
      .Page1.oPag.oPDCHKPEC_1_11.enabled = i_bVal
      .Page1.oPag.oPDCHKFAX_1_12.enabled = i_bVal
      .Page1.oPag.oPDCHKPTL_1_13.enabled = i_bVal
      .Page1.oPag.oPDCHKWWP_1_14.enabled = i_bVal
      .Page1.oPag.oPDCHKZCP_1_15.enabled = i_bVal
      .Page1.oPag.oPDCHKARC_1_16.enabled = i_bVal
      .Page1.oPag.oPDCHKANT_1_17.enabled = i_bVal
      .Page1.oPag.oPDRILSTA_1_18.enabled = i_bVal
      .Page1.oPag.oPDRILMAI_1_19.enabled = i_bVal
      .Page1.oPag.oPDRILPEC_1_20.enabled = i_bVal
      .Page1.oPag.oPDRILFAX_1_21.enabled = i_bVal
      .Page1.oPag.oPDRILPTL_1_22.enabled = i_bVal
      .Page1.oPag.oPDRILWWP_1_23.enabled = i_bVal
      .Page1.oPag.oPDRILZCP_1_24.enabled = i_bVal
      .Page1.oPag.oPDRILARC_1_25.enabled = i_bVal
      .Page1.oPag.oPDNMAXAL_1_26.enabled = i_bVal
      .Page1.oPag.oPDKEYPRO_2_3.enabled = i_bVal
      .Page1.oPag.oPDKEYPRO_2_4.enabled = i_bVal
      .Page1.oPag.oPDCONCON_2_5.enabled = i_bVal
      .Page1.oPag.oPDISOLAN_2_6.enabled = i_bVal
      .Page1.oPag.oPDCODSED_2_8.enabled = i_bVal
      .Page1.oPag.oPDCODORD_2_9.enabled = i_bVal
      .Page1.oPag.oPDOGGETT_2_10.enabled = i_bVal
      .Page1.oPag.oPD_TESTO_2_11.enabled = i_bVal
      .Page1.oPag.oPDFILTRO_2_12.enabled = i_bVal
      .Page1.oPag.oTIPRIFR_2_18.enabled = i_bVal
      .Page1.oPag.oTIPRIFE_2_19.enabled = i_bVal
      .Page1.oPag.oPDTIPRIA_2_20.enabled = i_bVal
      .Page1.oPag.oBtn_2_13.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPDCODPRO_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPDCODPRO_1_1.enabled = .t.
      endif
    endwith
    this.GSDM_MAP.SetStatus(i_cOp)
    this.GSDM_MDV.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PROMDOCU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSDM_MAP.SetChildrenStatus(i_cOp)
  *  this.GSDM_MDV.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODPRO,"PDCODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDDESPRO,"PDDESPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCLADOC,"PDCLADOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKOBB,"PDCHKOBB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKLOG,"PDCHKLOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKSTA,"PDCHKSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKMAI,"PDCHKMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKPEC,"PDCHKPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKFAX,"PDCHKFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKPTL,"PDCHKPTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKWWP,"PDCHKWWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKZCP,"PDCHKZCP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKARC,"PDCHKARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCHKANT,"PDCHKANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILSTA,"PDRILSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILMAI,"PDRILMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILPEC,"PDRILPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILFAX,"PDRILFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILPTL,"PDRILPTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILWWP,"PDRILWWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILZCP,"PDRILZCP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRILARC,"PDRILARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDNMAXAL,"PDNMAXAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDPROCES,"PDPROCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsdm_mpd
    * --- aggiunge alla Chiave ulteriore filtro su tipo IRDR
    IF EMPTY(i_cWhere)
      i_cWhere=' where 1=1'
    ENDIF
    i_cWhere=i_cWhere+" and PDPROCES='"+this.w_TipProcesso+"'"
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    i_lTable = "PROMDOCU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROMDOCU_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSDM_SPD with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PDREPORT C(100);
      ,t_PDDESREP C(50);
      ,t_PDKEYPRO C(100);
      ,t_PDCONCON C(60);
      ,t_PDISOLAN C(60);
      ,t_PDCODSED C(60);
      ,t_PDCODORD C(60);
      ,t_PDOGGETT M(10);
      ,t_PD_TESTO M(10);
      ,t_PDFILTRO M(10);
      ,t_TIPRIFR N(3);
      ,t_TIPRIFE N(3);
      ,t_PDTIPRIA N(3);
      ,PDREPORT C(100);
      ,t_PDTIPRIF C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsdm_mpdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPDREPORT_2_1.controlsource=this.cTrsName+'.t_PDREPORT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPDDESREP_2_2.controlsource=this.cTrsName+'.t_PDDESREP'
    this.oPgFRm.Page1.oPag.oPDKEYPRO_2_3.controlsource=this.cTrsName+'.t_PDKEYPRO'
    this.oPgFRm.Page1.oPag.oPDKEYPRO_2_4.controlsource=this.cTrsName+'.t_PDKEYPRO'
    this.oPgFRm.Page1.oPag.oPDCONCON_2_5.controlsource=this.cTrsName+'.t_PDCONCON'
    this.oPgFRm.Page1.oPag.oPDISOLAN_2_6.controlsource=this.cTrsName+'.t_PDISOLAN'
    this.oPgFRm.Page1.oPag.oPDCODSED_2_8.controlsource=this.cTrsName+'.t_PDCODSED'
    this.oPgFRm.Page1.oPag.oPDCODORD_2_9.controlsource=this.cTrsName+'.t_PDCODORD'
    this.oPgFRm.Page1.oPag.oPDOGGETT_2_10.controlsource=this.cTrsName+'.t_PDOGGETT'
    this.oPgFRm.Page1.oPag.oPD_TESTO_2_11.controlsource=this.cTrsName+'.t_PD_TESTO'
    this.oPgFRm.Page1.oPag.oPDFILTRO_2_12.controlsource=this.cTrsName+'.t_PDFILTRO'
    this.oPgFRm.Page1.oPag.oTIPRIFR_2_18.controlsource=this.cTrsName+'.t_TIPRIFR'
    this.oPgFRm.Page1.oPag.oTIPRIFE_2_19.controlsource=this.cTrsName+'.t_TIPRIFE'
    this.oPgFRm.Page1.oPag.oPDTIPRIA_2_20.controlsource=this.cTrsName+'.t_PDTIPRIA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(391)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDREPORT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROMDOCU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROMDOCU')
        i_extval=cp_InsertValODBCExtFlds(this,'PROMDOCU')
        local i_cFld
        i_cFld=" "+;
                  "(PDCODPRO,PDDESPRO,PDCLADOC,PDCHKOBB,PDCHKLOG"+;
                  ",PDCHKSTA,PDCHKMAI,PDCHKPEC,PDCHKFAX,PDCHKPTL"+;
                  ",PDCHKWWP,PDCHKZCP,PDCHKARC,PDCHKANT,PDRILSTA"+;
                  ",PDRILMAI,PDRILPEC,PDRILFAX,PDRILPTL,PDRILWWP"+;
                  ",PDRILZCP,PDRILARC,PDNMAXAL,PDPROCES,UTCC"+;
                  ",UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_PDCODPRO)+;
                    ","+cp_ToStrODBC(this.w_PDDESPRO)+;
                    ","+cp_ToStrODBCNull(this.w_PDCLADOC)+;
                    ","+cp_ToStrODBC(this.w_PDCHKOBB)+;
                    ","+cp_ToStrODBC(this.w_PDCHKLOG)+;
                    ","+cp_ToStrODBC(this.w_PDCHKSTA)+;
                    ","+cp_ToStrODBC(this.w_PDCHKMAI)+;
                    ","+cp_ToStrODBC(this.w_PDCHKPEC)+;
                    ","+cp_ToStrODBC(this.w_PDCHKFAX)+;
                    ","+cp_ToStrODBC(this.w_PDCHKPTL)+;
                    ","+cp_ToStrODBC(this.w_PDCHKWWP)+;
                    ","+cp_ToStrODBC(this.w_PDCHKZCP)+;
                    ","+cp_ToStrODBC(this.w_PDCHKARC)+;
                    ","+cp_ToStrODBC(this.w_PDCHKANT)+;
                    ","+cp_ToStrODBC(this.w_PDRILSTA)+;
                    ","+cp_ToStrODBC(this.w_PDRILMAI)+;
                    ","+cp_ToStrODBC(this.w_PDRILPEC)+;
                    ","+cp_ToStrODBC(this.w_PDRILFAX)+;
                    ","+cp_ToStrODBC(this.w_PDRILPTL)+;
                    ","+cp_ToStrODBC(this.w_PDRILWWP)+;
                    ","+cp_ToStrODBC(this.w_PDRILZCP)+;
                    ","+cp_ToStrODBC(this.w_PDRILARC)+;
                    ","+cp_ToStrODBC(this.w_PDNMAXAL)+;
                    ","+cp_ToStrODBC(this.w_PDPROCES)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROMDOCU')
        i_extval=cp_InsertValVFPExtFlds(this,'PROMDOCU')
        cp_CheckDeletedKey(i_cTable,0,'PDCODPRO',this.w_PDCODPRO)
        INSERT INTO (i_cTable);
              (PDCODPRO,PDDESPRO,PDCLADOC,PDCHKOBB,PDCHKLOG,PDCHKSTA,PDCHKMAI,PDCHKPEC,PDCHKFAX,PDCHKPTL,PDCHKWWP,PDCHKZCP,PDCHKARC,PDCHKANT,PDRILSTA,PDRILMAI,PDRILPEC,PDRILFAX,PDRILPTL,PDRILWWP,PDRILZCP,PDRILARC,PDNMAXAL,PDPROCES,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_PDCODPRO;
                  ,this.w_PDDESPRO;
                  ,this.w_PDCLADOC;
                  ,this.w_PDCHKOBB;
                  ,this.w_PDCHKLOG;
                  ,this.w_PDCHKSTA;
                  ,this.w_PDCHKMAI;
                  ,this.w_PDCHKPEC;
                  ,this.w_PDCHKFAX;
                  ,this.w_PDCHKPTL;
                  ,this.w_PDCHKWWP;
                  ,this.w_PDCHKZCP;
                  ,this.w_PDCHKARC;
                  ,this.w_PDCHKANT;
                  ,this.w_PDRILSTA;
                  ,this.w_PDRILMAI;
                  ,this.w_PDRILPEC;
                  ,this.w_PDRILFAX;
                  ,this.w_PDRILPTL;
                  ,this.w_PDRILWWP;
                  ,this.w_PDRILZCP;
                  ,this.w_PDRILARC;
                  ,this.w_PDNMAXAL;
                  ,this.w_PDPROCES;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRODDOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODDOCU_IDX,2])
      *
      * insert into PRODDOCU
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(PDCODPRO,PDREPORT,PDDESREP,PDKEYPRO,PDCONCON"+;
                  ",PDISOLAN,PDTIPRIF,PDCODSED,PDCODORD,PDOGGETT"+;
                  ",PD_TESTO,PDFILTRO,PDTIPRIA,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PDCODPRO)+","+cp_ToStrODBCNull(this.w_PDREPORT)+","+cp_ToStrODBC(this.w_PDDESREP)+","+cp_ToStrODBC(this.w_PDKEYPRO)+","+cp_ToStrODBC(this.w_PDCONCON)+;
             ","+cp_ToStrODBC(this.w_PDISOLAN)+","+cp_ToStrODBC(this.w_PDTIPRIF)+","+cp_ToStrODBC(this.w_PDCODSED)+","+cp_ToStrODBC(this.w_PDCODORD)+","+cp_ToStrODBC(this.w_PDOGGETT)+;
             ","+cp_ToStrODBC(this.w_PD_TESTO)+","+cp_ToStrODBC(this.w_PDFILTRO)+","+cp_ToStrODBC(this.w_PDTIPRIA)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'PDCODPRO',this.w_PDCODPRO,'PDREPORT',this.w_PDREPORT)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_PDCODPRO,this.w_PDREPORT,this.w_PDDESREP,this.w_PDKEYPRO,this.w_PDCONCON"+;
                ",this.w_PDISOLAN,this.w_PDTIPRIF,this.w_PDCODSED,this.w_PDCODORD,this.w_PDOGGETT"+;
                ",this.w_PD_TESTO,this.w_PDFILTRO,this.w_PDTIPRIA,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PROMDOCU
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PROMDOCU')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " PDDESPRO="+cp_ToStrODBC(this.w_PDDESPRO)+;
             ",PDCLADOC="+cp_ToStrODBCNull(this.w_PDCLADOC)+;
             ",PDCHKOBB="+cp_ToStrODBC(this.w_PDCHKOBB)+;
             ",PDCHKLOG="+cp_ToStrODBC(this.w_PDCHKLOG)+;
             ",PDCHKSTA="+cp_ToStrODBC(this.w_PDCHKSTA)+;
             ",PDCHKMAI="+cp_ToStrODBC(this.w_PDCHKMAI)+;
             ",PDCHKPEC="+cp_ToStrODBC(this.w_PDCHKPEC)+;
             ",PDCHKFAX="+cp_ToStrODBC(this.w_PDCHKFAX)+;
             ",PDCHKPTL="+cp_ToStrODBC(this.w_PDCHKPTL)+;
             ",PDCHKWWP="+cp_ToStrODBC(this.w_PDCHKWWP)+;
             ",PDCHKZCP="+cp_ToStrODBC(this.w_PDCHKZCP)+;
             ",PDCHKARC="+cp_ToStrODBC(this.w_PDCHKARC)+;
             ",PDCHKANT="+cp_ToStrODBC(this.w_PDCHKANT)+;
             ",PDRILSTA="+cp_ToStrODBC(this.w_PDRILSTA)+;
             ",PDRILMAI="+cp_ToStrODBC(this.w_PDRILMAI)+;
             ",PDRILPEC="+cp_ToStrODBC(this.w_PDRILPEC)+;
             ",PDRILFAX="+cp_ToStrODBC(this.w_PDRILFAX)+;
             ",PDRILPTL="+cp_ToStrODBC(this.w_PDRILPTL)+;
             ",PDRILWWP="+cp_ToStrODBC(this.w_PDRILWWP)+;
             ",PDRILZCP="+cp_ToStrODBC(this.w_PDRILZCP)+;
             ",PDRILARC="+cp_ToStrODBC(this.w_PDRILARC)+;
             ",PDNMAXAL="+cp_ToStrODBC(this.w_PDNMAXAL)+;
             ",PDPROCES="+cp_ToStrODBC(this.w_PDPROCES)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PROMDOCU')
          i_cWhere = cp_PKFox(i_cTable  ,'PDCODPRO',this.w_PDCODPRO  )
          UPDATE (i_cTable) SET;
              PDDESPRO=this.w_PDDESPRO;
             ,PDCLADOC=this.w_PDCLADOC;
             ,PDCHKOBB=this.w_PDCHKOBB;
             ,PDCHKLOG=this.w_PDCHKLOG;
             ,PDCHKSTA=this.w_PDCHKSTA;
             ,PDCHKMAI=this.w_PDCHKMAI;
             ,PDCHKPEC=this.w_PDCHKPEC;
             ,PDCHKFAX=this.w_PDCHKFAX;
             ,PDCHKPTL=this.w_PDCHKPTL;
             ,PDCHKWWP=this.w_PDCHKWWP;
             ,PDCHKZCP=this.w_PDCHKZCP;
             ,PDCHKARC=this.w_PDCHKARC;
             ,PDCHKANT=this.w_PDCHKANT;
             ,PDRILSTA=this.w_PDRILSTA;
             ,PDRILMAI=this.w_PDRILMAI;
             ,PDRILPEC=this.w_PDRILPEC;
             ,PDRILFAX=this.w_PDRILFAX;
             ,PDRILPTL=this.w_PDRILPTL;
             ,PDRILWWP=this.w_PDRILWWP;
             ,PDRILZCP=this.w_PDRILZCP;
             ,PDRILARC=this.w_PDRILARC;
             ,PDNMAXAL=this.w_PDNMAXAL;
             ,PDPROCES=this.w_PDPROCES;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_PDREPORT) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PRODDOCU_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PRODDOCU_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from PRODDOCU
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PDREPORT="+cp_ToStrODBC(&i_TN.->PDREPORT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PDREPORT=&i_TN.->PDREPORT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace PDREPORT with this.w_PDREPORT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRODDOCU
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PDDESREP="+cp_ToStrODBC(this.w_PDDESREP)+;
                     ",PDKEYPRO="+cp_ToStrODBC(this.w_PDKEYPRO)+;
                     ",PDCONCON="+cp_ToStrODBC(this.w_PDCONCON)+;
                     ",PDISOLAN="+cp_ToStrODBC(this.w_PDISOLAN)+;
                     ",PDTIPRIF="+cp_ToStrODBC(this.w_PDTIPRIF)+;
                     ",PDCODSED="+cp_ToStrODBC(this.w_PDCODSED)+;
                     ",PDCODORD="+cp_ToStrODBC(this.w_PDCODORD)+;
                     ",PDOGGETT="+cp_ToStrODBC(this.w_PDOGGETT)+;
                     ",PD_TESTO="+cp_ToStrODBC(this.w_PD_TESTO)+;
                     ",PDFILTRO="+cp_ToStrODBC(this.w_PDFILTRO)+;
                     ",PDTIPRIA="+cp_ToStrODBC(this.w_PDTIPRIA)+;
                     " ,PDREPORT="+cp_ToStrODBC(this.w_PDREPORT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and PDREPORT="+cp_ToStrODBC(PDREPORT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PDDESREP=this.w_PDDESREP;
                     ,PDKEYPRO=this.w_PDKEYPRO;
                     ,PDCONCON=this.w_PDCONCON;
                     ,PDISOLAN=this.w_PDISOLAN;
                     ,PDTIPRIF=this.w_PDTIPRIF;
                     ,PDCODSED=this.w_PDCODSED;
                     ,PDCODORD=this.w_PDCODORD;
                     ,PDOGGETT=this.w_PDOGGETT;
                     ,PD_TESTO=this.w_PD_TESTO;
                     ,PDFILTRO=this.w_PDFILTRO;
                     ,PDTIPRIA=this.w_PDTIPRIA;
                     ,PDREPORT=this.w_PDREPORT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and PDREPORT=&i_TN.->PDREPORT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSDM_MAP : Saving
      this.GSDM_MAP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PDCODPRO,"PECODPRO";
             )
      this.GSDM_MAP.mReplace()
      * --- GSDM_MDV : Saving
      this.GSDM_MDV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PDCODPRO,"DECODICE";
             )
      this.GSDM_MDV.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSDM_MAP : Deleting
    this.GSDM_MAP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PDCODPRO,"PECODPRO";
           )
    this.GSDM_MAP.mDelete()
    * --- GSDM_MDV : Deleting
    this.GSDM_MDV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PDCODPRO,"DECODICE";
           )
    this.GSDM_MDV.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_PDREPORT) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PRODDOCU_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PRODDOCU_IDX,2])
        *
        * delete PRODDOCU
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PDREPORT="+cp_ToStrODBC(&i_TN.->PDREPORT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PDREPORT=&i_TN.->PDREPORT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
        *
        * delete PROMDOCU
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_PDREPORT) ) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMDOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMDOCU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,13,.t.)
        if .o_PDCHKSTA<>.w_PDCHKSTA.or. .o_PDCHKMAI<>.w_PDCHKMAI.or. .o_PDCHKFAX<>.w_PDCHKFAX.or. .o_PDCHKPTL<>.w_PDCHKPTL.or. .o_PDCHKWWP<>.w_PDCHKWWP.or. .o_CDMODALL<>.w_CDMODALL
          .w_PDCHKZCP = iif((g_IZCP$'SA' or g_CPIN='S') and .w_CDMODALL<>'S' And g_DMIP<>'S', .w_PDCHKZCP,'N')
        endif
        if .o_PDCLADOC<>.w_PDCLADOC
          .w_PDCHKARC = IIF(empty(.w_PDCLADOC),'N',.w_PDCHKARC)
        endif
        if .o_PDCHKSTA<>.w_PDCHKSTA
          .w_PDCHKANT = IIF(.w_PDCHKSTA='S', .w_PDCHKANT, 'N')
        endif
        if .o_PDCHKSTA<>.w_PDCHKSTA
          .w_PDRILSTA = IIF(.w_PDCHKSTA='S',.w_PDRILSTA,'N')
        endif
        if .o_PDCHKMAI<>.w_PDCHKMAI
          .w_PDRILMAI = iif(.w_TipProcesso='IRP' and .w_PDCHKMAI='S','S','N')
        endif
        if .o_PDCHKPEC<>.w_PDCHKPEC
          .w_PDRILPEC = iif(.w_TipProcesso='IRP' and .w_PDCHKPEC='S','S','N')
        endif
        if .o_PDCHKFAX<>.w_PDCHKFAX
          .w_PDRILFAX = IIF(.w_PDCHKFAX='S',.w_PDRILFAX,'N')
        endif
        if .o_PDCHKPTL<>.w_PDCHKPTL
          .w_PDRILPTL = IIF(.w_PDCHKPTL='S',.w_PDRILPTL,'N')
        endif
        if .o_PDCHKWWP<>.w_PDCHKWWP
          .w_PDRILWWP = IIF(.w_PDCHKWWP='S',.w_PDRILWWP,'N')
        endif
        if .o_PDCHKZCP<>.w_PDCHKZCP
          .w_PDRILZCP = iif(.w_TipProcesso='IRP' and g_IZCP$'SA' and .w_PDCHKZCP='S','S','N')
        endif
        if .o_PDCHKARC<>.w_PDCHKARC
          .w_PDRILARC = iif(.w_TipProcesso='IRP' and .w_PDCHKARC='S','S','N')
        endif
        .DoRTCalc(25,31,.t.)
          .w_PDTIPRIF = IIF (isahe() , .w_TIPRIFE , .w_TIPRIFR)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        if .o_PDREPORT<>.w_PDREPORT
          .Calculate_UMDTPYPOLU()
        endif
        if .o_PDCODSED<>.w_PDCODSED
          .Calculate_HGKRMXSMXZ()
        endif
        if .o_PDCONCON<>.w_PDCONCON
          .Calculate_WOYPLMUNIK()
        endif
        if .o_PDCHKZCP<>.w_PDCHKZCP.or. .o_PDCHKARC<>.w_PDCHKARC.or. .o_PDCLADOC<>.w_PDCLADOC
          .Calculate_GZLQJIHXFQ()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(33,40,.t.)
        if .o_PDCODSED<>.w_PDCODSED
          .w_TIPRIFR = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        endif
        if .o_PDCODSED<>.w_PDCODSED
          .w_TIPRIFE = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        endif
        if .o_PDCODSED<>.w_PDCODSED.or. .o_TIPRIFE<>.w_TIPRIFE
          .w_PDTIPRIA = 'N'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(44,47,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PDTIPRIF with this.w_PDTIPRIF
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_UMDTPYPOLU()
    with this
          * --- Gsdm_bpd
          GSDM_BPD(this;
              ,'PR';
              ,.w_PDREPORT;
              ,.w_PDCODPRO;
             )
    endwith
  endproc
  proc Calculate_HGKRMXSMXZ()
    with this
          * --- Sbianca tipo sede
          .w_TIPRIFR = 'NO'
          .w_TIPRIFR = 'NO'
    endwith
  endproc
  proc Calculate_WOYPLMUNIK()
    with this
          * --- Valorizzazione campi legati all'intestatario
          .w_TIPRIFR = iif(empty(.w_PDCONCON),'',.w_TIPRIFR)
          .w_TIPRIFE = iif(empty(.w_PDCONCON),'',.w_TIPRIFE)
          .w_PDCODSED = iif(empty(.w_PDCONCON),'',.w_PDCODSED)
          .w_PDCODORD = iif(empty(.w_PDCONCON),'',.w_PDCODORD)
    endwith
  endproc
  proc Calculate_GZLQJIHXFQ()
    with this
          * --- Cambio valore archiviazione se web con E.D.S.
          .w_PDCHKARC = iif(.w_CDMODALL='E' AND g_CPIN='S' AND .w_PDCHKZCP='S' AND .w_PDCHKARC<>'S', CalcMsgPDCHKARC(), .w_PDCHKARC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPDCHKSTA_1_9.enabled = this.oPgFrm.Page1.oPag.oPDCHKSTA_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPDCHKFAX_1_12.enabled = this.oPgFrm.Page1.oPag.oPDCHKFAX_1_12.mCond()
    this.oPgFrm.Page1.oPag.oPDCHKPTL_1_13.enabled = this.oPgFrm.Page1.oPag.oPDCHKPTL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPDCHKWWP_1_14.enabled = this.oPgFrm.Page1.oPag.oPDCHKWWP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPDCHKZCP_1_15.enabled = this.oPgFrm.Page1.oPag.oPDCHKZCP_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPDCHKARC_1_16.enabled = this.oPgFrm.Page1.oPag.oPDCHKARC_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPDCHKANT_1_17.enabled = this.oPgFrm.Page1.oPag.oPDCHKANT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPDRILSTA_1_18.enabled = this.oPgFrm.Page1.oPag.oPDRILSTA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPDRILMAI_1_19.enabled = this.oPgFrm.Page1.oPag.oPDRILMAI_1_19.mCond()
    this.oPgFrm.Page1.oPag.oPDRILPEC_1_20.enabled = this.oPgFrm.Page1.oPag.oPDRILPEC_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPDRILFAX_1_21.enabled = this.oPgFrm.Page1.oPag.oPDRILFAX_1_21.mCond()
    this.oPgFrm.Page1.oPag.oPDRILPTL_1_22.enabled = this.oPgFrm.Page1.oPag.oPDRILPTL_1_22.mCond()
    this.oPgFrm.Page1.oPag.oPDRILWWP_1_23.enabled = this.oPgFrm.Page1.oPag.oPDRILWWP_1_23.mCond()
    this.oPgFrm.Page1.oPag.oPDRILZCP_1_24.enabled = this.oPgFrm.Page1.oPag.oPDRILZCP_1_24.mCond()
    this.oPgFrm.Page1.oPag.oPDRILARC_1_25.enabled = this.oPgFrm.Page1.oPag.oPDRILARC_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPDNMAXAL_1_26.enabled = this.oPgFrm.Page1.oPag.oPDNMAXAL_1_26.mCond()
    this.GSDM_MDV.enabled = this.oPgFrm.Page3.oPag.oLinkPC_5_1.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oPDISOLAN_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPDISOLAN_2_6.mCond()
    this.oPgFrm.Page1.oPag.oPDCODSED_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPDCODSED_2_8.mCond()
    this.oPgFrm.Page1.oPag.oPDCODORD_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPDCODORD_2_9.mCond()
    this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.mCond()
    this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.mCond()
    this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(Not(g_IZCP$'SA') Or g_CPIN='S' Or g_DMIP='S')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Permessi Corporate Portal"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page3.oPag.oLinkPC_5_1.visible=!this.oPgFrm.Page3.oPag.oLinkPC_5_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oPDKEYPRO_2_3.visible=!this.oPgFrm.Page1.oPag.oPDKEYPRO_2_3.mHide()
    this.oPgFrm.Page1.oPag.oPDKEYPRO_2_4.visible=!this.oPgFrm.Page1.oPag.oPDKEYPRO_2_4.mHide()
    this.oPgFrm.Page1.oPag.oPDFILTRO_2_12.visible=!this.oPgFrm.Page1.oPag.oPDFILTRO_2_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_13.visible=!this.oPgFrm.Page1.oPag.oBtn_2_13.mHide()
    this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.visible=!this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.mHide()
    this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.visible=!this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.mHide()
    this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.visible=!this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PDCLADOC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCLADOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_PDCLADOC)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDFIRDIG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_PDCLADOC))
          select CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDFIRDIG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCLADOC)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCLADOC) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oPDCLADOC_1_4'),i_cWhere,'GSUT_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDFIRDIG";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDFIRDIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCLADOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDFIRDIG";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_PDCLADOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_PDCLADOC)
            select CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDFIRDIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCLADOC = NVL(_Link_.CDCODCLA,space(15))
      this.w_DCLADOC = NVL(_Link_.CDDESCLA,space(40))
      this.w_TIPOARCH = NVL(_Link_.CDTIPARC,space(1))
      this.w_CDMODALL = NVL(_Link_.CDMODALL,space(1))
      this.w_FIRDIG = NVL(_Link_.CDFIRDIG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDCLADOC = space(15)
      endif
      this.w_DCLADOC = space(40)
      this.w_TIPOARCH = space(1)
      this.w_CDMODALL = space(1)
      this.w_FIRDIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOARCH='A'  and  INLIST(.w_CDMODALL,'F','E','I','S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida oppure con tipo-archiviazione manuale oppure con modalit�-archiviazione collegamento")
        endif
        this.w_PDCLADOC = space(15)
        this.w_DCLADOC = space(40)
        this.w_TIPOARCH = space(1)
        this.w_CDMODALL = space(1)
        this.w_FIRDIG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCLADOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.CDCODCLA as CDCODCLA104"+ ",link_1_4.CDDESCLA as CDDESCLA104"+ ",link_1_4.CDTIPARC as CDTIPARC104"+ ",link_1_4.CDMODALL as CDMODALL104"+ ",link_1_4.CDFIRDIG as CDFIRDIG104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on PROMDOCU.PDCLADOC=link_1_4.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and PROMDOCU.PDCLADOC=link_1_4.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDREPORT
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDREPORT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MOU',True,'OUT_PUTS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OUNOMREP like "+cp_ToStrODBC(trim(this.w_PDREPORT)+"%");

          i_ret=cp_SQL(i_nConn,"select OUNOMREP,OUDESPRG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OUNOMREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OUNOMREP',trim(this.w_PDREPORT))
          select OUNOMREP,OUDESPRG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OUNOMREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDREPORT)==trim(_Link_.OUNOMREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDREPORT) and !this.bDontReportError
            deferred_cp_zoom('OUT_PUTS','*','OUNOMREP',cp_AbsName(oSource.parent,'oPDREPORT_2_1'),i_cWhere,'GSUT_MOU',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMREP,OUDESPRG";
                     +" from "+i_cTable+" "+i_lTable+" where OUNOMREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMREP',oSource.xKey(1))
            select OUNOMREP,OUDESPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDREPORT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMREP,OUDESPRG";
                   +" from "+i_cTable+" "+i_lTable+" where OUNOMREP="+cp_ToStrODBC(this.w_PDREPORT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMREP',this.w_PDREPORT)
            select OUNOMREP,OUDESPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PDREPORT = NVL(_Link_.OUNOMREP,space(100))
      this.w_PDDESREP = NVL(_Link_.OUDESPRG,space(50))
    else
      this.w_PDDESREP = space(50)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUNOMREP,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDREPORT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPDCODPRO_1_1.value==this.w_PDCODPRO)
      this.oPgFrm.Page1.oPag.oPDCODPRO_1_1.value=this.w_PDCODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPDDESPRO_1_3.value==this.w_PDDESPRO)
      this.oPgFrm.Page1.oPag.oPDDESPRO_1_3.value=this.w_PDDESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCLADOC_1_4.value==this.w_PDCLADOC)
      this.oPgFrm.Page1.oPag.oPDCLADOC_1_4.value=this.w_PDCLADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDCLADOC_1_6.value==this.w_DCLADOC)
      this.oPgFrm.Page1.oPag.oDCLADOC_1_6.value=this.w_DCLADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKOBB_1_7.RadioValue()==this.w_PDCHKOBB)
      this.oPgFrm.Page1.oPag.oPDCHKOBB_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKLOG_1_8.RadioValue()==this.w_PDCHKLOG)
      this.oPgFrm.Page1.oPag.oPDCHKLOG_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKSTA_1_9.RadioValue()==this.w_PDCHKSTA)
      this.oPgFrm.Page1.oPag.oPDCHKSTA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKMAI_1_10.RadioValue()==this.w_PDCHKMAI)
      this.oPgFrm.Page1.oPag.oPDCHKMAI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKPEC_1_11.RadioValue()==this.w_PDCHKPEC)
      this.oPgFrm.Page1.oPag.oPDCHKPEC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKFAX_1_12.RadioValue()==this.w_PDCHKFAX)
      this.oPgFrm.Page1.oPag.oPDCHKFAX_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKPTL_1_13.RadioValue()==this.w_PDCHKPTL)
      this.oPgFrm.Page1.oPag.oPDCHKPTL_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKWWP_1_14.RadioValue()==this.w_PDCHKWWP)
      this.oPgFrm.Page1.oPag.oPDCHKWWP_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKZCP_1_15.RadioValue()==this.w_PDCHKZCP)
      this.oPgFrm.Page1.oPag.oPDCHKZCP_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKARC_1_16.RadioValue()==this.w_PDCHKARC)
      this.oPgFrm.Page1.oPag.oPDCHKARC_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCHKANT_1_17.RadioValue()==this.w_PDCHKANT)
      this.oPgFrm.Page1.oPag.oPDCHKANT_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILSTA_1_18.RadioValue()==this.w_PDRILSTA)
      this.oPgFrm.Page1.oPag.oPDRILSTA_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILMAI_1_19.RadioValue()==this.w_PDRILMAI)
      this.oPgFrm.Page1.oPag.oPDRILMAI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILPEC_1_20.RadioValue()==this.w_PDRILPEC)
      this.oPgFrm.Page1.oPag.oPDRILPEC_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILFAX_1_21.RadioValue()==this.w_PDRILFAX)
      this.oPgFrm.Page1.oPag.oPDRILFAX_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILPTL_1_22.RadioValue()==this.w_PDRILPTL)
      this.oPgFrm.Page1.oPag.oPDRILPTL_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILWWP_1_23.RadioValue()==this.w_PDRILWWP)
      this.oPgFrm.Page1.oPag.oPDRILWWP_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILZCP_1_24.RadioValue()==this.w_PDRILZCP)
      this.oPgFrm.Page1.oPag.oPDRILZCP_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRILARC_1_25.RadioValue()==this.w_PDRILARC)
      this.oPgFrm.Page1.oPag.oPDRILARC_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDNMAXAL_1_26.value==this.w_PDNMAXAL)
      this.oPgFrm.Page1.oPag.oPDNMAXAL_1_26.value=this.w_PDNMAXAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPDKEYPRO_2_3.value==this.w_PDKEYPRO)
      this.oPgFrm.Page1.oPag.oPDKEYPRO_2_3.value=this.w_PDKEYPRO
      replace t_PDKEYPRO with this.oPgFrm.Page1.oPag.oPDKEYPRO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDKEYPRO_2_4.value==this.w_PDKEYPRO)
      this.oPgFrm.Page1.oPag.oPDKEYPRO_2_4.value=this.w_PDKEYPRO
      replace t_PDKEYPRO with this.oPgFrm.Page1.oPag.oPDKEYPRO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCONCON_2_5.value==this.w_PDCONCON)
      this.oPgFrm.Page1.oPag.oPDCONCON_2_5.value=this.w_PDCONCON
      replace t_PDCONCON with this.oPgFrm.Page1.oPag.oPDCONCON_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDISOLAN_2_6.value==this.w_PDISOLAN)
      this.oPgFrm.Page1.oPag.oPDISOLAN_2_6.value=this.w_PDISOLAN
      replace t_PDISOLAN with this.oPgFrm.Page1.oPag.oPDISOLAN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODSED_2_8.value==this.w_PDCODSED)
      this.oPgFrm.Page1.oPag.oPDCODSED_2_8.value=this.w_PDCODSED
      replace t_PDCODSED with this.oPgFrm.Page1.oPag.oPDCODSED_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODORD_2_9.value==this.w_PDCODORD)
      this.oPgFrm.Page1.oPag.oPDCODORD_2_9.value=this.w_PDCODORD
      replace t_PDCODORD with this.oPgFrm.Page1.oPag.oPDCODORD_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDOGGETT_2_10.value==this.w_PDOGGETT)
      this.oPgFrm.Page1.oPag.oPDOGGETT_2_10.value=this.w_PDOGGETT
      replace t_PDOGGETT with this.oPgFrm.Page1.oPag.oPDOGGETT_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPD_TESTO_2_11.value==this.w_PD_TESTO)
      this.oPgFrm.Page1.oPag.oPD_TESTO_2_11.value=this.w_PD_TESTO
      replace t_PD_TESTO with this.oPgFrm.Page1.oPag.oPD_TESTO_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFILTRO_2_12.value==this.w_PDFILTRO)
      this.oPgFrm.Page1.oPag.oPDFILTRO_2_12.value=this.w_PDFILTRO
      replace t_PDFILTRO with this.oPgFrm.Page1.oPag.oPDFILTRO_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.RadioValue()==this.w_TIPRIFR)
      this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.SetRadio()
      replace t_TIPRIFR with this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.RadioValue()==this.w_TIPRIFE)
      this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.SetRadio()
      replace t_TIPRIFE with this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.RadioValue()==this.w_PDTIPRIA)
      this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.SetRadio()
      replace t_PDTIPRIA with this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDREPORT_2_1.value==this.w_PDREPORT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDREPORT_2_1.value=this.w_PDREPORT
      replace t_PDREPORT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDREPORT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDDESREP_2_2.value==this.w_PDDESREP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDDESREP_2_2.value=this.w_PDDESREP
      replace t_PDDESREP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDDESREP_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'PROMDOCU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PDCODPRO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPDCODPRO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PDCODPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOARCH='A'  and  INLIST(.w_CDMODALL,'F','E','I','S'))  and not(empty(.w_PDCLADOC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPDCLADOC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida oppure con tipo-archiviazione manuale oppure con modalit�-archiviazione collegamento")
          case   not(.w_PDNMAXAL>=0)  and (.w_PDCHKWWP='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPDNMAXAL_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSDM_MAP.CheckForm()
      if i_bres
        i_bres=  .GSDM_MAP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSDM_MDV.CheckForm()
      if i_bres
        i_bres=  .GSDM_MDV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsdm_mpd
      *Verifico l'inserimento di almeno una riga piena valida
      if this.numRow()= 0
        par1=iif(this.w_TipProcesso<>'IRP','un report','una query Infopublisher')
        ah_errormsg('Inserire almeno %1',48,'',par1)
        i_bRes=.f.
      endif  
      *Controlli su firma digitale, se attivata nella classe selezionata
      if this.w_FIRDIG='S' and this.w_PDCHKMAI # 'S' and this.w_PDCHKZCP # 'S' and this.w_PDCHKARC # 'S'
        Ah_errormsg("Attenzione: Classe documentale con attivo il check firma digitale, � richiesta l'attivazione di almeno una delle seguenti attivit�: Email e/o Corporate Portal e/o Archiviazione.",48,'')
        i_bRes=.f.
      endif
      
      if this.w_PDCHKZCP='S' and empty(this.w_PDCONCON) And g_CPIN<>'S'
        Ah_errormsg("Attenzione: [Servizio Web Application] Campo intestatario vuoto, non verra fatto alcun controllo sullo stesso",48,'')
      endif
      
      *--- Controllo Ad hoc Infinity
      If i_bRes And g_CPIN='S' And this.w_PDCHKZCP='S' And Empty(this.w_PDCLADOC)
        i_bRes = .f.
        ah_ErrorMsg("Occorre specificare una classe documentale")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_PDKEYPRO) and (not Empty(.w_PDREPORT) )
          .oNewFocus=.oPgFrm.Page1.oPag.oPDKEYPRO_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_PDKEYPRO) and (not Empty(.w_PDREPORT) )
          .oNewFocus=.oPgFrm.Page1.oPag.oPDKEYPRO_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not Empty(.w_PDREPORT) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDCLADOC = this.w_PDCLADOC
    this.o_CDMODALL = this.w_CDMODALL
    this.o_PDCHKSTA = this.w_PDCHKSTA
    this.o_PDCHKMAI = this.w_PDCHKMAI
    this.o_PDCHKPEC = this.w_PDCHKPEC
    this.o_PDCHKFAX = this.w_PDCHKFAX
    this.o_PDCHKPTL = this.w_PDCHKPTL
    this.o_PDCHKWWP = this.w_PDCHKWWP
    this.o_PDCHKZCP = this.w_PDCHKZCP
    this.o_PDCHKARC = this.w_PDCHKARC
    this.o_PDREPORT = this.w_PDREPORT
    this.o_PDCONCON = this.w_PDCONCON
    this.o_PDCODSED = this.w_PDCODSED
    this.o_TIPRIFE = this.w_TIPRIFE
    * --- GSDM_MAP : Depends On
    this.GSDM_MAP.SaveDependsOn()
    * --- GSDM_MDV : Depends On
    this.GSDM_MDV.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_PDREPORT) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PDREPORT=space(100)
      .w_PDDESREP=space(50)
      .w_PDKEYPRO=space(100)
      .w_PDKEYPRO=space(100)
      .w_PDCONCON=space(60)
      .w_PDISOLAN=space(60)
      .w_PDTIPRIF=space(2)
      .w_PDCODSED=space(60)
      .w_PDCODORD=space(60)
      .w_PDOGGETT=space(0)
      .w_PD_TESTO=space(0)
      .w_PDFILTRO=space(0)
      .w_TIPRIFR=space(10)
      .w_TIPRIFE=space(10)
      .w_PDTIPRIA=space(1)
      .DoRTCalc(1,26,.f.)
      if not(empty(.w_PDREPORT))
        .link_2_1('Full')
      endif
      .DoRTCalc(27,31,.f.)
        .w_PDTIPRIF = IIF (isahe() , .w_TIPRIFE , .w_TIPRIFR)
      .DoRTCalc(33,40,.f.)
        .w_TIPRIFR = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        .w_TIPRIFE = IIF(EMPTY(.w_PDCODSED),.w_PDTIPRIF,'NO')
        .w_PDTIPRIA = 'N'
    endwith
    this.DoRTCalc(44,47,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PDREPORT = t_PDREPORT
    this.w_PDDESREP = t_PDDESREP
    this.w_PDKEYPRO = t_PDKEYPRO
    this.w_PDKEYPRO = t_PDKEYPRO
    this.w_PDCONCON = t_PDCONCON
    this.w_PDISOLAN = t_PDISOLAN
    this.w_PDTIPRIF = t_PDTIPRIF
    this.w_PDCODSED = t_PDCODSED
    this.w_PDCODORD = t_PDCODORD
    this.w_PDOGGETT = t_PDOGGETT
    this.w_PD_TESTO = t_PD_TESTO
    this.w_PDFILTRO = t_PDFILTRO
    this.w_TIPRIFR = this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.RadioValue(.t.)
    this.w_TIPRIFE = this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.RadioValue(.t.)
    this.w_PDTIPRIA = this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PDREPORT with this.w_PDREPORT
    replace t_PDDESREP with this.w_PDDESREP
    replace t_PDKEYPRO with this.w_PDKEYPRO
    replace t_PDKEYPRO with this.w_PDKEYPRO
    replace t_PDCONCON with this.w_PDCONCON
    replace t_PDISOLAN with this.w_PDISOLAN
    replace t_PDTIPRIF with this.w_PDTIPRIF
    replace t_PDCODSED with this.w_PDCODSED
    replace t_PDCODORD with this.w_PDCODORD
    replace t_PDOGGETT with this.w_PDOGGETT
    replace t_PD_TESTO with this.w_PD_TESTO
    replace t_PDFILTRO with this.w_PDFILTRO
    replace t_TIPRIFR with this.oPgFrm.Page1.oPag.oTIPRIFR_2_18.ToRadio()
    replace t_TIPRIFE with this.oPgFrm.Page1.oPag.oTIPRIFE_2_19.ToRadio()
    replace t_PDTIPRIA with this.oPgFrm.Page1.oPag.oPDTIPRIA_2_20.ToRadio()
    if i_srv='A'
      replace PDREPORT with this.w_PDREPORT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsdm_mpdPag1 as StdContainer
  Width  = 795
  height = 478
  stdWidth  = 795
  stdheight = 478
  resizeXpos=479
  resizeYpos=223
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPDCODPRO_1_1 as StdField with uid="IYDUTJFTZU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PDCODPRO", cQueryName = "PDCODPRO",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 197768517,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=83, Left=159, Top=7, InputMask=replicate('X',10)

  add object oPDDESPRO_1_3 as StdField with uid="TWEWBOBLXN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PDDESPRO", cQueryName = "PDDESPRO",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 212845893,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=247, Top=7, InputMask=replicate('X',40)

  add object oPDCLADOC_1_4 as StdField with uid="HUJOBMNZFS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PDCLADOC", cQueryName = "PDCLADOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida oppure con tipo-archiviazione manuale oppure con modalit�-archiviazione collegamento",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 6900423,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=158, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_PDCLADOC"

  func oPDCLADOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCLADOC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCLADOC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oPDCLADOC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oPDCLADOC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_PDCLADOC
    i_obj.ecpSave()
  endproc

  add object oDCLADOC_1_6 as StdField with uid="KUWVITSLYW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DCLADOC", cQueryName = "DCLADOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 180110134,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=292, Top=32, InputMask=replicate('X',40)

  add object oPDCHKOBB_1_7 as StdCheck with uid="CYDNHXVXAZ",rtseq=6,rtrep=.f.,left=671, top=8, caption="Obbligatorio",;
    ToolTipText = "Se attivo: il processo deve essere eseguito obbligatoriamente",;
    HelpContextID = 187872568,;
    cFormVar="w_PDCHKOBB", bObbl = .f. , nPag = 1;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oPDCHKOBB_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKOBB,&i_cF..t_PDCHKOBB),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKOBB_1_7.GetRadio()
    this.Parent.oContained.w_PDCHKOBB = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKOBB_1_7.ToRadio()
    this.Parent.oContained.w_PDCHKOBB=trim(this.Parent.oContained.w_PDCHKOBB)
    return(;
      iif(this.Parent.oContained.w_PDCHKOBB=='S',1,;
      0))
  endfunc

  func oPDCHKOBB_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPDCHKLOG_1_8 as StdCheck with uid="WCEMYNTXZK",rtseq=7,rtrep=.f.,left=671, top=32, caption="Mantieni log",;
    ToolTipText = "Se attivo: mantiene log esecuzione di tutte le istanze del processo documentale",;
    HelpContextID = 130894531,;
    cFormVar="w_PDCHKLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKLOG_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKLOG,&i_cF..t_PDCHKLOG),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKLOG_1_8.GetRadio()
    this.Parent.oContained.w_PDCHKLOG = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKLOG_1_8.ToRadio()
    this.Parent.oContained.w_PDCHKLOG=trim(this.Parent.oContained.w_PDCHKLOG)
    return(;
      iif(this.Parent.oContained.w_PDCHKLOG=='S',1,;
      0))
  endfunc

  func oPDCHKLOG_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPDCHKSTA_1_9 as StdCheck with uid="DFYVNUPDMB",rtseq=8,rtrep=.f.,left=99, top=76, caption="Stampa",;
    ToolTipText = "Permette di eseguire il processo di stampa",;
    HelpContextID = 254981431,;
    cFormVar="w_PDCHKSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKSTA_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKSTA,&i_cF..t_PDCHKSTA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKSTA_1_9.GetRadio()
    this.Parent.oContained.w_PDCHKSTA = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKSTA_1_9.ToRadio()
    this.Parent.oContained.w_PDCHKSTA=trim(this.Parent.oContained.w_PDCHKSTA)
    return(;
      iif(this.Parent.oContained.w_PDCHKSTA=='S',1,;
      0))
  endfunc

  func oPDCHKSTA_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKSTA_1_9.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDCHKMAI_1_10 as StdCheck with uid="VHERCJBAFQ",rtseq=9,rtrep=.f.,left=176, top=76, caption="email",;
    ToolTipText = "Permette invio di documenti tramite e-mail",;
    HelpContextID = 154318143,;
    cFormVar="w_PDCHKMAI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKMAI_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKMAI,&i_cF..t_PDCHKMAI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKMAI_1_10.GetRadio()
    this.Parent.oContained.w_PDCHKMAI = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKMAI_1_10.ToRadio()
    this.Parent.oContained.w_PDCHKMAI=trim(this.Parent.oContained.w_PDCHKMAI)
    return(;
      iif(this.Parent.oContained.w_PDCHKMAI=='S',1,;
      0))
  endfunc

  func oPDCHKMAI_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPDCHKPEC_1_11 as StdCheck with uid="TCOLXFTHPT",rtseq=10,rtrep=.f.,left=240, top=76, caption="PEC",;
    ToolTipText = "Permette invio di documenti tramite PEC",;
    HelpContextID = 204649785,;
    cFormVar="w_PDCHKPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKPEC_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKPEC,&i_cF..t_PDCHKPEC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKPEC_1_11.GetRadio()
    this.Parent.oContained.w_PDCHKPEC = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKPEC_1_11.ToRadio()
    this.Parent.oContained.w_PDCHKPEC=trim(this.Parent.oContained.w_PDCHKPEC)
    return(;
      iif(this.Parent.oContained.w_PDCHKPEC=='S',1,;
      0))
  endfunc

  func oPDCHKPEC_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  proc oPDCHKPEC_1_11.mDefault
    with this.Parent.oContained
      if empty(.w_PDCHKPEC)
        .w_PDCHKPEC = 'N'
      endif
    endwith
  endproc

  add object oPDCHKFAX_1_12 as StdCheck with uid="VMRPCXUOJC",rtseq=11,rtrep=.f.,left=297, top=76, caption="FAX",;
    ToolTipText = "Permette invio di documenti tramite FAX",;
    HelpContextID = 36877646,;
    cFormVar="w_PDCHKFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKFAX_1_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKFAX,&i_cF..t_PDCHKFAX),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKFAX_1_12.GetRadio()
    this.Parent.oContained.w_PDCHKFAX = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKFAX_1_12.ToRadio()
    this.Parent.oContained.w_PDCHKFAX=trim(this.Parent.oContained.w_PDCHKFAX)
    return(;
      iif(this.Parent.oContained.w_PDCHKFAX=='S',1,;
      0))
  endfunc

  func oPDCHKFAX_1_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKFAX_1_12.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDCHKPTL_1_13 as StdCheck with uid="SIFTMFUQVD",rtseq=12,rtrep=.f.,left=348, top=76, caption="PostaLite",;
    ToolTipText = "Permette invio di documenti tramite Postel",;
    HelpContextID = 204649794,;
    cFormVar="w_PDCHKPTL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKPTL_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKPTL,&i_cF..t_PDCHKPTL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKPTL_1_13.GetRadio()
    this.Parent.oContained.w_PDCHKPTL = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKPTL_1_13.ToRadio()
    this.Parent.oContained.w_PDCHKPTL=trim(this.Parent.oContained.w_PDCHKPTL)
    return(;
      iif(this.Parent.oContained.w_PDCHKPTL=='S',1,;
      0))
  endfunc

  func oPDCHKPTL_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKPTL_1_13.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDCHKWWP_1_14 as StdCheck with uid="BVIJYMSFSI",rtseq=13,rtrep=.f.,left=432, top=76, caption="WE",;
    ToolTipText = "Permette invio di documenti a net-folder",;
    HelpContextID = 214780602,;
    cFormVar="w_PDCHKWWP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKWWP_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKWWP,&i_cF..t_PDCHKWWP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKWWP_1_14.GetRadio()
    this.Parent.oContained.w_PDCHKWWP = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKWWP_1_14.ToRadio()
    this.Parent.oContained.w_PDCHKWWP=trim(this.Parent.oContained.w_PDCHKWWP)
    return(;
      iif(this.Parent.oContained.w_PDCHKWWP=='S',1,;
      0))
  endfunc

  func oPDCHKWWP_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKWWP_1_14.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDCHKZCP_1_15 as StdCheck with uid="ONRIGLPSBO",rtseq=14,rtrep=.f.,left=482, top=76, caption="Web Application",;
    ToolTipText = "Permette invio di documenti al web-folder del Web Application",;
    HelpContextID = 103986502,;
    cFormVar="w_PDCHKZCP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKZCP_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKZCP,&i_cF..t_PDCHKZCP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKZCP_1_15.GetRadio()
    this.Parent.oContained.w_PDCHKZCP = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKZCP_1_15.ToRadio()
    this.Parent.oContained.w_PDCHKZCP=trim(this.Parent.oContained.w_PDCHKZCP)
    return(;
      iif(this.Parent.oContained.w_PDCHKZCP=='S',1,;
      0))
  endfunc

  func oPDCHKZCP_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKZCP_1_15.mCond()
    with this.Parent.oContained
      return (.w_CDMODALL<>'S' and (g_IZCP$'SA' or g_CPIN='S') And g_DMIP<>'S')
    endwith
  endfunc

  func oPDCHKZCP_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !((! empty(.w_PDCONCON) and .w_PDCHKZCP='S') or .w_PDCHKZCP#'S' OR g_CPIN='S')
         bRes=(cp_WarningMsg(thisform.msgFmt(" [Servizio Web Application] Campo intestatario vuoto, non verra fatto alcun controllo sullo stesso")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oPDCHKARC_1_16 as StdCheck with uid="EQZYDABEHW",rtseq=15,rtrep=.f.,left=604, top=76, caption="Archiviazione",;
    HelpContextID = 221427001,;
    cFormVar="w_PDCHKARC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKARC_1_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKARC,&i_cF..t_PDCHKARC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKARC_1_16.GetRadio()
    this.Parent.oContained.w_PDCHKARC = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKARC_1_16.ToRadio()
    this.Parent.oContained.w_PDCHKARC=trim(this.Parent.oContained.w_PDCHKARC)
    return(;
      iif(this.Parent.oContained.w_PDCHKARC=='S',1,;
      0))
  endfunc

  func oPDCHKARC_1_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKARC_1_16.mCond()
    with this.Parent.oContained
      return (not empty(.w_PDCLADOC))
    endwith
  endfunc

  add object oPDCHKANT_1_17 as StdCheck with uid="VUFQIYHSFI",rtseq=16,rtrep=.f.,left=705, top=76, caption="Anteprima",;
    ToolTipText = "Permette di eseguire l'anteprima di stampa sulla Print System",;
    HelpContextID = 47008438,;
    cFormVar="w_PDCHKANT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCHKANT_1_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDCHKANT,&i_cF..t_PDCHKANT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDCHKANT_1_17.GetRadio()
    this.Parent.oContained.w_PDCHKANT = this.RadioValue()
    return .t.
  endfunc

  func oPDCHKANT_1_17.ToRadio()
    this.Parent.oContained.w_PDCHKANT=trim(this.Parent.oContained.w_PDCHKANT)
    return(;
      iif(this.Parent.oContained.w_PDCHKANT=='S',1,;
      0))
  endfunc

  func oPDCHKANT_1_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDCHKANT_1_17.mCond()
    with this.Parent.oContained
      return (.w_PDCHKSTA='S')
    endwith
  endfunc

  add object oPDRILSTA_1_18 as StdCheck with uid="WWSTZXFEMB",rtseq=17,rtrep=.f.,left=99, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di stampa email � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 256156983,;
    cFormVar="w_PDRILSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILSTA_1_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILSTA,&i_cF..t_PDRILSTA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILSTA_1_18.GetRadio()
    this.Parent.oContained.w_PDRILSTA = this.RadioValue()
    return .t.
  endfunc

  func oPDRILSTA_1_18.ToRadio()
    this.Parent.oContained.w_PDRILSTA=trim(this.Parent.oContained.w_PDRILSTA)
    return(;
      iif(this.Parent.oContained.w_PDRILSTA=='S',1,;
      0))
  endfunc

  func oPDRILSTA_1_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILSTA_1_18.mCond()
    with this.Parent.oContained
      return (.w_PDCHKSTA='S')
    endwith
  endfunc

  add object oPDRILMAI_1_19 as StdCheck with uid="WVDGMGKYHK",rtseq=18,rtrep=.f.,left=176, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di invio email � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 155493695,;
    cFormVar="w_PDRILMAI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILMAI_1_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILMAI,&i_cF..t_PDRILMAI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILMAI_1_19.GetRadio()
    this.Parent.oContained.w_PDRILMAI = this.RadioValue()
    return .t.
  endfunc

  func oPDRILMAI_1_19.ToRadio()
    this.Parent.oContained.w_PDRILMAI=trim(this.Parent.oContained.w_PDRILMAI)
    return(;
      iif(this.Parent.oContained.w_PDRILMAI=='S',1,;
      0))
  endfunc

  func oPDRILMAI_1_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILMAI_1_19.mCond()
    with this.Parent.oContained
      return (.w_PDCHKMAI='S' and .w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDRILPEC_1_20 as StdCheck with uid="UNOHXSCWRU",rtseq=19,rtrep=.f.,left=240, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di invio PEC � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 205825337,;
    cFormVar="w_PDRILPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILPEC_1_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILPEC,&i_cF..t_PDRILPEC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILPEC_1_20.GetRadio()
    this.Parent.oContained.w_PDRILPEC = this.RadioValue()
    return .t.
  endfunc

  func oPDRILPEC_1_20.ToRadio()
    this.Parent.oContained.w_PDRILPEC=trim(this.Parent.oContained.w_PDRILPEC)
    return(;
      iif(this.Parent.oContained.w_PDRILPEC=='S',1,;
      0))
  endfunc

  func oPDRILPEC_1_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILPEC_1_20.mCond()
    with this.Parent.oContained
      return (.w_PDCHKPEC='S' and .w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDRILFAX_1_21 as StdCheck with uid="FIZBVIDYNW",rtseq=20,rtrep=.f.,left=297, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di invio FAX � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 38053198,;
    cFormVar="w_PDRILFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILFAX_1_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILFAX,&i_cF..t_PDRILFAX),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILFAX_1_21.GetRadio()
    this.Parent.oContained.w_PDRILFAX = this.RadioValue()
    return .t.
  endfunc

  func oPDRILFAX_1_21.ToRadio()
    this.Parent.oContained.w_PDRILFAX=trim(this.Parent.oContained.w_PDRILFAX)
    return(;
      iif(this.Parent.oContained.w_PDRILFAX=='S',1,;
      0))
  endfunc

  func oPDRILFAX_1_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILFAX_1_21.mCond()
    with this.Parent.oContained
      return (.w_PDCHKFAX='S')
    endwith
  endfunc

  add object oPDRILPTL_1_22 as StdCheck with uid="FYFCKHOFEX",rtseq=21,rtrep=.f.,left=348, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di invio Postel � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 205825346,;
    cFormVar="w_PDRILPTL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILPTL_1_22.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILPTL,&i_cF..t_PDRILPTL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILPTL_1_22.GetRadio()
    this.Parent.oContained.w_PDRILPTL = this.RadioValue()
    return .t.
  endfunc

  func oPDRILPTL_1_22.ToRadio()
    this.Parent.oContained.w_PDRILPTL=trim(this.Parent.oContained.w_PDRILPTL)
    return(;
      iif(this.Parent.oContained.w_PDRILPTL=='S',1,;
      0))
  endfunc

  func oPDRILPTL_1_22.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILPTL_1_22.mCond()
    with this.Parent.oContained
      return (.w_PDCHKPTL='S')
    endwith
  endfunc

  add object oPDRILWWP_1_23 as StdCheck with uid="MHSOEZSLJL",rtseq=22,rtrep=.f.,left=432, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di invio a net-folder � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 213605050,;
    cFormVar="w_PDRILWWP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILWWP_1_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILWWP,&i_cF..t_PDRILWWP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILWWP_1_23.GetRadio()
    this.Parent.oContained.w_PDRILWWP = this.RadioValue()
    return .t.
  endfunc

  func oPDRILWWP_1_23.ToRadio()
    this.Parent.oContained.w_PDRILWWP=trim(this.Parent.oContained.w_PDRILWWP)
    return(;
      iif(this.Parent.oContained.w_PDRILWWP=='S',1,;
      0))
  endfunc

  func oPDRILWWP_1_23.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILWWP_1_23.mCond()
    with this.Parent.oContained
      return (.w_PDCHKWWP='S')
    endwith
  endfunc

  add object oPDRILZCP_1_24 as StdCheck with uid="XHUFZNAXIP",rtseq=23,rtrep=.f.,left=482, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di invio documenti al web-folder � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 105162054,;
    cFormVar="w_PDRILZCP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILZCP_1_24.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILZCP,&i_cF..t_PDRILZCP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILZCP_1_24.GetRadio()
    this.Parent.oContained.w_PDRILZCP = this.RadioValue()
    return .t.
  endfunc

  func oPDRILZCP_1_24.ToRadio()
    this.Parent.oContained.w_PDRILZCP=trim(this.Parent.oContained.w_PDRILZCP)
    return(;
      iif(this.Parent.oContained.w_PDRILZCP=='S',1,;
      0))
  endfunc

  func oPDRILZCP_1_24.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILZCP_1_24.mCond()
    with this.Parent.oContained
      return (.w_PDCHKZCP='S' and (g_IZCP$'SA' and .w_TipProcesso<>'IRP' or g_CPIN='S' or g_DMIP='S'))
    endwith
  endfunc

  add object oPDRILARC_1_25 as StdCheck with uid="DYMTXISUTV",rtseq=24,rtrep=.f.,left=604, top=100, caption="Si",;
    ToolTipText = "Se attivo: il processo di archiviazione � rieseguibile pi� volte per lo stesso documento",;
    HelpContextID = 222602553,;
    cFormVar="w_PDRILARC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDRILARC_1_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDRILARC,&i_cF..t_PDRILARC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDRILARC_1_25.GetRadio()
    this.Parent.oContained.w_PDRILARC = this.RadioValue()
    return .t.
  endfunc

  func oPDRILARC_1_25.ToRadio()
    this.Parent.oContained.w_PDRILARC=trim(this.Parent.oContained.w_PDRILARC)
    return(;
      iif(this.Parent.oContained.w_PDRILARC=='S',1,;
      0))
  endfunc

  func oPDRILARC_1_25.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDRILARC_1_25.mCond()
    with this.Parent.oContained
      return (not empty(.w_PDCLADOC) and .w_PDCHKARC='S' and .w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDNMAXAL_1_26 as StdField with uid="STIYPNBXWD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PDNMAXAL", cQueryName = "PDNMAXAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo allegati per messaggio email di invio documenti a WE (0=nessun limite)",;
    HelpContextID = 60319042,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=749, Top=105, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"'

  func oPDNMAXAL_1_26.mCond()
    with this.Parent.oContained
      return (.w_PDCHKWWP='S')
    endwith
  endfunc

  func oPDNMAXAL_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PDNMAXAL>=0)
    endwith
    return bRes
  endfunc


  add object oObj_1_34 as cp_runprogram with uid="LATFMMWCUA",left=240, top=497, width=229,height=22,;
    caption='GSDM_BRP',;
   bGlobalFont=.t.,;
    prg="GSDM_BRP('A')",;
    cEvent = "w_PDCHKZCP Changed",;
    nPag=1;
    , HelpContextID = 259511222


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=32, top=130, width=734,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="PDREPORT",Label1="iif(w_TipProcesso<>'IRP',ah_Msgformat('Report'), ah_Msgformat('Query Infopublisher'))",Field2="PDDESREP",Label2="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168650106

  add object oStr_1_2 as StdString with uid="JQCCVLWCFM",Visible=.t., Left=27, Top=10,;
    Alignment=1, Width=129, Height=18,;
    Caption="Codice processo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="INKVYYXUUD",Visible=.t., Left=11, Top=32,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="MCCCOKDXHU",Visible=.t., Left=37, Top=77,;
    Alignment=1, Width=57, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KYFMTWZZTN",Visible=.t., Left=627, Top=106,;
    Alignment=1, Width=113, Height=18,;
    Caption="N.max allegati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TVFWWGQIOH",Visible=.t., Left=9, Top=103,;
    Alignment=1, Width=85, Height=18,;
    Caption="Rieseguibile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KAIZKMLYPJ",Visible=.t., Left=20, Top=58,;
    Alignment=0, Width=150, Height=17,;
    Caption="Attivit� processo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="TYQKPAAZJS",Visible=.t., Left=8, Top=433,;
    Alignment=1, Width=111, Height=18,;
    Caption="Filtro avanzato:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_TipProcesso='IRP')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="XEKKQJXLWD",Visible=.t., Left=8, Top=314,;
    Alignment=1, Width=111, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="KHZBHTSHIJ",Visible=.t., Left=8, Top=290,;
    Alignment=1, Width=111, Height=18,;
    Caption="Campo chiave:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_TipProcesso='IRP')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="UQNDLYMCEL",Visible=.t., Left=8, Top=409,;
    Alignment=1, Width=111, Height=22,;
    Caption="Testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KIIWLMRSHN",Visible=.t., Left=8, Top=385,;
    Alignment=1, Width=111, Height=22,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DUKULGOKOV",Visible=.t., Left=8, Top=290,;
    Alignment=1, Width=111, Height=18,;
    Caption="Query di rottura:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="VFLGGNCWUH",Visible=.t., Left=515, Top=314,;
    Alignment=1, Width=67, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oBox_1_32 as StdBox with uid="VHZGYCNQNP",left=5, top=72, width=783,height=1

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=23,top=150,;
    width=726+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=24,top=151,width=725+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='OUT_PUTS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPDKEYPRO_2_3.Refresh()
      this.Parent.oPDKEYPRO_2_4.Refresh()
      this.Parent.oPDCONCON_2_5.Refresh()
      this.Parent.oPDISOLAN_2_6.Refresh()
      this.Parent.oPDCODSED_2_8.Refresh()
      this.Parent.oPDCODORD_2_9.Refresh()
      this.Parent.oPDOGGETT_2_10.Refresh()
      this.Parent.oPD_TESTO_2_11.Refresh()
      this.Parent.oPDFILTRO_2_12.Refresh()
      this.Parent.oTIPRIFR_2_18.Refresh()
      this.Parent.oTIPRIFE_2_19.Refresh()
      this.Parent.oPDTIPRIA_2_20.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='OUT_PUTS'
        oDropInto=this.oBodyCol.oRow.oPDREPORT_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPDKEYPRO_2_3 as StdTrsField with uid="SGJMSHQPXA",rtseq=28,rtrep=.t.,;
    cFormVar="w_PDKEYPRO",value=space(100),;
    ToolTipText = "Identificazione documento (es. mvserial)",;
    HelpContextID = 219166021,;
    cTotal="", bFixedPos=.t., cQueryName = "PDKEYPRO",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=638, Left=122, Top=290, InputMask=replicate('X',100)

  func oPDKEYPRO_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipProcesso='IRP')
    endwith
    endif
  endfunc

  add object oPDKEYPRO_2_4 as StdTrsField with uid="XQFOFWWBZU",rtseq=29,rtrep=.t.,;
    cFormVar="w_PDKEYPRO",value=space(100),;
    ToolTipText = "Query per elenco valorizzazione filtri",;
    HelpContextID = 219166021,;
    cTotal="", bFixedPos=.t., cQueryName = "PDKEYPRO",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=616, Left=122, Top=290, InputMask=replicate('X',100)

  func oPDKEYPRO_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
    endif
  endfunc

  add object oPDCONCON_2_5 as StdTrsField with uid="RAWCHMDKDU",rtseq=30,rtrep=.t.,;
    cFormVar="w_PDCONCON",value=space(60),;
    ToolTipText = "Identificazione intestatario (es. mvtipcon+mvcodcon)",;
    HelpContextID = 9849532,;
    cTotal="", bFixedPos=.t., cQueryName = "PDCONCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=122, Top=314, InputMask=replicate('X',60)

  add object oPDISOLAN_2_6 as StdTrsField with uid="SXGRIZKBJL",rtseq=31,rtrep=.t.,;
    cFormVar="w_PDISOLAN",value=space(60),;
    ToolTipText = "Identificazione codice ISO lingua (es. lucodiso), il report sar� tradotto automaticamente nella relativa lingua",;
    HelpContextID = 142480708,;
    cTotal="", bFixedPos=.t., cQueryName = "PDISOLAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=176, Left=584, Top=314, InputMask=replicate('X',60)

  func oPDISOLAN_2_6.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
  endfunc

  add object oPDCODSED_2_8 as StdTrsField with uid="TJTVNQOFTU",rtseq=33,rtrep=.t.,;
    cFormVar="w_PDCODSED",value=space(60),;
    ToolTipText = "Identificazione sede (es. mvseddes)",;
    HelpContextID = 248100154,;
    cTotal="", bFixedPos=.t., cQueryName = "PDCODSED",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=439, Top=338, InputMask=replicate('X',60)

  func oPDCODSED_2_8.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP' and not empty(.w_PDCONCON))
    endwith
  endfunc

  add object oPDCODORD_2_9 as StdTrsField with uid="ZPOFBWQBMX",rtseq=34,rtrep=.t.,;
    cFormVar="w_PDCODORD",value=space(60),;
    ToolTipText = "Identificazione per conto di chi � spedita/ricevuta la merce(es. mvcodorn)",;
    HelpContextID = 180991290,;
    cTotal="", bFixedPos=.t., cQueryName = "PDCODORD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=122, Top=362, InputMask=replicate('X',60)

  func oPDCODORD_2_9.mCond()
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP' and not empty(.w_PDCONCON))
    endwith
  endfunc

  add object oPDOGGETT_2_10 as StdTrsMemo with uid="URZHHZZFYA",rtseq=35,rtrep=.t.,;
    cFormVar="w_PDOGGETT",value=space(0),;
    ToolTipText = "Descrizione documento, oggetto email, FAX,",;
    HelpContextID = 15889738,;
    cTotal="", bFixedPos=.t., cQueryName = "PDOGGETT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=22, Width=638, Left=122, Top=385

  add object oPD_TESTO_2_11 as StdTrsMemo with uid="YIFTLFYECM",rtseq=36,rtrep=.t.,;
    cFormVar="w_PD_TESTO",value=space(0),;
    ToolTipText = "Testo email, FAX,...",;
    HelpContextID = 249591109,;
    cTotal="", bFixedPos=.t., cQueryName = "PD_TESTO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=22, Width=638, Left=122, Top=409

  add object oPDFILTRO_2_12 as StdTrsMemo with uid="TYYIMYUCTF",rtseq=37,rtrep=.t.,;
    cFormVar="w_PDFILTRO",value=space(0),;
    ToolTipText = "Condizione di filtro su cursore di stampa",;
    HelpContextID = 4449605,;
    cTotal="", bFixedPos=.t., cQueryName = "PDFILTRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=43, Width=638, Left=122, Top=433

  func oPDFILTRO_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipProcesso='IRP')
    endwith
    endif
  endfunc

  add object oBtn_2_13 as cp_AskFile with uid="MSZEKQFMSF",width=20,height=18,;
   left=740, top=290,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare la query per la rottura del file";
    , HelpContextID = 147239722;
    , TABSTOP=.F.,cExt='VQR',var='w_PDKEYPRO';
  , bGlobalFont=.t.


  func oBtn_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipProcesso<>'IRP')
    endwith
   endif
  endfunc

  add object oTIPRIFR_2_18 as StdTrsCombo with uid="PSSHRTQBCG",rtrep=.t.,;
    cFormVar="w_TIPRIFR", RowSource=""+"Nessuna,"+"Generico,"+"Consegna,"+"Fatturazione,"+"Pagamento,"+"Avviso,"+"Residenza,"+"Stab.Org. Sogg. N.R." , ;
    HelpContextID = 35490358,;
    Height=22, Width=161, Left=122, Top=338,;
    cTotal="", cQueryName = "TIPRIFR",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTIPRIFR_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPRIFR,&i_cF..t_TIPRIFR),this.value)
    return(iif(xVal =1,'NO',;
    iif(xVal =2,'GE',;
    iif(xVal =3,'CO',;
    iif(xVal =4,'FA',;
    iif(xVal =5,'PA',;
    iif(xVal =6,'SO',;
    iif(xVal =7,'RE',;
    iif(xVal =8,'SB',;
    space(10))))))))))
  endfunc
  func oTIPRIFR_2_18.GetRadio()
    this.Parent.oContained.w_TIPRIFR = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIFR_2_18.ToRadio()
    this.Parent.oContained.w_TIPRIFR=trim(this.Parent.oContained.w_TIPRIFR)
    return(;
      iif(this.Parent.oContained.w_TIPRIFR=='NO',1,;
      iif(this.Parent.oContained.w_TIPRIFR=='GE',2,;
      iif(this.Parent.oContained.w_TIPRIFR=='CO',3,;
      iif(this.Parent.oContained.w_TIPRIFR=='FA',4,;
      iif(this.Parent.oContained.w_TIPRIFR=='PA',5,;
      iif(this.Parent.oContained.w_TIPRIFR=='SO',6,;
      iif(this.Parent.oContained.w_TIPRIFR=='RE',7,;
      iif(this.Parent.oContained.w_TIPRIFR=='SB',8,;
      0)))))))))
  endfunc

  func oTIPRIFR_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIPRIFR_2_18.mCond()
    with this.Parent.oContained
      return (EMPTY (.w_PDCODSED) and .w_TipProcesso<>'IRP' and not empty(.w_PDCONCON))
    endwith
  endfunc

  func oTIPRIFR_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! (isahr() or isalt()))
    endwith
    endif
  endfunc

  add object oTIPRIFE_2_19 as StdTrsCombo with uid="CHSVFKLBLR",rtrep=.t.,;
    cFormVar="w_TIPRIFE", RowSource=""+"Nessuna,"+"Generico,"+"Consegna,"+"Fatturazione,"+"Pagamento,"+"Sollecito\Avviso,"+"Residenza,"+"Stab.Org. Sogg. N.R.,"+"Amministrativa" , ;
    HelpContextID = 35490358,;
    Height=22, Width=161, Left=122, Top=338,;
    cTotal="", cQueryName = "TIPRIFE",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTIPRIFE_2_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPRIFE,&i_cF..t_TIPRIFE),this.value)
    return(iif(xVal =1,'NO',;
    iif(xVal =2,'GE',;
    iif(xVal =3,'CO',;
    iif(xVal =4,'FA',;
    iif(xVal =5,'PA',;
    iif(xVal =6,'SO',;
    iif(xVal =7,'RE',;
    iif(xVal =8,'SB',;
    iif(xVal =9,'AM',;
    space(10)))))))))))
  endfunc
  func oTIPRIFE_2_19.GetRadio()
    this.Parent.oContained.w_TIPRIFE = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIFE_2_19.ToRadio()
    this.Parent.oContained.w_TIPRIFE=trim(this.Parent.oContained.w_TIPRIFE)
    return(;
      iif(this.Parent.oContained.w_TIPRIFE=='NO',1,;
      iif(this.Parent.oContained.w_TIPRIFE=='GE',2,;
      iif(this.Parent.oContained.w_TIPRIFE=='CO',3,;
      iif(this.Parent.oContained.w_TIPRIFE=='FA',4,;
      iif(this.Parent.oContained.w_TIPRIFE=='PA',5,;
      iif(this.Parent.oContained.w_TIPRIFE=='SO',6,;
      iif(this.Parent.oContained.w_TIPRIFE=='RE',7,;
      iif(this.Parent.oContained.w_TIPRIFE=='SB',8,;
      iif(this.Parent.oContained.w_TIPRIFE=='AM',9,;
      0))))))))))
  endfunc

  func oTIPRIFE_2_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIPRIFE_2_19.mCond()
    with this.Parent.oContained
      return (EMPTY (.w_PDCODSED) and .w_TipProcesso<>'IRP')
    endwith
  endfunc

  func oTIPRIFE_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! isahe())
    endwith
    endif
  endfunc

  add object oPDTIPRIA_2_20 as StdTrsCheck with uid="WJSNIDSHNC",rtrep=.t.,;
    cFormVar="w_PDTIPRIA",  caption="Tutte",;
    ToolTipText = "Permette di inviare le mail/fax a tutte le sedi del tipo indicato nella combo tipo sede",;
    HelpContextID = 24853193,;
    Left=286, Top=338,;
    cTotal="", cQueryName = "PDTIPRIA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oPDTIPRIA_2_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDTIPRIA,&i_cF..t_PDTIPRIA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDTIPRIA_2_20.GetRadio()
    this.Parent.oContained.w_PDTIPRIA = this.RadioValue()
    return .t.
  endfunc

  func oPDTIPRIA_2_20.ToRadio()
    this.Parent.oContained.w_PDTIPRIA=trim(this.Parent.oContained.w_PDTIPRIA)
    return(;
      iif(this.Parent.oContained.w_PDTIPRIA=='S',1,;
      0))
  endfunc

  func oPDTIPRIA_2_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPDTIPRIA_2_20.mCond()
    with this.Parent.oContained
      return (EMPTY (.w_PDCODSED) and .w_TipProcesso<>'IRP' and .w_TIPRIFE<>'NO')
    endwith
  endfunc

  func oPDTIPRIA_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! isahe())
    endwith
    endif
  endfunc

  add object oStr_2_14 as StdString with uid="LCXGWJNYWU",Visible=.t., Left=8, Top=338,;
    Alignment=1, Width=111, Height=18,;
    Caption="Tipo sede:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="YSXCKTNKYE",Visible=.t., Left=360, Top=338,;
    Alignment=1, Width=76, Height=18,;
    Caption="Codice sede:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="ZBSKQTASWD",Visible=.t., Left=8, Top=362,;
    Alignment=1, Width=111, Height=18,;
    Caption="Per conto di:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsdm_mpdPag2 as StdContainer
    Width  = 795
    height = 478
    stdWidth  = 795
    stdheight = 478
  resizeXpos=619
  resizeYpos=320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="FONXAQPDHE",left=-1, top=4, width=798, height=473, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsdm_map",lower(this.oContained.GSDM_MAP.class))=0
        this.oContained.GSDM_MAP.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsdm_mpdPag3 as StdContainer
    Width  = 795
    height = 478
    stdWidth  = 795
    stdheight = 478
  resizeXpos=552
  resizeYpos=320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="FFVMVNXXPD",left=3, top=9, width=744, height=421, bOnScreen=.t.;


  func oLinkPC_5_1.mCond()
    with this.Parent.oContained
      return (.w_PDCHKZCP = 'S')
    endwith
  endfunc

  func oLinkPC_5_1.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(g_IZCP$'SA') Or g_CPIN='S')
    endwith
   endif
  endfunc
enddefine

* --- Defining Body row
define class tgsdm_mpdBodyRow as CPBodyRowCnt
  Width=716
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPDREPORT_2_1 as StdTrsField with uid="JCKQPMQMXX",rtseq=26,rtrep=.t.,;
    cFormVar="w_PDREPORT",value=space(100),isprimarykey=.t.,;
    ToolTipText = "Nome report riferito alla cartella di installazione (es. query\gsve_ord.frx)",;
    HelpContextID = 192980298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=353, Left=-2, Top=0, cSayPict=[repl('!',100)], cGetPict=[repl('!',100)], InputMask=replicate('X',100), bHasZoom = .t. , cLinkFile="OUT_PUTS", cZoomOnZoom="GSUT_MOU", oKey_1_1="OUNOMREP", oKey_1_2="this.w_PDREPORT"

  func oPDREPORT_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDREPORT_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPDREPORT_2_1.mZoom
      with this.Parent.oContained
        GSDM_BPD(this.Parent.oContained,"ZP")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oPDREPORT_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MOU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OUNOMREP=this.parent.oContained.w_PDREPORT
    i_obj.ecpSave()
  endproc

  add object oPDDESREP_2_2 as StdTrsField with uid="FUSKNIGKSN",rtseq=27,rtrep=.t.,;
    cFormVar="w_PDDESREP",value=space(50),;
    HelpContextID = 246400326,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=350, Left=361, Top=0, cSayPict=["@K"], cGetPict=["@K"], InputMask=replicate('X',50)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPDREPORT_2_1.When()
    return(.t.)
  proc oPDREPORT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPDREPORT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdm_mpd','PROMDOCU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PDCODPRO=PROMDOCU.PDCODPRO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsdm_mpd
func CalcMsgPDCHKARC()
   ah_ErrorMsg("Il processo documentale deve avere obbligatoriamente anche l'attivit� di archiviazione perch� collegato a classe documentale con modalit� archiviazione di tipo <E.D.S.> e attivit� di invio a Web application attivata")
return('S')
* --- Fine Area Manuale
