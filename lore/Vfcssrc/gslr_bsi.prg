* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bsi                                                        *
*              Sincronizzazione                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-16                                                      *
* Last revis.: 2012-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bsi",oParentObject)
return(i_retval)

define class tgslr_bsi as StdBatch
  * --- Local variables
  w_RESULT = space(250)
  w_PROG = 0
  w_LSSERIAL = space(10)
  w_Data = ctod("  /  /  ")
  w_Time = space(15)
  w_Ore = 0
  w_Min = 0
  w_Sec = 0
  w_PWDZIPLOG = space(254)
  w_PATERR = space(254)
  w_DESTINATARIO = space(2)
  w_TipoTrans = space(15)
  w_FLUSSO = 0
  w_Note = space(0)
  w_CODSED = space(2)
  w_ENTITA = space(15)
  w_NUMINS = 0
  w_NUMUPD = 0
  w_NUMDEL = 0
  w_NUMRIF = 0
  w_NUMOVW = 0
  w_LOOP = 0
  w_DBFFPT = space(254)
  w_OK = .f.
  w_PATH = space(254)
  w_TempPat = space(254)
  w_PatSalLog = space(254)
  w_PatErrLog = space(254)
  w_PatZip = space(254)
  * --- WorkFile variables
  PUBLMAST_idx=0
  PUBLDETT_idx=0
  LOG_SINC_idx=0
  DETT_LOG_idx=0
  ENT_LOG_idx=0
  PARALORE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sincronizzazione...
    * --- Recupero l'elenco delle sedi Pubblicatrici ordinate per ordine.
    *     Per ognuna vado a cercare se c'� un pacchetto...
    this.oParentObject.w_Msg = ""
    AddMsgNL("Elaborazione iniziata alle %1",this, Time() , , , ,, )
    if Not(CHKPATER())
      AddMsgNL("Il path di log e salvataggio dati indicato nei parametri logistica non esiste%0Impossibile continuare",this)
      AddMsgNL("Elaborazione terminata alle %1",this, Time() , , , ,, )
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_BACKUP="S"
      AddMsgNL("Avvio back up del database",this)
      this.w_RESULT = GSUT_BBT( this , this.oParentObject.w_DAYBCK , this.oParentObject.w_BCKNUM )
      if Not Empty( this.w_RESULT )
        AddMsgNL("Back up del database ha generato l'errore: %1",this , this.w_RESULT )
        i_retcode = 'stop'
        return
      else
        AddMsgNL("Back up del database terminato alle %1",this , Time())
      endif
    endif
    * --- Cursore valorizzato per la gestione log da questo batch e da GENDBLOG
    *     Con questo cursore verranno valorizzate le tabelle DETT_LOG e ENT_LOG
    Local L_OldArea
     
 CREATE CURSOR LOG_ERR(TIPO C(1), DSSERIAL C(10), DS__SEDE C(2), DSNUMPAC N(10,0), DEENTITA C(15), ; 
 DENUMINS N(6), DENUMUPD N(6), DENUMDEL N(6), DENUMRIF N(6), DENUMOVW N(6), DEZIPERR C(254), DEZIPOLD C(254))
    * --- Array che verr� riempito dalla Funzione Package
    *     1 = Nome archivio
    *     2 = DBF di appoggio con i dati
    *     3 = Entit� archivio
    Dimension ArrDbf[1,3]
    * --- Leggo la sede che Importa...
    * --- Read from PARALORE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PARALORE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARALORE_idx,2],.t.,this.PARALORE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PL__SEDE,PLPATERR"+;
        " from "+i_cTable+" PARALORE where ";
            +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PL__SEDE,PLPATERR;
        from (i_cTable) where;
            PLCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESTINATARIO = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
      this.w_PATERR = NVL(cp_ToDate(_read_.PLPATERR),cp_NullValue(_read_.PLPATERR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_LSSERIAL = Space(10)
    i_Conn=i_TableProp[this.LOG_SINC_IDX, 3]
    * --- Try
    local bErr_036A57A8
    bErr_036A57A8=bTrsErr
    this.Try_036A57A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore determinazione progressivo log elaborazione",this, )
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_036A57A8
    * --- End
    * --- Data del giorno, non di sistema
    this.w_Data = Date()
    * --- Inserisco il record nell'anagrafica del Log
    * --- Insert into LOG_SINC
    i_nConn=i_TableProp[this.LOG_SINC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_SINC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_SINC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSSERIAL"+",LS__DATA"+",LSPUBSIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOG_SINC','LSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Data),'LOG_SINC','LS__DATA');
      +","+cp_NullLink(cp_ToStrODBC("S"),'LOG_SINC','LSPUBSIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LS__DATA',this.w_Data,'LSPUBSIN',"S")
      insert into (i_cTable) (LSSERIAL,LS__DATA,LSPUBSIN &i_ccchkf. );
         values (;
           this.w_LSSERIAL;
           ,this.w_Data;
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    do case
      case this.oParentObject.w_TIPTRS="G"
        this.w_TipoTrans = Ah_MsgFormat("Globale")
      case this.oParentObject.w_TIPTRS="S"
        this.w_TipoTrans = Ah_MsgFormat("Per sede")
      case this.oParentObject.w_TIPTRS="E"
        this.w_TipoTrans = Ah_MsgFormat("Per entit�")
      case this.oParentObject.w_TIPTRS="I"
        this.w_TipoTrans = Ah_MsgFormat("Per istanza")
    endcase
    AddMsgNL("Transazione: %1",this, this.w_TipoTrans , , , ,, )
    * --- Try
    local bErr_0376B510
    bErr_0376B510=bTrsErr
    this.Try_0376B510()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_TIPTRS="G"
        * --- Se globale torno indietro 
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0376B510
    * --- End
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Calcolo i valori temporali per inserire il log.
    *     Cerco di mantenere gli stessi valori indicati nel messaggio
    this.w_Time = Alltrim(Time())
    this.w_Ore = Val(Left(this.w_Time, 2))
    this.w_Min = Val(Substr(this.w_Time, At(":", this.w_Time)+1, 2))
    this.w_Sec = VAl(Right(this.w_Time, 2))
    this.w_Note = SaveMemo( this.oParentObject.w_Msg )
    * --- Aggiorno l'anagrafica Log con i dati temporali di fine sincronizzazione
    * --- Write into LOG_SINC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOG_SINC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_SINC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.LOG_SINC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LS___LOG ="+cp_NullLink(cp_ToStrODBC(this.w_Note),'LOG_SINC','LS___LOG');
      +",LS___ORA ="+cp_NullLink(cp_ToStrODBC(this.w_Ore),'LOG_SINC','LS___ORA');
      +",LS___MIN ="+cp_NullLink(cp_ToStrODBC(this.w_Min),'LOG_SINC','LS___MIN');
      +",LS___SEC ="+cp_NullLink(cp_ToStrODBC(this.w_Sec),'LOG_SINC','LS___SEC');
          +i_ccchkf ;
      +" where ";
          +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
             )
    else
      update (i_cTable) set;
          LS___LOG = this.w_Note;
          ,LS___ORA = this.w_Ore;
          ,LS___MIN = this.w_Min;
          ,LS___SEC = this.w_Sec;
          &i_ccchkf. ;
       where;
          LSSERIAL = this.w_LSSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno il dettaglio del log in questo punto perch� sono fuori da tutte le transazioni
    if Used("LOG_ERR")
       
 Select * from LOG_ERR into cursor LOG_ERR Order by TIPO 
 Select("LOG_ERR") 
 Go Top
      do while Not EOF()
        * --- Avendo ordinato il cursore per tipo, prima avr� tutti i record del dettaglio del log
        *     poi i record del dettaglio delle entit�. Cos� evito i problemi di integrit�
        *     
        this.w_CODSED = DS__SEDE
        this.w_ENTITA = DEENTITA
        if TIPO="A"
          * --- Inserisco prima tutti i record del dettaglio del log
          this.w_PROG = DSNUMPAC
          * --- Insert into DETT_LOG
          i_nConn=i_TableProp[this.DETT_LOG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DETT_LOG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DETT_LOG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DSSERIAL"+",DS__SEDE"+",DSNUMPAC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'DETT_LOG','DSSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODSED),'DETT_LOG','DS__SEDE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PROG),'DETT_LOG','DSNUMPAC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DSSERIAL',this.w_LSSERIAL,'DS__SEDE',this.w_CODSED,'DSNUMPAC',this.w_PROG)
            insert into (i_cTable) (DSSERIAL,DS__SEDE,DSNUMPAC &i_ccchkf. );
               values (;
                 this.w_LSSERIAL;
                 ,this.w_CODSED;
                 ,this.w_PROG;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if TIPO= "B"
          * --- poi tutti i record del dettaglio per entit�
          this.w_NUMINS = DENUMINS
          this.w_NUMUPD = DENUMUPD
          this.w_NUMDEL = DENUMDEL
          this.w_NUMRIF = DENUMRIF
          this.w_NUMOVW = DENUMOVW
          this.w_PatSalLog = DEZIPOLD
          this.w_PatErrLog = DEZIPERR
          * --- Insert into ENT_LOG
          i_nConn=i_TableProp[this.ENT_LOG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ENT_LOG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ENT_LOG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DESERIAL"+",DE__SEDE"+",DEENTITA"+",DENUMINS"+",DENUMUPD"+",DENUMDEL"+",DENUMRIF"+",DENUMOVW"+",DEZIPERR"+",DEZIPOLD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'ENT_LOG','DESERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODSED),'ENT_LOG','DE__SEDE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ENTITA),'ENT_LOG','DEENTITA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMINS),'ENT_LOG','DENUMINS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMUPD),'ENT_LOG','DENUMUPD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDEL),'ENT_LOG','DENUMDEL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'ENT_LOG','DENUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMOVW),'ENT_LOG','DENUMOVW');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PatErrLog),'ENT_LOG','DEZIPERR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PatSalLog),'ENT_LOG','DEZIPOLD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_LSSERIAL,'DE__SEDE',this.w_CODSED,'DEENTITA',this.w_ENTITA,'DENUMINS',this.w_NUMINS,'DENUMUPD',this.w_NUMUPD,'DENUMDEL',this.w_NUMDEL,'DENUMRIF',this.w_NUMRIF,'DENUMOVW',this.w_NUMOVW,'DEZIPERR',this.w_PatErrLog,'DEZIPOLD',this.w_PatSalLog)
            insert into (i_cTable) (DESERIAL,DE__SEDE,DEENTITA,DENUMINS,DENUMUPD,DENUMDEL,DENUMRIF,DENUMOVW,DEZIPERR,DEZIPOLD &i_ccchkf. );
               values (;
                 this.w_LSSERIAL;
                 ,this.w_CODSED;
                 ,this.w_ENTITA;
                 ,this.w_NUMINS;
                 ,this.w_NUMUPD;
                 ,this.w_NUMDEL;
                 ,this.w_NUMRIF;
                 ,this.w_NUMOVW;
                 ,this.w_PatErrLog;
                 ,this.w_PatSalLog;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        Skip
      enddo
    endif
    * --- Chiudo il cursore
    if Used("LOG_ERR")
       
 Select LOG_ERR 
 Use
    endif
  endproc
  proc Try_036A57A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    cp_NextTableProg(this, i_Conn, "SELOG", "i_codazi,w_LSSERIAL")
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0376B510()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPTRS="G"
      * --- Se transazione per Sede apro la transazione e la chiudo alla fine della sincronizzazione della singola sede
      * --- begin transaction
      cp_BeginTrs()
    endif
    * --- Select from PUBLMAST
    i_nConn=i_TableProp[this.PUBLMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PUBLMAST_idx,2],.t.,this.PUBLMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PUBLMAST ";
          +" where SE__TIPO='P'";
          +" order by SEORDINE";
           ,"_Curs_PUBLMAST")
    else
      select * from (i_cTable);
       where SE__TIPO="P";
       order by SEORDINE;
        into cursor _Curs_PUBLMAST
    endif
    if used('_Curs_PUBLMAST')
      select _Curs_PUBLMAST
      locate for 1=1
      do while not(eof())
      * --- Tento di aprrire il pacchetto i-esimo legato alla sede di pubblicazione...
      this.w_PROG = Nvl( _Curs_PUBLMAST.SEULTPRO, 0 ) + 1
      this.w_RESULT = PACKAGE( _Curs_PUBLMAST.SECODICE , this.w_PROG , @ArrDbf , this , .f., this.oParentObject.w_VERBOSE )
      * --- Password Zip dei DBF generati per log errori
      this.w_PWDZIPLOG = Nvl(_Curs_PUBLMAST.SEPWDZIP,"")
      * --- Memorizzo nel log 
       
 L_OldArea= Select() 
 INSERT INTO LOG_ERR (TIPO, DSSERIAL, DS__SEDE, DSNUMPAC) Values ("A", this.w_LSSERIAL, _Curs_PUBLMAST.SECODICE, this.w_PROG) 
 Select(L_OldArea)
      if Not Empty( this.w_RESULT )
        AddMsgNL("Errore apertura pacchetto numero %1 dalla sede %2: %3", this , Alltrim(Str( this.w_PROG )) , _Curs_PUBLMAST.SECODICE , this.w_RESULT ,, ,)
      else
        * --- Try
        local bErr_03769F20
        bErr_03769F20=bTrsErr
        this.Try_03769F20()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if this.oParentObject.w_TIPTRS="S"
            * --- Se per sede torno indietro
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          if this.oParentObject.w_TIPTRS="G"
            * --- Raise
            i_Error="Error"
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03769F20
        * --- End
      endif
        select _Curs_PUBLMAST
        continue
      enddo
      use
    endif
    if this.oParentObject.w_TIPTRS="G"
      * --- Chiudo la transazione globale. Se fallisce un solo record non aggiorno
      * --- commit
      cp_EndTrs(.t.)
    endif
    return
  proc Try_03769F20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPTRS="S"
      * --- Se transazione per Sede apro la transazione e la chiudo alla fine della sincronizzazione della singola sede
      * --- begin transaction
      cp_BeginTrs()
    endif
    * --- Determino l'elenco delle entit� da sincronizzare per la sede in ordine
    *     di riga
    * --- Se nei parametri della logistica remota � specificato la sincronizzazione per flussi allora le entit� nelle sedi vegono elaborate in 2 fasi :
    *     1� fase : le entit� di ogni sede vengono elaborate in ordine inverso prendendo solo i record cancellati
    *     2� fase : le entit� di ogni sede vengono elaborate nell ordine specificato nelle sedi prendendo solo tutti i record non cancellati
    *     Nel caso in cui nei parametri della logistica remota non sia stata specificata la sincronizzazione per flussi allora ogni entit� viene sincronizzata una sola volta per ogni sede (i record cancellati e non cancellati vengono sincronizzati nello stesso flusso)
    if this.oParentObject.w_PLSINCAN="S"
      this.w_FLUSSO = 1
      * --- Select from PUBLDETT
      i_nConn=i_TableProp[this.PUBLDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PUBLDETT_idx,2],.t.,this.PUBLDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PUBLDETT ";
            +" where SECODICE="+cp_ToStrODBC(_Curs_PUBLMAST.SECODICE)+" And SE__TIPO="+cp_ToStrODBC(_Curs_PUBLMAST.SE__TIPO)+"";
            +" order by CPROWORD desc";
             ,"_Curs_PUBLDETT")
      else
        select * from (i_cTable);
         where SECODICE=_Curs_PUBLMAST.SECODICE And SE__TIPO=_Curs_PUBLMAST.SE__TIPO;
         order by CPROWORD desc;
          into cursor _Curs_PUBLDETT
      endif
      if used('_Curs_PUBLDETT')
        select _Curs_PUBLDETT
        locate for 1=1
        do while not(eof())
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_PUBLDETT
          continue
        enddo
        use
      endif
      this.w_FLUSSO = 2
    else
      this.w_FLUSSO = 0
    endif
    * --- Select from PUBLDETT
    i_nConn=i_TableProp[this.PUBLDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PUBLDETT_idx,2],.t.,this.PUBLDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PUBLDETT ";
          +" where SECODICE="+cp_ToStrODBC(_Curs_PUBLMAST.SECODICE)+" And SE__TIPO="+cp_ToStrODBC(_Curs_PUBLMAST.SE__TIPO)+"";
          +" order by CPROWORD";
           ,"_Curs_PUBLDETT")
    else
      select * from (i_cTable);
       where SECODICE=_Curs_PUBLMAST.SECODICE And SE__TIPO=_Curs_PUBLMAST.SE__TIPO;
       order by CPROWORD;
        into cursor _Curs_PUBLDETT
    endif
    if used('_Curs_PUBLDETT')
      select _Curs_PUBLDETT
      locate for 1=1
      do while not(eof())
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_PUBLDETT
        continue
      enddo
      use
    endif
    * --- Memorizzo nella sede pubblicatrice che ho ricevuto il pacchetto...
    * --- Write into PUBLMAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PUBLMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PUBLMAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PUBLMAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SEULTPRO ="+cp_NullLink(cp_ToStrODBC(this.w_PROG),'PUBLMAST','SEULTPRO');
          +i_ccchkf ;
      +" where ";
          +"SECODICE = "+cp_ToStrODBC(_Curs_PUBLMAST.SECODICE);
          +" and SE__TIPO = "+cp_ToStrODBC("P");
             )
    else
      update (i_cTable) set;
          SEULTPRO = this.w_PROG;
          &i_ccchkf. ;
       where;
          SECODICE = _Curs_PUBLMAST.SECODICE;
          and SE__TIPO = "P";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    AddMsgNL("Terminata sincronizzazione sede %1", this , _Curs_PUBLMAST.SECODICE , , ,, ,)
    AddMsgNL("", this , , , ,, ,)
    if this.oParentObject.w_TIPTRS="S"
      * --- Chiudo la transazione per sede. Se fallisce un solo record non la singola sede
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella i DBF dell'array in temp
    this.w_LOOP = 1
    do while this.w_LOOP<=Alen( ArrDbf , 1)
      if Type("ArrDbf[ this.w_LOOP,  2] ")="C"
        * --- Cancello i file DBF creati nella TEMP per sincronizzare
        this.w_DBFFPT = Alltrim(ArrDbf[ this.w_LOOP, 2])
        if Used( JUSTSTEM ( this.w_DBFFPT ) )
          * --- Se � in uso lo devo chiudere prima di cancellarlo altrimenti da errore
           
 Select (JUSTSTEM(this.w_DBFFPT) ) 
 Use
        endif
        * --- Elimino il DBF
        Erase(this.w_DBFFPT)
        * --- Cancello anche i file FPT
        this.w_DBFFPT = StrTran(this.w_DBFFPT, ".DBF", ".FPT")
        Erase(this.w_DBFFPT)
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Esce
    AddMsgNL("Elaborazione terminata alle %1",this, Time() , , , ,, )
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Zip con DBF errori
    this.w_OK = .T.
    if Directory(this.w_Path)
      * --- Se creata la cartella per salvataggio dati preesistenti, zippo il contenuto in uno
      *     Zip con lo stesso nome dell'entit�
      * --- Comprimo la cartella
      Local L_RetMsg
      L_RetMsg = ""
      if ! ZipUnZip("Z", this.w_PatZip, this.w_Path, this.w_PWDZIPLOG, @L_RetMsg)
        AddMsgNL("Problemi nella creazione dello zip %1%0%2",this, Alltrim(this.w_PatZip), L_RetMsg)
        this.w_OK = .F.
      endif
      * --- Elimino i DBF e FPT e le cartelle
      if Not DeleteFolder(this.w_TempPat+ Alltrim( _Curs_PUBLMAST.SECODICE ))
        AddMsgNL("Problemi nella cancellazione dei DBF cartella %1",this, this.w_TempPat+ Alltrim( _Curs_PUBLMAST.SECODICE ) )
        this.w_OK = .F.
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    AddMsgNL("Sincronizzazione sede %1 entit� %2", this , _Curs_PUBLMAST.SECODICE , _Curs_PUBLDETT.SEENTITA , ,, ,)
     
 Dimension ARRMIRROR (4) 
 ARRMIRROR=0
    * --- Try
    local bErr_036CBB10
    bErr_036CBB10=bTrsErr
    this.Try_036CBB10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_TIPTRS="E"
        * --- Ripristino i dati
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      if this.oParentObject.w_TIPTRS="S" Or this.oParentObject.w_TIPTRS="G"
        * --- Raise
        i_Error="Error"
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_036CBB10
    * --- End
  endproc
  proc Try_036CBB10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPTRS="E"
      * --- Se transazione per entit� apro la transazione e la chiudo dopo la sincronizzazione della singola entit�
      * --- begin transaction
      cp_BeginTrs()
    endif
    this.w_RESULT = SINCRONIZZA ( _Curs_PUBLMAST.SECODICE , _Curs_PUBLDETT.SEENTITA , @ArrDbf , @ArrMirror, This, this.oParentObject.w_VERBOSE, this.oParentObject.w_TIPTRS, this.w_LSSERIAL , this.w_FLUSSO)
    if Not Empty(this.w_PATERR)
      * --- AddBs: gestisce la '\' di fine path. Se c'� ok, se non c'� ce lo mette
      this.w_PATERR = Trim( AddBs(this.w_PATERR) )
      this.w_TempPat = TEMPADHOC() + "\Remote\Log\"
      * --- Se attivata la gestione di creazione DBF di log errori...
      * --- Path creazione DBF di salvataggio dati preesistenti
      this.w_PATH = this.w_TempPat+ Alltrim( _Curs_PUBLMAST.SECODICE ) +"\"+ this.w_DESTINATARIO +"\OldData\"+Alltrim( Str( this.w_PROG ) )+"\"
      this.w_PatZip = this.w_PATERR+"OldData_"+Alltrim(_Curs_PUBLDETT.SEENTITA)+"_"+Alltrim( _Curs_PUBLMAST.SECODICE ) +"_"+ this.w_DESTINATARIO+"_"+Right("0000000000"+Alltrim(str(this.w_Prog)),10)+".Ahz"
      this.w_PatSalLog = this.w_PatZip
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not File(this.w_PatSalLog)
        this.w_PatSalLog = ""
      else
        AddMsgNL("Creato zip con i dbf di salvataggio dati %1", this , Alltrim(this.w_PatSalLog) , , ,, ,)
      endif
      * --- Path creazione DBF di salvataggio dati errati
      this.w_PATH = this.w_TempPat+ Alltrim( _Curs_PUBLMAST.SECODICE ) +"\"+ this.w_DESTINATARIO +"\ErrorData\"+Alltrim( Str( this.w_PROG ) )+"\"
      this.w_PatZip = this.w_PATERR+"ErrorData_"+Alltrim(_Curs_PUBLDETT.SEENTITA)+"_"+Alltrim( _Curs_PUBLMAST.SECODICE ) +"_"+ this.w_DESTINATARIO+"_"+Right("0000000000"+Alltrim(str(this.w_Prog)),10)+".Ahz"
      this.w_PatErrLog = this.w_PatZip
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not File(this.w_PatErrLog)
        this.w_PatErrLog = ""
      else
        AddMsgNL("Creato zip con i dbf dei dati rifiutati %1", this , Alltrim(this.w_PatErrLog) , , ,, ,)
      endif
      * --- Aggiorno il Log
      *     
      *     Controllo che il record non sia gi� presente nel cursore per inserimento della GENDBLOG
      *     In questo caso vado in modifica
       
 L_OldArea= Select() 
 Select("LOG_ERR") 
 Locate For DSSERIAL = this.w_LSSERIAL And DS__SEDE = _Curs_PUBLMAST.SECODICE And DEENTITA = _Curs_PUBLDETT.SEENTITA
      if Found()
        * --- Se trovo il record modifico
        *     Potrebbe averlo gi� inserito la GENDBLOG nel caso di record rifiutati o sovrascritti
         
 Replace DENUMINS with ARRMIRROR(2) 
 Replace DENUMUPD with ARRMIRROR(1) 
 Replace DENUMDEL with ARRMIRROR(3) 
 Replace DEZIPERR with this.w_PatErrLog 
 Replace DEZIPOLD with this.w_PatSalLog
      else
        * --- altrimenti inserisco il record
        INSERT INTO LOG_ERR Values ("B", this.w_LSSERIAL, _Curs_PUBLMAST.SECODICE, this.w_PROG, _Curs_PUBLDETT.SEENTITA, ; 
 ARRMIRROR(2), ARRMIRROR(1), ARRMIRROR(3), 0, 0, this.w_PatErrLog, this.w_PatSalLog)
      endif
      Select(L_OldArea)
    endif
    if Not Empty( this.w_RESULT )
      AddMsgNL("Errore sincronizzazione%0%1", this , this.w_RESULT , , ,, ,)
      * --- Raise
      i_Error="ERROR"
      return
    else
      * --- Visualizzo il risultato...
      AddMsgNL("", this , , , , ,, )
      AddMsgNL("Aggiornato Mirror archivio %1",this, _Curs_PUBLDETT.SEENTITA , , , ,, )
      AddMsgNL("Record aggiornati %1",this, Alltrim(Str(ARRMIRROR (1))) , , , ,, )
      AddMsgNL("Record inseriti %1",this, Alltrim(Str(ARRMIRROR (2))) , , , ,, )
      AddMsgNL("Record eliminati %1",this, Alltrim(Str(ARRMIRROR (3))) , , , ,, )
      AddMsgNL("Record in mirror forzatamente allineati %1",this, Alltrim(Str(ARRMIRROR (4))) , , , ,, )
    endif
    if this.oParentObject.w_TIPTRS="E"
      * --- Chiudo la transazione per entit�.
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PUBLMAST'
    this.cWorkTables[2]='PUBLDETT'
    this.cWorkTables[3]='LOG_SINC'
    this.cWorkTables[4]='DETT_LOG'
    this.cWorkTables[5]='ENT_LOG'
    this.cWorkTables[6]='PARALORE'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PUBLMAST')
      use in _Curs_PUBLMAST
    endif
    if used('_Curs_PUBLDETT')
      use in _Curs_PUBLDETT
    endif
    if used('_Curs_PUBLDETT')
      use in _Curs_PUBLDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
