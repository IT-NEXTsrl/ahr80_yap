* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bce                                                        *
*              Controllo tabella principale                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-19                                                      *
* Last revis.: 2006-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bce",oParentObject)
return(i_retval)

define class tgslr_bce as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_MESS = space(254)
  w_OK = .f.
  * --- WorkFile variables
  SUENMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifiche alla conferma dell'entit�
    this.w_OK = .F.
    this.oParentObject.w_RESCHK = 0
    this.w_PADRE = THIS.oParentObject
    * --- Verifica che le tabelle principali siano Super Entit�
    this.w_PADRE.MarkPos()     
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow()
        if this.oParentObject.w_ENPRINCI="S"
          * --- Read from SUENMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SUENMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SUENMAST_idx,2],.t.,this.SUENMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" SUENMAST where ";
                  +"SUCODICE = "+cp_ToStrODBC(this.oParentObject.w_EN_TABLE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  SUCODICE = this.oParentObject.w_EN_TABLE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              this.w_MESS = "L'archivio principale deve essere una super entit�"
              this.oParentObject.w_ENPRINCI = "N"
              this.w_PADRE.SaveRow()     
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Ci deve essere una tabella principale
          this.w_OK = .T.
        else
          * --- Se tabella collegata il link � obbligatorio
          if Empty(this.oParentObject.w_EN__LINK)
            this.w_MESS = "Specificare il legame per tutte le tabelle collegate"
          endif
        endif
      endif
      if Not this.w_OK
        this.w_MESS = "Deve essere specificata una tabella principale"
      endif
      * --- Se tutto ok passo alla prossima riga altrimenti esco...
      if not Empty( this.w_MESS )
        ah_ErrorMsg("%1",,"", this.w_MESS)
        this.oParentObject.w_RESCHK = -1
        Exit
      else
        this.w_PADRE.NextRow()     
      endif
    enddo
    this.w_PADRE.RePos(.F.)     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SUENMAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
