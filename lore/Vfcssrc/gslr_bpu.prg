* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bpu                                                        *
*              Lancia pubblicazione                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-18                                                      *
* Last revis.: 2008-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bpu",oParentObject)
return(i_retval)

define class tgslr_bpu as StdBatch
  * --- Local variables
  w_ARCHIVIO = space(30)
  w_PADRE = .NULL.
  w_RESULT = space(254)
  w_IDXSEDE = 0
  w_LOOP = 0
  w_PLPATBCK = space(254)
  w_AGGTABSECO = .f.
  w_TSM_ARCHI = space(30)
  w_TSM_PADRE = space(30)
  w_Modificati = 0
  w_SEDE = space(2)
  w_LSSERIAL = space(10)
  w_Data = ctod("  /  /  ")
  w_Time = space(15)
  w_Ore = space(2)
  w_Min = space(2)
  w_Sec = 0
  w_Note = space(0)
  * --- WorkFile variables
  TAB_SECO_idx=0
  LOG_SINC_idx=0
  PARALORE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la pubblicazione
    *     ======================
    *     Recupera tutte le sedi di tipo ricevente e per ognuna determina l'elenco
    *     delle entit� da pubblicare..
    this.w_PADRE = this.oParentObject
    * --- Leggo la sede che esporta...
    * --- Read from PARALORE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PARALORE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARALORE_idx,2],.t.,this.PARALORE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PLPATBCK"+;
        " from "+i_cTable+" PARALORE where ";
            +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PLPATBCK;
        from (i_cTable) where;
            PLCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PLPATBCK = NVL(cp_ToDate(_read_.PLPATBCK),cp_NullValue(_read_.PLPATBCK))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo per la copia di backup (solo per pubblicazion "vera")
    if this.oParentObject.w_ONLYMIRR="N"
      if EMPTY (this.w_PLPATBCK) 
        if NOT AH_YESNO ("Nei parametri della logistica remota non � stato specificato il percorso per la copia di backup dei pacchetti. Procedo comunque?" )
          i_retcode = 'stop'
          return
        endif
      else
        if NOT DIRECTORY (this.w_PLPATBCK) 
          if NOT AH_YESNO ("Il percorso specificato nei parametri della logistica remota per le copie di backup dei pacchetti non � un percorso valido. Procedo comunque?" )
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    endif
    this.oParentObject.w_Msg = ""
    AddMsgNL("Elaborazione iniziata alle %1",this, Time() , , , ,, )
    if this.oParentObject.w_BACKUP="S"
      AddMsgNL("Avvio back up del database",this)
      this.w_RESULT = GSUT_BBT( this , this.oParentObject.w_DAYBCK , this.oParentObject.w_BCKNUM )
      if Not Empty( this.w_RESULT )
        AddMsgNL("Back up del database ha generato l'errore: %1",this , this.w_RESULT )
        i_retcode = 'stop'
        return
      else
        AddMsgNL("Back up del database terminato alle %1",this , Time())
      endif
    endif
    do case
      case this.oParentObject.w_ONLYMIRR="S"
        AddMsgNL("L'elaborazione prevede il solo aggiornamento delle tabelle di mirror delle super entit� presenti nelle sedi riceventi.",this)
        AddMsgNL("Non verr� generato il pacchetto dati e nemmeno aggiornato il progressivo nelle sedi",this)
      case this.oParentObject.w_ONLYMIRR="T"
        AddMsgNL("L'elaborazione prevede il solo aggiornamento delle tabelle di mirror di tutte le super entit�",this)
        AddMsgNL("Non verr� generato il pacchetto dati e nemmeno aggiornato il progressivo nelle sedi",this)
      otherwise
        if Not(CHKPATER())
          AddMsgNL("Il path di log e salvataggio dati indicato nei parametri logistica non esiste%0Impossibile continuare",this)
          AddMsgNL("Elaborazione terminata alle %1",this, Time() , , , ,, )
          i_retcode = 'stop'
          return
        endif
    endcase
    Dimension ARRCALC (1,4)
    * --- Azzero l'Array che verr� riempito dalla Funzione
    *     1 = Nome archivio
    *     2 = DBF di appoggio con i dati
    *     3 = Sede di destinazione
    *     4 = Entit� archivio
     
 ARRCALC(1,1)="" 
 ARRCALC(1,2)="" 
 ARRCALC(1,3)="" 
 ARRCALC(1,4)=""
    * --- Try
    local bErr_0263A5A8
    bErr_0263A5A8=bTrsErr
    this.Try_0263A5A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AddMsgNL("Pubblicazione archivio %1 errore: %2",this, alltrim(this.w_ARCHIVIO) , this.w_RESULT , , ,, )
      AddMsgNL("%1", this.w_PADRE, Repl("#", 50) )
      AddMsgNL("Attenzione: la creazione del pacchetto dati � fallita per gli errori sopra indicati", this.w_PADRE, this.w_SEDE )
      AddMsgNL("%1", this.w_PADRE, Repl("#", 50) )
    endif
    bTrsErr=bTrsErr or bErr_0263A5A8
    * --- End
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_ONLYMIRR="N"
      * --- Nel caso di solo aggiornamento mirror non creo il log
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_EDIT = .F.
  endproc
  proc Try_0263A5A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Funzione che cerca le tabelle secondarie con mirror di w_ARCHIVIO. In questa funzione devo eseguire la frase sql che mi ritorma
    *     record se ci sono record modificati e aggiorna la super entit� modificando il CPCCCHK a mano In questo caso la funzione torna .T.
    *     In questo caso devo rieseguire CREALRTEMP per prendere i nuovi record
    this.w_AGGTABSECO = .F.
    this.w_LOOP = 1
    * --- ARR_TSM contiene le TSM modificate e il numero di record del padre modificati
    *     
    *     ARR_TSM(n ,1) -> Tabella TSM
    *     ARR_TSM(n ,2) -> Tabella Super Entit� del TSM
    *     ARR_TSM(n ,3) -> Numero record Super Entit� modificati (CPCCCHK)
    AddMsgNL("Analisi differenze su TSM",this)
    Dimension ARR_TSM( 1,6)
    * --- Select from gslr_qet
    do vq_exec with 'gslr_qet',this,'_Curs_gslr_qet','',.f.,.t.
    if used('_Curs_gslr_qet')
      select _Curs_gslr_qet
      locate for 1=1
      do while not(eof())
      this.w_TSM_ARCHI = _Curs_gslr_qet.TSCODTAB
      * --- Crea il temporaneo con le righe della TSM che devono essere elaborate
      this.w_RESULT = creatsmtemp( this.w_TSM_ARCHI , this.w_PADRE , this.oParentObject.w_VERBOSE )
      if Empty( this.w_RESULT )
        * --- Se creato elenco differenze di tutte le TSM aggiorno tutte le super entit� cui fanno riferimento
        * --- Select from TAB_SECO
        i_nConn=i_TableProp[this.TAB_SECO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_SECO_idx,2],.t.,this.TAB_SECO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select TSCODICE  from "+i_cTable+" TAB_SECO ";
              +" where TSCODTAB="+cp_ToStrODBC(this.w_TSM_ARCHI)+"";
               ,"_Curs_TAB_SECO")
        else
          select TSCODICE from (i_cTable);
           where TSCODTAB=this.w_TSM_ARCHI;
            into cursor _Curs_TAB_SECO
        endif
        if used('_Curs_TAB_SECO')
          select _Curs_TAB_SECO
          locate for 1=1
          do while not(eof())
          this.w_TSM_PADRE = _Curs_TAB_SECO.TSCODICE
          * --- Aggiorna i record della super entit� relativa alla TSM modificata
           
 Dimension ARR_TSM( this.w_LOOP ,6) 
 Arr_Tsm(this.w_Loop, 1) = Alltrim(this.w_TSM_ARCHI) 
 Arr_Tsm(this.w_Loop, 2) = Alltrim(this.w_TSM_PADRE) 
 Arr_Tsm(this.w_Loop, 3) = 0 
 Arr_Tsm(this.w_Loop, 4) = 0 
 Arr_Tsm(this.w_Loop, 5) = 0 
 Arr_Tsm(this.w_Loop, 6) = 0 
 
 Local L_RetNum 
 L_RetNum=0
          this.w_RESULT = AGGTSMENT( this.w_TSM_ARCHI , this.w_TSM_PADRE, @L_RetNum , this.w_PADRE , this.oParentObject.w_VERBOSE)
          * --- Le variabili UpdRec, InsRec, DelRec servirebbero solo se AGGTSMMIR venisse
          *     lanciato contestualmente alla singola TSM per ogni Super Entit�
          *     Lanciato massivamente non riesco ad avere la Super Entit� per la quale ho 
          *     aggiornato i dati
           
 Arr_Tsm(this.w_Loop, 3) = L_RetNum 
 
 Local L_UpdRec, L_InsRec, L_DelRec 
 L_UpdRec=0 
 L_InsRec=0 
 L_DelRec=0
          this.w_LOOP = this.w_LOOP + 1
            select _Curs_TAB_SECO
            continue
          enddo
          use
        endif
      else
        * --- Drop temporary table TEMP_TSM
        i_nIdx=cp_GetTableDefIdx('TEMP_TSM')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TEMP_TSM')
        endif
        * --- Raise
        i_Error=this.w_RESULT
        return
      endif
      * --- Aggiorno il mirror della TSM
      if Empty( this.w_RESULT )
        this.w_RESULT = AGGTSMMIR( this.w_TSM_ARCHI , @L_UpdRec, @L_InsRec, @L_DelRec , this.w_PADRE , this.oParentObject.w_VERBOSE )
        if Not Empty(this.w_Result)
          AddMsgNL("Impossibile aggiornare super entit� per differenze riscontrate su TSM",this)
        endif
      else
        * --- Drop temporary table TEMP_TSM
        i_nIdx=cp_GetTableDefIdx('TEMP_TSM')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TEMP_TSM')
        endif
        * --- Raise
        i_Error=this.w_RESULT
        return
      endif
      * --- Elimino tabella temporanea di appoggio
      * --- Drop temporary table TEMP_TSM
      i_nIdx=cp_GetTableDefIdx('TEMP_TSM')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TEMP_TSM')
      endif
        select _Curs_gslr_qet
        continue
      enddo
      use
    endif
    if this.w_LOOP>1
      this.w_Modificati = 0
      this.w_LOOP = 1
      do while this.w_Loop <= Alen(ARR_TSM, 1)
        * --- Controllo se sono state modificate delle super entit�.
        *     In questo caso devo rieseguire la CREALRTEMP in modo da aggiungere 
        *     i nuovi record modificati
        this.w_Modificati = this.w_Modificati + ARR_TSM(this.w_Loop, 3)
        AddMsgNL("", this , , , , ,, )
        AddMsgNL("Aggiornato mirror TSM archivio %1 di super entit� %2",this, Alltrim(ARR_TSM(this.w_Loop, 1)), Alltrim(ARR_TSM(this.w_Loop, 2)) , , , , )
        AddMsgNL("Record aggiornati nella super entit� %1 n.%2",this, Alltrim(ARR_TSM(this.w_Loop, 2)) , Alltrim(Str(ARR_TSM(this.w_Loop, 3))) , , , , )
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    * --- Elenco delle super entit� da pubblicare...
    * --- Select from gslr_bpu
    do vq_exec with 'gslr_bpu',this,'_Curs_gslr_bpu','',.f.,.t.
    if used('_Curs_gslr_bpu')
      select _Curs_gslr_bpu
      locate for 1=1
      do while not(eof())
      this.w_ARCHIVIO = Alltrim(_Curs_gslr_bpu.EN_TABLE)
      * --- Creo per la super entit� l'elenco delle chiavi da aggiornare rispetto
      *     all'ultima pubblicazione.
      AddMsgNL("Analisi differenze per super entit� %1",this, this.w_ARCHIVIO , , , ,, )
      this.w_RESULT = CREALRTEMP( this.w_ARCHIVIO , this.w_PADRE , this.oParentObject.w_VERBOSE )
      if Not Empty( this.w_RESULT )
        * --- Raise
        i_Error=this.w_RESULT
        return
      endif
      if Not Empty( this.w_RESULT )
        * --- Si � verificato un errore in fase di pubblicazione, esco
        AddMsgNL("Errore determinazione differenze archivio %1, errore %2",this, this.w_ARCHIVIO , this.w_RESULT , , ,, )
        * --- Raise
        i_Error="ERROR"
        return
      else
        * --- Ho individuato per la super Entit�, le differenze rispetto all'ultima esportazione
        *     Ora per ogni coppia (entit� che fa riferimento a questa super Entit�, sede), creo i DBF
        *     da pubblicare, li accodo all'array da pubblicare
        * --- Select from gslr_bpu_1
        do vq_exec with 'gslr_bpu_1',this,'_Curs_gslr_bpu_1','',.f.,.t.
        if used('_Curs_gslr_bpu_1')
          select _Curs_gslr_bpu_1
          locate for 1=1
          do while not(eof())
          * --- Nel temporaneo ho le modifiche della Super Entit�, ora passo alla routine
          *     di generazione dei DBF la coppia Sede ed Entit� affinch� filtri opportunamente
          *     i dati dal temporaneo e valorizzi in modo opportuno i filtri verticali
          AddMsgNL("", this , , , , ,, )
          AddMsgNL("Generazione DBF sede %1 entit� %2",this, Alltrim(_Curs_gslr_bpu_1.SECODICE) , alltrim(_Curs_gslr_bpu_1.ENCODICE) , , ,, )
          this.w_RESULT = ENTTODBF( Alltrim( _Curs_gslr_bpu_1.SECODICE) , Alltrim( _Curs_gslr_bpu_1.ENCODICE ) , @ARRCALC , This , this.oParentObject.w_VERBOSE ) 
          if Not Empty( this.w_RESULT )
            * --- Si � verificato un errore in fase di generazione DBF, esco
            AddMsgNL("Errore generazione DBF archivio %1, errore %2",this, this.w_ARCHIVIO , this.w_RESULT , , ,, )
            * --- Raise
            i_Error="ERROR"
            return
          endif
            select _Curs_gslr_bpu_1
            continue
          enddo
          use
        endif
      endif
      * --- Ho analizzato la Super Entit�, ora vado ad aggiornare la tabella di Mirror
      *     Costruisco l'array per avere il risultato dell'aggiornamento
      Dimension ARRMIRROR (4)
      AddMsgNL("", this , , , , ,, )
      this.w_RESULT = AGGMIRROR( this.w_ARCHIVIO , @ARRMIRROR, .f. ,this.w_PADRE , this.oParentObject.w_VERBOSE )
      if Not Empty( ARRMIRROR (4) )
        * --- Si � verificato un errore in fase di aggiornamento Mirror, esco
        AddMsgNL("Errore aggiornamento Mirror archivio %1, errore %2",this, this.w_ARCHIVIO , ARRMIRROR (4) , , ,, )
        * --- Raise
        i_Error="ERROR"
        return
      else
        AddMsgNL("", this , , , , ,, )
        AddMsgNL("Aggiornato Mirror archivio %1",this, this.w_ARCHIVIO , , , ,, )
        AddMsgNL("Record aggiornati %1",this, Alltrim(Str(ARRMIRROR (1))) , , , ,, )
        AddMsgNL("Record inseriti %1",this, Alltrim(Str(ARRMIRROR (2))) , , , ,, )
        AddMsgNL("Record eliminati %1",this, Alltrim(Str(ARRMIRROR (3))) , , , ,, )
      endif
      AddMsgNL("", this , , , , ,, )
      * --- Drop temporary table TEMP_PUBBL
      i_nIdx=cp_GetTableDefIdx('TEMP_PUBBL')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TEMP_PUBBL')
      endif
        select _Curs_gslr_bpu
        continue
      enddo
      use
    endif
    * --- Se tutto OK  invio alle sedi, 
    *     aggiorno con il temporaneo i dati del mirror,
    *     cancello le tabelline VFP temporanee ed esco
    * --- Ordino l'array per le sedi (terza colonna )
    =ASORT( ARRCALC ,3)
    * --- Scorro la prima colonna dell'array ed ad ogni cambio si sede invio il pacchetto..
    this.w_LOOP = 1
    this.w_IDXSEDE = 1
    this.w_SEDE = ARRCALC[ 1 , 3 ] 
    if this.oParentObject.w_ONLYMIRR<>"T"
      if Empty( this.w_SEDE )
        AddMsgNL("Nessuna sede ricevente definita", this , , , ,, ,)
        * --- Raise
        i_Error="ERROR"
        return
      endif
    endif
    if this.oParentObject.w_ONLYMIRR="N"
      * --- Nel caso in cui eseguo pubblicazione completa creo lo zip per sede e aggiorno il pacchetto
      *     con la funzione CREAPUBBL
      Dimension ARRSEDE[1,3]
      do while this.w_LOOP<= ALEN( ARRCALC , 1)
        * --- Preparo il cursore per la sede
         
 Dimension ARRSEDE[ this.w_IDXSEDE ,3] 
 ARRSEDE[ this.w_IDXSEDE , 1 ]= ARRCALC[ this.w_LOOP, 1 ] 
 ARRSEDE[ this.w_IDXSEDE , 2 ]= ARRCALC[ this.w_LOOP, 2 ] 
 ARRSEDE[ this.w_IDXSEDE , 3 ]= ARRCALC[ this.w_LOOP, 4 ]
        this.w_LOOP = this.w_LOOP + 1
        this.w_IDXSEDE = this.w_IDXSEDE + 1
        if this.w_LOOP>ALEN( ARRCALC , 1) Or (this.w_SEDE <> ARRCALC[this.w_LOOP , 3] )
          * --- La sede � cambiata invio il pacchetto...
          AddMsgNL("", this , , , , ,, )
          this.w_RESULT = CREAPUBBL (@ARRSEDE , this.w_SEDE, This, this.oParentObject.w_VERBOSE)
          if Not Empty( this.w_RESULT )
            * --- Si � verificato un errore in fase di generazione DBF, esco
            AddMsgNL("Errore pubblicazione sede %1 , errore %2", this , this.w_SEDE , this.w_RESULT , ,, ,)
            * --- Raise
            i_Error="ERROR"
            return
          else
            AddMsgNL("Terminata pubblicazione sede %1", this , this.w_SEDE , , ,, ,)
          endif
          if this.w_LOOP<=ALEN( ARRCALC , 1) 
            this.w_IDXSEDE = 1
            this.w_SEDE = ARRCALC[ this.w_LOOP , 3 ] 
          endif
        endif
      enddo
    else
      * --- Nel caso di solo aggiornamento mirror...
      AddMsgNL("Terminato aggiornamento mirror", this)
    endif
    if File( Tempadhoc()+"\"+"CheckTablePlan.Dbf" )
      * --- Se esiste ancora nella temp il file 
      Erase Tempadhoc()+"\"+"CheckTablePlan.Dbf"
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Uscita dall'elaborazione...
    * --- Rimuovo i temporanei nella TEMP
    this.w_RESULT = ERASETMP( @ARRCALC )
    if Not Empty( this.w_RESULT )
      AddMsgNL("Errore cancellazione temporanei di appoggio %1", this , this.w_RESULT , , ,, ,)
    else
      AddMsgNL("Cancellazione temporanei di appoggio", this , , , ,, ,)
    endif
    * --- Anche se non presente non dovrebbe andare in errore
    * --- Drop temporary table TEMP_PUBBL
    i_nIdx=cp_GetTableDefIdx('TEMP_PUBBL')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TEMP_PUBBL')
    endif
    AddMsgNL("Elaborazione terminata alle %1",this, Time() , , , ,, )
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisco log pubblicazione: solo memo senza dettaglio per sede
    this.w_LSSERIAL = Space(10)
    i_Conn=i_TableProp[this.LOG_SINC_IDX, 3]
    * --- Try
    local bErr_036A4CC8
    bErr_036A4CC8=bTrsErr
    this.Try_036A4CC8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore determinazione progressivo log elaborazione",this, )
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_036A4CC8
    * --- End
    * --- Data del giorno, non di sistema
    this.w_Data = Date()
    this.w_Time = Alltrim(Time())
    this.w_Ore = Val(Left(this.w_Time, 2))
    this.w_Min = Val(Substr(this.w_Time, At(":", this.w_Time)+1, 2))
    this.w_Sec = Val(Right(this.w_Time, 2))
    this.w_Note = SaveMemo( this.oParentObject.w_Msg )
    * --- Insert into LOG_SINC
    i_nConn=i_TableProp[this.LOG_SINC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_SINC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_SINC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSSERIAL"+",LS__DATA"+",LS___ORA"+",LS___MIN"+",LS___SEC"+",LS___LOG"+",LSPUBSIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOG_SINC','LSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Data),'LOG_SINC','LS__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Ore),'LOG_SINC','LS___ORA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Min),'LOG_SINC','LS___MIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Sec),'LOG_SINC','LS___SEC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'LOG_SINC','LS___LOG');
      +","+cp_NullLink(cp_ToStrODBC("P"),'LOG_SINC','LSPUBSIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LS__DATA',this.w_Data,'LS___ORA',this.w_Ore,'LS___MIN',this.w_Min,'LS___SEC',this.w_Sec,'LS___LOG',this.w_NOTE,'LSPUBSIN',"P")
      insert into (i_cTable) (LSSERIAL,LS__DATA,LS___ORA,LS___MIN,LS___SEC,LS___LOG,LSPUBSIN &i_ccchkf. );
         values (;
           this.w_LSSERIAL;
           ,this.w_Data;
           ,this.w_Ore;
           ,this.w_Min;
           ,this.w_Sec;
           ,this.w_NOTE;
           ,"P";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc
  proc Try_036A4CC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    cp_NextTableProg(this, i_Conn, "SELOG", "i_codazi,w_LSSERIAL")
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TAB_SECO'
    this.cWorkTables[2]='LOG_SINC'
    this.cWorkTables[3]='PARALORE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_gslr_qet')
      use in _Curs_gslr_qet
    endif
    if used('_Curs_TAB_SECO')
      use in _Curs_TAB_SECO
    endif
    if used('_Curs_gslr_bpu')
      use in _Curs_gslr_bpu
    endif
    if used('_Curs_gslr_bpu_1')
      use in _Curs_gslr_bpu_1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
