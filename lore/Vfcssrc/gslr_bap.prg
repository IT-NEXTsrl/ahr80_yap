* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bap                                                        *
*              Apre treeview apertura pacchetto                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-16                                                      *
* Last revis.: 2006-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ZipFile,w_SEPWDZIP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bap",oParentObject,m.w_ZipFile,m.w_SEPWDZIP)
return(i_retval)

define class tgslr_bap as StdBatch
  * --- Local variables
  w_ZipFile = space(254)
  w_SEPWDZIP = space(254)
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia maschera TreeView per apertura pacchetto
    * --- Lancio visualizzazione pacchetto
    this.w_OBJ = GSLR_KTD(this)
    this.w_OBJ.w_PATH = this.w_ZipFile
    this.w_OBJ.w_SEPWDZIP = this.w_SEPWDZIP
    this.w_OBJ.SetControlsValue()     
    this.w_OBJ.NotifyEvent("Calcola")     
  endproc


  proc Init(oParentObject,w_ZipFile,w_SEPWDZIP)
    this.w_ZipFile=w_ZipFile
    this.w_SEPWDZIP=w_SEPWDZIP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ZipFile,w_SEPWDZIP"
endproc
