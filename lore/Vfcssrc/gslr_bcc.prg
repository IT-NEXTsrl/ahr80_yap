* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bcc                                                        *
*              Controllu sugli alias di campo duplicati                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-13                                                      *
* Last revis.: 2008-07-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bcc",oParentObject,m.pPar1)
return(i_retval)

define class tgslr_bcc as StdBatch
  * --- Local variables
  pPar1 = space(1)
  w_PADRE = .NULL.
  w_FIGLIO = .NULL.
  w_LOOP = 0
  w_CAMPO = space(50)
  w_CONTA = 0
  w_OLDESTABALI = space(30)
  w_TROVATO = .f.
  w_ESTENSIONE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica che i campi estensioni delle tabelle non utilizzino degli alias duplicati 
    *     Controlla tutti gli alias dei campi definiti sulla tabella estesa e verifica che non  vi sia un duplicato.
    *     Per ogni alias controlla che non sia ugauale al nome di un campo della tabella estesa 
    this.w_TROVATO = .F.
    this.oParentObject.w_CONTROLLO = .F.
    this.w_PADRE = this.oparentobject
    this.w_FIGLIO = this.oparentobject.gslr_mce
    this.w_ESTENSIONE = ""
    do case
      case this.pPar1="C"
        this.w_PADRE.markpos()     
        this.w_PADRE.FirstRow()     
        * --- Il cursore aliascampi conterr� tutti gli alias dei campi estensione definiti sulla tabella e verr� caricato ciclando sui figli di riga delle tabelle estese
        Create cursor aliascampi (codice char(254)) 
 WRCURSOR ("aliascampi")
        Create cursor aliastabelle (codice char(254)) 
 WRCURSOR ("aliastabelle")
        do while NOT this.w_PADRE.EOF_TRS()
          this.w_PADRE.Setrow()     
          if this.w_PADRE.fullrow()
            * --- Cicla sul figlio
            Insert into aliastabelle values (this.w_PADRE.w_ESTABALI)
            this.w_PADRE.ChildrenChangeRow()     
            this.w_FIGLIO.markpos()     
            this.w_FIGLIO.FirstRow()     
            do while NOT this.w_FIGLIO.EOF_TRS()
              this.w_FIGLIO.Setrow()     
              if this.w_FIGLIO.fullrow()
                Insert into aliascampi values (this.w_FIGLIO.w_CM_ALIAS)
              endif
              this.w_FIGLIO.nextrow()     
            enddo
            this.w_FIGLIO.Repos()     
          endif
          this.w_PADRE.nextrow()     
        enddo
        this.w_PADRE.Repos()     
        * --- Aggiunge i campi della tabella estesa (i campi della tabella estesa sono degli alias impliciti)
        this.w_LOOP = 1
        do while this.w_LOOP<= I_DCX.GetFieldsCount( this.oParentObject.w_ESCODTAB )
          this.w_CAMPO = i_DCX.GetFieldName( this.oParentObject.w_ESCODTAB , this.w_LOOP )
          Insert into aliascampi values (this.w_CAMPO)
          this.w_LOOP = this.w_LOOP + 1
        enddo
        * --- Verifica se tra gli alias ci sono dei duplicati
        SELECT codice,COUNT(*) as numero FROM aliascampi GROUP BY codice INTO CURSOR CONTEGGIO HAVING numero>1 
 SELECT CONTEGGIO 
        if RECCOUNT()>0
          this.oParentObject.w_MESS = ah_MsgFormat("Ci sono degli alias di campo duplicati. Tutti gli alias definiti sui campi devono essere univoci e differenti dai nomi dei campi della tabella estesa.")
          this.oParentObject.w_CONTROLLO = .T.
        endif
        if USED ("CONTEGGIO")
          SELECT CONTEGGIO 
 USE
        endif
        * --- Controlla che non ci siano degli alias di tabelle duplicati
        SELECT codice,COUNT(*) as numero FROM aliastabelle GROUP BY codice INTO CURSOR CONTEGGIO HAVING numero>1 
 SELECT CONTEGGIO 
        if RECCOUNT()>0
          this.oParentObject.w_MESS = ah_MsgFormat("Ci sono degli alias di tabella duplicati. Tutti gli alias definiti sulle tabelle devono essere univoci e differenti dai nomi delle tabella di estensione.")
          this.oParentObject.w_CONTROLLO = .T.
        endif
      case this.pPar1="D"
        this.w_OLDESTABALI = this.oParentObject.w_ES_TABLE
        this.w_TROVATO = .F.
        do while NOT this.w_TROVATO
          this.w_PADRE.markpos()     
          this.w_PADRE.FirstRow()     
          this.w_TROVATO = .T.
          do while NOT this.w_PADRE.EOF_TRS() AND this.w_TROVATO
            this.w_PADRE.Setrow()     
            if this.w_PADRE.fullrow() AND this.w_PADRE.RowStatus()<>"D"
              this.w_PADRE.Setrow()     
              if this.w_CONTA>0
                this.w_ESTENSIONE = alltrim(str (this.w_CONTA))
              else
                this.w_ESTENSIONE = ""
              endif
              if UPPER (alltrim(this.oParentObject.w_ESTABALI))==alltrim(this.w_OLDESTABALI) + this.w_ESTENSIONE
                this.w_TROVATO = .F.
              endif
            endif
            this.w_PADRE.nextrow()     
          enddo
          this.w_PADRE.Repos()     
          this.w_CONTA = this.w_CONTA + 1
        enddo
        if NOT EMPTY (this.oParentObject.w_ES_TABLE)
          this.oParentObject.w_NRIGA = this.w_ESTENSIONE
          this.oParentObject.w_ESTABALI = alltrim(this.w_OLDESTABALI) + alltrim (this.w_ESTENSIONE)
          this.oParentObject.w_ESRELAZI = FINDLINK(this.oParentObject.w_ES_TABLE, this.oParentObject.w_ESCODTAB , 0, .T.)
          this.oParentObject.w_ESRELAZI = strtran (" " + alltrim(this.oParentObject.w_ESRELAZI) , " "+alltrim(this.oParentObject.w_ES_TABLE)+"." , " "+alltrim (this.oParentObject.w_ESTABALI)+".")
        else
          this.oParentObject.w_ESTABALI = ""
          this.oParentObject.w_ESRELAZI = ""
          this.oParentObject.w_NRIGA = ""
        endif
    endcase
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED ("aliastabelle")
      SELECT aliastabelle 
 USE
    endif
    if USED ("aliascampi")
      SELECT aliascampi 
 USE
    endif
    if USED ("CONTEGGIO")
      SELECT CONTEGGIO 
 USE
    endif
  endproc


  proc Init(oParentObject,pPar1)
    this.pPar1=pPar1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar1"
endproc
