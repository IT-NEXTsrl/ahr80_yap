* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_ksp                                                        *
*              Salva pacchetto modificato                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-17                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_ksp",oParentObject))

* --- Class definition
define class tgslr_ksp as StdForm
  Top    = 160
  Left   = 318

  * --- Standard Properties
  Width  = 570
  Height = 145
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=169251735
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  PUBLMAST_IDX = 0
  cPrg = "gslr_ksp"
  cComment = "Salva pacchetto modificato"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPSAVE = space(1)
  w_SEDE = space(2)
  w_TIPOSEDE = space(1)
  w_DESCRI = space(50)
  w_NEWFILE = space(254)
  w_SALVA = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_kspPag1","gslr_ksp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPSAVE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PUBLMAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPSAVE=space(1)
      .w_SEDE=space(2)
      .w_TIPOSEDE=space(1)
      .w_DESCRI=space(50)
      .w_NEWFILE=space(254)
      .w_SALVA=.f.
      .w_TIPSAVE=oParentObject.w_TIPSAVE
      .w_SEDE=oParentObject.w_SEDE
      .w_TIPOSEDE=oParentObject.w_TIPOSEDE
      .w_NEWFILE=oParentObject.w_NEWFILE
      .w_SALVA=oParentObject.w_SALVA
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_SEDE))
          .link_1_4('Full')
        endif
          .DoRTCalc(3,5,.f.)
        .w_SALVA = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPSAVE=.w_TIPSAVE
      .oParentObject.w_SEDE=.w_SEDE
      .oParentObject.w_TIPOSEDE=.w_TIPOSEDE
      .oParentObject.w_NEWFILE=.w_NEWFILE
      .oParentObject.w_SALVA=.w_SALVA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oSEDE_1_4.visible=!this.oPgFrm.Page1.oPag.oSEDE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTIPOSEDE_1_5.visible=!this.oPgFrm.Page1.oPag.oTIPOSEDE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_7.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oNEWFILE_1_8.visible=!this.oPgFrm.Page1.oPag.oNEWFILE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SEDE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PUBLMAST_IDX,3]
    i_lTable = "PUBLMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2], .t., this.PUBLMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PUBLMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODICE like "+cp_ToStrODBC(trim(this.w_SEDE)+"%");
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPOSEDE);

          i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SE__TIPO,SECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SE__TIPO',this.w_TIPOSEDE;
                     ,'SECODICE',trim(this.w_SEDE))
          select SE__TIPO,SECODICE,SEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SE__TIPO,SECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SEDE)==trim(_Link_.SECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SEDE) and !this.bDontReportError
            deferred_cp_zoom('PUBLMAST','*','SE__TIPO,SECODICE',cp_AbsName(oSource.parent,'oSEDE_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOSEDE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPOSEDE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',oSource.xKey(1);
                       ,'SECODICE',oSource.xKey(2))
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(this.w_SEDE);
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPOSEDE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',this.w_TIPOSEDE;
                       ,'SECODICE',this.w_SEDE)
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDE = NVL(_Link_.SECODICE,space(2))
      this.w_DESCRI = NVL(_Link_.SEDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SEDE = space(2)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])+'\'+cp_ToStr(_Link_.SE__TIPO,1)+'\'+cp_ToStr(_Link_.SECODICE,1)
      cp_ShowWarn(i_cKey,this.PUBLMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPSAVE_1_2.RadioValue()==this.w_TIPSAVE)
      this.oPgFrm.Page1.oPag.oTIPSAVE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDE_1_4.value==this.w_SEDE)
      this.oPgFrm.Page1.oPag.oSEDE_1_4.value=this.w_SEDE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSEDE_1_5.RadioValue()==this.w_TIPOSEDE)
      this.oPgFrm.Page1.oPag.oTIPOSEDE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_7.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_7.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWFILE_1_8.value==this.w_NEWFILE)
      this.oPgFrm.Page1.oPag.oNEWFILE_1_8.value=this.w_NEWFILE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_kspPag1 as StdContainer
  Width  = 566
  height = 145
  stdWidth  = 566
  stdheight = 145
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPSAVE_1_2 as StdCombo with uid="YDLKNDEXEJ",rtseq=1,rtrep=.f.,left=113,top=12,width=193,height=21;
    , ToolTipText = "Indicare che tipo di azione si vuole eseguire sul nuovo pacchetto";
    , HelpContextID = 75424310;
    , cFormVar="w_TIPSAVE",RowSource=""+"Reinvia il pacchetto alla sede,"+"Salva il file su disco", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSAVE_1_2.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPSAVE_1_2.GetRadio()
    this.Parent.oContained.w_TIPSAVE = this.RadioValue()
    return .t.
  endfunc

  func oTIPSAVE_1_2.SetRadio()
    this.Parent.oContained.w_TIPSAVE=trim(this.Parent.oContained.w_TIPSAVE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSAVE=='I',1,;
      iif(this.Parent.oContained.w_TIPSAVE=='S',2,;
      0))
  endfunc

  add object oSEDE_1_4 as StdField with uid="PCOBDQOGWE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SEDE", cQueryName = "SEDE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Indicare la sede a cui si vuole reinviare il pacchetto modificato",;
    HelpContextID = 174071334,;
   bGlobalFont=.t.,;
    Height=21, Width=33, Left=113, Top=70, InputMask=replicate('X',2), cLinkFile="PUBLMAST", oKey_1_1="SE__TIPO", oKey_1_2="this.w_TIPOSEDE", oKey_2_1="SECODICE", oKey_2_2="this.w_SEDE"

  func oSEDE_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE<>'I')
    endwith
  endfunc

  func oSEDE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSEDE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oTIPOSEDE_1_5 as StdCombo with uid="YATBPFQSXH",rtseq=3,rtrep=.f.,left=113,top=42,width=159,height=21;
    , ToolTipText = "Indicare il tipo di sede alla quale si vuole reinviare il pacchetto modificato";
    , HelpContextID = 77259387;
    , cFormVar="w_TIPOSEDE",RowSource=""+"Sede da cui ricevo,"+"Sede verso cui pubblico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSEDE_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPOSEDE_1_5.GetRadio()
    this.Parent.oContained.w_TIPOSEDE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSEDE_1_5.SetRadio()
    this.Parent.oContained.w_TIPOSEDE=trim(this.Parent.oContained.w_TIPOSEDE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSEDE=='P',1,;
      iif(this.Parent.oContained.w_TIPOSEDE=='R',2,;
      0))
  endfunc

  func oTIPOSEDE_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE<>'I')
    endwith
  endfunc

  func oTIPOSEDE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SEDE)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_7 as StdField with uid="SRRUXCLKHI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione sede",;
    HelpContextID = 125891274,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=148, Top=70, InputMask=replicate('X',50)

  func oDESCRI_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE<>'I')
    endwith
  endfunc

  add object oNEWFILE_1_8 as StdField with uid="APLVUXDHME",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NEWFILE", cQueryName = "NEWFILE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path del nuovo file zip contenente il pacchetto modificato",;
    HelpContextID = 183651798,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=112, Top=52, InputMask=replicate('X',254)

  func oNEWFILE_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE='I')
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="WZGRGMINYR",left=540, top=53, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare lo zip del pacchetto dati";
    , HelpContextID = 169452758;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_NEWFILE=left(GETFILE('Ahz', 'AdHoc zip', 'Save as', 0, g_APPLICATION ),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPSAVE='I')
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="AYBNNJWSNT",left=510, top=96, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare il salvataggio";
    , HelpContextID = 176569158;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="MRZTKNJYPV",left=459, top=97, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il salvataggio del pacchetto modificato";
    , HelpContextID = 169280486;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="HQPDKEEHFB",Visible=.t., Left=16, Top=11,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipo salvataggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="TBETQYKTYX",Visible=.t., Left=16, Top=41,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipo sede:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE<>'I')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="VSPSJPPDXJ",Visible=.t., Left=16, Top=71,;
    Alignment=1, Width=93, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE<>'I')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="KSDUJTFZWN",Visible=.t., Left=15, Top=53,;
    Alignment=1, Width=94, Height=18,;
    Caption="Nuovo pacchetto:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPSAVE='I')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_ksp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
