* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_btw                                                        *
*              Treeview struttura entit�                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2013-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_btw",oParentObject,m.pTipo)
return(i_retval)

define class tgslr_btw as StdBatch
  * --- Local variables
  pTipo = space(5)
  w_ENCODICE = space(30)
  w_CONTA2 = 0
  w_CONTA3 = 0
  w_LCODICE = space(30)
  w_TIPO = space(1)
  w_PADRE = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea struttura treeview per ogni Super Entit�
    do case
      case this.pTipo = "Open"
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        L_NomeCursore = this.oParentObject.w_CURSORNA
        VQ_EXEC("..\LORE\EXE\QUERY\GSLR_QTW.VQR",this,L_NomeCursore)
        if Used(L_NomeCursore) And Reccount(L_NomeCursore)>0
           
 Select(L_NomeCursore) 
 Go Top
          this.w_CONTA2 = 0
          this.w_CONTA3 = 0
          do while Not Eof()
            if Empty(Nvl(ENCODICE,"")) And Empty(Nvl(SECODICE,""))
              Replace LVLKEY With "001"
            else
              if Empty(Nvl(SECODICE,""))
                this.w_CONTA2 = this.w_CONTA2 + 1
                this.w_CONTA3 = 0
                 
 Replace LVLKEY With "001." + Right("000"+Alltrim(Str(this.w_CONTA2,3,0)),3) 
 Replace CODICE With ENCODICE 
 Replace Descri with ENDESCRI
              else
                this.w_CONTA3 = this.w_CONTA3 + 1
                 
 Replace LVLKEY With "001." + Right("000"+Alltrim(Str(this.w_CONTA2,3,0)),3) +"."+Right("000"+Alltrim(Str(this.w_CONTA3,3,0)),3) 
 Replace CODICE With Left(SECODICE,2) 
 Replace Descri with Alltrim(SEDESCRI) + " (" +IIF(Right(SECODICE,1)="P",Ah_MsgFormat("Sede da cui ricevo"),Ah_MsgFormat("Sede verso cui pubblico"))+")"
              endif
            endif
            Skip
          enddo
          Select(L_NomeCursore) 
 index on lvlkey tag lvlkey collate "MACHINE"
          * --- Imposto i Bitmap
          this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
          This.oParentObject.NotifyEvent("Esegui")
        endif
      case this.pTipo = "Close"
        if used( ( this.oParentObject.w_CURSORNA ) )
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
      case this.pTipo = "Dett"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo = "DxBtn"
         
 Local nCmd 
 nCmd= 0 
 DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT 
 DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Dettagli") 
 ON SELECTION BAR 1 OF popCmd nCmd = 1 
 
 ACTIVATE POPUP popCmd 
 DEACTIVATE POPUP popCmd 
 RELEASE POPUPS popCmd
        if nCmd <>0
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre gestione
    this.w_LCODICE = Alltrim(Nvl( this.oParentObject.w_TREEVIEW.GetVAr("CODICE")," "))
    this.w_TIPO = Alltrim(Nvl( this.oParentObject.w_TREEVIEW.GetVAr("SE__TIPO")," "))
    this.w_PADRE = Alltrim(Nvl( this.oParentObject.w_TREEVIEW.GetVAr("PADRE")," "))
    do case
      case this.w_PADRE="S"
        this.w_PROG = GSLR_MSU()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_SUCODICE = this.w_LCODICE
        this.w_PROG.ecpSave()     
      case this.w_PADRE = "E"
        this.w_PROG = GSAR_MEN()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_ENCODICE = this.w_LCODICE
        this.w_PROG.ecpSave()     
      case this.w_PADRE = "D"
        this.w_PROG = GSLR_MSE()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_SECODICE = this.w_LCODICE
        this.w_PROG.w_SE__TIPO = this.w_TIPO
        this.w_PROG.ecpSave()     
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
