* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_mfv                                                        *
*              Filtri verticali                                                *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_10]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslr_mfv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslr_mfv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslr_mfv")
  return

* --- Class definition
define class tgslr_mfv as StdPCForm
  Width  = 837
  Height = 302
  Top    = 10
  Left   = 10
  cComment = "Filtri verticali"
  cPrg = "gslr_mfv"
  HelpContextID=46754921
  add object cnt as tcgslr_mfv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslr_mfv as PCContext
  w_PV__SEDE = space(2)
  w_PVCODENT = space(15)
  w_PV__TIPO = space(1)
  w_PVCODARC = space(30)
  w_PVFIELDS = space(50)
  w_PVOVERW = space(1)
  w_TIPO = space(1)
  w_CODARC = space(30)
  w_PVSTADIN = space(1)
  w_FUNZIONE = space(30)
  w_PV_VALUE = space(10)
  w_VALORE = space(10)
  proc Save(i_oFrom)
    this.w_PV__SEDE = i_oFrom.w_PV__SEDE
    this.w_PVCODENT = i_oFrom.w_PVCODENT
    this.w_PV__TIPO = i_oFrom.w_PV__TIPO
    this.w_PVCODARC = i_oFrom.w_PVCODARC
    this.w_PVFIELDS = i_oFrom.w_PVFIELDS
    this.w_PVOVERW = i_oFrom.w_PVOVERW
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_CODARC = i_oFrom.w_CODARC
    this.w_PVSTADIN = i_oFrom.w_PVSTADIN
    this.w_FUNZIONE = i_oFrom.w_FUNZIONE
    this.w_PV_VALUE = i_oFrom.w_PV_VALUE
    this.w_VALORE = i_oFrom.w_VALORE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PV__SEDE = this.w_PV__SEDE
    i_oTo.w_PVCODENT = this.w_PVCODENT
    i_oTo.w_PV__TIPO = this.w_PV__TIPO
    i_oTo.w_PVCODARC = this.w_PVCODARC
    i_oTo.w_PVFIELDS = this.w_PVFIELDS
    i_oTo.w_PVOVERW = this.w_PVOVERW
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_CODARC = this.w_CODARC
    i_oTo.w_PVSTADIN = this.w_PVSTADIN
    i_oTo.w_FUNZIONE = this.w_FUNZIONE
    i_oTo.w_PV_VALUE = this.w_PV_VALUE
    i_oTo.w_VALORE = this.w_VALORE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgslr_mfv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 837
  Height = 302
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=46754921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PUBLVERT_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "PUBLVERT"
  cKeySelect = "PV__SEDE,PVCODENT,PV__TIPO,PVCODARC"
  cKeyWhere  = "PV__SEDE=this.w_PV__SEDE and PVCODENT=this.w_PVCODENT and PV__TIPO=this.w_PV__TIPO and PVCODARC=this.w_PVCODARC"
  cKeyDetail  = "PV__SEDE=this.w_PV__SEDE and PVCODENT=this.w_PVCODENT and PV__TIPO=this.w_PV__TIPO and PVCODARC=this.w_PVCODARC and PVFIELDS=this.w_PVFIELDS"
  cKeyWhereODBC = '"PV__SEDE="+cp_ToStrODBC(this.w_PV__SEDE)';
      +'+" and PVCODENT="+cp_ToStrODBC(this.w_PVCODENT)';
      +'+" and PV__TIPO="+cp_ToStrODBC(this.w_PV__TIPO)';
      +'+" and PVCODARC="+cp_ToStrODBC(this.w_PVCODARC)';

  cKeyDetailWhereODBC = '"PV__SEDE="+cp_ToStrODBC(this.w_PV__SEDE)';
      +'+" and PVCODENT="+cp_ToStrODBC(this.w_PVCODENT)';
      +'+" and PV__TIPO="+cp_ToStrODBC(this.w_PV__TIPO)';
      +'+" and PVCODARC="+cp_ToStrODBC(this.w_PVCODARC)';
      +'+" and PVFIELDS="+cp_ToStrODBC(this.w_PVFIELDS)';

  cKeyWhereODBCqualified = '"PUBLVERT.PV__SEDE="+cp_ToStrODBC(this.w_PV__SEDE)';
      +'+" and PUBLVERT.PVCODENT="+cp_ToStrODBC(this.w_PVCODENT)';
      +'+" and PUBLVERT.PV__TIPO="+cp_ToStrODBC(this.w_PV__TIPO)';
      +'+" and PUBLVERT.PVCODARC="+cp_ToStrODBC(this.w_PVCODARC)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gslr_mfv"
  cComment = "Filtri verticali"
  i_nRowNum = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PV__SEDE = space(2)
  w_PVCODENT = space(15)
  w_PV__TIPO = space(1)
  w_PVCODARC = space(30)
  o_PVCODARC = space(30)
  w_PVFIELDS = space(50)
  w_PVOVERW = space(1)
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_CODARC = space(30)
  w_PVSTADIN = space(1)
  o_PVSTADIN = space(1)
  w_FUNZIONE = space(30)
  o_FUNZIONE = space(30)
  w_PV_VALUE = space(0)
  w_VALORE = space(0)
  o_VALORE = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_mfvPag1","gslr_mfv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_FIELDS'
    this.cWorkTables[2]='PUBLVERT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PUBLVERT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PUBLVERT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgslr_mfv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PUBLVERT where PV__SEDE=KeySet.PV__SEDE
    *                            and PVCODENT=KeySet.PVCODENT
    *                            and PV__TIPO=KeySet.PV__TIPO
    *                            and PVCODARC=KeySet.PVCODARC
    *                            and PVFIELDS=KeySet.PVFIELDS
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLVERT_IDX,2],this.bLoadRecFilter,this.PUBLVERT_IDX,"gslr_mfv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PUBLVERT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PUBLVERT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PUBLVERT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PV__SEDE',this.w_PV__SEDE  ,'PVCODENT',this.w_PVCODENT  ,'PV__TIPO',this.w_PV__TIPO  ,'PVCODARC',this.w_PVCODARC  )
      select * from (i_cTable) PUBLVERT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPO = space(1)
        .w_PV__SEDE = NVL(PV__SEDE,space(2))
        .w_PVCODENT = NVL(PVCODENT,space(15))
        .w_PV__TIPO = NVL(PV__TIPO,space(1))
        .w_PVCODARC = NVL(PVCODARC,space(30))
        .w_CODARC = .w_PVCODARC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PUBLVERT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
        .w_FUNZIONE = 'Altro'
          .w_VALORE = space(0)
          .w_PVFIELDS = NVL(PVFIELDS,space(50))
          * evitabile
          *.link_2_1('Load')
          .w_PVOVERW = NVL(PVOVERW,space(1))
          .w_PVSTADIN = NVL(PVSTADIN,space(1))
          .w_PV_VALUE = NVL(PV_VALUE,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PVFIELDS with .w_PVFIELDS
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CODARC = .w_PVCODARC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PV__SEDE=space(2)
      .w_PVCODENT=space(15)
      .w_PV__TIPO=space(1)
      .w_PVCODARC=space(30)
      .w_PVFIELDS=space(50)
      .w_PVOVERW=space(1)
      .w_TIPO=space(1)
      .w_CODARC=space(30)
      .w_PVSTADIN=space(1)
      .w_FUNZIONE=space(30)
      .w_PV_VALUE=space(0)
      .w_VALORE=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_PVCODARC = this.oparentobject.W_DPCODARC
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PVFIELDS))
         .link_2_1('Full')
        endif
        .w_PVOVERW = 'S'
        .DoRTCalc(7,7,.f.)
        .w_CODARC = .w_PVCODARC
        .w_PVSTADIN = 'S'
        .w_FUNZIONE = 'Altro'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PUBLVERT')
    this.DoRTCalc(11,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PUBLVERT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PV__SEDE,"PV__SEDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVCODENT,"PVCODENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PV__TIPO,"PV__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVCODARC,"PVCODARC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PVFIELDS C(50);
      ,t_PVOVERW N(3);
      ,t_PVSTADIN N(3);
      ,t_FUNZIONE N(3);
      ,t_PV_VALUE M(10);
      ,t_VALORE M(10);
      ,PVFIELDS C(50);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslr_mfvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVFIELDS_2_1.controlsource=this.cTrsName+'.t_PVFIELDS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.controlsource=this.cTrsName+'.t_PVOVERW'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPVSTADIN_2_3.controlsource=this.cTrsName+'.t_PVSTADIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFUNZIONE_2_4.controlsource=this.cTrsName+'.t_FUNZIONE'
    this.oPgFRm.Page1.oPag.oPV_VALUE_2_5.controlsource=this.cTrsName+'.t_PV_VALUE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVALORE_2_6.controlsource=this.cTrsName+'.t_VALORE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(97)
    this.AddVLine(168)
    this.AddVLine(303)
    this.AddVLine(459)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVFIELDS_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLVERT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLVERT_IDX,2])
      *
      * insert into PUBLVERT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PUBLVERT')
        i_extval=cp_InsertValODBCExtFlds(this,'PUBLVERT')
        i_cFldBody=" "+;
                  "(PV__SEDE,PVCODENT,PV__TIPO,PVCODARC,PVFIELDS"+;
                  ",PVOVERW,PVSTADIN,PV_VALUE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PV__SEDE)+","+cp_ToStrODBC(this.w_PVCODENT)+","+cp_ToStrODBC(this.w_PV__TIPO)+","+cp_ToStrODBC(this.w_PVCODARC)+","+cp_ToStrODBCNull(this.w_PVFIELDS)+;
             ","+cp_ToStrODBC(this.w_PVOVERW)+","+cp_ToStrODBC(this.w_PVSTADIN)+","+cp_ToStrODBC(this.w_PV_VALUE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PUBLVERT')
        i_extval=cp_InsertValVFPExtFlds(this,'PUBLVERT')
        cp_CheckDeletedKey(i_cTable,0,'PV__SEDE',this.w_PV__SEDE,'PVCODENT',this.w_PVCODENT,'PV__TIPO',this.w_PV__TIPO,'PVCODARC',this.w_PVCODARC,'PVFIELDS',this.w_PVFIELDS)
        INSERT INTO (i_cTable) (;
                   PV__SEDE;
                  ,PVCODENT;
                  ,PV__TIPO;
                  ,PVCODARC;
                  ,PVFIELDS;
                  ,PVOVERW;
                  ,PVSTADIN;
                  ,PV_VALUE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PV__SEDE;
                  ,this.w_PVCODENT;
                  ,this.w_PV__TIPO;
                  ,this.w_PVCODARC;
                  ,this.w_PVFIELDS;
                  ,this.w_PVOVERW;
                  ,this.w_PVSTADIN;
                  ,this.w_PV_VALUE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLVERT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PVFIELDS))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PUBLVERT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PVFIELDS="+cp_ToStrODBC(&i_TN.->PVFIELDS)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PUBLVERT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PVFIELDS=&i_TN.->PVFIELDS;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PVFIELDS))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PVFIELDS="+cp_ToStrODBC(&i_TN.->PVFIELDS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PVFIELDS=&i_TN.->PVFIELDS;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PVFIELDS with this.w_PVFIELDS
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PUBLVERT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PUBLVERT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PVOVERW="+cp_ToStrODBC(this.w_PVOVERW)+;
                     ",PVSTADIN="+cp_ToStrODBC(this.w_PVSTADIN)+;
                     ",PV_VALUE="+cp_ToStrODBC(this.w_PV_VALUE)+;
                     ",PVFIELDS="+cp_ToStrODBC(this.w_PVFIELDS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PVFIELDS="+cp_ToStrODBC(PVFIELDS)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PUBLVERT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PVOVERW=this.w_PVOVERW;
                     ,PVSTADIN=this.w_PVSTADIN;
                     ,PV_VALUE=this.w_PV_VALUE;
                     ,PVFIELDS=this.w_PVFIELDS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PVFIELDS=&i_TN.->PVFIELDS;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLVERT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PVFIELDS))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PUBLVERT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PVFIELDS="+cp_ToStrODBC(&i_TN.->PVFIELDS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PVFIELDS=&i_TN.->PVFIELDS;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PVFIELDS))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PUBLVERT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLVERT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_PVCODARC<>.w_PVCODARC
          .w_CODARC = .w_PVCODARC
        endif
        if .o_TIPO<>.w_TIPO
          .Calculate_HXJUCATPXZ()
        endif
        if .o_PVSTADIN<>.w_PVSTADIN
          .Calculate_RCAWYTKHGK()
        endif
        if .o_FUNZIONE<>.w_FUNZIONE
          .Calculate_DIIRARUETA()
        endif
        if .o_VALORE<>.w_VALORE
          .Calculate_RNNQLXIFDH()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_AUYRYRISHJ()
    with this
          * --- Aggiorna funzione al acaricamento
          gslr_bvf(this;
             )
    endwith
  endproc
  proc Calculate_HXJUCATPXZ()
    with this
          * --- Aggiorna il campo PVSTADIN al variare del tipo di sede
          .w_PVSTADIN = IIF (.w_TIPO='R' , 'S',.w_PVSTADIN)
    endwith
  endproc
  proc Calculate_RCAWYTKHGK()
    with this
          * --- Aggiorna il campo w_PV_VALUE delle funzioni dinamiche 
          .w_FUNZIONE = IIF (.w_PVSTADIN='D' , .w_FUNZIONE  , 'Altro')
          .w_PV_VALUE = .w_VALORE
    endwith
  endproc
  proc Calculate_DIIRARUETA()
    with this
          * --- Aggiorna il campo w_PV_VALUE delle funzioni dinamiche 
          .w_PV_VALUE = IIF(.w_FUNZIONE='Altro','',.w_FUNZIONE)
          .w_VALORE = ''
    endwith
  endproc
  proc Calculate_UIACRHTCKZ()
    with this
          * --- Controllo filtro verticale
          GSLR_BCR(this;
             )
    endwith
  endproc
  proc Calculate_RNNQLXIFDH()
    with this
          * --- Aggiorna il campo VALOREdelle funzioni dinamiche 
          .w_PV_VALUE = IIF(.w_FUNZIONE='Altro',.w_VALORE,.w_PV_VALUE)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVOVERW_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVOVERW_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVSTADIN_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPVSTADIN_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFUNZIONE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFUNZIONE_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVALORE_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVALORE_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_AUYRYRISHJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert row start") or lower(cEvent)==lower("Update row start")
          .Calculate_UIACRHTCKZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PVFIELDS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVFIELDS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_PVFIELDS)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_CODARC);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_CODARC;
                     ,'FLNAME',trim(this.w_PVFIELDS))
          select TBNAME,FLNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PVFIELDS)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PVFIELDS) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oPVFIELDS_2_1'),i_cWhere,'',"Campi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODARC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_CODARC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVFIELDS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PVFIELDS);
                   +" and TBNAME="+cp_ToStrODBC(this.w_CODARC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_CODARC;
                       ,'FLNAME',this.w_PVFIELDS)
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PVFIELDS = NVL(_Link_.FLNAME,space(50))
    else
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVFIELDS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPV_VALUE_2_5.value==this.w_PV_VALUE)
      this.oPgFrm.Page1.oPag.oPV_VALUE_2_5.value=this.w_PV_VALUE
      replace t_PV_VALUE with this.oPgFrm.Page1.oPag.oPV_VALUE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVFIELDS_2_1.value==this.w_PVFIELDS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVFIELDS_2_1.value=this.w_PVFIELDS
      replace t_PVFIELDS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVFIELDS_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.RadioValue()==this.w_PVOVERW)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.SetRadio()
      replace t_PVOVERW with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSTADIN_2_3.RadioValue()==this.w_PVSTADIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSTADIN_2_3.SetRadio()
      replace t_PVSTADIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSTADIN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFUNZIONE_2_4.RadioValue()==this.w_FUNZIONE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFUNZIONE_2_4.SetRadio()
      replace t_FUNZIONE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFUNZIONE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVALORE_2_6.value==this.w_VALORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVALORE_2_6.value=this.w_VALORE
      replace t_VALORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVALORE_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'PUBLVERT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_PVFIELDS))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PVCODARC = this.w_PVCODARC
    this.o_TIPO = this.w_TIPO
    this.o_PVSTADIN = this.w_PVSTADIN
    this.o_FUNZIONE = this.w_FUNZIONE
    this.o_VALORE = this.w_VALORE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PVFIELDS)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PVFIELDS=space(50)
      .w_PVOVERW=space(1)
      .w_PVSTADIN=space(1)
      .w_FUNZIONE=space(30)
      .w_PV_VALUE=space(0)
      .w_VALORE=space(0)
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_PVFIELDS))
        .link_2_1('Full')
      endif
        .w_PVOVERW = 'S'
      .DoRTCalc(7,8,.f.)
        .w_PVSTADIN = 'S'
        .w_FUNZIONE = 'Altro'
    endwith
    this.DoRTCalc(11,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PVFIELDS = t_PVFIELDS
    this.w_PVOVERW = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.RadioValue(.t.)
    this.w_PVSTADIN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSTADIN_2_3.RadioValue(.t.)
    this.w_FUNZIONE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFUNZIONE_2_4.RadioValue(.t.)
    this.w_PV_VALUE = t_PV_VALUE
    this.w_VALORE = t_VALORE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PVFIELDS with this.w_PVFIELDS
    replace t_PVOVERW with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVOVERW_2_2.ToRadio()
    replace t_PVSTADIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPVSTADIN_2_3.ToRadio()
    replace t_FUNZIONE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFUNZIONE_2_4.ToRadio()
    replace t_PV_VALUE with this.w_PV_VALUE
    replace t_VALORE with this.w_VALORE
    if i_srv='A'
      replace PVFIELDS with this.w_PVFIELDS
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslr_mfvPag1 as StdContainer
  Width  = 833
  height = 302
  stdWidth  = 833
  stdheight = 302
  resizeXpos=425
  resizeYpos=258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=9, width=817,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="PVFIELDS",Label1="Campo",Field2="PVOVERW",Label2="Sovrascrive",Field3="PVSTADIN",Label3="Tipo",Field4="FUNZIONE",Label4="Funzioni standard",Field5="VALORE",Label5="Valore",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 900474

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=28,;
    width=813+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=29,width=812+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_FIELDS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPV_VALUE_2_5.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oPVFIELDS_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oPV_VALUE_2_5 as StdTrsMemo with uid="URLAHVYLAB",rtseq=11,rtrep=.t.,;
    cFormVar="w_PV_VALUE",value=space(0),enabled=.f.,;
    HelpContextID = 228777787,;
    cTotal="", bFixedPos=.t., cQueryName = "PV_VALUE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=85, Left=873, Top=126

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslr_mfvBodyRow as CPBodyRowCnt
  Width=803
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPVFIELDS_2_1 as StdTrsField with uid="SLLUBUYUCI",rtseq=5,rtrep=.t.,;
    cFormVar="w_PVFIELDS",value=space(50),isprimarykey=.t.,;
    HelpContextID = 232017737,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=86, Left=-2, Top=0, InputMask=replicate('X',50), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_CODARC", oKey_2_1="FLNAME", oKey_2_2="this.w_PVFIELDS"

  func oPVFIELDS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVFIELDS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPVFIELDS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPVFIELDS_2_1.readonly and this.parent.oPVFIELDS_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_CODARC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_CODARC)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oPVFIELDS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campi",'',this.parent.oContained
   endif
  endproc

  add object oPVOVERW_2_2 as StdTrsCombo with uid="RITMWVDGMU",rtrep=.t.,;
    cFormVar="w_PVOVERW", RowSource=""+"No,"+"Si" , ;
    ToolTipText = "In fase di sincronizzazione mantiene il valore attuale sul database o meno. Se dato inserito il dato � valorizzato con valore",;
    HelpContextID = 203301130,;
    Height=22, Width=69, Left=86, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPVOVERW_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PVOVERW,&i_cF..t_PVOVERW),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oPVOVERW_2_2.GetRadio()
    this.Parent.oContained.w_PVOVERW = this.RadioValue()
    return .t.
  endfunc

  func oPVOVERW_2_2.ToRadio()
    this.Parent.oContained.w_PVOVERW=trim(this.Parent.oContained.w_PVOVERW)
    return(;
      iif(this.Parent.oContained.w_PVOVERW=='N',1,;
      iif(this.Parent.oContained.w_PVOVERW=='S',2,;
      0)))
  endfunc

  func oPVOVERW_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPVOVERW_2_2.mCond()
    with this.Parent.oContained
      return (.w_TIPO='P')
    endwith
  endfunc

  func oPVOVERW_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO='R')
    endwith
    endif
  endfunc

  add object oPVSTADIN_2_3 as StdTrsCombo with uid="ARJPQEZQVE",rtrep=.t.,;
    cFormVar="w_PVSTADIN", RowSource=""+"Filtro statico,"+"Filtro dinamico" , ;
    HelpContextID = 174055612,;
    Height=22, Width=133, Left=157, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPVSTADIN_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PVSTADIN,&i_cF..t_PVSTADIN),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oPVSTADIN_2_3.GetRadio()
    this.Parent.oContained.w_PVSTADIN = this.RadioValue()
    return .t.
  endfunc

  func oPVSTADIN_2_3.ToRadio()
    this.Parent.oContained.w_PVSTADIN=trim(this.Parent.oContained.w_PVSTADIN)
    return(;
      iif(this.Parent.oContained.w_PVSTADIN=='S',1,;
      iif(this.Parent.oContained.w_PVSTADIN=='D',2,;
      0)))
  endfunc

  func oPVSTADIN_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPVSTADIN_2_3.mCond()
    with this.Parent.oContained
      return (.w_TIPO<>'R')
    endwith
  endfunc

  add object oFUNZIONE_2_4 as StdTrsCombo with uid="LVBLUZYHGP",rtrep=.t.,;
    cFormVar="w_FUNZIONE", RowSource=""+"Trova cli./for. da P. iva,"+"Trova cli./for. da C. fiscale,"+"Trova clienti POS da P. iva,"+"Trova clienti POS da C. fiscale,"+"Altro" , ;
    ToolTipText = "Elenco di funzioni standard disponibili (per specificare una funzione che non � in elenco selezionate la voce 'Altro')",;
    HelpContextID = 19254683,;
    Height=22, Width=154, Left=292, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oFUNZIONE_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FUNZIONE,&i_cF..t_FUNZIONE),this.value)
    return(iif(xVal =1,"TraslacliDaIVA ( 'ANPARIVA' , 'ANTIPCON' , 'ANCODICE')",;
    iif(xVal =2,"TraslacliDaCF (  'ANCODFIS' , 'ANTIPCON' , 'ANCODICE' )",;
    iif(xVal =3,"LoreLookTab ('CLI_VEND','CLCODCLI','CLPARIVA','CLPARIVA')",;
    iif(xVal =4,"LoreLookTab ('CLI_VEND','CLCODCLI','CLCODFIS','CLCODFIS')",;
    iif(xVal =5,'Altro',;
    space(30)))))))
  endfunc
  func oFUNZIONE_2_4.GetRadio()
    this.Parent.oContained.w_FUNZIONE = this.RadioValue()
    return .t.
  endfunc

  func oFUNZIONE_2_4.ToRadio()
    this.Parent.oContained.w_FUNZIONE=trim(this.Parent.oContained.w_FUNZIONE)
    return(;
      iif(this.Parent.oContained.w_FUNZIONE=="TraslacliDaIVA ( 'ANPARIVA' , 'ANTIPCON' , 'ANCODICE')",1,;
      iif(this.Parent.oContained.w_FUNZIONE=="TraslacliDaCF (  'ANCODFIS' , 'ANTIPCON' , 'ANCODICE' )",2,;
      iif(this.Parent.oContained.w_FUNZIONE=="LoreLookTab ('CLI_VEND','CLCODCLI','CLPARIVA','CLPARIVA')",3,;
      iif(this.Parent.oContained.w_FUNZIONE=="LoreLookTab ('CLI_VEND','CLCODCLI','CLCODFIS','CLCODFIS')",4,;
      iif(this.Parent.oContained.w_FUNZIONE=='Altro',5,;
      0))))))
  endfunc

  func oFUNZIONE_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFUNZIONE_2_4.mCond()
    with this.Parent.oContained
      return (.w_PVSTADIN<>'S')
    endwith
  endfunc

  add object oVALORE_2_6 as StdTrsMemo with uid="EJIACBMMRA",rtseq=12,rtrep=.t.,;
    cFormVar="w_VALORE",value=space(0),;
    HelpContextID = 128621142,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=349, Left=449, Top=0

  func oVALORE_2_6.mCond()
    with this.Parent.oContained
      return (.w_PVSTADIN<>'D' OR .w_FUNZIONE='Altro')
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oPVFIELDS_2_1.When()
    return(.t.)
  proc oPVFIELDS_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPVFIELDS_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_mfv','PUBLVERT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PV__SEDE=PUBLVERT.PV__SEDE";
  +" and "+i_cAliasName2+".PVCODENT=PUBLVERT.PVCODENT";
  +" and "+i_cAliasName2+".PV__TIPO=PUBLVERT.PV__TIPO";
  +" and "+i_cAliasName2+".PVCODARC=PUBLVERT.PVCODARC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
