* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_alo                                                        *
*              Log pubblicazione/sincronizzazione                              *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_17]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-13                                                      *
* Last revis.: 2009-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_alo"))

* --- Class definition
define class tgslr_alo as StdForm
  Top    = 24
  Left   = 10

  * --- Standard Properties
  Width  = 595
  Height = 548+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-29"
  HelpContextID=41325463
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  LOG_SINC_IDX = 0
  cFile = "LOG_SINC"
  cKeySelect = "LSSERIAL"
  cKeyWhere  = "LSSERIAL=this.w_LSSERIAL"
  cKeyWhereODBC = '"LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';

  cKeyWhereODBCqualified = '"LOG_SINC.LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';

  cPrg = "gslr_alo"
  cComment = "Log pubblicazione/sincronizzazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LSSERIAL = space(10)
  w_LS__DATA = ctod('  /  /  ')
  w_LS___ORA = 0
  w_LS___MIN = 0
  w_LS___SEC = 0
  w_LS___LOG = space(0)
  w_LSPUBSIN = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_LSSERIAL = this.W_LSSERIAL

  * --- Children pointers
  GSLR_MDS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gslr_alo
  * --- Non permetto il caricamento poich� deve essere caricato in automatico
  Proc ecpLoad()
     Ah_ErrorMsg("Impossibile eseguire caricamento manuale",'i',g_APPLICATION)
     Return
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'LOG_SINC','gslr_alo')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_aloPag1","gslr_alo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log")
      .Pages(1).HelpContextID = 41777078
      .Pages(2).addobject("oPag","tgslr_aloPag2","gslr_alo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).HelpContextID = 268288609
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLSSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='LOG_SINC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LOG_SINC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LOG_SINC_IDX,3]
  return

  function CreateChildren()
    this.GSLR_MDS = CREATEOBJECT('stdDynamicChild',this,'GSLR_MDS',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSLR_MDS)
      this.GSLR_MDS.DestroyChildrenChain()
      this.GSLR_MDS=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSLR_MDS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSLR_MDS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSLR_MDS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSLR_MDS.SetKey(;
            .w_LSSERIAL,"DSSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSLR_MDS.ChangeRow(this.cRowID+'      1',1;
             ,.w_LSSERIAL,"DSSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSLR_MDS)
        i_f=.GSLR_MDS.BuildFilter()
        if !(i_f==.GSLR_MDS.cQueryFilter)
          i_fnidx=.GSLR_MDS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSLR_MDS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSLR_MDS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSLR_MDS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSLR_MDS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LSSERIAL = NVL(LSSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from LOG_SINC where LSSERIAL=KeySet.LSSERIAL
    *
    i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LOG_SINC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LOG_SINC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LOG_SINC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LSSERIAL = NVL(LSSERIAL,space(10))
        .op_LSSERIAL = .w_LSSERIAL
        .w_LS__DATA = NVL(cp_ToDate(LS__DATA),ctod("  /  /  "))
        .w_LS___ORA = NVL(LS___ORA,0)
        .w_LS___MIN = NVL(LS___MIN,0)
        .w_LS___SEC = NVL(LS___SEC,0)
        .w_LS___LOG = NVL(LS___LOG,space(0))
        .w_LSPUBSIN = NVL(LSPUBSIN,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'LOG_SINC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LSSERIAL = space(10)
      .w_LS__DATA = ctod("  /  /  ")
      .w_LS___ORA = 0
      .w_LS___MIN = 0
      .w_LS___SEC = 0
      .w_LS___LOG = space(0)
      .w_LSPUBSIN = space(1)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'LOG_SINC')
    this.DoRTCalc(1,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SELOG","i_codazi,w_LSSERIAL")
      .op_codazi = .w_codazi
      .op_LSSERIAL = .w_LSSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLSSERIAL_1_1.enabled = !i_bVal
      .Page1.oPag.oLS__DATA_1_2.enabled = i_bVal
      .Page1.oPag.oLS___ORA_1_4.enabled = i_bVal
      .Page1.oPag.oLS___MIN_1_6.enabled = i_bVal
      .Page1.oPag.oLS___SEC_1_8.enabled = i_bVal
      .Page1.oPag.oLS___LOG_1_9.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oLS__DATA_1_2.enabled = .t.
      endif
    endwith
    this.GSLR_MDS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'LOG_SINC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSLR_MDS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSSERIAL,"LSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LS__DATA,"LS__DATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LS___ORA,"LS___ORA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LS___MIN,"LS___MIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LS___SEC,"LS___SEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LS___LOG,"LS___LOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSPUBSIN,"LSPUBSIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
    i_lTable = "LOG_SINC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.LOG_SINC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.LOG_SINC_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SELOG","i_codazi,w_LSSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into LOG_SINC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LOG_SINC')
        i_extval=cp_InsertValODBCExtFlds(this,'LOG_SINC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LSSERIAL,LS__DATA,LS___ORA,LS___MIN,LS___SEC"+;
                  ",LS___LOG,LSPUBSIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_LS__DATA)+;
                  ","+cp_ToStrODBC(this.w_LS___ORA)+;
                  ","+cp_ToStrODBC(this.w_LS___MIN)+;
                  ","+cp_ToStrODBC(this.w_LS___SEC)+;
                  ","+cp_ToStrODBC(this.w_LS___LOG)+;
                  ","+cp_ToStrODBC(this.w_LSPUBSIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LOG_SINC')
        i_extval=cp_InsertValVFPExtFlds(this,'LOG_SINC')
        cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL)
        INSERT INTO (i_cTable);
              (LSSERIAL,LS__DATA,LS___ORA,LS___MIN,LS___SEC,LS___LOG,LSPUBSIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LSSERIAL;
                  ,this.w_LS__DATA;
                  ,this.w_LS___ORA;
                  ,this.w_LS___MIN;
                  ,this.w_LS___SEC;
                  ,this.w_LS___LOG;
                  ,this.w_LSPUBSIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.LOG_SINC_IDX,i_nConn)
      *
      * update LOG_SINC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'LOG_SINC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LS__DATA="+cp_ToStrODBC(this.w_LS__DATA)+;
             ",LS___ORA="+cp_ToStrODBC(this.w_LS___ORA)+;
             ",LS___MIN="+cp_ToStrODBC(this.w_LS___MIN)+;
             ",LS___SEC="+cp_ToStrODBC(this.w_LS___SEC)+;
             ",LS___LOG="+cp_ToStrODBC(this.w_LS___LOG)+;
             ",LSPUBSIN="+cp_ToStrODBC(this.w_LSPUBSIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'LOG_SINC')
        i_cWhere = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
        UPDATE (i_cTable) SET;
              LS__DATA=this.w_LS__DATA;
             ,LS___ORA=this.w_LS___ORA;
             ,LS___MIN=this.w_LS___MIN;
             ,LS___SEC=this.w_LS___SEC;
             ,LS___LOG=this.w_LS___LOG;
             ,LSPUBSIN=this.w_LSPUBSIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSLR_MDS : Saving
      this.GSLR_MDS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_LSSERIAL,"DSSERIAL";
             )
      this.GSLR_MDS.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSLR_MDS : Deleting
    this.GSLR_MDS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_LSSERIAL,"DSSERIAL";
           )
    this.GSLR_MDS.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.LOG_SINC_IDX,i_nConn)
      *
      * delete LOG_SINC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOG_SINC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_SINC_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SELOG","i_codazi,w_LSSERIAL")
          .op_LSSERIAL = .w_LSSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_LSPUBSIN='P')
    local i_show1
    i_show1=not(this.w_LSPUBSIN='P')
    this.oPgFrm.Pages(2).enabled=i_show1 and not(this.w_LSPUBSIN='P')
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Dettagli"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLSSERIAL_1_1.value==this.w_LSSERIAL)
      this.oPgFrm.Page1.oPag.oLSSERIAL_1_1.value=this.w_LSSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oLS__DATA_1_2.value==this.w_LS__DATA)
      this.oPgFrm.Page1.oPag.oLS__DATA_1_2.value=this.w_LS__DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oLS___ORA_1_4.value==this.w_LS___ORA)
      this.oPgFrm.Page1.oPag.oLS___ORA_1_4.value=this.w_LS___ORA
    endif
    if not(this.oPgFrm.Page1.oPag.oLS___MIN_1_6.value==this.w_LS___MIN)
      this.oPgFrm.Page1.oPag.oLS___MIN_1_6.value=this.w_LS___MIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLS___SEC_1_8.value==this.w_LS___SEC)
      this.oPgFrm.Page1.oPag.oLS___SEC_1_8.value=this.w_LS___SEC
    endif
    if not(this.oPgFrm.Page1.oPag.oLS___LOG_1_9.value==this.w_LS___LOG)
      this.oPgFrm.Page1.oPag.oLS___LOG_1_9.value=this.w_LS___LOG
    endif
    if not(this.oPgFrm.Page1.oPag.oLSPUBSIN_1_13.RadioValue()==this.w_LSPUBSIN)
      this.oPgFrm.Page1.oPag.oLSPUBSIN_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'LOG_SINC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSLR_MDS.CheckForm()
      if i_bres
        i_bres=  .GSLR_MDS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSLR_MDS : Depends On
    this.GSLR_MDS.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgslr_aloPag1 as StdContainer
  Width  = 591
  height = 548
  stdWidth  = 591
  stdheight = 548
  resizeXpos=255
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLSSERIAL_1_1 as StdField with uid="ZSBBJNMUBN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LSSERIAL", cQueryName = "LSSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo generazione log",;
    HelpContextID = 253682686,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=98, Top=17, InputMask=replicate('X',10)

  add object oLS__DATA_1_2 as StdField with uid="TNUJGIEVOK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LS__DATA", cQueryName = "LS__DATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data elaborazione",;
    HelpContextID = 132391945,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=324, Top=17

  add object oLS___ORA_1_4 as StdField with uid="NQIGXDQZIU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LS___ORA", cQueryName = "LS___ORA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ora elaborazione",;
    HelpContextID = 137634825,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=447, Top=17, cSayPict='"99"', cGetPict='"99"'

  add object oLS___MIN_1_6 as StdField with uid="AUJMNYZLIM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LS___MIN", cQueryName = "LS___MIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Minuto elaborazione",;
    HelpContextID = 97246212,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=491, Top=17, cSayPict='"99"', cGetPict='"99"'

  add object oLS___SEC_1_8 as StdField with uid="TVDBKOOXYZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LS___SEC", cQueryName = "LS___SEC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Secondo elaborazione",;
    HelpContextID = 197909497,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=535, Top=17, cSayPict='"99"', cGetPict='"99"'

  add object oLS___LOG_1_9 as StdMemo with uid="RBVUFKPBFT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LS___LOG", cQueryName = "LS___LOG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Log pubblicazione/sincronizzazione",;
    HelpContextID = 187966467,;
   bGlobalFont=.t.,;
    Height=460, Width=566, Left=3, Top=85, readonly=.T.


  add object oLSPUBSIN_1_13 as StdCombo with uid="CWYYXYTTIM",rtseq=7,rtrep=.f.,left=447,top=51,width=122,height=21, enabled=.f.;
    , ToolTipText = "Indica se si tratta del log della pubblicazione o quello della sincronizzazione";
    , HelpContextID = 166784004;
    , cFormVar="w_LSPUBSIN",RowSource=""+"Pubblicazione,"+"Sincronizzazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLSPUBSIN_1_13.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oLSPUBSIN_1_13.GetRadio()
    this.Parent.oContained.w_LSPUBSIN = this.RadioValue()
    return .t.
  endfunc

  func oLSPUBSIN_1_13.SetRadio()
    this.Parent.oContained.w_LSPUBSIN=trim(this.Parent.oContained.w_LSPUBSIN)
    this.value = ;
      iif(this.Parent.oContained.w_LSPUBSIN=='P',1,;
      iif(this.Parent.oContained.w_LSPUBSIN=='S',2,;
      0))
  endfunc

  add object oStr_1_3 as StdString with uid="GPSCWRTHWP",Visible=.t., Left=227, Top=17,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="TPJCBJQHIM",Visible=.t., Left=396, Top=17,;
    Alignment=1, Width=48, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TUYVLWCKEI",Visible=.t., Left=480, Top=19,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NUXOQEFJAU",Visible=.t., Left=2, Top=64,;
    Alignment=0, Width=85, Height=19,;
    Caption="Log"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="FFLLUFONYP",Visible=.t., Left=525, Top=19,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="APXSOKXHGU",Visible=.t., Left=7, Top=17,;
    Alignment=1, Width=88, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="VUMSYWNLTS",Visible=.t., Left=332, Top=50,;
    Alignment=1, Width=112, Height=18,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgslr_aloPag2 as StdContainer
  Width  = 591
  height = 548
  stdWidth  = 591
  stdheight = 548
  resizeXpos=430
  resizeYpos=476
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="WJLUVWHTIH",left=4, top=9, width=584, height=539, bOnScreen=.t.;
    , caption='\<Dettaglio'
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gslr_mds",lower(this.oContained.GSLR_MDS.class))=0
        this.oContained.GSLR_MDS.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_alo','LOG_SINC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LSSERIAL=LOG_SINC.LSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
