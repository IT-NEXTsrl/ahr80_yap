* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bcr                                                        *
*              Verifica correttezza filtri verticali dinamici da sedi          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-22                                                      *
* Last revis.: 2014-04-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bcr",oParentObject)
return(i_retval)

define class tgslr_bcr as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_EN__LINK = space(254)
  w_EN_TABLE = space(30)
  w_CODFIS = .f.
  w_CODIVA = .f.
  w_TIPCON = .f.
  w_CODICE = .f.
  w_CODCLI = .f.
  * --- WorkFile variables
  ENT_DETT_idx=0
  PUBLVERT_idx=0
  TAB_ESTE_idx=0
  CAMPIEST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se si definisce un filtro verticale su una chiave di una tabella principale allora tutte le relazioni definite sulla tabella principale che utilizzano il campo modificato con filtro verticale dovranno contenere 
    *     espressioni nel formato tab1CAMPO=tab2.CAMPO2 (non sono ammesse espressioni composte  (per ex:   ABS(tab1CAMPO)=tab2.CAMPO2 + 4 
    *     Se lanciato dalla maschera di definizione dei filtri verticali GSLR_MFV allora il batch recupera tutte le relazioni definite sulla tabella principale e le sottopene al controllo della funzione CheckRel (se il campo non compare nella relazione allora la relazione � considerata valida )
    This.bUpdateParentObject=.f.
    if not bTrsErr
      * --- L'immancabile chiamata alla CP_READXDC...
      =cp_ReadXdc()
      * --- Se si � impostato una chiave si fanno i controlli su tutte le relazioni in cui la tabella w_CODARC compare come tabella principale
      if alltrim(this.oParentObject.w_PVFIELDS) $ cp_KeyToSQL ( I_DCX.GetIdxDef( this.oParentObject.w_CODARC ,1 ) )
        * --- Recupera tutte le relazioni da sottoporre al controllo di correttezza
        * --- Select from ENT_DETT
        i_nConn=i_TableProp[this.ENT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ENT_DETT_idx,2],.t.,this.ENT_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select EN__LINK , EN_TABLE  from "+i_cTable+" ENT_DETT ";
              +" where ENCODICE="+cp_ToStrODBC(this.oParentObject.w_PVCODENT)+" And ENPRINCI='N'";
               ,"_Curs_ENT_DETT")
        else
          select EN__LINK , EN_TABLE from (i_cTable);
           where ENCODICE=this.oParentObject.w_PVCODENT And ENPRINCI="N";
            into cursor _Curs_ENT_DETT
        endif
        if used('_Curs_ENT_DETT')
          select _Curs_ENT_DETT
          locate for 1=1
          do while not(eof())
          this.w_EN__LINK = _Curs_ENT_DETT.EN__LINK
          this.w_EN_TABLE = _Curs_ENT_DETT.EN_TABLE
          * --- Se una delle relazioni non � corretata si interrompe la transazione
          if NOT checkrel ( alltrim(this.w_EN__LINK), alltrim(this.oParentObject.w_CODARC) , alltrim(this.oParentObject.w_PVFIELDS))
            this.w_MESS = ah_MsgFormat("Nell' entit� %1 �  definita la relazione tra le tabelle %2 e %3%0L' espressioni che coinvolge il campo %4 deve essere nel formato semplice tab1.campo1=tab2.campo2 !%0Operazione interrotta.",alltrim(this.oParentObject.w_PVCODENT), alltrim(this.oParentObject.w_CODARC) , alltrim(this.w_EN_TABLE) ,alltrim(this.oParentObject.w_PVFIELDS))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            exit
          endif
            select _Curs_ENT_DETT
            continue
          enddo
          use
        endif
      endif
    endif
    if not bTrsErr
      * --- Se sono stati definiti dei filtri dinamici standard (traslaclidaiava o traslaclidaCF) allora verifica che nelle tabelle estese siano 
      *     stati definiti le estensioni con ANCODICE, ANTIPCON e ANPARARIVA (o ANCOPDFIS)
      this.w_CODCLI = .F.
      this.w_CODFIS = .F.
      this.w_CODIVA = .F.
      this.w_TIPCON = .F.
      this.w_CODICE = .F.
      if this.oParentObject.w_FUNZIONE = "Altro" 
        i_retcode = 'stop'
        return
      endif
      * --- Verifica che la tabella comprenda l estensione ANTIPCON
      * --- Select from CAMPIEST
      i_nConn=i_TableProp[this.CAMPIEST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
            +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='ANTIPCON'";
             ,"_Curs_CAMPIEST")
      else
        select COUNT (*) as CONTA from (i_cTable);
         where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="ANTIPCON";
          into cursor _Curs_CAMPIEST
      endif
      if used('_Curs_CAMPIEST')
        select _Curs_CAMPIEST
        locate for 1=1
        do while not(eof())
        if CONTA>0 
          this.w_TIPCON = .T.
        endif
          select _Curs_CAMPIEST
          continue
        enddo
        use
      endif
      * --- Verifica che la tabella comprenda l estensione ANCODICE
      * --- Select from CAMPIEST
      i_nConn=i_TableProp[this.CAMPIEST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
            +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='ANCODICE'";
             ,"_Curs_CAMPIEST")
      else
        select COUNT (*) as CONTA from (i_cTable);
         where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="ANCODICE";
          into cursor _Curs_CAMPIEST
      endif
      if used('_Curs_CAMPIEST')
        select _Curs_CAMPIEST
        locate for 1=1
        do while not(eof())
        if CONTA>0 
          this.w_CODICE = .T.
        endif
          select _Curs_CAMPIEST
          continue
        enddo
        use
      endif
      * --- Verifica che la tabella comprenda l estensione CLCODCLI
      * --- Select from CAMPIEST
      i_nConn=i_TableProp[this.CAMPIEST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
            +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='CLCODCLI'";
             ,"_Curs_CAMPIEST")
      else
        select COUNT (*) as CONTA from (i_cTable);
         where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="CLCODCLI";
          into cursor _Curs_CAMPIEST
      endif
      if used('_Curs_CAMPIEST')
        select _Curs_CAMPIEST
        locate for 1=1
        do while not(eof())
        if CONTA>0 
          this.w_CODCLI = .T.
        endif
          select _Curs_CAMPIEST
          continue
        enddo
        use
      endif
      * --- Verifica che la tabella comprenda l estensione ANCODFIS o ANCODIVA
      do case
        case "TraslacliDaIVA" $ this.oParentObject.w_FUNZIONE
          if alltrim (this.oParentObject.w_PVCODARC)="CONTI"
            i_retcode = 'stop'
            return
          endif
          * --- Select from CAMPIEST
          i_nConn=i_TableProp[this.CAMPIEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
                +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='ANPARIVA'";
                 ,"_Curs_CAMPIEST")
          else
            select COUNT (*) as CONTA from (i_cTable);
             where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="ANPARIVA";
              into cursor _Curs_CAMPIEST
          endif
          if used('_Curs_CAMPIEST')
            select _Curs_CAMPIEST
            locate for 1=1
            do while not(eof())
            if CONTA>0 
              this.w_CODIVA = .T.
            endif
              select _Curs_CAMPIEST
              continue
            enddo
            use
          endif
          if NOT (this.w_TIPCON AND this.w_CODICE AND this.w_CODIVA)
            this.w_MESS = ah_MsgFormat("Per usare la funzione 'Trova cliente da P. iva' sulla tabella %1 � obbligatorio aver definito un estensione su %1 contenente i seguenti alias ANCODICE, ANTIPCON e ANPARIVA ", alltrim (this.oParentObject.w_PVCODARC))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        case "TraslacliDaCF" $ this.oParentObject.w_FUNZIONE 
          if alltrim (this.oParentObject.w_PVCODARC)="CONTI"
            i_retcode = 'stop'
            return
          endif
          * --- Select from CAMPIEST
          i_nConn=i_TableProp[this.CAMPIEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
                +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='ANCODFIS'";
                 ,"_Curs_CAMPIEST")
          else
            select COUNT (*) as CONTA from (i_cTable);
             where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="ANCODFIS";
              into cursor _Curs_CAMPIEST
          endif
          if used('_Curs_CAMPIEST')
            select _Curs_CAMPIEST
            locate for 1=1
            do while not(eof())
            if CONTA>0 
              this.w_CODFIS = .T.
            endif
              select _Curs_CAMPIEST
              continue
            enddo
            use
          endif
          if NOT (this.w_TIPCON AND this.w_CODICE AND this.w_CODFIS)
            this.w_MESS = ah_MsgFormat("Per usare la funzione 'Trova cliente da C. fiscale' sulla tabella %1 � obbligatorio aver definito un estensione su %1 contenente i seguenti alias: ANCODICE, ANTIPCON e ANCODFIS" , alltrim(this.oParentObject.w_PVCODARC))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        case this.oParentObject.w_FUNZIONE = "LoreLookTab ('CLI_VEND','CLCODCLI','CLPARIVA','CLPARIVA')" AND alltrim(this.oParentObject.w_PVCODARC)<>"CLI_VEND"
          * --- Verifica presenza dei campi CLCODCLI e CLPARIVA nel pacchetto da sincronizzare (devono essere
          * --- Select from CAMPIEST
          i_nConn=i_TableProp[this.CAMPIEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
                +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='CLPARIVA'";
                 ,"_Curs_CAMPIEST")
          else
            select COUNT (*) as CONTA from (i_cTable);
             where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="CLPARIVA";
              into cursor _Curs_CAMPIEST
          endif
          if used('_Curs_CAMPIEST')
            select _Curs_CAMPIEST
            locate for 1=1
            do while not(eof())
            if CONTA>0 
              this.w_CODIVA = .T.
            endif
              select _Curs_CAMPIEST
              continue
            enddo
            use
          endif
          if NOT (this.w_CODCLI AND this.w_CODIVA)
            this.w_MESS = ah_MsgFormat("Per usare la funzione 'Trova cliente POS da P. iva' sulla tabella %1 � obbligatorio aver definito un estensione su %1 contenente i seguenti alias CLCODCLI e CLPARIVA ", alltrim (this.oParentObject.w_PVCODARC))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        case this.oParentObject.w_FUNZIONE ="LoreLookTab ('CLI_VEND','CLCODCLI','CLCODFIS','CLCODFIS')" AND alltrim(this.oParentObject.w_PVCODARC)<>"CLI_VEND"
          * --- Verifica presenza dei campi CLCODCLI e CLCODFIS nel pacchetto da sincronizzare (devono essere
          * --- Select from CAMPIEST
          i_nConn=i_TableProp[this.CAMPIEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAMPIEST_idx,2],.t.,this.CAMPIEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" CAMPIEST ";
                +" where CMCODTAB= "+cp_ToStrODBC(this.oParentObject.w_PVCODARC)+" AND CM_CAMPO='CLCODFIS'";
                 ,"_Curs_CAMPIEST")
          else
            select COUNT (*) as CONTA from (i_cTable);
             where CMCODTAB= this.oParentObject.w_PVCODARC AND CM_CAMPO="CLCODFIS";
              into cursor _Curs_CAMPIEST
          endif
          if used('_Curs_CAMPIEST')
            select _Curs_CAMPIEST
            locate for 1=1
            do while not(eof())
            if CONTA>0 
              this.w_CODIVA = .T.
            endif
              select _Curs_CAMPIEST
              continue
            enddo
            use
          endif
          if NOT (this.w_CODCLI AND this.w_CODIVA)
            this.w_MESS = ah_MsgFormat("Per usare la funzione 'Trova cliente POS da C. fiscale' sulla tabella %1 � obbligatorio aver definito un estensione su %1 contenente i seguenti alias CLCODCLI e CLCODFIS ", alltrim (this.oParentObject.w_PVCODARC))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
      endcase
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ENT_DETT'
    this.cWorkTables[2]='PUBLVERT'
    this.cWorkTables[3]='TAB_ESTE'
    this.cWorkTables[4]='CAMPIEST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ENT_DETT')
      use in _Curs_ENT_DETT
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    if used('_Curs_CAMPIEST')
      use in _Curs_CAMPIEST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
