* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_kgm                                                        *
*              Genera mirror                                                   *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_12]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2009-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_kgm",oParentObject))

* --- Class definition
define class tgslr_kgm as StdForm
  Top    = 39
  Left   = 34

  * --- Standard Properties
  Width  = 665
  Height = 470+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-04-14"
  HelpContextID=236360599
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gslr_kgm"
  cComment = "Genera mirror"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ELIMINA = space(1)
  w_VERBOSE = .F.
  w_Msg = space(0)
  w_EDIT = .F.
  w_SELEZI = space(1)
  w_SUPERENT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_kgmPag1","gslr_kgm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generazione")
      .Pages(2).addobject("oPag","tgslr_kgmPag2","gslr_kgm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELIMINA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_SUPERENT = this.oPgFrm.Pages(2).oPag.SUPERENT
    DoDefault()
    proc Destroy()
      this.w_SUPERENT = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gslr_bgm with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ELIMINA=space(1)
      .w_VERBOSE=.f.
      .w_Msg=space(0)
      .w_EDIT=.f.
      .w_SELEZI=space(1)
          .DoRTCalc(1,3,.f.)
        .w_EDIT = .T.
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Genera le tabelle di mirror a partire dalle super entit�, se la tabella di mirror � gia presente non viene eseguita nessuna operazione."))
      .oPgFrm.Page2.oPag.SUPERENT.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(ah_MsgFormat("Attivando il check elimina � possibile eliminare tutte le tabelle di mirror prima di ricrearle.%0Nella pagina selezioni � possibile selezionare le super entit� di cui creare le tabelle di mirror."))
        .w_SELEZI = 'S'
      .oPgFrm.Page2.oPag.oObj_2_5.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Genera le tabelle di mirror a partire dalle super entit�, se la tabella di mirror � gia presente non viene eseguita nessuna operazione."))
        .oPgFrm.Page2.oPag.SUPERENT.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(ah_MsgFormat("Attivando il check elimina � possibile eliminare tutte le tabelle di mirror prima di ricrearle.%0Nella pagina selezioni � possibile selezionare le super entit� di cui creare le tabelle di mirror."))
        .oPgFrm.Page2.oPag.oObj_2_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Genera le tabelle di mirror a partire dalle super entit�, se la tabella di mirror � gia presente non viene eseguita nessuna operazione."))
        .oPgFrm.Page2.oPag.SUPERENT.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(ah_MsgFormat("Attivando il check elimina � possibile eliminare tutte le tabelle di mirror prima di ricrearle.%0Nella pagina selezioni � possibile selezionare le super entit� di cui creare le tabelle di mirror."))
        .oPgFrm.Page2.oPag.oObj_2_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page2.oPag.SUPERENT.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELIMINA_1_1.RadioValue()==this.w_ELIMINA)
      this.oPgFrm.Page1.oPag.oELIMINA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVERBOSE_1_4.RadioValue()==this.w_VERBOSE)
      this.oPgFrm.Page1.oPag.oVERBOSE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_5.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_5.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_4.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_kgmPag1 as StdContainer
  Width  = 662
  height = 470
  stdWidth  = 662
  stdheight = 470
  resizeXpos=152
  resizeYpos=120
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELIMINA_1_1 as StdCheck with uid="GGPLJILTML",rtseq=1,rtrep=.f.,left=7, top=65, caption="Elimina",;
    ToolTipText = "Se attivo, distrugge la tabella di mirror se presente",;
    HelpContextID = 252152762,;
    cFormVar="w_ELIMINA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIMINA_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELIMINA_1_1.GetRadio()
    this.Parent.oContained.w_ELIMINA = this.RadioValue()
    return .t.
  endfunc

  func oELIMINA_1_1.SetRadio()
    this.Parent.oContained.w_ELIMINA=trim(this.Parent.oContained.w_ELIMINA)
    this.value = ;
      iif(this.Parent.oContained.w_ELIMINA=='S',1,;
      0)
  endfunc


  add object oBtn_1_2 as StdButton with uid="SKOOVQACPP",left=557, top=418, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 236389350;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      with this.Parent.oContained
        gslr_bgm(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_EDIT)
      endwith
    endif
  endfunc


  add object oBtn_1_3 as StdButton with uid="AUWPCNUDEI",left=607, top=418, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243678022;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVERBOSE_1_4 as StdCheck with uid="FVKZVFABRD",rtseq=2,rtrep=.f.,left=589, top=65, caption="Verbose",;
    ToolTipText = "Se attivo il log mostra tutte le frasi svolte sul database",;
    HelpContextID = 162660778,;
    cFormVar="w_VERBOSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVERBOSE_1_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oVERBOSE_1_4.GetRadio()
    this.Parent.oContained.w_VERBOSE = this.RadioValue()
    return .t.
  endfunc

  func oVERBOSE_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_VERBOSE==.T.,1,;
      0)
  endfunc

  add object oMsg_1_5 as StdMemo with uid="QYBPPIIQMH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 236813254,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=323, Width=648, Left=7, Top=90, tabstop = .f., readonly = .t.


  add object oBtn_1_6 as StdButton with uid="QKNERQNDZN",left=7, top=418, width=48,height=45,;
    CpPicture="bmp\save.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per salvare il log di elaborazione";
    , HelpContextID = 77838886;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        STRTOFILE(.w_MSG, PUTFILE('',cp_MsgFormat("Log_Elaboraz_Mirror"),"TXT"))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_Msg ))
      endwith
    endif
  endfunc


  add object oObj_1_8 as cp_calclbl with uid="UGVRUJCXVA",left=10, top=4, width=640,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 122512666


  add object oObj_1_9 as cp_calclbl with uid="QJPNPKWHMA",left=10, top=17, width=640,height=44,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 122512666
enddefine
define class tgslr_kgmPag2 as StdContainer
  Width  = 662
  height = 470
  stdWidth  = 662
  stdheight = 470
  resizeXpos=237
  resizeYpos=129
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object SUPERENT as cp_szoombox with uid="VAYQJJKGWO",left=1, top=6, width=660,height=411,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",cTable="SUENMAST",cZoomFile="GSLR_KGM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,;
    nPag=2;
    , HelpContextID = 122512666


  add object oBtn_2_2 as StdButton with uid="HCMPQPPKOA",left=557, top=419, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 236389350;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        gslr_bgm (this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_EDIT)
      endwith
    endif
  endfunc


  add object oBtn_2_3 as StdButton with uid="LTUTKNVQTH",left=607, top=419, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243678022;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_4 as StdRadio with uid="ASHEAQLXZM",rtseq=5,rtrep=.f.,left=4, top=422, width=158,height=32;
    , ToolTipText = "Seleziona/deseleziona tutto";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 50291162
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 50291162
      this.Buttons(2).Top=15
      this.SetAll("Width",156)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutto")
      StdRadio::init()
    endproc

  func oSELEZI_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELEZI_2_4.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_4.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='N',2,;
      0))
  endfunc


  add object oObj_2_5 as cp_runprogram with uid="HBJVHXEJZM",left=-4, top=491, width=360,height=29,;
    caption='Seleziona entit�',;
   bGlobalFont=.t.,;
    prg="GSLR_BGM ('S')",;
    cEvent = "w_SELEZI Changed,Blank",;
    nPag=2;
    , HelpContextID = 265762180
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_kgm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
