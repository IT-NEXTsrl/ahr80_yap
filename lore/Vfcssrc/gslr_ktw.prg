* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_ktw                                                        *
*              Treeview struttura entit�                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_ktw",oParentObject))

* --- Class definition
define class tgslr_ktw as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 701
  Height = 591
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=82406505
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  SUENMAST_IDX = 0
  cPrg = "gslr_ktw"
  cComment = "Treeview struttura entit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CURSORNA = space(10)
  w_CODICE = space(30)
  w_DESENT = space(60)
  w_PADRE = space(1)
  w_TreeView = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_ktwPag1","gslr_ktw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TreeView = this.oPgFrm.Pages(1).oPag.TreeView
    DoDefault()
    proc Destroy()
      this.w_TreeView = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='SUENMAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURSORNA=space(10)
      .w_CODICE=space(30)
      .w_DESENT=space(60)
      .w_PADRE=space(1)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_CODICE))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
      .oPgFrm.Page1.oPag.TreeView.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .DoRTCalc(3,3,.f.)
        .w_PADRE = Alltrim(Nvl( .w_TREEVIEW.GetVAr('PADRE'),' '))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(1,3,.t.)
            .w_PADRE = Alltrim(Nvl( .w_TREEVIEW.GetVAr('PADRE'),' '))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.TreeView.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SUENMAST_IDX,3]
    i_lTable = "SUENMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SUENMAST_IDX,2], .t., this.SUENMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SUENMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SUENMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SUCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select SUCODICE,SUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SUCODICE',trim(this.w_CODICE))
          select SUCODICE,SUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.SUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('SUENMAST','*','SUCODICE',cp_AbsName(oSource.parent,'oCODICE_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUCODICE,SUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUCODICE',oSource.xKey(1))
            select SUCODICE,SUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUCODICE,SUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SUCODICE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUCODICE',this.w_CODICE)
            select SUCODICE,SUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.SUCODICE,space(30))
      this.w_DESENT = NVL(_Link_.SUDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(30)
      endif
      this.w_DESENT = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SUENMAST_IDX,2])+'\'+cp_ToStr(_Link_.SUCODICE,1)
      cp_ShowWarn(i_cKey,this.SUENMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_3.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_3.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESENT_1_4.value==this.w_DESENT)
      this.oPgFrm.Page1.oPag.oDESENT_1_4.value=this.w_DESENT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_ktwPag1 as StdContainer
  Width  = 697
  height = 591
  stdWidth  = 697
  stdheight = 591
  resizeXpos=349
  resizeYpos=490
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="MTIFDHWRUR",left=3, top=628, width=167,height=19,;
    caption='GSLR_BTW(Open)',;
   bGlobalFont=.t.,;
    prg="GSLR_BTW('Open')",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 206963395

  add object oCODICE_1_3 as StdField with uid="APLVUXDHME",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 76818214,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=92, Top=13, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="SUENMAST", oKey_1_1="SUCODICE", oKey_1_2="this.w_CODICE"

  func oCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SUENMAST','*','SUCODICE',cp_AbsName(this.parent,'oCODICE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESENT_1_4 as StdField with uid="PTEWOTAUEC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESENT", cQueryName = "DESENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 71372086,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=248, Top=13, InputMask=replicate('X',60)


  add object oBtn_1_5 as StdButton with uid="ACUBIZKGYY",left=596, top=45, width=48,height=45,;
    CpPicture="BMP\TREEVIEW.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la treeview";
    , HelpContextID = 198466374;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .NotifyEvent('Calcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_6 as cp_runprogram with uid="ENSKRVEEVZ",left=3, top=649, width=167,height=19,;
    caption='GSLR_BTW(Close)',;
   bGlobalFont=.t.,;
    prg="GSLR_BTW('Close')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 241981


  add object TreeView as cp_Treeview with uid="GYODZTEHZL",left=13, top=45, width=568,height=531,;
    caption='Object',;
   bGlobalFont=.t.,;
    cNodeBmp="tree1.bmp",cLeafBmp="Tree2.bmp",nIndent=20,cCursor="Vista",cShowFields="ALLTRIM(CODICE)+' - '+ALLTRIM(DESCRI)",cNodeShowField="",cLeafShowField="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 95591142


  add object oBtn_1_9 as StdButton with uid="UQKMJNJALB",left=596, top=469, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dettagli del conto/mastro";
    , HelpContextID = 144850335;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSLR_BTW(this.Parent.oContained,"Dett")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_PADRE))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="AYBNNJWSNT",left=596, top=531, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire dalla visualizzazione";
    , HelpContextID = 75089082;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="RQSKFAEHAZ",left=596, top=377, width=48,height=45,;
    CpPicture="bmp\esplodi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per esplodere la treeview";
    , HelpContextID = 159773882;
    , caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODICE))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="POVNMSFBDI",left=596, top=423, width=48,height=45,;
    CpPicture="bmp\implodi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per implodere la treeview";
    , HelpContextID = 159775354;
    , caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODICE))
      endwith
    endif
  endfunc


  add object oObj_1_13 as cp_runprogram with uid="BICCTZKADQ",left=3, top=670, width=274,height=19,;
    caption='GSLR_BTW(DxBtn)',;
   bGlobalFont=.t.,;
    prg="GSLR_BTW('DxBtn')",;
    cEvent = "w_treeview NodeRightClick",;
    nPag=1;
    , HelpContextID = 149385789

  add object oStr_1_8 as StdString with uid="KSDUJTFZWN",Visible=.t., Left=6, Top=14,;
    Alignment=1, Width=85, Height=18,;
    Caption="Super entit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_ktw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
