* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_ste                                                        *
*              Stampa tabelle estese                                           *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_12]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-21                                                      *
* Last revis.: 2008-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_ste",oParentObject))

* --- Class definition
define class tgslr_ste as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 529
  Height = 137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-05-13"
  HelpContextID=74017897
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  TAB_ESTE_IDX = 0
  XDC_TABLE_IDX = 0
  cPrg = "gslr_ste"
  cComment = "Stampa tabelle estese"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SECODICE = space(2)
  w_SECODICE2 = space(2)
  w_SEDESCRI = space(50)
  w_SEDESCRI2 = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_stePag1","gslr_ste",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSECODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TAB_ESTE'
    this.cWorkTables[2]='XDC_TABLE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SECODICE=space(2)
      .w_SECODICE2=space(2)
      .w_SEDESCRI=space(50)
      .w_SEDESCRI2=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SECODICE))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SECODICE2))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SECODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBPHNAME like "+cp_ToStrODBC(trim(this.w_SECODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select TBPHNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBPHNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBPHNAME',trim(this.w_SECODICE))
          select TBPHNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBPHNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SECODICE)==trim(_Link_.TBPHNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SECODICE) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBPHNAME',cp_AbsName(oSource.parent,'oSECODICE_1_2'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBPHNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBPHNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBPHNAME',oSource.xKey(1))
            select TBPHNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBPHNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBPHNAME="+cp_ToStrODBC(this.w_SECODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBPHNAME',this.w_SECODICE)
            select TBPHNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODICE = NVL(_Link_.TBPHNAME,space(2))
      this.w_SEDESCRI = NVL(_Link_.TBCOMMENT,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SECODICE = space(2)
      endif
      this.w_SEDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBPHNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SECODICE2
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBPHNAME like "+cp_ToStrODBC(trim(this.w_SECODICE2)+"%");

          i_ret=cp_SQL(i_nConn,"select TBPHNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBPHNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBPHNAME',trim(this.w_SECODICE2))
          select TBPHNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBPHNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SECODICE2)==trim(_Link_.TBPHNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SECODICE2) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBPHNAME',cp_AbsName(oSource.parent,'oSECODICE2_1_3'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBPHNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBPHNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBPHNAME',oSource.xKey(1))
            select TBPHNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBPHNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBPHNAME="+cp_ToStrODBC(this.w_SECODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBPHNAME',this.w_SECODICE2)
            select TBPHNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODICE2 = NVL(_Link_.TBPHNAME,space(2))
      this.w_SEDESCRI2 = NVL(_Link_.TBCOMMENT,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SECODICE2 = space(2)
      endif
      this.w_SEDESCRI2 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBPHNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSECODICE_1_2.value==this.w_SECODICE)
      this.oPgFrm.Page1.oPag.oSECODICE_1_2.value=this.w_SECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODICE2_1_3.value==this.w_SECODICE2)
      this.oPgFrm.Page1.oPag.oSECODICE2_1_3.value=this.w_SECODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDESCRI_1_8.value==this.w_SEDESCRI)
      this.oPgFrm.Page1.oPag.oSEDESCRI_1_8.value=this.w_SEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDESCRI2_1_10.value==this.w_SEDESCRI2)
      this.oPgFrm.Page1.oPag.oSEDESCRI2_1_10.value=this.w_SEDESCRI2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_stePag1 as StdContainer
  Width  = 525
  height = 137
  stdWidth  = 525
  stdheight = 137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSECODICE_1_2 as StdField with uid="MQPFKCNFRO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SECODICE", cQueryName = "SECODICE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Archivio d' inizio selezione",;
    HelpContextID = 153751147,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=117, Top=6, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBPHNAME", oKey_1_2="this.w_SECODICE"

  func oSECODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSECODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSECODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBPHNAME',cp_AbsName(this.parent,'oSECODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
  endproc

  add object oSECODICE2_1_3 as StdField with uid="LHSJXLHPYE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SECODICE2", cQueryName = "SECODICE2",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Archivio di fine selezione",;
    HelpContextID = 153751947,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=117, Top=29, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBPHNAME", oKey_1_2="this.w_SECODICE2"

  func oSECODICE2_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSECODICE2_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSECODICE2_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBPHNAME',cp_AbsName(this.parent,'oSECODICE2_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
  endproc


  add object oObj_1_4 as cp_outputCombo with uid="ZLERXIVPWT",left=117, top=58, width=397,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 103979750


  add object oBtn_1_5 as StdButton with uid="CCGSUHUDGX",left=419, top=86, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 67771686;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="YXYHLPQOEL",left=469, top=86, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66700474;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSEDESCRI_1_8 as StdField with uid="SHOKQHXTJZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SEDESCRI", cQueryName = "SEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 68165231,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=220, Top=6, InputMask=replicate('X',50)

  add object oSEDESCRI2_1_10 as StdField with uid="NPCDJKMEYD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SEDESCRI2", cQueryName = "SEDESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 68166031,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=220, Top=29, InputMask=replicate('X',50)

  add object oStr_1_1 as StdString with uid="DHELKCNPMQ",Visible=.t., Left=27, Top=6,;
    Alignment=1, Width=85, Height=18,;
    Caption="Da archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KURZTBJWHQ",Visible=.t., Left=7, Top=58,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DPYJZUKPEY",Visible=.t., Left=27, Top=29,;
    Alignment=1, Width=85, Height=18,;
    Caption="a archivio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_ste','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
