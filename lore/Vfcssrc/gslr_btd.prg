* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_btd                                                        *
*              Treeview DBF entit�                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_btd",oParentObject,m.pTipo)
return(i_retval)

define class tgslr_btd as StdBatch
  * --- Local variables
  pTipo = space(5)
  w_CURDIR = space(254)
  w_ENCODICE = space(30)
  w_DESCRI = space(60)
  w_CONTA2 = 0
  w_CONTA3 = 0
  w_Loop = 0
  w_Loop2 = 0
  w_NOMEFILE = space(254)
  w_ARCNAME = space(254)
  w_DESTAB = space(60)
  w_LVL1 = space(5)
  w_LVL2 = space(5)
  w_PATFILE = space(254)
  w_Padre = .NULL.
  w_Gest = .NULL.
  w_ALIAS = space(10)
  w_FTODEL = space(254)
  w_CANALE = space(1)
  w_SHARED = space(254)
  w_SEFTPHST = space(254)
  w_SEFTPUSR = space(254)
  w_SEFTPPWD = space(254)
  w_NomeZip = space(254)
  w_TEMPPATH = space(254)
  w_Destinatario = space(2)
  w_ZipFile = space(254)
  w_FDate = ctot("")
  w_FnewDate = ctot("")
  w_NewDir = space(254)
  w_NewFile = space(254)
  w_PL__SEDE = space(2)
  w_Mittente = space(2)
  w_Destinatario = space(2)
  w_TIPOSEDE = space(1)
  w_Sede = space(2)
  w_SALVA = .f.
  w_TIPSAVE = space(1)
  w_Attr = 0
  w_FTPHST = space(254)
  w_FTPUSR = space(254)
  w_FTPPWD = space(254)
  w_SHARED = space(254)
  w_FTPMOD = space(1)
  * --- WorkFile variables
  ENT_MAST_idx=0
  XDC_TABLE_idx=0
  PARALORE_idx=0
  PUBLMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea struttura treeview per i DBF contenuti nella cartella del pacchetto
    *     Utilizzata anche per i DBF del Log errori
    this.w_Padre = This.oParentObject
    this.w_Gest = this.w_Padre.oParentObject
    w_NumDir = 0
    if Not Empty(this.oParentObject.w_Path)
      * --- Se ho indicato uno zip calcolo la path nella temp
      *     alrimenti se apro la maschera e la chiudo con esc effettua la deletefolder
      this.w_TEMPPATH = TEMPADHOC() + "\Remote\Log\Treeview\"
    endif
    do case
      case this.pTipo = "Open"
        * --- Prima di scompattare la zip pulisco la cartella da possibili precedenti scompattazioni
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scompatto lo zip nella cartella tempadhoc
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Memorizzo la cartella di lavoro corrente w_CURDIR
        *     per poi riposizionarmi alla fine nella Done
        *     
        this.w_CURDIR = Sys(5)+Sys(2003)
        cd (this.w_TEMPPATH)
        * --- Si inseriscono, in un array, tutte le cartelle presenti nella cartelle indicata
        w_NumDir = ADIR(w_ARRAYDIR,"","D")
        if w_NumDir >0
          if EMPTY(this.oParentObject.w_CURSORNA)
            this.oParentObject.w_CURSORNA = SYS(2015)
            CREATE CURSOR (this.oParentObject.w_CURSORNA) (ENTITA C(30), DESCRI C(60), TABELLA C(30), DESTAB C(60), PATH C(254), LVLKEY C(12), CPBMPNAME C(254))
          else
             
 Select(this.oParentObject.w_CURSORNA) 
 Zap
          endif
          cd (this.w_Curdir)
          this.w_Loop = 1
          * --- Eseguo un ciclo nelle cartelle delle entit� per prendere i file
          this.w_LVL1 = "000"
          do while this.w_Loop <= w_NumDir
            * --- Evito le cartelle '.' e '..' del DOS
            if w_ARRAYDIR(this.w_Loop, 1) <> "." And w_ARRAYDIR(this.w_Loop, 1) <> ".." 
              ah_Msg(Alltrim(w_ARRAYDIR(this.w_Loop,1)),.T.)
              this.w_ENCODICE = Alltrim(w_ARRAYDIR(this.w_Loop,1))
              if Upper(Alltrim(this.w_ENCODICE))="CHECKTABLEPLAN"
                this.w_DESCRI = Ah_MsgFormat("Controllo data ultima modifica struttura")
              else
                * --- Leggo la descrizione dell'entit�
                * --- Read from ENT_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ENT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ENT_MAST_idx,2],.t.,this.ENT_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ENDESCRI"+;
                    " from "+i_cTable+" ENT_MAST where ";
                        +"ENCODICE = "+cp_ToStrODBC(this.w_ENCODICE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ENDESCRI;
                    from (i_cTable) where;
                        ENCODICE = this.w_ENCODICE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_DESCRI = NVL(cp_ToDate(_read_.ENDESCRI),cp_NullValue(_read_.ENDESCRI))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.w_LVL1 = Right("000"+ Alltrim(Str(this.w_Loop,4,0)), 3 )
              Insert into (this.oParentObject.w_CURSORNA) Values ( this.w_ENCODICE, this.w_DESCRI, Space(30), Space(50), "", this.w_LVL1, ".\bmp\Entity2.Bmp" )
              * --- Costruisco un oggetto che contiene i file DBF delle cartelle del pacchetto
              *     Visto che la ADIR crea un Array (limitato a 65000 elementi) in alcuni casi potrebbe creare errori
              *     Diciamo con una cartella contenente 13000 files
               
 fso = CreateObject("Scripting.FileSystemObject") 
 folder = fso.GetFolder(Alltrim(this.w_TempPath)+this.w_ENCODICE)
              cd (Alltrim(this.w_TempPath)+this.w_ENCODICE)
              this.w_Loop2 = 1
              this.w_PATFILE = ""
              For Each myFile in folder.Files
              ah_Msg(Alltrim(MyFile.Name),.T.)
              * --- Prendo il nome del file
              this.w_NOMEFILE = Alltrim(MyFile.Name)
              if Upper(Right(this.w_NOMEFILE,3))="DBF"
                this.w_ARCNAME = SubStr(this.w_NOMEFILE, 1, Rat(".", this.w_NOMEFILE ) -1 )
                this.w_PATFILE = Alltrim(this.w_TempPath)+Alltrim(this.w_ENCODICE)+"\"+this.w_NOMEFILE
                if Upper(Alltrim(this.w_ARCNAME))="CHECKTABLEPLAN"
                  * --- CheckTablePlan � il DBF di controllo plan check date
                  this.w_DESTAB = Ah_MsgFormat("Controllo data ultima modifica struttura")
                else
                  * --- Leggo la descrizione della tabella
                  * --- Read from XDC_TABLE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.XDC_TABLE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.XDC_TABLE_idx,2],.t.,this.XDC_TABLE_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "TBCOMMENT"+;
                      " from "+i_cTable+" XDC_TABLE where ";
                          +"TBNAME = "+cp_ToStrODBC(this.w_ARCNAME);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      TBCOMMENT;
                      from (i_cTable) where;
                          TBNAME = this.w_ARCNAME;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DESTAB = NVL(cp_ToDate(_read_.TBCOMMENT),cp_NullValue(_read_.TBCOMMENT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                this.w_LVL2 = Alltrim(this.w_LVL1)+"."+Right("000"+ Alltrim(Str(this.w_Loop2,4,0)), 3 )
                Insert into (this.oParentObject.w_CURSORNA) Values ( this.w_ARCNAME, this.w_DESTAB, this.w_ARCNAME, this.w_DESTAB, this.w_PATFILE, this.w_LVL2, ".\bmp\master.Bmp" )
                this.w_Loop2 = this.w_Loop2 + 1
              endif
              EndFor
              fso = null
            endif
            this.w_Loop = this.w_Loop + 1
          enddo
        endif
        cd (this.w_Curdir)
        L_NomeCursore = this.oParentObject.w_CURSORNA
        Select(L_NomeCursore) 
 index on lvlkey tag lvlkey collate "MACHINE"
        * --- Imposto i Bitmap
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        This.oParentObject.NotifyEvent("Esegui")
      case this.pTipo = "Close"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if used( ( this.oParentObject.w_CURSORNA ) )
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
      case this.pTipo = "Dett"
        this.w_ALIAS = Sys(2015)
        this.w_PATFILE = Alltrim(Nvl( this.oParentObject.w_TREEVIEW.GetVAr("PATH")," "))
        * --- Memorizzo la data ultima modifica del file
        this.w_FDate = FDATE(this.w_PATFILE, 1)
        * --- Visualizzo il DBF e lo chiudo
        Use( this.w_PATFILE ) in 0 Alias ( this.w_ALIAS ) 
 Select( this.w_ALIAS ) 
 Brow 
 Select( this.w_ALIAS ) 
 Use 
        * --- Cerco la nuova data del file
        *     Se modificato rispetto alla vecchia data chiedo se si vogliono salvare le modifiche
        this.w_FnewDate = FDATE(this.w_PATFILE, 1)
        if this.w_FDate <> this.w_FNewDate And Ah_YesNo("Si desidera salvare le modifiche?")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTipo = "DxBtn"
         
 Local nCmd 
 nCmd= 0 
 DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT 
 DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Dettagli") 
 ON SELECTION BAR 1 OF popCmd nCmd = 1 
 
 ACTIVATE POPUP popCmd 
 DEACTIVATE POPUP popCmd 
 RELEASE POPUPS popCmd
        if nCmd <>0
          * --- Simula la pressione del bottone dettagli....
          DO GSLR_BTD WITH THIS.OPARENTOBJECT , "Dett"
        endif
      case this.pTipo = "Canc"
        if Not Ah_YesNo("Si desidera eliminare la tabella selezionata dal pacchetto?")
          i_retcode = 'stop'
          return
        endif
        this.w_FTODEL = Alltrim(Nvl( this.oParentObject.w_TREEVIEW.GetVAr("PATH")," "))
        if File(this.w_FTODEL)
          ERASE (this.w_FTODEL)
        endif
        this.w_FTODEL = StrTran(this.w_PATFILE,".DBF",".FPT")
        if File(this.w_FTODEL)
          ERASE (this.w_FTODEL)
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Rieseguo l'interrogazione del pacchetto
        this.w_Padre.NotifyEvent("Calcola")     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scarica e scompatta
     
 Local L_OldErr, L_bErr, L_OldArea 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T. 
 L_OldArea=Select()
    if ! Directory(this.w_TEMPPATH)
      MakeDir(this.w_TEMPPATH)
    endif
    * --- Scarico il file in locale sulla temp
    this.w_ZipFile = ALLTRIM(this.oParentObject.w_Path)
    Local L_RetMsg
    L_RetMsg = ""
    if ! ZipUnZip("U", this.w_ZipFile, this.w_TEMPPATH, this.oParentObject.w_SEPWDZIP, @L_RetMsg)
      Ah_ErrorMsg("Errore nella scompattazione del file zip %1%0%2", , , this.w_ZipFile, L_RetMsg)
      * --- Rispristino la vecchia gestione errori
       
 On Error &L_OldErr 
 Select ( L_OldArea )
      * --- Si � verificato un errore
      i_retcode = 'stop'
      return
    endif
    * --- Reset flag errore
    L_bErr = .f.
    * --- Rispristino la vecchia gestione errori
     
 On Error &L_OldErr 
 Select ( L_OldArea )
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Salvataggio zip modificato
    this.w_SALVA = .F.
    * --- Leggo la sede nei parametri
    * --- Read from PARALORE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PARALORE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARALORE_idx,2],.t.,this.PARALORE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PL__SEDE"+;
        " from "+i_cTable+" PARALORE where ";
            +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PL__SEDE;
        from (i_cTable) where;
            PLCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PL__SEDE = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NomeZip = Alltrim(Substr(this.oParentObject.w_Path, Rat("\", this.oParentObject.w_Path)+1))
    if (Type("this.w_Gest")="O" And Upper(this.w_Gest.oParentObject.Class)="TGSLR_BVP") Or (Type("this.w_Gest")<>"O" And (Upper(Left(this.w_NomeZip,9))="ERRORDATA" Or Upper(Left(this.w_NomeZip,7))="OLDDATA" ) )
      * --- Indico come salvataggio il reinvio di dati alla sede
      this.w_TIPSAVE = "I"
      * --- Se la treeview DBF pacchetto � lanciata dalla visualizzazione pacchetti dati
      *     allora provo a ricostruire le sedi per risalvare il pacchetto nell'origine
      *     Se lanciato da men� invece controllo che il pacchetto non sia un pacchetto di errori
      * --- Prendo i primi due caratteri del nome file che sono la sede che invia i dati
      this.w_Mittente = Left(this.w_NomeZip, At("_", this.w_NomeZip) -1 )
      * --- Estrapolo il destinatario
      this.w_Destinatario = Alltrim(Substr(this.w_NomeZip, At("_", this.w_NomeZip) + 1, 2 ))
      * --- Prevedo il caso in cui il codice sede sia di un solo carattere, prendendone 2 
      *     comprenderei anche la _ che precede il progressivo
      this.w_Destinatario = StrTran(this.w_Destinatario, "_", "")
      if this.w_PL__SEDE=this.w_Mittente
        * --- Sede verso cui pubblico
        this.w_TIPOSEDE = "R"
        * --- Considero quindi il destinatario verso cui ho pubblicato il pacchetto
        this.w_Sede = this.w_Destinatario
      else
        if this.w_PL__SEDE=this.w_Destinatario
          * --- Sede da cui ricevo
          this.w_TIPOSEDE = "P"
          * --- Considero quindi il mittente da cui ricevo
          this.w_Sede = this.w_Mittente
        else
          * --- Nel caso in cui non riesco a rintracciare la sede indico come tipo di salvataggio quello su disco
          this.w_TIPSAVE = "S"
        endif
      endif
    else
      this.w_TIPSAVE = "S"
    endif
    if this.pTipo = "Canc"
      * --- Se ho cancellato un DBF dal pacchetto risalvo su se stesso
      this.w_NewFile = this.oParentObject.w_PATH
    else
      * --- Altrimenti do un nuovo nome
      this.w_NewFile = ADDBS(this.w_TEMPPATH)+this.w_NomeZip
    endif
    * --- Lancio maschera di salvataggio pacchetto modificato
    do GSLR_KSP with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NewFile = Alltrim(this.w_NewFile)
    if Not this.w_Salva
      * --- L'utente ha annullato il salvataggio sulla maschera
      Ah_ErrorMsg("Operazione annullata dall'utente", "Stop", g_APPLICATION)
      i_retcode = 'stop'
      return
    endif
    * --- Comprimo la cartella e creo il nuovo zip
    Local L_RetMsg
    L_RetMsg = ""
    if this.w_TIPSAVE="S" And File(this.w_NewFile)
      if Ah_YesNo("Il file %1 esiste gi�. Si desidera sovrascriverlo?", , this.w_NewFile)
        * --- Elimino lo zip di appoggio
        Erase(this.w_NewFile)
      endif
    endif
    if ! ZipUnZip("Z", this.w_NewFile, this.w_TEMPPATH, this.oParentObject.w_SEPWDZIP, @L_RetMsg)
      Ah_ErrorMsg("Problemi nella creazione dello zip %1%0%2", , ,this.w_NewFile, L_RetMsg)
      i_retcode = 'stop'
      return
    else
      if this.w_TIPSAVE="I"
        Ah_Msg("Creazione nuovo zip %1 completata", .T., .F., 1, this.w_NewFile)
      else
        Ah_ErrorMsg("Creazione nuovo zip %1 completata","i" , g_APPLICATION ,this.w_NewFile)
      endif
    endif
    if this.w_TIPSAVE="I"
      * --- Se ho selezionato invio dati allla sede...
      * --- Read from PUBLMAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PUBLMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PUBLMAST_idx,2],.t.,this.PUBLMAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SECANALE,SEFTPHST,SEFTPUSR,SEFTPPWD,SESHARED,SEFTPMOD"+;
          " from "+i_cTable+" PUBLMAST where ";
              +"SECODICE = "+cp_ToStrODBC(this.w_Sede);
              +" and SE__TIPO = "+cp_ToStrODBC(this.w_TIPOSEDE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SECANALE,SEFTPHST,SEFTPUSR,SEFTPPWD,SESHARED,SEFTPMOD;
          from (i_cTable) where;
              SECODICE = this.w_Sede;
              and SE__TIPO = this.w_TIPOSEDE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CANALE = NVL(cp_ToDate(_read_.SECANALE),cp_NullValue(_read_.SECANALE))
        this.w_FTPHST = NVL(cp_ToDate(_read_.SEFTPHST),cp_NullValue(_read_.SEFTPHST))
        this.w_FTPUSR = NVL(cp_ToDate(_read_.SEFTPUSR),cp_NullValue(_read_.SEFTPUSR))
        this.w_FTPPWD = NVL(cp_ToDate(_read_.SEFTPPWD),cp_NullValue(_read_.SEFTPPWD))
        this.w_SHARED = NVL(cp_ToDate(_read_.SESHARED),cp_NullValue(_read_.SESHARED))
        this.w_FTPMOD = NVL(cp_ToDate(_read_.SEFTPMOD),cp_NullValue(_read_.SEFTPMOD))
        use
        if i_Rows=0
          Ah_ErrorMsg("Dati relativi alla sede %1 inesistenti", , ,Alltrim(this.w_Sede))
          i_retcode = 'stop'
          return
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Creo connesione con posizione del pacchetto di origine
      if ! ConnessioneSede("C", this.w_Sede, this.w_TIPOSEDE)
        * --- Errore connessione
        Ah_ErrorMsg("Errore di connessione", "!", g_APPLICATION)
        i_retcode = 'stop'
        return
      endif
      * --- Copio nella cartella di destinazione se non ci sono errori
      * --- Reset flag errore
      L_bErr = .f.
      if this.w_CANALE="F"
        * --- Copio il file via FTP
        L_bErr = ! FtpClient("P", alltrim(this.w_FTPHST), alltrim(this.w_FTPUSR), alltrim(this.w_FTPPWD), alltrim(this.w_NewFile), alltrim(this.w_SHARED)+"/"+alltrim(this.w_NomeZip), 2, 0, .F. , this.w_FTPMOD)
      else
        Local l_macro
        l_macro = 'COPY FILE "' + this.w_NewFile +'" TO "'+ ADDBS(this.w_SHARED)+this.w_NomeZip +'"'
        &l_macro
      endif
      if L_bErr
        * --- Errore
        Ah_ErrorMsg("Impossibile copiare il file zip %1 in %2", "!", g_APPLICATION ,this.w_NewFIle, ADDBS(this.w_SHARED)+this.w_NomeZip)
      else
        * --- Ok
        Ah_ErrorMsg("Terminata copia del file zip %1 in %2", "i", g_APPLICATION ,this.w_NewFIle, ADDBS(this.w_SHARED)+this.w_NomeZip)
        * --- Elimino lo zip che nel caso di Reinvio del pacchetto � solo di appoggio
        Erase(this.w_NewFile)
      endif
      * --- Reset flag errore
      L_bErr = .f.
      * --- Se ho aperto una connessione RAS la chiudo
      ConnessioneSede("D", this.w_Sede, this.w_TIPOSEDE)
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina cursore della treeview e cancella cartella con zip scompattato (temp)
    if Not Empty(this.w_TempPath)
      * --- Cancello la cartella temporanea, � stato lanciato da GSLR_KVP
      if Not DeleteFolder(this.w_TempPath)
        * --- Se c'� errore di cartella la cartella rimane li...
      endif
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ENT_MAST'
    this.cWorkTables[2]='XDC_TABLE'
    this.cWorkTables[3]='PARALORE'
    this.cWorkTables[4]='PUBLMAST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
