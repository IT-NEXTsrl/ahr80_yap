* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bfi                                                        *
*              Filtro orizzontale                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-20                                                      *
* Last revis.: 2006-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bfi",oParentObject)
return(i_retval)

define class tgslr_bfi as StdBatch
  * --- Local variables
  w_FLNAME = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Facendo lo zoom sul filtro orizzontale, esegue uno zoom e preimposta il filtro 
    *     aggiungendo il nome della tabella
    vx_exec("gslr_qfi.vzm",this)
    this.oParentObject.w_DPFILTRO = Alltrim(this.oParentObject.w_DPFILTRO) + " " + Alltrim(this.oParentObject.w_DPCODARC)+"."+Alltrim(this.w_FLNAME)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
