* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bvp                                                        *
*              Visualizzazione pacchetti dati                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bvp",oParentObject,m.pTipo)
return(i_retval)

define class tgslr_bvp as StdBatch
  * --- Local variables
  pTipo = space(5)
  w_Loop = 0
  w_Loop2 = 0
  w_LVL1 = space(5)
  w_LVL2 = space(15)
  w_PL__SEDE = space(2)
  w_NUMZIP = 0
  w_SKELETON = space(15)
  w_Attr = 0
  w_CURFILEZIP = space(10)
  w_PROG = space(10)
  w_CANALE = space(1)
  w_SHARED = space(254)
  w_SEPWDZIP = space(254)
  w_SEFTPHST = space(254)
  w_SEFTPUSR = space(254)
  w_SEFTPPWD = space(254)
  w_SEFTPMOD = space(1)
  w_SEDE = space(2)
  w_TEMPPATH = space(254)
  w_ZipFile = space(254)
  * --- WorkFile variables
  PARALORE_idx=0
  PUBLMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea struttura treeview dei pacchetti pubblicati e ricevuti
    w_NumDir = 0
    do case
      case this.pTipo = "Open"
        * --- FillTree
        AH_MSG("Elaborazione treeview...")
        * --- Creo, se necessario, il cursore della treeview
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
          CREATE CURSOR (this.oParentObject.w_CURSORNA) (SEDE C(2), DESCRI C(50), PROG C(10), ULTPROG N(10), FILEZIP C(254), LVLKEY C(15), CPBMPNAME C(254))
        else
           
 Select(this.oParentObject.w_CURSORNA) 
 Zap
        endif
        * --- Leggo la sede nei parametri
        * --- Read from PARALORE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PARALORE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PARALORE_idx,2],.t.,this.PARALORE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PL__SEDE"+;
            " from "+i_cTable+" PARALORE where ";
                +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PL__SEDE;
            from (i_cTable) where;
                PLCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PL__SEDE = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Seleziono le sedi
        this.w_Loop = 0
        * --- Select from PUBLMAST
        i_nConn=i_TableProp[this.PUBLMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PUBLMAST_idx,2],.t.,this.PUBLMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select *  from "+i_cTable+" PUBLMAST ";
              +" where SE__TIPO = "+cp_ToStrODBC(this.oParentObject.w_TIPOSEDE)+"";
              +" order by SECODICE";
               ,"_Curs_PUBLMAST")
        else
          select * from (i_cTable);
           where SE__TIPO = this.oParentObject.w_TIPOSEDE;
           order by SECODICE;
            into cursor _Curs_PUBLMAST
        endif
        if used('_Curs_PUBLMAST')
          select _Curs_PUBLMAST
          locate for 1=1
          do while not(eof())
          this.w_Loop = this.w_Loop + 1
          this.w_LVL1 = RIGHT("000" + ALLTRIM(STR(this.w_Loop)), 3)
          Insert into (this.oParentObject.w_CURSORNA) Values ( _Curs_PUBLMAST.SECODICE, _Curs_PUBLMAST.SEDESCRI, "", 0, "", this.w_LVL1, ".\BMP\Sede.bmp")
          if ! ConnessioneSede("C", _Curs_PUBLMAST.SECODICE, this.oParentObject.w_TIPOSEDE)
            * --- Errore connessione
            i_retcode = 'stop'
            return
          endif
          * --- FileSystem
          if this.oParentObject.w_TIPOSEDE="P"
            this.w_SKELETON = _Curs_PUBLMAST.SECODICE+"_"+this.w_PL__SEDE+"*.ahz"
          else
            this.w_SKELETON = this.w_PL__SEDE + "_" + _Curs_PUBLMAST.SECODICE+"*.ahz"
          endif
          if _Curs_PUBLMAST.SECANALE="F"
            * --- Ftp
            this.w_CURFILEZIP = FtpClient("L", _Curs_PUBLMAST.SEFTPHST, _Curs_PUBLMAST.SEFTPUSR, _Curs_PUBLMAST.SEFTPPWD, ALLTRIM(_Curs_PUBLMAST.SESHARED)+"/"+this.w_SKELETON, , , , ,_Curs_PUBLMAST.SEFTPMOD )
            this.w_NUMZIP = 0
            if Type("This.w_CURFILEZIP")="C" And Used(this.w_CURFILEZIP)
               
 SELECT(this.w_CURFILEZIP) 
 GO TOP 
 DIMENSION aFileZip(RECCOUNT(this.w_CURFILEZIP),5) 
 COPY ALL TO ARRAY aFileZip 
 USE IN SELECT(this.w_CURFILEZIP)
              this.w_NUMZIP = ALEN(aFileZip, 1)
              this.w_Attr = 2
            endif
          else
            this.w_NUMZIP = ADIR(aFileZip, ADDBS(ALLTRIM(_Curs_PUBLMAST.SESHARED))+this.w_SKELETON)
            this.w_Attr = 5
          endif
          this.w_Loop2 = 1
          do while this.w_Loop2 <= this.w_NUMZIP
            if aFileZip[this.w_Loop2, this.w_Attr] <> "D"
              this.w_LVL2 = this.w_LVL1 + "." + RIGHT(".0000000000" + Alltrim(Str(this.w_Loop2)) , 10)
              this.w_PROG = SUBSTR(JUSTSTEM(aFileZip[this.w_Loop2, 1]), RAT("_", aFileZip[this.w_Loop2, 1])+1)
              Insert into (this.oParentObject.w_CURSORNA) Values (_Curs_PUBLMAST.SECODICE , "", this.w_PROG, _Curs_PUBLMAST.SEULTPRO, Alltrim(aFileZip[this.w_Loop2, 1]), this.w_LVL2, ".\BMP\lotti.bmp")
              this.w_Loop2 = this.w_Loop2 + 1
            endif
          enddo
          * --- Se ho aperto una connessione RAS la chiudo
          ConnessioneSede("D", _Curs_PUBLMAST.SECODICE, this.oParentObject.w_TIPOSEDE)
            select _Curs_PUBLMAST
            continue
          enddo
          use
        endif
        * --- Indici sulla chiave
        L_NomeCursore = this.oParentObject.w_CURSORNA
        Select(L_NomeCursore) 
 index on lvlkey tag lvlkey collate "MACHINE"
        * --- Elimino sedi ricevute, se richiesto
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Seleziono il cursore
        if this.oParentObject.w_ALLZIP = "S"
          this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        else
          this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORSR
        endif
        * --- Aggiorno la treeview
        This.oParentObject.NotifyEvent("Esegui")
      case this.pTipo = "Close"
        * --- Chiusura cursori
        USE IN SELECT( this.oParentObject.w_CURSORNA )
        USE IN SELECT( this.oParentObject.w_CURSORSR )
      case this.pTipo = "Dett"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo = "DxBtn"
         
 Local nCmd 
 nCmd= 0 
 DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT 
 DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Dettagli") 
 ON SELECTION BAR 1 OF popCmd nCmd = 1 
 
 ACTIVATE POPUP popCmd 
 DEACTIVATE POPUP popCmd 
 RELEASE POPUPS popCmd
        if nCmd <>0 And Not Empty(this.oParentObject.w_FILEZIP)
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTipo="AllZip"
        * --- Seleziono il cursore
        if USED(this.oParentObject.w_CURSORNA) And USED(this.oParentObject.w_CURSORSR)
          if this.oParentObject.w_ALLZIP = "S"
            this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
          else
            this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORSR
          endif
          * --- Aggiorno le o_ altrimenti questo batch viene lanciato ll'infinito e va in loop
          this.oParentObject.o_TIPOSEDE = this.oParentObject.w_TIPOSEDE
          * --- Aggiorno la treeview
          This.oParentObject.NotifyEvent("Esegui")
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino sedi ricevute
    this.oParentObject.w_CURSORSR = SYS(2015)
    SELECT * FROM (this.oParentObject.w_CURSORNA) WHERE VAL(PROG) > ULTPROG OR ULTPROG = 0 INTO CURSOR (this.oParentObject.w_CURSORSR)
    * --- Indici sulla chiave
    L_NomeCursore = this.oParentObject.w_CURSORSR
    Select(L_NomeCursore) 
 index on lvlkey tag lvlkey collate "MACHINE"
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scarica e scompatta
    this.w_SEDE = Alltrim(Nvl( this.oParentObject.w_TREEVIEW.GetVar("SEDE")," "))
    * --- Read from PUBLMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PUBLMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PUBLMAST_idx,2],.t.,this.PUBLMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SECANALE,SESHARED,SEPWDZIP,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD"+;
        " from "+i_cTable+" PUBLMAST where ";
            +"SECODICE = "+cp_ToStrODBC(this.w_SEDE);
            +" and SE__TIPO = "+cp_ToStrODBC(this.oParentObject.w_TIPOSEDE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SECANALE,SESHARED,SEPWDZIP,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD;
        from (i_cTable) where;
            SECODICE = this.w_SEDE;
            and SE__TIPO = this.oParentObject.w_TIPOSEDE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CANALE = NVL(cp_ToDate(_read_.SECANALE),cp_NullValue(_read_.SECANALE))
      this.w_SHARED = NVL(cp_ToDate(_read_.SESHARED),cp_NullValue(_read_.SESHARED))
      this.w_SEPWDZIP = NVL(cp_ToDate(_read_.SEPWDZIP),cp_NullValue(_read_.SEPWDZIP))
      this.w_SEFTPHST = NVL(cp_ToDate(_read_.SEFTPHST),cp_NullValue(_read_.SEFTPHST))
      this.w_SEFTPUSR = NVL(cp_ToDate(_read_.SEFTPUSR),cp_NullValue(_read_.SEFTPUSR))
      this.w_SEFTPPWD = NVL(cp_ToDate(_read_.SEFTPPWD),cp_NullValue(_read_.SEFTPPWD))
      this.w_SEFTPMOD = NVL(cp_ToDate(_read_.SEFTPMOD),cp_NullValue(_read_.SEFTPMOD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if ! ConnessioneSede("C", this.w_SEDE, this.oParentObject.w_TIPOSEDE)
      * --- Errore connessione
      i_retcode = 'stop'
      return
    endif
    * --- Scarico il file in locale sulla temp
    this.w_TEMPPATH = TEMPADHOC() + "\Remote"
    MakeDir(this.w_TEMPPATH)
     
 Local L_OldErr, L_bErr, L_OldArea 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T. 
 L_OldArea=Select()
    this.oParentObject.w_FILEZIP = ALLTRIM(this.oParentObject.w_FILEZIP)
    if this.w_CANALE="F"
      * --- Copio il file via FTP
      this.oParentObject.w_FILEZIP = ftpClient("E", this.w_SEFTPHST, this.w_SEFTPUSR, this.w_SEFTPPWD, ALLTRIM(this.w_SHARED)+"/"+STRTRAN(JUSTFNAME(this.oParentObject.w_FILEZIP), ".", "*."), , , , , this.w_SEFTPMOD)
      L_bErr = ! FtpClient("G", this.w_SEFTPHST, this.w_SEFTPUSR, this.w_SEFTPPWD, ALLTRIM(this.w_SHARED)+"/"+JUSTFNAME(this.oParentObject.w_FILEZIP), this.w_TEMPPATH+"\"+JUSTFNAME(this.oParentObject.w_FILEZIP), 2, 0, , this.w_SEFTPMOD )
    else
      Local l_macro
      l_macro = 'COPY FILE "' + ADDBS(ALLTRIM(this.w_SHARED))+JUSTFNAME(this.oParentObject.w_FILEZIP) +'" TO "'+ this.w_TEMPPATH + '"'
      &l_macro
    endif
    if L_bErr
      Ah_ErrorMsg("Impossibile copiare il file zip %1 in %2", , ,this.oParentObject.w_FILEZIP, this.w_TEMPPATH)
      * --- Si � verificato un errore
      ConnessioneSede("D", this.w_SEDE, this.oParentObject.w_TIPOSEDE)
      * --- Rispristino la vecchia gestione errori
       
 On Error &L_OldErr 
 Select ( L_OldArea )
      i_retcode = 'stop'
      return
    endif
    ConnessioneSede("D", this.w_SEDE, this.oParentObject.w_TIPOSEDE)
    * --- Reset flag errore
    L_bErr = .f.
    * --- Estraggo lo zip
    this.w_ZipFile = ALLTRIM(TEMPADHOC()+"\Remote\"+this.oParentObject.w_FILEZIP)
    * --- Rispristino la vecchia gestione errori
     
 On Error &L_OldErr 
 Select ( L_OldArea )
    * --- Lancio visualizzazione pacchetto
    gslr_bap(this,this.w_ZipFile, this.w_SEPWDZIP)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PARALORE'
    this.cWorkTables[2]='PUBLMAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PUBLMAST')
      use in _Curs_PUBLMAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
