* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_kvp                                                        *
*              Visualizzazione pacchetti dati                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2008-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_kvp",oParentObject))

* --- Class definition
define class tgslr_kvp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 563
  Height = 585
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-05-15"
  HelpContextID=219583383
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  SUENMAST_IDX = 0
  PARALORE_IDX = 0
  cPrg = "gslr_kvp"
  cComment = "Visualizzazione pacchetti dati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CURSORNA = space(10)
  w_FILEZIP = space(254)
  w_TIPOSEDE = space(1)
  o_TIPOSEDE = space(1)
  w_ALLZIP = space(1)
  w_CURSORSR = space(10)
  w_TreeView = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_kvpPag1","gslr_kvp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOSEDE_1_12
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TreeView = this.oPgFrm.Pages(1).oPag.TreeView
    DoDefault()
    proc Destroy()
      this.w_TreeView = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SUENMAST'
    this.cWorkTables[2]='PARALORE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURSORNA=space(10)
      .w_FILEZIP=space(254)
      .w_TIPOSEDE=space(1)
      .w_ALLZIP=space(1)
      .w_CURSORSR=space(10)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
      .oPgFrm.Page1.oPag.TreeView.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_FILEZIP = Alltrim(Nvl( .w_TREEVIEW.GetVar('FILEZIP'),' '))
        .w_TIPOSEDE = 'P'
        .w_ALLZIP = 'N'
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(1,1,.t.)
            .w_FILEZIP = Alltrim(Nvl( .w_TREEVIEW.GetVar('FILEZIP'),' '))
        if .o_TIPOSEDE<>.w_TIPOSEDE
          .Calculate_JSIRJPEMJW()
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return

  proc Calculate_JSIRJPEMJW()
    with this
          * --- Allzip
          .w_ALLZIP = IIF(.w_TIPOSEDE='R', 'S', .w_ALLZIP)
          GSLR_BVP(this;
              ,"AllZip";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oALLZIP_1_14.enabled_(this.oPgFrm.Page1.oPag.oALLZIP_1_14.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gslr_kvp
    If cEvent='w_TIPOSEDE Changed'
       this.NotifyEvent('Calcola')
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.TreeView.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILEZIP_1_9.value==this.w_FILEZIP)
      this.oPgFrm.Page1.oPag.oFILEZIP_1_9.value=this.w_FILEZIP
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSEDE_1_12.RadioValue()==this.w_TIPOSEDE)
      this.oPgFrm.Page1.oPag.oTIPOSEDE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oALLZIP_1_14.RadioValue()==this.w_ALLZIP)
      this.oPgFrm.Page1.oPag.oALLZIP_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOSEDE = this.w_TIPOSEDE
    return

enddefine

* --- Define pages as container
define class tgslr_kvpPag1 as StdContainer
  Width  = 559
  height = 585
  stdWidth  = 559
  stdheight = 585
  resizeXpos=349
  resizeYpos=490
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="MTIFDHWRUR",left=-1, top=594, width=274,height=19,;
    caption='GSLR_BVP(Open)',;
   bGlobalFont=.t.,;
    prg="GSLR_BVP('Open')",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 95026486


  add object oBtn_1_3 as StdButton with uid="ACUBIZKGYY",left=463, top=53, width=48,height=45,;
    CpPicture="bmp\treeview.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la treeview";
    , HelpContextID = 36414650;
    , caption='Ese\<gui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        .NotifyEvent('Calcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_4 as cp_runprogram with uid="ENSKRVEEVZ",left=-1, top=615, width=274,height=19,;
    caption='GSLR_BVP(Close)',;
   bGlobalFont=.t.,;
    prg="GSLR_BVP('Close')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 33796406


  add object TreeView as cp_Treeview with uid="GYODZTEHZL",left=14, top=53, width=438,height=488,;
    caption='Object',;
   bGlobalFont=.t.,;
    cNodeBmp="tree1.bmp",cLeafBmp="",nIndent=20,cCursor="Vista",cShowFields="ALLTRIM(SEDE)+ ' - ' + ALLTRIM(DESCRI), PROG",cNodeShowField="",cLeafShowField="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 139289882


  add object oBtn_1_6 as StdButton with uid="RQSKFAEHAZ",left=463, top=345, width=48,height=45,;
    CpPicture="bmp\esplodi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per esplodere la treeview";
    , HelpContextID = 142216006;
    , caption='\<Esplodi', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="POVNMSFBDI",left=463, top=391, width=48,height=45,;
    CpPicture="bmp\implodi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per implodere la treeview";
    , HelpContextID = 142214534;
    , caption='\<Implodi', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_8 as cp_runprogram with uid="BICCTZKADQ",left=-1, top=636, width=275,height=19,;
    caption='GSLR_BVP(DxBtn)',;
   bGlobalFont=.t.,;
    prg="GSLR_BVP('DxBtn')",;
    cEvent = "w_treeview NodeRightClick",;
    nPag=1;
    , HelpContextID = 182940214

  add object oFILEZIP_1_9 as StdField with uid="ZJWCUMIXSB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FILEZIP", cQueryName = "FILEZIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 67067562,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=79, Top=551, InputMask=replicate('X',254)


  add object oBtn_1_11 as StdButton with uid="UQKMJNJALB",left=463, top=437, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 90030689;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSLR_BVP(this.Parent.oContained,"Dett")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(Alltrim(.w_FILEZIP)))
      endwith
    endif
  endfunc


  add object oTIPOSEDE_1_12 as StdCombo with uid="WOIPJENFUT",rtseq=3,rtrep=.f.,left=63,top=11,width=173,height=21;
    , HelpContextID = 127591035;
    , cFormVar="w_TIPOSEDE",RowSource=""+"Sede da cui ricevo,"+"Sede verso cui pubblico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSEDE_1_12.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPOSEDE_1_12.GetRadio()
    this.Parent.oContained.w_TIPOSEDE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSEDE_1_12.SetRadio()
    this.Parent.oContained.w_TIPOSEDE=trim(this.Parent.oContained.w_TIPOSEDE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSEDE=='P',1,;
      iif(this.Parent.oContained.w_TIPOSEDE=='R',2,;
      0))
  endfunc

  add object oALLZIP_1_14 as StdRadio with uid="QGZORIUIVS",rtseq=4,rtrep=.f.,left=261, top=11, width=188,height=23;
    , ToolTipText = "Se attivo: visualizza tutti i pacchetti altrimenti solo quelli ancora da ricevere";
    , cFormVar="w_ALLZIP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oALLZIP_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Ancora da ricevere"
      this.Buttons(1).HelpContextID = 234511354
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Ancora da ricevere","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Tutti"
      this.Buttons(2).HelpContextID = 234511354
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Se attivo: visualizza tutti i pacchetti altrimenti solo quelli ancora da ricevere")
      StdRadio::init()
    endproc

  func oALLZIP_1_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oALLZIP_1_14.GetRadio()
    this.Parent.oContained.w_ALLZIP = this.RadioValue()
    return .t.
  endfunc

  func oALLZIP_1_14.SetRadio()
    this.Parent.oContained.w_ALLZIP=trim(this.Parent.oContained.w_ALLZIP)
    this.value = ;
      iif(this.Parent.oContained.w_ALLZIP=='N',1,;
      iif(this.Parent.oContained.w_ALLZIP=='S',2,;
      0))
  endfunc

  func oALLZIP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOSEDE='P')
    endwith
   endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="AYBNNJWSNT",left=463, top=528, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire dalla visualizzazione";
    , HelpContextID = 226900806;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_18 as cp_runprogram with uid="PYBPYQCXJV",left=-1, top=657, width=274,height=19,;
    caption='GSLR_BVP(AllZip)',;
   bGlobalFont=.t.,;
    prg="GSLR_BVP('AllZip')",;
    cEvent = "w_ALLZIP Changed",;
    nPag=1;
    , HelpContextID = 193941665

  add object oStr_1_10 as StdString with uid="TKHUIAEZPB",Visible=.t., Left=12, Top=552,;
    Alignment=1, Width=65, Height=18,;
    Caption="File zip:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TCKWXKEDGM",Visible=.t., Left=9, Top=11,;
    Alignment=1, Width=51, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TPXQWEVXDZ",Visible=.t., Left=108, Top=683,;
    Alignment=0, Width=487, Height=18,;
    Caption="Manual block notifyevent init per ricalcolare la treeview al cambio della combo tiposede"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_kvp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
