* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_mdp                                                        *
*              Dettaglio archivi                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-13                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslr_mdp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslr_mdp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslr_mdp")
  return

* --- Class definition
define class tgslr_mdp as StdPCForm
  Width  = 498
  Height = 263
  Top    = 10
  Left   = 10
  cComment = "Dettaglio archivi"
  cPrg = "gslr_mdp"
  HelpContextID=188126103
  add object cnt as tcgslr_mdp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslr_mdp as PCContext
  w_DP__SEDE = space(2)
  w_DP__TIPO = space(1)
  w_DPCODENT = space(15)
  w_DPCODARC = space(30)
  w_DPFILTRO = space(254)
  w_PRINCI = space(1)
  w_TIPO = space(1)
  proc Save(i_oFrom)
    this.w_DP__SEDE = i_oFrom.w_DP__SEDE
    this.w_DP__TIPO = i_oFrom.w_DP__TIPO
    this.w_DPCODENT = i_oFrom.w_DPCODENT
    this.w_DPCODARC = i_oFrom.w_DPCODARC
    this.w_DPFILTRO = i_oFrom.w_DPFILTRO
    this.w_PRINCI = i_oFrom.w_PRINCI
    this.w_TIPO = i_oFrom.w_TIPO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DP__SEDE = this.w_DP__SEDE
    i_oTo.w_DP__TIPO = this.w_DP__TIPO
    i_oTo.w_DPCODENT = this.w_DPCODENT
    i_oTo.w_DPCODARC = this.w_DPCODARC
    i_oTo.w_DPFILTRO = this.w_DPFILTRO
    i_oTo.w_PRINCI = this.w_PRINCI
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgslr_mdp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 498
  Height = 263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=188126103
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DETTPUBL_IDX = 0
  ENT_DETT_IDX = 0
  cFile = "DETTPUBL"
  cKeySelect = "DP__SEDE,DP__TIPO,DPCODENT"
  cKeyWhere  = "DP__SEDE=this.w_DP__SEDE and DP__TIPO=this.w_DP__TIPO and DPCODENT=this.w_DPCODENT"
  cKeyDetail  = "DP__SEDE=this.w_DP__SEDE and DP__TIPO=this.w_DP__TIPO and DPCODENT=this.w_DPCODENT and DPCODARC=this.w_DPCODARC"
  cKeyWhereODBC = '"DP__SEDE="+cp_ToStrODBC(this.w_DP__SEDE)';
      +'+" and DP__TIPO="+cp_ToStrODBC(this.w_DP__TIPO)';
      +'+" and DPCODENT="+cp_ToStrODBC(this.w_DPCODENT)';

  cKeyDetailWhereODBC = '"DP__SEDE="+cp_ToStrODBC(this.w_DP__SEDE)';
      +'+" and DP__TIPO="+cp_ToStrODBC(this.w_DP__TIPO)';
      +'+" and DPCODENT="+cp_ToStrODBC(this.w_DPCODENT)';
      +'+" and DPCODARC="+cp_ToStrODBC(this.w_DPCODARC)';

  cKeyWhereODBCqualified = '"DETTPUBL.DP__SEDE="+cp_ToStrODBC(this.w_DP__SEDE)';
      +'+" and DETTPUBL.DP__TIPO="+cp_ToStrODBC(this.w_DP__TIPO)';
      +'+" and DETTPUBL.DPCODENT="+cp_ToStrODBC(this.w_DPCODENT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gslr_mdp"
  cComment = "Dettaglio archivi"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DP__SEDE = space(2)
  o_DP__SEDE = space(2)
  w_DP__TIPO = space(1)
  o_DP__TIPO = space(1)
  w_DPCODENT = space(15)
  o_DPCODENT = space(15)
  w_DPCODARC = space(30)
  o_DPCODARC = space(30)
  w_DPFILTRO = space(254)
  w_PRINCI = space(1)
  w_TIPO = space(1)
  o_TIPO = space(1)

  * --- Children pointers
  GSLR_MFV = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSLR_MFV additive
    with this
      .Pages(1).addobject("oPag","tgslr_mdpPag1","gslr_mdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSLR_MFV
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ENT_DETT'
    this.cWorkTables[2]='DETTPUBL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DETTPUBL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DETTPUBL_IDX,3]
  return

  function CreateChildren()
    this.GSLR_MFV = CREATEOBJECT('stdLazyChild',this,'GSLR_MFV')
    return

  procedure NewContext()
    return(createobject('tsgslr_mdp'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSLR_MFV)
      this.GSLR_MFV.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSLR_MFV.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSLR_MFV)
      this.GSLR_MFV.DestroyChildrenChain()
      this.GSLR_MFV=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSLR_MFV.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSLR_MFV.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSLR_MFV.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSLR_MFV.ChangeRow(this.cRowID+'      1',1;
             ,.w_DP__SEDE,"PV__SEDE";
             ,.w_DP__TIPO,"PV__TIPO";
             ,.w_DPCODENT,"PVCODENT";
             ,.w_DPCODARC,"PVCODARC";
             )
      .WriteTo_GSLR_MFV()
    endwith
    select (i_cOldSel)
    return

procedure WriteTo_GSLR_MFV()
  if at('gslr_mfv',lower(this.GSLR_MFV.class))<>0
    if this.GSLR_MFV.cnt.w_TIPO<>this.w_TIPO or this.GSLR_MFV.cnt.w_PV__SEDE<>this.w_DP__SEDE or this.GSLR_MFV.cnt.w_PV__TIPO<>this.w_DP__TIPO or this.GSLR_MFV.cnt.w_PVCODENT<>this.w_DPCODENT or this.GSLR_MFV.cnt.w_PVCODARC<>this.w_DPCODARC
      this.GSLR_MFV.cnt.w_TIPO = this.w_TIPO
      this.GSLR_MFV.cnt.w_PV__SEDE = this.w_DP__SEDE
      this.GSLR_MFV.cnt.w_PV__TIPO = this.w_DP__TIPO
      this.GSLR_MFV.cnt.w_PVCODENT = this.w_DPCODENT
      this.GSLR_MFV.cnt.w_PVCODARC = this.w_DPCODARC
      this.GSLR_MFV.cnt.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DETTPUBL where DP__SEDE=KeySet.DP__SEDE
    *                            and DP__TIPO=KeySet.DP__TIPO
    *                            and DPCODENT=KeySet.DPCODENT
    *                            and DPCODARC=KeySet.DPCODARC
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTPUBL_IDX,2],this.bLoadRecFilter,this.DETTPUBL_IDX,"gslr_mdp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DETTPUBL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DETTPUBL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DETTPUBL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DP__SEDE',this.w_DP__SEDE  ,'DP__TIPO',this.w_DP__TIPO  ,'DPCODENT',this.w_DPCODENT  )
      select * from (i_cTable) DETTPUBL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPO = space(1)
        .w_DP__SEDE = NVL(DP__SEDE,space(2))
        .w_DP__TIPO = NVL(DP__TIPO,space(1))
        .w_DPCODENT = NVL(DPCODENT,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DETTPUBL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_PRINCI = space(1)
          .w_DPCODARC = NVL(DPCODARC,space(30))
          .link_2_1('Load')
          .w_DPFILTRO = NVL(DPFILTRO,space(254))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace DPCODARC with .w_DPCODARC
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DP__SEDE=space(2)
      .w_DP__TIPO=space(1)
      .w_DPCODENT=space(15)
      .w_DPCODARC=space(30)
      .w_DPFILTRO=space(254)
      .w_PRINCI=space(1)
      .w_TIPO=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_DPCODARC))
         .link_2_1('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DETTPUBL')
    this.DoRTCalc(5,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSLR_MFV.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DETTPUBL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSLR_MFV.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP__SEDE,"DP__SEDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DP__TIPO,"DP__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODENT,"DPCODENT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DPCODARC C(30);
      ,t_DPFILTRO C(254);
      ,DPCODARC C(30);
      ,t_PRINCI C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslr_mdpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODARC_2_1.controlsource=this.cTrsName+'.t_DPCODARC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDPFILTRO_2_2.controlsource=this.cTrsName+'.t_DPFILTRO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(233)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODARC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTPUBL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTPUBL_IDX,2])
      *
      * insert into DETTPUBL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DETTPUBL')
        i_extval=cp_InsertValODBCExtFlds(this,'DETTPUBL')
        i_cFldBody=" "+;
                  "(DP__SEDE,DP__TIPO,DPCODENT,DPCODARC,DPFILTRO,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DP__SEDE)+","+cp_ToStrODBC(this.w_DP__TIPO)+","+cp_ToStrODBC(this.w_DPCODENT)+","+cp_ToStrODBCNull(this.w_DPCODARC)+","+cp_ToStrODBC(this.w_DPFILTRO)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DETTPUBL')
        i_extval=cp_InsertValVFPExtFlds(this,'DETTPUBL')
        cp_CheckDeletedKey(i_cTable,0,'DP__SEDE',this.w_DP__SEDE,'DP__TIPO',this.w_DP__TIPO,'DPCODENT',this.w_DPCODENT,'DPCODARC',this.w_DPCODARC)
        INSERT INTO (i_cTable) (;
                   DP__SEDE;
                  ,DP__TIPO;
                  ,DPCODENT;
                  ,DPCODARC;
                  ,DPFILTRO;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DP__SEDE;
                  ,this.w_DP__TIPO;
                  ,this.w_DPCODENT;
                  ,this.w_DPCODARC;
                  ,this.w_DPFILTRO;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTPUBL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DPCODARC))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DETTPUBL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DPCODARC="+cp_ToStrODBC(&i_TN.->DPCODARC)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DETTPUBL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DPCODARC=&i_TN.->DPCODARC;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DPCODARC))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DPCODARC="+cp_ToStrODBC(&i_TN.->DPCODARC)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DPCODARC=&i_TN.->DPCODARC;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DPCODARC with this.w_DPCODARC
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DETTPUBL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DETTPUBL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DPFILTRO="+cp_ToStrODBC(this.w_DPFILTRO)+;
                     ",DPCODARC="+cp_ToStrODBC(this.w_DPCODARC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DPCODARC="+cp_ToStrODBC(DPCODARC)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DETTPUBL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DPFILTRO=this.w_DPFILTRO;
                     ,DPCODARC=this.w_DPCODARC;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DPCODARC=&i_TN.->DPCODARC;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask footer and header children to save themselves
      * --- GSLR_MFV : Saving
      this.GSLR_MFV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DP__SEDE,"PV__SEDE";
             ,this.w_DP__TIPO,"PV__TIPO";
             ,this.w_DPCODENT,"PVCODENT";
             ,this.w_DPCODARC,"PVCODARC";
             )
      this.GSLR_MFV.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSLR_MFV : Deleting
    this.GSLR_MFV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DP__SEDE,"PV__SEDE";
           ,this.w_DP__TIPO,"PV__TIPO";
           ,this.w_DPCODENT,"PVCODENT";
           ,this.w_DPCODARC,"PVCODARC";
           )
    this.GSLR_MFV.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTPUBL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DPCODARC))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DETTPUBL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DPCODARC="+cp_ToStrODBC(&i_TN.->DPCODARC)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DPCODARC=&i_TN.->DPCODARC;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DPCODARC))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DETTPUBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTPUBL_IDX,2])
    if i_bUpd
      with this
        if  .o_TIPO<>.w_TIPO.or. .o_DP__SEDE<>.w_DP__SEDE.or. .o_DP__TIPO<>.w_DP__TIPO.or. .o_DPCODENT<>.w_DPCODENT.or. .o_DPCODARC<>.w_DPCODARC
          .WriteTo_GSLR_MFV()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PRINCI with this.w_PRINCI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_XWPHHMRBVS()
    with this
          * --- Svuota filtro al cambio di archivio
          .w_DPFILTRO = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDPFILTRO_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDPFILTRO_2_2.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gslr_mdp
    If UPPER(cEvent)='W_DPCODARC CHANGED'
       this.ChildrenChangeRow()
    ENDIF
    
    If UPPER(cEvent)='W_DPCODARC LOSTFOCUS'
       this.ChildrenChangeRow()
    ENDIF
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_DPCODARC Changed")
          .Calculate_XWPHHMRBVS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DPCODARC
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
    i_lTable = "ENT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2], .t., this.ENT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODARC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MEN',True,'ENT_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EN_TABLE like "+cp_ToStrODBC(trim(this.w_DPCODARC)+"%");
                   +" and ENCODICE="+cp_ToStrODBC(this.w_DPCODENT);

          i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE,ENPRINCI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE,EN_TABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',this.w_DPCODENT;
                     ,'EN_TABLE',trim(this.w_DPCODARC))
          select ENCODICE,EN_TABLE,ENPRINCI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE,EN_TABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODARC)==trim(_Link_.EN_TABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODARC) and !this.bDontReportError
            deferred_cp_zoom('ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(oSource.parent,'oDPCODARC_2_1'),i_cWhere,'GSAR_MEN',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODENT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE,ENPRINCI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ENCODICE,EN_TABLE,ENPRINCI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE,ENPRINCI";
                     +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ENCODICE="+cp_ToStrODBC(this.w_DPCODENT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1);
                       ,'EN_TABLE',oSource.xKey(2))
            select ENCODICE,EN_TABLE,ENPRINCI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODARC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE,ENPRINCI";
                   +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(this.w_DPCODARC);
                   +" and ENCODICE="+cp_ToStrODBC(this.w_DPCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_DPCODENT;
                       ,'EN_TABLE',this.w_DPCODARC)
            select ENCODICE,EN_TABLE,ENPRINCI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODARC = NVL(_Link_.EN_TABLE,space(30))
      this.w_PRINCI = NVL(_Link_.ENPRINCI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODARC = space(30)
      endif
      this.w_PRINCI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKNFILE(.w_DPCODARC,'C')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DPCODARC = space(30)
        this.w_PRINCI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)+'\'+cp_ToStr(_Link_.EN_TABLE,1)
      cp_ShowWarn(i_cKey,this.ENT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODARC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODARC_2_1.value==this.w_DPCODARC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODARC_2_1.value=this.w_DPCODARC
      replace t_DPCODARC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODARC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPFILTRO_2_2.value==this.w_DPFILTRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPFILTRO_2_2.value=this.w_DPFILTRO
      replace t_DPFILTRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPFILTRO_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'DETTPUBL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSLR_MFV.CheckForm()
      if i_bres
        i_bres=  .GSLR_MFV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(IIF( .w_PRINCI='S' , CHECKEXP( .w_DPCODARC , .w_DPFILTRO, .F. , .w_TIPO='R'  )  , .T. ))
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("L'espressione contiene campi non presenti ne nella chiave ne nelle eventuali super entit� o tabelle estese definite sull'archivio.")
        case   not(CHKNFILE(.w_DPCODARC,'C')) and not(empty(.w_DPCODARC)) and (not(Empty(.w_DPCODARC)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODARC_2_1
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_DPCODARC))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DP__SEDE = this.w_DP__SEDE
    this.o_DP__TIPO = this.w_DP__TIPO
    this.o_DPCODENT = this.w_DPCODENT
    this.o_DPCODARC = this.w_DPCODARC
    this.o_TIPO = this.w_TIPO
    * --- GSLR_MFV : Depends On
    this.GSLR_MFV.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DPCODARC)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DPCODARC=space(30)
      .w_DPFILTRO=space(254)
      .w_PRINCI=space(1)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_DPCODARC))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(5,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DPCODARC = t_DPCODARC
    this.w_DPFILTRO = t_DPFILTRO
    this.w_PRINCI = t_PRINCI
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DPCODARC with this.w_DPCODARC
    replace t_DPFILTRO with this.w_DPFILTRO
    replace t_PRINCI with this.w_PRINCI
    if i_srv='A'
      replace DPCODARC with this.w_DPCODARC
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslr_mdpPag1 as StdContainer
  Width  = 494
  height = 263
  stdWidth  = 494
  stdheight = 263
  resizeXpos=358
  resizeYpos=176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_1_4 as StdButton with uid="WPNPIFDBIB",left=427, top=216, width=48,height=45,;
    CpPicture="bmp\filtra.bmp", caption="", nPag=1;
    , ToolTipText = "Elenco dei campi che non si vuole acquisire con relativo valore di default";
    , HelpContextID = 57834117;
    , Caption='\<Filtri vert.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_4.Click()
      this.Parent.oContained.GSLR_MFV.LinkPCClick()
      this.Parent.oContained.WriteTo_GSLR_MFV()
    endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=3, width=470,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="DPCODARC",Label1="Archivio",Field2="DPFILTRO",Label2="Filtro orizzontale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32653958

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=22,;
    width=466+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=23,width=465+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ENT_DETT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ENT_DETT'
        oDropInto=this.oBodyCol.oRow.oDPCODARC_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslr_mdpBodyRow as CPBodyRowCnt
  Width=456
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDPCODARC_2_1 as StdTrsField with uid="EEJZXABDMP",rtseq=4,rtrep=.t.,;
    cFormVar="w_DPCODARC",value=space(30),isprimarykey=.t.,;
    HelpContextID = 255190919,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=-2, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="ENT_DETT", cZoomOnZoom="GSAR_MEN", oKey_1_1="ENCODICE", oKey_1_2="this.w_DPCODENT", oKey_2_1="EN_TABLE", oKey_2_2="this.w_DPCODARC"

  proc oDPCODARC_2_1.mBefore
    with this.Parent.oContained
      .ChildrenChangeRow()
    endwith
  endproc

  proc oDPCODARC_2_1.mAfter
    with this.Parent.oContained
      .ChildrenChangeRow()
    endwith
  endproc

  func oDPCODARC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODARC_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDPCODARC_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oDPCODARC_2_1.readonly and this.parent.oDPCODARC_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.ENT_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStrODBC(this.Parent.oContained.w_DPCODENT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStr(this.Parent.oContained.w_DPCODENT)
    endif
    do cp_zoom with 'ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(this.parent,'oDPCODARC_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MEN',"Archivi",'',this.parent.oContained
   endif
  endproc
  proc oDPCODARC_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MEN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ENCODICE=w_DPCODENT
     i_obj.w_EN_TABLE=this.parent.oContained.w_DPCODARC
    i_obj.ecpSave()
  endproc

  add object oDPFILTRO_2_2 as StdTrsField with uid="PYFFVEJLWC",rtseq=5,rtrep=.t.,;
    cFormVar="w_DPFILTRO",value=space(254),;
    HelpContextID = 196851579,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=224, Left=227, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  func oDPFILTRO_2_2.mCond()
    with this.Parent.oContained
      return (.w_PRINCI='S')
    endwith
  endfunc

  func oDPFILTRO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(CHECKEXP( .w_DPCODARC , .w_DPFILTRO ,  .F. ,  .w_TIPO='R'))
         bRes=(cp_WarningMsg(thisform.msgFmt("L'espressione contiene campi non presenti ne nella chiave ne nell'eventuale super entit� definita sull'archivio.")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oDPFILTRO_2_2.mZoom
      with this.Parent.oContained
        do GSLR_BFI with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover
  * ---
  func oDPCODARC_2_1.When()
    return(.t.)
  proc oDPCODARC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDPCODARC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_mdp','DETTPUBL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DP__SEDE=DETTPUBL.DP__SEDE";
  +" and "+i_cAliasName2+".DP__TIPO=DETTPUBL.DP__TIPO";
  +" and "+i_cAliasName2+".DPCODENT=DETTPUBL.DPCODENT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
