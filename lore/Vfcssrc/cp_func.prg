* --- Container for functions
* --- START CHKPATER
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkpater                                                        *
*              Controllo path del log errori                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-24                                                      *
* Last revis.: 2006-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chkpater

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PATERR
  m.w_PATERR=space(254)
  private w_Ret
  m.w_Ret=.f.
* --- WorkFile variables
  private PARALORE_idx
  PARALORE_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "chkpater"
if vartype(__chkpater_hook__)='O'
  __chkpater_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chkpater()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkpater')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if chkpater_OpenTables()
  chkpater_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chkpater()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkpater')
Endif
*--- Activity log
if vartype(__chkpater_hook__)='O'
  __chkpater_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chkpater_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione per controllare se la cartella indicata nei parametri della logistica remota
  *     per la creazione del log errori esiste
  *     Se non presenti si considera che la gestione log � disattiva
  m.w_Ret = .T.
  * --- Read from PARALORE
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PARALORE_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PARALORE_idx,2],.t.,PARALORE_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PLPATERR"+;
      " from "+i_cTable+" PARALORE where ";
          +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PLPATERR;
      from (i_cTable) where;
          PLCODAZI = i_CODAZI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_PATERR = NVL(cp_ToDate(_read_.PLPATERR),cp_NullValue(_read_.PLPATERR))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if Not Empty(m.w_PATERR) And Not(Directory(m.w_PATERR))
    * --- Se indicata cartella ma non presente su disco...
    m.w_Ret = .F.
  endif
  i_retcode = 'stop'
  i_retval = m.w_Ret
  return
endproc


  function chkpater_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PARALORE'
    return(cp_OpenFuncTables(1))
* --- END CHKPATER
* --- START CHKTBLPAC
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chktblpac                                                       *
*              Controllo check plan date                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-24                                                      *
* Last revis.: 2006-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chktblpac
param pPathDbf,w_SEDE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_ENTITA
  m.w_ENTITA=space(15)
  private w_TABLE
  m.w_TABLE=space(30)
  private w_Ret
  m.w_Ret=space(0)
* --- WorkFile variables
  private PUBLDETT_idx
  PUBLDETT_idx=0
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "chktblpac"
if vartype(__chktblpac_hook__)='O'
  __chktblpac_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chktblpac('+Transform(pPathDbf)+','+Transform(w_SEDE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chktblpac')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if chktblpac_OpenTables()
  chktblpac_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_PUBLDETT')
  use in _Curs_PUBLDETT
endif
if used('_Curs_ENT_DETT')
  use in _Curs_ENT_DETT
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chktblpac('+Transform(pPathDbf)+','+Transform(w_SEDE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chktblpac')
Endif
*--- Activity log
if vartype(__chktblpac_hook__)='O'
  __chktblpac_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chktblpac_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Rutine che partendo dal pacchetto di sincronizzazione esamina il DBF 
  *     CheckplanDate per controllare il corretto allineamento delle tabelle
  *     tra database di pubblicazione e ricezione
  * --- Path scompattazione file CheckPlanDate.Dbf
  * --- Sede da cui ricevo
  m.w_Ret = ""
  * --- Ciclo sulle entit� e sulle tabelle di questa per controllare la data ultima modifica struttura
  * --- Select from PUBLDETT
  i_nConn=i_TableProp[PUBLDETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PUBLDETT_idx,2],.t.,PUBLDETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select * from "+i_cTable+" PUBLDETT ";
        +" where SECODICE = "+cp_ToStrODBC(m.w_SEDE)+" And SE__TIPO='P'";
         ,"_Curs_PUBLDETT")
  else
    select * from (i_cTable);
     where SECODICE = m.w_SEDE And SE__TIPO="P";
      into cursor _Curs_PUBLDETT
  endif
  if used('_Curs_PUBLDETT')
    select _Curs_PUBLDETT
    locate for 1=1
    do while not(eof())
    m.w_ENTITA = _Curs_PUBLDETT.SEENTITA
    * --- Select from ENT_DETT
    i_nConn=i_TableProp[ENT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ENT_DETT ";
          +" where ENCODICE = "+cp_ToStrODBC(m.w_ENTITA)+"";
           ,"_Curs_ENT_DETT")
    else
      select * from (i_cTable);
       where ENCODICE = m.w_ENTITA;
        into cursor _Curs_ENT_DETT
    endif
    if used('_Curs_ENT_DETT')
      select _Curs_ENT_DETT
      locate for 1=1
      do while not(eof())
      m.w_TABLE = _Curs_ENT_DETT.EN_TABLE
      m.w_Ret = m.w_Ret + CHKTBLSTR(m.w_TABLE, m.pPathDbf, "C")
        select _Curs_ENT_DETT
        continue
      enddo
      use
    endif
      select _Curs_PUBLDETT
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_Ret
  return
endproc


  function chktblpac_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='PUBLDETT'
    i_cWorkTables[2]='ENT_DETT'
    return(cp_OpenFuncTables(2))
* --- END CHKTBLPAC
* --- START CHKTBLSTR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chktblstr                                                       *
*              Gestione controllo tabelle aggiornate                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-24                                                      *
* Last revis.: 2006-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chktblstr
param pTabella,pFileDbf,pAzione

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_OK
  m.w_OK=.f.
  private w_Result
  m.w_Result=space(254)
  private w_DateMod
  m.w_DateMod=space(14)
  private w_DMod
  m.w_DMod=space(14)
* --- WorkFile variables
  private XDC_TABLE_idx
  XDC_TABLE_idx=0
  private ENT_LOG_idx
  ENT_LOG_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "chktblstr"
if vartype(__chktblstr_hook__)='O'
  __chktblstr_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chktblstr('+Transform(pTabella)+','+Transform(pFileDbf)+','+Transform(pAzione)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chktblstr')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if chktblstr_OpenTables()
  chktblstr_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chktblstr('+Transform(pTabella)+','+Transform(pFileDbf)+','+Transform(pAzione)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chktblstr')
Endif
*--- Activity log
if vartype(__chktblstr_hook__)='O'
  __chktblstr_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chktblstr_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Controllo stuttura tabelle oggetto della sincronizzazione/pubblicazione
  *     Crea un Dbf contenete il nome logico di ogni singola tabella oggetto della pubblicazione/sincronizzazione
  *     e la data del suo ultimo aggiornamento (DATEMOD) recuperato dala tabella CPTTBLS utilizzata gi� dal Painter per 
  *     l'aggiornamento database. 
  *     In fase di sincronizzazione, se le date di ultimo aggiornamento tabelle sono differenti rispetto a quelle della tabella dalla quale
  *     sono pubblicati i dati, verr� emesso avviso e la sincronizzazione sar� interrotta
  * --- Tabella da controllare / generare il record nel DBF
  * --- File DBF da generare / controllare
  * --- pAzione: 'C' Controllo date struttura in fase di Sincronizzazione
  *                    'S' Scrittura DBF contenente le date di ultima modifica tabella
  m.w_OK = .T.
  m.w_Result = ""
   
 Local lOldError 
 lOldError= On("Error") 
 On Error m.w_OK = .F.
  i_Conn=i_TableProp[XDC_TABLE_IDX, 3]
  do case
    case m.pAzione="C"
      if File(m.pFileDbf)
         
 Use ( m.pFileDbf ) in 0 
 Select(JUSTSTEM(m.pFileDBF))
        Select DateMod from (m.pFileDbf) where filename = Alltrim(m.pTabella) into cursor CONTROLLO
        if Used( JUSTSTEM(m.pFileDBF))
          Select(JUSTSTEM(m.pFileDBF)) 
 Use
        endif
        m.w_DateMod = ""
        * --- Contollo le date di ultima modifica struttura
        if Reccount("CONTROLLO") >0
          m.w_DateMod = CONTROLLO.DateMod
          * --- Leggo la data ultima modifica struttura della tabella di questo database
          SqlExec(i_Conn, "Select DateMod from CPTTBLS where FileName="+cp_ToStrODBC(m.pTabella),"CONTROLLO1")
          if Used("CONTROLLO1") And Reccount("CONTROLLO1")>0
            m.w_DMod = CONTROLLO1.DateMod
            if m.w_DateMod>m.w_DMod
              m.w_Result = Ah_MsgFormat("Tabella %1: data ultima modifica struttura maggiore rispetto a quella di origine%0Deve essere aggiornata la struttura nel database di pubblicazione",Alltrim(m.pTabella))
            else
              if m.w_DateMod<m.w_DMod
                m.w_Result = Ah_MsgFormat("Tabella %1: data ultima modifica struttura inferiore rispetto a quella di origine%0Deve essere aggiornata la struttura in questo database",Alltrim(m.pTabella))
              endif
            endif
          endif
        endif
      else
        m.w_Result = Ah_MsgFormat("Dbf di controllo struttura tabelle non presente nel pacchetto. Impossibile continuare")
      endif
    case m.pAzione="S"
      * --- Prendo il valore dell'ultima modifica alla struttura da CPTTBLS
      SqlExec(i_Conn, "Select FileName, DateMod from CPTTBLS where FileName="+cp_ToStrODBC(m.pTabella),"CONTROLLO")
      if Used("CONTROLLO") And Reccount("CONTROLLO")>0
        m.w_DateMod = CONTROLLO.DateMod
        if File(m.pFileDbf)
          * --- Se il file c'� gi� vado in append
          Insert into (m.pFileDbf) Values(Alltrim(m.pTabella), m.w_DateMod)
        else
          * --- Se il file non c'� ancora lo creo
           
 Select("CONTROLLO") 
 Copy to (m.pFileDbf)
        endif
        if Used( JUSTSTEM ( m.pFileDbf ) )
           
 Select( JUSTSTEM ( m.pFileDbf ) ) 
 Use
        endif
      endif
  endcase
  if Used("CONTROLLO")
     
 Select("CONTROLLO") 
 Use
  endif
  if Used("CONTROLLO1")
     
 Select("CONTROLLO1") 
 Use
  endif
  if Not m.w_OK
    m.w_Result = Ah_MsgFormat("Errore nella gestione del Dbf di controllo data ultima modifica struttura per tabella %1", Alltrim(m.pTabella))
  endif
  * --- Ripristino la On Error e l'area di memoria
   
 On Error &lOldError
  i_retcode = 'stop'
  i_retval = m.w_Result
  return
endproc


  function chktblstr_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='XDC_TABLE'
    i_cWorkTables[2]='ENT_LOG'
    return(cp_OpenFuncTables(2))
* --- END CHKTBLSTR
* --- START CONNESSIONESEDE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: connessionesede                                                 *
*              Connessione a sede                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-06                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func connessionesede
param w_ACTION,w_SEDE,w_TIPO,w_VERBOSE,w_PADRE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CANALE
  m.w_CANALE=space(1)
  private w_SERASCON
  m.w_SERASCON=space(254)
  private w_SERASUSR
  m.w_SERASUSR=space(254)
  private w_SERASPWD
  m.w_SERASPWD=space(254)
  private w_SERASDOM
  m.w_SERASDOM=space(254)
  private w_SERASFTP
  m.w_SERASFTP=space(1)
  private w_SHARED
  m.w_SHARED=space(254)
  private w_SEFTPMOD
  m.w_SEFTPMOD=space(1)
  private w_RET
  m.w_RET=.f.
* --- WorkFile variables
  private PUBLMAST_idx
  PUBLMAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "connessionesede"
if vartype(__connessionesede_hook__)='O'
  __connessionesede_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'connessionesede('+Transform(w_ACTION)+','+Transform(w_SEDE)+','+Transform(w_TIPO)+','+Transform(w_VERBOSE)+','+Transform(w_PADRE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'connessionesede')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if connessionesede_OpenTables()
  connessionesede_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'connessionesede('+Transform(w_ACTION)+','+Transform(w_SEDE)+','+Transform(w_TIPO)+','+Transform(w_VERBOSE)+','+Transform(w_PADRE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'connessionesede')
Endif
*--- Activity log
if vartype(__connessionesede_hook__)='O'
  __connessionesede_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure connessionesede_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Apre/Chiude connessione canale
  m.w_RET = .T.
  * --- Read from PUBLMAST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PUBLMAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PUBLMAST_idx,2],.t.,PUBLMAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "SECANALE,SERASCON,SERASUSR,SERASPWD,SERASDOM,SERASFTP,SESHARED,SEFTPMOD"+;
      " from "+i_cTable+" PUBLMAST where ";
          +"SECODICE = "+cp_ToStrODBC(m.w_SEDE);
          +" and SE__TIPO = "+cp_ToStrODBC(m.w_TIPO);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      SECANALE,SERASCON,SERASUSR,SERASPWD,SERASDOM,SERASFTP,SESHARED,SEFTPMOD;
      from (i_cTable) where;
          SECODICE = m.w_SEDE;
          and SE__TIPO = m.w_TIPO;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CANALE = NVL(cp_ToDate(_read_.SECANALE),cp_NullValue(_read_.SECANALE))
    m.w_SERASCON = NVL(cp_ToDate(_read_.SERASCON),cp_NullValue(_read_.SERASCON))
    m.w_SERASUSR = NVL(cp_ToDate(_read_.SERASUSR),cp_NullValue(_read_.SERASUSR))
    m.w_SERASPWD = NVL(cp_ToDate(_read_.SERASPWD),cp_NullValue(_read_.SERASPWD))
    m.w_SERASDOM = NVL(cp_ToDate(_read_.SERASDOM),cp_NullValue(_read_.SERASDOM))
    m.w_SERASFTP = NVL(cp_ToDate(_read_.SERASFTP),cp_NullValue(_read_.SERASFTP))
    m.w_SHARED = NVL(cp_ToDate(_read_.SESHARED),cp_NullValue(_read_.SESHARED))
    m.w_SEFTPMOD = NVL(cp_ToDate(_read_.SEFTPMOD),cp_NullValue(_read_.SEFTPMOD))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  do case
    case m.w_ACTION = "C"
      if m.w_CANALE="R" OR (m.w_CANALE="F" AND m.w_SERASFTP="S")
        * --- RAS - Apro connessione
        if Not RASDIAL("C", m.w_SERASCON, m.w_SERASUSR, m.w_SERASPWD, m.w_SERASDOM, IIF(m.w_VERBOSE, m.w_PADRE, .F.))
          if VarType( m.w_PADRE ) ="O" 
            AddMsgNL("Impossibile connettersi a %1", m.w_PADRE, m.w_SERASCON)
          endif
          * --- Si � verificato un errore
          m.w_RET = .F.
        endif
        * --- Se OK attendo che la connessione sia effettiva
        if m.w_RET
           
 Local L_OldErr, L_bErr, L_OldArea 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T. 
 L_OldArea=Select()
           
 Local l_nTime 
 l_nTime = SECONDS()
          * --- Ciclo finch� si verifica un errore, per evitare un ciclo infinito utilizzo l_nTime
          *     Devo ciclare perch� non � detto che riesca ad effettuare la copia subito dopo
          *     la connessione RAS, non sapendo quanto tempo evo aspettare, provo pi� volte.
          L_bErr = .t.
          * --- Se si verifica un errore L_bErr � a true
          do while L_bErr AND ((SECONDS() - l_nTime) < IIF(Type("g_RASTIMEOUT")="N", g_RASTIMEOUT, 30))
            * --- Se la dir va subito bene, entro nel ciclo solo una volta
            L_bErr = .f.
            if m.w_CANALE="F"
              * --- Copio il file via FTP
              L_bErr = ! FtpClient("E", w_SEFTPHST, w_SEFTPUSR, w_SEFTPPWD, m.w_SHARED, 2, 0, , , m.w_SEFTPMOD)
            else
              L_bErr = ! Directory(m.w_SHARED)
            endif
          enddo
          m.w_RET = ! L_bErr
          * --- Rispristino la vecchia gestione errori
           
 On Error &L_OldErr 
 Select ( L_OldArea )
        endif
      endif
    case m.w_ACTION = "D"
      if m.w_CANALE="R" OR (m.w_CANALE="F" AND m.w_SERASFTP="S")
        * --- Chiudo connessione RAS
        RASDIAL("D", m.w_SERASCON, m.w_SERASUSR, m.w_SERASPWD, m.w_SERASDOM, IIF(m.w_VERBOSE, m.w_PADRE, .F.))
      endif
    otherwise
      * --- Nessuna operazione
      m.w_RET = .F.
  endcase
  * --- Ritorno lo stato
  i_retcode = 'stop'
  i_retval = m.w_RET
  return
endproc


  function connessionesede_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PUBLMAST'
    return(cp_OpenFuncTables(1))
* --- END CONNESSIONESEDE
* --- START CREAJOIN
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creajoin                                                        *
*              Crea condizione di join                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-11                                                      *
* Last revis.: 2006-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creajoin
param w_ARCHIVIO,w_ALIAS1,w_ALIAS2

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LOOP
  m.w_LOOP=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_cJOINCOND
  m.w_cJOINCOND=space(254)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creajoin"
if vartype(__creajoin_hook__)='O'
  __creajoin_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creajoin('+Transform(w_ARCHIVIO)+','+Transform(w_ALIAS1)+','+Transform(w_ALIAS2)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creajoin')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
creajoin_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creajoin('+Transform(w_ARCHIVIO)+','+Transform(w_ALIAS1)+','+Transform(w_ALIAS2)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creajoin')
Endif
*--- Activity log
if vartype(__creajoin_hook__)='O'
  __creajoin_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creajoin_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Determina la condizione di Join tra tabella adhoc e mirror
  *     Riceve archivio ad hoc e i due alias da applicare alla condizione di Join
  if Empty( m.w_ALIAS1 )
    m.w_ALIAS1 = m.w_ARCHIVIO
  endif
  if Empty( m.w_ALIAS2)
    m.w_ALIAS2 = m.w_ARCHIVIO
  endif
  m.w_LOOP = 1
  m.w_BFIRST = .T.
  * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
  =cp_ReadXdc()
  m.w_ARCHIVIO = Alltrim( m.w_ARCHIVIO )
  m.w_IDXTABLE = cp_OpenTable( m.w_ARCHIVIO ,.T.)
  m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_ARCHIVIO ,1 ) )
  m.w_cJOINCOND = ""
  do while m.w_LOOP<>0
    m.w_LOOP = At( "," , w_CHIAVE )
    if m.w_LOOP>0
      m.w_CAMPO = Left( w_CHIAVE , m.w_LOOP -1 )
    else
      m.w_CAMPO = Alltrim( w_CHIAVE )
    endif
    * --- Valutando i campi nella chiave costruisco gli la condizione di Join tra le tabelle
    *     La condizione di Join � comune ad entrambe le query in Union
    m.w_cJOINCOND = m.w_cJOINCOND +iif( Not m.w_BFIRST , " AND  ","")+ m.w_ALIAS1+"." +m.w_CAMPO+" = "+m.w_ALIAS2+"."+m.w_CAMPO
    m.w_CHIAVE = Alltrim( Substr( w_CHIAVE , m.w_LOOP+1 , Len ( w_CHIAVE ) ) )
    m.w_BFIRST = .F.
  enddo
  i_retcode = 'stop'
  i_retval = m.w_cJOINCOND
  return
endproc


* --- END CREAJOIN
* --- START CREALRTEMP
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: crealrtemp                                                      *
*              Crea temporaneo lato server                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-18                                                      *
* Last revis.: 2012-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func crealrtemp
param w_ARCHNAME,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_TEMPIDX
  m.w_TEMPIDX=0
  private w_TEMPNAME
  m.w_TEMPNAME=space(30)
  private w_LOOP
  m.w_LOOP=0
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_EL_CMDSQL
  m.w_EL_CMDSQL=space(254)
  private w_MR_TABLE
  m.w_MR_TABLE=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_DBTYPE
  m.w_DBTYPE=space(50)
  private w_NCONN
  m.w_NCONN=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_TABLE_IDX
  m.w_TABLE_IDX=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_cJOINCOND
  m.w_cJOINCOND=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_IDXTABLEEXT
  m.w_IDXTABLEEXT=0
  private w_PHNAMEEXT
  m.w_PHNAMEEXT=space(50)
  private w_EXTJOINCOND
  m.w_EXTJOINCOND=space(254)
  private w_CONTAJOIN
  m.w_CONTAJOIN=0
  private w_EXT_TABLE
  m.w_EXT_TABLE=0
  private w_MIRRORFIELD
  m.w_MIRRORFIELD=space(254)
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private TAB_ESTE_idx
  TAB_ESTE_idx=0
  private CAMPIEST_idx
  CAMPIEST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "crealrtemp"
if vartype(__crealrtemp_hook__)='O'
  __crealrtemp_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'crealrtemp('+Transform(w_ARCHNAME)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'crealrtemp')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if crealrtemp_OpenTables()
  crealrtemp_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_SUENDETT')
  use in _Curs_SUENDETT
endif
if used('_Curs_TAB_ESTE')
  use in _Curs_TAB_ESTE
endif
if used('_Curs_CAMPIEST')
  use in _Curs_CAMPIEST
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'crealrtemp('+Transform(w_ARCHNAME)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'crealrtemp')
Endif
*--- Activity log
if vartype(__crealrtemp_hook__)='O'
  __crealrtemp_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure crealrtemp_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve come parametro nome tabella. 
  *     Crea e popola il temporaneo lato server chiamato TEMP_PUBBL.
  *     Costruisce quindi una  frase SQL mettendo in Join l'archivio sul database
  *     ed il relativo archivio Mirror. Come condizione impone anche che CPCCCHK siano
  *     differenti. Il tipo di Join � Left Outer affinch� le righe inserite abbiano un CPCCCHK
  *     La query in Union determina i cancellati.
  *     Ritorna una stringa, se piena contiene il messaggio di errore
  * --- Crea una frase del tipo (es. x dettaglio documenti)
  *     
  *     select doc_dett.mvserial,doc_dett.mvnumrif,doc_dett.cprownum,doc_dett.cpccchk, 
  *     mirror.cpccchk as Pucpccchk,mirror.aversion, mirror.ancestor,mirror.PUBCOSED, mirror.cpccchk_pu
  *     from forlvdoc_dett DOC_DETT left outer JOIN mirror mirror ON
  *     mirror.MVSERIAL=DOC_DETT.MVSERIAL AND
  *     mirror.MVNUMRIF=DOC_DETT.MVNUMRIF AND
  *     mirror.CPROWNUM=DOC_DETT.CPROWNUM 
  *     where mirror.cpccchk is null or mirror.CPCCCHK<>DOC_DETT.CPCCCHK 
  *     UNION all
  *     SELECT mirror.MVSERIAL, mirror.MVNUMRIF, mirror.CPROWNUM,DOC_DETT.CPCCCHK, 
  *     mirror.cpccchk as Pucpccchk,mirror.aversion, mirror.ancestor,mirror.PUBCOSED, mirror.cpccchk_pu 
  *     FROM mirror LEFT OUTER JOIN FORLVDOC_DETT DOC_DETT ON
  *     mirror.MVSERIAL=DOC_DETT.MVSERIAL AND
  *     mirror.MVNUMRIF=DOC_DETT.MVNUMRIF AND
  *     mirror.CPROWNUM=DOC_DETT.CPROWNUM 
  *     where mirror.cpccchk is not null and doc_dett.CPCCCHK is null 
  *     
  *     Nel caso di tabelle estese va anche a recuperare i dati letti da
  *     esse se non eliminate (in caso di dato eliminato i dati li recupera
  *     dal mirror).
  m.w_RESULT = ""
  * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
  =cp_ReadXdc()
  m.w_TARCHIVIO = Alltrim( m.w_ARCHNAME )
  m.w_IDXTABLE = cp_OpenTable( m.w_TARCHIVIO ,.T.)
  if m.w_IDXTABLE=0
    m.w_RESULT = ah_msgformat("Archvio %1 non esistente, impossibile creare registrazioni variate", m.w_TARCHIVIO )
  else
    m.w_NCONN = i_TableProp[ m.w_IDXTABLE , 3 ] 
    m.w_DBTYPE = cp_GetDatabaseType( m.w_NCONN )
    if m.w_DBTYPE="Unknown"
      m.w_RESULT = ah_msgformat("Per archivio %1 non � possibile determinare il tipo di server , impossibile creare tabella registrazioni variate", m.w_TARCHIVIO )
    else
      * --- Nel temporaneo sul database gia ho i dati aggiornati come se avessi
      *     gia aggiornato il mirror...
      *     Il mirror � aggiornato alla fine, a pubblicazione avvenuta con successo...
      m.w_VALEXP = VALIDEXP( m.w_TARCHIVIO )
      m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
      * --- Devo leggere la chiave dall'analisi...
      m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )
      * --- Due indici, uno per l'elenco delle tabelle (w_IDXTABLE) ed uno per
      *     navigare all'interno dell'XDC (w_TABLE_IDX)
      m.w_TABLE_IDX = I_dcx.GetTableIdx( m.w_TARCHIVIO )
      * --- Nome Tabella Mirror
      m.w_MR_TABLE = GetMirrorName( m.w_TARCHIVIO )
      * --- Verifico se la tabella di mirror esiste...
      if Not cp_ExistTable( m.w_MR_TABLE , m.w_NCONN ,"","","")
        m.w_RESULT = ah_msgformat("Per archivio %1 non � stato creato archivio di mirror. %0 Provvedere tramite la generazione mirror. ", m.w_TARCHIVIO , , )
      else
        m.w_LOOP = 1
        m.w_BFIRST = .T.
        m.w_CMDSQL = ""
        m.w_cJOINCOND = ""
        m.w_EL_CMDSQL = " Union All Select "
        do while m.w_LOOP<>0
          m.w_LOOP = At( "," , m.w_CHIAVE )
          if m.w_LOOP>0
            m.w_CAMPO = Left( m.w_CHIAVE , m.w_LOOP -1 )
          else
            m.w_CAMPO = Alltrim( m.w_CHIAVE )
          endif
          * --- Costruisce l'elenco dei campi da passare alla routine di generazione della
          *     tabella temporanea.
          *     Due elenchi di campi, uno per gestire i dati inseriti modificati, ed uno
          *     per gestire i dati eliminati (presenti solo nel Mirror). Dati presi da alias diverso
          m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_TARCHIVIO+"." +m.w_CAMPO
          m.w_EL_CMDSQL = m.w_EL_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_MR_TABLE+"." +m.w_CAMPO
          * --- Valutando i campi nella chiave costruisco gli la condizione di Join tra le tabelle
          *     La condizione di Join � comune ad entrambe le query in Union
          m.w_cJOINCOND = m.w_cJOINCOND +iif( Not m.w_BFIRST , " AND  ","")+ m.w_TARCHIVIO+"." +m.w_CAMPO+" = "+m.w_MR_TABLE+"."+m.w_CAMPO
          m.w_CHIAVE = Alltrim( Substr( m.w_CHIAVE , m.w_LOOP+1 , Len ( m.w_CHIAVE ) ) )
          m.w_BFIRST = .F.
        enddo
        * --- Vado a ricercare eventuali campi supplementari definiti nella Super Entit�...
        * --- Select from SUENDETT
        i_nConn=i_TableProp[SUENDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[SUENDETT_idx,2],.t.,SUENDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select SU_CAMPO from "+i_cTable+" SUENDETT ";
              +" where SUCODICE="+cp_ToStrODBC(m.w_ARCHNAME)+"";
               ,"_Curs_SUENDETT")
        else
          select SU_CAMPO from (i_cTable);
           where SUCODICE=m.w_ARCHNAME;
            into cursor _Curs_SUENDETT
        endif
        if used('_Curs_SUENDETT')
          select _Curs_SUENDETT
          locate for 1=1
          do while not(eof())
          * --- Se il campo non � gia compreso lo aggiungo
          if Not m.w_TARCHIVIO+"." +Alltrim(_Curs_SUENDETT.SU_CAMPO) $ m.w_CMDSQL
            m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_TARCHIVIO+"." +Alltrim(_Curs_SUENDETT.SU_CAMPO) 
            m.w_EL_CMDSQL = m.w_EL_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_MR_TABLE+"." +Alltrim(_Curs_SUENDETT.SU_CAMPO) 
          endif
            select _Curs_SUENDETT
            continue
          enddo
          use
        endif
        * --- Aggiunge le condizioni di join per le tabelle estese
        m.w_EXTJOINCOND = ""
        m.w_CONTAJOIN = 1
        * --- Select from TAB_ESTE
        i_nConn=i_TableProp[TAB_ESTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TAB_ESTE_idx,2],.t.,TAB_ESTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_ESTE ";
              +" where ESCODTAB="+cp_ToStrODBC(m.w_TARCHIVIO)+"";
               ,"_Curs_TAB_ESTE")
        else
          select * from (i_cTable);
           where ESCODTAB=m.w_TARCHIVIO;
            into cursor _Curs_TAB_ESTE
        endif
        if used('_Curs_TAB_ESTE')
          select _Curs_TAB_ESTE
          locate for 1=1
          do while not(eof())
          m.w_EXT_TABLE = alltrim(_Curs_TAB_ESTE.ES_TABLE)
          m.w_CONTAJOIN = m.w_CONTAJOIN + 1
          m.w_IDXTABLEEXT = cp_OpenTable( m.w_EXT_TABLE ,.T.)
          m.w_PHNAMEEXT = cp_SetAzi(i_TableProp[ m.w_IDXTABLEEXT ,2]) 
          m.w_EXTJOINCOND = m.w_EXTJOINCOND + " Left Outer Join " + alltrim (m.w_PHNAMEEXT) +" " + alltrim (_Curs_TAB_ESTE.ESTABALI) +" ON " + alltrim (_Curs_TAB_ESTE.ESRELAZI)+" )"
            select _Curs_TAB_ESTE
            continue
          enddo
          use
        endif
        * --- Vado ad aggiungere i campi delle tabelle estese....
        *     Se dato presente sul database recupero dall'archivio, se 
        *     dato cancellato dal mirror...
        * --- Select from CAMPIEST
        i_nConn=i_TableProp[CAMPIEST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[CAMPIEST_idx,2],.t.,CAMPIEST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAMPIEST ";
              +" where CMCODTAB="+cp_ToStrODBC(m.w_TARCHIVIO)+"";
               ,"_Curs_CAMPIEST")
        else
          select * from (i_cTable);
           where CMCODTAB=m.w_TARCHIVIO;
            into cursor _Curs_CAMPIEST
        endif
        if used('_Curs_CAMPIEST')
          select _Curs_CAMPIEST
          locate for 1=1
          do while not(eof())
          m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ alltrim (_Curs_CAMPIEST.CMTABALI) +"."+ alltrim (_Curs_CAMPIEST.CM_CAMPO)+" As "+alltrim (_Curs_CAMPIEST.CM_ALIAS)
          m.w_EL_CMDSQL = m.w_EL_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_MR_TABLE+"." +alltrim (_Curs_CAMPIEST.CM_ALIAS)+" As "+alltrim (_Curs_CAMPIEST.CM_ALIAS)
            select _Curs_CAMPIEST
            continue
          enddo
          use
        endif
        * --- Aggiungo campi tabella Mirror (uguali in etnrambe le select in union)
        * --- Leggo i dati sul Mirror e li aggiorno al loro nuovo valore (nel temporaneo il
        *     valore che avr� sul mirror a pubblicazione avvenuta con successo).
        *     Il temporaneo � utilizzato per pubblicare e poi distrutto, solo 
        *     al termine � aggiornato anche il mirror.
        m.w_MIRRORFIELD = iif( Not m.w_BFIRST , ", "," ")+m.w_TARCHIVIO+".CPCCCHK AS PUCPCCCHK, "
        * --- L'ancestor � letto dal mirror se non sono validatore altrimenti � il CPCCCHK sull'archivio
        m.w_MIRRORFIELD = m.w_MIRRORFIELD + cp_SetSQLFunctions("[CASE2( "+ Alltrim(w_VALEXP)+" , " + cp_SetSQLFunctions("[NVL( "+m.w_TARCHIVIO+".CPCCCHK ,"+m.w_MR_TABLE +".ANCESTOR )] ", CP_DBTYPE ) +" ,"+m.w_MR_TABLE+".ANCESTOR )] ", CP_DBTYPE )+" As Ancestor,"
        * --- Stesso discorso per la versione, se sono validatore la incremento altrimenti
        *     rimane quella letta dal Mirror (  )
        *     La Versione � aggiornata solo se il CPCCCHK sul dato � diverso
        *     dal CPCCCHK di pubblicazione e sono validatore.
        *     Quando il validatore pubblica, pubblica anche per se stesso per cui
        *     va ad aggiornare sul suo database i dati di pubblicazione
        m.w_MIRRORFIELD = m.w_MIRRORFIELD +cp_SetSQLFunctions("[NVL( "+m.w_MR_TABLE +".AVERSION,0 )] ", CP_DBTYPE ) +" + "+cp_SetSQLFunctions("[CASE2( "+ Alltrim(w_VALEXP) +" And "+m.w_TARCHIVIO+".CPCCCHK<>[NVL("+m.w_MR_TABLE+".CPCCCHK_PU , "+Cp_ToStrODBC("XXXXXXXXXXX")+" )] , 1 ,0 )]  ", CP_DBTYPE )+" As AVERSION,"
        * --- Metto anche i dati dell'eventuale pubblicatore che mi ha inviato il dato...
        m.w_MIRRORFIELD = m.w_MIRRORFIELD + m.w_MR_TABLE+".PUBCOSED as PUBCOSED, "+m.w_MR_TABLE+".CPCCCHK_PU as CPCCCHK_PU "
        m.w_CMDSQL = m.w_CMDSQL + m.w_MIRRORFIELD
        * --- Idem come sopra
        m.w_EL_CMDSQL = m.w_EL_CMDSQL + m.w_MIRRORFIELD
        * --- Aggiungo la condizione di Join (a sinistra la tabella di Mirror)
        m.w_EL_CMDSQL = m.w_EL_CMDSQL +" FROM "+m.w_MR_TABLE +" left outer Join "+m.w_PHNAME+" "+m.w_TARCHIVIO+" ON " + m.w_cJOINCOND
        * --- Aggiungo la condizione di Where
        m.w_EL_CMDSQL = m.w_EL_CMDSQL + " where ("+m.w_TARCHIVIO+".CPCCCHK is Null  And (" + m.w_MR_TABLE+".DELETE_PU  is Null  OR " + m.w_MR_TABLE+".DELETE_PU=0)) " 
        * --- Ho costruito la frase dalla Union in poi ora costruisco il pezzo iniziale, dalla FROM..
        m.w_cJOINCOND = " FROM "+ REPLICATE("(",m.w_CONTAJOIN) + m.w_PHNAME+" "+m.w_TARCHIVIO +" left outer Join "+m.w_MR_TABLE+" ON " + m.w_cJOINCOND +")"+ m.w_EXTJOINCOND
        * --- Ho definito la parte della SELECT, impostato la FROM devo definire la condizione di Join
        m.w_cJOINCOND = m.w_cJOINCOND + " where ("+m.w_MR_TABLE+".CPCCCHK is null or "+m.w_MR_TABLE+".CPCCCHK<>"+m.w_TARCHIVIO+".CPCCCHK )"
        m.w_cJOINCOND = m.w_cJOINCOND + m.w_EL_CMDSQL
        * --- Tenta la creazione della tabella lato server con le chiavi variate/aggiunte/Eliminate
        m.w_TEMPIDX = cp_AddTableDef("TEMP_PUBBL")
        m.w_TEMPNAME = i_TableProp[ m.w_TEMPIDX ,2] 
        if Not cp_CreateTempTable( m.w_nConn , m.w_TEMPNAME, m.w_CMDSQL , m.w_CJOINCOND )
          m.w_RESULT = ah_msgformat("Per archivio %1 errore creazione tabella lato server. Errore %2, frase %3", m.w_TARCHIVIO , Message(), "Select "+m.w_CMDSQL+m.w_CJOINCOND )
        else
          if VarType( m.pLog ) ="O" And m.pVerbose
            AddMsgNL("Creato temporaneo %1 per super entit� %2, frase %3 ", m.pLog , m.w_TEMPNAME , m.w_ARCHNAME , "Select "+m.w_CMDSQL+m.w_CJOINCOND ,, ,)
          endif
        endif
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function crealrtemp_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='SUENDETT'
    i_cWorkTables[2]='TAB_ESTE'
    i_cWorkTables[3]='CAMPIEST'
    return(cp_OpenFuncTables(3))
* --- END CREALRTEMP
* --- START CREAPUBBL
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creapubbl                                                       *
*              Dati da pubblicare                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-18                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creapubbl
param pArr,w_SEDE,w_PADRE,w_VERBOSE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_DIR
  m.w_DIR=space(254)
  private w_LOOP
  m.w_LOOP=0
  private w_DIR_DBF
  m.w_DIR_DBF=space(254)
  private w_ULTPRO
  m.w_ULTPRO=0
  private w_CANALE
  m.w_CANALE=space(1)
  private w_SHARED
  m.w_SHARED=space(254)
  private w_MITTENTE
  m.w_MITTENTE=space(2)
  private w_SEPWDZIP
  m.w_SEPWDZIP=space(254)
  private w_SEFTPHST
  m.w_SEFTPHST=space(254)
  private w_SEFTPUSR
  m.w_SEFTPUSR=space(254)
  private w_SEFTPPWD
  m.w_SEFTPPWD=space(254)
  private w_SEFTPMOD
  m.w_SEFTPMOD=space(1)
  private w_RET
  m.w_RET=space(254)
  private w_PLPATBCK
  m.w_PLPATBCK=space(254)
  private w_ZipFile
  m.w_ZipFile=space(254)
* --- WorkFile variables
  private PUBLDETT_idx
  PUBLDETT_idx=0
  private PUBLMAST_idx
  PUBLMAST_idx=0
  private PARALORE_idx
  PARALORE_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creapubbl"
if vartype(__creapubbl_hook__)='O'
  __creapubbl_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creapubbl('+Transform(pArr)+','+Transform(w_SEDE)+','+Transform(w_PADRE)+','+Transform(w_VERBOSE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creapubbl')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if creapubbl_OpenTables()
  creapubbl_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creapubbl('+Transform(pArr)+','+Transform(w_SEDE)+','+Transform(w_PADRE)+','+Transform(w_VERBOSE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creapubbl')
Endif
*--- Activity log
if vartype(__creapubbl_hook__)='O'
  __creapubbl_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creapubbl_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Invia alle varie sedi i relativi dati
  *     Riceve un Array con:
  *     1) Nome archivio
  *     2) Posizione Archivio Visual Fox Pro con i dati
  *     3) Entita
  *     e sede alla quale inviare il pacchetto, si preoccupa di creare il pacchetto.
  *     Eventuale oggetto per messaggi a video
  * --- Scorro l'array e costruisco il pacchetto.
  *     X ORA SVOGLE UNA COPIA NELLA CARTELLA 
  *     U:\AHRU40_REMOTO\LORE\remote\<Sede>
  * --- Leggo la sede che esporta...
  * --- Read from PARALORE
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PARALORE_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PARALORE_idx,2],.t.,PARALORE_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PL__SEDE,PLPATBCK"+;
      " from "+i_cTable+" PARALORE where ";
          +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PL__SEDE,PLPATBCK;
      from (i_cTable) where;
          PLCODAZI = i_CODAZI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_MITTENTE = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
    m.w_PLPATBCK = NVL(cp_ToDate(_read_.PLPATBCK),cp_NullValue(_read_.PLPATBCK))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  m.w_MITTENTE = Trim( m.w_MITTENTE )
  * --- Leggo l'ultimo progressivo di pubblicazione verso il ricevente
  * --- Read from PUBLMAST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PUBLMAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PUBLMAST_idx,2],.t.,PUBLMAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "SEULTPRO,SECANALE,SESHARED,SEPWDZIP,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD"+;
      " from "+i_cTable+" PUBLMAST where ";
          +"SECODICE = "+cp_ToStrODBC(m.w_SEDE);
          +" and SE__TIPO = "+cp_ToStrODBC("R");
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      SEULTPRO,SECANALE,SESHARED,SEPWDZIP,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD;
      from (i_cTable) where;
          SECODICE = m.w_SEDE;
          and SE__TIPO = "R";
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_ULTPRO = NVL(cp_ToDate(_read_.SEULTPRO),cp_NullValue(_read_.SEULTPRO))
    m.w_CANALE = NVL(cp_ToDate(_read_.SECANALE),cp_NullValue(_read_.SECANALE))
    m.w_SHARED = NVL(cp_ToDate(_read_.SESHARED),cp_NullValue(_read_.SESHARED))
    m.w_SEPWDZIP = NVL(cp_ToDate(_read_.SEPWDZIP),cp_NullValue(_read_.SEPWDZIP))
    m.w_SEFTPHST = NVL(cp_ToDate(_read_.SEFTPHST),cp_NullValue(_read_.SEFTPHST))
    m.w_SEFTPUSR = NVL(cp_ToDate(_read_.SEFTPUSR),cp_NullValue(_read_.SEFTPUSR))
    m.w_SEFTPPWD = NVL(cp_ToDate(_read_.SEFTPPWD),cp_NullValue(_read_.SEFTPPWD))
    m.w_SEFTPMOD = NVL(cp_ToDate(_read_.SEFTPMOD),cp_NullValue(_read_.SEFTPMOD))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  m.w_ULTPRO = m.w_ULTPRO + 1
  m.w_SHARED = Trim( m.w_SHARED )
  m.w_DIR = TempAdHoc()+"\Remote\"+ m.w_MITTENTE +"\"+Alltrim(m.w_SEDE)+"\"+Alltrim( Str( m.w_ULTPRO ) ) +"\"
  m.w_LOOP = 1
  * --- Ordino l'array per le entit� ( terza colonna)
  =ASORT( m.pARR ,3)
   
 Local L_OldErr, L_bErr, L_OldArea 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T. 
 L_OldArea=Select()
  do while m.w_LOOP<= ALEN( m.pArr , 1)
    * --- Reset flag errore
    L_bErr = .f.
    m.w_DIR_DBF = m.w_DIR+ ALLTRIM(pArr[ m.w_LOOP , 3 ]) +"\"
    * --- Se non esiste creo la cartella temporanea
    MakeDir(m.w_DIR_DBF)
     
 Use ( pArr[ m.w_LOOP , 2 ] ) 
 Copy To ( m.w_DIR_DBF + alltrim(pArr[ m.w_LOOP , 1 ]) ) 
 Use
    if L_bErr
      m.w_LOOP = ALEN( m.pArr , 1) + 1
      if VarType( m.w_PADRE ) ="O" 
        AddMsgNL("Errore pubblicazione sede %1, entita %2, archivio %3 , errore %4", m.w_PADRE , m.w_SEDE , pArr[ m.w_LOOP ,3], pArr[ m.w_LOOP ,1] , Message()+" "+Message(1) +" " + m.w_DIR_DBF + alltrim(pArr[ m.w_LOOP , 1 ]) )
      endif
      * --- Si � verificato un errore
      m.w_RET = Message()
    else
      if VarType( m.w_PADRE ) ="O" And m.w_VERBOSE
        AddMsgNL("Pubblicazione sede %1, preparazione entit� %2", m.w_PADRE , m.w_SEDE , ALLTRIM(pArr[m.w_LOOP, 3]))
      endif
      m.w_LOOP = m.w_LOOP + 1
    endif
  enddo
  * --- Sposto il Dbf per controllo plancheckdate nella stessa cartella degli altri Dbf in modo
  *     che rientri nello Zip
  if File(Tempadhoc()+"\"+"CheckTablePlan.DBF")
    * --- Creo una sottocartella per contenere il Dbf di controllo check date
    *     Si consedera che il Dbf in questione contiene i dati relativi a tutte le tabelle oggetto di pubblicazione
    *     quindi quelle relative a tutte le sedi
    if Not Directory(m.w_DIR+"CHECKTABLEPLAN\")
      MakeDir(m.w_DIR+"CHECKTABLEPLAN\")
    endif
    Copy file Tempadhoc()+"\"+"CheckTablePlan.Dbf" To m.w_DIR+"CHECKTABLEPLAN\CheckTablePlan.Dbf"
  endif
  * --- Reset flag errore
  L_bErr = .f.
  * --- Zip della cartella temporanea
  * --- Nome del file zip
  m.w_ZipFile = TempAdHoc()+"\Remote\"+m.w_MITTENTE +"_"+Alltrim(m.w_SEDE)+"_"+Right("0000000000" + Alltrim( Str( m.w_ULTPRO ) ), 10) + ".ahz"
  * --- Comprimo la cartella
  Local L_RetMsg
  L_RetMsg = ""
  if ! ZipUnZip("Z", m.w_ZipFile, m.w_DIR, m.w_SEPWDZIP, @L_RetMsg)
    if VarType( m.w_PADRE ) ="O" 
      AddMsgNL("Errore nella creazione del file zip %1%0%2", m.w_PADRE , m.w_ZipFile, L_RetMsg)
    endif
    * --- Si � verificato un errore
    m.w_RET = L_RetMsg
  else
    if VarType( m.w_PADRE ) ="O" And m.w_VERBOSE
      AddMsgNL("Creato file zip %1", m.w_PADRE, m.w_ZipFile)
    endif
  endif
  * --- Se esiste lo zip eseguo la connessione/copia
  if FILE(m.w_ZipFile)
    * --- Se lo zip esiste cancello la cartella che ho appena compresso
    if Not DeleteFolder(TempAdhoc()+"\Remote\" + m.w_MITTENTE)
      if VarType( m.w_PADRE ) ="O" 
        AddMsgNL("Errore impossibile cancellare la cartella %1", m.w_PADRE , TempAdhoc()+"\Remote\" + m.w_MITTENTE)
      endif
      * --- Si � verificato un errore
      m.w_RET = Message()
    else
      if VarType( m.w_PADRE ) ="O" And m.w_VERBOSE
        AddMsgNL("Cancellata cartella temporanea %1", m.w_PADRE , TempAdhoc()+"\Remote\" + m.w_MITTENTE)
      endif
    endif
    if ! ConnessioneSede("C", m.w_SEDE, "R", m.w_VERBOSE, m.w_PADRE)
      * --- Errore nella connessione
      m.w_RET = Ah_MsgFormat("Errore di connessione")
    endif
    * --- Copio nella cartella di destinazione se non ci sono errori
    if Empty(m.w_RET)
      * --- Reset flag errore
      L_bErr = .f.
      if m.w_CANALE="F"
        * --- Copio il file via FTP
        L_bErr = ! FtpClient("P", m.w_SEFTPHST, m.w_SEFTPUSR, m.w_SEFTPPWD, m.w_ZipFile, m.w_SHARED+"/"+JUSTFNAME(m.w_ZipFile), 2, 0, IIF(m.w_VERBOSE, m.w_PADRE, .F.) , m.w_SEFTPMOD )
      else
        Local l_macro
        l_macro = 'COPY FILE "' + m.w_ZipFile +'" TO "'+ ADDBS(m.w_SHARED)+JUSTFNAME(m.w_ZipFile) +'"'
        &l_macro
      endif
      if L_bErr
        if VarType( m.w_PADRE ) ="O" 
          AddMsgNL("Impossibile copiare il file zip %1 in %2", m.w_PADRE, JUSTFNAME(m.w_ZipFile), IIF(Empty(m.w_SHARED), ".\", m.w_SHARED))
        endif
        * --- Si � verificato un errore
        m.w_RET = Message()
      else
        if VarType( m.w_PADRE ) ="O" And m.w_VERBOSE
          AddMsgNL("Copia del file zip %1 in %2", m.w_PADRE, JUSTFNAME(m.w_ZipFile), IIF(Empty(m.w_SHARED), ".\", m.w_SHARED))
        endif
      endif
    endif
    * --- Reset flag errore
    L_bErr = .f.
    * --- Crea copia di bakup dei pacchetti
    if NOT EMPTY (m.w_PLPATBCK) AND DIRECTORY (m.w_PLPATBCK) 
      l_macro = 'COPY FILE "' + m.w_ZipFile +'" TO "'+ ADDBS(m.w_PLPATBCK)+JUSTFNAME(m.w_ZipFile) +'"'
      &l_macro
    endif
    * --- Cancellazione del file zip
    DELETE FILE (m.w_ZipFile)
    if L_bErr
      if VarType( m.w_PADRE ) ="O" 
        AddMsgNL("Impossibile cancellare il file zip %1", m.w_PADRE, m.w_ZipFile)
      endif
      * --- Si � verificato un errore
      m.w_RET = Message()
    else
      if VarType( m.w_PADRE ) ="O" And m.w_VERBOSE
        AddMsgNL("Cancellazione file zip temporaneo %1", m.w_PADRE, JUSTFNAME(m.w_ZipFile))
      endif
    endif
    * --- Se ho aperto una connessione RAS la chiudo
    ConnessioneSede("D", m.w_SEDE, "R", m.w_VERBOSE, m.w_PADRE)
  endif
  * --- Rispristino la vecchia gestione errori
   
 On Error &L_OldErr 
 Select ( L_OldArea )
  if Empty (m.w_RET)
    * --- Tutto OK, aggiorno il progressivo
    * --- Write into PUBLMAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[PUBLMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[PUBLMAST_idx,2])
    i_ccchkf=''
    func_SetCCCHKVarsWrite(@i_ccchkf,PUBLMAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SEULTPRO = "+cp_NullLink(cp_ToStrODBC(m.w_ULTPRO),'PUBLMAST','SEULTPRO');
          +i_ccchkf ;
      +" where ";
          +"SECODICE = "+cp_ToStrODBC(m.w_SEDE);
          +" and SE__TIPO = "+cp_ToStrODBC("R");
             )
    else
      update (i_cTable) set;
          SEULTPRO = m.w_ULTPRO;
          &i_ccchkf. ;
       where;
          SECODICE = m.w_SEDE;
          and SE__TIPO = "R";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  else
    AddMsgNL("%1", m.w_PADRE, Repl("#", 50) )
    AddMsgNL("Attenzione: progressivo pacchetto per sede %1 non aggiornato per gli errori sopra indicati", m.w_PADRE, m.w_SEDE )
    AddMsgNL("%1", m.w_PADRE, Repl("#", 50) )
  endif
  * --- Ritorno l'esito della pubblicazione
  i_retcode = 'stop'
  i_retval = m.w_RET
  return
endproc


  function creapubbl_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='PUBLDETT'
    i_cWorkTables[2]='PUBLMAST'
    i_cWorkTables[3]='PARALORE'
    return(cp_OpenFuncTables(3))
* --- END CREAPUBBL
* --- START CREATBLM
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creatblm                                                        *
*              Crea tabella mirror                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2014-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creatblm
param w_ARCHNAME,w_FIRSTDROP,w_Verbose,w_Log,w_SecTable

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_LOOP
  m.w_LOOP=0
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_DBTYPE
  m.w_DBTYPE=space(50)
  private w_NCONN
  m.w_NCONN=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_TEMPC
  m.w_TEMPC=space(40)
  private w_TABLE_IDX
  m.w_TABLE_IDX=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_FIELDIDX
  m.w_FIELDIDX=0
  private w_TRUEARCNAME
  m.w_TRUEARCNAME=space(30)
  private w_TARCHNAME
  m.w_TARCHNAME=space(30)
  private w_EST_TABLE
  m.w_EST_TABLE=space(20)
  private w_EST_TABELIDX
  m.w_EST_TABELIDX=0
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private TAB_ESTE_idx
  TAB_ESTE_idx=0
  private CAMPIEST_idx
  CAMPIEST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creatblm"
if vartype(__creatblm_hook__)='O'
  __creatblm_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creatblm('+Transform(w_ARCHNAME)+','+Transform(w_FIRSTDROP)+','+Transform(w_Verbose)+','+Transform(w_Log)+','+Transform(w_SecTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatblm')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if creatblm_OpenTables()
  creatblm_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_SUENDETT')
  use in _Curs_SUENDETT
endif
if used('_Curs_TAB_ESTE')
  use in _Curs_TAB_ESTE
endif
if used('_Curs_CAMPIEST')
  use in _Curs_CAMPIEST
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creatblm('+Transform(w_ARCHNAME)+','+Transform(w_FIRSTDROP)+','+Transform(w_Verbose)+','+Transform(w_Log)+','+Transform(w_SecTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatblm')
Endif
*--- Activity log
if vartype(__creatblm_hook__)='O'
  __creatblm_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creatblm_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato il nome logico di una tabella ne genera la struttura della tabella Mirror
  *     Se va tutto OK restituisce una stringa vuota altrimenti una stringa
  *     con il messaggio di errore incontrato gia tradotto
  *     Se passato il secondo parametro a .t., se esiste la tabella prima la droppa.
  *     
  *     Oltre a creare la tabella Mirror definisce un indice sulla tabella del database
  *     con la chiave ed il CPCCCHK.
  * --- SecTable : se .T.  crea mirror per tabelle secondarie
  m.w_RESULT = ""
  * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
  =cp_ReadXdc()
  * --- Le tabelle di appoggio sono sempre legate all'azienda...
  *     Eseguo una OpenTable dell'archivio, se tabella non trovata 
  *     vado in errore altrimenti uso il risultato per contestualizzare con l'azienda
  *     il nome
  m.w_TARCHNAME = Alltrim( m.w_ARCHNAME )
  m.w_IDXTABLE = cp_OpenTable( m.w_TARCHNAME ,.T.)
  if m.w_IDXTABLE=0
    m.w_RESULT = ah_msgformat("Archvio %1 non esistente, impossibile creare tabella mirror", m.w_TARCHNAME )
  else
    m.w_NCONN = i_TableProp[ m.w_IDXTABLE , 3 ] 
    m.w_DBTYPE = cp_GetDatabaseType( m.w_NCONN )
    if m.w_DBTYPE="Unknown"
      m.w_RESULT = ah_msgformat("Per archivio %1 non � possibile determinare il tipo di server , impossibile creare tabella mirror", m.w_TARCHNAME )
    else
      * --- Verifico se esiste la tabella di mirror..
      *     Pulisco la variabile di appoggio per forzare la ricerca sul database
      if Type("i_existenttbls")<>"U"
        i_existenttbls=""
      endif
      * --- Trovo il nome della tabella di mirror nel caso di super entit� o tabella secondaria con mirror
      if m.w_SecTable
        m.w_TRUEARCNAME = GetTsmName( m.w_TARCHNAME )
      else
        m.w_TRUEARCNAME = GetMirrorName( m.w_TARCHNAME )
      endif
      if Not cp_ExistTable( m.w_TRUEARCNAME ,m.w_nConn,"","","") Or m.w_FIRSTDROP
        * --- Se esiste e decido di elimiare prima di tutto elimino
        if m.w_FIRSTDROP And cp_ExistTable( m.w_TRUEARCNAME ,m.w_nConn,"","","") 
          creatblm_Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Empty( m.w_RESULT )
          m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
          m.w_CMDSQL = "Create Table "+ m.w_TRUEARCNAME+ "( "
          * --- Devo leggere la chiave dall'analisi...
          m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHNAME ,1 ) )
          m.w_TEMPC = m.w_CHIAVE+",CPCCCHK"
          * --- Due indici, uno per l'elenco delle tabelle (w_IDXTABLE) ed uno per
          *     navigare all'interno dell'XDC (w_TABLE_IDX)
          m.w_TABLE_IDX = I_dcx.GetTableIdx( m.w_TARCHNAME )
          * --- Determino il tipo di database
          * --- La chiave deve essere separata da virgole non gestisco espressioni
          *     differenti  (tipo pnserial+str(cprownum))
          *     cp_KEYTOSQL dovrebbe garantire la trasformazione in forma
          *     pnserial,cprownum
          m.w_LOOP = 1
          m.w_BFIRST = .T.
          do while m.w_LOOP<>0
            m.w_LOOP = At( "," , m.w_CHIAVE )
            if m.w_LOOP>0
              m.w_CAMPO = Left( m.w_CHIAVE , m.w_LOOP -1 )
            else
              m.w_CAMPO = Alltrim( m.w_CHIAVE )
            endif
            * --- Dato il nome del campo, ricerco il suo indice per scrivere la frase di
            *     aggiornamento
            m.w_FIELDIDX = I_DCX.GetFieldIdxIdx( m.w_TABLE_IDX , m.w_CAMPO )
            * --- Il tipo di database deve essere quello della tabella, altrimenti
            *     non ho modo di mettere in Join questo archivio con l'archivio dei
            *     dati.
            m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_CAMPO + " "+ db_FieldType( m.w_DBTYPE , i_dcx.GetFieldType[ m.w_TARCHNAME , m.w_FIELDIDX ], i_dcx.GetFieldLen[ m.w_TARCHNAME , m.w_FIELDIDX ], i_dcx.GetFieldDec[ m.w_TARCHNAME ,m.w_FIELDIDX ]) + " NOT NULL "
            m.w_CHIAVE = Alltrim( Substr( m.w_CHIAVE , m.w_LOOP+1 , Len ( m.w_CHIAVE ) ) )
            m.w_BFIRST = .F.
          enddo
          if Not m.w_SecTable
            * --- Vado a ricercare eventuali campi supplementari definiti nella Super Entit�...
            * --- Select from SUENDETT
            i_nConn=i_TableProp[SUENDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[SUENDETT_idx,2],.t.,SUENDETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select SU_CAMPO from "+i_cTable+" SUENDETT ";
                  +" where SUCODICE="+cp_ToStrODBC(m.w_TARCHNAME)+"";
                   ,"_Curs_SUENDETT")
            else
              select SU_CAMPO from (i_cTable);
               where SUCODICE=m.w_TARCHNAME;
                into cursor _Curs_SUENDETT
            endif
            if used('_Curs_SUENDETT')
              select _Curs_SUENDETT
              locate for 1=1
              do while not(eof())
              * --- Se il campo non � gia compreso lo aggiungo
              if Not Alltrim(_Curs_SUENDETT.SU_CAMPO) $ cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHNAME ,1 ) )
                m.w_FIELDIDX = I_DCX.GetFieldIdxIdx( m.w_TABLE_IDX , Alltrim(_Curs_SUENDETT.SU_CAMPO) )
                m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+Alltrim(_Curs_SUENDETT.SU_CAMPO) + " "+ db_FieldType( m.w_DBTYPE , i_dcx.GetFieldType[ m.w_TARCHNAME , m.w_FIELDIDX ], i_dcx.GetFieldLen[ m.w_TARCHNAME , m.w_FIELDIDX ], i_dcx.GetFieldDec[ m.w_TARCHNAME ,m.w_FIELDIDX ]) 
              endif
                select _Curs_SUENDETT
                continue
              enddo
              use
            endif
            * --- Aggiungo i campi di eventuali tabelle estese....
            *     (solo per mirror "veri", quindi non secondari )
            * --- Select from TAB_ESTE
            i_nConn=i_TableProp[TAB_ESTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[TAB_ESTE_idx,2],.t.,TAB_ESTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_ESTE ";
                  +" where ESCODTAB="+cp_ToStrODBC(m.w_TARCHNAME)+"";
                   ,"_Curs_TAB_ESTE")
            else
              select * from (i_cTable);
               where ESCODTAB=m.w_TARCHNAME;
                into cursor _Curs_TAB_ESTE
            endif
            if used('_Curs_TAB_ESTE')
              select _Curs_TAB_ESTE
              locate for 1=1
              do while not(eof())
              m.w_EST_TABLE = Alltrim( _Curs_TAB_ESTE.ES_TABLE )
              m.w_EST_TABELIDX = I_dcx.GetTableIdx( m.w_EST_TABLE )
              if m.w_EST_TABELIDX>0
                * --- Select from CAMPIEST
                i_nConn=i_TableProp[CAMPIEST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[CAMPIEST_idx,2],.t.,CAMPIEST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAMPIEST ";
                      +" where CMCODTAB="+cp_ToStrODBC(m.w_TARCHNAME)+" And CM_TABLE="+cp_ToStrODBC(m.w_EST_TABLE)+" And CMTABALI="+cp_ToStrODBC(_Curs_TAB_ESTE.ESTABALI)+"";
                       ,"_Curs_CAMPIEST")
                else
                  select * from (i_cTable);
                   where CMCODTAB=m.w_TARCHNAME And CM_TABLE=m.w_EST_TABLE And CMTABALI=_Curs_TAB_ESTE.ESTABALI;
                    into cursor _Curs_CAMPIEST
                endif
                if used('_Curs_CAMPIEST')
                  select _Curs_CAMPIEST
                  locate for 1=1
                  do while not(eof())
                  * --- Ogni campo � portato all'interno del mirror con il suo alias...
                  m.w_FIELDIDX = I_DCX.GetFieldIdxIdx( m.w_EST_TABELIDX , Alltrim(_Curs_CAMPIEST.CM_CAMPO) )
                  m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+Alltrim(_Curs_CAMPIEST.CM_ALIAS) + " "+ db_FieldType( m.w_DBTYPE , i_dcx.GetFieldType[ m.w_EST_TABLE , m.w_FIELDIDX ], i_dcx.GetFieldLen[ m.w_EST_TABLE , m.w_FIELDIDX ], i_dcx.GetFieldDec[ m.w_EST_TABLE ,m.w_FIELDIDX ]) 
                    select _Curs_CAMPIEST
                    continue
                  enddo
                  use
                endif
              endif
                select _Curs_TAB_ESTE
                continue
              enddo
              use
            endif
            * --- Aggiungo l'Ancestor, CPCCCHK di ultima pubblicazione ed il codice della
            *     sede che mi ha inviato il dato
            m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+" ANCESTOR "+ db_FieldType( m.w_DBTYPE , "C", 10, 0)+", CPCCCHK "+ db_FieldType( m.w_DBTYPE , "C", 10, 0)+" NOT NULL "
            * --- Aggiungo CPCCCCHK legato all'ultima ricezione pi� il versioning
            *     (la versione del file battezzata dal validatore)
            m.w_CMDSQL = m.w_CMDSQL +", AVERSION "+ db_FieldType( m.w_DBTYPE , "N", 10, 0)+", CPCCCHK_PU "+ db_FieldType( m.w_DBTYPE , "C", 10, 0)+" , PUBCOSED "+ db_FieldType( m.w_DBTYPE , "C", 2, 0)
            * --- Aggiungo Flag DELET_PU  per gestire i cancellati in pubblicazione 
            m.w_CMDSQL = m.w_CMDSQL +", DELETE_PU "+ db_FieldType( m.w_DBTYPE , "N", 1, 0)
          else
            m.w_CMDSQL = m.w_CMDSQL +", CPCCCHK "+ db_FieldType( m.w_DBTYPE , "C", 10, 0)+" NOT NULL "
          endif
          * --- Al termine aggiungo la definizione della chiave...(come la tabella principale + il CPCCCHK)
          m.w_CMDSQL = m.w_CMDSQL + db_InlinePrimaryKey( m.w_TARCHNAME , m.w_TRUEARCNAME , m.w_TEMPC, m.w_DBTYPE )+")"
          * --- Tento l'esecuzione...
          *     Pu� fallire se utente non amministratore...
          m.w_BRES = cp_SQL( m.w_NCONN , m.w_CMDSQL, .f. , .f. ,.t. )
          if VarType( m.w_Log ) ="O" And m.w_Verbose
            if m.w_SecTable
              AddMsgNL("Creato mirror %1 per tabella secondaria %2, frase:%0%3 ", m.w_Log , m.w_TRUEARCNAME , m.w_TARCHNAME , m.w_CMDSQL ,, ,)
            else
              AddMsgNL("Creato mirror %1 per super entit� %2, frase:%0%3 ", m.w_Log , m.w_TRUEARCNAME , m.w_TARCHNAME , m.w_CMDSQL ,, ,)
            endif
          endif
          if w_BRES<0
            m.w_RESULT = ah_msgformat("Per archivio %1 errore creazione tabella lato server. Errore %2, frase:%0%3", m.w_TARCHNAME , Message(), m.w_CMDSQL )
          else
            * --- Creo la chiave primaria
            if Upper(CP_DBTYPE)="DB2" Or Upper(CP_DBTYPE)="ORACLE"
              * --- Per DB2 e Oracle la primary Key non pu� essere pi� lunga di 18 caratteri
              *     Visto che nella Cp_DbAdm viene poi aggiunto Pk_ considero 15 caratteri
              m.w_BRES = ""
              creatblm_Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              m.w_BRES = db_CreatePrimaryKey( m.w_nConn , m.w_TARCHNAME , m.w_TRUEARCNAME , m.w_TEMPC , m.w_DBTYPE )
            endif
            if w_BRES<0
              m.w_RESULT = ah_msgformat("Per archivio %1 errore creazione chiave primaria. Errore %2", m.w_TARCHNAME , Message() )
              creatblm_Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Creato correttamente l'archivio Mirror ora vado a creare un indice sulla tabella
              *     del database con i campi chiave e CPCCCHK.
              *     L'indice si chiama LR_<NOME_TABELLA>_IDX
              if m.w_FIRSTDROP
                * --- Se elimino e ricreo, tento il DROP dell'indice, non segnalo errori se va male.
                if Upper(CP_DBTYPE)="DB2" Or Upper(CP_DBTYPE)="ORACLE"
                  * --- Per Oracle e DB2 l'indice non pu� essere maggiore di 18. Taglio il nome della tabella e mantengo _IDX
                  m.w_CMDSQL = db_DropIndex( m.w_TARCHNAME , m.w_PhName , Left( m.w_TRUEARCNAME , 14) +"_IDX" , m.w_DBTYPE)
                else
                  m.w_CMDSQL = db_DropIndex( m.w_TARCHNAME , m.w_PhName , m.w_TRUEARCNAME+"_IDX" , m.w_DBTYPE)
                endif
                =sqlexec( m.w_nConn , "drop index "+ m.w_CMDSQL )
              endif
              if Upper(CP_DBTYPE)="DB2" Or Upper(CP_DBTYPE)="ORACLE"
                * --- Per Oracle e DB2 l'indice non pu� essere maggiore di 18. Taglio il nome della tabella e mantengo _IDX
                m.w_CMDSQL = db_CreateIndex( m.w_TARCHNAME , m.w_PHNAME , Left( m.w_TRUEARCNAME, 14 ) +"_IDX" , m.w_TEMPC , m.w_DBTYPE )
              else
                m.w_CMDSQL = db_CreateIndex( m.w_TARCHNAME , m.w_PHNAME , m.w_TRUEARCNAME+"_IDX" , m.w_TEMPC , m.w_DBTYPE )
              endif
              m.w_BRES = cp_SQL( m.w_nConn ,"create index "+ m.w_CMDSQL ,"",.f. , .T. )
              if w_BRES<0
                if Upper(CP_DBTYPE)="DB2" Or Upper(CP_DBTYPE)="ORACLE"
                  * --- Per oracle e Db2 la creazione dell'indice su un campo che � gi� in chiave da errore.
                  *     Segnalo l'errore ma faccio andare a buon fine
                  AddMsgNL("Database: %1 %2%0Impossibile creare indice %3%0Il mirror � stato comunque creato correttamente", m.w_Log , m.w_DBTYPE , Message(), Alltrim(m.w_CMDSQL))
                  m.w_RESULT = ""
                else
                  m.w_RESULT = ah_msgformat("Per archivio %1 errore creazione indice di confronto. Errore %2", m.w_TARCHNAME , Message() )
                  creatblm_Pag2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
      else
        m.w_RESULT = ah_msgformat("Archvio %1 gia esistente, impossibile ricreare tabella mirror senza prima eliminarla.", m.w_TRUEARCNAME )
      endif
    endif
  endif
  cp_CloseTable( m.w_TARCHNAME)
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


procedure creatblm_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  m.w_BRES = cp_SQL( m.w_NCONN ,"Drop Table "+m.w_TRUEARCNAME , .f. , .f. ,.t. )
  if VarType( m.w_Log ) ="O" And m.w_Verbose
    AddMsgNL("Cancellazione mirror %1, frase:%0%2 ", m.w_Log , m.w_TRUEARCNAME , "Drop table" +m.w_TRUEARCNAME ,, ,)
  endif
  * --- Nell'eventualit� che fallisce la DROP
  if w_BRES<0
    m.w_RESULT = m.w_RESULT +" - " +ah_msgformat("Impossibile elimare tabella %1, errore %2", m.w_TRUEARCNAME , Message(), m.w_CMDSQL )
  endif
endproc


procedure creatblm_Pag3
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Creazione della chiave primaria
  *     Devo ricostruire le funzioni db_CreatePrimaryKey e db_PrimaryKeyConstraint
  *     della cp_Dbadm perch� su Oracle e DB2 creerebbe dei Pk_... i� lunghi di 18
  *     caratteri e darebbe errore
  m.w_BRES = cp_SQL( m.w_nConn, "alter table "+m.w_TRUEARCNAME+" add constraint pk_"+Left( m.w_TRUEARCNAME, 15 )+" primary key("+m.w_TEMPC+")" , m.w_DBTYPE )
endproc


  function creatblm_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='SUENDETT'
    i_cWorkTables[2]='TAB_ESTE'
    i_cWorkTables[3]='CAMPIEST'
    return(cp_OpenFuncTables(3))
* --- END CREATBLM
* --- START CREATSMTEMP
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creatsmtemp                                                     *
*              Creazione temporaneo TSM                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-18                                                      *
* Last revis.: 2008-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creatsmtemp
param w_ARCHNAME,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_TEMPIDX
  m.w_TEMPIDX=0
  private w_TEMPNAME
  m.w_TEMPNAME=space(30)
  private w_LOOP
  m.w_LOOP=0
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_EL_CMDSQL
  m.w_EL_CMDSQL=space(254)
  private w_MR_TABLE
  m.w_MR_TABLE=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_DBTYPE
  m.w_DBTYPE=space(50)
  private w_NCONN
  m.w_NCONN=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_TABLE_IDX
  m.w_TABLE_IDX=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_cJOINCOND
  m.w_cJOINCOND=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_MIRRORFIELD
  m.w_MIRRORFIELD=space(254)
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creatsmtemp"
if vartype(__creatsmtemp_hook__)='O'
  __creatsmtemp_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creatsmtemp('+Transform(w_ARCHNAME)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatsmtemp')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if creatsmtemp_OpenTables()
  creatsmtemp_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creatsmtemp('+Transform(w_ARCHNAME)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatsmtemp')
Endif
*--- Activity log
if vartype(__creatsmtemp_hook__)='O'
  __creatsmtemp_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creatsmtemp_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve come parametro nome tabella secondaria, super entit� e link fra di loro
  *     Cicla sulle tabelle secondarie con mirror e controlla se queste sono state modificate
  *     controllando il CPCCCHK attuale e quello precedente nel loro mirror
  *     In questo caso aggiorna il CPCCCHK dello stesso record nella tabella padre
  * --- Crea una frase del tipo (es. x dettaglio documenti)
  *     
  *     select doc_dett.mvserial,doc_dett.mvnumrif,doc_dett.cprownum,doc_dett.cpccchk
  *     from demodoc_dett DOC_DETT left outer JOIN tsm_mirror mirror ON
  *     DOC_DETT.MVSERIAL = tsm_mirror.MVSERIAL AND
  *     DOC_DETT.MVNUMRIF = tsm_mirror.MVNUMRIF AND
  *     DOC_DETT.CPROWNUM = tsm_mirror.CPROWNUM
  *     where tsm_mirror.cpccchk is null or tsm_mirror.CPCCCHK<>DOC_DETT.CPCCCHK
  *     UNION all
  *     SELECT tsm_mirror.MVSERIAL, tsm_mirror.MVNUMRIF, tsm_mirror.CPROWNUM, DOC_DETT.CPCCCHK
  *     FROM tsm_mirror LEFT OUTER JOIN DEMODOC_DETT DOC_DETT ON
  *     tsm_mirror.MVSERIAL=DOC_DETT.MVSERIAL AND
  *     tsm_mirror.MVNUMRIF=DOC_DETT.MVNUMRIF AND
  *     tsm_mirror.CPROWNUM=DOC_DETT.CPROWNUM
  *     where mirror.cpccchk is not null and doc_dett.CPCCCHK is null
  *     
  *     Con questa frase creo il TEMP_TSM con tutti i record inseriti, modificati, cancellati
  m.w_RESULT = ""
  * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
  =cp_ReadXdc()
  m.w_TARCHIVIO = Alltrim( m.w_ARCHNAME )
  m.w_IDXTABLE = cp_OpenTable( m.w_TARCHIVIO ,.T.)
  * --- Costruisco il link tra la TSM e la TEMP_PUBBL della super entit� padre 
  *     partendo dal link definito nella TSM sostituendo il nome tabella padre con la
  *     TempTable creata dalla funzione CREALRTEMP
  if m.w_IDXTABLE=0
    m.w_RESULT = ah_msgformat("Archvio %1 non esistente, impossibile creare registrazioni variate", m.w_TARCHIVIO )
  else
    m.w_NCONN = i_TableProp[ m.w_IDXTABLE , 3 ] 
    m.w_DBTYPE = cp_GetDatabaseType( m.w_NCONN )
    if m.w_DBTYPE="Unknown"
      m.w_RESULT = ah_msgformat("Per archivio %1 non � possibile determinare il tipo di server , impossibile creare tabella registrazioni variate", m.w_TARCHIVIO )
    else
      * --- Nel temporaneo sul database gia ho i dati aggiornati come se avessi
      *     gia aggiornato il mirror...
      *     Il mirror � aggiornato alla fine, a pubblicazione avvenuta con successo...
      m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
      * --- Devo leggere la chiave dall'analisi...
      m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )
      * --- Due indici, uno per l'elenco delle tabelle (w_IDXTABLE) ed uno per
      *     navigare all'interno dell'XDC (w_TABLE_IDX)
      m.w_TABLE_IDX = I_dcx.GetTableIdx( m.w_TARCHIVIO )
      * --- Nome Tabella Mirror
      m.w_MR_TABLE = GetTsmName( m.w_TARCHIVIO )
      * --- Verifico se la tabella di TSM � stata definita
      if Not cp_ExistTable( m.w_MR_TABLE , m.w_NCONN ,"","","")
        m.w_RESULT = ah_msgformat("Per archivio %1 non � stato creato archivio di mirror secondario (TSM). %0 Provvedere tramite la generazione mirror. ", m.w_TARCHIVIO , , )
        i_retcode = 'stop'
        i_retval = m.w_RESULT
        return
      endif
      m.w_LOOP = 1
      m.w_BFIRST = .T.
      m.w_CMDSQL = ""
      m.w_cJOINCOND = ""
      m.w_EL_CMDSQL = " Union All Select "
      m.w_PRIMA = .T.
      do while m.w_LOOP<>0
        m.w_LOOP = At( "," , m.w_CHIAVE )
        if m.w_LOOP>0
          m.w_CAMPO = Left( m.w_CHIAVE , m.w_LOOP -1 )
        else
          m.w_CAMPO = Alltrim( m.w_CHIAVE )
        endif
        * --- Costruisce l'elenco dei campi da passare alla routine di generazione della
        *     tabella temporanea.
        *     Due elenchi di campi, uno per gestire i dati inseriti modificati, ed uno
        *     per gestire i dati eliminati (presenti solo nel Mirror). Dati presi da alias diverso
        m.w_CMDSQL = m.w_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_TARCHIVIO+"." +m.w_CAMPO
        m.w_EL_CMDSQL = m.w_EL_CMDSQL +iif( Not m.w_BFIRST , ", ","")+ m.w_MR_TABLE+"." +m.w_CAMPO
        * --- Valutando i campi nella chiave costruisco la condizione di Join tra le tabelle
        *     La condizione di Join � comune ad entrambe le query in Union
        m.w_cJOINCOND = m.w_cJOINCOND +iif( Not m.w_BFIRST , " AND  ","")+ m.w_TARCHIVIO+"." +m.w_CAMPO+" = "+m.w_MR_TABLE+"."+m.w_CAMPO
        m.w_CHIAVE = Alltrim( Substr( m.w_CHIAVE , m.w_LOOP+1 , Len ( m.w_CHIAVE ) ) )
        m.w_BFIRST = .F.
      enddo
      * --- Aggiungo campi tabella Mirror (uguali in etnrambe le select in union)
      * --- Leggo i dati sul Mirror e li aggiorno al loro nuovo valore (nel temporaneo il
      *     valore che avr� sul mirror a pubblicazione avvenuta con successo).
      *     Il temporaneo � utilizzato per pubblicare e poi distrutto, solo 
      *     al termine � aggiornato anche il mirror.
      m.w_MIRRORFIELD = iif( Not m.w_BFIRST , ", "," ")+m.w_TARCHIVIO+".CPCCCHK AS CPCCCHK "
      * --- Aggiungo il CPCCCHK
      m.w_CMDSQL = m.w_CMDSQL + m.w_MIRRORFIELD
      * --- Idem come sopra
      m.w_EL_CMDSQL = m.w_EL_CMDSQL + m.w_MIRRORFIELD
      * --- Aggiungo la condizione di Join (a sinistra la tabella di Mirror)
      m.w_EL_CMDSQL = m.w_EL_CMDSQL +" FROM "+m.w_MR_TABLE +" left outer Join "+m.w_PHNAME+" "+m.w_TARCHIVIO+" ON " + m.w_cJOINCOND
      * --- Aggiungo la condizione di Where
      m.w_EL_CMDSQL = m.w_EL_CMDSQL + " where ("+m.w_MR_TABLE+".CPCCCHK is not null And "+m.w_TARCHIVIO+".CPCCCHK is Null )"
      * --- Ho costruito la frase dalla Union in poi ora costruisco il pezzo iniziale, dalla FROM..
      m.w_cJOINCOND = " FROM "+m.w_PHNAME+" "+m.w_TARCHIVIO +" left outer Join "+m.w_MR_TABLE+" ON " + m.w_cJOINCOND
      * --- Ho definito la parte della SELECT, impostato la FROM devo definire la condizione di Join
      m.w_cJOINCOND = m.w_cJOINCOND + " where ("+m.w_MR_TABLE+".CPCCCHK is null or "+m.w_MR_TABLE+".CPCCCHK<>"+m.w_TARCHIVIO+".CPCCCHK )"
      m.w_cJOINCOND = m.w_cJOINCOND + m.w_EL_CMDSQL
      * --- Tenta la creazione della tabella lato server con le chiavi variate/aggiunte/Eliminate
      m.w_TEMPIDX = cp_AddTableDef("TEMP_TSM")
      m.w_TEMPNAME = i_TableProp[ m.w_TEMPIDX ,2] 
      if Not cp_CreateTempTable( m.w_nConn , m.w_TEMPNAME, m.w_CMDSQL , m.w_CJOINCOND )
        m.w_RESULT = ah_msgformat("Per archivio %1 errore creazione tabella lato server. Errore %2, frase %3", m.w_TARCHIVIO , Message(), "Select "+m.w_CMDSQL+m.w_CJOINCOND )
      else
        if VarType( m.pLog ) ="O" And m.pVerbose
          AddMsgNL("Creato temporaneo %1 per tabella secondaria %2, frase %3 ", m.pLog , m.w_TEMPNAME , m.w_ARCHNAME , "Select "+m.w_CMDSQL+m.w_CJOINCOND ,, ,)
        endif
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function creatsmtemp_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='SUENDETT'
    return(cp_OpenFuncTables(1))
* --- END CREATSMTEMP
* --- START DEFSTRUC
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: defstruc                                                        *
*              Struttura aggiornamento                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func defstruc
param pSede,pEntita,pTabella,ArrVal,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LOOP
  m.w_LOOP=0
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_VERTRES
  m.w_VERTRES=0
  private w_ERRMESS
  m.w_ERRMESS=space(254)
  private w_VALORE
  m.w_VALORE=space(254)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "defstruc"
if vartype(__defstruc_hook__)='O'
  __defstruc_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'defstruc('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pTabella)+','+Transform(ArrVal)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'defstruc')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
defstruc_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'defstruc('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pTabella)+','+Transform(ArrVal)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'defstruc')
Endif
*--- Activity log
if vartype(__defstruc_hook__)='O'
  __defstruc_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure defstruc_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dati:
  *     Sede
  *     Entita
  *     Tabella
  *     Array (x riferimento)
  *     popola l'array con l'elenco dei campi come prima colonna,
  *     nella seconda colonna mette eventuali valori fissi
  *     presenti nei filtri verticali infine una terza colonna
  *     con 
  *     0 = nessun filtro verticale
  *     1 = filtro verticale di tipo fisso
  *     2 = filtro verticale del tipo mantieni valore
  *     
  *     restituisce '' se tutto ok altrimenti il messaggio di
  *     errore
  * --- Costruisco array con elenco campi
  m.w_LOOP = 1
  do while m.w_LOOP<= i_Dcx.GetFieldsCount( m.pTabella )
    m.w_CAMPO = Alltrim( i_Dcx.GetFieldName( m.pTABELLA , m.w_LOOP ) ) 
     
 Dimension ArrVal[ m.w_LOOP ,3][ 
 ArrVal[ m.w_LOOP ,1 ] = m.w_CAMPO
    m.w_VALORE = ""
    m.w_ERRMESS = ""
    m.w_VERTRES = VERTFILT( m.pSEDE , "P" , m.pENTITA , m.pTabella , m.w_CAMPO , @m.w_VALORE , @m.w_ERRMESS ) 
    if m.w_VERTRES=-1
      * --- L'applicazione del filtro verticale � andata in errore, segnalo ed esco
      i_retcode = 'stop'
      i_retval = m.w_ERRMESS
      return
    else
      if m.w_VERTRES=1
        * --- Ho trovato un filtro verticale lo vado ad applicare 
        if VarType( m.pLog ) ="O" And m.pVerbose
          AddMsgNL("Sincronizzazione per entita %1 per l'archivio secondario %2 applicato filtro verticale campo %3", m.pLog , m.pENTITA , m.pTABELLA , m.w_CAMPO ,, ,)
        endif
        * --- Ho trovato il valore, lo memorizzo e marco il fatto che questo valore
        *     non va letto dal DBF
         
 ArrVal[ m.w_LOOP ,2 ] = m.w_VALORE
      endif
      * --- Questo terzo valore mi dice come gestire il campo (mantengo o meno il valore sul DB)
      *     0 leggo dal file
      *     1 valore fisso
      *     2 mantengo il valore sul DB
      if m.w_VERTRES=3
        ArrVal[ m.w_LOOP ,2 ] = strtran ( alltrim(m.w_VALORE),"(", "( w_DATIRIGA , pLog, pVerbose , ") 
      endif
      ArrVal[ m.w_LOOP ,3 ] = m.w_VERTRES
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  * --- Metto il CPCCCHK finale
   
 Dimension ArrVal[ m.w_LOOP ,3 ] 
 ArrVal[ m.w_LOOP ,1 ] = "CPCCCHK" 
 ArrVal[ m.w_LOOP ,3 ] = 0
  i_retcode = 'stop'
  i_retval = m.w_ERRMESS
  return
endproc


* --- END DEFSTRUC
* --- START ENTTODBF
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: enttodbf                                                        *
*              Crea DBF per archivio                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-18                                                      *
* Last revis.: 2006-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func enttodbf
param w_SEDE,w_ENTITA,pArrResult,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_ROTTURA
  m.w_ROTTURA=space(254)
  private w_MAIN_DBF
  m.w_MAIN_DBF=space(10)
  private w_RESULT
  m.w_RESULT=space(254)
  private w_ARINDEX
  m.w_ARINDEX=0
* --- WorkFile variables
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "enttodbf"
if vartype(__enttodbf_hook__)='O'
  __enttodbf_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'enttodbf('+Transform(w_SEDE)+','+Transform(w_ENTITA)+','+Transform(pArrResult)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'enttodbf')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if enttodbf_OpenTables()
  enttodbf_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_ENT_DETT')
  use in _Curs_ENT_DETT
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'enttodbf('+Transform(w_SEDE)+','+Transform(w_ENTITA)+','+Transform(pArrResult)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'enttodbf')
Endif
*--- Activity log
if vartype(__enttodbf_hook__)='O'
  __enttodbf_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure enttodbf_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Data una sede ed un'entit� determina i dati da pubblicare che saranno
  *     trasformati in file DBF fisici. La funzione restituisce quindi un Array con 
  *     l'elenco delle tabelle ed il relativo DBF
  *     pLog contiene l'oggetto sul quale visualizzare il log delle operazioni
  * --- Devo recuperare tutte le informazioni relative all'entit�, quindi
  *     elenco archivi, filtri, ect. ect.
  m.w_SEDE = Alltrim(m.w_SEDE)
  m.w_ENTITA = Alltrim( m.w_ENTITA )
  * --- Al primo giro l'array � vuoto...
  if Empty ( m.pArrREsult(1 , 1 ) )
    m.w_ARINDEX = 0
  else
    m.w_ARINDEX = Alen( m.pArrREsult , 1 )
  endif
  * --- Select from ENT_DETT
  i_nConn=i_TableProp[ENT_DETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select EN_TABLE,EN__LINK,ENFILTRO,ENPRINCI from "+i_cTable+" ENT_DETT ";
        +" where ENCODICE= "+cp_ToStrODBC(m.w_ENTITA)+"";
        +" order by CPROWORD";
         ,"_Curs_ENT_DETT")
  else
    select EN_TABLE,EN__LINK,ENFILTRO,ENPRINCI from (i_cTable);
     where ENCODICE= m.w_ENTITA;
     order by CPROWORD;
      into cursor _Curs_ENT_DETT
  endif
  if used('_Curs_ENT_DETT')
    select _Curs_ENT_DETT
    locate for 1=1
    do while not(eof())
    * --- Archivi ordinati per CPROWORD, per costruzione il primo � l'archivio principale
    * --- Se archivio principale creo un DBF da utilizzare per gestire archivio per
    *     archivio i dati collegati...
    m.w_MAIN_DBF = Sys(2015)
    if VarType( m.pLog )="O"
      AddMsgNL("Determinazione dati entita %1 tabella %2",m.pLog, m.w_ENTITA , _Curs_ENT_DETT.EN_TABLE , , ,, )
    endif
    m.w_RESULT = ARCTODBF( m.w_ENTITA , _Curs_ENT_DETT.EN_TABLE, m.w_SEDE , m.w_MAIN_DBF , m.pLog , m.pVerbose ) 
    if Not Empty( m.w_RESULT )
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    else
      * --- Creo il Dbf per controllo struttura tabella da CPTTBLS
      m.w_RESULT = CHKTBLSTR(_Curs_ENT_DETT.EN_TABLE, Tempadhoc()+"\"+"CheckTablePlan.DBF", "S")
      if Not Empty(m.w_Result)
        i_retcode = 'stop'
        i_retval = m.w_RESULT
        return
      else
        * --- Salvo la tabella nella Temp...
         
 Local L_OldArea 
 L_OldArea=Select() 
 Select ( m.w_MAIN_DBF ) 
 Copy To Tempadhoc()+"\"+m.w_MAIN_DBF 
 Use 
 Select ( L_OldArea )
        * --- Memorizzo nell'array il risultato...
        m.w_ARINDEX = m.w_ARINDEX + 1
         
 Dimension pArrResult[ m.w_ARINDEX , 4 ] 
 pArrResult[ m.w_ARINDEX , 1 ] = _Curs_ENT_DETT.EN_TABLE 
 pArrResult[ m.w_ARINDEX , 2 ] = Tempadhoc()+"\"+m.w_MAIN_DBF 
 pArrResult[ m.w_ARINDEX , 3 ] = m.w_SEDE 
 pArrResult[ m.w_ARINDEX , 4 ] = m.w_ENTITA
      endif
    endif
      select _Curs_ENT_DETT
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function enttodbf_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ENT_DETT'
    return(cp_OpenFuncTables(1))
* --- END ENTTODBF
* --- START ERASETMP
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: erasetmp                                                        *
*              Elimina temporanei vfp                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-08                                                      *
* Last revis.: 2006-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func erasetmp
param pArr

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LOOP
  m.w_LOOP=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "erasetmp"
if vartype(__erasetmp_hook__)='O'
  __erasetmp_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'erasetmp('+Transform(pArr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'erasetmp')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
erasetmp_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'erasetmp('+Transform(pArr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'erasetmp')
Endif
*--- Activity log
if vartype(__erasetmp_hook__)='O'
  __erasetmp_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure erasetmp_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve elenco temporanei VFP creati per la pubblicazione
  *     e li va ad eliminare
  m.w_LOOP = 1
   
 Local L_OldErr, L_bErr 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T.
  do while m.w_LOOP<= ALEN( m.pArr , 1)
    if Not Empty( pArr[ m.w_LOOP , 2 ] ) And File( pArr[ m.w_LOOP , 2 ]+".DBF" ,1 )
      DROP TABLE ( pArr[ m.w_LOOP , 2 ] )
    endif
    if L_bErr
      m.w_LOOP =  ALEN( m.pArr , 1) + 1
    else
      m.w_LOOP = m.w_LOOP + 1
    endif
  enddo
  * --- Rispristino la vecchia gestione errori
  On Error &L_OldErr
  if L_bErr
    * --- Se qualcosa va storto segnalo...
    i_retcode = 'stop'
    i_retval = Message()
    return
  else
    i_retcode = 'stop'
    i_retval = ""
    return
  endif
endproc


* --- END ERASETMP
* --- START GENDBLOG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gendblog                                                        *
*              Genera DBF di salvataggio dati                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-05                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func gendblog
param pSede,pEntita,pArr,pArchivio,w_CHIAVE,pNumLog,pArrDBF,pLog,w_SERLOG

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_OK
  m.w_OK=.f.
  private w_PRINCI
  m.w_PRINCI=.f.
  private w_PATERR
  m.w_PATERR=space(254)
  private w_Log
  m.w_Log=space(254)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_DBF
  m.w_DBF=space(254)
  private w_ArcName
  m.w_ArcName=space(30)
  private w_Link
  m.w_Link=space(254)
  private w_LINKDEL
  m.w_LINKDEL=space(254)
  private w_PATFILE
  m.w_PATFILE=space(254)
  private w_CURDB
  m.w_CURDB=space(10)
  private w_CHIAVE_NAME
  m.w_CHIAVE_NAME=space(254)
  private w_LOOP
  m.w_LOOP=0
  private w_Ret
  m.w_Ret=space(254)
  private w_DBFERR
  m.w_DBFERR=space(1)
  private w_CURSORE
  m.w_CURSORE=space(10)
  private w_oPART
  m.w_oPART = .null.
  private w_oMess
  m.w_oMess = .null.
* --- WorkFile variables
  private PARALORE_idx
  PARALORE_idx=0
  private ENT_LOG_idx
  ENT_LOG_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "gendblog"
if vartype(__gendblog_hook__)='O'
  __gendblog_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'gendblog('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pArr)+','+Transform(pArchivio)+','+Transform(w_CHIAVE)+','+Transform(pNumLog)+','+Transform(pArrDBF)+','+Transform(pLog)+','+Transform(w_SERLOG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gendblog')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if gendblog_OpenTables()
  gendblog_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_GENDBLOG')
  use in _Curs_GENDBLOG
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'gendblog('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pArr)+','+Transform(pArchivio)+','+Transform(w_CHIAVE)+','+Transform(pNumLog)+','+Transform(pArrDBF)+','+Transform(pLog)+','+Transform(w_SERLOG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gendblog')
Endif
*--- Activity log
if vartype(__gendblog_hook__)='O'
  __gendblog_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure gendblog_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Salvataggio dati per errore di log
  *     Nel caso di sovrascrittura dati, vengono salvati i dati preesistenti, quindi quelli della tabella
  *     Nel caso di errore per dati inconsistenti, vengono presi i dati dai DBF con dati errati e salvati, sempre come DBF,
  *     in un'altra cartella
  *     
  *     I DBF di salvataggio dati saranno spostati in una cartella con il nome dell'entit� e avranno il nome della chiave del record salvato
  *     pi� il nome della tabella che li riguarda 
  *     Es.: documenti\0000000001_DOC_MAST.DBF
  * --- Arra con chiave entit� errata
  * --- pArchivio: tabella che d� errore
  * --- Chiave archivio errato
  * --- Codifica dell'errore
  *     Se 7, sovrascrittura dati, salva i dati preesistenti su un DBF
  *     Altrimenti significa che i dati dei DBF pubblicati dall'altra sede non sono validi 
  *     In questo caso salvo i record non validi dei DBF in altri DBF
  * --- Array contenente le informazioni che mi servono per salvare i dati in DBF di appoggio
  *     o mantenere i dati originali prima di sovrascriverli
  * --- Messaggio di errore da visualizzare. Se non passato e passato pNumLog sono presenti
  *     una serie di errori codificati
  * --- Seriale della gestione log da aggiornare
  m.w_OK = .T.
  Local L_OldArea
  * --- Leggo la modalit� di memorizzazione DBF di log
  * --- Read from PARALORE
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PARALORE_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PARALORE_idx,2],.t.,PARALORE_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PLDBFERR,PLPATERR"+;
      " from "+i_cTable+" PARALORE where ";
          +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PLDBFERR,PLPATERR;
      from (i_cTable) where;
          PLCODAZI = i_CODAZI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_DBFERR = NVL(cp_ToDate(_read_.PLDBFERR),cp_NullValue(_read_.PLDBFERR))
    m.w_PATERR = NVL(cp_ToDate(_read_.PLPATERR),cp_NullValue(_read_.PLPATERR))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
   
 Local lOldError 
 lOldError= On("Error") 
 On Error m.w_OK = .F.
  w_oMess=createobject("Ah_Message")
  if Not Empty(m.w_PATERR)
    * --- La cartella di gestione log deve esistere
    if Not Directory(m.w_PATERR)
      w_oMess.AddMsgPartNL("Cartella di memorizzazione DBF di log inesistente%0Impossibile generare log")
    else
      * --- costuisco subito messaggio di errore per includerlo nel DBF
      if Empty(m.pLog)
        do case
          case m.pNumLog=1
            w_oMess.AddMsgPartNL("Record da inserire precedentemente eliminato. Impossibile proseguire")
          case m.pNumLog=2
            w_oMess.AddMsgPartNL("Record da modificare/inserire non proveniente da validatore o mai validato. Impossibile proseguire")
          case m.pNumLog=3
            w_oMess.AddMsgPartNL("Record da modificare/inserire con versione non conforme a ultima validata. Impossibile proseguire")
          case m.pNumLog=4
            w_oMess.AddMsgPartNL("Record da modificare/inserire modificato localmente rispetto a ultima pubblicazione e attuale ricezione non validata. Impossibile proseguire")
          case m.pNumLog=5
            w_oMess.AddMsgPartNL("Record da modificare/inserire modificato localmente rispetto a ultima ricezione ed attuale ricezione non validata. Impossibile proseguire")
          case m.pNumLog=6
            w_oMess.AddMsgPartNL("Record da inserire con versione antecedente all'attuale. Impossibile proseguire")
          case m.pNumLog=7
            w_oMess.AddMsgPartNL("Record sovrascritto poich� ne � stata ricevuta una nuova versione dalla sede validatrice%0La sovrascrittura ha causato la perdita delle modifiche apportate localmente")
          case m.pNumLog=8
          case m.pNumLog=9
          case m.pNumLog=10
        endcase
      else
        * --- Se gli ho passato l'errore lo ripasso indietro
        m.w_oPART = m.w_oMess.AddMsgPartNL("%1")
        w_oPART.addParam(m.pLog)
      endif
      m.w_Log = m.w_oMess.ComposeMessage(.F.)
      if m.pNumLog = 7
        * --- Sovrascrittura: devo leggere i dati dalla tabella che stanno per essere sovrascritti
        m.w_CURDB = READTABLE( Alltrim(m.pArchivio) , "*" , @m.pArr , .F., .F. )
      else
        m.w_CURDB = SYS(2015)
        * --- Ricerca per entit� ed archivio il corrispondente DBF
        * --- Trovo il DBF
        m.w_DBF = LOOK_DBF( m.pEntita, m.pArchivio , @m.pArrDBF )
        * --- Scorro l'elenco dei campi ed i rispettivi valori...
        m.w_CMDSQL = "Select * From "+m.w_DBF+"  Where "
        m.w_LOOP = 1
        * --- Creo il filtro di Where con le chiavi che mi servono
        do while m.w_LOOP<=Alen( m.pArr , 1)
          m.w_CMDSQL = m.w_CMDSQL +iif( m.w_LOOP>1 , " AND  ","")+ pArr[ m.w_LOOP ,1 ]+" = "+Cp_ToStr( pArr[ m.w_LOOP ,2 ] )
          m.w_LOOP = m.w_LOOP + 1
        enddo
        * --- Estrappolo il cursore dal DBF 
        *     Il Dbf � lo stesso sul quale la funzione sincronizza sta effettuando la scansione 
        *     quindi non deve essere chiuso
         
 Local Comando 
 Comando = m.w_CMDSQL + " into cursor " + (m.w_CURDB) 
 &Comando
      endif
      if Not Empty(m.w_CURDB)
        if m.w_DBFERR="T"
          * --- Per tabella
          m.w_CHIAVE_NAME = Alltrim(m.pArchivio)
        else
          * --- Per record
          m.w_CHIAVE_NAME = Alltrim(m.w_CHIAVE)+"_"+Alltrim(m.pArchivio)
        endif
        * --- Indico che creo il DBF per tabella principale
        m.w_PRINCI = .T.
        gendblog_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Select from GENDBLOG
        do vq_exec with 'GENDBLOG',createobject('cp_getfuncvar'),'_Curs_GENDBLOG','',.f.,.t.
        if used('_Curs_GENDBLOG')
          select _Curs_GENDBLOG
          locate for 1=1
          do while not(eof())
          * --- Scorro le tabelle collegate e salvo i dati
          if Not Empty(Nvl(_Curs_GENDBLOG.EN_TABLE,"")) And Not Empty(Nvl(_Curs_GENDBLOG.EN__LINK,""))
            m.w_ArcName = Alltrim(_Curs_GENDBLOG.EN_TABLE)
            m.w_Link = Nvl(_Curs_GENDBLOG.EN__LINK,"")
            m.w_LOOP = 1
            * --- Creo il filtro di Where per selezionare i record dalla tabella collegata.
            *     Es.: MVSERIAL = '0000000001' dove MVSERIAL � la chiave della tabella collegata DOC_DETT
            *     Il filtro lo creo in base al Link specificato nelle Entit�
            do while m.w_LOOP<=Alen( m.pArr , 1)
              if m.pNumLog = 7
                * --- Su tabella
                m.w_LINK = StrTran( m.w_LINK , Alltrim(m.pArchivio)+"."+pArr[ m.w_LOOP , 1 ] , Cp_ToStrOdbc( pArr[ m.w_LOOP , 2 ] ) )
              else
                * --- Su DBF
                m.w_LINK = StrTran( m.w_LINK , Alltrim(m.pArchivio)+"."+pArr[ m.w_LOOP , 1 ] , Cp_ToStr( pArr[ m.w_LOOP , 2 ] ) )
              endif
              m.w_LOOP = m.w_LOOP + 1
            enddo
            * --- Elimino l'alias della tabella per la condizione 
            *     Where MTSERIAL = '0000000001' And MTNUMRIF=-20
            m.w_LINKDEL = StrTran( m.w_LINK , m.w_ArcName+"." , "" )
            * --- Creo il cursore con i dati relativi ai record
            if m.pNumLog = 7
              * --- Sovrascrittura: devo leggere i dati dalla tabella che stanno per essere sovrascritti
              m.w_CURDB = READTABLE( m.w_ArcName , "*" , @m.pArr , .F., .F., m.w_LINKDEL )
            else
              * --- Ricerca per entit� ed archivio il corrispondente DBF
              * --- Trovo il DBF
              m.w_DBF = LOOK_DBF( m.pEntita, m.w_ArcName , @m.pArrDBF )
              if Not Empty( m.w_DBF )
                m.w_CURDB = SYS(2015)
                * --- Scorro l'elenco dei campi ed i rispettivi valori...
                m.w_CMDSQL = "Select * From "+m.w_DBF+"  Where "+ Alltrim(m.w_LINKDEL)
                * --- Estrappolo il cursore dal DBF 
                 
 Local Comando 
 Comando = m.w_CMDSQL + " into cursor " + (m.w_CURDB) 
 &Comando 
 
 Select( JUSTSTEM(m.w_DBF) ) 
 Use
              else
                * --- Nel paccheto non ho il dato cercato, non posso produrre il dato non ricevuto
                *     per questo archivio
                m.w_CURDB = ""
              endif
            endif
            if Not Empty( m.w_CURDB )
              if m.w_DBFERR="T"
                * --- Per tabella
                m.w_CHIAVE_NAME = Alltrim(m.w_ArcName)
              else
                * --- Per record
                m.w_CHIAVE_NAME = Alltrim(m.w_CHIAVE)+"_"+Alltrim(m.w_ArcName)
              endif
              * --- Indico che creo il DBF per tabelle collegate
              m.w_PRINCI = .F.
              gendblog_Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
            select _Curs_GENDBLOG
            continue
          enddo
          use
        endif
      else
        m.w_OK = .F.
      endif
    endif
  else
    w_oMess.AddMsgPartNL("Gestione log e salvataggio dati disattivata")
  endif
  if Used("LOG_ERR")
    * --- Cerco il record nel cursore del Log con la stessa chiave
     
 L_OldArea= Select() 
 Select("LOG_ERR") 
 Locate For DSSERIAL = m.w_SERLOG And DS__SEDE = m.pSede And DEENTITA = m.pEntita
    if Found()
      * --- Trovato il record quindi modifico
      if m.pNumLog = 7
        * --- Metto +1 nel campo record sovrascritti nel dettaglio log entit�
         
 Replace DENUMOVW with DENUMOVW+1
      else
        * --- Negli altri casi sono errori
         
 Replace DENUMRIF with DENUMRIF+1
      endif
    else
      * --- altrimenti inserisco il record
      if m.pNumLog=7
        * --- Metto 1 nel campo record sovrascritti nel dettaglio log entit�
        INSERT INTO LOG_ERR Values ("B", m.w_SERLOG, m.pSede, 0, m.pEntita, ; 
 0, 0, 0, 0, 1, "", "")
      else
        * --- Negli altri casi sono errori
        INSERT INTO LOG_ERR Values ("B", m.w_SERLOG, m.pSede, 0, m.pEntita, ; 
 0, 0, 0, 1, 0, "", "")
      endif
    endif
    Select(L_OldArea)
  else
    m.w_OK = .F.
  endif
  if Not m.w_OK
    if m.pNumLog=7
      w_oMess.AddMsgPartNL("Impossibile generare DBF di salvataggio dati preesistenti")
    else
      w_oMess.AddMsgPartNL("Impossibile generare DBF dei dati errati")
    endif
  endif
  m.w_Ret = m.w_oMess.ComposeMessage()
  * --- Ripristino la On Error
  On Error &lOldError
  i_retcode = 'stop'
  i_retval = m.w_Ret
  return
endproc


procedure gendblog_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Salvataggio dati preesistenti
  * --- Chiave record: per salvataggio dati. D� il nome al DBF
  m.w_PATFILE = CALPATHLOG( Alltrim(m.pSede), Alltrim(m.pEntita), Alltrim(m.w_CHIAVE_NAME), IIF(m.pNumLog=7, "S", "E" ) )
  * --- Creo il file DBF di salvataggio dati preesistenti
  if Not Empty(m.w_PatFile)
    if m.w_PRINCI
      * --- Cursore di appoggio per aggiungere campo con errore
      m.w_CURSORE = Sys(2015)
      * --- Elimino dal messaggio gli invii
      m.w_Log = StrTran(m.w_Log, CHR(13)," ")
      m.w_Log = StrTran(m.w_Log, CHR(10),"")
      if File(m.w_PatFile)
        * --- Se il file c'� gi� vado in append
        Insert into (m.w_PATFILE) Select m.w_Log, * from (m.w_CURDB) 
      else
        * --- Se il file non c'� ancora lo creo
        *     (Cast se il messagio supera i 254 caratteri...)
         
 Select( m.w_CURDB ) 
 Select Cast(m.w_Log As Memo ) As Z_ERR_LOG, * From (m.w_CURDB) into Cursor (m.w_CURSORE)
        if m.w_OK
           
 Select(m.w_CURSORE) 
 Copy to (m.w_PATFILE)
        endif
      endif
      if Used(m.w_CURSORE)
         
 Select(m.w_CURSORE) 
 Use
      endif
    else
      if File(m.w_PatFile)
        * --- Se il file c'� gi� vado in append
        Insert into (m.w_PATFILE) Select * from (m.w_CURDB) 
      else
        * --- Se il file non c'� ancora lo creo
        if m.w_OK
           
 Select( m.w_CURDB ) 
 Copy to (m.w_PATFILE)
        endif
      endif
    endif
     
 Select( m.w_CURDB ) 
 Use
    if Used( JUSTSTEM ( m.w_PATFILE ) )
       
 Select( JUSTSTEM ( m.w_PATFILE ) ) 
 Use
    endif
  endif
endproc


  function gendblog_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='PARALORE'
    i_cWorkTables[2]='ENT_LOG'
    return(cp_OpenFuncTables(2))
* --- END GENDBLOG
* --- START GEST_LOG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gest_log                                                        *
*              Gestione log                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-29                                                      *
* Last revis.: 2006-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func gest_log
param pSede,pEntita,pArr,pLog,pNumLog,pArchivio,pArrDBF,w_SERLOG,ObjLog

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_MESS
  m.w_MESS=space(100)
  private w_LOOP
  m.w_LOOP=0
  private w_CURDB
  m.w_CURDB=space(10)
  private w_PATFILE
  m.w_PATFILE=space(254)
  private w_CHIAVE
  m.w_CHIAVE=space(254)
  private w_CHIAVE_NAME
  m.w_CHIAVE_NAME=space(254)
  private w_DBF
  m.w_DBF=space(254)
  private w_oMess
  m.w_oMess = .null.
  private w_oPart
  m.w_oPart = .null.
* --- WorkFile variables
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "gest_log"
if vartype(__gest_log_hook__)='O'
  __gest_log_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'gest_log('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pArr)+','+Transform(pLog)+','+Transform(pNumLog)+','+Transform(pArchivio)+','+Transform(pArrDBF)+','+Transform(w_SERLOG)+','+Transform(ObjLog)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gest_log')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if gest_log_OpenTables()
  gest_log_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'gest_log('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pArr)+','+Transform(pLog)+','+Transform(pNumLog)+','+Transform(pArchivio)+','+Transform(pArrDBF)+','+Transform(w_SERLOG)+','+Transform(ObjLog)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gest_log')
Endif
*--- Activity log
if vartype(__gest_log_hook__)='O'
  __gest_log_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure gest_log_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve:
  *     Sede
  *     Entita
  *     Array con chiave entita
  *     MSg di errore
  * --- Messaggio di errore da visualizzare. Se non passato e passato pNumLog sono presenti
  *     una serie di errori codificati
  * --- Codifica dell'errore
  * --- pArchivio: tabella che d� errore
  * --- Array contenente le informazioni che mi servono per salvare i dati in DBF di appoggio
  *     o mantenere i dati originali prima di sovrascriverli
  * --- Seriale della gestione log da aggiornare
  * --- Oggetto per scrivere il log nella message.
  *     In questo caso non controllo che sia attivo il verbose.
  *     I messaggi di errore devono essere sempre visualizzati
  * --- Costruisco il messaggio....
  * --- Oggetto per messaggi incrementali
  * --- Path di memorizzazione dati errati e salvataggio dati preesistenti
  * --- Chiave record: per salvataggio dati. D� il nome al DBF
  * --- Ricerca per entit� ed archivio il corrispondente DBF
  m.w_DBF = LOOK_DBF( m.pEntita, m.pArchivio , @m.pArrDBF )
  w_oMess=createobject("Ah_Message")
  m.w_oPART = m.w_oMESS.addmsgpartNL("%1")
  w_oPART.addParam(Repl("#",50))
  m.w_oPART = m.w_oMESS.addmsgpartNL("Sincronizzazione dati da sede %1 per entit� %2 .")
  w_oPART.addParam(m.pSede)
  w_oPART.addParam(m.pEntita)
  w_oMess.AddMsgPartNL("La registrazione identificata dai valori riportati sotto")
  m.w_LOOP = 1
  m.w_CHIAVE = ""
  do while m.w_LOOP<= Alen( m.pArr ,1 )
    m.w_oPART = m.w_oMESS.addmsgpartNL("Campo %1 valore %2 ")
    w_oPART.addParam(pArr[ m.w_LOOP ,1 ])
    w_oPART.addParam(Cp_ToStr( pArr[ m.w_LOOP ,2 ] ))
    m.w_CHIAVE = m.w_CHIAVE+ Alltrim( cp_ToStr( pArr[ m.w_LOOP ,2 ] ))
    m.w_LOOP = m.w_LOOP + 1
  enddo
  * --- Genero i DBF di Log e ritorna messaggio
  m.pLog = GENDBLOG(m.pSede, m.pEntita, @m.pArr, m.pArchivio, m.w_CHIAVE, m.pNumLog, @m.pArrDBF, m.pLog, m.w_SERLOG)
  m.w_oPART = m.w_oMESS.addmsgpartNL( "ha generato l'avviso %0 %1" )
  w_oPART.addParam(m.pLog)
  m.w_oPART = m.w_oMESS.addmsgpartNL("%1")
  w_oPART.addParam(Repl("#",50))
  m.w_MESS = m.w_oMess.ComposeMessage()
  AddMsgNL("%1", m.ObjLog , m.w_Mess)
endproc


  function gest_log_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ENT_DETT'
    return(cp_OpenFuncTables(1))
* --- END GEST_LOG
* --- START GET_SEDE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: get_sede                                                        *
*              Get_sede                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-05                                                      *
* Last revis.: 2006-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func get_sede
param pCodazi

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_SEDE
  m.w_SEDE=space(2)
* --- WorkFile variables
  private PARALORE_idx
  PARALORE_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "get_sede"
if vartype(__get_sede_hook__)='O'
  __get_sede_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'get_sede('+Transform(pCodazi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'get_sede')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if get_sede_OpenTables()
  get_sede_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'get_sede('+Transform(pCodazi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'get_sede')
Endif
*--- Activity log
if vartype(__get_sede_hook__)='O'
  __get_sede_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure get_sede_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Ritorna l'attuale Sede...
  *     Utilizza una variabile pubblica che contiene il codice della sede 
  *     concatenato con il codice dell'azienda.
  *     Se la var. pubblica non � definita la definisce e la costruisce.
  *     Se l'attuale codice Azienda non coincide rilegge il dato dal database..
  *     Come parametro riceve l'azienda (se non passato i_codazi)
  if Type("pCodazi")<>"C"
    m.pCodazi = i_CODAZI
  endif
  m.pCodazi = PADR( m.pCodazi ,5," ")
  if Type("g_LORESED")="U" Or ( Type("g_LORESED")="C" And Right( g_LORESED , 5 )<>m.pCODAZI )
    * --- Read from PARALORE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[PARALORE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[PARALORE_idx,2],.t.,PARALORE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PL__SEDE"+;
        " from "+i_cTable+" PARALORE where ";
            +"PLCODAZI = "+cp_ToStrODBC(m.pCODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PL__SEDE;
        from (i_cTable) where;
            PLCODAZI = m.pCODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_SEDE = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Type("g_LORESED")="U" 
      Public g_LORESED
    endif
    g_LORESED = Padr( m.w_SEDE , 2 , " " ) + m.pCodazi
  endif
  i_retcode = 'stop'
  i_retval = Left( g_LORESED , 2 )
  return
endproc


  function get_sede_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PARALORE'
    return(cp_OpenFuncTables(1))
* --- END GET_SEDE
* --- START GETFIELDREL
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getfieldrel                                                     *
*              Analizza il contenuto di una relazione tra tabelle collegate e pricipale*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-24                                                      *
* Last revis.: 2008-04-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getfieldrel
param pRELAZIONE,pTAB1,pCAMPO,pESATTO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(30)
  private w_POS
  m.w_POS=0
  private w_CONTA
  m.w_CONTA=0
  private w_PAROLA1
  m.w_PAROLA1=space(30)
  private w_PAROLA2
  m.w_PAROLA2=space(30)
  private w_CERCAPAROLA1
  m.w_CERCAPAROLA1=.f.
  private w_INDICE
  m.w_INDICE=0
  private w_ARRDIM
  m.w_ARRDIM=0
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getfieldrel"
if vartype(__getfieldrel_hook__)='O'
  __getfieldrel_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getfieldrel('+Transform(pRELAZIONE)+','+Transform(pTAB1)+','+Transform(pCAMPO)+','+Transform(pESATTO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getfieldrel')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if getfieldrel_OpenTables()
  getfieldrel_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getfieldrel('+Transform(pRELAZIONE)+','+Transform(pTAB1)+','+Transform(pCAMPO)+','+Transform(pESATTO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getfieldrel')
Endif
*--- Activity log
if vartype(__getfieldrel_hook__)='O'
  __getfieldrel_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getfieldrel_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve 4 parametri :
  *     pRELAZIONE : La stringa contenente la relazione su cui effettuare la ricerca
  *     pTab1 : nome  della prima tabella coinvolta nella relazione
  *     pCampo : nome del campo della tabella (pTab1) su cui effettuare la ricerca
  *     pESATTO : se pESATT==.F.  la scandisce dell Array dei Tocken alla ricerca di pTAB1.campo  � effettuata in base alla corrispondenza tocken[ ]==TAB1.campo, 
  *                        se pESATT==.T   la ricerca nel tocke � effettuata in base all espressione  tocken[ ] contiene TAB1.campo  ).
  *     Restituisce il nome del campo della tabella con cui il campo (pCampo) � in relazione 
  *     Se la ricerca non ha esito positivo allora la funzione restituisce una stringa vuota
  *     
  *     La funzione sostituisce le parentesi con SPACE ( non interessa l ordine dei confronti )
  *     Sostituisce i connettivi logici con una parentesi aperta (alla conclulsione delle sostituzioni la frase che rappresenta la relazione ha un formato normalizzato, i caratteri '=' e ')' 
  *     rappresentano i i terminatori delle parti che rappresentazno la relazione) Estrae la prima parola che si trova a sinista del simbolo '=', dopodiche cerca la parola che si trova 
  *     alla destra del simbolo '='  finch� non incontra il simbolo '('  e  copia le 2 parole trovate nell array contenente le parole trovate. La funzione procede cercando la successive 
  *      coppie di parole . Con le coppie di parole cos� ottenute compila l array dei token. La deimensione dell array dei tocken � calcolata in base al numero dei simboli = contenuti 
  *     nella relazione.In conclusione scandisce l' array dei tocken alla ricerca della parola pTAB1.CAMPO e restituisce il corrispondente elemento con cui pTAB1.CAMPO � in relazione
  *     
  m.w_INDICE = 0
  m.w_RESULT = ""
  * --- La funzione lavora su nomi di campi/tabele e sulle relazioni in formato normalizzato (maiuscolo). Anche il risultato restituito dalla funzione � il nome del campo ricercato espresso informato in maiuscolo
  m.pRELAZIONE = UPPER (m.pRELAZIONE)
  m.pTAB1 = UPPER (m.pTAB1)
  m.pCAMPO = UPPER (m.pCAMPO)
  * --- Sostituisce le parentesi aperte e chiuse con space (non interessa l ordine con cui viene calcola  l espressione) ( per semplificare i controlli l espressione viene riscritta in maiuscolo)
  m.pRELAZIONE = UPPER (STRTRAN(m.pRELAZIONE ,")"," "))
  m.pRELAZIONE = STRTRAN(m.pRELAZIONE ,"("," ")
  * --- Sostituisce gli operatori logici (AND, OR e NOT ) con '('. Ci interessa estrarre le coppie TAB1.campo=TAB2.campo per individuare il campo di TAB2 che � in relazione con il campo
  *      di TAB1 (per tale scopo gli operatori logici non servono, non interessa il significato logico dell espressione)
  m.pRELAZIONE = STRTRAN(m.pRELAZIONE ," AND ","(")
  m.pRELAZIONE = STRTRAN(m.pRELAZIONE ," OR ","(")
  m.pRELAZIONE = STRTRAN(m.pRELAZIONE ,"NOT ","(")
  m.w_POS = 1
  * --- Conta il numero di separatori (0<> o altri simboli non alfanumerici) contenuti nella relazione.
  do while m.w_POS<=LEN (m.pRELAZIONE)
    if NOT ISALPHA (SUBSTR ( m.pRELAZIONE , m.w_POS , 1))
      m.w_ARRDIM = m.w_ARRDIM+ 1
    endif
    m.w_POS = m.w_POS + 1
  enddo
  if m.w_ARRDIM>0
    Dimension Token[m.w_ARRDIM ,2]
    m.w_PAROLA1 = ""
    m.w_PAROLA2 = ""
    * --- w_CERCAPAROLA1=.T. cerca la prima parola (a sinistra del simbolo di uguaglianza)
    *     w_CERCAPAROLA1=.F. cerca la seconda parola (a destra del simbolo di uguaglianza)
    m.w_CERCAPAROLA1 = .T.
    * --- Scandisce la stringa che contiene la relazione
    m.w_POS = 1
    do while m.w_POS<=LEN (m.pRELAZIONE)
      * --- Se si incontra un carattere di fine parola e w_PAROLA1 non contiene pTAB1 o pTAB2 allora la cancello
      if ( NOT m.w_CERCAPAROLA1) AND SUBSTR ( m.pRELAZIONE , m.w_POS , 1)= "("
        m.w_CERCAPAROLA1 = .T.
        getfieldrel_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        m.w_PAROLA1 = ""
        m.w_PAROLA2 = ""
        * --- Carica le parole trovate nell array dei token
      endif
      if SUBSTR ( m.pRELAZIONE , m.w_POS , 1)= "="
        m.w_CERCAPAROLA1 = .F.
      endif
      if m.w_CERCAPAROLA1 AND SUBSTR ( m.pRELAZIONE , m.w_POS , 1)<> "(" AND SUBSTR ( m.pRELAZIONE , m.w_POS , 1) <> "="
        m.w_PAROLA1 = m.w_PAROLA1 + SUBSTR ( m.pRELAZIONE , m.w_POS , 1)
      endif
      if NOT m.w_CERCAPAROLA1 AND SUBSTR ( m.pRELAZIONE , m.w_POS , 1)<> "(" AND SUBSTR ( m.pRELAZIONE , m.w_POS , 1) <> "="
        m.w_PAROLA2 = m.w_PAROLA2 + SUBSTR ( m.pRELAZIONE , m.w_POS , 1)
      endif
      m.w_POS = m.w_POS + 1
    enddo
    getfieldrel_Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scandisce l Array dei Tocken alla ricerca di pTAB1.campo  (in base al parametro pESATTO la ricerca � su una corrispondenza tocken[ ]==TAB1.campo oppure  tocken[ ] contiene TAB1.campo  )
    m.w_CONTA = 1
    do while m.w_CONTA<=m.w_INDICE AND m.w_INDICE>0
      if TYPE ("Token[m.w_CONTA,1]")="C" AND TYPE ("Token[m.w_CONTA,2]")="C"
        if m.pESATTO
          if AT (alltrim(m.pTAB1)+"."+alltrim(m.pCAMPO) , Token[m.w_CONTA,1]) >0
            i_retcode = 'stop'
            i_retval = Token[m.w_CONTA,2]
            return
          endif
          if AT (alltrim(m.pTAB1)+"."+alltrim(m.pCAMPO) , Token[m.w_CONTA,2]) >0
            i_retcode = 'stop'
            i_retval = Token[m.w_CONTA,1]
            return
          endif
        else
          if Token[m.w_CONTA,1]==alltrim(m.pTAB1)+"."+alltrim(m.pCAMPO)
            i_retcode = 'stop'
            i_retval = Token[m.w_CONTA,2]
            return
          endif
          if Token[m.w_CONTA,2]==alltrim(m.pTAB1)+"."+alltrim(m.pCAMPO)
            i_retcode = 'stop'
            i_retval = Token[m.w_CONTA,1]
            return
          endif
        endif
      endif
      m.w_CONTA = m.w_CONTA + 1
    enddo
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


procedure getfieldrel_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  m.w_INDICE = m.w_INDICE + 1
  * --- E meglio mettere il controllo sula dimensione dell array poich� non si conoscono a priori il formato delle stringhe ricevute come parametri dalla funzione
  *     Nell array non si inseriscono tocken vuoti (al posto dei tocken vuoti si mette una stringa  di caratteri [###############] che in una corretta relazione tra tabelle non possono esserci )
  if m.w_ARRDIM >=m.w_INDICE
    Token[m.w_INDICE,1]= IIF (empty (alltrim (m.w_PAROLA1)),"###############",alltrim (m.w_PAROLA1)) 
 Token[m.w_INDICE,2]= IIF (empty (alltrim (m.w_PAROLA2)),"###############",alltrim (m.w_PAROLA2))
  endif
endproc


  function getfieldrel_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='SUENDETT'
    return(cp_OpenFuncTables(1))
* --- END GETFIELDREL
* --- START GETMIRRORNAME
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getmirrorname                                                   *
*              Determina nome tabella mirror                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2010-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getmirrorname
param pTable

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_IDXTABLE
  m.w_IDXTABLE=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getmirrorname"
if vartype(__getmirrorname_hook__)='O'
  __getmirrorname_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getmirrorname('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getmirrorname')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
getmirrorname_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getmirrorname('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getmirrorname')
Endif
*--- Activity log
if vartype(__getmirrorname_hook__)='O'
  __getmirrorname_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getmirrorname_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato il nome logico di una tabella restituisce il nome della corrispondente
  *     tabella di mirrror.
  *     Es. da CONTI in LR_DEMOCONTI
  *     
  m.w_IDXTABLE = cp_OpenTable( m.pTable ,.T.)
  m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
  cp_CloseTable( m.pTable)
  i_retcode = 'stop'
  i_retval = "LR_"+Alltrim( m.w_PHNAME )
  return
endproc


* --- END GETMIRRORNAME
* --- START GETTSMNAME
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gettsmname                                                      *
*              Determina nome tabella mirror                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2010-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func gettsmname
param pTable

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_IDXTABLE
  m.w_IDXTABLE=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "gettsmname"
if vartype(__gettsmname_hook__)='O'
  __gettsmname_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'gettsmname('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gettsmname')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
gettsmname_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'gettsmname('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gettsmname')
Endif
*--- Activity log
if vartype(__gettsmname_hook__)='O'
  __gettsmname_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure gettsmname_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato il nome logico di una tabella restituisce il nome della corrispondente
  *     tabella di mirrror per tabelle secondarie
  *     Es. da DOC_DETT in TSM_DEMODOC_DETT
  *     
  m.w_IDXTABLE = cp_OpenTable( m.pTable ,.T.)
  m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
  cp_CloseTable( m.pTable )
  i_retcode = 'stop'
  i_retval = "TSM_"+Alltrim( m.w_PHNAME )
  return
endproc


* --- END GETTSMNAME
* --- START ISPACKAGE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ispackage                                                       *
*              Trova pacchetto                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-17                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ispackage
param w_SEDE,w_PROG,w_CONNECT

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CANALE
  m.w_CANALE=space(1)
  private w_SHARED
  m.w_SHARED=space(254)
  private w_FILE
  m.w_FILE=space(254)
  private w_DESTINATARIO
  m.w_DESTINATARIO=space(2)
  private w_SEFTPHST
  m.w_SEFTPHST=space(254)
  private w_SEFTPUSR
  m.w_SEFTPUSR=space(254)
  private w_SEFTPPWD
  m.w_SEFTPPWD=space(254)
  private w_SEFTPMOD
  m.w_SEFTPMOD=space(1)
* --- WorkFile variables
  private PARALORE_idx
  PARALORE_idx=0
  private PUBLMAST_idx
  PUBLMAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ispackage"
if vartype(__ispackage_hook__)='O'
  __ispackage_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ispackage('+Transform(w_SEDE)+','+Transform(w_PROG)+','+Transform(w_CONNECT)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ispackage')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ispackage_OpenTables()
  ispackage_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ispackage('+Transform(w_SEDE)+','+Transform(w_PROG)+','+Transform(w_CONNECT)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ispackage')
Endif
*--- Activity log
if vartype(__ispackage_hook__)='O'
  __ispackage_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ispackage_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Data sede e numero pacchetto, determina se il pacchetto esiste, se si
  *     restituisce il percorso+il nome del pacchetto, altrimenti stringa vuota..
  * --- Leggo la sede che esporta...
  * --- Read from PARALORE
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PARALORE_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PARALORE_idx,2],.t.,PARALORE_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PL__SEDE"+;
      " from "+i_cTable+" PARALORE where ";
          +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PL__SEDE;
      from (i_cTable) where;
          PLCODAZI = i_CODAZI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_DESTINATARIO = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  m.w_DESTINATARIO = Trim( m.w_DESTINATARIO )
  * --- Read from PUBLMAST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PUBLMAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PUBLMAST_idx,2],.t.,PUBLMAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "SECANALE,SESHARED,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD"+;
      " from "+i_cTable+" PUBLMAST where ";
          +"SECODICE = "+cp_ToStrODBC(m.w_SEDE);
          +" and SE__TIPO = "+cp_ToStrODBC("P");
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      SECANALE,SESHARED,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD;
      from (i_cTable) where;
          SECODICE = m.w_SEDE;
          and SE__TIPO = "P";
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CANALE = NVL(cp_ToDate(_read_.SECANALE),cp_NullValue(_read_.SECANALE))
    m.w_SHARED = NVL(cp_ToDate(_read_.SESHARED),cp_NullValue(_read_.SESHARED))
    m.w_SEFTPHST = NVL(cp_ToDate(_read_.SEFTPHST),cp_NullValue(_read_.SEFTPHST))
    m.w_SEFTPUSR = NVL(cp_ToDate(_read_.SEFTPUSR),cp_NullValue(_read_.SEFTPUSR))
    m.w_SEFTPPWD = NVL(cp_ToDate(_read_.SEFTPPWD),cp_NullValue(_read_.SEFTPPWD))
    m.w_SEFTPMOD = NVL(cp_ToDate(_read_.SEFTPMOD),cp_NullValue(_read_.SEFTPMOD))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Se non sono gi� connesso effettuo la connessione
  if ! m.w_CONNECT And ! ConnessioneSede("C", m.w_SEDE, "P")
    * --- Errore durante la connessione
    i_retcode = 'stop'
    i_retval = ""
    return
  endif
  m.w_SHARED = Trim( m.w_SHARED )
  m.w_FILE = Alltrim(m.w_SEDE) +"_"+Alltrim(m.w_DESTINATARIO)+"_"+Right("0000000000"+Alltrim( Str( m.w_PROG ) ), 10) + ".ahz"
  do case
    case m.w_CANALE="C" Or m.w_CANALE="R"
      * --- RAS - Cartella condivisa
      if ! File(ADDBS(m.w_SHARED)+m.w_FILE)
        m.w_FILE = ""
      endif
    case m.w_CANALE="F"
      * --- FTP
      * --- metto un asterisco finale per evitare problemi con case-sensitive
      m.w_FILE = Alltrim(m.w_SEDE) +"_"+Alltrim(m.w_DESTINATARIO)+"_"+Right("0000000000"+Alltrim( Str( m.w_PROG ) ), 10) + "*.ahz"
      m.w_FILE = ftpClient("E", m.w_SEFTPHST, m.w_SEFTPUSR, m.w_SEFTPPWD, m.w_SHARED+"/" + m.w_FILE, , , , , m.w_SEFTPMOD )
  endcase
  * --- Disconnetto solo se sono stato io a connetterlo all'interno della funzione IsPackage
  if ! m.w_CONNECT And ! ConnessioneSede("D", m.w_SEDE, "P")
    * --- Errore durante la disconnessione
    i_retcode = 'stop'
    i_retval = ""
    return
  endif
  * --- Ritorno il nome del file se l'ho trovato
  i_retcode = 'stop'
  i_retval = m.w_FILE
  return
endproc


  function ispackage_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='PARALORE'
    i_cWorkTables[2]='PUBLMAST'
    return(cp_OpenFuncTables(2))
* --- END ISPACKAGE
* --- START LEGGI_DATI
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: leggi_dati                                                      *
*              Legge i dati dallo spazio condiviso                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-13                                                      *
* Last revis.: 2006-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func leggi_dati
param p_Array,pDir

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_DIR_DATI
  m.w_DIR_DATI=space(254)
  private W_LOOP
  m.W_LOOP=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "leggi_dati"
if vartype(__leggi_dati_hook__)='O'
  __leggi_dati_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'leggi_dati('+Transform(p_Array)+','+Transform(pDir)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'leggi_dati')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
leggi_dati_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'leggi_dati('+Transform(p_Array)+','+Transform(pDir)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'leggi_dati')
Endif
*--- Activity log
if vartype(__leggi_dati_hook__)='O'
  __leggi_dati_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure leggi_dati_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Legge i dati dall'area condivisa spostandoli all'interno di una cartella passata
  *     come parametro.
  *     
  *     Inoltre popola un array con l'elenco delle tabelle recuperate
  if Type("pDir")<>"C" or (Type("pDir")="C" And Not DIRECTORY(m.pDIr,1))
    * --- Se directory non passata salvo tutto nella cartella temporanea...
    m.pDir = TempAdhoc()
  endif
  * --- Per prova ora recupera i DBF presenti in
  *     U:\AHRU40_REMOTO\LORE\remote
  *     nella versione definitiva vi sar� una routine che preleva i dati dallo
  *     spazio condiviso e li copia localmente. Questa routine si occuper� di
  *     aprire il "pacco" e scompattarlo in una cartella..
  m.w_DIR_DATI = "U:\AHRU40_REMOTO\LORE\remote\"
  m.W_LOOP = 1
   
 Dimension _Leggi_Dati_(1) 
 Adir( _Leggi_Dati_ , m.w_DIR_DATI+"*.DBF" )
  do while m.w_LOOP<=ALEN( _Leggi_Dati_ , 1 )
    if Alen( m.p_Array )< m.w_LOOP
      Dimension pArray( m.w_LOOP )
    endif
    p_Array[ m.w_LOOP ] = _Leggi_Dati_[ m.W_LOOP ,1 ]
    m.W_LOOP = 1
  enddo
  i_retcode = 'stop'
  return
endproc


* --- END LEGGI_DATI
* --- START LOOK_DBF
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: look_dbf                                                        *
*              Ricerca DBF per entita e tabella                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-08                                                      *
* Last revis.: 2006-09-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func look_dbf
param w_ENTITA,w_TABELLA,pArr

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_DBF
  m.w_DBF=space(254)
  private w_LOOP
  m.w_LOOP=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "look_dbf"
if vartype(__look_dbf_hook__)='O'
  __look_dbf_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'look_dbf('+Transform(w_ENTITA)+','+Transform(w_TABELLA)+','+Transform(pArr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'look_dbf')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
look_dbf_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'look_dbf('+Transform(w_ENTITA)+','+Transform(w_TABELLA)+','+Transform(pArr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'look_dbf')
Endif
*--- Activity log
if vartype(__look_dbf_hook__)='O'
  __look_dbf_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure look_dbf_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato l'array che virtualizza il pacchetto, ricerca
  *     per il DBF corrispondente dell'archivio per l'entit�.
  *     
  *     Riceve l'array, l'entit� e l'archivio e restituisce
  *     il nome del DBF se trovato altrimenti ''
  m.w_LOOP = 1
  m.w_DBF = ""
  do while m.w_LOOP <= Alen( m.pArr , 1 ) 
    if Alltrim(Upper(pArr[ m.w_LOOP , 3 ] ) ) = Alltrim( Upper( m.w_ENTITA ) ) And Alltrim( Upper(pArr[ m.w_LOOP , 1 ] ) ) = Alltrim( Upper( m.w_TABELLA ) )+".DBF"
      m.w_DBF = pArr[ m.w_LOOP , 2 ] 
      * --- Esco..
      m.w_LOOP = Alen( m.pArr , 1 ) + 1
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  i_retcode = 'stop'
  i_retval = m.w_DBF
  return
endproc


* --- END LOOK_DBF
* --- START READMIRR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: readmirr                                                        *
*              Legge dal mirror                                                *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-28                                                      *
* Last revis.: 2006-06-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func readmirr
param w_ARCHIVIO,w_FIELDS,ARRVALUE,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(10)
  private w_MIRROR
  m.w_MIRROR=space(10)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "readmirr"
if vartype(__readmirr_hook__)='O'
  __readmirr_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'readmirr('+Transform(w_ARCHIVIO)+','+Transform(w_FIELDS)+','+Transform(ARRVALUE)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'readmirr')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
readmirr_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'readmirr('+Transform(w_ARCHIVIO)+','+Transform(w_FIELDS)+','+Transform(ARRVALUE)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'readmirr')
Endif
*--- Activity log
if vartype(__readmirr_hook__)='O'
  __readmirr_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure readmirr_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Legge dati dal Mirror
  *     Riceve archivio (Es. CONTI), una stringa con l'elenco dei campi ed un array
  *     con l'elenco dei  campi e relativi valori.
  *     Restituisce il nome del cursore con i dati, se restituisce una stringa vuota
  *     c'� stato un errore.
  m.w_MIRROR = GetMirrorName ( m.w_ARCHIVIO )
  if Empty( m.w_MIRROR )
    if VarType( m.pLog ) ="O" And m.pVerbose
      AddMsgNL("Errore impossibile determinare nome fisico mirror archivio %1 ", m.pLog , m.w_ARCHIVIO , , ,, ,)
    endif
    m.w_RESULT = ""
  else
    m.w_RESULT = READTABLE( m.w_MIRROR , m.w_FIELDS, @m.ARRVALUE , m.pLog, m.pVerbose ) 
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


* --- END READMIRR
* --- START REPALIAS
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: repalias                                                        *
*              Sostituisce Alias in filtro                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-11                                                      *
* Last revis.: 2008-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func repalias
param pExpr,pAlias1,pAlias2

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "repalias"
if vartype(__repalias_hook__)='O'
  __repalias_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'repalias('+Transform(pExpr)+','+Transform(pAlias1)+','+Transform(pAlias2)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'repalias')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
repalias_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'repalias('+Transform(pExpr)+','+Transform(pAlias1)+','+Transform(pAlias2)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'repalias')
Endif
*--- Activity log
if vartype(__repalias_hook__)='O'
  __repalias_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure repalias_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve come parametro un espressione di filtro un alias da sostituire
  *     e l'alias da utilizzare.
  *     Ritorna l'espressione con l'lalias 1 al posto dell'alias 2
  *     La ricerca � svolta su tutte le occorrenze in modo case insensitive
  m.pExpr = Alltrim( m.pExpr )
  m.pAlias1 = Alltrim( m.pAlias1)
  m.pAlias2 = Alltrim( m.pAlias2)
  * --- Se l'espressione inizia per palias1. allora sostituisco questa occorrenza...
  if upper(Left( m.pExpr , Len ( m.pAlias1 )+1 ))=upper(m.pAlias1 )+"."
    m.pExpr = Strtran( m.pExpr , m.pAlias1+".", m.pAlias2+"." ,1,1,1)
  endif
  * --- Gestisco i vari caratteri che possono essere prima del nome archivio...
  m.pExpr = Strtran( m.pExpr , " "+m.pAlias1+".", " "+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "="+m.pAlias1+".", "="+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "+"+m.pAlias1+".", "+"+m.pAlias2+".",1,-1,1)
  m.pExpr = Strtran( m.pExpr , "-"+m.pAlias1+".", "-"+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "*"+m.pAlias1+".", "*"+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "/"+m.pAlias1+".", "/"+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "("+m.pAlias1+".", "("+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , ">"+m.pAlias1+".", ">"+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "<"+m.pAlias1+".", "<"+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "["+m.pAlias1+".", "["+m.pAlias2+"." ,1,-1,1)
  m.pExpr = Strtran( m.pExpr , "]"+m.pAlias1+".", "]"+m.pAlias2+"." ,1,-1,1)
  i_retcode = 'stop'
  i_retval = m.pExpr
  return
endproc


* --- END REPALIAS
* --- START RICMIRROR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ricmirror                                                       *
*              Ricezione aggiorna mirror                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-28                                                      *
* Last revis.: 2006-06-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ricmirror
param w_ARCHIVIO,cOper,pArrValue,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(20)
  private w_MIRROR
  m.w_MIRROR=space(50)
  private w_LOOP
  m.w_LOOP=0
  private w_IDXKEY
  m.w_IDXKEY=0
  private w_IDXVALUE
  m.w_IDXVALUE=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ricmirror"
if vartype(__ricmirror_hook__)='O'
  __ricmirror_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ricmirror('+Transform(w_ARCHIVIO)+','+Transform(cOper)+','+Transform(pArrValue)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ricmirror')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
ricmirror_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ricmirror('+Transform(w_ARCHIVIO)+','+Transform(cOper)+','+Transform(pArrValue)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ricmirror')
Endif
*--- Activity log
if vartype(__ricmirror_hook__)='O'
  __ricmirror_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ricmirror_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Aggiorna il mirror in fase di ricezione
  *     Dato l'archivio (Super Entit�) decide come aggiornare l'archivio di mirror
  *     in fase di ricezione
  *     ArrValue contiene le chiavi eventuali campi supplementari (prime posizioni) pi� il CPCCCHK, l'ANCESTOR e AVERSION contenute nel DBF ricevuto
  *            (tre colonne la prima contiene il nome del campo,mentre la terza il valore, la terza 1 se chiave, 2 se valore supplementare 0 campo di servizio se campo chiave )
  *     cOper (I,D,U) mi dice l'operazione da svolgere
  *     Ritorna stringa vuota se tutto ok altrimenti l'errore
  m.w_ARCHIVIO = Alltrim( m.w_ARCHIVIO )
  m.w_MIRROR = GetMirrorName ( m.w_ARCHIVIO )
  * --- Leggo sul mirror le informazioni legate al record...
  m.w_RESULT = ""
  do case
    case m.cOper="D"
      * --- Cancello l'entrata dal Mirror
      if Not Empty( DELETETABLE( m.w_MIRROR , @m.pArrValue ,m.pLog,m.pVerbose ) )
        m.w_RESULT = AH_MsgFormat( "Errore cancellazione mirror archivio %1, errore %2" , m.w_ARCHIVIO , Message() ,, )
         
 i_Error=MSG_DELETE_ERROR
      else
        if VarType( m.pLog )="O" And m.pVerbose
          AddMsgNL("Aggiornamento mirror (Delete) super entit� %1",m.pLog, m.w_ARCHIVIO , , , ,, )
        endif
      endif
    case m.cOper="I"
      * --- Inserimento
      if Not Empty( INSERTTABLE( m.w_MIRROR , @m.pArrValue ,m.pLog,m.pVerbose ) )
        m.w_RESULT = AH_MsgFormat( "Errore inserimento mirror archivio %1, errore %2" , m.w_ARCHIVIO , Message() ,, )
         
 i_Error=MSG_INSERT_ERROR
      else
        if VarType( m.pLog )="O" And m.pVerbose
          AddMsgNL("Aggiornamento mirror (Insert) super entit� %1",m.pLog, m.w_ARCHIVIO , , , ,, )
        endif
      endif
    case m.cOper="U"
      * --- Costruisco gli array da passare alla WRITETABLE.
      *     Uno contiene le chiavi e rispettivi valori (pArrKey)
      *     Mentre l'altro contiene i campi da aggiornare in fase di
      *     ricezione quindi
      *     PUBCOSED
      *     ANCESTOR
      *     AVERSION (questo � passato gia aggiornato se la sede � il validatore)
      Dimension ArrKey[1,2], ArrVal[1,2]
      m.w_LOOP = 1
      m.w_IDXKEY = 1
      m.w_IDXVALUE = 1
      do while m.w_LOOP <= Alen ( m.pArrValue , 1)
        * --- Campi chiave
        if pArrValue[ m.w_LOOP ,3 ] = 1
           
 Dimension ArrKey[ m.w_IDXKEY ,2] 
 Arrkey[ m.w_IDXKEY ,1 ]=pArrValue[ m.w_LOOP ,1 ] 
 Arrkey[ m.w_IDXKEY ,2 ]=pArrValue[ m.w_LOOP ,2 ]
          m.w_IDXKEY = m.w_IDXKEY + 1
        endif
        * --- Campi di servizio
        if pArrValue[ m.w_LOOP ,3 ] = 0
           
 Dimension ArrVal[ m.w_IDXVALUE ,2] 
 ArrVal[ m.w_IDXVALUE ,1 ]=pArrValue[ m.w_LOOP ,1 ] 
 ArrVal[ m.w_IDXVALUE ,2 ]=pArrValue[ m.w_LOOP ,2 ]
          m.w_IDXVALUE = m.w_IDXVALUE + 1
        endif
        m.w_LOOP = m.w_LOOP + 1
      enddo
      if Not Empty( WRITETABLE( m.w_MIRROR , @ArrVal , @ArrKey , m.pLog,m.pVerbose ) )
        m.w_RESULT = AH_MsgFormat( "Errore modifica mirror archivio %1, errore %2" , m.w_ARCHIVIO , Message() ,, )
         
 i_Error=MSG_UPDATE_ERROR
      else
        if VarType( m.pLog )="O" And m.pVerbose
          AddMsgNL("Aggiornamento mirror (Update) super entit� %1",m.pLog, m.w_ARCHIVIO , , , ,, )
        endif
      endif
  endcase
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


* --- END RICMIRROR
* --- START PACKAGE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: package                                                         *
*              Apre pacchetto                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-17                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func package
param w_SEDE,w_PROG,pArr,pLog,pNoEstrai,w_VERBOSE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(240)
  private w_PATH
  m.w_PATH=space(254)
  private w_FILE
  m.w_FILE=space(254)
  private w_DESPATH
  m.w_DESPATH=space(254)
  private w_IDXARRAY
  m.w_IDXARRAY=0
  private w_CANALE
  m.w_CANALE=space(1)
  private w_SHARED
  m.w_SHARED=space(254)
  private w_SEPWDZIP
  m.w_SEPWDZIP=space(254)
  private w_SEFTPHST
  m.w_SEFTPHST=space(254)
  private w_SEFTPUSR
  m.w_SEFTPUSR=space(254)
  private w_SEFTPPWD
  m.w_SEFTPPWD=space(254)
  private w_SEFTPMOD
  m.w_SEFTPMOD=space(1)
  private w_TEMPPATH
  m.w_TEMPPATH=space(254)
  private w_RET
  m.w_RET=space(254)
  private w_ZipFile
  m.w_ZipFile=space(254)
  private w_LOOP
  m.w_LOOP=0
  private w_NDBF
  m.w_NDBF=0
  private w_CODENT
  m.w_CODENT=space(15)
  private w_PATHENT
  m.w_PATHENT=space(254)
  private w_TMPDBF
  m.w_TMPDBF=space(10)
* --- WorkFile variables
  private PUBLMAST_idx
  PUBLMAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "package"
if vartype(__package_hook__)='O'
  __package_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'package('+Transform(w_SEDE)+','+Transform(w_PROG)+','+Transform(pArr)+','+Transform(pLog)+','+Transform(pNoEstrai)+','+Transform(w_VERBOSE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'package')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if package_OpenTables()
  package_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'package('+Transform(w_SEDE)+','+Transform(w_PROG)+','+Transform(pArr)+','+Transform(pLog)+','+Transform(pNoEstrai)+','+Transform(w_VERBOSE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'package')
Endif
*--- Activity log
if vartype(__package_hook__)='O'
  __package_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure package_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Apre un pacchetto..
  *     Riceve 
  *      - Sede mittente
  *      - Numero pacchetto
  *      - Array  da riempire con entita, archivio, e posizione del DBF ricavato dal pacchetto su disco
  *      - pLog  = Oggetto sul quale mostrare il log operazioni
  *      - pNoEstrai - Per eventuale funzione di visualizzazione contenuto pacchetto (elenco entit� archivio)
  *     
  *     Se qualcosa va storto restituisce una stringa con il messaggio dell'errore...
  * --- Utilizzo il percorso DOS altrimenti le frasi sql su dbf vanno in errore
   i_cperrcorso = tempadhoc() 
 i_cpercorsodos=space (300) 
 DECLARE INTEGER GetShortPathName IN Kernel32 STRING @lpszLongPath, STRING @lpszShortPath, INTEGER cchBuffer 
 GetShortPathName(@i_cperrcorso , @i_cpercorsodos, 300) 
 i_cpercorsodos=alltrim ( ADDBS( i_cpercorsodos))
  * --- Read from PUBLMAST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PUBLMAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PUBLMAST_idx,2],.t.,PUBLMAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "SECANALE,SESHARED,SEPWDZIP,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD"+;
      " from "+i_cTable+" PUBLMAST where ";
          +"SECODICE = "+cp_ToStrODBC(m.w_SEDE);
          +" and SE__TIPO = "+cp_ToStrODBC("P");
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      SECANALE,SESHARED,SEPWDZIP,SEFTPHST,SEFTPUSR,SEFTPPWD,SEFTPMOD;
      from (i_cTable) where;
          SECODICE = m.w_SEDE;
          and SE__TIPO = "P";
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CANALE = NVL(cp_ToDate(_read_.SECANALE),cp_NullValue(_read_.SECANALE))
    m.w_SHARED = NVL(cp_ToDate(_read_.SESHARED),cp_NullValue(_read_.SESHARED))
    m.w_SEPWDZIP = NVL(cp_ToDate(_read_.SEPWDZIP),cp_NullValue(_read_.SEPWDZIP))
    m.w_SEFTPHST = NVL(cp_ToDate(_read_.SEFTPHST),cp_NullValue(_read_.SEFTPHST))
    m.w_SEFTPUSR = NVL(cp_ToDate(_read_.SEFTPUSR),cp_NullValue(_read_.SEFTPUSR))
    m.w_SEFTPPWD = NVL(cp_ToDate(_read_.SEFTPPWD),cp_NullValue(_read_.SEFTPPWD))
    m.w_SEFTPMOD = NVL(cp_ToDate(_read_.SEFTPMOD),cp_NullValue(_read_.SEFTPMOD))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
   
 Local L_OldErr, L_bErr, L_OldArea 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T. 
 L_OldArea=Select()
  * --- Apro connessione
  if ! ConnessioneSede("C", m.w_SEDE, "P", m.w_VERBOSE, m.pLog)
    i_retcode = 'stop'
    i_retval = Ah_MsgFormat("Errore di connessione")
    return
  endif
  * --- Verifico se il file esiste (la connessione � gi� aperta)
  m.w_FILE = ISPACKAGE( m.w_SEDE , m.w_PROG, .T. )
  if Empty( m.w_FILE )
    if VarType( m.pLog ) ="O"
      AddMsgNL("Per la sede %1 il pacchetto numero %2 non esiste", m.pLog , m.w_SEDE , Alltrim( Str( m.w_PROG ) ) , ,, ,)
    endif
    * --- Pacchetto non esistente
    * --- Se ho aperto una connessione RAS la chiudo
    ConnessioneSede("D", m.w_SEDE, "P", m.w_VERBOSE, m.pLog)
    i_retcode = 'stop'
    i_retval = Ah_MsgFormat("Pacchetto non trovato")
    return
  else
    * --- Scarico il file in locale sulla temp
    m.w_TEMPPATH = i_cpercorsodos+ "Remote"
    MakeDir(m.w_TEMPPATH)
    if m.w_CANALE="F"
      * --- Copio il file via FTP
      L_bErr = ! FtpClient("G", m.w_SEFTPHST, m.w_SEFTPUSR, m.w_SEFTPPWD, ALLTRIM(m.w_SHARED)+"/"+JUSTFNAME(m.w_FILE), m.w_TEMPPATH+"\"+JUSTFNAME(m.w_FILE), 2, 0, ,m.w_SEFTPMOD )
    else
      Local l_macro
      l_macro = 'COPY FILE "' + ADDBS(ALLTRIM(m.w_SHARED))+JUSTFNAME(m.w_FILE) +'" TO "'+ m.w_TEMPPATH + '"'
      &l_macro
    endif
    if L_bErr
      if VarType( m.pLog ) ="O" 
        AddMsgNL("Impossibile copiare il file zip %1 in %2", m.pLog, m.w_FILE, m.w_TEMPPATH)
      endif
      * --- Si � verificato un errore
      m.w_RET = Message()
    else
      if VarType( m.pLog ) ="O" And m.w_VERBOSE
        AddMsgNL("Copia del file zip %1 in %2", m.pLog, m.w_FILE, m.w_TEMPPATH)
      endif
    endif
    * --- Se ho aperto una connessione RAS la chiudo
    ConnessioneSede("D", m.w_SEDE, "P", m.w_VERBOSE, m.pLog)
    * --- Reset flag errore
    L_bErr = .f.
    * --- Estraggo lo zip
    m.w_ZipFile = i_cpercorsodos+"Remote\"+m.w_FILE
    Local L_RetMsg
    L_RetMsg = ""
    if ! ZipUnZip("U", m.w_ZipFile, ADDBS(m.w_TEMPPATH)+JUSTSTEM(m.w_FILE)+"\", m.w_SEPWDZIP, @L_RetMsg)
      if VarType( m.pLog ) ="O" 
        AddMsgNL("Errore nella scompattazione del file zip %1%0%2", m.pLog , m.w_ZipFile, L_RetMsg)
      endif
      * --- Si � verificato un errore
      m.w_RET = L_RetMsg
    else
      if VarType( m.pLog ) ="O" And m.w_VERBOSE
        AddMsgNL("Scompattato file zip %1", m.pLog, m.w_ZipFile)
      endif
    endif
    if ! l_bErr
      * --- Path di partenza
      m.w_PATH = ADDBS(m.w_TEMPPATH)+JUSTSTEM(m.w_FILE)
      m.w_RET = CHKTBLPAC(m.w_PATH+"\CHECKTABLEPLAN\CHECKTABLEPLAN.DBF", m.w_SEDE)
      if Not Empty(m.w_Ret)
        i_retcode = 'stop'
        i_retval = m.w_RET
        return
      endif
      * --- Mentre i cursori li creo nella cartella temporanea...
      m.w_DESPATH = i_cpercorsodos
      * --- Una volta che ISPACKAGE mi dice che nello spazio condiviso c'� un
      *     pacchetto lo vado ad aprire. Lo vado ad aprire estraendone i dati.
      *     I dati estratti sono posizionati nella Temp.
       
 Dimension DirArr[1,5] 
 Adir( DirArr , m.w_PATH+"\*.*" , "D" , 0 )
      * --- Scorro l'elenco delle Entit�..
      m.w_IDXARRAY = 1
      m.w_LOOP = 1
      do while m.w_LOOP <= Alen ( DirArr , 1 )
        if "D" $ DirArr [ m.w_LOOP , 5] And DirArr [ m.w_LOOP , 1] <>"." And DirArr [ m.w_LOOP , 1] <>".."
          m.w_CODENT = Alltrim( DirArr [ m.w_LOOP , 1] )
          * --- Scorro i DBF all'interno per determinare gli archivi dell'entit�...
          m.w_PATHENT = m.w_PATH+"\"+m.w_CODENT
           
 Dimension EntArr[1,5] 
 Adir( EntArr , m.w_PATHENT+"\*.DBF" , "A" , 0 )
          m.w_NDBF = 1
          do while m.w_NDBF <= Alen ( EntArr , 1 )
            m.w_TMPDBF = m.w_DESPATH + Sys(2015)+".DBF"
            * --- Copio nella tabella temporanea il file...
             
 Local L_OldArea 
 L_OldArea=Select() 
 Use m.w_PATHENT+"\"+EntArr[ m.w_NDBF ,1] in 0 
 Select( EntArr[ m.w_NDBF ,1] ) 
 Copy to ( m.w_TMPDBF ) 
 Use 
 Select ( L_OldArea )
            * --- Contenuto Array
            *     1 = Nome archivio
            *     2 = DBF di appoggio con i dati
            *     3 = Entit� archivio
             
 Declare pArr[ m.w_IDXARRAY , 3 ] 
 pArr[ m.w_IDXARRAY , 1 ]= EntArr [ m.w_NDBF , 1] 
 pArr[ m.w_IDXARRAY , 2 ]= m.w_TMPDBF 
 pArr[ m.w_IDXARRAY , 3 ]= m.w_CODENT
            m.w_IDXARRAY = m.w_IDXARRAY + 1
            * --- Rispristino la vecchia gestione errori
            On Error &L_OldErr
            if L_bErr
              * --- Se qualcosa va storto segnalo...
              i_retcode = 'stop'
              i_retval = Message()
              return
            endif
            m.w_NDBF = m.w_NDBF + 1
          enddo
        endif
        m.w_LOOP = m.w_LOOP + 1
      enddo
      * --- Se lo zip esiste cancello la cartella che ho appena compresso
      if Not DeleteFolder(m.w_PATH)
        * --- Cartella non cancellabile
        i_retcode = 'stop'
        i_retval = Message()
        return
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = ""
  return
endproc


  function package_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PUBLMAST'
    return(cp_OpenFuncTables(1))
* --- END PACKAGE
* --- START SINCRONIZZA
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: sincronizza                                                     *
*              Sincronizza entita                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-16                                                      *
* Last revis.: 2015-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func sincronizza
param w_SEDE,w_ENTITA,pArr,ARRMIRROR,pLog,pVerbose,w_TIPTRS,w_SERLOG,w_FLUSSO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_PR_DBF
  m.w_PR_DBF=space(254)
  private w_PRITAB
  m.w_PRITAB=space(30)
  private w_CURFILE
  m.w_CURFILE=space(254)
  private w_VALEXP
  m.w_VALEXP=space(254)
  private w_BVALIDATORE
  m.w_BVALIDATORE=.f.
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_CURDB
  m.w_CURDB=space(50)
  private w_CURMIRROR
  m.w_CURMIRROR=space(8)
  private w_IDX
  m.w_IDX=0
  private pArrMir
  m.pArrMir=space(10)
  private w_OK
  m.w_OK=.f.
  private w_LOOP
  m.w_LOOP=0
  private w_BATCHPRE
  m.w_BATCHPRE=space(50)
  private w_BATCHPOST
  m.w_BATCHPOST=space(50)
  private w_ORDDEL
  m.w_ORDDEL=space(1)
  private w_ORDINE
  m.w_ORDINE=space(254)
  private w_ORDDBF
  m.w_ORDDBF=space(254)
  private w_TIPORD
  m.w_TIPORD=space(1)
  private w_BATCHECK
  m.w_BATCHECK=space(50)
  private w_POS
  m.w_POS=0
  private w_ESPR
  m.w_ESPR=space(30)
  private w_NUOVORECORD
  m.w_NUOVORECORD=.f.
  private w_LEN
  m.w_LEN=0
  private w_SUPPCAMPO
  m.w_SUPPCAMPO=space(50)
  private w_ERRMESS
  m.w_ERRMESS=space(254)
  private w_VALORE
  m.w_VALORE=space(254)
  private w_VERTRES
  m.w_VERTRES=0
  private w_FILTROCANC
  m.w_FILTROCANC=space(254)
  private w_FILTRO
  m.w_FILTRO=space(254)
  private w_TMPS
  m.w_TMPS=space(10)
  private w_TMPS
  m.w_TMPS=space(10)
  private w_REC_MSG
  m.w_REC_MSG=space(254)
  private w_DOK
  m.w_DOK=.f.
  private w_AZIONE
  m.w_AZIONE=space(1)
  private w_DATIRIGA
  m.w_DATIRIGA=space(10)
  private w_NEWMIRR
  m.w_NEWMIRR=.f.
  private w_BAGGMIRROR
  m.w_BAGGMIRROR=.f.
  private w_TABELLA
  m.w_TABELLA=space(30)
  private w_DBF
  m.w_DBF=space(254)
  private w_CUR_IDX
  m.w_CUR_IDX=0
  private w_NOM_ARRAY
  m.w_NOM_ARRAY=space(10)
  private w_BKEEPDB
  m.w_BKEEPDB=.f.
  private w_TMPFIELDS
  m.w_TMPFIELDS=space(254)
  private w_LINKDEL
  m.w_LINKDEL=space(254)
  private w_CHIAVECOLL
  m.w_CHIAVECOLL=space(250)
  private w_VERTCURDB
  m.w_VERTCURDB=space(50)
  private w_CURVERTDB
  m.w_CURVERTDB=space(10)
  private w_SECTAB
  m.w_SECTAB = createobject("cp_MemoryCursor","..\LORE\EXE\QUERY\GSLR_SIN.VQR",createobject('cp_getfuncvar'))
* --- WorkFile variables
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private SUENDETT_idx
  SUENDETT_idx=0
  private ENT_MAST_idx
  ENT_MAST_idx=0
  private DETTPUBL_idx
  DETTPUBL_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "sincronizza"
if vartype(__sincronizza_hook__)='O'
  __sincronizza_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'sincronizza('+Transform(w_SEDE)+','+Transform(w_ENTITA)+','+Transform(pArr)+','+Transform(ARRMIRROR)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(w_TIPTRS)+','+Transform(w_SERLOG)+','+Transform(w_FLUSSO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'sincronizza')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if sincronizza_OpenTables()
  sincronizza_Page_1()
endif
cp_CloseFuncTables()
if used('_Curs_SUENDETT')
  use in _Curs_SUENDETT
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'sincronizza('+Transform(w_SEDE)+','+Transform(w_ENTITA)+','+Transform(pArr)+','+Transform(ARRMIRROR)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(w_TIPTRS)+','+Transform(w_SERLOG)+','+Transform(w_FLUSSO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'sincronizza')
Endif
*--- Activity log
if vartype(__sincronizza_hook__)='O'
  __sincronizza_hook__.post()
endif
w_SECTAB.Done()
select (i_oldarea)
return(i_retval)


procedure sincronizza_Page_1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Data sede ed Entit� sincronizza i dati contenuti nell'array
  *     ARRMIRROR conterr�: 
  *     il numero di record aggiornati
  *     il numero di record inseriti
  *     il numero di record eliminati
  *     il numero di entrate in Mirror inserite (solo per allineamento- se pForzaMIrror a .t.)
  *     pLog puntatore all'oggetto per visualizzare log (facoltativo)
  *     pVerbose, se a .t. logga tutte le frasi
  *     w_TIPTRS: tipo granularit� transazione
  *     w_SERLOG: Seriale anagrafica log
  * --- Array passato alla funzione
  *     1 = Nome archivio
  *     2 = DBF di appoggio con i dati
  *     3 = Entit� archivio
  * --- Leggo l'archivio principale dell'entit�
  m.w_ENTITA = Alltrim( m.w_ENTITA )
  m.w_RESULT = ""
  m.w_OK = .T.
  * --- Read from ENT_MAST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ENT_MAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ENT_MAST_idx,2],.t.,ENT_MAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ENBATPRE,ENBATPOS,ENORDDEL,ENORDINE,ENTIPORD,ENBATCHK"+;
      " from "+i_cTable+" ENT_MAST where ";
          +"ENCODICE = "+cp_ToStrODBC(m.w_ENTITA);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ENBATPRE,ENBATPOS,ENORDDEL,ENORDINE,ENTIPORD,ENBATCHK;
      from (i_cTable) where;
          ENCODICE = m.w_ENTITA;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_BATCHPRE = NVL(cp_ToDate(_read_.ENBATPRE),cp_NullValue(_read_.ENBATPRE))
    m.w_BATCHPOST = NVL(cp_ToDate(_read_.ENBATPOS),cp_NullValue(_read_.ENBATPOS))
    m.w_ORDDEL = NVL(cp_ToDate(_read_.ENORDDEL),cp_NullValue(_read_.ENORDDEL))
    m.w_ORDINE = NVL(cp_ToDate(_read_.ENORDINE),cp_NullValue(_read_.ENORDINE))
    m.w_TIPORD = NVL(cp_ToDate(_read_.ENTIPORD),cp_NullValue(_read_.ENTIPORD))
    m.w_BATCHECK = NVL(cp_ToDate(_read_.ENBATCHK),cp_NullValue(_read_.ENBATCHK))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Read from ENT_DETT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ENT_DETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "EN_TABLE"+;
      " from "+i_cTable+" ENT_DETT where ";
          +"ENCODICE = "+cp_ToStrODBC(m.w_ENTITA);
          +" and ENPRINCI = "+cp_ToStrODBC("S");
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      EN_TABLE;
      from (i_cTable) where;
          ENCODICE = m.w_ENTITA;
          and ENPRINCI = "S";
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_PRITAB = NVL(cp_ToDate(_read_.EN_TABLE),cp_NullValue(_read_.EN_TABLE))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Ordinamento cancellati, se N nessuno, se T cancellati in testa quindi se CPCCCHK � vuoto, sono cancellati e vanno in testa
  *     Viceversa per C cancellati in coda
  m.w_ORDDBF = IIF(m.w_ORDDEL="N","",IIF(m.w_ORDDEL="T", "IIF(Empty(Nvl(CPCCCHK,'')),'0','1')", "IIF(Empty(Nvl(CPCCCHK,'')),'1','0')"))
  if m.w_TIPORD="E"
    * --- Se ordinamento libero di tipo Order By lo aggiungo all'ordinamento su cancellati
    *     Nel caso fosse vuoto non devo mettere la virgola
    m.w_ORDDBF = Alltrim(m.w_ORDDBF + IIF(Not Empty(m.w_ORDDBF) AND NOT EMPTY (m.w_ORDINE),"+","")+ m.w_ORDINE)
  endif
  if Empty( m.w_PRITAB )
    if VarType( m.pLog ) ="O"
      AddMsgNL("Nessuna tabella principale per entita %1 ", m.pLog , m.w_ENTITA , ,, ,)
    endif
  else
    * --- Ricerco nell'array, per l'entit� il DBF che contiene l'aggiornamento per
    *     l'archivio principale...
    m.w_PRITAB = Alltrim( m.w_PRITAB )
    m.w_TABELLA = m.w_PRITAB
    sincronizza_Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Non trovo l'archivio principale o non ho aggiornamenti per l'entit�, esco tutto ok
    *     nessuna modifica
    if Empty( m.w_DBF )
      m.w_RESULT = AH_MsgFormat( "Pacchetto dalla sede %1 per l'entit� %2 non presenta struttura dati tabella principale %3 " , m.w_SEDE ,m.w_ENTITA , m.w_PRITAB )
    else
      * --- L'immancabile chiamata alla CP_READXDC...
      =cp_ReadXdc()
      m.w_PR_DBF = m.w_DBF
      * --- Costruisco l'array che utilizzer� per le operazione di scrittura sul database
      *     (archivio principale) e sul mirror 
      * --- 3 colonne, 
      *     1 = nome campo
      *     2 = contenuto corrispondente (valorizzato allo scandire dei record)
      *     3 = se 1 campo chiave, se 2 campo supplementare
      *     ArrMirr per aggiornare Mirror, mentre AggDb per leggere dal Db o dal mirror 
      *     (costruzione where)
      *     ArrCampi contiene tutti i campi con i relativi valori  (colonna 3 tipo verticalizzazione, colonan 4 eventuale funzione dinamica verticalizzazione)
      Dimension pArrMir[1 , 3], pArrDb[1,4], ArrCampi[1,4] 
      m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_PRITAB ,1 ) )
      m.w_IDX = 1
      m.w_LOOP = 1
      do while m.w_LOOP<>0
        m.w_LOOP = At( "," , m.w_CHIAVE )
        if m.w_LOOP>0
          m.w_CAMPO = Left( m.w_CHIAVE , m.w_LOOP -1 )
        else
          m.w_CAMPO = Alltrim( m.w_CHIAVE )
        endif
         
 Dimension pArrMir[ m.w_IDX ,3 ] 
 pArrMir[ m.w_IDX ,1 ] = w_CAMPO 
 pArrMir[ m.w_IDX ,3 ] = 1 
 
 Dimension pArrDb[ m.w_IDX ,4 ] 
 pArrDb[ m.w_IDX ,1 ] = w_CAMPO
        m.w_CHIAVE = Alltrim( Substr( m.w_CHIAVE , m.w_LOOP+1 , Len ( m.w_CHIAVE ) ) )
        m.w_IDX = m.w_IDX + 1
      enddo
      * --- Vado a ricercare eventuali campi supplementari definiti nella Super Entit�...
      * --- Select from SUENDETT
      i_nConn=i_TableProp[SUENDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[SUENDETT_idx,2],.t.,SUENDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select SU_CAMPO from "+i_cTable+" SUENDETT ";
            +" where SUCODICE="+cp_ToStrODBC(m.w_PRITAB)+"";
             ,"_Curs_SUENDETT")
      else
        select SU_CAMPO from (i_cTable);
         where SUCODICE=m.w_PRITAB;
          into cursor _Curs_SUENDETT
      endif
      if used('_Curs_SUENDETT')
        select _Curs_SUENDETT
        locate for 1=1
        do while not(eof())
        m.w_SUPPCAMPO = Alltrim(_Curs_SUENDETT.SU_CAMPO)
        * --- Se il campo non � gia compreso lo aggiungo
        if Not m.w_SUPPCAMPO $ cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_PRITAB ,1 ) )
           
 Dimension pArrMir[ m.w_IDX ,3 ] 
 pArrMir[ m.w_IDX ,1 ] = Alltrim( m.w_SUPPCAMPO ) 
 pArrMir[ m.w_IDX ,3 ] = 2
          m.w_IDX = m.w_IDX + 1
        endif
          select _Curs_SUENDETT
          continue
        enddo
        use
      endif
      * --- All'array delle chiavi aggiungo i campi per gestire l'inserimento sul mirror...
       
 Dimension pArrMir[ m.w_IDX + 4 ,3 ] 
 pArrMir[ m.w_IDX ,1 ] = "PUBCOSED" 
 pArrMir[ m.w_IDX ,2 ] = m.w_SEDE 
 pArrMir[ m.w_IDX ,3 ] = 0 
 pArrMir[ m.w_IDX +1 ,1 ] = "CPCCCHK_PU" 
 pArrMir[ m.w_IDX +1 ,3 ] = 0
       
 pArrMir[ m.w_IDX +2 ,1 ] = "ANCESTOR" 
 pArrMir[ m.w_IDX +2 ,3 ] = 0 
 pArrMir[ m.w_IDX +3 ,1 ] = "AVERSION" 
 pArrMir[ m.w_IDX +3 ,3 ] = 0
      * --- CPCCCHK � chiave del mirror (x velocizzare la query di matching) per
      *     cui va valorizzato...
       
 pArrMir[ m.w_IDX +4 ,1 ] = "CPCCCHK" 
 pArrMir[ m.w_IDX +4 ,3 ] = 0
      * --- Scorro i campi della tabella (XDC) per costruire la frase di aggiornamento..
      m.w_LOOP = 1
      do while m.w_LOOP<= i_Dcx.GetFieldsCount( m.w_PRITAB )
         
 Dimension ArrCampi[ m.w_LOOP , 4 ] 
 ArrCampi[ m.w_LOOP ,1 ] = Alltrim( i_Dcx.GetFieldName( m.w_PRITAB , m.w_LOOP ) ) 
        m.w_VALORE = ""
        m.w_ERRMESS = ""
        m.w_VERTRES = VERTFILT( m.w_SEDE , "P" , m.w_ENTITA , m.w_PRITAB , i_Dcx.GetFieldName( m.w_PRITAB , m.w_LOOP ) , @m.w_VALORE , @m.w_ERRMESS ,@ArrCampi) 
        if m.w_VERTRES=-1
          * --- L'applicazione del filtro verticale � andata in errore, segnalo ed esco
          m.w_RESULT = m.w_ERRMESS
          m.w_OK = .F.
          m.w_LOOP = i_Dcx.GetFieldsCount( m.w_PRITAB )+1
        else
          * --- Questo terzo valore mi dice come gestire il campo (mantengo o meno il valore sul DB)
          *     0 leggo dal file
          *     1 valore fisso
          *     2 mantengo il valore sul DB
          *     3 filtro dinamico
          do case
            case m.w_VERTRES=1
              * --- Ho trovato un filtro verticale lo vado ad applicare 
              if VarType( m.pLog ) ="O" And m.pVerbose
                AddMsgNL("Sincronizzazione per entita %1 per l'archivio secondario %2 applicato filtro verticale campo %3", m.pLog , m.w_ENTITA , m.w_PRITAB , i_Dcx.GetFieldName( m.w_PRITAB , m.w_LOOP ) ,, ,)
              endif
              * --- Ho trovato il valore, lo memorizzo e marco il fatto che questo valore
              *     non va letto dal DBF
               
 ArrCampi[ m.w_LOOP ,2 ] = m.w_VALORE
            case m.w_VERTRES=3 and not empty( m.w_VALORE )
              * --- Ho un filtro verticale dinamico...
               
 ArrCampi[ m.w_LOOP ,4 ] = m.w_VALORE
          endcase
          ArrCampi[ m.w_LOOP ,3 ] = m.w_VERTRES
          m.w_LOOP = m.w_LOOP + 1
        endif
      enddo
      * --- Metto il CPCCCHK finale
       
 Dimension ArrCampi[ m.w_LOOP ,4 ] 
 ArrCampi[ m.w_LOOP ,1 ] = "CPCCCHK" 
 ArrCampi[ m.w_LOOP ,3 ] = 0
      if m.w_OK
        * --- Creo un cursore con all'interno le informazioni legate alle tabelle
        *     secondarie
        w_SECTAB.FillFromQuery()
        * --- Creo le strutture per gestire i filtri verticali delle tabelle collegate..
        * --- Costruzione filtri verticali per archivi collegati.
        *     Questa attivit� va svolta in modo preliminare per ottimizzare le 
        *     letture dei filtri "fissi" e necessaria per gestire il mantenimento del
        *     dato se l'archivio secondario � aggiornato tramite il meccanismo della
        *     cancellazione inserimento. La cancellazione deve essere svolta prima di tutti
        *     gli archivi in ordine decrescente e poi si passer� all'inserimento degli archivi nell'
        *     ordine contrario.
        w_SECTAB.GoTop()
        do while Not m.w_SECTAB.Eof()
          * --- Definisce la struttura della tabella collegata andando anche a 
          *     determinare per quali campi vi � un filtro verticale.
          *     
          *     Il risultato � messo nell'array g_ar_<tabella>
           
 Local L_MacroDim 
 L_MacroDim = "g_Arr_"+Alltrim( m.w_SECTAB.EN_TABLE ) 
 Dimension &L_MacroDim.[1,3]
          * --- w_DATIRIGA contiene i dati che verranno memorizzati sul DB (le funzioni inserite come filtri verticali accedono ai dati su cui effettuano il filtro tramite questo parametro)
          m.w_RESULT = DEFSTRUC( m.w_SEDE , m.w_ENTITA , alltrim(m.w_SECTAB.EN_TABLE) , @&L_MacroDim , m.pLog, m.pVerbose )
          if Not Empty( m.w_RESULT )
            m.w_RESULT = AH_MsgFormat( "Definizione strutture da sede %1 per entita %2 , archivio secondario %3 ha generato errore %4 " , m.w_SEDE ,m.w_ENTITA , Alltrim( m.w_SECTAB.EN_TABLE ) , m.w_RESULT )
            m.w_OK = .F.
            Exit
          else
            if VarType( m.pLog ) ="O" And m.pVerbose
              AddMsgNL("Creata struttura sede %1 per entita %2, elemento %3 tabella secondaria %4", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG , Alltrim(m.w_SECTAB.EN_TABLE) , ,)
            endif
          endif
          w_SECTAB.Skip(1)
        enddo
        * --- Array che conterr� i valori sul DB da riportare per i filtri verticali...
        *     prima colonna nome tabella collegata, seconda cursore con i dati
        *     originali...
        Dimension aCurVert[1,2]
        if m.w_OK
          * --- Gestione errore select
           
 Local L_OldError, L_Messaggio, L_FraseSql 
 L_Messaggio="" 
 L_OldError = On("Error") 
 On Error L_Messaggio=Message() 
 L_FraseSql = ""
          * --- --Elabora solo i cancellati oppure elabora gli altri tipi di record
          * --- Se nei parametri della logistica remota � specificata la sincronizzazione per flussi allora si hanno i seguenti casi :
          *     w_FLUSSO=0  -> si elaborano tutti i record dell entit�
          *     w_FLUSSO=1  -> si elaborano i record cancellati dell entit�
          *     w_FLUSSO=2  -> si elaborano i record non cancellati dell entit�
          do case
            case m.w_FLUSSO=0
              m.w_FILTROCANC = ""
            case m.w_FLUSSO=1
              m.w_FILTROCANC = " EMPTY (NVL(  CPCCCHK, '')) "
            case m.w_FLUSSO=2
              m.w_FILTROCANC = " NOT EMPTY (NVL(  CPCCCHK, '')) "
          endcase
          * --- Ho un aggiornamento, scorro il DBF e per ogni record mi chiedo cosa fare..
          *     Prima applico il filtro Orizzontale utilizzando una SELECT
          * --- Read from DETTPUBL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[DETTPUBL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[DETTPUBL_idx,2],.t.,DETTPUBL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPFILTRO"+;
              " from "+i_cTable+" DETTPUBL where ";
                  +"DP__SEDE = "+cp_ToStrODBC(m.w_SEDE);
                  +" and DP__TIPO = "+cp_ToStrODBC("P");
                  +" and DPCODENT = "+cp_ToStrODBC(m.w_ENTITA);
                  +" and DPCODARC = "+cp_ToStrODBC(m.w_PRITAB);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPFILTRO;
              from (i_cTable) where;
                  DP__SEDE = m.w_SEDE;
                  and DP__TIPO = "P";
                  and DPCODENT = m.w_ENTITA;
                  and DPCODARC = m.w_PRITAB;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_FILTRO = NVL(cp_ToDate(_read_.DPFILTRO),cp_NullValue(_read_.DPFILTRO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if USED ( JUSTSTEM(m.w_PR_DBF) )
            SELECT ( JUSTSTEM(m.w_PR_DBF) ) 
 
          else
            Use ( m.w_PR_DBF ) in 0
          endif
          Go Top
          if not Empty( m.w_FILTRO ) And m.w_FILTRO<>"1=1"
            m.w_TMPS = Sys(2015)
            Local L_FILTRO, L_Alias, L_Ordine, L_CampoOrd, L_FreeOrder 
 
 L_FILTRO=Alltrim(m.w_FILTRO) + IIF( m.w_FLUSSO=0 , "" , " AND " +m.w_FILTROCANC ) 
 L_Alias=m.w_PRITAB 
 L_Ordine = m.w_ORDDBF 
 L_CampoOrd = Sys(2015) 
 L_FreeOrder = Alltrim(m.w_ORDINE)
            if Not Empty(m.w_ORDDBF)
              if Not Empty(m.w_ORDINE) And m.w_TIPORD="O"
                * --- Se presente anche l'ordinamento libero di tipo Order by devo aggiungerlo
                L_FraseSql ="Select  *, " + L_Ordine + " as "+ L_CampoOrd + " From  " + m.w_PR_DBF + " " + L_Alias + "  Where " + L_FILTRO +" Into Cursor " + m.w_TMPS + " Order by " + L_CampoOrd + "," + L_FreeOrder + " NoFilter"
              else
                * --- Se c'� l'ordinamento libero di tipo espressione e ordinamento per cancellati aggiungo il campo
                *     per ordinare
                L_FraseSql ="Select  *, " + L_Ordine + " as "+ L_CampoOrd + " From  " + m.w_PR_DBF + " " + L_Alias + "  Where " + L_FILTRO +" Into Cursor " + m.w_TMPS + " Order by " + L_CampoOrd + " NoFilter"
              endif
            else
              if Not Empty(m.w_ORDINE) And m.w_TIPORD="O"
                * --- Solo ordinamento libero di tipo order by
                L_FraseSql ="Select  * From  " + m.w_PR_DBF + " " + L_Alias + "  Where " + L_FILTRO +" Into Cursor " + m.w_TMPS + " Order by " + L_FreeOrder + " NoFilter"
              else
                * --- Nessun ordinamento
                L_FraseSql ="Select  * From  " + m.w_PR_DBF + " " + L_Alias + "  Where " + L_FILTRO +" Into Cursor " + m.w_TMPS + " NoFilter"
              endif
            endif
            * --- Eseguo tramite macro cos� posso catturare la frase per Verbose
            &L_FraseSql
            * --- Chiudo il cursore utilzizato per determinare i dati..
             
 Select ( JUSTSTEM ( m.w_PR_DBF ) ) 
 Use
            m.w_CURFILE = m.w_TMPS
          else
            * --- Se c'� solo ordinamento
            if Not Empty(m.w_ORDDBF) Or (Not Empty(m.w_ORDINE) And m.w_TIPORD<>"N")
              m.w_TMPS = Sys(2015)
              Local L_Alias, L_Ordine, L_CampoOrd, L_FreeOrder , L_FILTRO 
 
 L_FILTRO= IIF( m.w_FLUSSO=0 , "" , " where " +m.w_FILTROCANC ) 
 L_Alias=m.w_PRITAB 
 L_Ordine = m.w_ORDDBF 
 L_CampoOrd = Sys(2015) 
 L_FreeOrder = Alltrim(m.w_ORDINE)
              * --- Se c'� l'ordinamento aggiungo un campo sul quale devo fare l'ordine
              if Not Empty(m.w_ORDINE) And m.w_TIPORD="O" And Not Empty(m.w_ORDDBF)
                * --- E' presente sia l'ordinamento per cancellati che quello libero di tipo  Order by
                L_FraseSql ="Select  *, &L_Ordine as &L_CampoOrd From ( w_PR_DBF ) &L_Alias " + L_FILTRO + " Into Cursor ( w_TMPS ) Order by  &L_CampoOrd, &L_FreeOrder NoFilter"
              else
                if Not Empty(m.w_ORDDBF)
                  * --- E' presente o l'ordinamento per cancellati o quello libero con espressione o tutti e due
                  *     che devono creare un nuovo campo sul quale fare l'ordinamento
                  L_FraseSql = "Select  *, &L_Ordine as &L_CampoOrd From ( w_PR_DBF ) &L_Alias  " + L_FILTRO + "  Into Cursor ( w_TMPS ) Order by &L_CampoOrd NoFilter"
                else
                  * --- E' presente solo l'ordinamento libero di tipo Order by
                  L_FraseSql ="Select  * From ( w_PR_DBF ) &L_Alias  " + L_FILTRO + "  Into Cursor ( w_TMPS ) Order by &L_FreeOrder NoFilter"
                endif
              endif
              &L_FraseSql
              * --- Chiudo il cursore utilzizato per determinare i dati..
               
 Select ( JUSTSTEM ( m.w_PR_DBF ) ) 
 Use
              m.w_CURFILE = m.w_TMPS
            else
              * --- Se nei parametri della logistica remota � specificata la sincronizzazione a flussi allora si filtrano sui recor (nel flusso 1 vengono sincronizzati i cancellati e nel flusso 2 vengono sincronizzati i non cancellati)
              if m.w_FLUSSO <> 0
                Local L_Alias , L_FILTRO 
 L_FILTRO= " where " +m.w_FILTROCANC 
 L_Alias=m.w_PRITAB
                m.w_TMPS = Sys(2015)
                L_FraseSql ="Select  * From ( w_PR_DBF ) &L_Alias  " + L_FILTRO + "  Into Cursor ( w_TMPS ) NoFilter"
                &L_FraseSql
                * --- Chiudo il cursore utilzizato per determinare i dati..
                 
 Select ( JUSTSTEM ( m.w_PR_DBF ) ) 
 Use
                m.w_CURFILE = m.w_TMPS
              else
                * --- Altrimenti considero direttamente il DBF
                m.w_CURFILE = JUSTSTEM(m.w_PR_DBF)
              endif
            endif
          endif
          * --- Ripristino errore
          On Error &L_OldError
          * --- Visualizzo la frase
          if Not Empty(L_FraseSql)
            if VarType( m.pLog ) ="O" And m.pVerbose
              * --- Se verbose attivo visualizzo frase e nel caso di errore anche il messaggio
              if Empty(L_Messaggio)
                AddMsgNL("Frase: %1", m.pLog, L_FraseSql)
              else
                AddMsgNL("Frase: %1%0Messaggio: %2", m.pLog, L_FraseSql, L_Messaggio)
              endif
            else
              * --- Se c'� un errore visualizzo comunque frase e errore
              if VarType( m.pLog ) ="O" And Not Empty(L_Messaggio)
                AddMsgNL("Frase: %1%0Messaggio: %2", m.pLog, L_FraseSql, L_Messaggio)
              endif
            endif
          endif
          * --- Determino l'espressione di validazione. Converto evantuale espressione
          *     per il database VFP (pacchetti)
          m.w_VALEXP = cp_SetSQLFunctions( VALIDEXP( m.w_PRITAB ) , "VFP" )
          * --- Al risultato modifico l'Alias....
          m.w_VALEXP = StrTran( m.w_VALEXP , m.w_PRITAB+"." , m.w_CURFILE+"." , 1 ,-1 , 1)
          if m.w_OK
            do while Not Eof ( m.w_CURFILE )
              * --- Rimetto w_OK a .T. in modo che se la transazione � per Istanza se fallisce un giro 
              *     al secondo ripeta le operazioni comunque
              if m.w_TIPTRS="I"
                m.w_OK = .T.
              endif
              * --- Try
              sincronizza_Try_0294F948()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                if m.w_TIPTRS="I"
                  * --- Ripristino i valori dell'istanza
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                else
                  * --- Se non per istanza notifico solo l'errore
                  m.w_OK = .F.
                endif
              endif
              * --- End
            enddo
          endif
           
 Select ( m.w_CURFILE ) 
 Use
          * --- Rimuovo i cursori relativi ad eventuali filtri verticali del tipo
          *     mantieni valori sul DB
          m.w_LOOP = 1
          do while m.w_LOOP <= Alen( aCurVert , 1 ) 
            if Type("aCurVert[ m.w_LOOP , 2 ]")="C"
               
 Select ( aCurVert[ m.w_LOOP , 2 ] ) 
 Skip
            endif
            m.w_LOOP = m.w_LOOP + 1
          enddo
          w_SECTAB.Destroy()
        endif
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc
  proc sincronizza_Try_0294F948()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  if m.w_TIPTRS="I"
    * --- Se transazione per istanza apro la transazione e la chiudo dopo la sincronizzazione del record riferito
    *     alla tabella principale. Implica quindi la sincronizzazione anche dei record relativi alle tabelle collegate
    * --- begin transaction
    cp_BeginTrs()
  endif
  * --- Costruisco una frase per leggere il cpccchk sull'archivio..
  *     Es. Select CPCCCHK From DEMOCONTI Where ANTIPCON=Dbf.Antipcon And ANCODICE=Dbf.Ancodice
  * --- Contiene l'espressione che identifica il dato (x messaggistica)
  m.w_REC_MSG = ""
  * --- w_DOK mi dice se il dato � aggiornabile (ha superato tutte le verifiche ed � accettabile)
  m.w_DOK = .T.
  * --- Azione da intraprendere per i dati collegati alla tabella princiaple,
  *     rispecchia l'azione svolta sul dato (I=Insert, U=Update, 'D' delete, N nessuna
  *     azione da intraprendere). Se il dato sulla tabella principale non � da aggiornare
  *     i dati collegati non sono passati!
  m.w_AZIONE = "N"
  * --- Assegno i valori sul cursore degli array delle chiavi
  m.w_LOOP = 1
  do while m.w_LOOP<=Alen( m.pArrMir , 1)
    if m.w_LOOP<=Alen( pArrDb ,1 )
       
 pArrDb[ m.w_LOOP ,2 ] = Eval(m.w_CURFILE+"."+pArrDb[ m.w_LOOP ,1 ] )
      m.w_REC_MSG = m.w_REC_MSG + IIF( Empty( m.w_REC_MSG ) , "" ," And ") +pArrDb[ m.w_LOOP ,1 ] + "="+Cp_ToStrOdbc( pArrDb[ m.w_LOOP ,2 ] )
    endif
    * --- Se campo chiave o supplementare lo aggiungo..
    if pArrMir[ m.w_LOOP ,3 ] <>0
       
 pArrMir[ m.w_LOOP ,2 ] = Eval(m.w_CURFILE+"."+pArrMir[ m.w_LOOP , 1 ] )
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  if m.w_OK
    m.w_LOOP = 1
    do while m.w_LOOP<= i_Dcx.GetFieldsCount( m.w_PRITAB )
      * --- Se filtro verticale a valore fisso non leggo dal pacchetto...
      do case
        case ArrCampi[ m.w_LOOP , 3 ] = 0
           
 ArrCampi[ m.w_LOOP , 2 ] = Eval(m.w_CURFILE+"."+i_Dcx.GetFieldName( m.w_PRITAB , m.w_LOOP ))
        case ArrCampi[ m.w_LOOP ,3 ] =3 and not empty( ArrCampi[ m.w_LOOP , 4 ] )
          * --- Se filtro dinamico valuta la funzione (da aggiungere i parametri)
          *     ===================================================
          *     ===================================================
          * --- w_DATIRIGA contiene i dati che verranno memorizzati sul DB (le funzioni inserite come filtri verticali accedono ai dati su cui effettuano il filtro tramite questo parametro)
          Select( m.w_CURFILE ) 
 SCATTER NAME m.w_DATIRIGA
          if btrserr=.F.
            m.w_ESPR = alltrim(ArrCampi[ m.w_LOOP , 4 ])
            ArrCampi[ m.w_LOOP , 2 ] = Eval( strtran (alltrim(ArrCampi[ m.w_LOOP , 4 ]) , "(" , "(w_DATIRIGA ,pLog, pVerbose , "))
            if btrserr=.T.
              AddMsgNL("La valutazione dell' espressione %1 ha generato l' errore:%2",m.pLog, alltrim(m.w_ESPR) , Message())
            endif
          endif
          * --- Se il filtro verticale modifica un campo chiave si aggiorna l array delle chiavi (la vecchia chiave viene memorizzata in pArrDb[  ,3 ]
          m.w_POS = 1
          do while m.w_POS<=ALEN (pArrDb,1)
            if pArrDb[ m.w_POS,1 ] =ArrCampi[ m.w_LOOP , 1 ] 
              pArrDb[ m.w_POS,4 ] =pArrDb[ m.w_POS,2 ] 
 pArrDb[ m.w_POS,2 ] = ArrCampi[ m.w_LOOP , 2 ] 
            endif
            m.w_POS = m.w_POS + 1
          enddo
          * --- Se filtro verticale aggiorna ARRMIR
          m.w_POS = 1
          do while m.w_POS<=ALEN (m.pArrMir,1)
            if pArrMir[ m.w_POS,1 ] =ArrCampi[ m.w_LOOP , 1 ] 
              pArrMir[ m.w_POS,2 ] = ArrCampi[ m.w_LOOP , 2 ] 
            endif
            m.w_POS = m.w_POS + 1
          enddo
      endcase
      m.w_LOOP = m.w_LOOP + 1
    enddo
    * --- Completo la frase di lettura.sul database
    *     - w_CURDB => DATI LETTI SUL DATABASE
    *     - w_CURMIRROR => DATI LETTI SUL MIRROR
    *     - w_CURFILE => DATI LETTI DAL FILE INVIATOMI
    m.w_CURDB = READTABLE( m.w_PRITAB , "CPCCCHK" , @pArrDb , m.pLog, m.pVerbose )
    if Empty( m.w_CURDB )
      m.w_RESULT = AH_MsgFormat( "Sincronizzazione da sede %1, entit� %2 ha generato errore " , m.w_SEDE , m.w_ENTITA ,, )
      m.w_OK = .F.
    else
      if VarType( m.pLog ) ="O" And m.pVerbose
        AddMsgNL("Sincronizza con sede %1 per entita %2 ", m.pLog , m.w_SEDE , m.w_ENTITA , ,, ,)
      endif
      * --- Verifico se la sede ricevente � validatore del dato...
      *     La validazione avviene sul dato in entrata...
      m.w_BVALIDATORE = Eval( m.w_VALEXP )
      * --- Scorro i campi della tabella (XDC) per costruire la frase di aggiornamento..
      * --- Metto il CPCCCHK finale
       
 ArrCampi[ m.w_LOOP ,2 ] = Eval(m.w_CURFILE+".CPCCCHK" ) 
      * --- Elemento inserito, se sono il validatore verifico se presente sul
      *     mirror il dato (potrebbe essere stato in precedenza cancellato)
      m.w_CURMIRROR = READMIRR( m.w_PRITAB , "*", @pArrDb , m.pLog , m.pVerbose )
      if Empty( m.w_CURMIRROR )
        m.w_RESULT = AH_MsgFormat( "Errore lettura mirror archivio %1" , m.w_PRITAB , ,, )
        m.w_OK = .F.
      else
        m.w_NEWMIRR = Eof( m.w_CURMIRROR ) And Bof( m.w_CURMIRROR ) 
        * --- Possono accadere tre cose:
        *     a) l'elemento � stato inserito (Cursore creato dal database vuoto)
        *     b) l'elemento � stato eliminato (CPCCCHK null sul DBF e cursore pieno), questa considerazione 
        *     b1 introduce un quarto caso, CPCCCCHK null e cursore vuoto => elemento eliminato in entrambi i database
        *     c) l'elemento � stato modificato (CPCCCHK diversi)
        do case
          case RecCount( m.w_CURDB ) = 0 And Not IsNull( Eval(m.w_CURFILE+".CPCCCHK") )
            * --- Verifico se ho almeno un record...
            if m.w_NEWMIRR
              * --- Nessun record posso inserire...
              sincronizza_Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              if m.w_BVALIDATORE
                * --- Il record non � inseribile gestisco l'errore con il log...
                gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 1, m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
                m.w_DOK = .f.
              else
                * --- Se non sono validatore accetto sempre il nuovo record...
                sincronizza_Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          case RecCount( m.w_CURDB ) <> 0 And IsNull( Eval(m.w_CURFILE+".CPCCCHK") )
            * --- Lancio il programma indicato come Preroutine nelle Entit�
            LTrsOk = .T.
            if Not empty(m.w_BATCHECK)
              * --- GSAR_BAD( .Null. , w_Azione, w_Serial, .Null., @LTrsOk )
              l_PrgCheck = Alltrim(m.w_BATCHECK)+"( .Null., 'D', @ArrCampi, .Null., @LTrsOk)"
              m.w_RESULT = &l_PrgCheck
              if Not Empty(m.w_Result)
                * --- Se ci sono errori/worning devo eseguire il log in modo che salvi i dati nei DBF
                gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
              endif
            endif
            if Not Empty(m.w_BATCHPRE) And LTrsOk
              l_Prg = Alltrim(m.w_BATCHPRE)+"( @ArrCampi, 'Pre', 'D', @LTrsOk)"
              m.w_RESULT = &l_Prg
              if Not Empty(m.w_Result)
                * --- Se ci sono errori/worning devo eseguire il log in modo che salvi i dati nei DBF
                gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
              endif
            endif
            if LTrsOk
              * --- Se i controlli nel batch sono andati a buon fine svuoto il messaggio in modo
              *     che la transazione venga chiusa correttamente, altrimenti emetto l'errore 
              *     e fallisce la transazione.
              *     w_Result potrebbe essere comunque pieno con errori di tipo Worning che per�
              *     non devono fare fallire la transazione. E' la variabile LTrsOk che indica questo
              m.w_RESULT = ""
            else
              * --- Se deve chiudere la transazione...
              m.w_OK = .F.
              * --- in w_Result metto solo un messaggio generico poich� la gest_log e gendblog
              *     hanno gi� inserito lo stesso messaggio. Altrimenti sarabbe raddoppiato
              m.w_RESULT = Ah_MsgFormat("Consultare log sincronizzazione")
            endif
            if m.w_BVALIDATORE
              sincronizza_Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if m.w_OK AND m.w_DOK
              * --- Elimino dal database
              m.w_AZIONE = "D"
              sincronizza_Page_8()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if m.w_OK
                if Not Empty( DELETETABLE( m.w_PRITAB , @pArrDb ,m.pLog,m.pVerbose ) )
                  m.w_RESULT = AH_MsgFormat( "Comando Delete su archivio %1 ha generato errore " , m.w_PRITAB, )
                  m.w_OK = .F.
                else
                  if VarType( m.pLog ) ="O" And m.pVerbose
                    AddMsgNL("Sincronizza con sede %1 per entita %2, elemento %3 eliminato", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG ,, ,)
                  endif
                  sincronizza_Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          case Eval(m.w_CURDB+".CPCCCHK") <> Eval(m.w_CURFILE+".CPCCCHK") 
            sincronizza_Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          otherwise
            if VarType( m.pLog ) ="O" And m.pVerbose
              AddMsgNL("Sincronizza con sede %1 per entita %2, elemento %3 Nessuna azione da intraprendere sui dati", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG ,, ,)
            endif
            * --- w_BAGGMIRROR  potrebbe essere evitata, per maggior chiarezza di
            *     codice � lascita per distinguere meglio i vari casi gestiti...
            m.w_BAGGMIRROR = .F.
            * --- Se il dato attuale non ha ancestor e quanto mi arriva dal file ha l'ancestor
            *     (se sono qua significa che entrambi hanno il medesimo CPCCCHK) vuol
            *     dire che il dato locale ha tutto ma non ha i dati del validatore, per cui
            *     aggiorno i dati del mirror in modo forzato
            if Empty( Nvl(Eval(m.w_CURMIRROR+".ANCESTOR"),"") ) And Not Empty( nvl(Eval(m.w_CURFILE+".ANCESTOR"),"") )
              m.w_BAGGMIRROR = .T.
            else
              * --- Non sono il validatore del dato
              if Not m.w_BVALIDATORE
                * --- Abbiamo entrambi i dati sul mirror, potrebbe essere il caso in cui io non
                *     sono validatore del dato ed il record mi � stato re inviato dal validatore
                *     validando la mia modifica:
                *     A invia il dato ed A � il validatore
                *     B lo riceve e lo modifica re inviandolo ad A
                *     A lo riceve e lo valida (non muta il CPCCCHK) e lo re invia con una
                *       nuova versione a B
                *     B lo riceve hanno entrambi il solito CPCCCHK B deve solo aggiornare
                *        i dati sul mirror (versione, sede)
                * --- Il dato mi arriva da un validatore..
                if Nvl(Eval(m.w_CURFILE+".CPCCCHK"),REPLICATE("X",11))=nvl(Eval(m.w_CURFILE+".ANCESTOR"),REPLICATE("X",11))
                  * --- Il validatore mi invia una versione pi� aggiornata della mia, aggiorno il mirror
                  if Nvl(Eval(m.w_CURMIRROR+".AVERSION"),0)<Eval(m.w_CURFILE+".AVERSION")
                    m.w_BAGGMIRROR = .T.
                  else
                    * --- Nel caso in cui la versione attuale
                    *     (di un non validatore) � superiore
                    *     alla versione inviata da un validatore
                    *     e i CPCCCHK coincidono � da considerarsi
                    *     un caso impossibile (presenza di 2 validatori ?). 
                    *     Si sceglie di non gestire il caso...
                  endif
                endif
              endif
            endif
            if m.w_BAGGMIRROR
              sincronizza_Page_7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if m.w_OK
                m.ARRMIRROR (4) = m.ARRMIRROR (4) + 1
              endif
            endif
        endcase
        * --- Sincronizzazione eventuali archivi secondari legati alla tabella principale
        *     definiti all'interno dell'Entit�.
        *     Solo se tutto ok (w_OK) e se il dato � aggiornabile (w_DOK)
        if m.w_OK And m.w_DOK and ( m.w_AZIONE="I" Or m.w_AZIONE="U" )
          sincronizza_Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Lancio programma definito come PostRoutine nelle Entit�
        if m.w_Azione$"UI" And ( Not Empty(m.w_BATCHPOST) Or Not Empty(m.w_BATCHECK) ) And m.w_OK
          LTrsOk = .T.
          if Not empty(m.w_BATCHECK)
            * --- Lancio batch dei controlli prima di chiudere lanciare il batch post sincronizza
            l_PrgCheck = Alltrim(m.w_BATCHECK)+"( .Null.,'"+ Alltrim(m.w_AZIONE) + "', @ArrCampi, .Null., @LTrsOk)"
            m.w_RESULT = &l_PrgCheck
            if Not Empty(m.w_Result)
              * --- Se ci sono errori/worning devo eseguire il log in modo che salvi i dati nei DBF
              gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
            endif
          endif
          if Not empty(m.w_BATCHPOST) And LTrsOk
            * --- Lancio batch post sincronizzazione
            l_Prg = Alltrim(m.w_BATCHPOST)+"( @ArrCampi, 'Post' ,'"+ Alltrim(m.w_AZIONE) + "', @LTrsOk )"
            m.w_RESULT = &l_Prg
            if Not Empty(m.w_Result)
              * --- Se ci sono errori comunque eseguo il log per creare i DBF di salvataggio
              gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA, @m.pArr, m.w_SERLOG, m.pLog )
            endif
          endif
          if LTrsOk
            * --- Se i controlli nel batch sono andati a buon fine svuoto il messaggio in modo
            *     che la transazione venga chiusa correttamente, altrimenti emetto l'errore 
            *     e fallisce la transazione.
            *     w_Result potrebbe essere comunque pieno con errori di tipo Worning che per�
            *     non devono fare fallire la transazione. E' la variabile LTrsOk che indica questo
            m.w_RESULT = ""
          else
            * --- Se deve chiudere la transazione...
            m.w_OK = .F.
            * --- in w_Result metto solo un messaggio generico poich� la gest_log e gendblog
            *     hanno gi� inserito lo stesso messaggio. Altrimenti sarabbe raddoppiato
            m.w_RESULT = Ah_MsgFormat("Consultare log sincronizzazione")
          endif
        endif
        if m.w_OK
          * --- Solo se va tutto bene aggiorno l'array con i record aggiornati
          do case
            case m.w_Azione="I"
              m.ARRMIRROR (2) = m.ARRMIRROR (2) + 1
            case m.w_Azione="U"
              m.ARRMIRROR (1) = m.ARRMIRROR (1) + 1
            case m.w_Azione="D"
              m.ARRMIRROR (3) = m.ARRMIRROR (3) + 1
          endcase
        endif
         
 Select ( m.w_CURMIRROR ) 
 Use
      endif
      * --- Rimuove il cursore creato..
      if Used( m.w_CURDB )
         
 Select( m.w_CURDB ) 
 Use
      endif
    endif
  endif
   
 Select ( m.w_CURFILE ) 
 Skip
  if Not m.w_OK
    * --- Se errore metto una raise
    * --- Raise
    i_Error="Error"
    return
  endif
  if m.w_TIPTRS="I"
    * --- Chiudo la transazione per istanza.
    * --- commit
    cp_EndTrs(.t.)
  endif
    return


procedure sincronizza_Page_2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Ricerca per entit� ed archivio il corrispondente DBF
  m.w_DBF = LOOK_DBF( m.w_ENTITA, m.w_TABELLA , @m.pArr )
endproc


procedure sincronizza_Page_3
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Inserisco all'interno del database il dato ricevuto...
  *     Invio al database...
  if Not Empty( INSERTTABLE( m.w_PRITAB , @ArrCampi ,m.pLog,m.pVerbose ) )
    m.w_RESULT = AH_MsgFormat( "Comando Insert su archivio %1 ha generato errore " , m.w_PRITAB, )
    gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
    m.w_OK = .F.
  else
    if VarType( m.pLog ) ="O" And m.pVerbose
      AddMsgNL("Sincronizza con sede %1 per entita %2, elemento %3 inserito", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG ,, ,)
    endif
    m.w_NUOVORECORD = .T.
    sincronizza_Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    m.w_NUOVORECORD = .F.
    m.w_AZIONE = "I"
  endif
endproc


procedure sincronizza_Page_4
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato modificato..
  if m.w_BVALIDATORE
    sincronizza_Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  else
    * --- Non sono validatore
    if Nvl(Eval(m.w_CURFILE+".CPCCCHK"),REPLICATE("X",11))<>nvl(Eval(m.w_CURFILE+".ANCESTOR"),REPLICATE("X",11))
      * --- Non mi arriva da un validatore
      sincronizza_Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Arriva da un validatore, verifico la versione..
      if Nvl(Eval(m.w_CURFILE+".AVERSION"),0)<=Nvl( Eval(m.w_CURMIRROR+".AVERSION"), 0 )
        * --- Un validatore mi ha mandato una versione del file pi� vecchia della mia
        *     rifiuto il record..
        gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 6, m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
        m.w_DOK = .f.
      else
        * --- Ho 3 situazioni possibili:
        *     a) il cpccchk sul mirror � nullo (significa che non ho il mirror, quindi non ho mai pubblicato ne mai ricevuto)
        *     b) il cpccchk sul mirror � vuoto (space(10)), significa che non ho mai pubblicato e che ho ricevuto almeno una volta
        *     c) il CPCCCHK � pieno significa che ho pubblicato
        *     
        *     Se a) come faccio a determinare se il dato � stato modificato rispetto all'ultima sincronizzazione ? Per me � sempre da sovrascrivere
        *     
        *     Se b) � SPACE(10)  allora chi riceve non ha mai pubblicato, per� ha sicuramente gia ricevuto il file (la presenza del mirror prova questo), quindi se CPCCCHK_PU 
        *     � diverso da CPCCCHK attuale allora do il messaggio altrimenti non segnalo nulla (significa che il dato � identico a quanto ricevuto in pubblicazione)
        *     
        *     Se c) confronto il CPCCCHK sul dato e quello sul mirror, se diversi, verifico che lo sia anche rispetto al CPCCCHK_PU se pieno.
        if Eval( m.w_CURDB +".CPCCCHK")<> IIF ( NOT EMPTY ( NVL (Eval(m.w_CURMIRROR+".CPCCCHK"), " ")) , Eval(m.w_CURMIRROR+".CPCCCHK") , IIF ( NOT EMPTY (Nvl(Eval(m.w_CURMIRROR+".CPCCCHK_PU") , " ")) , Eval(m.w_CURMIRROR+".CPCCCHK_PU") , Eval( m.w_CURFILE +".CPCCCHK") ) )
          * --- Segnalo la sovrascrittura...
          if VarType( m.pLog ) ="O"
            AddMsgNL("Sincronizza con sede %1 per entita %2, elemento %3 sovrascritto", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG ,, ,)
          endif
          gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 7 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
        endif
        sincronizza_Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endif
endproc


procedure sincronizza_Page_5
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Verifico la validazione...
  do case
    case Nvl(Eval(m.w_CURDB+".CPCCCHK"),REPLICATE("X",11))<>nvl(Eval(m.w_CURFILE+".ANCESTOR"),REPLICATE("X",11))
      * --- Il dato ha una validazione non riconosciuta
      gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 2, m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
      m.w_DOK = .f.
    case Nvl(Eval(m.w_CURMIRROR+".AVERSION"),0)<>Eval(m.w_CURFILE+".AVERSION")
      * --- Il dato ha una versione non valida
      gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 3 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
      m.w_DOK = .f.
    otherwise
      * --- La validazione � buona, verifico che il dato non sia stato modificato
      *     dall'ultima pubblicazione
      if Nvl( Eval( m.w_CURDB +".CPCCCHK"),REPLICATE("X",11))<>Nvl( Eval(m.w_CURMIRROR+".CPCCCHK_PU") ,REPLICATE("X",11))
        * --- Il dato � stato modificato rispetto all'ultima ricezione, ho un conflitto..
        if m.w_BVALIDATORE
          * --- Il validatore ha modificato il dato ma non lo ha ancora pubblicato, il
          *     le modifiche che mi arrivano sono inaccettabili
          gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 4, m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
          m.w_DOK = .f.
        else
          * --- Il dato � stato modificato sul database rispetto al mirror, inoltre chi mi invia
          *     il dato non � il validatore, quindi due sedi non validatrici hanno portato
          *     sulla stessa versione una modifica, ho un conflitto non accetto il dato
          gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , "", 5 , m.w_TABELLA, @m.pArr, m.w_SERLOG, m.pLog )
          m.w_DOK = .f.
        endif
      else
        sincronizza_Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
  endcase
endproc


procedure sincronizza_Page_6
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Tutto ok, posso modificare il dato sul database...
  Local TestMacro
  LTrsOk = .T.
  if Not empty(m.w_BATCHECK)
    * --- Lancio il batch dei controlli
    *     Se modifico considero la cosa come una cancellazione seguita da
    *     un'inserzione....
    l_PrgCheck = Alltrim(m.w_BATCHECK)+"( .Null., 'D', @ArrCampi, .Null., @LTrsOk)"
    m.w_RESULT = &l_PrgCheck
    if Not Empty(m.w_Result)
      * --- Se ci sono errori/worning devo eseguire il log in modo che salvi i dati nei DBF
      gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
    endif
  endif
  if Not Empty(m.w_BATCHPRE) And LTrsOk
    l_Prg = Alltrim(m.w_BATCHPRE)+"( @ArrCampi, 'Pre' ,'U', @LTrsOk)"
    m.w_RESULT = &l_Prg
    * --- Se ci sono errori comunque eseguo il log per creare i DBF di salvataggio
    if Not Empty(m.w_Result)
      gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0, m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
    endif
  endif
  if LTrsOk
    * --- Se i controlli nel batch sono andati a buon fine svuoto il messaggio in modo
    *     che la transazione venga chiusa correttamente, altrimenti emetto l'errore 
    *     e fallisce la transazione.
    *     w_Result potrebbe essere comunque pieno con errori di tipo Worning che per�
    *     non devono fare fallire la transazione. E' la variabile LTrsOk che indica questo
    m.w_RESULT = ""
  else
    * --- Se deve chiudere la transazione...
    m.w_OK = .F.
    * --- in w_Result metto solo un messaggio generico poich� la gest_log e gendblog
    *     hanno gi� inserito lo stesso messaggio. Altrimenti sarabbe raddoppiato
    m.w_RESULT = Ah_MsgFormat("Consultare log sincronizzazione")
  endif
  if m.w_OK
    * --- Nel caso abbia inserito filtri verticali senza sovrascrittura elimino
    *     dal comando di UPDATE i valori da non sovrascrivere...
    Dimension wArrCampi[1,2]
    m.w_OK = APP_VERT( @ArrCampi , @wArrCampi) 
    if Not Empty( WRITETABLE( m.w_PRITAB , @wArrCampi ,@pArrDb ,m.pLog,m.pVerbose ) )
      m.w_RESULT = AH_MsgFormat( "Comando Update su archivio %1 ha generato errore " , m.w_PRITAB, )
      gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
      m.w_OK = .F.
    else
      if VarType( m.pLog ) ="O" And m.pVerbose
        AddMsgNL("Sincronizza con sede %1 per entita %2, elemento %3 modificato", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG ,, ,)
      endif
      sincronizza_Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Gestione archivi collegati.
      *     Se modifico il dato posso avere due criteri di aggiornamento della
      *     tabella secondaria
      *     a) Cancella / Inserisce
      *     b) Modifica
      *     Nel primo caso prima di cancellare devo mantenere i valori presenti sul DB
      *     in modo da passarli al successivo interimento. Utilizzo un array per contenere
      *     nome tabella secondaria e relativo cursore da passare alla routine di sincronizzazione.
      *     
      *     Se modifica non occorre fare nulla..
      * --- Aggiorno archivi collegati
      * --- Devo cancellare inverto l'ordine di processo..
      w_SECTAB.gobottom()
      m.w_CUR_IDX = 0
      do while Not m.w_SECTAB.Bof()
        if m.w_SECTAB.ENUPDATE="N"
          * --- Se esistono filtri del tipo mantieni dato sul DB per la tabella...
          * --- Popola array con i valori da scrivere sul DB
          m.w_TMPFIELDS = ""
          m.w_BKEEPDB = .F.
          m.w_NOM_ARRAY = "g_Arr_"+Alltrim( m.w_SECTAB.EN_TABLE )
          m.w_LOOP = 1
          Local L_Nom_Array 
 L_Nom_Array = m.w_NOM_ARRAY
          do while m.w_LOOP<=Alen( ( m.w_NOM_ARRAY ) , 1)
            * --- Filtro verticale con mantiene valore sul DB...
             TestMacro=&L_NOM_ARRAY.[ m.w_LOOP , 3]
            if  TestMacro=2
              m.w_TMPFIELDS = m.w_TMPFIELDS + iif( Not Empty( m.w_TMPFIELDS ), "," ," " ) +&L_NOM_ARRAY.[ m.w_LOOP , 1]
              m.w_BKEEPDB = .T.
            endif
            m.w_LOOP = m.w_LOOP + 1
          enddo
          * --- Se devo mantenere il valore sul DB eseguo la lettura del dato
          *     e memorizzo il tutto all'interno di un cursore...
          if m.w_BKEEPDB
            * --- Se trovo tra i filtri verticali un filtro che mi dice di mantenere il valore sul
            *     db allora costruisco un DBF contenenete i campi  della chiave pi�
            *     i campi da manterere prima di svoglere la DELETE. Se svolgo un Update
            *     il campo non sar� passato al server...
            m.w_LINKDEL = Alltrim( m.w_SECTAB.EN__LINK ) 
            * --- Accodo ai campi nei filtri verticali la chiave della tabella collegata..
            m.w_CHIAVECOLL = cp_KeyToSQL ( I_DCX.GetIdxDef( Alltrim( m.w_SECTAB.EN_TABLE ) ,1 ) )
            m.w_LOOP = 1
            do while m.w_LOOP<>0
              m.w_LOOP = At( "," , m.w_CHIAVECOLL )
              if m.w_LOOP>0
                m.w_CAMPO = Left( m.w_CHIAVECOLL , m.w_LOOP -1 )
              else
                m.w_CAMPO = Alltrim( m.w_CHIAVECOLL )
              endif
              m.w_TMPFIELDS = m.w_TMPFIELDS + iif( Not Empty( m.w_TMPFIELDS ), "," ," " ) + w_CAMPO
              m.w_CHIAVECOLL = Alltrim( Substr( m.w_CHIAVECOLL , m.w_LOOP+1 , Len ( m.w_CHIAVECOLL ) ) )
            enddo
            m.w_LOOP = 1
            do while m.w_LOOP<=Alen( pArrDB , 1)
              m.w_LINKDEL = StrTran( m.w_LINKDEL , m.w_TABELLA+"."+pArrDb[ m.w_LOOP , 1 ] , Cp_ToStrOdbc( pArrDb[ m.w_LOOP , 2 ] ) )
              m.w_LOOP = m.w_LOOP + 1
            enddo
            * --- Elimino l'alias della tabella per la condizione 
            *     (Delete From DEMODOC_DETT Where MVSERIAL='00000001'
            m.w_LINKDEL = StrTran( m.w_LINKDEL , Alltrim(m.w_SECTAB.EN_TABLE)+"." , "" )
            m.w_VERTCURDB = READTABLE( Alltrim( m.w_SECTAB.EN_TABLE ), m.w_TMPFIELDS , .f., m.pLog, m.pVerbose , m.w_LINKDEL )
            if Empty( m.w_VERTCURDB)
              * --- Errore nella lettura dei dati presenti sul database...
              m.w_RESULT = AH_MsgFormat( "Comando Update su archivio %1 ha generato errore in lettura dati [filtro verticale mantieni dato] tabella collegata %2 " , m.w_PRITAB, Alltrim( m.w_SECTAB.EN_TABLE ) )
              m.w_OK = .F.
            else
              * --- Memorizzo cursore e tabella sull'array...
              m.w_CUR_IDX = m.w_CUR_IDX + 1
               
 Dimension aCurVert( m.w_CUR_IDX , 2 ) 
 aCurVert[ m.w_CUR_IDX ,1]= Alltrim( m.w_SECTAB.EN_TABLE ) 
 aCurVert[ m.w_CUR_IDX ,2]= m.w_VERTCURDB
            endif
          endif
          if m.w_OK
            * --- Chiama la routine di sincronizzazione della tabella collegata per eliminarla
            *     Ora lo posso fare, o appena salvato in locale i valori realtivi ai filtri verticali
            L_NOM_ARRAY= m.w_NOM_ARRAY
            TestMacro=Not Empty(SINCCOLL( m.w_SEDE , m.w_ENTITA , m.w_SECTAB.EN_TABLE , m.w_TABELLA, "D", @pArrDB , m.w_SECTAB.EN__LINK , m.w_SECTAB.ENUPDATE, m.w_SECTAB.ENNEEDFL="S" ,"", @&L_NOM_ARRAY , m.pLog, m.pVerbose , m.w_SECTAB.TS__LINK ) )
            if TestMacro
              m.w_RESULT = AH_MsgFormat( "Aggiornamento da sede %1 per entita %2 , archivio secondario %3 ha generato errore " , m.w_SEDE ,m.w_ENTITA , Alltrim( m.w_SECTAB.EN_TABLE ) )
              gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
              m.w_OK = .F.
            else
              if VarType( m.pLog ) ="O" And m.pVerbose
                AddMsgNL("Sincronizzazione con sede %1 per entita %2, elemento %3 tabella secondaria %4", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG , Alltrim(m.w_SECTAB.EN_TABLE) , ,)
              endif
            endif
          endif
        endif
        w_SECTAB.Skip(-1)
      enddo
      m.w_AZIONE = "U"
    endif
  endif
endproc


procedure sincronizza_Page_7
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Aggiorno Mirror..Aggiungo le colonne PUBCOSED, CPCCCHK_PU,ANCESTOR,AVERSION
  *     Il validatore in fase di ricezione deve dare la nuova versione perch� la regola
  *     � che se due sedi inviano il dato modificato verso il validatore partendo dal
  *     medesimo ancestor, il validatore accetta il dato della prima, battezzando una nuova
  *     versione, e rifiuta il dato della seconda perch� ormai partito da una 
  *     versione "vecchia"
  m.w_IDX = Alen( m.pArrMir , 1 )
   
 pArrMir[ m.w_IDX -3 ,2 ] = Eval( m.w_CURFILE+".CPCCCHK" ) 
 pArrMir[ m.w_IDX -2 ,2 ] = Eval( m.w_CURFILE+".ANCESTOR" ) 
 pArrMir[ m.w_IDX-1 ,2 ] = Eval( m.w_CURFILE+".AVERSION" ) +IIF( m.w_BVALIDATORE , 1 ,0 )
  * --- Leggo il CPCCCHK dal mirror per aggiornamenti.
  *     (il CPCCCHK non va aggiornato in fase di ricezione)
   
 pArrMir[ m.w_IDX ,2 ] =Eval(m.w_CURMIRROR+".CPCCCHK" )
  * --- Se si inserisce un nuovo record il campo DELETE_PU deve essere valorizzato a 0 e non deve contenere valori nulli altrimenti i record cancellati vengono pubblicati pi� volte
  if m.w_NUOVORECORD
    * --- Ridimensiono l array per aggiungere 1 elemento
    m.w_LEN = ALEN (m.pArrMir,1)+1
    Dimension pArrMir[ m.w_LEN , ALEN (m.pArrMir,2) ] 
 pArrMir[ m.w_LEN ,1 ] = "DELETE_PU" 
 pArrMir[ m.w_LEN ,2 ] = 0 
 pArrMir[ m.w_LEN ,3 ] = 0
  endif
  if Not Empty( RICMIRROR( m.w_PRITAB , iif(m.w_NEWMIRR,"I","U"), @m.pArrMIr , m.pLog , m.pVerbose ) )
    * --- Creo una nuova entrata sul mirror
    m.w_RESULT = AH_MsgFormat( "Comando aggiornamento mirror per archivio %1 ha generato errore " , m.w_PRITAB, )
    m.w_OK = .F.
  endif
  if m.w_NUOVORECORD
    * --- Ripristina l array alla sua dimensione
    Dimension pArrMir[ m.w_LEN - 1 , ALEN (m.pArrMir,2) ]
  endif
endproc


procedure sincronizza_Page_8
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Aggiorno archivi collegati
  * --- Per ogni tabella secondaria procedo con l'aggiornamento, se cancello inverto l'ordine
  *     , quindi cancello prima le tabelle pi� "lontane" dalla tabella principale.
  if m.w_AZIONE="D"
    w_SECTAB.gobottom()
  else
    w_SECTAB.GoTop()
  endif
  do while Not iif( m.w_AZIONE="D" , m.w_SECTAB.Bof() , m.w_SECTAB.Eof() )
    m.w_CURVERTDB = ""
    * --- Ricerco nell'array delle traduzioni il cursore con i valori sul DB...
    *     Solo in modifica
    if m.w_AZIONE="U"
      m.w_LOOP = 1
      do while m.w_LOOP <= Alen( aCurVert , 1 ) 
        if Type("aCurVert[ m.w_LOOP , 1 ]")="C" and Upper( aCurVert[ m.w_LOOP , 1 ]) = Upper( alltrim(m.w_SECTAB.EN_TABLE) ) 
          m.w_CURVERTDB = aCurVert[ m.w_LOOP , 2 ] 
          * --- Esco..
          m.w_LOOP = Alen( aCurVert , 1 ) + 1
        endif
        m.w_LOOP = m.w_LOOP + 1
      enddo
    endif
    * --- Chiama la routine di sincronizzazione della tabella collegata...
    m.w_NOM_ARRAY = "g_Arr_"+Alltrim( m.w_SECTAB.EN_TABLE )
    L_NOM_ARRAY= m.w_NOM_ARRAY
    TestMacro=Not Empty( SINCCOLL( m.w_SEDE , m.w_ENTITA , alltrim(m.w_SECTAB.EN_TABLE) , m.w_TABELLA, m.w_AZIONE , @pArrDB , m.w_SECTAB.EN__LINK , m.w_SECTAB.ENUPDATE, m.w_SECTAB.ENNEEDFL="S" , m.w_CURVERTDB , @&L_NOM_ARRAY , m.pLog, m.pVerbose, m.w_SECTAB.TS__LINK ) )
    if TestMacro
      m.w_RESULT = AH_MsgFormat( "Aggiornamento da sede %1 per entita %2 , archivio secondario %3 ha generato errore " , m.w_SEDE ,m.w_ENTITA , Alltrim( m.w_SECTAB.EN_TABLE ) )
      gest_log( m.w_SEDE, m.w_ENTITA , @pArrDb , m.w_Result, 0 , m.w_TABELLA , @m.pArr, m.w_SERLOG, m.pLog )
      m.w_OK = .F.
      exit
    else
      if VarType( m.pLog ) ="O" And m.pVerbose
        AddMsgNL("Sincronizzazione con sede %1 per entita %2, elemento %3 tabella secondaria %4", m.pLog , m.w_SEDE , m.w_ENTITA , m.w_REC_MSG , Alltrim(m.w_SECTAB.EN_TABLE) , ,)
      endif
    endif
    if m.w_AZIONE="D"
      w_SECTAB.Skip(-1)
    else
      w_SECTAB.Skip(1)
    endif
  enddo
endproc


  function sincronizza_OpenTables()
    dimension i_cWorkTables[max(1,4)]
    i_cWorkTables[1]='ENT_DETT'
    i_cWorkTables[2]='SUENDETT'
    i_cWorkTables[3]='ENT_MAST'
    i_cWorkTables[4]='DETTPUBL'
    return(cp_OpenFuncTables(4))
* --- END SINCRONIZZA
* --- START TISINENT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: tisinent                                                        *
*              Controllo tabella appartenente alle entit�                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-11                                                      *
* Last revis.: 2008-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func tisinent
param pTable

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_Ok
  m.w_Ok=.f.
* --- WorkFile variables
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "tisinent"
if vartype(__tisinent_hook__)='O'
  __tisinent_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'tisinent('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'tisinent')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if tisinent_OpenTables()
  tisinent_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_ENT_DETT')
  use in _Curs_ENT_DETT
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'tisinent('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'tisinent')
Endif
*--- Activity log
if vartype(__tisinent_hook__)='O'
  __tisinent_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure tisinent_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione lanciata dalla Cp_Forms per controllare se la gestione della quale � stato modificato 
  *     un child integrato, fa parte delle entit� del Lore. In questo caso verr� forzato l'aggiornamento anche 
  *     della gestione padre, mettendo bUpdated =.T.
  m.w_Ok = .F.
  * --- Controllo se la tabella della gestione � presente nelle entit�
  * --- Select from ENT_DETT
  i_nConn=i_TableProp[ENT_DETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select * from "+i_cTable+" ENT_DETT ";
        +" where EN_TABLE="+cp_ToStrODBC(m.pTable)+"";
         ,"_Curs_ENT_DETT")
  else
    select * from (i_cTable);
     where EN_TABLE=m.pTable;
      into cursor _Curs_ENT_DETT
  endif
  if used('_Curs_ENT_DETT')
    select _Curs_ENT_DETT
    locate for 1=1
    do while not(eof())
    m.w_Ok = .T.
    exit
      select _Curs_ENT_DETT
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_Ok
  return
endproc


  function tisinent_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ENT_DETT'
    return(cp_OpenFuncTables(1))
* --- END TISINENT
* --- START SINCCOLL
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: sinccoll                                                        *
*              Sincronizza tabelle collegate                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-06                                                      *
* Last revis.: 2010-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func sinccoll
param pSede,pEntita,pTABELLA,pPriTab,pAzione,pArrPriKey,pLink,pUpdate,pObbl,pVERTCURDB,ArrVal,pLog,pVerbose,pTSMLink

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(254)
  private w_DBF
  m.w_DBF=space(254)
  private w_BINS
  m.w_BINS=.f.
  private w_ESPR
  m.w_ESPR=space(30)
  private w_NOMCUR
  m.w_NOMCUR=space(10)
  private w_DATIRIGA
  m.w_DATIRIGA=space(10)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_NCONN
  m.w_NCONN=0
  private w_BRES
  m.w_BRES=.f.
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_LOOP
  m.w_LOOP=0
  private w_LINK
  m.w_LINK=space(254)
  private w_LINKDBF
  m.w_LINKDBF=space(254)
  private w_LINKDEL
  m.w_LINKDEL=space(254)
  private w_TSMLINK
  m.w_TSMLINK=space(254)
  private w_TSMNAME
  m.w_TSMNAME=space(254)
  private w_CURDB
  m.w_CURDB=space(10)
  private w_TSMCAMPI
  m.w_TSMCAMPI=space(254)
  private w_CAMPOREL
  m.w_CAMPOREL=space(30)
  private w_POS
  m.w_POS=0
  private w_ARRPOS
  m.w_ARRPOS=0
  private w_NRESWRITE
  m.w_NRESWRITE=0
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_IDX
  m.w_IDX=0
  private w_FIELDS
  m.w_FIELDS=space(254)
  private w_VERTJOIN
  m.w_VERTJOIN=space(254)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "sinccoll"
if vartype(__sinccoll_hook__)='O'
  __sinccoll_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'sinccoll('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pTABELLA)+','+Transform(pPriTab)+','+Transform(pAzione)+','+Transform(pArrPriKey)+','+Transform(pLink)+','+Transform(pUpdate)+','+Transform(pObbl)+','+Transform(pVERTCURDB)+','+Transform(ArrVal)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pTSMLink)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'sinccoll')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
sinccoll_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'sinccoll('+Transform(pSede)+','+Transform(pEntita)+','+Transform(pTABELLA)+','+Transform(pPriTab)+','+Transform(pAzione)+','+Transform(pArrPriKey)+','+Transform(pLink)+','+Transform(pUpdate)+','+Transform(pObbl)+','+Transform(pVERTCURDB)+','+Transform(ArrVal)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pTSMLink)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'sinccoll')
Endif
*--- Activity log
if vartype(__sinccoll_hook__)='O'
  __sinccoll_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure sinccoll_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Sincronizza tabelle collegate, riceve la chiave del record della tabella
  *     principale, l'entit� da aggiornare, la sede, l'elenco dei DBF e
  *     l'operazione da applicare (Insert / Update / Delete )
  *     
  *     (La tabella principale � gia stata aggiornata da SINCRONIZZA)
  *     pSede  = Sede (x gestione fitlri verticali)
  *     pEntita  = Entita
  *     pTABELLA = Nome fisico tabella da aggiornare
  *     pPriTab = Nome tabella principale (utilizzata per gestire l'espressione di Link)
  *     pAZIONE = Record tab. principale inserito, modificato, eliminato
  *     pArrPriKey = Array con la chiave della tabella principale, identifica il record per il quale occorre allienare il dettaglio
  *                     campo 1 nome del campo nella chiave, 2 corrispondente valore
  *     pLink = Espressione di legame tra tabella principale e secondaria
  *     pUpdate = Tipo di aggiornamento (significativo in caso di modifica, se modifica o distrugge e ricrea)
  *     pObbl = Se .t. e manca il file DBF annullo la sincronizzazione
  *     pVERTCURDB  = Array contenente eventuali filtri verticali da mantenere rispetto al DB
  *                                 se <>''. Contiene la chiave ed i campi da non modificare
  *     ArrVal = Array con struttura tabella secondaria, con tre colonne
  *                 1) nome del campo
  *                 2) eventuale valore fisso (filtro verticale)
  *                 3) stato fitlro verticale, 0 no, 1 fisso, 2 da mantenere
  *     pLog = Oggetto a cui passare i messaggi
  *     pVerbose = Visualizzo anche le frasi..
  *     pTSMLink = Se pieno allora la tabella collegata � un TSM, e il parametro � la condizione di join da applicare...
  * --- Ricerco il DBF corrispondente..
  m.pTABELLA = Alltrim( m.pTabella )
  m.w_DBF = LOOK_DBF( alltrim(m.pENTITA), alltrim(m.pTABELLA) , @pArr )
  if Empty( m.w_DBF )
    if m.pObbl
      m.w_RESULT = AH_MsgFormat("Sincronizzazione per entita %1 per l'archivio secondario obbligatorio %2 non esistono dati", m.pLog , m.pENTITA , m.pTABELLA)
      if VarType( m.pLog ) ="O"
        AddMsgNL("Sincronizzazione per entita %1 per l'archivio secondario obbligatorio %2 non esistono dati", m.pLog , m.pENTITA , m.pTABELLA , ,, ,)
      endif
    else
      if VarType( m.pLog ) ="O" And m.pVerbose
        AddMsgNL("Sincronizzazione per entita %1 per l'archivio secondario %2 non esistono dati", m.pLog , m.pENTITA , m.pTABELLA , ,, ,)
      endif
    endif
  else
    m.pPriTab = Alltrim( m.pPriTab )
    * --- Ho trovato il DBF, mi muovo in base all'azione...
    sinccoll_Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    m.w_NOMCUR = Sys( 2015 )
    * --- Determino gli array da passare alle routine di scrittura sul database.
    *     ArrKey conterr� i campi nella chiavi ed i rispettivi valori, mentre
    *     ArrVal conterr� tutti i campi con i rispettivi valori.
    *     Insertia un'altra colonna numerica, se  <>0 il campo non deve 
    *     essere valorizzato con i dati dal DBF perch� oggetto di filtro verticale
    *     (1 valore fisso, 2 letto dal valore atuale del DATABASE)
    Dimension ArrKey[1,3]
    * --- Non recupero i filtri verticali se st� cancellando....
    sinccoll_Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Recupero dal DBF i dati relativi all'entit� valutata al momento...
    * --- Utilizzate una serie di variabili locali non Painter per svolgere la Macro
    *     senza m.
     
 Local L_OldErr, bErr, L_ExecSel 
 L_OldErr=On("ERROR") 
 bErr=.f. 
 On Error bErr=.t. 
 L_ExecSel="Select * From "+m.w_DBF+" " +m.pTabella+" Where "+m.w_LINKDBF+" Into Cursor "+m.w_NOMCUR+" NoFilter "
    if Not Empty( m.pVERTCURDB )
      * --- Metto in Join il DBF del pacchetto con il DBF contenente i dati letti
      *     relativi ai filtri verticali (in questo caso creo il cursore come scrivibile
      L_ExecSel=L_ExecSel+" READWRITE" 
 &L_ExecSel
      * --- Se si � verificato un errore prova a riesceguire la query (c'� un caso in cui il fox da un errore "column '' not found"
      *     anche se la frase Sql � corretta. E' un errore che si verifica a volte con pacchetti grossi. Rieseguendo una seconda volta
      *     la query la frase viene eseguita correttaemtne senza errori 
      if bErr 
        bErr=.f.
        &L_ExecSel
      endif
      * --- Vado in Update sul cursore creato andando ad applicare i valori presenti sul database
      *     (non posso mettere in Join pr via della possibile eccessiva lunghezza della query generata
      *     per elencare i campi da prendere e quelli da mettere in NVL con i valori sul
      *     db)
      L_ExecSel="Update "+ m.w_NOMCUR+" Set "+ m.w_FIELDS+" From "+ m.pVERTCURDB+" FILT_VERT Where "+m.w_VERTJOIN
    endif
    * --- L'esescuzione di una query sul temporaneo VFP ne provoca la sua apertura
    *     lo devo quindi chiudere...
    * --- Se si � verificato un errore prova a riesceguire la query (c'� un caso in cui il fox da un errore "column '' not found"
    *     anche se la frase Sql � corretta. E' un errore che si verifica a volte con pacchetti grossi. Rieseguendo una seconda volta
    *     la query la frase viene eseguita correttaemtne senza errori 
    &L_ExecSel
    if bErr AND Empty( m.pVERTCURDB )
      bErr=.f.
      &L_ExecSel
      Select ( JUSTSTEM(m.w_DBF) ) 
 Use 
 On Error &L_OldErr
    else
      Select ( JUSTSTEM(m.w_DBF) ) 
 Use 
 On Error &L_OldErr
    endif
    if bErr
      m.w_RESULT = AH_MsgFormat( "Lettura pacchetto tabella %1 ha generato errore %2" , m.pTabella, Message()+" "+Message(1)+" "+L_ExecSel )
      if VarType( m.pLog ) ="O"
        AddMsgNL( m.w_RESULT ,m.pLog )
      endif
    else
      if m.pAzione="D" Or ( m.pAzione="U" And m.pUpdate="N" )
        if Not Empty( DELETETABLE( m.pTABELLA , Null ,m.pLog,m.pVerbose, m.w_LINKDEL ) )
          sinccoll_Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          i_retval = AH_MsgFormat( "Comando Delete su archivio %1 ha generato errore " , m.pTabella, )
          return
        endif
        if m.pAzione="D"
          * --- Se operazione cancellazione, ho terminato
          sinccoll_Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          i_retval = ""
          return
        endif
      endif
      * --- Mi dice se devo inserire o modificare i dati. Inserisco se inserito il record
      *     nella tabella principale oppure se operazione di cancella/inserisce
      *     come tipo di agigornamento
      m.w_BINS = m.pAzione="I" Or ( m.pAzione="U" And m.pUpdate="N" )
      * --- Scorro il DBF cos� determinato...
       
 Select( m.w_NOMCUR ) 
 Go Top
      * --- - w_DATIRIGA contiene i dati che verranno memorizzati sul DB (le funzioni inserite come filtri verticali accedono ai dati su cui effettuano il filtro tramite questo parametro)
      Select( m.w_NOMCUR ) 
 SCATTER NAME m.w_DATIRIGA
      do while Not Eof( m.w_NOMCUR )
        * --- Costruisco l'array dei valori da scrivere sul DB
        * --- Nel caso di funzioni dinamiche bisogna sostituire l espressione con la valutazione dell espressione (senza una copia si sovrascriverbbe l espressione da valutare cosicch� 
        *     al successivo record la valutazione darrebbe errore)
        ACOPY( m.ArrVal, CopiaArrVal )
        sinccoll_Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if m.w_BINS
          sinccoll_Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          sinccoll_Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Rispristina eventuali funzioni dinamiche
        ACOPY( CopiaArrVal , m.ArrVal )
         
 Select ( m.w_NOMCUR ) 
 Skip
      enddo
      * --- Se tabella TSm vado ad aggiornare anche la tabella TSM...
      if Not Empty( m.w_TSMLINK )
        * --- Elimino le entrate esistenti...
        if Not Empty( DELETETABLE( m.w_TSMNAME , Null ,m.pLog,m.pVerbose, StrTran( m.w_TSMLINK , m.pTABELLA+"." , m.w_TSMNAME+"." ) ) )
          sinccoll_Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if VarType( m.pLog )="O" And m.pVerbose
            AddMsgNL( "Aggiornamento cancellazione entrate TSM %1 ha generato errore " , m.pLog , m.w_TSMNAME )
          endif
          i_retcode = 'stop'
          i_retval = AH_MsgFormat( "Aggiornamento cancellazione entrate TSM %1 ha generato errore " , m.w_TSMNAME )
          return
        else
          * --- Passo alla INSERT delle entrate appena sincronizzate...
          *     Creo un comando SQL del tipo
          *     INSERT INTO TSM_archivio (<elenco campi>) Select <elenco campi> From Archivio
          m.w_IDXTABLE = cp_OpenTable( m.pTabella ,.T.)
          m.w_NCONN = i_TableProp[ m.w_IDXTABLE , 3 ] 
          m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
          m.w_CMDSQL = "INSERT INTO "+ m.w_TSMNAME+" ( " +m.w_TSMCAMPI+") Select "+m.w_TSMCAMPI+" From "
          m.w_CMDSQL = m.w_CMDSQL + m.w_PHNAME+" "+ m.pTABELLA +" Where " + m.w_TSMLINK
          m.w_BRES = cp_SQL( m.w_NCONN , m.w_CMDSQL, .f. , .f. ,.t. )
          if m.w_BRES<0
            sinccoll_Pag7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if VarType( m.pLog )="O" And m.pVerbose
              AddMsgNL("Errore aggiornamento TSM %1, Errore: %2, frase %3 ", m.pLog , m.w_TSMNAME , Message(), m.w_CMDSQL )
            endif
            i_retcode = 'stop'
            i_retval = ah_msgformat("Errore aggiornamento TSM %1, Errore: %2, frase %3 ", m.w_TSMNAME , Message(), m.w_CMDSQL )
            return
          else
            if VarType( m.pLog )="O" And m.pVerbose
              AddMsgNL("Aggiornamento TSM %1, frase %2 ", m.pLog , m.w_TSMNAME , m.w_CMDSQL )
            endif
          endif
        endif
      endif
    endif
  endif
  sinccoll_Pag7()
  if i_retcode='stop' or !empty(i_Error)
    return
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


procedure sinccoll_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Crea l'espressione di link tra tabella collegata e principale cablando all'interno
  *     i valori della chiave della tabella principale...
  *     Quindi scorro l'array che contiene la chiave del record della tabella
  *     principale da aggiornare, e per ogni campo ricerco all'interno dell'espressione
  *     di link TABELLA.NOME CAMPO e lo sostituisco con il corrispondente valore..
  *     Costruisco due link, uno per il database (CP_TOSTRODBC) ed uno
  *     per il dbf Fox (direttamente l'elemento dell'array)
  m.w_LINK = Alltrim( m.pLink )
  m.w_LINKDBF = m.w_LINK
  m.w_LINKDEL = m.w_LINK
  m.w_LOOP = 1
  * --- Per gestione TSM....
  m.w_TSMLINK = Alltrim( Nvl( m.pTSMLink , "" ) )
  do while m.w_LOOP<=Alen( m.pArrPriKey , 1)
    * --- Se un filtroverticale ha modificato il valore del campo chiave bisogna effettuare la ricerca sul DBF usando la vecchia chiave (memorizzata in pArrPriKey[ w_LOOP , 3 ]  )
    m.w_LINK = StrTran( m.w_LINK , m.pPriTab+"."+pArrPriKey[ m.w_LOOP , 1 ] , Cp_ToStrOdbc( pArrPriKey[ m.w_LOOP , 2] ) )
    if EMPTY (pArrPriKey[ m.w_LOOP , 4 ] )
      m.w_LINKDBF = StrTran( m.w_LINKDBF , m.pPriTab+"."+pArrPriKey[ m.w_LOOP , 1 ] , cp_ToStr( pArrPriKey[ m.w_LOOP , 2 ] ))
    else
      m.w_LINKDBF = StrTran( m.w_LINKDBF , m.pPriTab+"."+pArrPriKey[ m.w_LOOP , 1 ] , cp_ToStr( pArrPriKey[ m.w_LOOP , 4 ] ))
    endif
    * --- Se tabella TSM al termine della sincronizzazione eseguo due operazioni,
    *     la prima una DELETE FROM TSM WHERE <chiave>=<chiave> e una
    *     INSERT INTO TSM SELECT <campi> FROM ARCHIVIO where <chiave>=<chiave>
    if Not Empty( m.w_TSMLINK )
      * --- Condizione da applicare alla Select...
      m.w_TSMLINK = StrTran( m.w_TSMLINK , m.pPriTab+"."+pArrPriKey[ m.w_LOOP , 1 ] , Cp_ToStrOdbc( pArrPriKey[ m.w_LOOP , 2 ] ) )
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  * --- Elimino l'alias della tabella per la condizione 
  *     (Delete From DEMODOC_DETT Where MVSERIAL='00000001'
  m.w_LINKDEL = StrTran( m.w_LINK , m.pTabella+"." , "" )
  if Not Empty( m.w_TSMLINK )
    * --- Determino l'elenco dei campi all'interno della tabella TSM..
    m.w_TSMNAME = GetTsmName ( m.pTabella )
    m.w_CURDB = READTABLE( m.w_TSMNAME , "*" , .F. , m.pLog, m.pVerbose , "1=0")
    if Empty( m.w_CURDB )
      * --- Errore nella lettura dei dati presenti sul database...
      sinccoll_Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      m.w_RESULT = AH_MsgFormat( "Errore determinazione elenco campi TSM archivio %1 " , m.w_TSMNAME)
    else
      * --- Leggo i campi dal cursore e costruisco l'elenco per la Select...
      m.w_LOOP = 1
      m.w_TSMCAMPI = ""
      Afield(El_CampiTSM , m.w_CURDB )
      do while m.w_LOOP<=Alen( El_CampiTSM , 1)
        m.w_TSMCAMPI = m.w_TSMCAMPI + iif(empty( m.w_TSMCAMPI ),"",",") + El_CampiTSM[ m.w_LOOP , 1 ]
        m.w_LOOP = m.w_LOOP + 1
      enddo
    endif
     
 Select ( m.w_CURDB ) 
 Use
  endif
endproc


procedure sinccoll_Pag3
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Popola array con i valori da scrivere sul DB
  m.w_LOOP = 1
  do while m.w_LOOP<=Alen( m.ArrVal , 1)
    do case
      case ArrVal[ m.w_LOOP , 3 ]<>1 AND ArrVal[ m.w_LOOP , 3 ]<>3
        ArrVal[ m.w_LOOP ,2 ] = Eval(m.w_NOMCUR+"."+ArrVal[ m.w_LOOP ,1 ] )
      case ArrVal[ m.w_LOOP , 3 ]=3
        * --- Nel caso di espressione dinamica viene deve essere valutata l espressione presente sul filtro verticale
        if btrserr=.F.
          m.w_ESPR = ArrVal[ m.w_LOOP ,2 ] 
          ArrVal[ m.w_LOOP ,2 ] = Eval( alltrim (ArrVal[ m.w_LOOP ,2 ] ))
          if btrserr=.T.
            AddMsgNL("La valutazione dell' espressione %1 ha generato l' errore:%2",m.pLog, alltrim (m.w_ESPR) , Message())
          endif
        endif
    endcase
    * --- Nel caso di modifica, prelevo anche i valori delle chiavi..
    if m.w_LOOP<=Alen( ArrKey,1 )
      if ArrVal[ m.w_LOOP , 3 ]=3
        * --- Se un filtro verticale cambia una chiave allora si aggioran l array delle chiavi inserendovi la chiave ricalclolata applicando il  filtro verticale
        ArrKey[ m.w_LOOP ,2 ] =ArrVal[ m.w_LOOP ,2 ]
      else
        ArrKey[ m.w_LOOP ,2 ] = Eval(m.w_NOMCUR+"."+ArrKey[ m.w_LOOP ,1 ] )
      endif
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  * --- Se un filtro vericale ha modificato una chiave della tabela primaria allora si estrae il nome del campo che � in relazione con lachiave dellla tabella verticale che � stata modificata dal filtro verticale
  m.w_POS = 1
  * --- Se un filtro verticale ha modificato le chiavi allora aggiorna gli array arrkey e arrval con il nuovo valore determinato dal filtro verticale
  do while m.w_POS<= ALEN (m.pArrPriKey,1)
    if NOT EMPTY ( m.pArrPriKey [m.w_POS,4])
      m.w_CAMPOREL = getfieldrel (m.pLink , m.pPriTab , m.pArrPriKey [ m.w_POS,1] )
      m.w_CAMPOREL = STRTRAN (m.w_CAMPOREL,alltrim(m.pTABELLA)+".","")
      * --- Sostituzione della chiave in ARRKEY
      m.w_ARRPOS = 1
      do while m.w_ARRPOS<= ALEN (Arrkey,1)
        if Arrkey [ m.w_ARRPOS, 1 ] = m.w_CAMPOREL
          * --- Se sulla  chiave � gi� stato applicato  un filtro verticale allora le modifiche alla chiave della tabella principale (effettuate tramite filtri verticali sulla chiave della
          *      tabella principale) non vengono riportate sulle chiavi della tabella collegata (le chiavi usate nelle relazioni tra tabella pricipale e collegate) 
          if NOT (ArrVal[ m.w_ARRPOS , 3 ]=3)
            Arrkey [ m.w_ARRPOS,2 ] =pArrPriKey[ m.w_POS,2]
          endif
        endif
        m.w_ARRPOS = m.w_ARRPOS + 1
      enddo
      * --- Sostituzione della chiave in ARRVAL
      m.w_ARRPOS = 1
      do while m.w_ARRPOS<= ALEN (m.Arrval,1)
        if m.Arrval [ m.w_ARRPOS, 1 ] = m.w_CAMPOREL
          * --- Se sulla  chiave � gi� stato applicato  un filtro verticale allora le modifiche alla chiave della tabella principale (effettuate tramite filtri verticali sulla chiave della
          *      tabella principale) non vengono riportate sulle chiavi della tabella collegata (le chiavi usate nelle relazioni tra tabella pricipale e collegate) 
          if NOT (ArrVal[ m.w_ARRPOS , 3 ]=3)
            m.Arrval [ m.w_ARRPOS,2 ] =pArrPriKey[ m.w_POS,2 ]
          endif
        endif
        m.w_ARRPOS = m.w_ARRPOS + 1
      enddo
    endif
    m.w_POS = m.w_POS + 1
  enddo
endproc


procedure sinccoll_Pag4
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Inserisce il dato sul DB
  if Not Empty( INSERTTABLE( m.pTabella , @m.ArrVal ,m.pLog,m.pVerbose ) )
    sinccoll_Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    i_retval = AH_MsgFormat( "Errore aggiornamento (Inserzione) entit� %1 archivio %2",m.pLog, m.pEntita , m.pTabella )
    return
  else
    if VarType( m.pLog ) ="O" And m.pVerbose
      AddMsgNL("Aggiornamento (Inserzione) entit� %1 archivio %2",m.pLog, m.pEntita , m.pTabella)
    endif
  endif
endproc


procedure sinccoll_Pag5
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Modifica il dato sul DB
  * --- Nel caso abbia inserito filtri verticali senza sovrascrittura elimino
  *     dal comando di UPDATE i valori da non sovrascrivere...
  Dimension wArrCampi[1,2]
  m.w_OK = APP_VERT( @m.ArrVal , @wArrCampi) 
  if Not Empty( WRITETABLE( m.pTabella , @wArrCampi , @ArrKey , m.pLog,m.pVerbose, "",@m.w_NRESWRITE ) )
    sinccoll_Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    i_retval = AH_MsgFormat( "Errore aggiornamento (Modifica) entit� %1 archivio %2",m.pLog, , m.pEntita , m.pTabella )
    return
  else
    if m.w_NRESWRITE<>0
      if VarType( m.pLog )="O" And m.pVerbose
        AddMsgNL("Aggiornamento (Modifica) entit� %1 archivio %2",m.pLog, , m.pEntita , m.pTabella , ,, )
      endif
    else
      * --- Nessun record modificato, il record non esiste procedo con l'interimento...
      sinccoll_Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endif
endproc


procedure sinccoll_Pag6
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Costruisce array gestendo anche filtri verticali se presenti...
  * --- Scorro i campi della tabella (XDC) per costruire la frase di aggiornamento..
  m.w_FIELDS = ""
  m.w_VERTJOIN = ""
  if Not Empty( m.pVERTCURDB )
    m.w_LOOP = 1
    do while m.w_LOOP<= Alen( m.ArrVal , 1 )
      * --- Questo terzo valore mi dice come gestire il campo (mantengo o meno il valore sul DB)
      *     0 leggo dal file
      *     1 valore fisso
      *     2 mantengo il valore sul DB
      if ArrVal[ m.w_LOOP ,3 ] =2
        m.w_FIELDS = m.w_FIELDS + iif( Not Empty( m.w_FIELDS ), "," ,"" ) +ArrVal[ m.w_LOOP , 1]+"=FILT_VERT."+ArrVal[ m.w_LOOP , 1]
      endif
      m.w_LOOP = m.w_LOOP + 1
    enddo
  endif
  * --- La determinazione della chiave serve solo nel caso di modifica,
  *     per popolare la condizione di Where del comando Update.
  *     L'array � comunque poplato sempre per gestire, in inserimento
  *     l'eventuale presenza del dato. Se inserisco l'entit� ed una delle sue 
  *     tabelle collegate presenta gia record sul database, do un warning ed
  *     eseguo l'Update (potremmo pilotare questo comportamento a livello
  *     di Entit�)
  m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.pTABELLA ,1 ) )
  m.w_LOOP = 1
  m.w_IDX = 1
  do while m.w_LOOP<>0
    m.w_LOOP = At( "," , m.w_CHIAVE )
    if m.w_LOOP>0
      m.w_CAMPO = Left( m.w_CHIAVE , m.w_LOOP -1 )
    else
      m.w_CAMPO = Alltrim( m.w_CHIAVE )
    endif
     
 Dimension ArrKey[ m.w_IDX ,3 ] 
 ArrKey[ m.w_IDX ,1 ] = m.w_CAMPO
    if Not Empty( m.pVERTCURDB )
      m.w_VERTJOIN = m.w_VERTJOIN + iif( Not Empty( m.w_VERTJOIN ), " And " ,"" ) + m.w_NOMCUR +"."+ArrKey[ m.w_IDX , 1 ]+"=FILT_VERT."+ArrKey[ m.w_IDX, 1 ]
    endif
    * --- Il campo di una chiave non pu� essere oggetto di filtro
    *     verticale
    ArrKey[ m.w_IDX ,3 ] = 0
    m.w_CHIAVE = Alltrim( Substr( m.w_CHIAVE , m.w_LOOP+1 , Len ( m.w_CHIAVE ) ) )
    m.w_IDX = m.w_IDX + 1
  enddo
endproc


procedure sinccoll_Pag7
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Rimozione temporanei...
  if Used( m.w_NOMCUR )
     
 Select ( m.w_NOMCUR ) 
 Use
  endif
endproc


* --- END SINCCOLL
* --- START TRASLACLIDAIVA
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: traslaclidaiva                                                  *
*              Ricerca il codice cliente conoscendone la partita iva           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-09                                                      *
* Last revis.: 2008-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func traslaclidaiva
param pDatiRiga,pLog,pVerbose,pCampoIva,pAntipcon,pAncodice

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PARIVA
  m.w_PARIVA=space(11)
  private w_TIPCON
  m.w_TIPCON=space(1)
  private w_ANCODICE
  m.w_ANCODICE=space(15)
* --- WorkFile variables
  private CONTI_idx
  CONTI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "traslaclidaiva"
if vartype(__traslaclidaiva_hook__)='O'
  __traslaclidaiva_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'traslaclidaiva('+Transform(pDatiRiga)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pCampoIva)+','+Transform(pAntipcon)+','+Transform(pAncodice)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'traslaclidaiva')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if traslaclidaiva_OpenTables()
  traslaclidaiva_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_CONTI')
  use in _Curs_CONTI
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'traslaclidaiva('+Transform(pDatiRiga)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pCampoIva)+','+Transform(pAntipcon)+','+Transform(pAncodice)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'traslaclidaiva')
Endif
*--- Activity log
if vartype(__traslaclidaiva_hook__)='O'
  __traslaclidaiva_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure traslaclidaiva_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione applicabiel come filtro verticale dinamico che dato il pacchetto determina il codice intestatario
  *     che sul database corrente ha una certa partita IVA.
  *     
  *     
  *     Assiomi filtri verticali dinamici:
  *     
  *     a) I filtri verticali possono usufruire come dati di partenza, dei soli dati sul pacchetto della tabella in esame.
  *     
  *     Corollario: Se definisco un qualsiasi tipo di filtro verticale su di un campo, la sua applicazione non pu� essere 
  *     utilizzata su filtri dinamici di altri campi. Nei filtri dinamici avr� sempre il valore del pacchetto.
  *     
  *     b) Se il filtro dinamico � applicato su di un campo appartenente alla chiave allora occorre che tutte le relazioni dell'entit� 
  *     che contengono il campo lo utilizzino in forma semplice(*).
  *     (*) Forma semplice significa che la condizione di Join tra archivio principale e collegato � di uguaglianza, quindi (A=B O B=A)
  *     
  *     La funzione riceve come parametro la variabile su cui si � appoggiata
  *     la SCATTER sulla riga della tabella principale in esame
  *     
  *     Eventuale oggetto su cui evidenziare il log
  *     Flag Verbose
  *     
  *     
  *     
  *     Le funzioni possono essere chiamate con i parametri  w_DATIRIGA  pLog e  pVerbose. Specificando tali parametri a tempo di esecuzione verranno passati (in w_DATIRIGA � un array 
  *     contenente il record di dati da sincronizzare - pLog conterra il riferimento al log degli errori- pVerbose indica se � richiesto una messaggistica dettagliata)
  *     
  *     esempio di chiamata : TraslacliDaIVA ( w_DATIRIGA  pLog ,  pVerbose ,  altri parametri)
  *     Gli altri parametri sono nomi di campi passati tamite l array pDatiRiga su cui la funzione deve lavorare (per esempio 'ANPARIVA,ANCODFIS' ). 
  * --- pDatiriga contiene l array dei dati (il record pasato per la sincronizzazione)
  *     pCAmpoIva contiene il nome del campo che contiene la partita iva (per esempio ANPARIVA oppure ANPARIVA1 , .... )
  *     pLog � il log dei messaggi
  *     pVerbose indica se � richiesta una messaggistica pi� dettagliata per il log (oltre ai messaggi di errore vengono visualizzati messaggi che indicano la cronologia delle operazioni che vengono svolte)
  l_COMANDO = "pDatiRiga."+ alltrim (m.pCampoIva)
  l_COMANDO2 = "pDatiRiga."+ alltrim (m.pAntipcon)
  l_COMANDO3 = "pDatiRiga."+ alltrim (m.pAncodice)
  m.w_TIPCON = &l_COMANDO2
  m.w_PARIVA = &l_COMANDO
  if Not Empty( m.w_PARIVA )
    * --- Select from CONTI
    i_nConn=i_TableProp[CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[CONTI_idx,2],.t.,CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ANCODICE from "+i_cTable+" CONTI ";
          +" where ANPARIVA="+cp_ToStrODBC(m.w_PARIVA)+" AND ANTIPCON="+cp_ToStrODBC(m.w_TIPCON)+"";
           ,"_Curs_CONTI")
    else
      select ANCODICE from (i_cTable);
       where ANPARIVA=m.w_PARIVA AND ANTIPCON=m.w_TIPCON;
        into cursor _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      select _Curs_CONTI
      locate for 1=1
      do while not(eof())
      m.w_ANCODICE = _Curs_CONTI.ANCODICE
      if VarType( m.pLog ) ="O" And m.pVerbose
        m.w_OLDCON = &l_COMANDO3
        AddMsgNL("Funzione TRASLACLI applicata ad intestatario su pacchetto %1 con partita IVA %2, trasformato in %3 ", m.pLog, w_OLDCON, m.w_PARIVA , m.w_ANCODICE)
      endif
      exit
        select _Curs_CONTI
        continue
      enddo
      use
    endif
  endif
  if EMPTY (m.w_ANCODICE)
    m.w_ANCODICE = &l_COMANDO3
  endif
  i_retcode = 'stop'
  i_retval = m.w_ANCODICE
  return
endproc


  function traslaclidaiva_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='CONTI'
    return(cp_OpenFuncTables(1))
* --- END TRASLACLIDAIVA
* --- START VALIDEXP
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: validexp                                                        *
*              Espressione di validit�                                         *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-28                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func validexp
param w_TARCHIVIO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_VALEXP
  m.w_VALEXP=space(254)
  private w_VALINT
  m.w_VALINT=space(1)
  private w_VALCAM
  m.w_VALCAM=space(30)
  private w_TMPS
  m.w_TMPS=space(50)
* --- WorkFile variables
  private ENT_MAST_idx
  ENT_MAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "validexp"
if vartype(__validexp_hook__)='O'
  __validexp_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'validexp('+Transform(w_TARCHIVIO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'validexp')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if validexp_OpenTables()
  validexp_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_VALIDEXP')
  use in _Curs_VALIDEXP
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'validexp('+Transform(w_TARCHIVIO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'validexp')
Endif
*--- Activity log
if vartype(__validexp_hook__)='O'
  __validexp_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure validexp_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Data la super entit� determina la condizione per individuare i record
  *     per i quali la sede � validatrice.
  *     Nessuna condizione = validatore di tutto l'archivio
  *     Riceve anche il tipo del database per costruire una frase corretta da 
  *     applicare a seconda del database (se non passato quello di default
  *     quindi CP_DBTYPE).
  *     Il tipo ci serve per costruire l'espressione di validazione anche 
  *     sul pacchetto
  m.w_VALEXP = ""
  * --- Tipo database utilizzato per gestire la validazione intrinseca
  * --- Select from VALIDEXP
  do vq_exec with 'VALIDEXP',createobject('cp_getfuncvar'),'_Curs_VALIDEXP','',.f.,.t.
  if used('_Curs_VALIDEXP')
    select _Curs_VALIDEXP
    locate for 1=1
    do while not(eof())
    * --- Leggo il tipo di validazione, se intrinseca o meno, se intrinseca
    *     allora valodi con le prime due lettere del campo segnalato
    m.w_VALINT = _Curs_VALIDEXP.ENVALINT
    m.w_VALCAM = _Curs_VALIDEXP.ENVALCAM
    if m.w_VALINT="N"
      * --- Metto in Or le varie condizioni di validazione presenti direttamente sulle
      *     tabelle principali delle varie entit�...
      if Not Empty( Nvl(_Curs_VALIDEXP.ENVALEXP,"") ) 
        if Not Empty( Nvl(_Curs_VALIDEXP.ENFILTRO,"") ) 
          * --- Se entit� con filtro allora metto in AND le condizioni di filtro e validazione
          m.w_VALEXP = m.w_VALEXP + iif( Empty( m.w_VALEXP ) , "" , " Or " ) +"(" +Alltrim(_Curs_VALIDEXP.ENVALEXP)+" And (" + Alltrim(_Curs_VALIDEXP.ENFILTRO)+" ) )"
        else
          m.w_VALEXP = m.w_VALEXP + iif( Empty( m.w_VALEXP ) , "" , " Or " ) +"(" + Alltrim(_Curs_VALIDEXP.ENVALEXP)+" )"
        endif
      else
        * --- Se non ho validazione ed ho solo il filtro utilizzo solo il filtro come validazione
        if Not Empty( Nvl(_Curs_VALIDEXP.ENFILTRO,"") ) 
          * --- Se entit� con filtro allora metto in AND le condizioni di filtro e validazione
          m.w_VALEXP = m.w_VALEXP + iif( Empty( m.w_VALEXP ) , "" , " Or " ) +"(" + Alltrim(_Curs_VALIDEXP.ENFILTRO)+" )"
        endif
      endif
    else
      * --- Se la sede � validatrice intrinseca la condizione di validiti� �
      *     data dai primi due caratteri del campo...
      * --- Metto in Or la condizione di validazione INTRINSECA, quindi aggiungo
      *     in automatico l'alias (tabella principale) al campo e da questo recupero
      *     i primi 2 caratteri che devono coincidere con l'identificativo della sede
      *     corrente...
      m.w_TMPS =  "[SUBSTR( "+Alltrim(m.w_TARCHIVIO)+"."+ Alltrim( m.w_VALCAM )+",1,2)] = [GLOBALVAR( g_cPrefiProg )]"
      m.w_VALEXP = m.w_VALEXP + iif( Empty( m.w_VALEXP ) , "" , " Or " ) +"(" + m.w_TMPS +" )"
    endif
      select _Curs_VALIDEXP
      continue
    enddo
    use
  endif
  * --- Nessuna condizione = validatore di tutto l'archivio
  m.w_VALEXP = Alltrim( iif( Empty( m.w_VALEXP ) , "1=1" , m.w_VALEXP ) )
  i_retcode = 'stop'
  i_retval = m.w_VALEXP
  return
endproc


  function validexp_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ENT_MAST'
    return(cp_OpenFuncTables(1))
* --- END VALIDEXP
* --- START VERTFILT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: vertfilt                                                        *
*              Lettura filtro verticale                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2008-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func vertfilt
param pSede,pTipo,pEntita,pArchivio,pCampo,pResult,pErrMess,pArrCampi

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_VALORE
  m.w_VALORE=space(254)
  private w_OVERW
  m.w_OVERW=space(1)
  private w_PVSTADIN
  m.w_PVSTADIN=space(1)
* --- WorkFile variables
  private PUBLVERT_idx
  PUBLVERT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "vertfilt"
if vartype(__vertfilt_hook__)='O'
  __vertfilt_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'vertfilt('+Transform(pSede)+','+Transform(pTipo)+','+Transform(pEntita)+','+Transform(pArchivio)+','+Transform(pCampo)+','+Transform(pResult)+','+Transform(pErrMess)+','+Transform(pArrCampi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'vertfilt')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if vertfilt_OpenTables()
  vertfilt_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'vertfilt('+Transform(pSede)+','+Transform(pTipo)+','+Transform(pEntita)+','+Transform(pArchivio)+','+Transform(pCampo)+','+Transform(pResult)+','+Transform(pErrMess)+','+Transform(pArrCampi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'vertfilt')
Endif
*--- Activity log
if vartype(__vertfilt_hook__)='O'
  __vertfilt_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure vertfilt_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Date
  *     Sede
  *     Entita
  *     Archivio e Campo
  *     (+ gli usuali parametri per gestire la messagistica a video)
  *     calcola se presente l'eventuale filtro verticale.
  *     Restituisce 0 se non ha trovato filtro, 1 se lo ha torvato ed applicato
  *     2 se, in fase di sincronizzazione desidero mantenere il valore sul DB
  *     con successo,3 indica un espressione dinamica -1 se c' un errore, in questo caso valorizza anche pErrMess
  *     con il messaggio di errore (pErrMess va passata per riferimento)
  *     Il valore lo restituisce all'interno del parametro pResult passato per riferimento
  * --- pResult non ha tipo, conterr� la macro dell'eventuale filtro verticale
  * --- Il campo potrebbe avere un filtro verticale per la sede...
  * --- PVSTADIN indica se l espressione del filtro � dinamica o statica
  m.w_VALORE = ""
  * --- Read from PUBLVERT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PUBLVERT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PUBLVERT_idx,2],.t.,PUBLVERT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PV_VALUE,PVOVERW,PVSTADIN"+;
      " from "+i_cTable+" PUBLVERT where ";
          +"PV__SEDE = "+cp_ToStrODBC(m.pSEDE);
          +" and PV__TIPO = "+cp_ToStrODBC(m.pTipo);
          +" and PVCODENT = "+cp_ToStrODBC(m.pENTITA);
          +" and PVCODARC = "+cp_ToStrODBC(m.pARCHIVIO);
          +" and PVFIELDS = "+cp_ToStrODBC(m.pCAMPO);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PV_VALUE,PVOVERW,PVSTADIN;
      from (i_cTable) where;
          PV__SEDE = m.pSEDE;
          and PV__TIPO = m.pTipo;
          and PVCODENT = m.pENTITA;
          and PVCODARC = m.pARCHIVIO;
          and PVFIELDS = m.pCAMPO;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_VALORE = NVL(cp_ToDate(_read_.PV_VALUE),cp_NullValue(_read_.PV_VALUE))
    w_OVERWRITE = NVL(cp_ToDate(_read_.PVOVERW),cp_NullValue(_read_.PVOVERW))
    m.w_PVSTADIN = NVL(cp_ToDate(_read_.PVSTADIN),cp_NullValue(_read_.PVSTADIN))
    use
    if i_Rows=0
    i_retcode = 'stop'
    i_retval = 0
    return
    endif
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if m.w_PVSTADIN="D"
    * --- Se filtro dinamico ritorno 3 e la funziona da applicare
    m.pResult = m.w_VALORE
    i_retcode = 'stop'
    i_retval = 3
    return
  endif
  * --- Se nessun filtro definito (se vuoto lo accetto in quanto potrebbe voler significar o dato
  *     vuoto o null, se campo con integrit� referenziale )
  *     
  *     Determinio il valore del filtro se sovrascrivo il dato o st� pubblicando
  if w_OVERWRITE="S" Or m.pTipo="R"
    * --- Il valore � sempre da intedere come una macro...
     
 Local L_OldErr, L_bErr, L_Macro 
 L_OldErr= ON("Error") 
 L_bErr=.f. 
 On Error L_bErr=.T. 
 L_Macro=m.w_VALORE
    m.w_VALORE = L_Macro
     
 On Error &L_OldErr
    if L_bErr
      * --- Se qualcosa va storto segnalo...
      m.pErrMess = AH_MsgFormat( "Sede %1, lettura valore default campo %2, Entit� %3 archivio %4 ha generato errore %5" , m.pSede, m.pCAMPO, m.pENTITA, m.pARCHIVIO, Message() )
      i_retcode = 'stop'
      i_retval = -1
      return
    else
      m.pResult = m.w_VALORE
      * --- Se tipo uguale Pubblicatore, allora potrei aver deciso di non sovrascrivere il
      *     dato, in questo caso restituisco 2 (i valore mi pu� servire se record da inserire)
      if w_OVERWRITE="N" And m.pTipo="P"
        i_retcode = 'stop'
        i_retval = 2
        return
      else
        i_retcode = 'stop'
        i_retval = 1
        return
      endif
    endif
  else
    * --- Se tipo uguale Pubblicatore, allora potrei aver deciso di non sovrascrivere il
    *     dato, in questo caso restituisco 2
    if w_OVERWRITE="N" And m.pTipo="P"
      i_retcode = 'stop'
      i_retval = 2
      return
    else
      i_retcode = 'stop'
      i_retval = 0
      return
    endif
  endif
endproc


  function vertfilt_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PUBLVERT'
    return(cp_OpenFuncTables(1))
* --- END VERTFILT
* --- START TRASLACLIDACF
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: traslaclidacf                                                   *
*              Ricerca il codice cliente conoscendone la partita iva           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-09                                                      *
* Last revis.: 2008-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func traslaclidacf
param pDatiRiga,pLog,pVerbose,pCampoIva,pAntipcon,pAncodice

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODFIS
  m.w_CODFIS=space(11)
  private w_TIPCON
  m.w_TIPCON=space(1)
  private w_ANCODICE
  m.w_ANCODICE=space(15)
* --- WorkFile variables
  private CONTI_idx
  CONTI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "traslaclidacf"
if vartype(__traslaclidacf_hook__)='O'
  __traslaclidacf_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'traslaclidacf('+Transform(pDatiRiga)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pCampoIva)+','+Transform(pAntipcon)+','+Transform(pAncodice)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'traslaclidacf')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if traslaclidacf_OpenTables()
  traslaclidacf_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_CONTI')
  use in _Curs_CONTI
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'traslaclidacf('+Transform(pDatiRiga)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pCampoIva)+','+Transform(pAntipcon)+','+Transform(pAncodice)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'traslaclidacf')
Endif
*--- Activity log
if vartype(__traslaclidacf_hook__)='O'
  __traslaclidacf_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure traslaclidacf_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione applicabiel come filtro verticale dinamico che dato il pacchetto determina il codice intestatario
  *     che sul database corrente ha una certa partita IVA.
  *     
  *     
  *     Assiomi filtri verticali dinamici:
  *     
  *     a) I filtri verticali possono usufruire come dati di partenza, dei soli dati sul pacchetto della tabella in esame.
  *     
  *     Corollario: Se definisco un qualsiasi tipo di filtro verticale su di un campo, la sua applicazione non pu� essere 
  *     utilizzata su filtri dinamici di altri campi. Nei filtri dinamici avr� sempre il valore del pacchetto.
  *     
  *     b) Se il filtro dinamico � applicato su di un campo appartenente alla chiave allora occorre che tutte le relazioni dell'entit� 
  *     che contengono il campo lo utilizzino in forma semplice(*).
  *     (*) Forma semplice significa che la condizione di Join tra archivio principale e collegato � di uguaglianza, quindi (A=B O B=A)
  *     
  *     La funzione riceve come parametro la variabile su cui si � appoggiata
  *     la SCATTER sulla riga della tabella principale in esame
  *     
  *     Eventuale oggetto su cui evidenziare il log
  *     Flag Verbose
  *     
  *     
  *     Le funzioni possono essere chiamate con i parametri  w_DATIRIGA  pLog e  pVerbose. Specificando tali parametri a tempo di esecuzione verranno passati (in w_DATIRIGA � un array 
  *     contenente il record di dati da sincronizzare - pLog conterra il riferimento al log degli errori- pVerbose indica se � richiesto una messaggistica dettagliata)
  *     
  *     esempio di chiamata : TraslacliDaCF( w_DATIRIGA  pLog ,  pVerbose ,  altri parametri)
  *     
  *     Gli altri parametri sono nomi di campi passati tamite l array pDatiRiga su cui la funzione deve lavorare (per esempio 'ANPARIVA,ANCODFIS' ). 
  * --- pDatiriga contiene l array dei dati (il record pasato per la sincronizzazione)
  *     pCAmpoIva contiene il nome del campo che contiene la partita iva (per esempio ANPARIVA oppure ANPARIVA1 , .... )
  *     pLog � il log dei messaggi
  *     pVerbose indica se � richiesta una messaggistica pi� dettagliata per il log (oltre ai messaggi di errore vengono visualizzati messaggi che indicano la cronologia delle operazioni che vengono svolte)
  l_COMANDO = "pDatiRiga."+ alltrim (m.pCampoIva)
  l_COMANDO2 = "pDatiRiga."+ alltrim (m.pAntipcon)
  l_COMANDO3 = "pDatiRiga."+ alltrim (m.pAncodice)
  m.w_TIPCON = &l_COMANDO2
  m.w_CODFIS = &l_COMANDO
  if Not Empty( m.w_CODFIS )
    * --- Select from CONTI
    i_nConn=i_TableProp[CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[CONTI_idx,2],.t.,CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ANCODICE from "+i_cTable+" CONTI ";
          +" where ANCODFIS="+cp_ToStrODBC(m.w_CODFIS)+" AND ANTIPCON="+cp_ToStrODBC(m.w_TIPCON)+"";
           ,"_Curs_CONTI")
    else
      select ANCODICE from (i_cTable);
       where ANCODFIS=m.w_CODFIS AND ANTIPCON=m.w_TIPCON;
        into cursor _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      select _Curs_CONTI
      locate for 1=1
      do while not(eof())
      m.w_ANCODICE = _Curs_CONTI.ANCODICE
      if VarType( m.pLog ) ="O" And m.pVerbose
        m.w_OLDCON = &l_COMANDO3
        AddMsgNL("Funzione TRASLACLI applicata ad intestatario su pacchetto %1 con codice fiscale %2, trasformato in %3 ", m.pLog, w_OLDCON, m.w_CODFIS , m.w_ANCODICE)
      endif
      exit
        select _Curs_CONTI
        continue
      enddo
      use
    endif
  endif
  if EMPTY (m.w_ANCODICE)
    m.w_ANCODICE = &l_COMANDO3
  endif
  i_retcode = 'stop'
  i_retval = m.w_ANCODICE
  return
endproc


  function traslaclidacf_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='CONTI'
    return(cp_OpenFuncTables(1))
* --- END TRASLACLIDACF
* --- START AGGMIRROR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: aggmirror                                                       *
*              Aggiorna mirror                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-11                                                      *
* Last revis.: 2009-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func aggmirror
param w_ARCHIVIO,ARRMIRROR,bTransaction,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CONDJOIN
  m.w_CONDJOIN=space(254)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_CONN
  m.w_CONN=0
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_LOOP
  m.w_LOOP=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_ELFIELD
  m.w_ELFIELD=space(200)
  private w_VALEXP
  m.w_VALEXP=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_JOIN
  m.w_JOIN=space(254)
  private w_IDXTABLEEXT
  m.w_IDXTABLEEXT=0
  private w_PHNAMEEXT
  m.w_PHNAMEEXT=space(50)
  private w_EXTJOINCOND
  m.w_EXTJOINCOND=space(254)
  private w_EXTJOINCONDINS
  m.w_EXTJOINCONDINS=space(254)
  private w_CONTAJOIN
  m.w_CONTAJOIN=0
  private w_EXT_TABLE
  m.w_EXT_TABLE=0
  private w_INSERT1
  m.w_INSERT1=space(254)
  private w_VALEXP_DB2
  m.w_VALEXP_DB2=space(254)
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private TAB_ESTE_idx
  TAB_ESTE_idx=0
  private CAMPIEST_idx
  CAMPIEST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "aggmirror"
if vartype(__aggmirror_hook__)='O'
  __aggmirror_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'aggmirror('+Transform(w_ARCHIVIO)+','+Transform(ARRMIRROR)+','+Transform(bTransaction)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggmirror')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if aggmirror_OpenTables()
  aggmirror_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_TAB_ESTE')
  use in _Curs_TAB_ESTE
endif
if used('_Curs_SUENDETT')
  use in _Curs_SUENDETT
endif
if used('_Curs_CAMPIEST')
  use in _Curs_CAMPIEST
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'aggmirror('+Transform(w_ARCHIVIO)+','+Transform(ARRMIRROR)+','+Transform(bTransaction)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggmirror')
Endif
*--- Activity log
if vartype(__aggmirror_hook__)='O'
  __aggmirror_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure aggmirror_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Aggiorna la tabella di Mirror, riceve l'Archivio, il nome logico della
  *     super entit� da aggiornare.
  *     Restituisce una array con alla posizione 
  *     1 i record aggiornati, 
  *     2 i record inseriti
  *     3 i record cancellati
  *     4 '' se tutto Ok altrimenti il msg di errore...
  * --- Se .t. svolge l'aggiornamento sotto transazione
  m.w_TARCHIVIO = Alltrim( m.w_ARCHIVIO )
  * --- Per aggiornare il Mirror devo costruire l'elenco dei filtri di validazione
  *     presenti nelle varie definizioni di Entit�
  m.w_VALEXP = VALIDEXP( m.w_TARCHIVIO )
  * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
  =cp_ReadXdc()
  m.w_IDXTABLE = cp_OpenTable( m.w_TARCHIVIO ,.T.)
  m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
  * --- Costruisco la condizione di Join tra archivio e Mirror
  *     Nel caso di DB2 Oracle non devo usare alias
  *     perch� nell'update la condizione di join diventa una condizione di Where
  m.w_MIRROR = Alltrim(GETMIRRORNAME( m.w_TARCHIVIO ))
  if CP_DBTYPE="SQLServer"
    m.w_CONDJOIN = CREAJOIN( m.w_TARCHIVIO , "" , "MIRROR" ) 
  else
    m.w_CONDJOIN = CREAJOIN( m.w_TARCHIVIO , m.w_PHNAME , w_MIRROR ) 
  endif
  if CP_DBTYPE="SQLServer"
    m.w_JOIN = m.w_PHNAME+" "+m.w_TARCHIVIO+" Inner Join "+w_MIRROR +" MIRROR on "+m.w_CONDJOIN
    m.w_JOIN = m.w_JOIN + " And "+m.w_TARCHIVIO+".CPCCCHK<>MIRROR.CPCCCHK"
  else
    m.w_JOIN = " Where "+m.w_CONDJOIN
    m.w_JOIN = m.w_JOIN + " And "+m.w_PHNAME+".CPCCCHK<>" + w_MIRROR+ ".CPCCCHK"
  endif
  * --- Aggiunge le condizioni di join per le tabelle estese
  m.w_EXTJOINCOND = ""
  m.w_EXTJOINCONDINS = ""
  m.w_CONTAJOIN = 1
  * --- Select from TAB_ESTE
  i_nConn=i_TableProp[TAB_ESTE_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[TAB_ESTE_idx,2],.t.,TAB_ESTE_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_ESTE ";
        +" where ESCODTAB="+cp_ToStrODBC(m.w_TARCHIVIO)+"";
         ,"_Curs_TAB_ESTE")
  else
    select * from (i_cTable);
     where ESCODTAB=m.w_TARCHIVIO;
      into cursor _Curs_TAB_ESTE
  endif
  if used('_Curs_TAB_ESTE')
    select _Curs_TAB_ESTE
    locate for 1=1
    do while not(eof())
    m.w_EXT_TABLE = alltrim(_Curs_TAB_ESTE.ES_TABLE)
    m.w_CONTAJOIN = m.w_CONTAJOIN + 1
    m.w_IDXTABLEEXT = cp_OpenTable( m.w_EXT_TABLE ,.T.)
    m.w_PHNAMEEXT = cp_SetAzi(i_TableProp[ m.w_IDXTABLEEXT ,2]) 
    * --- Per SQL Server l'alias � il nome logico, per db2 e oracle il nome fisico
    if CP_DBTYPE="SQLServer"
      m.w_EXTJOINCOND = m.w_EXTJOINCOND + " Left Outer Join " + alltrim (m.w_PHNAMEEXT) +" " + alltrim (_Curs_TAB_ESTE.ESTABALI) +" ON " + Alltrim( _Curs_TAB_ESTE.ESRELAZI )+" )"
      m.w_EXTJOINCONDINS = m.w_EXTJOINCOND
    else
      m.w_EXTJOINCOND = m.w_EXTJOINCOND + " Left Outer Join " + alltrim (m.w_PHNAMEEXT) +" " + alltrim (_Curs_TAB_ESTE.ESTABALI) +" ON " + Repalias( _Curs_TAB_ESTE.ESRELAZI , m.w_TARCHIVIO, m.w_PHNAME )+" )"
      * --- Per la select in inserimento non va utilizzato come alias dell'archivio il nome fisico
      m.w_EXTJOINCONDINS = m.w_EXTJOINCONDINS + " Left Outer Join " + alltrim (m.w_PHNAMEEXT) +" " + alltrim (_Curs_TAB_ESTE.ESTABALI) +" ON " + Alltrim( _Curs_TAB_ESTE.ESRELAZI)+" )"
    endif
      select _Curs_TAB_ESTE
      continue
    enddo
    use
  endif
  if CP_DBTYPE="SQLServer"
    m.w_JOIN = REPLICATE("(",m.w_CONTAJOIN-1) + m.w_JOIN+ m.w_EXTJOINCOND
  else
    m.w_JOIN = REPLICATE("(",m.w_CONTAJOIN-1) +m.w_PHNAME + m.w_EXTJOINCOND +m.w_JOIN
  endif
  m.w_JOIN = " From "+m.w_JOIN
  m.w_CONN = i_TableProp[ cp_OpenTable( "TEMP_PUBBL" ,.T.) , 3 ] 
  ARRMIRROR[4] = ""
  * --- Try
  aggmirror_Try_036A4F08()
  * --- Catch
  if !empty(i_Error)
    i_ErrMsg=i_Error
    i_Error=''
    if m.bTransaction
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    ARRMIRROR[4] = AH_MsgFormat( "Comando %1 ha generato errore %2" , m.w_CMDSQL, Message() )
  endif
  * --- End
endproc
  proc aggmirror_Try_036A4F08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  if m.bTransaction
    * --- begin transaction
    cp_BeginTrs()
  endif
  * --- Primo passo aggiorno i CPCCCHK dei record modificati, quindi svolgo un'UPDATE
  *     sulla tabella Mirror
  if CP_DBTYPE="SQLServer"
    m.w_CMDSQL = "Update " + w_MIRROR + " Set CPCCCHK="+ m.w_TARCHIVIO+".CPCCCHK"
    * --- Se il record � cancellato marco la cancellazione come pubblicata DELETE_PU=1
    m.w_CMDSQL = m.w_CMDSQL + " , DELETE_PU="+cp_SetSQLFunctions( " [CASE2( [NVL("+m.w_TARCHIVIO+".CPCCCHK , '')]='', 1,0 )]", CP_DBTYPE)
  else
    m.w_CMDSQL = "Update " + w_MIRROR + " Set CPCCCHK= ( Select "+ m.w_PHNAME +".CPCCCHK "+ m.w_JOIN +" )"
    * --- Se il record � cancellato marco la cancellazione come pubblicata DELETE_PU=1
    m.w_CMDSQL = m.w_CMDSQL + "  , DELETE_PU= ( Select "+ cp_SetSQLFunctions( " [CASE2( [NVL("+m.w_PHNAME+".CPCCCHK , '')]='', 1,0 )]", CP_DBTYPE) +" "+ m.w_JOIN +" )"
  endif
  * --- Devo leggere la chiave dall'analisi...
  * --- Aggiungo ai vari campi la tabella di appartenenza (da CAMPO a ARCHIVIO.CAMPO)
  m.w_LOOP = 1
  m.w_BFIRST = .T.
  m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )+",CPCCCHK "
  m.w_ELFIELD = ""
  do while m.w_LOOP<>0
    m.w_LOOP = At( "," , m.w_CHIAVE )
    if m.w_LOOP>0
      m.w_CAMPO = Left( m.w_CHIAVE , m.w_LOOP -1 )
    else
      m.w_CAMPO = Alltrim( m.w_CHIAVE )
    endif
    * --- Costruisco l'elenco dei campi
    m.w_ELFIELD = m.w_ELFIELD +iif( Not m.w_BFIRST , " , ","")+ m.w_TARCHIVIO+"." +w_CAMPO
    m.w_CHIAVE = Alltrim( Substr( m.w_CHIAVE , m.w_LOOP+1 , Len ( m.w_CHIAVE ) ) )
    m.w_BFIRST = .F.
  enddo
  * --- Vado a ricercare eventuali campi supplementari definiti nella Super Entit�...
  m.w_INSERT1 = ""
  * --- Select from SUENDETT
  i_nConn=i_TableProp[SUENDETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[SUENDETT_idx,2],.t.,SUENDETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select SU_CAMPO from "+i_cTable+" SUENDETT ";
        +" where SUCODICE="+cp_ToStrODBC(m.w_TARCHIVIO)+"";
         ,"_Curs_SUENDETT")
  else
    select SU_CAMPO from (i_cTable);
     where SUCODICE=m.w_TARCHIVIO;
      into cursor _Curs_SUENDETT
  endif
  if used('_Curs_SUENDETT')
    select _Curs_SUENDETT
    locate for 1=1
    do while not(eof())
    * --- Se il campo non � gia compreso lo aggiungo
    if Not m.w_TARCHIVIO+"." +Alltrim(_Curs_SUENDETT.SU_CAMPO) $ m.w_CMDSQL
      if CP_DBTYPE="SQLServer"
        m.w_CMDSQL = m.w_CMDSQL +" , " +Alltrim(_Curs_SUENDETT.SU_CAMPO) +"="+m.w_TARCHIVIO+"." +Alltrim(_Curs_SUENDETT.SU_CAMPO) 
      else
        m.w_CMDSQL = m.w_CMDSQL +" , " +Alltrim(_Curs_SUENDETT.SU_CAMPO) +"=( Select "+ m.w_PHNAME+"."+Alltrim(_Curs_SUENDETT.SU_CAMPO) + m.w_JOIN + " )"
      endif
    endif
    if Not Alltrim(_Curs_SUENDETT.SU_CAMPO) $ cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )
      * --- Campi per la Insert, nomi e valori...
      m.w_INSERT1 = m.w_INSERT1 + "," + Alltrim(_Curs_SUENDETT.SU_CAMPO)
      m.w_ELFIELD = m.w_ELFIELD +","+ m.w_TARCHIVIO+"." +Alltrim(_Curs_SUENDETT.SU_CAMPO)
    endif
      select _Curs_SUENDETT
      continue
    enddo
    use
  endif
  * --- Vado ad aggiungere i campi delle tabelle estese....
  *     Se dato presente sul database recupero dall'archivio, se 
  *     dato cancellato dal mirror...
  * --- Select from CAMPIEST
  i_nConn=i_TableProp[CAMPIEST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CAMPIEST_idx,2],.t.,CAMPIEST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAMPIEST ";
        +" where CMCODTAB="+cp_ToStrODBC(m.w_TARCHIVIO)+"";
         ,"_Curs_CAMPIEST")
  else
    select * from (i_cTable);
     where CMCODTAB=m.w_TARCHIVIO;
      into cursor _Curs_CAMPIEST
  endif
  if used('_Curs_CAMPIEST')
    select _Curs_CAMPIEST
    locate for 1=1
    do while not(eof())
    if CP_DBTYPE="SQLServer"
      m.w_CMDSQL = m.w_CMDSQL +" , " +alltrim (_Curs_CAMPIEST.CM_ALIAS) +"="+Alltrim(_Curs_CAMPIEST.CMTABALI)+"." +alltrim (_Curs_CAMPIEST.CM_CAMPO)
    else
      * --- x Oracle e DB2 si � scelto di ripetere tutta la Join in realt� pu�
      *     essere sufficiente eseguire la sola Join collegata alla tabella di estensione...
      m.w_CMDSQL = m.w_CMDSQL +" , " +alltrim (_Curs_CAMPIEST.CM_ALIAS) +"=( Select "+ alltrim (_Curs_CAMPIEST.CMTABALI)+"."+alltrim (_Curs_CAMPIEST.CM_CAMPO) + m.w_JOIN + " )"
    endif
    * --- Campi per la Insert, nomi e valori...
    m.w_INSERT1 = m.w_INSERT1 + "," +alltrim (_Curs_CAMPIEST.CM_ALIAS)
    m.w_ELFIELD = m.w_ELFIELD +","+Alltrim(_Curs_CAMPIEST.CMTABALI)+"." +alltrim (_Curs_CAMPIEST.CM_CAMPO)
      select _Curs_CAMPIEST
      continue
    enddo
    use
  endif
  * --- Valorizzazione ANCESTOR
  * --- Attribuzione versioning, crc pubblicatore e sede pubblicatore
  *     (solo se validatore pubblica).
  *     La Versione � aggiornata solo se il CPCCCHK sul dato � diverso
  *     dal CPCCCHK di pubblicazione e sono validatore.
  *     Quando il validatore pubblica, pubblica anche per se stesso per cui
  *     va ad aggiornare sul suo database i dati di pubblicazione
  if CP_DBTYPE="SQLServer"
    m.w_CMDSQL = m.w_CMDSQL+" , ANCESTOR = "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP +" , "+m.w_TARCHIVIO+".CPCCCHK ,MIRROR.ANCESTOR )] ",CP_DBTYPE)
    m.w_CMDSQL = m.w_CMDSQL+" , AVERSION = AVERSION + "+cp_SetSQLFunctions("[CASE2( "+ Alltrim(m.w_VALEXP) +" And "+m.w_TARCHIVIO+".CPCCCHK<>[NVL(MIRROR.CPCCCHK_PU , "+Cp_ToStrODBC("XXXXXXXXXXX")+" )] , 1 ,0 )] ",CP_DBTYPE)
    m.w_CMDSQL = m.w_CMDSQL+" , CPCCCHK_PU = "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP +" , "+m.w_TARCHIVIO+".CPCCCHK ,MIRROR.CPCCCHK_PU )] ",CP_DBTYPE)
    m.w_CMDSQL = m.w_CMDSQL+" , PUBCOSED = "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP +" , "+ cp_ToStrODBC(Get_Sede()) +" ,MIRROR.PUBCOSED )] ",CP_DBTYPE)
    m.w_CMDSQL = m.w_CMDSQL + m.w_JOIN
  else
    * --- Nel caso di DB2 e Oracle devo usare il nome fisico della tabella
    m.w_VALEXP_DB2 = REPALIAS( m.w_VALEXP , m.w_TARCHIVIO, m.w_PHNAME)
    m.w_CMDSQL = m.w_CMDSQL+" , ANCESTOR = ( Select "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP_DB2 +" , "+m.w_PHNAME+".CPCCCHK,  "+w_MIRROR+".ANCESTOR )] ",CP_DBTYPE)+" "+m.w_JOIN+" )"
    m.w_CMDSQL = m.w_CMDSQL+" , AVERSION = AVERSION + ( Select "+cp_SetSQLFunctions("[CASE2( "+ Alltrim(m.w_VALEXP_DB2) +" And "+m.w_PHNAME+".CPCCCHK<>[NVL("+w_MIRROR+".CPCCCHK_PU , "+Cp_ToStrODBC("XXXXXXXXXXX")+" )] , 1 ,0 )] ",CP_DBTYPE)+" "+m.w_JOIN+" )"
    m.w_CMDSQL = m.w_CMDSQL+" , CPCCCHK_PU = (Select "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP_DB2 +" , "+m.w_PHNAME+".CPCCCHK ,"+w_MIRROR+".CPCCCHK_PU )] ",CP_DBTYPE) +" "+m.w_JOIN+" )"
    m.w_CMDSQL = m.w_CMDSQL+" , PUBCOSED = (Select "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP_DB2 +" , "+ cp_ToStrODBC(Get_Sede()) +" ,"+ w_MIRROR+".PUBCOSED )] ",CP_DBTYPE)+" "+m.w_JOIN+" )"
    m.w_CMDSQL = m.w_CMDSQL+ " Where Exists ( select 1 " + m.w_JOIN + " )"
  endif
  ARRMIRROR[1]= cp_TrsSQL( m.w_CONN , m.w_CMDSQL ) 
  if bTrsErr
     
 i_Error=MSG_WRITE_ERROR 
 Return
  else
    if VarType( m.pLog )="O" And m.pVerbose
      AddMsgNL("Aggiornamento mirror (Update) super entit� %1, frase %2",m.pLog, m.w_ARCHIVIO , m.w_CMDSQL , , ,, )
    endif
  endif
  * --- Secondo Passo: Aggiorno alla tabella Mirror i record inseriti..
  * --- Ricalcolo la condizione di Join poich� per l'insert � corretta quella standart Sql Server
  m.w_CONDJOIN = CREAJOIN( m.w_TARCHIVIO , "" , "MIRROR" ) 
  m.w_CMDSQL = "Insert Into " + GETMIRRORNAME( m.w_TARCHIVIO ) +" ( "+cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )+",CPCCCHK"+ m.w_INSERT1 + ",ANCESTOR,AVERSION, CPCCCHK_PU ,PUBCOSED,DELETE_PU) "
  m.w_CMDSQL = m.w_CMDSQL + " SELECT "+m.w_ELFIELD +"," +cp_SetSQLFunctions("[CASE2( "+ Alltrim(m.w_VALEXP) +" , "+m.w_TARCHIVIO+".CPCCCHK ,MIRROR.ANCESTOR )] ",CP_DBTYPE)+" As ANCESTOR, "
  m.w_CMDSQL = m.w_CMDSQL +cp_SetSQLFunctions("[CASE2( "+ Alltrim(m.w_VALEXP) +" , 1 ,0 )] ",CP_DBTYPE)
  m.w_CMDSQL = m.w_CMDSQL+" ,  "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP +" , "+m.w_TARCHIVIO+".CPCCCHK ,MIRROR.CPCCCHK_PU )] ",CP_DBTYPE)
  m.w_CMDSQL = m.w_CMDSQL+" ,  "+cp_SetSQLFunctions("[CASE2( "+ m.w_VALEXP +" , "+ cp_ToStrODBC(Get_Sede()) +" ,MIRROR.PUBCOSED )] ",CP_DBTYPE)+",0"
  m.w_CMDSQL = m.w_CMDSQL +" From "+REPLICATE("(",m.w_CONTAJOIN) +m.w_PHNAME+" "+m.w_TARCHIVIO+" Left Outer Join " +GETMIRRORNAME( m.w_TARCHIVIO ) +" MIRROR on "+m.w_CONDJOIN+" ) "+ m.w_EXTJOINCONDINS
  m.w_CMDSQL = m.w_CMDSQL +" Where MIRROR.CPCCCHK Is Null And "+m.w_TARCHIVIO+".CPCCCHK Is Not Null "
  ARRMIRROR[2] = cp_TrsSQL( m.w_CONN , m.w_CMDSQL ) 
  if bTrsErr
     
 i_Error=MSG_INSERT_ERROR 
 Return
  else
    if VarType( m.pLog )="O" And m.pVerbose
      AddMsgNL("Aggiornamento mirror (Insert) super entit� %1, frase %2",m.pLog, m.w_ARCHIVIO , m.w_CMDSQL , , ,, )
    endif
  endif
  * --- Terzo passo, elimino i record eliminati da ad hoc anche dal mirror
  *     Non elimino i record per cui sono validatore
  m.w_CONDJOIN = CREAJOIN( m.w_TARCHIVIO , "" , GETMIRRORNAME( m.w_TARCHIVIO ) ) 
  * --- La condizione di validazione la utilizzo per non cancellare record nel
  *     mirror del validatore cancellati dal database dal validatore.
  *     
  *     Sostuisco l'alias originale con quello del Mirror.
  m.w_VALEXP = REPALIAS( m.w_VALEXP , m.w_TARCHIVIO, GETMIRRORNAME( m.w_TARCHIVIO ))
  m.w_CMDSQL = "Delete From " + GETMIRRORNAME( m.w_TARCHIVIO ) +" where Not exists( select 1 from "+ m.w_PHNAME+" " +m.w_TARCHIVIO
  m.w_CMDSQL = m.w_CMDSQL + " where ("+ m.w_CONDJOIN+ " ) ) And  Not ("+cp_SetSQLFunctions(m.w_VALEXP, CP_DBTYPE )+")"
  ARRMIRROR[3] = cp_TrsSQL( m.w_CONN , m.w_CMDSQL ) 
  if bTrsErr
     
 i_Error=MSG_DELETE_ERROR 
 Return
  else
    if VarType( m.pLog )="O" And m.pVerbose
      AddMsgNL("Aggiornamento mirror (Delete) super entit� %1, frase %2",m.pLog, m.w_ARCHIVIO , m.w_CMDSQL , , ,, )
    endif
  endif
  * --- Per i record cancellati dalle tabelle del database per cui si � validatori imposta il campo DELETE_PU=1 per non pubblicare pi� tali record
  m.w_CMDSQL = "Update " + GETMIRRORNAME( m.w_TARCHIVIO ) +" set DELETE_PU=1 where DELETE_PU=0 AND Not exists( select 1 from "+ m.w_PHNAME+" " +m.w_TARCHIVIO
  m.w_CMDSQL = m.w_CMDSQL + " where ("+ m.w_CONDJOIN+ " ) ) "
  cp_TrsSQL( m.w_CONN , m.w_CMDSQL ) 
  if bTrsErr
     
 i_Error=MSG_DELETE_ERROR 
 Return
  else
    if VarType( m.pLog )="O" And m.pVerbose
      AddMsgNL("Aggiornamento campo DELETE_PU del mirror per gerstire i cancellati pubblicati della super entit� %1, frase %2",m.pLog, m.w_ARCHIVIO , m.w_CMDSQL , , ,, )
    endif
  endif
  if m.bTransaction
    * --- commit
    cp_EndTrs(.t.)
  endif
    return


  function aggmirror_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='SUENDETT'
    i_cWorkTables[2]='TAB_ESTE'
    i_cWorkTables[3]='CAMPIEST'
    return(cp_OpenFuncTables(3))
* --- END AGGMIRROR
* --- START AGGSUPENT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: aggsupent                                                       *
*              Aggiorna super entit� da TSM                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-08                                                      *
* Last revis.: 2015-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func aggsupent
param w_ARCHNAME,w_ARC_PADRE,w_RETNUM,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_TEMPIDX
  m.w_TEMPIDX=0
  private w_TEMPNAME
  m.w_TEMPNAME=space(30)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_EL_CMDSQL
  m.w_EL_CMDSQL=space(254)
  private w_MR_TABLE
  m.w_MR_TABLE=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_DBTYPE
  m.w_DBTYPE=space(50)
  private w_NCONN
  m.w_NCONN=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_TABLE_IDX
  m.w_TABLE_IDX=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_cJOINCOND
  m.w_cJOINCOND=space(254)
  private w_LINKBTWTEMP
  m.w_LINKBTWTEMP=space(30)
  private w_LINKTOPADRE
  m.w_LINKTOPADRE=space(30)
  private w_LINKTOSON
  m.w_LINKTOSON=space(30)
  private w_PHNAME_PADRE
  m.w_PHNAME_PADRE=space(50)
  private w_TempTablePadre
  m.w_TempTablePadre=space(30)
  private w_IDXTABLE_PADRE
  m.w_IDXTABLE_PADRE=0
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_CHIAVE_PRIM
  m.w_CHIAVE_PRIM=space(30)
  private w_PRIMA
  m.w_PRIMA=.f.
  private w_IDXTABLE2
  m.w_IDXTABLE2=0
  private w_LINK
  m.w_LINK=space(30)
  private w_CURNAME
  m.w_CURNAME=space(10)
  private w_ELFIELDNULL
  m.w_ELFIELDNULL=space(200)
  private w_BRES
  m.w_BRES=space(0)
* --- WorkFile variables
  private TEMP_PUBBL_idx
  TEMP_PUBBL_idx=0
  private TEMP_TSM_idx
  TEMP_TSM_idx=0
  private TAB_SECO_idx
  TAB_SECO_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "aggsupent"
if vartype(__aggsupent_hook__)='O'
  __aggsupent_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'aggsupent('+Transform(w_ARCHNAME)+','+Transform(w_ARC_PADRE)+','+Transform(w_RETNUM)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggsupent')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if aggsupent_OpenTables()
  aggsupent_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'aggsupent('+Transform(w_ARCHNAME)+','+Transform(w_ARC_PADRE)+','+Transform(w_RETNUM)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggsupent')
Endif
*--- Activity log
if vartype(__aggsupent_hook__)='O'
  __aggsupent_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure aggsupent_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione che aggiorna la tabella relativa alla super entit� in base ai record 
  *     elaborati per il controllo modifiche alle singole TSM della super entit�.
  *     
  *     Quindi andiamo a forzare il CPCCCHK della tabella principale se la
  *     tabella secondaria � modificata e se la tabella principale non �
  *     gia nella tabella di generazione pubblicazione.
  *     
  *     Con il TEMP_TSM creato dalla CREATETSMTEMP aggiorno i record della super entit� con una frase del tipo
  *     
  *     Update DOC_MAST set CPCCCHK = cp_ToStrODBC(cp_NewCCChk()) from 
  *     DOC_MAST inner join TEMP_TSM on 
  *     DOC_MAST.MVSERIAL = TEMP_TSM.MVSERIAL 
  *     left outer join TEMP_PUBBL on
  *     TEMP_TSM.MVSERIAL = TEMP_PUBBL.MVSERIAL
  *     where TEMP_PUBBL.MVSERIAL is null
  *     Group by TEMP_TSM.MVSERIAL
  * --- Controllo se sono stati inseriti dei record altrimenti mi fermo
  *     Parametri
  *     w_ARCHNAME= nome archivio collegato
  *     w_ARC_PADRE = Nome tabella principale
  *     w_RETNUM= Variabile passata per riferimento, conterr� i record per i quali sar� aggiornato il CPCCCHK sulla tabella principale
  *     pLog, pVerbose = Usuali parametri per mostrare a vide l'attivit� della funzione
  =cp_ReadXdc()
  m.w_TARCHIVIO = Alltrim( m.w_ARC_PADRE )
  m.w_IDXTABLE_PADRE = cp_OpenTable( Alltrim(m.w_ARC_PADRE) ,.T.)
  m.w_PHNAME_PADRE = cp_SetAzi(i_TableProp[ m.w_IDXTABLE_PADRE ,2]) 
  m.w_IDXTABLE = cp_OpenTable( "TEMP_PUBBL" ,.T.)
  m.w_TempTablePadre = i_TableProp[ m.w_IDXTABLE, 2] 
  m.w_IDXTABLE2 = cp_OpenTable( "TEMP_TSM" ,.T.)
  m.w_TEMPNAME = i_TableProp[ m.w_IDXTABLE2 ,2] 
  m.w_LINKTOSON = Alltrim( Strtran(m.w_LINK, Alltrim(m.w_ARC_PADRE), Alltrim(m.w_TempTablePadre) ))
  m.w_NCONN = i_TableProp[ m.w_IDXTABLE , 3 ] 
  m.w_RESULT = ""
  m.w_CURNAME = Sys(2015)
   
 Local TSM_CURSOR , TestMacro 
 TSM_CURSOR = m.w_CURNAME
  * --- Leggo il link tra i due archivi...
  * --- Read from TAB_SECO
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[TAB_SECO_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[TAB_SECO_idx,2],.t.,TAB_SECO_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "TS__LINK"+;
      " from "+i_cTable+" TAB_SECO where ";
          +"TSCODICE = "+cp_ToStrODBC(m.w_ARC_PADRE);
          +" and TSCODTAB = "+cp_ToStrODBC(m.w_ARCHNAME);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      TS__LINK;
      from (i_cTable) where;
          TSCODICE = m.w_ARC_PADRE;
          and TSCODTAB = m.w_ARCHNAME;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_LINK = NVL(cp_ToDate(_read_.TS__LINK),cp_NullValue(_read_.TS__LINK))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if Empty( m.w_LINK )
    m.w_RESULT = ah_msgformat("Non definita relazione in super entit� di %1 con %2. Impossibile proseguire.", m.w_TARCHIVIO , Alltrim( m.w_ARCHNAME ) )
  else
    * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
    =sqlexec( m.w_nConn , "Select Count(*) as TSM_Added from "+ m.w_TEMPNAME, m.w_CURNAME )
     TestMacro=Reccount(m.w_CURNAME) >0 And &TSM_CURSOR..TSM_Added>0
    if TestMacro
      if m.w_IDXTABLE=0 Or m.w_IDXTABLE2=0
        m.w_RESULT = ah_msgformat("Archvio %1 o %2 non esistente, impossibile aggiornare super entit�", "TEMP_PUBBL", "TEMP_TSM" )
      else
        m.w_DBTYPE = cp_GetDatabaseType( m.w_NCONN )
        if m.w_DBTYPE="Unknown"
          m.w_RESULT = ah_msgformat("Per archivio %1 non � possibile determinare il tipo di server , impossibile creare tabella registrazioni variate", m.w_TARCHIVIO )
        else
          * --- Nel temporaneo sul database ho i dati aggiornati come se avessi
          *     gia aggiornato il mirror...
          *     Il mirror � aggiornato alla fine, a pubblicazione avvenuta con successo...
          m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
          * --- Devo leggere la chiave dall'analisi...
          m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )
          * --- Se chiave multipla prendo solo il primo campo
          if At( "," , m.w_CHIAVE )>0
            m.w_CAMPO = Alltrim( Left( m.w_CHIAVE , At( "," , m.w_CHIAVE ) -1 ) )
          else
            m.w_CAMPO = Alltrim(m.w_CHIAVE)
          endif
          * --- Nel caso in cui la chiave fosse multipla mi basta che il valore del primo campo
          *     sia null per capire che non il record della tabella principale (super entit�)
          *     non sia gi� stato considerato
          m.w_ELFIELDNULL = m.w_TempTablePadre+"." +m.w_CAMPO +" is null "
          m.w_ELFIELDNULL = " ( " + m.w_ELFIELDNULL + " ) "
          * --- Due indici, uno per l'elenco delle tabelle (w_IDXTABLE) ed uno per
          *     navigare all'interno dell'XDC (w_TABLE_IDX)
          m.w_TABLE_IDX = I_dcx.GetTableIdx( m.w_TARCHIVIO )
          m.w_CMDSQL = ""
          m.w_LINKTOPADRE = Alltrim( StrTran(m.w_LINK, Alltrim(m.w_ARCHNAME), Alltrim(m.w_TEMPNAME) ))
          m.w_LINKBTWTEMP = Alltrim( StrTran(m.w_LINKTOPADRE, Alltrim(m.w_ARC_PADRE), Alltrim(m.w_TempTablePadre) ))
          * --- Aggiungo la left outer join con la tabella TEMP_PUBBL del padre per escludere quelle
          *     gi� considerati dalla super entit�
          * --- Aggiungo la join verso Temp_Pubbl del padre per escludere quelli gi� esaminati dalla super entit�
          m.w_cJOINCOND = m.w_cJOINCOND + " left outer join " + m.w_TempTablePadre + " ON " + m.w_LinkToPadre
          * --- Vado a forzare il CPCCCHK della tabella principale, escludo da questa scrittura
          *     eventuali record presenti nella tabella di mirror (cpccchk is null).
          *     Devo considerare anche eventuali record inviati da una sede, modificati
          *     tramite dettaglio e successiavmente re inviati. In questo caso sono nel
          *     mirror, devo comunque aggioranre il CPCCCHK altrimenti non sarebbero re inviati alla
          *     sede che in origine li aveva inviati (CPCCCHK di pubblicazione=CPCCCHK attuale)
          * --- Update DOC_MAST set CPCCCHK = cp_ToStrODBC(cp_NewCCChk()) from 
          *     DOC_MAST inner join TEMP_TSM on 
          *     DOC_MAST.MVSERIAL = TEMP_TSM.MVSERIAL 
          *     left outer join TEMP_PUBBL on
          *     TEMP_TSM.MVSERIAL = TEMP_PUBBL.MVSERIAL
          *     where ( TEMP_PUBBL.MVSERIAL is null or CPCCCHK_PU=PUCPCCCHK )
          m.w_CMDSQL = "Update "+ m.w_PHNAME_PADRE + " set CPCCCHK = " + cp_ToStrODBC(cp_NewCCChk()) + " From "
          m.w_CMDSQL = m.w_CMDSQL + m.w_PHNAME_PADRE + " " + m.w_TARCHIVIO +" inner join " + m.w_TEMPNAME +" on "
          m.w_CMDSQL = m.w_CMDSQL + m.w_LINKTOPADRE +" left outer join " + m.w_TempTablePadre + " on "
          m.w_CMDSQL = m.w_CMDSQL + m.w_LINKBTWTEMP +" where ("+ m.w_ELFIELDNULL+ " Or "+m.w_TempTablePadre +".CPCCCHK_PU="+ m.w_TempTablePadre+".PUCPCCCHK"+")"
          m.w_BRES = cp_TrsSQL( m.w_NCONN , m.w_CMDSQL )
          if m.w_BRES<0
            m.w_RESULT = ah_msgformat("Impossibile aggiornare archivio %1%0Errore %2, frase:%0%3", m.w_TARCHIVIO , Message(), m.w_CMDSQL )
            if VarType( m.pLog ) ="O" And m.pVerbose
              AddMsgNL("Impossibile aggiornare archivio %1%0Errore %2, frase:%0%3", m.w_TARCHIVIO , Message(), m.w_CMDSQL ,, ,)
            endif
          else
            if VarType( m.pLog ) ="O" And m.pVerbose
              AddMsgNL("Aggiornato archivio %1%0Frase:%0%2", m.w_TARCHIVIO , m.w_CMDSQL ,, ,)
            endif
            m.w_RESULT = ""
          endif
          m.w_RETNUM = IIF(m.w_BRES<0, 0, m.w_BRES)
        endif
      endif
    endif
    if Used(m.w_CURNAME)
       
 Select (m.w_CURNAME) 
 Use
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function aggsupent_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='TEMP_PUBBL'
    i_cWorkTables[2]='TEMP_TSM'
    i_cWorkTables[3]='TAB_SECO'
    return(cp_OpenFuncTables(3))
* --- END AGGSUPENT
* --- START AGGTSMENT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: aggtsment                                                       *
*              Aggiorna super entit� da TSM                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-08                                                      *
* Last revis.: 2015-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func aggtsment
param w_ARCHNAME,w_ARC_PADRE,w_RETNUM,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_TEMPIDX
  m.w_TEMPIDX=0
  private w_TEMPNAME
  m.w_TEMPNAME=space(30)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_EL_CMDSQL
  m.w_EL_CMDSQL=space(254)
  private w_MR_TABLE
  m.w_MR_TABLE=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_DBTYPE
  m.w_DBTYPE=space(50)
  private w_NCONN
  m.w_NCONN=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_TABLE_IDX
  m.w_TABLE_IDX=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_cJOINCOND
  m.w_cJOINCOND=space(254)
  private w_LINKBTWTEMP
  m.w_LINKBTWTEMP=space(30)
  private w_LINKTOPADRE
  m.w_LINKTOPADRE=space(30)
  private w_LINKTOSON
  m.w_LINKTOSON=space(30)
  private w_PHNAME_PADRE
  m.w_PHNAME_PADRE=space(50)
  private w_TempTablePadre
  m.w_TempTablePadre=space(30)
  private w_IDXTABLE_PADRE
  m.w_IDXTABLE_PADRE=0
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_CHIAVE_PRIM
  m.w_CHIAVE_PRIM=space(30)
  private w_PRIMA
  m.w_PRIMA=.f.
  private w_IDXTABLE2
  m.w_IDXTABLE2=0
  private w_LINK
  m.w_LINK=space(30)
  private w_CURNAME
  m.w_CURNAME=space(10)
  private w_BRES
  m.w_BRES=space(0)
* --- WorkFile variables
  private TEMP_TSM_idx
  TEMP_TSM_idx=0
  private TAB_SECO_idx
  TAB_SECO_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "aggtsment"
if vartype(__aggtsment_hook__)='O'
  __aggtsment_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'aggtsment('+Transform(w_ARCHNAME)+','+Transform(w_ARC_PADRE)+','+Transform(w_RETNUM)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggtsment')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if aggtsment_OpenTables()
  aggtsment_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'aggtsment('+Transform(w_ARCHNAME)+','+Transform(w_ARC_PADRE)+','+Transform(w_RETNUM)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggtsment')
Endif
*--- Activity log
if vartype(__aggtsment_hook__)='O'
  __aggtsment_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure aggtsment_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione che aggiorna la tabella relativa alla super entit� in base ai record 
  *     elaborati per il controllo modifiche alle TSM considerate in Distinct.
  *     L'aggiornamento delle Super Entit� � massivo. Se una TSM apartiene a pi� 
  *     Super Entit�, le super entit� vengono aggiornare contemporaneamente in questo passaggio
  *     
  *     Quindi andiamo a forzare il CPCCCHK della tabella principale se la
  *     tabella secondaria � modificata.
  *     
  *     Con il TEMP_TSM creato dalla CREATETSMTEMP aggiorno i record della super entit� con una frase del tipo
  *     
  *     Update DOC_MAST set CPCCCHK = cp_ToStrODBC(cp_NewCCChk()) from 
  *     DOC_MAST inner join TEMP_TSM on 
  *     DOC_MAST.MVSERIAL = TEMP_TSM.MVSERIAL 
  *     Group by TEMP_TSM.MVSERIAL
  * --- Parametri
  *     w_ARCHNAME= nome archivio collegato
  *     w_ARC_PADRE = Nome tabella principale
  *     w_RETNUM= Variabile passata per riferimento, conterr� i record per i quali sar� aggiornato il CPCCCHK sulla tabella principale
  *     pLog, pVerbose = Usuali parametri per mostrare a vide l'attivit� della funzione
  =cp_ReadXdc()
  m.w_TARCHIVIO = Alltrim( m.w_ARC_PADRE )
  m.w_ARCHNAME = Alltrim(m.w_ARCHNAME)
  m.w_IDXTABLE_PADRE = cp_OpenTable( Alltrim(m.w_ARC_PADRE) ,.T.)
  m.w_PHNAME_PADRE = cp_SetAzi(i_TableProp[ m.w_IDXTABLE_PADRE ,2]) 
  m.w_IDXTABLE2 = cp_OpenTable( "TEMP_TSM" ,.T.)
  m.w_TEMPNAME = i_TableProp[ m.w_IDXTABLE2 ,2] 
  m.w_NCONN = i_TableProp[ m.w_IDXTABLE2 , 3 ] 
  m.w_RESULT = ""
  m.w_CURNAME = Sys(2015)
   
 Local TSM_CURSOR,TestMacro 
 TSM_CURSOR = m.w_CURNAME
  * --- Leggo il link tra i due archivi...
  * --- Read from TAB_SECO
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[TAB_SECO_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[TAB_SECO_idx,2],.t.,TAB_SECO_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "TS__LINK"+;
      " from "+i_cTable+" TAB_SECO where ";
          +"TSCODICE = "+cp_ToStrODBC(m.w_ARC_PADRE);
          +" and TSCODTAB = "+cp_ToStrODBC(m.w_ARCHNAME);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      TS__LINK;
      from (i_cTable) where;
          TSCODICE = m.w_ARC_PADRE;
          and TSCODTAB = m.w_ARCHNAME;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_LINK = NVL(cp_ToDate(_read_.TS__LINK),cp_NullValue(_read_.TS__LINK))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if Empty( m.w_LINK )
    m.w_RESULT = ah_msgformat("Non definita relazione in super entit� di %1 con %2. Impossibile proseguire.", m.w_TARCHIVIO , Alltrim( m.w_ARCHNAME ) )
  else
    * --- Gestisco il caso in cui il dizionario dati non � ancora stato caricato.
    =sqlexec( m.w_nConn , "Select Count(*) as TSM_Added from "+ m.w_TEMPNAME, m.w_CURNAME )
    TestMacro=Reccount(m.w_CURNAME) >0 And &TSM_CURSOR..TSM_Added>0
    if TestMacro
      if m.w_IDXTABLE2=0
        m.w_RESULT = ah_msgformat("Archvio %1 non esistente, impossibile aggiornare super entit�", "TEMP_TSM" )
      else
        m.w_DBTYPE = cp_GetDatabaseType( m.w_NCONN )
        if m.w_DBTYPE="Unknown"
          m.w_RESULT = ah_msgformat("Per archivio %1 non � possibile determinare il tipo di server , impossibile creare tabella registrazioni variate", m.w_TARCHIVIO )
        else
          * --- Nel temporaneo sul database ho i dati aggiornati come se avessi
          *     gia aggiornato il mirror...
          *     Il mirror � aggiornato alla fine, a pubblicazione avvenuta con successo...
          m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE2 ,2]) 
          * --- Devo leggere la chiave dall'analisi...
          m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) )
          * --- Nel caso in cui la chiave fosse multipla mi basta che il valore del primo campo
          *     sia null per capire che non il record della tabella principale (super entit�)
          *     non sia gi� stato considerato
          * --- Due indici, uno per l'elenco delle tabelle (w_IDXTABLE) ed uno per
          *     navigare all'interno dell'XDC (w_TABLE_IDX)
          m.w_TABLE_IDX = I_dcx.GetTableIdx( m.w_TARCHIVIO )
          m.w_CMDSQL = ""
          m.w_LINKTOPADRE = Alltrim( StrTran(m.w_LINK, Alltrim(m.w_ARCHNAME), Alltrim(m.w_TEMPNAME) ))
          m.w_cJOINCOND = m.w_cJOINCOND + " left outer join " + m.w_TempTablePadre + " ON " + m.w_LinkToPadre
          * --- Vado a forzare il CPCCCHK sulle tabelle principali se queste lo hanno
          *     uguale alla corrispondente tabella di mirror. In qusto modo modifico il CPCCCHK
          *     ai soli record che ne hanno effetivamente bisogno..
          * --- SqlServer
          *     
          *     Update DOC_MAST set CPCCCHK = cp_ToStrODBC(cp_NewCCChk()) from 
          *     DOC_MAST inner join TEMP_TSM on 
          *     DOC_MAST.MVSERIAL = TEMP_TSM.MVSERIAL 
          *     Inner join LR_DOC_MAST ON LR_DOC_MAST.MVSERIAL=DOC_MAST.MVSERIAL 
          *     AND LR_DOC_MAST.CPCCCHK=DOC_MAST.CPCCCHK
          *     
          *     Db2/Oracle
          *     Update DOC_MAST set CPCCCHK = cp_ToStrODBC(cp_NewCCChk())
          *     Where Exists( Select 1 from TEMP_TSM Where
          *     DOC_MAST.MVSERIAL = TEMP_TSM.MVSERIAL 
          *     Inner join LR_DOC_MAST ON LR_DOC_MAST.MVSERIAL=DOC_MAST.MVSERIAL 
          *     AND LR_DOC_MAST.CPCCCHK=DOC_MAST.CPCCCHK )
          * --- Nome Tabella Mirror
          m.w_MR_TABLE = GETMIRRORNAME( m.w_TARCHIVIO )
          if m.w_DBTYPE="SQLServer"
            m.w_CMDSQL = "Update "+ m.w_PHNAME_PADRE + " set CPCCCHK = " + cp_ToStrODBC(cp_NewCCChk()) + " From "
            m.w_CMDSQL = m.w_CMDSQL + m.w_PHNAME_PADRE + " " + m.w_TARCHIVIO +" inner join " + m.w_TEMPNAME +" on "
            * --- Se la registrazione ha nel mirror un cpccchk uguale a quanto presente
            *     nella tabella principale o il CPCCCHK � vuoto vado ad aggiornare il CPCCCHK
            *     altrimenti la registrazione, in virt� del mirror sar� cmq pubblicata.
            *     CPCCCHK vuoto sebbene anch'esso rende la registrazione pubblicabile, non
            *     la farebbe inviare a chi me l'ha inviata perch� il CPCCCHK coincederebeb con
            *     quello del pubblicatore, quindi in qeusto caso occorre modificare cmq il CPCCCHK
            m.w_LINKTOPADRE = m.w_LINKTOPADRE + " inner Join " + m.w_MR_TABLE +" on "
            m.w_LINKTOPADRE = m.w_LINKTOPADRE + CreaJoin( m.w_TARCHIVIO , m.w_TARCHIVIO, m.w_MR_TABLE ) 
            m.w_CMDSQL = m.w_CMDSQL + m.w_LINKTOPADRE
          else
            * --- Nella join che per DB2 e Oracle si trasforma in una Where, devo indicare invece
            *     dell'alias della tabella padre il vero nome fisico
            * --- Includo solo le registrazioni presenti sul mirror e non modificate...
            m.w_LINKTOPADRE = Alltrim( StrTran(m.w_LINKTOPADRE, Alltrim(m.w_ARC_PADRE), Alltrim(m.w_PHNAME_PADRE) ))
            m.w_CMDSQL = "Update "+ m.w_PHNAME_PADRE + " set CPCCCHK = " + cp_ToStrODBC(cp_NewCCChk())
            m.w_CMDSQL = m.w_CMDSQL + " Where Exists ( select 1 from " + m.w_TEMPNAME + " where " + m.w_LINKTOPADRE + " )"
            m.w_CMDSQL = m.w_CMDSQL + " And Exists ( select 1 from " + Alltrim(m.w_MR_TABLE) + " where " + CreaJoin( m.w_ARC_PADRE , m.w_PHNAME_PADRE, m.w_MR_TABLE ) + " )"
          endif
          m.w_BRES = cp_TrsSQL( m.w_NCONN , m.w_CMDSQL )
          if m.w_BRES<0
            m.w_RESULT = ah_msgformat("Impossibile aggiornare archivio %1%0Errore %2, frase:%0%3", m.w_TARCHIVIO , Message(), m.w_CMDSQL )
            if VarType( m.pLog ) ="O" And m.pVerbose
              AddMsgNL("Impossibile aggiornare archivio %1%0Errore %2, frase:%0%3", m.w_TARCHIVIO , Message(), m.w_CMDSQL ,, ,)
            endif
          else
            if VarType( m.pLog ) ="O" And m.pVerbose
              AddMsgNL("Aggiornato archivio %1%0Frase:%0%2", m.w_TARCHIVIO , m.w_CMDSQL ,, ,)
            endif
            m.w_RESULT = ""
          endif
          m.w_RETNUM = IIF(m.w_BRES<0, 0, m.w_BRES)
        endif
      endif
    endif
    if Used(m.w_CURNAME)
       
 Select (m.w_CURNAME) 
 Use
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function aggtsment_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='TEMP_TSM'
    i_cWorkTables[2]='TAB_SECO'
    return(cp_OpenFuncTables(2))
* --- END AGGTSMENT
* --- START AGGTSMMIR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: aggtsmmir                                                       *
*              Aggiorna mirror TSM                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-08                                                      *
* Last revis.: 2006-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func aggtsmmir
param w_ARCHNAME,w_UpdRec,w_InsRec,w_DelRec,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(250)
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_TEMPIDX
  m.w_TEMPIDX=0
  private w_TEMPNAME
  m.w_TEMPNAME=space(30)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_EL_CMDSQL
  m.w_EL_CMDSQL=space(254)
  private w_MR_TABLE
  m.w_MR_TABLE=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_DBTYPE
  m.w_DBTYPE=space(50)
  private w_NCONN
  m.w_NCONN=0
  private w_TABLE_IDX
  m.w_TABLE_IDX=0
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_cJOINCOND
  m.w_cJOINCOND=space(254)
  private w_LINKBTWTEMP
  m.w_LINKBTWTEMP=space(30)
  private w_LINKTOPADRE
  m.w_LINKTOPADRE=space(30)
  private w_LINKTOSON
  m.w_LINKTOSON=space(30)
  private w_PHNAME_PADRE
  m.w_PHNAME_PADRE=space(50)
  private w_TempTablePadre
  m.w_TempTablePadre=space(30)
  private w_IDXTABLE_PADRE
  m.w_IDXTABLE_PADRE=0
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_CHIAVE_PRIM
  m.w_CHIAVE_PRIM=space(30)
  private w_PRIMA
  m.w_PRIMA=.f.
  private w_IDXTABLE2
  m.w_IDXTABLE2=0
  private w_Where
  m.w_Where=space(254)
  private w_BRES
  m.w_BRES=space(0)
* --- WorkFile variables
  private TEMP_TSM_idx
  TEMP_TSM_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "aggtsmmir"
if vartype(__aggtsmmir_hook__)='O'
  __aggtsmmir_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'aggtsmmir('+Transform(w_ARCHNAME)+','+Transform(w_UpdRec)+','+Transform(w_InsRec)+','+Transform(w_DelRec)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggtsmmir')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if aggtsmmir_OpenTables()
  aggtsmmir_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'aggtsmmir('+Transform(w_ARCHNAME)+','+Transform(w_UpdRec)+','+Transform(w_InsRec)+','+Transform(w_DelRec)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggtsmmir')
Endif
*--- Activity log
if vartype(__aggtsmmir_hook__)='O'
  __aggtsmmir_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure aggtsmmir_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione che aggiorna il mirror della TSM utilizzando il TEMP_TSM creato dalla CREATETSMTEMP
  *     
  *     Aggiorno il TSM_MIRROR con una frase del tipo
  *     
  *     Insert into TSM_MIRROR select * from
  *     TEMP_TSM left outer join TSM_MIRROR on
  *     TEMP_TSM.MVSERIAL = TSM_MIRROR.MVSERIAL AND
  *     TEMP_TSM.MVNUMRIF = TSM_MIRROR.MVNUMRIF AND
  *     TEMP_TSM.CPROWNUM = TSM_MIRROR.CPROWNUM
  *     Where TSM_MIRROR.MVSERIAL is null
  *     
  *     Update TSM_MIRROR set CPCCCHK = TEMP_TSM.CPCCCHK from
  *     TSM_MIRROR inner join TEMP_TSM on
  *     TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND
  *     TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND
  *     TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM
  *     Where TEMP_TSM.CPCCCHK is not null
  *     
  *     e per i cancellati
  *     
  *     Delete from TSM_MIRROR from 
  *     TSM_MIRROR inner join TEMP_TSM on
  *     TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND
  *     TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND
  *     TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM
  *     where TEMP_TSM.CPCCCHK is null
  * --- Con la prima frase inserisce i record nel mirror dove non sono ancora presenti (MVSERIAL is null)
  *     Con la seconda modifico i record gi� presenti aggiornando il CPCCCHK
  *     Con la seconda frase quelli cancellati (CPCCCHK non presente nella tabella )
  *     La join con Se_Mirror (il mirror della super entit�) serve poich� con il filtro SE_Mirro.CPCCCHK is null
  *     prender� della TSM solo i record che non sono gi� stati valutati per modifica sulla super entit� e
  *     per le quali la TSM verr� gi� riportata come tabella collegata dell'Entit�
  *     
  *     
  *     Parametri
  *     w_Archname = Archivio TSM del quale deve essere aggiornato il mirror
  *     pLog e pVerbose = parametri usati per aggiornamento log
  *     w_UpdRec = variabile passata per riferimento che indica i record modificati nel mirror
  *     w_InsRec = variabile passata per riferimento che indica i record inseriti nel mirror
  *     w_DelRec = variabile passata per riferimento che indica i record cancellati dal mirror
  =cp_ReadXdc()
  * --- Recupero il nome del Mirror del TSM
  m.w_TARCHIVIO = GetTsmName(Alltrim( m.w_ARCHNAME ))
  m.w_IDXTABLE = cp_OpenTable( "TEMP_TSM" ,.T.)
  m.w_TEMPNAME = Alltrim(i_TableProp[ m.w_IDXTABLE ,2] )
  m.w_NCONN = i_TableProp[ m.w_IDXTABLE , 3 ] 
  m.w_RESULT = ""
  if m.w_IDXTABLE=0
    m.w_RESULT = ah_msgformat("Archvio %1 non esistente, impossibile aggiornare mirror", "TEMP_TSM" )
  else
    m.w_DBTYPE = cp_GetDatabaseType( m.w_NCONN )
    if m.w_DBTYPE="Unknown"
      m.w_RESULT = ah_msgformat("Per archivio %1 non � possibile determinare il tipo di server , impossibile creare tabella registrazioni variate", m.w_TARCHIVIO )
    else
      * --- Creo la condizione di join
      m.w_cJOINCOND = CREAJOIN(Alltrim(m.w_ARCHNAME), Alltrim(m.w_TARCHIVIO), Alltrim(m.w_TEMPNAME) )
      * --- Dalla condizione di join estrapolo la prima chiave
      m.w_CHIAVE_PRIM = Substr(m.w_cJoinCOND, At(".", m.w_cJoinCOND) + 1)
      m.w_CHIAVE_PRIM = Alltrim(Left(m.w_CHIAVE_PRIM, At("=", m.w_CHIAVE_PRIM) - 1) )
      m.w_CMDSQL = ""
      * --- Sql Server
      *     Update TSM_MIRROR set CPCCCHK = TEMP_TSM.CPCCCHK from
      *     TSM_MIRROR inner join TEMP_TSM on
      *     TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND
      *     TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND
      *     TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM
      *     Where TEMP_TSM.CPCCCHK is not null
      *     
      *     Db2/Oracle
      *     Update TSM_MIRROR set CPCCCHK = ( Select TEMP_TSM.CPCCCHK from TEMP_TSM Where TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND 
      *     TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND 
      *     TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM)
      *     Where TEMP_TSM.CPCCCHK is not null And Exists ( Select 1 From TEMP_TSM where 
      *     TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM)
      if m.w_DBTYPE="SQLServer"
        m.w_CMDSQL = m.w_TARCHIVIO + " inner join " + m.w_TEMPNAME + " ON " + m.w_cJOINCOND
        m.w_CMDSQL = "Update "+ m.w_TARCHIVIO + " set CPCCCHK =  " + m.w_TEMPNAME + ".CPCCCHK from " + m.w_CMDSQL
        m.w_CMDSQL = m.w_CMDSQL + " where "+ m.w_TEMPNAME +".CPCCCHK" + " is not null"
      else
        * --- Nella join che per DB2 e Oracle si trasforma in una Where, devo indicare invece
        *     dell'alias della tabella padre il vero nome fisico
        m.w_Where = m.w_cJOINCOND + " And " + m.w_TEMPNAME +".CPCCCHK" + " is not null "
        m.w_CMDSQL = "Update "+ m.w_TARCHIVIO + " set CPCCCHK = " 
        m.w_CMDSQL = m.w_CMDSQL + " ( Select  " + m.w_TEMPNAME + ".CPCCCHK from " + m.w_TEMPNAME + " Where " + m.w_Where + " )"
        m.w_CMDSQL = m.w_CMDSQL+ " Where Exists ( select 1 from " + m.w_TEMPNAME + " where " + m.w_Where + " )"
      endif
      aggtsmmir_Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorna record modificati
      m.w_UpdRec = IIF(m.w_BRES<0, 0, m.w_BRES)
      if Not m.w_BRES<0
        * --- Insert into TSM_MIRROR select * from
        *     TEMP_TSM left outer join TSM_MIRROR on
        *     TEMP_TSM.MVSERIAL = TSM_MIRROR.MVSERIAL AND
        *     TEMP_TSM.MVNUMRIF = TSM_MIRROR.MVNUMRIF AND
        *     TEMP_TSM.CPROWNUM = TSM_MIRROR.CPROWNUM
        *     Where TSM_MIRROR.MVSERIA is null
        m.w_CMDSQL = m.w_TEMPNAME + " left outer join " + m.w_TARCHIVIO + " ON " + m.w_cJOINCOND
        m.w_CMDSQL = "Insert into "+ m.w_TARCHIVIO + " select " + m.w_TEMPNAME+ ".* from " + m.w_CMDSQL
        m.w_CMDSQL = m.w_CMDSQL + " where "+ m.w_TARCHIVIO +"."+Alltrim(m.w_CHIAVE_PRIM)+ " is null"
        aggtsmmir_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Aggiorna record inseriti
        m.w_InsRec = IIF(m.w_BRES<0, 0, m.w_BRES)
      endif
      if Not m.w_BRES<0
        if m.w_DBTYPE="SQLServer"
          * --- Delete from TSM_MIRROR from 
          *     TSM_MIRROR inner join TEMP_TSM on
          *     TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND
          *     TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND
          *     TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM
          *     where TEMP_TSM.CPCCCHK is null
          m.w_CMDSQL = m.w_TARCHIVIO + " inner join " + m.w_TEMPNAME + " ON " + m.w_cJOINCOND
          m.w_CMDSQL = "Delete from "+ m.w_TARCHIVIO + " from " + m.w_CMDSQL
          m.w_CMDSQL = m.w_CMDSQL + " where "+ m.w_TEMPNAME +".CPCCCHK" + " is null"
        else
          * --- Delete from TSM_MIRROR Where Exists (Select 1 from TEMP_TSM Where 
          *     TSM_MIRROR.MVSERIAL = TEMP_TSM.MVSERIAL AND
          *     TSM_MIRROR.MVNUMRIF = TEMP_TSM.MVNUMRIF AND
          *     TSM_MIRROR.CPROWNUM = TEMP_TSM.CPROWNUM 
          *     And TEMP_TSM.CPCCCHK is null )
          m.w_Where = m.w_cJOINCOND + " And " + m.w_TEMPNAME +".CPCCCHK" + " is null "
          m.w_CMDSQL = "Delete from "+ m.w_TARCHIVIO + " Where Exists (Select 1 from " + m.w_TEMPNAME + " Where " + m.w_Where + " )"
        endif
        aggtsmmir_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Aggiorna record modificati
        m.w_DelRec = IIF(m.w_BRES<0, 0, m.w_BRES)
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


procedure aggtsmmir_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Esegue frase Sql
  m.w_BRES = cp_TrsSQL( m.w_NCONN , m.w_CMDSQL )
  if m.w_BRES<0
    m.w_RESULT = ah_msgformat("Impossibile aggiornare archivio %1%0Errore %2, frase:%0%3", m.w_TARCHIVIO , Message(), m.w_CMDSQL )
    if VarType( m.pLog ) ="O" And m.pVerbose
      AddMsgNL("Impossibile aggiornare archivio %1%0Errore %2, frase:%0%3", m.w_TARCHIVIO , Message(), m.w_CMDSQL ,, ,)
    endif
  else
    if VarType( m.pLog ) ="O" And m.pVerbose
      AddMsgNL("Aggiornato archivio %1%0Frase:%0%2", m.w_TARCHIVIO , m.w_CMDSQL ,, ,)
    endif
    m.w_RESULT = ""
  endif
endproc


  function aggtsmmir_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='TEMP_TSM'
    return(cp_OpenFuncTables(1))
* --- END AGGTSMMIR
* --- START APP_VERT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: app_vert                                                        *
*              Applica filtri verticali                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-05                                                      *
* Last revis.: 2006-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func app_vert
param pArr,pArrWrite

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_WINDX
  m.w_WINDX=0
  private w_LOOP
  m.w_LOOP=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "app_vert"
if vartype(__app_vert_hook__)='O'
  __app_vert_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'app_vert('+Transform(pArr)+','+Transform(pArrWrite)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'app_vert')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
app_vert_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'app_vert('+Transform(pArr)+','+Transform(pArrWrite)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'app_vert')
Endif
*--- Activity log
if vartype(__app_vert_hook__)='O'
  __app_vert_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure app_vert_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Riceve due array per riferimento, il primo contierr�
  *     l'elenco dei campi il valore corrispondente ed un
  *     flag di stato che pu� valere
  *     0 = Nessun filtro verticale
  *     1 = filtro verticale fisso
  *     2 = mantieni valore sul DB
  *     partendo dal primo array passa i valori al secondo 
  *     elimiando i dati da mantenere sul databasa.
  *     Funzione da utilizzare per aggiornare il dato
  Dimension pArrWrite[1,2]
  m.w_LOOP = 1
  m.w_WINDX = 1
  do while m.w_LOOP<= Alen( m.pArr ,1 )
    if pArr[ m.w_LOOP ,3 ] <>2
       
 Dimension pArrWrite[ m.w_WINDX , 2 ] 
 pArrWrite[ m.w_WINDX , 1 ]=pArr[ m.w_LOOP ,1 ] 
 pArrWrite[ m.w_WINDX , 2 ]=pArr[ m.w_LOOP ,2 ] 
      m.w_WINDX = m.w_WINDX + 1
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  i_retcode = 'stop'
  i_retval = .T.
  return
endproc


* --- END APP_VERT
* --- START ARCTODBF
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: arctodbf                                                        *
*              Da archivio a DBF                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-19                                                      *
* Last revis.: 2009-02-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func arctodbf
param w_ENTITA,w_ARCHIVIO,w_SEDE,w_CURNAME,pLog,pVerbose

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=space(254)
  private w_CONN
  m.w_CONN=0
  private w_FILTRO
  m.w_FILTRO=space(254)
  private w_DPFILTRO
  m.w_DPFILTRO=space(254)
  private w_CMDSQL
  m.w_CMDSQL=space(254)
  private w_CHIAVE
  m.w_CHIAVE=space(254)
  private w_LOOP
  m.w_LOOP=0
  private w_CAMPO
  m.w_CAMPO=space(50)
  private w_VALORE
  m.w_VALORE=space(254)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_TEMPNAME
  m.w_TEMPNAME=space(254)
  private w_CONDJOIN
  m.w_CONDJOIN=space(254)
  private w_LINK
  m.w_LINK=space(254)
  private w_PRINCI
  m.w_PRINCI=space(1)
  private w_BFIRST
  m.w_BFIRST=.f.
  private w_VERTRES
  m.w_VERTRES=0
  private w_ERRMESS
  m.w_ERRMESS=space(254)
  private w_BNORMALE
  m.w_BNORMALE=.f.
  private w_TEST
  m.w_TEST=0
  private w_ARCHPRIN
  m.w_ARCHPRIN=space(100)
  private w_PR_FILTRO
  m.w_PR_FILTRO=space(254)
  private w_PRINDPFILTRO
  m.w_PRINDPFILTRO=space(254)
  private w_VALEXP
  m.w_VALEXP=space(254)
* --- WorkFile variables
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private DETTPUBL_idx
  DETTPUBL_idx=0
  private PUBLVERT_idx
  PUBLVERT_idx=0
  private SUENDETT_idx
  SUENDETT_idx=0
  private CAMPIEST_idx
  CAMPIEST_idx=0
  private TAB_ESTE_idx
  TAB_ESTE_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "arctodbf"
if vartype(__arctodbf_hook__)='O'
  __arctodbf_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'arctodbf('+Transform(w_ENTITA)+','+Transform(w_ARCHIVIO)+','+Transform(w_SEDE)+','+Transform(w_CURNAME)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'arctodbf')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if arctodbf_OpenTables()
  arctodbf_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_TAB_ESTE')
  use in _Curs_TAB_ESTE
endif
if used('_Curs_CAMPIEST')
  use in _Curs_CAMPIEST
endif
if used('_Curs_TAB_ESTE')
  use in _Curs_TAB_ESTE
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'arctodbf('+Transform(w_ENTITA)+','+Transform(w_ARCHIVIO)+','+Transform(w_SEDE)+','+Transform(w_CURNAME)+','+Transform(pLog)+','+Transform(pVerbose)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'arctodbf')
Endif
*--- Activity log
if vartype(__arctodbf_hook__)='O'
  __arctodbf_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure arctodbf_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dati:
  *     -Entit�
  *     -Archivio
  *     -Sede
  *     -Nome cursore
  *     - Oggetto a cui inviare il log operazioni
  *     crea un cursore chiamato Nome Cursore con all'interno
  *     i dati da pubblicare per l'archivio Archivio all'interno 
  *     dell'entit� per la sede.
  *     Quindi legge il filtro sull'entit�, lo mette in AND con
  *     il filtro orizzontale presente sulla sede per l'archivio.
  *     Infine prende l'elenco dei campi presenti per l'archivio
  *     in analisi e, se presenti filtri verticali sulla sede, li
  *     va ad applicare.
  *     
  *     Ritorna una stringa vuota se tutto ok o altrimenti
  *     il messaggio di errore.
  m.w_ENTITA = Alltrim( m.w_ENTITA )
  m.w_ARCHIVIO = Alltrim( m.w_ARCHIVIO )
  m.w_SEDE = Alltrim( m.w_SEDE )
  m.w_CURNAME = Alltrim( m.w_CURNAME )
  * --- L'immancabile chiamata alla CP_READXDC...
  =cp_ReadXdc()
  m.w_TEMPNAME = upper(Alltrim( i_TableProp[ cp_OpenTable( "TEMP_PUBBL" ,.T.) , 2 ] ))
  m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_ARCHIVIO ,1 ) )
  m.w_CONN = i_TableProp[ cp_OpenTable( "TEMP_PUBBL" ,.T.) , 3 ] 
  * --- Leggo eventuale filtro impostato sull'entit� per l'archivio
  * --- Read from ENT_DETT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ENT_DETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ENFILTRO,EN__LINK,ENPRINCI"+;
      " from "+i_cTable+" ENT_DETT where ";
          +"ENCODICE = "+cp_ToStrODBC(m.w_ENTITA);
          +" and EN_TABLE = "+cp_ToStrODBC(m.w_ARCHIVIO);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ENFILTRO,EN__LINK,ENPRINCI;
      from (i_cTable) where;
          ENCODICE = m.w_ENTITA;
          and EN_TABLE = m.w_ARCHIVIO;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_FILTRO = NVL(cp_ToDate(_read_.ENFILTRO),cp_NullValue(_read_.ENFILTRO))
    m.w_LINK = NVL(cp_ToDate(_read_.EN__LINK),cp_NullValue(_read_.EN__LINK))
    m.w_PRINCI = NVL(cp_ToDate(_read_.ENPRINCI),cp_NullValue(_read_.ENPRINCI))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Leggo filtro orizzontale inserito nella Sede
  * --- Read from DETTPUBL
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[DETTPUBL_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[DETTPUBL_idx,2],.t.,DETTPUBL_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "DPFILTRO"+;
      " from "+i_cTable+" DETTPUBL where ";
          +"DP__SEDE = "+cp_ToStrODBC(m.w_SEDE);
          +" and DP__TIPO = "+cp_ToStrODBC("R");
          +" and DPCODENT = "+cp_ToStrODBC(m.w_ENTITA);
          +" and DPCODARC = "+cp_ToStrODBC(m.w_ARCHIVIO);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      DPFILTRO;
      from (i_cTable) where;
          DP__SEDE = m.w_SEDE;
          and DP__TIPO = "R";
          and DPCODENT = m.w_ENTITA;
          and DPCODARC = m.w_ARCHIVIO;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_DPFILTRO = NVL(cp_ToDate(_read_.DPFILTRO),cp_NullValue(_read_.DPFILTRO))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if Not Empty( m.w_DPFILTRO )
    * --- Converto il filtro orizzontale in base al database sul quale deve essere applicato 
    *     in fase di pubblicazione
    m.w_DPFILTRO = cp_SetSQLFunctions( m.w_DPFILTRO , CP_DBTYPE )
    * --- Se all'interno del filtro trovo un filtro su tabella estesa allora lo applico
    *     sempre sul temporaneo, vado quindi a modificare l'alias...
    * --- Select from TAB_ESTE
    i_nConn=i_TableProp[TAB_ESTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TAB_ESTE_idx,2],.t.,TAB_ESTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ESTABALI from "+i_cTable+" TAB_ESTE ";
          +" where ESCODTAB="+cp_ToStrODBC(m.w_ARCHIVIO)+"";
           ,"_Curs_TAB_ESTE")
    else
      select ESTABALI from (i_cTable);
       where ESCODTAB=m.w_ARCHIVIO;
        into cursor _Curs_TAB_ESTE
    endif
    if used('_Curs_TAB_ESTE')
      select _Curs_TAB_ESTE
      locate for 1=1
      do while not(eof())
      m.w_DPFILTRO = Repalias( m.w_DPFILTRO , Alltrim(_Curs_TAB_ESTE.ESTABALI), m.w_TEMPNAME )
        select _Curs_TAB_ESTE
        continue
      enddo
      use
    endif
  endif
  * --- Determino quindi il filtro totale mettendo i due filtri in AND
  m.w_FILTRO = "("+IIF( Not Empty( m.w_FILTRO ), Alltrim( m.w_FILTRO ), " 1=1 ") + ")"
  m.w_FILTRO = "( " + m.w_FILTRO + IIF( Not Empty( m.w_DPFILTRO ), " And ("+ Alltrim(m.w_DPFILTRO) + ")","")+" )"
  * --- Costruisco la frase da inviare al server per avere il cursore VFP contenenente i dati
  m.w_CMDSQL = "Select "
  m.w_LOOP = 1
  m.w_CONDJOIN = ""
  do while m.w_LOOP<= I_DCX.GetFieldsCount( m.w_ARCHIVIO )
    m.w_CAMPO = i_DCX.GetFieldName( m.w_ARCHIVIO , m.w_LOOP )
    * --- Il campo potrebbe avere un filtro verticale per la sede...
    m.w_VALORE = ""
    m.w_VERTRES = VERTFILT( m.w_SEDE , "R" , m.w_ENTITA , m.w_ARCHIVIO , m.w_CAMPO , @m.w_VALORE , @m.w_ERRMESS ) 
    if m.w_VERTRES=-1
      * --- L'applicazione del filtro verticale � andata in errore, segnalo ed esco
      i_retcode = 'stop'
      i_retval = m.w_ERRMESS
      return
    else
      if m.w_VERTRES=1
        * --- Ho trovato un filtro verticale lo vado ad applicare nella frase...
        m.w_VALORE = Cp_tostrodbc( m.w_VALORE )
        m.w_CAMPO = m.w_VALORE+" As " +m.w_CAMPO
        * --- L'alias � determinante per evitare errori di ambiguita, va sempre inserito!
        m.w_CMDSQL = m.w_CMDSQL + IIF( m.w_LOOP=1, "", "," ) + m.w_CAMPO
      else
        * --- Nessun filtro verticale...
        m.w_BNORMALE = .T.
        if m.w_PRINCI="S"
          * --- Se il campo � nella chiave o � all'interno dei campi definiti sulla Super entit�
          *     allora devo effettuare una Nvl tra il campo sul database ed il campo sul mirror
          *     per ottenere i valori dei campi cancellati. La sola chiave non mi � sufficiente
          *     devo gestire anche i campi nella super entit� (x filtrare anche i cancellati)
          if i_DCX.IsInKey( m.w_CAMPO , m.w_CHIAVE )
            m.w_BNORMALE = .F.
          else
            * --- Read from SUENDETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[SUENDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[SUENDETT_idx,2],.t.,SUENDETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPROWORD"+;
                " from "+i_cTable+" SUENDETT where ";
                    +"SUCODICE = "+cp_ToStrODBC(m.w_ARCHIVIO);
                    +" and SU_CAMPO = "+cp_ToStrODBC(m.w_CAMPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPROWORD;
                from (i_cTable) where;
                    SUCODICE = m.w_ARCHIVIO;
                    and SU_CAMPO = m.w_CAMPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              m.w_TEST = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se trovo un CPROWORD il campo da leggere potrebbe essere sul mirror...
            *     (se non esiste il codice sul database => dato cancellato)
            m.w_BNORMALE = m.w_TEST=0
          endif
        endif
        if m.w_BNORMALE
          * --- L'alias � determinante per evitare errori di ambiguita, va sempre inserito!
          m.w_CMDSQL = m.w_CMDSQL + IIF( m.w_LOOP=1, "", "," ) + m.w_ARCHIVIO +"."+m.w_CAMPO+" As "+ m.w_CAMPO
        else
          * --- Applico una funzione di NVL tra dato sul DB e sul mirror...
          m.w_CMDSQL = m.w_CMDSQL + IIF( m.w_LOOP=1, "", "," ) + cp_SetSQLFunctions("[NVL( "+m.w_ARCHIVIO+"."+m.w_CAMPO+" , "+ m.w_TEMPNAME +"."+m.w_CAMPO+" )] ", CP_DBTYPE )+" As "+m.w_CAMPO
        endif
      endif
    endif
    if m.w_PRINCI="S"
      if i_DCX.IsInKey( m.w_CAMPO , m.w_CHIAVE )
        * --- Se tabella Principale
        *     Creo la condizione di Join tra TEMP_PUBBL e tabella sul database.
        *     La condizione di Inner join � sulle chiavi.
        *     Scorro i campi e appena trovo una chiave aggiungo una condizione
        *     alla join
        m.w_CONDJOIN = m.w_CONDJOIN + iif( Empty( m.w_CONDJOIN) , "", " And ") + m.w_ARCHIVIO+"."+m.w_CAMPO+"=" + m.w_TEMPNAME+"."+ m.w_CAMPO
      endif
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  * --- Aggiunge i campi estensione, prelevati sempre dal temporaneo 
  *     con le differenze con il loro alias...
  * --- Select from CAMPIEST
  i_nConn=i_TableProp[CAMPIEST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CAMPIEST_idx,2],.t.,CAMPIEST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAMPIEST ";
        +" where CMCODTAB="+cp_ToStrODBC(m.w_ARCHIVIO)+"";
         ,"_Curs_CAMPIEST")
  else
    select * from (i_cTable);
     where CMCODTAB=m.w_ARCHIVIO;
      into cursor _Curs_CAMPIEST
  endif
  if used('_Curs_CAMPIEST')
    select _Curs_CAMPIEST
    locate for 1=1
    do while not(eof())
    m.w_CMDSQL = m.w_CMDSQL +"," + m.w_TEMPNAME +"."+ alltrim (_Curs_CAMPIEST.CM_ALIAS)
      select _Curs_CAMPIEST
      continue
    enddo
    use
  endif
  m.w_CMDSQL = m.w_CMDSQL + " , "+ m.w_ARCHIVIO + ".CPCCCHK "
  * --- Se tabella principale aggiungo anche l'Ancestor ed il versioning..
  if m.w_PRINCI="S"
    m.w_CMDSQL = m.w_CMDSQL + " , "+ m.w_TEMPNAME +".ANCESTOR As ANCESTOR " 
    m.w_CMDSQL = m.w_CMDSQL + " , "+ m.w_TEMPNAME +".AVERSION As AVERSION "
  endif
  m.w_IDXTABLE = cp_OpenTable( m.w_ARCHIVIO ,.T.)
  m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
  * --- Ho l'elenco dei campi passo a costruire la parte di From (importante Alias per la presenza di un eventuale filtro )
  *     Anche per la condizione di Join
  * --- ATTENZIONE: STRTRAN con parametri 1,-1,1 indica che devono essere sostituite tutte le occorrenze
  *     e che la sostituzione deve essere CASE INSENSITIVE
  if m.w_PRINCI<>"S"
    * --- Ricerco archivio principale
    * --- Leggo eventuale filtro a livello di entit� per l'archivio principale...
    * --- Read from ENT_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[ENT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "EN_TABLE,ENFILTRO"+;
        " from "+i_cTable+" ENT_DETT where ";
            +"ENCODICE = "+cp_ToStrODBC(m.w_ENTITA);
            +" and ENPRINCI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        EN_TABLE,ENFILTRO;
        from (i_cTable) where;
            ENCODICE = m.w_ENTITA;
            and ENPRINCI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_ARCHPRIN = NVL(cp_ToDate(_read_.EN_TABLE),cp_NullValue(_read_.EN_TABLE))
      m.w_PR_FILTRO = NVL(cp_ToDate(_read_.ENFILTRO),cp_NullValue(_read_.ENFILTRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    m.w_ARCHPRIN = upper(Alltrim(m.w_ARCHPRIN) )
    * --- Ricerco la tabella principale nella sede, e verifico se per questa
    *     esiste un filtro....
    * --- Read from DETTPUBL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[DETTPUBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[DETTPUBL_idx,2],.t.,DETTPUBL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DPFILTRO"+;
        " from "+i_cTable+" DETTPUBL where ";
            +"DP__SEDE = "+cp_ToStrODBC(m.w_SEDE);
            +" and DP__TIPO = "+cp_ToStrODBC("R");
            +" and DPCODENT = "+cp_ToStrODBC(m.w_ENTITA);
            +" and DPCODARC = "+cp_ToStrODBC(m.w_ARCHPRIN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DPFILTRO;
        from (i_cTable) where;
            DP__SEDE = m.w_SEDE;
            and DP__TIPO = "R";
            and DPCODENT = m.w_ENTITA;
            and DPCODARC = m.w_ARCHPRIN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_PRINDPFILTRO = NVL(cp_ToDate(_read_.DPFILTRO),cp_NullValue(_read_.DPFILTRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty( m.w_PRINDPFILTRO )
      m.w_PRINDPFILTRO = cp_SetSQLFunctions( m.w_PRINDPFILTRO , CP_DBTYPE )
      m.w_PRINDPFILTRO = Repalias( m.w_PRINDPFILTRO , m.w_ARCHPRIN, m.w_TEMPNAME )
      * --- La stessa operazione la ripeto per tutte le tabelle estese collegate alla
      *     tabella principale
      * --- Select from TAB_ESTE
      i_nConn=i_TableProp[TAB_ESTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TAB_ESTE_idx,2],.t.,TAB_ESTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESTABALI from "+i_cTable+" TAB_ESTE ";
            +" where ESCODTAB="+cp_ToStrODBC(m.w_ARCHPRIN)+"";
             ,"_Curs_TAB_ESTE")
      else
        select ESTABALI from (i_cTable);
         where ESCODTAB=m.w_ARCHPRIN;
          into cursor _Curs_TAB_ESTE
      endif
      if used('_Curs_TAB_ESTE')
        select _Curs_TAB_ESTE
        locate for 1=1
        do while not(eof())
        m.w_PRINDPFILTRO = Repalias( m.w_PRINDPFILTRO , _Curs_TAB_ESTE.ESTABALI, m.w_TEMPNAME )
          select _Curs_TAB_ESTE
          continue
        enddo
        use
      endif
    endif
    * --- Sostituisco il nome dell'archivio principale con il temporaneo con gli
    *     elementi variati.....
    m.w_CONDJOIN = Repalias( m.w_LINK , m.w_ARCHPRIN, m.w_TEMPNAME )
    m.w_CMDSQL = m.w_CMDSQL +" FROM (" + m.w_TEMPNAME + " Inner Join " +m.w_PHNAME +" " +m.w_ARCHIVIO +" ON " + m.w_CONDJOIN +" ) "
  else
    m.w_CMDSQL = m.w_CMDSQL +" FROM (" + m.w_TEMPNAME + " Left Outer Join " +m.w_PHNAME +" " +m.w_ARCHIVIO +" ON " + m.w_CONDJOIN +" )"
    m.w_ARCHPRIN = upper(Alltrim( m.w_ARCHIVIO ) )
  endif
  * --- Eventuale filtro...
  m.w_CMDSQL = m.w_CMDSQL +" WHERE 1=1 "
  if m.w_PRINCI<>"S"
    * --- Il filtro sulle tabelle collegate legato o al filtro sull'entit�, o al filtro
    *     sulle tabelle estese collegate alla tabella princiaple si applica
    *     sempre sui dati del temporaneo....
    if Not Empty( m.w_PR_FILTRO )
      * --- Applico il filtro presente nella tabella principale a livello di entit�
      m.w_CMDSQL = m.w_CMDSQL +" AND ( " + Repalias( m.w_PR_FILTRO , m.w_ARCHPRIN, m.w_TEMPNAME )+" )"
    endif
    if Not Empty( m.w_PRINDPFILTRO )
      m.w_CMDSQL = m.w_CMDSQL +" AND ("+m.w_PRINDPFILTRO+" ) "
    endif
  endif
  if Not Empty( m.w_FILTRO ) 
    * --- Infine il filtro impostato direttamente sull'archivio
    m.w_CMDSQL = m.w_CMDSQL +" AND (( " + m.w_FILTRO+" )"
    * --- Gestisco i record cancellati, in questo caso vado ad applicare il filtro ma sulla
    *     tabella temporanea che contiene le modifiche. Quindi per quei record che
    *     hanno cpccchk null sull'archivio e soddisfano il filtro sul Mirror.
    *     (i campi nei filtri devono essere dichiarati sulla super entit� e quindi sono
    *     presenti sul mirror)
    m.w_CMDSQL = m.w_CMDSQL +" OR ( "+m.w_ARCHIVIO+".CPCCCHK Is Null And " + Repalias( m.w_FILTRO , m.w_ARCHIVIO, m.w_TEMPNAME )+" ))"
  endif
  m.w_VALEXP = VALIDEXP( m.w_ARCHPRIN )
  m.w_VALEXP = Repalias( m.w_VALEXP , m.w_ARCHPRIN, m.w_TEMPNAME ) 
  * --- Escludo eventuali record che hanno come sede di pubblicazione
  *     la sede al quale dovrei inviarli e CPCCCHK ( CPCCCHK_PU ) 
  *     di pubblicazione uguale al CPCCCHK ( PUCPCCCHK ) nel TEMPPUBBL
  *     e per i quali non sono validatore. Se sono validatore invio cmq il dato
  *     per aggiornare la versione...
  *     
  *     (<condizione di validazione> or  Not( Nvl( TEMPPUBL.PUBCOSED,'' ) = w_SEDE And 
  *     TEMPPUBL.CPCCCHK_PU=TEMPPUBL.PUCPCCCHK) )
  m.w_CMDSQL = m.w_CMDSQL+" And ( "+ cp_SetSQLFunctions(m.w_VALEXP, CP_DBTYPE) +" Or "
  m.w_CMDSQL = m.w_CMDSQL +" Not(" +cp_SetSQLFunctions("[NVL( "+ m.w_TEMPNAME +".PUBCOSED , '  ' )] ", CP_DBTYPE )+ "="+Cp_ToStrOdbc(m.w_SEDE)+" And "
  m.w_CMDSQL = m.w_CMDSQL + cp_SetSQLFunctions( "[NVL( "+m.w_TEMPNAME+".CPCCCHK_PU,[SPACES(10)])]", CP_DBTYPE) + "="
  m.w_CMDSQL = m.w_CMDSQL + cp_SetSQLFunctions( "[NVL( "+m.w_TEMPNAME+".PUCPCCCHK,[SPACES(10)])]", CP_DBTYPE)+") )"
  * --- Invio al server la frase...
  *     Utilizzo la connessione relativa al temporaneo
  if cp_SQL( m.w_CONN , m.w_CMDSQL , m.w_CURNAME ,.f. , .t. ) = -1
    m.w_RESULT = AH_MsgFormat( "Comando %1 ha generato errore %2" , m.w_CMDSQL, Message() )
  else
    if VarType( m.pLog )="O"
      if m.pVerbose
        AddMsgNL("Entit� %1 tabella %2, frase %3 ", m.pLog , m.w_ENTITA , m.w_ARCHIVIO , m.w_CMDSQL ,, ,)
      endif
      AddMsgNL("Per entita %1 tabella %2 pubblicate %3 registrazioni",m.pLog, m.w_ENTITA , m.w_ARCHIVIO , alltrim(str( RecCount( m.w_CURNAME ) )) , ,, )
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function arctodbf_OpenTables()
    dimension i_cWorkTables[max(1,6)]
    i_cWorkTables[1]='ENT_DETT'
    i_cWorkTables[2]='DETTPUBL'
    i_cWorkTables[3]='PUBLVERT'
    i_cWorkTables[4]='SUENDETT'
    i_cWorkTables[5]='CAMPIEST'
    i_cWorkTables[6]='TAB_ESTE'
    return(cp_OpenFuncTables(6))
* --- END ARCTODBF
* --- START CALPATHLOG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calpathlog                                                      *
*              Trova/crea cartella di log                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-17                                                      *
* Last revis.: 2006-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func calpathlog
param w_SEDE,w_ENTITA,w_CHIAVE,w_TIPO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CANALE
  m.w_CANALE=space(1)
  private w_PATH
  m.w_PATH=space(254)
  private w_PATERR
  m.w_PATERR=space(254)
  private w_DESTINATARIO
  m.w_DESTINATARIO=space(2)
  private w_PROG
  m.w_PROG=0
* --- WorkFile variables
  private PARALORE_idx
  PARALORE_idx=0
  private PUBLMAST_idx
  PUBLMAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "calpathlog"
if vartype(__calpathlog_hook__)='O'
  __calpathlog_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'calpathlog('+Transform(w_SEDE)+','+Transform(w_ENTITA)+','+Transform(w_CHIAVE)+','+Transform(w_TIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calpathlog')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if calpathlog_OpenTables()
  calpathlog_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'calpathlog('+Transform(w_SEDE)+','+Transform(w_ENTITA)+','+Transform(w_CHIAVE)+','+Transform(w_TIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calpathlog')
Endif
*--- Activity log
if vartype(__calpathlog_hook__)='O'
  __calpathlog_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure calpathlog_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Data la sede nella quale si sta sincronizzando, ritorna il path di memorizzazione dei
  *     dati errati o salvataggio dati preesistenti (per sovrascrittura)
  * --- w_TIPO: 'S' salvataggio dati preesistenti per sovrascrittura
  *     'E' memorizzazione record con conflitti da DBF
  * --- Leggo la sede che Importa...
  * --- Read from PARALORE
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PARALORE_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PARALORE_idx,2],.t.,PARALORE_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PL__SEDE,PLPATERR"+;
      " from "+i_cTable+" PARALORE where ";
          +"PLCODAZI = "+cp_ToStrODBC(i_CODAZI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PL__SEDE,PLPATERR;
      from (i_cTable) where;
          PLCODAZI = i_CODAZI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_DESTINATARIO = NVL(cp_ToDate(_read_.PL__SEDE),cp_NullValue(_read_.PL__SEDE))
    m.w_PATERR = NVL(cp_ToDate(_read_.PLPATERR),cp_NullValue(_read_.PLPATERR))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Se � stata specificata nei parametri la cartella per Log...
  if Not Empty(m.w_PATERR) And Directory( m.w_PATERR )
    m.w_DESTINATARIO = Trim( m.w_DESTINATARIO )
    * --- Read from PUBLMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[PUBLMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[PUBLMAST_idx,2],.t.,PUBLMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SEULTPRO"+;
        " from "+i_cTable+" PUBLMAST where ";
            +"SECODICE = "+cp_ToStrODBC(m.w_SEDE);
            +" and SE__TIPO = "+cp_ToStrODBC("P");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SEULTPRO;
        from (i_cTable) where;
            SECODICE = m.w_SEDE;
            and SE__TIPO = "P";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_PROG = NVL(cp_ToDate(_read_.SEULTPRO),cp_NullValue(_read_.SEULTPRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo la path nella temp per generare i DBF. Alla fine questa cartella verr� eliminata
    *     e verr� creato loo zip dei DBF nella cartella indicata nei parametri
    m.w_PATERR = TEMPADHOC() + "\Remote\Log\"
    * --- Tolgo gli apici dalla chiave
    m.w_CHIAVE = Alltrim(StrTran(m.w_CHIAVE,"'",""))+".DBF"
    * --- Se non � stata specificata la cartella di memorizzazione log non lo creo
    if m.w_TIPO="S"
      * --- Se esiste creo una cartella del tipo
      *     C:\CARTELLA\SedePubblicazione\SedeDestinazione\OldData\Entita\NumeroPacchetto\ChiaveArchivioPrincipale.Dbf
      m.w_PATH = m.w_PATERR+ Alltrim( m.w_SEDE ) +"\"+ m.w_DESTINATARIO +"\OldData\"+Alltrim( Str( m.w_PROG+1 ) )+"\"+Alltrim(m.w_ENTITA) +"\"
    else
      * --- Se esiste creo una cartella del tipo
      *     C:\CARTELLA\SedePubblicazione\SedeDestinazione\OldData\Entita\NumeroPacchetto\ChiaveArchivioPrincipale.Dbf
      m.w_PATH = m.w_PATERR+ Alltrim( m.w_SEDE ) +"\"+ m.w_DESTINATARIO +"\ErrorData\"+Alltrim( Str( m.w_PROG+1 ) )+"\"+Alltrim(m.w_ENTITA) +"\"
    endif
    if Not Directory( m.w_PATH) 
      * --- Se la cartella non esiste ancora la creo
      MakeDir (m.w_Path)
    endif
    i_retcode = 'stop'
    i_retval = Alltrim(m.w_PATH)+m.w_CHIAVE
    return
  else
    i_retcode = 'stop'
    i_retval = ""
    return
  endif
endproc


  function calpathlog_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='PARALORE'
    i_cWorkTables[2]='PUBLMAST'
    return(cp_OpenFuncTables(2))
* --- END CALPATHLOG
* --- START CHECKEXP
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: checkexp                                                        *
*              Check filter expr                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-24                                                      *
* Last revis.: 2009-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func checkexp
param w_ARCHIVIO,w_EXP,w_BMSG,bIncludeExt

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=.f.
  private w_LOOP
  m.w_LOOP=0
  private w_STATO
  m.w_STATO=0
  private w_INDEX
  m.w_INDEX=0
  private w_TOKEN
  m.w_TOKEN=space(254)
  private w_CHAR
  m.w_CHAR=space(1)
  private w_TESTFIELDS
  m.w_TESTFIELDS=space(254)
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_ULT_SEP
  m.w_ULT_SEP=space(1)
  private w_FUNZPAINTER
  m.w_FUNZPAINTER=.f.
  private w_TEXP
  m.w_TEXP=space(254)
  private w_AP_INDEX
  m.w_AP_INDEX=0
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private CAMPIEST_idx
  CAMPIEST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "checkexp"
if vartype(__checkexp_hook__)='O'
  __checkexp_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'checkexp('+Transform(w_ARCHIVIO)+','+Transform(w_EXP)+','+Transform(w_BMSG)+','+Transform(bIncludeExt)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'checkexp')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if checkexp_OpenTables()
  checkexp_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_SUENDETT')
  use in _Curs_SUENDETT
endif
if used('_Curs_CAMPIEST')
  use in _Curs_CAMPIEST
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'checkexp('+Transform(w_ARCHIVIO)+','+Transform(w_EXP)+','+Transform(w_BMSG)+','+Transform(bIncludeExt)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'checkexp')
Endif
*--- Activity log
if vartype(__checkexp_hook__)='O'
  __checkexp_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure checkexp_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Verifica se l'espressione passata contiene solo campi definiti o
  *     nella chiave dell'archivio oppure nell'eventuale super entit� definita
  *     per esso. Funzione per verifica che l'utente non possa definire filtri
  *     non gestibili con la struttura definita.
  *     w_BMSG se .T. restituisce il messaggio di verifica
  *     bIncludeExt se .t. include nel controllo, oltre all'archivio, anche le sue tabelle estese ed i reltivi campi (come alias), di default tali informazioni sono sono incluse
  m.w_FUNZPAINTER = .F.
  m.w_ULT_SEP = " "
  m.w_RESULT = .T.
  if not Empty( m.w_EXP )
    m.w_TARCHIVIO = Alltrim( m.w_ARCHIVIO )
    * --- Definizione dell'array con i separatori..
     
 Dimension Asep[26] 
 Asep[1]=" " 
 Asep[2]="." 
 Asep[3]="(" 
 Asep[4]=")" 
 Asep[5]="+" 
 Asep[6]="-" 
 Asep[7]="/" 
 Asep[8]="*" 
 Asep[9]="," 
 Asep[10]="[" 
 Asep[11]="]" 
 Asep[12]="0" 
 Asep[13]="1" 
 Asep[14]="2" 
 Asep[15]="3" 
 Asep[16]="4" 
 Asep[17]="5" 
 Asep[18]="6" 
 Asep[19]="7" 
 Asep[20]="8" 
 Asep[21]="9" 
 Asep[22]="{" 
 Asep[23]="}" 
 Asep[24]="=" 
 Asep[25]=">" 
 Asep[26]="<"
    * --- Definizione array separatori di valori (stringhe)
     
 Dimension Aapici[2] 
 Aapici[1]='"' 
 Aapici[2]="'"
    * --- Definzione dell'Array delle parole da non considerare token (operatori)
     
 Dimension AOper[8] 
 Aoper[1]="AND" 
 Aoper[2]="OR" 
 Aoper[3]="NOT" 
 Aoper[4]="LEFT" 
 Aoper[5]="RIGHT" 
 Aoper[6]="LIKE" 
 Aoper[7]="IS" 
 Aoper[8]="NULL" 
 
    * --- Determino l'elenco dei campi da verificare separati da virgole,
    *     utilizzer� l'operatore $ contando sul fatto che la virgola � vistra come
    *     operatore non dovrei aver problemi di ambiguit�
    =cp_ReadXdc()
    m.w_TESTFIELDS = Upper( cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_TARCHIVIO ,1 ) ) )
    * --- Tra i Token validi metto il nome dell'archivio...
    m.w_TESTFIELDS = m.w_TESTFIELDS + " , " + m.w_TARCHIVIO
    * --- Aggiungo eventuali campi presenti nella definizione della Super Entit�
    * --- Select from SUENDETT
    i_nConn=i_TableProp[SUENDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[SUENDETT_idx,2],.t.,SUENDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SU_CAMPO from "+i_cTable+" SUENDETT ";
          +" where SUCODICE="+cp_ToStrODBC(m.w_ARCHIVIO)+"";
           ,"_Curs_SUENDETT")
    else
      select SU_CAMPO from (i_cTable);
       where SUCODICE=m.w_ARCHIVIO;
        into cursor _Curs_SUENDETT
    endif
    if used('_Curs_SUENDETT')
      select _Curs_SUENDETT
      locate for 1=1
      do while not(eof())
      m.w_TESTFIELDS = m.w_TESTFIELDS + " , " + Upper( Alltrim( _Curs_SUENDETT.SU_CAMPO ) )
        select _Curs_SUENDETT
        continue
      enddo
      use
    endif
    * --- Aggiungo anche i campi (alias) nelle tabelle estese e gli alias delle tabelle estese
    *     se il tipo di controllo lo prevede
    if m.bIncludeExt
      * --- Select from CAMPIEST
      i_nConn=i_TableProp[CAMPIEST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[CAMPIEST_idx,2],.t.,CAMPIEST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CM_ALIAS,CMTABALI from "+i_cTable+" CAMPIEST ";
            +" where CMCODTAB="+cp_ToStrODBC(m.w_ARCHIVIO)+"";
             ,"_Curs_CAMPIEST")
      else
        select CM_ALIAS,CMTABALI from (i_cTable);
         where CMCODTAB=m.w_ARCHIVIO;
          into cursor _Curs_CAMPIEST
      endif
      if used('_Curs_CAMPIEST')
        select _Curs_CAMPIEST
        locate for 1=1
        do while not(eof())
        m.w_TESTFIELDS = m.w_TESTFIELDS + " , " + Upper( Alltrim( _Curs_CAMPIEST.CM_ALIAS ) )
        m.w_TESTFIELDS = m.w_TESTFIELDS + " , " + Upper( Alltrim( _Curs_CAMPIEST.CMTABALI ) )
          select _Curs_CAMPIEST
          continue
        enddo
        use
      endif
    endif
    m.w_TEXP = UPPER( Alltrim( m.w_EXP ) )+Asep[22]
    * --- Sostituisco eventuali stringhe con il primo separatore...
    m.w_LOOP = 1
    do while m.w_LOOP<= ALen ( AApici , 1 )
      if Mod(Occurs( AApici[ m.w_LOOP ], m.w_TEXP ) , 2 ) <>0
        ah_ErrorMsg("Espressione %1 non corretta. Apici (%2) non impostati correttamente",,"", Alltrim(m.w_TEXP) , AApici[ m.w_LOOP ] )
        i_retcode = 'stop'
        i_retval = .F.
        return
      endif
      * --- Ricerco l'apice...
      m.w_AP_INDEX = ATC( AApici[ m.w_LOOP ] , m.w_TEXP) 
      do while m.w_AP_INDEX>0
        m.w_TEXP = STUFF( m.w_TEXP , m.w_AP_INDEX , ATC( AApici[ m.w_LOOP ] , m.w_TEXP,2) - m.w_AP_INDEX +1 , Asep[1] )
        m.w_AP_INDEX = ATC( AApici[ m.w_LOOP ] , m.w_TEXP) 
      enddo
      m.w_LOOP = m.w_LOOP + 1
    enddo
    * --- w_STATO:
    *     0 = Nothing
    *     1 = incontrato un separatore (passo al 2 solo se trovo un carattere non separatore)
    m.w_STATO = 0
    m.w_LOOP = 1
    * --- Analizzo la stringa andando a cercare un token di tipo campo...
    do while m.w_LOOP<= Len ( m.w_TEXP )
      * --- Analizzo la stringa...
      m.w_CHAR = Substr( m.w_TEXP , m.w_LOOP ,1 )
      if m.w_STATO=0
        * --- Finch� ho separatori non mi muovo da questo stato...
        if ASCAN(ASEP, m.w_CHAR ,-1,-1,0,6)=0
          * --- Il carattere non � un separatore...
          m.w_INDEX = m.w_LOOP
          m.w_STATO = 1
        endif
        * --- Memorizza l ultimo separatore (serve per consentire l utilizzo di funzioni painter come [GLOBALVAR(...)] all interno delle espressioni
        if m.w_STATO=0
          m.w_ULT_SEP = m.w_CHAR
        endif
      else
        if m.w_STATO=1
          * --- Finch� non trovo un separatore definisco un token...  (le funzioni painter racchiuse tra [..] vengono accettate senza analisi)
          m.w_FUNZPAINTER = m.w_ULT_SEP="[" AND m.w_CHAR= "]" 
          * --- Il separatore Asep[22] non � uasto all interno delle funzioni painter. Tale separatore viene aggiunto alla fine dell espressione per indicarne la fine.
          *      Nel caso di utilizzo di funzioni painter bisogna evitare di accettare espressioni che aprono la quadra [ ma non la chiudono ]
          if ASCAN(ASEP, m.w_CHAR ,-1,-1,0,6)<>0 AND (m.w_ULT_SEP<>"[" OR m.w_CHAR=Asep[22])
            * --- Il carattere � un separatore., ho il Token (da w_INDEX a w_LOOP -1)
            m.w_TOKEN = Substr( m.w_TEXP , m.w_INDEX , m.w_LOOP - m.w_INDEX )
            * --- Verifico se il token non � tra gli operatori...
            if ASCAN(Aoper, upper(m.w_TOKEN) , 1 )= 0
              * --- E' un Token, occorre verificare se presente nella chiave...
              if Not m.w_TOKEN $ m.w_TESTFIELDS 
                * --- Non � presente tra i campi non posso accettare il filtro...
                if m.w_BMSG
                  ah_ErrorMsg("Espressione %1 non corretta. Il campo %2 non pu� essere utilizzato",,"", Alltrim(m.w_EXP) , m.w_TOKEN )
                endif
                i_retcode = 'stop'
                i_retval = .F.
                return
              endif
            endif
            * --- Token accettabile o operatore, cmq si riparte...
            m.w_STATO = 0
            m.w_INDEX = 0
          endif
          if m.w_FUNZPAINTER
            m.w_STATO = 0
            m.w_INDEX = 0
          endif
        endif
      endif
      m.w_LOOP = m.w_LOOP + 1
    enddo
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function checkexp_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='SUENDETT'
    i_cWorkTables[2]='CAMPIEST'
    return(cp_OpenFuncTables(2))
* --- END CHECKEXP
* --- START CHECKORD
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: checkord                                                        *
*              Check expr ordine su entit�                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-14                                                      *
* Last revis.: 2006-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func checkord
param pTabPrinc,pExpr

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_TARCHIVIO
  m.w_TARCHIVIO=space(30)
  private w_Mirror
  m.w_Mirror=space(0)
  private w_Res
  m.w_Res=0
  private w_OK
  m.w_OK=.f.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "checkord"
if vartype(__checkord_hook__)='O'
  __checkord_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'checkord('+Transform(pTabPrinc)+','+Transform(pExpr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'checkord')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
checkord_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'checkord('+Transform(pTabPrinc)+','+Transform(pExpr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'checkord')
Endif
*--- Activity log
if vartype(__checkord_hook__)='O'
  __checkord_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure checkord_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Controllo la espressione di ordinamento sulla Entit�.
  *     I campi utilizzati nella espressione devono essere presenti nella super entit� relativa
  *     poich� altrimenti non vengono valorizzati nel mirror e non vengono valorizzati nel pacchetto nel caso
  *     di record cancellato, causando l'impossibilit� di eseguire l'ordinamento corretto in fase di sincronizzazione
  *     
  *     Per eseguire il controllo simulo una frase Sql con l'espressione sulla tabella di mirror relativa alla tabella principale della entit�
  m.w_OK = .T.
  m.w_TARCHIVIO = Alltrim( m.pTabPrinc )
  m.w_Mirror = GETMIRRORNAME( m.w_TARCHIVIO ) 
  m.w_Res = SqlExec(1 , "Select * from " + Alltrim(m.w_Mirror), "CONTROLLO")
  m.pExpr = Upper(Alltrim(m.pExpr))
  m.pExpr = StrTran(m.pExpr, " DESC", "")
  m.pExpr = StrTran(m.pExpr, " ASC", "")
  if m.w_Res<0
    ah_ErrorMsg("Tabella di mirror non ancora generata. Impossibile eseguire il controllo","Stop")
  else
     
 Local L_OldError 
 L_OldError = On("Error") 
 On Error m.w_OK=.F.
     
 Local Macro 
 Macro=m.pExpr 
 Select &Macro As CHKORD From CONTROLLO Into Cursor TEST 
 
    if Not m.w_OK
      ah_ErrorMsg("Espressione non valida. Controllare che tutti i campi utilizzati siano definiti nella super entit� e che la tabella di mirror sia stata rigenerata correttamente","Stop")
    else
      ah_ErrorMsg("Verifica espressione completata con esito positivo","i")
    endif
     
 On Error &L_OldError
  endif
  if Used("CONTROLLO")
     
 Select CONTROLLO 
 Use
  endif
  if Used("TEST")
     
 Select TEST 
 Use
  endif
  i_retcode = 'stop'
  i_retval = .T.
  return
endproc


* --- END CHECKORD
* --- START CHECKREL
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: checkrel                                                        *
*              Analizza il contenuto di una relazione tra tabelle collegate e pricipale*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-24                                                      *
* Last revis.: 2008-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func checkrel
param pRELAZIONE,pTAB1,pCAMPO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CONTA
  m.w_CONTA=0
  private w_RESULT
  m.w_RESULT=space(30)
  private w_PAROLA
  m.w_PAROLA=space(30)
  private w_LEFTPAROLA
  m.w_LEFTPAROLA=space(30)
  private w_RIGHTPAROLA
  m.w_RIGHTPAROLA=space(30)
  private w_POS
  m.w_POS=0
  private w_SEPARATORI
  m.w_SEPARATORI=space(20)
* --- WorkFile variables
  private SUENDETT_idx
  SUENDETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "checkrel"
if vartype(__checkrel_hook__)='O'
  __checkrel_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'checkrel('+Transform(pRELAZIONE)+','+Transform(pTAB1)+','+Transform(pCAMPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'checkrel')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if checkrel_OpenTables()
  checkrel_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'checkrel('+Transform(pRELAZIONE)+','+Transform(pTAB1)+','+Transform(pCAMPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'checkrel')
Endif
*--- Activity log
if vartype(__checkrel_hook__)='O'
  __checkrel_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure checkrel_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- RIceve 2 parametri
  *     pRELAZIONE : Stringa contenente la relazione tra tabella principale e collegatte
  *     pTAB1 : Tabella co�involta nella relazione
  *     pCAMPO : Campo della tabella coinvolta nella relazione
  *     
  *     Resitituisce .T. se pTAB1.CAMPO � usato correttamente nella relazione ( nel formato  pTAB1.pCAMPO=pTAB2.pCAMPO2  ) oppure se non � usatop nella relazione
  *     
  *     La funzione verifica che pTAB1.pCAMPO sia usata nella relazione nel formato  pTAB1.pCAMPO=pTAB2.pCAMPO2  senza effettuarvi calcoli sopra  (es :  pTAB1.pCAMPO + 4 =pTAB2.pCAMPO2 oppure pTAB1.pCAMPO + 4 <>pTAB2.pCAMPO2  searebbero espressioni non corrette)  
  *     La funzione si avvale di  getfieldrel() per estrarre la le parti che compongono la relazione 
  m.w_SEPARATORI = " "+")"+","+"["+"+"+"-" + "/" + "*" + "=" + "<" + ">" + "]"+"(" 
  * --- Estrae la prima partedell espressione
  m.w_PAROLA = getfieldrel (m.pRELAZIONE , m.pTAB1, m.pCAMPO,.T.)
  if EMPTY (m.w_PAROLA)
    i_retcode = 'stop'
    i_retval = .T.
    return
  endif
  m.w_CONTA = AT( "." , m.w_PAROLA )
  checkrel_Pag2()
  if i_retcode='stop' or !empty(i_Error)
    return
  endif
  * --- Controlla la seconda parte dell' espressione
  m.w_PAROLA = getfieldrel (m.pRELAZIONE , m.w_LEFTPAROLA , m.w_RIGHTPAROLA,.T.)
  if EMPTY (m.w_PAROLA)
    i_retcode = 'stop'
    i_retval = .T.
    return
  endif
  m.w_CONTA = AT( "." , m.w_PAROLA )
  checkrel_Pag2()
  if i_retcode='stop' or !empty(i_Error)
    return
  endif
  i_retcode = 'stop'
  i_retval = .T.
  return
endproc


procedure checkrel_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- w_PAROLA deve essere nel fomrato TAB1.CAMPO quindi non deve conttenere spazi e deve contenere solo caratteri alfanumerici tranne il simbolo di punteggiatura tra TAB1 e CAMPO
  * --- Il punto di divisione tra TAB1 e CAMPO deve esserci e non deve essere alla fine o all inizio dela stringa
  if m.w_CONTA<=1 OR m.w_CONTA=LEN (m.w_PAROLA)
    i_retcode = 'stop'
    i_retval = .F.
    return
  endif
  m.w_POS = 1
  * --- Controllo la parte a sinistra del punto
  m.w_LEFTPAROLA = ""
  do while m.w_POS< m.w_CONTA
    if SUBSTR ( m.w_PAROLA , m.w_POS , 1) $m.w_SEPARATORI
      i_retcode = 'stop'
      i_retval = .F.
      return
    endif
    m.w_LEFTPAROLA = m.w_LEFTPAROLA + SUBSTR ( m.w_PAROLA , m.w_POS , 1)
    m.w_POS = m.w_POS + 1
  enddo
  * --- Controllo la parte a destra del punto
  m.w_POS = m.w_CONTA + 1 
  m.w_RIGHTPAROLA = ""
  do while m.w_POS<= LEN (m.w_PAROLA)
    if SUBSTR ( m.w_PAROLA , m.w_POS , 1) $ m.w_SEPARATORI
      i_retcode = 'stop'
      i_retval = .F.
      return
    endif
    m.w_RIGHTPAROLA = m.w_RIGHTPAROLA + SUBSTR ( m.w_PAROLA , m.w_POS , 1)
    m.w_POS = m.w_POS + 1
  enddo
endproc


  function checkrel_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='SUENDETT'
    return(cp_OpenFuncTables(1))
* --- END CHECKREL
* --- START LORELOOKTAB
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: lorelooktab                                                     *
*              Ricerca il codice cliente conoscendone la partita iva           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-09                                                      *
* Last revis.: 2010-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func lorelooktab
param pDatiRiga,pLog,pVerbose,pTabella,pCampo,pChiave1,pValore1,pChiave2,pValore2,pChiave3,pValore3,pChiave4,pValore4,pChiave5,pValore5,pChiave6,pValore6

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LETTO
  m.w_LETTO=space(11)
  private w_OLDLETTO
  m.w_OLDLETTO=space(11)
* --- WorkFile variables
  private CLI_VEND_idx
  CLI_VEND_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "lorelooktab"
if vartype(__lorelooktab_hook__)='O'
  __lorelooktab_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'lorelooktab('+Transform(pDatiRiga)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pTabella)+','+Transform(pCampo)+','+Transform(pChiave1)+','+Transform(pValore1)+','+Transform(pChiave2)+','+Transform(pValore2)+','+Transform(pChiave3)+','+Transform(pValore3)+','+Transform(pChiave4)+','+Transform(pValore4)+','+Transform(pChiave5)+','+Transform(pValore5)+','+Transform(pChiave6)+','+Transform(pValore6)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'lorelooktab')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if lorelooktab_OpenTables()
  lorelooktab_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'lorelooktab('+Transform(pDatiRiga)+','+Transform(pLog)+','+Transform(pVerbose)+','+Transform(pTabella)+','+Transform(pCampo)+','+Transform(pChiave1)+','+Transform(pValore1)+','+Transform(pChiave2)+','+Transform(pValore2)+','+Transform(pChiave3)+','+Transform(pValore3)+','+Transform(pChiave4)+','+Transform(pValore4)+','+Transform(pChiave5)+','+Transform(pValore5)+','+Transform(pChiave6)+','+Transform(pValore6)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'lorelooktab')
Endif
*--- Activity log
if vartype(__lorelooktab_hook__)='O'
  __lorelooktab_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure lorelooktab_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione applicabiel come filtro verticale dinamico . Funziona come la LookTab, ossia prende come parametro un tabella e un campo e dei filtri.
  *     Restituisce il valore letto dal campo (specificato come parametro) nella tabella (specificata come parametro) applicando i filtri (specificati come paraetri dalle coppie pChiave e pValore)
  *     La funzione legge i valori che user� come filtri dai campi presenti sul DBF da sincronizzare (i nomi dei campi sono specificati dai parametri pValori)
  *     Se non si sono specificati filtri oppure il valore letto dal database � EMPTY allora la funzione restituisce il valore del campo (specificato dal parametro pCampo) presente nel DBF da sincronizzare
  *     
  * --- pTabella indica il noeme della tabella del database su cui fare la lettura
  *     pCAmpo indica il nome del campo che bisogna leggere sulla tabella pTabella
  *     pChiave indica il nome dei campi della tabella usati come filtri 
  *     pValore indicano i valori da usare nei filtri 
  *     pDatiRiga � il nome del cursore che contiene i dati del DBF da sincronizzare
  *     pLog � il log dei messaggi
  *     pVerbose indica se � richiesta una messaggistica pi� dettagliata per il log (oltre ai messaggi di errore vengono visualizzati messaggi che indicano la cronologia delle operazioni che vengono svolte)
  * --- Crea le macro per effettuare le letture sul DBF
  l_Valore1 = IIF (vartype (m.pValore1)="C" , "pDatiRiga."+ alltrim (m.pValore1),m.pValore1 )
  l_Valore2 = IIF (vartype (m.pValore2)="C" , "pDatiRiga."+ alltrim (m.pValore2),m.pValore2 )
  l_Valore3 = IIF (vartype (m.pValore3)="C" , "pDatiRiga."+ alltrim (m.pValore3),m.pValore3 )
  l_Valore4 = IIF (vartype (m.pValore4)="C" , "pDatiRiga."+ alltrim (m.pValore4),m.pValore4 )
  l_Valore5 = IIF (vartype (m.pValore5)="C" , "pDatiRiga."+ alltrim (m.pValore5),m.pValore5 )
  * --- Lettura dati dal DBF
  if vartype (l_Valore1)="C"
    l_Valore1 = &l_Valore1
  else
    l_Valore1 = l_Valore1 
  endif
  if vartype (l_Valore2)="C"
    l_Valore2 = &l_Valore2
  else
    l_Valore2 = l_Valore2 
  endif
  if vartype (l_Valore3)="C"
    l_Valore3 = &l_Valore3
  else
    l_Valore3 =l_Valore3 
  endif
  if vartype (l_Valore4)="C"
    l_Valore4 =&l_Valore4
  else
    l_Valore4 =l_Valore4 
  endif
  if vartype (l_Valore5)="C"
    l_Valore5 = &l_Valore5
  else
    l_Valore5 = l_Valore5 
  endif
  if not empty (l_Valore1) AND not EMPTY (m.pTabella) AND not empty (m.pCampo) AND NOT EMPTY (m.pChiave1)
    m.w_LETTO = LookTab (m.pTabella,m.pCampo,m.pChiave1,l_Valore1 ,m.pChiave2,l_Valore2 ,m.pChiave3,l_Valore3 ,m.pChiave4,l_Valore4 ,m.pChiave5,l_Valore5 )
  else
    m.w_LETTO = ""
    if VarType( m.pLog ) ="O" And m.pVerbose
      AddMsgNL("Funzione LoreLookTab : mancano alcuni dei parametri obbligatori (tabella,campo da leggere e un filtro)")
    endif
  endif
  l_Campo="pDatiRiga."+ alltrim (m.pCampo)
  m.w_OLDLETTO = &l_Campo
  * --- Se non riesce a fare la trascodifica allora restituisce il valore del dato presente sul DBF
  if EMPTY ( NVL (m.w_LETTO,""))
    m.w_LETTO = m.w_OLDLETTO
  endif
  if VarType( m.pLog ) ="O" And m.pVerbose
    AddMsgNL("Funzione LoreLookTab applicata valore  %1   trasformato in %2 ", m.pLog, m.w_OLDLETTO, m.w_LETTO)
  endif
  i_retcode = 'stop'
  i_retval = m.w_LETTO
  return
endproc


  function lorelooktab_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='CLI_VEND'
    return(cp_OpenFuncTables(1))
* --- END LORELOOKTAB
