* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bas                                                        *
*              Aggiornamento saldi documenti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-25                                                      *
* Last revis.: 2014-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_AGGSAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bas",oParentObject,m.w_SERIAL,m.w_AGGSAL)
return(i_retval)

define class tgslr_bas as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_AGGSAL = 0
  * --- WorkFile variables
  SALDIART_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di aggiornamento saldi articoli in fase di sincronizzazione.
    *     Lanciato da FLR_DOCPRE e FLR_DOCPOS
    * --- w_AGGSAL valo + o -
    *     - Storno i saldi con i vecchi valori
    *     + Aggiorno i saldi con i nuovi valori
    *     
    *     - Viene lanciato da FLR_DOCPRE
    *     + Viene lanciato da FLR_DOCPOS
    * --- Aggiornamento saldi
    *     Non sono gestiti la data e il valore di ultimo prezzo di vendita/costo di acquisto
    *     Devo dividere in due scritture perch� nella write from query non posso utilizzare 
    *     come operatore una variabile. Nel prg viene generato il calcolo dell'operatore,
    *     dove viene utilizzato il cursore generato con la query, prima di avere eseguito la query.
    *     Ovviamente non viene trovato il cursore
    * --- Try
    local bErr_0304F990
    bErr_0304F990=bTrsErr
    this.Try_0304F990()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0304F990
    * --- End
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SCCODICE,SCCODMAG,SCCODCAN"
      do vq_exec with 'gslrcqas',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER = SALDICOM.SCQTAPER+_t2.ESIST";
          +",SCQTOPER = SALDICOM.SCQTOPER+_t2.ORDI";
          +",SCQTIPER = SALDICOM.SCQTIPER+_t2.IMPE";
          +",SCQTRPER = SALDICOM.SCQTRPER+_t2.RISE";
          +i_ccchkf;
          +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
          +"SALDICOM.SCQTAPER = SALDICOM.SCQTAPER+_t2.ESIST";
          +",SALDICOM.SCQTOPER = SALDICOM.SCQTOPER+_t2.ORDI";
          +",SALDICOM.SCQTIPER = SALDICOM.SCQTIPER+_t2.IMPE";
          +",SALDICOM.SCQTRPER = SALDICOM.SCQTRPER+_t2.RISE";
          +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = t2.SCCODCAN";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
          +"SCQTAPER,";
          +"SCQTOPER,";
          +"SCQTIPER,";
          +"SCQTRPER";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SCQTAPER+t2.ESIST,";
          +"SCQTOPER+t2.ORDI,";
          +"SCQTIPER+t2.IMPE,";
          +"SCQTRPER+t2.RISE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
          +"SCQTAPER = SALDICOM.SCQTAPER+_t2.ESIST";
          +",SCQTOPER = SALDICOM.SCQTOPER+_t2.ORDI";
          +",SCQTIPER = SALDICOM.SCQTIPER+_t2.IMPE";
          +",SCQTRPER = SALDICOM.SCQTRPER+_t2.RISE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
              +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
              +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER = (select "+i_cTable+".SCQTAPER+ESIST from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTOPER = (select "+i_cTable+".SCQTOPER+ORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTIPER = (select "+i_cTable+".SCQTIPER+IMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTRPER = (select "+i_cTable+".SCQTRPER+RISE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Try
    local bErr_0304E340
    bErr_0304E340=bTrsErr
    this.Try_0304E340()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0304E340
    * --- End
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'gslr_qas',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = SALDIART.SLQTAPER+_t2.ESIST";
          +",SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
          +",SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
          +",SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTAPER = SALDIART.SLQTAPER+_t2.ESIST";
          +",SALDIART.SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
          +",SALDIART.SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
          +",SALDIART.SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTAPER,";
          +"SLQTOPER,";
          +"SLQTIPER,";
          +"SLQTRPER";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTAPER+t2.ESIST,";
          +"SLQTOPER+t2.ORDI,";
          +"SLQTIPER+t2.IMPE,";
          +"SLQTRPER+t2.RISE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTAPER = SALDIART.SLQTAPER+_t2.ESIST";
          +",SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
          +",SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
          +",SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = (select "+i_cTable+".SLQTAPER+ESIST from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTOPER = (select "+i_cTable+".SLQTOPER+ORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTIPER = (select "+i_cTable+".SLQTIPER+IMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPER = (select "+i_cTable+".SLQTRPER+RISE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Nel caso non scrivo niente indico che non ho aggiornato i saldi
    i_retcode = 'stop'
    i_retval = i_Rows<>0
    return
  endproc
  proc Try_0304F990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- La query prende dal documento MVKEYSAL e MVCODMAG/T che non esistono 
    *     come chiave nei saldi.
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gslrcqis",this.SALDICOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0304E340()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- La query prende dal documento MVKEYSAL e MVCODMAG/T che non esistono 
    *     come chiave nei saldi.
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gslr_qis",this.SALDIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_SERIAL,w_AGGSAL)
    this.w_SERIAL=w_SERIAL
    this.w_AGGSAL=w_AGGSAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SALDIART'
    this.cWorkTables[2]='SALDICOM'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_AGGSAL"
endproc
