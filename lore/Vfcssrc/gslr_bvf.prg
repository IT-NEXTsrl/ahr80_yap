* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bvf                                                        *
*              Controllu sugli alias di campo duplicati                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-13                                                      *
* Last revis.: 2010-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bvf",oParentObject)
return(i_retval)

define class tgslr_bvf as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- VAlorizza la combo delle funzioni correttamente in base al valore del campo PV_VALUE
    this.w_PADRE = this.oparentobject
    this.w_PADRE.markpos()     
    this.w_PADRE.FirstRow ()     
    do while NOT this.w_PADRE.EOF_TRS()
      this.w_PADRE.Setrow()     
      if this.w_PADRE.fullrow()
        this.w_PADRE.Setrow()     
        this.oParentObject.w_FUNZIONE = IIF( this.oParentObject.w_PV_VALUE="TraslacliDaIVA ( 'ANPARIVA' , 'ANTIPCON' , 'ANCODICE')" , "TraslacliDaIVA ( 'ANPARIVA' , 'ANTIPCON' , 'ANCODICE')" , IIF ( this.oParentObject.w_PV_VALUE="TraslacliDaCF (  'ANCODFIS' , 'ANTIPCON' , 'ANCODICE' )" , "TraslacliDaCF (  'ANCODFIS' , 'ANTIPCON' , 'ANCODICE' )" , "Altro" ) )
        if this.oParentObject.w_PV_VALUE="LoreLookTab ('CLI_VEND','CLCODCLI','CLPARIVA','CLPARIVA')"
          this.oParentObject.w_FUNZIONE = "LoreLookTab ('CLI_VEND','CLCODCLI','CLPARIVA','CLPARIVA')"
        endif
        if this.oParentObject.w_PV_VALUE="LoreLookTab ('CLI_VEND','CLCODCLI','CLCODFIS','CLCODFIS')"
          this.oParentObject.w_FUNZIONE = "LoreLookTab ('CLI_VEND','CLCODCLI','CLCODFIS','CLCODFIS')"
        endif
        this.oParentObject.w_VALORE = IIF(this.oParentObject.w_FUNZIONE="Altro",this.w_PADRE.get ("t_PV_VALUE"),"")
        this.w_PADRE.Saverow()     
      endif
      this.w_PADRE.nextrow()     
    enddo
    this.w_PADRE.Repos()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
